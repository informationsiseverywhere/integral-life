/*
 * File: T5617pt.java
 * Date: 30 August 2009 2:24:13
 * Author: Quipoz Limited
 * 
 * Class transformed from T5617PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T5617rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5617.
*
*
*****************************************************************
* </pre>
*/
public class T5617pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(56).isAPartOf(wsaaPrtLine001, 20, FILLER).init("Sum Assured Term Based Surrender Factors           S5617");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(71);
	private FixedLengthStringData filler10 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 50, FILLER).init("Base Unit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(9, 0).isAPartOf(wsaaPrtLine004, 62).setPattern("ZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(19);
	private FixedLengthStringData filler12 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine005, 0, FILLER).init("   Premium Duration");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(8);
	private FixedLengthStringData filler13 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 0, FILLER).init("   Years");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(76);
	private FixedLengthStringData filler14 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine007, 12, FILLER).init("1      2      3     4      5      6      7      8      9     10");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(79);
	private FixedLengthStringData filler16 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 0, FILLER).init("   0+");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(79);
	private FixedLengthStringData filler26 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  10+");
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(79);
	private FixedLengthStringData filler36 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  20+");
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(79);
	private FixedLengthStringData filler46 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  30+");
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(79);
	private FixedLengthStringData filler56 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 0, FILLER).init("  40+");
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 73).setPattern("ZZZZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5617rec t5617rec = new T5617rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5617pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5617rec.t5617Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5617rec.unit);
		fieldNo008.set(t5617rec.insprm01);
		fieldNo009.set(t5617rec.insprm02);
		fieldNo010.set(t5617rec.insprm03);
		fieldNo011.set(t5617rec.insprm04);
		fieldNo012.set(t5617rec.insprm05);
		fieldNo013.set(t5617rec.insprm06);
		fieldNo014.set(t5617rec.insprm07);
		fieldNo015.set(t5617rec.insprm08);
		fieldNo016.set(t5617rec.insprm09);
		fieldNo017.set(t5617rec.insprm10);
		fieldNo018.set(t5617rec.insprm11);
		fieldNo019.set(t5617rec.insprm12);
		fieldNo020.set(t5617rec.insprm13);
		fieldNo021.set(t5617rec.insprm14);
		fieldNo022.set(t5617rec.insprm15);
		fieldNo023.set(t5617rec.insprm16);
		fieldNo024.set(t5617rec.insprm17);
		fieldNo025.set(t5617rec.insprm18);
		fieldNo026.set(t5617rec.insprm19);
		fieldNo027.set(t5617rec.insprm20);
		fieldNo028.set(t5617rec.insprm21);
		fieldNo029.set(t5617rec.insprm22);
		fieldNo030.set(t5617rec.insprm23);
		fieldNo031.set(t5617rec.insprm24);
		fieldNo032.set(t5617rec.insprm25);
		fieldNo033.set(t5617rec.insprm26);
		fieldNo034.set(t5617rec.insprm27);
		fieldNo035.set(t5617rec.insprm28);
		fieldNo036.set(t5617rec.insprm29);
		fieldNo037.set(t5617rec.insprm30);
		fieldNo038.set(t5617rec.insprm31);
		fieldNo039.set(t5617rec.insprm32);
		fieldNo040.set(t5617rec.insprm33);
		fieldNo041.set(t5617rec.insprm34);
		fieldNo042.set(t5617rec.insprm35);
		fieldNo043.set(t5617rec.insprm36);
		fieldNo044.set(t5617rec.insprm37);
		fieldNo045.set(t5617rec.insprm38);
		fieldNo046.set(t5617rec.insprm39);
		fieldNo047.set(t5617rec.insprm40);
		fieldNo048.set(t5617rec.insprm41);
		fieldNo049.set(t5617rec.insprm42);
		fieldNo050.set(t5617rec.insprm43);
		fieldNo051.set(t5617rec.insprm44);
		fieldNo052.set(t5617rec.insprm45);
		fieldNo053.set(t5617rec.insprm46);
		fieldNo054.set(t5617rec.insprm47);
		fieldNo055.set(t5617rec.insprm48);
		fieldNo056.set(t5617rec.insprm49);
		fieldNo057.set(t5617rec.insprm50);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
