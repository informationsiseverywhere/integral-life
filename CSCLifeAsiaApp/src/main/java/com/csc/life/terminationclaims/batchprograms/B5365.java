/*
 * File: B5365.java
 * Date: 29 August 2009 21:13:31
 * Author: Quipoz Limited
 *
 * Class transformed from B5365.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.dao.AnnypaypfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RgpdetpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.life.productdefinition.tablestructures.Ty501rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.RegrpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.RegxpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.life.terminationclaims.dataaccess.model.Regxpf;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.terminationclaims.tablestructures.T6762rec;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*REMARKS.
*              REGULAR PAYMENTS PROCESSING
*              ===========================
*
*  Overview
*
*  This batch program will process all of the Regular Payments
*  that are due to make their next payment.  It will be run
*  periodically, perhaps daily, although this is not strictly
*  necessary.  Where media runs are processed on a less than
*  daily basis it may not be necesary to run Regular Payments
*  every day.  Another consideration is that the job should be
*  run before any collection so that any Waiver of Premium
*  claims are able to post their monies to the appropriate
*  contract suspense accounts ready for the collection run to
*  pick up.
*  It runs directly after B5364, which 'splits' the REGP file to '
*  produce multiple temporary Regular Payments files, REGPXPF,
*  which will contain the payment records where the Next Payment
*  Date is less than or equal to the Effective Date of the run.
*
*  The processing will be driven by the details held on these
*  temporary Regular Payments files.
*
*  Each payment will be further screened by checking its
*  Payment Status against the allowable codes on table T6693.
*  Another screening will then be made by checking the risk
*  and premium status codes of the contract and associated
*  component against the allowable codes on table T5679.
*
*  Regular Payments that are past their Review Dates or Final
*  Payment Dates will not be processed.
*
*  The actual processing of the payment will be carried out by
*  generic subroutines.  These will be obtained from T5671 and
*  there may be up to four subroutines for each component.  They
*  will be called in succession and will perform all of the
*  detail processing.
*
*  The main program will then advance the Next Payment Date by
*  one frequency.  The payment will be processed again if the
*  Next Payment Date is still within the Effective Date of the
*  run.
*
*  If the Next Payment Date is advanced beyond the Final
*  Payment Date, which will happen if the Final Payment Date
*  does not fall on a payment anniversary, then the Next
*  Payment Date will be set to the Final Payment Date.
*
*  Any Anniversary processing will be performed by Anniversary
*  Processing subroutines and will be called from within the
*  generic subroutines themselves.
*
*  The following control totals are maintained within the
*  program:
*
*       CT01 - Number of regular payment records extracted
*       CT02 - Number of records processed
*       CT03 - Number of records rejected
*       CT04 - Number of locked contracts
*       CT05 - Payments with ineligible status
*       CT06 - Contracts with ineligible status
*       CT07 - Components with ineligible status
*       CT08 - Number of payments in review
*       CT09 - Number of payments Terminating
*
*
*  PROCESSING
*  Batch Splitter Program B5364
*
*  B5364 is run first to create temporary files containing only
*  those records from the Regular Payments file, REGP, whose
*  Next Payment Date is less than or equal to (not greater than)
*  the Effective Date of the run, and whose Company matches the
*  sign on company.  The file is sequenced as follows:
*
*       1. Contract Number     -  CHDRNUM
*       2. Component Code      -  CRTABLE
*       3. Transaction Number  -  TRANNO  (Descending)
*
*
*  COBOL Program
*
*  1000-INITIALISE section
*
*   Issue an override to read the correct REGXPF for this run.
*
*   The CRTTMPF process has already created a file specific to
*   the run using the REGXPF fields and is identified by
*   concatenating the following:-
*
*    'REGX'
*     BPRD-SYSTEM-PARAM04
*     BSSC-SCHEDULE-NUMBER
*
*    eg. REGX2B0001,  for the first run
*        REGX2B0002,  for the second etc.
*
*   The number of threads would have been created by the
*   CRTTMPF process given the parameters of the process
*   definition of the CRTTMPF.
*
*   To get the correct member of the above physical for B5365
*   to read from, concatenate the following:
*
*    'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*   Open the input file.
*
*   Frequently referenced tables are stored in working storage
*   arrays to minimise disk IO.  These tables include the
*   following:
*
*    T5671 - Generic Program Switching
*            Key: Transaction Code || CRTABLE
*    T5679 - Transaction Codes For Status
*            Key: Transaction Code
*    T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*    T6762 - Regular payments exception codes
*            Key: Program name
*
*    N.B. CRTABLE may be set to '****'.
*
*
*  2000-READ section
*
*   Read the REGX records sequentially incrementing control
*   total CT01.
*
*   If end of file move ENDP to WSSP-EDTERROR.
*
*
*  2500-EDIT section
*
*   Initialise WSSP-EDTERROR to OK.
*
*   Check contract for processing eligiblity.  If ineligible
*   increment control total CT06 and move SPACES to
*   WSSP-EDTERROR.
*
*   Read and validate the current transaction code held on T6693
*   against those on the extra data screen.
*
*   Read and validate the transaction code against those held
*   on T5679.
*
*   Read and validate the CHDR Risk Status and Premium Status
*   against those held on T5679.
*
*   Read and validate the COVR Risk Status and Premium Status
*   against those held on T5679.
*
*   If the Review Date is greater than or equal to the Next
*   Payment Date process next record.
*
*   'Soft lock' the contract, if it is to be processed.
*
*  3000-UPDATE section
*
*   Increment control total CT02.
*
*   Read and hold the regular payments record using logical
*   file REGP.
*
*   Read table T5671 with the Transaction Code and Component
*   Code to find any generic subroutines for the contract.
*
*   Set the REGP Validflag to '2' and perform a REWRT on the
*   REGP file.  Set it back to '1' and perform a KEEPS on REGP.
*
*   Check each of the four subroutine entries on T5671 in turn.
*   For each one that has an entry, call it using the copybook
*   REGPSUBREC.  On return from each subroutine check the return
*   status.  If this is not O-K then pass it to the fatal error
*   routine along with the rest of the linkage area and perform
*   fatal error processing.
*
*   When all of the subroutines have been successfully called
*   read CHDR with READH, rewrite the CHDR with a VALIDFLAG of
*   '2'.  Increment TRANNO by 1, set the Validflag back to '1'
*   and write the new record.
*
*   Write a PTRN record with the new incremented TRANNO.
*
*   Perform a RETRV on REGP.  Set the Last Paid Date to the Next
*   Payment Date and advance the Next Payment Date by one
*   Regular Payment Frequency.
*
*   If the new Next Payment Date is greater than the Final
*   Payment Date then set the Next Payment Date to the Final
*   Payment Date.
*
*   Place the new TRANNO on REGP and perform a WRITR on REGP.
*
*   If the Next Payment Date is still less than or equal to the
*   Effective Date then process the Regular Payment again.
*
*   Remove the 'soft lock' from the contract.
*
*   Read the next REGP record.
*
*
*  4000-CLOSE section
*
*    Close the input file.
*
*    Delete the override function for the REGXPF file.
*
*
*    Error Processing:
*
*      Perform the 600-FATAL-ERROR section. The
*      SYSR-SYSERR-TYPE flag does not need to be set in this
*      program, because MAINB takes care of a system errors.
*
*          (BATD processing is handled in MAINB)
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5365 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5365");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
		/*  These fields are required by MAINB processing and should not
		  be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/*  REGX parameters.*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	protected FixedLengthStringData wsaaRegxFn = new FixedLengthStringData(10);
	protected FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaRegxFn, 0, FILLER).init("REGX");
	protected FixedLengthStringData wsaaRegxRunid = new FixedLengthStringData(2).isAPartOf(wsaaRegxFn, 4);
	protected ZonedDecimalData wsaaRegxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaRegxFn, 6).setUnsigned();

	protected FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	protected FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	protected ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

		/* Arrays to store regularly referenced data to reduce the number
		 of accesses to the ITEMPF.
		 Array sizes are also delared so that an increase of table size
		 does not impact its processing.
		 Table T6693 - Allowable Actions for Status*/
	private FixedLengthStringData wsaaT6693Key1 = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat1 = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 0);
	private FixedLengthStringData wsaaT6693Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key1, 2);
	//private static final int wsaaT6693Size = 200;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6693Size = 1000;

		/* WSAA-T6693-ARRAY */
	private FixedLengthStringData[] wsaaT6693Rec = FLSInittedArray (1000, 78);
	private FixedLengthStringData[] wsaaT6693Key = FLSDArrayPartOfArrayStructure(6, wsaaT6693Rec, 0);
	private FixedLengthStringData[] wsaaT6693Paystat = FLSDArrayPartOfArrayStructure(2, wsaaT6693Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6693Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT6693Key, 2, HIVALUES);
	private FixedLengthStringData[][] wsaaT6693Data = FLSDArrayPartOfArrayStructure(12, 6, wsaaT6693Rec, 6);
	private FixedLengthStringData[][] wsaaT6693Rgpystat = FLSDArrayPartOfArrayStructure(2, wsaaT6693Data, 0);
	private FixedLengthStringData[][] wsaaT6693Trcode = FLSDArrayPartOfArrayStructure(4, wsaaT6693Data, 2);

		/* Table T5671 - Coverage/Rider Switching*/
	private FixedLengthStringData wsaaT5671Key1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Transcd1 = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key1, 0);
	private FixedLengthStringData wsaaT5671Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key1, 4);
	//private static final int wsaaT5671Size = 500
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5671Size = 1000;

		/* WSAA-T5671-ARRAY
		03  WSAA-T5671-REC            OCCURS 50
		03  WSAA-T5671-REC            OCCURS 250               <V65L21>*/
	private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray (1000, 48);
	private FixedLengthStringData[] wsaaT5671Key = FLSDArrayPartOfArrayStructure(8, wsaaT5671Rec, 0);
	private FixedLengthStringData[] wsaaT5671Transcd = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Data = FLSDArrayPartOfArrayStructure(40, wsaaT5671Rec, 8);
	private FixedLengthStringData[] wsaaT5671Subprogs = FLSDArrayPartOfArrayStructure(40, wsaaT5671Data, 0);
	private FixedLengthStringData[][] wsaaT5671Subprog = FLSDArrayPartOfArrayStructure(4, 10, wsaaT5671Subprogs, 0);
		/* Table T6689 - Payment reason codes*/
	//private static final int wsaaT6689Size = 50;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6689Size = 1000;

		/* WSAA-T6689-ARRAY */
	private FixedLengthStringData[] wsaaT6689Rec = FLSInittedArray (1000, 4);
	private FixedLengthStringData[] wsaaT6689Key = FLSDArrayPartOfArrayStructure(4, wsaaT6689Rec, 0, HIVALUES);
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String ivrm = "IVRM";

	private FixedLengthStringData wsaaItemFound = new FixedLengthStringData(1).init("N");
	private Validator itemFound = new Validator(wsaaItemFound, "Y");

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");

	private FixedLengthStringData wsaaValidTrcode = new FixedLengthStringData(1).init("N");
	private Validator validTrcode = new Validator(wsaaValidTrcode, "Y");

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysRegpkey = new FixedLengthStringData(20).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysRegpkey, 0);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 20);
		/* WSAA-MISCELLANEOUS */
	protected String wsaaStopPayment = "";
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private IntegerData wsaaSubprogIx = new IntegerData();
	
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private T5671rec t5671rec = new T5671rec();
	protected T5679rec t5679rec = new T5679rec();
	private T6693rec t6693rec = new T6693rec();
	private T6762rec t6762rec = new T6762rec();
	private Regpsubrec regpsubrec = new Regpsubrec();
	//ILIFE-1138 STARTS
	private Ty501rec ty501rec = new Ty501rec();
	//ILIFE-1138 ENDS
	protected int intBatchID = 0;
	protected int intBatchExtractSize;
	protected Iterator<Regxpf> iteratorList;
	protected RegxpfDAO regxpfDAO = getApplicationContext().getBean("regxpfDAO", RegxpfDAO.class);
	protected RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private RegrpfDAO regrpfDAO = getApplicationContext().getBean("regrpfDAO", RegrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Regxpf regxpfRec;
	protected Regppf regpIO;
	private RegpTableDAM regpIOTableDAM = new RegpTableDAM();
	protected Chdrpf chdrrgpIO;
	protected int curTranno = -1;
	private Map<String, Integer> trannoMap = null;
	private Map<String, List<Itempf>> t5679Map = null;
	private Map<String, List<Itempf>> t6762Map = null;
	private Map<String, List<Itempf>> t6693Map = null;
	private Map<String, List<Itempf>> t5671Map = null;
	private Map<String, List<Itempf>> t6689Map = null;
	private Map<String, List<Itempf>> ty501Map = null;
	protected Map<String,List<Regppf>> regpMap = null;
	protected Map<String,List<Chdrpf>> chdrrgpMap = null;
	protected Map<String,List<Covrpf>> covrMap = null;
	protected Map<String,List<Payrpf>> payrMap = null;
	
	private List<Regrpf> insertRegrpfList;
	protected Map<String, List<Chdrpf>> insertChdrpayMap;
	protected Map<Long, Chdrpf> updateChdrpayMap;
	protected Map<String, List<Payrpf>> insertPayrMap;
	protected Map<Long, Payrpf> updatePayrMap;
	private List<Ptrnpf> insertPtrnList;
	protected Map<Long, Regppf> updateRegpMap;
	private List<Long> updateRegpenqMap;
	protected Map<String, List<Regppf>> insertRegpenqMap;
	private Payrpf lastPayrpf = null;
	private int wsaaT6689Ix = 1;
	private int wsaaT5671Ix = 1;
	private int wsaaT6693Ix = 1;
	/* CONTROL-TOTALS */
	private int ct01Value = 0;
	protected int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private int ct06Value = 0;
	private int ct07Value = 0;
	protected int ct08Value = 0;
	protected int ct09Value = 0;
	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);//ILIFE-3955
	protected Itempf itempf = null;
	private Td5h7rec td5h7rec = new Td5h7rec();
	private RgpdetpfDAO rgpdetpfDAO = getApplicationContext().getBean("rgpdetpfDAO", RgpdetpfDAO.class);//ILIFE-3955
	private Rgpdetpf rgpdetpf = null;
	private static final String rrhf = "RRHF";
	//=====ICIL-554=======
	protected boolean ctcnl002Permission = false; 
	//String IW from table t3623
	protected String payResonCode="WP";
	private String statcode="IW";
	
	private String prevChdrnum = new String();
	private String currChdrnum = new String();
	protected boolean canWritePtrn =false;
	boolean canWriteChdr =false;
	protected Map<String, List<Itempf>> t6694ListMap;
	private T6694rec t6694rec = new T6694rec();
	private FixedLengthStringData wsaaWopFlag = new FixedLengthStringData(1);
	private String wsaaSacscode = "LP";
	private String wsaaSacstype = "WO"; 
	private Validator wopMop = new Validator(wsaaWopFlag, "Y");
	private PackedDecimalData wsaaSinstamt05 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinstamt06 = new PackedDecimalData(17, 2);
	boolean CMRPY005Permission  = false;
	protected String rgpytype = new String();
	private String PmntTerm = "PT";
	private String PmntInProg = "PI";
	private String statPaid = "PD";
	private final String statPend = "PP";
	private Annyregpf annyregpf = null;
	private AnnyregpfDAO annyregpfDAO = getApplicationContext().getBean("annyregpfDAO" , AnnyregpfDAO.class);
	private Annypaypf annypaypf = new Annypaypf();
	private AnnypaypfDAO annypaypfDAO = getApplicationContext().getBean("annypaypfDAO" , AnnypaypfDAO.class);
	protected List<Annypaypf> annypaypfList;
	protected List<Annypaypf> annypaypfList1;
	protected List<Annypaypf> annypaypfList3;
	boolean suotr008Permission = false;
	private String premter = "TR";
	private String tranCodeCreate = "TASJ";
	private String tranCodeModify = "TASK";
	boolean firstRead = true;
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readNext1190,
		exit1199,
		rejectRecord2540,
		exit2549,
		notFound2690,
		exit2699
	}

	public B5365() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*START*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
	
	   suotr008Permission  = FeaConfg.isFeatureExist("2", "SUOTR008", appVars, "IT");
	   ctcnl002Permission  = FeaConfg.isFeatureExist("2", "BTPRO013", appVars, "IT");  //ICIL-554
	   CMRPY005Permission  = FeaConfg.isFeatureExist("2", "CMRPY005", appVars, "IT");
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of REGXPF.*/
		wsaaRegxRunid.set(bprdIO.getSystemParam04());
		wsaaRegxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(REGXPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaRegxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Call routine to get processing date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* set up the TRANID.*/
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmUser.set(999999);
		varcom.vrcmTermid.set(SPACES);
		/* Open required files.*/
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		readChunkRecord();
		/* Read T5679 for valid contract statii.*/
		String coy = bsprIO.getCompany().toString();
		
		t5679Map = itemDAO.loadSmartTable("IT", coy, "T5679");
		if(t5679Map != null && t5679Map.containsKey(bprdIO.getAuthCode())){
			Itempf itempf = t5679Map.get(bprdIO.getAuthCode()).get(0);
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}else{
			syserrrec.params.set("T5679:" + bprdIO.getAuthCode());
			fatalError600();
		}
		
		/* Read T6762 to get valid exception codes.*/
		t6762Map = itemDAO.loadSmartTable("IT", coy, "T6762");
		if(t6762Map != null && t6762Map.containsKey(bprdIO.getBatchProgram())){
			Itempf itempf = t6762Map.get(bprdIO.getBatchProgram()).get(0);
			t6762rec.t6762Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}else{
			syserrrec.params.set("T6762:" + bprdIO.getBatchProgram());
			fatalError600();
		}
		
		/* Read T6693 for valid transaction codes*/
		wsaaT6693Ix = 1;
		t6693Map = itemDAO.loadSmartTable("IT", coy, "T6693");
		loadT66931100();
	
		/* Read T5671 for coverage switching details*/
		wsaaT5671Ix = 1;
		t5671Map = itemDAO.loadSmartTable("IT", coy, "T5671");
		loadT56711200();
	
		/* Read T6689 for reason codes*/
		wsaaT6689Ix = 1;
		t6689Map = itemDAO.loadSmartTable("IT", coy, "T6689");
		loadT66891300();
		
		ty501Map = itemDAO.loadSmartTable("IT", coy, "TY501");
		t6694ListMap = itemDAO.loadSmartTable("IT", coy, "T6694");
		regpIO = new Regppf();
	}

protected void loadT66931100()
 {
		if (t6693Map == null || t6693Map.isEmpty()) {
			return;
		}
		for (String itemitem : t6693Map.keySet()) {
			for (Itempf item : t6693Map.get(itemitem)) {
				if (wsaaT6693Ix > wsaaT6693Size) {
					syserrrec.statuz.set(h791);
					syserrrec.params.set("t6693");
					fatalError600();
				}

				if (item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) > 0
						|| item.getItmto().compareTo(bsscIO.getEffectiveDate().getbigdata()) < 0) {
					continue;
				}
				t6693rec.t6693Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT6693Key[wsaaT6693Ix].set(item.getItemitem());
				for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)) {
					wsaaT6693Trcode[wsaaT6693Ix][wsaaSub.toInt()].set(t6693rec.trcode[wsaaSub.toInt()]);
					wsaaT6693Rgpystat[wsaaT6693Ix][wsaaSub.toInt()].set(t6693rec.rgpystat[wsaaSub.toInt()]);
				}
				wsaaT6693Ix++;
			}
		}
	}

protected void loadT56711200()
 {
		if (t5671Map == null || t5671Map.isEmpty()) {
			return;
		}
		for (String itemitem : t5671Map.keySet()) {
			wsaaT5671Key1.set(itemitem);
			if (isNE(wsaaT5671Transcd1, bprdIO.getAuthCode())) {
				continue;
			}
			for (Itempf item : t5671Map.get(itemitem)) {
				if (wsaaT5671Ix > wsaaT5671Size) {
					syserrrec.statuz.set(h791);
					syserrrec.params.set("t5671");
					fatalError600();
				}
				if (isNE(item.getItemseq(), SPACES)) {
					continue;
				}
				t5671rec.t5671Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT5671Key[wsaaT5671Ix].set(item.getItemitem());
				wsaaT5671Subprogs[wsaaT5671Ix].set(t5671rec.subprogs);
				wsaaT5671Ix++;
			}

		}
	}

protected void loadT66891300()
 {
		if (t6689Map == null || t6689Map.isEmpty()) {
			return;
		}
		for (String itemitem : t6689Map.keySet()) {
			for (Itempf item : t6689Map.get(itemitem)) {
				if (wsaaT6689Ix > wsaaT6689Size) {
					syserrrec.statuz.set(h791);
					syserrrec.params.set("t6689");
					fatalError600();
				}
				wsaaT6689Key[wsaaT6689Ix].set(item.getItemitem());
				wsaaT6689Ix++;
			}
		}
	}
protected void readChunkRecord() {
	List<Regxpf> regxpfList = regxpfDAO.searchRegxpfRecord(wsaaRegxFn.toString(), wsaaThreadMember.toString(),
			intBatchExtractSize, intBatchID);
	iteratorList = regxpfList.iterator();
	if (regxpfList != null && !regxpfList.isEmpty()) {
		List<String> chdrnumSet = new ArrayList<>();
		for (Regxpf r : regxpfList) {
			chdrnumSet.add(r.getChdrnum());
		}
		regpMap = regppfDAO.searchRegppfByChdrnum(chdrnumSet);
		chdrrgpMap = chdrpfDAO.searchChdrmjaByChdrnum(chdrnumSet);
		covrMap = covrpfDAO.searchCovrBenbilldate(bsprIO.getCompany().trim(), chdrnumSet);
		payrMap  = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrnumSet);
	}
}
protected void readFile2000()
	{
		/*START*/
		/* Read the REGP records in turn.*/
		if (iteratorList != null && iteratorList.hasNext()) {
			regxpfRec = iteratorList.next();
			currChdrnum = regxpfRec.getChdrnum();
			ct01Value++;
			/* Set up the key for the SYSR- copybook should a system error*/
			/* for this instalment occur.*/
			wsysChdrnum.set(regxpfRec.getChdrnum());
			/* Set up the keyc(regxpfRec.getChdrnum());
			/*EXIT*/
		} else {
			intBatchID++;
			clearList2100();
			readChunkRecord();
			if (iteratorList != null && iteratorList.hasNext()) {
				regxpfRec = iteratorList.next();	
				currChdrnum = regxpfRec.getChdrnum();
				ct01Value++;
				wsysChdrnum.set(regxpfRec.getChdrnum());
			} else {
				wsspEdterror.set(varcom.endp);
				if(canWritePtrn && isNE(prevChdrnum,SPACES)) {
					writePtrn3700();
					canWritePtrn =false;
				}
				return ;
			}
		}
		
		if (isNE(currChdrnum,prevChdrnum) ){
			if(canWritePtrn && isNE(prevChdrnum,SPACES)) {
				writePtrn3700();
				canWritePtrn =false;
			}
			
			
			prevChdrnum = currChdrnum;
		}
	}
private void clearList2100() {
	if (regpMap != null) {
		regpMap.clear();
	}
	if (chdrrgpMap != null) {
		chdrrgpMap.clear();
	}
	if (payrMap != null) {
		payrMap.clear();
	}
	if(trannoMap != null){
	    trannoMap.clear();
	}
	lastPayrpf = null;
}
protected void edit2500()
	{
		if(start2500()){
			ct03Value++;
			regpIO = new Regppf(); // ILIFE-6644
		}
	}

protected boolean start2500()
	{
		/* Perform validation of input record.*/
		/* If valid 'soft lock' the contract and continue processing.*/
		/* Otherwise reject record and return for next in file.*/
	   wsspEdterror.set(varcom.oK);
	   /* Perform a RLSE on REGP to make sure the D/B is not locked.*/
	  if(!firstRead) {//ILB-1320 For the first read, there is not any inquriy for the regpIOTableDAM
		regpIOTableDAM.setFunction(varcom.rlse);
	
		SmartFileCode.execute(appVars, regpIOTableDAM);
		if (isNE(regpIOTableDAM.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIOTableDAM.getParams());
			syserrrec.statuz.set(regpIOTableDAM.getStatuz());
			fatalError600();
		}
		/* If the record read matches the previous record down to*/
		/* coverage key and regular payments number, if the*/
		/* call to the T5671 subroutines returned an error read*/
		/* the next record.*/
		if (regxpfRec.getChdrnum().equals(regpIO.getChdrnum())
		&& regxpfRec.getNextPaydate() == regpIO.getNextPaydate()
		&& regxpfRec.getCoverage().equals(regpIO.getCoverage())
		&& regxpfRec.getRider().equals(regpIO.getRider())
		&& regxpfRec.getPlanSuffix() == regpIO.getPlanSuffix()
		&& isNE(regpsubrec.subStatuz,varcom.oK)) {
			wsspEdterror.set(SPACES);
			return true;
		}
		}
		/* Read and hold the REGP record, we need some of the info*/
		/* for writting to the REGR file if there are exception errors*/
		readRegp2550();
		checkT66932600();
		if (isEQ(wsspEdterror,SPACES)) {
			return true;
		}
		readChdrCovr2700();
		if (isEQ(wsspEdterror,SPACES)) {
			return true;
		}
		softlock2900();
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			ct04Value++;
			Regrpf regrIO = new Regrpf();
			wsspEdterror.set(SPACES);
			regrIO.setExcode(t6762rec.regpayExcpCsfl.toString());
			regrIO.setExreport("Y");
			reportProcess5000(regrIO);
			return true;
		}
		return false;
	}

protected void readRegp2550()
 {
		if (regpMap != null && regpMap.containsKey(regxpfRec.getChdrnum())) {
			for (Regppf r : regpMap.get(regxpfRec.getChdrnum())) {
				if (regxpfRec.getLife().equals(r.getLife()) && regxpfRec.getChdrcoy().equals(r.getChdrcoy())
						&& regxpfRec.getCoverage().equals(r.getCoverage()) && regxpfRec.getRider().equals(r.getRider())
						&& regxpfRec.getRgpynum() == r.getRgpynum()) {
					regpIO = r;
					break;
				}
			}

		}
	}
protected void checkT66932600()
	{
		if(!start2600()){
			notFound2690();
		}
	}

protected boolean start2600()
	{
		/* Check through T6693 to find allowable actions from current*/
		/* status.*/
		/* If actions from current status found read through array of*/
		/* allowable transactions to determine if current transaction*/
		/* valid.*/
		wsaaT6693Ix = 1;
		wsaaT6693Paystat1.set(regxpfRec.getRgpystat());
		wsaaT6693Crtable1.set(regxpfRec.getCrtable());
		 searchlabel1:
		{
			for (; isLT(wsaaT6693Ix,wsaaT6693Rec.length); wsaaT6693Ix++){
				if (isEQ(wsaaT6693Key[wsaaT6693Ix],wsaaT6693Key1)) {
					itemFound.setTrue();
					break searchlabel1;
				}
			}
			wsaaItemFound.set("N");
		}
		if (!itemFound.isTrue()) {
			wsaaT6693Ix = 1;
			wsaaT6693Crtable1.set("****");
			 searchlabel2:
			{
				for (; isLT(wsaaT6693Ix,wsaaT6693Rec.length); wsaaT6693Ix++){
					if (isEQ(wsaaT6693Key[wsaaT6693Ix],wsaaT6693Key1)) {
						itemFound.setTrue();
						break searchlabel2;
					}
				}
			}
		}
		if (!itemFound.isTrue()) {
			return false;
		}
		wsaaValidTrcode.set("N");
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			if (isEQ(bprdIO.getAuthCode(),wsaaT6693Trcode[wsaaT6693Ix][wsaaSub.toInt()])) {
				validTrcode.setTrue();
			}
		}
		if (validTrcode.isTrue()) {
			return true;
		}
		return false;
	}

protected void notFound2690()
	{
		ct05Value++;
		wsspEdterror.set(SPACES);
		Regrpf regrIO = new Regrpf();
		regrIO.setExcode(t6762rec.regpayExcpIpst.toString());
		regrIO.setExreport("Y");
		reportProcess5000(regrIO);
	}

protected void readChdrCovr2700()
 {
		/* Read the contract header and validate the status against those */
		/* on T5679. */
		/* Read in the relevant contract details. */

		if (chdrrgpMap != null && chdrrgpMap.containsKey(regxpfRec.getChdrnum())) {
			for (Chdrpf c : chdrrgpMap.get(regxpfRec.getChdrnum()))
				if ("CH".equals(c.getChdrpfx()) && bsprIO.getCompany().toString().equals(c.getChdrcoy().toString())) {
					chdrrgpIO = c;
					break;
				}
		}
		if (chdrrgpIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		}

		/* Validate the contract statii against T5679. */
		wsaaValidStatus.set("N");
		wsaaStatcode.set(chdrrgpIO.getStatcode());
		wsaaPstcde.set(chdrrgpIO.getPstcde());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)) {
			validateChdrStatus2720();
		}
		if (!validStatus.isTrue()) {
			ct06Value++;
			wsspEdterror.set(SPACES);
			Regrpf regrIO = new Regrpf();
			regrIO.setExcode(t6762rec.regpayExcpIchs.toString());
			regrIO.setExreport("Y");
			reportProcess5000(regrIO);
			return;
		}
		/* Read the coverage record and validate the status against those */
		/* on T5679. */
		/* Read in the relevant COVR record. */
		Covrpf covrIO = null;
		if (covrMap != null && covrMap.containsKey(regxpfRec.getChdrnum())) {
			for (Covrpf c : covrMap.get(regxpfRec.getChdrnum())) {
				if (c.getChdrcoy().equals(bsprIO.getCompany().toString()) && c.getLife().equals(regxpfRec.getLife())
						&& c.getCoverage().equals(regxpfRec.getCoverage()) && c.getRider().equals(regxpfRec.getRider())
						&& c.getPlanSuffix() == regxpfRec.getPlanSuffix()) {
					covrIO = c;
					break;
				}
			}
		}
		if (covrIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		} else {//IJTI-320 START
			/* Validate the coverage statii against T5679. */
			wsaaValidStatus.set("N");
			// ILIFE-1138 STARTS
			if ((isEQ(wsaaStatcode, "RD") || isEQ(wsaaStatcode, "DH")) && isEQ(wsaaPstcde, "DH")) {
				wsaaStatcode.set(covrIO.getStatcode());
				wsaaPstcde.set(covrIO.getPstatcode());
				readTy501Covrstat(covrIO.getCrtable());
			} else {
				wsaaStatcode.set(covrIO.getStatcode());
				wsaaPstcde.set(covrIO.getPstatcode());
				for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)) {
					validateCovrStatus2750();
				}
			}
		}
		//IJTI-320END
		// ILIFE-1138 ENDS
		if (!validStatus.isTrue()) {
			ct07Value++;
			wsspEdterror.set(SPACES);
			Regrpf regrIO = new Regrpf();
			regrIO.setExcode(t6762rec.regpayExcpIcos.toString());
			regrIO.setExreport("Y");
			reportProcess5000(regrIO);
		}

	}

protected void validateChdrStatus2720()
	{
		/*START*/
		/* Validate contract risk status*/
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()],wsaaStatcode)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()],wsaaPstcde)) {
					wsaaSub.set(13);
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

protected void validateCovrStatus2750()
	{
		/*START*/
		/* Validate coverage risk status*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()],wsaaStatcode)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()],wsaaPstcde)) {
					wsaaSub.set(13);
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

//ILIFE-1138 STARTS
protected void  readTy501Covrstat(String crtable) 
 {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(crtable);
		stringVariable1.addExpression(bprdIO.getAuthCode());
		String keyItem = stringVariable1.toString();
		if (ty501Map != null && ty501Map.containsKey(keyItem)) {
			Itempf itemIO = ty501Map.get(keyItem).get(0);
			ty501rec.ty501Rec.set(StringUtil.rawToString(itemIO.getGenarea()));
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || validStatus.isTrue()); wsaaSub.add(1)) {
				if (isEQ(ty501rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
					for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || validStatus.isTrue()); wsaaSub.add(1)) {
						if (isEQ(ty501rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
							validStatus.setTrue();
						}
					}
				}
			}
		}
	}
//ILIFE-1138 ENDS

protected void softlock2900()
	{
		/* 'Soft lock' the contract, if it is to be processed.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(regxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(regxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void update3000()
	{
		/* Perform update processing.*/
		/* If the situation arises of the Next Payment Date being equal,*/
		/* or being set equal, to the Final Payment Date while still*/
		/* being less than or equal to the Effective Date then a loop*/
		/* will occur.  To avoid this the indicator WSAA-STOP-PAYMENT is*/
		/* set to various values depending on whether it is the first*/
		/* time (N), the second or last time (L) or processing complete*/
		/* (Y).  Processing proceeds on this basis.*/
		wsaaStopPayment = "N";
		if (isEQ(regpIO.getNextPaydate(),regpIO.getFinalPaydate())) {
			wsaaStopPayment = "L";
		}
		while ( !((isGT(regpIO.getNextPaydate(),regpIO.getRevdte()))
		|| (isGT(regpIO.getNextPaydate(),bsscIO.getEffectiveDate()))
		|| (isEQ(wsaaStopPayment,"Y")))) {
			updatePayments3100();
		}
	
		/* Increment no of records processed*/
		/* note: this is at record level not instalment*/
		ct02Value++;
		/* Check if the record is going into review and still eligible*/
		/* to be processed.*/
		if (isGTE(regpIO.getNextPaydate(),regpIO.getRevdte())
		&& isLTE(regpIO.getNextPaydate(),bsscIO.getEffectiveDate())) {
			ct08Value++;
		}
		/* Check if the record is going to be terminated and is still*/
		/* eligible to be processed.*/
		if (isGTE(regpIO.getNextPaydate(),regpIO.getFinalPaydate())) {
			ct09Value++;
		}
		/* PERFORM A000-STATISTICS.                             <LA4557>*/
		if (isEQ(wsaaStopPayment, "Y")) {
			/*CONTINUE_STMT*/
		}
		else {
			a000Statistics();
		}
		releaseSoftlock3900();
	}

protected void updatePayments3100()
	{
		/*START*/
		/* Perform update payments processing.*/
		checkT56713300();
		updateRegpValidflag3400();
		genericProcessing3500();
		if (isEQ(wsaaStopPayment,"Y")) {
			return ;
		}
		if(suotr008Permission && isEQ(chdrrgpIO.getStatcode(),"MA") && isEQ(chdrrgpIO.getPstcde(),"AR"))
		{
			if(!canWriteChdr)
			{
			   rewriteChdr3600();
			   canWriteChdr = true;
			}
		}
		else
		{
			rewriteChdr3600();
		}
		
		rewritePayr3200();
		
		if(suotr008Permission)
		{
			if(isEQ(chdrrgpIO.getStatcode(),"MA") && isEQ(chdrrgpIO.getPstcde(),"AR"))
			{	
			    updateAnnypaypf();
		    	updateAnnyregpf();
			}
			
			annypaypfList3 = annypaypfDAO.getAnnypaypfRecords(regpIO.getChdrcoy(), regpIO.getChdrnum().trim());
		   if(!annypaypfList3.isEmpty() &&  isEQ(chdrrgpIO.getStatcode(),"MA") && isEQ(chdrrgpIO.getPstcde(),"AR"))
			   updateRegpf();
		   else
			   updateRegp3800();
		}
		else
		{
			updateRegp3800();
		}
		
		canWritePtrn = true;
		/*EXIT*/
	}

protected void rewritePayr3200()
 {
		/* Obtain the PAYR record */
        if(lastPayrpf == null || !chdrrgpIO.getChdrnum().equals(lastPayrpf.getChdrnum())){
    		if (payrMap != null && payrMap.containsKey(chdrrgpIO.getChdrnum())) {
    			for (Payrpf p : payrMap.get(chdrrgpIO.getChdrnum())) {
    				if (chdrrgpIO.getChdrcoy().toString().equals(p.getChdrcoy())) {
    				    lastPayrpf = p;
    					break;
    				}
    			}
    		}
        }
        if (lastPayrpf == null) {
            syserrrec.params.set(chdrrgpIO.getChdrnum());
            fatalError600();
        }
		/* Write validflag '2' Payer (PAYR) record */

		if (updatePayrMap == null) {
		    updatePayrMap = new LinkedHashMap<Long, Payrpf>();
		}
		if(!updatePayrMap.containsKey(lastPayrpf.getUniqueNumber())){
		    Payrpf updatePayr = new Payrpf(lastPayrpf);
		    updatePayr.setValidflag("2");
		    updatePayrMap.put(lastPayrpf.getUniqueNumber(), updatePayr);
		}

		if (insertPayrMap == null) {
		    insertPayrMap = new LinkedHashMap<>();
		}
		Payrpf insertPayr = new Payrpf(lastPayrpf);
		insertPayr.setTranno(curTranno);
		insertPayr.setValidflag("2");// will update it before commit
		if(insertPayrMap.containsKey(insertPayr.getChdrnum())){
		    insertPayrMap.get(insertPayr.getChdrnum()).add(insertPayr);
		}else{
		    List<Payrpf> insertPayrList = new LinkedList<>();
		    insertPayrList.add(insertPayr);
		    insertPayrMap.put(insertPayr.getChdrnum(), insertPayrList);
		}
		lastPayrpf = insertPayr;
	}

protected void checkT56713300()
	{
		/*START*/
		/* Check through T5671 to find any generic subroutines for*/
		/* contract*/
		wsaaT5671Transcd1.set(bprdIO.getAuthCode());
		wsaaT5671Crtable1.set(regxpfRec.getCrtable());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5671Rec);
		IntegerData wsaaT5671IxInt = new IntegerData(wsaaT5671Ix);
		as1.setIndices(wsaaT5671IxInt);
		as1.addSearchKey(wsaaT5671Key, wsaaT5671Key1, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
			wsaaT5671Ix = wsaaT5671IxInt.toInt();
		}
		else {
			return ;
		}
		/*EXIT*/
	}

protected void genericProcessing3500()
	{
		/*START*/
		/* Perform section to call generic subroutine(s) if present.*/
		for (wsaaSubprogIx.set(1); !(isGT(wsaaSubprogIx,4)
		|| isEQ(wsaaStopPayment,"Y")); wsaaSubprogIx.add(1)){
			if (isNE(wsaaT5671Subprog[wsaaT5671Ix][wsaaSubprogIx.toInt()],SPACES)) {
				callGeneric3550();
			}
		}
		/*EXIT*/
	}
protected void regpTableDAMRead()
{
	/* Read the Regular Payments record.*/
	/* Don't lock it at this stage as it needs to be*/
	/* locked per payment due*/
	regpIOTableDAM.setDataArea(SPACES);
	regpIOTableDAM.setChdrcoy(regxpfRec.getChdrcoy());
	regpIOTableDAM.setChdrnum(regxpfRec.getChdrnum());
	regpIOTableDAM.setLife(regxpfRec.getLife());
	regpIOTableDAM.setCoverage(regxpfRec.getCoverage());
	regpIOTableDAM.setRider(regxpfRec.getRider());
	regpIOTableDAM.setRgpynum(regxpfRec.getRgpynum());
	regpIOTableDAM.setFunction(varcom.readr);
	regpIOTableDAM.setFormat("REGPREC");
	SmartFileCode.execute(appVars, regpIOTableDAM);
	if (isNE(regpIOTableDAM.getStatuz(),varcom.oK)) {
		syserrrec.params.set(regpIOTableDAM.getParams());
		syserrrec.statuz.set(regpIOTableDAM.getStatuz());
		fatalError600();
	}
	firstRead = false;
}
protected void updateRegpValidflag3400()
{
	/*START*/
	regpTableDAMRead();
	regpIOTableDAM.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, regpIOTableDAM);
	if (isNE(regpIOTableDAM.getStatuz(),varcom.oK)) {
		syserrrec.params.set(regpIOTableDAM.getParams());
		syserrrec.statuz.set(regpIOTableDAM.getStatuz());
		fatalError600();
	}
	/*EXIT*/
}

protected void callGeneric3550()
	{
		/* Call generic subroutine.*/
		regpsubrec.subStatuz.set(varcom.oK);
		regpsubrec.subEffdate.set(bsscIO.getEffectiveDate());
		regpsubrec.subCompany.set(bsprIO.getCompany());
		regpsubrec.subLanguage.set(bsscIO.getLanguage());
		regpsubrec.subBatcpfx.set(batcdorrec.prefix);
		regpsubrec.subBatctrcde.set(bprdIO.getAuthCode());
		regpsubrec.subBatcactyr.set(bsscIO.getAcctYear());
		regpsubrec.subBatcactmn.set(bsscIO.getAcctMonth());
		regpsubrec.subBatcbatch.set(batcdorrec.batch);
		regpsubrec.subBatcbrn.set(bsscIO.getInitBranch());
		regpsubrec.subBatccoy.set(bsprIO.getCompany());
		regpsubrec.subCnttype.set(chdrrgpIO.getCnttype());
		regpsubrec.subTranid.set(ZERO);
		regpsubrec.subTermid.set(varcom.vrcmTranid);
		if(isNE(regpIOTableDAM.getUser(),ZERO)){
			regpsubrec.subUser.set(regpIOTableDAM.getUser());
			callProgram(wsaaT5671Subprog[wsaaT5671Ix][wsaaSubprogIx.toInt()], regpsubrec.batcsubRec);
			if (isNE(regpsubrec.subStatuz,varcom.oK)) {
				checkReason3950();
				if (!itemFound.isTrue()) {
					wsysSysparams.set(regpsubrec.batcsubRec);
					syserrrec.params.set(wsysSystemErrorParams);
					syserrrec.statuz.set(regpsubrec.subStatuz);
					fatalError600();
				}
				else {
					ct05Value++;
					wsaaStopPayment = "Y";
					Regrpf regrIO = new Regrpf();
					regrIO.setExcode(regpsubrec.subStatuz.toString());
					regrIO.setExreport("Y");
					reportProcess5000(regrIO);
				}
		} 
		
		}
	}

protected void rewriteChdr3600()
	{
		/* Obtain the CHDR record using CHDRPAY logical.                */
		if(updateChdrpayMap == null){
		    updateChdrpayMap = new LinkedHashMap<>();
		}
	      /* Write validflag '2' Contract Header (CHDR) record            */
		Chdrpf updateChdrpf = new Chdrpf(chdrrgpIO);
        updateChdrpf.setUniqueNumber(chdrrgpIO.getUniqueNumber());
        updateChdrpf.setValidflag('2');
        updateChdrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
		if(!updateChdrpayMap.containsKey(chdrrgpIO.getUniqueNumber())){
		    updateChdrpayMap.put(chdrrgpIO.getUniqueNumber(), updateChdrpf);
		}
		
		/* Rewrite contract record with incremented TRANNO              */
		/* ADD 1                       TO CHDRRGP-TRANNO.               */
		/* MOVE UPDAT                  TO CHDRRGP-FUNCTION.             */
		/* CALL 'CHDRRGPIO'            USING CHDRRGP-PARAMS.            */
		/* IF  CHDRRGP-STATUZ          NOT = O-K                        */
		/*     MOVE CHDRRGP-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE CHDRRGP-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 600-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		Chdrpf insertChdrpf = new Chdrpf(chdrrgpIO);
		if(trannoMap == null){
		    trannoMap = new HashMap<>();
		}
		if(trannoMap.containsKey(insertChdrpf.getChdrnum())){
		    curTranno = trannoMap.get(insertChdrpf.getChdrnum());
		}else{
		    curTranno = insertChdrpf.getTranno() + 1;
		}
		trannoMap.put(insertChdrpf.getChdrnum(), curTranno);
		insertChdrpf.setTranno(curTranno);
		insertChdrpf.setValidflag('2');
		insertChdrpf.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		insertChdrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
		insertChdrpf.setUniqueNumber(chdrrgpIO.getUniqueNumber());
		/* Check if "WOP" method is selected, perform the Sinstamt         */
		/* validation. Otherwise skip this validation.                     */
		
		if(readT6694()){
		
			wsaaWopFlag.set(SPACES);
			if (isEQ(t6694rec.sacscode, wsaaSacscode)
			&& isEQ(t6694rec.sacstype, wsaaSacstype)) {
				wopMop.setTrue();
			}
			if ((isNE(regpIO.getDestkey(), SPACES))
			&& isEQ(regpIO.getDestkey(), chdrrgpIO.getChdrnum())
			&& wopMop.isTrue()) {
				/*    COMPUTE WSAA-SINSTAMT05   = ((REGP-PYMT * -1) +           */
				/*                                  CHDRLIF-SINSTAMT05)         */
				/*     COMPUTE WSAA-SINSTAMT05   = (REGP-PYMT * -1)      <LA2110>*/
				/*    COMPUTE WSAA-SINSTAMT05   = (REGP-PYMT * -1) +   <LFA1126>*/
				/*    COMPUTE WSAA-SINSTAMT05   = REGP-PYMT +           <LA1172>*/
				if(CMRPY005Permission){
					compute(wsaaSinstamt05, 2).set(mult(regpIO.getNetamt(), -1));	
				} else {
					compute(wsaaSinstamt05, 2).set(mult(regpIO.getPymt(), -1));
				}
				compute(wsaaSinstamt06, 2).set((add(add(add(add(add(insertChdrpf.getSinstamt01(), insertChdrpf.getSinstamt02()), insertChdrpf.getSinstamt03()), insertChdrpf.getSinstamt04()),insertChdrpf.getSinstamt05()))));
				
			}
		}
		if(insertChdrpayMap == null){
		    insertChdrpayMap = new LinkedHashMap<>();
		}
		if(!insertChdrpayMap.containsKey(chdrrgpIO.getChdrnum())){
		    List<Chdrpf> chdrList = new LinkedList<>();		   
		    chdrList.add(insertChdrpf);
		    insertChdrpayMap.put(chdrrgpIO.getChdrnum(), chdrList);
		}
		else 
		{			
		    	insertChdrpayMap.get(chdrrgpIO.getChdrnum()).add(insertChdrpf);
		}		    
		
	}

protected boolean readT6694() {
	
	
		String keyItemitem = regpIO.getRgpymop();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t6694ListMap.containsKey(keyItemitem)){	
			itempfList = t6694ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((bsscIO.getEffectiveDate().toInt()) >= Integer.parseInt(itempf.getItmfrm().toString()) 
							&& bsscIO.getEffectiveDate().toInt() <= Integer.parseInt(itempf.getItmto().toString())){
						t6694rec.t6694Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						return true;
					}
				}else{
					t6694rec.t6694Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					return true;				
				}				
			}		
		}
		return false;

}

protected void writePtrn3700()
	{
		/* Create a PTRN record*/
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setTermid(varcom.vrcmTermid.toString());
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		ptrnIO.setUserT(varcom.vrcmUser.toInt());
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(bsprIO.getCompany().toString());
		ptrnIO.setBatcbrn(bsscIO.getInitBranch().toString());
		ptrnIO.setBatcactyr(bsscIO.getAcctYear().toInt());
		ptrnIO.setBatctrcde(bprdIO.getAuthCode().toString());
		ptrnIO.setBatcactmn(bsscIO.getAcctMonth().toInt());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		/*    MOVE CHDRRGP-TRANNO         TO PTRN-TRANNO.                  */
		ptrnIO.setTranno(curTranno);
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setChdrpfx("CH");
		/*    MOVE CHDRRGP-CHDRCOY        TO PTRN-CHDRCOY.                 */
		ptrnIO.setChdrcoy(bsprIO.getCompany().toString());
		ptrnIO.setChdrnum(prevChdrnum);
		ptrnIO.setValidflag("1");
		if(insertPtrnList == null){
			insertPtrnList = new ArrayList<>();
		}
		insertPtrnList.add(ptrnIO);
	}


protected void updateRegp3800()
 {
		/* readh REGP rec, using a different logical */
		Regppf updateRegppf = new Regppf(regpIO);
		/* Update the REGP record with valid flag of '2' */
		updateRegppf.setValidflag("2");
		if (updateRegpenqMap == null) {
		    updateRegpenqMap = new ArrayList<>();
		}
		if(!updateRegpenqMap.contains(regpIO.getUniqueNumber())){
		    updateRegpenqMap.add(regpIO.getUniqueNumber());
		}
		
		regpIOTableDAM.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIOTableDAM);
		if (isNE(regpIOTableDAM.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIOTableDAM.getParams());
			syserrrec.statuz.set(regpIOTableDAM.getStatuz());
			fatalError600();
		}
		
		regpIOTableDAM.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIOTableDAM);
		if (isNE(regpIOTableDAM.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIOTableDAM.getParams());
			syserrrec.statuz.set(regpIOTableDAM.getStatuz());
			fatalError600();
		}
		/* Update the REGP date fields. */
		/* Call DATCON2 to calculate the Next Payment Date. */
		/* Check if the Final Payment Date has been reached. */
		regpIO.setAnvdate(regpIOTableDAM.getAnvdate().toInt());
		regpIO.setPymt(regpIOTableDAM.getPymt().getbigdata());
		regpIO.setTotamnt(regpIOTableDAM.getTotamnt().getbigdata());
		regpIO.setLastPaydate(regpIO.getNextPaydate());
		datcon2rec.frequency.set(regpIO.getRegpayfreq());
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(regpIO.getNextPaydate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		regpIO.setNextPaydate(datcon2rec.intDate2.toInt());   //here change of 
		
		
			
	
	
		
		
		if (isGTE(regpIO.getNextPaydate(), regpIO.getFinalPaydate())) {
			regpIO.setNextPaydate(regpIO.getFinalPaydate());
			if (isEQ(wsaaStopPayment, "L")) {
				wsaaStopPayment = "Y";
			}
			if (isEQ(wsaaStopPayment, "N")) {
				wsaaStopPayment = "L";
			}
		}
		/* Update REGP with the TRANNO with the new */
		/* value from the contract. */
		/* MOVE CHDRRGP-TRANNO TO REGP-TRANNO. */
		regpIO.setTranno(curTranno);
		if (insertRegpenqMap == null) {
		    insertRegpenqMap = new LinkedHashMap<>();
		}
		Regppf insertRegppf = new Regppf(regpIO);
		insertRegppf.setValidflag("2");
		rgpytype = regpIO.getRgpytype();
		if(insertRegpenqMap.containsKey(insertRegppf.getChdrnum())){
		    insertRegpenqMap.get(insertRegppf.getChdrnum()).add(insertRegppf);
		}else{
		    List<Regppf> regpList = new LinkedList<Regppf>();
		    regpList.add(insertRegppf);
		    insertRegpenqMap.put(insertRegppf.getChdrnum(), regpList);
		}
		boolean isFeatureConfig = false;
		isFeatureConfig  = FeaConfg.isFeatureExist(regpIO.getChdrcoy(), "SUOTR006", appVars, "IT");
		 if(isFeatureConfig){
			insertRegPayDetails();
		 }
		
		 /* write a non-exception record to the REGR reporting file. */
		Regrpf regrIO = new Regrpf();
		regrIO.setExcode(SPACES.toString());
		regrIO.setExreport(SPACES.toString());
		reportProcess5000(regrIO);
	}

protected void releaseSoftlock3900()
	{
		/* Release the 'soft lock' on the contract.*/
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.entity.set(regxpfRec.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(999999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}

	}

protected void checkReason3950()
	{
		/*READ-STORED-TABLE*/
		wsaaT6689Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT6689Ix,wsaaT6689Rec.length); wsaaT6689Ix++){
				if (isEQ(wsaaT6689Key[wsaaT6689Ix],regpsubrec.subStatuz)) {
					itemFound.setTrue();
					break searchlabel1;
				}
			}
			wsaaItemFound.set("N");
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		contotrec.totval.set(ct01Value);
		contotrec.totno.set(1);
		callContot001();
		ct01Value = 0;
		contotrec.totval.set(ct02Value);
		contotrec.totno.set(2);
		callContot001();
		ct02Value = 0;
		contotrec.totval.set(ct03Value);
		contotrec.totno.set(3);
		callContot001();
		ct03Value = 0;
		contotrec.totval.set(ct04Value);
		contotrec.totno.set(4);
		callContot001();
		ct04Value = 0;
		contotrec.totval.set(ct05Value);
		contotrec.totno.set(5);
		callContot001();
		ct05Value = 0;
		contotrec.totval.set(ct06Value);
		contotrec.totno.set(6);
		callContot001();
		ct06Value = 0;
		contotrec.totval.set(ct07Value);
		contotrec.totno.set(7);
		callContot001();
		ct07Value = 0;
		contotrec.totval.set(ct08Value);
		contotrec.totno.set(8);
		callContot001();
		ct08Value = 0;
		contotrec.totval.set(ct09Value);
		contotrec.totno.set(9);
		callContot001();
		ct09Value = 0;

        if (insertRegrpfList != null && !insertRegrpfList.isEmpty()) {
            regrpfDAO.insertRegrpfRecord(insertRegrpfList);
            insertRegrpfList.clear();
        }
        if (updateChdrpayMap != null && !updateChdrpayMap.isEmpty()) {
            chdrpfDAO.updateInvalidChdrRecordWithUniqueNumber(bsscIO.getEffectiveDate().toInt(),new LinkedList<>(updateChdrpayMap.keySet()));
            updateChdrpayMap.clear();
        }
        if (insertChdrpayMap != null && !insertChdrpayMap.isEmpty()) {
            List<Chdrpf> resultList = new LinkedList<>();
            for (String key : insertChdrpayMap.keySet()) {
                List<Chdrpf> insertList = insertChdrpayMap.get(key);
                insertList.get(insertList.size() - 1).setValidflag('1');
             
                insertList.get(insertList.size() - 1).setCurrto(varcom.vrcmMaxDate.toInt());
                if(ctcnl002Permission){	
    			    if(rgpytype.equals(payResonCode)){
    			    	insertList.get(insertList.size() - 1).setStatcode(t5679rec.setCnRiskStat.toString()); 
    	    		}
    		    }
                resultList.add(insertList.get(insertList.size() - 1));
            }
            
            chdrpfDAO.insertChdrRcds(resultList);
            insertChdrpayMap.clear();
            if(suotr008Permission) {
	            List<Annypaypf> annypaypfList2 = annypaypfDAO.getRecordbyStat(regpIO.getChdrcoy(), 
	            		regpIO.getChdrnum().trim(),statPend); 
	        	
	        	chdrStatusupdate(annypaypfList2);
            }
        }


        if (insertPtrnList != null && !insertPtrnList.isEmpty()) {
            ptrnpfDAO.insertPtrnPF(insertPtrnList);
            insertPtrnList.clear();
        }
        if (updatePayrMap != null && !updatePayrMap.isEmpty()) {
            payrpfDAO.updatePayrRecord(new LinkedList<>(updatePayrMap.values()), 0);
            updatePayrMap.clear();
        }
        if (insertPayrMap != null && !insertPayrMap.isEmpty()) {
            List<Payrpf> payrResultList = new LinkedList<>();
            for (String key : insertPayrMap.keySet()) {
                List<Payrpf> insertList = insertPayrMap.get(key);
                insertList.get(insertList.size() - 1).setValidflag("1");
                payrResultList.add(insertList.get(insertList.size() - 1));
            }
            payrpfDAO.insertPayrpfList(payrResultList);
            insertPayrMap.clear();
        }
        
        if (updateRegpenqMap != null && !updateRegpenqMap.isEmpty()) {
            regppfDAO.updateRegpRecord("2",updateRegpenqMap);
            updateRegpenqMap.clear();
        }
        
        if (updateRegpMap != null && !updateRegpMap.isEmpty()) {
            regppfDAO.updateRegpfRecord(new LinkedList<>(updateRegpMap.values()));
            updateRegpMap.clear();
        }
        
        
        
        if (insertRegpenqMap != null && !insertRegpenqMap.isEmpty()) {
            List<Regppf> resultList = new LinkedList<>();
            for (String key : insertRegpenqMap.keySet()) {
                List<Regppf> insertList = insertRegpenqMap.get(key);
                insertList.get(insertList.size() - 1).setValidflag("1");
                resultList.addAll(insertList);
            }
            regppfDAO.insertRegppfRecord(resultList);
            insertRegpenqMap.clear();
        }
	}

protected void chdrStatusupdate(List<Annypaypf> annypaypfList2) {
	List<Ptrnpf> ptrnpfObjList =  ptrnpfDAO.searchPtrnrevData(regpIO.getChdrcoy(), regpIO.getChdrnum().trim());
    boolean tranCodeflag = false;
            	
    for(Ptrnpf ptrnpfList : ptrnpfObjList) {
        if(ptrnpfList.getBatctrcde().equals(tranCodeCreate) || ptrnpfList.getBatctrcde().equals(tranCodeModify)) {
           tranCodeflag = true;
         } 
    }
	
	if((annypaypfList2.isEmpty() || isEQ(regpIO.getFinalPaydate(),annyregpf.getFinalpmntdt())) && tranCodeflag)
	{
		Chdrpf chdrpfitem = chdrpfDAO.getchdrRecord(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum());
		if (chdrpfitem != null)
			chdrpfDAO.updateChdrpfTranlusedPstcdeTranno(chdrpfitem.getChdrcoy().toString(), 
					chdrpfitem.getChdrnum(), premter);
	}
}

protected void rollback3600()
	{
		/*START*/
		/** No additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*START*/
		/* Close all files and delete the override function*/
		wsaaQcmdexc.set("DLTOVR FILE(REGXPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void reportProcess5000(Regrpf regrIO)
	{
		/* Write detail record to regular reporting file REGR*/
		/* This will be printed out in the Regular Payments*/
		/* reporting program B5370. Note that the records*/
		/* may be exception or non-exception records.*/
		regrIO.setChdrcoy(regxpfRec.getChdrcoy());
		regrIO.setChdrnum(regxpfRec.getChdrnum());
		regrIO.setLife(regxpfRec.getLife());
		regrIO.setCoverage(regxpfRec.getCoverage());
		regrIO.setRider(regxpfRec.getRider());
		regrIO.setRgpynum(regxpfRec.getRgpynum());
		regrIO.setRgpytype(regxpfRec.getRgpytype());
		regrIO.setRgpystat(regxpfRec.getRgpystat());
		regrIO.setCrtable(regxpfRec.getCrtable());
		regrIO.setTranno(regxpfRec.getTranno());
		regrIO.setPymt(regpIO.getPymt());
		regrIO.setCurrcd(regpIO.getCurrcd());
		regrIO.setPrcnt(regpIO.getPrcnt());
		regrIO.setPayreason(regpIO.getPayreason());
		regrIO.setRevdte(regpIO.getRevdte());
		regrIO.setFirstPaydate(regpIO.getFirstPaydate());
		regrIO.setLastPaydate(regpIO.getLastPaydate());
		regrIO.setProgname(bprdIO.getBatchProgram().toString());
		if(insertRegrpfList == null){
			insertRegrpfList = new ArrayList<Regrpf>();
		}
		insertRegrpfList.add(regrIO);
	}


protected void a000Statistics()
	{
		lifsttrrec.batccoy.set(batcdorrec.company);
		lifsttrrec.batcbrn.set(batcdorrec.branch);
		lifsttrrec.batcactyr.set(batcdorrec.actyear);
		lifsttrrec.batcactmn.set(batcdorrec.actmonth);
		lifsttrrec.batctrcde.set(bprdIO.getAuthCode());
		lifsttrrec.batcbatch.set(batcdorrec.batch);
		lifsttrrec.chdrcoy.set(chdrrgpIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrrgpIO.getChdrnum());
		/*    MOVE CHDRRGP-TRANNO         TO LIFS-TRANNO.          <LA4828>*/
		lifsttrrec.tranno.set(curTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}

	}
protected void readTd5h7()
{
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(regpIO.getChdrcoy());
	itempf.setItmfrm(new BigDecimal(getCurrentBusinessDate()));
	itempf.setItmto(new BigDecimal(getCurrentBusinessDate()));
	itempf.setItemtabl("TD5H7");
	itempf.setItemitem(chdrrgpIO.getCnttype());
	itempf.setValidflag("1");
	List<Itempf> itempfList  = itemDAO.findByItemDates(itempf);
	if(itempfList!=null && !itempfList.isEmpty()){
		td5h7rec.td5h7Rec.set(StringUtil.rawToString( itempfList.get(0).getGenarea()));
	}
	else {
		itempf.setItemitem("***");
		itempfList  = itemDAO.findByItemDates(itempf);
		if(itempfList!=null && !itempfList.isEmpty()){
			td5h7rec.td5h7Rec.set(StringUtil.rawToString( itempfList.get(0).getGenarea()));
		}
		else {
			syserrrec.params.set(td5h7rec.td5h7Rec);
			syserrrec.statuz.set(rrhf);
			fatalError600();
		}
	}
}

public  Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   }


public void updateAnnypaypf(){
	
	annypaypfList=annypaypfDAO.getRecordbyStat(regpIO.getChdrcoy(), regpIO.getChdrnum().trim(),statPend); 
	if(!annypaypfList.isEmpty())
	{
		for (int i = 0; i < annypaypfList.size(); i++) {
			
			annypaypf = annypaypfList.get(i);
			
			if(isEQ(annypaypf.getPmntno(),regpIO.getRgpynum()))
			{
				annypaypf.setChdrcoy(regpIO.getChdrcoy());
				annypaypf.setChdrnum(regpIO.getChdrnum());
				annypaypf.setPmntno(regpIO.getRgpynum());
				annypaypfDAO.updateFlag(annypaypf);
				
				Annypaypf annypayNew = annypaypf;
				annypayNew.setValidflag("1");
				annypayNew.setEffectivedt(datcon1rec.intDate.toInt());
				annypayNew.setPaidamt(regpIO.getPymt());
				annypayNew.setPmntstat(statPaid);
				annypayNew.setTranno(curTranno);
				annypayNew.setBatctrcde(bprdIO.getAuthCode().toString());
				annypayNew.setPmntno(regpIO.getRgpynum());
				annypaypfDAO.insertAnnypaypf(annypayNew);
			}
		
		}
		
	}
}

public void updateAnnyregpf(){
	
	annyregpf = new Annyregpf();
	annyregpf = annyregpfDAO.getAnnyregpf(regpIO.getChdrcoy(), regpIO.getChdrnum().trim());
	annypaypfList1=annypaypfDAO.getRecordbyStat(regpIO.getChdrcoy(), regpIO.getChdrnum().trim(),statPend); 
	
	Optional<Annyregpf> isExists = Optional.ofNullable(annyregpf);
	if (isExists.isPresent()) {
		
	annyregpfDAO.updateValidflag(annyregpf);
	
	Annyregpf annyregNew = annyregpf;
	annyregNew.setValidflag("1");
	annyregNew.setEffectivedt(datcon1rec.intDate.toInt());
	annyregNew.setTranno(curTranno);
	annyregNew.setBatctrcde(bprdIO.getAuthCode().toString());
	
	if(annypaypfList1.isEmpty() || isEQ(regpIO.getFinalPaydate(),annyregpf.getFinalpmntdt()))
	{
		annyregNew.setAnnpstcde(PmntTerm);
		
		if(isEQ(annyregpf.getAnnpmntopt(),"02") || isEQ(annyregpf.getAnnpmntopt(),"03"))
		{
			annyregNew.setAnniversdt(regpIO.getFinalPaydate());
		}
	}
	else if(annypaypfList1.size()>1 || isEQ(regpIO.getFinalPaydate(),annyregpf.getFrstpmntdt()))
	{
		annyregNew.setAnnpstcde(PmntInProg);
	}
	
	if(isLT(regpIO.getFinalPaydate(),annyregpf.getFinalpmntdt()) && ((isEQ(annyregpf.getAnnpmntopt(),"02") && isGT(regpIO.getRgpynum(),1)) || (isEQ(annyregpf.getAnnpmntopt(),"03")  && isGT(regpIO.getRgpynum(),2))))
	{	
		datcon2rec.intDate1.set(annyregpf.getAnniversdt());
		datcon2rec.frequency.set(annyregpf.getAnnpmntfreq());
		datcon2rec.freqFactor.set("1");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		
		annyregNew.setAnniversdt(datcon2rec.intDate2.toInt());
	}
	
	annyregpfDAO.insertAnnyregpf(annyregNew);
 }
}

public void updateRegpf(){

	/* readh REGP rec, using a different logical */
	Regppf updateRegppf = new Regppf(regpIO);
	/* Update the REGP record with valid flag of '2' */
	updateRegppf.setValidflag("2");
	updateRegppf.setLastPaydate(regpIO.getNextPaydate());
	updateRegppf.setNextPaydate(varcom.vrcmMaxDate.toInt()); 
	
	if (updateRegpMap == null) {
	    updateRegpMap = new LinkedHashMap<Long, Regppf>();
	}
	if(!updateRegpMap.containsKey(regpIO.getUniqueNumber())){
		updateRegpMap.put(regpIO.getUniqueNumber(), updateRegppf);
	}
	
	regpIOTableDAM.setFunction(varcom.retrv);
	SmartFileCode.execute(appVars, regpIOTableDAM);
	if (isNE(regpIOTableDAM.getStatuz(),varcom.oK)) {
		syserrrec.params.set(regpIOTableDAM.getParams());
		syserrrec.statuz.set(regpIOTableDAM.getStatuz());
		fatalError600();
	}
	
	regpIOTableDAM.setFunction(varcom.rlse);
	SmartFileCode.execute(appVars, regpIOTableDAM);
	if (isNE(regpIOTableDAM.getStatuz(),varcom.oK)) {
		syserrrec.params.set(regpIOTableDAM.getParams());
		syserrrec.statuz.set(regpIOTableDAM.getStatuz());
		fatalError600();
	}
	
	
	regpIO.setAnvdate(regpIOTableDAM.getAnvdate().toInt());
	regpIO.setPymt(regpIOTableDAM.getPymt().getbigdata());
	regpIO.setTotamnt(regpIOTableDAM.getTotamnt().getbigdata());
	regpIO.setLastPaydate(regpIO.getNextPaydate());
	
	regpIO.setNextPaydate(varcom.vrcmMaxDate.toInt()); 
	regpIO.setTranno(curTranno);
	
	if (insertRegpenqMap == null) {
	    insertRegpenqMap = new LinkedHashMap<>();
	}
	
	Regppf insertRegppf = new Regppf(regpIO);
	insertRegppf.setValidflag("2");
	rgpytype = regpIO.getRgpytype();

	if(!insertRegpenqMap.containsKey(insertRegppf.getChdrnum())){
		List<Regppf> regpList = new LinkedList<Regppf>();
	    regpList.add(insertRegppf);
		insertRegpenqMap.put(insertRegppf.getChdrnum(), regpList);
	}
	else{
		 insertRegpenqMap.get(insertRegppf.getChdrnum()).add(insertRegppf);
	}

	 /* write a non-exception record to the REGR reporting file. */
	Regrpf regrIO = new Regrpf();
	regrIO.setExcode(SPACES.toString());
	regrIO.setExreport(SPACES.toString());
	reportProcess5000(regrIO);

	
}
	


 public void insertRegPayDetails(){
			if( isEQ(regpIO.getRgpymop(), "A")){
			rgpdetpf = new Rgpdetpf();
			rgpdetpf.setChdrcoy(regpIO.getChdrcoy());
			rgpdetpf.setChdrnum(regpIO.getChdrnum());
			rgpdetpf.setPlnsfx(regpIO.getPlanSuffix());
			rgpdetpf.setLife(regpIO.getLife());
			rgpdetpf.setCoverage(regpIO.getCoverage());
			rgpdetpf.setRider(regpIO.getRider());
			rgpdetpf.setCurrcd(regpIO.getCurrcd());
			rgpdetpf.setValidflag("1");
			rgpdetpf.setTranno(String.valueOf(curTranno));
			rgpdetpf.setApcaplamt(regpIO.getPymt());
			rgpdetpf.setApintamt(BigDecimal.ZERO);
			readTd5h7();
			rgpdetpf.setAplstcapdate(getCurrentBusinessDate());
			rgpdetpf.setAplstintbdte(getCurrentBusinessDate());
			datcon2rec.intDate1.set(rgpdetpf.getAplstcapdate());
			if (isNE(td5h7rec.intcapfreq,SPACES)) {
				datcon2rec.frequency.set(td5h7rec.intcapfreq);
				datcon2rec.freqFactor.set("1");
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				rgpdetpf.setApnxtcapdate(datcon2rec.intDate2.toInt());
				
			}
			else {
				rgpdetpf.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
			}
			datcon2rec.intDate1.set(rgpdetpf.getAplstintbdte());
			if (isNE(td5h7rec.intcalfreq,SPACES)) {
				datcon2rec.frequency.set(td5h7rec.intcalfreq);
				datcon2rec.freqFactor.set("1");
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				rgpdetpf.setApnxtintbdte(datcon2rec.intDate2.toInt());
			}
			else {
				rgpdetpf.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
			 }
			rgpdetpfDAO.insertIntoRpdetpf(rgpdetpf);
		}			
  }
}
