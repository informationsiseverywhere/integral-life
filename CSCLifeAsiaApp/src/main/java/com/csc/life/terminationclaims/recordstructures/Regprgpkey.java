package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:44
 * Description:
 * Copybook name: REGPRGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regprgpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regprgpFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData regprgpKey = new FixedLengthStringData(256).isAPartOf(regprgpFileKey, 0, REDEFINE);
  	public FixedLengthStringData regprgpChdrcoy = new FixedLengthStringData(1).isAPartOf(regprgpKey, 0);
  	public FixedLengthStringData regprgpChdrnum = new FixedLengthStringData(8).isAPartOf(regprgpKey, 1);
  	public FixedLengthStringData regprgpLife = new FixedLengthStringData(2).isAPartOf(regprgpKey, 9);
  	public FixedLengthStringData regprgpCoverage = new FixedLengthStringData(2).isAPartOf(regprgpKey, 11);
  	public FixedLengthStringData regprgpRider = new FixedLengthStringData(2).isAPartOf(regprgpKey, 13);
  	public PackedDecimalData regprgpRgpynum = new PackedDecimalData(5, 0).isAPartOf(regprgpKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(regprgpKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regprgpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regprgpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}