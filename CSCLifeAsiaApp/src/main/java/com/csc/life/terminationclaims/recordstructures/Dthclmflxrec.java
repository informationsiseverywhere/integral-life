package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Dennis Puhawan
 * @version
 * Creation Date: Tue, 24 Feb 2015 02:01:21
 * Description:
 * Copybook name: DTHCLMFLXREC
 *
 * Copyright (2015) CSC Asia, all rights reserved
 *
 */
public class Dthclmflxrec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData dthclmflxrec = new FixedLengthStringData(getDthclmflxRecSize());
	  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(dthclmflxrec, 0);
	  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(dthclmflxrec, 1);
	  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(dthclmflxrec, 9);
	  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(dthclmflxrec, 12);
	  	public PackedDecimalData susamt = new PackedDecimalData(17, 2).isAPartOf(dthclmflxrec, 15);
	  	public PackedDecimalData nextinsamt = new PackedDecimalData(17, 2).isAPartOf(dthclmflxrec, 24);
	  	public ZonedDecimalData ptdate = new ZonedDecimalData(8, 0).isAPartOf(dthclmflxrec, 33);
	  	public ZonedDecimalData dtofdeath = new ZonedDecimalData(8, 0).isAPartOf(dthclmflxrec, 41);
	  	public ZonedDecimalData btdate = new ZonedDecimalData(8, 0).isAPartOf(dthclmflxrec, 49);
	  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(dthclmflxrec, 57);
	  	public FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(dthclmflxrec, 65);
	  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(dthclmflxrec, 68);

		public void initialize() {
			COBOLFunctions.initialize(dthclmflxrec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			dthclmflxrec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}
		public int getDthclmflxRecSize()
		{
			return 72;
		}


	}