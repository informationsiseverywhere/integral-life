/*
 * File: Pr513.java
 * Date: 30 August 2009 1:34:25
 * Author: Quipoz Limited
 * 
 * Class transformed from PR513.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.procedures.Trcdechk;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tranchkrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.screens.Sr513ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*             PR513 - AUTO LAPSE/PAID-UP REVERSAL
*               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*                   ( CLONE FROM P5697)
*Initialise
*----------
*
*  For this version BENEFIT BILLING DATE was added
*
*  Clear the subfile ready for loading.
*
*  The  details of the contract being worked with will have been
*  stored  in  the  CHDRMJA I/O module. Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*    -  Contract Type, (CNTTYPE) - long description from T5688,
*    -  Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*    -  Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*    -  The owner's client (CLTS) details.
*    -  The joint  owner's  client (CLTS) details if they exist.
*
*  Ensure that the last active transaction is a Lapse/Paid-up
*  transaction by sequentially reading PTRNREV and ignoring
*  non-significant transactions. Non-significant transactions
*  is defined as those transaction having an entry in T6661
*  with no subroutine specified and the reversal flag set to N.
*
*  If the last active transaction is not a Lapse/Paid-up, then
*  no reversal request can be entertained.
*
*  Load the subfile as follows:
*
*          Read  the  first  COVRENQ  record on the contract, using
*          zero  in  Plan  Suffix,  '01'  in  the  Life field and a
*          function of BEGN.
*
*          If the Plan Suffix from the returned record is zero then
*          add 1 to it before display. Obtain the long descriptions
*          for  the  coverage risk status, STATCODE, from T5682 and
*          the  premium  status  code,  PSTATCODE,  from  T5681 and
*          display them.
*
*          Check the transaction number TRANNO of the COVRENQ record.
*          If it is the same as the TRANNO of the PTRNREV record,
*          then automatically select this COVR for reversal. Otherwise,
*          protect this COVR from selection.
*
*          Repeat  the  above  line  for as mamy times as there are
*          summarised policies, incrementing the Plan Suffix by one
*          for each subfile record written.
*
*          Store  the  life number and increment the Plan Suffix by
*          one.  Perform  a BEGN to obtain the first COVRENQ record
*          for  the  next  Plan  Suffix.  Write  out its details as
*          before  and repeat the process until either the Company,
*          Contract Header Number or Life Number changes.
*
*  Load all pages  required  in  the subfile and set the subfile
*  more indicator to no.
*
*Updating
*--------
*
*  Call the AT module to perform the reversal.
*
*
*Next Program
*------------
*
*  Control will return to the sub-menu from this program.
*
*
*****************************************************************
* </pre>
*/
public class Pr513 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR513");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private String wsaaTranError = "N";

	private FixedLengthStringData wsaaDayOld = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYyOld = new ZonedDecimalData(4, 0).isAPartOf(wsaaDayOld, 0).setUnsigned();
	private ZonedDecimalData wsaaMmOld = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayOld, 4).setUnsigned();
	private ZonedDecimalData wsaaDdOld = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayOld, 6).setUnsigned();

	private FixedLengthStringData wsaaDayNew = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYyNew = new ZonedDecimalData(4, 0).isAPartOf(wsaaDayNew, 0).setUnsigned();
	private ZonedDecimalData wsaaMmNew = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayNew, 4).setUnsigned();
	private ZonedDecimalData wsaaDdNew = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayNew, 6).setUnsigned();
	private FixedLengthStringData wsaaForwardTranscode = new FixedLengthStringData(4).init(SPACES);
		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRstatdesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPstatdesc = new FixedLengthStringData(30);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();

	private FixedLengthStringData wsaaProcessingMsg = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaMsgProcess = new FixedLengthStringData(19).isAPartOf(wsaaProcessingMsg, 0).init(SPACES);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 19, FILLER).init(SPACES);
	private ZonedDecimalData wsaaMsgTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaProcessingMsg, 20).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(3).isAPartOf(wsaaProcessingMsg, 25, FILLER).init(" - ");
	private FixedLengthStringData wsaaMsgTrantype = new FixedLengthStringData(4).isAPartOf(wsaaProcessingMsg, 28).init(SPACES);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 32, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgTrandesc = new FixedLengthStringData(27).isAPartOf(wsaaProcessingMsg, 33);

	private FixedLengthStringData wsaaConfirmMsg = new FixedLengthStringData(60);
	private FixedLengthStringData filler4 = new FixedLengthStringData(19).isAPartOf(wsaaConfirmMsg, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsg = new FixedLengthStringData(22).isAPartOf(wsaaConfirmMsg, 19).init(SPACES);
	private FixedLengthStringData filler5 = new FixedLengthStringData(19).isAPartOf(wsaaConfirmMsg, 41, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
		/* ERRORS */
	private String f910 = "F910";
	private String rlaf = "RLAF";
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5688 = "T5688";
	private String t6661 = "T6661";
	private String tr386 = "TR386";
	private String itemrec = "ITEMREC";
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6661rec t6661rec = new T6661rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tranchkrec tranchkrec = new Tranchkrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sr513ScreenVars sv = ScreenProgram.getScreenVars( Sr513ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		readPtrn1085, 
		badTrans1086, 
		subfileLoad1080, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Pr513() {
		super();
		screenVars = sv;
		new ScreenModel("Sr513", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					retrvContract1020();
					readLifeDetails1050();
					jointLifeDetails1060();
					contractTypeStatus1070();
				}
				case readPtrn1085: {
					readPtrn1085();
				}
				case badTrans1086: {
					badTrans1086();
				}
				case subfileLoad1080: {
					subfileLoad1080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaTranError = "N";
		wsaaBatckey.set(wsspcomn.batchkey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaForwardTranscode.set(t6661rec.trcode);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR513", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvContract1020()
	{
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/*HEADER-DETAIL*/
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.register.set(chdrmjaIO.getRegister());
	}

protected void readLifeDetails1050()
	{
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1060()
	{
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			sv.jlife.set("NONE");
			sv.jlifename.set("NONE");
		}
		else {
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				syserrrec.statuz.set(cltsIO.getStatuz());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

protected void contractTypeStatus1070()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

protected void readPtrn1085()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),ptrnrevIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),ptrnrevIO.getChdrnum())) {
			ptrnrevIO.setStatuz(varcom.endp);
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ptrnrevIO.getBatctrcde(),wsaaForwardTranscode)) {
			wsaaMsgTranno.set(ptrnrevIO.getTranno());
			wsaaMsgTrantype.set(ptrnrevIO.getBatctrcde());
			getDesc1800();
			wsaaMsgTrandesc.set(descIO.getLongdesc());
			itemIO.setFormat(itemrec);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr386);
			wsaaTr386Lang.set(wsspcomn.language);
			wsaaTr386Pgm.set(wsaaProg);
			wsaaTr386Id.set(SPACES);
			itemIO.setItemitem(wsaaTr386Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			tr386rec.tr386Rec.set(itemIO.getGenarea());
			wsaaMsgProcess.set(tr386rec.progdesc01);
			sv.sfcdesc.set(wsaaProcessingMsg);
			wsaaMsg.set(tr386rec.progdesc02);
			sv.confirm.set(wsaaConfirmMsg);
			sv.sfcdescOut[varcom.nd.toInt()].set(SPACES);
			sv.sfcdesdOut[varcom.nd.toInt()].set(SPACES);
			goTo(GotoLabel.subfileLoad1080);
		}
		tranchkrec.codeCheckRec.set(SPACES);
		tranchkrec.tcdeStatuz.set(varcom.oK);
		tranchkrec.tcdeTranCode.set(ptrnrevIO.getBatctrcde());
		tranchkrec.tcdeCompany.set(wsspcomn.company);
		callProgram(Trcdechk.class, tranchkrec.codeCheckRec);
		if (isNE(tranchkrec.tcdeStatuz,varcom.oK)
		&& isNE(tranchkrec.tcdeStatuz,varcom.mrnf)) {
			syserrrec.params.set(tranchkrec.codeCheckRec);
			syserrrec.statuz.set(tranchkrec.tcdeStatuz);
			fatalError600();
		}
		if (isEQ(tranchkrec.tcdeStatuz,varcom.mrnf)) {
			goTo(GotoLabel.badTrans1086);
		}
		if (isEQ(tranchkrec.tcdeStatuz,varcom.oK)) {
			ptrnrevIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readPtrn1085);
		}
	}

protected void badTrans1086()
	{
		sv.sfcdescOut[varcom.nd.toInt()].set("Y");
		sv.sfcdesdOut[varcom.nd.toInt()].set("Y");
		sv.chdrnumErr.set(rlaf);
		wsspcomn.edterror.set("Y");
		wsaaTranError = "Y";
	}

protected void subfileLoad1080()
	{
		covrenqIO.setDataArea(SPACES);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setLife("01");
		covrenqIO.setCoverage("00");
		covrenqIO.setRider("00");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrenqIO.getStatuz(),varcom.endp)
		|| isNE(chdrmjaIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),covrenqIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
		}
		wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
		if (isEQ(covrenqIO.getPlanSuffix(),ZERO)) {
			wsaaPlanSuffix.add(1);
		}
		sv.planSuffix.set(wsaaPlanSuffix);
		sv.life.set(covrenqIO.getLife());
		sv.coverage.set(covrenqIO.getCoverage());
		sv.rider.set(covrenqIO.getRider());
		sv.statcode.set(covrenqIO.getStatcode());
		sv.pstatcode.set(covrenqIO.getPstatcode());
		sv.dteapp.set(covrenqIO.getBenBillDate());
		wsaaDayOld.set(sv.dteapp);
		covrStatusDescs1600();
		if (isEQ(covrenqIO.getTranno(),ptrnrevIO.getTranno())
		&& isEQ(sv.sfcdesdOut[varcom.nd.toInt()],SPACES)) {
			sv.select.set("X");
			sv.selectOut[varcom.nd.toInt()].set("N");
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.select.set(SPACES);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR513", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.add(1);
		if (isEQ(covrenqIO.getPlanSuffix(),ZERO)) {
			while ( !(isGT(wsaaPlanSuffix,chdrmjaIO.getPolsum()))) {
				processSuffixGroup1100();
			}
			
		}
		wsaaLife.set(covrenqIO.getLife());
		covrenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrenqIO.setPlanSuffix(wsaaPlanSuffix);
		covrenqIO.setCoverage("00");
		covrenqIO.setRider("00");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError600();
		}
		while ( !(isNE(covrenqIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covrenqIO.getLife(),wsaaLife)
		|| isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processPolicy1200();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void processSuffixGroup1100()
	{
		para1100();
	}

protected void para1100()
	{
		sv.planSuffix.set(wsaaPlanSuffix);
		sv.statcode.set(covrenqIO.getStatcode());
		sv.pstatcode.set(covrenqIO.getPstatcode());
		sv.rstatdesc.set(wsaaRstatdesc);
		sv.pstatdesc.set(wsaaPstatdesc);
		if (isEQ(covrenqIO.getTranno(),ptrnrevIO.getTranno())
		&& isEQ(sv.sfcdesdOut[varcom.nd.toInt()],SPACES)) {
			sv.select.set("X");
			sv.selectOut[varcom.nd.toInt()].set("N");
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.select.set(SPACES);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR513", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.add(1);
	}

protected void processPolicy1200()
	{
		para1200();
	}

protected void para1200()
	{
		sv.planSuffix.set(covrenqIO.getPlanSuffix());
		wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
		sv.statcode.set(covrenqIO.getStatcode());
		sv.pstatcode.set(covrenqIO.getPstatcode());
		covrStatusDescs1600();
		if (isEQ(covrenqIO.getTranno(),ptrnrevIO.getTranno())
		&& isEQ(sv.sfcdesdOut[varcom.nd.toInt()],SPACES)) {
			sv.select.set("X");
			sv.selectOut[varcom.nd.toInt()].set("N");
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.select.set(SPACES);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR513", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.add(1);
		covrenqIO.setFunction(varcom.begn);
		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrenqIO.setPlanSuffix(wsaaPlanSuffix);
		covrenqIO.setCoverage("00");
		covrenqIO.setRider("00");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError600();
		}
	}

protected void covrStatusDescs1600()
	{
		para1600();
	}

protected void para1600()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5681);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrenqIO.getPstatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatdesc.fill("?");
			wsaaPstatdesc.fill("?");
		}
		else {
			sv.pstatdesc.set(descIO.getLongdesc());
			wsaaPstatdesc.set(descIO.getLongdesc());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5682);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrenqIO.getStatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.rstatdesc.fill("?");
			wsaaRstatdesc.fill("?");
		}
		else {
			sv.rstatdesc.set(descIO.getLongdesc());
			wsaaRstatdesc.set(descIO.getLongdesc());
		}
	}

protected void getDesc1800()
	{
		para1810();
	}

protected void para1810()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
		wsaaDayNew.set(sv.dteapp);
		if (isEQ(sv.dteapp,varcom.vrcmMaxDate)) {
			sv.dteappErr.set("E186");
			wsspcomn.edterror.set("Y");
		}
		if (isNE(wsaaDdOld,ZERO)) {
			if (isNE(wsaaDdOld,wsaaDdNew)) {
				sv.dteappErr.set("E032");
				wsspcomn.edterror.set("Y");
			}
		}
		if (isEQ(wsaaTranError,"Y")) {
			sv.chdrnumErr.set(rlaf);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.enttyp.set(chdrmjaIO.getChdrpfx());
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrmjaIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTranDate.set(varcom.vrcmDate);
		wsaaTranTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaPlnsfx.set(ZERO);
		wsaaSupflag.set("N");
		wsaaTodate.set(ptrnrevIO.getPtrneff());
		wsaaSuppressTo.set(ZERO);
		wsaaCfiafiTranno.set(ZERO);
		wsaaCfiafiTranCode.set(SPACES);
		wsaaBbldat.set(sv.dteapp);
		atreqrec.transArea.set(wsaaTransArea);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
