package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6651screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6651ScreenVars sv = (S6651ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6651screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6651ScreenVars screenVars = (S6651ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.puffamt02.setClassString("");
		screenVars.feepc02.setClassString("");
		screenVars.pufeemin02.setClassString("");
		screenVars.pufeemax02.setClassString("");
		screenVars.puffamt03.setClassString("");
		screenVars.feepc03.setClassString("");
		screenVars.pufeemin03.setClassString("");
		screenVars.pufeemax03.setClassString("");
		screenVars.puffamt04.setClassString("");
		screenVars.feepc04.setClassString("");
		screenVars.pufeemin04.setClassString("");
		screenVars.pufeemax04.setClassString("");
		screenVars.puffamt05.setClassString("");
		screenVars.feepc05.setClassString("");
		screenVars.pufeemin05.setClassString("");
		screenVars.pufeemax05.setClassString("");
		screenVars.puffamt06.setClassString("");
		screenVars.feepc06.setClassString("");
		screenVars.pufeemin06.setClassString("");
		screenVars.pufeemax06.setClassString("");
		screenVars.adjustiu.setClassString("");
		screenVars.bidoffer.setClassString("");
		screenVars.ovrsuma.setClassString("");
	}

/**
 * Clear all the variables in S6651screen
 */
	public static void clear(VarModel pv) {
		S6651ScreenVars screenVars = (S6651ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.puffamt02.clear();
		screenVars.feepc02.clear();
		screenVars.pufeemin02.clear();
		screenVars.pufeemax02.clear();
		screenVars.puffamt03.clear();
		screenVars.feepc03.clear();
		screenVars.pufeemin03.clear();
		screenVars.pufeemax03.clear();
		screenVars.puffamt04.clear();
		screenVars.feepc04.clear();
		screenVars.pufeemin04.clear();
		screenVars.pufeemax04.clear();
		screenVars.puffamt05.clear();
		screenVars.feepc05.clear();
		screenVars.pufeemin05.clear();
		screenVars.pufeemax05.clear();
		screenVars.puffamt06.clear();
		screenVars.feepc06.clear();
		screenVars.pufeemin06.clear();
		screenVars.pufeemax06.clear();
		screenVars.adjustiu.clear();
		screenVars.bidoffer.clear();
		screenVars.ovrsuma.clear();
	}
}
