package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.financials.dataaccess.dao.CheqpfDAO;
import com.csc.fsu.financials.dataaccess.dao.PreqpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Cheqpf;
import com.csc.fsu.financials.dataaccess.dao.model.Preqpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.dao.PymtpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.dataaccess.model.Pymtpf;
import com.csc.fsu.general.procedures.AlocnoUtil;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Wsspdsbs;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.fsu.general.tablestructures.T3699rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.life.terminationclaims.recordstructures.PaymentValdrec;
import com.csc.life.terminationclaims.screens.Sjl54ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Pjl54 extends ScreenProgCS{
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL54");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sjl54ScreenVars sv = ScreenProgram.getScreenVars( Sjl54ScreenVars.class);
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Alocnorec alocnorec = new Alocnorec();
	private T3688rec t3688rec = new T3688rec();
	private CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private BnfypfDAO bnfypfDAO = getApplicationContext().getBean("bnfypfDAO", BnfypfDAO.class);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private BabrpfDAO babrpfDAO = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	private CrsvpfDAO crsvpfDAO = getApplicationContext().getBean("crsvpfDAO", CrsvpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private CheqpfDAO cheqpfDAO = getApplicationContext().getBean("cheqpfDAO", CheqpfDAO.class);
	private PreqpfDAO preqpfDAO = getApplicationContext().getBean("preqpfDAO", PreqpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private PymtpfDAO pymtpfDAO = getApplicationContext().getBean("pymtpfDAO", PymtpfDAO.class);
	private Cattpf cattpf;
	private Pymtpf pymtpf;
	private FixedLengthStringData wsaaToday = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTodayDate = new ZonedDecimalData(8).isAPartOf(wsaaToday);
	private ZonedDecimalData wsaaTodayYear = new ZonedDecimalData(4).isAPartOf(wsaaToday, 0);
	private ZonedDecimalData wsaaTodayMnth = new ZonedDecimalData(2).isAPartOf(wsaaToday);
	private ZonedDecimalData wsaaTodayDay = new ZonedDecimalData(2).isAPartOf(wsaaToday);
	private FixedLengthStringData wsaaNextday = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaNextdayDate = new ZonedDecimalData(8).isAPartOf(wsaaNextday);
	private ZonedDecimalData wsaaNextdayYear = new ZonedDecimalData(4).isAPartOf(wsaaNextday, 0);
	private ZonedDecimalData wsaaNextdayMnth = new ZonedDecimalData(2).isAPartOf(wsaaNextday);
	private ZonedDecimalData wsaaNextdayDay = new ZonedDecimalData(2).isAPartOf(wsaaNextday);
	private FixedLengthStringData wsaaNexttoday = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaNexttodayDate = new ZonedDecimalData(8).isAPartOf(wsaaNexttoday);
	private ZonedDecimalData wsaaNexttodayYear = new ZonedDecimalData(4).isAPartOf(wsaaNexttoday, 0);
	private ZonedDecimalData wsaaNexttodayMnth = new ZonedDecimalData(2).isAPartOf(wsaaNexttoday);
	private ZonedDecimalData wsaaNexttodayDay = new ZonedDecimalData(2).isAPartOf(wsaaNexttoday);
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private Datcon1rec datcon1 = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private StringBuilder totaldays = new StringBuilder();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private String jl80 = "JL80";
	private String e186 = "E186";   
	private String g599 = "G599"; 
	private String rl28 = "RL28";  
	private List<Bnfypf> bnfypfObjList;
	private ZonedDecimalData percentTotal;
	private ZonedDecimalData amountTotal;
	private Cheqpf cheqpf = new Cheqpf();
	private List<Cheqpf> cheqBulkUpdList = new ArrayList<>();
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl54.class);
	private Preqpf preqpf = new Preqpf();
	private Wsspdsbs wsspdsbs = new Wsspdsbs();
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData wsaaLgnmName = new FixedLengthStringData(80);
	private Ptrnpf ptrnpf = new Ptrnpf();
	private Crsvpf crsvpfItem = new Crsvpf();
	private Batckey wsaaBatckey1 = new Batckey();
	private List<Ptrnpf> ptrnpfList = new ArrayList<>();
	private Tr386rec tr386rec = new Tr386rec();
	private Clbapf clbapf;
	private List<Pymtpf> pymtpfList = new ArrayList<>();
	private List<Pymtpf> pymtpfUpdateList = new ArrayList<>();
	private List<Preqpf> preqpfUpdateList = new ArrayList<>();
	private String Sjl54 = "Sjl54";  
	private boolean accountFoundFlag;
	private List<String> cureentpayeeList =  new ArrayList<>();
	private List<String> modifiedpayeeList ;
	private String rq = "RQ";  
	private String rm = "RM"; 
	private String rc = "RC";  
	private String it = "IT" ;
	private String cn = "CN" ;
	private String t5688 = "T5688" ;
	private String dy = "DY";
	private String t3699 = "T3699" ;
	private String longUserID;
	private ZonedDecimalData amountValue;
	private ExternalisedRules er = new ExternalisedRules();
	private PaymentValdrec paymentValdrec = new PaymentValdrec();
	private Map<String,List<String>> inPutParam = new HashMap<>();
	private boolean isErrCode;
	private boolean isErrCode1;
	private static final String CLAIMPYMTVALD = "CLAIMPYMTVALD";
	private static final String PERCENT = "percent";
	private static final String AMOUNT = "amount";
	
	public Pjl54() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl54", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);		
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			throw e;
		}
	}
	
	@Override
	protected void initialise1000()
	{
		if(isNE(wsspcomn.flag,"Y")){
			initialise1001();
			a7050Begin();
		}
	}

	protected void initialise1001()
	{
		longUserID = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString());
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(Varcom.sclr);
		processScreen(Sjl54, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		if(isEQ(wsspcomn.sbmaction, "A")) {
			createDetails();
		}
		
		if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "E")) {
			modifyDetails();
		}
		
		setDate1005();
		sv.reqntype.set("4");
		scrnparams.subfileRrn.set(1);
		wsaaCount.set(1);
		for(wsaaCount.set(1);wsaaCount.toInt()<15;wsaaCount.add(1)) {
			loadSubfile1003();
		}
	}
	
	protected void createDetails() {
		if (isEQ(sv.payNum, SPACES)) {
			alocnorec.function.set("NEXT ");
			alocnorec.prefix.set(rq);
			alocnorec.genkey.set("02");
			alocnorec.company.set(wsspcomn.company);
			alocnorec = AlocnoUtil.getInstance().process(alocnorec); 
			if (isEQ(alocnorec.statuz, Varcom.bomb)) {
				syserrrec.statuz.set(alocnorec.statuz);
				fatalError600();
			}
			else {
				if (isNE(alocnorec.statuz, Varcom.oK)) {
					scrnparams.errorCode.set(alocnorec.statuz);
					wsspcomn.edterror.set("Y");
				}
				else {
					sv.payNum.set(alocnorec.alocNo);
				}
			}
		}

		sv.claimNum.set(wsspcomn.wsaaclaimno);
 		cattpf = cattpfDAO.approvedClaimRecord(sv.claimNum.toString());
		if(null != cattpf) {
			Chdrpf chdrpfObj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), cattpf.getChdrnum());
			sv.cnttype.set(chdrpfObj.getCnttype());
			Descpf descpfObj = descDAO.getdescData(it, t5688, chdrpfObj.getCnttype(), 
					wsspcomn.company.toString(), wsspcomn.language.toString());
			if (descpfObj!=null) {
				sv.ctypedes.set(descpfObj.getLongdesc().trim());
			}
		
		Crsvpf crsvpfObj = crsvpfDAO.getRecord(cattpf.getChdrnum().trim());
		if(null != crsvpfObj) {
			sv.totalPayAmt.set(crsvpfObj.getBalo());
		}
		sv.contractNum.set(cattpf.getChdrnum().trim());
		sv.clientnum.set(cattpf.getLifcnum().trim());
		Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), cattpf.getLifcnum().trim());
		if(null != clntpfObj) {
			sv.clntName.set(clntpfObj.getLsurname().trim().concat(" ")
					.concat(clntpfObj.getLgivname().trim()));		
			bnfypfObjList = bnfypfDAO.getBnfymnaByCoyAndNumAndBtype(wsspcomn.company.toString(), 
					cattpf.getChdrnum(), "DB");
			}	
		}
	}
	
	protected void modifyDetails() {
		sv.payNum.set(wsspcomn.confirmationKey);
		pymtpfList = pymtpfDAO.fetchRecords(sv.payNum.toString());
		if(null != pymtpfList) {
			sv.claimNum.set(pymtpfList.get(0).getClaimnum().trim());
		}
		Cattpf cattpfObj = cattpfDAO.approvedClaimRecord(sv.claimNum.toString().trim());
		if(null != cattpfObj) {
			sv.contractNum.set(cattpfObj.getChdrnum());
		}	
		Chdrpf chdrpfObj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), sv.contractNum.trim());
		if(null != chdrpfObj) {
			sv.cnttype.set(chdrpfObj.getCnttype());
		
			Descpf descpfObj = descDAO.getdescData(it, t5688, chdrpfObj.getCnttype(), 
					wsspcomn.company.toString(), wsspcomn.language.toString());
			if (descpfObj!=null) {
				sv.ctypedes.set(descpfObj.getLongdesc().trim());
			}
		}
		Crsvpf crsvpfObj = crsvpfDAO.getRecord(sv.contractNum.trim());
		if(null != crsvpfObj) {
			sv.totalPayAmt.set(crsvpfObj.getBalo());
				if(null != chdrpfObj) {
					sv.clientnum.set(chdrpfObj.getCownnum().trim());
				
				Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), chdrpfObj.getCownnum().trim());
				if(null != clntpfObj) {
					sv.clntName.set(clntpfObj.getLsurname().trim().concat(" ")
							.concat(clntpfObj.getLgivname().trim()));
				}
			}
		}
	}
	
	protected void setDate1005() {
		datcon1.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1.datcon1Rec);
		wsaaToday.set(datcon1.intDate);
		
		datcon2rec.intDate1.set(wsaaToday);
		datcon2rec.frequency.set(dy);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNextday.set(datcon2rec.intDate2); 
		
		datcon2rec.intDate1.set(wsaaNextday);
		datcon2rec.frequency.set(dy);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNexttoday.set(datcon2rec.intDate2); 
		
		if(isEQ(wsaaTodayYear,wsaaNextdayYear)) {
			setPaydateTypeOne();
		}
		
		else {
			setPaydateTypeTwo();
		}
	}
	
	protected void setPaydateTypeOne() {
		Itempf itempf = itempfDAO.findItemByItem(wsspcomn.fsuco.toString(), t3699, wsaaTodayYear.toString());
		T3699rec t3699recTemp = new T3699rec();
		if(null != itempf) {
			t3699recTemp.t3699Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
			if(isEQ(wsaaTodayMnth,wsaaNextdayMnth)) {
				for (int i = wsaaTodayMnth.toInt(); i <= 12; i++) {
					totaldays.append((t3699recTemp.daywh[i]).toString());
				}
			}
			else
			{
				for (int i = wsaaNextdayMnth.toInt(); i <= 12; i++) {
					totaldays.append((t3699recTemp.daywh[i]).toString());
				}
			}
			setPaydate();
		}
	}

	protected void setPaydate() {
		for(int i=wsaaNextdayDay.toInt(); i<=totaldays.length(); i++) {
			if(totaldays.toString().charAt(i-1) =='.') {
				int newDay = i;
				wsaaTodayDay.set(newDay);
				if(isEQ(wsaaTodayMnth,wsaaNextdayMnth)) {
					sv.payDate.set(wsaaTodayDate);
				}
				else {
					wsaaTodayMnth.set(wsaaNextdayMnth);
					sv.payDate.set(wsaaTodayDate);
				}
				break;
			}
		}
	}
	
	protected void setPaydateTypeTwo() {
		Itempf itempf = itempfDAO.findItemByItem(wsspcomn.fsuco.toString(), t3699, wsaaNextdayYear.toString());
		T3699rec t3699recTemp = new T3699rec();
		if(null != itempf) {
			t3699recTemp.t3699Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
			if(isEQ(wsaaNextdayMnth,wsaaNexttodayMnth)) {
				for (int i = wsaaNextdayMnth.toInt(); i <= 12; i++) {
					totaldays.append((t3699recTemp.daywh[i]).toString());
				}
			}	
			else
			{
				for (int i = wsaaNexttodayMnth.toInt(); i <= 12; i++) {
					totaldays.append((t3699recTemp.daywh[i]).toString());
				}
			}
			setPaydateTwo();
		}
	}

	protected void setPaydateTwo() {
		for(int i=wsaaNexttodayDay.toInt(); i<=totaldays.length(); i++) {
			if(totaldays.toString().charAt(i-1) =='.') {
				int newDay = i;
				wsaaNexttodayDay.set(newDay);
				if(isEQ(wsaaNextdayMnth,wsaaNexttodayMnth)) {
					sv.payDate.set(wsaaNexttodayDate);
				}
				else {
					wsaaNextdayMnth.set(wsaaNexttodayMnth);
					sv.payDate.set(wsaaNexttodayDate);
				}
				break;
			}
		}
	}
	
	protected void a7050Begin()
	{
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		Itempf itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
		if(null != itempf) {
			tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
			if (isEQ(wsspcomn.sbmaction, "A")) {
				sv.scrndesc.set(tr386rec.progdesc[1].toString());
			} else if (isEQ(wsspcomn.sbmaction, "B")) {
				sv.scrndesc.set(tr386rec.progdesc[2].toString());
			} else if (isEQ(wsspcomn.sbmaction, "E")) {
				sv.scrndesc.set(tr386rec.progdesc[3].toString());
			}
		}
	}
	
	protected void loadSubfile1003()
	{
		if (isEQ(wsspcomn.sbmaction, "A")) {
			createSubfie();
		}
		if (isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "E")) {
			modifySubfile();
		}
	}

	protected void screenIo9000()
	{
		processScreen(Sjl54, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void modifySubfile() {
		if(pymtpfList.isEmpty()) {
			loadEmptySubfile1004();
		}
		else {
			for(int iy=0 ; iy<15 ; iy++) {
				if(pymtpfList.size()<=iy) {
					loadEmptySubfile1004();
				}
				else {
					fillmodifydata(iy);
					scrnparams.function.set(Varcom.sadd);
					screenIo9000();
					wsaaCount.add(1);
				}
			}
			wsaaCount.set(20);
		}
	}

	protected void fillmodifydata(int iy) {
		if(pymtpfList.get(iy) != null) {
			sv.clttwo.set(pymtpfList.get(iy).getPayee());
			sv.clntID.set(pymtpfList.get(iy).getClntname());
			sv.bankacckey.set(pymtpfList.get(iy).getBankacckey());
			sv.babrdc.set(pymtpfList.get(iy).getBankkey().concat("  ,").concat(pymtpfList.get(iy).getBankdesc()));
			sv.accdesc.set(pymtpfList.get(iy).getAcctype());
			sv.bankaccdsc.set(pymtpfList.get(iy).getAcctname());
			sv.prcent.set(pymtpfList.get(iy).getPrnt());
			sv.pymt.set(pymtpfList.get(iy).getAmount());
			sv.seqenum.set(wsaaCount);
			sv.cheqpaynum.set(pymtpfList.get(iy).getPaynum_cheq());
		}
	}

	protected void createSubfie() {
		if(bnfypfObjList.isEmpty()) {
			loadEmptySubfile1005();
		}
		else {
			for(int iy=0 ; iy<15 ; iy++) {
				if(bnfypfObjList.size()<=iy) {
					loadEmptySubfile1004();
				}
				else {
					sv.clttwo.set(bnfypfObjList.get(iy).getBnyclt());
					sv.prcent.set(bnfypfObjList.get(iy).getBnypc());
					Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), sv.clttwo.trim());
					checkClient(clntpfObj);
					sv.pymt.set((sv.prcent.getbigdata().multiply(sv.totalPayAmt.getbigdata())).divide(new BigDecimal(100)));
					sv.seqenum.set(wsaaCount); 
					scrnparams.function.set(Varcom.sadd);
					screenIo9000();
					wsaaCount.add(1);
				}
			}
			wsaaCount.set(20);
		}
	}

	protected void checkClient(Clntpf clntpfObj) {
		if(clntpfObj != null) {
			sv.clntID.set(clntpfObj.getLsurname().trim().concat(" ").concat(clntpfObj.getLgivname()));
		}
	}
	
	protected void loadEmptySubfile1004() {
		sv.clttwo.set(SPACES);
		sv.clntID.set(SPACES);
		sv.babrdc.set(SPACES);
		sv.accdesc.set(SPACES);
		sv.bankacckey.set(SPACES);
		sv.bankaccdsc.set(SPACES);
		sv.prcent.set(ZERO);
		sv.pymt.set(ZERO);
		sv.seqenum.set(wsaaCount);
		sv.cheqpaynum.set(SPACES);
		scrnparams.function.set(Varcom.sadd);
		processScreen(Sjl54, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
	}
	
	protected void loadEmptySubfile1005() {
		sv.clttwo.set(SPACES);
		sv.clntID.set(SPACES);
		sv.babrdc.set(SPACES);
		sv.accdesc.set(SPACES);
		sv.bankacckey.set(SPACES);
		sv.bankaccdsc.set(SPACES);
		sv.prcent.set(ZERO);
		sv.pymt.set(ZERO);
		sv.seqenum.set(wsaaCount);
		scrnparams.function.set(Varcom.sadd);
		processScreen(Sjl54, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	@Override
	protected void preScreenEdit() {
		if(isEQ(wsspcomn.sbmaction, "E")) {
			sv.clttwoOut[Varcom.pr.toInt()].set("Y"); 
			sv.clntIDOut[Varcom.pr.toInt()].set("Y"); 
			sv.babrdcOut[Varcom.pr.toInt()].set("Y"); 
			sv.accdescOut[Varcom.pr.toInt()].set("Y"); 
			sv.bankacckeyOut[Varcom.pr.toInt()].set("Y"); 
			sv.bankaccdscOut[Varcom.pr.toInt()].set("Y"); 
			sv.prcentOut[Varcom.pr.toInt()].set("Y"); 
			sv.pymtOut[Varcom.pr.toInt()].set("Y"); 
			scrnparams.function.set(Varcom.prot);
		}
	}
	
	@Override
	protected void screenEdit2000() {
		modifiedpayeeList =  new ArrayList<>();
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(Varcom.srnch);
		processScreen(Sjl54, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		percentTotal = new ZonedDecimalData(5, 2);
		amountTotal = new ZonedDecimalData(17, 2);
		inPutParam.put(PERCENT, new ArrayList<String>());
		inPutParam.put(AMOUNT, new ArrayList<String>());
		while (!(isEQ(scrnparams.statuz,Varcom.endp))) {
			findDetails();
			validateSubfile2100();
			updateErrorIndicators2120();
			readNextModifiedRecord2130();
		}
		settErrors();
	}

	protected void settErrors() {
		if(isNE(wsspcomn.sbmaction, "E") && (modifiedpayeeList.isEmpty())) {
				scrnparams.errorCode.set(jl80);
				sv.clttwoErr.set(jl80);
				sv.clttwoOut[Varcom.ri.toInt()].set("Y");
				wsspcomn.edterror.set("Y");
				sv.errorSubfile.set("Y");
				return;
			}
		if(isNE(wsspcomn.sbmaction, "E")) {
			amountValidate();
		}
	}

	protected void amountValidate() {
		
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(CLAIMPYMTVALD)) {
			paymentValdrec = new PaymentValdrec();
			paymentValdrec.setCntType(sv.cnttype.toString());
			paymentValdrec.setPaymentDate(sv.payDate.toInt());
			paymentValdrec.setPaymentMethod(sv.reqntype.toString());
			paymentValdrec.setNumOfPayments(inPutParam.get(PERCENT).size());
			paymentValdrec.setTotalPymtAmount(sv.totalPayAmt.getbigdata());
			paymentValdrec.setRecPercent(inPutParam.get(PERCENT));
			paymentValdrec.setPymtAmount(inPutParam.get(AMOUNT));
			paymentValdrec.setOutTotalPymtAmount("");
			paymentValdrec.setOutTotalRecPercent("");
			callProgram(CLAIMPYMTVALD,paymentValdrec);
			
			if(isNE(percentTotal,0)) {
				scrnparams.errorCode.set(paymentValdrec.getOutTotalRecPercent()); //e631
			}
			if(isEQ(scrnparams.errorCode,SPACES) && isNE(amountTotal,0))
			scrnparams.errorCode.set(paymentValdrec.getOutTotalPymtAmount()); //jl82

		}
		if (isNE(scrnparams.errorCode,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		processScreen(Sjl54, sv);

	}
	
	protected void findDetails(){
		wsspcomn.chdrCownnum.set(SPACES);
		if(isEQ(sv.clttwo,SPACES)){
			sv.clntID.clear();
		}
		if(isEQ(sv.bankacckey, SPACES)) {
			sv.babrdc.clear();
			sv.accdesc.clear();
			sv.bankaccdsc.clear();
		}
		Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), sv.clttwo.trim());
		if(clntpfObj != null) {
			getCientData(clntpfObj);
		}	

		callVPMSforCalculation();
		amountValue = new ZonedDecimalData(17, 2);
		
		if(!paymentValdrec.getOutRecPercent().isEmpty() &&
				paymentValdrec.getOutRecPercent().get(0).matches(".*[a-zA-Z]+.*")) {
			isErrCode = true;
		}else {
			isErrCode = false;
		}
		if(!paymentValdrec.getOutPymtAmount().isEmpty() &&
				paymentValdrec.getOutPymtAmount().get(0).matches(".*[a-zA-Z]+.*")) {
			isErrCode1 = true;
		}else {
			isErrCode1 = false;
		}
		
		if(!isErrCode1 && isNE(sv.prcent,0)) {
			amountValue.set(paymentValdrec.getOutPymtAmount().get(0));
			
		}
		if(!isErrCode && isEQ(sv.prcent,0)) {
			sv.prcent.set(paymentValdrec.getOutRecPercent().get(0));
		}
		double d = sv.prcent.toDouble();
		String numberD = String.valueOf(d);
		if(!numberD.contains(".0")) {
			sv.prcentErr.set(rl28);
			sv.prcentOut[Varcom.ri.toInt()].set("Y");
		}
		if(isEQ(sv.pymt,0) && isNE(sv.prcent,0)){
			sv.pymt.set(amountValue);
		}
		inPutParam.get(PERCENT).add(sv.prcent.getbigdata().toString());
		inPutParam.get(AMOUNT).add(sv.pymt.getbigdata().toString());
		percentTotal.add(sv.prcent);
		amountTotal.add(sv.pymt);
		pymtpfList = pymtpfDAO.fetchRecords(sv.payNum.toString());
		if(isNE(sv.clttwo,SPACES)){
			modifiedpayeeList.add(sv.clttwo.toString());
		}
	}

	protected void getCientData(Clntpf clntpfObj) {
		sv.clntID.set(clntpfObj.getLsurname().trim().concat(" ").concat(clntpfObj.getLgivname()));	
		if(!isEQ(sv.bankacckey,SPACES)) {
			clbapf = new Clbapf();
			clbapf.setClntpfx(cn);
			clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
			clbapf.setClntnum(clntpfObj.getClntnum().trim());
			List<Clbapf> clbalist = clbapfDAO.searchClbapfDatabyObject(clbapf);
			for(Clbapf tempClba : clbalist) {
				if(isEQ(sv.bankacckey.trim(),tempClba.getBankacckey().trim())) {
					accountFoundFlag = true;
					Babrpf babrpfObj = babrpfDAO.searchBabrpfByBankkey(tempClba.getBankkey());
					writeDetails(tempClba, babrpfObj);
				}
			}
		}
	}
	
	protected void callVPMSforCalculation() {
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(CLAIMPYMTVALD)) {
			paymentValdrec = new PaymentValdrec();
			paymentValdrec.setCntType(sv.cnttype.toString());
			paymentValdrec.setPaymentDate(sv.payDate.toInt());
			paymentValdrec.setPaymentMethod(sv.reqntype.toString());
			paymentValdrec.setNumOfPayments(1);
			paymentValdrec.setTotalPymtAmount(sv.totalPayAmt.getbigdata());
			paymentValdrec.getRecPercent().add(sv.prcent.getbigdata().toString());
			paymentValdrec.getPymtAmount().add(sv.pymt.getbigdata().toString());
			paymentValdrec.setOutTotalPymtAmount("");
			paymentValdrec.setOutTotalRecPercent("");
			callProgram(CLAIMPYMTVALD,paymentValdrec);
		}
	}

	protected void writeDetails(Clbapf tempClba, Babrpf babrpfObj) {
		if((null != babrpfObj.getBankkey()) && (null != babrpfObj.getKanabank()) && (null != babrpfObj.getKanabranch()))
			sv.babrdc.set(babrpfObj.getBankkey().trim().concat(", ".concat(babrpfObj.getKanabank().trim()
					.concat(" ".concat(babrpfObj.getKanabranch().trim())))));
		Descpf descpfObj = descDAO.getdescData(it, "TR338", tempClba.getBnkactyp().trim(), 
				wsspcomn.fsuco.toString(), wsspcomn.language.toString());
		sv.accdesc.set(descpfObj.getLongdesc());
		sv.bankaccdsc.set(tempClba.getBankaccdsc().trim());
	}	
	
	protected void validateSubfile2100()
	{
		boolean checkfieldFlag = false;
		if((isNE(sv.bankacckey,SPACES) || isNE(sv.prcent,0) || isNE(sv.pymt,0))){
			checkfieldFlag = true;
		}
		if (isEQ(sv.clttwo,SPACES) && checkfieldFlag){
			scrnparams.errorCode.set(jl80);
			sv.clttwoErr.set(jl80);
			sv.clttwoOut[Varcom.ri.toInt()].set("Y");
			wsspcomn.edterror.set("Y");
			return;
		}
		if(isNE(sv.clttwo,SPACES)) {
			validate();
		}
	}

	protected void validate() {
		if(isEQ(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(e186);
			sv.bankacckeyOut[Varcom.ri.toInt()].set("Y");
		}
		
		if((!accountFoundFlag) && isNE(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(g599);
			sv.bankacckeyOut[Varcom.ri.toInt()].set("Y");
		}
		
		checkAmount();
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		
	}

	protected void checkAmount() {
		
		if(isEQ(sv.prcentErr, SPACES) && isErrCode) {
			sv.prcentErr.set(paymentValdrec.getOutRecPercent().get(0));
			sv.prcentOut[Varcom.ri.toInt()].set("Y");
		}
		
		if(isErrCode1 && isEQ(sv.pymtErr, SPACES)) {
			sv.pymtErr.set(paymentValdrec.getOutPymtAmount().get(0));	// g822, e631
			sv.pymtOut[Varcom.ri.toInt()].set("Y");
		}	
	}
	
	protected void updateErrorIndicators2120() {
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(Varcom.supd);
		processScreen(Sjl54, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void readNextModifiedRecord2130()
	{
		scrnparams.function.set(Varcom.srnch);
		processScreen(Sjl54, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	@Override
	protected void update3000() {
		checkexistingRecords();	
		
		for(int upflag=1 ; upflag<15 ; upflag++) {
			scrnparams.subfileRrn.set(upflag);
			scrnparams.function.set(Varcom.sread);
			processScreen(Sjl54, sv);
			if (!scrnparams.statuz.equals(Varcom.oK)
			&& !scrnparams.statuz.equals(Varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			if(isNE(sv.clttwo,SPACES) && isNE(sv.prcent,SPACES) && isNE(sv.pymt,SPACES)) {
				addCheqData();
				addPreqData();
				addPymtData();
			}
			
		}
		addPtrnData();
		addCrsvData();
		insertCheqpf(cheqBulkUpdList);
		insertPreqpf(preqpfUpdateList);
		insertPymtpf(pymtpfUpdateList);
		if(isNE(wsspcomn.flag,"Y")){
			insertPtrnpf(ptrnpfList);
			updateCrsvpf(crsvpfItem);
			updateChdrpf();
		}
	}

	protected void checkexistingRecords() {
		if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "A")) {
			pymtpfList = pymtpfDAO.fetchRecords(sv.payNum.toString());
			for(Pymtpf pymtpfitem : pymtpfList) {
				cureentpayeeList.add(pymtpfitem.getPayee());
			}
			cureentpayeeList.removeAll(modifiedpayeeList);
			
			for(String payee : cureentpayeeList ) {
				pymtpfDAO.deletePymtRecord(sv.payNum.toString().trim(), payee);
			}
			checkcheq();
		}
	}

	protected void checkcheq() {
		for(Pymtpf pymtpfobj : pymtpfList) {
			for(String payee : cureentpayeeList) {
				if(isEQ(payee,pymtpfobj.getPayee())) {
					cheqpfDAO.updateRecord(pymtpfobj.getPaynum_cheq());
				}
			}
		}
	}
	
	protected void getPaymentNum() {
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set(rq);
		alocnorec.genkey.set(t3688rec.reqnpfx);
		alocnorec.company.set(wsspcomn.company);
		alocnorec = AlocnoUtil.getInstance().process(alocnorec); 
		if (isEQ(alocnorec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		else {
			if (isNE(alocnorec.statuz, Varcom.oK)) {
				scrnparams.errorCode.set(alocnorec.statuz);
				wsspcomn.edterror.set("Y");
			}
			else {
				cheqpf.setReqnno(alocnorec.alocNo.toString());
				cheqpf.setReqnrev(alocnorec.alocNo.toString());
			}
		}
	}
	
	protected void addCheqData() {
		
		cheqpf = new Cheqpf();
		cheqpf.setReqntype("4");
		cheqpf.setReqncoy(wsspcomn.company.toString());
		cheqpf.setPayamt(sv.pymt.getbigdata());
		cheqpf.setReqnbcde(wsspcomn.bankcode.toString());
		cheqpf.setClntcoy(wsspcomn.fsuco.toString());
		cheqpf.setClntnum01(sv.clttwo.toString());
		cheqpf.setClntnum02(SPACES.toString());
		Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), sv.clttwo.trim());
		if(null != clntpfObj) {
			cheqpf.setClttype01(clntpfObj.getClttype());
			cheqpf.setSurnam01(clntpfObj.getLsurname());
			cheqpf.setCapname(clntpfObj.getLsurname().toUpperCase());
		}
		cheqpf.setClttype02(SPACES.toString());
		cheqpf.setSurnam02(SPACES.toString());
		cheqpf.setSalninit01(SPACES.toString());
		cheqpf.setSalninit02(SPACES.toString());
		cheqpf.setCheqno(SPACES.toString());
		cheqpf.setCheqdupn(SPACES.toString());
		cheqpf.setBankkey(sv.babrdc.substring(0, 10));
		Clbapf clbapfObj = clbapfDAO.searchClbapfRecordData(sv.babrdc.substring(0, 10),
				sv.bankacckey.trim(), wsspcomn.fsuco.toString(), sv.clttwo.trim(), cn);
		cheqpf.setBankacckey(sv.bankacckey.toString());
		if(clbapfObj!=null) {
			cheqpf.setFacthous(clbapfObj.getFacthous());
		}
		cheqpf.setRequser(varcom.vrcmUser.toInt());
		cheqpf.setReqdate(datcon1.intDate.toInt());
		cheqpf.setReqtime(varcom.vrcmTime.toInt());
		cheqpf.setAuthuser(ZERO.intValue());
		cheqpf.setAuthdate(ZERO.intValue());
		cheqpf.setAuthtime(ZERO.intValue());
		cheqpf.setAppruser(ZERO.intValue());
		cheqpf.setApprdte(ZERO.intValue());
		cheqpf.setApprtime(ZERO.intValue());
		if(isEQ(wsspcomn.sbmaction, "A") || isEQ(wsspcomn.sbmaction, "B")) {
			cheqpf.setProcind(rq);
		}
		if(isEQ(wsspcomn.sbmaction, "E")) {
			cheqpf.setProcind(rc);
		}
		cheqpf.setPaydate(sv.payDate.toInt());
		cheqpf.setArchdate(ZERO.intValue());
		cheqpf.setPresflag("N");
		cheqpf.setPresdate(ZERO.intValue());
		cheqpf.setStmtpage(ZERO.intValue());
		Chdrpf chdrpfObbj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),sv.contractNum.trim());
		if(null != chdrpfObbj) {
			cheqpf.setPaycurr(chdrpfObbj.getCntcurr());
		}
		cheqpf.setTrdt(Integer.parseInt(getCobolDate()));
		cheqpf.setTrtm(Integer.parseInt(getCobolTime().substring(0, 6)));
		cheqpf.setUserT(varcom.vrcmUser.toInt());
		cheqpf.setTermid(varcom.vrcmTermid.toString());
		cheqpf.setBranch(wsspcomn.branch.toString());
		cheqpf.setRevind("Y");
		cheqpf.setInvoice_no(SPACES.toString());
		cheqpf.setDuplicate_payment(SPACES.toString());
		cheqpf.setDces(SPACES.toString());
		cheqpf.setValuedate(SPACES.toString());
		cheqpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
		cheqpf.setReasoncd(SPACES.toString());
		cheqpfDAO.setCacheObject(cheqpf);
		modifyCheq();
	}

	private void modifyCheq() {
		if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "E") ||  isEQ(wsspcomn.sbmaction, "A")) {
			pymtpfList = pymtpfDAO.fetchRecords(sv.payNum.toString());
			boolean foundFlag = false;
			if(pymtpfList != null) {
				for(Pymtpf p : pymtpfList) {
					foundFlag = checkforCheq(foundFlag, p);
				}	
			}
			if(isNE(foundFlag,true)) {
				getPaymentNum();
				cheqpfDAO.insertCheqpfObj(cheqpf);
			}
			else {
				cheqpf.setReqnno(sv.cheqpaynum.toString());
				cheqpf.setReqnrev(sv.cheqpaynum.toString());
				cheqBulkUpdList.add(cheqpf);
			}	
		}
	}

	protected boolean checkforCheq(boolean foundFlag, Pymtpf p) {
		if(isEQ(p.getPayee(),sv.clttwo)){
			foundFlag = true;
			sv.cheqpaynum.set(p.getPaynum_cheq());
		}
		return foundFlag;
	}
	
	protected void addPreqData() {
		Chdrpf chdrpfObbj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),sv.contractNum.trim());
		for(int i=0; i<=1; i++) {
			preqpf = new Preqpf();
			preqpf.setRdocpfx(rq);
			preqpf.setRdoccoy(wsspcomn.company.toString().trim());
			preqpf.setRdocnum(cheqpf.getReqnno().trim());
			preqpf.setJrnseq(i);
			preqpf.setBranch(wsspcomn.branch.toString().trim());
			preqpf.setRldgcoy(wsspcomn.company.toString().trim());
			preqpf.setGenlcoy(wsspcomn.company.toString().trim());
			if(null != chdrpfObbj) {
				preqpf.setGenlcur(chdrpfObbj.getCntcurr().trim());
			}
			if(isEQ(i,0)) {
				preqpf.setSacscode("BK");
				preqpf.setSacstyp("PY");
				preqpf.setRldgacct("JP");
				preqpf.setGlsign("-");
				preqpf.setGlcode("BANKACCOUNT");
			}	
			else {
				preqpf.setSacscode("LP");
				preqpf.setSacstyp("PS");
				preqpf.setRldgacct(sv.contractNum.toString());
				preqpf.setGlsign("+");
				preqpf.setGlcode("PYMTSUSP");
			}
			preqpf.setCnttot(wsspdsbs.hdrContot.toInt());
			preqpf.setPostmonth(wsaaBatckey.batcBatcactmn.toString().trim());
			preqpf.setPostyear(wsaaBatckey.batcBatcactyr.toString().trim());
			preqpf.setEffdate(wsaaToday.toInt());
			preqpf.setTrandesc(wsaaLgnmName.toString().trim());
			preqpf.setTranref(SPACES.stringValue());
			preqpf.setOrigamt(sv.pymt.getbigdata());
			preqpf.setAcctamt(sv.pymt.getbigdata());
			preqpf.setOrigccy(cheqpf.getPaycurr().trim());
			preqpf.setCrate(BigDecimal.ZERO);
			preqpf.setTaxcat(SPACES.stringValue());
			preqpf.setFrcdate(ZERO.intValue());
			preqpf.setRcamt(BigDecimal.ZERO);
			preqpf.setTrdt(Integer.parseInt(getCobolDate()));
			preqpf.setTrtm(Integer.parseInt(getCobolTime().substring(0, 6)));
			preqpf.setUser_t(varcom.vrcmUser.toInt());
			preqpf.setTermid(varcom.vrcmTermid.toString().trim());
			preqpf.setExtra(SPACES.stringValue());
			preqpf.setAgntpfx(SPACES.stringValue());
			preqpf.setAgntcoy(SPACES.stringValue());
			preqpf.setAgntnum(SPACES.stringValue());
			preqpf.setZ6taxcde(SPACES.stringValue());
			preqpf.setZ6taxtyp(SPACES.stringValue());
			preqpf.setUniqk(SPACES.stringValue());
			preqpf.setTacc(SPACES.stringValue());
			preqpf.setPacc(SPACES.stringValue());
			preqpf.setZextra(SPACES.stringValue());
			preqpf.setZ6taxinv(SPACES.stringValue());
			preqpf.setZtxinvdt(ZERO.intValue());
			preqpf.setDflag(SPACES.stringValue());
			preqpf.setZ6paikey(ZERO.intValue());
			preqpf.setInd(SPACES.stringValue());
			modifypreq();
		}
	}

	protected void modifypreq() {
		if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "E") || isEQ(wsspcomn.sbmaction, "A")) {
			pymtpfList = pymtpfDAO.fetchRecords(sv.payNum.toString());
			boolean foundflag = false;
				if(pymtpfList != null) {
				for(Pymtpf p : pymtpfList) {
					foundflag = checkPreq(foundflag, p);
				}
			}
			if(isNE(foundflag,true)) {
				preqpfDAO.insertPreqpfRecord(preqpf);
			}
			else {
				preqpfUpdateList.add(preqpf);
			}	
		}
	}

	protected boolean checkPreq(boolean foundflag, Pymtpf p) {
		if(isEQ(p.getPayee(),sv.clttwo)){
			foundflag = true;
		}
		return foundflag;
	}	
	
	protected void addPymtData() {
		pymtpf = new Pymtpf();
		pymtpf.setPaynum_pymt(sv.payNum.toString());
		pymtpf.setClaimnum(sv.claimNum.toString());
		pymtpf.setPayee(sv.clttwo.toString());
		pymtpf.setClntname(sv.clntID.toString());
		pymtpf.setBankkey(sv.babrdc.substring(0, 10));
		pymtpf.setBankdesc(sv.babrdc.substring(11));
		pymtpf.setAcctype(sv.accdesc.toString());
		pymtpf.setBankacckey(sv.bankacckey.toString());
		pymtpf.setAcctname(sv.bankaccdsc.toString());
		pymtpf.setPrnt(sv.prcent.getbigdata());
		pymtpf.setAmount(sv.pymt.getbigdata());	
		if (isEQ(wsspcomn.sbmaction, "A")){
			pymtpf.setProcind(rq);
			pymtpf.setModuser("");
			pymtpf.setModdate(varcom.vrcmMaxDate.toInt());
		}
		if (isEQ(wsspcomn.sbmaction, "B")){
			pymtpf.setProcind(rm);
			pymtpf.setModuser(longUserID);
			pymtpf.setModdate(datcon1.intDate.toInt());
		}
		if (isEQ(wsspcomn.sbmaction, "E")){
			pymtpf.setProcind(rc);
			pymtpf.setModuser(longUserID);
			pymtpf.setModdate(datcon1.intDate.toInt());
		}
		pymtpf.setReqntype("4");
		pymtpf.setReqnbcde(wsspcomn.bankcode.toString());
		pymtpf.setPaydate(sv.payDate.toInt());
		pymtpf.setCreateuser(longUserID);
		pymtpf.setCreatedate(datcon1.intDate.toInt());
		pymtpf.setAppruser("");
		pymtpf.setApprdte(varcom.vrcmMaxDate.toInt());
		pymtpf.setAuthuser("");
		pymtpf.setAuthdate(varcom.vrcmMaxDate.toInt());
		pymtpf.setTotalpyblamount(sv.totalPayAmt.getbigdata());
		pymtpf.setPaynum_cheq(cheqpf.getReqnno());
		if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "E") || isEQ(wsspcomn.sbmaction, "A")) {
			pymtpfList = pymtpfDAO.fetchRecords(sv.payNum.toString());
			boolean foundflag = false;
			if(pymtpfList != null) {
				for(Pymtpf p : pymtpfList) {
					foundflag = checkPymt(foundflag, p);
				}
			}
			if(isNE(foundflag,true)) {
					pymtpfDAO.insertRecord(pymtpf);
			}
			else {
				pymtpfUpdateList.add(pymtpf);
			}
		}
		sv.cheqpaynum.set(SPACES);
	}

	protected boolean checkPymt(boolean foundflag, Pymtpf p) {
		if(isEQ(sv.cheqpaynum,p.getPaynum_cheq())){
			foundflag = true;
		}
		return foundflag;
	}
	
	protected void addPtrnData(){
		
		ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrcoy(wsspcomn.company.toString());
		ptrnpf.setChdrnum(sv.contractNum.trim());
		ptrnpf.setRecode(SPACES.stringValue());
		Chdrpf chdrpfObbj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),sv.contractNum.trim());
		if(null != chdrpfObbj) {
			ptrnpf.setTranno(chdrpfObbj.getTranno() + 1);
		}
		ptrnpf.setPtrneff(sv.payDate.toInt());
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(wsspcomn.company.toString());
		wsaaBatckey1.set(wsspcomn.batchkey);
		ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
		ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
		ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
		ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
		ptrnpf.setPrtflg(SPACES.stringValue());
		ptrnpf.setValidflag("1");
		ptrnpf.setDatesub(datcon1.intDate.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
		ptrnpf.setCrtuser(SPACES.stringValue());
		ptrnpfList.add(ptrnpf);
	}
	
	protected void addCrsvData(){
		if (isEQ(wsspcomn.sbmaction, "A") || (isEQ(wsspcomn.sbmaction, "B"))){
			crsvpfItem.setPaydate(sv.payDate.toInt());
			crsvpfItem.setWreqamt(amountTotal.getbigdata());
			crsvpfItem.setBalo(sv.totalPayAmt.getbigdata());
		}
		if (isEQ(wsspcomn.sbmaction, "E")){
			crsvpfItem.setPaydate(varcom.vrcmMaxDate.toInt());
			crsvpfItem.setWreqamt(BigDecimal.ZERO);
			crsvpfItem.setBalo(sv.totalPayAmt.getbigdata());
		}
	}
	
	
	protected void insertCheqpf(List<Cheqpf> cheqBulkUpdList) {
		if (cheqBulkUpdList != null && !cheqBulkUpdList.isEmpty()){ 
			boolean result = cheqpfDAO.updateCheqpfRecords(cheqBulkUpdList);
			if (!result) {
				LOGGER.error("Insert CheqPF record failed.");
				fatalError600();
			}else cheqBulkUpdList.clear();
		}	
	}
	
	protected void insertPreqpf(List<Preqpf> preqpfUpdateList) {
		if ((isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "A")) 
				&& (preqpfUpdateList != null && !preqpfUpdateList.isEmpty())){ 
			boolean result = preqpfDAO.updatePreqpf(preqpfUpdateList);
			if (!result) {
				LOGGER.error("Insert Preqpf record failed.");
				fatalError600();
			}else preqpfUpdateList.clear();
		}	
	}
	
	protected void insertPymtpf(List<Pymtpf> pymtpfUpdateList) {
		if(pymtpfUpdateList != null && !pymtpfUpdateList.isEmpty()){
			boolean result = pymtpfDAO.updateRecords(pymtpfUpdateList);
			if (!result) {
				fatalError600();
			} else pymtpfUpdateList.clear();
		}
	}
	
	protected void insertPtrnpf(List<Ptrnpf> ptrnpfList) {
		if(ptrnpfList != null && !ptrnpfList.isEmpty()){
			boolean result = ptrnpfDAO.insertPtrnPF(ptrnpfList);
			if (!result) {
				fatalError600();
			} else ptrnpfList.clear();
		}
	}
	
	protected void updateCrsvpf(Crsvpf crsvpfItem) {
		crsvpfItem.setChdrnum(sv.contractNum.trim());
		crsvpfDAO.updateCrsvRecord(crsvpfItem);
	}
	
	private void updateChdrpf() {
		Chdrpf chdrpfitem = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),sv.contractNum.trim());
		Chdrpf chdrpf =  new Chdrpf();
		if(chdrpfitem != null) {
			chdrpf.setChdrnum(chdrpfitem.getChdrnum());
			chdrpf.setChdrcoy(chdrpfitem.getChdrcoy());
			chdrpf.setTranno(chdrpfitem.getTranno() + 1);
		}
		List<Chdrpf> chdrpfList = new ArrayList<>();
		chdrpfList.add(chdrpf);
		boolean isUpdateContractHeader = chdrpfDAO.updateChdrTrannoByChdrnum(chdrpfList);
		if (!isUpdateContractHeader) {
			LOGGER.error("Update Contract Header failed.");
			fatalError600();
		}else {
			chdrpfList.clear();
		}
	} 
	
	@Override
	protected void whereNext4000()
	{
		wsspcomn.flag.set("F");
		wsspcomn.chdrValidflag.set("F");
		wsspcomn.programPtr.add(1);
	}
}	