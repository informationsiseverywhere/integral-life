package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6002screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6002ScreenVars sv = (S6002ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6002screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6002ScreenVars screenVars = (S6002ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.pumethdesc.setClassString("");
		screenVars.pstatdesc.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.pumeth.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.bilfrqdesc.setClassString("");
		screenVars.statdesc.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.fundAmount.setClassString("");
		screenVars.newinsval.setClassString("");
		screenVars.pufee.setClassString("");
	}

/**
 * Clear all the variables in S6002screen
 */
	public static void clear(VarModel pv) {
		S6002ScreenVars screenVars = (S6002ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.crtable.clear();
		screenVars.crtabdesc.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.pumethdesc.clear();
		screenVars.pstatdesc.clear();
		screenVars.billfreq.clear();
		screenVars.pumeth.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.bilfrqdesc.clear();
		screenVars.statdesc.clear();
		screenVars.numpols.clear();
		screenVars.planSuffix.clear();
		screenVars.sumin.clear();
		screenVars.fundAmount.clear();
		screenVars.newinsval.clear();
		screenVars.pufee.clear();
	}
}
