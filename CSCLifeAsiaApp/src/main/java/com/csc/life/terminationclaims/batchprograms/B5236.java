/*
 * File: B5236.java
 * Date: 29 August 2009 21:02:31
 * Author: Quipoz Limited
 * 
 * Class transformed from B5236.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.terminationclaims.dataaccess.RegprgpTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*          REGPPF CONVERSION PROGRAM.
*          --------------------------
*
*    This program forms part of the 9405 Annuities Development.
*
*    This batch program will convert the existing Regular
*    Payment Records (REPG) to include the new field for
*    Certificate of Existence Letter date.
*
*    Initialise to MAX-DATE the Certificate of Existence
*    Date on all Regular Payment Records (REPG).
*
*    This program should only be run once per Company and
*    before any annuity contracts are created.
*
*****************************************************************
* </pre>
*/
public class B5236 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5236");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
	private static final String regprgprec = "REGPRGPREC";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private RegprgpTableDAM regprgpIO = new RegprgpTableDAM();
	private P6671par p6671par = new P6671par();

	public B5236() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaChdrnumfrm.set(p6671par.chdrnum);
		wsaaChdrnumto.set(p6671par.chdrnum1);
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(ZERO);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set("99999999");
		}
		regprgpIO.setChdrcoy(bupaIO.getCompany());
		regprgpIO.setChdrnum(wsaaChdrnumfrm);
		regprgpIO.setLife(SPACES);
		regprgpIO.setCoverage(SPACES);
		regprgpIO.setRider(SPACES);
		regprgpIO.setRgpynum(ZERO);
		regprgpIO.setFunction(varcom.begn);
		regprgpIO.setFormat(regprgprec);
	}

protected void readFile2000()
	{
					readFile2010();
					nextr2080();
				}

protected void readFile2010()
	{
	//performance improvement --  atiwari23 
	//regprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	//regprgpIO.setFitKeysSearch("CHDRNUM");

		SmartFileCode.execute(appVars, regprgpIO);
		if (isNE(regprgpIO.getStatuz(),varcom.oK)
		&& isNE(regprgpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprgpIO.getParams());
			syserrrec.statuz.set(regprgpIO.getStatuz());
			fatalError600();
		}
		if (isLT(regprgpIO.getChdrnum(),wsaaChdrnumfrm)
		|| isGT(regprgpIO.getChdrnum(),wsaaChdrnumto)) {
			regprgpIO.setStatuz(varcom.endp);
		}
		if (isEQ(regprgpIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void nextr2080()
	{
		regprgpIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		regprgpIO.setCertdate(99999999);
		regprgpIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, regprgpIO);
		if (isNE(regprgpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprgpIO.getParams());
			syserrrec.statuz.set(regprgpIO.getStatuz());
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		regprgpIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
