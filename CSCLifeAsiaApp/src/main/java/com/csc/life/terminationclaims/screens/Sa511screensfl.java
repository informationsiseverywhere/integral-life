package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sa511screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 4, 5, 6, 7, 2, 3, 10, 9, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa511ScreenVars sv = (Sa511ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa511screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa511screensfl, 
			sv.Sa511screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sa511ScreenVars sv = (Sa511ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa511screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sa511ScreenVars sv = (Sa511ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa511screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sa511screensflWritten.gt(0))
		{
			sv.sa511screensfl.setCurrentIndex(0);
			sv.Sa511screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sa511ScreenVars sv = (Sa511ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa511screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa511ScreenVars screenVars = (Sa511ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");				
				screenVars.select.setFieldName("select");
				screenVars.notifnum.setFieldName("notifnum");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.notifcdate.setFieldName("notifcdate");
				screenVars.notiflupdate.setFieldName("notiflupdate");
				screenVars.notifcdateDisp.setFieldName("notifcdateDisp");
				screenVars.notiflupdateDisp.setFieldName("notiflupdateDisp");
				screenVars.ntfstat.setFieldName("ntfstat");
				screenVars.userid.setFieldName("userid");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.notifnum.set(dm.getField("notifnum"));
			screenVars.longdesc.set(dm.getField("longdesc"));
			screenVars.notifcdate.set(dm.getField("notifcdate"));
			screenVars.notiflupdate.set(dm.getField("notiflupdate"));
			screenVars.notifcdateDisp.set(dm.getField("notifcdateDisp"));
			screenVars.notiflupdateDisp.set(dm.getField("notiflupdateDisp"));
			screenVars.ntfstat.set(dm.getField("ntfstat"));
			screenVars.userid.set(dm.getField("userid"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa511ScreenVars screenVars = (Sa511ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.notifnum.setFieldName("notifnum");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.notifcdate.setFieldName("notifcdate");
				screenVars.notiflupdate.setFieldName("notiflupdate");
				screenVars.notifcdateDisp.setFieldName("notifcdateDisp");
				screenVars.notiflupdateDisp.setFieldName("notiflupdateDisp");
				screenVars.ntfstat.setFieldName("ntfstat");
				screenVars.userid.setFieldName("userid");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("notifnum").set(screenVars.notifnum);
			dm.getField("longdesc").set(screenVars.longdesc);
			dm.getField("notifcdate").set(screenVars.notifcdate);
			dm.getField("notiflupdate").set(screenVars.notiflupdate);
			dm.getField("notifcdateDisp").set(screenVars.notifcdateDisp);
			dm.getField("notiflupdateDisp").set(screenVars.notiflupdateDisp);
			dm.getField("ntfstat").set(screenVars.ntfstat);
			dm.getField("userid").set(screenVars.userid);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sa511screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sa511ScreenVars screenVars = (Sa511ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.notifnum.clearFormatting();
		screenVars.longdesc.clearFormatting();
		screenVars.notifcdate.clearFormatting();
		screenVars.notiflupdate.clearFormatting();
		screenVars.notifcdateDisp.clearFormatting();
		screenVars.notiflupdateDisp.clearFormatting();
		screenVars.ntfstat.clearFormatting();
		screenVars.userid.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sa511ScreenVars screenVars = (Sa511ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.notifnum.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.notifcdate.setClassString("");
		screenVars.notiflupdate.setClassString("");
		screenVars.notifcdateDisp.setClassString("");
		screenVars.notiflupdateDisp.setClassString("");
		screenVars.ntfstat.setClassString("");
		screenVars.userid.setClassString("");
	}

/**
 * Clear all the variables in S5186screensfl
 */
	public static void clear(VarModel pv) {
		Sa511ScreenVars screenVars = (Sa511ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.notifnum.clear();
		screenVars.longdesc.clear();
		screenVars.notifcdate.clear();
		screenVars.notiflupdate.clear();
		screenVars.notifcdateDisp.clear();
		screenVars.notiflupdateDisp.clear();
		screenVars.ntfstat.clear();
		screenVars.userid.clear();
	}
}
