/*
 * File: Pd5jm.java
 * Date: 05 December 2018
 * Author: qzhang52
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */

package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.screens.S5284ScreenVars;
import com.csc.life.terminationclaims.screens.S5318ScreenVars;
import com.csc.life.terminationclaims.screens.Sd5jmScreenVars;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * Generation Parameters - SCRVER(02)            Do Not Delete|
 *
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *
 * Death Claim Payees Confirmation Window
 * --------------------------------
 * This program will let the user Confim that the Death Claim Payees
 *
 ***********************************************************************
 * </pre>
 */

public class Pd5jm extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5JM");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	/* ERRORS */
	private String rrli = "RRLI";
	private Sd5jmScreenVars sv = ScreenProgram.getScreenVars(Sd5jmScreenVars.class);
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S5284ScreenVars sv5284 = ScreenProgram.getScreenVars(S5284ScreenVars.class);
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		preExit,
		exit2090,
		exit3090
	}

	public Pd5jm() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5jm", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000()
	{
		initialise1010();
	}

	protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
	}

	protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

	protected void preStart()
	{
		return;
	}

	protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

	protected void screenIo2010()
	{

		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		wsspcomn.confirmationKey.set(SPACES);
		if (isNE(sv.confirm,"Y") ) {
			sv5284.confirmErr.set(rrli);
			wsspcomn.confirmationKey.set("N");
		}
	}


	protected void whereNext4000()
	{
        //wsspcomn.nextprog.set(wsaaProg);
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
