package com.csc.life.terminationclaims.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:37
 * Description:
 * Copybook name: TAXCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Taxcpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData taxcpyRec = new FixedLengthStringData(160);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(taxcpyRec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(taxcpyRec, 1);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(taxcpyRec, 9);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(taxcpyRec, 11);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(taxcpyRec, 13);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(taxcpyRec, 15);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(taxcpyRec, 18);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(taxcpyRec, 21);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(taxcpyRec, 25);
  	public FixedLengthStringData paycurr = new FixedLengthStringData(3).isAPartOf(taxcpyRec, 28);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(taxcpyRec, 31);
  	public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(batckey, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(taxcpyRec, 50);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(taxcpyRec, 54);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(taxcpyRec, 58);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(taxcpyRec, 61);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(taxcpyRec, 62);
  	public ZonedDecimalData nextPaydate = new ZonedDecimalData(8, 0).isAPartOf(taxcpyRec, 70);
  	public ZonedDecimalData pymt = new ZonedDecimalData(17, 2).isAPartOf(taxcpyRec, 78);
  	public ZonedDecimalData capcont = new ZonedDecimalData(17, 2).isAPartOf(taxcpyRec, 95);
  	public ZonedDecimalData prcnt = new ZonedDecimalData(7, 3).isAPartOf(taxcpyRec, 112);
  	public ZonedDecimalData regpPrcnt = new ZonedDecimalData(7, 3).isAPartOf(taxcpyRec, 119);
  	public FixedLengthStringData regpayfreq = new FixedLengthStringData(2).isAPartOf(taxcpyRec, 126);
  	public ZonedDecimalData taxamount = new ZonedDecimalData(17, 2).isAPartOf(taxcpyRec, 128);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(taxcpyRec, 145);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(taxcpyRec, 150);
	public FixedLengthStringData paymentFreq = new FixedLengthStringData(2).isAPartOf(taxcpyRec, 154);
  	public ZonedDecimalData taxCapAmount = new ZonedDecimalData(4,0).isAPartOf(taxcpyRec, 156);


	public void initialize() {
		COBOLFunctions.initialize(taxcpyRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		taxcpyRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}