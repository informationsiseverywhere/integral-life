package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.CovppfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Covppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovppfDAOImpl extends BaseDAOImpl<Covppf> implements CovppfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(CovppfDAOImpl.class);

	@Override
	public void insertCovppfRecord(Covppf covppf) {
		StringBuilder sb = new StringBuilder("INSERT INTO COVPPF(CHDRPFX,CHDRCOY,CHDRNUM,VALIDFLAG,EFFDATE,TRCODE,COVRPRPSE,USRPRF, JOBNM, DATIME, CRTABLE,COVERAGE,RIDER ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, covppf.getChdrpfx());
			ps.setString(2, covppf.getChdrcoy());
			ps.setString(3, covppf.getChdrnum());
			ps.setString(4, covppf.getValidflag());
			ps.setInt(5, covppf.getEffdate());
			ps.setString(6, covppf.getTranCode());
			ps.setString(7,covppf.getCovrprpse());
			ps.setString(8, getUsrprf());			    
			ps.setString(9, getJobnm());		
			ps.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
			ps.setString(11,covppf.getCrtable());
			ps.setString(12, covppf.getCoverage());
			ps.setString(13, covppf.getRider());
			ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("insertCovppf()", e);
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("insertCovppf()", ex);
			throw new RuntimeException(ex);
		}
		finally {
			close(ps, null);			
		}
		
	}

	//IBPLIFE-2133 Start
	@Override
	public Covppf getCovppfCrtable(String chdrcoy, String chdrnum, String crtable) {
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRPFX,CHDRCOY,"
				+ "CHDRNUM,VALIDFLAG,EFFDATE,TRCODE,COVRPRPSE,CRTABLE,COVERAGE,RIDER FROM COVPPF ");
		sb.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND CRTABLE = ? AND VALIDFLAG='1'");
		LOGGER.info(sb.toString());
		ResultSet sqlCovppf1rs = null;
	    PreparedStatement psCovpSelect = getPrepareStatement(sb.toString());
	    Covppf covppf = null;
	    try {
	    		psCovpSelect.setString(1, chdrcoy);
	    		psCovpSelect.setString(2, chdrnum);
	    		psCovpSelect.setString(3, crtable);
	            sqlCovppf1rs = executeQuery(psCovpSelect);
	            while (sqlCovppf1rs.next()) {
	            		covppf = new Covppf();
	            		covppf.setUniqueNumber(sqlCovppf1rs.getLong("UNIQUE_NUMBER")); //IBPLIFE-6053
	            		covppf.setChdrpfx(sqlCovppf1rs.getString("CHDRPFX"));
	            		covppf.setChdrcoy(sqlCovppf1rs.getString("CHDRCOY"));
	            		covppf.setChdrnum(sqlCovppf1rs.getString("CHDRNUM"));
	            		covppf.setValidflag(sqlCovppf1rs.getString("VALIDFLAG"));
	            		covppf.setEffdate(sqlCovppf1rs.getInt("EFFDATE"));
	            		covppf.setTranCode(sqlCovppf1rs.getString("TRCODE"));
	            		covppf.setCovrprpse(sqlCovppf1rs.getString("COVRPRPSE"));
	            		covppf.setCrtable(sqlCovppf1rs.getString("CRTABLE"));
	            		covppf.setCoverage(sqlCovppf1rs.getString("COVERAGE"));
	            		covppf.setRider(sqlCovppf1rs.getString("RIDER"));
	            }
	        } catch (SQLException e) {
			LOGGER.error("getCovppfCrtable()", e);
	            throw new SQLRuntimeException(e);
	        }catch(Exception ex){
			LOGGER.error("getCovppfCrtable()", ex);
				ex.printStackTrace();
			} finally {
	            close(psCovpSelect, sqlCovppf1rs);
	        }
		return covppf;
	}

	@Override
	public int updateCovppfCrtable(Covppf covppf) {
		StringBuilder sb = new StringBuilder("UPDATE COVPPF SET EFFDATE=?, COVRPRPSE=?,COVERAGE=?,RIDER=?, DATIME=? "
				+ "WHERE UNIQUE_NUMBER = ?"); //IBPLIFE-6053
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		int executeUpdate;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, covppf.getEffdate());
			ps.setString(2, covppf.getCovrprpse());
			ps.setString(3, covppf.getCoverage());
			ps.setString(4, covppf.getRider());
			ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			ps.setLong(6, covppf.getUniqueNumber()); //IBPLIFE-6053
			executeUpdate = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("updateCovppfCrtable()", e);
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("updateCovppfCrtable()", ex);
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		return executeUpdate;
	}
	//IBPLIFE-2133 End
	@Override
	public Covppf getCovppfCrtable(String chdrcoy, String chdrnum, String crtable,String coverage,String rider) {
		StringBuilder sb = new StringBuilder("SELECT CHDRPFX,CHDRCOY,CHDRNUM,VALIDFLAG,EFFDATE,TRCODE,COVRPRPSE,CRTABLE,COVERAGE,RIDER ,UNIQUE_NUMBER FROM COVPPF ");
		sb.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND CRTABLE = ? AND VALIDFLAG='1' AND COVERAGE=? AND RIDER=?");
		LOGGER.info(sb.toString());
		ResultSet sqlCovppf1rs = null;
	    PreparedStatement psCovpSelect = getPrepareStatement(sb.toString());
	    Covppf covppf = null;
	    try {
	    		psCovpSelect.setString(1, chdrcoy);
	    		psCovpSelect.setString(2, chdrnum);
	    		psCovpSelect.setString(3, crtable);
	    		psCovpSelect.setString(4, coverage);
	    		psCovpSelect.setString(5, rider);
	            sqlCovppf1rs = executeQuery(psCovpSelect);
	            while (sqlCovppf1rs.next()) {
	            		covppf = new Covppf();
	            		covppf.setChdrpfx(sqlCovppf1rs.getString("CHDRPFX"));
	            		covppf.setChdrcoy(sqlCovppf1rs.getString("CHDRCOY"));
	            		covppf.setChdrnum(sqlCovppf1rs.getString("CHDRNUM"));
	            		covppf.setValidflag(sqlCovppf1rs.getString("VALIDFLAG"));
	            		covppf.setEffdate(sqlCovppf1rs.getInt("EFFDATE"));
	            		covppf.setTranCode(sqlCovppf1rs.getString("TRCODE"));
	            		covppf.setCovrprpse(sqlCovppf1rs.getString("COVRPRPSE"));
	            		covppf.setCrtable(sqlCovppf1rs.getString("CRTABLE"));
	            		covppf.setCoverage(sqlCovppf1rs.getString("COVERAGE"));
	            		covppf.setRider(sqlCovppf1rs.getString("RIDER"));
						covppf.setUniqueNumber(sqlCovppf1rs.getLong("UNIQUE_NUMBER"));
	            }
	        } catch (SQLException e) {
			LOGGER.error("getCovppfCrtable1()", e);
	            throw new SQLRuntimeException(e);
	        }catch(Exception ex){
			LOGGER.error("getCovppfCrtable1()", ex);
				ex.printStackTrace();
			} finally {
	            close(psCovpSelect, sqlCovppf1rs);
	        }
		return covppf;
	}
}
