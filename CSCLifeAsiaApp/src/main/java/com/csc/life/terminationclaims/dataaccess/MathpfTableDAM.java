package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MathpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:49
 * Class transformed from MATHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MathpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 198;
	public FixedLengthStringData mathrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData mathpfRecord = mathrec;
	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(mathrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(mathrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(mathrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(mathrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(mathrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(mathrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(mathrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(mathrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(mathrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(mathrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(mathrec);
	public PackedDecimalData policyloan = DD.policyloan.copy().isAPartOf(mathrec);
	public PackedDecimalData tdbtamt = DD.tdbtamt.copy().isAPartOf(mathrec);
	public PackedDecimalData otheradjst = DD.otheradjst.copy().isAPartOf(mathrec);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(mathrec);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(mathrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(mathrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(mathrec);
	public PackedDecimalData estimateTotalValue = DD.estimtotal.copy().isAPartOf(mathrec);
	public PackedDecimalData clamamt = DD.clamamt.copy().isAPartOf(mathrec);
	public PackedDecimalData zrcshamt = DD.zrcshamt.copy().isAPartOf(mathrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(mathrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(mathrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(mathrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MathpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MathpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MathpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MathpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MathpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MathpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MathpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MATHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRNUM, " +
							"CHDRCOY, " +
							"PLNSFX, " +
							"LIFE, " +
							"JLIFE, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"USER_T, " +
							"EFFDATE, " +
							"CURRCD, " +
							"POLICYLOAN, " +
							"TDBTAMT, " +
							"OTHERADJST, " +
							"REASONCD, " +
							"RESNDESC, " +
							"CNTTYPE, " +
							"TRTM, " +
							"ESTIMTOTAL, " +
							"CLAMAMT, " +
							"ZRCSHAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrnum,
                                     chdrcoy,
                                     planSuffix,
                                     life,
                                     jlife,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     user,
                                     effdate,
                                     currcd,
                                     policyloan,
                                     tdbtamt,
                                     otheradjst,
                                     reasoncd,
                                     resndesc,
                                     cnttype,
                                     transactionTime,
                                     estimateTotalValue,
                                     clamamt,
                                     zrcshamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrnum.clear();
  		chdrcoy.clear();
  		planSuffix.clear();
  		life.clear();
  		jlife.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		user.clear();
  		effdate.clear();
  		currcd.clear();
  		policyloan.clear();
  		tdbtamt.clear();
  		otheradjst.clear();
  		reasoncd.clear();
  		resndesc.clear();
  		cnttype.clear();
  		transactionTime.clear();
  		estimateTotalValue.clear();
  		clamamt.clear();
  		zrcshamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMathrec() {
  		return mathrec;
	}

	public FixedLengthStringData getMathpfRecord() {
  		return mathpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMathrec(what);
	}

	public void setMathrec(Object what) {
  		this.mathrec.set(what);
	}

	public void setMathpfRecord(Object what) {
  		this.mathpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(mathrec.getLength());
		result.set(mathrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}