package com.csc.life.terminationclaims.dataaccess;

import com.csc.life.newbusiness.dataaccess.FluppfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FluprgpTableDAM.java
 * Date: Sun, 30 Aug 2009 03:37:57
 * Class transformed from FLUPRGP.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FluprgpTableDAM extends FluppfTableDAM {

	public FluprgpTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("FLUPRGP");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", CLAMNUM"
		             + ", FUPNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "CLAMNUM, " +
		            "FUPNO, " +
		            "TRANNO, " +
		            "FUPTYP, " +
		            "FUPCDE, " +
		            "FUPSTS, " +
		            "FUPDT, " +
		            "FUPRMK, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "EFFDATE, " +
		            "CRTUSER, " +
		            "CRTDATE, " +
		            "LSTUPUSER, " +
		            "ZLSTUPDT, " +
		            "FUPRCVD, " +
		            "EXPRDATE, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "CLAMNUM ASC, " +
		            "FUPNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "CLAMNUM DESC, " +
		            "FUPNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               clamnum,
                               fupno,
                               tranno,
                               fuptype,
                               fupcode,
                               fupstat,
                               fupremdt,
                               fupremk,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               life,
                               jlife,
                               userProfile,
                               jobName,
                               datime,
                               effdate,
                               crtuser,
                               crtdate,
                               lstupuser,
                               zlstupdt,
                               fuprcvd,
                               exprdate,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(237);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getClamnum().toInternal()
					+ getFupno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, clamnum);
			what = ExternalData.chop(what, fupno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(clamnum.toInternal());
	nonKeyFiller40.setInternal(fupno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(183);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getTranno().toInternal()
					+ getFuptype().toInternal()
					+ getFupcode().toInternal()
					+ getFupstat().toInternal()
					+ getFupremdt().toInternal()
					+ getFupremk().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getEffdate().toInternal()
					+ getCrtuser().toInternal()
					+ getCrtdate().toInternal()
					+ getLstupuser().toInternal()
					+ getZlstupdt().toInternal()
					+ getFuprcvd().toInternal()
					+ getExprdate().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, fuptype);
			what = ExternalData.chop(what, fupcode);
			what = ExternalData.chop(what, fupstat);
			what = ExternalData.chop(what, fupremdt);
			what = ExternalData.chop(what, fupremk);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, crtuser);
			what = ExternalData.chop(what, crtdate);
			what = ExternalData.chop(what, lstupuser);
			what = ExternalData.chop(what, zlstupdt);
			what = ExternalData.chop(what, fuprcvd);
			what = ExternalData.chop(what, exprdate);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getClamnum() {
		return clamnum;
	}
	public void setClamnum(Object what) {
		clamnum.set(what);
	}
	public PackedDecimalData getFupno() {
		return fupno;
	}
	public void setFupno(Object what) {
		setFupno(what, false);
	}
	public void setFupno(Object what, boolean rounded) {
		if (rounded)
			fupno.setRounded(what);
		else
			fupno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getFuptype() {
		return fuptype;
	}
	public void setFuptype(Object what) {
		fuptype.set(what);
	}	
	public FixedLengthStringData getFupcode() {
		return fupcode;
	}
	public void setFupcode(Object what) {
		fupcode.set(what);
	}	
	public FixedLengthStringData getFupstat() {
		return fupstat;
	}
	public void setFupstat(Object what) {
		fupstat.set(what);
	}	
	public PackedDecimalData getFupremdt() {
		return fupremdt;
	}
	public void setFupremdt(Object what) {
		setFupremdt(what, false);
	}
	public void setFupremdt(Object what, boolean rounded) {
		if (rounded)
			fupremdt.setRounded(what);
		else
			fupremdt.set(what);
	}	
	public FixedLengthStringData getFupremk() {
		return fupremk;
	}
	public void setFupremk(Object what) {
		fupremk.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getCrtuser() {
		return crtuser;
	}
	public void setCrtuser(Object what) {
		crtuser.set(what);
	}	
	public PackedDecimalData getCrtdate() {
		return crtdate;
	}
	public void setCrtdate(Object what) {
		setCrtdate(what, false);
	}
	public void setCrtdate(Object what, boolean rounded) {
		if (rounded)
			crtdate.setRounded(what);
		else
			crtdate.set(what);
	}	
	public FixedLengthStringData getLstupuser() {
		return lstupuser;
	}
	public void setLstupuser(Object what) {
		lstupuser.set(what);
	}	
	public PackedDecimalData getZlstupdt() {
		return zlstupdt;
	}
	public void setZlstupdt(Object what) {
		setZlstupdt(what, false);
	}
	public void setZlstupdt(Object what, boolean rounded) {
		if (rounded)
			zlstupdt.setRounded(what);
		else
			zlstupdt.set(what);
	}	
	public PackedDecimalData getFuprcvd() {
		return fuprcvd;
	}
	public void setFuprcvd(Object what) {
		setFuprcvd(what, false);
	}
	public void setFuprcvd(Object what, boolean rounded) {
		if (rounded)
			fuprcvd.setRounded(what);
		else
			fuprcvd.set(what);
	}	
	public PackedDecimalData getExprdate() {
		return exprdate;
	}
	public void setExprdate(Object what) {
		setExprdate(what, false);
	}
	public void setExprdate(Object what, boolean rounded) {
		if (rounded)
			exprdate.setRounded(what);
		else
			exprdate.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		clamnum.clear();
		fupno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		tranno.clear();
		fuptype.clear();
		fupcode.clear();
		fupstat.clear();
		fupremdt.clear();
		fupremk.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		life.clear();
		jlife.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();
		effdate.clear();
		crtuser.clear();
		crtdate.clear();
		lstupuser.clear();
		zlstupdt.clear();
		fuprcvd.clear();
		exprdate.clear();		
	}


}