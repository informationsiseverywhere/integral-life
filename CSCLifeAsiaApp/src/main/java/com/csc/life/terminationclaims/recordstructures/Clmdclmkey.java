package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:44
 * Description:
 * Copybook name: CLMDCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Clmdclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData clmdclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData clmdclmKey = new FixedLengthStringData(64).isAPartOf(clmdclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData clmdclmChdrcoy = new FixedLengthStringData(1).isAPartOf(clmdclmKey, 0);
  	public FixedLengthStringData clmdclmChdrnum = new FixedLengthStringData(8).isAPartOf(clmdclmKey, 1);
  	public FixedLengthStringData clmdclmCoverage = new FixedLengthStringData(2).isAPartOf(clmdclmKey, 9);
  	public FixedLengthStringData clmdclmRider = new FixedLengthStringData(2).isAPartOf(clmdclmKey, 11);
  	public FixedLengthStringData clmdclmCrtable = new FixedLengthStringData(4).isAPartOf(clmdclmKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(clmdclmKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(clmdclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		clmdclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}