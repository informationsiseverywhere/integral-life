package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl52screensfl extends Subfile{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {6, 7, 8, 9, 10};
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {6, 7, 8, 9, 10}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 16, 1, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl52ScreenVars sv = (Sjl52ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.Sjl52screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.Sjl52screensfl, 
			sv.Sjl52screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl52ScreenVars sv = (Sjl52ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.Sjl52screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl52ScreenVars sv = (Sjl52ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.Sjl52screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		if (ind3.isOn() && sv.Sjl52screensflWritten.gt(0))
		{
			sv.Sjl52screensfl.setCurrentIndex(0);
			sv.Sjl52screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl52ScreenVars sv = (Sjl52ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.Sjl52screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl52ScreenVars screenVars = (Sjl52ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.claimNum.setFieldName("claimNum");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.clntId.setFieldName("clntId");
				screenVars.contactDateDisp.setFieldName("contactDateDisp");
				screenVars.claimType.setFieldName("claimType");
				screenVars.seqenum.setFieldName("seqenum");
				screenVars.select.setFieldName("select");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.claimNum.set(dm.getField("claimNum"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.clntId.set(dm.getField("clntId"));
			screenVars.contactDateDisp.set(dm.getField("contactDateDisp"));
			screenVars.claimType.set(dm.getField("claimType"));
			screenVars.seqenum.set(dm.getField("seqenum"));
			screenVars.select.set(dm.getField("select"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl52ScreenVars screenVars = (Sjl52ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.claimNum.setFieldName("claimNum");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.clntId.setFieldName("clntId");
				screenVars.contactDateDisp.setFieldName("contactDateDisp");
				screenVars.claimType.setFieldName("claimType");
				screenVars.seqenum.setFieldName("seqenum");
				screenVars.select.setFieldName("select");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("claimNum").set(screenVars.claimNum);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("clntId").set(screenVars.clntId);
			dm.getField("contactDateDisp").set(screenVars.contactDateDisp);
			dm.getField("claimType").set(screenVars.claimType);
			dm.getField("seqenum").set(screenVars.seqenum);
			dm.getField("select").set(screenVars.select);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl52screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl52ScreenVars screenVars = (Sjl52ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.claimNum.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.clntId.clearFormatting();
		screenVars.contactDateDisp.clearFormatting();
		screenVars.claimType.clearFormatting();
		screenVars.seqenum.clearFormatting();
		screenVars.select.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl52ScreenVars screenVars = (Sjl52ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.claimNum.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.clntId.setClassString("");
		screenVars.contactDateDisp.setClassString("");
		screenVars.claimType.setClassString("");
		screenVars.seqenum.setClassString("");
		screenVars.select.setClassString("");
	}

/**
 * Clear all the variables in Sjl52screensfl
 */
	public static void clear(VarModel pv) {
		Sjl52ScreenVars screenVars = (Sjl52ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.claimNum.clear();
		screenVars.chdrnum.clear();
		screenVars.clntId.clear();
		screenVars.contactDateDisp.clear();
		screenVars.claimType.clear();
		screenVars.seqenum.clear();
		screenVars.select.clear();
	}
}
