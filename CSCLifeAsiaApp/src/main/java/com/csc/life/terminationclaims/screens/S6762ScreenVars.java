package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6762
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6762ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(308);
	public FixedLengthStringData dataFields = new FixedLengthStringData(84).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData regpayExcpCsfl = DD.rgpaycsfl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData regpayExcpExpd = DD.rgpayexpd.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData regpayExcpIchs = DD.rgpayichs.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData regpayExcpIcos = DD.rgpayicos.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData regpayExcpInrv = DD.rgpayinrv.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData regpayExcpInsf = DD.rgpayinsf.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData regpayExcpIpst = DD.rgpayipst.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData regpayExcpLtma = DD.rgpayltma.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData regpayExcpNopr = DD.rgpaynopr.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData regpayExcpTinf = DD.rgpaytinf.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 84);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData rgpaycsflErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData rgpayexpdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData rgpayichsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData rgpayicosErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData rgpayinrvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData rgpayinsfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData rgpayipstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData rgpayltmaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData rgpaynoprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData rgpaytinfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 140);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] rgpaycsflOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] rgpayexpdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] rgpayichsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] rgpayicosOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] rgpayinrvOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] rgpayinsfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] rgpayipstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] rgpayltmaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] rgpaynoprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] rgpaytinfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6762screenWritten = new LongData(0);
	public LongData S6762protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6762ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, regpayExcpCsfl, regpayExcpIchs, regpayExcpIcos, regpayExcpIpst, regpayExcpInrv, regpayExcpExpd, regpayExcpInsf, regpayExcpLtma, regpayExcpNopr, regpayExcpTinf};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, rgpaycsflOut, rgpayichsOut, rgpayicosOut, rgpayipstOut, rgpayinrvOut, rgpayexpdOut, rgpayinsfOut, rgpayltmaOut, rgpaynoprOut, rgpaytinfOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, rgpaycsflErr, rgpayichsErr, rgpayicosErr, rgpayipstErr, rgpayinrvErr, rgpayexpdErr, rgpayinsfErr, rgpayltmaErr, rgpaynoprErr, rgpaytinfErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6762screen.class;
		protectRecord = S6762protect.class;
	}

}
