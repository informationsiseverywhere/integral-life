package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HbnfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:05
 * Class transformed from HBNF.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HbnfTableDAM extends HbnfpfTableDAM {

	public HbnfTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HBNF");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "MORTCLS, " +
		            "LIVESNO, " +
		            "WAIVERPREM, " +
		            "CLNTNUM01, " +
		            "CLNTNUM02, " +
		            "CLNTNUM03, " +
		            "CLNTNUM04, " +
		            "CLNTNUM05, " +
		            "CLNTNUM06, " +
		            "CLNTNUM07, " +
		            "CLNTNUM08, " +
		            "CLNTNUM09, " +
		            "CLNTNUM10, " +
		            "RELATION01, " +
		            "RELATION02, " +
		            "RELATION03, " +
		            "RELATION04, " +
		            "RELATION05, " +
		            "RELATION06, " +
		            "RELATION07, " +
		            "RELATION08, " +
		            "RELATION09, " +
		            "RELATION10, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "EFFDATE, " +
		            "CRTABLE, " +
		            "BENPLN, " +
		            "ZUNIT, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               mortcls,
                               livesno,
                               waiverprem,
                               clntnum01,
                               clntnum02,
                               clntnum03,
                               clntnum04,
                               clntnum05,
                               clntnum06,
                               clntnum07,
                               clntnum08,
                               clntnum09,
                               clntnum10,
                               relation01,
                               relation02,
                               relation03,
                               relation04,
                               relation05,
                               relation06,
                               relation07,
                               relation08,
                               relation09,
                               relation10,
                               life,
                               coverage,
                               rider,
                               effdate,
                               crtable,
                               benpln,
                               zunit,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);

	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(177);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getMortcls().toInternal()
					+ getLivesno().toInternal()
					+ getWaiverprem().toInternal()
					+ getClntnum01().toInternal()
					+ getClntnum02().toInternal()
					+ getClntnum03().toInternal()
					+ getClntnum04().toInternal()
					+ getClntnum05().toInternal()
					+ getClntnum06().toInternal()
					+ getClntnum07().toInternal()
					+ getClntnum08().toInternal()
					+ getClntnum09().toInternal()
					+ getClntnum10().toInternal()
					+ getRelation01().toInternal()
					+ getRelation02().toInternal()
					+ getRelation03().toInternal()
					+ getRelation04().toInternal()
					+ getRelation05().toInternal()
					+ getRelation06().toInternal()
					+ getRelation07().toInternal()
					+ getRelation08().toInternal()
					+ getRelation09().toInternal()
					+ getRelation10().toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getEffdate().toInternal()
					+ getCrtable().toInternal()
					+ getBenpln().toInternal()
					+ getZunit().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, mortcls);
			what = ExternalData.chop(what, livesno);
			what = ExternalData.chop(what, waiverprem);
			what = ExternalData.chop(what, clntnum01);
			what = ExternalData.chop(what, clntnum02);
			what = ExternalData.chop(what, clntnum03);
			what = ExternalData.chop(what, clntnum04);
			what = ExternalData.chop(what, clntnum05);
			what = ExternalData.chop(what, clntnum06);
			what = ExternalData.chop(what, clntnum07);
			what = ExternalData.chop(what, clntnum08);
			what = ExternalData.chop(what, clntnum09);
			what = ExternalData.chop(what, clntnum10);
			what = ExternalData.chop(what, relation01);
			what = ExternalData.chop(what, relation02);
			what = ExternalData.chop(what, relation03);
			what = ExternalData.chop(what, relation04);
			what = ExternalData.chop(what, relation05);
			what = ExternalData.chop(what, relation06);
			what = ExternalData.chop(what, relation07);
			what = ExternalData.chop(what, relation08);
			what = ExternalData.chop(what, relation09);
			what = ExternalData.chop(what, relation10);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, benpln);
			what = ExternalData.chop(what, zunit);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getMortcls() {
		return mortcls;
	}
	public void setMortcls(Object what) {
		mortcls.set(what);
	}	
	public FixedLengthStringData getLivesno() {
		return livesno;
	}
	public void setLivesno(Object what) {
		livesno.set(what);
	}	
	public FixedLengthStringData getWaiverprem() {
		return waiverprem;
	}
	public void setWaiverprem(Object what) {
		waiverprem.set(what);
	}	
	public FixedLengthStringData getClntnum01() {
		return clntnum01;
	}
	public void setClntnum01(Object what) {
		clntnum01.set(what);
	}	
	public FixedLengthStringData getClntnum02() {
		return clntnum02;
	}
	public void setClntnum02(Object what) {
		clntnum02.set(what);
	}	
	public FixedLengthStringData getClntnum03() {
		return clntnum03;
	}
	public void setClntnum03(Object what) {
		clntnum03.set(what);
	}	
	public FixedLengthStringData getClntnum04() {
		return clntnum04;
	}
	public void setClntnum04(Object what) {
		clntnum04.set(what);
	}	
	public FixedLengthStringData getClntnum05() {
		return clntnum05;
	}
	public void setClntnum05(Object what) {
		clntnum05.set(what);
	}	
	public FixedLengthStringData getClntnum06() {
		return clntnum06;
	}
	public void setClntnum06(Object what) {
		clntnum06.set(what);
	}	
	public FixedLengthStringData getClntnum07() {
		return clntnum07;
	}
	public void setClntnum07(Object what) {
		clntnum07.set(what);
	}	
	public FixedLengthStringData getClntnum08() {
		return clntnum08;
	}
	public void setClntnum08(Object what) {
		clntnum08.set(what);
	}	
	public FixedLengthStringData getClntnum09() {
		return clntnum09;
	}
	public void setClntnum09(Object what) {
		clntnum09.set(what);
	}	
	public FixedLengthStringData getClntnum10() {
		return clntnum10;
	}
	public void setClntnum10(Object what) {
		clntnum10.set(what);
	}	
	public FixedLengthStringData getRelation01() {
		return relation01;
	}
	public void setRelation01(Object what) {
		relation01.set(what);
	}	
	public FixedLengthStringData getRelation02() {
		return relation02;
	}
	public void setRelation02(Object what) {
		relation02.set(what);
	}	
	public FixedLengthStringData getRelation03() {
		return relation03;
	}
	public void setRelation03(Object what) {
		relation03.set(what);
	}	
	public FixedLengthStringData getRelation04() {
		return relation04;
	}
	public void setRelation04(Object what) {
		relation04.set(what);
	}	
	public FixedLengthStringData getRelation05() {
		return relation05;
	}
	public void setRelation05(Object what) {
		relation05.set(what);
	}	
	public FixedLengthStringData getRelation06() {
		return relation06;
	}
	public void setRelation06(Object what) {
		relation06.set(what);
	}	
	public FixedLengthStringData getRelation07() {
		return relation07;
	}
	public void setRelation07(Object what) {
		relation07.set(what);
	}	
	public FixedLengthStringData getRelation08() {
		return relation08;
	}
	public void setRelation08(Object what) {
		relation08.set(what);
	}	
	public FixedLengthStringData getRelation09() {
		return relation09;
	}
	public void setRelation09(Object what) {
		relation09.set(what);
	}	
	public FixedLengthStringData getRelation10() {
		return relation10;
	}
	public void setRelation10(Object what) {
		relation10.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getBenpln() {
		return benpln;
	}
	public void setBenpln(Object what) {
		benpln.set(what);
	}	
	public PackedDecimalData getZunit() {
		return zunit;
	}
	public void setZunit(Object what) {
		setZunit(what, false);
	}
	public void setZunit(Object what, boolean rounded) {
		if (rounded)
			zunit.setRounded(what);
		else
			zunit.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getRelations() {
		return new FixedLengthStringData(relation01.toInternal()
										+ relation02.toInternal()
										+ relation03.toInternal()
										+ relation04.toInternal()
										+ relation05.toInternal()
										+ relation06.toInternal()
										+ relation07.toInternal()
										+ relation08.toInternal()
										+ relation09.toInternal()
										+ relation10.toInternal());
	}
	public void setRelations(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getRelations().getLength()).init(obj);
	
		what = ExternalData.chop(what, relation01);
		what = ExternalData.chop(what, relation02);
		what = ExternalData.chop(what, relation03);
		what = ExternalData.chop(what, relation04);
		what = ExternalData.chop(what, relation05);
		what = ExternalData.chop(what, relation06);
		what = ExternalData.chop(what, relation07);
		what = ExternalData.chop(what, relation08);
		what = ExternalData.chop(what, relation09);
		what = ExternalData.chop(what, relation10);
	}
	public FixedLengthStringData getRelation(BaseData indx) {
		return getRelation(indx.toInt());
	}
	public FixedLengthStringData getRelation(int indx) {

		switch (indx) {
			case 1 : return relation01;
			case 2 : return relation02;
			case 3 : return relation03;
			case 4 : return relation04;
			case 5 : return relation05;
			case 6 : return relation06;
			case 7 : return relation07;
			case 8 : return relation08;
			case 9 : return relation09;
			case 10 : return relation10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setRelation(BaseData indx, Object what) {
		setRelation(indx.toInt(), what);
	}
	public void setRelation(int indx, Object what) {

		switch (indx) {
			case 1 : setRelation01(what);
					 break;
			case 2 : setRelation02(what);
					 break;
			case 3 : setRelation03(what);
					 break;
			case 4 : setRelation04(what);
					 break;
			case 5 : setRelation05(what);
					 break;
			case 6 : setRelation06(what);
					 break;
			case 7 : setRelation07(what);
					 break;
			case 8 : setRelation08(what);
					 break;
			case 9 : setRelation09(what);
					 break;
			case 10 : setRelation10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getClntnums() {
		return new FixedLengthStringData(clntnum01.toInternal()
										+ clntnum02.toInternal()
										+ clntnum03.toInternal()
										+ clntnum04.toInternal()
										+ clntnum05.toInternal()
										+ clntnum06.toInternal()
										+ clntnum07.toInternal()
										+ clntnum08.toInternal()
										+ clntnum09.toInternal()
										+ clntnum10.toInternal());
	}
	public void setClntnums(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getClntnums().getLength()).init(obj);
	
		what = ExternalData.chop(what, clntnum01);
		what = ExternalData.chop(what, clntnum02);
		what = ExternalData.chop(what, clntnum03);
		what = ExternalData.chop(what, clntnum04);
		what = ExternalData.chop(what, clntnum05);
		what = ExternalData.chop(what, clntnum06);
		what = ExternalData.chop(what, clntnum07);
		what = ExternalData.chop(what, clntnum08);
		what = ExternalData.chop(what, clntnum09);
		what = ExternalData.chop(what, clntnum10);
	}
	public FixedLengthStringData getClntnum(BaseData indx) {
		return getClntnum(indx.toInt());
	}
	public FixedLengthStringData getClntnum(int indx) {

		switch (indx) {
			case 1 : return clntnum01;
			case 2 : return clntnum02;
			case 3 : return clntnum03;
			case 4 : return clntnum04;
			case 5 : return clntnum05;
			case 6 : return clntnum06;
			case 7 : return clntnum07;
			case 8 : return clntnum08;
			case 9 : return clntnum09;
			case 10 : return clntnum10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setClntnum(BaseData indx, Object what) {
		setClntnum(indx.toInt(), what);
	}
	public void setClntnum(int indx, Object what) {

		switch (indx) {
			case 1 : setClntnum01(what);
					 break;
			case 2 : setClntnum02(what);
					 break;
			case 3 : setClntnum03(what);
					 break;
			case 4 : setClntnum04(what);
					 break;
			case 5 : setClntnum05(what);
					 break;
			case 6 : setClntnum06(what);
					 break;
			case 7 : setClntnum07(what);
					 break;
			case 8 : setClntnum08(what);
					 break;
			case 9 : setClntnum09(what);
					 break;
			case 10 : setClntnum10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		mortcls.clear();
		livesno.clear();
		waiverprem.clear();
		clntnum01.clear();
		clntnum02.clear();
		clntnum03.clear();
		clntnum04.clear();
		clntnum05.clear();
		clntnum06.clear();
		clntnum07.clear();
		clntnum08.clear();
		clntnum09.clear();
		clntnum10.clear();
		relation01.clear();
		relation02.clear();
		relation03.clear();
		relation04.clear();
		relation05.clear();
		relation06.clear();
		relation07.clear();
		relation08.clear();
		relation09.clear();
		relation10.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		effdate.clear();
		crtable.clear();
		benpln.clear();
		zunit.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}