package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Td5hprec  extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData td5hpRec = new FixedLengthStringData(230);
	  	public FixedLengthStringData remadays = new FixedLengthStringData(100).isAPartOf(td5hpRec, 0);
	  	public ZonedDecimalData[] remaday = ZDArrayPartOfStructure(20, 5, 0, remadays, 0);
	  	public FixedLengthStringData filler = new FixedLengthStringData(100).isAPartOf(remadays, 0, FILLER_REDEFINE);
	  	public ZonedDecimalData remaday01 = new ZonedDecimalData(5, 0).isAPartOf(filler, 0);
	  	public ZonedDecimalData remaday02 = new ZonedDecimalData(5, 0).isAPartOf(filler, 5);
	  	public ZonedDecimalData remaday03 = new ZonedDecimalData(5, 0).isAPartOf(filler, 10);
	  	public ZonedDecimalData remaday04 = new ZonedDecimalData(5, 0).isAPartOf(filler, 15);
	  	public ZonedDecimalData remaday05 = new ZonedDecimalData(5, 0).isAPartOf(filler, 20);
	  	public ZonedDecimalData remaday06 = new ZonedDecimalData(5, 0).isAPartOf(filler, 25);
	  	public ZonedDecimalData remaday07 = new ZonedDecimalData(5, 0).isAPartOf(filler, 30);
	  	public ZonedDecimalData remaday08 = new ZonedDecimalData(5, 0).isAPartOf(filler, 35);
	  	public ZonedDecimalData remaday09 = new ZonedDecimalData(5, 0).isAPartOf(filler, 40);
	  	public ZonedDecimalData remaday10 = new ZonedDecimalData(5, 0).isAPartOf(filler, 45);
	  	public ZonedDecimalData remaday11 = new ZonedDecimalData(5, 0).isAPartOf(filler, 50);
	  	public ZonedDecimalData remaday12 = new ZonedDecimalData(5, 0).isAPartOf(filler, 55);
	  	public ZonedDecimalData remaday13 = new ZonedDecimalData(5, 0).isAPartOf(filler, 60);
	  	public ZonedDecimalData remaday14 = new ZonedDecimalData(5, 0).isAPartOf(filler, 65);
	  	public ZonedDecimalData remaday15 = new ZonedDecimalData(5, 0).isAPartOf(filler, 70);
	  	public ZonedDecimalData remaday16 = new ZonedDecimalData(5, 0).isAPartOf(filler, 75);
	  	public ZonedDecimalData remaday17 = new ZonedDecimalData(5, 0).isAPartOf(filler, 80);
	  	public ZonedDecimalData remaday18 = new ZonedDecimalData(5, 0).isAPartOf(filler, 85);
	  	public ZonedDecimalData remaday19 = new ZonedDecimalData(5, 0).isAPartOf(filler, 90);
	  	public ZonedDecimalData remaday20 = new ZonedDecimalData(5, 0).isAPartOf(filler, 95);
	  	public FixedLengthStringData rates = new FixedLengthStringData(120).isAPartOf(td5hpRec, 100);
	  	public ZonedDecimalData[] rate = ZDArrayPartOfStructure(20, 6, 4, rates, 0);
	  	public FixedLengthStringData filler1 = new FixedLengthStringData(120).isAPartOf(rates, 0, FILLER_REDEFINE);
		public ZonedDecimalData rate01 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 0);
		public ZonedDecimalData rate02 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 6);
		public ZonedDecimalData rate03 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 12);
		public ZonedDecimalData rate04 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 18);
		public ZonedDecimalData rate05 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 24);
		public ZonedDecimalData rate06 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 30);
		public ZonedDecimalData rate07 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 36);
		public ZonedDecimalData rate08 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 42);
		public ZonedDecimalData rate09 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 48);
		public ZonedDecimalData rate10 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 54);
		public ZonedDecimalData rate11 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 60);
		public ZonedDecimalData rate12 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 66);
		public ZonedDecimalData rate13 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 72);
		public ZonedDecimalData rate14 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 78);
		public ZonedDecimalData rate15 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 84);
		public ZonedDecimalData rate16 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 90);
		public ZonedDecimalData rate17 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 96);
		public ZonedDecimalData rate18 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 102);
		public ZonedDecimalData rate19 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 108);
		public ZonedDecimalData rate20 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 114);
	  	public FixedLengthStringData filler2 = new FixedLengthStringData(10).isAPartOf(td5hpRec, 220, FILLER);


		public void initialize() {
			COBOLFunctions.initialize(td5hpRec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			td5hpRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
