package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:27
 * Description:
 * Copybook name: HCLABCAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclabcakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclabcaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclabcaKey = new FixedLengthStringData(256).isAPartOf(hclabcaFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclabcaChdrcoy = new FixedLengthStringData(1).isAPartOf(hclabcaKey, 0);
  	public FixedLengthStringData hclabcaChdrnum = new FixedLengthStringData(8).isAPartOf(hclabcaKey, 1);
  	public FixedLengthStringData hclabcaLife = new FixedLengthStringData(2).isAPartOf(hclabcaKey, 9);
  	public FixedLengthStringData hclabcaCoverage = new FixedLengthStringData(2).isAPartOf(hclabcaKey, 11);
  	public FixedLengthStringData hclabcaRider = new FixedLengthStringData(2).isAPartOf(hclabcaKey, 13);
  	public FixedLengthStringData hclabcaHosben = new FixedLengthStringData(5).isAPartOf(hclabcaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(236).isAPartOf(hclabcaKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclabcaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclabcaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}