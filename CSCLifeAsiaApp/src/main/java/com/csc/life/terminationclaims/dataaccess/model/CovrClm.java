package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class CovrClm{
    private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String validflag;
	private int tranno;
	private int currfrom;
	private int currto;
	private String statcode;
	private String pstatcode;
	private String statreasn;
	private int crrcd;
	private int anbAtCcd;
	private String sex;
	private String reptcd01;
	private String reptcd02;
	private String reptcd03;
	private String reptcd04;
	private String reptcd05;
	private String reptcd06;
	private BigDecimal crInstamt01;
	private BigDecimal crInstamt02;
	private BigDecimal crInstamt03;
	private BigDecimal crInstamt04;
	private BigDecimal crInstamt05;
	private String premCurrency;
	private String termid;
	private int transactionDate;
	private int transactionTime;
	private String statFund;
	private String statSect;
	private String statSubsect;
	private String crtable;
	private int riskCessDate;
	private int premCessDate;
	private int benCessDate;
	private int nextActDate;
	private int riskCessAge;
	private int premCessAge;
	private int benCessAge;
	private int riskCessTerm;
	private int premCessTerm;
	private int benCessTerm;
	private BigDecimal sumins;
	private String sicurr;
	private BigDecimal varSumInsured;
	private String mortcls;
	private String liencd;
	private String ratingClass;
	private String indexationInd;
	private String bonusInd;
	private String deferPerdCode;
	private BigDecimal deferPerdAmt;
	private String deferPerdInd;
	private BigDecimal totMthlyBenefit;
	private BigDecimal singp;
	private BigDecimal instprem;
	private BigDecimal estMatValue01;
	private BigDecimal estMatValue02;
	private int estMatDate01;
	private int estMatDate02;
	private BigDecimal estMatInt01;
	private BigDecimal estMatInt02;
	private String campaign;
	private BigDecimal statSumins;
	private int rtrnyrs;
	private int premCessAgeMth;
	private int premCessAgeDay;
	private int premCessTermMth;
	private int premCessTermDay;
	private int riskCessAgeMth;
	private int riskCessAgeDay;
	private int riskCessTermMth;
	private int riskCessTermDay;
	private String jlLsInd;
	private int unitStatementDate;
	private int convertInitialUnits;
	private String userProfile;
	private String jobName;
	private String datime;
	private int user;
	
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getJlife() {
        return jlife;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
   
    public int getPlanSuffix() {
        return planSuffix;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public String getValidflag() {
        return validflag;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public int getTranno() {
        return tranno;
    }
    public void setTranno(int tranno) {
        this.tranno = tranno;
    }
    public int getCurrfrom() {
        return currfrom;
    }
    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }
    public int getCurrto() {
        return currto;
    }
    public void setCurrto(int currto) {
        this.currto = currto;
    }
    public String getStatcode() {
        return statcode;
    }
    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }
    public String getPstatcode() {
        return pstatcode;
    }
    public void setPstatcode(String pstatcode) {
        this.pstatcode = pstatcode;
    }
    public String getStatreasn() {
        return statreasn;
    }
    public void setStatreasn(String statreasn) {
        this.statreasn = statreasn;
    }
    public int getCrrcd() {
        return crrcd;
    }
    public void setCrrcd(int crrcd) {
        this.crrcd = crrcd;
    }
    public int getAnbAtCcd() {
        return anbAtCcd;
    }
    public void setAnbAtCcd(int anbAtCcd) {
        this.anbAtCcd = anbAtCcd;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    public String getReptcd01() {
        return reptcd01;
    }
    public void setReptcd01(String reptcd01) {
        this.reptcd01 = reptcd01;
    }
    public String getReptcd02() {
        return reptcd02;
    }
    public void setReptcd02(String reptcd02) {
        this.reptcd02 = reptcd02;
    }
    public String getReptcd03() {
        return reptcd03;
    }
    public void setReptcd03(String reptcd03) {
        this.reptcd03 = reptcd03;
    }
    public String getReptcd04() {
        return reptcd04;
    }
    public void setReptcd04(String reptcd04) {
        this.reptcd04 = reptcd04;
    }
    public String getReptcd05() {
        return reptcd05;
    }
    public void setReptcd05(String reptcd05) {
        this.reptcd05 = reptcd05;
    }
    public String getReptcd06() {
        return reptcd06;
    }
    public void setReptcd06(String reptcd06) {
        this.reptcd06 = reptcd06;
    }
    public BigDecimal getCrInstamt01() {
        return crInstamt01;
    }
    public void setCrInstamt01(BigDecimal crInstamt01) {
        this.crInstamt01 = crInstamt01;
    }
    public BigDecimal getCrInstamt02() {
        return crInstamt02;
    }
    public void setCrInstamt02(BigDecimal crInstamt02) {
        this.crInstamt02 = crInstamt02;
    }
    public BigDecimal getCrInstamt03() {
        return crInstamt03;
    }
    public void setCrInstamt03(BigDecimal crInstamt03) {
        this.crInstamt03 = crInstamt03;
    }
    public BigDecimal getCrInstamt04() {
        return crInstamt04;
    }
    public void setCrInstamt04(BigDecimal crInstamt04) {
        this.crInstamt04 = crInstamt04;
    }
    public BigDecimal getCrInstamt05() {
        return crInstamt05;
    }
    public void setCrInstamt05(BigDecimal crInstamt05) {
        this.crInstamt05 = crInstamt05;
    }
    public String getPremCurrency() {
        return premCurrency;
    }
    public void setPremCurrency(String premCurrency) {
        this.premCurrency = premCurrency;
    }
    public String getTermid() {
        return termid;
    }
    public void setTermid(String termid) {
        this.termid = termid;
    }
    public int getTransactionDate() {
        return transactionDate;
    }
    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }
    public int getTransactionTime() {
        return transactionTime;
    }
    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }
    public String getStatFund() {
        return statFund;
    }
    public void setStatFund(String statFund) {
        this.statFund = statFund;
    }
    public String getStatSect() {
        return statSect;
    }
    public void setStatSect(String statSect) {
        this.statSect = statSect;
    }
    public String getStatSubsect() {
        return statSubsect;
    }
    public void setStatSubsect(String statSubsect) {
        this.statSubsect = statSubsect;
    }
    public String getCrtable() {
        return crtable;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    public int getRiskCessDate() {
        return riskCessDate;
    }
    public void setRiskCessDate(int riskCessDate) {
        this.riskCessDate = riskCessDate;
    }
    public int getPremCessDate() {
        return premCessDate;
    }
    public void setPremCessDate(int premCessDate) {
        this.premCessDate = premCessDate;
    }
    public int getBenCessDate() {
        return benCessDate;
    }
    public void setBenCessDate(int benCessDate) {
        this.benCessDate = benCessDate;
    }
    public int getNextActDate() {
        return nextActDate;
    }
    public void setNextActDate(int nextActDate) {
        this.nextActDate = nextActDate;
    }
    public int getRiskCessAge() {
        return riskCessAge;
    }
    public void setRiskCessAge(int riskCessAge) {
        this.riskCessAge = riskCessAge;
    }
    public int getPremCessAge() {
        return premCessAge;
    }
    public void setPremCessAge(int premCessAge) {
        this.premCessAge = premCessAge;
    }
    public int getBenCessAge() {
        return benCessAge;
    }
    public void setBenCessAge(int benCessAge) {
        this.benCessAge = benCessAge;
    }
    public int getRiskCessTerm() {
        return riskCessTerm;
    }
    public void setRiskCessTerm(int riskCessTerm) {
        this.riskCessTerm = riskCessTerm;
    }
    public int getPremCessTerm() {
        return premCessTerm;
    }
    public void setPremCessTerm(int premCessTerm) {
        this.premCessTerm = premCessTerm;
    }
    public int getBenCessTerm() {
        return benCessTerm;
    }
    public void setBenCessTerm(int benCessTerm) {
        this.benCessTerm = benCessTerm;
    }
    public BigDecimal getSumins() {
        return sumins;
    }
    public void setSumins(BigDecimal sumins) {
        this.sumins = sumins;
    }
    public String getSicurr() {
        return sicurr;
    }
    public void setSicurr(String sicurr) {
        this.sicurr = sicurr;
    }
    public BigDecimal getVarSumInsured() {
        return varSumInsured;
    }
    public void setVarSumInsured(BigDecimal varSumInsured) {
        this.varSumInsured = varSumInsured;
    }
    public String getMortcls() {
        return mortcls;
    }
    public void setMortcls(String mortcls) {
        this.mortcls = mortcls;
    }
    public String getLiencd() {
        return liencd;
    }
    public void setLiencd(String liencd) {
        this.liencd = liencd;
    }
    public String getRatingClass() {
        return ratingClass;
    }
    public void setRatingClass(String ratingClass) {
        this.ratingClass = ratingClass;
    }
    public String getIndexationInd() {
        return indexationInd;
    }
    public void setIndexationInd(String indexationInd) {
        this.indexationInd = indexationInd;
    }
    public String getBonusInd() {
        return bonusInd;
    }
    public void setBonusInd(String bonusInd) {
        this.bonusInd = bonusInd;
    }
    public String getDeferPerdCode() {
        return deferPerdCode;
    }
    public void setDeferPerdCode(String deferPerdCode) {
        this.deferPerdCode = deferPerdCode;
    }
    public BigDecimal getDeferPerdAmt() {
        return deferPerdAmt;
    }
    public void setDeferPerdAmt(BigDecimal deferPerdAmt) {
        this.deferPerdAmt = deferPerdAmt;
    }
    public String getDeferPerdInd() {
        return deferPerdInd;
    }
    public void setDeferPerdInd(String deferPerdInd) {
        this.deferPerdInd = deferPerdInd;
    }
    public BigDecimal getTotMthlyBenefit() {
        return totMthlyBenefit;
    }
    public void setTotMthlyBenefit(BigDecimal totMthlyBenefit) {
        this.totMthlyBenefit = totMthlyBenefit;
    }
    public BigDecimal getSingp() {
        return singp;
    }
    public void setSingp(BigDecimal singp) {
        this.singp = singp;
    }
    public BigDecimal getInstprem() {
        return instprem;
    }
    public void setInstprem(BigDecimal instprem) {
        this.instprem = instprem;
    }
    public BigDecimal getEstMatValue01() {
        return estMatValue01;
    }
    public void setEstMatValue01(BigDecimal estMatValue01) {
        this.estMatValue01 = estMatValue01;
    }
    public BigDecimal getEstMatValue02() {
        return estMatValue02;
    }
    public void setEstMatValue02(BigDecimal estMatValue02) {
        this.estMatValue02 = estMatValue02;
    }
    public int getEstMatDate01() {
        return estMatDate01;
    }
    public void setEstMatDate01(int estMatDate01) {
        this.estMatDate01 = estMatDate01;
    }
    public int getEstMatDate02() {
        return estMatDate02;
    }
    public void setEstMatDate02(int estMatDate02) {
        this.estMatDate02 = estMatDate02;
    }
    public BigDecimal getEstMatInt01() {
        return estMatInt01;
    }
    public void setEstMatInt01(BigDecimal estMatInt01) {
        this.estMatInt01 = estMatInt01;
    }
    public BigDecimal getEstMatInt02() {
        return estMatInt02;
    }
    public void setEstMatInt02(BigDecimal estMatInt02) {
        this.estMatInt02 = estMatInt02;
    }
    public String getCampaign() {
        return campaign;
    }
    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }
    public BigDecimal getStatSumins() {
        return statSumins;
    }
    public void setStatSumins(BigDecimal statSumins) {
        this.statSumins = statSumins;
    }
    public int getRtrnyrs() {
        return rtrnyrs;
    }
    public void setRtrnyrs(int rtrnyrs) {
        this.rtrnyrs = rtrnyrs;
    }
    public int getPremCessAgeMth() {
        return premCessAgeMth;
    }
    public void setPremCessAgeMth(int premCessAgeMth) {
        this.premCessAgeMth = premCessAgeMth;
    }
    public int getPremCessAgeDay() {
        return premCessAgeDay;
    }
    public void setPremCessAgeDay(int premCessAgeDay) {
        this.premCessAgeDay = premCessAgeDay;
    }
    public int getPremCessTermMth() {
        return premCessTermMth;
    }
    public void setPremCessTermMth(int premCessTermMth) {
        this.premCessTermMth = premCessTermMth;
    }
    public int getPremCessTermDay() {
        return premCessTermDay;
    }
    public void setPremCessTermDay(int premCessTermDay) {
        this.premCessTermDay = premCessTermDay;
    }
    public int getRiskCessAgeMth() {
        return riskCessAgeMth;
    }
    public void setRiskCessAgeMth(int riskCessAgeMth) {
        this.riskCessAgeMth = riskCessAgeMth;
    }
    public int getRiskCessAgeDay() {
        return riskCessAgeDay;
    }
    public void setRiskCessAgeDay(int riskCessAgeDay) {
        this.riskCessAgeDay = riskCessAgeDay;
    }
    public int getRiskCessTermMth() {
        return riskCessTermMth;
    }
    public void setRiskCessTermMth(int riskCessTermMth) {
        this.riskCessTermMth = riskCessTermMth;
    }
    public int getRiskCessTermDay() {
        return riskCessTermDay;
    }
    public void setRiskCessTermDay(int riskCessTermDay) {
        this.riskCessTermDay = riskCessTermDay;
    }
    public String getJlLsInd() {
        return jlLsInd;
    }
    public void setJlLsInd(String jlLsInd) {
        this.jlLsInd = jlLsInd;
    }
    public int getUnitStatementDate() {
        return unitStatementDate;
    }
    public void setUnitStatementDate(int unitStatementDate) {
        this.unitStatementDate = unitStatementDate;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
    
    public int getConvertInitialUnits() {
        return convertInitialUnits;
    }
    public void setConvertInitialUnits(int convertInitialUnits) {
        this.convertInitialUnits = convertInitialUnits;
    }
    
    public int getUser() {
        return user;
    }
    public void setUser(int user) {
        this.user = user;
    }
    
}

    
    