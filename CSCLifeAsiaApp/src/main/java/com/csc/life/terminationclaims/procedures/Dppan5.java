/*
 * File: Dppan5.java
 * Date: 29 August 2009 22:46:49
 * Author: Quipoz Limited
 * 
 * Class transformed from DPPAN5.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  DPPAN5 - Death Claim Processing Subroutine No. 5
*  -------------------------------------------------
*
*  This program is an item entry on T6598,  the death claim
*  subroutine method table.
*
*  The  following  linkage  information  is passed  to this
*  subroutine:-
*
*             - DTHCPY
*
*  The  process is  for  Deferred  Annuity  components that
*  have NOT vested.
*
*  This   subroutine  creates   the  accounting   movements
*  required  for  the  death  claim based on the entries on
*  T5645 for the subroutine name - DPPAN5.
*
*  The Following T5645 table entries are as follows:-
*
*  Line #      Description
*
*   1          Component Level Credit to pending death
*              account.
*
*   2          Component  Level Debit to death claims
*              funding account.
*
*  Component Level will always apply as to get the premiums
*  paid balance, component level was needed.
*
*  NB. The  Plan  Suffix  is needed in this program to post
*  the correct  ACMV RLDGACCT field with the full component
*  key, ie. with Plan Suffix. To do this the online program
*  DCCAN5 moves the Plan Suffix in to CDTH-ELEMENT. This in
*  turn  is  moved to S5256-VFUND and ultimately ends up in
*  CLMD-VIRTUAL-FUND.  The  AT module moves this CLMD value
*  to DTHP-ELEMENT before calling  this program. Basically,
*  DTHP-ELEMENT = Plan Suffix in this case!
*
*  This  processing  is similar to that performed by DCC001.
*
*****************************************************************
* </pre>
*/
public class Dppan5 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "DPPAN5";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private String t5645 = "T5645";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Dthcpy dthcpy = new Dthcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit290, 
		exit602
	}

	public Dppan5() {
		super();
	}

public void mainline(Object... parmArray)
	{
		dthcpy.deathRec = convertAndSetParam(dthcpy.deathRec, parmArray, 0);
		try {
			startSubr000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr000()
	{
		/*PARA*/
		dthcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(dthcpy.batckey);
		readTabT5645100();
		wsaaJrnseq.set(ZERO);
		if (isEQ(dthcpy.fieldType,"S")) {
			componentPosting200();
		}
		/*EXIT*/
		exitProgram();
	}

protected void readTabT5645100()
	{
		read110();
	}

protected void read110()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(dthcpy.chdrChdrcoy);
		descIO.setLanguage(dthcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void componentPosting200()
	{
		try {
			para201();
		}
		catch (GOTOException e){
		}
	}

protected void para201()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.substituteCode[6].set(dthcpy.crtable);
		wsaaRldgChdrnum.set(dthcpy.chdrChdrnum);
		wsaaRldgLife.set(dthcpy.lifeLife);
		wsaaRldgCoverage.set(dthcpy.covrCoverage);
		wsaaRldgRider.set(dthcpy.covrRider);
		wsaaRldgPlanSuffix.set(dthcpy.element);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(dthcpy.chdrChdrnum);
		lifacmvrec.tranno.set(dthcpy.tranno);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(dthcpy.effdate);
		lifacmvrec.origcurr.set(dthcpy.currcode);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.termid.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		lifacmvrec.contot.set(t5645rec.cnttot[1]);
		lifacmvrec.rldgcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(dthcpy.chdrChdrnum);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(dthcpy.cnttype);
		lifacmvrec.origamt.set(dthcpy.actualVal);
		lifacmvrec.termid.set(dthcpy.termid);
		lifacmvrec.user.set(dthcpy.user);
		lifacmvrec.transactionTime.set(dthcpy.time);
		lifacmvrec.transactionDate.set(dthcpy.date_var);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		if (isNE(lifacmvrec.origamt,0)) {
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				fatalError600();
			}
		}
		else {
			goTo(GotoLabel.exit290);
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[2]);
		lifacmvrec.glcode.set(t5645rec.glmap[2]);
		lifacmvrec.glsign.set(t5645rec.sign[2]);
		lifacmvrec.contot.set(t5645rec.cnttot[2]);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error600();
				}
				case exit602: {
					exit602();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit602);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		dthcpy.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
