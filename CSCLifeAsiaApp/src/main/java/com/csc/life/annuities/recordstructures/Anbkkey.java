package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:14
 * Description:
 * Copybook name: ANBKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Anbkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData anbkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData anbkKey = new FixedLengthStringData(64).isAPartOf(anbkFileKey, 0, REDEFINE);
  	public FixedLengthStringData anbkChdrcoy = new FixedLengthStringData(1).isAPartOf(anbkKey, 0);
  	public FixedLengthStringData anbkChdrnum = new FixedLengthStringData(8).isAPartOf(anbkKey, 1);
  	public FixedLengthStringData anbkLife = new FixedLengthStringData(2).isAPartOf(anbkKey, 9);
  	public FixedLengthStringData anbkCoverage = new FixedLengthStringData(2).isAPartOf(anbkKey, 11);
  	public FixedLengthStringData anbkRider = new FixedLengthStringData(2).isAPartOf(anbkKey, 13);
  	public PackedDecimalData anbkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(anbkKey, 15);
  	public PackedDecimalData anbkTranno = new PackedDecimalData(5, 0).isAPartOf(anbkKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(anbkKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(anbkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		anbkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}