package com.csc.life.annuities.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.annuities.screens.Sjl09ScreenVars;
import com.csc.life.annuities.tablestructures.Tjl09rec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl09 extends ScreenProgCS{

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl09");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl09.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO",ItempfDAO.class);
	private Itemkey wsaaItemkey = new Itemkey();
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	
	private Tjl09rec tjl09rec = new Tjl09rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl09ScreenVars sv = ScreenProgram.getScreenVars(Sjl09ScreenVars.class);
	Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	
	public Pjl09() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl09", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline {}", e);// Expected exception for control flow purposes
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline {}", e);// Expected exception for control flow purposes
		}
	}

	@Override
	protected void initialise1000() {

		initialise1010();
    }

	protected void initialise1010()	{
		
			
		sv.dataArea.set(SPACES);
		String wsaaItemlang;
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaItemcoy = wsaaItemkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItemkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItemkey.itemItemtabl.toString();
		wsaaItemlang = wsspcomn.language.toString();
		sv.company.set(wsaaItemcoy);
		sv.tabl.set(wsaaItemtabl);
		sv.item.set(wsaaItemitem);
		Descpf descpf =descDAO.getdescData("IT", wsaaItemtabl, wsaaItemitem, wsaaItemcoy, wsaaItemlang);
		if (descpf==null) {
			return;
		}
		sv.longdesc.set(descpf.getLongdesc());
	
		List<Itempf> itempf = itemDAO.getItempf( "IT", wsaaItemcoy, wsaaItemtabl, wsaaItemitem);
		
		if (itempf.isEmpty()) {
			return;
		} else {
			tjl09rec.tjl09Rec.set(StringUtil.rawToString(itempf.get(0).getGenarea()));
		}

			
		sv.wthldngflag.set(tjl09rec.wthldngflag);
			
		
		
		
	}
	
	
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(Varcom.prot);
		}
	}

	@Override
	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		
	}
	
@Override
	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			sv.wthldngflagOut[Varcom.pr.toInt()].set("Y");//IJL-568
			return;
		}
		if (isNE(sv.wthldngflag, tjl09rec.wthldngflag)) {
			tjl09rec.wthldngflag.set(sv.wthldngflag);
			Itempf itempf1 = new Itempf();
			itempf1.setGenarea(tjl09rec.tjl09Rec.toString().getBytes());
			itempf1.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
					.getGENAREAJString(tjl09rec.tjl09Rec.toString().getBytes(), tjl09rec));
			itempf1.setItempfx(smtpfxcpy.item.toString());
			itempf1.setItemtabl(wsaaItemtabl);
			itempf1.setItemcoy(wsaaItemcoy);
			itempf1.setItemitem(wsaaItemitem);
			itemDAO.updateByKey(itempf1, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
		}
	}
	
	@Override
	protected void whereNext4000() {
		wsspcomn.nextprog.set(wsaaProg); 
		wsspcomn.programPtr.add(1);
	}
}

