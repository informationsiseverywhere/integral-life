package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6687
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6687ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(132);
	public FixedLengthStringData dataFields = new FixedLengthStringData(52).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData taxrelsub = DD.taxrelsub.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 52);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData taxrelsubErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 72);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] taxrelsubOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6687screenWritten = new LongData(0);
	public LongData S6687protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6687ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, taxrelsub};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, taxrelsubOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, taxrelsubErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6687screen.class;
		protectRecord = S6687protect.class;
	}

}
