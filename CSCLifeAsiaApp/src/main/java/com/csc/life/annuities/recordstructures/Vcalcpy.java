package com.csc.life.annuities.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:03
 * Description:
 * Copybook name: VCALCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vcalcpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(65);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rec, 0);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rec, 1);
  	public ZonedDecimalData age = new ZonedDecimalData(3, 0).isAPartOf(rec, 5).setUnsigned();
  	public FixedLengthStringData sex = new FixedLengthStringData(1).isAPartOf(rec, 8);
  	public ZonedDecimalData agej = new ZonedDecimalData(3, 0).isAPartOf(rec, 9).setUnsigned();
  	public FixedLengthStringData sexj = new FixedLengthStringData(1).isAPartOf(rec, 12);
  	public FixedLengthStringData oldFreqann = new FixedLengthStringData(2).isAPartOf(rec, 13);
  	public FixedLengthStringData oldAdvance = new FixedLengthStringData(1).isAPartOf(rec, 15);
  	public FixedLengthStringData oldArrears = new FixedLengthStringData(1).isAPartOf(rec, 16);
  	public ZonedDecimalData oldGuarperd = new ZonedDecimalData(3, 0).isAPartOf(rec, 17);
  	public FixedLengthStringData newFreqann = new FixedLengthStringData(2).isAPartOf(rec, 20);
  	public FixedLengthStringData newAdvance = new FixedLengthStringData(1).isAPartOf(rec, 22);
  	public FixedLengthStringData newArrears = new FixedLengthStringData(1).isAPartOf(rec, 23);
  	public ZonedDecimalData newGuarperd = new ZonedDecimalData(3, 0).isAPartOf(rec, 24);
  	public ZonedDecimalData oldAmount = new ZonedDecimalData(17, 2).isAPartOf(rec, 27);
  	public ZonedDecimalData newAmount = new ZonedDecimalData(17, 2).isAPartOf(rec, 44);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rec, 61);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}