package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:20
 * Description:
 * Copybook name: T6699REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6699rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6699Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData sacstypes = new FixedLengthStringData(20).isAPartOf(t6699Rec, 0);
  	public FixedLengthStringData[] sacstype = FLSArrayPartOfStructure(10, 2, sacstypes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(sacstypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData sacstype01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData sacstype02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData sacstype03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData sacstype04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData sacstype05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData sacstype06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData sacstype07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData sacstype08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData sacstype09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData sacstype10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(480).isAPartOf(t6699Rec, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6699Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6699Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}