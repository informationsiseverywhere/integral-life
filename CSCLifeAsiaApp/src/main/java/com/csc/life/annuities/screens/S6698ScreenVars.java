package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6698
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6698ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(652);
	public FixedLengthStringData dataFields = new FixedLengthStringData(172).isAPartOf(dataArea, 0);
	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(30).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(10, 3, 0, ageIssageTos, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
	public ZonedDecimalData ageIssageTo01 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData ageIssageTo02 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData ageIssageTo03 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData ageIssageTo04 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData ageIssageTo05 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData ageIssageTo06 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData ageIssageTo07 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData ageIssageTo08 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData ageIssageTo09 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData ageIssageTo10 = DD.aageto.copyToZonedDecimal().isAPartOf(filler,27);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData dssnum = DD.dssnum.copy().isAPartOf(dataFields,31);
	public ZonedDecimalData earningCap = DD.earncap.copyToZonedDecimal().isAPartOf(dataFields,39);
	public FixedLengthStringData inrevnum = DD.inrevnum.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,58);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,66);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,74);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData pclimits = new FixedLengthStringData(50).isAPartOf(dataFields, 112);
	public ZonedDecimalData[] pclimit = ZDArrayPartOfStructure(10, 5, 2, pclimits, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(50).isAPartOf(pclimits, 0, FILLER_REDEFINE);
	public ZonedDecimalData pclimit01 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData pclimit02 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,5);
	public ZonedDecimalData pclimit03 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData pclimit04 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData pclimit05 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,20);
	public ZonedDecimalData pclimit06 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,25);
	public ZonedDecimalData pclimit07 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData pclimit08 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,35);
	public ZonedDecimalData pclimit09 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,40);
	public ZonedDecimalData pclimit10 = DD.pclimit.copyToZonedDecimal().isAPartOf(filler1,45);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,162);
	public ZonedDecimalData taxrelpc = DD.taxrelpc.copyToZonedDecimal().isAPartOf(dataFields,167);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 172);
	public FixedLengthStringData aagetosErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] aagetoErr = FLSArrayPartOfStructure(10, 4, aagetosErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(aagetosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData aageto01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData aageto02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData aageto03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData aageto04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData aageto05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData aageto06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData aageto07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData aageto08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData aageto09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData aageto10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData dssnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData earncapErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData inrevnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pclimitsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] pclimitErr = FLSArrayPartOfStructure(10, 4, pclimitsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(pclimitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pclimit01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData pclimit02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData pclimit03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData pclimit04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData pclimit05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData pclimit06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData pclimit07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData pclimit08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData pclimit09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData pclimit10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData taxrelpcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 292);
	public FixedLengthStringData aagetosOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] aagetoOut = FLSArrayPartOfStructure(10, 12, aagetosOut, 0);
	public FixedLengthStringData[][] aagetoO = FLSDArrayPartOfArrayStructure(12, 1, aagetoOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(aagetosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] aageto01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] aageto02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] aageto03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] aageto04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] aageto05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] aageto06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] aageto07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] aageto08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] aageto09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] aageto10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] dssnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] earncapOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] inrevnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData pclimitsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] pclimitOut = FLSArrayPartOfStructure(10, 12, pclimitsOut, 0);
	public FixedLengthStringData[][] pclimitO = FLSDArrayPartOfArrayStructure(12, 1, pclimitOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(pclimitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pclimit01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] pclimit02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] pclimit03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] pclimit04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] pclimit05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] pclimit06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] pclimit07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] pclimit08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] pclimit09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] pclimit10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] taxrelpcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6698screenWritten = new LongData(0);
	public LongData S6698protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6698ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itmfrmOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itmtoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto01Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit01Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit02Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto03Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit03Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto04Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit04Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto05Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit05Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto06Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit06Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto07Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit07Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto08Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit08Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto09Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit09Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aageto10Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pclimit10Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, ageIssageTo01, pclimit01, ageIssageTo02, pclimit02, ageIssageTo03, pclimit03, ageIssageTo04, pclimit04, ageIssageTo05, pclimit05, ageIssageTo06, pclimit06, ageIssageTo07, pclimit07, ageIssageTo08, pclimit08, ageIssageTo09, pclimit09, ageIssageTo10, pclimit10, taxrelpc, earningCap, inrevnum, dssnum};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, aageto01Out, pclimit01Out, aageto02Out, pclimit02Out, aageto03Out, pclimit03Out, aageto04Out, pclimit04Out, aageto05Out, pclimit05Out, aageto06Out, pclimit06Out, aageto07Out, pclimit07Out, aageto08Out, pclimit08Out, aageto09Out, pclimit09Out, aageto10Out, pclimit10Out, taxrelpcOut, earncapOut, inrevnumOut, dssnumOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, aageto01Err, pclimit01Err, aageto02Err, pclimit02Err, aageto03Err, pclimit03Err, aageto04Err, pclimit04Err, aageto05Err, pclimit05Err, aageto06Err, pclimit06Err, aageto07Err, pclimit07Err, aageto08Err, pclimit08Err, aageto09Err, pclimit09Err, aageto10Err, pclimit10Err, taxrelpcErr, earncapErr, inrevnumErr, dssnumErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6698screen.class;
		protectRecord = S6698protect.class;
	}

}
