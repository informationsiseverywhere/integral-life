/*
 * File: T6623pt.java
 * Date: 30 August 2009 2:27:27
 * Author: Quipoz Limited
 * 
 * Class transformed from T6623PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.annuities.tablestructures.T6623rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6623.
*
*
*****************************************************************
* </pre>
*/
public class T6623pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Dialogue Decision                        S6623");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company :");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table :");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item :");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(70);
	private FixedLengthStringData filler7 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine003, 6, FILLER).init("Freq     In Advance    In Arrears     Guaranteed     Commutation");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(65);
	private FixedLengthStringData filler9 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine004, 44, FILLER).init("Period         Factor");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(68);
	private FixedLengthStringData filler11 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 6);
	private FixedLengthStringData filler12 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 15);
	private FixedLengthStringData filler13 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 29);
	private FixedLengthStringData filler14 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 44).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine005, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(68);
	private FixedLengthStringData filler16 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 6);
	private FixedLengthStringData filler17 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 15);
	private FixedLengthStringData filler18 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 29);
	private FixedLengthStringData filler19 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 44).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine006, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(68);
	private FixedLengthStringData filler21 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 6);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 15);
	private FixedLengthStringData filler23 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 29);
	private FixedLengthStringData filler24 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 44).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine007, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(68);
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 6);
	private FixedLengthStringData filler27 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 15);
	private FixedLengthStringData filler28 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 29);
	private FixedLengthStringData filler29 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 44).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(68);
	private FixedLengthStringData filler31 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 6);
	private FixedLengthStringData filler32 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 15);
	private FixedLengthStringData filler33 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 29);
	private FixedLengthStringData filler34 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 44).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(68);
	private FixedLengthStringData filler36 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 6);
	private FixedLengthStringData filler37 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 15);
	private FixedLengthStringData filler38 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 29);
	private FixedLengthStringData filler39 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 44).setPattern("ZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(68);
	private FixedLengthStringData filler41 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 6);
	private FixedLengthStringData filler42 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 15);
	private FixedLengthStringData filler43 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 29);
	private FixedLengthStringData filler44 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 44).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine011, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(68);
	private FixedLengthStringData filler46 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 6);
	private FixedLengthStringData filler47 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 15);
	private FixedLengthStringData filler48 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 29);
	private FixedLengthStringData filler49 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 44).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(68);
	private FixedLengthStringData filler51 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 6);
	private FixedLengthStringData filler52 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 15);
	private FixedLengthStringData filler53 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 29);
	private FixedLengthStringData filler54 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 44).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine013, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(68);
	private FixedLengthStringData filler56 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 6);
	private FixedLengthStringData filler57 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 15);
	private FixedLengthStringData filler58 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 29);
	private FixedLengthStringData filler59 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 44).setPattern("ZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(8, 3).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(67);
	private FixedLengthStringData filler61 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler62 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine015, 50, FILLER).init("Page Up/Page Down");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(17);
	private FixedLengthStringData filler63 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" F1=Help  F3=Exit");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6623rec t6623rec = new T6623rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6623pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6623rec.t6623Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t6623rec.freqann01);
		fieldNo010.set(t6623rec.freqann02);
		fieldNo015.set(t6623rec.freqann03);
		fieldNo020.set(t6623rec.freqann04);
		fieldNo025.set(t6623rec.freqann05);
		fieldNo030.set(t6623rec.freqann06);
		fieldNo035.set(t6623rec.freqann07);
		fieldNo040.set(t6623rec.freqann08);
		fieldNo045.set(t6623rec.freqann09);
		fieldNo050.set(t6623rec.freqann10);
		fieldNo006.set(t6623rec.advance01);
		fieldNo011.set(t6623rec.advance02);
		fieldNo016.set(t6623rec.advance03);
		fieldNo021.set(t6623rec.advance04);
		fieldNo026.set(t6623rec.advance05);
		fieldNo031.set(t6623rec.advance06);
		fieldNo036.set(t6623rec.advance07);
		fieldNo041.set(t6623rec.advance08);
		fieldNo046.set(t6623rec.advance09);
		fieldNo051.set(t6623rec.advance10);
		fieldNo007.set(t6623rec.arrears01);
		fieldNo012.set(t6623rec.arrears02);
		fieldNo017.set(t6623rec.arrears03);
		fieldNo022.set(t6623rec.arrears04);
		fieldNo027.set(t6623rec.arrears05);
		fieldNo032.set(t6623rec.arrears06);
		fieldNo037.set(t6623rec.arrears07);
		fieldNo042.set(t6623rec.arrears08);
		fieldNo047.set(t6623rec.arrears09);
		fieldNo052.set(t6623rec.arrears10);
		fieldNo008.set(t6623rec.guarperd01);
		fieldNo013.set(t6623rec.guarperd02);
		fieldNo018.set(t6623rec.guarperd03);
		fieldNo023.set(t6623rec.guarperd04);
		fieldNo028.set(t6623rec.guarperd05);
		fieldNo033.set(t6623rec.guarperd06);
		fieldNo038.set(t6623rec.guarperd07);
		fieldNo043.set(t6623rec.guarperd08);
		fieldNo048.set(t6623rec.guarperd09);
		fieldNo053.set(t6623rec.guarperd10);
		fieldNo009.set(t6623rec.comtfact01);
		fieldNo014.set(t6623rec.comtfact02);
		fieldNo019.set(t6623rec.comtfact03);
		fieldNo024.set(t6623rec.comtfact04);
		fieldNo029.set(t6623rec.comtfact05);
		fieldNo034.set(t6623rec.comtfact06);
		fieldNo039.set(t6623rec.comtfact07);
		fieldNo044.set(t6623rec.comtfact08);
		fieldNo049.set(t6623rec.comtfact09);
		fieldNo054.set(t6623rec.comtfact10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
