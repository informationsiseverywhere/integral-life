package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnypaypfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.life.annuities.screens.Sjl39ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl39 extends ScreenProgCS {	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl39.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL39");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	/* ERRORS */
	private static final String F917 = "F917";	
	private static final String G957 = "G957";	
	private static final String H093 = "H093";
	private static final String E944 = "E944";
	private static final String SJL39 = "Sjl39";	
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl39ScreenVars sv = ScreenProgram.getScreenVars(Sjl39ScreenVars.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Chdrpf chdrpf = new Chdrpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected Clntpf clntpf;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	protected ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private AnnyregpfDAO annyregpfDAO = getApplicationContext().getBean("annyregpfDAO" , AnnyregpfDAO.class);
	private AnnypaypfDAO annypaypfDAO = getApplicationContext().getBean("annypaypfDAO" , AnnypaypfDAO.class);
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);	
	private FixedLengthStringData wsaaRecordFoundFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaNoRecordFound = new Validator(wsaaRecordFoundFlag, "N");
	private Validator wsaaRecordFound = new Validator(wsaaRecordFoundFlag, "Y");
	private static final String T5688 = "T5688";	
	private static final String TJL12 = "TJL12";
	private static final String T3588 = "T3588";
	private static final String T3623 = "T3623";
	private static final String TJL48 = "TJL48";	
	private static final String PP = "PP"; // "Payment Pending", "Pending"
	private static final String PT = "PT"; //"Payment Terminated", "Terminate"
	private static final String ONE_TIME_PAYMENT = "01";
	private List<Clntpf> clntpfList = null;	
	private Batckey wsaaBatckey = new Batckey();	
	private String wsaaItemlang;
	private Annyregpf annyregpf = new Annyregpf();		
	private String wsaaAnnPmntSts;	
	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);	
	private PackedDecimalData wsaaSubfileCount = new PackedDecimalData(2, 0);
	Integer index;
	int pageSize;
	int startInde;
	int endIndex;
	private List<Annypaypf> annypaypfList = null;	
	
	private FixedLengthStringData wsaaTasj = new FixedLengthStringData(4).init("TASJ");
	private FixedLengthStringData wsaaTasl = new FixedLengthStringData(4).init("TASL");
	private FixedLengthStringData wsaaTask = new FixedLengthStringData(4).init("TASK");
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private Gensswrec gensswrec = new Gensswrec();
	
	Annypaypf annypaypf ;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		EXIT2090,
		EXIT2190,
		EXIT2219,		
		UPDATEEROORINDICATORS2670,
		BYPASSSTART4010,
		NEXTPROGRAM4080, 
		NEXTPROGRAM4085, 
		EXIT4090
    }

	public Pjl39() {
		super();
		screenVars = sv;
		new ScreenModel(SJL39, AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}

	@Override
	protected void initialise1000() {
		initialise1010();				
	}
	
	
	private void initialise1010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		sv.action1.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);			
		wsaaItemlang = wsspcomn.language.toString();
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(Varcom.sclr);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask) 
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
		{		
			annyregpf = annyregpfDAO.getAnnyregpf(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
		}
		else
		{
			 annyregpf = annyregpfDAO.getAnnyregpfbyEffdt(wsspcomn.company.toString(), 
					 wsspcomn.chdrChdrnum.toString().trim(), wsspcomn.effdate.toInt(), wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
		}
		
		Optional<Annyregpf> isExists = Optional.ofNullable(annyregpf);
		if (isExists.isPresent()) {
			if (null != annyregpf.getChdrnum())
				sv.chdrnum.set(annyregpf.getChdrnum());
			retrvContract1020();
			if (null != annyregpf.getAnnpstcde()){			
				wsaaAnnPmntSts = annyregpf.getAnnpstcde();
			}
			loadHeader1030();			
			if (null != annyregpf.getTotannufund())
				sv.totannufund.set(annyregpf.getTotannufund());
			if (null != annyregpf.getAnnpmntopt())
				sv.annpmntopt.set(annyregpf.getAnnpmntopt());
			if (null != annyregpf.getAnnupmnttrm())
				sv.annupmnttrm.set(annyregpf.getAnnupmnttrm());
			if (null != annyregpf.getPartpmntamt())
				sv.partpmntamt.set(annyregpf.getPartpmntamt());
			if (null != annyregpf.getAnnpmntfreq())
				sv.annpmntfreq.set(annyregpf.getAnnpmntfreq());
			if (null != annyregpf.getDfrrdtrm())
				sv.dfrrdtrm.set(annyregpf.getDfrrdtrm());
			if (null != annyregpf.getDfrrdamt())
				sv.dfrrdamt.set(annyregpf.getDfrrdamt());
			if (null != annyregpf.getAnnupmntamt())
				sv.annupmntamt.set(annyregpf.getAnnupmntamt());						
			if (null != annyregpf.getPayee()){
				sv.payee.set(annyregpf.getPayee());
				getPayeeName();
			}	
			
			if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)
					||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask) 
					||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
			{		
				annypaypfList = annypaypfDAO.getAnnypaypfRecords(wsspcomn.company.toString(), 
						wsspcomn.chdrChdrnum.toString().trim());
				
			}
			else
			{
				annypaypfList = annypaypfDAO.getAnnypaypfbyEffdt(wsspcomn.company.toString(), 
						wsspcomn.chdrChdrnum.toString().trim(), wsspcomn.effdate.toInt(), wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
			}
			
			index =0;
			pageSize = annypaypfList.size();
			
			processPmntplan1100();
		}else {
			sv.chdrnumErr.set(F917);
			return;
		}
	}
	
	protected void processPmntplan1100()
	{
		para1100();
		loadSubfilePage1100();
	}
	
	protected void para1100()
	{
		sv.subfileFields.set(SPACES);
		sv.selectOut[Varcom.pr.toInt()].set(SPACES);
		sv.selectOut[Varcom.nd.toInt()].set(SPACES);		
		if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask)
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl)){
			return ;
		}
	}

	
	protected void loadSubfilePage1100()
	{
		//LOAD-SUBFILE
		wsaaSubfileCount.set(ZERO);	
		while ( !(annypaypfList.isEmpty()							//ZJNPG-2992
		|| isEQ(wsaaSubfileCount, pageSize)) && index <= annypaypfList.size()-1) {

			annypaypf = annypaypfList.get(index);
			loadSubfileRecords1200();
			index++;
			
		}
		
	/*	CHECK-FOR-END-OF-FILE
		 If its end of file set the 'MORE RECORDS' indicator for the
		 subfile to 'N'.*/
		if (annypaypfList.isEmpty()) {			
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	/*	CHECK-FOR-RECORDS-ADDED
		 If there were no records added to the subfile output a message.*/
		if (isEQ(wsaaSubfileCount, ZERO)) {
			sv.selectErr.set(G957);
		
		}
		//EXIT
		//**     LOADS RECORDS INTO THE SUBFILE UNTIL THERE ARE NO MORE*//*
		//**     DATABASE RECORDS OR THE SUBFILE PAGE IS FULL*//*
	}
	
	protected void loadSubfileRecords1200()
	{
		loadRecords1210();
	}
	
	protected void loadRecords1210()
	{
		/*CHECK-FOR-FILTER*/
		/* If the kanji corporate name is spaces in working storage then*/
		/* there is no scan to worry about, just write the record to the*/
		/* subfile and set the match indicator.*/
		writeRecToSubfile5200();		
	}

protected void writeRecToSubfile5200()
	{
		/*WRITE-SUBFILE-RECORD*/
		/* Increment the subfile count.*/
		wsaaSubfileCount.add(1);
		/*LOAD-SUBFILE-FIELDS*/
		/*  Split the bank and Branch description field from the database*/
		/*  record into two separate fields. Load the screen fields of the*/
		/*  subfile record.*/
		sv.pmntno.set(annypaypf.getPmntno());
		sv.pmntdate.set(annypaypf.getPmntdt());
		if (null != annypaypf.getAnnaamt())
			sv.annuityamt.set(annypaypf.getAnnaamt());
		if (null != annypaypf.getInttax())
			sv.intrate.set(annypaypf.getInttax());
		if (null != annypaypf.getAnnintamt())
			sv.annuitywint.set(annypaypf.getAnnintamt());
		else 
			sv.annuitywint.set(ZERO);
		if (null != annypaypf.getWitholdtax())
			sv.wholdtax.set(annypaypf.getWitholdtax());
		else
			sv.wholdtax.set(ZERO);
		if (null != annypaypf.getPaidamt())
			sv.paidamt.set(annypaypf.getPaidamt());	
		else
			sv.paidamt.set(ZERO);
		if (null != annypaypf.getPmntstat()){
			String pmntStatus = iafDescDAO.getItemLongDesc("IT", wsspcomn.company.toString(),
					TJL48, annypaypf.getPmntstat().trim(), wsaaItemlang);
			sv.pmntstatus.set(pmntStatus);	
		}else{
			sv.pmntstatus.set(SPACES);
		}
		//Write the record to the subfile.
		scrnparams.function.set(Varcom.sadd);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		//EXIT
	}	
	
	
	private void getPayeeName(){
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.payee.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || clntpfList.isEmpty())
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		sv.payeename.set(wsspcomn.longconfname);
	}
	protected void retrvContract1020()
	{	
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(Varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),Varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}	
	}
	
	protected void loadHeader1030()
	{
		try {
			headings1031();					
		}
		catch (GOTOException e){
			LOGGER.info("loadHeader1030:", e);
		}
	}	
	
	protected void headings1031()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());		
		sv.cownnum.set(chdrpf.getCownnum());
		readClientRecord(chdrpf.getCownnum());
		if(clntpf != null){
			sv.ownername.set(clntpf.getSurname().trim().concat(", ").concat(clntpf.getLgivname()));
		}
		readLifeRecord();					
		loadLongDesc();
	}	
	
	protected void readClientRecord(String clntnum){
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), clntnum.trim());		
	}
	protected void readLifeRecord(){
		Lifepf lifepfModel = null;
		List<Lifepf> lifeList = lifepfDAO.getLifeList(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(lifeList == null){
			return;
		}else{
			lifepfModel=lifeList.get(0);
		}
		sv.lifenum.set(lifepfModel.getLifcnum());
		clntpf.setClntnum(lifepfModel.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpfList= clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || clntpfList.isEmpty())
		{
			return;
		}
		for(Clntpf clnt :clntpfList)
		{
			plainname(clnt);
		}
		sv.lifename.set(wsspcomn.longconfname);		
	}
	
	protected void loadLongDesc(){
		Map<String, String> itemMap = new HashMap<>();		
		itemMap.put(TJL12, wsaaAnnPmntSts);
		itemMap.put(T5688, chdrpf.getCnttype());
		itemMap.put(T3623, chdrpf.getStatcode());
		itemMap.put(T3588, chdrpf.getPstcde());
		Map<String, Descpf> descMap = descdao.searchMultiDescpf(wsspcomn.company.toString(),
				wsspcomn.language.toString(), itemMap);
		if (!descMap.containsKey(T5688)) {			
			sv.ctypedes.set(SPACES);
		} else {
			sv.ctypedes.set(descMap.get(T5688).getLongdesc());
		}
		if (!descMap.containsKey(TJL12)) {			
			sv.annpmntstatus.set(SPACES);
		} else {
			sv.annpmntstatus.set(descMap.get(TJL12).getLongdesc());
		}
	}		
		
	protected void plainname(Clntpf clntpf)
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return ;
		}		
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
	
	protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}	
	
	@Override
	protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Output the screen.                                              */
		return ;
		/*PRE-EXIT*/
	}	
	
	@Override
	protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();			
		}
		catch (GOTOException e){
			LOGGER.info("screenEdit2000:", e);
		}
	}
	
	protected void screenIo2010()
	{		                                  
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(Varcom.oK);
		}
		/*ROLL-UP
		 Roll up processing - fills next page of the subfile.*/
		if (isEQ(scrnparams.statuz, Varcom.rolu)) {	
			
			List<Annypaypf> annypaypfTempList;
			
			if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)
					||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask)
					||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
			{		
				annypaypfTempList = annypaypfDAO.getAnnypaypfRecords(wsspcomn.company.toString(), 
						wsspcomn.chdrChdrnum.toString().trim());
				
			}
			else
			{
				annypaypfTempList = annypaypfDAO.getAnnypaypfbyEffdt(wsspcomn.company.toString(), 
						wsspcomn.chdrChdrnum.toString().trim(), wsspcomn.effdate.toInt(), wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
			}
			
			annypaypfList.addAll(annypaypfTempList);
			//ZJNPG-2992 :: Ends
			
            loadSubfilePage1100();
			wsspcomn.edterror.set(SPACES);
			  /*The exit paragraph name will be standardised to 2090 -EXIT.    
			       GO TO 2900-EXIT.*/                                          
			goTo(GotoLabel.EXIT2090);
		}
	}
	
	protected void checkForRecordSelect2030()
	{
		/* Check to see if a record has been 'Selected' from the screen,
		 and if so load the information from that record into the WSSP
		 fields.*/
		wsaaNoRecordFound.setTrue();
		while ( !(wsaaRecordFound.isTrue()
		|| isEQ(scrnparams.statuz, Varcom.endp))) {
			checkForSelection2100();
		}
		
		if (wsaaRecordFound.isTrue()) {
			loadWsspData2200();
			 /* The exit paragraph name will be standardised to 2090 -EXIT.    
			       GO TO 2900-EXIT. */                                         
			goTo(GotoLabel.EXIT2090);
		}		
	}
	
	protected void checkForSelection2100()
	{
		//CHECK
		scrnparams.function.set(Varcom.srnch);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, Varcom.endp)) {
			wsspwindow.value.set(SPACES);
			return ;
		}
		if (isNE(sv.select, SPACES)) {
			wsaaRecordFound.setTrue();
		}
		//EXIT
		//**     LOAD WSSP FIELDS FROM SELECTED DATABASE RECORD*//*
	}
	
	protected void validateSubfile2060()
	{
		scrnparams.function.set(Varcom.srnch);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if(isEQ(sv.action1,"1")) {
			validateSubfile2600();
		}		
	}	
	
	protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case UPDATEEROORINDICATORS2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void validation2610()
	{
		if(isEQ(sv.action1,"1")){
			validateSelect4910(); 
		}
	}
	
	protected void loadWsspData2200()
	{
		check2200();
	}

	protected void check2200()
	{
		 /*When a record has been selected on the screen, the database
		 record is read for the selection to obtain additional fields
		 required for passing back via the WSSP fields.*/		

	}
	
	@Override
	protected void update3000() {
		//**  No updating of database takes place.*//*	
	}	
		
	
	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
	@Override
	protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
				case BYPASSSTART4010: 
					bypassStart4010();
				case NEXTPROGRAM4085: 
					nextProgram4085();
				case EXIT4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	
	protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.EXIT4090);
		}		
		/*    If returning from a program further down the stack then*/
		/*    bypass the start on the subfile.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.BYPASSSTART4010);
		}
		/*    Re-start the subfile.*/
		scrnparams.statuz.set(Varcom.oK);
		scrnparams.function.set(Varcom.sstrt);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void bypassStart4010()
	{
		if (isEQ(sv.select, SPACES)) {
			while ( !(isNE(sv.select, SPACES)
			|| isEQ(scrnparams.statuz, Varcom.endp))) {
				readSubfile4100();
			}			
		}
		
		
		/*  Nothing pressed at all, end working*/
		if ((isEQ(scrnparams.statuz, Varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) && (isNE(sv.action1,"1")) ) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.EXIT4090);
		}
		/* All requests services,*/
		if ((isEQ(scrnparams.statuz, Varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) && (isNE(sv.action1,"1")) ) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			goTo(GotoLabel.EXIT4090);
		}
		if (isEQ(scrnparams.statuz, Varcom.endp)  &&  (isNE(sv.action1,"1"))) {
			goTo(GotoLabel.NEXTPROGRAM4085);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar1 = 0; loopVar1 != 8; loopVar1 += 1){
			saveProgramStack4200();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		//ILJ-286 Starts
		if (isEQ(sv.action1, "1")) {			
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			gensswrec.function.set("A");
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, Varcom.oK)
					&& isNE(gensswrec.statuz, Varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar3 = 0; loopVar3 != 8; loopVar3 += 1){
				loadProgramStack4300();
			}
		}
		if (isEQ(sv.select, "1") && isNE(sv.action1,"1")) {	
			wsspcomn.selno.set(scrnparams.subfileRrn);
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			gensswrec.function.set("B");
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, Varcom.oK)
					&& isNE(gensswrec.statuz, Varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			/* If an entry on T1675 was not found by Genswch, redisplay the    */
			/*   the screen with an error.                                     */
			sv.action1.set(SPACES);
			
			if (isEQ(gensswrec.statuz, Varcom.mrnf)) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
				scrnparams.errorCode.set(H093);
				wsspcomn.nextprog.set(scrnparams.scrname);
				return;
			}
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar3 = 0; loopVar3 != 8; loopVar3 += 1){
				loadProgramStack4300();
			}
			nextProgram4080();
		}		
		
		/* Blank out action to ensure it is not processed again*/
		/* MOVE SPACE                  TO Sjl39-SELECT.                 */
		/*    Read and store the selected PTRNENQ record.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
				
	
	}
	
	protected void nextProgram4085(){
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}
	
	protected void nextProgram4080()
	{
		sv.select.set(SPACES);
		scrnparams.function.set(Varcom.supd);
		processScreen("Sjl39", sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz, Varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(H093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return;
		}
	}
	
	protected void saveProgramStack4200()
	{
		/*PARA*/
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

	protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(Varcom.srdn);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	protected void validateSelect4910(){
		if(isEQ(sv.action1,1)){
			if(!(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)
					||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask))){			
				sv.selectErr.set(E944);	
				sv.action1.set(SPACES);
			}
			if( null!= annyregpf
					&& (ONE_TIME_PAYMENT.equals(annyregpf.getAnnpmntopt()) 
							|| PP.equals(annyregpf.getAnnpstcde())
							|| PT.equals(annyregpf.getAnnpstcde()))) {
				sv.selectErr.set(E944);
				sv.action1.set(SPACES);
				goTo(GotoLabel.UPDATEEROORINDICATORS2670);
			}
		}
	}
//ILJ-286 Ends
	protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(Varcom.supd);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(Varcom.srnch);
		processScreen(SJL39, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
		
}
