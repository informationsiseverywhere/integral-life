package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:19
 * Description:
 * Copybook name: T6698REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6698rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6698Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(30).isAPartOf(t6698Rec, 0);
  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(10, 3, 0, ageIssageTos, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData ageIssageTo09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData ageIssageTo10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public FixedLengthStringData dssnum = new FixedLengthStringData(8).isAPartOf(t6698Rec, 30);
  	public ZonedDecimalData earningCap = new ZonedDecimalData(11, 0).isAPartOf(t6698Rec, 38);
  	public FixedLengthStringData inrevnum = new FixedLengthStringData(8).isAPartOf(t6698Rec, 49);
  	public FixedLengthStringData pclimits = new FixedLengthStringData(50).isAPartOf(t6698Rec, 57);
  	public ZonedDecimalData[] pclimit = ZDArrayPartOfStructure(10, 5, 2, pclimits, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(50).isAPartOf(pclimits, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pclimit01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData pclimit02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData pclimit03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData pclimit04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData pclimit05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData pclimit06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData pclimit07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData pclimit08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData pclimit09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData pclimit10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public ZonedDecimalData taxrelpc = new ZonedDecimalData(5, 2).isAPartOf(t6698Rec, 107);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(388).isAPartOf(t6698Rec, 112, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6698Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6698Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}