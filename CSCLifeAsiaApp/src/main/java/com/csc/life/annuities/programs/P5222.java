/*
 * File: P5222.java
 * Date: 30 August 2009 0:19:15
 * Author: Quipoz Limited
 * 
 * Class transformed from P5222.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.AnbkTableDAM;
import com.csc.life.annuities.dataaccess.RegtbrkTableDAM;
import com.csc.life.annuities.dataaccess.RegtvstTableDAM;
import com.csc.life.annuities.dataaccess.VstdTableDAM;
import com.csc.life.annuities.screens.S5222ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.AnnylnbTableDAM;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.terminationclaims.tablestructures.T6690rec;
import com.csc.life.terminationclaims.tablestructures.T6692rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
* This program is part of the 9405 Annuities Development.  It is
* called via T5671 switching when a user is creating, modifying
* or enquiring upon a Vesting Annuity proposal.  It can also be
* accessed during Vesting Approval in enquiry mode.
*
* This program controls the Vesting Annuity Payment Details
* screen, which is used to modify or enquire upon annuity
* payment details held on the Regular Payments Temporary file,
* REGT.
*
* If in enquiry mode all fields except the window select fields
* will be protected.
*
* Display the REGT details on the screen  looking  up  all  of
* the descriptions from DESC where appropriate.
*
* If  the  bank details on REGT are non-blank set a '+' in the
* Bank Details indicator field.
*
* Validation rules.
* =================
*
* Reason  Code.
*  This is a mandatory field. The item must exist on the Regular
*  Payment Reason Codes table, T6692.  A window facility is  for
*  this field.
*
* Evidence.
*  This field is free format and optional.
*
* Payment Method.
*  This is a mandatory field.  If the entry on  Regular  Payment
*  Methods  table,  T6694,  for  the  method of payment entered,
*  indicates that bank details are required an X will be  forced
*  into  the  Bank  Details selection field to ensure that  bank
*  details will be set up.
*
* Payment Currency.
*  This  is  a mandatory field.  It will  be  held  on the  REGT
*  created  by  P5329.  It can be overidden and must be a  valid
*  item on T3629.
*
* Frequency Code.
*  This  is a mandatory field.  It will be held on the REGT file
*  created  by P5329.  It can  only be overridden if the Regular
*  Claim  Detail  rules  held  on  table  T6696  allow frequency
*  override. If the Frequency  Code has  changed then the  Total
*  Sum  Assured  field  must  also be  re-calculated to bring it
*  into line with the new frequency.
*
* Payee  Client.
*  If the entry on the Regular Payment Methods of Payment table,
*  T6694,  for  the  method of  payment entered indicates that a
*  payee is required this field  is  mandatory,  otherwise it is
*  not required and will error if completed.
*
* Percentage.
*  If the  percentage and amount  fields  are  blank, obtain the
*  the  default  percentage  from  table T6696 and calculate the
*  amount from this.  If the  percentage is changed, recalculate
*  the amount as a  percentage of the sum assured on the screen.
*  If both  the percentage  and amount  fields are  changed, the
*  amount takes precedence and the percentage is recalculated.
*
* Destination  Key.
*  If  the  'Contract  Details  Required'  indicator   on  T6694
*  is 'Y' then a valid contract  number must  be  entered  here.
*  Otherwise the field must be blank.
*
* Payment Amount.
*  This is the amount to be paid for the regular payment record.
*  The total  payment amount for  all Immediate annuity payments
*  payable  during the  same period can not be  greater than the
*  sum assured for the Immediate Annuity component.
*
* Effective date.
*  This  is  the  Effective  Date  of  Annuity  payments  and is
*  shown here for information only.
*
* First Payment Date.
*  This  is obtained from the REGT record and is shown here  for
*  information only.
*
* Review Date.
*  This is obtained from the REGT record and is protected if the
*  coverage has a guaranteed payment period.
*
* Final Payment Date.
*  This is optional  and  if  left  blank will  be  set  to  Max
*  Date. If entered it must not be less than  the First  Payment
*  date.
*
* Anniversary Date.
*  This may be entered by the user  but  if not  then  calculate
*  it from the effective Date and the Indexation Frequency  from
*  T6696. The default is Max Date.
*
* Tables Used.
*      T3629 - Currency Code Details
*      Key: CURRCD
*      T5606 - Regular Benefit Edit Rules
*      Key: Validation Item Key (from T5671) || Currency Code
*      T5671 - Generic Program Switching
*      Key: Transaction Code || CRTABLE
*      T5688 - Contract Definition
*      Key: Contract Type
*      T6692 - Regular Payment Reason Codes
*      Key: Regular Payment Reason Code
*      T6694 - Regular Payment Method of Payment
*      Key: Regular Payment MOP
*      T6696 - Regular Claim Detail Rules
*      Key: CRTABLE || Reason Code
*
*****************************************************************
* </pre>
*/
public class P5222 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5222");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);

	private FixedLengthStringData wsaaTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTran = new FixedLengthStringData(5).isAPartOf(wsaaTranCurrency, 0);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3).isAPartOf(wsaaTranCurrency, 5);
	private FixedLengthStringData wsaaPayclt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFreq2 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private String wsaaTotalUsed = "";
	private FixedLengthStringData wsaaBkfreqann = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaArrears = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaFreqann = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaPayamt1 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPayamt2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrcnt = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaLastPrcnt = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaInterimPrcnt = new PackedDecimalData(12, 9).setUnsigned();
	private PackedDecimalData wsaaPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaLastPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPymt2 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData index2 = new PackedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaT5671Pgm = new FixedLengthStringData(4);
		/* TABLES */
	private static final String t3590 = "T3590";
	private static final String t3629 = "T3629";
	private static final String t5606 = "T5606";
	private static final String t5671 = "T5671";
	private static final String t5688 = "T5688";
	private static final String t6690 = "T6690";
	private static final String t6691 = "T6691";
	private static final String t6692 = "T6692";
	private static final String t6694 = "T6694";
	private static final String t6696 = "T6696";
//	private static final String TD5G2 = "TD5G2";//FWANG3
	//ILIFE-5330
	private static final String t6625 = "T6625";
	private AnbkTableDAM anbkIO = new AnbkTableDAM();
		/*Annuity Details Life New Business*/
	private AnnylnbTableDAM annylnbIO = new AnnylnbTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Regular Payments Temporary Details*/
	private RegtbrkTableDAM regtbrkIO = new RegtbrkTableDAM();
		/*Regular Payment Temporary Record Vesting*/
	private RegtvstTableDAM regtvstIO = new RegtvstTableDAM();
	private VstdTableDAM vstdIO = new VstdTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private T6690rec t6690rec = new T6690rec();
	private T6692rec t6692rec = new T6692rec();
	private T6694rec t6694rec = new T6694rec();
	private T6696rec t6696rec = new T6696rec();
	private Wssplife wssplife = new Wssplife();
	//ILIFE-5330
	private T6625rec t6625rec = new T6625rec();
	
	private S5222ScreenVars sv = ScreenProgram.getScreenVars( S5222ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
//	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class); //fwang3
	private boolean isBankDirect= false;
	
	//ILIFE-8175
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		checkDestination2060, 
		checkForErrors2080, 
		exit2090, 
		exit4090
	}

	public P5222() {
		super();
		screenVars = sv;
		new ScreenModel("S5222", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	****   Initialise fields for showing on screen.
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			create1060();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
		//	ILJ-49 End
		wsaaPayclt.set(SPACES);
		wsaaLastFreq.set(SPACES);
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.revdate.set(varcom.vrcmMaxDate);
		wsaaPayamt1.set(ZERO);
		wsaaPayamt2.set(ZERO);
		wsaaPayrseqno.set(ZERO);
		wsaaLastPymt.set(ZERO);
		wsaaPymt.set(ZERO);
		wsaaPymt2.set(ZERO);
		wsaaLastPrcnt.set(ZERO);
		wsaaInterimPrcnt.set(ZERO);
		wsaaPrcnt.set(ZERO);
		wsaaFreq.set(ZERO);
		wsaaFreq2.set(ZERO);
		sv.pymt.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.vstpay.set(ZERO);
		sv.totalamt.set(ZERO);
		sv.totalamtOut[varcom.nd.toInt()].set("Y");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		//ILIFE-8175
				chdrpf = chdrpfDAO.getCacheObject(chdrpf);
				if(null==chdrpf) {
					chdrmjaIO.setFunction(varcom.retrv);
					SmartFileCode.execute(appVars, chdrmjaIO);
					if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
						syserrrec.params.set(chdrmjaIO.getParams());
						fatalError600();
					}
					else {
						chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
						if(null==chdrpf) {
							fatalError600();
						}
						else {
							chdrpfDAO.setCacheObject(chdrpf);
						}
					}
				}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		vstdIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstdIO.getParams());
			fatalError600();
		}
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill(" ");
		}
		descIO.setDescitem(chdrpf.getCntcurr());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		}
		else {
			sv.currds.fill(" ");
		}
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1))) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cownnum.set(chdrpf.getCownnum());
		sv.occdate.set(chdrpf.getOccdate());
		sv.currcd.set(chdrpf.getCntcurr());
		regtvstIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		//ILIFE-8175
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		if (isGT(covrpf.getPayrseqno(),0)) {
			wsaaPayrseqno.set(covrpf.getPayrseqno());
		}
		else {
			wsaaPayrseqno.set(1);
		}
		/*    If the user has entered a lump sum, the payment calculations*/
		/*    to follow will use the lump sum amount plus the adjustment*/
		/*    for the lump sum.  Otherwise, the calculations to follow*/
		/*    will use the benefit payment sum plus the adjustment sum for*/
		/*    the payment sum.*/
		if (isNE(regtvstIO.getRegpayfreq(),"00")) {
			wsaaPayamt1.set(vstdIO.getVstpay());
		}
		else {
			wsaaPayamt1.set(vstdIO.getVstlump());
		}
		/*    Read Life Details.*/
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrnum(covrpf.getChdrnum());
		lifelnbIO.setChdrcoy(covrpf.getChdrcoy());
		lifelnbIO.setLife(covrpf.getLife());
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
	}

protected void create1060()
	{
		setScreenDetails1300();
		/*    If the user has chosen to vest part of the plan, appropriate*/
		/*    data must be set up for the breakout routine.  Therefore, if*/
		/*    part plan vesting has been chosen - read ANBK, otherwise*/
		/*    (ie., whole plan) - read ANNY.*/
		if (isEQ(covrpf.getPlanSuffix(),0)) {
			annylnbIO.setPlanSuffix(covrpf.getPlanSuffix());
			readAnny1400();
		}
		else {
			if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
				annylnbIO.setPlanSuffix(covrpf.getPlanSuffix());
				readAnny1400();
			}
			else {
				readAnbk1500();
				if (isEQ(anbkIO.getStatuz(),varcom.mrnf)) {
					annylnbIO.setPlanSuffix(ZERO);
					readAnny1400();
				}
			}
		}
		/*    Read PAYR record to obtain contract currreny.*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrpf.getChdrcoy());
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(wsaaPayrseqno);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getStatuz());
			fatalError600();
		}
		/*    Read T5671 for validation item key (used as part of key for*/
		/*    T5606).*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covrpf.getCrtable());
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/*    Find the Edit rules associated with the program.*/
		for (index2.set(1); !(isGT(index2,4)); index2.add(1)){
			wsaaT5671Pgm.set(subString(t5671rec.pgm[index2.toInt()], 2, 4));
			/*        IF T5671-PGM(INDEX2)     = WSSP-LASTPROG                 */
			/*        IF T5671-PGM(INDEX2)     = WSSP-LASTPROG(2:4)            */
			if (isEQ(wsaaT5671Pgm,subString(wsspcomn.lastprog, 2, 4))) {
				wsaaTran.set(t5671rec.edtitm[index2.toInt()]);
				index2.set(5);
			}
		}
		/*    Read T5606 for frequency.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5606);
		wsaaCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsaaTranCurrency);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(t5606,itdmIO.getItemtabl())
		|| isNE(wsaaTranCurrency,itdmIO.getItemitem())) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
		/*    Recalculate the payment amount if the REGTVST frequency and*/
		/*    the frequency on T5606 differ.*/
		if (isNE(regtvstIO.getRegpayfreq(),t5606rec.benfreq)) {
			wsaaFreq.set(t5606rec.benfreq);
			wsaaFreq2.set(regtvstIO.getRegpayfreq());
			if (isEQ(regtvstIO.getRegpayfreq(),"00")) {
				wsaaFreq.set(1);
				wsaaFreq2.set(1);
			}
			compute(wsaaPayamt2, 3).setRounded((div((mult(wsaaPayamt1,wsaaFreq)),wsaaFreq2)));
		}
		else {
			wsaaPayamt2.set(wsaaPayamt1);
		}
		zrdecplrec.amountIn.set(wsaaPayamt2);
		callRounding5000();
		wsaaPayamt2.set(zrdecplrec.amountOut);
		sv.vstpay.set(wsaaPayamt2);
	}

	/**
	* <pre>
	**** Sections performed from the 1000 section above.
	* </pre>
	*/
protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		/*    Look up the contract details of the client owner (CLTS)*/
		/*    and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void setScreenDetails1300()
	{
		read1310();
	}

protected void read1310()
	{
	     //ILIFE-5330 start
		//read t6625
			itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
		}
		t6625rec.t6625Rec.set(itdmIO.getGenarea());
		/*    Read Payment Type Details.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(t6690);
		itemIO.setItempfx("IT");
		if (isEQ(wsspcomn.flag,"M")) {
			itemIO.setItemitem(t6625rec.ncovvest);	
		}
		else {
			itemIO.setItemitem(covrpf.getCrtable());
		}		
		 //ILIFE-5330 end
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6690rec.t6690Rec.set(itemIO.getGenarea());
		descIO.setDescitem(t6690rec.rgpytype);
		descIO.setDesctabl(t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill(" ");
		}
		/*    Read the frequency code description.*/
		descIO.setDescitem(regtvstIO.getRegpayfreq());
		descIO.setDesctabl(t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		}
		else {
			sv.frqdesc.fill(" ");
		}
		/*    Read the claim currency description.*/
		descIO.setDescitem(regtvstIO.getCurrcd());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill(" ");
		}

		/*    Set up the Screen.*/
		/*
		 * fwang3 ICIL-4
		 */
/*		if (isEQ(wsspcomn.flag, "I")) {
			sv.payoutoption.set(regtvstIO.getPayoutopt());
		} else if (isEQ(wsspcomn.flag, "M")) {
			if (isEQ(regtvstIO.getPayoutopt(), SPACES)) {
				initPayoutOption();
			} else {
				sv.payoutoption.set(regtvstIO.getPayoutopt());
			}
		} else if (isEQ(wsspcomn.flag, "C")) {
			initPayoutOption();
		} else {
			sv.payoutoption.set(regtvstIO.getPayoutopt());
		}*/
		sv.rgpynum.set(regtvstIO.getRgpynum());
		sv.regpayfreq.set(regtvstIO.getRegpayfreq());
		wsaaFreq.set(regtvstIO.getRegpayfreq());
		wsaaFreq2.set(regtvstIO.getRegpayfreq());
		wsaaLastFreq.set(regtvstIO.getRegpayfreq());
		sv.claimcur.set(regtvstIO.getCurrcd());
		sv.crtdate.set(regtvstIO.getCrtdate());
		sv.revdate.set(regtvstIO.getRevdte());
		sv.firstPaydate.set(regtvstIO.getFirstPaydate());
		sv.cltype.set(regtvstIO.getPayreason());
		sv.payclt.set(regtvstIO.getPayclt());
		sv.claimevd.set(regtvstIO.getClaimevd());

		sv.prcnt.set(regtvstIO.getPrcnt());
		wsaaLastPrcnt.set(regtvstIO.getPrcnt());
		sv.destkey.set(regtvstIO.getDestkey());
		sv.totalamt.set(regtvstIO.getTotamnt());
		sv.anvdate.set(regtvstIO.getAnvdate());
		sv.pymt.set(regtvstIO.getPymt());
		wsaaLastPymt.set(regtvstIO.getPymt());
		
		isBankDirect = FeaConfg.isFeatureExist(regtvstIO.getChdrcoy().toString().trim(), "SUOTR006", appVars, "IT");//BSD-ICIL-261
		if(regtvstIO.getRgpymop().toString().equals(" ") || regtvstIO.getRgpymop().toString() == null){
			if(isBankDirect){
				sv.rgpymop.set("B");
			}
		}else{
			sv.rgpymop.set(regtvstIO.getRgpymop());
		}
		
		
		/*    If a lump sum has been entered, then the final paydate is*/
		/*    known and can be shown on the screen.*/
		if (isEQ(regtvstIO.getRegpayfreq(),"00")) {
			sv.finalPaydate.set(regtvstIO.getFinalPaydate());
		}
		if (isNE(regtvstIO.getPayclt(),SPACES)) {
			wsaaPayclt.set(regtvstIO.getPayclt());
		}
		/*    Read Payee Details*/
		if (isNE(regtvstIO.getPayclt(),SPACES)) {
			cltsIO.setClntnum(regtvstIO.getPayclt());
			getClientDetails1200();
			if ((isEQ(cltsIO.getStatuz(),varcom.mrnf)
			|| isNE(cltsIO.getValidflag(),1))) {
				sv.payenmeErr.set(errorsInner.e335);
				sv.payenme.set(SPACES);
			}
			else {
				plainname();
				sv.payenme.set(wsspcomn.longconfname);
			}
		}
		/*    Read payment reason description.*/
		if (isNE(regtvstIO.getPayreason(),SPACES)) {
			descIO.setDescitem(regtvstIO.getPayreason());
			descIO.setDesctabl(t6692);
			findDesc1100();
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.clmdesc.set(descIO.getLongdesc());
			}
			else {
				sv.clmdesc.set(SPACES);
			}
		}
		/*    Read payment method description.*/
		if (isNE(regtvstIO.getRgpymop(),SPACES)) {
			descIO.setDescitem(regtvstIO.getRgpymop());
			descIO.setDesctabl(t6694);
			findDesc1100();
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.rgpyshort.set(descIO.getShortdesc());
			}
			else {
				sv.rgpyshort.set(SPACES);
			}
		}
		/*    If the REGT record contains bank details, move a '+' into*/
		/*    the bank indicator field.*/
		if (isNE(regtvstIO.getBankkey(),SPACES)
		|| isNE(regtvstIO.getBankacckey(),SPACES)) {
			sv.ddind.set("+");
		}
	}

	/**
	 * fwang3 ICIL-4
	 */
/*	private void initPayoutOption() {
		String item = chdrmjaIO.getCnttype().trim() + "1";
		List<Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), TD5G2, item);
		if (items.isEmpty()) {
			sv.payoutoptionErr.set("RRAX"); // not defined in TD5G2
			wsspcomn.edterror.set("Y");
			sv.payoutoption.set(SPACE);
		} else {
			sv.payoutoption.set(getDefaultPayoutOption(item));
		}
	}*/

	/**
	 * fwang3 ICIL-4
	 */
/*	private String getDefaultPayoutOption(String item) {
		descIO.setDescitem(item);
		descIO.setDesctabl(TD5G2);
		findDesc1100();
		return descIO.getShortdesc().toString();
	}	*/
	
	/**
	* <pre>
	**** Read Annuity proposal file using REGTVST values for key.
	* </pre>
	*/
protected void readAnny1400()
	{
		startRead1410();
	}

protected void startRead1410()
	{
		annylnbIO.setChdrcoy(regtvstIO.getChdrcoy());
		annylnbIO.setChdrnum(regtvstIO.getChdrnum());
		annylnbIO.setLife(regtvstIO.getLife());
		annylnbIO.setCoverage(regtvstIO.getCoverage());
		annylnbIO.setRider(regtvstIO.getRider());
		annylnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annylnbIO);
		if (isNE(annylnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annylnbIO.getParams());
			fatalError600();
		}
		/*    Move ANNYLNB fields into W-S which are used later in prog.*/
		wsaaBkfreqann.set(annylnbIO.getFreqann());
		wsaaArrears.set(annylnbIO.getArrears());
	}

	/**
	* <pre>
	**** Read Annuity proposal file using REGTVST values for key.
	* </pre>
	*/
protected void readAnbk1500()
	{
		startRead1510();
	}

protected void startRead1510()
	{
		anbkIO.setChdrcoy(regtvstIO.getChdrcoy());
		anbkIO.setChdrnum(regtvstIO.getChdrnum());
		anbkIO.setLife(regtvstIO.getLife());
		anbkIO.setCoverage(regtvstIO.getCoverage());
		anbkIO.setRider(regtvstIO.getRider());
		anbkIO.setPlanSuffix(regtvstIO.getPlanSuffix());
		anbkIO.setTranno(wsspcomn.tranno);
		anbkIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anbkIO);
		if (isNE(anbkIO.getStatuz(),varcom.oK)
		&& isNE(anbkIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anbkIO.getParams());
			fatalError600();
		}
		/*    Move ANBK fields into W-S which are used later in prog.*/
		if (isNE(anbkIO.getStatuz(),varcom.mrnf)) {
			wsaaBkfreqann.set(anbkIO.getFreqann());
			wsaaArrears.set(anbkIO.getArrears());
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     Retrieve screen fields and exit.
	* </pre>
	*/
protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			protectScr2100();
		}
		/*    If a lump sum payment is being processed, protect the payment*/
		/*    amount, the percentage,the frequency and also the last       */
		/*    payment date.                                                */
		if (isEQ(regtvstIO.getRegpayfreq(),"00")) {
			sv.pymtOut[varcom.pr.toInt()].set("Y");
			sv.prcntOut[varcom.pr.toInt()].set("Y");
		//	sv.prcntOut[varcom.nd.toInt()].set("Y");	
			sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
			sv.epaydateOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
					checkPayment2030();
//					checkAnnuityPayoutOption();//fwang3 ICIL-4
					checkBankDetails2035();
					checkCurrency2040();
					checkFrequency2045();
					checkAnnutiyFrequency(); // ICIL- 267
					validatePayee2050();
					checkPercentage2055();
				case checkDestination2060: 
					checkDestination2060();
					checkPayDates2075();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5222IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                        S5222-DATA-AREA.         */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*    Validate fields.*/
		if (isNE(scrnparams.statuz,varcom.oK)) {
			wsspcomn.edterror.set("Y");
			if (isNE(scrnparams.statuz,varcom.calc)) {
				scrnparams.errorCode.set(errorsInner.curs);
				goTo(GotoLabel.exit2090);
			}
		}
		if ((isNE(sv.ddind,"+")
		&& isNE(sv.ddind,"X")
		&& isNE(sv.ddind,SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			if ((isEQ(sv.ddind,"X")
			&& isEQ(regtvstIO.getBankkey(),SPACES))) {
				sv.ddindErr.set(errorsInner.e493);
				wsspcomn.edterror.set("Y");
			}
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.cltype,SPACES)) {
			sv.cltypeErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(t6692);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.cltype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.cltypeErr.set(errorsInner.g521);
			itemIO.setGenarea(SPACES);
		}
		else {
			descIO.setDescitem(sv.cltype);
			descIO.setDesctabl(t6692);
			findDesc1100();
			sv.clmdesc.set(descIO.getLongdesc());
		}
		t6692rec.t6692Rec.set(itemIO.getGenarea());
		/*    Read T6696.*/
		//ILIFE-5330 start
		if (isEQ(wsspcomn.flag,"M")) {
			//itemIO.setItemitem(t6625rec.ncovvest);
			wsaaT6696Crtable.set(t6625rec.ncovvest);
		}
		else {
			wsaaT6696Crtable.set(covrpf.getCrtable());
		}
		//ILIFE-5330 end
		wsaaT6696Cltype.set(sv.cltype);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(t6696);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6696Key);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(t6696,itdmIO.getItemtabl()))
		|| (isNE(wsaaT6696Key,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.cltypeErr.set(errorsInner.g522);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		t6696rec.t6696Rec.set(itdmIO.getGenarea());
	}
protected void checkPayment2030()
	{
		if (isEQ(sv.rgpymop,SPACES)) {
			sv.rgpymopErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

	/**
	 * fwang3 ICIL-4
	 */
/*	private void checkAnnuityPayoutOption() {
		if (isEQ(sv.payoutoption, SPACES)) {
			sv.payoutoptionErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}*/
	/**
	* <pre>
	*    Check if Bank Details are required on the current Contract.
	* </pre>
	*/
protected void checkBankDetails2035()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(t6694,itdmIO.getItemtabl()))
		|| (isNE(sv.rgpymop,itdmIO.getItemitem().trim()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		else {
			descIO.setDescitem(sv.rgpymop);
			descIO.setDesctabl(t6694);
			findDesc1100();
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		setBankIndicator2200();
	}

protected void checkCurrency2040()
	{
		if (isEQ(sv.claimcur,SPACES)) {
			sv.claimcur.set(chdrpf.getCntcurr());
		}
		descIO.setDescitem(sv.claimcur);
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill(" ");
		}
	}

protected void checkFrequency2045()
	{
		if (isEQ(sv.regpayfreq,SPACES)) {
			sv.regpayfreqErr.set(errorsInner.e186);
		}
		else {
			if ((isNE(sv.regpayfreq,wsaaLastFreq)
			&& isEQ(sv.errorIndicators,SPACES))) {
				calculateFrequency2300();
			}
		}
	}

protected void checkAnnutiyFrequency(){ 
	if(isBankDirect){
		if(!sv.regpayfreq.toString().equalsIgnoreCase("01") && sv.rgpymop.toString().equalsIgnoreCase("A")){
			sv.rgpymopErr.set(errorsInner.rrfp);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}
}

protected void validatePayee2050()
	{
		if (isEQ(t6694rec.payeereq,"Y")) {
			if (isEQ(sv.payclt,SPACES)) {
				sv.payclt.set(sv.cownnum);
			}
		}
		else {
			if (isNE(sv.payclt,SPACES)) {
				sv.paycltErr.set(errorsInner.e492);
			}
		}
		if (isEQ(sv.payclt,SPACES)) {
			sv.payenme.set(SPACES);
		}
		if (isEQ(t6694rec.payeereq,"Y")) {
			payeeValidation2400();
		}
	}

protected void checkPercentage2055()
	{
		if (isEQ(wsaaPayamt1,ZERO)) {
			sv.pymt.set(ZERO);
			sv.prcnt.set(ZERO);
			scrnparams.errorCode.set(errorsInner.g516);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkDestination2060);
		}
		if (isNE(sv.cltypeErr,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkDestination2060);
		}
		if ((isEQ(sv.pymt,ZERO))
		|| (isNE(sv.prcnt,wsaaLastPrcnt)
		&& isNE(sv.prcnt,ZERO)
		&& isEQ(sv.pymt,wsaaLastPymt))) {
			calculatePayment2500();
		}
		else {
			if (isNE(sv.pymt,wsaaLastPymt)
			|| isEQ(sv.prcnt,ZERO)) {
				if (isLT(sv.pymt,0)) {
					sv.pymtErr.set(errorsInner.f351);
				}
				else {
					calculatePercentage2600();
				}
			}
		}
		if ((isEQ(sv.pymtErr,SPACES)
		&& isEQ(sv.prcntErr,SPACES))) {
			wsaaLastPymt.set(sv.pymt);
			wsaaLastPrcnt.set(sv.prcnt);
		}
	}

protected void checkDestination2060()
	{
		if (isEQ(t6694rec.contreq,"Y")) {
			if (isEQ(sv.destkey,SPACES)) {
				sv.destkeyErr.set(errorsInner.e186);
			}
		}
		else {
			if (isNE(sv.destkey,SPACES)) {
				sv.destkeyErr.set(errorsInner.e492);
			}
		}
		if ((isEQ(t6694rec.contreq,"Y")
		&& isEQ(sv.destkeyErr,SPACES))) {
			chdrenqIO.setChdrnum(sv.destkey);
			checkContractNo2700();
		}
	}

protected void checkPayDates2075()
	{
		if ((isNE(sv.anvdate,varcom.vrcmMaxDate)
		&& isNE(sv.anvdate,ZERO))) {
			if (isLT(sv.anvdate,sv.crtdate)) {
				sv.anvdateErr.set(errorsInner.g534);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		if ((isEQ(sv.anvdate,varcom.vrcmMaxDate)
		|| isEQ(sv.anvdate,ZERO))) {
			if (isEQ(t6696rec.inxfrq,SPACES)) {
				sv.anvdate.set(varcom.vrcmMaxDate);
			}
			else {
				datcon2rec.freqFactor.set(1);
				datcon2rec.frequency.set(t6696rec.inxfrq);
				datcon2rec.intDate2.set(ZERO);
				datcon2rec.intDate1.set(sv.crtdate);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				sv.anvdate.set(datcon2rec.intDate2);
			}
		}
		if ((isNE(sv.finalPaydate,varcom.vrcmMaxDate))
		&& (isNE(sv.finalPaydate,ZERO))) {
			if (isLT(sv.finalPaydate,sv.firstPaydate)) {
				sv.epaydateErr.set(errorsInner.g311);
			}
		}
		else {
			sv.finalPaydate.set(varcom.vrcmMaxDate);
		}
		if (isNE(wsaaPayclt,sv.payclt)) {
			if (isEQ(t6694rec.bankreq,"Y")) {
				regtvstIO.setBankkey(SPACES);
				regtvstIO.setBankacckey(SPACES);
				sv.ddind.set("X");
			}
		}
		if (isNE(regtvstIO.getRegpayfreq(),"00")
		&& isEQ(sv.regpayfreqErr,SPACES)) {
			startAccumulation2800();
		}
		else {
			if (isEQ(vstdIO.getVstpay(),0)) {
				wsaaTotalUsed = "Y";
			}
			else {
				wsaaTotalUsed = "N";
			}
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(wsaaPymt,sv.vstpay)) {
			sv.totalamtOut[varcom.nd.toInt()].set(SPACES);
			sv.totalamtErr.set(errorsInner.g515);
			sv.pymtErr.set(errorsInner.g515);
			sv.totalamt.set(wsaaPymt);
		}
		else {
			sv.totalamtOut[varcom.nd.toInt()].set("Y");
			if (isEQ(wsaaPymt,sv.vstpay)) {
				wsaaTotalUsed = "Y";
			}
			else {
				wsaaTotalUsed = "N";
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void protectScr2100()
	{
		/*PROTECT*/
		sv.paycltOut[varcom.pr.toInt()].set("Y");
		sv.cltypeOut[varcom.pr.toInt()].set("Y");
		sv.claimevdOut[varcom.pr.toInt()].set("Y");
		sv.rgpymopOut[varcom.pr.toInt()].set("Y");
		sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
		sv.destkeyOut[varcom.pr.toInt()].set("Y");
		sv.pymtOut[varcom.pr.toInt()].set("Y");
		sv.claimcurOut[varcom.pr.toInt()].set("Y");
		sv.prcntOut[varcom.pr.toInt()].set("Y");
		sv.anvdateOut[varcom.pr.toInt()].set("Y");
		sv.epaydateOut[varcom.pr.toInt()].set("Y");
//		sv.payoutoptionOut[varcom.pr.toInt()].set("Y");//fwang3 ICIL-4
		/*EXIT*/
	}

protected void setBankIndicator2200()
	{
			set2210();
		}

protected void set2210()
	{
		if (isNE(sv.ddindErr,SPACES)) {
			return ;
		}
		if (isEQ(t6694rec.bankreq,"Y")) {
			if ((isEQ(regtvstIO.getBankkey(),SPACES))
			|| (isEQ(regtvstIO.getBankacckey(),SPACES))) {
				sv.ddind.set("X");
			}
			else {
				if (isNE(sv.ddind,"X")) {
					sv.ddind.set("+");
				}
			}
		}
		else {
			
			if (isEQ(sv.ddind,"X") && isEQ(regtvstIO.getBankkey(),SPACES)) {
				sv.ddindErr.set(errorsInner.e570);
				sv.ddind.set(SPACES);
			}
			else if ((isEQ(sv.ddind,"X") || isEQ(sv.ddind,"+")) && isNE(regtvstIO.getBankkey(),SPACES)){
					sv.ddind.set(SPACES);
				}
			else {
				sv.ddind.set(SPACES);
			}
		}
	}

protected void calculateFrequency2300()
	{
			calculation2310();
		}

protected void calculation2310()
	{
		if (isNE(regtvstIO.getRegpayfreq(),"00")
		&& isEQ(sv.regpayfreq,"00")) {
			sv.regpayfreqErr.set(errorsInner.e925);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(t6696rec.frqoride,"N")) {
			sv.regpayfreq.set(wsaaBkfreqann);
			sv.regpayfreqErr.set(errorsInner.g514);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaLastFreq.set(sv.regpayfreq);
			wsaaFreq2.set(sv.regpayfreq);
			wsaaFreq.set(t5606rec.benfreq);
			wsaaPayamt2.set(ZERO);
			compute(wsaaPayamt2, 3).setRounded(div((mult(wsaaPayamt1,wsaaFreq)),wsaaFreq2));
			zrdecplrec.amountIn.set(wsaaPayamt2);
			callRounding5000();
			wsaaPayamt2.set(zrdecplrec.amountOut);
			sv.vstpay.set(wsaaPayamt2);
			descIO.setDescitem(sv.regpayfreq);
			descIO.setDesctabl(t3590);
			findDesc1100();
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.frqdesc.set(descIO.getShortdesc());
			}
			else {
				sv.frqdesc.fill(" ");
			}
			recalculateFirstPaydate2350();
			if (isEQ(sv.pymt,wsaaLastPymt)
			&& isEQ(sv.prcnt,wsaaLastPrcnt)) {
				calculatePercentage2600();
				wsaaLastPrcnt.set(sv.prcnt);
			}
		}
	}

protected void recalculateFirstPaydate2350()
	{
		/*RECALC*/
		if (isNE(wsaaArrears,SPACES)) {
			datcon2rec.intDate1.set(wsspcomn.currfrom);
			datcon2rec.frequency.set(sv.regpayfreq);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			sv.firstPaydate.set(datcon2rec.intDate2);
		}
		/*EXIT*/
	}

protected void payeeValidation2400()
	{
		/*VALIDATE*/
		if (isEQ(sv.paycltErr,SPACES)) {
			cltsIO.setClntnum(sv.payclt);
			getClientDetails1200();
			if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
			|| isNE(cltsIO.getValidflag(),1)) {
				sv.paycltErr.set(errorsInner.e335);
				sv.payenme.set(SPACES);
			}
			else {
				plainname();
				sv.payenme.set(wsspcomn.longconfname);
			}
		}
		/*EXIT*/
	}

protected void calculatePayment2500()
	{
		/*CALCULATION*/
		if (isEQ(sv.pymt,ZERO)
		&& isEQ(sv.prcnt,ZERO)) {
			sv.prcnt.set(t6696rec.dfclmpct);
		}
		compute(sv.pymt, 3).setRounded(div((mult(wsaaPayamt2,sv.prcnt)),100));
		zrdecplrec.amountIn.set(sv.pymt);
		callRounding5000();
		sv.pymt.set(zrdecplrec.amountOut);
		/*    Check that % is not greater than maximum % allowed in T6696.*/
		/*    If it is, error on the percentage field.*/
		if (isGT(sv.prcnt,t6696rec.mxovrpct)) {
			sv.prcntErr.set(errorsInner.g515);
		}
		/*    Check that % is not less than minimum % allowed in T6696.*/
		/*    If it is, error on the percentage field.*/
		if (isLT(sv.prcnt,t6696rec.mnovrpct)) {
			sv.prcntErr.set(errorsInner.g517);
		}
		/*EXIT*/
	}

protected void calculatePercentage2600()
	{
		/*CALCULATION*/
		/*    As the payment amount has been changed, we must recalculate*/
		/*    the percentage based on this new payment amount.*/
		/*    If we are dealing with a lump-sum (S5222-REGPAYFREQ = '00'),*/
		/*    the percentage should be set to zeroes.*/
		if (isEQ(sv.regpayfreq,"00")) {
			sv.prcnt.set(ZERO);
			return ;
		}
		compute(wsaaInterimPrcnt, 10).setRounded(div(sv.pymt,wsaaPayamt2));
		compute(wsaaPrcnt, 10).setRounded(mult(wsaaInterimPrcnt,100));
		sv.prcnt.set(wsaaPrcnt);
		/*    Check that % calculated is not greater than maximum % allowed*/
		/*    in T6696.  If it is, error on the payment field as the amount*/
		/*    is too large.*/
		if (isGT(sv.prcnt,t6696rec.mxovrpct)) {
			sv.pymtErr.set(errorsInner.g515);
		}
		/*    Check that % calculated is not less than minimum % allowed*/
		/*    in T6696.  If it is and the payment amount is zero, error on*/
		/*    the % field, since the default claim % on T6696 is incorrect,*/
		/*    else error on the payment field as the amount is too small.*/
		if (isLT(sv.prcnt,t6696rec.mnovrpct)) {
			sv.pymtErr.set(errorsInner.g517);
		}
	}

protected void checkContractNo2700()
	{
		/*CHECK*/
		chdrenqIO.setChdrcoy(chdrpf.getChdrcoy());
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if ((isNE(chdrenqIO.getStatuz(),varcom.oK))
		&& (isNE(chdrenqIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.mrnf)) {
			sv.destkeyErr.set(errorsInner.g126);
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void startAccumulation2800()
	{
		begin2810();
	}

protected void begin2810()
	{
		wsaaPymt.set(ZERO);
		wsaaPymt2.set(ZERO);
		regtbrkIO.setChdrcoy(regtvstIO.getChdrcoy());
		regtbrkIO.setChdrnum(regtvstIO.getChdrnum());
		regtbrkIO.setLife(regtvstIO.getLife());
		regtbrkIO.setCoverage(regtvstIO.getCoverage());
		regtbrkIO.setRider(regtvstIO.getRider());
		regtbrkIO.setRgpynum(ZERO);
		regtbrkIO.setSeqnbr(ZERO);
		regtbrkIO.setPlanSuffix(regtvstIO.getPlanSuffix());
		regtbrkIO.setStatuz(varcom.oK);
		regtbrkIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regtbrkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtbrkIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(regtbrkIO.getStatuz(),varcom.endp))) {
			accumRegt2900();
		}
		
		compute(wsaaPymt, 3).setRounded(add(wsaaPymt,sv.pymt));
	}

protected void accumRegt2900()
	{
			accum2910();
		}

protected void accum2910()
	{
		SmartFileCode.execute(appVars, regtbrkIO);
		if ((isNE(regtbrkIO.getStatuz(),varcom.oK))
		&& (isNE(regtbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtbrkIO.getParams());
			fatalError600();
		}
		if (isEQ(regtbrkIO.getStatuz(),varcom.endp)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT                                          */
			return ;
		}
		/*    Check all claims and add up all payment amounts excluding the*/
		/*    one which is on screen, namely when*/
		/*    REGTBRK-RGPYNUM = S5222-RGPYNUM.*/
		/*    If the REGT record found does not belong to the same coverage*/
		/*    and rider or does not have the same plan suffix, move ENDP to*/
		/*    the status to end the loop.*/
		if ((isNE(regtvstIO.getChdrcoy(),regtbrkIO.getChdrcoy()))
		|| (isNE(regtvstIO.getChdrnum(),regtbrkIO.getChdrnum()))
		|| (isNE(regtvstIO.getLife(),regtbrkIO.getLife()))
		|| (isNE(regtvstIO.getCoverage(),regtbrkIO.getCoverage()))
		|| (isNE(regtvstIO.getRider(),regtbrkIO.getRider()))
		|| (isNE(regtvstIO.getPlanSuffix(),regtbrkIO.getPlanSuffix()))) {
			regtbrkIO.setStatuz(varcom.endp);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT                                          */
			return ;
		}
		if ((isNE(regtbrkIO.getRgpynum(),sv.rgpynum)
		&& isNE(regtbrkIO.getRegpayfreq(),"00"))) {
			if (isEQ(regtbrkIO.getRegpayfreq(),sv.regpayfreq)) {
				compute(wsaaPymt, 3).setRounded(add(wsaaPymt,regtbrkIO.getPymt()));
			}
			else {
				wsaaFreq2.set(sv.regpayfreq);
				wsaaFreq.set(regtbrkIO.getRegpayfreq());
				wsaaPymt2.set(regtbrkIO.getPymt());
				compute(wsaaPymt2, 3).setRounded((div((mult(wsaaPymt2,wsaaFreq)),wsaaFreq2)));
				compute(wsaaPymt, 3).setRounded(add(wsaaPymt,wsaaPymt2));
			}
		}
		regtbrkIO.setFunction(varcom.nextr);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		updateRegt3100();
	}

protected void updateRegt3100()
	{
			update3110();
		}

protected void update3110()
	{
		if (isEQ(t6694rec.contreq,"Y")) {
			regtvstIO.setDestkey(sv.destkey);
			regtvstIO.setGlact(t6694rec.glact);
		}
		else {
			regtvstIO.setDestkey(SPACES);
			regtvstIO.setGlact(t6694rec.glact);
		}
//		regtvstIO.setPayoutopt(sv.payoutoption);//FWANG3
		regtvstIO.setPlanSuffix(vstdIO.getPlanSuffix());
		regtvstIO.setSacscode(t6694rec.sacscode);
		regtvstIO.setSacstype(t6694rec.sacstype);
		regtvstIO.setDebcred(t6694rec.debcred);
		regtvstIO.setRgpytype(t6690rec.rgpytype);
		regtvstIO.setRgpynum(sv.rgpynum);
		regtvstIO.setPayclt(sv.payclt);
		regtvstIO.setPayreason(sv.cltype);
		regtvstIO.setClaimevd(sv.claimevd);
		regtvstIO.setRgpymop(sv.rgpymop);
		regtvstIO.setRegpayfreq(sv.regpayfreq);
		regtvstIO.setPrcnt(sv.prcnt);
		regtvstIO.setPymt(sv.pymt);
		regtvstIO.setCurrcd(sv.claimcur);
		regtvstIO.setCrtdate(sv.crtdate);
		regtvstIO.setRevdte(sv.revdate);
		regtvstIO.setFirstPaydate(sv.firstPaydate);
		regtvstIO.setAnvdate(sv.anvdate);
		regtvstIO.setFinalPaydate(sv.finalPaydate);
		if (isNE(t6694rec.bankreq,"Y")) {
			regtvstIO.setBankkey(SPACES);
			regtvstIO.setBankacckey(SPACES);
		}
		regtvstIO.setFunction(varcom.keeps);
		regtvstIO.setFormat(formatsInner.regtvstrec);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.ddind,"X")) {
			return ;
		}
		regtvstIO.setFunction(varcom.writs);
		regtvstIO.setFormat(formatsInner.regtvstrec);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		wssplife.updateFlag.set(wsaaTotalUsed);
	}

protected void whereNext4000()
	{
		try {
			nextProgram4000();
			popUp4010();
			genssw4020();
			nextProgram4030();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4000()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		if (isEQ(sv.ddind,"X")) {
			regtvstIO.setFunction(varcom.keeps);
			regtvstIO.setFormat(formatsInner.regtvstrec);
			SmartFileCode.execute(appVars, regtvstIO);
			if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(regtvstIO.getParams());
				fatalError600();
			}
		}
		else {
			if (isEQ(sv.ddind,"?")) {
				checkBankDetails4400();
			}
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		&& isNE(sv.ddind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}

protected void popUp4010()
	{
		gensswrec.function.set(SPACES);
		if ((isEQ(sv.ddind,"X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
			sv.ddind.set("?");
			gensswrec.function.set("A");
		}
	}

protected void genssw4020()
	{
		if (isEQ(gensswrec.function,SPACES)) {
			
			/*if(isNE(sv.ddind,"+") && isNE(wsspcomn.flag, "I")){
				regtvstIO.setBankkey(SPACES);
				regtvstIO.setBankacckey(SPACES);
				regtvstIO.setFunction(varcom.rewrt);
				regtvstIO.setFormat(formatsInner.regtvstrec);
				SmartFileCode.execute(appVars, regtvstIO);
				if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(regtvstIO.getParams());
					fatalError600();
				}
			}*/
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz,varcom.oK))
		&& (isNE(gensswrec.statuz,varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4030()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void checkBankDetails4400()
	{
		bank4400();
	}

protected void bank4400()
	{
		regtvstIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		regtvstIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		if ((isEQ(regtvstIO.getBankkey(),SPACES))
		&& (isEQ(regtvstIO.getBankacckey(),SPACES))) {
			sv.ddind.set(SPACES);
		}
		else {
			sv.ddind.set("+");
			wsaaPayclt.set(sv.payclt);
		}
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
	private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
	private FixedLengthStringData e570 = new FixedLengthStringData(4).init("E570");
	private FixedLengthStringData e925 = new FixedLengthStringData(4).init("E925");
	private FixedLengthStringData f351 = new FixedLengthStringData(4).init("F351");
	private FixedLengthStringData g126 = new FixedLengthStringData(4).init("G126");
	private FixedLengthStringData g311 = new FixedLengthStringData(4).init("G311");
	private FixedLengthStringData g514 = new FixedLengthStringData(4).init("G514");
	private FixedLengthStringData g515 = new FixedLengthStringData(4).init("G515");
	private FixedLengthStringData g516 = new FixedLengthStringData(4).init("G516");
	private FixedLengthStringData g517 = new FixedLengthStringData(4).init("G517");
	private FixedLengthStringData g521 = new FixedLengthStringData(4).init("G521");
	private FixedLengthStringData g522 = new FixedLengthStringData(4).init("G522");
	private FixedLengthStringData g529 = new FixedLengthStringData(4).init("G529");
	private FixedLengthStringData g534 = new FixedLengthStringData(4).init("G534");
	private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
	private FixedLengthStringData rrfp = new FixedLengthStringData(4).init("RRFP");
	
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData regtvstrec = new FixedLengthStringData(10).init("REGTVSTREC");
}

}
