package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sjl39
 * @version 1.0 generated on 05/03/20 06:38
 * @author Quipoz
 */
public class Sjl39ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(640);
	public FixedLengthStringData dataFields = new FixedLengthStringData(320).isAPartOf(dataArea, 0);	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData annpmntstatus = DD.premstatus_longdesc.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData cownnum = DD.ownersel.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,136);
	public FixedLengthStringData payee = DD.ownersel.copy().isAPartOf(dataFields,183);
	public FixedLengthStringData payeename = DD.ownername.copy().isAPartOf(dataFields,193);
	public FixedLengthStringData annpmntopt = DD.annpmntopt.copy().isAPartOf(dataFields, 240);
	public ZonedDecimalData annupmnttrm = DD.annupmnttrm.copyToZonedDecimal().isAPartOf(dataFields,242);
	public ZonedDecimalData dfrrdtrm = DD.dfrrdtrm.copyToZonedDecimal().isAPartOf(dataFields,245);
	public ZonedDecimalData dfrrdamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,248);
	public FixedLengthStringData annpmntfreq = DD.billfreq.copy().isAPartOf(dataFields, 265);
	public ZonedDecimalData totannufund = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,267);
	public ZonedDecimalData partpmntamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,284);
	public ZonedDecimalData annupmntamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,301);	
	public FixedLengthStringData action1 = DD.action.copy().isAPartOf(dataFields,318);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,319);//ILJ-286
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 320);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData annpmntstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);	
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);	
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData payeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);	
	public FixedLengthStringData payeenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData annpmntoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData annupmnttrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData dfrrdtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData dfrrdamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData annpmntfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData totannufundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData partpmntamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);	
	public FixedLengthStringData annupmntamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);	
	public FixedLengthStringData action1Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);	
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 400);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] annpmntstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);	
	public FixedLengthStringData[] annupmntamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] totannufundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);	
	public FixedLengthStringData[] annpmntoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] annupmnttrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] partpmntamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] annpmntfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] dfrrdtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] dfrrdamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] payeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] payeenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] action1Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
			
	public FixedLengthStringData subfileArea = new FixedLengthStringData(355);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(128).isAPartOf(subfileArea, 0);
	public ZonedDecimalData pmntno = DD.pmntno.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData pmntdate = DD.pmntdate.copyToZonedDecimal().isAPartOf(subfileFields,5);
	public ZonedDecimalData annuityamt = DD.amnt.copyToZonedDecimal().isAPartOf(subfileFields,13);
	public ZonedDecimalData intrate = DD.amnt.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData annuitywint = DD.amnt.copyToZonedDecimal().isAPartOf(subfileFields,47);
	public ZonedDecimalData wholdtax = DD.amnt.copyToZonedDecimal().isAPartOf(subfileFields,64);
	public ZonedDecimalData paidamt = DD.amnt.copyToZonedDecimal().isAPartOf(subfileFields,81);
	public FixedLengthStringData pmntstatus = DD.pmntstatus.copy().isAPartOf(subfileFields,98);
	public FixedLengthStringData fillh = DD.fillh.copy().isAPartOf(subfileFields,118);
	public FixedLengthStringData filll = DD.filll.copy().isAPartOf(subfileFields,121);
	public FixedLengthStringData hflag = DD.hflag.copy().isAPartOf(subfileFields,124);	
	public FixedLengthStringData hselect = DD.hselect.copy().isAPartOf(subfileFields,125);
	public FixedLengthStringData dataloc = DD.dataloc.copy().isAPartOf(subfileFields,126);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,127);	
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(56).isAPartOf(subfileArea, 128);
	public FixedLengthStringData pmntnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData pmntdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData annuityamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData intrateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData annuitywintErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData wholdtaxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData paidamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData pmntstatusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData fillhErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData filllErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData hflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData hselectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData datalocErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(168).isAPartOf(subfileArea, 184);
	public FixedLengthStringData[] pmntnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] pmntdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] annuityamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] intrateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] annuitywintOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] wholdtaxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] paidamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] pmntstatusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] fillhOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] filllOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);	
	public FixedLengthStringData[] hselectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);	
	public FixedLengthStringData[] datalocOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 352);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData pmntdateDisp = new FixedLengthStringData(10);		
	public FixedLengthStringData numsel =  new FixedLengthStringData(10);	
	
	public LongData Sjl39screensflWritten = new LongData(0);
	public LongData Sjl39screenctlWritten = new LongData(0);
	public LongData Sjl39screenWritten = new LongData(0);
	public LongData Sjl39protectWritten = new LongData(0);
	public GeneralTable sjl39screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sjl39screensfl;
	}

	public Sjl39ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","13",null, null, null, null, null, null, null, null});			
		screenSflFields = new BaseData[] {pmntno, pmntdate, annuityamt, intrate, annuitywint, wholdtax, paidamt, pmntstatus, fillh, filll, hflag, hselect, dataloc, select, indxflg};
		screenSflOutFields = new BaseData[][] {pmntnoOut, pmntdateOut, annuityamtOut, intrateOut, annuitywintOut, wholdtaxOut, paidamtOut, pmntstatusOut, fillhOut, filllOut, hflagOut, hselectOut, datalocOut, selectOut, indxflgOut};
		screenSflErrFields = new BaseData[] {pmntnoErr, pmntdateErr, annuityamtErr, intrateErr, annuitywintErr, wholdtaxErr, paidamtErr, pmntstatusErr, fillhErr, filllErr, hflagErr, hselectErr, datalocErr, selectErr, indxflgErr};
		screenSflDateFields = new BaseData[] {pmntdate};
		screenSflDateErrFields = new BaseData[] {pmntdateErr};
		screenSflDateDispFields = new BaseData[] {pmntdateDisp};
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, annpmntstatus, cownnum, ownername, lifenum, lifename, payee, payeename, annpmntopt, annupmnttrm, dfrrdtrm, dfrrdamt, annpmntfreq, totannufund, partpmntamt, annupmntamt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, annpmntstatusOut, cownnumOut, ownernameOut, lifenumOut, lifenameOut, payeeOut, payeenameOut, annpmntoptOut, annupmnttrmOut, dfrrdtrmOut, dfrrdamtOut, annpmntfreqOut, totannufundOut, partpmntamtOut, annupmntamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, annpmntstatusErr, cownnumErr, ownernameErr, lifenumErr, lifenameErr, payeeErr, payeenameErr, annpmntoptErr, annupmnttrmErr, dfrrdtrmErr, dfrrdamtErr, annpmntfreqErr, totannufundErr, partpmntamtErr, annupmntamtErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl39screen.class;
		screenSflRecord = Sjl39screensfl.class;
		screenCtlRecord = Sjl39screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl39protect.class;
	}
	
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl39screenctl.lrec.pageSubfile);
	}

}

