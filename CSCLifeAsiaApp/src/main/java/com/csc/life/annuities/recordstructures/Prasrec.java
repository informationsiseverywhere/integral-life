package com.csc.life.annuities.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:56
 * Description:
 * Copybook name: PRASREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Prasrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData prascalcRec = new FixedLengthStringData(56);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(prascalcRec, 0);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(2).isAPartOf(prascalcRec, 4);
  	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(prascalcRec, 6);
  	public ZonedDecimalData incomeSeqNo = new ZonedDecimalData(2, 0).isAPartOf(prascalcRec, 14).setUnsigned();
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(prascalcRec, 16);
  	public PackedDecimalData grossprem = new PackedDecimalData(17, 2).isAPartOf(prascalcRec, 19);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(prascalcRec, 28);
  	public FixedLengthStringData taxrelmth = new FixedLengthStringData(4).isAPartOf(prascalcRec, 33);
  	public FixedLengthStringData company = new FixedLengthStringData(2).isAPartOf(prascalcRec, 37);
  	public FixedLengthStringData inrevnum = new FixedLengthStringData(8).isAPartOf(prascalcRec, 39);
  	public PackedDecimalData taxrelamt = new PackedDecimalData(17, 2).isAPartOf(prascalcRec, 47);


	public void initialize() {
		COBOLFunctions.initialize(prascalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		prascalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}