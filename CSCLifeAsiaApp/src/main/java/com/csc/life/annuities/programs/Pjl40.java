/*
 * File: Pjl40.java
 * Date: 12 March 2020 10:39:37 PM
 * Author: agoel51
 * 
 * Class transformed from Pjl40.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.annuities.dataaccess.dao.AnnypaypfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.life.annuities.screens.Sjl40ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Pjl40 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl40");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("03");
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl40.class);

	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Sjl40ScreenVars sv = ScreenProgram.getScreenVars(Sjl40ScreenVars.class);
	private Batckey wsaaBatckey1 = new Batckey();
	private Ptrnpf ptrnpf = null;
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
	
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Chdrpf chdrpf = new Chdrpf();
	private Annyregpf annyregpf = new Annyregpf();
	private AnnyregpfDAO annyregpfDAO = getApplicationContext().getBean("annyregpfDAO" , AnnyregpfDAO.class);
	private Annypaypf annypaypf = new Annypaypf();
	private AnnypaypfDAO annypaypfDAO = getApplicationContext().getBean("annypaypfDAO" , AnnypaypfDAO.class);
	protected List<Annypaypf> annypaypfList;
	protected List<Annypaypf> annypaypfList1;
	protected List<Annypaypf> annypaypfList2;
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private Regppf regppf = null;
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	private final String PP = "PP";
	private final String PT = "PT";
	private final String PD = "PD";
	private String wsaaAnnPmntSts;
	private static final String T5688 = "T5688";
	private static final String TJL12 = "TJL12";
	private static final String T3588 = "T3588";
	private static final String JL65 = "JL65";
	private static final String JL54 = "JL54";
	private static final String E186 = "E186";

	private FixedLengthStringData wsaaTasr = new FixedLengthStringData(4).init("TASR");
	private FixedLengthStringData wsaaTask = new FixedLengthStringData(4).init("TASK");
	
	private Chdrpf chdrpfNew = new Chdrpf();
	private int tranno=0;

	public Pjl40() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl40", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/

@Override
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}

@Override
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
@Override
protected void initialise1000()
	{
	
		initialise1010();
		
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		retrvContract1020();
		annyregpf = annyregpfDAO.getAnnyregpf(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.annpmntstatus.set(annyregpf.getAnnpstcde());
		wsaaAnnPmntSts = annyregpf.getAnnpstcde();
		sv.annpmntopt.set(annyregpf.getAnnpmntopt());
		annypaypfList=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PP);
		sv.nextpmntdt.set(annypaypfList.get(0).getNextpaydt());
		sv.annupmntamt.set(annyregpf.getAnnupmntamt());
		sv.dfrrdamt.set(annyregpf.getDfrrdamt());
		sv.totbalannamt.set(SPACES);
		sv.newpmntdt.set(sv.nextpmntdt);
		chdrpfNew = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
		tranno=chdrpfNew.getTranno()+1;
		loadLongDesc();
		annypaypfList1=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PT);
		if(isEQ(annyregpf.getDfrrdamt().toBigInteger(),ZERO))
		{
			if(!annypaypfList1.isEmpty())
			{
				compute(sv.totbalannamt,2).set(mult((annypaypfList1.size()+1),sv.annupmntamt));
			}
			else
			{
				compute(sv.totbalannamt,2).set(mult(annypaypfList.size(),sv.annupmntamt));
			}
			
			sv.finalpayamt.set(sv.totbalannamt);
		}
		
		else if(isEQ(annyregpf.getAnnupmntamt().toBigInteger(),ZERO))
		{
			
			if(!annypaypfList1.isEmpty())
			{
				compute(sv.totbalannamt,2).set(mult((annypaypfList1.size()+1),sv.dfrrdamt));
			}
			else
			{
				compute(sv.totbalannamt,2).set(mult(annypaypfList.size(),sv.dfrrdamt));
			}
			
			
			sv.finalpayamt.set(annyregpf.getDfrrdamt());
		}
		

	}

protected void loadLongDesc(){
	Map<String, String> itemMap = new HashMap<>();		
	itemMap.put(TJL12, wsaaAnnPmntSts);
	itemMap.put(T5688, chdrpf.getCnttype());
	itemMap.put(T3588, chdrpf.getPstcde());
	Map<String, Descpf> descMap = descdao.searchMultiDescpf(wsspcomn.company.toString(),
			wsspcomn.language.toString(), itemMap);
	if (!descMap.containsKey(T5688)) {			
		sv.ctypedes.set(SPACES);
	} else {
		sv.ctypedes.set(descMap.get(T5688).getLongdesc());
	}
	if (!descMap.containsKey(TJL12)) {			
		sv.annpmntstatus.set(SPACES);
	} else {
		sv.annpmntstatus.set(descMap.get(TJL12).getLongdesc());
	}
}

protected void retrvContract1020()
{	
	chdrpf = chdrpfDAO.getCacheObject(chdrpf);
	if(null==chdrpf) {
		chdrmjaIO.setFunction(Varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		else {
			chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
			if(null==chdrpf) {
				fatalError600();
			}
			else {
				chdrpfDAO.setCacheObject(chdrpf);
			}
		}
	}	
}


protected void preScreenEdit()
	{
	  /*PRE-START*/
		
	if (isEQ(wsspcomn.flag, "I")) {
		sv.newpmntdtOut[Varcom.pr.toInt()].set("Y");

	} 
	if (isEQ(wsspcomn.flag, "M")) {
		sv.newpmntdtOut[Varcom.pr.toInt()].set("N");
	}

		/*PRE-EXIT*/
	}
@Override
protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(Varcom.oK);
			return;
		}
		wsspcomn.edterror.set(Varcom.oK);
		
		if (isEQ(scrnparams.statuz,Varcom.kill)) {
			return;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}	
	}

protected void validate2020()
	{
		
		 if (isEQ(scrnparams.statuz, Varcom.kill)) {
	         return;
	     }
	     if (isEQ(wsspcomn.flag, "I")) {
	         return;
	     }
	     if (isEQ(scrnparams.statuz, Varcom.calc)) {
	         wsspcomn.edterror.set("Y");
	     }
	     
		datcon1rec.function.set(Varcom.tday);  
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		
		if (isGT(sv.newpmntdt, sv.nextpmntdt)) 
		{
			sv.newpmntdtErr.set(JL65);
		}
		
		else if(isLT(sv.newpmntdt, datcon1rec.intDate)) 
		{
			sv.newpmntdtErr.set(JL54);
		}
		
		else if (isEQ(sv.newpmntdt, SPACES)) 
		{
			sv.newpmntdtErr.set(E186);
		}
	
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}

	}


@Override
protected void update3000()
	{
		updateDatabase3010();
		writePtrn3900();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required / WSSP*/
		updateStatus();
		updateAnnyreg();
		updateRegppf();
		
	}

protected void updateStatus()
{
	
	annypaypfList=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PD); 
	if(!annypaypfList.isEmpty())
	{
		for (int i = 0; i < annypaypfList.size(); i++) {
			
			annypaypf = annypaypfList.get(i);
			annypaypfDAO.updateValidflg(annypaypf);
			
			
			Annypaypf annypayNew1 = annypaypf;

			annypayNew1.setEffectivedt(datcon1rec.intDate.toInt());
			annypayNew1.setValidflag("2");
			annypayNew1.setTranno(tranno);
			annypayNew1.setBatctrcde(wsaaTask.toString());
			
			try {
				annypaypfDAO.insertAnnypaypf(annypayNew1);
			} catch (Exception ex) {
				LOGGER.error("failed to insert into Annypaypf:",ex);
			}
			
			
			Annypaypf annypayNew = annypaypf;

			annypayNew.setEffectivedt(datcon1rec.intDate.toInt());
			annypayNew.setValidflag("1");
			annypayNew.setTranno(tranno);
			annypayNew.setBatctrcde(wsaaTasr.toString());
			
			try {
				annypaypfDAO.insertAnnypaypf(annypayNew);
			} catch (Exception ex) {
				LOGGER.error("Failed to insert:",ex);
			}
		}
		
	}
	
	annypaypfList=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PP); 
	if(!annypaypfList.isEmpty())
	{
		for (int i = 0; i < annypaypfList.size(); i++) {
		
			annypaypf = annypaypfList.get(i);
			annypaypfDAO.updateValidflg(annypaypf);

			Annypaypf annypaypfNew1 = annypaypf;

			annypaypfNew1.setEffectivedt(datcon1rec.intDate.toInt());
			annypaypfNew1.setValidflag("2");
			annypaypfNew1.setTranno(tranno);
			annypaypfNew1.setBatctrcde(wsaaTask.toString());
			
			if(isEQ(i,"0") || isEQ(i,0)  || isEQ(i,ZERO))
			{	
				annypaypfNew1.setPmntstat(PP);
				annypaypfNew1.setAnnaamt(sv.totbalannamt.getbigdata());
				annypaypfNew1.setAnnintamt(sv.totbalannamt.getbigdata());
				annypaypfNew1.setPmntdt(sv.newpmntdt.toInt());
			}
			else
			{
				annypaypfNew1.setPmntstat(PT);
				annypaypfNew1.setAnnaamt(BigDecimal.ZERO);
				annypaypfNew1.setAnnintamt(BigDecimal.ZERO);
			}
			try {
				annypaypfDAO.insertAnnypaypf(annypaypfNew1);
			} catch (Exception ex) {
				LOGGER.error("Insertion in Annypaypf failed:",ex);
			}
			
			
			
			Annypaypf annypaypfNew = annypaypf;

			annypaypfNew.setEffectivedt(datcon1rec.intDate.toInt());
			annypaypfNew.setValidflag("1");
			annypaypfNew.setTranno(tranno);
			annypaypfNew.setBatctrcde(wsaaTasr.toString());
			
			if(isEQ(i,"0") || isEQ(i,0)  || isEQ(i,ZERO))
			{	
				annypaypfNew.setPmntstat(PP);
				annypaypfNew.setAnnaamt(sv.totbalannamt.getbigdata());
				annypaypfNew.setAnnintamt(sv.totbalannamt.getbigdata());
				annypaypfNew.setPmntdt(sv.newpmntdt.toInt());
			}
			else
			{
				annypaypfNew.setPmntstat(PT);
				annypaypfNew.setAnnaamt(BigDecimal.ZERO);
				annypaypfNew.setAnnintamt(BigDecimal.ZERO);
			}
			try {
				annypaypfDAO.insertAnnypaypf(annypaypfNew);
			} catch (Exception ex) {
				LOGGER.error("Insertion failed:",ex);
			}

			
		}
	
	}
}

protected void updateAnnyreg()
{
	annyregpf = annyregpfDAO.getAnnyregpf(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
	
	annyregpfDAO.updateValidflag(annyregpf);
	
	
	Annyregpf annyregNew1 = annyregpf;
	annyregNew1.setFinalpmntdt(sv.newpmntdt.toInt());
	annyregNew1.setEffectivedt(datcon1rec.intDate.toInt());
	annyregNew1.setValidflag("2");
	annyregNew1.setTranno(tranno);
	annyregNew1.setBatctrcde(wsaaTask.toString());
	annyregpfDAO.insertAnnyregpf(annyregNew1);

	Annyregpf annyregNew = annyregpf;
	annyregNew.setFinalpmntdt(sv.newpmntdt.toInt());
	annyregNew.setEffectivedt(datcon1rec.intDate.toInt());
	annyregNew.setValidflag("1");
	annyregNew.setTranno(tranno);
	annyregNew.setBatctrcde(wsaaTasr.toString());
	annyregpfDAO.insertAnnyregpf(annyregNew);

}

protected void updateRegppf()
{
	annypaypfList=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PT); 
	if(!annypaypfList.isEmpty())
	{
		regppf= new Regppf();
		regppf.setChdrcoy(wsspcomn.company.toString());
		regppf.setChdrnum(wsspcomn.chdrChdrnum.toString().trim());
		regppf.setFirstPaydate(sv.nextpmntdt.toInt());
		regppf.setNextPaydate(varcom.vrcmMaxDate.toInt());
		regpDAO.updateNextDate(regppf);	
	}
	
	annypaypfList=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PP); 
	if(!annypaypfList.isEmpty())
	{
	   for (int i = 0; i < annypaypfList.size(); i++) {
		annypaypf = annypaypfList.get(i);
		regppf= new Regppf();
		regppf.setChdrcoy(wsspcomn.company.toString());
		regppf.setChdrnum(wsspcomn.chdrChdrnum.toString().trim());
		regppf.setRgpynum(annypaypfList.get(i).getPmntno());
		regppf.setNextPaydate(sv.newpmntdt.toInt());
		regppf.setPymt(sv.totbalannamt.getbigdata());
		regpDAO.updateNDate(regppf);
		
	   }
		
	}
	
}

protected void writePtrn3900() {

	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrpfx("CH");
	ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	ptrnpf.setChdrnum(chdrpf.getChdrnum());
	ptrnpf.setTranno(chdrpf.getTranno()+1);
	ptrnpf.setTrdt(varcom.vrcmDate.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setTermid(varcom.vrcmTermid.toString());
	ptrnpf.setUserT(varcom.vrcmUser.toInt());
	ptrnpf.setPtrneff(sv.newpmntdt.toInt());
	ptrnpf.setDatesub(datcon1rec.intDate.toInt());
	wsaaBatckey1.set(wsspcomn.batchkey);
	ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
	ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
	ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
	ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
	ptrnpf.setBatccoy(wsaaBatckey1.batcBatccoy.toString());
	ptrnpf.setBatcpfx(wsaaBatckey1.batcBatcpfx.toString());
	ptrnpf.setBatctrcde(wsaaTasr.toString());
	List<Ptrnpf> list = new ArrayList<Ptrnpf>();
	list.add(ptrnpf);
	if (!ptrnpfDAO.insertPtrnPF(list)) {
		syserrrec.params.set(chdrmjaIO.getChdrcoy().toString()
		.concat(chdrmjaIO.getChdrnum().toString()));
		fatalError600();
	}
}

@Override
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg); 
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}



}
