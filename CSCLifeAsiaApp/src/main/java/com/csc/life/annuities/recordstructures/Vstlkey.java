package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:12
 * Description:
 * Copybook name: VSTLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vstlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vstlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData vstlKey = new FixedLengthStringData(64).isAPartOf(vstlFileKey, 0, REDEFINE);
  	public FixedLengthStringData vstlChdrcoy = new FixedLengthStringData(1).isAPartOf(vstlKey, 0);
  	public FixedLengthStringData vstlChdrnum = new FixedLengthStringData(8).isAPartOf(vstlKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(vstlKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vstlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vstlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}