package com.csc.life.annuities.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AnnyregpfDAOImpl extends BaseDAOImpl<Annyregpf> implements AnnyregpfDAO{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AnnyregpfDAOImpl.class);	
	
	private static final String EFFECTIVEDT = "EFFECTIVEDT";
	private static final String VALIDFLAG = "VALIDFLAG";

	@Override
	public Annyregpf getAnnyregpf(String chdrcoy, String chdrnum) {		
		 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,OCCDATE,CNTBRANCH,COWNNUM,LIFCNUM,CLTSEX,ANNUITYPE,BENEFTYPE,TOTANNUFUND,ANNPMNTOPT,ANNUPMNTTRM,"
                 + " PARTPMNTAMT,ANNPMNTFREQ,DFRRDTRM,DFRRDAMT,ANNUPMNTAMT,FUNDSETUPDT,PMNTSTARTDT,"
                 + " DOCACCPTDT,FRSTPMNTDT,ANNIVERSDT,FINALPMNTDT,"
                 + " ANNPSTCDE, VALIDFLAG, EFFECTIVEDT, PAYEE,PMNTCURR,RGPYMOP,PAYEEACCNUM,BANKCD,BRANCHCD,"
                 + " BANKACCDSC,BNKACTYP, UNIQUE_NUMBER,TRANNO FROM ANNYREGPF "
                 + " WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG='1' ");
         sqlCovrSelect1
                 .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
       
 		ResultSet rs = null;
 		Annyregpf annyregpf = null;

 		try { 	 			
 			ps.setString(1, chdrcoy.trim()); 			
 			ps.setString(2, chdrnum);
 			
 			rs = ps.executeQuery();

 			while (rs.next()) {
 				annyregpf = new Annyregpf(); 				
 				annyregpf.setChdrcoy(rs.getString(1));
 				annyregpf.setChdrnum(rs.getString(2));
 				annyregpf.setOccdate(rs.getInt(3));
 				annyregpf.setCntbranch(rs.getString("CNTBRANCH"));
 				annyregpf.setCownnum(rs.getString("COWNNUM"));
 				annyregpf.setLifecnum(rs.getString("LIFCNUM"));
 				annyregpf.setCltsex(rs.getString("CLTSEX"));
 				annyregpf.setAnnuitype(rs.getString("ANNUITYPE"));
 				annyregpf.setBeneftype(rs.getString("BENEFTYPE"));
 				annyregpf.setTotannufund(rs.getBigDecimal("TOTANNUFUND"));
 				annyregpf.setAnnpmntopt(rs.getString("ANNPMNTOPT"));
 				annyregpf.setAnnupmnttrm(rs.getInt("ANNUPMNTTRM"));
 				annyregpf.setPartpmntamt(rs.getBigDecimal("PARTPMNTAMT"));
 				annyregpf.setAnnpmntfreq(rs.getString("ANNPMNTFREQ"));
 				annyregpf.setDfrrdtrm(rs.getInt("DFRRDTRM"));
 				annyregpf.setDfrrdamt(rs.getBigDecimal("DFRRDAMT"));
 				annyregpf.setAnnupmntamt(rs.getBigDecimal("ANNUPMNTAMT"));
 				annyregpf.setFundsetupdt(rs.getInt("FUNDSETUPDT"));
 				annyregpf.setPmntstartdt(rs.getInt("PMNTSTARTDT"));
 				annyregpf.setDocaccptdt(rs.getInt("DOCACCPTDT"));
 				annyregpf.setFrstpmntdt(rs.getInt("FRSTPMNTDT"));
 				annyregpf.setAnniversdt(rs.getInt("ANNIVERSDT"));
 				annyregpf.setFinalpmntdt(rs.getInt("FINALPMNTDT"));	
 				annyregpf.setAnnpstcde(rs.getString("ANNPSTCDE"));
 				annyregpf.setValidflag(rs.getString(VALIDFLAG));
 				annyregpf.setEffectivedt(rs.getInt(EFFECTIVEDT));
 				annyregpf.setTranno(rs.getInt("TRANNO"));
 				annyregpf.setPayee(rs.getString("PAYEE"));
 				annyregpf.setPmntcurr(rs.getString("PMNTCURR"));
 				annyregpf.setRgpymop(rs.getString("RGPYMOP"));
 				annyregpf.setPayeeaccnum(rs.getString("PAYEEACCNUM"));
 				annyregpf.setBankCd(rs.getString("BANKCD"));
 				annyregpf.setBranchCd(rs.getString("BRANCHCD"));
 				annyregpf.setBankaccdsc(rs.getString("BANKACCDSC"));
 				annyregpf.setBnkactyp(rs.getString("BNKACTYP"));
 				annyregpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER")); 
			}
		} catch (SQLException e) {
			LOGGER.error("getAnnyregpf:", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return annyregpf;
	
	}
	
	@Override
	public Annyregpf getAnnyregpfbyEffdt(String chdrcoy,String chdrnum, int effectivedt, int tranno, String batctrcde){		
		 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,OCCDATE,CNTBRANCH,COWNNUM,LIFCNUM,CLTSEX,ANNUITYPE,BENEFTYPE,TOTANNUFUND,ANNPMNTOPT,ANNUPMNTTRM,"
                 + "PARTPMNTAMT,ANNPMNTFREQ,DFRRDTRM,DFRRDAMT,ANNUPMNTAMT,FUNDSETUPDT,PMNTSTARTDT,DOCACCPTDT,FRSTPMNTDT,ANNIVERSDT,FINALPMNTDT,"
                 + "ANNPSTCDE,PAYEE,PMNTCURR,RGPYMOP,PAYEEACCNUM,BANKCD, "
                 + "BRANCHCD,BANKACCDSC,BNKACTYP,  VALIDFLAG, EFFECTIVEDT, TRANNO, BATCTRCDE, UNIQUE_NUMBER FROM ANNYREGPF "
                 + " WHERE CHDRCOY = ? AND CHDRNUM = ?  AND EFFECTIVEDT = ? AND TRANNO=? AND BATCTRCDE=? ");
         sqlCovrSelect1
                 .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
       
 		ResultSet rs = null;
 		Annyregpf annyregpf = null;

 		try { 	 			
 			ps.setString(1, chdrcoy.trim()); 			
 			ps.setString(2, chdrnum);
 			ps.setInt(3, effectivedt);
 			ps.setInt(4, tranno);
 			ps.setString(5, batctrcde);
 			
 			rs = ps.executeQuery();

 			while (rs.next()) {
 				annyregpf = new Annyregpf(); 				
 				annyregpf.setChdrcoy(rs.getString(1));
 				annyregpf.setChdrnum(rs.getString(2));
 				annyregpf.setOccdate(rs.getInt("OCCDATE"));
 				annyregpf.setCntbranch(rs.getString("CNTBRANCH"));
 				annyregpf.setCownnum(rs.getString("COWNNUM"));
 				annyregpf.setLifecnum(rs.getString("LIFCNUM"));
 				annyregpf.setCltsex(rs.getString("CLTSEX"));
 				annyregpf.setAnnuitype(rs.getString("ANNUITYPE"));
 				annyregpf.setBeneftype(rs.getString("BENEFTYPE"));
 				annyregpf.setTotannufund(rs.getBigDecimal("TOTANNUFUND"));
 				annyregpf.setAnnpmntopt(rs.getString("ANNPMNTOPT"));
 				annyregpf.setAnnupmnttrm(rs.getInt("ANNUPMNTTRM"));
 				annyregpf.setPartpmntamt(rs.getBigDecimal("PARTPMNTAMT"));
 				annyregpf.setAnnpmntfreq(rs.getString("ANNPMNTFREQ"));
 				annyregpf.setDfrrdtrm(rs.getInt("DFRRDTRM"));
 				annyregpf.setDfrrdamt(rs.getBigDecimal("DFRRDAMT"));
 				annyregpf.setAnnupmntamt(rs.getBigDecimal("ANNUPMNTAMT"));
 				annyregpf.setFundsetupdt(rs.getInt("FUNDSETUPDT"));
 				annyregpf.setPmntstartdt(rs.getInt("PMNTSTARTDT"));
 				annyregpf.setDocaccptdt(rs.getInt("DOCACCPTDT"));
 				annyregpf.setFrstpmntdt(rs.getInt("FRSTPMNTDT"));
 				annyregpf.setAnniversdt(rs.getInt("ANNIVERSDT"));
 				annyregpf.setFinalpmntdt(rs.getInt("FINALPMNTDT"));	
 				annyregpf.setValidflag(rs.getString(VALIDFLAG));
 				annyregpf.setEffectivedt(rs.getInt(EFFECTIVEDT));
 				annyregpf.setTranno(rs.getInt("TRANNO"));
 				annyregpf.setBatctrcde(rs.getString("BATCTRCDE"));
 				annyregpf.setAnnpstcde(rs.getString("ANNPSTCDE"));
 				annyregpf.setPayee(rs.getString("PAYEE"));
 				annyregpf.setPmntcurr(rs.getString("PMNTCURR"));
 				annyregpf.setRgpymop(rs.getString("RGPYMOP"));
 				annyregpf.setPayeeaccnum(rs.getString("PAYEEACCNUM"));
 				annyregpf.setBankCd(rs.getString("BANKCD"));
 				annyregpf.setBranchCd(rs.getString("BRANCHCD"));
 				annyregpf.setBankaccdsc(rs.getString("BANKACCDSC"));
 				annyregpf.setBnkactyp(rs.getString("BNKACTYP"));
 				annyregpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER")); 
			}
		} catch (SQLException e) {
			LOGGER.error("getAnnyregpfbyEffdt:", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return annyregpf;
	
	}
	

	@Override
	public void insertAnnyregpf(Annyregpf pf) {
		String stmt = "INSERT INTO ANNYREGPF (CHDRCOY,CHDRNUM,OCCDATE,CNTBRANCH,COWNNUM,LIFCNUM,CLTSEX,ANNUITYPE,BENEFTYPE,TOTANNUFUND,ANNPMNTOPT,ANNUPMNTTRM,"
                 + "PARTPMNTAMT,ANNPMNTFREQ,DFRRDTRM,DFRRDAMT,ANNUPMNTAMT,FUNDSETUPDT,PMNTSTARTDT,DOCACCPTDT,FRSTPMNTDT,ANNIVERSDT,FINALPMNTDT,"
                 + "ANNPSTCDE, VALIDFLAG, EFFECTIVEDT,BATCTRCDE,TRANNO,PAYEE,PMNTCURR,RGPYMOP,PAYEEACCNUM,BANKCD,BRANCHCD,BANKACCDSC,BNKACTYP,USRPRF,DATIME) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = getPrepareStatement(stmt);
		try {
			int i=1;
			ps.setString(i++, pf.getChdrcoy());
			ps.setString(i++, pf.getChdrnum());
			ps.setInt(i++, pf.getOccdate());
			ps.setString(i++, pf.getCntbranch());
			ps.setString(i++, pf.getCownnum());
			ps.setString(i++, pf.getLifecnum());
			ps.setString(i++, pf.getCltsex());
			ps.setString(i++, pf.getAnnuitype());
			ps.setString(i++, pf.getBeneftype());
			ps.setBigDecimal(i++, pf.getTotannufund());
			ps.setString(i++, pf.getAnnpmntopt());
			ps.setInt(i++, pf.getAnnupmnttrm());
			ps.setBigDecimal(i++, pf.getPartpmntamt());
			ps.setString(i++, pf.getAnnpmntfreq());
			ps.setInt(i++, pf.getDfrrdtrm());
			ps.setBigDecimal(i++, pf.getDfrrdamt());
			ps.setBigDecimal(i++, pf.getAnnupmntamt());
			ps.setInt(i++, pf.getFundsetupdt());
			ps.setInt(i++, pf.getPmntstartdt());			
			ps.setInt(i++, pf.getDocaccptdt());		
			ps.setInt(i++, pf.getFrstpmntdt());
			ps.setInt(i++, pf.getAnniversdt());
			ps.setInt(i++, pf.getFinalpmntdt());
			ps.setString(i++, pf.getAnnpstcde());
			ps.setString(i++, pf.getValidflag());
			ps.setInt(i++, pf.getEffectivedt());
			ps.setString(i++, pf.getBatctrcde());
			ps.setInt(i++, pf.getTranno());
			ps.setString(i++, pf.getPayee());
			ps.setString(i++, pf.getPmntcurr());
			ps.setString(i++, pf.getRgpymop());
			ps.setString(i++, pf.getPayeeaccnum());
			ps.setString(i++, pf.getBankCd());
			ps.setString(i++, pf.getBranchCd());
			ps.setString(i++, pf.getBankaccdsc());
			ps.setString(i++, pf.getBnkactyp());
			ps.setString(i++, this.getUsrprf());			
            ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));          
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertAnnyregpf:", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		
	}

	@Override
	public void updateAnnyregpf(Annyregpf pf) {
		String stmt = "UPDATE ANNYREGPF SET ANNUITYPE=?,BENEFTYPE=?,TOTANNUFUND=?,ANNPMNTOPT=?,ANNUPMNTTRM=?,"
                 + "PARTPMNTAMT=?,ANNPMNTFREQ=?,DFRRDTRM=?,DFRRDAMT=?,ANNUPMNTAMT=?,FUNDSETUPDT=?,PMNTSTARTDT=?,DOCACCPTDT=?,FRSTPMNTDT=?,ANNIVERSDT=?,FINALPMNTDT=?,"
                 + "ANNPSTCDE=?,PAYEE=?,PMNTCURR=?,RGPYMOP=?,PAYEEACCNUM=?,BANKCD=?,BRANCHCD=?,BANKACCDSC=?,BNKACTYP=?,USRPRF=?,DATIME=? WHERE unique_number=? ";
		PreparedStatement ps = getPrepareStatement(stmt);
		try {
			int i=1;
			ps.setString(i++, pf.getAnnuitype());
			ps.setString(i++, pf.getBeneftype());
			ps.setBigDecimal(i++, pf.getTotannufund());
			ps.setString(i++, pf.getAnnpmntopt());
			ps.setInt(i++, pf.getAnnupmnttrm());
			ps.setBigDecimal(i++, pf.getPartpmntamt());
			ps.setString(i++, pf.getAnnpmntfreq());
			ps.setInt(i++, pf.getDfrrdtrm());
			ps.setBigDecimal(i++, pf.getDfrrdamt());
			ps.setBigDecimal(i++, pf.getAnnupmntamt());
			ps.setInt(i++, pf.getFundsetupdt());
			ps.setInt(i++, pf.getPmntstartdt());			
			ps.setInt(i++, pf.getDocaccptdt());		
			ps.setInt(i++, pf.getFrstpmntdt());
			ps.setInt(i++, pf.getAnniversdt());
			ps.setInt(i++, pf.getFinalpmntdt());
			ps.setString(i++, pf.getAnnpstcde());
			ps.setString(i++, pf.getPayee());
			ps.setString(i++, pf.getPmntcurr());
			ps.setString(i++, pf.getRgpymop());
			ps.setString(i++, pf.getPayeeaccnum());
			ps.setString(i++, pf.getBankCd());
			ps.setString(i++, pf.getBranchCd());
			ps.setString(i++, pf.getBankaccdsc());
			ps.setString(i++, pf.getBnkactyp());
			ps.setString(i++, this.getUsrprf());			
            ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));        
			ps.setLong(i++, pf.getUniqueNumber());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateAnnyregpf:", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	@Override
	public void updateValidflag(Annyregpf annyregpf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYREGPF SET VALIDFLAG='2'");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM=? AND VALIDFLAG= ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setString(1, annyregpf.getChdrcoy());
				ps.setString(2, annyregpf.getChdrnum());
			    ps.setString(3, "1");	
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateValidflag:",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
}
	@Override
	public void updateDate(Annyregpf annyregpf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYREGPF SET FINALPMNTDT=?  WHERE CHDRCOY = ? AND CHDRNUM = ?  ");		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setInt(1, annyregpf.getFinalpmntdt());			    
			    ps.setString(2, annyregpf.getChdrcoy());
			    ps.setString(3, annyregpf.getChdrnum());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateDate:",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
}
		public void updateStat(Annyregpf annyregpf){		
			StringBuilder sb = new StringBuilder("");
			sb.append("UPDATE ANNYREGPF SET ANNPSTCDE=?  WHERE CHDRCOY = ? AND CHDRNUM = ? ");		
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	

					ps.setString(1, annyregpf.getAnnpstcde());			    
				    ps.setString(2, annyregpf.getChdrcoy());
				    ps.setString(3, annyregpf.getChdrnum());
				    
				    ps.executeUpdate();				
					
				
			}catch (SQLException e) {
				LOGGER.error("updateStat:",e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
		
		
	}
		
		
		
		public void updateAnnDate(Annyregpf annyregpf){		
			StringBuilder sb = new StringBuilder("");
			sb.append("UPDATE ANNYREGPF SET ANNIVERSDT=? ");
			sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? ");
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	

					ps.setInt(1, annyregpf.getAnniversdt());			    
				    ps.setString(2, annyregpf.getChdrcoy());
				    ps.setString(3, annyregpf.getChdrnum());
				    
				    ps.executeUpdate();				
					
				
			}catch (SQLException e) {
				LOGGER.error("updateAnnDate:",e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
	}
		
}


