package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5233screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 9;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {14, 21, 4, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5233ScreenVars sv = (S5233ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5233screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5233screensfl, 
			sv.S5233screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5233ScreenVars sv = (S5233ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5233screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5233ScreenVars sv = (S5233ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5233screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5233screensflWritten.gt(0))
		{
			sv.s5233screensfl.setCurrentIndex(0);
			sv.S5233screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5233ScreenVars sv = (S5233ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5233screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5233ScreenVars screenVars = (S5233ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.plansfx.setFieldName("plansfx");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.vstpay.setFieldName("vstpay");
				screenVars.vstlump.setFieldName("vstlump");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.select.setFieldName("select");
				screenVars.rider.setFieldName("rider");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.plansfx.set(dm.getField("plansfx"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.vstpay.set(dm.getField("vstpay"));
			screenVars.vstlump.set(dm.getField("vstlump"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.select.set(dm.getField("select"));
			screenVars.rider.set(dm.getField("rider"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5233ScreenVars screenVars = (S5233ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.plansfx.setFieldName("plansfx");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.vstpay.setFieldName("vstpay");
				screenVars.vstlump.setFieldName("vstlump");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.select.setFieldName("select");
				screenVars.rider.setFieldName("rider");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("plansfx").set(screenVars.plansfx);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("vstpay").set(screenVars.vstpay);
			dm.getField("vstlump").set(screenVars.vstlump);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("select").set(screenVars.select);
			dm.getField("rider").set(screenVars.rider);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5233screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5233ScreenVars screenVars = (S5233ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.plansfx.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.vstpay.clearFormatting();
		screenVars.vstlump.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.rider.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5233ScreenVars screenVars = (S5233ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.plansfx.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.vstpay.setClassString("");
		screenVars.vstlump.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.select.setClassString("");
		screenVars.rider.setClassString("");
	}

/**
 * Clear all the variables in S5233screensfl
 */
	public static void clear(VarModel pv) {
		S5233ScreenVars screenVars = (S5233ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.plansfx.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.vstpay.clear();
		screenVars.vstlump.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.select.clear();
		screenVars.rider.clear();
	}
}
