/*
 * File: P6697.java
 * Date: 30 August 2009 0:53:43
 * Author: Quipoz Limited
 * 
 * Class transformed from P6697.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.annuities.procedures.T6697pt;
import com.csc.life.annuities.screens.S6697ScreenVars;
import com.csc.life.annuities.tablestructures.T6697rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P6697 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6697");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "";
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/* ERRORS */
	private static final String e026 = "E026";
	private static final String e027 = "E027";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6697rec t6697rec = new T6697rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6697ScreenVars sv = ScreenProgram.getScreenVars( S6697ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readPrimaryRecord1020, 
		generalArea1045, 
		other1080, 
		other3080, 
		exit3090, 
		continue4020, 
		other4080
	}

	public P6697() {
		super();
		screenVars = sv;
		new ScreenModel("S6697", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case readPrimaryRecord1020: 
					readPrimaryRecord1020();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				case other1080: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isNE(wsaaFirstTime, "Y")) {
			goTo(GotoLabel.readPrimaryRecord1020);
		}
		if (isEQ(itemIO.getItemseq(), SPACES)) {
			wsaaSeq.set(ZERO);
		}
		else {
			wsaaSeq.set(itemIO.getItemseq());
		}
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
	}

protected void readPrimaryRecord1020()
	{
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq,ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq);
		}
		/*READ-RECORD*/
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaFirstTime = "N";
	}

protected void moveToScreen1040()
	{
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)
		&& isEQ(itemIO.getItemseq(), SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)
		&& isEQ(wsspcomn.flag, "I")) {
			scrnparams.errorCode.set(e026);
			wsaaSeq.subtract(1);
			goTo(GotoLabel.other1080);
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t6697rec.t6697Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t6697rec.empconpc.set(ZERO);
		t6697rec.issdate.set(varcom.maxdate);
		t6697rec.sfodate.set(varcom.maxdate);
	}

protected void generalArea1045()
	{
		sv.adminref.set(t6697rec.adminref);
		sv.contypes.set(t6697rec.contypes);
		sv.crcodes.set(t6697rec.crcodes);
		sv.empcon.set(t6697rec.empcon);
		sv.empconpc.set(t6697rec.empconpc);
		sv.issdate.set(t6697rec.issdate);
		sv.maxfunds.set(t6697rec.maxfunds);
		sv.pentype.set(t6697rec.pentype);
		sv.sfodate.set(t6697rec.sfodate);
		sv.sfonum.set(t6697rec.sfonum);
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
					screenIo2010();
					exit2090();
				}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.rold)
		&& isEQ(itemIO.getItemseq(),SPACES)) {
			scrnparams.errorCode.set(e027);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)
		&& isEQ(itemIO.getItemseq(),"99")) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq,ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(t6697rec.t6697Rec);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setFunction(varcom.writr);
			itemIO.setFormat(itemrec);
		}
		else {
			itemIO.setFunction(varcom.rewrt);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.adminref,t6697rec.adminref)) {
			t6697rec.adminref.set(sv.adminref);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.contypes,t6697rec.contypes)) {
			t6697rec.contypes.set(sv.contypes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.crcodes,t6697rec.crcodes)) {
			t6697rec.crcodes.set(sv.crcodes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.empcon,t6697rec.empcon)) {
			t6697rec.empcon.set(sv.empcon);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.empconpc,t6697rec.empconpc)) {
			t6697rec.empconpc.set(sv.empconpc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.issdate,t6697rec.issdate)) {
			t6697rec.issdate.set(sv.issdate);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxfunds,t6697rec.maxfunds)) {
			t6697rec.maxfunds.set(sv.maxfunds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pentype,t6697rec.pentype)) {
			t6697rec.pentype.set(sv.pentype);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sfodate,t6697rec.sfodate)) {
			t6697rec.sfodate.set(sv.sfodate);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sfonum,t6697rec.sfonum)) {
			t6697rec.sfonum.set(sv.sfonum);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation4020();
				case continue4020: 
					continue4020();
				case other4080: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation4020()
	{
		if (isNE(scrnparams.statuz,varcom.rolu)
		&& isNE(scrnparams.statuz,varcom.rold)) {
			goTo(GotoLabel.continue4020);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaSeq.add(1);
		}
		else {
			wsaaSeq.subtract(1);
		}
		goTo(GotoLabel.other4080);
	}

protected void continue4020()
	{
		wsspcomn.programPtr.add(1);
		wsaaSeq.set(ZERO);
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T6697pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
