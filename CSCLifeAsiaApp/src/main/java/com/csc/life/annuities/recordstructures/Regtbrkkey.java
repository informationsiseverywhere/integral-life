package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:46
 * Description:
 * Copybook name: REGTBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regtbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regtbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regtbrkKey = new FixedLengthStringData(64).isAPartOf(regtbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData regtbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(regtbrkKey, 0);
  	public FixedLengthStringData regtbrkChdrnum = new FixedLengthStringData(8).isAPartOf(regtbrkKey, 1);
  	public FixedLengthStringData regtbrkLife = new FixedLengthStringData(2).isAPartOf(regtbrkKey, 9);
  	public FixedLengthStringData regtbrkCoverage = new FixedLengthStringData(2).isAPartOf(regtbrkKey, 11);
  	public FixedLengthStringData regtbrkRider = new FixedLengthStringData(2).isAPartOf(regtbrkKey, 13);
  	public PackedDecimalData regtbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(regtbrkKey, 15);
  	public PackedDecimalData regtbrkRgpynum = new PackedDecimalData(5, 0).isAPartOf(regtbrkKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(regtbrkKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regtbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regtbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}