package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 06/03/20 05:43
 * @author Quipoz
 */
public class Sjl39screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sjl39screensfl";
		lrec.subfileClass = Sjl39screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 18;
		lrec.pageSubfile = 20;//IFSU-3555
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 5, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl39ScreenVars sv = (Sjl39ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl39screenctlWritten, sv.Sjl39screensflWritten, av, sv.sjl39screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl39ScreenVars screenVars = (Sjl39ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.annpmntstatus.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");		
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.payee.setClassString("");
		screenVars.payeename.setClassString("");		
		screenVars.annpmntopt.setClassString("");
		screenVars.annupmnttrm.setClassString("");
		screenVars.dfrrdtrm.setClassString("");
		screenVars.dfrrdamt.setClassString("");
		screenVars.annpmntfreq.setClassString("");
		screenVars.totannufund.setClassString("");
		screenVars.partpmntamt.setClassString("");
		screenVars.annupmntamt.setClassString("");		
	}

/**
 * Clear all the variables in Sjl39screenctl
 */
	public static void clear(VarModel pv) {
		Sjl39ScreenVars screenVars = (Sjl39ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();		
		screenVars.annpmntstatus.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();		
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.payee.clear();
		screenVars.payeename.clear();		
		screenVars.annpmntopt.clear();
		screenVars.annupmnttrm.clear();
		screenVars.dfrrdtrm.clear();
		screenVars.dfrrdamt.clear();
		screenVars.annpmntfreq.clear();
		screenVars.totannufund.clear();
		screenVars.partpmntamt.clear();
		screenVars.annupmntamt.clear();		
	}
}
