/*
 * File: Vstannc.java
 * Date: 30 August 2009 2:54:17
 * Author: Quipoz Limited
 * 
 * Class transformed from VSTANNC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.annuities.recordstructures.Vstarec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.dataaccess.CovrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrmatTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* OVERVIEW
*
* VSTANNC - Vesting Calculation.
* This subroutine forms part of the 9405 Annuities Development.
* It will be called from the Calculation Methods table, T6598,
* during the on-line part of the vesting transaction and by the
* pending vesting batch process.
*
* It will calculate the amount available at vesting based on the
* sum assured, plus any bonuses if the component is with-profit.
* It will call another subroutine, from T6598, if a commutation
* percentage is input during the vesting transaction.
*
* The amounts available at vesting will be held on the vesting
* details file, VSTD as follows:
*
*      Field type          Value description
*      ==========          =================
*           S              Sum Assured
*           B              Reversionary Bonus
*           T              Terminal Bonus
*           X              Additional Bonus
*           M              Interim Bonus
*
* --------------------------------------------------
*
*
*****************************************************************
* </pre>
*/
public class Vstannc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VSTANNC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSuminsTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAcblBalTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIntBonusTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTrmBonusTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaExtBonusTot = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaIntbonus = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrmbonus = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaExtbonus = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaIntbonusDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaTrmbonusDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaExtbonusDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaEvstperd = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaLowerDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaUpperDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSubv = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaVestingAllow = "";
	private String wsaaPremiumOk = "";
	private String wsaaStatusOk = "";

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, "1");
	private Validator partPlan = new Validator(wsaaPlanSwitch, "2");
	private Validator summaryPartPlan = new Validator(wsaaPlanSwitch, "3");

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
		/* WSAA-CHDR-KEY */
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaT6639Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaMaturityCalcMeth = new FixedLengthStringData(4).isAPartOf(wsaaT6639Key, 0);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT6639Key, 4);
		/* ERRORS */
	private String h134 = "H134";
	private String g408 = "G408";
	private String h147 = "H147";
	private String f321 = "F321";
	private String h214 = "H214";
	private String f089 = "F089";
	private String f409 = "F409";
		/* FORMATS */
	private String acblrec = "ACBLREC";
	private String covrmatrec = "COVRMATREC";
	private String covrclmrec = "COVRCLMREC";
		/* TABLES */
	private String t5645 = "T5645";
	private String t5679 = "T5679";
	private String t6639 = "T6639";
	private String t6625 = "T6625";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Bonusrec bonusrec = new Bonusrec();
		/*Claim Coverage Record*/
	private CovrclmTableDAM covrclmIO = new CovrclmTableDAM();
		/*Maturity/Expiry logical over COVR*/
	private CovrmatTableDAM covrmatIO = new CovrmatTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T6625rec t6625rec = new T6625rec();
	private T6639rec t6639rec = new T6639rec();
	private Varcom varcom = new Varcom();
	private Vstarec vstarec = new Vstarec();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2009, 
		readNextCovrclm2105, 
		exit2509, 
		exit2609, 
		exit2709, 
		exit3009, 
		exit3509, 
		exit3609, 
		exit3709, 
		errorProg610
	}

	public Vstannc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vstarec.vestingRec = convertAndSetParam(vstarec.vestingRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAIN-ROUTINE*/
		syserrrec.subrname.set(wsaaSubr);
		initialization1000();
		if (wholePlan.isTrue()) {
			processWholePlan2000();
		}
		else {
			processPartPlan3000();
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialization1000()
	{
		init1000();
	}

protected void init1000()
	{
		wsaaChdrcoy.set(vstarec.chdrChdrcoy);
		wsaaChdrnum.set(vstarec.chdrChdrnum);
		wsaaCoverage.set(vstarec.covrCoverage);
		wsaaRider.set(vstarec.covrRider);
		wsaaLife.set(vstarec.lifeLife);
		wsaaPlanSuffix.set(vstarec.planSuffix);
		wsaaBatckey.set(vstarec.batckey);
		wsaaSuminsTot.set(ZERO);
		wsaaAcblBalTot.set(ZERO);
		wsaaIntBonusTot.set(ZERO);
		wsaaTrmBonusTot.set(ZERO);
		wsaaExtBonusTot.set(ZERO);
		vstarec.status.set(varcom.oK);
		vstarec.actvalTotal.set(0);
		for (wsaaSubv.set(1); !(isGT(wsaaSubv,6)); wsaaSubv.add(1)){
			vstarec.actualVal[wsaaSubv.toInt()].set(ZERO);
			vstarec.type[wsaaSubv.toInt()].set(SPACES);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaChdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(f321);
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaPlanSwitch.set(vstarec.planSwitch);
		if (summaryPartPlan.isTrue()) {
			wsaaPlanSuffix.set(ZERO);
			vstarec.planSuffix.set(ZERO);
		}
	}

protected void processWholePlan2000()
	{
		try {
			processCalculations2001();
			setCalculations2006();
		}
		catch (GOTOException e){
		}
	}

protected void processCalculations2001()
	{
		covrclmIO.setDataArea(SPACES);
		covrclmIO.setChdrcoy(wsaaChdrcoy);
		covrclmIO.setChdrnum(wsaaChdrnum);
		covrclmIO.setLife(wsaaLife);
		covrclmIO.setCoverage(wsaaCoverage);
		covrclmIO.setRider(wsaaRider);
		covrclmIO.setFormat(covrclmrec);
		covrclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrclmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(),varcom.oK)
		&& isNE(covrclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			fatalError600();
		}
		if (isNE(covrclmIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(covrclmIO.getChdrnum(),wsaaChdrnum)
		|| isNE(covrclmIO.getLife(),wsaaLife)
		|| isNE(covrclmIO.getCoverage(),wsaaCoverage)
		|| isNE(covrclmIO.getRider(),wsaaRider)
		|| isEQ(covrclmIO.getStatuz(),varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
			vstarec.status.set(h214);
			goTo(GotoLabel.exit2009);
		}
		readTableT66395000();
		wsaaCrtable.set(covrclmIO.getCrtable());
		readTableT66256000();
		while ( !(isEQ(covrclmIO.getStatuz(),varcom.endp)
		|| isNE(covrclmIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(covrclmIO.getChdrnum(),wsaaChdrnum)
		|| isNE(covrclmIO.getLife(),wsaaLife)
		|| isNE(covrclmIO.getCoverage(),wsaaCoverage)
		|| isNE(covrclmIO.getRider(),wsaaRider))) {
			calcSumBonuses2100();
		}
		
	}

protected void setCalculations2006()
	{
		setValTotals2200();
		for (wsaaSubv.set(1); !(isGT(wsaaSubv,6)); wsaaSubv.add(1)){
			vstarec.actvalTotal.add(vstarec.actualVal[wsaaSubv.toInt()]);
		}
	}

protected void calcSumBonuses2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					setDateRange2101();
				}
				case readNextCovrclm2105: {
					readNextCovrclm2105();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setDateRange2101()
	{
		compute(wsaaEvstperd, 0).set(mult(t6625rec.evstperd,-1));
		datcon2rec.intDate1.set(covrclmIO.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaEvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaLowerDate.set(datcon2rec.intDatex2);
		datcon2rec.intDate1.set(covrclmIO.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6625rec.lvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaUpperDate.set(datcon2rec.intDatex2);
		if (isNE(covrclmIO.getValidflag(),"1")
		|| isLT(vstarec.effdate,wsaaLowerDate)
		|| isGT(vstarec.effdate,wsaaUpperDate)) {
			goTo(GotoLabel.readNextCovrclm2105);
		}
		wsaaVestingAllow = "N";
		wsaaPremiumOk = "N";
		wsaaStatusOk = "N";
		for (wsaaSub.set(1); !(isEQ(wsaaVestingAllow,"Y")
		|| isGT(wsaaSub,12)); wsaaSub.add(1)){
			checkStatus2150();
		}
		if (isEQ(wsaaVestingAllow,"N")) {
			goTo(GotoLabel.readNextCovrclm2105);
		}
		vstarec.status.set(varcom.oK);
		wsaaSuminsTot.add(covrclmIO.getSumins());
		getRbAmt2300();
		initializeBonsPara2400();
		if (isNE(t6625rec.revBonusMeth,SPACES)) {
			getInterimBonus2500();
			getTerminalBonus2600();
			getExtraBonus2700();
		}
	}

protected void readNextCovrclm2105()
	{
		covrclmIO.setFormat(covrclmrec);
		covrclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(),varcom.oK)
		&& isNE(covrclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkStatus2150()
	{
		/*CHECK-START*/
		if (isEQ(covrclmIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
			wsaaStatusOk = "Y";
		}
		if (isEQ(covrclmIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaPremiumOk = "Y";
		}
		if (isEQ(wsaaStatusOk,"Y")
		&& isEQ(wsaaPremiumOk,"Y")) {
			wsaaVestingAllow = "Y";
		}
		/*EXIT*/
	}

protected void setValTotals2200()
	{
		setValues2201();
	}

protected void setValues2201()
	{
		wsaaSubv.set(1);
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaSuminsTot);
		vstarec.type[wsaaSubv.toInt()].set("S");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaAcblBalTot);
		vstarec.type[wsaaSubv.toInt()].set("B");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaTrmBonusTot);
		vstarec.type[wsaaSubv.toInt()].set("T");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaExtBonusTot);
		vstarec.type[wsaaSubv.toInt()].set("X");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaIntBonusTot);
		vstarec.type[wsaaSubv.toInt()].set("M");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
	}

protected void getRbAmt2300()
	{
		readT56452301();
	}

protected void readT56452301()
	{
		readTableT56457000();
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(vstarec.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(vstarec.chdrChdrnum);
		wsaaAcblLife.set(vstarec.lifeLife);
		wsaaAcblCoverage.set(vstarec.covrCoverage);
		wsaaAcblRider.set(vstarec.covrRider);
		wsaaPlan.set(covrclmIO.getPlanSuffix());
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(vstarec.currcode);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isNE(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaAcblBalTot.add(acblIO.getSacscurbal());
		}
	}

protected void initializeBonsPara2400()
	{
		setUpPara2401();
	}

protected void setUpPara2401()
	{
		bonusrec.bonusRec.set(SPACES);
		bonusrec.rvBonusSa.set(ZERO);
		bonusrec.trmBonusSa.set(ZERO);
		bonusrec.intBonusSa.set(ZERO);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.rvBonusBon.set(ZERO);
		bonusrec.trmBonusBon.set(ZERO);
		bonusrec.intBonusBon.set(ZERO);
		bonusrec.rvBalLy.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		bonusrec.batckey.set(vstarec.batckey);
		bonusrec.transcd.set(wsaaBatckey.batcBatctrcde);
		bonusrec.effectiveDate.set(vstarec.effdate);
		if (isEQ(covrclmIO.getUnitStatementDate(),varcom.vrcmMaxDate)) {
			bonusrec.unitStmtDate.set(covrclmIO.getCrrcd());
		}
		else {
			bonusrec.unitStmtDate.set(covrclmIO.getUnitStatementDate());
		}
		bonusrec.language.set(vstarec.language);
		bonusrec.tranno.set(covrclmIO.getTranno());
		bonusrec.cntcurr.set(vstarec.chdrCurr);
		bonusrec.cnttype.set(vstarec.chdrType);
		bonusrec.chdrChdrcoy.set(covrclmIO.getChdrcoy());
		bonusrec.chdrChdrnum.set(covrclmIO.getChdrnum());
		bonusrec.lifeLife.set(covrclmIO.getLife());
		bonusrec.covrCoverage.set(covrclmIO.getCoverage());
		bonusrec.covrRider.set(covrclmIO.getRider());
		bonusrec.plnsfx.set(covrclmIO.getPlanSuffix());
		bonusrec.premStatus.set(covrclmIO.getPstatcode());
		bonusrec.crtable.set(covrclmIO.getCrtable());
		bonusrec.sumin.set(covrclmIO.getSumins());
		bonusrec.allocMethod.set("C");
		bonusrec.bonusCalcMeth.set(t6625rec.revBonusMeth);
		bonusrec.user.set(covrclmIO.getUser());
		bonusrec.bonusPeriod.set(1);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(vstarec.crrcd);
		datcon3rec.intDate2.set(vstarec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		bonusrec.termInForce.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(vstarec.crrcd);
		datcon3rec.intDate2.set(covrclmIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		bonusrec.duration.set(datcon3rec.freqFactor);
		bonusrec.statuz.set(varcom.oK);
	}

protected void getInterimBonus2500()
	{
		try {
			callIntbonusRoutine2501();
		}
		catch (GOTOException e){
		}
	}

protected void callIntbonusRoutine2501()
	{
		wsaaIntbonus.set(t6639rec.intBonusProg);
		if (isEQ(t6639rec.intBonusProg,SPACES)) {
			goTo(GotoLabel.exit2509);
		}
		callProgram(t6639rec.intBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaIntbonus);
			fatalError600();
		}
		compute(wsaaIntBonusTot, 2).add(add(bonusrec.intBonusSa,bonusrec.intBonusBon));
	}

protected void getTerminalBonus2600()
	{
		try {
			callTrmbonusRoutine2601();
		}
		catch (GOTOException e){
		}
	}

protected void callTrmbonusRoutine2601()
	{
		wsaaTrmbonus.set(t6639rec.trmBonusProg);
		if (isEQ(t6639rec.trmBonusProg,SPACES)) {
			goTo(GotoLabel.exit2609);
		}
		callProgram(t6639rec.trmBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaTrmbonus);
			fatalError600();
		}
		compute(wsaaTrmBonusTot, 2).add(add(bonusrec.trmBonusSa,bonusrec.trmBonusBon));
	}

protected void getExtraBonus2700()
	{
		try {
			callAddbonusRoutine2701();
		}
		catch (GOTOException e){
		}
	}

protected void callAddbonusRoutine2701()
	{
		wsaaExtbonus.set(t6639rec.addBonusProg);
		if (isEQ(t6639rec.addBonusProg,SPACES)) {
			goTo(GotoLabel.exit2709);
		}
		callProgram(t6639rec.addBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaExtbonus);
			fatalError600();
		}
		compute(wsaaExtBonusTot, 2).add(add(bonusrec.extBonusSa,bonusrec.extBonusBon));
	}

protected void processPartPlan3000()
	{
		try {
			processCalculations3001();
		}
		catch (GOTOException e){
		}
	}

protected void processCalculations3001()
	{
		covrmatIO.setDataArea(SPACES);
		covrmatIO.setChdrcoy(wsaaChdrcoy);
		covrmatIO.setChdrnum(wsaaChdrnum);
		covrmatIO.setPlanSuffix(wsaaPlanSuffix);
		covrmatIO.setLife(wsaaLife);
		covrmatIO.setCoverage(wsaaCoverage);
		covrmatIO.setRider(wsaaRider);
		covrmatIO.setFormat(covrmatrec);
		covrmatIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmatIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)
		&& isNE(covrmatIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
		if (isNE(covrmatIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(covrmatIO.getChdrnum(),wsaaChdrnum)
		|| isNE(covrmatIO.getPlanSuffix(),wsaaPlanSuffix)
		|| isNE(covrmatIO.getLife(),wsaaLife)
		|| isNE(covrmatIO.getCoverage(),wsaaCoverage)
		|| isNE(covrmatIO.getRider(),wsaaRider)
		|| isEQ(covrmatIO.getStatuz(),varcom.endp)) {
			covrmatIO.setStatuz(varcom.endp);
			vstarec.status.set(h214);
			goTo(GotoLabel.exit3009);
		}
		readTableT66395000();
		wsaaCrtable.set(covrmatIO.getCrtable());
		readTableT66256000();
		compute(wsaaEvstperd, 0).set(mult(t6625rec.evstperd,-1));
		datcon2rec.intDate1.set(covrmatIO.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaEvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaLowerDate.set(datcon2rec.intDatex2);
		datcon2rec.intDate1.set(covrmatIO.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6625rec.lvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaUpperDate.set(datcon2rec.intDatex2);
		if (isNE(covrmatIO.getValidflag(),"1")
		|| isLT(vstarec.effdate,wsaaLowerDate)
		|| isGT(vstarec.effdate,wsaaUpperDate)) {
			covrmatIO.setStatuz(varcom.endp);
			vstarec.status.set(f089);
			goTo(GotoLabel.exit3009);
		}
		wsaaVestingAllow = "N";
		wsaaPremiumOk = "N";
		wsaaStatusOk = "N";
		for (wsaaSub.set(1); !(isEQ(wsaaVestingAllow,"Y")
		|| isGT(wsaaSub,12)); wsaaSub.add(1)){
			checkStatus3050();
		}
		if (isEQ(wsaaVestingAllow,"N")) {
			vstarec.status.set(f409);
			goTo(GotoLabel.exit3009);
		}
		else {
			vstarec.status.set(varcom.oK);
		}
		calcSumBonuses3100();
		setValTotals3200();
		for (wsaaSubv.set(1); !(isGT(wsaaSubv,6)); wsaaSubv.add(1)){
			vstarec.actvalTotal.add(vstarec.actualVal[wsaaSubv.toInt()]);
		}
	}

protected void checkStatus3050()
	{
		/*CHECK-START*/
		if (isEQ(covrmatIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
			wsaaStatusOk = "Y";
		}
		if (isEQ(covrmatIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaPremiumOk = "Y";
		}
		if (isEQ(wsaaStatusOk,"Y")
		&& isEQ(wsaaPremiumOk,"Y")) {
			wsaaVestingAllow = "Y";
		}
		/*EXIT*/
	}

protected void calcSumBonuses3100()
	{
		/*CALCULATIONS*/
		wsaaSuminsTot.add(covrmatIO.getSumins());
		getRbAmt3300();
		initializeBonsPara3400();
		if (isNE(t6625rec.revBonusMeth,SPACES)) {
			getInterimBonus3500();
			getTerminalBonus3600();
			getExtraBonus3700();
		}
		/*EXIT*/
	}

protected void setValTotals3200()
	{
		setValues3201();
	}

protected void setValues3201()
	{
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaSuminsTot,ZERO)) {
				compute(wsaaSuminsTot, 3).setRounded(div(wsaaSuminsTot,vstarec.polsum));
			}
		}
		wsaaSubv.set(1);
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaSuminsTot);
		vstarec.type[wsaaSubv.toInt()].set("S");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaAcblBalTot,ZERO)) {
				compute(wsaaAcblBalTot, 3).setRounded(div(wsaaAcblBalTot,vstarec.polsum));
			}
		}
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaAcblBalTot);
		vstarec.type[wsaaSubv.toInt()].set("B");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaTrmBonusTot,ZERO)) {
				compute(wsaaTrmBonusTot, 3).setRounded(div(wsaaTrmBonusTot,vstarec.polsum));
			}
		}
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaTrmBonusTot);
		vstarec.type[wsaaSubv.toInt()].set("T");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaExtBonusTot,ZERO)) {
				compute(wsaaExtBonusTot, 3).setRounded(div(wsaaExtBonusTot,vstarec.polsum));
			}
		}
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaExtBonusTot);
		vstarec.type[wsaaSubv.toInt()].set("X");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaIntBonusTot,ZERO)) {
				compute(wsaaIntBonusTot, 3).setRounded(div(wsaaIntBonusTot,vstarec.polsum));
			}
		}
		vstarec.actualVal[wsaaSubv.toInt()].set(wsaaIntBonusTot);
		vstarec.type[wsaaSubv.toInt()].set("M");
		vstarec.status.set(varcom.oK);
		wsaaSubv.add(1);
	}

protected void getRbAmt3300()
	{
		start3301();
	}

protected void start3301()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vstarec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(vstarec.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(vstarec.chdrChdrnum);
		wsaaAcblLife.set(vstarec.lifeLife);
		wsaaAcblCoverage.set(vstarec.covrCoverage);
		wsaaAcblRider.set(vstarec.covrRider);
		wsaaPlan.set(vstarec.planSuffix);
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(vstarec.currcode);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isNE(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaAcblBalTot.add(acblIO.getSacscurbal());
		}
	}

protected void initializeBonsPara3400()
	{
		setUpPara3401();
	}

protected void setUpPara3401()
	{
		wsaaBatckey.set(vstarec.batckey);
		bonusrec.bonusRec.set(SPACES);
		bonusrec.rvBonusSa.set(ZERO);
		bonusrec.trmBonusSa.set(ZERO);
		bonusrec.intBonusSa.set(ZERO);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.rvBonusBon.set(ZERO);
		bonusrec.trmBonusBon.set(ZERO);
		bonusrec.intBonusBon.set(ZERO);
		bonusrec.rvBalLy.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		bonusrec.batckey.set(vstarec.batckey);
		bonusrec.transcd.set(wsaaBatckey.batcBatctrcde);
		bonusrec.effectiveDate.set(vstarec.effdate);
		if (isEQ(covrmatIO.getUnitStatementDate(),varcom.vrcmMaxDate)) {
			bonusrec.unitStmtDate.set(covrmatIO.getCrrcd());
		}
		else {
			bonusrec.unitStmtDate.set(covrmatIO.getUnitStatementDate());
		}
		bonusrec.language.set(vstarec.language);
		bonusrec.tranno.set(covrmatIO.getTranno());
		bonusrec.cntcurr.set(vstarec.chdrCurr);
		bonusrec.cnttype.set(vstarec.chdrType);
		bonusrec.chdrChdrcoy.set(covrmatIO.getChdrcoy());
		bonusrec.chdrChdrnum.set(covrmatIO.getChdrnum());
		bonusrec.lifeLife.set(covrmatIO.getLife());
		bonusrec.covrCoverage.set(covrmatIO.getCoverage());
		bonusrec.covrRider.set(covrmatIO.getRider());
		bonusrec.plnsfx.set(covrmatIO.getPlanSuffix());
		bonusrec.premStatus.set(covrmatIO.getPstatcode());
		bonusrec.crtable.set(covrmatIO.getCrtable());
		bonusrec.sumin.set(covrmatIO.getSumins());
		bonusrec.allocMethod.set("C");
		bonusrec.bonusCalcMeth.set(t6625rec.revBonusMeth);
		bonusrec.user.set(covrmatIO.getUser());
		bonusrec.bonusPeriod.set(1);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(vstarec.crrcd);
		datcon3rec.intDate2.set(vstarec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		bonusrec.termInForce.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(vstarec.crrcd);
		datcon3rec.intDate2.set(covrmatIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		bonusrec.duration.set(datcon3rec.freqFactor);
	}

protected void getInterimBonus3500()
	{
		try {
			callIntbonusRoutine3501();
		}
		catch (GOTOException e){
		}
	}

protected void callIntbonusRoutine3501()
	{
		wsaaIntbonus.set(t6639rec.intBonusProg);
		if (isEQ(t6639rec.intBonusProg,SPACES)) {
			goTo(GotoLabel.exit3509);
		}
		callProgram(t6639rec.intBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaIntbonus);
			fatalError600();
		}
		compute(wsaaIntBonusTot, 2).add(add(bonusrec.intBonusSa,bonusrec.intBonusBon));
	}

protected void getTerminalBonus3600()
	{
		try {
			callTrmbonusRoutine3601();
		}
		catch (GOTOException e){
		}
	}

protected void callTrmbonusRoutine3601()
	{
		wsaaTrmbonus.set(t6639rec.trmBonusProg);
		if (isEQ(t6639rec.trmBonusProg,SPACES)) {
			goTo(GotoLabel.exit3609);
		}
		callProgram(t6639rec.trmBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaTrmbonus);
			fatalError600();
		}
		compute(wsaaTrmBonusTot, 2).add(add(bonusrec.trmBonusSa,bonusrec.trmBonusBon));
	}

protected void getExtraBonus3700()
	{
		try {
			callAddbonusRoutine3701();
		}
		catch (GOTOException e){
		}
	}

protected void callAddbonusRoutine3701()
	{
		wsaaExtbonus.set(t6639rec.addBonusProg);
		if (isEQ(t6639rec.addBonusProg,SPACES)) {
			goTo(GotoLabel.exit3709);
		}
		callProgram(t6639rec.addBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaExtbonus);
			fatalError600();
		}
		compute(wsaaExtBonusTot, 2).add(add(bonusrec.extBonusSa,bonusrec.extBonusBon));
	}

protected void readTableT66395000()
	{
		readT66395000();
	}

protected void readT66395000()
	{
		itdmIO.setDataKey(SPACES);
		wsaaMaturityCalcMeth.set(vstarec.matCalcMeth);
		wsaaPstatcode.set(vstarec.pstatcode);
		itdmIO.setItemcoy(vstarec.chdrChdrcoy);
		itdmIO.setItemtabl(t6639);
		itdmIO.setItemitem(wsaaT6639Key);
		itdmIO.setItmfrm(vstarec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),vstarec.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6639)
		|| isNE(itdmIO.getItemitem(),wsaaT6639Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT6639Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g408);
			fatalError600();
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
	}

protected void readTableT66256000()
	{
		readT66256000();
	}

protected void readT66256000()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(vstarec.chdrChdrcoy);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(vstarec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),vstarec.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(),wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaCrtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h147);
			fatalError600();
		}
		t6625rec.t6625Rec.set(itdmIO.getGenarea());
	}

protected void readTableT56457000()
	{
		readT56457000();
	}

protected void readT56457000()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vstarec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		vstarec.status.set(varcom.bomb);
		exitProgram();
	}
}
