package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnypaypfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.life.annuities.procedures.Wthldng;
import com.csc.life.annuities.recordstructures.Wthldngrec;
import com.csc.life.annuities.screens.Sjl46ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl46 extends ScreenProgCS {	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl46");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl46.class);
	private static final String F917 = "F917";	
	private static final String SJL46 = "Sjl46";	
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl46ScreenVars sv = ScreenProgram.getScreenVars(Sjl46ScreenVars.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Chdrpf chdrpf = new Chdrpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected Clntpf clntpf;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	protected ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private AnnyregpfDAO annyregpfDAO = getApplicationContext().getBean("annyregpfDAO" , AnnyregpfDAO.class);
	private AnnypaypfDAO annypaypfDAO = getApplicationContext().getBean("annypaypfDAO" , AnnypaypfDAO.class);
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);	
	private static final String T5688 = "T5688";	
	private static final String TJL48 = "TJL48";			
	private List<Clntpf> clntpfList = null;	
	private Batckey wsaaBatckey = new Batckey();	
	private FixedLengthStringData wsaaTasj = new FixedLengthStringData(4).init("TASJ");
	private FixedLengthStringData wsaaTasl = new FixedLengthStringData(4).init("TASL");
	private FixedLengthStringData wsaaTask = new FixedLengthStringData(4).init("TASK");
	private Annyregpf annyregpf = new Annyregpf();	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		EXIT2090,
		EXIT2190,
		EXIT2219,
		READNEXTRECORD1240,				
    }
	Integer index;
	int pageSize;
	int startIndex;
	int endIndex;
	
	private Wthldngrec wthldngrec = new Wthldngrec();
	
	
	public Pjl46() {
		super();
		screenVars = sv;
		new ScreenModel(SJL46, AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	@Override
	protected void initialise1000() {
		initialise1010();				
	}
	
	
	private void initialise1010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);			
		String wsaaItemlang = wsspcomn.language.toString();
		sv.dataArea.set(SPACES);
		wthldngrec.expenseratio.set(0);
		wthldngrec.anualannamt.set(0);
		wthldngrec.annothrincome.set(0);
		wthldngrec.annreqdexpense.set(0);
		sv.pmntno.set(wsspcomn.selno);
		boolean displaycalculations = true;
		
		Annypaypf annypaypf;
		if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask) ||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
		{		
			annypaypf = annypaypfDAO.getAnnypaypfRecord(wsspcomn.company.toString(), 
					wsspcomn.chdrChdrnum.toString().trim(), sv.pmntno.toInt());
			
		}
		else
		{
			annypaypf = annypaypfDAO.getAnnypaypfRecords(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),
					sv.pmntno.toInt(), wsspcomn.effdate.toInt(), wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
		}
		
		

		if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask) ||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
		{		
			annyregpf = annyregpfDAO.getAnnyregpf(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
			
		}
		else
		{
			annyregpf = annyregpfDAO.getAnnyregpfbyEffdt(wsspcomn.company.toString(),
					wsspcomn.chdrChdrnum.toString().trim(), wsspcomn.effdate.toInt(), wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
		}
		
		if (null != annyregpf.getChdrnum())
			sv.chdrnum.set(annyregpf.getChdrnum());
		retrvContract1020();
					
			loadHeader1030();	
			loadLongDesc();//ILJ-567
			if (null != annyregpf.getTotannufund())
				sv.totannufund.set(annyregpf.getTotannufund());
			
						
			if (null != annyregpf.getPayee()){
				sv.payee.set(annyregpf.getPayee());
				getPayeeName();
			}
				
			Annypaypf annypaypffirstRecord = annypaypfDAO.getAnnypaypfRecord(wsspcomn.company.toString(),
											wsspcomn.chdrChdrnum.toString().trim(), 1);
			Optional<Annyregpf> isExists = Optional.ofNullable(annyregpf);
			if (isExists.isPresent()) {
				wthldngrec.currncy.set(annyregpf.getPmntcurr());//ILJ-569
				wthldngrec.payeetyp.set(clntpf.getClttype());
				wthldngrec.chdrcoy.set(wsspcomn.company.toString().trim());
				wthldngrec.chdrnum.set(annyregpf.getChdrnum());
				wthldngrec.annpmntfreq.set(annyregpf.getAnnpmntfreq());
				wthldngrec.annpmntopt.set(annyregpf.getAnnpmntopt());
				wthldngrec.annupmnttrm.set(annyregpf.getAnnupmnttrm());
				wthldngrec.cownnum.set(annyregpf.getCownnum());
				wthldngrec.paymtdt.set(annypaypf.getPmntdt());
				wthldngrec.payee.set(annyregpf.getPayee());
				wthldngrec.totannufund.set(annyregpf.getTotannufund());
				wthldngrec.cnttype.set(chdrpf.getCnttype());
				if(isEQ(annypaypf.getAnnpmntopt(),"03")){
					wthldngrec.partialpmnt.set(annypaypffirstRecord.getAnnaamt());
					if(isEQ(annypaypf.getPmntno(),1)){
						wthldngrec.annpmntopt.set("00");
					}
				}
				else{
					wthldngrec.partialpmnt.set(0);
				}
				
				callProgram(Wthldng.class, wthldngrec.wthldngRec);
				if(isNE(wthldngrec.statuz,Varcom.oK)){
					syserrrec.statuz.set(wthldngrec.statuz);
					fatalError600();
				}
			
			if(isEQ(annypaypf.getAnnpmntopt(),"03") && isEQ(annypaypf.getPmntno(),1)){
				displaycalculations = false;
				sv.annupmntamt.set(wthldngrec.partialpmnt);
				sv.withholdtaxstus.set("No Withholding Tax");
			}
			else{
				sv.annupmntamt.set(annypaypf.getAnnaamt());//ILJ-567
				sv.withholdtaxstus.set(wthldngrec.wthldtaxstatus);
			}
			if(!displaycalculations){
				wthldngrec.expenseratio.set(0);
				wthldngrec.anualannamt.set(0);
				wthldngrec.annothrincome.set(0);
				wthldngrec.annreqdexpense.set(0);
			}
			if(isEQ(annypaypf.getWitholdtax(),BigDecimal.ZERO)){
				sv.withholdtaxstus.set("No Withholding Tax");
			}
			if(isNE(wthldngrec.expenseratio,0)) {
				sv.reqrexpenseratio.set(wthldngrec.expenseratio.toInt());
			}
			if(isNE(wthldngrec.annreqdexpense,0)) {
				sv.reqrexpense.set(wthldngrec.annreqdexpense); 
			}
			
			if(isNE(wthldngrec.annothrincome,0)) {
				sv.otherincome.set(wthldngrec.annothrincome); 
			}
			
			if(null != annypaypf.getWitholdtax()) {
				sv.wholdtax.set(annypaypf.getWitholdtax());
			}
			
			if(isNE(wthldngrec.wthldngtaxrate.toInt(),0) 
					&& displaycalculations 
					&& isNE(annypaypf.getWitholdtax(),0)) {//ILJ-625 
				sv.whhtr.set(wthldngrec.wthldngtaxrate);
			}
			
			if(isNE(wthldngrec.ttlprmpaid,0)) {
				sv.prmpaid.set(wthldngrec.ttlprmpaid);
			}
			
			if(isNE(wthldngrec.anualannamt,0)) {
				sv.annualannamt.set(wthldngrec.anualannamt);
			}
			
		
			if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask) ||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
			{		
				annypaypf = annypaypfDAO.getAnnypaypfRecord(wsspcomn.company.toString(),
						wsspcomn.chdrChdrnum.toString().trim(), sv.pmntno.toInt());
				
			}
			else
			{
				annypaypf = annypaypfDAO.getAnnypaypfRecords(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),
						sv.pmntno.toInt(), wsspcomn.effdate.toInt(), wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
			}
			
			
			
			
			if(null != annypaypf) {
				if (null != annypaypf.getPmntstat()){
					String pmntStatus = iafDescDAO.getItemLongDesc("IT", wsspcomn.company.toString(),
							TJL48, annypaypf.getPmntstat().trim(), wsaaItemlang);
					sv.annpmntstatus.set(pmntStatus);	
				}else{
					sv.annpmntstatus.set(SPACES);
				}
				
				if(null != annypaypf.getPmntdt()) {
					sv.pmntdate.set(annypaypf.getPmntdt());
				}
				
				if (null != annypaypf.getAnnupmnttrm() && displaycalculations ) {
					sv.annupmnttrm.set(annypaypf.getAnnupmnttrm());
				}
				else{
					sv.annupmnttrm.set(SPACES);//ILJ-624
				}
				if (null != annypaypf.getAnnpmntfreq() && displaycalculations && isNE(annypaypf.getAnnpmntopt(),"05")) {
					sv.annpmntfreq.set(annypaypf.getAnnpmntfreq());
				}
				
				if(null != annypaypf.getAnnintamt()) {
					sv.annuitywint.set(annypaypf.getAnnintamt()); 
				}
				
				if (null != annypaypf.getPaidamt())
					sv.paidamt.set(annypaypf.getPaidamt());	
				
			}
			sv.annpmntfreqOut[Varcom.pr.toInt()].set("Y");	
			
			
		}else {
			sv.chdrnumErr.set(F917);
		}
	}
	
	
	private void getPayeeName(){
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.payee.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || (clntpfList.isEmpty()))
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		sv.payeename.set(wsspcomn.longconfname);
	}
	protected void retrvContract1020()
	{	
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(Varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),Varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}	
	}
	
	protected void loadHeader1030()
	{
		try {
			headings1031();					
		}
		catch (GOTOException e){
			LOGGER.info("loadHeader1030:", e);
		}
	}	
	
	protected void headings1031()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());		
		sv.cownnum.set(chdrpf.getCownnum());
		readClientRecord(chdrpf.getCownnum());
		if(clntpf != null){
			sv.ownername.set(clntpf.getSurname().trim().concat(", ").concat(clntpf.getLgivname()));
		}
		readLifeRecord();					
	}	
	
	protected void readClientRecord(String clntnum){
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), clntnum.trim());		
	}
	protected void readLifeRecord(){
		Lifepf lifepfModel = null;
		List<Lifepf> lifeList = lifepfDAO.getLifeList(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(lifeList == null){
			return;
		}else{
			lifepfModel=lifeList.get(0);
		}
		sv.lifenum.set(lifepfModel.getLifcnum());
		clntpf.setClntnum(lifepfModel.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpfList= clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || (clntpfList.isEmpty()))
		{
			return;
		}
		for(Clntpf clntpf1 :clntpfList)
		{
			plainname(clntpf1);
		}
		sv.lifename.set(wsspcomn.longconfname);		
	}
	
	protected void loadLongDesc(){
		Map<String, String> itemMap = new HashMap<>();		
		itemMap.put(T5688, chdrpf.getCnttype());
		Map<String, Descpf> descMap = descdao.searchMultiDescpf
				(wsspcomn.company.toString(),wsspcomn.language.toString(), itemMap);
		if (!descMap.containsKey(T5688)) {			
			sv.ctypedes.set(SPACES);
		} else {
			sv.ctypedes.set(descMap.get(T5688).getLongdesc());
		}
	}		
		
	protected void plainname(Clntpf clntpf)
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return ;
		}		
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
	
	protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}	
	
	@Override
	protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Output the screen.                                              */
		/*PRE-EXIT*/
	}	
	@Override
	protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
			LOGGER.info("screenEdit2000:", e);
			/* Expected exception for control flow purposes. */
		}
	}
	
	protected void screenIo2010()
	{		                                  
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(Varcom.oK);
		}
		/*ROLL-UP
		 Roll up processing - fills next page of the subfile.*/
		if (isEQ(scrnparams.statuz, Varcom.rolu)) {					
            
			
			wsspcomn.edterror.set(SPACES);
			                                         
			goTo(GotoLabel.EXIT2090);
		}
	}
	
	@Override
	protected void update3000() {
		//**  No updating of database takes place.*//*	
	}	
	@Override			
	protected void whereNext4000() {
		wsspcomn.nextprog.set(wsaaProg); 
		wsspcomn.programPtr.add(1);
	}	
		
}