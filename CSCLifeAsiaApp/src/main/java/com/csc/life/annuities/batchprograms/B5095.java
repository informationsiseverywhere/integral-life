/*
 * File: B5095.java
 * Date: December 5, 2013 9:06:35 AM ICT
 * Author: CSC
 * 
 * Class transformed from B5095.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.PayrpfTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.csc.fsuframework.core.FeaConfg;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;

/**
* <pre>
*
*REMARKS.
*   This is a skeleton for a batch mainline program.
*
*   The basic procedure division logic is for reading via SQL and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*    Read first primary file record
*
*    Write details to report while not primary file EOF
*     - look up referred to records for output details
*     - if new page, write headings
*     - write details
*     - read next primary file record
*
*   Control totals:
*     01  -  Number of pages printed
*
*
*   These remarks must be replaced by what the program actually
*     does.
*
*****************************************************************
* </pre>
*/
public class B5095 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private BaseData[] sqlparams = null;
	private BaseData[] sqlnullind = null;
	private BaseData[] sqlinto = null;
	private String sqlstmt = "";
	private java.sql.ResultSet sqlchdrpf1rs = null;
	private java.sql.PreparedStatement sqlchdrpf1ps = null;
	private java.sql.Connection sqlchdrpf1conn = null;
	private String sqlchdrpf1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5095");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLastBillchnl = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLastCnttype = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaBlankMethod = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBillchnl = new FixedLengthStringData(1).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 1);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData tranid = new FixedLengthStringData(14);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrpfTableDAM sqlaPayrf = new PayrpfTableDAM();
	private T6654rec t6654rec = new T6654rec();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	private static final String t6654 = "T6654";

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190, 
		eof580, 
		exit590
	}

	public B5095() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void main110()
	{
		initialise200();
		readFirstRecord300();
		while ( !(endOfFile.isTrue())) {
			printReport400();
		}
		
		finished900();
		goTo(GotoLabel.exit190);
	}

protected void sqlError189()
	{
		sqlError();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		wsaaCompany.set(runparmrec.company);
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		/*  Save any pension details already set up*/
		sqlerrorflag = false;
		try {
			sqlstmt = " INSERT INTO   " + getAppVars().getTableNameOverriden("PAYRQT") + " " +
"(CHDRNUM, TAXRELMTH, INCSEQNO)" +
" ( SELECT  CHDRNUM, TAXRELMTH, INCSEQNO" +
" FROM   " + getAppVars().getTableNameOverriden("PAYRPF") + " " +
" WHERE (TAXRELMTH <> ?" +
" OR INCSEQNO <> 0)" +
" AND VALIDFLAG = '1'" +
" AND CHDRCOY = ?)";
			sqlparams = new BaseData[] {wsaaBlankMethod, wsaaCompany};
			getAppVars().executeSimpleUpdate(sqlstmt, sqlparams, getAppVars().isImmediateCommit(), new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.fsu.general.dataaccess.PayrpfTableDAM()});
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError189();
			throw new RuntimeException("B5095: ERROR (1) - Unexpected return from a transformed GO TO statement: 189-SQL-ERROR");
		}
		/*  Record number of records saved in control total no 1.*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(getAppVars().getNumberOfRowsProcessed());
		callContot001();
		/*  Clear existing records for company*/
		sqlerrorflag = false;
		try {
			sqlstmt = " DELETE FROM  " + getAppVars().getTableNameOverriden("PAYRPF") + " " +
" WHERE CHDRCOY = ?";
			sqlparams = new BaseData[] {wsaaCompany};
			getAppVars().executeSimpleUpdate(sqlstmt, sqlparams, getAppVars().isImmediateCommit(), new com.csc.fsu.general.dataaccess.PayrpfTableDAM());
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError189();
			throw new RuntimeException("B5095: ERROR (2) - Unexpected return from a transformed GO TO statement: 189-SQL-ERROR");
		}
		/*  Record number of records deleted in control total no 2.*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(getAppVars().getNumberOfRowsProcessed());
		callContot001();
		/* Set up fileds for table read*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t6654);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
	}

protected void readFirstRecord300()
	{
		/*BEGIN-READING*/
		/*  Define the query required by declaring a cursor*/
		sqlchdrpf1 = " SELECT  CHDRCOY, CHDRNUM, 1, CURRFROM, VALIDFLAG, BILLCHNL, BILLFREQ, BILLCD, BTDATE, BTDATE, PTDATE, BILLCURR, CNTCURR, SINSTAMT01, SINSTAMT02, SINSTAMT03, SINSTAMT04, SINSTAMT05, SINSTAMT06, " + wsaaBlankMethod + ", BILLSUPR, BILLSPFROM, BILLSPTO, MANDREF, GRUPKEY, 0, NOTSSUPR, NOTSSPFROM, NOTSSPTO, APLSUPR, APLSPFROM, APLSPTO, PSTCDE, " + wsaaTermid + ", 0, 0, 0, CHDRPFX, CNTTYPE, TRANID" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE CHDRPFX = 'CH'" +
" AND CHDRCOY = ?" +
" AND SERVUNIT = 'LP'" +
" ORDER BY CHDRPFX, CHDRCOY, CHDRNUM";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlchdrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrpf1ps = getAppVars().prepareStatementEmbeded(sqlchdrpf1conn, sqlchdrpf1, "CHDRPF");
			getAppVars().setDBString(sqlchdrpf1ps, 1, wsaaCompany);
			sqlchdrpf1rs = getAppVars().executeQuery(sqlchdrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError189();
			throw new RuntimeException("B5095: ERROR (3) - Unexpected return from a transformed GO TO statement: 189-SQL-ERROR");
		}
		/*   Fetch first record*/
		fetchPrimary500();
		/*EXIT*/
	}

protected void printReport400()
	{
		readReferredToRecords410();
		writeDetail450();
		readNextRecord460();
	}

protected void readReferredToRecords410()
	{
		/* Retrieve saved pension details (if they exist)*/
		sqlerrorflag = false;
		try {
			sqlstmt = " SELECT  TAXRELMTH, INCSEQNO" +
" FROM   " + getAppVars().getTableNameOverriden("PAYRQT") + " " +
" WHERE CHDRNUM = ?";
			sqlinto = new BaseData[] { sqlaPayrf.taxrelmth, sqlaPayrf.incomeSeqNo};
			sqlparams = new BaseData[] {sqlaPayrf.chdrnum};
			getAppVars().executeSingleRowQuery(sqlstmt, sqlparams, sqlinto);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError189();
			throw new RuntimeException("B5095: ERROR (4) - Unexpected return from a transformed GO TO statement: 189-SQL-ERROR");
		}
		/*  Read billing control table entry, if details change*/
		if (isNE(wsaaLastBillchnl, sqlaPayrf.billchnl)
		|| isNE(wsaaLastCnttype, cnttype)) {
			if(BTPRO028Permission) {
				String key= sqlaPayrf.billchnl.toString().trim()+cnttype.toString().trim()+sqlaPayrf.billfreq.toString().trim();
				if(!readT6654(key)) {
					key= sqlaPayrf.billchnl.toString().trim().concat(cnttype.toString().trim()).concat("**");
					if(!readT6654(key)) {
						key= sqlaPayrf.billchnl.toString().trim().concat("*****");
						if(!readT6654(key)) {
							conerrrec.params.set(itemIO.getParams());
							databaseError006();
						}
					}
				}
			}
			else {
			wsaaCnttype.set(cnttype);
			wsaaLastCnttype.set(cnttype);
			wsaaBillchnl.set(sqlaPayrf.billchnl);
			wsaaLastBillchnl.set(sqlaPayrf.billchnl);
			itemIO.setItemitem(wsaaItemitem);
			readT6654600();
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				wsaaCnttype.set("***");
				itemIO.setItemitem(wsaaItemitem);
				readT6654600();
				if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
					conerrrec.params.set(itemIO.getParams());
					databaseError006();
				}
			}
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		}
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(runparmrec.company.toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void writeDetail450()
	{
		/* Calculate next billing date*/
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(sqlaPayrf.btdate);
		compute(datcon2rec.freqFactor, 0).set(sub(0, t6654rec.leadDays));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		sqlaPayrf.nextdate.set(datcon2rec.intDate2);
		/*  Set up "record update" stamp*/
		varcom.vrcmCompTranid.set(tranid);
		sqlaPayrf.termid.set(varcom.vrcmCompTermid);
		if (isEQ(varcom.vrcmCompTranidN, NUMERIC)) {
			varcom.vrcmTranidN.set(varcom.vrcmCompTranidN);
			sqlaPayrf.transactionDate.set(varcom.vrcmDate);
			sqlaPayrf.transactionTime.set(varcom.vrcmTime);
			sqlaPayrf.user.set(varcom.vrcmUser);
		}
		else {
			varcom.vrcmTranidN.set(0);
			sqlaPayrf.transactionDate.set(0);
			sqlaPayrf.transactionTime.set(0);
			sqlaPayrf.user.set(0);
		}
		/*  Write payer record*/
		sqlerrorflag = false;
		try {
			sqlstmt = " INSERT INTO   " + getAppVars().getTableNameOverriden("PAYRPF") + " " +
" VALUES ('" + sqlaPayrf.payrrec + "')";
			sqlparams = null;
			getAppVars().executeSimpleUpdate(sqlstmt, sqlparams, getAppVars().isImmediateCommit(), new com.csc.fsu.general.dataaccess.PayrpfTableDAM());
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError189();
			throw new RuntimeException("B5095: ERROR (5) - Unexpected return from a transformed GO TO statement: 189-SQL-ERROR");
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void readNextRecord460()
	{
		fetchPrimary500();
		/*EXIT*/
	}

protected void fetchPrimary500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fetchRecord510();
				case eof580: 
					eof580();
				case exit590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord510()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlchdrpf1rs)) {
				getAppVars().getDBObject(sqlchdrpf1rs, 1, sqlaPayrf.payrrec);
				getAppVars().getDBObject(sqlchdrpf1rs, 2, cnttype);
				getAppVars().getDBObject(sqlchdrpf1rs, 3, tranid);
			}
			else {
				goTo(GotoLabel.eof580);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError189();
			throw new RuntimeException("B5095: ERROR (6) - Unexpected return from a transformed GO TO statement: 189-SQL-ERROR");
		}
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit590);
	}

protected void eof580()
	{
		wsaaEof.set("Y");
	}

protected void readT6654600()
	{
		/*CALL-ITEMIO*/
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void finished900()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlchdrpf1conn, sqlchdrpf1ps, sqlchdrpf1rs);
		/*EXIT*/
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
}
