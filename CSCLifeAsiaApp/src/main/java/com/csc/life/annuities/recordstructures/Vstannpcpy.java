package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:09
 * Description:
 * Copybook name: VSTANNPCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vstannpcpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(199);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rec, 1);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rec, 9);
  	public PackedDecimalData planSuffix = new PackedDecimalData(5, 0).isAPartOf(rec, 11);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rec, 14);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rec, 16);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rec, 18);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(rec, 22);
  	public ZonedDecimalData elvstfct = new ZonedDecimalData(6, 5).isAPartOf(rec, 27);
  	public PackedDecimalData origpay = new PackedDecimalData(17, 2).isAPartOf(rec, 33);
  	public PackedDecimalData vstpaya = new PackedDecimalData(17, 2).isAPartOf(rec, 42);
  	public FixedLengthStringData[] type = FLSArrayPartOfStructure(6, 1, rec, 51);
  	public PackedDecimalData[] amnt = PDArrayPartOfStructure(6, 17, 2, rec, 57);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rec, 111);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rec, 114);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(rec, 115);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rec, 179);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(rec, 182);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(rec, 186).setUnsigned();
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rec, 192);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rec, 195);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}