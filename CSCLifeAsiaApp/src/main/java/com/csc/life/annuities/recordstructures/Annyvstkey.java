package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:17
 * Description:
 * Copybook name: ANNYVSTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Annyvstkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData annyvstFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData annyvstKey = new FixedLengthStringData(64).isAPartOf(annyvstFileKey, 0, REDEFINE);
  	public FixedLengthStringData annyvstChdrcoy = new FixedLengthStringData(1).isAPartOf(annyvstKey, 0);
  	public FixedLengthStringData annyvstChdrnum = new FixedLengthStringData(8).isAPartOf(annyvstKey, 1);
  	public FixedLengthStringData annyvstLife = new FixedLengthStringData(2).isAPartOf(annyvstKey, 9);
  	public FixedLengthStringData annyvstCoverage = new FixedLengthStringData(2).isAPartOf(annyvstKey, 11);
  	public FixedLengthStringData annyvstRider = new FixedLengthStringData(2).isAPartOf(annyvstKey, 13);
  	public PackedDecimalData annyvstPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(annyvstKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(annyvstKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(annyvstFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		annyvstFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}