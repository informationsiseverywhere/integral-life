package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Tjl45rec extends ExternalData{
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData tjl45Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData mtoi = new ZonedDecimalData(15, 0).isAPartOf(tjl45Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(485).isAPartOf(tjl45Rec, 4, FILLER);

		
	public void initialize() {
		COBOLFunctions.initialize(tjl45Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl45Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}

