/*
 * File: P5223at.java
 * Date: 30 August 2009 0:19:37
 * Author: Quipoz Limited
 *
 * Class transformed from P5223AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.annuities.dataaccess.AnbkTableDAM;
import com.csc.life.annuities.dataaccess.Annyvf1TableDAM;
import com.csc.life.annuities.dataaccess.RegtbrkTableDAM;
import com.csc.life.annuities.dataaccess.RegtvstTableDAM;
import com.csc.life.annuities.dataaccess.VstdTableDAM;
import com.csc.life.annuities.dataaccess.VstdbrkTableDAM;
import com.csc.life.annuities.recordstructures.Vstannpcpy;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5673rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.CovrrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.statistics.dataaccess.SttrTableDAM;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrmatTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgpTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*
* P5223AT
* =======
*
* This new program forms part of the 9405 Annuities Development.
* It  is  the  AT  processing  for  registration of vesting on a
* deferred annuity.
* Its purpose is  to  update  the  Contract  Header,  Payer  and
* Coverage  details  and  to  perform  any generic processing by
* calling subroutines from T5671 for this transaction.
*
*
* Contract Header (CHDR)
* ======================
*
* Valid flag '2' the existing record and write a new valid  flag
* '1'  record with the risk and premium statii set as entered on
* T5679 for the transaction code.
*
*
* Payer (PAYR)
* ============
*
* Valid flag '2' the existing record and write a new valid  flag
* '1'  record with the risk and premium statii set as entered on
* T5679 for the transaction code.  (Contract statii).
*
*
* Coverage (COVR)
* ===============
*
* Valid flag '2' the existing record and write a new valid  flag
* '1'  record with the risk and premium statii set as entered on
* T5679 for the transaction code.  On the  new  valid  flag  '1'
* record set the sum assured and premium amounts as zero.
*
* Also, write a new COVR record with the next available coverage
* number.  On this new coverage the Coverage Type (CRTABLE) will
* be  set to the 'New Coverage after Vesting' from T6625 and the
* premium and sum assured amounts set to  the  values  from  the
* valid flag '2' coverage record.
* If  the 'New Coverage after Vesting' is not on T5673, Contract
* Structure, this processing will error.
*
* Annuity Details (ANNY)
* ======================
*
* The Annuity Details for the old coverage will remain unaltered.
*
* The online program will have written a new ANNY record with any
* changed  details  if  entered.  If  no  change  to the ANNY has
* occurred then no new ANNY will have been written.
*
* This program tries to read the  ANNY  file  for a Validflag '1'
* record. The record it finds on the Read will  be  what makes up
* the  new  ANNY  record with the new coverage key  identified by
* the  above  COVR  processing.  We  then need to delete the ANNY
* record  (if  created)  that  was  temporarily  storing  the new
* ANNY  details  under  the  old  coverage  key.   This  can   be
* identified by a match on TRANNO.
*
* Regular Payment (REGT)
* ======================
*
* REGT  records  are  created in the online program with the old
* coverage key. This program needs to rewrite these records with
* the correct  coverage key  and then delete the REGT's with the
* old coverage key.
*
* Statistics
* ==========
*
* The statistics subroutine, LIFSTTR, will be  called  to  write
* statistics records for this transaction.
*
*
* Calculations
* ============
*
* The  Calculation  Methods  table, T6598, will be read with the
* maturity method from T5687 for this component  to  obtain  the
* calculation  and  processing  subroutines.   These subroutines
* will do all of  the  calculations  and  write  any  accounting
* records required, not this AT program.
*
*
*****************************************************************
* </pre>
*/
public class P5223at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5223AT");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);

	private FixedLengthStringData wsaaLastVstd = new FixedLengthStringData(15);
	private FixedLengthStringData wsaaLastVstdChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaLastVstd, 0);
	private FixedLengthStringData wsaaLastVstdChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLastVstd, 1);
	private FixedLengthStringData wsaaLastVstdLife = new FixedLengthStringData(2).isAPartOf(wsaaLastVstd, 9);
	private FixedLengthStringData wsaaLastVstdCoverage = new FixedLengthStringData(2).isAPartOf(wsaaLastVstd, 11);
	private FixedLengthStringData wsaaLastVstdRider = new FixedLengthStringData(2).isAPartOf(wsaaLastVstd, 13);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* ERRORS */
	private static final String h999 = "H999";
		/* TABLES */
	private static final String t6625 = "T6625";
	private static final String t5673 = "T5673";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	private static final String t1693 = "T1693";
	private static final String t7508 = "T7508";
	private AnbkTableDAM anbkIO = new AnbkTableDAM();
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Annuity Details Valid Flag 1 only*/
	private Annyvf1TableDAM annyvf1IO = new Annyvf1TableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Maturity/Expiry logical over COVR*/
	private CovrmatTableDAM covrmatIO = new CovrmatTableDAM();
		/*Components for Regular Withdrawals.*/
	private CovrrgpTableDAM covrrgpIO = new CovrrgpTableDAM();
		/*Component - Life renewals*/
	private CovrrnlTableDAM covrrnlIO = new CovrrnlTableDAM();
	private IncrTableDAM incrIO = new IncrTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Regular Payments Temporary Details*/
	private RegtbrkTableDAM regtbrkIO = new RegtbrkTableDAM();
		/*Regular Payment Temporary Record Vesting*/
	private RegtvstTableDAM regtvstIO = new RegtvstTableDAM();
	private SttrTableDAM sttrIO = new SttrTableDAM();
	private VstdTableDAM vstdIO = new VstdTableDAM();
	private VstdbrkTableDAM vstdbrkIO = new VstdbrkTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Vstannpcpy vstannpcpy = new Vstannpcpy();
	private T5673rec t5673rec = new T5673rec();
	private T6625rec t6625rec = new T6625rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T1693rec t1693rec = new T1693rec();
	private T7508rec t7508rec = new T7508rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	private WsaaFlagsIndicatorsStoresInner wsaaFlagsIndicatorsStoresInner = new WsaaFlagsIndicatorsStoresInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		newCovr3201,
		errorProg610
	}

	public P5223at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialize1000();
		checkBreakout2000();
		while ( !(isEQ(vstdIO.getStatuz(),varcom.endp))) {
			vstdProcess3000();
		}

		updateChdrLifePayr4000();
		statistics5000();
		writePtrn6000();
		/*FINISH-OFF*/
		updateBatchHead7000();
		dryProcessing7500();
		releaseSoftlock8000();
		exitProgram();
	}

protected void initialize1000()
	{
		getChdr1000();
	}

protected void getChdr1000()
	{
		wsaaFlagsIndicatorsStoresInner.wsaaCovrRiskFlag.set("N");
		wsaaFlagsIndicatorsStoresInner.wsaaValidCombination.set("N");
		wsaaFlagsIndicatorsStoresInner.wsaaInstprem.set(ZERO);
		wsaaFlagsIndicatorsStoresInner.wsaaTranno.set(ZERO);
		wsaaFlagsIndicatorsStoresInner.wsaaT5673ContItem.set(ZERO);
		wsaaFlagsIndicatorsStoresInner.wsaaNewCoverage.set(ZERO);
		wsaaFlagsIndicatorsStoresInner.wsaaLargestCoverage.set(ZERO);
		wsaaFlagsIndicatorsStoresInner.wsaaNextCoverage.set(ZERO);
		wsaaFlagsIndicatorsStoresInner.wsaaTotInstprem.set(ZERO);
		wsaaLastVstd.set(ZERO);
		wsaaFlagsIndicatorsStoresInner.wsaaSub.set(1);
		wsaaFlagsIndicatorsStoresInner.wsaaTableSub.set(1);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Retrieve the contract header and store the trans number:*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction("READR");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			fatalError600();
		}
		/* Increment the transaction numbers for use throughout*/
		/* the program:*/
		compute(wsaaFlagsIndicatorsStoresInner.wsaaTranno, 0).set(add(1, chdrlifIO.getTranno()));
		statusCodes1100();
		getFirstVstd1200();
	}

protected void statusCodes1100()
	{
		readTable1100();
	}

protected void readTable1100()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void getFirstVstd1200()
	{
		begn1200();
	}

protected void begn1200()
	{
		vstdIO.setDataArea(SPACES);
		vstdIO.setChdrcoy(atmodrec.company);
		vstdIO.setChdrnum(wsaaPrimaryChdrnum);
		vstdIO.setTranno(ZERO);
		vstdIO.setPlanSuffix(ZERO);
		vstdIO.setFunction(varcom.begnh);
		vstdIO.setFormat(formatsInner.vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(vstdIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(vstdIO.getStatuz(),varcom.oK)) {
			if (isNE(vstdIO.getChdrcoy(),atmodrec.company)
			|| isNE(vstdIO.getChdrnum(),wsaaPrimaryChdrnum)
			|| isNE(vstdIO.getTranno(), wsaaFlagsIndicatorsStoresInner.wsaaTranno)) {
				sysrSyserrRecInner.sysrParams.set(vstdIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(vstdIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void checkBreakout2000()
	{
			start2000();
		}

protected void start2000()
	{
		/* If the lowest plan suffix to be vested is greater than the*/
		/* policies summarised on the contract header or if the plan*/
		/* suffix is zero, i.e. the vesting is to be done on all the*/
		/* policies, no breakout is required.*/
		/* breakout is required.*/
		vstdbrkIO.setDataArea(SPACES);
		vstdbrkIO.setChdrcoy(atmodrec.company);
		vstdbrkIO.setChdrnum(wsaaPrimaryChdrnum);
		vstdbrkIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		vstdbrkIO.setPlanSuffix(ZERO);
		vstdbrkIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		vstdbrkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vstdbrkIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");

		vstdbrkIO.setFormat(formatsInner.vstdbrkrec);
		SmartFileCode.execute(appVars, vstdbrkIO);
		if (isNE(vstdbrkIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(vstdbrkIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(vstdbrkIO.getStatuz());
			fatalError600();
		}
		if (isEQ(vstdbrkIO.getStatuz(),varcom.oK)) {
			if (isNE(vstdbrkIO.getChdrcoy(),atmodrec.company)
			|| isNE(vstdbrkIO.getChdrnum(),wsaaPrimaryChdrnum)
			|| isNE(vstdbrkIO.getTranno(), wsaaFlagsIndicatorsStoresInner.wsaaTranno)) {
				sysrSyserrRecInner.sysrParams.set(vstdbrkIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(vstdbrkIO.getStatuz());
				fatalError600();
			}
		}
		if (isGT(vstdbrkIO.getPlanSuffix(),chdrlifIO.getPolsum())
		|| isEQ(vstdbrkIO.getPlanSuffix(),0)) {
			return ;
		}
		brkoutrec.brkOldSummary.set(chdrlifIO.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(sub(vstdbrkIO.getPlanSuffix(),1));
		brkoutrec.brkChdrnum.set(chdrlifIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrlifIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(SPACES);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(brkoutrec.brkStatuz);
			fatalError600();
		}
		anbkIO.setDataArea(SPACES);
		anbkIO.setChdrcoy(atmodrec.company);
		anbkIO.setChdrnum(wsaaPrimaryChdrnum);
		anbkIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		anbkIO.setPlanSuffix(ZERO);
		anbkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, anbkIO);
		if (isNE(anbkIO.getStatuz(),varcom.oK)
		&& isNE(anbkIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(anbkIO.getParams());
			fatalError600();
		}
		if (isEQ(anbkIO.getStatuz(),varcom.oK)) {
			if (isNE(anbkIO.getChdrnum(),wsaaPrimaryChdrnum)
			|| isNE(anbkIO.getChdrcoy(),atmodrec.company)
			|| isNE(anbkIO.getTranno(), wsaaFlagsIndicatorsStoresInner.wsaaTranno)) {
				anbkIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, anbkIO);
				if (isNE(anbkIO.getStatuz(),varcom.oK)) {
					sysrSyserrRecInner.sysrParams.set(anbkIO.getParams());
					fatalError600();
				}
				anbkIO.setStatuz(varcom.endp);
			}
		}
		while ( !(isEQ(anbkIO.getStatuz(),varcom.endp))) {
			checkAnbk2100();
		}

	}

protected void checkAnbk2100()
	{
		checks2100();
	}

protected void checks2100()
	{
		annyIO.setChdrcoy(atmodrec.company);
		annyIO.setChdrnum(wsaaPrimaryChdrnum);
		annyIO.setLife(anbkIO.getLife());
		annyIO.setCoverage(anbkIO.getCoverage());
		annyIO.setRider(anbkIO.getRider());
		annyIO.setPlanSuffix(anbkIO.getPlanSuffix());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(annyIO.getParams());
			fatalError600();
		}
		annyIO.setGuarperd(anbkIO.getGuarperd());
		annyIO.setFreqann(anbkIO.getFreqann());
		annyIO.setArrears(anbkIO.getArrears());
		annyIO.setAdvance(anbkIO.getAdvance());
		annyIO.setDthpercn(anbkIO.getDthpercn());
		annyIO.setDthperco(anbkIO.getDthperco());
		annyIO.setIntanny(anbkIO.getIntanny());
		annyIO.setWithprop(anbkIO.getWithprop());
		annyIO.setWithoprop(anbkIO.getWithoprop());
		annyIO.setPpind(anbkIO.getPpind());
		annyIO.setCapcont(anbkIO.getCapcont());
		annyIO.setValidflag(anbkIO.getValidflag());
		annyIO.setNomlife(anbkIO.getNomlife());
		annyIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		/* Rewrite ANNY:*/
		annyIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(annyIO.getParams());
			fatalError600();
		}
		anbkIO.setFunction(varcom.delet);
		anbkIO.setFormat(formatsInner.anbkrec);
		SmartFileCode.execute(appVars, anbkIO);
		if (isNE(anbkIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(anbkIO.getParams());
			fatalError600();
		}
		anbkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, anbkIO);
		if (isNE(anbkIO.getStatuz(),varcom.oK)
		&& isNE(anbkIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(anbkIO.getParams());
			fatalError600();
		}
		if (isEQ(anbkIO.getStatuz(),varcom.oK)) {
			if (isNE(anbkIO.getChdrnum(),wsaaPrimaryChdrnum)
			|| isNE(anbkIO.getChdrcoy(),atmodrec.company)
			|| isNE(anbkIO.getTranno(), wsaaFlagsIndicatorsStoresInner.wsaaTranno)) {
				anbkIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, anbkIO);
				if (isNE(anbkIO.getStatuz(),varcom.oK)) {
					sysrSyserrRecInner.sysrParams.set(anbkIO.getParams());
					fatalError600();
				}
				anbkIO.setStatuz(varcom.endp);
			}
		}
	}

protected void vstdProcess3000()
	{
		/*START*/
		incrCheck3900();
		updateCovrStatii3100();
		writeNewCovr3200();
		writeNewAnny3400();
		writeNewRegts3500();
		deleteOldRegts3600();
		doAccounting3700();
		updateAndGetNextVstd3800();
		/*EXIT*/
	}

protected void updateCovrStatii3100()
	{
			start3100();
		}

protected void start3100()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(vstdIO.getChdrcoy());
		covrIO.setChdrnum(vstdIO.getChdrnum());
		covrIO.setLife(vstdIO.getLife());
		covrIO.setCoverage(vstdIO.getCoverage());
		covrIO.setRider(vstdIO.getRider());
		covrIO.setPlanSuffix(vstdIO.getPlanSuffix());
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			fatalError600();
		}
		/* Valid Flag 1/2 processing is performed on the old Coverage.*/
		wsaaFlagsIndicatorsStoresInner.wsaaTotInstprem.add(covrIO.getInstprem());
		wsaaFlagsIndicatorsStoresInner.wsaaInstprem.set(covrIO.getInstprem());
		covrIO.setValidflag("2");
		covrIO.setCurrto(vstdIO.getEffdate());
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(payrIO.getBillfreq(),"00")) {
			if (isNE(t5679rec.setSngpCovStat,SPACES)) {
				covrIO.setPstatcode(t5679rec.setSngpCovStat);
			}
		}
		else {
			if (isNE(t5679rec.setCovPremStat,SPACES)) {
				covrIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		if (isNE(t5679rec.setCovRiskStat,SPACES)) {
			covrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		covrIO.setCurrto(99999999);
		covrIO.setCurrfrom(vstdIO.getEffdate());
		covrIO.setValidflag("1");
		covrIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		covrIO.setUser(wsaaUser);
		covrIO.setTermid(wsaaTermid);
		covrIO.setTransactionTime(wsaaTransactionTime);
		covrIO.setTransactionDate(wsaaTransactionDate);
		covrIO.setSumins(ZERO);
		covrIO.setInstprem(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdIO.getRider(),"00")) {
			return ;
		}
		covrmatIO.setParams(SPACES);
		covrmatIO.setChdrcoy(vstdIO.getChdrcoy());
		covrmatIO.setChdrnum(vstdIO.getChdrnum());
		covrmatIO.setLife(vstdIO.getLife());
		covrmatIO.setCoverage(vstdIO.getCoverage());
		covrmatIO.setRider(vstdIO.getRider());
		covrmatIO.setPlanSuffix(vstdIO.getPlanSuffix());
		covrmatIO.setFormat(formatsInner.covrmatrec);
		covrmatIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrmatIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrmatIO.getStatuz());
			fatalError600();
		}
		while ( !(isNE(covrmatIO.getChdrcoy(),vstdIO.getChdrcoy())
		|| isNE(covrmatIO.getChdrnum(),vstdIO.getChdrnum())
		|| isNE(covrmatIO.getLife(),vstdIO.getLife())
		|| isNE(covrmatIO.getCoverage(),vstdIO.getCoverage())
		|| isNE(covrmatIO.getPlanSuffix(),vstdIO.getPlanSuffix())
		|| isEQ(covrmatIO.getStatuz(),varcom.endp))) {
			updateRider3110();
		}

		if (isNE(covrmatIO.getStatuz(),varcom.endp)) {
			covrmatIO.setFormat(formatsInner.covrmatrec);
			covrmatIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, covrmatIO);
			if (isNE(covrmatIO.getStatuz(),varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(covrmatIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(covrmatIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void updateRider3110()
	{
					start3110();
					readNextCovr3112();
				}

protected void start3110()
	{
		if (isEQ(covrmatIO.getRider(),"00")) {
			return ;
		}
		if (isNE(covrmatIO.getPlanSuffix(),vstdIO.getPlanSuffix())) {
			return ;
		}
		covrmatIO.setValidflag("2");
		covrmatIO.setFormat(formatsInner.covrmatrec);
		covrmatIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrmatIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrmatIO.getStatuz());
			fatalError600();
		}
		covrmatIO.setValidflag("1");
		covrmatIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		covrmatIO.setUser(wsaaUser);
		covrmatIO.setTermid(wsaaTermid);
		covrmatIO.setTransactionTime(wsaaTransactionTime);
		covrmatIO.setTransactionDate(wsaaTransactionDate);
		if (isNE(t5679rec.setRidRiskStat,SPACES)) {
			covrmatIO.setStatcode(t5679rec.setRidRiskStat);
		}
		if (isNE(t5679rec.setRidPremStat,SPACES)) {
			covrmatIO.setPstatcode(t5679rec.setRidPremStat);
		}
		covrmatIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrmatIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrmatIO.getStatuz());
			fatalError600();
		}
	}

protected void readNextCovr3112()
	{
		covrmatIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)
		&& isNE(covrmatIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrmatIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrmatIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writeNewCovr3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3200();
				case newCovr3201:
					newCovr3201();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3200()
	{
		if (isEQ(vstdIO.getChdrcoy(),wsaaLastVstdChdrcoy)
		&& isEQ(vstdIO.getChdrnum(),wsaaLastVstdChdrnum)
		&& isEQ(vstdIO.getLife(),wsaaLastVstdLife)
		&& isEQ(vstdIO.getCoverage(),wsaaLastVstdCoverage)
		&& isEQ(vstdIO.getRider(),wsaaLastVstdRider)) {
			goTo(GotoLabel.newCovr3201);
		}
		else {
			wsaaLastVstdChdrcoy.set(vstdIO.getChdrcoy());
			wsaaLastVstdChdrnum.set(vstdIO.getChdrnum());
			wsaaLastVstdLife.set(vstdIO.getLife());
			wsaaLastVstdCoverage.set(vstdIO.getCoverage());
			wsaaLastVstdRider.set(vstdIO.getRider());
		}
		wsaaFlagsIndicatorsStoresInner.wsaaLargestCoverage.set(ZERO);
		covrrnlIO.setParams(SPACES);
		covrrnlIO.setChdrcoy(vstdIO.getChdrcoy());
		covrrnlIO.setChdrnum(vstdIO.getChdrnum());
		covrrnlIO.setLife(vstdIO.getLife());
		covrrnlIO.setCoverage(vstdIO.getCoverage());
		covrrnlIO.setRider(vstdIO.getRider());
		covrrnlIO.setPlanSuffix(vstdIO.getPlanSuffix());
		covrrnlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrrnlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrrnlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrrnlIO.getStatuz(),varcom.endp))) {
			nextComponent3210();
		}

		compute(wsaaFlagsIndicatorsStoresInner.wsaaNewCoverage, 0).set(add(1, wsaaFlagsIndicatorsStoresInner.wsaaLargestCoverage));
	}

protected void newCovr3201()
	{
		covrIO.setCoverage(wsaaFlagsIndicatorsStoresInner.wsaaNewCoverage);
		calcRiskCessAndAnb3220();
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(vstdIO.getCrtable());
		itdmIO.setItmfrm(vstdIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(),vstdIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
		covrIO.setValidflag("1");
		covrIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		covrIO.setUser(wsaaUser);
		covrIO.setTermid(wsaaTermid);
		covrIO.setTransactionTime(wsaaTransactionTime);
		covrIO.setTransactionDate(wsaaTransactionDate);
		covrIO.setSumins(vstdIO.getVstpay());
		covrIO.setInstprem(wsaaFlagsIndicatorsStoresInner.wsaaInstprem);
		covrIO.setStatcode(t5679rec.setCovRiskStat);
		covrIO.setCrtable(t6625rec.ncovvest);
		covrIO.setCrrcd(vstdIO.getEffdate());
		covrIO.setCurrfrom(vstdIO.getEffdate());
		covrIO.setRiskCessAge(99);
		covrIO.setRiskCessTerm(ZERO);
		covrIO.setCoverage(wsaaFlagsIndicatorsStoresInner.wsaaNewCoverage);
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			fatalError600();
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setItemtabl(t5673);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(),t5673)
		|| isNE(itdmIO.getItemitem(),chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrStatuz.set(h999);
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			fatalError600();
		}
		t5673rec.t5673Rec.set(itdmIO.getGenarea());
		wsaaFlagsIndicatorsStoresInner.wsaaValidCombination.set(SPACES);
		while ( !(isNE(wsaaFlagsIndicatorsStoresInner.wsaaValidCombination, SPACES))) {
			findCoverage3230();
		}

		if (isEQ(wsaaFlagsIndicatorsStoresInner.wsaaValidCombination, "N")) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(h999);
			fatalError600();
		}
	}

protected void nextComponent3210()
	{
			start3210();
		}

protected void start3210()
	{
		/* Perform this loop until get the last COVERAGE, checking each*/
		/* time whether the coverage number is greater than the largest*/
		/* coverage and moving in the new largest coverage number if*/
		/* this is the case.*/
		SmartFileCode.execute(appVars, covrrnlIO);
		if (isNE(covrrnlIO.getStatuz(),varcom.oK)
		&& isNE(covrrnlIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrrnlIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrrnlIO.getParams());
			fatalError600();
		}
		if ((isNE(covrrnlIO.getChdrnum(),vstdIO.getChdrnum())
		|| isNE(covrrnlIO.getChdrcoy(),vstdIO.getChdrcoy()))) {
			covrrnlIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrrnlIO.getStatuz(),varcom.endp)) {
			return ;
		}
		/* The reason for having WSAA-COVERAGES, is that COVERAGE is*/
		/* a alphanumeric field, so it is safer if it is moved to a*/
		/* numeric field before mathematical operations are performed*/
		/* on it:*/
		wsaaFlagsIndicatorsStoresInner.wsaaNextCoverage.set(covrrnlIO.getCoverage());
		if (isGT(wsaaFlagsIndicatorsStoresInner.wsaaNextCoverage, wsaaFlagsIndicatorsStoresInner.wsaaLargestCoverage)) {
			wsaaFlagsIndicatorsStoresInner.wsaaLargestCoverage.set(wsaaFlagsIndicatorsStoresInner.wsaaNextCoverage);
		}
		covrrnlIO.setFunction(varcom.nextr);
	}

protected void calcRiskCessAndAnb3220()
	{
		start3220();
	}

protected void start3220()
	{
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(vstdIO.getChdrcoy());
		lifemjaIO.setChdrnum(vstdIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifemjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(lifemjaIO.getChdrcoy(),vstdIO.getChdrcoy())
		|| isNE(lifemjaIO.getChdrnum(),vstdIO.getChdrnum())) {
			sysrSyserrRecInner.sysrParams.set(lifemjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/* Calculate risk cessation date.*/
		datcon2rec.freqFactor.set(99);
		datcon2rec.frequency.set("01");
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(lifemjaIO.getCltdob());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon2rec.datcon2Rec);
			sysrSyserrRecInner.sysrStatuz.set(datcon2rec.statuz);
			fatalError600();
		}
		covrIO.setRiskCessDate(datcon2rec.intDate2);
		/* Calculate age next birthday at risk commencement date.*/
		/*    MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1.              */
		/*    MOVE VSTD-EFFDATE           TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3'              USING DTC3-DATCON3-REC.          */
		/*    IF DTC3-STATUZ               = O-K                           */
		/*        MOVE DTC3-FREQ-FACTOR   TO COVR-ANB-AT-CCD               */
		/*    ELSE                                                         */
		/*        MOVE DTC3-DATCON3-REC   TO SYSR-PARAMS                   */
		/*        PERFORM 600-FATAL-ERROR                                  */
		/*    END-IF.                                                      */
		/* Routine to calculate Age next/nearest/last birthday             */
		/* replaces above.                                                 */
		/*  Identify FSU Company                                           */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(atmodrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(atmodrec.language);
		agecalcrec.cnttype.set(chdrlifIO.getCnttype());
		agecalcrec.intDate1.set(lifemjaIO.getCltdob());
		agecalcrec.intDate2.set(vstdIO.getEffdate());
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(agecalcrec.statuz);
			fatalError600();
		}
		covrIO.setAnbAtCcd(agecalcrec.agerating);
	}

protected void findCoverage3230()
	{
			start3230();
		}

protected void start3230()
	{
		for (wsaaFlagsIndicatorsStoresInner.wsaaTableSub.set(1); !(isGT(wsaaFlagsIndicatorsStoresInner.wsaaTableSub, 8)
		|| isEQ(wsaaFlagsIndicatorsStoresInner.wsaaValidCombination, "Y")); wsaaFlagsIndicatorsStoresInner.wsaaTableSub.add(1)){
			if (isEQ(t5673rec.ctable[wsaaFlagsIndicatorsStoresInner.wsaaTableSub.toInt()], covrIO.getCrtable())) {
				wsaaFlagsIndicatorsStoresInner.wsaaValidCombination.set("Y");
			}
		}
		if (isEQ(wsaaFlagsIndicatorsStoresInner.wsaaValidCombination, "Y")) {
			return ;
		}
		/* If the coverage has not been found look in the continuation*/
		/* table.*/
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5673);
		itdmIO.setItemitem(t5673rec.gitem);
		wsaaFlagsIndicatorsStoresInner.wsaaT5673ContItem.set(t5673rec.gitem);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(),t5673)
		|| isNE(itdmIO.getItemitem(), wsaaFlagsIndicatorsStoresInner.wsaaT5673ContItem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaFlagsIndicatorsStoresInner.wsaaValidCombination.set("N");
		}
		else {
			t5673rec.t5673Rec.set(itdmIO.getGenarea());
		}
		for (wsaaFlagsIndicatorsStoresInner.wsaaTableSub.set(1); !(isGT(wsaaFlagsIndicatorsStoresInner.wsaaTableSub, 8)
		|| isEQ(t5673rec.ctable[wsaaFlagsIndicatorsStoresInner.wsaaTableSub.toInt()], covrIO.getCrtable())); wsaaFlagsIndicatorsStoresInner.wsaaTableSub.add(1)){
			if (isEQ(t5673rec.ctable[wsaaFlagsIndicatorsStoresInner.wsaaTableSub.toInt()], covrIO.getCrtable())) {
				wsaaFlagsIndicatorsStoresInner.wsaaValidCombination.set("Y");
			}
		}
	}

protected void writeNewAnny3400()
	{
			start3400();
		}

protected void start3400()
	{
		annyvf1IO.setParams(SPACES);
		annyvf1IO.setFunction(varcom.readr);
		annyvf1IO.setChdrcoy(vstdIO.getChdrcoy());
		annyvf1IO.setChdrnum(vstdIO.getChdrnum());
		annyvf1IO.setLife(vstdIO.getLife());
		annyvf1IO.setCoverage(vstdIO.getCoverage());
		annyvf1IO.setRider(vstdIO.getRider());
		annyvf1IO.setPlanSuffix(vstdIO.getPlanSuffix());
		SmartFileCode.execute(appVars, annyvf1IO);
		if (isNE(annyvf1IO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(annyvf1IO.getParams());
			fatalError600();
		}
		if (isEQ(annyvf1IO.getTranno(), wsaaFlagsIndicatorsStoresInner.wsaaTranno)) {
			wsaaFlagsIndicatorsStoresInner.wsaaAnnyChangeDtls.set("Y");
		}
		else {
			wsaaFlagsIndicatorsStoresInner.wsaaAnnyChangeDtls.set("N");
		}
		annyvf1IO.setCoverage(wsaaFlagsIndicatorsStoresInner.wsaaNewCoverage);
		annyvf1IO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		annyvf1IO.setFormat(formatsInner.annyvf1rec);
		annyvf1IO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, annyvf1IO);
		if (isNE(annyvf1IO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(annyvf1IO.getParams());
			fatalError600();
		}
		/* If Annuity Details were changed, an ANNY record will have been*/
		/* written with the new details but the old coverage key. If no*/
		/* details were changed, no new ANNY would have been written. The*/
		/* above TRANNO check establishes this and therefore decides*/
		/* whether the temporary record created has to be deleted.*/
		if (isEQ(wsaaFlagsIndicatorsStoresInner.wsaaAnnyChangeDtls, "N")) {
			return ;
		}
		/* Read and Hold the original ANNY Validflag '1' record that was*/
		/* created in the online transaction.*/
		annyvf1IO.setParams(SPACES);
		annyvf1IO.setFunction(varcom.readh);
		annyvf1IO.setChdrcoy(vstdIO.getChdrcoy());
		annyvf1IO.setChdrnum(vstdIO.getChdrnum());
		annyvf1IO.setLife(vstdIO.getLife());
		annyvf1IO.setCoverage(vstdIO.getCoverage());
		annyvf1IO.setRider(vstdIO.getRider());
		annyvf1IO.setPlanSuffix(vstdIO.getPlanSuffix());
		SmartFileCode.execute(appVars, annyvf1IO);
		if (isNE(annyvf1IO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(annyvf1IO.getParams());
			fatalError600();
		}
		annyvf1IO.setFormat(formatsInner.annyvf1rec);
		annyvf1IO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, annyvf1IO);
		if (isNE(annyvf1IO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(annyvf1IO.getParams());
			fatalError600();
		}
	}

protected void writeNewRegts3500()
	{
		start3500();
	}

protected void start3500()
	{
		regtbrkIO.setParams(SPACES);
		regtbrkIO.setChdrcoy(vstdIO.getChdrcoy());
		regtbrkIO.setChdrnum(vstdIO.getChdrnum());
		regtbrkIO.setLife(vstdIO.getLife());
		regtbrkIO.setCoverage(vstdIO.getCoverage());
		regtbrkIO.setRider(vstdIO.getRider());
		regtbrkIO.setPlanSuffix(vstdIO.getPlanSuffix());
		regtbrkIO.setSeqnbr(ZERO);
		regtbrkIO.setRgpynum(ZERO);
		regtbrkIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regtbrkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtbrkIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, regtbrkIO);
		if (isNE(regtbrkIO.getStatuz(),varcom.oK)
		&& isNE(regtbrkIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(regtbrkIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(regtbrkIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdIO.getChdrcoy(),regtbrkIO.getChdrcoy())
		|| isNE(vstdIO.getChdrnum(),regtbrkIO.getChdrnum())
		|| isNE(vstdIO.getLife(),regtbrkIO.getLife())
		|| isNE(vstdIO.getCoverage(),regtbrkIO.getCoverage())
		|| isNE(vstdIO.getRider(),regtbrkIO.getRider())
		|| isNE(vstdIO.getPlanSuffix(),regtbrkIO.getPlanSuffix())
		|| isEQ(regtbrkIO.getStatuz(),varcom.endp)) {
			regtbrkIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(regtbrkIO.getStatuz(),varcom.endp))) {
			insertRegts3510();
		}

	}

protected void insertRegts3510()
	{
		start3510();
	}

protected void start3510()
	{
		regtvstIO.setParams(SPACES);
		regtvstIO.setChdrcoy(regtbrkIO.getChdrcoy());
		regtvstIO.setChdrnum(regtbrkIO.getChdrnum());
		regtvstIO.setLife(regtbrkIO.getLife());
		regtvstIO.setCoverage(wsaaFlagsIndicatorsStoresInner.wsaaNewCoverage);
		regtvstIO.setRider(regtbrkIO.getRider());
		regtvstIO.setSeqnbr(regtbrkIO.getSeqnbr());
		regtvstIO.setRgpynum(regtbrkIO.getRgpynum());
		regtvstIO.setSacscode(regtbrkIO.getSacscode());
		regtvstIO.setSacstype(regtbrkIO.getSacstype());
		regtvstIO.setGlact(regtbrkIO.getGlact());
		regtvstIO.setDebcred(regtbrkIO.getDebcred());
		regtvstIO.setDestkey(regtbrkIO.getDestkey());
		regtvstIO.setPaycoy(regtbrkIO.getPaycoy());
		regtvstIO.setPayclt(regtbrkIO.getPayclt());
		regtvstIO.setRgpymop(regtbrkIO.getRgpymop());
		regtvstIO.setRegpayfreq(regtbrkIO.getRegpayfreq());
		regtvstIO.setCurrcd(regtbrkIO.getCurrcd());
		regtvstIO.setPymt(regtbrkIO.getPymt());
		regtvstIO.setPrcnt(regtbrkIO.getPrcnt());
		regtvstIO.setTotamnt(regtbrkIO.getTotamnt());
		regtvstIO.setPayreason(regtbrkIO.getPayreason());
		regtvstIO.setClaimevd(regtbrkIO.getClaimevd());
		regtvstIO.setBankkey(regtbrkIO.getBankkey());
		regtvstIO.setBankacckey(regtbrkIO.getBankacckey());
		regtvstIO.setCrtdate(regtbrkIO.getCrtdate());
		regtvstIO.setFirstPaydate(regtbrkIO.getFirstPaydate());
		regtvstIO.setRevdte(regtbrkIO.getRevdte());
		regtvstIO.setFinalPaydate(regtbrkIO.getFinalPaydate());
		regtvstIO.setAnvdate(regtbrkIO.getAnvdate());
		regtvstIO.setRgpytype(regtbrkIO.getRgpytype());
		regtvstIO.setCrtable(covrIO.getCrtable());
		regtvstIO.setPlanSuffix(regtbrkIO.getPlanSuffix());
		regtvstIO.setPayoutopt(regtbrkIO.getPayoutopt());//fwang3 ICIL-4
		regtvstIO.setFunction(varcom.writr);
		regtvstIO.setFormat(formatsInner.regtvstrec);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(regtvstIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(regtvstIO.getStatuz());
			fatalError600();
		}
		regtbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regtbrkIO);
		if (isNE(regtbrkIO.getStatuz(),varcom.oK)
		&& isNE(regtbrkIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(regtbrkIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(regtbrkIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdIO.getChdrcoy(),regtbrkIO.getChdrcoy())
		|| isNE(vstdIO.getChdrnum(),regtbrkIO.getChdrnum())
		|| isNE(vstdIO.getLife(),regtbrkIO.getLife())
		|| isNE(vstdIO.getCoverage(),regtbrkIO.getCoverage())
		|| isNE(vstdIO.getRider(),regtbrkIO.getRider())
		|| isNE(vstdIO.getPlanSuffix(),regtbrkIO.getPlanSuffix())
		|| isEQ(regtbrkIO.getStatuz(),varcom.endp)) {
			regtbrkIO.setStatuz(varcom.endp);
		}
	}

protected void deleteOldRegts3600()
	{
		start3600();
	}

protected void start3600()
	{
		regtvstIO.setParams(SPACES);
		regtvstIO.setChdrcoy(vstdIO.getChdrcoy());
		regtvstIO.setChdrnum(vstdIO.getChdrnum());
		regtvstIO.setLife(vstdIO.getLife());
		regtvstIO.setCoverage(vstdIO.getCoverage());
		regtvstIO.setRider(vstdIO.getRider());
		regtvstIO.setPlanSuffix(vstdIO.getPlanSuffix());
		regtvstIO.setSeqnbr(ZERO);
		regtvstIO.setRgpynum(ZERO);
		regtvstIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)
		&& isNE(regtvstIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(regtvstIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(regtvstIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdIO.getChdrcoy(),regtvstIO.getChdrcoy())
		|| isNE(vstdIO.getChdrnum(),regtvstIO.getChdrnum())
		|| isNE(vstdIO.getLife(),regtvstIO.getLife())
		|| isNE(vstdIO.getCoverage(),regtvstIO.getCoverage())
		|| isNE(vstdIO.getRider(),regtvstIO.getRider())
		|| isNE(vstdIO.getPlanSuffix(),regtvstIO.getPlanSuffix())
		|| isEQ(regtvstIO.getStatuz(),varcom.endp)) {
			regtvstIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(regtvstIO.getStatuz(),varcom.endp))) {
			deleteRegts3610();
		}

	}

protected void deleteRegts3610()
	{
		start3610();
	}

protected void start3610()
	{
		regtvstIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(regtvstIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(regtvstIO.getStatuz());
			fatalError600();
		}
		regtvstIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(),varcom.oK)
		&& isNE(regtvstIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(regtvstIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(regtvstIO.getStatuz());
			fatalError600();
		}
		/* Rewrite the last REGT record to unlock it.*/
		if (isNE(vstdIO.getChdrcoy(),regtvstIO.getChdrcoy())
		|| isNE(vstdIO.getChdrnum(),regtvstIO.getChdrnum())
		|| isNE(vstdIO.getLife(),regtvstIO.getLife())
		|| isNE(vstdIO.getCoverage(),regtvstIO.getCoverage())
		|| isNE(vstdIO.getRider(),regtvstIO.getRider())
		|| isNE(vstdIO.getPlanSuffix(),regtvstIO.getPlanSuffix())
		|| isEQ(regtvstIO.getStatuz(),varcom.endp)) {
			regtvstIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, regtvstIO);
			if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(regtvstIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(regtvstIO.getStatuz());
				fatalError600();
			}
			regtvstIO.setStatuz(varcom.endp);
		}
	}

protected void doAccounting3700()
	{
		start3700();
	}

protected void start3700()
	{
		/* Table T5687 is read to obtain maturity method:*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(vstdIO.getCrtable());
		itdmIO.setItmfrm(vstdIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),vstdIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(atmodrec.company);
			itdmIO.setItemtabl(t5687);
			itdmIO.setItemitem(vstdIO.getCrtable());
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/* Table T6598 is consulted to check for any calculation*/
		/* routines:*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.maturityCalcMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
			procesprog3710();
		}
	}

protected void procesprog3710()
	{
		start3710();
	}

protected void start3710()
	{
		/* Set up COPYBOOK FOR VSTANNP:*/
		vstannpcpy.rec.set(SPACES);
		vstannpcpy.crtable.set(vstdIO.getCrtable());
		vstannpcpy.chdrcoy.set(vstdIO.getChdrcoy());
		vstannpcpy.chdrnum.set(vstdIO.getChdrnum());
		vstannpcpy.life.set(vstdIO.getLife());
		vstannpcpy.planSuffix.set(vstdIO.getPlanSuffix());
		vstannpcpy.coverage.set(vstdIO.getCoverage());
		vstannpcpy.rider.set(vstdIO.getRider());
		vstannpcpy.effdate.set(vstdIO.getEffdate());
		vstannpcpy.elvstfct.set(vstdIO.getElvstfct());
		vstannpcpy.vstpaya.set(vstdIO.getVstpaya());
		vstannpcpy.origpay.set(vstdIO.getOrigpay());
		for (wsaaFlagsIndicatorsStoresInner.wsaaIndex.set(1); !(isGT(wsaaFlagsIndicatorsStoresInner.wsaaIndex, 6)); wsaaFlagsIndicatorsStoresInner.wsaaIndex.add(1)){
			vstannpcpy.type[wsaaFlagsIndicatorsStoresInner.wsaaIndex.toInt()].set(vstdIO.getType(wsaaFlagsIndicatorsStoresInner.wsaaIndex));
			vstannpcpy.amnt[wsaaFlagsIndicatorsStoresInner.wsaaIndex.toInt()].set(vstdIO.getVstamt(wsaaFlagsIndicatorsStoresInner.wsaaIndex));
		}
		vstannpcpy.cnttype.set(chdrlifIO.getCnttype());
		vstannpcpy.cntcurr.set(chdrlifIO.getCntcurr());
		vstannpcpy.language.set(atmodrec.language);
		vstannpcpy.batckey.set(atmodrec.batchKey);
		vstannpcpy.user.set(wsaaUser);
		vstannpcpy.tranno.set(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		vstannpcpy.termid.set(wsaaTermid);
		vstannpcpy.statuz.set(varcom.oK);
		callProgram(t6598rec.procesprog, vstannpcpy.rec);
		if (isNE(vstannpcpy.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(vstannpcpy.statuz);
			fatalError600();
		}
	}

protected void updateAndGetNextVstd3800()
	{
		start3800();
	}

protected void start3800()
	{
		/* Update the VSTD with the new coverage number.*/
		vstdIO.setNewcovr(wsaaFlagsIndicatorsStoresInner.wsaaNewCoverage);
		vstdIO.setFormat(formatsInner.vstdrec);
		vstdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(vstdIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		/* Store the VSTD fields needed in later processing before*/
		/* reading the next record.*/
		wsaaFlagsIndicatorsStoresInner.wsaaEffdate.set(vstdIO.getEffdate());
		wsaaFlagsIndicatorsStoresInner.wsaaLife.set(vstdIO.getLife());
		vstdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)
		&& isNE(vstdIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(vstdIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(vstdIO.getStatuz(),varcom.oK)) {
			if (isNE(vstdIO.getChdrcoy(),atmodrec.company)
			|| isNE(vstdIO.getChdrnum(),wsaaPrimaryChdrnum)
			|| isNE(vstdIO.getTranno(), wsaaFlagsIndicatorsStoresInner.wsaaTranno)) {
				vstdIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, vstdIO);
				if (isNE(vstdIO.getStatuz(),varcom.oK)) {
					sysrSyserrRecInner.sysrParams.set(vstdIO.getParams());
					sysrSyserrRecInner.sysrStatuz.set(vstdIO.getStatuz());
					fatalError600();
				}
				vstdIO.setStatuz(varcom.endp);
				vstdIO.setEffdate(wsaaFlagsIndicatorsStoresInner.wsaaEffdate);
				vstdIO.setLife(wsaaFlagsIndicatorsStoresInner.wsaaLife);
			}
		}
	}

protected void incrCheck3900()
	{
			para3910();
		}

protected void para3910()
	{
		incrIO.setDataArea(SPACES);
		incrIO.setChdrcoy(vstdIO.getChdrcoy());
		incrIO.setChdrnum(vstdIO.getChdrnum());
		incrIO.setLife(vstdIO.getLife());
		incrIO.setCoverage(vstdIO.getCoverage());
		incrIO.setRider(vstdIO.getRider());
		incrIO.setPlanSuffix(vstdIO.getPlanSuffix());
		incrIO.setFunction(varcom.readr);
		incrIO.setFormat(formatsInner.incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)
		&& isNE(incrIO.getStatuz(),varcom.mrnf)) {
			sysrSyserrRecInner.sysrStatuz.set(incrIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			fatalError600();
		}
		if (isEQ(incrIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		incrIO.setValidflag("2");
		if (isNE(t5679rec.setCovPremStat,SPACES)) {
			incrIO.setPstatcode(t5679rec.setCovPremStat);
		}
		if (isNE(t5679rec.setCovRiskStat,SPACES)) {
			incrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		incrIO.setTransactionDate(wsaaTransactionDate);
		incrIO.setTransactionTime(wsaaTransactionTime);
		incrIO.setUser(wsaaUser);
		incrIO.setTermid(wsaaTermid);
		incrIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		incrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(incrIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			fatalError600();
		}
	}

protected void updateChdrLifePayr4000()
	{
			start4000();
		}

protected void start4000()
	{
		/* COVRRGP is used to read all the valid flag 1 COVRs*/
		/* to check if any are in force.  If there are no more*/
		/* in force coverages, then the statii on the CHDR, LIFE*/
		/* and PAYR will be updated as the contract will be fully*/
		/* vested.  If there are any in force Coverages then the*/
		/* contract is only partially vested so leave the statii*/
		/* alone.*/
		covrrgpIO.setDataArea(SPACES);
		covrrgpIO.setChdrcoy(atmodrec.company);
		covrrgpIO.setChdrnum(wsaaPrimaryChdrnum);
		covrrgpIO.setPlanSuffix(9999);
		covrrgpIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrrgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrrgpIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrrgpIO.setFormat(formatsInner.covrrgprec);
		while ( !(isEQ(covrrgpIO.getStatuz(),varcom.endp)
		|| wsaaFlagsIndicatorsStoresInner.covrAtRisk.isTrue())) {
			checkAllCovr4100();
		}

		/* Rewrite old CHDR and write new one:*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			fatalError600();
		}
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setCurrto(vstdIO.getEffdate());
		chdrlifIO.setValidflag("2");
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrlifIO.getStatuz());
			fatalError600();
		}
		chdrlifIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		chdrlifIO.setStattran(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		chdrlifIO.setTranlused(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		chdrlifIO.setValidflag("1");
		setPrecision(chdrlifIO.getSinstamt01(), 2);
		chdrlifIO.setSinstamt01(sub(chdrlifIO.getSinstamt01(), wsaaFlagsIndicatorsStoresInner.wsaaTotInstprem));
		setPrecision(chdrlifIO.getSinstamt06(), 2);
		chdrlifIO.setSinstamt06(sub(chdrlifIO.getSinstamt06(), wsaaFlagsIndicatorsStoresInner.wsaaTotInstprem));
		/* If there are no more in force coverages, set the*/
		/* SINSTAMT02 i.e. the policy fee to zeroes.*/
		if (!wsaaFlagsIndicatorsStoresInner.covrAtRisk.isTrue()) {
			if (isNE(t5679rec.setCnRiskStat,SPACES)) {
				chdrlifIO.setStatcode(t5679rec.setCnRiskStat);
			}
			if (isNE(t5679rec.setCnPremStat,SPACES)) {
				chdrlifIO.setPstatcode(t5679rec.setCnPremStat);
			}
			chdrlifIO.setSinstamt02(ZERO);
			chdrlifIO.setSinstamt06(ZERO);
		}
		chdrlifIO.setCurrfrom(vstdIO.getEffdate());
		chdrlifIO.setCurrto(99999999);
		chdrlifIO.setFunction(varcom.writr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrlifIO.getStatuz());
			fatalError600();
		}
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction("READH");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* If there are no more in force coverages, set the*/
		/* SINSTAMT02 i.e. the policy fee to zeroes.*/
		payrIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		payrIO.setEffdate(vstdIO.getEffdate());
		payrIO.setValidflag("1");
		setPrecision(payrIO.getSinstamt01(), 2);
		payrIO.setSinstamt01(sub(payrIO.getSinstamt01(), wsaaFlagsIndicatorsStoresInner.wsaaTotInstprem));
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(sub(payrIO.getSinstamt06(), wsaaFlagsIndicatorsStoresInner.wsaaTotInstprem));
		if (!wsaaFlagsIndicatorsStoresInner.covrAtRisk.isTrue()) {
			payrIO.setPstatcode(t5679rec.setCnPremStat);
			payrIO.setSinstamt02(ZERO);
			payrIO.setSinstamt06(ZERO);
		}
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Rewrite all the old LIFEs and write new ones if the Contract*/
		/* Risk is changing.*/
		if (wsaaFlagsIndicatorsStoresInner.covrAtRisk.isTrue()) {
			return ;
		}
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(atmodrec.company);
		lifemjaIO.setChdrnum(wsaaPrimaryChdrnum);
		lifemjaIO.setLife(vstdIO.getLife());
		lifemjaIO.setJlife(ZERO);
		lifemjaIO.setFunction("BEGNH");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifemjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(lifemjaIO.getChdrcoy(),atmodrec.company)
		|| isNE(lifemjaIO.getChdrnum(),wsaaPrimaryChdrnum)) {
			lifemjaIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(lifemjaIO.getStatuz(),varcom.endp))) {
			updateLife4200();
		}

	}

protected void checkAllCovr4100()
	{
			start4100();
		}

protected void start4100()
	{
		SmartFileCode.execute(appVars, covrrgpIO);
		if (isNE(covrrgpIO.getStatuz(),varcom.oK)
		&& isNE(covrrgpIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrrgpIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrrgpIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrrgpIO.getStatuz(),varcom.endp)
		|| isNE(covrrgpIO.getChdrcoy(),atmodrec.company)
		|| isNE(covrrgpIO.getChdrnum(),wsaaPrimaryChdrnum)) {
			covrrgpIO.setStatuz(varcom.endp);
			return ;
		}
		/* Check the COVR status with the value on T5679. If they are*/
		/* the same, set the flag, and drop out:*/
		wsaaFlagsIndicatorsStoresInner.wsaaCovrRiskFlag.set("N");
		wsaaFlagsIndicatorsStoresInner.wsaaRiskStatOk.set("N");
		wsaaFlagsIndicatorsStoresInner.wsaaPremStatOk.set("N");
		for (wsaaFlagsIndicatorsStoresInner.wsaaSub.set(1); !(isGT(wsaaFlagsIndicatorsStoresInner.wsaaSub, 12)
		|| wsaaFlagsIndicatorsStoresInner.covrAtRisk.isTrue()); wsaaFlagsIndicatorsStoresInner.wsaaSub.add(1)){
			checkStatus4110();
		}
		covrrgpIO.setFunction(varcom.nextr);
	}

protected void checkStatus4110()
	{
		/*START*/
		if (isEQ(covrrgpIO.getStatcode(), t5679rec.covRiskStat[wsaaFlagsIndicatorsStoresInner.wsaaSub.toInt()])) {
			wsaaFlagsIndicatorsStoresInner.wsaaRiskStatOk.set("Y");
		}
		if (isEQ(covrrgpIO.getPstatcode(), t5679rec.covPremStat[wsaaFlagsIndicatorsStoresInner.wsaaSub.toInt()])) {
			wsaaFlagsIndicatorsStoresInner.wsaaPremStatOk.set("Y");
		}
		if (isEQ(wsaaFlagsIndicatorsStoresInner.wsaaRiskStatOk, "Y")
		&& isEQ(wsaaFlagsIndicatorsStoresInner.wsaaPremStatOk, "Y")) {
			wsaaFlagsIndicatorsStoresInner.covrAtRisk.setTrue();
		}
		/*EXIT*/
	}

protected void updateLife4200()
	{
		start4200();
	}

protected void start4200()
	{
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setCurrto(vstdIO.getEffdate());
		lifemjaIO.setValidflag("2");
		lifemjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifemjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		lifemjaIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		lifemjaIO.setCurrfrom(vstdIO.getEffdate());
		lifemjaIO.setCurrto(99999999);
		lifemjaIO.setValidflag("1");
		lifemjaIO.setUser(wsaaUser);
		lifemjaIO.setTermid(wsaaTermid);
		lifemjaIO.setTransactionTime(wsaaTransactionTime);
		lifemjaIO.setTransactionDate(wsaaTransactionDate);
		if (!wsaaFlagsIndicatorsStoresInner.covrAtRisk.isTrue()) {
			if (isNE(t5679rec.setLifeStat,SPACES)) {
				lifemjaIO.setStatcode(t5679rec.setLifeStat);
			}
		}
		lifemjaIO.setFunction(varcom.writr);
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifemjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		lifemjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifemjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(lifemjaIO.getChdrcoy(),atmodrec.company)
		|| isNE(lifemjaIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
		}
	}

protected void statistics5000()
	{
		stats5000();
	}

protected void stats5000()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifsttrrec.lifsttrRec);
			sysrSyserrRecInner.sysrStatuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void writePtrn6000()
	{
		ptrn6000();
	}

protected void ptrn6000()
	{
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTransactionDate(ZERO);
		ptrnIO.setTransactionTime(ZERO);
		ptrnIO.setUser(ZERO);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(wsaaFlagsIndicatorsStoresInner.wsaaTranno);
		ptrnIO.setPtrneff(vstdIO.getEffdate());
		/* MOVE VSTD-EFFDATE           TO PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void updateBatchHead7000()
	{
		/*UPDATES*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			sysrSyserrRecInner.sysrStatuz.set(batcuprec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void dryProcessing7500()
	{
		start7510();
	}

protected void start7510()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlifIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75087600();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75087600();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		/* MOVE TDAY                   TO DTC1-FUNCTION.        <LA5184>*/
		/* CALL 'DATCON1'           USING DTC1-DATCON1-REC.     <LA5184>*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrlifIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(t1693rec.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(drypDryprcRecInner.drypDryprcRec);
			sysrSyserrRecInner.sysrStatuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75087600()
	{
		start7610();
	}

protected void start7610()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void releaseSoftlock8000()
	{
		softlock8000();
	}

protected void softlock8000()
	{
		/* Release Soft locked record.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(atmodrec.primaryKey);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(sftlockrec.sftlockRec);
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					fatal601();
				case errorProg610:
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatal601()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void errorProg610()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void stop699()
	{
		stopRun();
	}
/*
 * Class transformed  from Data Structure WSAA-FLAGS-INDICATORS-STORES--INNER
 */
private static final class WsaaFlagsIndicatorsStoresInner {

		/* WSAA-FLAGS-INDICATORS-STORES */
	private FixedLengthStringData wsaaCovrRiskFlag = new FixedLengthStringData(1);
	private Validator covrAtRisk = new Validator(wsaaCovrRiskFlag, "Y");
	private FixedLengthStringData wsaaValidCombination = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaT5673ContItem = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaInstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotInstprem = new ZonedDecimalData(17, 2);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTableSub = new PackedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNewCoverage = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLargestCoverage = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextCoverage = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaOldCurrfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRiskStatOk = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPremStatOk = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAnnyChangeDtls = new FixedLengthStringData(1);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData vstdbrkrec = new FixedLengthStringData(10).init("VSTDBRKREC");
	private FixedLengthStringData vstdrec = new FixedLengthStringData(10).init("VSTDREC");
	private FixedLengthStringData lifemjarec = new FixedLengthStringData(10).init("LIFEMJAREC");
	private FixedLengthStringData regtvstrec = new FixedLengthStringData(10).init("REGTVSTREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData covrrgprec = new FixedLengthStringData(10).init("COVRRGPREC");
	private FixedLengthStringData covrmatrec = new FixedLengthStringData(10).init("COVRMATREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData anbkrec = new FixedLengthStringData(10).init("ANBKREC");
	private FixedLengthStringData annyvf1rec = new FixedLengthStringData(10).init("ANNYVF1REC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
