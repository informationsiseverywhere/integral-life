package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Bnkdesc;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Bnkdescrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.dao.AnnypaypfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.life.annuities.procedures.Wthldng;
import com.csc.life.annuities.recordstructures.Wthldngrec;
import com.csc.life.annuities.screens.Sjl15ScreenVars;
import com.csc.life.annuities.tablestructures.Vpmannuregrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl15 extends ScreenProgCS {	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl15.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL15");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	/* ERRORS */
	private static final String F917 = "F917";
	private static final String JL44 = "JL44";
	private static final String JL45 = "JL45";
	private static final String JL46 = "JL46";
	private static final String JL47 = "JL47";
	private static final String JL48 = "JL48";
	private static final String JL49 = "JL49";
	private static final String JL50 = "JL50";
	private static final String JL51 = "JL51";
	private static final String JL52 = "JL52";
	private static final String JL53 = "JL53";
	private static final String JL54 = "JL54";
	private static final String JL64 = "JL64";
	private static final String JL66 = "JL66";
	private static final String RFS8 = "RFS8";
	private static final String E133 = "E133";
	private static final String E186 = "E186";	
	private static final String P233 = "P233";		
    private static final String G620 = "G620";	
	private static final String JL68 = "JL68";
	private static final String JL69 = "JL69";
	private static final String JL70 = "JL70";
	private static final String H893 = "H893";
	private static final String INSERT_FAILED_IN_ANNYPAYPF = "Failed to insert data in Annypaypf :";
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl15ScreenVars sv = ScreenProgram.getScreenVars(Sjl15ScreenVars.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Chdrpf chdrpf = new Chdrpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected Clntpf clntpf;	
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	protected ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private AnnyregpfDAO annyregpfDAO = getApplicationContext().getBean("annyregpfDAO" , AnnyregpfDAO.class);
	private AnnypaypfDAO annypaypfDAO = getApplicationContext().getBean("annypaypfDAO" , AnnypaypfDAO.class);
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);		
	protected Bnkdescrec bnkdescrec = new Bnkdescrec();
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private Tr386rec tr386rec = new Tr386rec();	
	private T5645rec t5645rec = new T5645rec();		
	private static final String T5688 = "T5688";
	private static final String T3629 = "T3629";
	private static final String TJL12 = "TJL12";
	private static final String T3588 = "T3588";
	private static final String T3623 = "T3623";
    private static final String T5645 = "T5645";
	private static final String PP = "PP";
	private static final String PI = "PI";	
	private static final String PT = "PT";
	private static final String AR = "AR";
	private static final String AL = "AL";
	private static final String FREQUENCY_YEARLY = "01";
	private static final String FREQUENCY_DAILY = "DY";
	private List<Clntpf> clntpfList = null;	
	private Batckey wsaaBatckey = new Batckey();	
	private Annyregpf annyregpf = new Annyregpf();
	private Gensswrec gensswrec = new Gensswrec();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);	
	private String wsaaAnnPmntSts;
	private PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);
	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private Datcon2rec datcon2rec = new Datcon2rec();
	private boolean ispermission = false;
	private FixedLengthStringData wsaaPayee = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPmntcurr = new FixedLengthStringData(3);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Batckey wsaaBatckey1 = new Batckey();	
	private AcmvpfDAO acmvpfDAO =  getApplicationContext().getBean("acmvDAO", AcmvpfDAO.class);	
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	public ZonedDecimalData wsaaDate1 = new ZonedDecimalData(8, 0);	
	private Annypaypf annypaypf = null;
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private Regppf regppf = null;
	protected List<Annypaypf> annypaypfList;	
	private boolean insertReg =false;
	private boolean changeReg =false;
	private FixedLengthStringData wsaaTask = new FixedLengthStringData(4).init("TASK");
	private FixedLengthStringData wsaaTasj = new FixedLengthStringData(4).init("TASJ");
	private FixedLengthStringData wsaaTasl = new FixedLengthStringData(4).init("TASL");
	private Wthldngrec wthldngrec = new Wthldngrec();	
	private Chdrpf chdrpfNew = new Chdrpf();
	private int tranno=0;

	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		EXIT2190,
		EXIT2219,
		EXIT2490,
		EXIT3490
    }

	public Pjl15() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl15", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}

	@Override
	protected void initialise1000() {
		try {
			initialise1010();	
			a7050Begin();
			protect1020();
			cont1030();
		} catch (GOTOException e) {
			LOGGER.info("initialise1000:", e);
		}
	}
	
	protected void initialise1010() {
		wsaaBatckey.set(wsspcomn.batchkey);			
		ispermission=FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "JPNCN001",appVars, "IT");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		sv.dataArea.set(SPACES);
		readT5645();
		chdrpfNew = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(),
				wsspcomn.chdrChdrnum.toString().trim());
		tranno=chdrpfNew.getTranno()+1;
		if (isNE(wsspcomn.flag, "I") && isNE(wsspcomn.flag, "M")) {			
			sv.annpmntopt.set(SPACES);
			sv.annupmnttrm.set(ZERO);
			sv.partpmntamt.set(ZERO);
			sv.annpmntfreq.set(wsspcomn.chdrRpfx);
			sv.dfrrdtrm.set(ZERO);
			sv.dfrrdamt.set(ZERO);
			sv.annupmntamt.set(ZERO);
			sv.fundsetupdt.set(varcom.vrcmMaxDate);
			sv.pmntstartdt.set(varcom.vrcmMaxDate);
			sv.docaccptdt.set(varcom.vrcmMaxDate);
			sv.frstpmntdt.set(varcom.vrcmMaxDate);
			sv.anniversdt.set(varcom.vrcmMaxDate);
			sv.finalpmntdt.set(varcom.vrcmMaxDate);
			sv.annpmntstatus.set(SPACES);
			sv.payee.set(SPACES);
			sv.payeename.set(SPACES);
			sv.pmntcurr.set(SPACES);
			sv.pmntcurrdesc.set(SPACES);
			sv.rgpymop.set(SPACES);
			sv.payeeaccnum.set(SPACES);
			sv.bankCd.set(SPACES);
			sv.branchCd.set(SPACES);
			sv.kanabank.set(SPACES);
			sv.kanabranch.set(SPACES);
			sv.bankaccdsc.set(SPACES);
			sv.bnkactyp.set(SPACES);
			wsaaAnnPmntSts = PP;
			sv.annpmntfreq.set(wsspcomn.chdrRpfx);
			retrvContract1020();
			loadHeader1030();
			loadPayeeData1040();
			loadContractPremCessDt1050();			
		}else {
			initialise1040();
		}
	}
	private void initialise1040() {
		if (isEQ(wsspcomn.chdrChdrnum, SPACE)) {
			sv.chdrnumErr.set(F917);
			return;
		}
		
		if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask)
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
		{		
			annyregpf = annyregpfDAO.getAnnyregpf(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
		}
		else
		{
			 annyregpf = annyregpfDAO.getAnnyregpfbyEffdt(wsspcomn.company.toString(),
					 wsspcomn.chdrChdrnum.toString().trim(), wsspcomn.effdate.toInt(),
					 wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
		}
		
		Optional<Annyregpf> isExists = Optional.ofNullable(annyregpf);
		if (isExists.isPresent()) {
			if (null != annyregpf.getChdrnum())
				sv.chdrnum.set(annyregpf.getChdrnum());
			retrvContract1020();
			if (null != annyregpf.getAnnpstcde()){			
				wsaaAnnPmntSts = annyregpf.getAnnpstcde();
			}
			loadHeader1030();			
			if (null != annyregpf.getTotannufund())
				sv.totannufund.set(annyregpf.getTotannufund());
			if (null != annyregpf.getAnnpmntopt())
				sv.annpmntopt.set(annyregpf.getAnnpmntopt());
			if (null != annyregpf.getAnnupmnttrm())
				sv.annupmnttrm.set(annyregpf.getAnnupmnttrm());
			if (null != annyregpf.getPartpmntamt())
				sv.partpmntamt.set(annyregpf.getPartpmntamt());
			if (null != annyregpf.getAnnpmntfreq())
				sv.annpmntfreq.set(annyregpf.getAnnpmntfreq());
			if (null != annyregpf.getDfrrdtrm())
				sv.dfrrdtrm.set(annyregpf.getDfrrdtrm());
			if (null != annyregpf.getDfrrdamt())
				sv.dfrrdamt.set(annyregpf.getDfrrdamt());
			if (null != annyregpf.getAnnupmntamt())
				sv.annupmntamt.set(annyregpf.getAnnupmntamt());
			if (null != annyregpf.getFundsetupdt())
				sv.fundsetupdt.set(annyregpf.getFundsetupdt());
			if (null != annyregpf.getPmntstartdt())
				sv.pmntstartdt.set(annyregpf.getPmntstartdt());
			if (null != annyregpf.getDocaccptdt())
				sv.docaccptdt.set(annyregpf.getDocaccptdt());			
			if (null != annyregpf.getFrstpmntdt())
				sv.frstpmntdt.set(annyregpf.getFrstpmntdt());
			if (null != annyregpf.getAnniversdt())
				sv.anniversdt.set(annyregpf.getAnniversdt());
			if (null != annyregpf.getFinalpmntdt())
				sv.finalpmntdt.set(annyregpf.getFinalpmntdt());
			
			if (null != annyregpf.getPayee()){
				sv.payee.set(annyregpf.getPayee());
				wsaaPayee.set(sv.payee);	//ILJ-751
				getPayeeName();
			}
			if (null != annyregpf.getPmntcurr()){
				sv.pmntcurr.set(annyregpf.getPmntcurr());
				String currdesc = getCurrDesc(sv.pmntcurr.toString().trim());		
				sv.pmntcurrdesc.set(currdesc);
			}			
			if (null != annyregpf.getRgpymop())
				sv.rgpymop.set(annyregpf.getRgpymop());
			if (null != annyregpf.getPayeeaccnum()){
				sv.payeeaccnum.set(annyregpf.getPayeeaccnum());
				loadBankDetails();
			}
		}else {
			sv.chdrnumErr.set(F917);
			return;
		}
	}
	
	private void protect1020() {
		if (isEQ(wsspcomn.flag, "I")) {
			sv.annpmntoptOut[Varcom.pr.toInt()].set("Y");
			sv.annupmnttrmOut[Varcom.pr.toInt()].set("Y");
			sv.partpmntamtOut[Varcom.pr.toInt()].set("Y");
			sv.annpmntfreqOut[Varcom.pr.toInt()].set("Y");
			sv.dfrrdtrmOut[Varcom.pr.toInt()].set("Y");
			sv.docaccptdtOut[Varcom.pr.toInt()].set("Y");
			sv.frstpmntdtOut[Varcom.pr.toInt()].set("Y");			
			sv.payeeOut[Varcom.pr.toInt()].set("Y");
			sv.payeenameOut[Varcom.pr.toInt()].set("Y");
			sv.pmntcurrOut[Varcom.pr.toInt()].set("Y");
			sv.pmntcurrdescOut[Varcom.pr.toInt()].set("Y");
			sv.rgpymopOut[Varcom.pr.toInt()].set("Y");
			sv.payeeaccnumOut[Varcom.pr.toInt()].set("Y");
		} 
		if (isEQ(wsspcomn.flag, "M")) {
			if(PP.equals(annyregpf.getAnnpstcde().trim())){
				sv.annpmntoptOut[Varcom.pr.toInt()].set("N");
				sv.annupmnttrmOut[Varcom.pr.toInt()].set("N");
				sv.partpmntamtOut[Varcom.pr.toInt()].set("N");
				sv.annpmntfreqOut[Varcom.pr.toInt()].set("N");
				sv.dfrrdtrmOut[Varcom.pr.toInt()].set("N");
				sv.docaccptdtOut[Varcom.pr.toInt()].set("N");
				sv.frstpmntdtOut[Varcom.pr.toInt()].set("N");			
				sv.payeeOut[Varcom.pr.toInt()].set("N");
				sv.payeenameOut[Varcom.pr.toInt()].set("N");
				sv.pmntcurrOut[Varcom.pr.toInt()].set("N");
				sv.pmntcurrdescOut[Varcom.pr.toInt()].set("N");
				sv.rgpymopOut[Varcom.pr.toInt()].set("N");
				sv.payeeaccnumOut[Varcom.pr.toInt()].set("N");
			}
			if(PI.equals(annyregpf.getAnnpstcde().trim())){
				sv.annpmntoptOut[Varcom.pr.toInt()].set("Y");
				sv.annupmnttrmOut[Varcom.pr.toInt()].set("Y");
				sv.partpmntamtOut[Varcom.pr.toInt()].set("Y");
				sv.annpmntfreqOut[Varcom.pr.toInt()].set("Y");
				sv.dfrrdtrmOut[Varcom.pr.toInt()].set("Y");
				sv.docaccptdtOut[Varcom.pr.toInt()].set("Y");
				sv.frstpmntdtOut[Varcom.pr.toInt()].set("Y");			
				sv.payeeOut[Varcom.pr.toInt()].set("N");
				sv.payeenameOut[Varcom.pr.toInt()].set("N");
				sv.pmntcurrOut[Varcom.pr.toInt()].set("Y");
				sv.pmntcurrdescOut[Varcom.pr.toInt()].set("Y");
				sv.rgpymopOut[Varcom.pr.toInt()].set("Y");
				sv.payeeaccnumOut[Varcom.pr.toInt()].set("N");
			}			
		}
	}
	
	protected void readT5645()
	{
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(T5645);
		itempf.setItemitem(wsaaProg.toString());
	    itempf.setItemseq("  ");
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(T5645).concat(wsaaProg.toString()));
			fatalError600();
		}else
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
	}
	
	protected void cont1030(){
		
		List<Annypaypf> annypaypfRecords;
		
		if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTasj)
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTask)
				||isEQ(wsaaBatckey.batcBatctrcde,wsaaTasl))
		{		
			 annypaypfRecords = annypaypfDAO.getAnnypaypfRecords(wsspcomn.company.toString(),
					 wsspcomn.chdrChdrnum.toString().trim());
			
		}
		else
		{
			 annypaypfRecords = annypaypfDAO.getAnnypaypfbyEffdt(wsspcomn.company.toString(), 
					 wsspcomn.chdrChdrnum.toString().trim(), wsspcomn.effdate.toInt(), 
					 wsspcomn.tranno.toInt(), wsspcomn.batctrcde.toString());
		}
		
		if(null == annypaypfRecords || annypaypfRecords.isEmpty()){
			sv.annpayplan.set("X");
		}else{
			sv.annpayplan.set("+");
		}
	}
	
	private void getPayeeName(){
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.payee.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || clntpfList.isEmpty())
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		sv.payeename.set(wsspcomn.longconfname);
	}
	protected void retrvContract1020()
	{	
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(Varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),Varcom.oK)&&isNE(chdrmjaIO.getStatuz(), Varcom.mrnf)) {//ILJ-552
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString());//ILJ-552
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}	
	}
	
	protected void loadHeader1030()
	{
		try {
			headings1031();					
		}
		catch (GOTOException e){
			LOGGER.info("loadHeader1030:", e);
		}
	}	
	
	protected void headings1031()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.occdate.set(chdrpf.getOccdate());
		sv.servbr.set(chdrpf.getCntbranch());
		sv.cownnum.set(chdrpf.getCownnum());
		readClientRecord(chdrpf.getCownnum());
		if(clntpf != null){//ILJ-552
			sv.cownername.set(clntpf.getSurname().trim().concat(", ").concat(clntpf.getLgivname()));
		}
		readLifeRecord();
		sv.annuitype.set(wsspcomn.servunit);
		sv.beneftype.set(wsspcomn.chdrstcdb);		
		getTotAnnuityAmt();
		sv.totannufund.set(wsaaCurbal);	
		loadLongDesc();
	}
	
	protected void loadPayeeData1040(){
		wsaaPayee.set(sv.cownnum);
		wsaaPmntcurr.set(chdrpf.getBillcurr());
		sv.payee.set(sv.cownnum);
		sv.payeename.set(sv.cownername);
		sv.pmntcurr.set(chdrpf.getBillcurr());
		String currdesc = getCurrDesc(chdrpf.getBillcurr());		
		sv.pmntcurrdesc.set(currdesc);
		sv.numsel.set(sv.payee);
	}
	
	protected void readClientRecord(String clntnum){
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), clntnum.trim());		
	}
	protected void readLifeRecord(){
		Lifepf lifepfModel = null;
		List<Lifepf> lifeList = lifepfDAO.getLifeList(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(lifeList == null){
			return;
		}else{
			lifepfModel=lifeList.get(0);
		}
		sv.lifenum.set(lifepfModel.getLifcnum());
		clntpf.setClntnum(lifepfModel.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpfList= clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || clntpfList.isEmpty())
		{
			return;
		}
		for(Clntpf clnt :clntpfList)
		{
			plainname(clnt);
		}
		sv.lifename.set(wsspcomn.longconfname);	
		sv.cltsex.set(lifepfModel.getCltsex());
	}
	
	protected void loadLongDesc(){
		Map<String, String> itemMap = new HashMap<>();		
		itemMap.put(TJL12, wsaaAnnPmntSts);
		itemMap.put(T5688, chdrpf.getCnttype());
		itemMap.put(T3623, chdrpf.getStatcode());
		itemMap.put(T3588, chdrpf.getPstcde());
		Map<String, Descpf> descMap = descdao.searchMultiDescpf(wsspcomn.company.toString(),
				wsspcomn.language.toString(), itemMap);
		if (!descMap.containsKey(T5688)) {			
			sv.ctypedes.set(SPACES);
		} else {
			sv.ctypedes.set(descMap.get(T5688).getLongdesc());
		}
		if (!descMap.containsKey(TJL12)) {			
			sv.annpmntstatus.set(SPACES);
		} else {
			sv.annpmntstatus.set(descMap.get(TJL12).getLongdesc());
		}
	}
	
	protected void loadBankDetails(){
		if(isNE(sv.payeeaccnum, SPACES)){
			Clbapf clbapf = new Clbapf();
			clbapf.setBankacckey(sv.payeeaccnum.toString().trim());
			clbapf.setClntnum(sv.payee.toString().trim());
			List<Clbapf> clbapfList = clbapfDAO.getRecordByBankAccKey(clbapf);
			if(null != clbapfList && !clbapfList.isEmpty()){
				Clbapf clbapfRecord = clbapfList.get(0);
				bnkdescrec.bankkey.set(clbapfRecord.getBankkey());
				callProgram(Bnkdesc.class, bnkdescrec.bnkdescRec);
				if (isNE(bnkdescrec.statuz, Varcom.oK)) {
					syserrrec.statuz.set(bnkdescrec.statuz);
					fatalError600();
				}
				if(ispermission){
					String bankKey = clbapfRecord.getBankkey().trim();
					String[] bankKeyArr = bankKey.split(" ", 2);
					sv.bankCd.set(bankKeyArr[0].trim());
					if(bankKeyArr.length >1)
						sv.branchCd.set(bankKeyArr[1].trim());
					sv.kanabank.set(bnkdescrec.kanabank);
					sv.kanabranch.set(bnkdescrec.kanabranch);
					sv.bankaccdsc.set(clbapfRecord.getBankaccdsc());
					sv.bnkactyp.set(clbapfRecord.getBnkactyp());
				}				
			}
		}
	}
	
	private String getCurrDesc(String billCurr){
		return iafDescDAO.getItemLongDesc("IT", wsspcomn.company.toString().trim(), T3629, billCurr,
				wsspcomn.language.toString());		
	}
	
	protected void plainname(Clntpf clntpf)
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return ;
		}		
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
	
	protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
	
	
	protected void getTotAnnuityAmt(){
		wsaaCurbal.set(ZERO);
		Acmvpf acmvpf = new Acmvpf();
		acmvpf.setRldgacct(wsspcomn.chdrChdrnum.toString().trim());
		acmvpf.setRldgcoy(wsspcomn.company.toString().trim().charAt(0));
		acmvpf.setSacscode(t5645rec.sacscode01.toString());
		acmvpf.setSacstyp(t5645rec.sacstype01.toString());
		List<Acmvpf> acmvpfList = acmvpfDAO.searchAcmvpfRecord(acmvpf);
		if(acmvpfList !=null && !acmvpfList.isEmpty()){
			for(Acmvpf acmv : acmvpfList){
				if (isEQ(acmv.getFrcdate(), varcom.vrcmMaxDate)) {
					wsaaCurbal.set(acmv.getAcctamt().longValue());
					break;
				}
			}
		}else
		{
			wsaaCurbal.set(ZERO);
		}
	}
	
	@Override
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(Varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}
		return;
		
	}
	
	@Override
	protected void screenEdit2000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		screenIo2001();		
		validate2010();
		validatePayee2020();
		calcAnniversDt2020();
		loadBankDetails();
		calcAnnuPymntAmt();
		validateSelectionFields2070();
	}
	
	protected void screenIo2001() {
		wsspcomn.edterror.set(Varcom.oK);
	}
	
	private void validate2010() {		
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);		
		wsaaToday.set(datcon1rec.intDate);
		if(isNE(sv.payee, SPACES) && isNE(sv.payee, wsaaPayee)){							
			wsaaPayee.set(sv.payee);
			sv.numsel.set(sv.payee);
			sv.payeeaccnum.set(SPACES); //ILJ-686
			getPayeeName();
		}
		if(isEQ(sv.payee, SPACES))	{
			sv.payeename.set(SPACES);
			if(isNE(sv.payeeaccnum, SPACES)) {
				sv.payeeaccnum.set(SPACES);	 //ILJ-751
			}
		} else if(isEQ(sv.payeeaccnum, SPACES))	{
			getPayeeName();
		}
		if(isNE(sv.pmntcurr, SPACES) && isNE(sv.pmntcurr, wsaaPmntcurr)){			
			wsaaPmntcurr.set(sv.pmntcurr);				
			sv.pmntcurrdesc.set(getCurrDesc(sv.pmntcurr.toString().trim()));			
		}		
		if(isEQ(wsspcomn.flag, "M") && PI.equals(annyregpf.getAnnpstcde().trim())){
			return;
		}
		if (isEQ(sv.annpmntopt, SPACE)){
			sv.annpmntoptErr.set(JL44);
		}						
		if ((isEQ(sv.annpmntopt, "02") || isEQ(sv.annpmntopt, "03")) && isEQ(sv.annupmnttrm, ZERO)){			
			sv.annupmnttrmErr.set(JL45);
		}
		if ((isEQ(sv.annpmntopt, "01") || isEQ(sv.annpmntopt, "04") || isEQ(sv.annpmntopt, "05")) 
				&& isNE(sv.annupmnttrm, ZERO)){			
			sv.annupmnttrmErr.set(JL46);
		}
		if ((isEQ(sv.annpmntopt, "01") || isEQ(sv.annpmntopt, "04") || isEQ(sv.annpmntopt, "05"))
				&& isNE(sv.annpmntfreq, SPACE)){			
			sv.annpmntfreqErr.set(JL68);
		}
		if ((isEQ(sv.annpmntopt, "03") || isEQ(sv.annpmntopt, "05")) && isEQ(sv.partpmntamt, ZERO)){			
			sv.partpmntamtErr.set(JL47);
		}
		if ((isEQ(sv.annpmntopt, "03") || isEQ(sv.annpmntopt, "05")) && isGT(sv.partpmntamt, sv.totannufund)){			
			sv.partpmntamtErr.set(JL66);
		}
		if ((isEQ(sv.annpmntopt, "01") || isEQ(sv.annpmntopt, "02") || isEQ(sv.annpmntopt, "04"))
				&& isNE(sv.partpmntamt, ZERO)){			
			sv.partpmntamtErr.set(JL48);
		}
		if ((isEQ(sv.annpmntopt, "02") || isEQ(sv.annpmntopt, "03")) && isEQ(sv.annpmntfreq, SPACE)){
			sv.annpmntfreqErr.set(JL49);
		}
		if ((isEQ(sv.annpmntopt, "04") || isEQ(sv.annpmntopt, "05")) && isEQ(sv.dfrrdtrm, ZERO)){			
			sv.dfrrdtrmErr.set(JL50);
		}
		if ((isEQ(sv.annpmntopt, "01") || isEQ(sv.annpmntopt, "02") || isEQ(sv.annpmntopt, "03")) 
				&& isNE(sv.dfrrdtrm, ZERO)){			
			sv.dfrrdtrmErr.set(JL51);
		}
		if (isEQ(sv.annpmntopt, "05")){				
			compute(sv.dfrrdamt, 2).set(sub(sv.totannufund, sv.partpmntamt));
		}else
			if (isEQ(sv.annpmntopt, "04")){				
				sv.dfrrdamt.set(sv.totannufund);
			}else{
				sv.dfrrdamt.set(ZERO);
			}
		
		if (isEQ(sv.docaccptdt, varcom.vrcmMaxDate)){				
			sv.docaccptdtErr.set(JL52);
		}
		if (isNE(sv.docaccptdt, varcom.vrcmMaxDate) && isGT(sv.docaccptdt, wsaaToday)) {
			sv.docaccptdtErr.set(RFS8);
		}
		if (isEQ(sv.frstpmntdt, varcom.vrcmMaxDate)){				
			sv.frstpmntdtErr.set(JL53);
		}
		if (isNE(sv.frstpmntdt, varcom.vrcmMaxDate) && isGT(sv.frstpmntdt, sv.finalpmntdt)){				
			sv.frstpmntdtErr.set(JL69);
		}
		if (isEQ(sv.annpmntopt, "04") && isNE(sv.frstpmntdt, sv.finalpmntdt)){				
			sv.frstpmntdtErr.set(JL70);
		}
		if (isNE(sv.frstpmntdt, varcom.vrcmMaxDate) && isLT(sv.frstpmntdt, wsaaToday)) {
			sv.frstpmntdtErr.set(JL54);
		}
		if (isEQ(sv.annpmntopt, "01") && isNE(sv.frstpmntdt, varcom.vrcmMaxDate)){				
			sv.finalpmntdt.set(sv.frstpmntdt);
		}		
		if ((isEQ(sv.annpmntopt, "02") || isEQ(sv.annpmntopt, "03"))
				&& isNE(sv.annupmnttrm, ZERO) && isNE(sv.pmntstartdt, varcom.vrcmMaxDate) && isNE(sv.annpmntfreq, SPACE)){				
			calFinalpmntdt();
		}
		if ((isEQ(sv.annpmntopt, "04") || isEQ(sv.annpmntopt, "05"))
				&& isNE(sv.dfrrdtrm, ZERO) && isNE(sv.pmntstartdt, varcom.vrcmMaxDate)){				
			calFinalpmntdtForDfrd();
		}			
	}
	
	private void validatePayee2020() {	
		if (isEQ(sv.rgpymop, "B") && isEQ(sv.payee, SPACE)){				
			sv.payeeErr.set(E133);		
		}
		if (isEQ(sv.pmntcurr, SPACE)){				
			sv.pmntcurrErr.set(JL64);			
		}
		if (isEQ(sv.rgpymop, SPACE)){				
			sv.rgpymopErr.set(E186);		
		}
		if (isNE(sv.rgpymop, SPACE) && isNE(sv.rgpymop, "B")){				
			sv.rgpymopErr.set(P233);		
		}		
		if (isEQ(sv.rgpymop, "B") && isEQ(sv.payeeaccnum, SPACE)){				
			sv.payeeaccnumErr.set(E186);
		}
		if (isEQ(sv.rgpymop, SPACE) && isNE(sv.payeeaccnum, SPACE)){				
			sv.payeeaccnumErr.set(H893);
		}
		
		if (isNE(sv.errorIndicators, SPACES))
			wsspcomn.edterror.set("Y");

		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	protected void validateSelectionFields2070()
	{
		if (isNE(sv.annpayplan, "X")
				&& isNE(sv.annpayplan, "+")
				&& isNE(sv.annpayplan, " ")) {
			sv.annpayplanErr.set(G620);
		}
	}
	
	protected void calFinalpmntdt()
	{
		if (isEQ(sv.annupmnttrm, 0)) {
			return ;
		}
		wsaaDate1.set(sv.pmntstartdt);
		compute(wsaaCount, 0).set(mult(sv.annpmntfreq.toInt(),sv.annupmnttrm));
		for(int i=1; i<wsaaCount.toInt() ; i++) {
		 calcNextPmntDt();
		 wsaaDate1.set(datcon2rec.intDate2);
		}
		
		sv.finalpmntdt.set(wsaaDate1);

	}
	
	protected void calFinalpmntdtForDfrd()
	{
		if (isEQ(sv.dfrrdtrm, 0)) {
			return ;
		}
		datcon2rec.freqFactor.set(sv.dfrrdtrm.toInt());
		datcon2rec.intDate1.set(sv.pmntstartdt);
		datcon2rec.frequency.set(FREQUENCY_YEARLY);
		callDatcon2();
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			sv.finalpmntdt.set(datcon2rec.statuz);
			return ;
		}
		sv.finalpmntdt.set(datcon2rec.intDate2);
	}
	
	protected void calcAnnuPymntAmt(){
		try {
			if ((isEQ(sv.annpmntopt, "02") || isEQ(sv.annpmntopt, "03")) 
					&& isNE(sv.annupmnttrm, ZERO) && isNE(sv.annpmntfreq, SPACE)){
				getVpmsAnnupmntamt();
			}else{
				sv.annupmntamt.set(ZERO);
			}
		} catch (Exception e) {		
			LOGGER.info("calcAnnuPymntAmt:", e);
			sv.annupmntamt.set(ZERO);
		}
	}
	
	protected void callDatcon2()
	{
		/*PARA*/		
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	
	private void getVpmsAnnupmntamt(){
		Vpmannuregrec vpmannuregrec = new Vpmannuregrec();		
		vpmannuregrec.cnttype.set(sv.cnttype);	
		vpmannuregrec.annpmntopt.set(sv.annpmntopt);		
		vpmannuregrec.annpartpmnt.set(sv.partpmntamt);
		vpmannuregrec.annpmntfreq.set(sv.annpmntfreq);
		vpmannuregrec.annpmnttrm.set(sv.annupmnttrm);
		vpmannuregrec.totannfund.set(sv.totannufund);			
		if((AppVars.getInstance().getAppConfig().isVpmsEnable() && ExternalisedRules.isCallExternal("VPMANNUREG")))
		{
			callProgram("VPMANNUREG",vpmannuregrec);			
    		if (isNE(vpmannuregrec.statuz, Varcom.oK)) {
    			sv.annupmntamtErr.set(vpmannuregrec.statuz);		
    			goTo(GotoLabel.EXIT2490);
    		}else{
				sv.annupmntamt.set(vpmannuregrec.outputannpmntamt);
			}
			
		}
	}
	protected void loadContractPremCessDt1050()
	{
		List<Covrpf> covrpfList = covrpfDAO.getCovrsurByComAndNum(wsspcomn.company.toString(), sv.chdrnum.toString());
		if(!covrpfList.isEmpty())
		{
			Covrpf covrpf = covrpfList.get(0);
			if(isNE(covrpf.getPremCessDate(),varcom.vrcmMaxDate) 
					&& isNE(covrpf.getPremCessDate(),ZERO)
					&& isNE(covrpf.getPremCessDate(),SPACES))		
				sv.fundsetupdt.set(covrpf.getPremCessDate());
			else
				sv.fundsetupdt.set(varcom.vrcmMaxDate);
		}
		else
			sv.fundsetupdt.set(varcom.vrcmMaxDate);
		sv.pmntstartdt.set(sv.fundsetupdt);
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);		
		wsaaToday.set(datcon1rec.intDate);
		sv.docaccptdt.set(wsaaToday);	
	}
	
	protected void calcAnniversDt2020()
	{
		if ((isEQ(sv.annpmntopt, "01") || isEQ(sv.annpmntopt, "04") || isEQ(sv.annpmntopt, "05"))){	
			sv.anniversdt.set(varcom.vrcmMaxDate);
			return;
		}
		if(isEQ(sv.annpmntfreq, SPACES) || isEQ(sv.frstpmntdt, varcom.vrcmMaxDate)){
			return;
		}
		if(!(PI.equals(annyregpf.getAnnpstcde())))
		{
			if(isEQ(sv.frstpmntdt, sv.finalpmntdt)){
				sv.anniversdt.set(sv.finalpmntdt);
				return;
			}
			ZonedDecimalData wsaaDate2 = new ZonedDecimalData(8, 0);
			wsaaDate2.set(sv.pmntstartdt);
			while(isGTE(sv.frstpmntdt, wsaaDate2)){	
				calcNxtDt(wsaaDate2);				
				wsaaDate2.set(datcon2rec.intDate2);	
			}
			sv.anniversdt.set(wsaaDate2);
		}
	}		
	
	@Override
	protected void update3000() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}		
		
		updateAnnyreg3010();	
		updateAnnypaypf3040();
		
		if (isEQ(wsspcomn.flag, "M")&&(
				isEQ(sv.annpmntopt.trim(),"02")||
				isEQ(sv.annpmntopt.trim(),"03"))) {
				updateWitholdingTax();	
		}
		
		updateRegpf3060();
		if (isEQ(sv.annpayplan, "X")) {
			return ;
		}else{
			updateChdrRecord3020();
			writePtrn3030();		
		}
	}
	
	protected void loadWsspFields3010()
	{
		/*     SPECIAL EXIT PROCESSING*/
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.EXIT3490);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.EXIT3490);
		}
		if (isEQ(sv.annpayplan, "X")) {
			goTo(GotoLabel.EXIT3490);
		}
	}

	
	protected void updateAnnyreg3010()
	{
		annyregpf = annyregpfDAO.getAnnyregpf(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
		Optional<Annyregpf> isExists = Optional.ofNullable(annyregpf);
		if (!isExists.isPresent()) {
			updateDatabase3010();
		}
		else
		{
			if(PI.equals(annyregpf.getAnnpstcde()))
			{
				   insertReg= false;
				   changeReg=false;
				   if(isNE(sv.payeeaccnum.trim(),annyregpf.getPayeeaccnum().trim())
						   || isNE(sv.payee.trim(),annyregpf.getPayee().trim()))
				   {	
				   	   changeReg=true;
				   }
				   
				   if(annyregpf.getTranno()!=tranno)
				   {
					   if(changeReg)
					   {
						   updateDatabase3020();
					   	   updateRegppf();
					   }
					   else
					   {
						   updateDatabase3020();
					   }
				   }
				   else
				   {
					   if(changeReg)
					   {
						   updateDatabase3070();
					   	   updateRegppf();
					   }
					   else
					   {
						   updateDatabase3070();
					   }
				   }
				  
			}
			else if (PP.equals(annyregpf.getAnnpstcde()))
			{
				insertReg = annyregChange(annyregpf);
				
				  if(annyregpf.getTranno()!=tranno)
				   {
					   updateDatabase3020();
				   }
				   else
				   {
					   if(insertReg)
					   {
						   updateDatabase3070();
					   }
				   }
	
			   }
		}
	}
	
	protected void updateRegppf()
	{
			regppf= new Regppf();
			regppf.setChdrcoy(wsspcomn.company.toString());
			regppf.setChdrnum(wsspcomn.chdrChdrnum.toString().trim());
			regppf.setPayclt(sv.payee.toString());
			regppf.setBankacckey(sv.payeeaccnum.toString().trim());
			regppf.setBankkey(bnkdescrec.bankkey.toString().trim());
			regpDAO.updatePayee(regppf);

	}
	
	
	private void updateDatabase3070() {
		annyregpf.setAnnuitype(sv.annuitype.toString());
		annyregpf.setBeneftype(sv.beneftype.toString());
		annyregpf.setAnnpmntopt(sv.annpmntopt.toString());
		annyregpf.setAnnupmnttrm(sv.annupmnttrm.toInt());			
		annyregpf.setTotannufund(sv.totannufund.getbigdata());
		annyregpf.setPartpmntamt(sv.partpmntamt.getbigdata());
		annyregpf.setDfrrdamt(sv.dfrrdamt.getbigdata());
		annyregpf.setAnnupmntamt(sv.annupmntamt.getbigdata());
		annyregpf.setDfrrdtrm(sv.dfrrdtrm.toInt());
		annyregpf.setAnnpmntfreq(sv.annpmntfreq.toString());
		annyregpf.setFundsetupdt(sv.fundsetupdt.toInt());
		annyregpf.setPmntstartdt(sv.pmntstartdt.toInt());
		annyregpf.setDocaccptdt(sv.docaccptdt.toInt());
		annyregpf.setFrstpmntdt(sv.frstpmntdt.toInt());
		annyregpf.setAnniversdt(sv.anniversdt.toInt());
		annyregpf.setFinalpmntdt(sv.finalpmntdt.toInt());			
		annyregpf.setAnnpstcde(wsaaAnnPmntSts);
		annyregpf.setPayee(sv.payee.toString());
		annyregpf.setPmntcurr(sv.pmntcurr.toString());
		annyregpf.setRgpymop(sv.rgpymop.toString());
		annyregpf.setPayeeaccnum(sv.payeeaccnum.toString());
		annyregpf.setBankCd(sv.bankCd.toString());
		annyregpf.setBranchCd(sv.branchCd.toString());
		annyregpf.setBankaccdsc(sv.bankaccdsc.toString());
		annyregpf.setBnkactyp(sv.bnkactyp.toString());			
		annyregpf.setUsrprf(wsspcomn.username.toString());
		annyregpf.setDatime(new Timestamp(new Date().getTime()));
		try {
			annyregpfDAO.updateAnnyregpf(annyregpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert data in Annyregpf :", ex);
		}
	}		
	
	
	// Insert
		private void updateDatabase3010() {
			
			annyregpf= new Annyregpf();
			
			annyregpf.setChdrnum(sv.chdrnum.toString());
			annyregpf.setChdrcoy(wsspcomn.company.toString().trim());
			annyregpf.setOccdate(sv.occdate.toInt());
			annyregpf.setCntbranch(sv.servbr.toString());
			annyregpf.setCownnum(sv.cownnum.toString());
			annyregpf.setLifecnum(sv.lifenum.toString());
			annyregpf.setCltsex(sv.cltsex.toString());
			annyregpf.setAnnuitype(sv.annuitype.toString());
			annyregpf.setBeneftype(sv.beneftype.toString());
			annyregpf.setAnnpmntopt(sv.annpmntopt.toString());
			annyregpf.setAnnupmnttrm(sv.annupmnttrm.toInt());			
			annyregpf.setTotannufund(sv.totannufund.getbigdata());
			annyregpf.setPartpmntamt(sv.partpmntamt.getbigdata());
			annyregpf.setDfrrdamt(sv.dfrrdamt.getbigdata());
			annyregpf.setAnnupmntamt(sv.annupmntamt.getbigdata());
			annyregpf.setDfrrdtrm(sv.dfrrdtrm.toInt());
			annyregpf.setAnnpmntfreq(sv.annpmntfreq.toString());
			annyregpf.setFundsetupdt(sv.fundsetupdt.toInt());
			annyregpf.setPmntstartdt(sv.pmntstartdt.toInt());
			annyregpf.setDocaccptdt(sv.docaccptdt.toInt());
			annyregpf.setFrstpmntdt(sv.frstpmntdt.toInt());
			annyregpf.setAnniversdt(sv.anniversdt.toInt());
			annyregpf.setFinalpmntdt(sv.finalpmntdt.toInt());			
			annyregpf.setAnnpstcde(wsaaAnnPmntSts);
			annyregpf.setValidflag("1");
			annyregpf.setEffectivedt(datcon1rec.intDate.toInt());
			annyregpf.setTranno(tranno);
			annyregpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
			annyregpf.setPayee(sv.payee.toString());
			annyregpf.setPmntcurr(sv.pmntcurr.toString());
			annyregpf.setRgpymop(sv.rgpymop.toString());
			annyregpf.setPayeeaccnum(sv.payeeaccnum.toString());
			annyregpf.setBankCd(sv.bankCd.toString());
			annyregpf.setBranchCd(sv.branchCd.toString());
			annyregpf.setBankaccdsc(sv.bankaccdsc.toString());
			annyregpf.setBnkactyp(sv.bnkactyp.toString());			
			annyregpf.setUsrprf(wsspcomn.username.toString());
			annyregpf.setDatime(new Timestamp(new Date().getTime()));
			try {
				annyregpfDAO.insertAnnyregpf(annyregpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to insert data in Annyregpf :", ex);
			}
		}

		// Modify
		private void updateDatabase3020() {
			
			try {
				annyregpfDAO.updateValidflag(annyregpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to update validflag in Annyregpf :", ex);
			}
			
			updateDatabase3010();
		}		
		
		protected BigDecimal getWthldngtax(int pmntdt){
			
			BigDecimal wthldngtaxamt = BigDecimal.ZERO;
			
			wthldngrec.chdrnum.set(sv.chdrnum.toString());
			clntpf.setClntcoy(wsspcomn.fsuco.toString());
			clntpf.setClntnum(sv.payee.toString());
			clntpf.setClntpfx("CN");
			clntpfList = clntpfDAO.readClientpfData(clntpf);
			clntpf = clntpfList.get(0);
			wthldngrec.payeetyp.set(clntpf.getClttype());
			wthldngrec.chdrcoy.set(wsspcomn.company.toString().trim());
			wthldngrec.chdrnum.set(sv.chdrnum);
			wthldngrec.annpmntfreq.set(sv.annpmntfreq);
			wthldngrec.annpmntopt.set(sv.annpmntopt);
			wthldngrec.annupmnttrm.set(sv.annupmnttrm);
			wthldngrec.cnttype.set(sv.cnttype);
			wthldngrec.cownnum.set(sv.cownnum);
			wthldngrec.paymtdt.set(pmntdt);
			wthldngrec.payee.set(sv.payee);
			wthldngrec.totannufund.set(sv.totannufund);
			if(isEQ(sv.annpmntopt,"03")){
				wthldngrec.partialpmnt.set(sv.partpmntamt.getbigdata());
			}
			else{
				wthldngrec.partialpmnt.set(0);
			}
			
			wthldngrec.currncy.set(sv.pmntcurr);
			
			callProgram(Wthldng.class, wthldngrec.wthldngRec);
			if(isEQ(wthldngrec.statuz,Varcom.oK)){
				wthldngtaxamt = wthldngrec.wthldngtaxamt.getbigdata();
			}
			else{
				syserrrec.statuz.set(wthldngrec.statuz);
				fatalError600();
			}
			return wthldngtaxamt;
		}
		protected void updateWitholdingTax(){
			BigDecimal witholdtax;
				
			List<Annypaypf> annpaypfrecords = annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(),
													wsspcomn.chdrChdrnum.toString().trim(),PP);
			for(Annypaypf annpaypf : annpaypfrecords ){
				if(isEQ(sv.payee.toString().trim(),sv.cownnum.toString().trim())){
					witholdtax = getWthldngtax(annpaypf.getPmntdt());
				}
				else{
					witholdtax = BigDecimal.ZERO;
				}
				annpaypf.setWitholdtax(witholdtax);
				annypaypfDAO.updatewthldngtax(annpaypf);
				//ILJ-686 starts
				updateannamt(annpaypf.getAnnaamt(),annpaypf.getWitholdtax(),annpaypf.getPmntno());
				
			}
			
		}
		protected void updateannamt(BigDecimal annaamt,BigDecimal witholdingtax, int pmntno){
			
			List<Regppf> regppflist = regpDAO.readRegpRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString());
			for(Regppf regppf1 : regppflist ){
				if(isEQ(regppf1.getRgpynum(),pmntno)){
					regppf1.setPymt(annaamt.subtract(witholdingtax));
					 regpDAO.updatepymnt(regppf1);
				}
			}	
		}
		//ILJ-686 ends
		protected void updateChdrRecord3020(){					
			chdrpf = chdrpfDAO.updateChdrpfTranlusedPstcdeTranno(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), AR);
		} 
		
		@Override
		protected void whereNext4000() {
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
				wsaaX.set(wsspcomn.programPtr);
				wsaaY.set(0);
				for (int loopVar1 = 0; loopVar1 != 8; loopVar1 += 1) {
					saveProgram4100();
				}
			}
			if (isEQ(sv.annpayplan, "X")) {
				sv.annpayplan.set("?");
				gensswrec.function.set("A");
				callGenssw4300();
				return ;
			}
			if (isEQ(sv.annpayplan, "?")) {
				annuityRet4900();
			}			
			endChoices4050();		
		}

		protected void callGenssw4300() {
			callSubroutine4310();
		}

		protected void callSubroutine4310() {
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, Varcom.oK) && isNE(gensswrec.statuz, Varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}

			if (isEQ(gensswrec.statuz, Varcom.mrnf)) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
				scrnparams.errorCode.set("H093");
				wsspcomn.nextprog.set(scrnparams.scrname);
				return;
			}
			compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
			wsaaY.set(1);
			for (int loopVar3 = 0; loopVar3 != 8; loopVar3 += 1) {
				loadProgram4400();
			}
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
		}

		protected void loadProgram4400() {
			wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
			wsaaX.add(1);
			wsaaY.add(1);
		}

		protected void endChoices4050() {
			/*    No more selected (or none)*/
			/*       - restore stack form wsaa to wssp*/
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsaaX.set(wsspcomn.programPtr);
				wsaaY.set(0);
				for (int loopVar2 = 0; loopVar2 != 8; loopVar2 += 1){
					restoreProgram4200();
				}
			}
			/*    If current stack action is * then re-display screen*/
			/*       (in this case, some other option(s) were requested)*/
			/*    Otherwise continue as normal.*/
		

			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.nextprog.set(scrnparams.scrname);
			} else {
				wsspcomn.programPtr.add(1);
			}
		}
		
		protected void restoreProgram4200()
		{
			/*RESTORE*/
			wsaaX.add(1);
			wsaaY.add(1);
			wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
			/*EXIT*/
		}

		protected void saveProgram4100() {
			/* SAVE */
			wsaaX.add(1);
			wsaaY.add(1);
			wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
			/* EXIT */
		}
		
		protected void annuityRet4900()
		{
			/*PARA*/
			/* On return from this  request, the current stack "action" will   */
			/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
			/* handle the return from options and extras:                      */
			/*  - calculate  the  premium  as  described  above  in  the       */
			/*       'Validation' section, and  check  that it is within       */
			/*       the tolerance limit,                                      */
			sv.annpayplan.set("+");
			
			/*  - restore the saved programs to the program stack              */
			compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
			wsaaY.set(1);
			for (int loopVar10 = 0; loopVar10 != 8; loopVar10 += 1){
				restore4610();
			}
			/*  - blank  out  the  stack  "action"                             */
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*       Set  WSSP-NEXTPROG to  the current screen                 */
			/*       name (thus returning to re-display the screen).           */
			wsspcomn.nextprog.set(scrnparams.scrname);
			/* Turn the protect switch off:                                 */
			sv.annpayplanOut[Varcom.pr.toInt()].set("N");
			/*EXIT*/
		}
		
		protected void restore4610()
		{
			/*PARA*/
			wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
			wsaaY.add(1);
			wsaaX.add(1);
			/*EXIT*/
		}
		
		protected void a7050Begin()
		{
			wsaaTr386Lang.set(wsspcomn.language);
			wsaaTr386Pgm.set(wsaaProg);
			Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
			
			tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
			if (isEQ(wsspcomn.sbmaction, "A")) {
				sv.scrndesc.set(tr386rec.progdesc[1].toString());
			} else if (isEQ(wsspcomn.sbmaction, "B")) {
				sv.scrndesc.set(tr386rec.progdesc[2].toString());
			}
			else if (isEQ(wsspcomn.sbmaction, "I")) {
				sv.scrndesc.set(tr386rec.progdesc[3].toString());
			}
		}
		
		protected void writePtrn3030() {

			datcon1rec.function.set(Varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			Ptrnpf ptrnpf = new Ptrnpf();
			ptrnpf.setChdrpfx("CH");
			ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
			ptrnpf.setChdrnum(chdrpf.getChdrnum());
			ptrnpf.setTranno(chdrpf.getTranno());
			ptrnpf.setTrdt(varcom.vrcmDate.toInt());
			ptrnpf.setTrtm(varcom.vrcmTime.toInt());
			ptrnpf.setTermid(varcom.vrcmTermid.toString());
			ptrnpf.setUserT(varcom.vrcmUser.toInt());
			if(isEQ(wsaaBatckey.batcBatctrcde,wsaaTask))
			{	
				annypaypfList=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PP); 
				if(!annypaypfList.isEmpty())
				{
					ptrnpf.setPtrneff(annypaypfList.get(0).getPmntdt());
				}
			}
			else
			{	
				ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
			}
			ptrnpf.setDatesub(datcon1rec.intDate.toInt());
			wsaaBatckey1.set(wsspcomn.batchkey);
			ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
			ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
			ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
			ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
			ptrnpf.setBatccoy(wsaaBatckey1.batcBatccoy.toString());
			ptrnpf.setBatcpfx(wsaaBatckey1.batcBatcpfx.toString());
			ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
			List<Ptrnpf> list = new ArrayList<>();
			list.add(ptrnpf);
			if (!ptrnpfDAO.insertPtrnPF(list)) {
				syserrrec.params.set(chdrpf.getChdrcoy().toString()
						.concat(chdrpf.getChdrnum()));
				fatalError600();
			}
		}
		
	protected void gensww4010() 
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, Varcom.oK)
		&& isNE(gensswrec.statuz, Varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, Varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set("H093");
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    Load from gensw to WSSP.*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; loopVar3 != 8; loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}
	
	protected void updateAnnypaypf3040(){							
		wsaaTranno.set(ZERO);	
		wsaaCount.set(ZERO);		
		// Return if Annuity Payment Status found "Payment Pending" and "Payment Terminated"
		if( null!= annyregpf && (PT.equals(annyregpf.getAnnpstcde()))) {
			return;
		}
	
		// Return if frequency is "Daily"
		if(isEQ(sv.annpmntfreq, FREQUENCY_DAILY)){
			return;
		}	
		
		annypaypfList=annypaypfDAO.getAnnypaypfRecords(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim()); 
		if(!annypaypfList.isEmpty())
		{	
			if( null!= annyregpf && (PP.equals(annyregpf.getAnnpstcde())))
			{
			   
			   if(annypaypfList.get(0).getTranno()!=tranno)
				{	
					if(insertReg)
					{
						 updateAnnypaypf3020();
						 insertAnnypaypf();
					}
					else
					{
						updateAnnypaypf3060();
					}
				 
				}
			   else
			   {
				   if(insertReg)
					{
					   	deleteAnnypaypf3020();
						insertAnnypaypf();
					}
			   }
			}
			
			else if(null!= annyregpf && (PI.equals(annyregpf.getAnnpstcde())))
			{
				if(annypaypfList.get(0).getTranno()!=tranno)
				{	
					if(changeReg)
					{
						updateAnnypaypf3060();
						changeReg=false;
					}
					else
					{
						updateAnnypaypf3060();
					}
				 
				}
				
			}
			
		}
		else
		{
			insertAnnypaypf();
		}
		
	  
	} 
	
	protected void updateAnnypaypf3060()
	{
		List<Annypaypf> annypaypfList1;
		
		annypaypfList1 = annypaypfDAO.getAnnypaypfRecords(wsspcomn.company.toString(),
				 wsspcomn.chdrChdrnum.toString().trim());
		if(!annypaypfList1.isEmpty())
		{
			for (int i = 0; i < annypaypfList1.size(); i++) {
				
				annypaypf = annypaypfList1.get(i);
				annypaypfDAO.updateValidflg(annypaypf);

				Annypaypf annypaypfNew1 = annypaypf;

				annypaypfNew1.setEffectivedt(datcon1rec.intDate.toInt());
				annypaypfNew1.setValidflag("1");
				annypaypfNew1.setTranno(tranno);
				annypaypfNew1.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
				
				try {
					annypaypfDAO.insertAnnypaypf(annypaypfNew1);
				} catch (Exception ex) {
					LOGGER.error("Insertion in Annypaypf failed:",ex);
				}
			
			}
		}

	}
	
	protected void insertAnnypaypf(){
		
		annypaypf = new Annypaypf();
		BigDecimal wthldngtax;
		// 01 - One Time Payment
		if (isEQ(sv.annpmntopt, "01")){		
			compute(wsaaTranno, 0).set(add(1,wsaaTranno));			
			annypaypf.setChdrnum(chdrpf.getChdrnum());
			annypaypf.setChdrcoy(chdrpf.getChdrcoy().toString());			
			annypaypf.setPmntno(wsaaTranno.toInt());
			annypaypf.setPmntdt(sv.frstpmntdt.toInt());
			annypaypf.setNextpaydt(sv.frstpmntdt.toInt());
			annypaypf.setAnnaamt(sv.totannufund.getbigdata());
			annypaypf.setAnnintamt(sv.totannufund.getbigdata());
			annypaypf.setPmntstat(PP);//"Pending"
			annypaypf.setAnnpmntopt(sv.annpmntopt.toString());
			annypaypf.setAnnupmnttrm(sv.annupmnttrm.toInt());
			annypaypf.setAnnpmntfreq(sv.annpmntfreq.toString());
			annypaypf.setDfrrdtrm(sv.dfrrdtrm.toInt());
			annypaypf.setWitholdtax(BigDecimal.ZERO);//ILJ-286
			annypaypf.setValidflag("1");
			annypaypf.setEffectivedt(datcon1rec.intDate.toInt());
			annypaypf.setTranno(tranno);
			annypaypf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
			try {
				annypaypfDAO.insertAnnypaypf(annypaypf);
			} catch (Exception ex) {
				LOGGER.error(INSERT_FAILED_IN_ANNYPAYPF,ex);
			}
		}
		// 02 - Regular Annuity
		if (isEQ(sv.annpmntopt, "02")){			
			int freq = sv.annpmntfreq.toInt();
			wsaaDate1.set(sv.anniversdt);
			compute(wsaaCount, 0).set(mult(freq,sv.annupmnttrm));
			for(int i=1; i<=wsaaCount.toInt() ; i++) {
				compute(wsaaTranno, 0).set(add(1,wsaaTranno));
				annypaypf.setPmntno(wsaaTranno.toInt());
				if(isEQ(wsaaTranno,1)){
					annypaypf.setPmntdt(sv.frstpmntdt.toInt());
					annypaypf.setNextpaydt(sv.frstpmntdt.toInt());
					if(isGT(sv.frstpmntdt, sv.pmntstartdt)){
						calcPaymntAmt();
					}else{
						annypaypf.setAnnaamt(sv.annupmntamt.getbigdata());
						annypaypf.setAnnintamt(sv.annupmntamt.getbigdata());
					}
				}else if(isEQ(wsaaTranno,2)){					
					annypaypf.setPmntdt(wsaaDate1.toInt());
					annypaypf.setNextpaydt(wsaaDate1.toInt());
					annypaypf.setAnnaamt(sv.annupmntamt.getbigdata());
					annypaypf.setAnnintamt(sv.annupmntamt.getbigdata());
				}else{
					calcNextPmntDt();
					annypaypf.setPmntdt(datcon2rec.intDate2.toInt());
					annypaypf.setNextpaydt(datcon2rec.intDate2.toInt());
					wsaaDate1.set(datcon2rec.intDate2);
					annypaypf.setAnnaamt(sv.annupmntamt.getbigdata());
					annypaypf.setAnnintamt(sv.annupmntamt.getbigdata());
				}
				annypaypf.setChdrnum(chdrpf.getChdrnum());
				annypaypf.setChdrcoy(chdrpf.getChdrcoy().toString());				
				annypaypf.setPmntstat(PP);//"Pending"
				annypaypf.setAnnpmntopt(sv.annpmntopt.toString());
				annypaypf.setAnnupmnttrm(sv.annupmnttrm.toInt());
				annypaypf.setAnnpmntfreq(sv.annpmntfreq.toString());
				annypaypf.setDfrrdtrm(sv.dfrrdtrm.toInt());
				wthldngtax = getWthldngtax(annypaypf.getPmntdt());
				annypaypf.setWitholdtax(wthldngtax);//ILJ-286
				annypaypf.setValidflag("1");
				annypaypf.setEffectivedt(datcon1rec.intDate.toInt());
				annypaypf.setTranno(tranno);
				annypaypf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
				
				try {
					annypaypfDAO.insertAnnypaypf(annypaypf);
				} catch (Exception ex) {
					LOGGER.error(INSERT_FAILED_IN_ANNYPAYPF,ex);
				}
			}					
		}
		// 03 - Partial Payment and Regular Annuity
        if (isEQ(sv.annpmntopt, "03")){	
        	int freq = sv.annpmntfreq.toInt();
			wsaaDate1.set(sv.anniversdt);
			compute(wsaaCount, 0).set(add(1,mult(freq,sv.annupmnttrm)));
			for(int i=1; i<=wsaaCount.toInt() ; i++) {
				compute(wsaaTranno, 0).set(add(1,wsaaTranno));
				annypaypf.setPmntno(wsaaTranno.toInt());
				if(isEQ(wsaaTranno,1)){
					annypaypf.setPmntdt(sv.frstpmntdt.toInt());
					annypaypf.setNextpaydt(sv.frstpmntdt.toInt());
					annypaypf.setAnnaamt(sv.partpmntamt.getbigdata());
					annypaypf.setAnnintamt(sv.partpmntamt.getbigdata());					
				}else if(isEQ(wsaaTranno,2)){
					annypaypf.setPmntdt(sv.frstpmntdt.toInt());
					annypaypf.setNextpaydt(sv.frstpmntdt.toInt());
					if(isGT(sv.frstpmntdt, sv.pmntstartdt)){
						calcPaymntAmt();
					}else{
						annypaypf.setAnnaamt(sv.annupmntamt.getbigdata());
						annypaypf.setAnnintamt(sv.annupmntamt.getbigdata());
					}														
				}else if(isEQ(wsaaTranno,3)){
					annypaypf.setPmntdt(wsaaDate1.toInt());
					annypaypf.setNextpaydt(wsaaDate1.toInt());
					annypaypf.setAnnaamt(sv.annupmntamt.getbigdata());
					annypaypf.setAnnintamt(sv.annupmntamt.getbigdata());							
				}else{
					calcNextPmntDt();
					annypaypf.setPmntdt(datcon2rec.intDate2.toInt());
					annypaypf.setNextpaydt(datcon2rec.intDate2.toInt());
					annypaypf.setAnnaamt(sv.annupmntamt.getbigdata());
					annypaypf.setAnnintamt(sv.annupmntamt.getbigdata());
					wsaaDate1.set(datcon2rec.intDate2);
				}	
				annypaypf.setChdrnum(chdrpf.getChdrnum());
				annypaypf.setChdrcoy(chdrpf.getChdrcoy().toString());	
				annypaypf.setPmntstat(PP);
				annypaypf.setAnnpmntopt(sv.annpmntopt.toString());
				annypaypf.setAnnupmnttrm(sv.annupmnttrm.toInt());
				annypaypf.setAnnpmntfreq(sv.annpmntfreq.toString());
				annypaypf.setDfrrdtrm(sv.dfrrdtrm.toInt());
				annypaypf.setValidflag("1");
				annypaypf.setEffectivedt(datcon1rec.intDate.toInt());
				annypaypf.setTranno(tranno);
				annypaypf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
				if(isEQ(wsaaTranno,1)){
					annypaypf.setWitholdtax(BigDecimal.ZERO);
				}
				else{
					wthldngtax = getWthldngtax(annypaypf.getPmntdt());
					annypaypf.setWitholdtax(wthldngtax);//ILJ-286
				}
				try {
					annypaypfDAO.insertAnnypaypf(annypaypf);
				} catch (Exception ex) {
					LOGGER.error(INSERT_FAILED_IN_ANNYPAYPF,ex);
				}
			}	
		}
        // 04 - Deferred
        if (isEQ(sv.annpmntopt, "04")){	
        	compute(wsaaTranno, 0).set(add(1,wsaaTranno));
        	annypaypf.setChdrnum(chdrpf.getChdrnum());
			annypaypf.setChdrcoy(chdrpf.getChdrcoy().toString());	
        	annypaypf.setPmntno(wsaaTranno.toInt());
        	annypaypf.setPmntdt(sv.finalpmntdt.toInt());	
        	annypaypf.setNextpaydt(sv.finalpmntdt.toInt());
        	annypaypf.setAnnaamt(sv.totannufund.getbigdata());
			annypaypf.setAnnintamt(sv.totannufund.getbigdata());
			annypaypf.setPmntstat(PP); //"Pending"
			annypaypf.setAnnpmntopt(sv.annpmntopt.toString());
			annypaypf.setAnnupmnttrm(sv.annupmnttrm.toInt());
			annypaypf.setAnnpmntfreq(sv.annpmntfreq.toString());
			annypaypf.setDfrrdtrm(sv.dfrrdtrm.toInt());
			annypaypf.setWitholdtax(BigDecimal.ZERO);//ILJ-286
			annypaypf.setValidflag("1");
			annypaypf.setEffectivedt(datcon1rec.intDate.toInt());
			annypaypf.setTranno(tranno);
			annypaypf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
			try {
				annypaypfDAO.insertAnnypaypf(annypaypf);
			} catch (Exception ex) {
				LOGGER.error(INSERT_FAILED_IN_ANNYPAYPF,ex);
			}
		}
        // 05 - Partial Payment and Deferred
        if (isEQ(sv.annpmntopt, "05")){
        	wsaaCount.add(2);
        	while ((isGTE(wsaaCount,1))) {
				compute(wsaaTranno, 0).set(add(1,wsaaTranno));
				annypaypf.setPmntno(wsaaTranno.toInt());
				if(isEQ(wsaaTranno,1)){
					annypaypf.setPmntdt(sv.frstpmntdt.toInt());
					annypaypf.setNextpaydt(sv.frstpmntdt.toInt());
					annypaypf.setAnnaamt(sv.partpmntamt.getbigdata());
					annypaypf.setAnnintamt(sv.partpmntamt.getbigdata());
				}
				if(isEQ(wsaaTranno,2)){
					annypaypf.setPmntdt(sv.finalpmntdt.toInt());
					annypaypf.setNextpaydt(sv.finalpmntdt.toInt());
					annypaypf.setAnnaamt(sv.dfrrdamt.getbigdata());
					annypaypf.setAnnintamt(sv.dfrrdamt.getbigdata());						
				}	
				annypaypf.setChdrnum(chdrpf.getChdrnum());
				annypaypf.setChdrcoy(chdrpf.getChdrcoy().toString());	
				annypaypf.setPmntstat(PP);  //"Pending"
				annypaypf.setAnnpmntopt(sv.annpmntopt.toString());
				annypaypf.setAnnupmnttrm(sv.annupmnttrm.toInt());
				annypaypf.setAnnpmntfreq(sv.annpmntfreq.toString());
				annypaypf.setDfrrdtrm(sv.dfrrdtrm.toInt());
				annypaypf.setWitholdtax(BigDecimal.ZERO);//ILJ-286
				annypaypf.setValidflag("1");
				annypaypf.setEffectivedt(datcon1rec.intDate.toInt());
				annypaypf.setTranno(tranno);
				annypaypf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
				wsaaCount.subtract(1);
				try {
					annypaypfDAO.insertAnnypaypf(annypaypf);
				} catch (Exception ex) {
					LOGGER.error(INSERT_FAILED_IN_ANNYPAYPF,ex);
				}
			}      	        	
		}  
		
	}
	
	private void calcPaymntAmt(){		
		PackedDecimalData wasaaAmt = new PackedDecimalData(17, 2);
		ZonedDecimalData wsaaDate2 = new ZonedDecimalData(8, 0);
		wsaaDate2.set(sv.pmntstartdt);
		ZonedDecimalData wsaabCount = new ZonedDecimalData(5, 0).setUnsigned();
		wsaabCount.set(ZERO);
		while(isGTE(sv.frstpmntdt, wsaaDate2)){			
		compute(wasaaAmt, 2).set(add(wasaaAmt, sv.annupmntamt));		
		calcNxtDt(wsaaDate2);
		wsaaDate2.set(datcon2rec.intDate2);	
		wsaabCount.add(1);
		}
		annypaypf.setAnnaamt(wasaaAmt.getbigdata());
		annypaypf.setAnnintamt(wasaaAmt.getbigdata());
		wsaaDate1.set(wsaaDate2);
		wsaabCount.subtract(1);		
		compute(wsaaCount, 0).set(sub(wsaaCount,wsaabCount));
	}
	
	protected void calcNxtDt(ZonedDecimalData wsaaDate2)
	{
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(wsaaDate2);
		datcon2rec.frequency.set(sv.annpmntfreq);		
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
	}
	
	protected boolean annyregChange(Annyregpf annyregpf)
	{
		BigDecimal b1,b2;
		  
        b1 = sv.partpmntamt.getbigdata(); 
        b2 = sv.dfrrdamt.getbigdata();
		
		if(isNE(sv.annpmntopt,annyregpf.getAnnpmntopt()))
			return true;
		else if(isNE(sv.annpmntfreq,annyregpf.getAnnpmntfreq()))
			return true;
		else if(sv.annupmnttrm.toInt()!=annyregpf.getAnnupmnttrm()) 
			return true;
		else if(sv.dfrrdtrm.toInt()!=annyregpf.getDfrrdtrm())
			return true;
		else if(isNE(sv.payeeaccnum.trim(),annyregpf.getPayeeaccnum().trim()))
			return true;
		else if(isNE(sv.pmntcurr.trim(),annyregpf.getPmntcurr().trim()))
			return true;
		else if(isNE(sv.payee.trim(),annyregpf.getPayee().trim()))
			return true;
		else if(isNE(sv.frstpmntdt,annyregpf.getFrstpmntdt()))
			return true;
		else if (!(b1.compareTo(annyregpf.getPartpmntamt()) == 0)) { 
	            return true;
	        } 
		else if (!(b2.compareTo(annyregpf.getDfrrdamt()) == 0)) { 
	            return true;
	        } 
		else
			return false;
		
		
	}
	
	
	protected void updateRegpf3060(){

		List<Covrpf> covrpfList=covrpfDAO.getCovrsurByComAndNum(wsspcomn.company.toString(), sv.chdrnum.toString());
		Covrpf covrpf = new Covrpf();
		if(null!= covrpfList && !covrpfList.isEmpty())
		{
			covrpf = covrpfList.get(0);
		}
		
		List<Regppf> list=regpDAO.readRegpfRecord(wsspcomn.company.toString(), 
				wsspcomn.chdrChdrnum.toString().trim(),covrpf.getLife(),
				covrpf.getCoverage(),covrpf.getRider(), t5645rec.sacscode01.toString(),t5645rec.sacstype01.toString());
		
		if(!list.isEmpty() && (PI.equals(annyregpf.getAnnpstcde()) || PT.equals(annyregpf.getAnnpstcde()))) {
			return;
		}
		
		if(list.isEmpty())
		{
			insertRegpf3070(covrpf);
		}
		else
		{
			if(insertReg)
			{
				deleteRegppf(covrpf);
				insertRegpf3070(covrpf);
			}
		}
	}
	
	protected void deleteRegppf(Covrpf covrpf){	
		// To delete existing Regppf records if Annuity Registration Modify	
		try {
			regpDAO.deleteRegppfRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString().trim(),
					covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(), 
					t5645rec.sacscode01.toString(),t5645rec.sacstype01.toString());
		} catch (Exception ex) {
			LOGGER.error("Failed to delete data in Regppf :", ex);
		}
	}

	
	protected void insertRegpf3070(Covrpf covrpf){		

		int num=0;

		annypaypfList=annypaypfDAO.getRecordbyStat(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(),PP); 
		
		if(!annypaypfList.isEmpty())
		{
			for (int i = 0; i < annypaypfList.size(); i++) {
				
				  regppf = new Regppf();
				  num++;
				  regppf.setChdrcoy(chdrpf.getChdrcoy().toString());
		          regppf.setChdrnum(chdrpf.getChdrnum());
		          regppf.setLife(covrpf.getLife());
		          regppf.setCoverage(covrpf.getCoverage());
		          regppf.setRider(covrpf.getRider());
		          regppf.setRgpynum(num);
		          regppf.setSacscode(t5645rec.sacscode01.toString());
		          regppf.setSacstype(t5645rec.sacstype01.toString());
		          regppf.setGlact(t5645rec.glmap01.toString());
		          regppf.setDebcred("");
		          regppf.setValidflag("1");
		          regppf.setDestkey(chdrpf.getChdrnum());
		          regppf.setPayclt(sv.payee.toString());
		          regppf.setRgpymop(annyregpf.getRgpymop());
		          regppf.setRegpayfreq(annyregpf.getAnnpmntfreq());
		          regppf.setCurrcd(chdrpf.getBillcurr());
		          regppf.setPymt(annypaypfList.get(i).getAnnaamt().subtract(annypaypfList.get(i).getWitholdtax()));//ILJ-281
		          regppf.setTotamnt(annypaypfList.get(i).getAnnaamt());
		          regppf.setRgpystat(AL);
		          regppf.setPayreason("**");
		          regppf.setClaimevd("");
		          regppf.setBankkey(bnkdescrec.bankkey.toString().trim());
		          regppf.setBankacckey(sv.payeeaccnum.toString().trim());
		          regppf.setCrtdate(varcom.vrcmMaxDate.toInt());
		          regppf.setAprvdate(varcom.vrcmMaxDate.toInt());
		          regppf.setFirstPaydate(annypaypfList.get(i).getPmntdt());
		          regppf.setNextPaydate(annypaypfList.get(i).getPmntdt());
		          regppf.setRevdte(varcom.vrcmMaxDate.toInt());

		          
		          if(isEQ(i,ZERO))
        		  {
        			  regppf.setLastPaydate(varcom.vrcmMaxDate.toInt());
        		  }
        		  else
        		  {
        			  regppf.setLastPaydate(annypaypfList.get(i-1).getPmntdt());
        			    
        		  }
		          
		          regppf.setFinalPaydate(annypaypfList.get(i).getPmntdt());
		          regppf.setAnvdate(annyregpf.getAnniversdt());
		          regppf.setCancelDate(varcom.vrcmMaxDate.toInt());
		          regppf.setIncurdt(varcom.vrcmMaxDate.toInt());
		          regppf.setRgpytype("");
		          regppf.setCrtable(covrpf.getCrtable());
		          regppf.setTermid(varcom.vrcmTermid.toString());
		          regppf.setTransactionDate(varcom.vrcmDate.toInt());
		          regppf.setTransactionTime(varcom.vrcmTime.toInt());
		          regppf.setPaycoy(wsspcomn.fsuco.toString());
		          regppf.setCertdate(varcom.vrcmMaxDate.toInt());
		          regppf.setClamparty("");
		          regppf.setPrcnt(BigDecimal.ZERO);
		          regppf.setPlanSuffix(covrpf.getPlanSuffix());
		          regppf.setTranno(chdrpf.getTranno());
		          regppf.setJobName(appVars.getLoggedOnUser());
		          regppf.setUserProfile(wsspcomn.username.toString());
		          regppf.setUser(varcom.vrcmUser.toInt());
		          regppf.setRecvdDate(varcom.vrcmMaxDate.toInt());
		          regppf.setDatime(new Timestamp(new Date().getTime()).toString());
		          regppf.setWdrgpymop("");
		          regppf.setWdbankkey("");
		          regppf.setWdbankacckey("");
		          
		      	try {
					regpDAO.insertRegppfRecord(regppf);
				} catch (Exception ex) {
					LOGGER.error("Failed to insert data in Regppf :", ex);
				}
				
			}
			
		
		}
	
	
	} 
	
	
	protected void calcNextPmntDt()
	{
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(wsaaDate1);
		datcon2rec.frequency.set(sv.annpmntfreq);		
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
	}

	protected void updateAnnypaypf3020(){	
		// To update valid flag of existing Annypaypf records if Annuity Registration Modify	
		try {
			annypaypfDAO.updateValidflag(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		} catch (Exception ex) {
			LOGGER.error("Failed to update valid flag in Annypaypf :", ex);
		}
	}
	
	protected void deleteAnnypaypf3020(){	
		// To delete existing Annypaypf records if Annuity Registration Modify	
		try {
			annypaypfDAO.deleteAnnypaypfRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(),tranno);
		} catch (Exception ex) {
			LOGGER.error("Failed to delete data in Annypaypf :", ex);
		}
	}
}
