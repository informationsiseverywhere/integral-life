/*
 * File: P5221.java
 * Date: 30 August 2009 0:19:01
 * Author: Quipoz Limited
 *
 * Class transformed from P5221.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.AnbkTableDAM;
import com.csc.life.annuities.dataaccess.VstdTableDAM;
import com.csc.life.annuities.procedures.Vpxvest;
import com.csc.life.annuities.recordstructures.Vpxvestrec;
import com.csc.life.annuities.recordstructures.Vstarec;
import com.csc.life.annuities.recordstructures.Vstccpy;
import com.csc.life.annuities.screens.S5221ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.dip.jvpms.web.ExternalisedRules;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  This is a Program which forms part of the 9405 Annuities
*Development. It forms part of the processing required to
*register Vesting on a Deferred Annuity component. It controls
*the Vesting Details Screen, S5221.
*
*The details of the Component selected will be displayed on the
*Screen for information. Fields available for input are the
*early/late vesting factor field, the percentage to commute field,
*the lump sum field and the Annuity Details window select field.
*
*The benefit payable will be calculated by the Vesting
*Calculation subroutine which is obtained from T6598, using
*the maturity method, from T5687 for the component, as the key.
*
*If a percentage to commute is entered, it must not be greater
*than the maximum percentage to commute on T6625 for this
*component. In order to calculate the lump sum and new benefit
*amount payable, the commutation calculation subroutine is
*obtained from T6598 and is called. The key to T6598 in this
*case is the commutation method from T6625 for the component.
*
*If the item is not found on T6598, then a message stating that
*manual adjustment is required and the benefit amount and lump
*sum fields can be changed by the user.
*
*The next program to be displayed is determined by OPTSWITCH.
*
*Vesting header and detail records will be created on the VSTH
*and VSTD files respectively.
*
*****************************************************************
* </pre>
*/
public class P5221 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5221");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaExit = new FixedLengthStringData(1);
	private PackedDecimalData wsaaVcalFactor = new PackedDecimalData(11, 6);
	private FixedLengthStringData wsaaFreqann = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAdvance = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaArrears = new FixedLengthStringData(1);
	private PackedDecimalData wsaaGuarperd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaNomlife = new FixedLengthStringData(8);
	private PackedDecimalData wsaaDthperco = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaDthpercn = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private String wsaaManualComt = "";
	private PackedDecimalData wsaaVstlump = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaComtperc = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaElvstfct = new PackedDecimalData(6, 5);
	private PackedDecimalData wsaaBirthDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBirthDateJ = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaLowerDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaUpperDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEvstperd = new ZonedDecimalData(3, 0);
	private String wsaaOutOfRange = "";
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSexj = new FixedLengthStringData(1);
		/* TABLES */
	private static final String t5681 = "T5681";
	private static final String t5682 = "T5682";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6625 = "T6625";
	private static final String t6598 = "T6598";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String payrrec = "PAYRREC";
	private static final String vstdrec = "VSTDREC";
	private AnbkTableDAM anbkIO = new AnbkTableDAM();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private VstdTableDAM vstdIO = new VstdTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5687rec t5687rec = new T5687rec();
	private T6625rec t6625rec = new T6625rec();
	private T6598rec t6598rec = new T6598rec();
	private Vstarec vstarec = new Vstarec();
	private Vstccpy vstccpy = new Vstccpy();
	private Optswchrec optswchrec = new Optswchrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S5221ScreenVars sv = ScreenProgram.getScreenVars( S5221ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	//ILIFE-8175
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End
	private ExternalisedRules er = new ExternalisedRules();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		continue1030,
		exit1090,
		checkForErrors2080,
		exit2090
	}

	public P5221() {
		super();
		screenVars = sv;
		new ScreenModel("S5221", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case continue1030: {
					continue1030();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
		//ILJ-49 End 
		wsaaExit.set(SPACES);
		wsaaFreqann.set(SPACES);
		wsaaArrears.set(SPACES);
		wsaaAdvance.set(SPACES);
		wsaaSex.set(SPACES);
		wsaaSexj.set(SPACES);
		sv.comtperc.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.vstlump.set(ZERO);
		sv.vstpay.set(ZERO);
		sv.origpay.set(ZERO);
		sv.elvstfct.set(ZERO);
		sv.vstpaya.set(ZERO);
		wsaaVstlump.set(ZERO);
		wsaaGuarperd.set(ZERO);
		wsaaEvstperd.set(ZERO);
		wsaaBirthDateJ.set(ZERO);
		wsaaComtperc.set(ZERO);
		wsaaElvstfct.set(ZERO);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		wsaaLowerDate.set(varcom.vrcmMaxDate);
		wsaaUpperDate.set(varcom.vrcmMaxDate);
		sv.vstpayaOut[varcom.nd.toInt()].set("Y");
		wsaaManualComt = "N";
		wsaaOutOfRange = "N";
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optdsc.set(optswchrec.optsDsc[1]);
		sv.optind.set("+");
		//ILIFE-8175
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.currcd.set(chdrpf.getCntcurr());
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1400();
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		sv.cnttype.set(chdrpf.getCnttype());
		compute(wsspcomn.tranno, 0).set(add(chdrpf.getTranno(),1));
		//ILIFE-8175
				covrpf = covrpfDAO.getCacheObject(covrpf);
				if(null==covrpf) {
					covrmjaIO.setFunction(varcom.retrv);
					SmartFileCode.execute(appVars, covrmjaIO);
					if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
						syserrrec.params.set(covrmjaIO.getParams());
						fatalError600();
				}
				else {
					covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
							covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
					if(null==covrpf) {
						fatalError600();
					}
				else {
					covrpfDAO.setCacheObject(covrpf);
					}
				}
			}
		/*covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		sv.planSuffix.set(covrpf.getPlanSuffix());
		sv.occdate.set(covrpf.getCrrcd());
		vstdIO.setDataArea(SPACES);
		vstdIO.setChdrcoy(covrpf.getChdrcoy());
		vstdIO.setChdrnum(covrpf.getChdrnum());
		vstdIO.setLife(covrpf.getLife());
		vstdIO.setCoverage(covrpf.getCoverage());
		vstdIO.setRider(covrpf.getRider());
		vstdIO.setPlanSuffix(covrpf.getPlanSuffix());
		vstdIO.setTranno(ZERO);
		vstdIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		vstdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vstdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		vstdIO.setFormat(vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)
		&& isNE(vstdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vstdIO.getParams());
			syserrrec.statuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(vstdIO.getStatuz(),varcom.oK)
		&& isEQ(covrpf.getChdrcoy(),vstdIO.getChdrcoy())
		&& isEQ(covrpf.getChdrnum(),vstdIO.getChdrnum())
		&& isEQ(covrpf.getLife(),vstdIO.getLife())
		&& isEQ(covrpf.getCoverage(),vstdIO.getCoverage())
		&& isEQ(covrpf.getRider(),vstdIO.getRider())
		&& isEQ(covrpf.getPlanSuffix(),vstdIO.getPlanSuffix())) {
			wsaaExit.set("Y");
			goTo(GotoLabel.exit1090);
		}
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(),1).toInt());
		wsaaTranno.set(chdrpf.getTranno());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5681);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrpf.getPstatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstate.fill("?");
		}
		else {
			sv.pstate.set(descIO.getShortdesc());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5682);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrpf.getStatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.rstate.fill("?");
		}
		else {
			sv.rstate.set(descIO.getShortdesc());
		}
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		sv.ptdate.set(payrIO.getPtdate());
		lifemjaIO.setChdrcoy(chdrpf.getChdrcoy());
		lifemjaIO.setChdrnum(chdrpf.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");

		//performance improvement -- Anjali
		/*lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");*/

		lifemjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(),lifemjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),lifemjaIO.getChdrnum())
		|| isNE(lifemjaIO.getJlife(),"00")
		&& isNE(lifemjaIO.getJlife(), "  ")) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		getClientDetails1400();
		wsaaBirthDate.set(cltsIO.getCltdob());
		wsaaSex.set(cltsIO.getCltsex());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		getClientDetails1400();
		wsaaBirthDateJ.set(cltsIO.getCltdob());
		wsaaSexj.set(cltsIO.getCltsex());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void continue1030()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");*/

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
		}
		t6625rec.t6625Rec.set(itdmIO.getGenarea());
		compute(wsaaEvstperd, 0).set(mult(t6625rec.evstperd,-1));
		datcon2rec.intDate1.set(covrpf.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaEvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaLowerDate.set(datcon2rec.intDatex2);
		datcon2rec.intDate1.set(covrpf.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6625rec.lvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaUpperDate.set(datcon2rec.intDatex2);
		if (isGTE(wsspcomn.currfrom,wsaaLowerDate)
		&& isLTE(wsspcomn.currfrom,wsaaUpperDate)) {
			obtainMaturityCalc1200();
		}
		else {
			wsaaOutOfRange = "Y";
			scrnparams.errorCode.set(errorsInner.f089);
		}
		sv.comtperc.set(ZERO);
		sv.vstlump.set(ZERO);
		if (isEQ(covrpf.getRiskCessDate(),wsspcomn.currfrom)) {
			sv.elvstfct.set(1);
		}
		else {
			sv.elvstfct.set(ZERO);
		}
		compute(sv.vstpay, 6).setRounded(mult(sv.origpay,sv.elvstfct));
		zrdecplrec.amountIn.set(sv.vstpay);
		callRounding5000();
		sv.vstpay.set(zrdecplrec.amountOut);
	}

protected void obtainMaturityCalc1200()
	{
		read1210();
	}

protected void read1210()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.f294);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(itemrec);
		itemIO.setItemtabl(t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t5687rec.maturityCalcMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		vstarec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		vstarec.chdrChdrnum.set(chdrpf.getChdrnum());
		vstarec.planSuffix.set(covrpf.getPlanSuffix());
		vstarec.polsum.set(chdrpf.getPolsum());
		vstarec.lifeLife.set(covrpf.getLife());
		vstarec.covrCoverage.set(covrpf.getCoverage());
		vstarec.covrRider.set(covrpf.getRider());
		vstarec.crtable.set(covrpf.getCrtable());
		vstarec.crrcd.set(covrpf.getCrrcd());
		vstarec.effdate.set(wsspcomn.currfrom);
		vstarec.actualValues.set(ZERO);
		vstarec.currcode.set(covrpf.getPremCurrency());
		vstarec.actvalTotal.set(ZERO);
		vstarec.chdrCurr.set(chdrpf.getCntcurr());
		vstarec.chdrType.set(chdrpf.getCnttype());
		vstarec.matCalcMeth.set(t5687rec.maturityCalcMeth);
		vstarec.pstatcode.set(covrpf.getPstatcode());
		vstarec.status.set("****");
		vstarec.language.set(wsspcomn.language);
		if (isNE(covrpf.getPlanSuffix(),0)) {
			if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
				vstarec.planSwitch.set(2);
			}
			else {
				vstarec.planSuffix.set(ZERO);
				vstarec.planSwitch.set(3);
			}
		}
		else {
			vstarec.planSwitch.set(1);
		}
		vstarec.batckey.set(wsaaBatckey);
		/* IVE-914 LIFE Annuties DAN Product - Vesting Calculation Method VEST(VSTANNC) 
		 * Start * */
		//callProgram(t6598rec.calcprog, vstarec.vestingRec);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t6598rec.calcprog.toString())))) 
		{
			callProgram(t6598rec.calcprog, vstarec.vestingRec);
		}	
		else
		{			
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(vstarec.vestingRec);
			Vpxvestrec vpxvestrec=new Vpxvestrec();
			vpxvestrec.function.set("INIT");
			vstarec.actualVal01.set(ZERO);
			vstarec.actualVal02.set(ZERO);
			vstarec.actualVal03.set(ZERO);
			vstarec.actualVal04.set(ZERO);
			vstarec.actualVal05.set(ZERO);
			vstarec.actualVal06.set(ZERO);
			callProgram(Vpxvest.class, vpmcalcrec.vpmcalcRec,vpxvestrec);
			callProgram(t6598rec.calcprog, vstarec.vestingRec,vpxvestrec);
			
			vstarec.type01.set("S");
			vstarec.type02.set("B");
			vstarec.type03.set("T");
			vstarec.type04.set("X");
			vstarec.type05.set("M");

		}
		/* IVE-914 End* */		
		if (isNE(vstarec.status,varcom.oK)) {
			syserrrec.statuz.set(vstarec.status);
			syserrrec.params.set(vstarec.vestingRec);
			fatalError600();
		}
		zrdecplrec.amountIn.set(vstarec.actvalTotal);
		callRounding5000();
		vstarec.actvalTotal.set(zrdecplrec.amountOut);
		sv.origpay.set(vstarec.actvalTotal);
		wssplife.bigAmt.set(vstarec.actvalTotal);
		sv.vstpaya.set(vstarec.actvalTotal);
	}

protected void getClientDetails1400()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsaaExit,"Y")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsaaManualComt,"Y")) {
			sv.vstpayOut[varcom.pr.toInt()].set(SPACES);
		}
		else {
			sv.vstpayOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsaaOutOfRange,"Y")) {
			scrnparams.function.set("PROT");
		}
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(wsaaOutOfRange,"Y")) {
			scrnparams.errorCode.set("F089");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.optind,SPACES)) {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelCode.set("CBOX");
			optswchrec.optsInd[1].set(sv.optind);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				sv.optindErr.set(optswchrec.optsStatuz);
			}
		}
		if (isEQ(sv.optind,"X")) {
			if (isEQ(sv.comtperc,ZERO)
			&& isEQ(sv.vstlump,ZERO)) {
				wsaaComtperc.set(ZERO);
				wsaaVstlump.set(ZERO);
				sv.vstpay.set(ZERO);
				goTo(GotoLabel.exit2090);
			}
			else {
				sv.optindErr.set(errorsInner.h224);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		if (isEQ(sv.elvstfct,ZERO)) {
			sv.elvstfctErr.set(errorsInner.f121);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(wsaaManualComt,"Y")) {
			manualCommutation2100();
		}
		else {
			automaticCommutation2200();
		}
		if (isLT(sv.vstlump,ZERO)) {
			sv.vstlumpErr.set(errorsInner.h213);
			sv.vstlump.set(ZERO);
		}
		if (isGT(sv.comtperc,t6625rec.comtperc)) {
			sv.comtpercErr.set(errorsInner.f067);
			sv.comtperc.set(ZERO);
			sv.vstlump.set(ZERO);
			sv.vstpay.set(wssplife.bigAmt);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void manualCommutation2100()
	{
			begin2110();
		}

protected void begin2110()
	{
		if ((isEQ(sv.vstlump,ZERO)
		&& isNE(sv.comtperc,ZERO))
		|| (isEQ(sv.comtperc,ZERO)
		&& isNE(sv.vstlump,ZERO))) {
			sv.vstlumpErr.set("F071");
			sv.comtpercErr.set("F071");
			return ;
		}
		if (isEQ(sv.vstlump,ZERO)
		&& isEQ(sv.comtperc,ZERO)) {
			compute(sv.vstpay, 6).setRounded(mult(wssplife.bigAmt,sv.elvstfct));
		}
		zrdecplrec.amountIn.set(sv.vstpay);
		callRounding5000();
		sv.vstpay.set(zrdecplrec.amountOut);
		if (isEQ(sv.vstpay,ZERO)
		&& isNE(sv.comtperc,100)) {
			sv.vstpayErr.set(errorsInner.h215);
			return ;
		}
		wsaaVstlump.set(sv.vstlump);
		wsaaComtperc.set(sv.comtperc);
	}

protected void automaticCommutation2200()
	{
			start2201();
		}

protected void start2201()
	{
		if (isEQ(sv.vstlump,ZERO)
		&& isEQ(sv.comtperc,ZERO)) {
			compute(sv.vstpay, 6).setRounded(mult(wssplife.bigAmt,sv.elvstfct));
			zrdecplrec.amountIn.set(sv.vstpay);
			callRounding5000();
			sv.vstpay.set(zrdecplrec.amountOut);
			return ;
		}
		if (isEQ(sv.vstlump,wsaaVstlump)
		&& isEQ(sv.comtperc,wsaaComtperc)
		&& isEQ(sv.elvstfct,wsaaElvstfct)) {
			return ;
		}
		if (isNE(sv.vstlump,ZERO)
		&& isNE(sv.comtperc,ZERO)) {
			sv.vstlumpErr.set(errorsInner.h413);
			sv.comtpercErr.set(errorsInner.h413);
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(itemrec);
		itemIO.setItemtabl(t6598);
		itemIO.setItempfx("IT");
		if (isNE(wsaaBirthDateJ,ZERO)) {
			itemIO.setItemitem(t6625rec.comtmthj);
		}
		else {
			itemIO.setItemitem(t6625rec.comtmeth);
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			wsaaManualComt = "Y";
			scrnparams.errorCode.set("F072");
			wsspcomn.edterror.set("Y");
			return ;
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		if (isNE(covrpf.getPlanSuffix(),0)) {
			if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
				annyIO.setPlanSuffix(covrpf.getPlanSuffix());
				checkAnny2210();
			}
			else {
				checkAnbk2220();
				if (isEQ(anbkIO.getStatuz(),varcom.mrnf)) {
					annyIO.setPlanSuffix(ZERO);
					checkAnny2210();
				}
			}
		}
		else {
			annyIO.setPlanSuffix(covrpf.getPlanSuffix());
			checkAnny2210();
		}
		datcon3rec.intDate1.set(wsaaBirthDate);
		datcon3rec.intDate2.set(wsspcomn.currfrom);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			vstccpy.age.set(datcon3rec.freqFactor);
		}
		else {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isNE(wsaaBirthDateJ,ZERO)) {
			datcon3rec.intDate1.set(wsaaBirthDateJ);
			datcon3rec.intDate2.set(wsspcomn.currfrom);
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isEQ(datcon3rec.statuz,varcom.oK)) {
				vstccpy.agej.set(datcon3rec.freqFactor);
			}
			else {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
		}
		vstccpy.company.set(covrpf.getChdrcoy());
		vstccpy.crtable.set(covrpf.getCrtable());
		vstccpy.sex.set(wsaaSex);
		vstccpy.freqann.set(wsaaFreqann);
		vstccpy.advance.set(wsaaAdvance);
		vstccpy.arrears.set(wsaaArrears);
		vstccpy.guarperd.set(wsaaGuarperd);
		vstccpy.sexj.set(wsaaSexj);
		vstccpy.nomlife.set(wsaaNomlife);
		vstccpy.dthperco.set(wsaaDthperco);
		vstccpy.dthpercn.set(wsaaDthpercn);
		if (isNE(sv.comtperc,wsaaComtperc)
		|| isEQ(sv.vstlump,ZERO)) {
			vstccpy.vstcperc.set(sv.comtperc);
		}
		else {
			vstccpy.vstcperc.set(ZERO);
		}
		if (isNE(sv.vstlump,wsaaVstlump)
		|| isEQ(sv.comtperc,ZERO)) {
			vstccpy.vstlump.set(sv.vstlump);
		}
		else {
			vstccpy.vstlump.set(ZERO);
		}
		compute(vstccpy.vstreduce, 6).setRounded(mult(wssplife.bigAmt,sv.elvstfct));
		compute(vstccpy.vstpay, 6).setRounded(mult(wssplife.bigAmt,sv.elvstfct));
	    /*IVE-915 LIFE Annuties DAN Product - Vesting Calculation Method VCOM(VSTCOMM) - Integration with latest PA compatible models (IntLifeVesting.pms) started*/
		//callProgram(t6598rec.calcprog, vstccpy.rec);

        if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
		{
        	callProgram(t6598rec.calcprog, vstccpy.rec);
		}
		else
		{
			vstccpy.vstlumpsum.set(ZERO);
			callProgram(t6598rec.calcprog, vstccpy.rec,chdrpf); 
			vstccpy.vstlump.set(vstccpy.vstlumpsum);			
		}
        
        /*IVE-915 LIFE Annuties DAN Product - Vesting Calculation Method VCOM(VSTCOMM) - Integration with latest PA compatible models (IntLifeVesting.pms) end*/   
		/* If zeros are returned after the call to VSTCOMM, an*/
		/* Error to this effect is displayed, and the Manual*/
		/* Commutation Flag is set to  'Y'.*/
		zrdecplrec.amountIn.set(vstccpy.vstlump);
		callRounding5000();
		vstccpy.vstlump.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(vstccpy.vstreduce);
		callRounding5000();
		vstccpy.vstreduce.set(zrdecplrec.amountOut);
		if (isEQ(vstccpy.vstlump,ZERO)
		&& isEQ(vstccpy.vstcperc,ZERO)) {
			wsaaManualComt = "Y";
			sv.vstpayErr.set("F072");
			wsspcomn.edterror.set("Y");
			compute(sv.vstpay, 6).setRounded(mult(wssplife.bigAmt,sv.elvstfct));
			zrdecplrec.amountIn.set(sv.vstpay);
			callRounding5000();
			sv.vstpay.set(zrdecplrec.amountOut);
			return ;
		}
		sv.comtperc.set(vstccpy.vstcperc);
		wsaaComtperc.set(vstccpy.vstcperc);
		sv.vstlump.set(vstccpy.vstlump);
		wsaaVstlump.set(vstccpy.vstlump);
		wsaaElvstfct.set(sv.elvstfct);
		sv.vstpay.set(vstccpy.vstreduce);
	}

protected void checkAnny2210()
	{
		start2211();
	}

protected void start2211()
	{
		annyIO.setChdrcoy(covrpf.getChdrcoy());
		annyIO.setChdrnum(covrpf.getChdrnum());
		annyIO.setLife(covrpf.getLife());
		annyIO.setCoverage(covrpf.getCoverage());
		annyIO.setRider(covrpf.getRider());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		wsaaFreqann.set(annyIO.getFreqann());
		wsaaAdvance.set(annyIO.getAdvance());
		wsaaArrears.set(annyIO.getArrears());
		wsaaGuarperd.set(annyIO.getGuarperd());
		wsaaNomlife.set(annyIO.getNomlife());
		wsaaDthperco.set(annyIO.getDthperco());
		wsaaDthpercn.set(annyIO.getDthpercn());
	}

protected void checkAnbk2220()
	{
		start2221();
	}

protected void start2221()
	{
		anbkIO.setChdrcoy(covrpf.getChdrcoy());
		anbkIO.setChdrnum(covrpf.getChdrnum());
		anbkIO.setLife(covrpf.getLife());
		anbkIO.setCoverage(covrpf.getCoverage());
		anbkIO.setRider(covrpf.getRider());
		anbkIO.setPlanSuffix(covrpf.getPlanSuffix());
		anbkIO.setTranno(wsaaTranno);
		anbkIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anbkIO);
		if (isNE(anbkIO.getStatuz(),varcom.oK)
		&& isNE(anbkIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anbkIO.getParams());
			syserrrec.statuz.set(anbkIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anbkIO.getStatuz(),varcom.oK)) {
			wsaaFreqann.set(anbkIO.getFreqann());
			wsaaAdvance.set(anbkIO.getAdvance());
			wsaaArrears.set(anbkIO.getArrears());
			wsaaGuarperd.set(anbkIO.getGuarperd());
			wsaaNomlife.set(anbkIO.getNomlife());
			wsaaDthperco.set(anbkIO.getDthperco());
			wsaaDthpercn.set(anbkIO.getDthpercn());
		}
	}

protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(wsaaExit,"Y")) {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		if (isEQ(sv.optind,"X")) {
			return ;
		}
		vstdIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstdIO.getParams());
			syserrrec.statuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstarec.actvalTotal,wssplife.bigAmt)) {
			compute(wsaaVcalFactor, 7).setRounded(div(vstarec.actvalTotal,wssplife.bigAmt));
			compute(vstarec.actualVal01, 7).setRounded(mult(vstarec.actualVal01,wsaaVcalFactor));
			compute(vstarec.actualVal02, 7).setRounded(mult(vstarec.actualVal02,wsaaVcalFactor));
			compute(vstarec.actualVal03, 7).setRounded(mult(vstarec.actualVal03,wsaaVcalFactor));
			compute(vstarec.actualVal04, 7).setRounded(mult(vstarec.actualVal04,wsaaVcalFactor));
			compute(vstarec.actualVal05, 7).setRounded(mult(vstarec.actualVal05,wsaaVcalFactor));
			compute(vstarec.actualVal06, 7).setRounded(mult(vstarec.actualVal06,wsaaVcalFactor));
		}
		vstdIO.setChdrnum(sv.chdrnum);
		vstdIO.setChdrcoy(chdrpf.getChdrcoy());
		vstdIO.setLife(covrpf.getLife());
		vstdIO.setCoverage(covrpf.getCoverage());
		vstdIO.setRider(covrpf.getRider());
		vstdIO.setPlanSuffix(sv.planSuffix);
		vstdIO.setTranno(wsaaTranno);
		vstdIO.setJlife(covrpf.getJlife());
		vstdIO.setVstpay(sv.vstpay);
		vstdIO.setVstlump(sv.vstlump);
		vstdIO.setVstperc(sv.comtperc);
		vstdIO.setVstpaya(sv.vstpaya);
		vstdIO.setOrigpay(sv.origpay);
		vstdIO.setElvstfct(sv.elvstfct);
		vstdIO.setCrtable(covrpf.getCrtable());
		vstdIO.setEffdate(wsspcomn.currfrom);
		vstdIO.setVestamt01(vstarec.actualVal01);
		vstdIO.setVestamt02(vstarec.actualVal02);
		vstdIO.setVestamt03(vstarec.actualVal03);
		vstdIO.setVestamt04(vstarec.actualVal04);
		vstdIO.setVestamt05(vstarec.actualVal05);
		vstdIO.setVestamt06(vstarec.actualVal06);
		vstdIO.setFieldType01(vstarec.type01);
		vstdIO.setFieldType02(vstarec.type02);
		vstdIO.setFieldType03(vstarec.type03);
		vstdIO.setFieldType04(vstarec.type04);
		vstdIO.setFieldType05(vstarec.type05);
		vstdIO.setFieldType06(vstarec.type06);
		vstdIO.setNewcovr(SPACES);
		rounding6000();
		vstdIO.setFunction(varcom.writr);
		vstdIO.setFormat(vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstdIO.getParams());
			syserrrec.statuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		vstdIO.setFunction(varcom.keeps);
		vstdIO.setFormat(vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstdIO.getParams());
			syserrrec.statuz.set(vstdIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsaaExit,"Y")) {
			wsspcomn.programPtr.subtract(1);
			return ;
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			optswchCall4100();
			compute(sv.vstpay, 6).setRounded(mult(wssplife.bigAmt,sv.elvstfct));
			zrdecplrec.amountIn.set(sv.vstpay);
			callRounding5000();
			sv.vstpay.set(zrdecplrec.amountOut);
			if (isEQ(wssplife.updateFlag,"N")) {
				scrnparams.errorCode.set(errorsInner.h223);
				wsaaManualComt = "Y";
			}
			else {
				wsaaManualComt = "N";
				if (isNE(wssplife.bigAmt,sv.origpay)) {
					sv.vstpayaOut[varcom.nd.toInt()].set(SPACES);
					sv.vstpaya.set(wssplife.bigAmt);
				}
			}
			return ;
		}
		if (isEQ(sv.optind,"X")) {
			wssplife.updateFlag.set("Y");
			optswchCall4100();
			sv.optind.set(optswchrec.optsInd[1]);
		}
		sv.optind.set("+");
		wsspcomn.programPtr.add(1);
	}

protected void optswchCall4100()
	{
		call4110();
	}

protected void call4110()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK))
		&& (isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void rounding6000()
	{
		start6100();
	}

protected void start6100()
	{
		zrdecplrec.amountIn.set(vstdIO.getVestamt01());
		callRounding5000();
		vstdIO.setVestamt01(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(vstdIO.getVestamt02());
		callRounding5000();
		vstdIO.setVestamt02(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(vstdIO.getVestamt03());
		callRounding5000();
		vstdIO.setVestamt03(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(vstdIO.getVestamt04());
		callRounding5000();
		vstdIO.setVestamt04(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(vstdIO.getVestamt05());
		callRounding5000();
		vstdIO.setVestamt05(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(vstdIO.getVestamt06());
		callRounding5000();
		vstdIO.setVestamt06(zrdecplrec.amountOut);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f067 = new FixedLengthStringData(94).init("F067");
	private FixedLengthStringData h413 = new FixedLengthStringData(4).init("H413");
	private FixedLengthStringData h213 = new FixedLengthStringData(4).init("H213");
	private FixedLengthStringData f089 = new FixedLengthStringData(4).init("F089");
	private FixedLengthStringData h215 = new FixedLengthStringData(4).init("H215");
	private FixedLengthStringData h223 = new FixedLengthStringData(4).init("H223");
	private FixedLengthStringData f121 = new FixedLengthStringData(4).init("F121");
	private FixedLengthStringData h224 = new FixedLengthStringData(4).init("H224");
}
}
