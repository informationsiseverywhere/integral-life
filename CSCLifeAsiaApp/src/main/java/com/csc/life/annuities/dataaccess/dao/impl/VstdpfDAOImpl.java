package com.csc.life.annuities.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.annuities.dataaccess.dao.VstdpfDAO;
import com.csc.life.annuities.dataaccess.model.Vstdpf;
import com.csc.life.productdefinition.dataaccess.dao.impl.LifepfDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class VstdpfDAOImpl extends BaseDAOImpl<Vstdpf> implements VstdpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(LifepfDAOImpl.class);

	public int getVstdpfList(String chdrcoy, String chdrnum){
		StringBuilder sqlLifeSelect1 = new StringBuilder(
				"SELECT  COUNT(*) ");
		sqlLifeSelect1.append("FROM VSTDPF WHERE CHDRCOY = ? AND CHDRNUM = ? 	");	


		PreparedStatement ps = getPrepareStatement(sqlLifeSelect1.toString());
		ResultSet sqllifepf1rs = null;
		List<Vstdpf> vstdpfList = null;
		try {
			ps.setString(1,chdrcoy);
			ps.setString(2, chdrnum);


			sqllifepf1rs = executeQuery(ps);
			vstdpfList = new ArrayList<Vstdpf>();
			while (sqllifepf1rs.next()) {

				return sqllifepf1rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error("getLifeList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		return 0;
	}

}
