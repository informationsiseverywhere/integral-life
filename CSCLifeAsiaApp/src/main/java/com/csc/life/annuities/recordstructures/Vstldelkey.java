package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:12
 * Description:
 * Copybook name: VSTLDELKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vstldelkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vstldelFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData vstldelKey = new FixedLengthStringData(64).isAPartOf(vstldelFileKey, 0, REDEFINE);
  	public FixedLengthStringData vstldelChdrcoy = new FixedLengthStringData(1).isAPartOf(vstldelKey, 0);
  	public FixedLengthStringData vstldelChdrnum = new FixedLengthStringData(8).isAPartOf(vstldelKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(vstldelKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vstldelFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vstldelFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}