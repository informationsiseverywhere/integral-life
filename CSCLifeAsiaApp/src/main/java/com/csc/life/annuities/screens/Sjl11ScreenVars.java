package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl11ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(768);
	public FixedLengthStringData dataFields = new FixedLengthStringData(432).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 39);	
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,44);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,52);
	public FixedLengthStringData anntypdesc = DD.anntypdesc.copy().isAPartOf(dataFields, 60);	
	public FixedLengthStringData annuitype = DD.annuitype.copy().isAPartOf(dataFields, 110);	
	
	public FixedLengthStringData bentypsdesc = new FixedLengthStringData(300).isAPartOf(dataFields, 112);
	public FixedLengthStringData[] bentypdesc = FLSArrayPartOfStructure(6, 50, bentypsdesc, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(300).isAPartOf(bentypsdesc, 0, FILLER_REDEFINE);
	public FixedLengthStringData bentypdesc01 = DD.bentypdesc.copy().isAPartOf(filler1, 0);
	public FixedLengthStringData bentypdesc02 = DD.bentypdesc.copy().isAPartOf(filler1, 50);
	public FixedLengthStringData bentypdesc03 = DD.bentypdesc.copy().isAPartOf(filler1, 100);
	public FixedLengthStringData bentypdesc04 = DD.bentypdesc.copy().isAPartOf(filler1, 150);
	public FixedLengthStringData bentypdesc05 = DD.bentypdesc.copy().isAPartOf(filler1, 200);
	public FixedLengthStringData bentypdesc06 = DD.bentypdesc.copy().isAPartOf(filler1, 250);
	
	public FixedLengthStringData beneftypes = new FixedLengthStringData(18).isAPartOf(dataFields, 412);
	public FixedLengthStringData[] beneftype = FLSArrayPartOfStructure(6, 3, beneftypes, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(18).isAPartOf(beneftypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData beneftype01 = DD.beneftype.copy().isAPartOf(filler2, 0);
	public FixedLengthStringData beneftype02 = DD.beneftype.copy().isAPartOf(filler2, 3);
	public FixedLengthStringData beneftype03 = DD.beneftype.copy().isAPartOf(filler2, 6);
	public FixedLengthStringData beneftype04 = DD.beneftype.copy().isAPartOf(filler2, 9);
	public FixedLengthStringData beneftype05 = DD.beneftype.copy().isAPartOf(filler2, 12);
	public FixedLengthStringData beneftype06 = DD.beneftype.copy().isAPartOf(filler2, 15);	
	
	public FixedLengthStringData defannpayfreq = DD.annfreq.copy().isAPartOf(dataFields, 430);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 432);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData anntypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData annuitypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	
	public FixedLengthStringData bentypsdescErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] bentypdescErr = FLSArrayPartOfStructure(6, 4, bentypsdescErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(bentypsdescErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bentypdesc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData bentypdesc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData bentypdesc03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData bentypdesc04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData bentypdesc05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData bentypdesc06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	
	public FixedLengthStringData beneftypesErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] beneftypeErr = FLSArrayPartOfStructure(6, 4, beneftypesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(beneftypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData beneftype01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData beneftype02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData beneftype03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData beneftype04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData beneftype05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData beneftype06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	
	public FixedLengthStringData defannpayfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 516);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] anntypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] annuitypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);	
	
	public FixedLengthStringData bentypsdescOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] bentypdescOut = FLSArrayPartOfStructure(6, 12, bentypsdescOut, 0);
	public FixedLengthStringData[][] bentypdescO = FLSDArrayPartOfArrayStructure(12, 1, bentypdescOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(72).isAPartOf(bentypsdescOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bentypdesc01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] bentypdesc02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] bentypdesc03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] bentypdesc04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] bentypdesc05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] bentypdesc06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	
	public FixedLengthStringData beneftypesOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] beneftypeOut = FLSArrayPartOfStructure(6, 12, beneftypesOut, 0);
	public FixedLengthStringData[][] beneftypeO = FLSDArrayPartOfArrayStructure(12, 1, beneftypeOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(72).isAPartOf(beneftypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] beneftype01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] beneftype02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] beneftype03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] beneftype04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] beneftype05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] beneftype06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	
	public FixedLengthStringData[] defannpayfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	
	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sjl11screenWritten = new LongData(0);
	public LongData Sjl11protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl11ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {	
		fieldIndMap.put(annuitypeOut, new String[] { "05", "45", "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(beneftype01Out, new String[] { "06", "46", "-06", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(beneftype02Out, new String[] { "07", "47", "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(beneftype03Out, new String[] { "08", "48", "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(beneftype04Out, new String[] { "09", "49", "-09", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(beneftype05Out, new String[] { "10", "50", "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(beneftype06Out, new String[] { "11", "51", "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(defannpayfreqOut, new String[] { "12", "52", "-12", null, null, null, null, null, null, null, null, null });
		
		screenFields = new BaseData[] { company, tabl, item, longdesc, itmfrm, itmto, anntypdesc, annuitype, bentypdesc01, bentypdesc02, bentypdesc03, bentypdesc04, bentypdesc05, bentypdesc06, beneftype01, beneftype02, beneftype03, beneftype04, beneftype05, beneftype06, defannpayfreq};
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, anntypdescOut, annuitypeOut, bentypdesc01Out, bentypdesc02Out, bentypdesc03Out, bentypdesc04Out, bentypdesc05Out, bentypdesc06Out, beneftype01Out, beneftype02Out, beneftype03Out, beneftype04Out, beneftype05Out, beneftype06Out, defannpayfreqOut };
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, anntypdescErr, annuitypeErr, bentypdesc01Err, bentypdesc02Err, bentypdesc03Err, bentypdesc04Err, bentypdesc05Err, bentypdesc06Err,  beneftype01Err, beneftype02Err, beneftype03Err, beneftype04Err, beneftype05Err, beneftype06Err, defannpayfreqErr };
				
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl11screen.class;
		protectRecord = Sjl11protect.class;
	}

}
