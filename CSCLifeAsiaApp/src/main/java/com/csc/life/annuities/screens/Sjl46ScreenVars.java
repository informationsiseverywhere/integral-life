package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl46ScreenVars extends SmartVarModel {
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(889);
	public FixedLengthStringData dataFields = new FixedLengthStringData(473).isAPartOf(dataArea, 0);	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData annpmntstatus = DD.premstatus_longdesc.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData cownnum = DD.ownersel.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,136);
	public FixedLengthStringData payee = DD.ownersel.copy().isAPartOf(dataFields,183);
	public FixedLengthStringData payeename = DD.ownername.copy().isAPartOf(dataFields,193);
	
	public ZonedDecimalData pmntno = DD.pmntno.copyToZonedDecimal().isAPartOf(dataFields,240);
	public ZonedDecimalData pmntdate = DD.pmntdate.copyToZonedDecimal().isAPartOf(dataFields,245);
	public ZonedDecimalData totannufund = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,253);
	public ZonedDecimalData prmpaid = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,270);
	public ZonedDecimalData annupmnttrm = DD.annupmnttrm.copyToZonedDecimal().isAPartOf(dataFields,287);
	public FixedLengthStringData annpmntfreq = DD.billfreq.copy().isAPartOf(dataFields, 290);
	public ZonedDecimalData annualannamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,292);
	public FixedLengthStringData withholdtaxstus = DD.withholdtaxstus.copy().isAPartOf(dataFields,309);
	public ZonedDecimalData reqrexpenseratio = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,339);
	public ZonedDecimalData reqrexpense = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,356);
	public ZonedDecimalData otherincome = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,373);
	public ZonedDecimalData whhtr = DD.whhtr.copyToZonedDecimal().isAPartOf(dataFields,390);
	public ZonedDecimalData annupmntamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,405);	
	public ZonedDecimalData annuitywint = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,422);
	public ZonedDecimalData wholdtax = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,439);
	public ZonedDecimalData paidamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,456);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 473);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData annpmntstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);	
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);	
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData payeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);	
	public FixedLengthStringData payeenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData pmntnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData pmntdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData totannufundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData prmpaidErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData annupmnttrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData annpmntfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData annualannamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData withholdtaxstusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData reqrexpenseratioErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData reqrexpenseErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData otherincomeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData whhtrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData annupmntamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData annuitywintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData wholdtaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData paidamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 577);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] annpmntstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);	
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] payeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] payeenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);	
	public FixedLengthStringData[] pmntnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);	
	public FixedLengthStringData[] pmntdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);	
	public FixedLengthStringData[] totannufundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);	
	public FixedLengthStringData[] prmpaidOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);	
	public FixedLengthStringData[] annupmnttrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);	
	public FixedLengthStringData[] annpmntfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);	
	public FixedLengthStringData[] annualannamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);	
	public FixedLengthStringData[] withholdtaxstusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);	
	public FixedLengthStringData[] reqrexpenseratioOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);	
	public FixedLengthStringData[] reqrexpenseOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);	
	public FixedLengthStringData[] otherincomeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);	
	public FixedLengthStringData[] whhtrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);	
	public FixedLengthStringData[] annupmntamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);	
	public FixedLengthStringData[] annuitywintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);	
	public FixedLengthStringData[] wholdtaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);	
	public FixedLengthStringData[] paidamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);	
		
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData pmntdateDisp = new FixedLengthStringData(10);		
	public FixedLengthStringData numsel =  new FixedLengthStringData(10);	
	

	public LongData Sjl46screenWritten = new LongData(0);
	public LongData Sjl46protectWritten = new LongData(0);
	
	@Override
	public boolean hasSubfile() {
		return false;
	}

	public Sjl46ScreenVars() {
		super();
		initialiseScreenVars();
	}
		
	@Override
	protected final void initialiseScreenVars() {
		fieldIndMap.put(annpmntfreqOut,new String[] {"01","02","-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, annpmntstatus, cownnum, ownername, lifenum, 
				lifename, payee, payeename, pmntno, totannufund, prmpaid, annupmnttrm, 
				annpmntfreq, annualannamt, withholdtaxstus, reqrexpenseratio, reqrexpense, 
				otherincome, whhtr, annupmntamt, annuitywint,wholdtax, paidamt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut,
			annpmntstatusOut, cownnumOut, ownernameOut, lifenumOut, lifenameOut, payeeOut,
			payeenameOut, pmntnoOut, totannufundOut, prmpaidOut, annupmnttrmOut, 
			annpmntfreqOut, annualannamtOut, withholdtaxstusOut, reqrexpenseratioOut,
			reqrexpenseOut, otherincomeOut, whhtrOut, annupmntamtOut, annuitywintOut, 
			wholdtaxOut, paidamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, annpmntstatusErr, cownnumErr,ownernameErr, 
				lifenumErr, lifenameErr, payeeErr, payeenameErr, pmntnoErr, totannufundErr, prmpaidErr, annupmnttrmErr, 
				annpmntfreqErr, annualannamtErr, withholdtaxstusErr, reqrexpenseratioErr, reqrexpenseErr, otherincomeErr, whhtrErr, 
				annupmntamtErr, annuitywintErr,wholdtaxErr, paidamtErr};
		screenDateFields = new BaseData[] {pmntdate};
		screenDateErrFields = new BaseData[] {pmntdateErr};
		screenDateDispFields = new BaseData[] {pmntdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl46screen.class;
		protectRecord = Sjl46protect.class;
	}
	


}
