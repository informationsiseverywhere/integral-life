package com.csc.life.annuities.dataaccess;

import com.csc.life.terminationclaims.dataaccess.AnnypfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: Annyvf1TableDAM.java
 * Date: Sun, 30 Aug 2009 03:29:16
 * Class transformed from ANNYVF1.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Annyvf1TableDAM extends AnnypfTableDAM {

	public Annyvf1TableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ANNYVF1");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "GUARPERD, " +
		            "FREQANN, " +
		            "ARREARS, " +
		            "ADVANCE, " +
		            "DTHPERCN, " +
		            "DTHPERCO, " +
		            "INTANNY, " +
		            "WITHPROP, " +
		            "WITHOPROP, " +
		            "PPIND, " +
		            "CAPCONT, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "NOMLIFE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               guarperd,
                               freqann,
                               arrears,
                               advance,
                               dthpercn,
                               dthperco,
                               intanny,
                               withprop,
                               withoprop,
                               ppind,
                               capcont,
                               validflag,
                               tranno,
                               nomlife,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(103);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getGuarperd().toInternal()
					+ getFreqann().toInternal()
					+ getArrears().toInternal()
					+ getAdvance().toInternal()
					+ getDthpercn().toInternal()
					+ getDthperco().toInternal()
					+ getIntanny().toInternal()
					+ getWithprop().toInternal()
					+ getWithoprop().toInternal()
					+ getPpind().toInternal()
					+ getCapcont().toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getNomlife().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, guarperd);
			what = ExternalData.chop(what, freqann);
			what = ExternalData.chop(what, arrears);
			what = ExternalData.chop(what, advance);
			what = ExternalData.chop(what, dthpercn);
			what = ExternalData.chop(what, dthperco);
			what = ExternalData.chop(what, intanny);
			what = ExternalData.chop(what, withprop);
			what = ExternalData.chop(what, withoprop);
			what = ExternalData.chop(what, ppind);
			what = ExternalData.chop(what, capcont);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, nomlife);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getGuarperd() {
		return guarperd;
	}
	public void setGuarperd(Object what) {
		setGuarperd(what, false);
	}
	public void setGuarperd(Object what, boolean rounded) {
		if (rounded)
			guarperd.setRounded(what);
		else
			guarperd.set(what);
	}	
	public FixedLengthStringData getFreqann() {
		return freqann;
	}
	public void setFreqann(Object what) {
		freqann.set(what);
	}	
	public FixedLengthStringData getArrears() {
		return arrears;
	}
	public void setArrears(Object what) {
		arrears.set(what);
	}	
	public FixedLengthStringData getAdvance() {
		return advance;
	}
	public void setAdvance(Object what) {
		advance.set(what);
	}	
	public PackedDecimalData getDthpercn() {
		return dthpercn;
	}
	public void setDthpercn(Object what) {
		setDthpercn(what, false);
	}
	public void setDthpercn(Object what, boolean rounded) {
		if (rounded)
			dthpercn.setRounded(what);
		else
			dthpercn.set(what);
	}	
	public PackedDecimalData getDthperco() {
		return dthperco;
	}
	public void setDthperco(Object what) {
		setDthperco(what, false);
	}
	public void setDthperco(Object what, boolean rounded) {
		if (rounded)
			dthperco.setRounded(what);
		else
			dthperco.set(what);
	}	
	public PackedDecimalData getIntanny() {
		return intanny;
	}
	public void setIntanny(Object what) {
		setIntanny(what, false);
	}
	public void setIntanny(Object what, boolean rounded) {
		if (rounded)
			intanny.setRounded(what);
		else
			intanny.set(what);
	}	
	public FixedLengthStringData getWithprop() {
		return withprop;
	}
	public void setWithprop(Object what) {
		withprop.set(what);
	}	
	public FixedLengthStringData getWithoprop() {
		return withoprop;
	}
	public void setWithoprop(Object what) {
		withoprop.set(what);
	}	
	public FixedLengthStringData getPpind() {
		return ppind;
	}
	public void setPpind(Object what) {
		ppind.set(what);
	}	
	public PackedDecimalData getCapcont() {
		return capcont;
	}
	public void setCapcont(Object what) {
		setCapcont(what, false);
	}
	public void setCapcont(Object what, boolean rounded) {
		if (rounded)
			capcont.setRounded(what);
		else
			capcont.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getNomlife() {
		return nomlife;
	}
	public void setNomlife(Object what) {
		nomlife.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		guarperd.clear();
		freqann.clear();
		arrears.clear();
		advance.clear();
		dthpercn.clear();
		dthperco.clear();
		intanny.clear();
		withprop.clear();
		withoprop.clear();
		ppind.clear();
		capcont.clear();
		validflag.clear();
		tranno.clear();
		nomlife.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}