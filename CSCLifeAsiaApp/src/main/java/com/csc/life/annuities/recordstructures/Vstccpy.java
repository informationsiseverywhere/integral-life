package com.csc.life.annuities.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:11
 * Description:
 * Copybook name: VSTCCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vstccpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(113);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rec, 0);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rec, 1);
  	public ZonedDecimalData age = new ZonedDecimalData(3, 0).isAPartOf(rec, 5).setUnsigned();
  	public FixedLengthStringData sex = new FixedLengthStringData(1).isAPartOf(rec, 8);
  	public FixedLengthStringData freqann = new FixedLengthStringData(2).isAPartOf(rec, 9);
  	public FixedLengthStringData advance = new FixedLengthStringData(1).isAPartOf(rec, 11);
  	public FixedLengthStringData arrears = new FixedLengthStringData(1).isAPartOf(rec, 12);
  	public ZonedDecimalData guarperd = new ZonedDecimalData(7, 2).isAPartOf(rec, 13);
  	public ZonedDecimalData vstcperc = new ZonedDecimalData(7, 2).isAPartOf(rec, 20).setUnsigned();
  	public ZonedDecimalData vstlump = new ZonedDecimalData(17, 2).isAPartOf(rec, 27);
  	public ZonedDecimalData vstreduce = new ZonedDecimalData(17, 2).isAPartOf(rec, 44);
  	public ZonedDecimalData vstpay = new ZonedDecimalData(17, 2).isAPartOf(rec, 61);
  	public FixedLengthStringData nomlife = new FixedLengthStringData(8).isAPartOf(rec, 78);
  	public PackedDecimalData dthpercn = new PackedDecimalData(5, 2).isAPartOf(rec, 86);
  	public PackedDecimalData dthperco = new PackedDecimalData(5, 2).isAPartOf(rec, 89);
  	public ZonedDecimalData agej = new ZonedDecimalData(3, 0).isAPartOf(rec, 92).setUnsigned();
  	public FixedLengthStringData sexj = new FixedLengthStringData(1).isAPartOf(rec, 95);
  	public ZonedDecimalData vstlumpsum = new ZonedDecimalData(17, 2).isAPartOf(rec, 96);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}