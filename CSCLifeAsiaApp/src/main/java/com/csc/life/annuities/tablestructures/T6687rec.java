package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:12
 * Description:
 * Copybook name: T6687REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6687rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6687Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData taxrelsub = new FixedLengthStringData(8).isAPartOf(t6687Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(492).isAPartOf(t6687Rec, 8, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6687Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6687Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}