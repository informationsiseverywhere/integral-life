package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6698screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6698ScreenVars sv = (S6698ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6698screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6698ScreenVars screenVars = (S6698ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.ageIssageTo01.setClassString("");
		screenVars.pclimit01.setClassString("");
		screenVars.ageIssageTo02.setClassString("");
		screenVars.pclimit02.setClassString("");
		screenVars.ageIssageTo03.setClassString("");
		screenVars.pclimit03.setClassString("");
		screenVars.ageIssageTo04.setClassString("");
		screenVars.pclimit04.setClassString("");
		screenVars.ageIssageTo05.setClassString("");
		screenVars.pclimit05.setClassString("");
		screenVars.ageIssageTo06.setClassString("");
		screenVars.pclimit06.setClassString("");
		screenVars.ageIssageTo07.setClassString("");
		screenVars.pclimit07.setClassString("");
		screenVars.ageIssageTo08.setClassString("");
		screenVars.pclimit08.setClassString("");
		screenVars.ageIssageTo09.setClassString("");
		screenVars.pclimit09.setClassString("");
		screenVars.ageIssageTo10.setClassString("");
		screenVars.pclimit10.setClassString("");
		screenVars.taxrelpc.setClassString("");
		screenVars.earningCap.setClassString("");
		screenVars.inrevnum.setClassString("");
		screenVars.dssnum.setClassString("");
	}

/**
 * Clear all the variables in S6698screen
 */
	public static void clear(VarModel pv) {
		S6698ScreenVars screenVars = (S6698ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.ageIssageTo01.clear();
		screenVars.pclimit01.clear();
		screenVars.ageIssageTo02.clear();
		screenVars.pclimit02.clear();
		screenVars.ageIssageTo03.clear();
		screenVars.pclimit03.clear();
		screenVars.ageIssageTo04.clear();
		screenVars.pclimit04.clear();
		screenVars.ageIssageTo05.clear();
		screenVars.pclimit05.clear();
		screenVars.ageIssageTo06.clear();
		screenVars.pclimit06.clear();
		screenVars.ageIssageTo07.clear();
		screenVars.pclimit07.clear();
		screenVars.ageIssageTo08.clear();
		screenVars.pclimit08.clear();
		screenVars.ageIssageTo09.clear();
		screenVars.pclimit09.clear();
		screenVars.ageIssageTo10.clear();
		screenVars.pclimit10.clear();
		screenVars.taxrelpc.clear();
		screenVars.earningCap.clear();
		screenVars.inrevnum.clear();
		screenVars.dssnum.clear();
	}
}
