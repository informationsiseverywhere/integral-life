package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6697
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6697ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(961);
	public FixedLengthStringData dataFields = new FixedLengthStringData(161).isAPartOf(dataArea, 0);
	public FixedLengthStringData adminref = DD.adminref.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,5);
	public FixedLengthStringData contypes = new FixedLengthStringData(13).isAPartOf(dataFields, 6);
	public FixedLengthStringData[] contype = FLSArrayPartOfStructure(13, 1, contypes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(contypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData contype01 = DD.contype.copy().isAPartOf(filler,0);
	public FixedLengthStringData contype02 = DD.contype.copy().isAPartOf(filler,1);
	public FixedLengthStringData contype03 = DD.contype.copy().isAPartOf(filler,2);
	public FixedLengthStringData contype04 = DD.contype.copy().isAPartOf(filler,3);
	public FixedLengthStringData contype05 = DD.contype.copy().isAPartOf(filler,4);
	public FixedLengthStringData contype06 = DD.contype.copy().isAPartOf(filler,5);
	public FixedLengthStringData contype07 = DD.contype.copy().isAPartOf(filler,6);
	public FixedLengthStringData contype08 = DD.contype.copy().isAPartOf(filler,7);
	public FixedLengthStringData contype09 = DD.contype.copy().isAPartOf(filler,8);
	public FixedLengthStringData contype10 = DD.contype.copy().isAPartOf(filler,9);
	public FixedLengthStringData contype11 = DD.contype.copy().isAPartOf(filler,10);
	public FixedLengthStringData contype12 = DD.contype.copy().isAPartOf(filler,11);
	public FixedLengthStringData contype13 = DD.contype.copy().isAPartOf(filler,12);
	public FixedLengthStringData crcodes = new FixedLengthStringData(52).isAPartOf(dataFields, 19);
	public FixedLengthStringData[] crcode = FLSArrayPartOfStructure(13, 4, crcodes, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(52).isAPartOf(crcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData crcode01 = DD.crcode.copy().isAPartOf(filler1,0);
	public FixedLengthStringData crcode02 = DD.crcode.copy().isAPartOf(filler1,4);
	public FixedLengthStringData crcode03 = DD.crcode.copy().isAPartOf(filler1,8);
	public FixedLengthStringData crcode04 = DD.crcode.copy().isAPartOf(filler1,12);
	public FixedLengthStringData crcode05 = DD.crcode.copy().isAPartOf(filler1,16);
	public FixedLengthStringData crcode06 = DD.crcode.copy().isAPartOf(filler1,20);
	public FixedLengthStringData crcode07 = DD.crcode.copy().isAPartOf(filler1,24);
	public FixedLengthStringData crcode08 = DD.crcode.copy().isAPartOf(filler1,28);
	public FixedLengthStringData crcode09 = DD.crcode.copy().isAPartOf(filler1,32);
	public FixedLengthStringData crcode10 = DD.crcode.copy().isAPartOf(filler1,36);
	public FixedLengthStringData crcode11 = DD.crcode.copy().isAPartOf(filler1,40);
	public FixedLengthStringData crcode12 = DD.crcode.copy().isAPartOf(filler1,44);
	public FixedLengthStringData crcode13 = DD.crcode.copy().isAPartOf(filler1,48);
	public FixedLengthStringData empcon = DD.empcon.copy().isAPartOf(dataFields,71);
	public ZonedDecimalData empconpc = DD.empconpc.copyToZonedDecimal().isAPartOf(dataFields,72);
	public ZonedDecimalData issdate = DD.issdate.copyToZonedDecimal().isAPartOf(dataFields,77);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData maxfunds = new FixedLengthStringData(13).isAPartOf(dataFields, 123);
	public FixedLengthStringData[] maxfund = FLSArrayPartOfStructure(13, 1, maxfunds, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(13).isAPartOf(maxfunds, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxfund01 = DD.maxfund.copy().isAPartOf(filler2,0);
	public FixedLengthStringData maxfund02 = DD.maxfund.copy().isAPartOf(filler2,1);
	public FixedLengthStringData maxfund03 = DD.maxfund.copy().isAPartOf(filler2,2);
	public FixedLengthStringData maxfund04 = DD.maxfund.copy().isAPartOf(filler2,3);
	public FixedLengthStringData maxfund05 = DD.maxfund.copy().isAPartOf(filler2,4);
	public FixedLengthStringData maxfund06 = DD.maxfund.copy().isAPartOf(filler2,5);
	public FixedLengthStringData maxfund07 = DD.maxfund.copy().isAPartOf(filler2,6);
	public FixedLengthStringData maxfund08 = DD.maxfund.copy().isAPartOf(filler2,7);
	public FixedLengthStringData maxfund09 = DD.maxfund.copy().isAPartOf(filler2,8);
	public FixedLengthStringData maxfund10 = DD.maxfund.copy().isAPartOf(filler2,9);
	public FixedLengthStringData maxfund11 = DD.maxfund.copy().isAPartOf(filler2,10);
	public FixedLengthStringData maxfund12 = DD.maxfund.copy().isAPartOf(filler2,11);
	public FixedLengthStringData maxfund13 = DD.maxfund.copy().isAPartOf(filler2,12);
	public FixedLengthStringData pentype = DD.pentype.copy().isAPartOf(dataFields,136);
	public ZonedDecimalData sfodate = DD.sfodate.copyToZonedDecimal().isAPartOf(dataFields,137);
	public FixedLengthStringData sfonum = DD.sfonum.copy().isAPartOf(dataFields,145);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(200).isAPartOf(dataArea, 161);
	public FixedLengthStringData adminrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData contypesErr = new FixedLengthStringData(52).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] contypeErr = FLSArrayPartOfStructure(13, 4, contypesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(52).isAPartOf(contypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData contype01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData contype02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData contype03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData contype04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData contype05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData contype06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData contype07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData contype08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData contype09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData contype10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData contype11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData contype12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData contype13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData crcodesErr = new FixedLengthStringData(52).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] crcodeErr = FLSArrayPartOfStructure(13, 4, crcodesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(52).isAPartOf(crcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crcode01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData crcode02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData crcode03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData crcode04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData crcode05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData crcode06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData crcode07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData crcode08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData crcode09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData crcode10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData crcode11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData crcode12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData crcode13Err = new FixedLengthStringData(4).isAPartOf(filler4, 48);
	public FixedLengthStringData empconErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData empconpcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData issdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData maxfundsErr = new FixedLengthStringData(52).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData[] maxfundErr = FLSArrayPartOfStructure(13, 4, maxfundsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(52).isAPartOf(maxfundsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxfund01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData maxfund02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData maxfund03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData maxfund04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData maxfund05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData maxfund06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData maxfund07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData maxfund08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData maxfund09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData maxfund10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData maxfund11Err = new FixedLengthStringData(4).isAPartOf(filler5, 40);
	public FixedLengthStringData maxfund12Err = new FixedLengthStringData(4).isAPartOf(filler5, 44);
	public FixedLengthStringData maxfund13Err = new FixedLengthStringData(4).isAPartOf(filler5, 48);
	public FixedLengthStringData pentypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData sfodateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData sfonumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(600).isAPartOf(dataArea, 361);
	public FixedLengthStringData[] adminrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData contypesOut = new FixedLengthStringData(156).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] contypeOut = FLSArrayPartOfStructure(13, 12, contypesOut, 0);
	public FixedLengthStringData[][] contypeO = FLSDArrayPartOfArrayStructure(12, 1, contypeOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(156).isAPartOf(contypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] contype01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] contype02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] contype03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] contype04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] contype05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] contype06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] contype07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] contype08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] contype09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] contype10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] contype11Out = FLSArrayPartOfStructure(12, 1, filler6, 120);
	public FixedLengthStringData[] contype12Out = FLSArrayPartOfStructure(12, 1, filler6, 132);
	public FixedLengthStringData[] contype13Out = FLSArrayPartOfStructure(12, 1, filler6, 144);
	public FixedLengthStringData crcodesOut = new FixedLengthStringData(156).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] crcodeOut = FLSArrayPartOfStructure(13, 12, crcodesOut, 0);
	public FixedLengthStringData[][] crcodeO = FLSDArrayPartOfArrayStructure(12, 1, crcodeOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(156).isAPartOf(crcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crcode01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] crcode02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] crcode03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] crcode04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] crcode05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] crcode06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] crcode07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] crcode08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] crcode09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] crcode10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] crcode11Out = FLSArrayPartOfStructure(12, 1, filler7, 120);
	public FixedLengthStringData[] crcode12Out = FLSArrayPartOfStructure(12, 1, filler7, 132);
	public FixedLengthStringData[] crcode13Out = FLSArrayPartOfStructure(12, 1, filler7, 144);
	public FixedLengthStringData[] empconOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] empconpcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] issdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData maxfundsOut = new FixedLengthStringData(156).isAPartOf(outputIndicators, 396);
	public FixedLengthStringData[] maxfundOut = FLSArrayPartOfStructure(13, 12, maxfundsOut, 0);
	public FixedLengthStringData[][] maxfundO = FLSDArrayPartOfArrayStructure(12, 1, maxfundOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(156).isAPartOf(maxfundsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxfund01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] maxfund02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] maxfund03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] maxfund04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] maxfund05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] maxfund06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] maxfund07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] maxfund08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] maxfund09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] maxfund10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] maxfund11Out = FLSArrayPartOfStructure(12, 1, filler8, 120);
	public FixedLengthStringData[] maxfund12Out = FLSArrayPartOfStructure(12, 1, filler8, 132);
	public FixedLengthStringData[] maxfund13Out = FLSArrayPartOfStructure(12, 1, filler8, 144);
	public FixedLengthStringData[] pentypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] sfodateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] sfonumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData issdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData sfodateDisp = new FixedLengthStringData(10);

	public LongData S6697screenWritten = new LongData(0);
	public LongData S6697protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6697ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, maxfund01, maxfund02, maxfund03, maxfund04, maxfund05, maxfund06, maxfund07, maxfund08, maxfund09, maxfund10, maxfund11, maxfund12, maxfund13, sfonum, adminref, contype01, contype02, contype03, contype04, contype05, contype06, contype07, contype08, contype09, contype10, contype11, contype12, contype13, crcode01, crcode02, crcode03, crcode04, crcode05, crcode06, crcode07, crcode08, crcode09, crcode10, crcode11, crcode12, crcode13, sfodate, issdate, empconpc, empcon, pentype};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, maxfund01Out, maxfund02Out, maxfund03Out, maxfund04Out, maxfund05Out, maxfund06Out, maxfund07Out, maxfund08Out, maxfund09Out, maxfund10Out, maxfund11Out, maxfund12Out, maxfund13Out, sfonumOut, adminrefOut, contype01Out, contype02Out, contype03Out, contype04Out, contype05Out, contype06Out, contype07Out, contype08Out, contype09Out, contype10Out, contype11Out, contype12Out, contype13Out, crcode01Out, crcode02Out, crcode03Out, crcode04Out, crcode05Out, crcode06Out, crcode07Out, crcode08Out, crcode09Out, crcode10Out, crcode11Out, crcode12Out, crcode13Out, sfodateOut, issdateOut, empconpcOut, empconOut, pentypeOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, maxfund01Err, maxfund02Err, maxfund03Err, maxfund04Err, maxfund05Err, maxfund06Err, maxfund07Err, maxfund08Err, maxfund09Err, maxfund10Err, maxfund11Err, maxfund12Err, maxfund13Err, sfonumErr, adminrefErr, contype01Err, contype02Err, contype03Err, contype04Err, contype05Err, contype06Err, contype07Err, contype08Err, contype09Err, contype10Err, contype11Err, contype12Err, contype13Err, crcode01Err, crcode02Err, crcode03Err, crcode04Err, crcode05Err, crcode06Err, crcode07Err, crcode08Err, crcode09Err, crcode10Err, crcode11Err, crcode12Err, crcode13Err, sfodateErr, issdateErr, empconpcErr, empconErr, pentypeErr};
		screenDateFields = new BaseData[] {sfodate, issdate};
		screenDateErrFields = new BaseData[] {sfodateErr, issdateErr};
		screenDateDispFields = new BaseData[] {sfodateDisp, issdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6697screen.class;
		protectRecord = S6697protect.class;
	}

}
