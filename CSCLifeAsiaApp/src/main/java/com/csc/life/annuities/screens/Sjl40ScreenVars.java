package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SJL40
 * @version 1.0 generated on 06/03/20 06:38
 * @author agoel51
 */
public class Sjl40ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(333);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(157).isAPartOf(dataArea, 0);	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData annpmntstatus = DD.premstatus_longdesc.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData annpmntopt = DD.annpmntopt.copy().isAPartOf(dataFields, 41);
	public ZonedDecimalData nextpmntdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,43);
	public ZonedDecimalData annupmntamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,51);
	public ZonedDecimalData dfrrdamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,68);
	public ZonedDecimalData totbalannamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,85);
	public ZonedDecimalData newpmntdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,102);
	public ZonedDecimalData finalpayamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,110);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,127);
	

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 157);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData annpmntstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData annpmntoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData nextpmntdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData annupmntamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);	
	public FixedLengthStringData dfrrdamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData totbalannamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData newpmntdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData finalpayamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 201);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] annpmntstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] annpmntoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] nextpmntdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] annupmntamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] dfrrdamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] totbalannamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] newpmntdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] finalpayamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);    
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 112);
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData nextpmntdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData newpmntdtDisp = new FixedLengthStringData(10);

	public LongData Sjl40screenWritten = new LongData(0);
	public LongData Sjl40protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sjl40ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(annpmntoptOut,new String[] {"01","40","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annpmntstatusOut,new String[] {"02","41","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nextpmntdtOut,new String[] {"03","42","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annupmntamtOut,new String[] {"04","43","-04",null, null, null, null, null, null, null, null, null});		
		fieldIndMap.put(dfrrdamtOut,new String[] {"05","44","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totbalannamtOut,new String[] {"06","45","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(newpmntdtOut,new String[] {"07","46","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(finalpayamtOut,new String[] {"08","47", "-08",null, null, null, null, null, null, null, null, null});
		
		
		screenFields = new BaseData[] {chdrnum, cnttype,annpmntstatus,annpmntopt,nextpmntdt,annupmntamt,dfrrdamt,totbalannamt,newpmntdt,finalpayamt,ctypedes};
		
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut,annpmntstatusOut,annpmntoptOut,nextpmntdtOut,annupmntamtOut,dfrrdamtOut,totbalannamtOut,newpmntdtOut,finalpayamtOut,ctypedesOut};	
			
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr,annpmntstatusErr,annpmntoptErr,nextpmntdtErr,annupmntamtErr,dfrrdamtErr,totbalannamtErr,newpmntdtErr,finalpayamtErr,ctypedesErr};
		
		screenDateFields = new BaseData[]{nextpmntdt,newpmntdt};
		screenDateErrFields = new BaseData[]{nextpmntdtErr, newpmntdtErr};
		screenDateDispFields = new BaseData[]{nextpmntdtDisp, newpmntdtDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;		
		screenRecord = Sjl40screen.class;
		protectRecord = Sjl40protect.class;
	}

}

