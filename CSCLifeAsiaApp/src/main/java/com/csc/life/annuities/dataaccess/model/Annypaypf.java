package com.csc.life.annuities.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ANNYPAYPF", schema = "VM1DTA")
public class Annypaypf {	

	private long uniqueNumber;
	private String chdrnum;
	private String chdrcoy;
	private String annpmntopt;
	private Integer annupmnttrm;	
	private String annpmntfreq;
	private Integer dfrrdtrm;
	private Integer pmntno;
	private Integer pmntdt;
	private Integer nextpaydt;
	private Integer effectivedt;
	private String validflag;
	private Integer tranno;
	private String batctrcde;
	private BigDecimal annaamt;
	private BigDecimal annintamt;
	private BigDecimal paidamt;
	private BigDecimal inttax;
	private BigDecimal witholdtax;
	private String pmntstat;
	private String usrprf;
	private Timestamp datime;
	
	public Annypaypf() {
		super();
	}
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public Integer getPmntno() {
		return pmntno;
	}

	public void setPmntno(Integer pmntno) {
		this.pmntno = pmntno;
	}

	public Integer getPmntdt() {
		return pmntdt;
	}

	public void setPmntdt(Integer pmntdt) {
		this.pmntdt = pmntdt;
	}

	public BigDecimal getAnnaamt() {
		return annaamt;
	}

	public void setAnnaamt(BigDecimal annaamt) {
		this.annaamt = annaamt;
	}

	public BigDecimal getAnnintamt() {
		return annintamt;
	}

	public void setAnnintamt(BigDecimal annintamt) {
		this.annintamt = annintamt;
	}

	public BigDecimal getPaidamt() {
		return paidamt;
	}

	public void setPaidamt(BigDecimal paidamt) {
		this.paidamt = paidamt;
	}

	public BigDecimal getInttax() {
		return inttax;
	}

	public void setInttax(BigDecimal inttax) {
		this.inttax = inttax;
	}

	public BigDecimal getWitholdtax() {
		return witholdtax;
	}

	public void setWitholdtax(BigDecimal witholdtax) {
		this.witholdtax = witholdtax;
	}

	public String getPmntstat() {
		return pmntstat;
	}

	public void setPmntstat(String pmntstat) {
		this.pmntstat = pmntstat;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public Timestamp getDatime() {
		return datime;
	}

	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}

	public String getAnnpmntopt() {
		return annpmntopt;
	}

	public void setAnnpmntopt(String annpmntopt) {
		this.annpmntopt = annpmntopt;
	}

	public Integer getAnnupmnttrm() {
		return annupmnttrm;
	}

	public void setAnnupmnttrm(Integer annupmnttrm) {
		this.annupmnttrm = annupmnttrm;
	}

	public String getAnnpmntfreq() {
		return annpmntfreq;
	}

	public void setAnnpmntfreq(String annpmntfreq) {
		this.annpmntfreq = annpmntfreq;
	}

	public Integer getDfrrdtrm() {
		return dfrrdtrm;
	}

	public void setDfrrdtrm(Integer dfrrdtrm) {
		this.dfrrdtrm = dfrrdtrm;
	}

	public Integer getNextpaydt() {
		return nextpaydt;
	}

	public void setNextpaydt(Integer nextpaydt) {
		this.nextpaydt = nextpaydt;
	}

	public Integer getEffectivedt() {
		return effectivedt;
	}

	public void setEffectivedt(Integer effectivedt) {
		this.effectivedt = effectivedt;
	}	
	
	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	
}
