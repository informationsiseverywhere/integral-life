/*
 * File: Gdelanny.java
 * Date: 29 August 2009 22:49:34
 * Author: Quipoz Limited
 *
 * Class transformed from GDELANNY.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This  is  a  new  Subroutine & forms part of the 9405 Annuities
* Development. It is  called  upon  via T5671 Generic Processing.
*
* It  will  be  called  using  the  standard   generic   reversal
* subroutine linkage GREVERSREC. ANNY annuity records are created
* as   new   coverage  numbers  for  the   process   of   Vesting
* Registration.
*
* The  purpose  of  this  Subroutine  is  to  delete  the current
* Valid-Flag  '1'  ANNY records, based on a match of TRANNO.
*
*****************************************************************
* </pre>
*/
public class Gdelanny extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GDELANNY";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String annyrec = "ANNYREC";
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private Greversrec greversrec = new Greversrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextAnny3180,
		error690
	}

	public Gdelanny() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		deleteAnny2000();
		/*EXIT*/
		exitProgram();
	}

protected void deleteAnny2000()
	{
		start2010();
	}

protected void start2010()
	{
		annyIO.setParams(SPACES);
		annyIO.setChdrcoy(greversrec.chdrcoy);
		annyIO.setChdrnum(greversrec.chdrnum);
		annyIO.setLife(greversrec.life);
		annyIO.setCoverage(greversrec.coverage);
		annyIO.setRider(greversrec.rider);
		annyIO.setPlanSuffix(greversrec.planSuffix);
		annyIO.setFormat(annyrec);
		annyIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		annyIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		annyIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		while ( !(isEQ(annyIO.getStatuz(),varcom.endp)
		|| isNE(annyIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(annyIO.getChdrnum(),greversrec.chdrnum))) {
			deleteAnnys3100();
		}

	}

protected void deleteAnnys3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3100();
				}
				case nextAnny3180: {
					nextAnny3180();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3100()
	{
		if (isEQ(annyIO.getTranno(),greversrec.tranno)) {
			annyIO.setFunction(varcom.deltd);
		}
		else {
			goTo(GotoLabel.nextAnny3180);
		}
		annyIO.setFormat(annyrec);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
	}

protected void nextAnny3180()
	{
		annyIO.setFunction(varcom.nextr);
		annyIO.setFormat(annyrec);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case error690: {
					error690();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.error690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void error690()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
