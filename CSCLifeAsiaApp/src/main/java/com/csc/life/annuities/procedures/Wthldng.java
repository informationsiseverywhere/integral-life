package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.life.annuities.recordstructures.Wthldngrec;
import com.csc.life.annuities.tablestructures.Tjl09rec;
import com.csc.life.annuities.tablestructures.Tjl11rec;
import com.csc.life.annuities.tablestructures.Tjl44rec;
import com.csc.life.annuities.tablestructures.Tjl45rec;
import com.csc.smart.recordstructures.Conerrrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Wthldng extends SMARTCodeModel{
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String WSAASUBR = "WTHLDNG";
	private static final Logger LOGGER = LoggerFactory.getLogger(Wthldng.class);
	protected Syserrrec syserrrec = new Syserrrec();
	protected Wsspcomn wsspcomn = new Wsspcomn();
	protected Wthldngrec wthldngrec =  new Wthldngrec(); 
	private Itempf itempf;
	private Tjl11rec tjl11rec = new Tjl11rec();
	private Tjl09rec tjl09rec = new Tjl09rec();
	
	List<Itempf> itempflist = new ArrayList<>();
	private Tjl45rec tjl45rec = new Tjl45rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Conerrrec conerrrec = new Conerrrec();
	
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	protected Clntpf clntpf = new Clntpf();
	protected boolean exitflag = false;
	
	private static final String TJL11 = "TJL11";
	private static final String TJL09 = "TJL09";
	private static final String TJL44 = "TJL44";
	private static final String TJL45 = "TJL45";
	private static final String NOWITHHOLDINGTAX = "No Withholding Tax";
	
	
	
	
	public Wthldng() {
		super();
	}

	/**
	* The mainline method is the default entry point to the class
	*/
	@Override
public void mainline(Object... parmArray)
	{
	wthldngrec.wthldngRec = convertAndSetParam(wthldngrec.wthldngRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			LOGGER.info("mainline {}", e);
		}
	}

protected void mainline000(){
	
	para010();
	exit090();
}

protected void para010(){
	

	syserrrec.subrname.set(WSAASUBR);
	wthldngrec.statuz.set(Varcom.oK);
	wthldngrec.wthldngtaxamt.set(0);
	getTotlpremiumpaid();//ILJ-626
	getTjl11Data( wthldngrec.cnttype.toString());
	if(exitflag){
		wthldngrec.wthldtaxstatus.set(NOWITHHOLDINGTAX);
		return;
	}
	getTjl09Data(tjl11rec.annuitype.toString());
	if(tjl09rec.wthldngflag.toString().trim().equals("N")
			||isNE(wthldngrec.payeetyp.toString(),"P")){
		exitflag=true;
	}
	
	if(exitflag
			||!(isEQ(wthldngrec.annpmntopt.toString(),"02")||isEQ(wthldngrec.annpmntopt.toString(),"03"))
			||isNE(wthldngrec.cownnum.toString(),wthldngrec.payee.toString())){ 
		 //02 option corresponds to Regular payment option
		
		wthldngrec.wthldtaxstatus.set(NOWITHHOLDINGTAX);
		wthldngrec.wthldngtaxamt.set(0);
		return;
	}
	CalculateWitholdingTax();
	
}

protected void getTjl11Data(String itemitem){
	
	itempf = new Itempf();

	itempf.setItempfx("IT");
	itempf.setItemcoy(wthldngrec.chdrcoy.toString().trim());
	itempf.setItemtabl(TJL11);
	itempf.setItemitem(itemitem);
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		exitflag=true;
	}
	else{
		tjl11rec.tjl11Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
}
protected void getTjl09Data(String itemitem){
	
	itempf = new Itempf();

	itempf.setItempfx("IT");
	itempf.setItemcoy(wthldngrec.chdrcoy.toString().trim());
	itempf.setItemtabl(TJL09);
	itempf.setItemitem(itemitem);
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		exitflag=true;
	}
	else{
		tjl09rec.tjl09Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
}


private void CalculateWitholdingTax(){
	BigDecimal mintaxblincm ;
	getTjl44Data(wthldngrec.chdrcoy.toString());
	getTjl45Data(wthldngrec.chdrcoy.toString());
	
	
	for(Itempf itempf1: itempflist){
		if(itempf1!=null &&
				isGTE(wthldngrec.paymtdt.getbigdata(),itempf1.getItmfrm())
				&&isLTE(wthldngrec.paymtdt.getbigdata(),itempf1.getItmto())){
			Tjl44rec tjl44rec = new Tjl44rec();
			tjl44rec.tjl44Rec.set(StringUtil.rawToString(itempf1.getGenarea()));
			wthldngrec.wthldngtaxrate.set(tjl44rec.whhtr.getbigdata());
		}
	}	
	mintaxblincm = gettaxblincminorigcurr(tjl45rec.mtoi.getbigdata(),wthldngrec.currncy.toString());
	wthldngrec.mintaxblincm.set(mintaxblincm); 
	
	if((AppVars.getInstance().getAppConfig().isVpmsEnable() && ExternalisedRules.isCallExternal("WTHLDINGTAX")))
	{
		callProgram("WTHLDINGTAX",wthldngrec);
		if(isNE(wthldngrec.statuz, Varcom.oK)) {							
				wsspcomn.edterror.set("Y");
				wthldngrec.statuz.set("BOMB");
		}
		else{
			
			if(isGT(wthldngrec.wthldngtaxamt,ZERO)){
				wthldngrec.wthldtaxstatus.set("Withhold Tax");	
			}
			else{
				wthldngrec.wthldtaxstatus.set(NOWITHHOLDINGTAX);
			}
		}
		
	}
	
}

protected void getTjl44Data(String itemitem){
	
	itempflist = itemDAO.readItempf("IT",wthldngrec.chdrcoy.toString().trim(), TJL44, itemitem);
	
	}

protected void getTjl45Data(String itemitem){
		
		itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(wthldngrec.chdrcoy.toString().trim());
		itempf.setItemtabl(TJL45);
		itempf.setItemitem(itemitem);
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf != null) {
			tjl45rec.tjl45Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		
}

protected void getTotlpremiumpaid(){
	BigDecimal totlprmpaid = BigDecimal.ZERO;
	List<Acblpf> acblpfList = acblDao.searchAcblenqRecord(wthldngrec.chdrcoy.toString(), wthldngrec.chdrnum.toString());
	for(Acblpf acblpf : acblpfList){
		if(acblpf.getSacscode().equals("LE")&&acblpf.getSacstyp().equals("LP")){
			totlprmpaid = totlprmpaid.add(acblpf.getSacscurbal().abs());
		}
			
	}
	wthldngrec.ttlprmpaid.set(totlprmpaid);
}
protected BigDecimal gettaxblincminorigcurr(BigDecimal tabltaxblincm, String paymtcurrency){
	BigDecimal taxblincm ;
	
	if (isNE(paymtcurrency.trim(),"JPY")) {
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(wthldngrec.chdrcoy);
		conlinkrec.cashdate.set(wthldngrec.paymtdt);
		conlinkrec.currIn.set("JPY");
		conlinkrec.currOut.set(paymtcurrency);
		conlinkrec.amountIn.set(tabltaxblincm);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isEQ(conlinkrec.statuz,"BOMB")) {
			conerrrec.statuz.set(conlinkrec.statuz);
			
		}
		taxblincm = conlinkrec.amountOut.getbigdata();
		
	}else {
		taxblincm= tabltaxblincm;
	}
	
	return taxblincm;
}
protected void exit090()
{
	exitProgram();
}

}
