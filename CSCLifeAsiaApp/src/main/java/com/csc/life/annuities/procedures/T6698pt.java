/*
 * File: T6698pt.java
 * Date: 30 August 2009 2:31:17
 * Author: Quipoz Limited
 * 
 * Class transformed from T6698PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.annuities.tablestructures.T6698rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6698.
*
*
*****************************************************************
* </pre>
*/
public class T6698pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6698rec t6698rec = new T6698rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6698pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6698rec.t6698Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo011.set(t6698rec.ageIssageTo01);
		generalCopyLinesInner.fieldNo012.set(t6698rec.pclimit01);
		generalCopyLinesInner.fieldNo013.set(t6698rec.ageIssageTo02);
		generalCopyLinesInner.fieldNo014.set(t6698rec.pclimit02);
		generalCopyLinesInner.fieldNo015.set(t6698rec.ageIssageTo03);
		generalCopyLinesInner.fieldNo016.set(t6698rec.pclimit03);
		generalCopyLinesInner.fieldNo017.set(t6698rec.ageIssageTo04);
		generalCopyLinesInner.fieldNo018.set(t6698rec.pclimit04);
		generalCopyLinesInner.fieldNo019.set(t6698rec.ageIssageTo05);
		generalCopyLinesInner.fieldNo020.set(t6698rec.pclimit05);
		generalCopyLinesInner.fieldNo021.set(t6698rec.ageIssageTo06);
		generalCopyLinesInner.fieldNo022.set(t6698rec.pclimit06);
		generalCopyLinesInner.fieldNo023.set(t6698rec.ageIssageTo07);
		generalCopyLinesInner.fieldNo024.set(t6698rec.pclimit07);
		generalCopyLinesInner.fieldNo025.set(t6698rec.ageIssageTo08);
		generalCopyLinesInner.fieldNo026.set(t6698rec.pclimit08);
		generalCopyLinesInner.fieldNo027.set(t6698rec.ageIssageTo09);
		generalCopyLinesInner.fieldNo028.set(t6698rec.pclimit09);
		generalCopyLinesInner.fieldNo029.set(t6698rec.ageIssageTo10);
		generalCopyLinesInner.fieldNo030.set(t6698rec.pclimit10);
		generalCopyLinesInner.fieldNo007.set(t6698rec.taxrelpc);
		generalCopyLinesInner.fieldNo009.set(t6698rec.earningCap);
		generalCopyLinesInner.fieldNo008.set(t6698rec.inrevnum);
		generalCopyLinesInner.fieldNo010.set(t6698rec.dssnum);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Pension Relief Rates                    S6698");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 25, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(68);
	private FixedLengthStringData filler9 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  % Basic Tax Relief:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine004, 22).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 28, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine004, 40, FILLER).init("Inland Revenue No.");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 60);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(68);
	private FixedLengthStringData filler12 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine005, 0, FILLER).init("  Earnings Cap:");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine005, 22).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 33, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine005, 40, FILLER).init("DSS No.");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 60);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(54);
	private FixedLengthStringData filler15 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(42).isAPartOf(wsaaPrtLine006, 12, FILLER).init("To Age           Maximum % of Gross Salary");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(41);
	private FixedLengthStringData filler17 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 13).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(41);
	private FixedLengthStringData filler19 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 13).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(41);
	private FixedLengthStringData filler21 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 13).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(41);
	private FixedLengthStringData filler23 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 13).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(41);
	private FixedLengthStringData filler25 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 13).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(41);
	private FixedLengthStringData filler27 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 13).setPattern("ZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(41);
	private FixedLengthStringData filler29 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 13).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(41);
	private FixedLengthStringData filler31 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 13).setPattern("ZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(41);
	private FixedLengthStringData filler33 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 13).setPattern("ZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine015, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(41);
	private FixedLengthStringData filler35 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 13).setPattern("ZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine016, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 35).setPattern("ZZZ.ZZ");
}
}
