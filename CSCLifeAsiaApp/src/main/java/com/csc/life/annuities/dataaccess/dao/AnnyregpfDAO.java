package com.csc.life.annuities.dataaccess.dao;

import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AnnyregpfDAO extends BaseDAO<Annyregpf>{
	public Annyregpf getAnnyregpf(String coy,String chdrnum);
	public Annyregpf getAnnyregpfbyEffdt(String chdrcoy,String chdrnum, int effectivedt, int tranno, String batctrcde);
	public void insertAnnyregpf(Annyregpf annyregpf);
	public void updateAnnyregpf(Annyregpf annyregpf);
	public void updateValidflag(Annyregpf annyregpf);
	public void updateDate(Annyregpf annyregpf);
	public void updateStat(Annyregpf annyregpf);
	public void updateAnnDate(Annyregpf annyregpf);
}
