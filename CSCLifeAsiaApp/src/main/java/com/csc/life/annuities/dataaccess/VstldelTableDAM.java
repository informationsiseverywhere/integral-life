package com.csc.life.annuities.dataaccess;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: VstldelTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:42
 * Class transformed from VSTLDEL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class VstldelTableDAM extends VstlpfTableDAM {

	public VstldelTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("VSTLDEL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "SALUT, " +
		            "SURNAME, " +
		            "GIVNAME, " +
		            "INITIALS, " +
		            "CLTADDR01, " +
		            "CLTADDR02, " +
		            "CLTADDR03, " +
		            "CLTADDR04, " +
		            "CLTADDR05, " +
		            "CLTPCODE, " +
		            "CRTABLE01, " +
		            "CRTABLE02, " +
		            "CRTABLE03, " +
		            "CRTABLE04, " +
		            "CRTABLE05, " +
		            "VSTAMNT01, " +
		            "VSTAMNT02, " +
		            "VSTAMNT03, " +
		            "VSTAMNT04, " +
		            "VSTAMNT05, " +
		            "DESCRIP01, " +
		            "DESCRIP02, " +
		            "DESCRIP03, " +
		            "DESCRIP04, " +
		            "DESCRIP05, " +
		            "CURRCD01, " +
		            "CURRCD02, " +
		            "CURRCD03, " +
		            "CURRCD04, " +
		            "CURRCD05, " +
		            "VSTDATE01, " +
		            "VSTDATE02, " +
		            "VSTDATE03, " +
		            "VSTDATE04, " +
		            "VSTDATE05, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               cowncoy,
                               cownnum,
                               salut,
                               surname,
                               givname,
                               initials,
                               cltaddr01,
                               cltaddr02,
                               cltaddr03,
                               cltaddr04,
                               cltaddr05,
                               cltpcode,
                               crtable01,
                               crtable02,
                               crtable03,
                               crtable04,
                               crtable05,
                               vestingAmount01,
                               vestingAmount02,
                               vestingAmount03,
                               vestingAmount04,
                               vestingAmount05,
                               descrip01,
                               descrip02,
                               descrip03,
                               descrip04,
                               descrip05,
                               currcd01,
                               currcd02,
                               currcd03,
                               currcd04,
                               currcd05,
                               vestingDate01,
                               vestingDate02,
                               vestingDate03,
                               vestingDate04,
                               vestingDate05,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(387+DD.cltaddr.length*5); //vdivisala ILIFE-3212
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getSalut().toInternal()
					+ getSurname().toInternal()
					+ getGivname().toInternal()
					+ getInitials().toInternal()
					+ getCltaddr01().toInternal()
					+ getCltaddr02().toInternal()
					+ getCltaddr03().toInternal()
					+ getCltaddr04().toInternal()
					+ getCltaddr05().toInternal()
					+ getCltpcode().toInternal()
					+ getCrtable01().toInternal()
					+ getCrtable02().toInternal()
					+ getCrtable03().toInternal()
					+ getCrtable04().toInternal()
					+ getCrtable05().toInternal()
					+ getVestingAmount01().toInternal()
					+ getVestingAmount02().toInternal()
					+ getVestingAmount03().toInternal()
					+ getVestingAmount04().toInternal()
					+ getVestingAmount05().toInternal()
					+ getDescrip01().toInternal()
					+ getDescrip02().toInternal()
					+ getDescrip03().toInternal()
					+ getDescrip04().toInternal()
					+ getDescrip05().toInternal()
					+ getCurrcd01().toInternal()
					+ getCurrcd02().toInternal()
					+ getCurrcd03().toInternal()
					+ getCurrcd04().toInternal()
					+ getCurrcd05().toInternal()
					+ getVestingDate01().toInternal()
					+ getVestingDate02().toInternal()
					+ getVestingDate03().toInternal()
					+ getVestingDate04().toInternal()
					+ getVestingDate05().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, salut);
			what = ExternalData.chop(what, surname);
			what = ExternalData.chop(what, givname);
			what = ExternalData.chop(what, initials);
			what = ExternalData.chop(what, cltaddr01);
			what = ExternalData.chop(what, cltaddr02);
			what = ExternalData.chop(what, cltaddr03);
			what = ExternalData.chop(what, cltaddr04);
			what = ExternalData.chop(what, cltaddr05);
			what = ExternalData.chop(what, cltpcode);
			what = ExternalData.chop(what, crtable01);
			what = ExternalData.chop(what, crtable02);
			what = ExternalData.chop(what, crtable03);
			what = ExternalData.chop(what, crtable04);
			what = ExternalData.chop(what, crtable05);
			what = ExternalData.chop(what, vestingAmount01);
			what = ExternalData.chop(what, vestingAmount02);
			what = ExternalData.chop(what, vestingAmount03);
			what = ExternalData.chop(what, vestingAmount04);
			what = ExternalData.chop(what, vestingAmount05);
			what = ExternalData.chop(what, descrip01);
			what = ExternalData.chop(what, descrip02);
			what = ExternalData.chop(what, descrip03);
			what = ExternalData.chop(what, descrip04);
			what = ExternalData.chop(what, descrip05);
			what = ExternalData.chop(what, currcd01);
			what = ExternalData.chop(what, currcd02);
			what = ExternalData.chop(what, currcd03);
			what = ExternalData.chop(what, currcd04);
			what = ExternalData.chop(what, currcd05);
			what = ExternalData.chop(what, vestingDate01);
			what = ExternalData.chop(what, vestingDate02);
			what = ExternalData.chop(what, vestingDate03);
			what = ExternalData.chop(what, vestingDate04);
			what = ExternalData.chop(what, vestingDate05);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getSalut() {
		return salut;
	}
	public void setSalut(Object what) {
		salut.set(what);
	}	
	public FixedLengthStringData getSurname() {
		return surname;
	}
	public void setSurname(Object what) {
		surname.set(what);
	}	
	public FixedLengthStringData getGivname() {
		return givname;
	}
	public void setGivname(Object what) {
		givname.set(what);
	}	
	public FixedLengthStringData getInitials() {
		return initials;
	}
	public void setInitials(Object what) {
		initials.set(what);
	}	
	public FixedLengthStringData getCltaddr01() {
		return cltaddr01;
	}
	public void setCltaddr01(Object what) {
		cltaddr01.set(what);
	}	
	public FixedLengthStringData getCltaddr02() {
		return cltaddr02;
	}
	public void setCltaddr02(Object what) {
		cltaddr02.set(what);
	}	
	public FixedLengthStringData getCltaddr03() {
		return cltaddr03;
	}
	public void setCltaddr03(Object what) {
		cltaddr03.set(what);
	}	
	public FixedLengthStringData getCltaddr04() {
		return cltaddr04;
	}
	public void setCltaddr04(Object what) {
		cltaddr04.set(what);
	}	
	public FixedLengthStringData getCltaddr05() {
		return cltaddr05;
	}
	public void setCltaddr05(Object what) {
		cltaddr05.set(what);
	}	
	public FixedLengthStringData getCltpcode() {
		return cltpcode;
	}
	public void setCltpcode(Object what) {
		cltpcode.set(what);
	}	
	public FixedLengthStringData getCrtable01() {
		return crtable01;
	}
	public void setCrtable01(Object what) {
		crtable01.set(what);
	}	
	public FixedLengthStringData getCrtable02() {
		return crtable02;
	}
	public void setCrtable02(Object what) {
		crtable02.set(what);
	}	
	public FixedLengthStringData getCrtable03() {
		return crtable03;
	}
	public void setCrtable03(Object what) {
		crtable03.set(what);
	}	
	public FixedLengthStringData getCrtable04() {
		return crtable04;
	}
	public void setCrtable04(Object what) {
		crtable04.set(what);
	}	
	public FixedLengthStringData getCrtable05() {
		return crtable05;
	}
	public void setCrtable05(Object what) {
		crtable05.set(what);
	}	
	public PackedDecimalData getVestingAmount01() {
		return vestingAmount01;
	}
	public void setVestingAmount01(Object what) {
		setVestingAmount01(what, false);
	}
	public void setVestingAmount01(Object what, boolean rounded) {
		if (rounded)
			vestingAmount01.setRounded(what);
		else
			vestingAmount01.set(what);
	}	
	public PackedDecimalData getVestingAmount02() {
		return vestingAmount02;
	}
	public void setVestingAmount02(Object what) {
		setVestingAmount02(what, false);
	}
	public void setVestingAmount02(Object what, boolean rounded) {
		if (rounded)
			vestingAmount02.setRounded(what);
		else
			vestingAmount02.set(what);
	}	
	public PackedDecimalData getVestingAmount03() {
		return vestingAmount03;
	}
	public void setVestingAmount03(Object what) {
		setVestingAmount03(what, false);
	}
	public void setVestingAmount03(Object what, boolean rounded) {
		if (rounded)
			vestingAmount03.setRounded(what);
		else
			vestingAmount03.set(what);
	}	
	public PackedDecimalData getVestingAmount04() {
		return vestingAmount04;
	}
	public void setVestingAmount04(Object what) {
		setVestingAmount04(what, false);
	}
	public void setVestingAmount04(Object what, boolean rounded) {
		if (rounded)
			vestingAmount04.setRounded(what);
		else
			vestingAmount04.set(what);
	}	
	public PackedDecimalData getVestingAmount05() {
		return vestingAmount05;
	}
	public void setVestingAmount05(Object what) {
		setVestingAmount05(what, false);
	}
	public void setVestingAmount05(Object what, boolean rounded) {
		if (rounded)
			vestingAmount05.setRounded(what);
		else
			vestingAmount05.set(what);
	}	
	public FixedLengthStringData getDescrip01() {
		return descrip01;
	}
	public void setDescrip01(Object what) {
		descrip01.set(what);
	}	
	public FixedLengthStringData getDescrip02() {
		return descrip02;
	}
	public void setDescrip02(Object what) {
		descrip02.set(what);
	}	
	public FixedLengthStringData getDescrip03() {
		return descrip03;
	}
	public void setDescrip03(Object what) {
		descrip03.set(what);
	}	
	public FixedLengthStringData getDescrip04() {
		return descrip04;
	}
	public void setDescrip04(Object what) {
		descrip04.set(what);
	}	
	public FixedLengthStringData getDescrip05() {
		return descrip05;
	}
	public void setDescrip05(Object what) {
		descrip05.set(what);
	}	
	public FixedLengthStringData getCurrcd01() {
		return currcd01;
	}
	public void setCurrcd01(Object what) {
		currcd01.set(what);
	}	
	public FixedLengthStringData getCurrcd02() {
		return currcd02;
	}
	public void setCurrcd02(Object what) {
		currcd02.set(what);
	}	
	public FixedLengthStringData getCurrcd03() {
		return currcd03;
	}
	public void setCurrcd03(Object what) {
		currcd03.set(what);
	}	
	public FixedLengthStringData getCurrcd04() {
		return currcd04;
	}
	public void setCurrcd04(Object what) {
		currcd04.set(what);
	}	
	public FixedLengthStringData getCurrcd05() {
		return currcd05;
	}
	public void setCurrcd05(Object what) {
		currcd05.set(what);
	}	
	public PackedDecimalData getVestingDate01() {
		return vestingDate01;
	}
	public void setVestingDate01(Object what) {
		setVestingDate01(what, false);
	}
	public void setVestingDate01(Object what, boolean rounded) {
		if (rounded)
			vestingDate01.setRounded(what);
		else
			vestingDate01.set(what);
	}	
	public PackedDecimalData getVestingDate02() {
		return vestingDate02;
	}
	public void setVestingDate02(Object what) {
		setVestingDate02(what, false);
	}
	public void setVestingDate02(Object what, boolean rounded) {
		if (rounded)
			vestingDate02.setRounded(what);
		else
			vestingDate02.set(what);
	}	
	public PackedDecimalData getVestingDate03() {
		return vestingDate03;
	}
	public void setVestingDate03(Object what) {
		setVestingDate03(what, false);
	}
	public void setVestingDate03(Object what, boolean rounded) {
		if (rounded)
			vestingDate03.setRounded(what);
		else
			vestingDate03.set(what);
	}	
	public PackedDecimalData getVestingDate04() {
		return vestingDate04;
	}
	public void setVestingDate04(Object what) {
		setVestingDate04(what, false);
	}
	public void setVestingDate04(Object what, boolean rounded) {
		if (rounded)
			vestingDate04.setRounded(what);
		else
			vestingDate04.set(what);
	}	
	public PackedDecimalData getVestingDate05() {
		return vestingDate05;
	}
	public void setVestingDate05(Object what) {
		setVestingDate05(what, false);
	}
	public void setVestingDate05(Object what, boolean rounded) {
		if (rounded)
			vestingDate05.setRounded(what);
		else
			vestingDate05.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getVstdates() {
		return new FixedLengthStringData(vestingDate01.toInternal()
										+ vestingDate02.toInternal()
										+ vestingDate03.toInternal()
										+ vestingDate04.toInternal()
										+ vestingDate05.toInternal());
	}
	public void setVstdates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getVstdates().getLength()).init(obj);
	
		what = ExternalData.chop(what, vestingDate01);
		what = ExternalData.chop(what, vestingDate02);
		what = ExternalData.chop(what, vestingDate03);
		what = ExternalData.chop(what, vestingDate04);
		what = ExternalData.chop(what, vestingDate05);
	}
	public PackedDecimalData getVstdate(BaseData indx) {
		return getVstdate(indx.toInt());
	}
	public PackedDecimalData getVstdate(int indx) {

		switch (indx) {
			case 1 : return vestingDate01;
			case 2 : return vestingDate02;
			case 3 : return vestingDate03;
			case 4 : return vestingDate04;
			case 5 : return vestingDate05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setVstdate(BaseData indx, Object what) {
		setVstdate(indx, what, false);
	}
	public void setVstdate(BaseData indx, Object what, boolean rounded) {
		setVstdate(indx.toInt(), what, rounded);
	}
	public void setVstdate(int indx, Object what) {
		setVstdate(indx, what, false);
	}
	public void setVstdate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setVestingDate01(what, rounded);
					 break;
			case 2 : setVestingDate02(what, rounded);
					 break;
			case 3 : setVestingDate03(what, rounded);
					 break;
			case 4 : setVestingDate04(what, rounded);
					 break;
			case 5 : setVestingDate05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getVstamnts() {
		return new FixedLengthStringData(vestingAmount01.toInternal()
										+ vestingAmount02.toInternal()
										+ vestingAmount03.toInternal()
										+ vestingAmount04.toInternal()
										+ vestingAmount05.toInternal());
	}
	public void setVstamnts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getVstamnts().getLength()).init(obj);
	
		what = ExternalData.chop(what, vestingAmount01);
		what = ExternalData.chop(what, vestingAmount02);
		what = ExternalData.chop(what, vestingAmount03);
		what = ExternalData.chop(what, vestingAmount04);
		what = ExternalData.chop(what, vestingAmount05);
	}
	public PackedDecimalData getVstamnt(BaseData indx) {
		return getVstamnt(indx.toInt());
	}
	public PackedDecimalData getVstamnt(int indx) {

		switch (indx) {
			case 1 : return vestingAmount01;
			case 2 : return vestingAmount02;
			case 3 : return vestingAmount03;
			case 4 : return vestingAmount04;
			case 5 : return vestingAmount05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setVstamnt(BaseData indx, Object what) {
		setVstamnt(indx, what, false);
	}
	public void setVstamnt(BaseData indx, Object what, boolean rounded) {
		setVstamnt(indx.toInt(), what, rounded);
	}
	public void setVstamnt(int indx, Object what) {
		setVstamnt(indx, what, false);
	}
	public void setVstamnt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setVestingAmount01(what, rounded);
					 break;
			case 2 : setVestingAmount02(what, rounded);
					 break;
			case 3 : setVestingAmount03(what, rounded);
					 break;
			case 4 : setVestingAmount04(what, rounded);
					 break;
			case 5 : setVestingAmount05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getDescrips() {
		return new FixedLengthStringData(descrip01.toInternal()
										+ descrip02.toInternal()
										+ descrip03.toInternal()
										+ descrip04.toInternal()
										+ descrip05.toInternal());
	}
	public void setDescrips(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDescrips().getLength()).init(obj);
	
		what = ExternalData.chop(what, descrip01);
		what = ExternalData.chop(what, descrip02);
		what = ExternalData.chop(what, descrip03);
		what = ExternalData.chop(what, descrip04);
		what = ExternalData.chop(what, descrip05);
	}
	public FixedLengthStringData getDescrip(BaseData indx) {
		return getDescrip(indx.toInt());
	}
	public FixedLengthStringData getDescrip(int indx) {

		switch (indx) {
			case 1 : return descrip01;
			case 2 : return descrip02;
			case 3 : return descrip03;
			case 4 : return descrip04;
			case 5 : return descrip05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDescrip(BaseData indx, Object what) {
		setDescrip(indx.toInt(), what);
	}
	public void setDescrip(int indx, Object what) {

		switch (indx) {
			case 1 : setDescrip01(what);
					 break;
			case 2 : setDescrip02(what);
					 break;
			case 3 : setDescrip03(what);
					 break;
			case 4 : setDescrip04(what);
					 break;
			case 5 : setDescrip05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCurrcds() {
		return new FixedLengthStringData(currcd01.toInternal()
										+ currcd02.toInternal()
										+ currcd03.toInternal()
										+ currcd04.toInternal()
										+ currcd05.toInternal());
	}
	public void setCurrcds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCurrcds().getLength()).init(obj);
	
		what = ExternalData.chop(what, currcd01);
		what = ExternalData.chop(what, currcd02);
		what = ExternalData.chop(what, currcd03);
		what = ExternalData.chop(what, currcd04);
		what = ExternalData.chop(what, currcd05);
	}
	public FixedLengthStringData getCurrcd(BaseData indx) {
		return getCurrcd(indx.toInt());
	}
	public FixedLengthStringData getCurrcd(int indx) {

		switch (indx) {
			case 1 : return currcd01;
			case 2 : return currcd02;
			case 3 : return currcd03;
			case 4 : return currcd04;
			case 5 : return currcd05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCurrcd(BaseData indx, Object what) {
		setCurrcd(indx.toInt(), what);
	}
	public void setCurrcd(int indx, Object what) {

		switch (indx) {
			case 1 : setCurrcd01(what);
					 break;
			case 2 : setCurrcd02(what);
					 break;
			case 3 : setCurrcd03(what);
					 break;
			case 4 : setCurrcd04(what);
					 break;
			case 5 : setCurrcd05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCrtables() {
		return new FixedLengthStringData(crtable01.toInternal()
										+ crtable02.toInternal()
										+ crtable03.toInternal()
										+ crtable04.toInternal()
										+ crtable05.toInternal());
	}
	public void setCrtables(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCrtables().getLength()).init(obj);
	
		what = ExternalData.chop(what, crtable01);
		what = ExternalData.chop(what, crtable02);
		what = ExternalData.chop(what, crtable03);
		what = ExternalData.chop(what, crtable04);
		what = ExternalData.chop(what, crtable05);
	}
	public FixedLengthStringData getCrtable(BaseData indx) {
		return getCrtable(indx.toInt());
	}
	public FixedLengthStringData getCrtable(int indx) {

		switch (indx) {
			case 1 : return crtable01;
			case 2 : return crtable02;
			case 3 : return crtable03;
			case 4 : return crtable04;
			case 5 : return crtable05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCrtable(BaseData indx, Object what) {
		setCrtable(indx.toInt(), what);
	}
	public void setCrtable(int indx, Object what) {

		switch (indx) {
			case 1 : setCrtable01(what);
					 break;
			case 2 : setCrtable02(what);
					 break;
			case 3 : setCrtable03(what);
					 break;
			case 4 : setCrtable04(what);
					 break;
			case 5 : setCrtable05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCltaddrs() {
		return new FixedLengthStringData(cltaddr01.toInternal()
										+ cltaddr02.toInternal()
										+ cltaddr03.toInternal()
										+ cltaddr04.toInternal()
										+ cltaddr05.toInternal());
	}
	public void setCltaddrs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCltaddrs().getLength()).init(obj);
	
		what = ExternalData.chop(what, cltaddr01);
		what = ExternalData.chop(what, cltaddr02);
		what = ExternalData.chop(what, cltaddr03);
		what = ExternalData.chop(what, cltaddr04);
		what = ExternalData.chop(what, cltaddr05);
	}
	public FixedLengthStringData getCltaddr(BaseData indx) {
		return getCltaddr(indx.toInt());
	}
	public FixedLengthStringData getCltaddr(int indx) {

		switch (indx) {
			case 1 : return cltaddr01;
			case 2 : return cltaddr02;
			case 3 : return cltaddr03;
			case 4 : return cltaddr04;
			case 5 : return cltaddr05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCltaddr(BaseData indx, Object what) {
		setCltaddr(indx.toInt(), what);
	}
	public void setCltaddr(int indx, Object what) {

		switch (indx) {
			case 1 : setCltaddr01(what);
					 break;
			case 2 : setCltaddr02(what);
					 break;
			case 3 : setCltaddr03(what);
					 break;
			case 4 : setCltaddr04(what);
					 break;
			case 5 : setCltaddr05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		cowncoy.clear();
		cownnum.clear();
		salut.clear();
		surname.clear();
		givname.clear();
		initials.clear();
		cltaddr01.clear();
		cltaddr02.clear();
		cltaddr03.clear();
		cltaddr04.clear();
		cltaddr05.clear();
		cltpcode.clear();
		crtable01.clear();
		crtable02.clear();
		crtable03.clear();
		crtable04.clear();
		crtable05.clear();
		vestingAmount01.clear();
		vestingAmount02.clear();
		vestingAmount03.clear();
		vestingAmount04.clear();
		vestingAmount05.clear();
		descrip01.clear();
		descrip02.clear();
		descrip03.clear();
		descrip04.clear();
		descrip05.clear();
		currcd01.clear();
		currcd02.clear();
		currcd03.clear();
		currcd04.clear();
		currcd05.clear();
		vestingDate01.clear();
		vestingDate02.clear();
		vestingDate03.clear();
		vestingDate04.clear();
		vestingDate05.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}