package com.csc.life.annuities.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ANNYREGPF", schema = "VM1DTA")
public class Annyregpf {
	
	private long uniqueNumber;
	private String chdrnum;
	private String chdrcoy;
	private Integer occdate;
	private String cntbranch;
	private String cownnum;
	private String lifecnum;
	private String cltsex;
	private String annuitype;
	private String beneftype;
	private BigDecimal totannufund;
	private String annpmntopt;
	private Integer annupmnttrm;
	private BigDecimal partpmntamt;
	private String annpmntfreq;
	private Integer dfrrdtrm;
	private BigDecimal dfrrdamt;
	private BigDecimal annupmntamt;
	private Integer fundsetupdt;
	private Integer pmntstartdt;
	private Integer docaccptdt;
	private Integer frstpmntdt;
	private Integer anniversdt;
	private Integer finalpmntdt;
	private Integer effectivedt;
	private String validflag;
	private Integer tranno;
	private String batctrcde;
	private String annpstcde;	
	private String payee;
	private String pmntcurr;
	private String rgpymop;
	private String payeeaccnum;
	private String bankCd;
	private String branchCd;
	private String bankaccdsc;
	private String bnkactyp;
	private String usrprf;
	private Timestamp datime;
	
	public Annyregpf() {
		super();
	}
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public Integer getOccdate() {
		return occdate;
	}

	public void setOccdate(Integer occdate) {
		this.occdate = occdate;
	}

	public String getCntbranch() {
		return cntbranch;
	}

	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}

	public String getCownnum() {
		return cownnum;
	}

	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}

	public String getLifecnum() {
		return lifecnum;
	}

	public void setLifecnum(String lifecnum) {
		this.lifecnum = lifecnum;
	}

	public String getCltsex() {
		return cltsex;
	}

	public void setCltsex(String cltsex) {
		this.cltsex = cltsex;
	}

	public String getAnnuitype() {
		return annuitype;
	}

	public void setAnnuitype(String annuitype) {
		this.annuitype = annuitype;
	}

	public String getBeneftype() {
		return beneftype;
	}

	public void setBeneftype(String beneftype) {
		this.beneftype = beneftype;
	}

	public BigDecimal getTotannufund() {
		return totannufund;
	}

	public void setTotannufund(BigDecimal totannufund) {
		this.totannufund = totannufund;
	}

	public String getAnnpmntopt() {
		return annpmntopt;
	}

	public void setAnnpmntopt(String annpmntopt) {
		this.annpmntopt = annpmntopt;
	}

	public Integer getAnnupmnttrm() {
		return annupmnttrm;
	}

	public void setAnnupmnttrm(Integer annupmnttrm) {
		this.annupmnttrm = annupmnttrm;
	}

	public BigDecimal getPartpmntamt() {
		return partpmntamt;
	}

	public void setPartpmntamt(BigDecimal partpmntamt) {
		this.partpmntamt = partpmntamt;
	}

	public String getAnnpmntfreq() {
		return annpmntfreq;
	}

	public void setAnnpmntfreq(String annpmntfreq) {
		this.annpmntfreq = annpmntfreq;
	}

	public Integer getDfrrdtrm() {
		return dfrrdtrm;
	}

	public void setDfrrdtrm(Integer dfrrdtrm) {
		this.dfrrdtrm = dfrrdtrm;
	}

	public BigDecimal getDfrrdamt() {
		return dfrrdamt;
	}

	public void setDfrrdamt(BigDecimal dfrrdamt) {
		this.dfrrdamt = dfrrdamt;
	}

	public BigDecimal getAnnupmntamt() {
		return annupmntamt;
	}

	public void setAnnupmntamt(BigDecimal annupmntamt) {
		this.annupmntamt = annupmntamt;
	}

	public Integer getFundsetupdt() {
		return fundsetupdt;
	}

	public void setFundsetupdt(Integer fundsetupdt) {
		this.fundsetupdt = fundsetupdt;
	}

	public Integer getPmntstartdt() {
		return pmntstartdt;
	}

	public void setPmntstartdt(Integer pmntstartdt) {
		this.pmntstartdt = pmntstartdt;
	}

	public Integer getDocaccptdt() {
		return docaccptdt;
	}

	public void setDocaccptdt(Integer docaccptdt) {
		this.docaccptdt = docaccptdt;
	}

	public Integer getFrstpmntdt() {
		return frstpmntdt;
	}

	public void setFrstpmntdt(Integer frstpmntdt) {
		this.frstpmntdt = frstpmntdt;
	}

	public Integer getAnniversdt() {
		return anniversdt;
	}

	public void setAnniversdt(Integer anniversdt) {
		this.anniversdt = anniversdt;
	}

	public Integer getFinalpmntdt() {
		return finalpmntdt;
	}

	public void setFinalpmntdt(Integer finalpmntdt) {
		this.finalpmntdt = finalpmntdt;
	}

	public String getAnnpstcde() {
		return annpstcde;
	}

	public void setAnnpstcde(String annpstcde) {
		this.annpstcde = annpstcde;
	}

	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String getPmntcurr() {
		return pmntcurr;
	}

	public void setPmntcurr(String pmntcurr) {
		this.pmntcurr = pmntcurr;
	}

	public String getRgpymop() {
		return rgpymop;
	}

	public void setRgpymop(String rgpymop) {
		this.rgpymop = rgpymop;
	}

	public String getPayeeaccnum() {
		return payeeaccnum;
	}

	public void setPayeeaccnum(String payeeaccnum) {
		this.payeeaccnum = payeeaccnum;
	}

	public String getBankCd() {
		return bankCd;
	}

	public void setBankCd(String bankCd) {
		this.bankCd = bankCd;
	}

	public String getBranchCd() {
		return branchCd;
	}

	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}

	public String getBankaccdsc() {
		return bankaccdsc;
	}

	public void setBankaccdsc(String bankaccdsc) {
		this.bankaccdsc = bankaccdsc;
	}

	public String getBnkactyp() {
		return bnkactyp;
	}

	public void setBnkactyp(String bnkactyp) {
		this.bnkactyp = bnkactyp;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public Timestamp getDatime() {
		return datime;
	}

	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}	

	public Integer getEffectivedt() {
		return effectivedt;
	}

	public void setEffectivedt(Integer effectivedt) {
		this.effectivedt = effectivedt;
	}	
	
	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
}
