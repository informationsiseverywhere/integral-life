/*
 * File: P5229.java
 * Date: 30 August 2009 0:20:52
 * Author: Quipoz Limited
 *
 * Class transformed from P5229.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import org.apache.commons.lang.StringUtils;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.VstdTableDAM;
import com.csc.life.annuities.screens.S5229ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This submenu program forms part of the 9405 Annuities Devel-
* opment and controls the submenu screen, S5229, for Vesting of
* a Deferred Annuity.
*
* The submenu forms part of Surrenders and Claims processing.
* From the submenu the user will be able to register or approve
* the vesting of a deferred annuity component.
*
* The contract number and effective date will be entered on the
* submenu screen.
*
* The contract and coverage statuses will be checked against the
* Status Requirements for Transaction table, T5679, for the
* transaction chosen.
*
* If the transaction requested is vesting register, at least one
* of the components attached to the contract must have a maturity
* method on T5687 for the component code and must be an annuity
* component registered on the Annuity Details table, T6625.
*
* The effective date of the vesting cannot be earlier than the
* Risk Cessation date less the number of lead years held on T6625
* and cannot be more than the risk cessation date plus the
* number of lag years held on T6625 for at least one of the
* components for the contract.
*
* At least one vesting registered record must exist for the
* vesting to be approved.
*
*****************************************************************
* </pre>
*/
public class P5229 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5229");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaLowerDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaUpperDate = new ZonedDecimalData(8, 0).setUnsigned();
	private String wsaaEligible = "";
	private String wsaaValidStatus = "";
	private String wsaaVests = "";
	private ZonedDecimalData wsaaEvstperd = new ZonedDecimalData(3, 0);
	private String wsaaNotFound = "";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6625 = "T6625";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String vstdrec = "VSTDREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private VstdTableDAM vstdIO = new VstdTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6625rec t6625rec = new T6625rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S5229ScreenVars sv = ScreenProgram.getScreenVars( S5229ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private  Regppf regppf = new Regppf(); 
	Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private Acblpf acblpf = new Acblpf();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	 private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);//ILIFE-3955
	 private T5645rec t5645rec = new T5645rec();
	 

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2219, 
		exit2319, 
		exit2639, 
		exit12090, 
		keeps3070, 
		batching3080, 
		exit3090
	}

	public P5229() {
		super();
		screenVars = sv;
		new ScreenModel("S5229", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		sv.efdate.set(varcom.maxdate);
		
		boolean isFeatureConfig  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUOTR006", appVars, "IT");
		 
		if(isFeatureConfig){
			sv.actionFlag.set("Y");
		}else{
			sv.actionFlag.set("N");
		}
		
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5229IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5229-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			return ;
		}
		if (isEQ(sv.action, "B")
		|| isEQ(sv.action, "A")) {
			checkForVstd2700();
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		try {
			validateChdrsel2210();
			maturityOrVesting2215();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateChdrsel2210()
	{
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(errorsInner.e186);
			goTo(GotoLabel.exit2219);
		}
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.e544);
			goTo(GotoLabel.exit2219);
		}
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim()); 
		chdrpfDAO.setCacheObject(chdrpf);
		boolean isFeatureConfig  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUOTR006", appVars, "IT");
		
		if(isFeatureConfig && isEQ(sv.action.toString() , "D") ||isEQ(sv.action.toString() , "E")){
			regppf = new Regppf();
			regppf.setChdrnum(chdrmjaIO.getChdrnum().toString());
			regppf = regpDAO.getRegpRecord(regppf);
			if (regppf == null) {
				if (isEQ(sv.chdrselErr, SPACES)){
					sv.chdrselErr.set(errorsInner.e717);
					goTo(GotoLabel.exit2219);
				}
				else{
					sv.chdrselErr.set(errorsInner.e944);
					goTo(GotoLabel.exit2219);
				}
			}
			if(isEQ(sv.action.toString() , "E")){
				Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5645",wsaaProg.toString());
				if(itempf != null)
				{
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					acblpf = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), StringUtils.rightPad(sv.chdrsel.toString(), 16), t5645rec.sacstype01.toString().trim(), chdrmjaIO.getCntcurr().toString()); 
					int balance =  acblpf!=null?acblpf.getSacscurbal().intValue():0;
					if(balance== 0) {
						if (isEQ(sv.chdrselErr, SPACES))
					  	{
								sv.chdrselErr.set(errorsInner.e717);
								goTo(GotoLabel.exit2219);
				
					  	}
					  	else
					  	{
					  		sv.chdrselErr.set(errorsInner.rrhh);
							goTo(GotoLabel.exit2219);
						}
					}
				}
				else {
						syserrrec.params.set("t5645:" + wsaaProg);
						fatalError600();
				}
			}
		}
		/*  For transactions on policies,*/
		/* check the contract selected is of the correct status*/
		/* and from correct branch.*/
		if (isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrmjaIO.getCntbranch())) {
			sv.chdrselErr.set(errorsInner.e455);
			goTo(GotoLabel.exit2219);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			goTo(GotoLabel.exit2219);
		}
		if (isNE(sv.chdrselErr, SPACES)) {
			goTo(GotoLabel.exit2219);
		}
		else {
			checkStatus2300();
		}
		if (isNE(sv.chdrselErr, SPACES)
		|| isEQ(sv.action, "I")
		|| isEQ(sv.action, "B")) {
			goTo(GotoLabel.exit2219);
		}
	}

protected void maturityOrVesting2215()
	{
		validateEffdate2400();
		if (isNE(sv.errorIndicators, SPACES)) {
			return ;
		}
		chkMaturity2500();
		if (isEQ(wsaaValidStatus, "N")) {
			sv.chdrselErr.set(errorsInner.e767);
			return ;
		}
		if (isEQ(sv.action, "A")) {
			if (isNE(wsaaVests, "Y")) {
				sv.chdrselErr.set(errorsInner.f066);
				return ;
			}
		}
		if (isEQ(wsaaEligible, "N")) {
			sv.efdateErr.set(errorsInner.f065);
		}
	}

protected void checkStatus2300()
	{
		readStatusTable2300();
	}

protected void readStatusTable2300()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		lookForStat2310();
	}

protected void lookForStat2310()
	{
		try {
			riskLoop2310();
			premLoop2315();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void riskLoop2310()
	{
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 12)
		|| isEQ(chdrmjaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()]))) {
			wsaaSub.add(1);
		}
		
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(errorsInner.e767);
			goTo(GotoLabel.exit2319);
		}
	}

protected void premLoop2315()
	{
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 12)
		|| isEQ(chdrmjaIO.getPstatcode(), t5679rec.cnPremStat[wsaaSub.toInt()]))) {
			wsaaSub.add(1);
		}
		
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(errorsInner.e767);
		}
	}

	/**
	* <pre>
	****  Effective date is mandatory, must be a valid date and cannot
	****  be earlier than the commencement date.
	* </pre>
	*/
protected void validateEffdate2400()
	{
		chkEffdate2400();
	}

protected void chkEffdate2400()
	{
		if (isEQ(sv.efdate, varcom.vrcmMaxDate)) {
			sv.efdateErr.set(errorsInner.h046);
			return ;
		}
		else {
			datcon1rec.function.set("CONV");
			datcon1rec.intDate.set(sv.efdate);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				sv.efdateErr.set(errorsInner.e032);
				return ;
			}
		}
		if (isGT(chdrmjaIO.getOccdate(), sv.efdate)) {
			sv.efdateErr.set(errorsInner.t044);
			return ;
		}
	}

protected void chkMaturity2500()
	{
		/*INITIAL*/
		wsaaEligible = "N";
		wsaaValidStatus = "N";
		/*READ-COVRMJA*/
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		wsaaNotFound = "N";
		while ( !(isEQ(wsaaNotFound, "Y")
		|| (isEQ(wsaaEligible, "Y")
		&& isEQ(wsaaVests, "Y")))) {
			covrmjaLoop2600();
		}
		
		/*EXIT*/
	}

protected void covrmjaLoop2600()
	{
		loop2610();
	}

protected void loop2610()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			wsaaNotFound = "Y";
			return ;
		}
		/* COVR statcode and pstatcode must not be matured or vesting*/
		/* status.*/
		if (isNE(wsaaValidStatus, "Y")) {
			chkComponentsStatii2620();
		}
		/*  Read T5687.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(sv.efdate);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			itdmIO.setItemtabl(t5687);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isNE(t5687rec.maturityCalcMeth, SPACES)) {
			checkVesting2640();
		}
		/* Effective date has to be within the date range set by the*/
		/* lead and lag years for it to vest.*/
		if (isGTE(sv.efdate, wsaaLowerDate)
		&& isLTE(sv.efdate, wsaaUpperDate)
		&& isEQ(wsaaValidStatus, "Y")) {
			wsaaEligible = "Y";
		}
		/*  Move NEXTR to COVRMJA-FUNCTION.*/
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void chkComponentsStatii2620()
	{
		try {
			riskLoop2630();
			premLoop2635();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void riskLoop2630()
	{
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 12)
		|| isEQ(covrmjaIO.getStatcode(), t5679rec.covRiskStat[wsaaSub.toInt()]))) {
			wsaaSub.add(1);
		}
		
		if (isGT(wsaaSub, 12)) {
			goTo(GotoLabel.exit2639);
		}
	}

protected void premLoop2635()
	{
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 12)
		|| isEQ(covrmjaIO.getPstatcode(), t5679rec.covPremStat[wsaaSub.toInt()]))) {
			wsaaSub.add(1);
		}
		
		if (isGT(wsaaSub, 12)) {
			return ;
		}
		if (isEQ(covrmjaIO.getPstatcode(), t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaValidStatus = "Y";
		}
	}

protected void checkVesting2640()
	{
		vesting2650();
	}

protected void vesting2650()
	{
		/* Read T6625 table to check if component can vest. If it can,*/
		/* set a valid effective date range for the component, otherwise*/
		/* leave this section.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(sv.efdate);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t6625)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaVests = "N";
			return ;
		}
		else {
			wsaaVests = "Y";
		}
		/* Set lower date range from lead years, moving the returned*/
		/* value into WSAA-LOWER-DATE.*/
		t6625rec.t6625Rec.set(itdmIO.getGenarea());
		compute(wsaaEvstperd, 0).set(mult(t6625rec.evstperd, -1));
		datcon2rec.intDate1.set(covrmjaIO.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaEvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaLowerDate.set(datcon2rec.intDatex2);
		/* Set upper date range from lag years, moving the returned*/
		/* value into WSAA-UPPER-DATE.*/
		datcon2rec.intDate1.set(covrmjaIO.getRiskCessDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6625rec.lvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaUpperDate.set(datcon2rec.intDatex2);
	}

protected void checkForVstd2700()
	{
		begin2710();
	}

protected void begin2710()
	{
		vstdIO.setDataArea(SPACES);
		vstdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		vstdIO.setChdrnum(chdrmjaIO.getChdrnum());
		vstdIO.setLife(ZERO);
		vstdIO.setCoverage(ZERO);
		vstdIO.setRider(ZERO);
		vstdIO.setPlanSuffix(ZERO);
		vstdIO.setTranno(ZERO);
		vstdIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		vstdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vstdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		vstdIO.setFormat(vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(), varcom.oK)
		&& isNE(vstdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vstdIO.getParams());
			syserrrec.statuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(sv.action, "B")) {
			if (isNE(vstdIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
			|| isNE(vstdIO.getChdrnum(), chdrmjaIO.getChdrnum())
			|| isEQ(vstdIO.getStatuz(), varcom.endp)) {
				sv.chdrselErr.set(errorsInner.h217);
			}
		}
		else {
			if (isEQ(sv.action, "A")) {
				if (isEQ(vstdIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
				&& isEQ(vstdIO.getChdrnum(), chdrmjaIO.getChdrnum())
				&& isNE(vstdIO.getStatuz(), varcom.endp)) {
					sv.chdrselErr.set(errorsInner.h219);
				}
			}
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT                                          */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT                                          */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case keeps3070: 
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		wsspcomn.currfrom.set(sv.efdate);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/* Update WSSP Key details*/
		if (isEQ(scrnparams.action, "I")) {
			wsspcomn.flag.set("I");
			chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrsel);
			chdrenqIO.setFunction(varcom.reads);
			chdrenqIO.setFormat(chdrenqrec);
			//chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim()); //ILIFE-6816
			chdrpfDAO.setCacheObject(chdrpf); //ILIFE-6816
			SmartFileCode.execute(appVars, chdrenqIO);
			goTo(GotoLabel.keeps3070);
		}
		else {
			if (isEQ(sv.action, "B")) {
				wsspcomn.flag.set("I");
			}
			else {
				wsspcomn.flag.set("M");
			}
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		/* Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S5229-CHDRSEL          TO SFTL-ENTITY                   */
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void keeps3070()
	{
		/* Store the contract header for use by the transaction programs*/
		chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e032 = new FixedLengthStringData(4).init("E032");
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData f065 = new FixedLengthStringData(4).init("F065");
	private FixedLengthStringData f066 = new FixedLengthStringData(4).init("F066");
	private FixedLengthStringData h217 = new FixedLengthStringData(4).init("H217");
	private FixedLengthStringData h219 = new FixedLengthStringData(4).init("H219");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData h046 = new FixedLengthStringData(4).init("H046");
	private FixedLengthStringData t044 = new FixedLengthStringData(4).init("T044");
	private FixedLengthStringData e944 = new FixedLengthStringData(4).init("E944");
	private FixedLengthStringData rrhh = new FixedLengthStringData(4).init("RRHH");
	private FixedLengthStringData e717 = new FixedLengthStringData(4).init("E717");
	
	
}
}