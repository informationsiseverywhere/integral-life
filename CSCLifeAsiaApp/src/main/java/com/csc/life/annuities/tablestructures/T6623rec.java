package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:24
 * Description:
 * Copybook name: T6623REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6623rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6623Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData advances = new FixedLengthStringData(10).isAPartOf(t6623Rec, 0);
  	public FixedLengthStringData[] advance = FLSArrayPartOfStructure(10, 1, advances, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(advances, 0, FILLER_REDEFINE);
  	public FixedLengthStringData advance01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData advance02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData advance03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData advance04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData advance05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData advance06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData advance07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData advance08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData advance09 = new FixedLengthStringData(1).isAPartOf(filler, 8);
  	public FixedLengthStringData advance10 = new FixedLengthStringData(1).isAPartOf(filler, 9);
  	public FixedLengthStringData arrearss = new FixedLengthStringData(10).isAPartOf(t6623Rec, 10);
  	public FixedLengthStringData[] arrears = FLSArrayPartOfStructure(10, 1, arrearss, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(10).isAPartOf(arrearss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData arrears01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData arrears02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData arrears03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData arrears04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData arrears05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData arrears06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData arrears07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
  	public FixedLengthStringData arrears08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
  	public FixedLengthStringData arrears09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
  	public FixedLengthStringData arrears10 = new FixedLengthStringData(1).isAPartOf(filler1, 9);
  	public FixedLengthStringData comtfacts = new FixedLengthStringData(80).isAPartOf(t6623Rec, 20);
  	public ZonedDecimalData[] comtfact = ZDArrayPartOfStructure(10, 8, 3, comtfacts, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(80).isAPartOf(comtfacts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData comtfact01 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 0);
  	public ZonedDecimalData comtfact02 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 8);
  	public ZonedDecimalData comtfact03 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 16);
  	public ZonedDecimalData comtfact04 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 24);
  	public ZonedDecimalData comtfact05 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 32);
  	public ZonedDecimalData comtfact06 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 40);
  	public ZonedDecimalData comtfact07 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 48);
  	public ZonedDecimalData comtfact08 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 56);
  	public ZonedDecimalData comtfact09 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 64);
  	public ZonedDecimalData comtfact10 = new ZonedDecimalData(8, 3).isAPartOf(filler2, 72);
  	public FixedLengthStringData freqanns = new FixedLengthStringData(20).isAPartOf(t6623Rec, 100);
  	public FixedLengthStringData[] freqann = FLSArrayPartOfStructure(10, 2, freqanns, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(freqanns, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freqann01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData freqann02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData freqann03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData freqann04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData freqann05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData freqann06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData freqann07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData freqann08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData freqann09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData freqann10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	public FixedLengthStringData guarperds = new FixedLengthStringData(30).isAPartOf(t6623Rec, 120);
  	public ZonedDecimalData[] guarperd = ZDArrayPartOfStructure(10, 3, 0, guarperds, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(30).isAPartOf(guarperds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData guarperd01 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData guarperd02 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 3);
  	public ZonedDecimalData guarperd03 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 6);
  	public ZonedDecimalData guarperd04 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 9);
  	public ZonedDecimalData guarperd05 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 12);
  	public ZonedDecimalData guarperd06 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 15);
  	public ZonedDecimalData guarperd07 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 18);
  	public ZonedDecimalData guarperd08 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 21);
  	public ZonedDecimalData guarperd09 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 24);
  	public ZonedDecimalData guarperd10 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 27);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(350).isAPartOf(t6623Rec, 150, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6623Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6623Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}