package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:17
 * Description:
 * Copybook name: T6697REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6697rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6697Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData adminref = new FixedLengthStringData(5).isAPartOf(t6697Rec, 0);
  	public FixedLengthStringData contypes = new FixedLengthStringData(13).isAPartOf(t6697Rec, 5);
  	public FixedLengthStringData[] contype = FLSArrayPartOfStructure(13, 1, contypes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(contypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData contype01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData contype02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData contype03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData contype04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData contype05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData contype06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData contype07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData contype08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData contype09 = new FixedLengthStringData(1).isAPartOf(filler, 8);
  	public FixedLengthStringData contype10 = new FixedLengthStringData(1).isAPartOf(filler, 9);
  	public FixedLengthStringData contype11 = new FixedLengthStringData(1).isAPartOf(filler, 10);
  	public FixedLengthStringData contype12 = new FixedLengthStringData(1).isAPartOf(filler, 11);
  	public FixedLengthStringData contype13 = new FixedLengthStringData(1).isAPartOf(filler, 12);
  	public FixedLengthStringData crcodes = new FixedLengthStringData(52).isAPartOf(t6697Rec, 18);
  	public FixedLengthStringData[] crcode = FLSArrayPartOfStructure(13, 4, crcodes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(52).isAPartOf(crcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crcode01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData crcode02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData crcode03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData crcode04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData crcode05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData crcode06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData crcode07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData crcode08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData crcode09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData crcode10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData crcode11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData crcode12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData crcode13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
  	public FixedLengthStringData empcon = new FixedLengthStringData(1).isAPartOf(t6697Rec, 70);
  	public ZonedDecimalData empconpc = new ZonedDecimalData(5, 2).isAPartOf(t6697Rec, 71);
  	public ZonedDecimalData issdate = new ZonedDecimalData(8, 0).isAPartOf(t6697Rec, 76);
  	public FixedLengthStringData maxfunds = new FixedLengthStringData(13).isAPartOf(t6697Rec, 84);
  	public FixedLengthStringData[] maxfund = FLSArrayPartOfStructure(13, 1, maxfunds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(13).isAPartOf(maxfunds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData maxfund01 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
  	public FixedLengthStringData maxfund02 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
  	public FixedLengthStringData maxfund03 = new FixedLengthStringData(1).isAPartOf(filler2, 2);
  	public FixedLengthStringData maxfund04 = new FixedLengthStringData(1).isAPartOf(filler2, 3);
  	public FixedLengthStringData maxfund05 = new FixedLengthStringData(1).isAPartOf(filler2, 4);
  	public FixedLengthStringData maxfund06 = new FixedLengthStringData(1).isAPartOf(filler2, 5);
  	public FixedLengthStringData maxfund07 = new FixedLengthStringData(1).isAPartOf(filler2, 6);
  	public FixedLengthStringData maxfund08 = new FixedLengthStringData(1).isAPartOf(filler2, 7);
  	public FixedLengthStringData maxfund09 = new FixedLengthStringData(1).isAPartOf(filler2, 8);
  	public FixedLengthStringData maxfund10 = new FixedLengthStringData(1).isAPartOf(filler2, 9);
  	public FixedLengthStringData maxfund11 = new FixedLengthStringData(1).isAPartOf(filler2, 10);
  	public FixedLengthStringData maxfund12 = new FixedLengthStringData(1).isAPartOf(filler2, 11);
  	public FixedLengthStringData maxfund13 = new FixedLengthStringData(1).isAPartOf(filler2, 12);
  	public FixedLengthStringData pentype = new FixedLengthStringData(1).isAPartOf(t6697Rec, 97);
  	public ZonedDecimalData sfodate = new ZonedDecimalData(8, 0).isAPartOf(t6697Rec, 98);
  	public FixedLengthStringData sfonum = new FixedLengthStringData(11).isAPartOf(t6697Rec, 106);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(383).isAPartOf(t6697Rec, 117, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6697Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6697Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}