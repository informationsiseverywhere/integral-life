package com.csc.life.annuities.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Wthldngrec extends ExternalData{
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData wthldngRec = new FixedLengthStringData(223);
	
	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(wthldngRec,0);
	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(wthldngRec,1);
	public FixedLengthStringData cownnum  = new FixedLengthStringData(10).isAPartOf(wthldngRec,9);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(wthldngRec,19);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(wthldngRec,22);
  	public FixedLengthStringData wthldtaxstatus = new FixedLengthStringData(20).isAPartOf(wthldngRec,26);
  	public FixedLengthStringData annpmntopt = new FixedLengthStringData(2).isAPartOf(wthldngRec,46);
  	public FixedLengthStringData annpmntfreq = new FixedLengthStringData(2).isAPartOf(wthldngRec,48);  	
  	public FixedLengthStringData payee = new FixedLengthStringData(10).isAPartOf(wthldngRec,50);
  	public PackedDecimalData totannufund = new PackedDecimalData(17, 2).isAPartOf(wthldngRec,60);
  	public ZonedDecimalData annupmnttrm = new ZonedDecimalData(3, 0).isAPartOf(wthldngRec,77);
  	public ZonedDecimalData paymtdt = new ZonedDecimalData(8).isAPartOf(wthldngRec,80);
	public ZonedDecimalData ttlprmpaid = new ZonedDecimalData(17,2).isAPartOf(wthldngRec,88);
	public FixedLengthStringData payeetyp = new FixedLengthStringData(1).isAPartOf(wthldngRec,105);
	public FixedLengthStringData currncy = new FixedLengthStringData(3).isAPartOf(wthldngRec,106); 
	public ZonedDecimalData wthldngtaxrate = new ZonedDecimalData(6, 3).isAPartOf(wthldngRec,109);
	public ZonedDecimalData mintaxblincm = new ZonedDecimalData(17,2).isAPartOf(wthldngRec, 115);
	public PackedDecimalData partialpmnt = new PackedDecimalData(17).isAPartOf(wthldngRec,132);
	
	public PackedDecimalData expenseratio = new PackedDecimalData(6, 3).isAPartOf(wthldngRec,149);
	public PackedDecimalData anualannamt = new PackedDecimalData(17, 2).isAPartOf(wthldngRec,155);
	public PackedDecimalData annreqdexpense = new PackedDecimalData(17, 2).isAPartOf(wthldngRec,172);
	public PackedDecimalData annothrincome = new PackedDecimalData(17, 2).isAPartOf(wthldngRec,189);
	public PackedDecimalData wthldngtaxamt = new PackedDecimalData(17).isAPartOf(wthldngRec,206);
	
	

	@Override
	public void initialize() {
		COBOLFunctions.initialize(wthldngRec);
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			wthldngRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}
