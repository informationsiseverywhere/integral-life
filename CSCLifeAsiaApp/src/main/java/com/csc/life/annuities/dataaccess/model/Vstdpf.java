package com.csc.life.annuities.dataaccess.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;

public class Vstdpf {
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private String jlife;
	private int effdate;
	private String crtable;
	private int vstlump;
	private String type01;
	private String type02;
	private String type03;
	private String type04;
	private String type05;
	private String type06;
	private BigDecimal vstamt01;
	private BigDecimal vstamt02;
	private BigDecimal vstamt03;
	private BigDecimal vstamt04;
	private BigDecimal vstamt05;
	private BigDecimal vstamt06;
	private BigDecimal vstpay;
	private BigDecimal vstperc;
	private BigDecimal vstpaya;
	private BigDecimal origpay;
	private BigDecimal elvstfct;
	private String newcovr;
	private String jobName;
	private String userProfile;
	private String datime;




	public Vstdpf() {
	}
	public Vstdpf(Vstdpf vstdpf){
		this.chdrcoy=vstdpf.chdrcoy;
		this.chdrnum=vstdpf.chdrnum;
		this.life=vstdpf.life;
		this.coverage=vstdpf.coverage;
		this.rider=vstdpf.rider;
		this.planSuffix=vstdpf.planSuffix;
		this.tranno=vstdpf.tranno;
		this.jlife=vstdpf.jlife;
		this.effdate=vstdpf.effdate;
		this.crtable=vstdpf.crtable;
		this.vstlump=vstdpf.vstlump;
		this.type01=vstdpf.type01;
		this.type02=vstdpf.type02;
		this.type03=vstdpf.type03;
		this.type04=vstdpf.type04;
		this.type05=vstdpf.type05;
		this.type06=vstdpf.type06;
		this.vstamt01=vstdpf.vstamt01;
		this.vstamt02=vstdpf.vstamt02;
		this.vstamt03=vstdpf.vstamt03;
		this.vstamt04=vstdpf.vstamt04;
		this.vstamt05=vstdpf.vstamt05;
		this.vstamt06=vstdpf.vstamt06;
		this.vstpay=vstdpf.vstpay;
		this.vstperc=vstdpf.vstperc;
		this.vstpaya=vstdpf.vstpaya;
		this.origpay=vstdpf.origpay;
		this.elvstfct=vstdpf.elvstfct;
		this.newcovr=vstdpf.newcovr;
		this.userProfile=vstdpf.userProfile;
		this.jobName=vstdpf.jobName;
		this.datime=vstdpf.datime;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getVstlump() {
		return vstlump;
	}
	public void setVstlump(int vstlump) {
		this.vstlump = vstlump;
	}
	public String getType01() {
		return type01;
	}
	public void setType01(String type01) {
		this.type01 = type01;
	}
	public String getType02() {
		return type02;
	}
	public void setType02(String type02) {
		this.type02 = type02;
	}
	public String getType03() {
		return type03;
	}
	public void setType03(String type03) {
		this.type03 = type03;
	}
	public String getType04() {
		return type04;
	}
	public void setType04(String type04) {
		this.type04 = type04;
	}
	public String getType05() {
		return type05;
	}
	public void setType05(String type05) {
		this.type05 = type05;
	}
	public String getType06() {
		return type06;
	}
	public void setType06(String type06) {
		this.type06 = type06;
	}
	public BigDecimal getVstamt01() {
		return vstamt01;
	}
	public void setVstamt01(BigDecimal vstamt01) {
		this.vstamt01 = vstamt01;
	}
	public BigDecimal getVstamt02() {
		return vstamt02;
	}
	public void setVstamt02(BigDecimal vstamt02) {
		this.vstamt02 = vstamt02;
	}
	public BigDecimal getVstamt03() {
		return vstamt03;
	}
	public void setVstamt03(BigDecimal vstamt03) {
		this.vstamt03 = vstamt03;
	}
	public BigDecimal getVstamt04() {
		return vstamt04;
	}
	public void setVstamt04(BigDecimal vstamt04) {
		this.vstamt04 = vstamt04;
	}
	public BigDecimal getVstamt05() {
		return vstamt05;
	}
	public void setVstamt05(BigDecimal vstamt05) {
		this.vstamt05 = vstamt05;
	}
	public BigDecimal getVstamt06() {
		return vstamt06;
	}
	public void setVstamt06(BigDecimal vstamt06) {
		this.vstamt06 = vstamt06;
	}
	public BigDecimal getVstpay() {
		return vstpay;
	}
	public void setVstpay(BigDecimal vstpay) {
		this.vstpay = vstpay;
	}
	public BigDecimal getVstperc() {
		return vstperc;
	}
	public void setVstperc(BigDecimal vstperc) {
		this.vstperc = vstperc;
	}
	public BigDecimal getVstpaya() {
		return vstpaya;
	}
	public void setVstpaya(BigDecimal vstpaya) {
		this.vstpaya = vstpaya;
	}
	public BigDecimal getOrigpay() {
		return origpay;
	}
	public void setOrigpay(BigDecimal origpay) {
		this.origpay = origpay;
	}
	public BigDecimal getElvstfct() {
		return elvstfct;
	}
	public void setElvstfct(BigDecimal elvstfct) {
		this.elvstfct = elvstfct;
	}
	public String getNewcovr() {
		return newcovr;
	}
	public void setNewcovr(String newcovr) {
		this.newcovr = newcovr;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}





}


