package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5233
 * @version 1.0 generated on 30/08/09 06:38
 * @author Quipoz
 */
public class S5233ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(334);
	public FixedLengthStringData dataFields = new FixedLengthStringData(174).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 174);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 214);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(183);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(53).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,2);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,10);
	public ZonedDecimalData plansfx = DD.plansfx.copyToZonedDecimal().isAPartOf(subfileFields,12);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,16);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,18);
	public ZonedDecimalData vstlump = DD.vstlump.copyToZonedDecimal().isAPartOf(subfileFields,19);
	public ZonedDecimalData vstpay = DD.vstpay.copyToZonedDecimal().isAPartOf(subfileFields,36);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 53);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData plansfxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData vstlumpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData vstpayErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 85);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] plansfxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] vstlumpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] vstpayOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 181);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S5233screensflWritten = new LongData(0);
	public LongData S5233screenctlWritten = new LongData(0);
	public LongData S5233screenWritten = new LongData(0);
	public LongData S5233protectWritten = new LongData(0);
	public GeneralTable s5233screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5233screensfl;
	}

	public S5233ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","04","-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {plansfx, life, coverage, vstpay, vstlump, effdate, select, rider};
		screenSflOutFields = new BaseData[][] {plansfxOut, lifeOut, coverageOut, vstpayOut, vstlumpOut, effdateOut, selectOut, riderOut};
		screenSflErrFields = new BaseData[] {plansfxErr, lifeErr, coverageErr, vstpayErr, vstlumpErr, effdateErr, selectErr, riderErr};
		screenSflDateFields = new BaseData[] {effdate};
		screenSflDateErrFields = new BaseData[] {effdateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, cntcurr, chdrstatus, lifenum, lifename, jlife, jlifename, ctypedes, premstatus};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, cntcurrOut, chdrstatusOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, ctypedesOut, premstatusOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, cntcurrErr, chdrstatusErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, ctypedesErr, premstatusErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5233screen.class;
		screenSflRecord = S5233screensfl.class;
		screenCtlRecord = S5233screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5233protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5233screenctl.lrec.pageSubfile);
	}
}
