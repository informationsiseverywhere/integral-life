/*
 * File: T6697pt.java
 * Date: 30 August 2009 2:31:02
 * Author: Quipoz Limited
 * 
 * Class transformed from T6697PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.annuities.tablestructures.T6697rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6697.
*
*
*****************************************************************
* </pre>
*/
public class T6697pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6697rec t6697rec = new T6697rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6697pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6697rec.t6697Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo014.set(t6697rec.maxfund01);
		generalCopyLinesInner.fieldNo017.set(t6697rec.maxfund02);
		generalCopyLinesInner.fieldNo020.set(t6697rec.maxfund03);
		generalCopyLinesInner.fieldNo023.set(t6697rec.maxfund04);
		generalCopyLinesInner.fieldNo026.set(t6697rec.maxfund05);
		generalCopyLinesInner.fieldNo029.set(t6697rec.maxfund06);
		generalCopyLinesInner.fieldNo032.set(t6697rec.maxfund07);
		generalCopyLinesInner.fieldNo035.set(t6697rec.maxfund08);
		generalCopyLinesInner.fieldNo038.set(t6697rec.maxfund09);
		generalCopyLinesInner.fieldNo041.set(t6697rec.maxfund10);
		generalCopyLinesInner.fieldNo044.set(t6697rec.maxfund11);
		generalCopyLinesInner.fieldNo047.set(t6697rec.maxfund12);
		generalCopyLinesInner.fieldNo050.set(t6697rec.maxfund13);
		generalCopyLinesInner.fieldNo005.set(t6697rec.sfonum);
		generalCopyLinesInner.fieldNo011.set(t6697rec.adminref);
		generalCopyLinesInner.fieldNo013.set(t6697rec.contype01);
		generalCopyLinesInner.fieldNo016.set(t6697rec.contype02);
		generalCopyLinesInner.fieldNo019.set(t6697rec.contype03);
		generalCopyLinesInner.fieldNo022.set(t6697rec.contype04);
		generalCopyLinesInner.fieldNo025.set(t6697rec.contype05);
		generalCopyLinesInner.fieldNo028.set(t6697rec.contype06);
		generalCopyLinesInner.fieldNo031.set(t6697rec.contype07);
		generalCopyLinesInner.fieldNo034.set(t6697rec.contype08);
		generalCopyLinesInner.fieldNo037.set(t6697rec.contype09);
		generalCopyLinesInner.fieldNo040.set(t6697rec.contype10);
		generalCopyLinesInner.fieldNo043.set(t6697rec.contype11);
		generalCopyLinesInner.fieldNo046.set(t6697rec.contype12);
		generalCopyLinesInner.fieldNo049.set(t6697rec.contype13);
		generalCopyLinesInner.fieldNo012.set(t6697rec.crcode01);
		generalCopyLinesInner.fieldNo015.set(t6697rec.crcode02);
		generalCopyLinesInner.fieldNo018.set(t6697rec.crcode03);
		generalCopyLinesInner.fieldNo021.set(t6697rec.crcode04);
		generalCopyLinesInner.fieldNo024.set(t6697rec.crcode05);
		generalCopyLinesInner.fieldNo027.set(t6697rec.crcode06);
		generalCopyLinesInner.fieldNo030.set(t6697rec.crcode07);
		generalCopyLinesInner.fieldNo033.set(t6697rec.crcode08);
		generalCopyLinesInner.fieldNo036.set(t6697rec.crcode09);
		generalCopyLinesInner.fieldNo039.set(t6697rec.crcode10);
		generalCopyLinesInner.fieldNo042.set(t6697rec.crcode11);
		generalCopyLinesInner.fieldNo045.set(t6697rec.crcode12);
		generalCopyLinesInner.fieldNo048.set(t6697rec.crcode13);
		datcon1rec.intDate.set(t6697rec.sfodate);
		callDatcon1100();
		generalCopyLinesInner.fieldNo007.set(datcon1rec.extDate);
		datcon1rec.intDate.set(t6697rec.issdate);
		callDatcon1100();
		generalCopyLinesInner.fieldNo009.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo006.set(t6697rec.empconpc);
		generalCopyLinesInner.fieldNo008.set(t6697rec.empcon);
		generalCopyLinesInner.fieldNo010.set(t6697rec.pentype);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine019);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine020);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Pension Contract Details                   S6697");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(69);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  SFO Approval No.");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine003, 33, FILLER).init("    Employee Contribution %:");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine003, 63).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(64);
	private FixedLengthStringData filler9 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  SFO Approval Date:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 22);
	private FixedLengthStringData filler10 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine004, 37, FILLER).init("Employee Contribution:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 63);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(64);
	private FixedLengthStringData filler12 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine005, 0, FILLER).init("  Issue Date:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 22);
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine005, 37, FILLER).init("Pension Type:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 63);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(27);
	private FixedLengthStringData filler15 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  Administrators Ref:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 22);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(64);
	private FixedLengthStringData filler16 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(56).isAPartOf(wsaaPrtLine007, 8, FILLER).init("Coverage/Rider    Contribution Type    Max Funding Limit");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(55);
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 6, FILLER).init("1");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 11);
	private FixedLengthStringData filler20 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine008, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler21 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 54);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(55);
	private FixedLengthStringData filler22 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 6, FILLER).init("2");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 11);
	private FixedLengthStringData filler24 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine009, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler25 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 54);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(55);
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler27 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 6, FILLER).init("3");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 11);
	private FixedLengthStringData filler28 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine010, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler29 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 54);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(55);
	private FixedLengthStringData filler30 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler31 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 6, FILLER).init("4");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 11);
	private FixedLengthStringData filler32 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine011, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler33 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 54);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(55);
	private FixedLengthStringData filler34 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 6, FILLER).init("5");
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 11);
	private FixedLengthStringData filler36 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine012, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 33);
	private FixedLengthStringData filler37 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 54);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(55);
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler39 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 6, FILLER).init("6");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 11);
	private FixedLengthStringData filler40 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine013, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 33);
	private FixedLengthStringData filler41 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 54);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(55);
	private FixedLengthStringData filler42 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler43 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 6, FILLER).init("7");
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 11);
	private FixedLengthStringData filler44 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine014, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 33);
	private FixedLengthStringData filler45 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine014, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 54);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(55);
	private FixedLengthStringData filler46 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler47 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 6, FILLER).init("8");
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 11);
	private FixedLengthStringData filler48 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine015, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 33);
	private FixedLengthStringData filler49 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine015, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 54);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(55);
	private FixedLengthStringData filler50 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler51 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 6, FILLER).init("9");
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 11);
	private FixedLengthStringData filler52 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine016, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 33);
	private FixedLengthStringData filler53 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine016, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 54);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(55);
	private FixedLengthStringData filler54 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler55 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 5, FILLER).init("10");
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 11);
	private FixedLengthStringData filler56 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine017, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 33);
	private FixedLengthStringData filler57 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine017, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 54);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(55);
	private FixedLengthStringData filler58 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler59 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 5, FILLER).init("11");
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 11);
	private FixedLengthStringData filler60 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine018, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 33);
	private FixedLengthStringData filler61 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine018, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 54);

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(55);
	private FixedLengthStringData filler62 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine019, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler63 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine019, 5, FILLER).init("12");
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine019, 11);
	private FixedLengthStringData filler64 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine019, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 33);
	private FixedLengthStringData filler65 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine019, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 54);

	private FixedLengthStringData wsaaPrtLine020 = new FixedLengthStringData(55);
	private FixedLengthStringData filler66 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine020, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler67 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine020, 5, FILLER).init("13");
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine020, 11);
	private FixedLengthStringData filler68 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine020, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine020, 33);
	private FixedLengthStringData filler69 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine020, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine020, 54);
	}
}
