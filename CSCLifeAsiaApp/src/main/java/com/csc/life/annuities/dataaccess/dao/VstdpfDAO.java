package com.csc.life.annuities.dataaccess.dao;


import com.csc.life.annuities.dataaccess.model.Vstdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface VstdpfDAO extends BaseDAO<Vstdpf> {
	
	public int getVstdpfList(String chdrcoy, String chdrnum);
	
}