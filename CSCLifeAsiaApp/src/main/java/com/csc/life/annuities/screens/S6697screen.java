package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6697screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6697ScreenVars sv = (S6697ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6697screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6697ScreenVars screenVars = (S6697ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.maxfund01.setClassString("");
		screenVars.maxfund02.setClassString("");
		screenVars.maxfund03.setClassString("");
		screenVars.maxfund04.setClassString("");
		screenVars.maxfund05.setClassString("");
		screenVars.maxfund06.setClassString("");
		screenVars.maxfund07.setClassString("");
		screenVars.maxfund08.setClassString("");
		screenVars.maxfund09.setClassString("");
		screenVars.maxfund10.setClassString("");
		screenVars.maxfund11.setClassString("");
		screenVars.maxfund12.setClassString("");
		screenVars.maxfund13.setClassString("");
		screenVars.sfonum.setClassString("");
		screenVars.adminref.setClassString("");
		screenVars.contype01.setClassString("");
		screenVars.contype02.setClassString("");
		screenVars.contype03.setClassString("");
		screenVars.contype04.setClassString("");
		screenVars.contype05.setClassString("");
		screenVars.contype06.setClassString("");
		screenVars.contype07.setClassString("");
		screenVars.contype08.setClassString("");
		screenVars.contype09.setClassString("");
		screenVars.contype10.setClassString("");
		screenVars.contype11.setClassString("");
		screenVars.contype12.setClassString("");
		screenVars.contype13.setClassString("");
		screenVars.crcode01.setClassString("");
		screenVars.crcode02.setClassString("");
		screenVars.crcode03.setClassString("");
		screenVars.crcode04.setClassString("");
		screenVars.crcode05.setClassString("");
		screenVars.crcode06.setClassString("");
		screenVars.crcode07.setClassString("");
		screenVars.crcode08.setClassString("");
		screenVars.crcode09.setClassString("");
		screenVars.crcode10.setClassString("");
		screenVars.crcode11.setClassString("");
		screenVars.crcode12.setClassString("");
		screenVars.crcode13.setClassString("");
		screenVars.sfodateDisp.setClassString("");
		screenVars.issdateDisp.setClassString("");
		screenVars.empconpc.setClassString("");
		screenVars.empcon.setClassString("");
		screenVars.pentype.setClassString("");
	}

/**
 * Clear all the variables in S6697screen
 */
	public static void clear(VarModel pv) {
		S6697ScreenVars screenVars = (S6697ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.maxfund01.clear();
		screenVars.maxfund02.clear();
		screenVars.maxfund03.clear();
		screenVars.maxfund04.clear();
		screenVars.maxfund05.clear();
		screenVars.maxfund06.clear();
		screenVars.maxfund07.clear();
		screenVars.maxfund08.clear();
		screenVars.maxfund09.clear();
		screenVars.maxfund10.clear();
		screenVars.maxfund11.clear();
		screenVars.maxfund12.clear();
		screenVars.maxfund13.clear();
		screenVars.sfonum.clear();
		screenVars.adminref.clear();
		screenVars.contype01.clear();
		screenVars.contype02.clear();
		screenVars.contype03.clear();
		screenVars.contype04.clear();
		screenVars.contype05.clear();
		screenVars.contype06.clear();
		screenVars.contype07.clear();
		screenVars.contype08.clear();
		screenVars.contype09.clear();
		screenVars.contype10.clear();
		screenVars.contype11.clear();
		screenVars.contype12.clear();
		screenVars.contype13.clear();
		screenVars.crcode01.clear();
		screenVars.crcode02.clear();
		screenVars.crcode03.clear();
		screenVars.crcode04.clear();
		screenVars.crcode05.clear();
		screenVars.crcode06.clear();
		screenVars.crcode07.clear();
		screenVars.crcode08.clear();
		screenVars.crcode09.clear();
		screenVars.crcode10.clear();
		screenVars.crcode11.clear();
		screenVars.crcode12.clear();
		screenVars.crcode13.clear();
		screenVars.sfodateDisp.clear();
		screenVars.sfodate.clear();
		screenVars.issdateDisp.clear();
		screenVars.issdate.clear();
		screenVars.empconpc.clear();
		screenVars.empcon.clear();
		screenVars.pentype.clear();
	}
}
