/*
 * File: Pd5hf.java
 * Date: 13 August 2018 0:19:15
 * Author: Quipoz Limited
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.annuities.dataaccess.RegtbrkTableDAM;
import com.csc.life.annuities.dataaccess.RegtvstTableDAM;
import com.csc.life.annuities.screens.Sd5hfScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RgpdetpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.terminationclaims.tablestructures.T6690rec;
import com.csc.life.terminationclaims.tablestructures.T6692rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
 

public class Pd5hf extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5HF");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);

	private FixedLengthStringData wsaaTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTran = new FixedLengthStringData(5).isAPartOf(wsaaTranCurrency, 0);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3).isAPartOf(wsaaTranCurrency, 5);
	private FixedLengthStringData wsaaPayclt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFreq2 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private String wsaaTotalUsed = "";
	private FixedLengthStringData wsaaBkfreqann = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaArrears = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaFreqann = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaPayamt1 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPayamt2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrcnt = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaLastPrcnt = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaInterimPrcnt = new PackedDecimalData(12, 9).setUnsigned();
	private PackedDecimalData wsaaPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaLastPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPymt2 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData index2 = new PackedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaT5671Pgm = new FixedLengthStringData(4);
		/* TABLES */
	private static final String t3590 = "T3590";
	private static final String t3629 = "T3629";
	private static final String t5606 = "T5606";
	private static final String t5671 = "T5671";
	private static final String t5688 = "T5688";
	private static final String t6690 = "T6690";
	private static final String t6691 = "T6691";
	private static final String t6692 = "T6692";
	private static final String t6694 = "T6694";
	private static final String t6696 = "T6696";
	private static final String TD5G2 = "TD5G2";
	private static final String t6625 = "T6625";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private RegtbrkTableDAM regtbrkIO = new RegtbrkTableDAM();
	private RegtvstTableDAM regtvstIO = new RegtvstTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5606rec t5606rec = new T5606rec();
	private T6690rec t6690rec = new T6690rec();
	private T6692rec t6692rec = new T6692rec();
	private T6694rec t6694rec = new T6694rec();
	private T6696rec t6696rec = new T6696rec();
	private Wssplife wssplife = new Wssplife();
	private T6625rec t6625rec = new T6625rec();
	
	private Sd5hfScreenVars sv = ScreenProgram.getScreenVars( Sd5hfScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class); 
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private Regppf regppf = new Regppf();
	private static final String vstdrec = "VSTDREC";
	private Ptrnpf ptrnpf = null;
	private Batckey wsaaBatckey1 = new Batckey();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
	private Sftlockrec sftlockrec = new Sftlockrec();
	private List<Regppf> regppfList;
	private Chdrpf chdrpf;
	private int chdrTranno;
	private List<Chdrpf> chdrBulkUpdtList;
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private String regtvstrec = "REGTVSTREC";
	private Td5h7rec Td5h7rec = new Td5h7rec(); 
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class); 
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private ZonedDecimalData wsaaConvertedNet = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private BigDecimal accAmtLPAP = new BigDecimal(0);
	private BigDecimal accAmtLPAN = new BigDecimal(0);
	private List<Acblpf> acblenqListLPAP = null;
	private List<Acblpf> acblenqListLPAN = null;
	private String oldpayMethod = "";
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO",
			AcblpfDAO.class);
	private RgpdetpfDAO rgpdetpfDAO = getApplicationContext().getBean("rgpdetpfDAO", RgpdetpfDAO.class);//ILIFE-3955
	private ItempfDAO itempfDAO =  getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private  com.csc.smart400framework.dataaccess.model.Itempf itempf = new com.csc.smart400framework.dataaccess.model.Itempf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf = null;
	private Itempf itdmpf = new Itempf();
	private Payreqrec payreqrec2 = new Payreqrec();
	private Subcoderec subcoderec = new Subcoderec();
	private T3629rec t3629rec = new T3629rec();
   
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		checkDestination2060, 
		checkForErrors2080, 
		exit2090, 
		exit4090
	}

	public Pd5hf() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5hf", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	****   Initialise fields for showing on screen.
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		wsaaPayclt.set(SPACES);
		wsaaLastFreq.set(SPACES);
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.revdate.set(varcom.vrcmMaxDate);
		wsaaPayamt1.set(ZERO);
		wsaaPayamt2.set(ZERO);
		wsaaPayrseqno.set(ZERO);
		wsaaLastPymt.set(ZERO);
		wsaaPymt.set(ZERO);
		wsaaPymt2.set(ZERO);
		wsaaLastPrcnt.set(ZERO);
		wsaaInterimPrcnt.set(ZERO);
		wsaaPrcnt.set(ZERO);
		wsaaFreq.set(ZERO);
		wsaaFreq2.set(ZERO);
		sv.pymt.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.vstpay.set(ZERO);
		sv.totalamt.set(ZERO);
		sv.totalamtOut[varcom.nd.toInt()].set("Y");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		
		descpf=descDAO.getdescData("IT", t5688, chdrmjaIO.getCnttype().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
		if (descpf!=null) {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		
		descpf=descDAO.getdescData("IT", t3629, chdrmjaIO.getCntcurr().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
		if (descpf !=null) {
			sv.currds.set(descpf.getShortdesc());
		}
		
		String ownerName = plainnameOwner(chdrmjaIO.getCownnum().toString());
		sv.ownername.set(ownerName);
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cownnum.set(chdrmjaIO.getCownnum());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.currcd.set(chdrmjaIO.getCntcurr());
		
		chdrTranno = chdrmjaIO.tranno.toInt() + 1;
		
		List<Covrpf> list = covrpfDAO.getCovrmjaByComAndNum(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
		if(list !=null && list.size()!=0){
			covrpf = list.get(0);
		}
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItmfrm(new BigDecimal(getCurrentBusinessDate()));
		itempf.setItmto(new BigDecimal(getCurrentBusinessDate()));
		itempf.setItemtabl("TD5H7");
		itempf.setItemitem(chdrmjaIO.getCnttype().toString());
		itempf.setValidflag("1");
		List<com.csc.smart400framework.dataaccess.model.Itempf> itempfList  = itemDAO.findByItemDates(itempf);
		
		if(itempfList!=null && !itempfList.isEmpty()){
		Td5h7rec.td5h7Rec.set(StringUtil.rawToString( itempfList.get(0).getGenarea()));
		} 
		getClientDetails1200();
		readRegpTable();
		readT6625();
		
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5645",wsaaProg.toString().trim());
		
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		acblenqListLPAP = acblpfDAO.getAcblenqRecord(
				wsspcomn.company.toString(), StringUtils.rightPad(chdrmjaIO.getChdrnum().toString(), 16),t5645rec.sacscode[1].toString(),t5645rec.sacstype[1].toString());

		acblenqListLPAN =acblpfDAO.getAcblenqRecord(
				wsspcomn.company.toString(), StringUtils.rightPad(chdrmjaIO.getChdrnum().toString(), 16),t5645rec.sacscode[2].toString(),t5645rec.sacstype[2].toString());
		
		if (acblenqListLPAP != null && !acblenqListLPAP.isEmpty()) {
			accAmtLPAP = acblenqListLPAP.get(0).getSacscurbal();
			accAmtLPAP = new BigDecimal(ZERO.toString()).subtract(accAmtLPAP);	
		}
		if (acblenqListLPAN != null &&    !acblenqListLPAN.isEmpty()) {
			accAmtLPAN =  acblenqListLPAN.get(0).getSacscurbal();
			accAmtLPAN = new BigDecimal(ZERO.toString()).subtract(accAmtLPAN);
			}
		
		oldpayMethod = sv.rgpymop.toString();
		
	}
	
	protected void readT6625(){
	itdmpf = itempfDAO.readItdmpf("IT",wsspcomn.company.toString(),t6625,wsspcomn.currfrom.toInt(),//IJTI-1410
			StringUtils.rightPad(covrpf.getCrtable(), 8));
	if(itdmpf !=null){
		t6625rec.t6625Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
	}
	
	String t6625ItemItem;
	if (isEQ(wsspcomn.flag,"M")) {
		t6625ItemItem =  t6625rec.ncovvest.toString();
	}
	else {
		t6625ItemItem = covrpf.getCrtable();//IJTI-1410
	}	
	List<com.csc.smart400framework.dataaccess.model.Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), t6690, t6625ItemItem);
	if (!items.isEmpty()) {
		t6690rec.t6690Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	}

	descpf=descDAO.getdescData("IT", t6691, t6690rec.rgpytype.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
	if (descpf !=null) {
		sv.rgpytypesd.set(descpf.getShortdesc());
	}
	
	/*    Read the frequency code description.*/
	descpf=descDAO.getdescData("IT", t3590, regppf.getRegpayfreq(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
	if (descpf !=null) {
		sv.frqdesc.set(descpf.getShortdesc());
	}
	
	descpf=descDAO.getdescData("IT", t3629, regppf.getCurrcd(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
	if (descpf !=null) {
		sv.clmcurdsc.set(descpf.getShortdesc());

	}
}
protected void readRegpTable(){
	regppf.setChdrnum(chdrmjaIO.chdrnum.toString());
	regppf = regpDAO.getRegpRecord(regppf);
	if (regppf != null) {
		sv.rgpynum.set(regppf.getRgpynum());
		sv.regpayfreq.set(regppf.getRegpayfreq());
		wsaaFreq2.set(regppf.getRegpayfreq());
		wsaaLastFreq.set(regppf.getRegpayfreq());
		sv.claimcur.set(regppf.getCurrcd());
		sv.crtdate.set(regppf.getCrtdate());
		sv.revdate.set(regppf.getRevdte());
		sv.firstPaydate.set(regppf.getFirstPaydate());
		sv.cltype.set(regppf.getPayreason());
		sv.claimevd.set(regppf.getClaimevd());
		sv.rgpymop.set(regppf.getRgpymop());
		sv.pymt.set(regppf.getPymt());
		sv.prcnt.set(regppf.getPrcnt());
		sv.destkey.set(regppf.getDestkey());
		sv.totalamt.set(regppf.getTotamnt());
		sv.anvdate.set(regppf.getAnvdate());
		sv.finalPaydate.set(regppf.getFinalPaydate());
		sv.payclt.set(regppf.getPayclt());	
		if(isNE(regppf.getPayclt(),SPACES)){
			sv.payclt.set(regppf.getPayclt());
			plainnamePayee(regppf.getPayclt());
		}
		chdrpf = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), chdrmjaIO.getChdrnum().toString());
		if (chdrpf != null) {
			sv.lifcnum.set(chdrpf.getCownnum());
			plainname(chdrpf.getCownnum());
		}
		moveRegppfToRegtvstIO();
		sv.vstpay.set(regppf.getPymt());
	}
}
protected void moveRegppfToRegtvstIO(){
	regtvstIO.setChdrnum(regppf.getChdrnum());
	regtvstIO.setChdrcoy(regppf.getChdrcoy());
	regtvstIO.setLife(regppf.getLife());
	regtvstIO.setCoverage(regppf.getCoverage());
	regtvstIO.setRider(regppf.getRider());
	regtvstIO.setGlact(regppf.getGlact());
	regtvstIO.setDebcred(regppf.getDebcred());
	regtvstIO.setDestkey(regppf.getDestkey()); 
	regtvstIO.setPaycoy(regppf.getPaycoy());
	regtvstIO.setPayclt(chdrmjaIO.getCownnum());
	regtvstIO.setRgpymop(regppf.getRgpymop());
	regtvstIO.setRegpayfreq(regppf.getRegpayfreq());
	regtvstIO.setCurrcd(regppf.getCurrcd());
	regtvstIO.setPymt(regppf.getPymt());
	regtvstIO.setPrcnt(regppf.getPrcnt());
	regtvstIO.setTotamnt(regppf.getTotamnt());
	regtvstIO.setPayreason(regppf.getPayreason());
	regtvstIO.setClaimevd(regppf.getClaimevd());
	regtvstIO.setCrtdate(regppf.getCrtable());
	regtvstIO.setFirstPaydate(regppf.getFirstPaydate());
	regtvstIO.setRevdte(regppf.getRevdte());
	regtvstIO.setFinalPaydate(regppf.getFinalPaydate());
	regtvstIO.setAnvdate(regppf.getAnvdate());
	regtvstIO.setRgpytype(regppf.getRgpytype());
	regtvstIO.setCrtable(regppf.getCrtable());
	regtvstIO.setPlanSuffix(regppf.getPlanSuffix());
	regtvstIO.setFormat(regtvstrec);
	regtvstIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, regtvstIO);
	if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
		regtvstIO.setParams(regtvstIO.getParams());
		fatalError600();
	}
}


protected void getClientDetails1200()
	{
	clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), chdrmjaIO.getCownnum().toString());
	if (clntpf == null) {
		syserrrec.params.set(chdrmjaIO.getCownnum().toString());
		fatalError600();

	}
	}
	/**
	 * fwang3 ICIL-4
	 */
	private void initPayoutOption() {
		String item = chdrmjaIO.getCnttype().trim() + "1";
		List<com.csc.smart400framework.dataaccess.model.Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), TD5G2, item);
		if (items.isEmpty()) {
			sv.payoutoptionErr.set("RRAX"); // not defined in TD5G2
			wsspcomn.edterror.set("Y");
			sv.payoutoption.set(SPACE);
		} else {
			sv.payoutoption.set(getDefaultPayoutOption(item));
		}
	}

	/**
	 * fwang3 ICIL-4
	 */
	private String getDefaultPayoutOption(String item) {
		
		descpf=descDAO.getdescData("IT", TD5G2, item, wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
		String sdesc = "";
		if (descpf !=null) {
			sdesc = descpf.getShortdesc();//IJTI-1410
		}
		return sdesc;
	}	
	
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     Retrieve screen fields and exit.
	* </pre>
	*/
protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.flag,"M")) {
			protectScr2100();
		}
		/*    If a lump sum payment is being processed, protect the payment*/
		/*    amount, the percentage,the frequency and also the last       */
		/*    payment date.                                                */
		if (isEQ(regppf.getRegpayfreq(),"00")) {
			sv.pymtOut[varcom.pr.toInt()].set("Y");
			sv.prcntOut[varcom.pr.toInt()].set("Y");
			sv.prcntOut[varcom.nd.toInt()].set("Y");
			sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
			sv.epaydateOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
					//checkAnnuityPayoutOption();
					checkBankDetails2035();
					checkCurrency2040();
					checkFrequency2045();
					validateDestinationKey();
					checkPercentage2055();
					checkAnnutiyFrequency();

					checkPayDates2075();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
protected void validateDestinationKey(){
	if(sv.rgpymop.toString() != " " || sv.rgpymop.toString() != null) {
		if(sv.rgpymop.toString().equalsIgnoreCase("T") || 
				sv.rgpymop.toString().equalsIgnoreCase("P")){
				sv.destkeyOut[varcom.pr.toInt()].set("N");
				if(isEQ(sv.destkey,SPACES)){
					sv.destkeyErr.set(errorsInner.e186);
				}
		} else{
		//	sv.destkey.set("");
			sv.destkeyOut[varcom.pr.toInt()].set("Y");
		}
	}
}
protected void checkAnnutiyFrequency(){ 
	
	if(!sv.regpayfreq.toString().equalsIgnoreCase("01") && sv.rgpymop.toString().equalsIgnoreCase("A")){
		sv.rgpymopErr.set(errorsInner.rrfp);
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

}
protected void screenIo2010()
	{
		/*    CALL 'Sd5hfIO'              USING SCRN-SCREEN-PARAMS         */
		/*                                        Sd5hf-DATA-AREA.         */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		wsspcomn.chdrCownnum.set(chdrpf.getCownnum());//IJTI-1410
		wsspcomn.chdrClntpfx.set("CN");
		wsspcomn.chdrClntcoy.set(wsspcomn.fsuco);
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*    Validate fields.*/
		if (isNE(scrnparams.statuz,varcom.oK)) {
			wsspcomn.edterror.set("Y");
			if (isNE(scrnparams.statuz,varcom.calc)) {
				scrnparams.errorCode.set(errorsInner.curs);
				goTo(GotoLabel.exit2090);
			}
		}
		if ((isNE(sv.ddind,"+")
		&& isNE(sv.ddind,"X")
		&& isNE(sv.ddind,SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			if ((isEQ(sv.ddind,"X")
			&& isEQ(regppf.getBankkey(),SPACES))) {
				sv.ddindErr.set(errorsInner.e493);
				wsspcomn.edterror.set("Y");
			}
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.cltype,SPACES)) {
			sv.cltypeErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	
	List<com.csc.smart400framework.dataaccess.model.Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), t6692, sv.cltype.toString());
	if (!items.isEmpty()) {
		t6692rec.t6692Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	} 

	if(items.get(0) == null || items.size() == 0 ){
		sv.cltypeErr.set(errorsInner.g521);
	}else {
		descpf=descDAO.getdescData("IT", t6692, sv.cltype.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
		if (descpf!=null) {
			descpf.getLongdesc();
		}
	}
	wsaaT6696Crtable.set(covrpf.getCrtable());
	wsaaT6696Cltype.set(sv.cltype);
	itdmpf = itempfDAO.readItdmpf("IT",wsspcomn.company.toString(),t6696,wsspcomn.currfrom.toInt(),//IJTI-1410
			StringUtils.rightPad(wsaaT6696Key.toString(), 8)
			);
	if(itdmpf !=null){
		if ((isNE(wsspcomn.company,itdmpf.getItemcoy()))
				|| (isNE(t6696,itdmpf.getItemtabl()))
				|| (isNE(wsaaT6696Key,itdmpf.getItemitem()))) {

			sv.cltypeErr.set(errorsInner.g522);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		t6696rec.t6696Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
	}
	}
private void checkAnnuityPayoutOption() {
	if (isEQ(sv.payoutoption, SPACES)) {
		sv.payoutoptionErr.set(errorsInner.e186);
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}
}
	/**
	* <pre>
	*    Check if Bank Details are required on the current Contract.
	* </pre>
	*/
protected void checkBankDetails2035()
	{
	itdmpf = itempfDAO.readItdmpf("IT",wsspcomn.company.toString(),t6694,wsspcomn.currfrom.toInt(),//IJTI-1410
			StringUtils.rightPad(sv.rgpymop.toString(), 8));
			
		if(itdmpf !=null){
			if ((isNE(wsspcomn.company,itdmpf.getItemcoy()))
					|| (isNE(t6694,itdmpf.getItemtabl()))
					|| (isNE(sv.rgpymop,itdmpf.getItemitem().trim()))) {
				sv.rgpymopErr.set(errorsInner.g529);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}else {

				descpf=descDAO.getdescData("IT", t6694, sv.rgpymop.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
				if (descpf !=null) {
					sv.rgpyshort.set(descpf.getShortdesc());
				}
		t6694rec.t6694Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
		setBankIndicator2200();
	}
		
		}
		
	}
protected void checkCurrency2040()
	{
		if (isEQ(sv.claimcur,SPACES)) {
			sv.claimcur.set(chdrmjaIO.getCntcurr());
		}
		descpf=descDAO.getdescData("IT", t3629, sv.claimcur.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
		if (descpf !=null) {
			sv.clmcurdsc.set(descpf.getShortdesc());
		}
	}

protected void checkFrequency2045()
	{
		if (isEQ(sv.regpayfreq,SPACES)) {
			sv.regpayfreqErr.set(errorsInner.e186);
		}
		else {
			if ((isNE(sv.regpayfreq,wsaaLastFreq)
			&& isEQ(sv.errorIndicators,SPACES))) {
				calculateFrequency2300();
			}
		}
	}

protected void checkPercentage2055()
	{

		if ((isEQ(sv.pymt,ZERO))
		|| (isNE(sv.prcnt,wsaaLastPrcnt)
		&& isNE(sv.prcnt,ZERO)
		&& isEQ(sv.pymt,wsaaLastPymt))) {
			calculatePayment2500();
		}
		else {
			if (isNE(sv.pymt,wsaaLastPymt)
			|| isEQ(sv.prcnt,ZERO)) {
				if (isLT(sv.pymt,0)) {
					sv.pymtErr.set(errorsInner.f351);
				}
				else {
					calculatePercentage2600();
				}
			}
		}
		if ((isEQ(sv.pymtErr,SPACES)
		&& isEQ(sv.prcntErr,SPACES))) {
			wsaaLastPymt.set(sv.pymt);
			wsaaLastPrcnt.set(sv.prcnt);
		}
	}

protected void checkDestination2060()
	{
		if ((isEQ(t6694rec.contreq,"Y")
		&& isEQ(sv.destkeyErr,SPACES))) {
			//chdrenqIO.setChdrnum(sv.destkey);
			checkContractNo2700();
		}

	}

protected void checkPayDates2075()
	{
		if ((isNE(sv.anvdate,varcom.vrcmMaxDate)
		&& isNE(sv.anvdate,ZERO))) {
			if (isLT(sv.anvdate,sv.crtdate)) {
				sv.anvdateErr.set(errorsInner.g534);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		if ((isEQ(sv.anvdate,varcom.vrcmMaxDate)
		|| isEQ(sv.anvdate,ZERO))) {
			if (isEQ(t6696rec.inxfrq,SPACES)) {
				sv.anvdate.set(varcom.vrcmMaxDate);
			}
			else {
				datcon2rec.freqFactor.set(1);
				datcon2rec.frequency.set(t6696rec.inxfrq);
				datcon2rec.intDate2.set(ZERO);
				datcon2rec.intDate1.set(sv.crtdate);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				sv.anvdate.set(datcon2rec.intDate2);
			}
		}
		if ((isNE(sv.finalPaydate,varcom.vrcmMaxDate))
		&& (isNE(sv.finalPaydate,ZERO))) {
			if (isLT(sv.finalPaydate,sv.firstPaydate)) {
				sv.epaydateErr.set(errorsInner.g311);
			}
		}
		else {
			sv.finalPaydate.set(varcom.vrcmMaxDate);
		}
		if (isNE(wsaaPayclt,sv.payclt)) {
			if (isEQ(t6694rec.bankreq,"Y")) {
				regppf.setBankkey("");
				regppf.setBankacckey("");
				sv.ddind.set("X");
			}
		}

		
		if (isNE(regppf.getRegpayfreq(),"00")
		&& isEQ(sv.regpayfreqErr,SPACES)) {
			startAccumulation2800();
		}

	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void protectScr2100()
	{
		/*PROTECT*/
		sv.paycltOut[varcom.pr.toInt()].set("Y");
//		sv.cltypeOut[varcom.pr.toInt()].set("Y");
		sv.claimevdOut[varcom.pr.toInt()].set("Y");
		
		if (isEQ(wsspcomn.flag,"I")){
		sv.rgpymopOut[varcom.pr.toInt()].set("Y");
		}
//		sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
//		sv.destkeyOut[varcom.pr.toInt()].set("Y");
		sv.pymtOut[varcom.pr.toInt()].set("Y");
		sv.claimcurOut[varcom.pr.toInt()].set("Y");
		sv.prcntOut[varcom.pr.toInt()].set("Y");
		sv.anvdateOut[varcom.pr.toInt()].set("Y");
		sv.epaydateOut[varcom.pr.toInt()].set("Y");
//		sv.payoutoptionOut[varcom.pr.toInt()].set("Y");//fwang3 ICIL-4
		/*EXIT*/
	}

protected void setBankIndicator2200()
	{
			set2210();
		}

protected void set2210()
	{
		if (isNE(sv.ddindErr,SPACES)) {
			return ;
		}

		if (isEQ(t6694rec.bankreq,"Y")) {
			if ((isEQ(regtvstIO.getBankkey(), SPACES))
					|| (isEQ(regtvstIO.getBankacckey(), SPACES))) {
				sv.ddind.set("X");
			} else {
				if (isNE(sv.ddind, "X")) {
					sv.ddind.set("+");
				}
			}
		}
		
	
		
		else {
			if (isEQ(sv.ddind,"X")) {
				sv.ddindErr.set(errorsInner.e570);
			}
			else {
				regtvstIO.setBankkey(SPACES);
				regtvstIO.setBankacckey(SPACES);
				sv.ddind.set(SPACES);
			}
		}
	}

protected void calculateFrequency2300()
	{
			calculation2310();
		}

protected void calculation2310()
	{
		if (isNE(regppf.getRegpayfreq(),"00")
		&& isEQ(sv.regpayfreq,"00")) {
			sv.regpayfreqErr.set(errorsInner.e925);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(t6696rec.frqoride,"N")) {
			sv.regpayfreq.set(wsaaBkfreqann);
			sv.regpayfreqErr.set(errorsInner.g514);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaLastFreq.set(sv.regpayfreq);
			wsaaFreq2.set(sv.regpayfreq);
			wsaaFreq.set(t5606rec.benfreq);
			wsaaPayamt2.set(ZERO);
			compute(wsaaPayamt2, 3).setRounded(div((mult(wsaaPayamt1,wsaaFreq)),wsaaFreq2));
			zrdecplrec.amountIn.set(wsaaPayamt2);
			callRounding5000();
			wsaaPayamt2.set(zrdecplrec.amountOut);
			sv.vstpay.set(regtvstIO.getTotamnt());
			
			descpf=descDAO.getdescData("IT", t3590, sv.regpayfreq.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1410
			if (descpf !=null) {
				sv.frqdesc.set(descpf.getShortdesc());
			}
	
			recalculateFirstPaydate2350();
			if (isEQ(sv.pymt,wsaaLastPymt)
			&& isEQ(sv.prcnt,wsaaLastPrcnt)) {
				calculatePercentage2600();
				wsaaLastPrcnt.set(sv.prcnt);
			}
		}
	}

protected void recalculateFirstPaydate2350()
	{
		/*RECALC*/
		if (isNE(wsaaArrears,SPACES)) {
			datcon2rec.intDate1.set(wsspcomn.currfrom);
			datcon2rec.frequency.set(sv.regpayfreq);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			sv.firstPaydate.set(datcon2rec.intDate2);
		}
		/*EXIT*/
	}

protected void calculatePayment2500()
	{
		/*CALCULATION*/
		if (isEQ(sv.pymt,ZERO)
		&& isEQ(sv.prcnt,ZERO)) {
			sv.prcnt.set(t6696rec.dfclmpct);
		}
		compute(sv.pymt, 3).setRounded(div((mult(wsaaPayamt2,sv.prcnt)),100));
		zrdecplrec.amountIn.set(sv.pymt);
		callRounding5000();
		sv.pymt.set(zrdecplrec.amountOut);
		/*    Check that % is not greater than maximum % allowed in T6696.*/
		/*    If it is, error on the percentage field.*/
		if (isGT(sv.prcnt,t6696rec.mxovrpct)) {
			sv.prcntErr.set(errorsInner.g515);
		}
		/*    Check that % is not less than minimum % allowed in T6696.*/
		/*    If it is, error on the percentage field.*/
		if (isLT(sv.prcnt,t6696rec.mnovrpct)) {
			sv.prcntErr.set(errorsInner.g517);
		}
		/*EXIT*/
	}

protected void calculatePercentage2600()
	{
		/*CALCULATION*/
		/*    As the payment amount has been changed, we must recalculate*/
		/*    the percentage based on this new payment amount.*/
		/*    If we are dealing with a lump-sum (Sd5hf-REGPAYFREQ = '00'),*/
		/*    the percentage should be set to zeroes.*/
		if (isEQ(sv.regpayfreq,"00")) {
			sv.prcnt.set(ZERO);
			return ;
		}
		compute(wsaaInterimPrcnt, 10).setRounded(div(sv.pymt,wsaaPayamt2));
		compute(wsaaPrcnt, 10).setRounded(mult(wsaaInterimPrcnt,100));
		sv.prcnt.set(wsaaPrcnt);
		/*    Check that % calculated is not greater than maximum % allowed*/
		/*    in T6696.  If it is, error on the payment field as the amount*/
		/*    is too large.*/
		if (isGT(sv.prcnt,t6696rec.mxovrpct)) {
			sv.pymtErr.set(errorsInner.g515);
		}
		/*    Check that % calculated is not less than minimum % allowed*/
		/*    in T6696.  If it is and the payment amount is zero, error on*/
		/*    the % field, since the default claim % on T6696 is incorrect,*/
		/*    else error on the payment field as the amount is too small.*/
		if (isLT(sv.prcnt,t6696rec.mnovrpct)) {
			sv.pymtErr.set(errorsInner.g517);
		}
	}

protected void checkContractNo2700()
	{
	
	chdrpf = chdrpfDAO.getChdrenqRecord(chdrmjaIO.getChdrcoy().toString(),sv.destkey.toString());
	if(chdrpf == null){
		sv.destkeyErr.set(errorsInner.g126);
		wsspcomn.edterror.set("Y");
	}
		/*EXIT*/
	}

protected void startAccumulation2800()
	{
		begin2810();
	}

protected void begin2810()
	{
		wsaaPymt.set(ZERO);
		wsaaPymt2.set(ZERO);
		regtbrkIO.setChdrcoy(regppf.getChdrcoy());
		regtbrkIO.setChdrnum(regppf.getChdrnum());
		regtbrkIO.setLife(regppf.getLife());
		regtbrkIO.setCoverage(regppf.getCoverage());
		regtbrkIO.setRider(regppf.getRider());
		regtbrkIO.setRgpynum(ZERO);
		regtbrkIO.setSeqnbr(ZERO);
		regtbrkIO.setPlanSuffix(regppf.getPlanSuffix());
		regtbrkIO.setStatuz(varcom.oK);
		regtbrkIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regtbrkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtbrkIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(regtbrkIO.getStatuz(),varcom.endp))) {
			accumRegt2900();
		}
		
		compute(wsaaPymt, 3).setRounded(add(wsaaPymt,sv.pymt));
	}

protected void accumRegt2900()
	{
			accum2910();
		}

protected void accum2910()
	{
		SmartFileCode.execute(appVars, regtbrkIO);
		if ((isNE(regtbrkIO.getStatuz(),varcom.oK))
		&& (isNE(regtbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtbrkIO.getParams());
			fatalError600();
		}
		if (isEQ(regtbrkIO.getStatuz(),varcom.endp)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT                                          */
			return ;
		}
		/*    Check all claims and add up all payment amounts excluding the*/
		/*    one which is on screen, namely when*/
		/*    REGTBRK-RGPYNUM = Sd5hf-RGPYNUM.*/
		/*    If the REGT record found does not belong to the same coverage*/
		/*    and rider or does not have the same plan suffix, move ENDP to*/
		/*    the status to end the loop.*/
		if ((isNE(regppf.getChdrcoy(),regtbrkIO.getChdrcoy()))
		|| (isNE(regppf.getChdrnum(),regtbrkIO.getChdrnum()))
		|| (isNE(regppf.getLife(),regtbrkIO.getLife()))
		|| (isNE(regppf.getCoverage(),regtbrkIO.getCoverage()))
		|| (isNE(regppf.getRider(),regtbrkIO.getRider()))
		|| (isNE(regppf.getPlanSuffix(),regtbrkIO.getPlanSuffix()))) {
			regtbrkIO.setStatuz(varcom.endp);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT                                          */
			return ;
		}
		if ((isNE(regtbrkIO.getRgpynum(),sv.rgpynum)
		&& isNE(regtbrkIO.getRegpayfreq(),"00"))) {
			if (isEQ(regtbrkIO.getRegpayfreq(),sv.regpayfreq)) {
				compute(wsaaPymt, 3).setRounded(add(wsaaPymt,regtbrkIO.getPymt()));
			}
			else {
				wsaaFreq2.set(sv.regpayfreq);
				wsaaFreq.set(regtbrkIO.getRegpayfreq());
				wsaaPymt2.set(regtbrkIO.getPymt());
				compute(wsaaPymt2, 3).setRounded((div((mult(wsaaPymt2,wsaaFreq)),wsaaFreq2)));
				compute(wsaaPymt, 3).setRounded(add(wsaaPymt,wsaaPymt2));
			}
		}
		regtbrkIO.setFunction(varcom.nextr);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I") || isEQ(sv.ddind, "X")) {
			return ;
		}
		
		updateRegt3100();
		unlkSoftLock3270();
		
	}

protected void updateRegt3100(){

	if(!regppf.getRgpymop().equalsIgnoreCase(sv.rgpymop.toString())){
		update3110();
	} else{
		return;
	}
}

protected void update3110(){

	if (isEQ(sv.ddind,"X")) {
		return ;
	}
	 regppfList = new ArrayList<>();
	 if(sv.rgpymop.toString().equalsIgnoreCase("B")){
		 readBankDetailsFromRegt();
		 if(isEQ(sv.payclt,SPACES) && isNE(regtvstIO.getPayclt(),SPACES)){
			 regppf.setPayclt(regtvstIO.getPayclt().toString());
		 }
	 }
	 regppf.setPayreason(sv.cltype.toString());
	 regppf.setChdrnum(sv.chdrnum.toString());
	 regppf.setRgpymop(sv.rgpymop.toString());
	 regppf.setDestkey(sv.destkey.toString());
     updateValidFlagRegppf();
     insertRegppf();
     if(oldpayMethod.equalsIgnoreCase("A")){
     insertRegPayDetails();
     }
     writePtrn3900();
     updateChdrRecord3020();
     if(oldpayMethod.equalsIgnoreCase("A")){
       postings360();
       if(isEQ(t6694rec.payeereq, "Y"))
    	   callPayreq380();
     }
}
protected void readBankDetailsFromRegt(){
	regtvstIO.setFunction("RETRV");
	SmartFileCode.execute(appVars, regtvstIO);
	if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(regtvstIO.getParams());
		fatalError600();
	}
	regppf.setBankkey(regtvstIO.getBankkey().toString());
	regppf.setBankacckey(regtvstIO.getBankacckey().toString());

}
protected void updateValidFlagRegppf(){
	regppf.setUser(varcom.vrcmUser.toInt());
	/*regppf.setChdrnum(sv.chdrnum.toString());
	regppf.setRgpymop(sv.rgpymop.toString());*/
	regppfList.add(regppf);
	regpDAO.updateValidflag(regppfList);
	
}
protected void insertRegppf(){
	
	  if(regppfList!= null && !regppfList.isEmpty()){
	    	for(Regppf Rgpdetpftmp : regppfList){
	    		Rgpdetpftmp.setTranno(chdrTranno);
	    		Rgpdetpftmp.setUser(varcom.vrcmUser.toInt());
	    		Rgpdetpftmp.setValidflag("1");
	    		
	    		if (isEQ(t6694rec.contreq,"Y")) {
	    			Rgpdetpftmp.setDestkey(sv.destkey.toString());
	    			Rgpdetpftmp.setGlact(t6694rec.glact.toString());
	    		}
	    		else {
	    			Rgpdetpftmp.setDestkey(SPACES.stringValue());
	    			Rgpdetpftmp.setGlact(t6694rec.glact.toString());
	    		} 
	    		Rgpdetpftmp.setSacscode(t6694rec.sacscode.toString());
	    		Rgpdetpftmp.setSacstype(t6694rec.sacstype.toString());
	    		Rgpdetpftmp.setDebcred(t6694rec.debcred.toString()); 
	    		Rgpdetpftmp.setRgpymop(sv.rgpymop.toString()); 
	    		Rgpdetpftmp.setChdrnum(sv.chdrnum.toString());
	    	}
	    	
	     }
	
	regpDAO.insertRegppfRecord(regppfList);
}
protected void updateChdrRecord3020(){
	chdrpf = new Chdrpf();
	chdrBulkUpdtList = new ArrayList<>();
	chdrpf.setUniqueNumber(chdrmjaIO.unique_number.toLong());
	chdrpf.setTranno(chdrTranno);
	chdrBulkUpdtList.add(chdrpf);		
	chdrpfDAO.updateChdrTrannoByUniqueNo(chdrBulkUpdtList);
}
protected void unlkSoftLock3270(){
	/*    Release the soft lock on the contract.*/
	sftlockrec.sftlockRec.set(SPACES);
	sftlockrec.company.set(wsspcomn.company);
	sftlockrec.entity.set(chdrmjaIO.getChdrnum());
	sftlockrec.enttyp.set("CH");
	/*  MOVE 'T703'                 TO SFTL-TRANSACTION.             */
	sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
	sftlockrec.statuz.set(SPACES);
	sftlockrec.function.set("UNLK");
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
	/*EXIT*/
}
protected void writePtrn3900() {

	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	if (isNE(datcon1rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon1rec.datcon1Rec);
		syserrrec.statuz.set(datcon1rec.statuz);
		fatalError600();
	}
	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrpfx("CH");
	ptrnpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
	ptrnpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
	ptrnpf.setTranno(chdrTranno);
	ptrnpf.setTrdt(varcom.vrcmDate.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setTermid(varcom.vrcmTermid.toString());
	ptrnpf.setUserT(varcom.vrcmUser.toInt());
	ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
	ptrnpf.setDatesub(datcon1rec.intDate.toInt());
	wsaaBatckey1.set(wsspcomn.batchkey);
	ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
	ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
	ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
	ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
	ptrnpf.setBatccoy(wsaaBatckey1.batcBatccoy.toString());
	ptrnpf.setBatcpfx(wsaaBatckey1.batcBatcpfx.toString());
	ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
	List<Ptrnpf> list = new ArrayList<Ptrnpf>();
	list.add(ptrnpf);
	if (!ptrnpfDAO.insertPtrnPF(list)) {
		syserrrec.params.set(chdrmjaIO.getChdrcoy().toString()
				.concat(chdrmjaIO.getChdrnum().toString()));
		fatalError600();
	}
}
public Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   } 
protected void whereNext4000()
	{
		try {

			if(isEQ(wsspcomn.sbmaction,"D")){
				nextProgram4000();	
			} else {
				nextProgram4030();
			}
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4000()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		if (isEQ(sv.ddind,"X")) {
			
			regtvstIO.setFunction(varcom.keeps);
			regtvstIO.setFormat(formatsInner.regtvstrec);
			SmartFileCode.execute(appVars, regtvstIO);
			if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(regtvstIO.getParams());
				fatalError600();
			}
			popUp4010();
			genssw4020();
			return;
		}
		else {
			if (isEQ(sv.ddind,"?")) {
				checkBankDetails4400();
			}
		}
	
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		} else {
			if(isEQ(wsspcomn.sbmaction , "D")){
					regtvstIO.setFunction(varcom.rlse);
					SmartFileCode.execute(appVars, regtvstIO);
					if (isNE(regtvstIO.getStatuz(), varcom.oK)) {
						 syserrrec.params.set(regtvstIO.getParams());
						 fatalError600();
					}
					wsspcomn.programPtr.add(1);						
				}
			
			}
		}

protected void popUp4010()
	{
		gensswrec.function.set(SPACES);
		if ((isEQ(sv.ddind,"X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
			sv.ddind.set("?");
			gensswrec.function.set("A");
		}
	}

protected void genssw4020()
	{
		if (isEQ(gensswrec.function,SPACES)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz,varcom.oK))
		&& (isNE(gensswrec.statuz,varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
		nextProgram4030();
	}

protected void nextProgram4030()
	{
	    wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void checkBankDetails4400()
	{
		bank4400();
	}

protected void bank4400()
	{
		regtvstIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(), varcom.oK)) {
			 syserrrec.params.set(regtvstIO.getParams());
			 fatalError600();
		}
		if ((isEQ(regtvstIO.getBankkey(), SPACES))
				&& (isEQ(regtvstIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		} else {
			sv.ddind.set("+");
			wsaaPayclt.set(sv.payclt);
		}
	}

protected void plainname(String clntnum) {
	/* PLAIN-100 */
	clntpf = clntpfDAO.findClientByClntnum(clntnum);
	StringBuilder stringVariable1 = new StringBuilder();
	stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
	stringVariable1.append(", ");
	stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
	sv.linsname.setLeft(stringVariable1.toString());

} 
protected void plainnamePayee(String clntnum) {
	/* PLAIN-100 */
	clntpf = clntpfDAO.findClientByClntnum(clntnum);
	StringBuilder stringVariable1 = new StringBuilder();
	stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
	stringVariable1.append(", ");
	stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
	sv.payenme.setLeft(stringVariable1.toString());

} 
protected String plainnameOwner(String clntnum) {
	/* PLAIN-100 */
	clntpf = clntpfDAO.findClientByClntnum(clntnum);
	StringBuilder stringVariable1 = new StringBuilder();
	stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
	stringVariable1.append(", ");
	stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
	return stringVariable1.toString();
} 
protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
	private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
	private FixedLengthStringData e570 = new FixedLengthStringData(4).init("E570");
	private FixedLengthStringData e925 = new FixedLengthStringData(4).init("E925");
	private FixedLengthStringData f351 = new FixedLengthStringData(4).init("F351");
	private FixedLengthStringData g126 = new FixedLengthStringData(4).init("G126");
	private FixedLengthStringData g311 = new FixedLengthStringData(4).init("G311");
	private FixedLengthStringData g514 = new FixedLengthStringData(4).init("G514");
	private FixedLengthStringData g515 = new FixedLengthStringData(4).init("G515");
	private FixedLengthStringData g516 = new FixedLengthStringData(4).init("G516");
	private FixedLengthStringData g517 = new FixedLengthStringData(4).init("G517");
	private FixedLengthStringData g521 = new FixedLengthStringData(4).init("G521");
	private FixedLengthStringData g522 = new FixedLengthStringData(4).init("G522");
	private FixedLengthStringData g529 = new FixedLengthStringData(4).init("G529");
	private FixedLengthStringData g534 = new FixedLengthStringData(4).init("G534");
	private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
	private FixedLengthStringData rrfp = new FixedLengthStringData(4).init("RRFP");
	
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData regtvstrec = new FixedLengthStringData(10).init("REGTVSTREC");
}

protected void postings360()
{
	
	lifacmvrec1.lifacmvRec.set(SPACES);
	wsaaRldgacct.set(SPACES);
	wsaaSequenceNo.set(ZERO);
	lifacmvrec1.function.set("PSTW");
	/* Read T1688 for transaction code description.                 */
	descpf=descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	if(descpf!=null)
	lifacmvrec1.trandesc.set(descpf.getLongdesc());
	wsaaBatckey.set(wsspcomn.batchkey);
	lifacmvrec1.batccoy.set(wsaaBatckey.batcBatccoy);
	lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
	lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
	lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
	lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
	lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
	lifacmvrec1.rdocnum.set(regppf.getChdrnum());
	compute(lifacmvrec1.tranno, 0).set(chdrTranno);
	lifacmvrec1.rldgcoy.set(regppf.getChdrcoy());
	lifacmvrec1.origcurr.set(regppf.getCurrcd());
	lifacmvrec1.tranref.set(regppf.getChdrnum());
	lifacmvrec1.crate.set(ZERO);
	lifacmvrec1.genlcoy.set(regppf.getChdrcoy());
	lifacmvrec1.genlcur.set(regppf.getCurrcd());
	lifacmvrec1.effdate.set(datcon1rec.intDate.toInt());
	lifacmvrec1.rcamt.set(ZERO);
	lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec1.transactionDate.set(ZERO);
	lifacmvrec1.transactionTime.set(ZERO);
	lifacmvrec1.user.set(varcom.vrcmUser);
	lifacmvrec1.termid.set(varcom.vrcmTermid);
	lifacmvrec1.substituteCode[1].set(chdrmjaIO.getCnttype());
	lifacmvrec1.substituteCode[6].set(regppf.getCrtable());
	/* For component level accounting, the whole entity field       */
	/* LIFA-RLDGACCT must be filled.  For contract level accounting */
	/* only the contract header no. is needed in the entity field   */
	/* LIFA-RLDGACCT.                                               */
	if (isEQ(t5688rec.comlvlacc, "Y")) {
		wsaaPlan.set(regppf.getPlanSuffix());
		wsaaRldgLife.set(regppf.getLife());
		wsaaRldgCoverage.set(regppf.getCoverage());
		wsaaRldgRider.set(regppf.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
	}
	lifacmvrec1.rldgacct.set(regppf.getChdrnum());
	lifacmvrec1.sacscode.set(t5645rec.sacscode[1]);
	lifacmvrec1.sacstyp.set(t5645rec.sacstype[1]);
	lifacmvrec1.glcode.set(t5645rec.glmap[1]);
	lifacmvrec1.glsign.set(t5645rec.sign[1]);
	lifacmvrec1.contot.set(t5645rec.cnttot[1]);
	
	lifacmvrec1.origamt.set(accAmtLPAP); // lp Ap only  
	lifacmvrec1.acctamt.set(accAmtLPAP);

	wsaaSequenceNo.add(1);
	lifacmvrec1.jrnseq.set(wsaaSequenceNo);
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
		return ; 
	}
	lifacmvrec1.sacscode.set(t5645rec.sacscode[2]);
	lifacmvrec1.sacstyp.set(t5645rec.sacstype[2]);
	lifacmvrec1.glcode.set(t5645rec.glmap[2]);
	lifacmvrec1.glsign.set(t5645rec.sign[2]);
	lifacmvrec1.contot.set(t5645rec.cnttot[2]);
	lifacmvrec1.origamt.set(accAmtLPAN);
	lifacmvrec1.acctamt.set(accAmtLPAN);
	wsaaSequenceNo.add(1);
	lifacmvrec1.jrnseq.set(wsaaSequenceNo);
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
	}
	if(isEQ(sv.rgpymop, "B")){
		lifacmvrec1.sacscode.set(t5645rec.sacscode[3]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[3]);
		lifacmvrec1.glcode.set(t5645rec.glmap[3]);
		lifacmvrec1.glsign.set(t5645rec.sign[3]);
		lifacmvrec1.contot.set(t5645rec.cnttot[3]);
	}else if (isEQ(sv.rgpymop, "C")) {
		lifacmvrec1.sacscode.set(t5645rec.sacscode[4]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[4]);
		lifacmvrec1.glcode.set(t5645rec.glmap[4]);
		lifacmvrec1.glsign.set(t5645rec.sign[4]);
		lifacmvrec1.contot.set(t5645rec.cnttot[4]);
	}else{
		lifacmvrec1.sacscode.set(t6694rec.sacscode.toString());
		lifacmvrec1.sacstyp.set(t6694rec.sacstype.toString());
		lifacmvrec1.glcode.set(t6694rec.glact);
		lifacmvrec1.glsign.set(t6694rec.debcred.toString());
	}
	
	lifacmvrec1.origamt.set(add(accAmtLPAP,accAmtLPAN));   
	lifacmvrec1.acctamt.set(add(accAmtLPAP,accAmtLPAN));
	wsaaSequenceNo.add(1);
	lifacmvrec1.jrnseq.set(wsaaSequenceNo);
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
		return ; 
	}
	
 }

protected void callPayreq380()
{
	payreqrec2.rec.set(SPACES);
	/* Read table T3629 in order to obtain the bankcode            *   */
	com.csc.smart400framework.dataaccess.model.Itempf itempf = new com.csc.smart400framework.dataaccess.model.Itempf();
	itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), t3629,regppf.getCurrcd());
	if(itempf != null) {
		t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	payreqrec2.bankcode.set(t3629rec.bankcode);
	payreqrec2.batckey.set(wsspcomn.batchkey);
	payreqrec2.frmRldgacct.set(SPACES);
	payreqrec2.frmRldgacct.set(regppf.getChdrnum());
	subcoderec.codeRec.set(SPACES);
	subcoderec.code[1].set(chdrmjaIO.getCnttype());
	if(isEQ(sv.rgpymop, "B")) {
		subcoderec.glcode.set(t5645rec.glmap[3]);
		subcode();
		payreqrec2.glcode.set(subcoderec.glcode);
		payreqrec2.sacscode.set(t5645rec.sacscode[3]);
		payreqrec2.sacstype.set(t5645rec.sacstype[3]);
		payreqrec2.sign.set(t5645rec.sign[3]);
		payreqrec2.cnttot.set(t5645rec.cnttot[3]);
	}
	if(isEQ(sv.rgpymop, "C")) {
		subcoderec.glcode.set(t5645rec.glmap[4]);
		subcode();
		payreqrec2.glcode.set(subcoderec.glcode);
		payreqrec2.sacscode.set(t5645rec.sacscode[4]);
		payreqrec2.sacstype.set(t5645rec.sacstype[4]);
		payreqrec2.sign.set(t5645rec.sign[4]);
		payreqrec2.cnttot.set(t5645rec.cnttot[4]);
	}
	payreqrec2.effdate.set(datcon1rec.intDate.toInt());
	payreqrec2.paycurr.set(regppf.getCurrcd());
	payreqrec2.pymt.set(accAmtLPAP);
	payreqrec2.termid.set(varcom.vrcmTermid.toString());
	payreqrec2.user.set(varcom.vrcmUser.toInt());
	payreqrec2.reqntype.set(t6694rec.reqntype);
	payreqrec2.clntcoy.set(regtvstIO.getPaycoy());
	payreqrec2.clntnum.set(regtvstIO.getPayclt());
	if (isNE(regtvstIO.getBankkey(), SPACES)) {
		payreqrec2.bankkey.set(regtvstIO.getBankkey());
		payreqrec2.bankacckey.set(regtvstIO.getBankacckey());
	}
	payreqrec2.zbatctrcde.set(wsaaBatckey.batcBatctrcde);			
	payreqrec2.tranref.set(regppf.getChdrnum());
	payreqrec2.language.set(wsspcomn.language.toString());
	payreqrec2.function.set("REQN");
	callProgram(Payreq.class, payreqrec2.rec);
	if (isNE(payreqrec2.statuz, varcom.oK)) {
		syserrrec.statuz.set(payreqrec2.statuz);
	}
}

protected void subcode()
{
	sbcd100Start();
}

protected void sbcd100Start()
{
	if (isNE(subcoderec.code[1], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[1]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[2], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[2]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[3], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[3]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[4], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[4]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[5], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[5]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[6], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[6]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
	}
}

public void insertRegPayDetails(){
	
    List<Rgpdetpf> rgpdetpfList;
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(regppf.getChdrnum());
    rgpdetpfList = rgpdetpfDAO.searchRpdetpfRecord(chdrnumList, regppf.getChdrcoy());
    
    if(rgpdetpfList!= null && !rgpdetpfList.isEmpty()){
    	for(Rgpdetpf Rgpdetpftmp : rgpdetpfList){
    		Rgpdetpftmp.setApcaplamt(new BigDecimal(0));
    		Rgpdetpftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
    		Rgpdetpftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
    		Rgpdetpftmp.setValidflag("2");
    		Rgpdetpftmp.setApintamt(BigDecimal.ZERO);
    	}
     }
    rgpdetpfDAO.updateRegpRecCapDate(rgpdetpfList);
}
}
