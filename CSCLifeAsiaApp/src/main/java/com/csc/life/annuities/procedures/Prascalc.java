/*
 * File: Prascalc.java
 * Date: 30 August 2009 1:57:34
 * Author: Quipoz Limited
 *
 * Class transformed from PRASCALC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.clients.dataaccess.SoinTableDAM;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6697rec;
import com.csc.life.annuities.tablestructures.T6698rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*            PRAS CALCULATION SUBROUTINE
*          *******************************
*
*  This pras tax relief calculation subroutine will calculate
*  the amount of tax relief applicable. The routine will check
*  to see if an individuals salary may be used to fund pensions
*  net of tax (SOINPF) and if the contract may bill net of
*  tax (T6697). If both are true, the rate of tax relief at
*  the passed effective date will be looked up (T6698) and
*  applied to the gross amount passed. The Inland Revenue's
*  client number will also be passed back.
*
*
*  (1). If the source of income sequence number passed is zero
*       return zero tax relief and blank Inland Revenue number.
*
*  (2). Otherwise, look up source of income for client passed
*
*  (3). Look up pension details for contract type passed.
*
*  (4). If the source of income and pension both allow
*       net premiums
*
*         - Calculate the tax relief as the gross premium
*           passed * tax relief % from T6697 for the tax relief
*           method passed.
*
*         - Return the Inland Revenue client number.
*
*
*****************************************************************
* </pre>
*/
public class Prascalc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PRASCALC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private String t6697 = "T6697";
	private String t6698 = "T6698";
		/* FORMATS */
	private String soinrec = "SOINREC";
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Prasrec prasrec = new Prasrec();
		/*Clients - Source of Income Details Logic*/
	private SoinTableDAM soinIO = new SoinTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6697rec t6697rec = new T6697rec();
	private T6698rec t6698rec = new T6698rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit590,
		exit1090
	}

	public Prascalc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		prasrec.prascalcRec = convertAndSetParam(prasrec.prascalcRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		para100();
		exit190();
	}

protected void para100()
	{
		syserrrec.subrname.set(wsaaProg);
		varcom.vrcmTime.set(getCobolTime());
		soinIO.setStatuz("O-K");
		/*MAIN*/
		mainProcessing1000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void databaseError500()
	{
		try {
			para500();
		}
		catch (GOTOException e){
		}
		finally{
			exit590();
		}
	}

protected void para500()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			prasrec.statuz.set(syserrrec.statuz);
			goTo(GotoLabel.exit590);
		}
		prasrec.statuz.set(syserrrec.statuz);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrType);
	}

protected void exit590()
	{
		exit190();
	}

protected void mainProcessing1000()
	{
		try {
			para1010();
		}
		catch (GOTOException e){
		}
	}

protected void para1010()
	{
		if (isEQ(prasrec.incomeSeqNo,0)) {
			prasrec.taxrelamt.set(ZERO);
			prasrec.inrevnum.set(SPACES);
			goTo(GotoLabel.exit1090);
		}
		soinIO.setClntcoy(prasrec.clntcoy);
		soinIO.setClntnum(prasrec.clntnum);
		soinIO.setIncomeSeqNo(prasrec.incomeSeqNo);
		soinIO.setFunction(varcom.readr);
		soinIO.setFormat(soinrec);
		SmartFileCode.execute(appVars, soinIO);
		if (isNE(soinIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(soinIO.getStatuz());
			syserrrec.params.set(soinIO.getParams());
			databaseError500();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(prasrec.company);
		itemIO.setItemtabl(t6697);
		itemIO.setItemitem(prasrec.cnttype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError500();
		}
		t6697rec.t6697Rec.set(itemIO.getGenarea());
		if (isEQ(soinIO.getPrasInd(),"N")
		&& isEQ(t6697rec.empcon,"N")) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(prasrec.company);
			itdmIO.setItemtabl(t6698);
			itdmIO.setItmfrm(prasrec.effdate);
			itdmIO.setItemitem(prasrec.taxrelmth);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(itdmIO.getStatuz());
				syserrrec.params.set(itdmIO.getParams());
				databaseError500();
			}
			else {
				t6698rec.t6698Rec.set(itdmIO.getGenarea());
			}
			compute(prasrec.taxrelamt, 2).set(div(mult(prasrec.grossprem,t6698rec.taxrelpc),100));
			prasrec.inrevnum.set(t6698rec.inrevnum);
		}
		else {
			prasrec.taxrelamt.set(ZERO);
			prasrec.inrevnum.set(SPACES);
		}
	}
}
