package com.csc.life.annuities.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: VstdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:56
 * Class transformed from VSTDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class VstdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 183;
	public FixedLengthStringData vstdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData vstdpfRecord = vstdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(vstdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(vstdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(vstdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(vstdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(vstdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(vstdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(vstdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(vstdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(vstdrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(vstdrec);
	public PackedDecimalData vstlump = DD.vstlump.copy().isAPartOf(vstdrec);
	public FixedLengthStringData fieldType01 = DD.type.copy().isAPartOf(vstdrec);
	public FixedLengthStringData fieldType02 = DD.type.copy().isAPartOf(vstdrec);
	public FixedLengthStringData fieldType03 = DD.type.copy().isAPartOf(vstdrec);
	public FixedLengthStringData fieldType04 = DD.type.copy().isAPartOf(vstdrec);
	public FixedLengthStringData fieldType05 = DD.type.copy().isAPartOf(vstdrec);
	public FixedLengthStringData fieldType06 = DD.type.copy().isAPartOf(vstdrec);
	public PackedDecimalData vstpay = DD.vstpay.copy().isAPartOf(vstdrec);
	public PackedDecimalData vstperc = DD.vstperc.copy().isAPartOf(vstdrec);
	public PackedDecimalData vestamt01 = DD.vstamt.copy().isAPartOf(vstdrec);
	public PackedDecimalData vestamt02 = DD.vstamt.copy().isAPartOf(vstdrec);
	public PackedDecimalData vestamt03 = DD.vstamt.copy().isAPartOf(vstdrec);
	public PackedDecimalData vestamt04 = DD.vstamt.copy().isAPartOf(vstdrec);
	public PackedDecimalData vestamt05 = DD.vstamt.copy().isAPartOf(vstdrec);
	public PackedDecimalData vestamt06 = DD.vstamt.copy().isAPartOf(vstdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(vstdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(vstdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(vstdrec);
	public FixedLengthStringData newcovr = DD.newcovr.copy().isAPartOf(vstdrec);
	public PackedDecimalData elvstfct = DD.elvstfct.copy().isAPartOf(vstdrec);
	public PackedDecimalData vstpaya = DD.vstpaya.copy().isAPartOf(vstdrec);
	public PackedDecimalData origpay = DD.origpay.copy().isAPartOf(vstdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public VstdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for VstdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public VstdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for VstdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public VstdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for VstdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public VstdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("VSTDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"JLIFE, " +
							"EFFDATE, " +
							"CRTABLE, " +
							"VSTLUMP, " +
							"TYPE01, " +
							"TYPE02, " +
							"TYPE03, " +
							"TYPE04, " +
							"TYPE05, " +
							"TYPE06, " +
							"VSTPAY, " +
							"VSTPERC, " +
							"VSTAMT01, " +
							"VSTAMT02, " +
							"VSTAMT03, " +
							"VSTAMT04, " +
							"VSTAMT05, " +
							"VSTAMT06, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"NEWCOVR, " +
							"ELVSTFCT, " +
							"VSTPAYA, " +
							"ORIGPAY, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     jlife,
                                     effdate,
                                     crtable,
                                     vstlump,
                                     fieldType01,
                                     fieldType02,
                                     fieldType03,
                                     fieldType04,
                                     fieldType05,
                                     fieldType06,
                                     vstpay,
                                     vstperc,
                                     vestamt01,
                                     vestamt02,
                                     vestamt03,
                                     vestamt04,
                                     vestamt05,
                                     vestamt06,
                                     userProfile,
                                     jobName,
                                     datime,
                                     newcovr,
                                     elvstfct,
                                     vstpaya,
                                     origpay,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		jlife.clear();
  		effdate.clear();
  		crtable.clear();
  		vstlump.clear();
  		fieldType01.clear();
  		fieldType02.clear();
  		fieldType03.clear();
  		fieldType04.clear();
  		fieldType05.clear();
  		fieldType06.clear();
  		vstpay.clear();
  		vstperc.clear();
  		vestamt01.clear();
  		vestamt02.clear();
  		vestamt03.clear();
  		vestamt04.clear();
  		vestamt05.clear();
  		vestamt06.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		newcovr.clear();
  		elvstfct.clear();
  		vstpaya.clear();
  		origpay.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getVstdrec() {
  		return vstdrec;
	}

	public FixedLengthStringData getVstdpfRecord() {
  		return vstdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setVstdrec(what);
	}

	public void setVstdrec(Object what) {
  		this.vstdrec.set(what);
	}

	public void setVstdpfRecord(Object what) {
  		this.vstdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(vstdrec.getLength());
		result.set(vstdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}