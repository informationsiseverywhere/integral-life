package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:17
 * Description:
 * Copybook name: ANNYVF1KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Annyvf1key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData annyvf1FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData annyvf1Key = new FixedLengthStringData(64).isAPartOf(annyvf1FileKey, 0, REDEFINE);
  	public FixedLengthStringData annyvf1Chdrcoy = new FixedLengthStringData(1).isAPartOf(annyvf1Key, 0);
  	public FixedLengthStringData annyvf1Chdrnum = new FixedLengthStringData(8).isAPartOf(annyvf1Key, 1);
  	public FixedLengthStringData annyvf1Life = new FixedLengthStringData(2).isAPartOf(annyvf1Key, 9);
  	public FixedLengthStringData annyvf1Coverage = new FixedLengthStringData(2).isAPartOf(annyvf1Key, 11);
  	public FixedLengthStringData annyvf1Rider = new FixedLengthStringData(2).isAPartOf(annyvf1Key, 13);
  	public PackedDecimalData annyvf1PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(annyvf1Key, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(annyvf1Key, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(annyvf1FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		annyvf1FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}