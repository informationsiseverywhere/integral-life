package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:11
 * Description:
 * Copybook name: VSTDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vstdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vstdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData vstdKey = new FixedLengthStringData(64).isAPartOf(vstdFileKey, 0, REDEFINE);
  	public FixedLengthStringData vstdChdrcoy = new FixedLengthStringData(1).isAPartOf(vstdKey, 0);
  	public FixedLengthStringData vstdChdrnum = new FixedLengthStringData(8).isAPartOf(vstdKey, 1);
  	public FixedLengthStringData vstdLife = new FixedLengthStringData(2).isAPartOf(vstdKey, 9);
  	public FixedLengthStringData vstdCoverage = new FixedLengthStringData(2).isAPartOf(vstdKey, 11);
  	public FixedLengthStringData vstdRider = new FixedLengthStringData(2).isAPartOf(vstdKey, 13);
  	public PackedDecimalData vstdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(vstdKey, 15);
  	public PackedDecimalData vstdTranno = new PackedDecimalData(5, 0).isAPartOf(vstdKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(vstdKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vstdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vstdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}