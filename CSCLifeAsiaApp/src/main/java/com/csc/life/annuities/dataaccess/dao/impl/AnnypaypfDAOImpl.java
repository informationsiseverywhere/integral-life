package com.csc.life.annuities.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.annuities.dataaccess.dao.AnnypaypfDAO;
import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AnnypaypfDAOImpl extends BaseDAOImpl<Annypaypf> implements AnnypaypfDAO{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AnnypaypfDAOImpl.class);
	
	
	private static final String ANNAMT = "ANNAMT";
	private static final String ANNINTAMT = "ANNINTAMT";
	private static final String INTTAX = "INTTAX";
	private static final String NEXTPAYDT = "NEXTPAYDT";
	private static final String PAIDAMT = "PAIDAMT";
	private static final String PMNTNO = "PMNTNO";
	private static final String PMNTSTAT = "PMNTSTAT";
	private static final String UNIQUE_NUMBER = "UNIQUE_NUMBER";
	private static final String PMNTDT = "PMNTDT";
	private static final String WITHOLDTAX = "WITHOLDTAX";
	private static final String EFFECTIVEDT = "EFFECTIVEDT";
	private static final String VALIDFLAG = "VALIDFLAG";
	private static final String TRANNO = "TRANNO";
	private static final String DFRRDTRM = "DFRRDTRM";
	private static final String BATCTRCDE = "BATCTRCDE";
	private static final String ANNUPMNTTRM = "ANNUPMNTTRM";
	private static final String ANNPMNTOPT = "ANNPMNTOPT";
	private static final String ANNPMNTFREQ = "ANNPMNTFREQ";
	


	@Override
	public Annypaypf getAnnypaypf(String chdrcoy, String chdrnum) {		
		 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,PMNTNO,PMNTDT,NEXTPAYDT,ANNAMT,ANNINTAMT,  "
                 + " PAIDAMT,PMNTSTAT,INTTAX,WITHOLDTAX,UNIQUE_NUMBER"
                 + " FROM ANNYPAYPF WHERE CHDRCOY = ? AND CHDRNUM = ? ORDER BY PMNTNO ASC ");

         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
       
 		ResultSet rs = null;
 		Annypaypf annypaypf = null;

 		try { 	 			
 			ps.setString(1, chdrcoy.trim()); 			
 			ps.setString(2, chdrnum);
 			
 			rs = ps.executeQuery();

 			while (rs.next()) {
 				annypaypf = new Annypaypf(); 				
 				annypaypf.setChdrcoy(rs.getString(1));
 				annypaypf.setChdrnum(rs.getString(2));
 				annypaypf.setPmntno(rs.getInt(PMNTNO));
 				annypaypf.setPmntdt(rs.getInt(PMNTDT));
 				annypaypf.setNextpaydt(rs.getInt(NEXTPAYDT));
 				annypaypf.setAnnaamt(rs.getBigDecimal(ANNAMT));
 				annypaypf.setAnnintamt(rs.getBigDecimal(ANNINTAMT));
 				annypaypf.setPaidamt(rs.getBigDecimal(PAIDAMT));
 				annypaypf.setInttax(rs.getBigDecimal(INTTAX));
 				annypaypf.setWitholdtax(rs.getBigDecimal(WITHOLDTAX));
 				
 				annypaypf.setPmntstat(rs.getString(PMNTSTAT));
 				annypaypf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER)); 
			}
		} catch (SQLException e) {
			LOGGER.error("getAnnypaypf():", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return annypaypf;
	
	}
	
	
	
	@Override
	public List<Annypaypf> getRecordbyStat(String chdrcoy, String chdrnum, String status) {		

		 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,ANNPMNTOPT,PMNTNO,ANNUPMNTTRM,DFRRDTRM,ANNPMNTFREQ,PMNTDT,NEXTPAYDT,ANNAMT,ANNINTAMT, "
                 + " PAIDAMT,PMNTSTAT,INTTAX,WITHOLDTAX,UNIQUE_NUMBER FROM ANNYPAYPF "
                 + " WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTSTAT=? AND VALIDFLAG='1' ORDER BY PMNTNO ASC ");
   
        List<Annypaypf> list = new ArrayList<Annypaypf>();
		PreparedStatement stmt = getPrepareStatement(sqlCovrSelect1.toString());
		ResultSet rs = null;
		try {
			stmt.setString(1, chdrcoy.trim()); 			
			stmt.setString(2, chdrnum);
			stmt.setString(3, status);
			rs = stmt.executeQuery();

			Annypaypf annypaypf = null;
			 
			while (rs.next()) {
				annypaypf = new Annypaypf();
				
				annypaypf.setChdrcoy(rs.getString(1));
 				annypaypf.setChdrnum(rs.getString(2));
 				annypaypf.setAnnupmnttrm(rs.getInt(ANNUPMNTTRM));
 				annypaypf.setDfrrdtrm(rs.getInt(DFRRDTRM));
 				annypaypf.setAnnpmntfreq(rs.getString(ANNPMNTFREQ));
 				annypaypf.setAnnpmntopt(rs.getString(ANNPMNTOPT));
 				annypaypf.setPmntno(rs.getInt(PMNTNO));
 				annypaypf.setPmntdt(rs.getInt(PMNTDT));
 				annypaypf.setNextpaydt(rs.getInt(NEXTPAYDT));
 				annypaypf.setAnnaamt(rs.getBigDecimal(ANNAMT));
 				annypaypf.setAnnintamt(rs.getBigDecimal(ANNINTAMT));
 				annypaypf.setPaidamt(rs.getBigDecimal(PAIDAMT));
 				annypaypf.setInttax(rs.getBigDecimal(INTTAX));
 				annypaypf.setWitholdtax(rs.getBigDecimal(WITHOLDTAX));
 				annypaypf.setPmntstat(rs.getString(PMNTSTAT));
 				annypaypf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER)); 
				
				list.add(annypaypf);
			}
		} catch (SQLException e) {
			LOGGER.error("getRecordbyStat(): ", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
			
		}

		return list;
	
	}
	
	public void updateDate(Annypaypf annypaypf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYPAYPF SET PMNTDT=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO= ? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setInt(1, annypaypf.getPmntdt());			    
			    ps.setString(2, annypaypf.getChdrcoy());
			    ps.setString(3, annypaypf.getChdrnum());
			    ps.setInt(4, annypaypf.getPmntno());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateDate()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
}
		
		public void updateValidflg(Annypaypf annypaypf){		
			StringBuilder sb = new StringBuilder("");
			sb.append("UPDATE ANNYPAYPF SET VALIDFLAG='2' ");
			sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO= ? AND VALIDFLAG='1' AND UNIQUE_NUMBER=? ");
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());
				
				    ps.setString(1, annypaypf.getChdrcoy());
				    ps.setString(2, annypaypf.getChdrnum());
				    ps.setInt(3, annypaypf.getPmntno());
				    ps.setLong(4, annypaypf.getUniqueNumber());
				    
				    ps.executeUpdate();				
					
				
			}catch (SQLException e) {
				LOGGER.error("updateValidflg()",e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}	
		
		
	}
	
	public void updateRecord(Annypaypf annypaypf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYPAYPF SET PMNTSTAT=?,ANNAMT = ?,ANNINTAMT=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO= ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setString(1, annypaypf.getPmntstat());	
				ps.setBigDecimal(2, annypaypf.getAnnaamt());
				ps.setBigDecimal(3, annypaypf.getAnnintamt());
			    ps.setString(4, annypaypf.getChdrcoy());
			    ps.setString(5, annypaypf.getChdrnum());
			    ps.setInt(6, annypaypf.getPmntno());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		
	}
	//ILJ-286 starts
	public void updatewthldngtax(Annypaypf annypaypf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYPAYPF SET WITHOLDTAX=?");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO= ? AND VALIDFLAG='1' ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setBigDecimal(1, annypaypf.getWitholdtax());	
				ps.setString(2, annypaypf.getChdrcoy());
				ps.setString(3, annypaypf.getChdrnum());
				ps.setInt(4, annypaypf.getPmntno());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updatewthldngtax()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		
	}
	//ILJ-286 ends
	@Override
	public void insertAnnypaypf(Annypaypf pf) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO ANNYPAYPF (CHDRNUM,CHDRCOY,ANNPMNTOPT,ANNUPMNTTRM,DFRRDTRM,ANNPMNTFREQ,"
				+ " PMNTNO,PMNTDT,NEXTPAYDT,ANNAMT,INTTAX,ANNINTAMT,WITHOLDTAX,PAIDAMT,PMNTSTAT, VALIDFLAG, EFFECTIVEDT,BATCTRCDE,TRANNO,USRPRF,DATIME) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		try {
			int i=1;
			ps.setString(i++, pf.getChdrnum());
			ps.setString(i++, pf.getChdrcoy());			
			ps.setString(i++, pf.getAnnpmntopt());
			ps.setInt(i++, pf.getAnnupmnttrm());
			ps.setInt(i++, pf.getDfrrdtrm());
			ps.setString(i++, pf.getAnnpmntfreq());			
			ps.setInt(i++, pf.getPmntno());
			ps.setInt(i++, pf.getPmntdt());
			ps.setInt(i++, pf.getNextpaydt());
			ps.setBigDecimal(i++, pf.getAnnaamt());
			ps.setBigDecimal(i++, pf.getInttax());
			ps.setBigDecimal(i++, pf.getAnnintamt());
			ps.setBigDecimal(i++, pf.getWitholdtax());
			ps.setBigDecimal(i++, pf.getPaidamt());			
			ps.setString(i++, pf.getPmntstat());	
			ps.setString(i++, pf.getValidflag());
			ps.setInt(i++, pf.getEffectivedt());
			ps.setString(i++, pf.getBatctrcde());
			ps.setInt(i++, pf.getTranno());
			ps.setString(i++, this.getUsrprf());			
            ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));          
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertAnnypaypf()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		
	}
	
	@Override
	public List<Annypaypf> getAnnypaypfRecords(String chdrcoy, String chdrnum) {		
		 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,ANNPMNTOPT,ANNUPMNTTRM,DFRRDTRM,ANNPMNTFREQ, "
                 + " PMNTNO,PMNTDT,VALIDFLAG, EFFECTIVEDT,NEXTPAYDT,ANNAMT,ANNINTAMT,  "
                 + " PAIDAMT,PMNTSTAT,INTTAX,WITHOLDTAX,UNIQUE_NUMBER,TRANNO, BATCTRCDE "
                 + " FROM ANNYPAYPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG='1' ");
         sqlCovrSelect1
                 .append(" ORDER BY PMNTNO ASC");
         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
       
 		ResultSet rs = null;
 		Annypaypf annypaypf = null;
 		List<Annypaypf> list = new ArrayList<Annypaypf>();

 		try { 	 			
 			ps.setString(1, chdrcoy.trim()); 			
 			ps.setString(2, chdrnum); 						
 			rs = ps.executeQuery();

 			while (rs.next()) {
 				annypaypf = new Annypaypf(); 				
 				annypaypf.setChdrcoy(rs.getString(1));
 				annypaypf.setChdrnum(rs.getString(2));
 				annypaypf.setAnnpmntopt(rs.getString(ANNPMNTOPT));	 				
 				annypaypf.setAnnupmnttrm(rs.getInt(ANNUPMNTTRM));
 				annypaypf.setDfrrdtrm(rs.getInt(DFRRDTRM));
 				annypaypf.setAnnpmntfreq(rs.getString(ANNPMNTFREQ));
 				annypaypf.setPmntno(rs.getInt(PMNTNO));
 				annypaypf.setPmntdt(rs.getInt(PMNTDT));
 				annypaypf.setValidflag(rs.getString(VALIDFLAG));
 				annypaypf.setEffectivedt(rs.getInt(EFFECTIVEDT));
 				annypaypf.setNextpaydt(rs.getInt(NEXTPAYDT));
 				annypaypf.setAnnaamt(rs.getBigDecimal(ANNAMT));
 				annypaypf.setAnnintamt(rs.getBigDecimal(ANNINTAMT));
 				annypaypf.setPaidamt(rs.getBigDecimal(PAIDAMT));
 				annypaypf.setInttax(rs.getBigDecimal(INTTAX));
 				annypaypf.setWitholdtax(rs.getBigDecimal(WITHOLDTAX)); 				
 				annypaypf.setPmntstat(rs.getString(PMNTSTAT));
 				annypaypf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER));
 				annypaypf.setTranno(rs.getInt(TRANNO));
 				annypaypf.setBatctrcde(rs.getString(BATCTRCDE));
 				list.add(annypaypf);
			}
		} catch (SQLException e) {
			LOGGER.error("getAnnypaypfRecords()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return list;
	
	}
	
	
	
	@Override
	public List<Annypaypf> getAnnypaypfbyEffdt(String chdrcoy,String chdrnum, int effectivedt, int tranno, String batctrcde) {		
		 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,ANNPMNTOPT,ANNUPMNTTRM,DFRRDTRM,ANNPMNTFREQ, "
                 + " PMNTNO,PMNTDT,VALIDFLAG, EFFECTIVEDT, TRANNO, BATCTRCDE,NEXTPAYDT,ANNAMT,ANNINTAMT,  "
                 + " PAIDAMT,PMNTSTAT,INTTAX,WITHOLDTAX,UNIQUE_NUMBER "
                 + " FROM ANNYPAYPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND EFFECTIVEDT= ? AND TRANNO=? AND BATCTRCDE=? ");
         sqlCovrSelect1
                 .append(" ORDER BY PMNTNO ASC");
         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
       
 		ResultSet rs = null;
 		Annypaypf annypaypf = null;
 		List<Annypaypf> list = new ArrayList<Annypaypf>();

 		try { 	 			
 			ps.setString(1, chdrcoy.trim()); 			
 			ps.setString(2, chdrnum); 	
 			ps.setInt(3, effectivedt);
 			ps.setInt(4, tranno);
 			ps.setString(5, batctrcde);
 			
 			rs = ps.executeQuery();

 			while (rs.next()) {
 				annypaypf = new Annypaypf(); 				
 				annypaypf.setChdrcoy(rs.getString(1));
 				annypaypf.setChdrnum(rs.getString(2));
 				annypaypf.setAnnpmntopt(rs.getString(ANNPMNTOPT));	 				
 				annypaypf.setAnnupmnttrm(rs.getInt(ANNUPMNTTRM));
 				annypaypf.setDfrrdtrm(rs.getInt(DFRRDTRM));
 				annypaypf.setAnnpmntfreq(rs.getString(ANNPMNTFREQ));
 				annypaypf.setPmntno(rs.getInt(PMNTNO));
 				annypaypf.setPmntdt(rs.getInt(PMNTDT));
 				annypaypf.setValidflag(rs.getString(VALIDFLAG));
 				annypaypf.setEffectivedt(rs.getInt(EFFECTIVEDT));
 				annypaypf.setTranno(rs.getInt(TRANNO));
 				annypaypf.setBatctrcde(rs.getString(BATCTRCDE));
 				annypaypf.setNextpaydt(rs.getInt(NEXTPAYDT));
 				annypaypf.setAnnaamt(rs.getBigDecimal(ANNAMT));
 				annypaypf.setAnnintamt(rs.getBigDecimal(ANNINTAMT));
 				annypaypf.setPaidamt(rs.getBigDecimal(PAIDAMT));
 				annypaypf.setInttax(rs.getBigDecimal(INTTAX));
 				annypaypf.setWitholdtax(rs.getBigDecimal(WITHOLDTAX)); 				
 				annypaypf.setPmntstat(rs.getString(PMNTSTAT));
 				annypaypf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER));  				
 				list.add(annypaypf);
			}
		} catch (SQLException e) {
			LOGGER.error("getAnnypaypfbyEffdt()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return list;
	
	}
	

//ILJ-286 start
	@Override
	public Annypaypf getAnnypaypfRecord(String chdrcoy, String chdrnum, int pmntno) {
		
		StringBuilder sqlCovrSelect1 = new StringBuilder(
                "SELECT CHDRCOY,CHDRNUM,ANNPMNTOPT,ANNUPMNTTRM,DFRRDTRM,ANNPMNTFREQ,PMNTNO,PMNTDT,ANNAMT,ANNINTAMT,  "
                + " PAIDAMT,PMNTSTAT,INTTAX,WITHOLDTAX,UNIQUE_NUMBER "
                + " FROM ANNYPAYPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO = ? AND VALIDFLAG='1'");
    
        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
      
		ResultSet rs = null;
		Annypaypf annypaypf = null;

		try { 	 			
			ps.setString(1, chdrcoy.trim()); 			
			ps.setString(2, chdrnum); 	
			ps.setInt(3, pmntno); 	
			rs = ps.executeQuery();

			while (rs.next()) {
				annypaypf = new Annypaypf(); 				
				annypaypf.setChdrcoy(rs.getString(1));
				annypaypf.setChdrnum(rs.getString(2));
				annypaypf.setAnnpmntopt(rs.getString(ANNPMNTOPT));	 				
				annypaypf.setAnnupmnttrm(rs.getInt(ANNUPMNTTRM));
				annypaypf.setDfrrdtrm(rs.getInt(DFRRDTRM));
				annypaypf.setAnnpmntfreq(rs.getString(ANNPMNTFREQ));
				annypaypf.setPmntno(rs.getInt(PMNTNO));
				annypaypf.setPmntdt(rs.getInt(PMNTDT));
				annypaypf.setAnnaamt(rs.getBigDecimal(ANNAMT));
				annypaypf.setAnnintamt(rs.getBigDecimal(ANNINTAMT));
				annypaypf.setPaidamt(rs.getBigDecimal(PAIDAMT));
				annypaypf.setInttax(rs.getBigDecimal(INTTAX));
				annypaypf.setWitholdtax(rs.getBigDecimal(WITHOLDTAX)); 				
				annypaypf.setPmntstat(rs.getString(PMNTSTAT));
				annypaypf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER));  
			}
		} catch (SQLException e) {
			LOGGER.error("getAnnypaypfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return annypaypf;
	}
	//ILJ-286 ends
	
	
	
	
	
	//ILJ-286 start
		@Override
		public Annypaypf getAnnypaypfRecords(String chdrcoy, String chdrnum, int pmntno , int effectivedt, int tranno, String batctrcde) {
			
			StringBuilder sqlCovrSelect1 = new StringBuilder(
	                "SELECT CHDRCOY,CHDRNUM,ANNPMNTOPT,ANNUPMNTTRM,DFRRDTRM,ANNPMNTFREQ,PMNTNO,PMNTDT,ANNAMT,ANNINTAMT,  "
	                + " PAIDAMT,PMNTSTAT,INTTAX,WITHOLDTAX,UNIQUE_NUMBER, EFFECTIVEDT, TRANNO, BATCTRCDE "
	                + " FROM ANNYPAYPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO = ? AND EFFECTIVEDT= ? AND TRANNO=? AND BATCTRCDE=?");
	    
	        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
	      
			ResultSet rs = null;
			Annypaypf annypaypf = null;

			try { 	 			
				ps.setString(1, chdrcoy.trim()); 			
				ps.setString(2, chdrnum); 	
				ps.setInt(3, pmntno); 	
				ps.setInt(4, effectivedt);
	 			ps.setInt(5, tranno);
	 			ps.setString(6, batctrcde);
				rs = ps.executeQuery();

				while (rs.next()) {
					annypaypf = new Annypaypf(); 				
					annypaypf.setChdrcoy(rs.getString(1));
					annypaypf.setChdrnum(rs.getString(2));
					annypaypf.setAnnpmntopt(rs.getString(ANNPMNTOPT));	 				
					annypaypf.setAnnupmnttrm(rs.getInt(ANNUPMNTTRM));
					annypaypf.setDfrrdtrm(rs.getInt(DFRRDTRM));
					annypaypf.setAnnpmntfreq(rs.getString(ANNPMNTFREQ));
					annypaypf.setPmntno(rs.getInt(PMNTNO));
					annypaypf.setPmntdt(rs.getInt(PMNTDT));
					annypaypf.setAnnaamt(rs.getBigDecimal(ANNAMT));
					annypaypf.setAnnintamt(rs.getBigDecimal(ANNINTAMT));
					annypaypf.setPaidamt(rs.getBigDecimal(PAIDAMT));
					annypaypf.setInttax(rs.getBigDecimal(INTTAX));
					annypaypf.setWitholdtax(rs.getBigDecimal(WITHOLDTAX)); 				
					annypaypf.setPmntstat(rs.getString(PMNTSTAT));
					annypaypf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER)); 
	 				annypaypf.setEffectivedt(rs.getInt(EFFECTIVEDT));
	 				annypaypf.setTranno(rs.getInt(TRANNO));
	 				annypaypf.setBatctrcde(rs.getString(BATCTRCDE));
				}
			} catch (SQLException e) {
				LOGGER.error("getAnnypaypfRecords()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return annypaypf;
		}
	
	@Override
	public void updateAnnypaypfByAnnpmntopt(Annypaypf pf) {
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYPAYPF SET PMNTNO=?,PMNTDT=?,NEXTPAYDT=?,ANNAMT=?,INTTAX=?,ANNINTAMT=?,"
				+ " WITHOLDTAX=?,PAIDAMT=?,PMNTSTAT=?,ANNUPMNTTRM=?,DFRRDTRM=?,ANNPMNTFREQ=?,USRPRF=?,DATIME=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND ANNPMNTOPT=? ");		
		PreparedStatement ps = getPrepareStatement(sb.toString());				
		try {
			int i=1;
			ps.setInt(i++, pf.getPmntno());
			ps.setInt(i++, pf.getPmntdt());
			ps.setInt(i++, pf.getNextpaydt());
			ps.setBigDecimal(i++, pf.getAnnaamt());
			ps.setBigDecimal(i++, pf.getInttax());
			ps.setBigDecimal(i++, pf.getAnnintamt());
			ps.setBigDecimal(i++, pf.getWitholdtax());
			ps.setBigDecimal(i++, pf.getPaidamt());
			ps.setString(i++, pf.getPmntstat());
			ps.setInt(i++, pf.getAnnupmnttrm());
			ps.setInt(i++, pf.getDfrrdtrm());
			ps.setString(i++, pf.getAnnpmntfreq());							
			ps.setString(i++, this.getUsrprf());			
            ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));   			
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateAnnypaypf()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	@Override
	public void deleteAnnypaypfRecord(String chdrcoy, String chdrnum, int tranno) {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM ANNYPAYPF ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND TRANNO=?");		
		PreparedStatement ps = getPrepareStatement(sb.toString());		
		try {
			ps.setString(1, chdrcoy.trim()); 			
 			ps.setString(2, chdrnum); 	
 			ps.setInt(3, tranno);
 			ps.executeUpdate();			
		} catch (SQLException e) {
			LOGGER.error("deleteAnnypaypfRecord()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	public void updateAnnypayrecord(Annypaypf annypaypf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYPAYPF SET PMNTSTAT=?,PAIDAMT=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO=?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setString(1, annypaypf.getPmntstat());
				ps.setBigDecimal(2, annypaypf.getPaidamt());
			    ps.setString(3, annypaypf.getChdrcoy());
			    ps.setString(4, annypaypf.getChdrnum());
			    ps.setInt(5, annypaypf.getPmntno());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateStat()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
			
	}
	
	
	
	public void updateFlag(Annypaypf annypaypf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYPAYPF SET VALIDFLAG='2' ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND PMNTNO=? AND VALIDFLAG='1' ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

			    ps.setString(1, annypaypf.getChdrcoy());
			    ps.setString(2, annypaypf.getChdrnum());
			    ps.setInt(3, annypaypf.getPmntno());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateFlag()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
			
	}
	
	@Override
	public void updateValidflag(String chdrcoy, String chdrnum){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ANNYPAYPF SET VALIDFLAG='2'");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM=? AND VALIDFLAG= ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setString(1, chdrcoy);
				ps.setString(2, chdrnum);
			    ps.setString(3, "1");	
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateValidflag:",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
}
	
}
