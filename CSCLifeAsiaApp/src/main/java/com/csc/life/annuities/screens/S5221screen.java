package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5221screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5221ScreenVars sv = (S5221ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5221screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5221ScreenVars screenVars = (S5221ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.currdesc.setClassString("");
		screenVars.optind.setClassString("");
		screenVars.vstlump.setClassString("");
		screenVars.comtperc.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.optdsc.setClassString("");
		screenVars.vstpay.setClassString("");
		screenVars.origpay.setClassString("");
		screenVars.vstpaya.setClassString("");
		screenVars.elvstfct.setClassString("");
	}

/**
 * Clear all the variables in S5221screen
 */
	public static void clear(VarModel pv) {
		S5221ScreenVars screenVars = (S5221ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.pstate.clear();
		screenVars.rstate.clear();
		screenVars.planSuffix.clear();
		screenVars.currcd.clear();
		screenVars.currdesc.clear();
		screenVars.optind.clear();
		screenVars.vstlump.clear();
		screenVars.comtperc.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.optdsc.clear();
		screenVars.vstpay.clear();
		screenVars.origpay.clear();
		screenVars.vstpaya.clear();
		screenVars.elvstfct.clear();
	}
}
