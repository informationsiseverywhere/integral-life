package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6623screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6623ScreenVars sv = (S6623ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6623screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6623ScreenVars screenVars = (S6623ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.freqann01.setClassString("");
		screenVars.freqann02.setClassString("");
		screenVars.freqann03.setClassString("");
		screenVars.freqann04.setClassString("");
		screenVars.freqann05.setClassString("");
		screenVars.freqann06.setClassString("");
		screenVars.freqann07.setClassString("");
		screenVars.freqann08.setClassString("");
		screenVars.freqann09.setClassString("");
		screenVars.freqann10.setClassString("");
		screenVars.advance01.setClassString("");
		screenVars.advance02.setClassString("");
		screenVars.advance03.setClassString("");
		screenVars.advance04.setClassString("");
		screenVars.advance05.setClassString("");
		screenVars.advance06.setClassString("");
		screenVars.advance07.setClassString("");
		screenVars.advance08.setClassString("");
		screenVars.advance09.setClassString("");
		screenVars.advance10.setClassString("");
		screenVars.arrears01.setClassString("");
		screenVars.arrears02.setClassString("");
		screenVars.arrears03.setClassString("");
		screenVars.arrears04.setClassString("");
		screenVars.arrears05.setClassString("");
		screenVars.arrears06.setClassString("");
		screenVars.arrears07.setClassString("");
		screenVars.arrears08.setClassString("");
		screenVars.arrears09.setClassString("");
		screenVars.arrears10.setClassString("");
		screenVars.guarperd01.setClassString("");
		screenVars.guarperd02.setClassString("");
		screenVars.guarperd03.setClassString("");
		screenVars.guarperd04.setClassString("");
		screenVars.guarperd05.setClassString("");
		screenVars.guarperd06.setClassString("");
		screenVars.guarperd07.setClassString("");
		screenVars.guarperd08.setClassString("");
		screenVars.guarperd09.setClassString("");
		screenVars.guarperd10.setClassString("");
		screenVars.comtfact01.setClassString("");
		screenVars.comtfact02.setClassString("");
		screenVars.comtfact03.setClassString("");
		screenVars.comtfact04.setClassString("");
		screenVars.comtfact05.setClassString("");
		screenVars.comtfact06.setClassString("");
		screenVars.comtfact07.setClassString("");
		screenVars.comtfact08.setClassString("");
		screenVars.comtfact09.setClassString("");
		screenVars.comtfact10.setClassString("");
	}

/**
 * Clear all the variables in S6623screen
 */
	public static void clear(VarModel pv) {
		S6623ScreenVars screenVars = (S6623ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.freqann01.clear();
		screenVars.freqann02.clear();
		screenVars.freqann03.clear();
		screenVars.freqann04.clear();
		screenVars.freqann05.clear();
		screenVars.freqann06.clear();
		screenVars.freqann07.clear();
		screenVars.freqann08.clear();
		screenVars.freqann09.clear();
		screenVars.freqann10.clear();
		screenVars.advance01.clear();
		screenVars.advance02.clear();
		screenVars.advance03.clear();
		screenVars.advance04.clear();
		screenVars.advance05.clear();
		screenVars.advance06.clear();
		screenVars.advance07.clear();
		screenVars.advance08.clear();
		screenVars.advance09.clear();
		screenVars.advance10.clear();
		screenVars.arrears01.clear();
		screenVars.arrears02.clear();
		screenVars.arrears03.clear();
		screenVars.arrears04.clear();
		screenVars.arrears05.clear();
		screenVars.arrears06.clear();
		screenVars.arrears07.clear();
		screenVars.arrears08.clear();
		screenVars.arrears09.clear();
		screenVars.arrears10.clear();
		screenVars.guarperd01.clear();
		screenVars.guarperd02.clear();
		screenVars.guarperd03.clear();
		screenVars.guarperd04.clear();
		screenVars.guarperd05.clear();
		screenVars.guarperd06.clear();
		screenVars.guarperd07.clear();
		screenVars.guarperd08.clear();
		screenVars.guarperd09.clear();
		screenVars.guarperd10.clear();
		screenVars.comtfact01.clear();
		screenVars.comtfact02.clear();
		screenVars.comtfact03.clear();
		screenVars.comtfact04.clear();
		screenVars.comtfact05.clear();
		screenVars.comtfact06.clear();
		screenVars.comtfact07.clear();
		screenVars.comtfact08.clear();
		screenVars.comtfact09.clear();
		screenVars.comtfact10.clear();
	}
}
