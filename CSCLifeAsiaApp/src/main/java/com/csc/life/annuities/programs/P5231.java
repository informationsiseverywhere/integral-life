/*
 * File: P5231.java
 * Date: 30 August 2009 0:21:01
 * Author: Quipoz Limited
 * 
 * Class transformed from P5231.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.recordstructures.P5231par;
import com.csc.life.terminationclaims.screens.S5231ScreenVars;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
* OVERVIEW: Parameter prompt program
* ==================================
*
* This is a parameter promt screen which selects and validates
* data used by B5231 a batch program which forms part of the 9405
* Annuities Development.  It  will  be  run  as  part  of  batch
* schedule  PENDVEST,  which  can  be  run as and when required.
* This will probably form part of the regular  batch  processing
* but is unlikely to be run on a daily basis.
*
* It  will select any component with a risk cessation date which
* falls between  the  dates  entered  on  the  parameter  prompt
* screen,  S5231.    The  only restriction on the dates input on
* this screen is that the 'To date' is greater  than  the  'From
* date'.
*
*
*****************************************************************
* </pre>
*/
public class P5231 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5231");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String f571 = "F571";
	private String g730 = "G730";
	private String g639 = "G639";
	private String g640 = "G640";
	private String f665 = "F665";
		/* FORMATS */
	private String bscdrec = "BSCDREC";
	private String bsscrec = "BSSCREC";
	private String buparec = "BUPAREC";
	private String bppdrec = "BPPDREC";
		/*Parameter Prompt Definition - Multi-thre*/
	private BppdTableDAM bppdIO = new BppdTableDAM();
		/*Schedule Definition - Multi Thread Batch*/
	private BscdTableDAM bscdIO = new BscdTableDAM();
		/*Schedule File - Multi-Thread Batch*/
	private BsscTableDAM bsscIO = new BsscTableDAM();
		/*User Parameter Area - Multi-Thread Batch*/
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P5231par p5231par = new P5231par();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5231ScreenVars sv = ScreenProgram.getScreenVars( S5231ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit
	}

	public P5231() {
		super();
		screenVars = sv;
		new ScreenModel("S5231", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
		sv.dateto.set(varcom.maxdate);
		sv.datefrm.set(ZERO);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		sv.errorIndicators.set(SPACES);
		if (isEQ(sv.datefrm,99999999)) {
			sv.datefrmErr.set(f665);
		}
		if (isEQ(sv.dateto,varcom.maxdate)) {
			sv.dateto.set(31129999);
			wsspcomn.edterror.set("Y");
		}
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sv.dateto);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			sv.datetoErr.set(g639);
		}
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sv.datefrm);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			sv.datefrmErr.set(g640);
		}
		if (isEQ(sv.errorIndicators,SPACES)) {
			if (isGT(sv.datefrm,sv.dateto)) {
				sv.datefrmErr.set(g730);
			}
		}
		if (isNE(sv.chdrnum,SPACES)
		&& isNE(sv.chdrnum1,SPACES)) {
			if (isGT(sv.chdrnum,sv.chdrnum1)) {
				sv.chdrnumfrmErr.set(f571);
				sv.chdrnumtoErr.set(f571);
			}
		}
		else {
			if (isEQ(sv.chdrnum,SPACES)
			&& isNE(sv.chdrnum1,SPACES)) {
				sv.chdrnumfrmErr.set(f571);
				sv.chdrnumtoErr.set(f571);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

protected void moveParameters3100()
	{
		/*MOVE-TO-PARM-RECORD*/
		p5231par.chdrnum.set(sv.chdrnum);
		p5231par.chdrnum1.set(sv.chdrnum1);
		p5231par.datefrm.set(sv.datefrm);
		p5231par.dateto.set(sv.dateto);
		/*EXIT*/
	}

protected void writeParamRecord3200()
	{
		writBupa3210();
	}

protected void writBupa3210()
	{
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(p5231par.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
