/*
 * File: Revvest.java
 * Date: 30 August 2009 2:11:52
 * Author: Quipoz Limited
 * 
 * Class transformed from REVVEST.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.annuities.dataaccess.VstdTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.dataaccess.LiferevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.statistics.dataaccess.CovrstaTableDAM;
import com.csc.life.statistics.dataaccess.CovrstsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*              VESTING REVERSAL SUBROUTINE.
*
*OVERVIEW.
*
*  This subroutine will be called  from  the  Reversal Routines
*  Table,  T6661,  when  reversing  the registration of Vesting
*  on a deferred Annuities Contract.
*
*  It  will reverse the updates to the following files, changed
*  during the registration of vesting:
*
*
*CONTRACT HEADER (CHDR)
*
*       Delete the valid flag '1' record created during Vesting
*       Registration and reinstate the valid flag '2' record.
*
*PAYER (PAYR)
*
*       Delete the valid flag '1' record created during Vesting
*       Registration and reinstate the valid flag '2' record.
*
*LIFE (LIFE)
*
*       Delete the valid flag '1'  record  with  a match on the
*       Tranno, possibly  created during Vesting.  ie.  if  the
*       whole contract was vested the LIFE  is altered. If only
*       a  policy  within  the  plan  is vested the LIFE record
*       remains unchanged. If a Validflag '1' record is deleted
*       reinstate the valid flag '2' record.
*
*COVERAGE (COVR)
*
*       Delete the valid flag '1' record created during Vesting
*       Registration and reinstate the valid flag '2' record.
*       Also,  delete  the  new coverage created during Vesting
*       Registration.
*
*VESTING DETAIL (VSTD)
*
*       Delete all  the  valid  flag  '1' record created during
*       Vesting Registration with  a match on Tranno.
*
*STATISTICS AND ACCOUNTING (STTR & ACMV)
*
*       These will be reversed by the current processing within
*       REVGENAT. i.e. no need to include it in REVVEST.
*
*GENERIC SUBROUTINES
*
*       Table T5671  will be  read and any reversal subroutines
*       found will also be called.
*       As  both  ANNY  and REGP records are generic, they will
*       both  be  deleted  via  generic  subroutines entered on
*       T5671.
*
*       These Subroutines are:
*                             GDELANNY - Delete ANNY.
*                             GDELREGT - Delete REGT.
*
*****************************************************************
* </pre>
*/
public class Revvest extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVVEST";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
		/* WSAA-LIFE-KEY */
	private FixedLengthStringData wsaaChdrcoyLife = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnumLife = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLifeLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaJlifeLife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaBatchkey = new FixedLengthStringData(65);
	private FixedLengthStringData wsaaContkey = new FixedLengthStringData(19).isAPartOf(wsaaBatchkey, 0);
	private FixedLengthStringData wsaaBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 0);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaContkey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaContkey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaContkey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaContkey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaContkey, 14);
	private FixedLengthStringData wsaaOldBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatchkey, 19);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrstaTableDAM covrstaIO = new CovrstaTableDAM();
	private CovrstsTableDAM covrstsIO = new CovrstsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private LiferevTableDAM liferevIO = new LiferevTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private VstdTableDAM vstdIO = new VstdTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Revvest() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processChdrs3000();
		processPayrs4000();
		processCovrs5000();
		processVstds6000();
		while ( !(isEQ(liferevIO.getStatuz(), varcom.endp))) {
			processLife7000();
		}
		
		processAcmvs8000();
		processAcmvOptical8010();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		/* Get today's date.*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void processChdrs3000()
	{
		/*START*/
		/*  READ Contract Header CHDR*/
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		/* MOVE BEGNH                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "1")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/* MOVE DELET                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "2")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void processPayrs4000()
	{
		start4000();
	}

protected void start4000()
	{
		/*  BEGNH Contract Header PAYR*/
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		/* MOVE BEGNH                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), "1")
		|| isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			fatalError600();
		}
		/* MOVE DELET                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			fatalError600();
		}
		payrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)
		&& isNE(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), "2")
		|| isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			fatalError600();
		}
		payrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			fatalError600();
		}
	}

protected void processCovrs5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* Use COVR logical view to loop around coverage/rider records*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setLife(ZERO);
		covrIO.setCoverage(ZERO);
		covrIO.setRider(ZERO);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			reverseCovrs5100();
		}
		
	}

protected void reverseCovrs5100()
	{
		start5100();
	}

protected void start5100()
	{
		/* This section checks the TRANNO of the COVR just read, then*/
		/* calls the Delete COVR Section, which Deletes the COVRs with*/
		/* a Validflag of '1', and will then call The Generic*/
		/* Processing Section, which will come into effect when the*/
		/* earlier COVRS are reinstated to Validflag '1'.*/
		if (isEQ(reverserec.tranno, covrIO.getTranno())) {
			deleteAndReinstate5200();
			genericProcessing5300();
		}
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
	}

protected void deleteAndReinstate5200()
	{
		start5200();
	}

protected void start5200()
	{
		/* This Section calls the COVRSTS Record and compares it to*/
		/* the initial COVR values. If these are found to be unequal,*/
		/* a status of ENDP is moved to the COVRSTS-STATUZ. If the*/
		/* TRANNO on the COVRSTS is found to be >= the REVE-TRANNO*/
		/* the record is deleted, otherwise the VALIDFLAG '2' record*/
		/* is replaced by the VALIDFLAG '1'.*/
		covrstsIO.setParams(SPACES);
		covrstsIO.setChdrcoy(covrIO.getChdrcoy());
		covrstsIO.setChdrnum(covrIO.getChdrnum());
		covrstsIO.setLife(covrIO.getLife());
		covrstsIO.setCoverage(covrIO.getCoverage());
		covrstsIO.setRider(covrIO.getRider());
		covrstsIO.setPlanSuffix(covrIO.getPlanSuffix());
		covrstsIO.setTranno(reverserec.tranno);
		covrstsIO.setFunction(varcom.readh);
		covrstsIO.setFormat(formatsInner.covrstsrec);
		SmartFileCode.execute(appVars, covrstsIO);
		if (isNE(covrstsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrstsIO.getParams());
			syserrrec.statuz.set(covrstsIO.getStatuz());
			fatalError600();
		}
		/* Check for a increase record created by the forward           */
		/* transaction and delete it if one exists.                     */
		checkIncr5500();
		covrstsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrstsIO);
		if (isNE(covrstsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrstsIO.getParams());
			syserrrec.statuz.set(covrstsIO.getStatuz());
			fatalError600();
		}
		reinstateCovr5400();
	}

protected void genericProcessing5300()
	{
		start5310();
	}

protected void start5310()
	{
		/* Read T5671 to see if any Generic processing is*/
		/*  required for the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.life.set(covrIO.getLife());
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.crtable.set(covrIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		wsaaBatchkey.set(reverserec.batchkey);
		wsaaOldBatctrcde.set(reverserec.oldBatctrcde);
		greversrec.batckey.set(wsaaBatchkey);
		greversrec.effdate.set(payrlifIO.getPtdate());
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs5330();
		}
	}

protected void callTrevsubs5330()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void reinstateCovr5400()
	{
		start5410();
	}

protected void start5410()
	{
		covrstaIO.setParams(SPACES);
		covrstaIO.setChdrcoy(covrIO.getChdrcoy());
		covrstaIO.setChdrnum(covrIO.getChdrnum());
		covrstaIO.setLife(covrIO.getLife());
		covrstaIO.setCoverage(covrIO.getCoverage());
		covrstaIO.setRider(covrIO.getRider());
		covrstaIO.setPlanSuffix(covrIO.getPlanSuffix());
		covrstaIO.setTranno(reverserec.tranno);
		/* MOVE BEGNH                  TO COVRSTA-FUNCTION.             */
		covrstaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrstaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrstaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		covrstaIO.setFormat(formatsInner.covrstarec);
		SmartFileCode.execute(appVars, covrstaIO);
		if (isNE(covrstaIO.getStatuz(), varcom.oK)
		&& isNE(covrstaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrstaIO.getParams());
			syserrrec.statuz.set(covrstaIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrstaIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(covrstaIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(covrstaIO.getLife(), covrIO.getLife())
		|| isNE(covrstaIO.getCoverage(), covrIO.getCoverage())
		|| isNE(covrstaIO.getRider(), covrIO.getRider())
		|| isNE(covrstaIO.getPlanSuffix(), covrIO.getPlanSuffix())) {
			covrstaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrstaIO.getStatuz(), varcom.oK)) {
			covrstaIO.setValidflag("1");
			covrstaIO.setCurrto(99999999);
			/*     MOVE REWRT              TO COVRSTA-FUNCTION              */
			covrstaIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, covrstaIO);
			if (isNE(covrstaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrstaIO.getParams());
				syserrrec.statuz.set(covrstaIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void checkIncr5500()
	{
		readh5510();
	}

protected void readh5510()
	{
		/* Look for a historic INCR record for the forward transaction. */
		/* If one exists for this component, delete it, thereby         */
		/* reinstating the previous record.                             */
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrstsIO.getChdrcoy());
		incrselIO.setChdrnum(covrstsIO.getChdrnum());
		incrselIO.setLife(covrstsIO.getLife());
		incrselIO.setCoverage(covrstsIO.getCoverage());
		incrselIO.setRider(covrstsIO.getRider());
		incrselIO.setPlanSuffix(covrstsIO.getPlanSuffix());
		incrselIO.setTranno(reverserec.tranno);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(formatsInner.incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)
		&& isNE(incrselIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			fatalError600();
		}
		if (isEQ(incrselIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			fatalError600();
		}
	}

protected void processVstds6000()
	{
		start6000();
	}

protected void start6000()
	{
		/* Use VSTD logical view to loop around coverage/rider records*/
		vstdIO.setParams(SPACES);
		vstdIO.setChdrcoy(reverserec.company);
		vstdIO.setChdrnum(reverserec.chdrnum);
		vstdIO.setLife(ZERO);
		vstdIO.setCoverage(ZERO);
		vstdIO.setRider(ZERO);
		vstdIO.setPlanSuffix(ZERO);
		vstdIO.setTranno(ZERO);
		vstdIO.setFormat(formatsInner.vstdrec);
		/* MOVE BEGNH                  TO VSTD-FUNCTION.                */
		vstdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		vstdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vstdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(), varcom.oK)
		&& isNE(vstdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vstdIO.getParams());
			fatalError600();
		}
		if (isNE(vstdIO.getChdrcoy(), reverserec.company)
		|| isNE(vstdIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(vstdIO.getStatuz(), varcom.endp)) {
			vstdIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(vstdIO.getStatuz(), varcom.endp))) {
			deleteVstds6100();
		}
		
	}

protected void deleteVstds6100()
	{
		start6100();
		nextVstd6180();
	}

protected void start6100()
	{
		/* This section checks the TRANNO of the VSTD just read, it*/
		/* will be deleted if the VSTD Tranno matches.*/
		/* REWRT is not necessary,as BEGNH is replaced by BEGN,record   */
		/* is not held,so read next record.                             */
		if (isEQ(reverserec.tranno, vstdIO.getTranno())) {
			/*    MOVE DELET               TO VSTD-FUNCTION                 */
			vstdIO.setFunction(varcom.deltd);
		}
		else {
			/*    MOVE REWRT               TO VSTD-FUNCTION                 */
			return ;
		}
		vstdIO.setFormat(formatsInner.vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(vstdIO.getParams());
			fatalError600();
		}
	}

protected void nextVstd6180()
	{
		vstdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(), varcom.oK)
		&& isNE(vstdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vstdIO.getParams());
			fatalError600();
		}
		if (isNE(vstdIO.getChdrcoy(), reverserec.company)
		|| isNE(vstdIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(vstdIO.getStatuz(), varcom.endp)) {
			vstdIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processLife7000()
	{
		lives7010();
	}

protected void lives7010()
	{
		liferevIO.setParams(SPACES);
		liferevIO.setChdrnum(reverserec.chdrnum);
		liferevIO.setChdrcoy(reverserec.company);
		liferevIO.setTranno(reverserec.tranno);
		liferevIO.setFormat(formatsInner.liferevrec);
		/* MOVE BEGNH                  TO LIFEREV-FUNCTION.             */
		liferevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		liferevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, liferevIO);
		if ((isNE(liferevIO.getStatuz(), varcom.oK))
		&& (isNE(liferevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(liferevIO.getParams());
			fatalError600();
		}
		if ((isNE(liferevIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(liferevIO.getChdrcoy(), reverserec.company))
		|| (isNE(liferevIO.getValidflag(), 1))
		|| (isEQ(liferevIO.getStatuz(), varcom.endp))
		|| (isNE(liferevIO.getTranno(), reverserec.tranno))) {
			liferevIO.setStatuz(varcom.endp);
			return ;
		}
		/* Store the key for the BEGN on the LIFE logical view*/
		wsaaChdrnumLife.set(liferevIO.getChdrnum());
		wsaaChdrcoyLife.set(liferevIO.getChdrcoy());
		wsaaLifeLife.set(liferevIO.getLife());
		wsaaJlifeLife.set(liferevIO.getJlife());
		lifeDelAndUpdate7200();
	}

protected void lifeDelAndUpdate7200()
	{
		deleteAndUpdate7210();
	}

protected void deleteAndUpdate7210()
	{
		/* Delete the valid flag 1 record*/
		/* MOVE DELET                  TO LIFEREV-FUNCTION.             */
		liferevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, liferevIO);
		if (isNE(liferevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(liferevIO.getParams());
			fatalError600();
		}
		/* Perform a BEGNH on the LIFE logical view using the key stored*/
		/* to find the valid flag two record*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(wsaaChdrcoyLife);
		lifeIO.setChdrnum(wsaaChdrnumLife);
		lifeIO.setLife(wsaaLifeLife);
		lifeIO.setJlife(wsaaJlifeLife);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		/* MOVE BEGNH                  TO LIFE-FUNCTION.                */
		lifeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		lifeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE"); 
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(), varcom.oK))
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if ((isNE(lifeIO.getChdrnum(), wsaaChdrnumLife))
		|| (isNE(lifeIO.getChdrcoy(), wsaaChdrcoyLife))
		|| (isNE(lifeIO.getLife(), wsaaLifeLife))
		|| (isNE(lifeIO.getJlife(), wsaaJlifeLife))
		|| (isNE(lifeIO.getValidflag(), "2"))
		|| (isEQ(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		/* Update the valid flag 2 record*/
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		/* MOVE REWRT                  TO LIFE-FUNCTION.                */
		lifeIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
	}

protected void processAcmvs8000()
	{
		start8000();
	}

protected void start8000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
	}

protected void processAcmvOptical8010()
	{
		start8010();
	}

protected void start8010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			fatalError600();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				fatalError600();
			}
		}
	}

protected void reverseAcmvs8100()
	{
		start8100();
		readNextAcmv8180();
	}

protected void start8100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/* we are trying to reverse.*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

protected void readNextAcmv8180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError601();
				case errorProg610: 
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		reverserec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC   ");
	private FixedLengthStringData covrstarec = new FixedLengthStringData(10).init("COVRSTAREC");
	private FixedLengthStringData covrstsrec = new FixedLengthStringData(10).init("COVRSTSREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData vstdrec = new FixedLengthStringData(10).init("VSTDREC");
	private FixedLengthStringData liferevrec = new FixedLengthStringData(10).init("LIFEREVREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData incrselrec = new FixedLengthStringData(10).init("INCRSELREC");
}
}
