package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 22 Aug 2012 15:34:00
 * Description:
 * Copybook name: Vpxvestrec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxvestrec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
  	public FixedLengthStringData vpxvestRec = new FixedLengthStringData(974);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxvestRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxvestRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxvestRec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxvestRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(738).isAPartOf(vpxvestRec, 136);

  	
  	public PackedDecimalData covrCount = new PackedDecimalData(3, 0).isAPartOf(dataRec, 0).init("ZERO");
  	public PackedDecimalData acblCount = new PackedDecimalData(3, 0).isAPartOf(dataRec, 2).init("ZERO");
  	public FixedLengthStringData bonusFunction = new FixedLengthStringData(4).isAPartOf(dataRec, 4);
  	
	public FixedLengthStringData bonusCalcMeth01 = new FixedLengthStringData(4).isAPartOf(dataRec, 8);
	public FixedLengthStringData bonusCalcMeth02 = new FixedLengthStringData(4).isAPartOf(dataRec, 12);
	public FixedLengthStringData bonusCalcMeth03 = new FixedLengthStringData(4).isAPartOf(dataRec, 16);
	public FixedLengthStringData bonusCalcMeth04 = new FixedLengthStringData(4).isAPartOf(dataRec, 20);
	public FixedLengthStringData bonusCalcMeth05 = new FixedLengthStringData(4).isAPartOf(dataRec, 24);
	public FixedLengthStringData bonusCalcMeth06 = new FixedLengthStringData(4).isAPartOf(dataRec, 28);
	public FixedLengthStringData bonusCalcMeth07 = new FixedLengthStringData(4).isAPartOf(dataRec, 32);
	public FixedLengthStringData bonusCalcMeth08 = new FixedLengthStringData(4).isAPartOf(dataRec, 36);
	public FixedLengthStringData bonusCalcMeth09 = new FixedLengthStringData(4).isAPartOf(dataRec, 40);
	public FixedLengthStringData bonusCalcMeth10 = new FixedLengthStringData(4).isAPartOf(dataRec, 44);
	
	public PackedDecimalData bonusEffDate01 = new PackedDecimalData(8,0).isAPartOf(dataRec, 48).init("ZERO");
	public PackedDecimalData bonusEffDate02 = new PackedDecimalData(8,0).isAPartOf(dataRec, 53).init("ZERO");
	public PackedDecimalData bonusEffDate03 = new PackedDecimalData(8,0).isAPartOf(dataRec, 58).init("ZERO");
	public PackedDecimalData bonusEffDate04 = new PackedDecimalData(8,0).isAPartOf(dataRec, 63).init("ZERO");
	public PackedDecimalData bonusEffDate05 = new PackedDecimalData(8,0).isAPartOf(dataRec, 68).init("ZERO");
	public PackedDecimalData bonusEffDate06 = new PackedDecimalData(8,0).isAPartOf(dataRec, 73).init("ZERO");
	public PackedDecimalData bonusEffDate07 = new PackedDecimalData(8,0).isAPartOf(dataRec, 78).init("ZERO");
	public PackedDecimalData bonusEffDate08 = new PackedDecimalData(8,0).isAPartOf(dataRec, 83).init("ZERO");
	public PackedDecimalData bonusEffDate09 = new PackedDecimalData(8,0).isAPartOf(dataRec, 88).init("ZERO");
	public PackedDecimalData bonusEffDate10 = new PackedDecimalData(8,0).isAPartOf(dataRec, 93).init("ZERO");

	public FixedLengthStringData bonusPremStatus01 = new FixedLengthStringData(4).isAPartOf(dataRec, 98);
	public FixedLengthStringData bonusPremStatus02 = new FixedLengthStringData(4).isAPartOf(dataRec, 102);
	public FixedLengthStringData bonusPremStatus03 = new FixedLengthStringData(4).isAPartOf(dataRec, 106);
	public FixedLengthStringData bonusPremStatus04 = new FixedLengthStringData(4).isAPartOf(dataRec, 110);
	public FixedLengthStringData bonusPremStatus05 = new FixedLengthStringData(4).isAPartOf(dataRec, 114);
	public FixedLengthStringData bonusPremStatus06 = new FixedLengthStringData(4).isAPartOf(dataRec, 118);
	public FixedLengthStringData bonusPremStatus07 = new FixedLengthStringData(4).isAPartOf(dataRec, 122);
	public FixedLengthStringData bonusPremStatus08 = new FixedLengthStringData(4).isAPartOf(dataRec, 126);
	public FixedLengthStringData bonusPremStatus09 = new FixedLengthStringData(4).isAPartOf(dataRec, 130);
	public FixedLengthStringData bonusPremStatus10 = new FixedLengthStringData(4).isAPartOf(dataRec, 134);
	
	public PackedDecimalData bonusUnitStmtDate01 = new PackedDecimalData(8,0).isAPartOf(dataRec, 138).init("ZERO");
	public PackedDecimalData bonusUnitStmtDate02 = new PackedDecimalData(8,0).isAPartOf(dataRec, 143).init("ZERO");
	public PackedDecimalData bonusUnitStmtDate03 = new PackedDecimalData(8,0).isAPartOf(dataRec, 148).init("ZERO");
	public PackedDecimalData bonusUnitStmtDate04 = new PackedDecimalData(8,0).isAPartOf(dataRec, 153).init("ZERO");;
	public PackedDecimalData bonusUnitStmtDate05 = new PackedDecimalData(8,0).isAPartOf(dataRec, 158).init("ZERO");;
	public PackedDecimalData bonusUnitStmtDate06 = new PackedDecimalData(8,0).isAPartOf(dataRec, 163).init("ZERO");
	public PackedDecimalData bonusUnitStmtDate07 = new PackedDecimalData(8,0).isAPartOf(dataRec, 168).init("ZERO");
	public PackedDecimalData bonusUnitStmtDate08 = new PackedDecimalData(8,0).isAPartOf(dataRec, 173).init("ZERO");
	public PackedDecimalData bonusUnitStmtDate09 = new PackedDecimalData(8,0).isAPartOf(dataRec, 178).init("ZERO");
	public PackedDecimalData bonusUnitStmtDate10 = new PackedDecimalData(8,0).isAPartOf(dataRec, 183).init("ZERO");
	
	public PackedDecimalData bonusSumin01 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 188).init("ZERO");
	public PackedDecimalData bonusSumin02 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 197).init("ZERO");
	public PackedDecimalData bonusSumin03 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 306).init("ZERO");
	public PackedDecimalData bonusSumin04 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 315).init("ZERO");
	public PackedDecimalData bonusSumin05 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 324).init("ZERO");
	public PackedDecimalData bonusSumin06 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 333).init("ZERO");
	public PackedDecimalData bonusSumin07 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 342).init("ZERO");
	public PackedDecimalData bonusSumin08 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 351).init("ZERO");
	public PackedDecimalData bonusSumin09 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 360).init("ZERO");
	public PackedDecimalData bonusSumin10 = new PackedDecimalData(17, 2).isAPartOf(dataRec, 369).init("ZERO");

  	public PackedDecimalData sacscurbal01 = new PackedDecimalData(17,2).isAPartOf(dataRec, 378).init("ZERO");
  	public PackedDecimalData sacscurbal02 = new PackedDecimalData(17,2).isAPartOf(dataRec, 387).init("ZERO");
  	public PackedDecimalData sacscurbal03 = new PackedDecimalData(17,2).isAPartOf(dataRec, 396).init("ZERO");
  	public PackedDecimalData sacscurbal04 = new PackedDecimalData(17,2).isAPartOf(dataRec, 405).init("ZERO");
  	public PackedDecimalData sacscurbal05 = new PackedDecimalData(17,2).isAPartOf(dataRec, 414).init("ZERO");
  	public PackedDecimalData sacscurbal06 = new PackedDecimalData(17,2).isAPartOf(dataRec, 423).init("ZERO");
  	public PackedDecimalData sacscurbal07 = new PackedDecimalData(17,2).isAPartOf(dataRec, 432).init("ZERO");
  	public PackedDecimalData sacscurbal08 = new PackedDecimalData(17,2).isAPartOf(dataRec, 441).init("ZERO");
  	public PackedDecimalData sacscurbal09 = new PackedDecimalData(17,2).isAPartOf(dataRec, 450).init("ZERO");
  	public PackedDecimalData sacscurbal10 = new PackedDecimalData(17,2).isAPartOf(dataRec, 459).init("ZERO");
	
	public PackedDecimalData extrasacscurbal01 = new PackedDecimalData(17,2).isAPartOf(dataRec, 468).init("ZERO");
  	public PackedDecimalData extrasacscurbal02 = new PackedDecimalData(17,2).isAPartOf(dataRec, 477).init("ZERO");
  	public PackedDecimalData extrasacscurbal03 = new PackedDecimalData(17,2).isAPartOf(dataRec, 486).init("ZERO");
  	public PackedDecimalData extrasacscurbal04 = new PackedDecimalData(17,2).isAPartOf(dataRec, 495).init("ZERO");
  	public PackedDecimalData extrasacscurbal05 = new PackedDecimalData(17,2).isAPartOf(dataRec, 504).init("ZERO");
  	public PackedDecimalData extrasacscurbal06 = new PackedDecimalData(17,2).isAPartOf(dataRec, 513).init("ZERO");
  	public PackedDecimalData extrasacscurbal07 = new PackedDecimalData(17,2).isAPartOf(dataRec, 522).init("ZERO");
  	public PackedDecimalData extrasacscurbal08 = new PackedDecimalData(17,2).isAPartOf(dataRec, 531).init("ZERO");
  	public PackedDecimalData extrasacscurbal09 = new PackedDecimalData(17,2).isAPartOf(dataRec, 540).init("ZERO");
  	public PackedDecimalData extrasacscurbal10 = new PackedDecimalData(17,2).isAPartOf(dataRec, 549).init("ZERO");
  	
  	public PackedDecimalData interimsacscurbal01 = new PackedDecimalData(17,2).isAPartOf(dataRec, 558).init("ZERO");
  	public PackedDecimalData interimsacscurbal02 = new PackedDecimalData(17,2).isAPartOf(dataRec, 567).init("ZERO");
  	public PackedDecimalData interimsacscurbal03 = new PackedDecimalData(17,2).isAPartOf(dataRec, 576).init("ZERO");
  	public PackedDecimalData interimsacscurbal04 = new PackedDecimalData(17,2).isAPartOf(dataRec, 585).init("ZERO");
  	public PackedDecimalData interimsacscurbal05 = new PackedDecimalData(17,2).isAPartOf(dataRec, 594).init("ZERO");
  	public PackedDecimalData interimsacscurbal06 = new PackedDecimalData(17,2).isAPartOf(dataRec, 603).init("ZERO");
  	public PackedDecimalData interimsacscurbal07 = new PackedDecimalData(17,2).isAPartOf(dataRec, 612).init("ZERO");
  	public PackedDecimalData interimsacscurbal08 = new PackedDecimalData(17,2).isAPartOf(dataRec, 621).init("ZERO");
  	public PackedDecimalData interimsacscurbal09 = new PackedDecimalData(17,2).isAPartOf(dataRec, 630).init("ZERO");
  	public PackedDecimalData interimsacscurbal10 = new PackedDecimalData(17,2).isAPartOf(dataRec, 639).init("ZERO");
	
  	public PackedDecimalData terminalsacscurbal01 = new PackedDecimalData(17,2).isAPartOf(dataRec, 648).init("ZERO");
  	public PackedDecimalData terminalsacscurbal02 = new PackedDecimalData(17,2).isAPartOf(dataRec, 657).init("ZERO");
  	public PackedDecimalData terminalsacscurbal03 = new PackedDecimalData(17,2).isAPartOf(dataRec, 666).init("ZERO");
  	public PackedDecimalData terminalsacscurbal04 = new PackedDecimalData(17,2).isAPartOf(dataRec, 675).init("ZERO");
  	public PackedDecimalData terminalsacscurbal05 = new PackedDecimalData(17,2).isAPartOf(dataRec, 684).init("ZERO");
  	public PackedDecimalData terminalsacscurbal06 = new PackedDecimalData(17,2).isAPartOf(dataRec, 693).init("ZERO");
  	public PackedDecimalData terminalsacscurbal07 = new PackedDecimalData(17,2).isAPartOf(dataRec, 702).init("ZERO");
  	public PackedDecimalData terminalsacscurbal08 = new PackedDecimalData(17,2).isAPartOf(dataRec, 711).init("ZERO");
  	public PackedDecimalData terminalsacscurbal09 = new PackedDecimalData(17,2).isAPartOf(dataRec, 720).init("ZERO");
  	public PackedDecimalData terminalsacscurbal10 = new PackedDecimalData(17,2).isAPartOf(dataRec, 729).init("ZERO");
  	
  	 	
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxvestRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxvestRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}