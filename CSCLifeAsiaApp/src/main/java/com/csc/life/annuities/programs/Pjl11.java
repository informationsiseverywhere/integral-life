package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;

import com.csc.life.annuities.screens.Sjl11ScreenVars;
import com.csc.life.annuities.tablestructures.Tjl11rec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl11 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL11");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");	
	/* ERRORS */
	private String e026 = "E026";
	private String e027 = "E027";	
	private String h357 = "H357";
	private String h366 = "H366";
	private static final String tjl09 = "TJL09";
	private static final String tjl10 = "TJL10";
	private String wsaaItempfx;
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	private String wsaaItemseq;
	private String wsaaItemlang;
	private String wsaaItemfrm;
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaFirstTime = "Y";
	private Tjl11rec tjl11rec = new Tjl11rec();
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	private Itempf itempf = null;	
	private Itemkey wsaaItemkey = new Itemkey();
	private Itmdkey wsaaItemdkey = new Itmdkey();	
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl11ScreenVars sv = ScreenProgram.getScreenVars(Sjl11ScreenVars.class);
	private boolean isDataChanged = false;		

	public Pjl11() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl11", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		sv.errorIndicators.set(SPACES);
		wsaaItemseq = wsaaItemkey.itemItemseq.toString();
		if (isNE(wsaaFirstTime, "Y")) {
			if (isNE(wsaaSeq, ZERO)) {
				wsaaItemseq = wsaaSeq.toString();
			}
		} else {
			sv.dataArea.set(SPACES);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaItempfx = wsaaItemkey.itemItempfx.toString();
		wsaaItemcoy = wsaaItemkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItemkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItemkey.itemItemtabl.toString();
		wsaaItemlang = wsspcomn.language.toString();		
		sv.company.set(wsaaItemkey.itemItemcoy);
		wsaaItemdkey.set(wsspsmart.itmdkey);
		sv.tabl.set(wsaaItemdkey.itemItemtabl);
		sv.item.set(wsaaItemdkey.itemItemitem);	
		wsaaItemfrm = wsaaItemdkey.itemItmfrm.toString();
		String longdesc = iafDescDAO.getItemLongDesc(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem,
				wsaaItemlang);
		sv.longdesc.set(longdesc);		
		itempf = itemDAO.findItemByDate(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem, wsaaItemfrm, "1");
		if (itempf == null) {
			if(isEQ(wsspcomn.flag, "I")) {
				scrnparams.errorCode.set(e026);
				if(isGT(wsaaSeq,ZERO)) {
					wsaaSeq.subtract(1);
				}
			} else {
				tjl11rec.tjl11Rec.set(SPACES);
			}
		} else {
			tjl11rec.tjl11Rec.set(itempf.getGenareaString());
		}
		sv.itmfrm.set(itempf.getItmfrm());
		sv.itmto.set(itempf.getItmto());
		sv.annuitype.set(tjl11rec.annuitype);		
		sv.anntypdesc.set(tjl11rec.anntypdesc);
		sv.beneftypes.set(tjl11rec.beneftypes);		
		sv.bentypsdesc.set(tjl11rec.bentypsdesc);
		sv.defannpayfreq.set(tjl11rec.defannpayfreq);		
		wsaaFirstTime = "N";
	}	
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
	}

	@SuppressWarnings("static-access")
	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rold) && isEQ(wsaaSeq, ZERO)) {
			scrnparams.errorCode.set(e027);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu) && isEQ(wsaaSeq, "99")) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		validate2010();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void validate2010() {		
		if(isEQ(sv.annuitype, SPACES)){
			sv.annuitypeErr.set(h366);
		}
		if(isEQ(sv.beneftype01, SPACES) && isEQ(sv.beneftype02, SPACES) && isEQ(sv.beneftype03, SPACES) &&
				isEQ(sv.beneftype04, SPACES) && isEQ(sv.beneftype05, SPACES) && isEQ(sv.beneftype06, SPACES)){
			sv.beneftype01Err.set(h366);
			sv.beneftype02Err.set(h366);
			sv.beneftype03Err.set(h366);
			sv.beneftype04Err.set(h366);
			sv.beneftype05Err.set(h366);
			sv.beneftype06Err.set(h366);
		}	
		for (int i = 1; i <= 6; i++) {
			if(isNE(sv.beneftype[i], SPACES)){
				for (int j = i + 1; j <= 6; j++) {
					if (isNE(sv.beneftype[j], SPACES) && isEQ(sv.beneftype[i], sv.beneftype[j])) {
						sv.beneftypeErr[i].set(h357);
						sv.beneftypeErr[j].set(h357);
					}			
				}
			}
		}
		if(isEQ(sv.annuitype, SPACES)){
			sv.anntypdesc.set(SPACES);
		}else{
			sv.anntypdesc.set(iafDescDAO.getItemLongDesc(wsaaItempfx, wsaaItemcoy, tjl09, sv.annuitype.toString().trim(),wsaaItemlang));
		}
		for (int i = 1; i <= 6; i++) {
			if(isEQ(sv.beneftype[i], SPACES)){							
				sv.bentypdesc[i].set(SPACES);								
			}else{
				sv.bentypdesc[i].set(iafDescDAO.getItemLongDesc(wsaaItempfx, wsaaItemcoy, tjl10, sv.beneftype[i].toString().trim(),wsaaItemlang));
			}
		}
	}		

	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.annuitype, tjl11rec.annuitype)) {
			tjl11rec.annuitype.set(sv.annuitype);			
			isDataChanged = true;
		}
		if (isNE(sv.anntypdesc, tjl11rec.anntypdesc)) {			
			tjl11rec.anntypdesc.set(sv.anntypdesc);
			isDataChanged = true;
		}
		if (isNE(sv.beneftypes, tjl11rec.beneftypes)) {
			tjl11rec.beneftypes.set(sv.beneftypes);			
			isDataChanged = true;
		}	
		if (isNE(sv.bentypsdesc, tjl11rec.bentypsdesc)) {			
			tjl11rec.bentypsdesc.set(sv.bentypsdesc);
			isDataChanged = true;
		}
		if (isNE(sv.defannpayfreq, tjl11rec.defannpayfreq)) {
			tjl11rec.defannpayfreq.set(sv.defannpayfreq);
			isDataChanged = true;
		}		
		if (isNE(sv.itmfrm,itempf.getItmfrm())) {
			itempf.setItmfrm(new BigDecimal(sv.itmfrm.toString()));
			isDataChanged = true;
		}
		if (isNE(sv.itmto,itempf.getItmto())) {
			itempf.setItmto(new BigDecimal(sv.itmto.toString()));
			isDataChanged = true;
		}
		if(isDataChanged) {
			if(itempf==null) {
				writeData();
			} else {
				itempf.setGenarea(tjl11rec.tjl11Rec.toString().getBytes());
				itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
						.getGENAREAJString(tjl11rec.tjl11Rec.toString().getBytes(), tjl11rec));
				itempf.setItempfx(wsaaItempfx);
				itempf.setItemcoy(wsaaItemcoy);
				itempf.setItemtabl(wsaaItemtabl);
				itempf.setItemitem(wsaaItemitem);
				itempf.setItemseq(wsaaItemseq);
				itemDAO.updateSmartTableItem(itempf);
			}
		}
	}
	
	protected void writeData() {
		Itempf item = new Itempf();
		item.setItempfx(wsaaItempfx);
		item.setItemcoy(wsaaItemcoy);
		item.setItemtabl(wsaaItemtabl);
		item.setItemitem(wsaaItemitem);
		item.setItemseq(wsaaSeq.toString());
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		item.setTranid(varcom.vrcmCompTranid.toString());
		item.setTableprog(wsaaProg.toString());
		item.setValidflag("1");
		item.setItmfrm(new BigDecimal(0));
		item.setItmto(new BigDecimal(0));
		item.setGenarea(tjl11rec.tjl11Rec.toString().getBytes());
		item.setGenareaj("");
		itemDAO.insertSmartTableItem(item);
	}

	@SuppressWarnings("static-access")
	protected void whereNext4000() {
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaSeq.add(1);
		} else if (isEQ(scrnparams.statuz, varcom.rold)) {
			wsaaSeq.subtract(1);
		} else {
			wsspcomn.programPtr.add(1);
			wsaaSeq.set(ZERO);
		}
	}
}
