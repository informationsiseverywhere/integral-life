package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:48
 * Description:
 * Copybook name: REGTVSTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regtvstkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regtvstFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regtvstKey = new FixedLengthStringData(64).isAPartOf(regtvstFileKey, 0, REDEFINE);
  	public FixedLengthStringData regtvstChdrcoy = new FixedLengthStringData(1).isAPartOf(regtvstKey, 0);
  	public FixedLengthStringData regtvstChdrnum = new FixedLengthStringData(8).isAPartOf(regtvstKey, 1);
  	public FixedLengthStringData regtvstLife = new FixedLengthStringData(2).isAPartOf(regtvstKey, 9);
  	public FixedLengthStringData regtvstCoverage = new FixedLengthStringData(2).isAPartOf(regtvstKey, 11);
  	public FixedLengthStringData regtvstRider = new FixedLengthStringData(2).isAPartOf(regtvstKey, 13);
  	public PackedDecimalData regtvstPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(regtvstKey, 15);
  	public PackedDecimalData regtvstRgpynum = new PackedDecimalData(5, 0).isAPartOf(regtvstKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(regtvstKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regtvstFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regtvstFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}