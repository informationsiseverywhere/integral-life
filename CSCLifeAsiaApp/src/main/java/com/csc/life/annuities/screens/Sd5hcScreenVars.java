package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sd5hc
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class Sd5hcScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(678);
	public FixedLengthStringData dataFields = new FixedLengthStringData(278).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
		
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,19);
	

	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData currdesc = DD.currdesc.copy().isAPartOf(dataFields,52);
	
	
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,90);
	
	//ommencement date 
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,137);
	
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,145);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,192);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,196);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,206);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,214);
	
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(dataFields,224);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,225);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,238);
	//bdatedisplay
	public ZonedDecimalData btdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,239);
	//calculate frequency
	public FixedLengthStringData intcalfreq = DD.regpayfreq.copy().isAPartOf(dataFields,247);
	//capitalize frequency
	public FixedLengthStringData intcapfreq = DD.regpayfreq.copy().isAPartOf(dataFields,249);
	//Interest Rate
	public ZonedDecimalData intRate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields,251);
	//Accumulated  
	public ZonedDecimalData accamnt = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,259);	
	public FixedLengthStringData refcode = DD.insuffmn.copy().isAPartOf(dataFields,276);
	public FixedLengthStringData ansrepy = DD.ansrepy.copy().isAPartOf(dataFields,277);
	
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(100).isAPartOf(dataArea, 278);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
    public FixedLengthStringData rgpymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
    public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
    public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
    public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
    
    public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
    public FixedLengthStringData intcalfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
    public FixedLengthStringData intcapfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
    public FixedLengthStringData intRateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
    public FixedLengthStringData accamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
    public FixedLengthStringData refcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
    public FixedLengthStringData ansrepyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);

    
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(300).isAPartOf(dataArea, 368);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] rgpymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] intcalfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] intcapfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] intRateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] accamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] refcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] ansrepyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	
	public LongData Sd5hcscreenWritten = new LongData(0);
	public LongData Sd5hcprotectWritten = new LongData(0);
	public FixedLengthStringData msgPopup = new FixedLengthStringData(1000);
	public FixedLengthStringData msgresult = new FixedLengthStringData(10);
	public FixedLengthStringData msgDisplay = new FixedLengthStringData(10);
	public boolean hasSubfile() {
		return false;
	}


	public Sd5hcScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rgpymopOut,new String[] {"04","23","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
	//	fieldIndMap.put(rgpymopOut,new String[] {"04","23","-04",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, lifcnum, linsname,  chdrstatus, rstate, planSuffix, currcd, currdesc,  ptdate, occdate, rgpymop,ddind,btdate,intcalfreq,intcapfreq,intRate,accamnt,register,refcode,ansrepy};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut,  chdrstatusOut, rstateOut, plnsfxOut, currcdOut, currdescOut,  ptdateOut, occdateOut,rgpymopOut,ddindOut,btdateOut,intcalfreqOut,intcapfreqOut,intRateOut,accamntOut,registerOut,refcodeOut,ansrepyOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr,  chdrstatusErr, rstateErr, plnsfxErr, currcdErr, currdescErr,ptdateErr, occdateErr, rgpymopErr,ddindErr,btdateErr,intcalfreqErr,intcapfreqErr,intRateErr,accamntErr,registerErr,refcodeErr,ansrepyErr};
		screenDateFields = new BaseData[] {ptdate, occdate,btdate};
		screenDateErrFields = new BaseData[] {ptdateErr, occdateErr,btdateErr};
		screenDateDispFields = new BaseData[] {ptdateDisp, occdateDisp,btdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5hcscreen.class;
		protectRecord = Sd5hcprotect.class;
	}

}
