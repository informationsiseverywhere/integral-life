package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5221
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class S5221ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(758);
	public FixedLengthStringData dataFields = new FixedLengthStringData(374).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public ZonedDecimalData comtperc = DD.comtperc.copyToZonedDecimal().isAPartOf(dataFields,11);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData currdesc = DD.currdesc.copy().isAPartOf(dataFields,57);
	public ZonedDecimalData elvstfct = DD.elvstfct.copyToZonedDecimal().isAPartOf(dataFields,87);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,148);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,156);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,203);
	public FixedLengthStringData optdsc = DD.optdsc.copy().isAPartOf(dataFields,211);
	public FixedLengthStringData optind = DD.optind.copy().isAPartOf(dataFields,226);
	public ZonedDecimalData origpay = DD.origpay.copyToZonedDecimal().isAPartOf(dataFields,227);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,244);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,291);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,295);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,305);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,313);
	public ZonedDecimalData vstlump = DD.vstlump.copyToZonedDecimal().isAPartOf(dataFields,323);
	public ZonedDecimalData vstpay = DD.vstpay.copyToZonedDecimal().isAPartOf(dataFields,340);
	public ZonedDecimalData vstpaya = DD.vstpaya.copyToZonedDecimal().isAPartOf(dataFields,357);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 374);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData comtpercErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData elvstfctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData optdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData optindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData origpayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData vstlumpErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData vstpayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData vstpayaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 470);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] comtpercOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] elvstfctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] origpayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] vstlumpOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] vstpayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] vstpayaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5221screenWritten = new LongData(0);
	public LongData S5221protectWritten = new LongData(0);
	
		 


	public boolean hasSubfile() {
		return false;
	}


	public S5221ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(optindOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vstlumpOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comtpercOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vstpayOut,new String[] {"02","50","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vstpayaOut,new String[] {null, null, null, "20",null, null, null, null, null, null, null, null});
		fieldIndMap.put(elvstfctOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, pstate, rstate, planSuffix, currcd, currdesc, optind, vstlump, comtperc, ptdate, occdate, optdsc, vstpay, origpay, vstpaya, elvstfct};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, pstateOut, rstateOut, plnsfxOut, currcdOut, currdescOut, optindOut, vstlumpOut, comtpercOut, ptdateOut, occdateOut, optdscOut, vstpayOut, origpayOut, vstpayaOut, elvstfctOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, pstateErr, rstateErr, plnsfxErr, currcdErr, currdescErr, optindErr, vstlumpErr, comtpercErr, ptdateErr, occdateErr, optdscErr, vstpayErr, origpayErr, vstpayaErr, elvstfctErr};
		screenDateFields = new BaseData[] {ptdate, occdate};
		screenDateErrFields = new BaseData[] {ptdateErr, occdateErr};
		screenDateDispFields = new BaseData[] {ptdateDisp, occdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5221screen.class;
		protectRecord = S5221protect.class;
	}

}
