/*
 * File: P5225at.java
 * Date: 30 August 2009 0:20:12
 * Author: Quipoz Limited
 *
 * Class transformed from P5225AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.annuities.dataaccess.VstdbrkTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgpTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
* OVERVIEW
* ========
*
* P5225
* =====
*
* Softlocks the  Contract  Header  (CHDR)  and  submits  the  AT
* request.
*
* P5225AT
* =======
*
* This  is  a new program which forms part of the 9405 Annuities
* Development.    It  forms  part  of   the   Vesting   Approval
* processing.
*
* The program is driven by VSTD records, and for each one it
* finds the new coverage written by P5223AT (found by field
* VSTDBRK-NEWCOVR) and updates its statii, as well as valid
* flag 1 / 2 the records. The other Coverages associated with
* the contract are left unprocessed.
*
* It only changes the Statii on the CHDR and PAYR records
* if the contact is not at risk. To do this a check is required
* only on the CHDRs and not on all the COVRs, since the vesting
* registration program (P5223AT) has checked all the COVRs risk
* statii and updated the CHDR accordingly.
*
* When finished with them, it  deletes  Vesting Detail (VSTD)
* records created by the Vesting Registration transaction.
* It will also read T5671 and call any subroutine  found  to  do
* generic processing.
*
*****************************************************************
* </pre>
*/
public class P5225at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5225AT");
	private PackedDecimalData wsaaVstdEffdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 16);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 24);
		/* WSAA-FLAGS-INDICATORS-STORES */
	private String wsaaValidCombination = "";
	private PackedDecimalData wsaaOldTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaChdrRiskFlag = new FixedLengthStringData(1);
	private Validator chdrAtRisk = new Validator(wsaaChdrRiskFlag, "Y");

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5671 = "T5671";
	private static final String t7508 = "T7508";
		/* FORMATS */
	private static final String vstdbrkrec = "VSTDBRKREC";
	private static final String liferec = "LIFEREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String covrrec = "COVRREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String payrrec = "PAYRREC";
	private static final String chdrrec = "CHDRREC";
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Components for Regular Withdrawals.*/
	private CovrrgpTableDAM covrrgpIO = new CovrrgpTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private VstdbrkTableDAM vstdbrkIO = new VstdbrkTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Isuallrec isuallrec = new Isuallrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T7508rec t7508rec = new T7508rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		errorProg610
	}

	public P5225at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialize1000();
		while ( !(isEQ(vstdbrkIO.getStatuz(),varcom.endp))) {
			processVstdbrk2000();
		}

		updateChdrPayrLife3000();
		writePtrn4000();
		/*FINISH-OFF*/
		updateBatchHead5000();
		dryProcessing5500();
		releaseSoftlock6000();
		exitProgram();
	}

protected void initialize1000()
	{
		getChdr1000();
	}

protected void getChdr1000()
	{
		wsaaChdrRiskFlag.set("N");
		wsaaNewTranno.set(ZERO);
		wsaaOldTranno.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* READR the contract header and store the trans number:*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		chdrlnbIO.setChdrcoy(atmodrec.company);
		chdrlnbIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setChdrcoy(atmodrec.company);
		chdrlnbIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Increment the transaction number for use throughout*/
		/* the program. The old tranno is also stored and is used*/
		/* in the key in the PROCESS-NEW-COVERAGE section.*/
		wsaaOldTranno.set(chdrlnbIO.getTranno());
		compute(wsaaNewTranno, 0).set(add(1,wsaaOldTranno));
		statusCodes1100();
		getPayrDetails1200();
		readFirstVstdbrkRecord1300();
	}

protected void statusCodes1100()
	{
		readTable1100();
	}

protected void readTable1100()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void getPayrDetails1200()
	{
		/*STARTS*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction("READH");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readFirstVstdbrkRecord1300()
	{
		starts1300();
	}

protected void starts1300()
	{
		vstdbrkIO.setDataArea(SPACES);
		vstdbrkIO.setChdrcoy(atmodrec.company);
		vstdbrkIO.setChdrnum(wsaaPrimaryChdrnum);
		vstdbrkIO.setTranno(ZERO);
		vstdbrkIO.setPlanSuffix(ZERO);
		vstdbrkIO.setFunction(varcom.begnh);
		vstdbrkIO.setFormat(vstdbrkrec);
		SmartFileCode.execute(appVars, vstdbrkIO);
		if (isNE(vstdbrkIO.getStatuz(),varcom.oK)
		&& isNE(vstdbrkIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(vstdbrkIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(vstdbrkIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdbrkIO.getChdrcoy(),atmodrec.company)
		|| isNE(vstdbrkIO.getChdrnum(),wsaaPrimaryChdrnum)) {
			vstdbrkIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, vstdbrkIO);
			if (isNE(vstdbrkIO.getStatuz(),varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(vstdbrkIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(vstdbrkIO.getStatuz());
				fatalError600();
			}
			vstdbrkIO.setStatuz(varcom.endp);
		}
	}

protected void processVstdbrk2000()
	{
		/*START*/
		readCovr2100();
		updateCoverageStatii2200();
		doGenericProcessing2300();
		deleteVstdbrk2400();
		readNextVstdbrk2500();
		/*EXIT*/
	}

protected void readCovr2100()
	{
		starts2100();
	}

protected void starts2100()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(vstdbrkIO.getChdrcoy());
		covrIO.setChdrnum(vstdbrkIO.getChdrnum());
		covrIO.setCoverage(vstdbrkIO.getNewcovr());
		covrIO.setLife(vstdbrkIO.getLife());
		covrIO.setPlanSuffix(vstdbrkIO.getPlanSuffix());
		covrIO.setRider(vstdbrkIO.getRider());
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void updateCoverageStatii2200()
	{
		starts2200();
	}

protected void starts2200()
	{
		covrIO.setValidflag("2");
		covrIO.setTranno(wsaaOldTranno);
		covrIO.setCurrto(vstdbrkIO.getEffdate());
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(payrIO.getBillfreq(),"00")) {
			if (isNE(t5679rec.setSngpCovStat,SPACES)) {
				covrIO.setPstatcode(t5679rec.setSngpCovStat);
			}
		}
		else {
			if (isNE(t5679rec.setCovPremStat,SPACES)) {
				covrIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		if (isNE(t5679rec.setCovRiskStat,SPACES)) {
			covrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		covrIO.setCurrfrom(vstdbrkIO.getEffdate());
		covrIO.setCurrto(99999999);
		covrIO.setValidflag("1");
		covrIO.setTranno(wsaaNewTranno);
		covrIO.setUser(wsaaUser);
		covrIO.setTermid(wsaaTermid);
		covrIO.setTransactionTime(wsaaTransactionTime);
		covrIO.setTransactionDate(wsaaTransactionDate);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void doGenericProcessing2300()
	{
			starts2300();
		}

protected void starts2300()
	{
		/* Lookup the generic component processing programs (T5671)*/
		/* and call each non blank subroutine.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.addExpression(covrIO.getCrtable(), SPACES);
		stringVariable1.setStringInto(wsaaT5671Item);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5671);
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		isuallrec.company.set(chdrlnbIO.getChdrcoy());
		isuallrec.chdrnum.set(chdrlnbIO.getChdrnum());
		isuallrec.life.set(covrIO.getLife());
		isuallrec.coverage.set(covrIO.getCoverage());
		isuallrec.rider.set(covrIO.getRider());
		isuallrec.planSuffix.set(covrIO.getPlanSuffix());
		isuallrec.freqFactor.set(1);
		isuallrec.batchkey.set(atmodrec.batchKey);
		isuallrec.transactionDate.set(wsaaEffdate);
		isuallrec.transactionTime.set(wsaaTransactionTime);
		isuallrec.user.set(wsaaUser);
		isuallrec.termid.set(wsaaTermid);
		isuallrec.newTranno.set(ZERO);
		isuallrec.effdate.set(payrIO.getPtdate());
		isuallrec.language.set(atmodrec.language);
		isuallrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		isuallrec.function.set(SPACES);
		isuallrec.oldcovr.set(SPACES);
		isuallrec.oldrider.set(SPACES);
		isuallrec.runDate.set(varcom.vrcmMaxDate);
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlnbIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		isuallrec.statuz.set(varcom.oK);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,3)); wsaaSub1.add(1)){
			if (isNE(t5671rec.subprog[wsaaSub1.toInt()],SPACES)) {
				callProgram(t5671rec.subprog[wsaaSub1.toInt()], isuallrec.isuallRec);
				if (isNE(isuallrec.statuz,varcom.oK)) {
					sysrSyserrRecInner.sysrParams.set(isuallrec.isuallRec);
					sysrSyserrRecInner.sysrStatuz.set(isuallrec.statuz);
					fatalError600();
				}
			}
		}
	}

protected void deleteVstdbrk2400()
	{
		/*START*/
		vstdbrkIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, vstdbrkIO);
		if (isNE(vstdbrkIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(vstdbrkIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(vstdbrkIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readNextVstdbrk2500()
	{
		start2500();
	}

protected void start2500()
	{
		wsaaVstdEffdate.set(vstdbrkIO.getEffdate());
		wsaaLife.set(vstdbrkIO.getLife());
		vstdbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vstdbrkIO);
		if (isNE(vstdbrkIO.getStatuz(),varcom.oK)
		&& isNE(vstdbrkIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(vstdbrkIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(vstdbrkIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdbrkIO.getChdrcoy(),atmodrec.company)
		|| isNE(vstdbrkIO.getChdrnum(),wsaaPrimaryChdrnum)) {
			vstdbrkIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, vstdbrkIO);
			if (isNE(vstdbrkIO.getStatuz(),varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(vstdbrkIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(vstdbrkIO.getStatuz());
				fatalError600();
			}
			vstdbrkIO.setStatuz(varcom.endp);
			vstdbrkIO.setEffdate(wsaaVstdEffdate);
			vstdbrkIO.setLife(wsaaLife);
		}
	}

protected void updateChdrPayrLife3000()
	{
			start3000();
		}

protected void start3000()
	{
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlnbIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		chdrIO.setDataArea(SPACES);
		chdrIO.setChdrcoy(atmodrec.company);
		chdrIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrIO.setCurrfrom(99999999);
		chdrIO.setValidflag("1");
		chdrIO.setChdrpfx("CH");
		chdrIO.setFunction(varcom.begn);
		chdrIO.setFormat(chdrrec);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "VALIDFLAG");
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(chdrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.endp)
		|| isNE(chdrIO.getChdrcoy(),atmodrec.company)
		|| isNE(chdrIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(chdrIO.getValidflag(),"1")) {
			sysrSyserrRecInner.sysrParams.set(chdrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatcode(),t5679rec.setCnRiskStat)) {
			chdrAtRisk.setTrue();
		}
		chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setChdrcoy(atmodrec.company);
		chdrlnbIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlnbIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setCurrto(vstdbrkIO.getEffdate());
		chdrlnbIO.setValidflag("2");
		chdrlnbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlnbIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		if (!chdrAtRisk.isTrue()) {
			getStatii3200();
		}
		chdrlnbIO.setTranno(wsaaNewTranno);
		chdrlnbIO.setStattran(wsaaNewTranno);
		chdrlnbIO.setTranlused(wsaaNewTranno);
		chdrlnbIO.setValidflag("1");
		chdrlnbIO.setCurrfrom(vstdbrkIO.getEffdate());
		chdrlnbIO.setCurrto(99999999);
		chdrlnbIO.setFunction(varcom.writr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlnbIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction("READH");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setFormat(payrrec);
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setTranno(wsaaNewTranno);
		payrIO.setEffdate(vstdbrkIO.getEffdate());
		payrIO.setValidflag("1");
		if (!chdrAtRisk.isTrue()) {
			if (isNE(t5679rec.setCnPremStat,SPACES)) {
				payrIO.setPstatcode(t5679rec.setCnPremStat);
			}
		}
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (chdrAtRisk.isTrue()) {
			return ;
		}
		lifeIO.setDataArea(SPACES);
		lifeIO.setChdrcoy(atmodrec.company);
		lifeIO.setChdrnum(wsaaPrimaryChdrnum);
		lifeIO.setLife(vstdbrkIO.getLife());
		lifeIO.setJlife(ZERO);
		lifeIO.setCurrfrom(99999999);
		lifeIO.setFunction("BEGNH");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isNE(lifeIO.getChdrcoy(),atmodrec.company)
		|| isNE(lifeIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(lifeIO.getLife(),vstdbrkIO.getLife())
		|| isEQ(lifeIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		lifeIO.setFormat(liferec);
		lifeIO.setCurrto(vstdbrkIO.getEffdate());
		lifeIO.setValidflag("2");
		lifeIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		lifeIO.setTranno(wsaaNewTranno);
		lifeIO.setCurrfrom(vstdbrkIO.getEffdate());
		lifeIO.setCurrto(99999999);
		lifeIO.setValidflag("1");
		lifeIO.setUser(wsaaUser);
		lifeIO.setTermid(wsaaTermid);
		lifeIO.setTransactionTime(wsaaTransactionTime);
		lifeIO.setTransactionDate(wsaaTransactionDate);
		if (isNE(t5679rec.setLifeStat,SPACES)) {
			lifeIO.setStatcode(t5679rec.setLifeStat);
		}
		lifeIO.setFunction(varcom.writr);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifeIO.getStatuz());
			fatalError600();
		}
	}

protected void getStatii3200()
	{
		/*START*/
		if (isEQ(payrIO.getBillfreq(),"00")) {
			if (isNE(t5679rec.setSngpCnStat,SPACES)) {
				chdrlnbIO.setPstatcode(t5679rec.setSngpCnStat);
			}
		}
		else {
			if (isNE(t5679rec.setCnPremStat,SPACES)) {
				chdrlnbIO.setPstatcode(t5679rec.setCnPremStat);
			}
		}
		if (isNE(t5679rec.setCnRiskStat,SPACES)) {
			chdrlnbIO.setStatcode(t5679rec.setCnRiskStat);
		}
		/*EXIT*/
	}

protected void writePtrn4000()
	{
		ptrn4000();
	}

protected void ptrn4000()
	{
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTransactionDate(ZERO);
		ptrnIO.setTransactionTime(ZERO);
		ptrnIO.setUser(ZERO);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setPtrneff(vstdbrkIO.getEffdate());
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaEffdate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void updateBatchHead5000()
	{
		/*UPDATES*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			sysrSyserrRecInner.sysrStatuz.set(batcuprec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void dryProcessing5500()
	{
		start5510();
	}

protected void start5510()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlnbIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75085600();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75085600();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		/* MOVE TDAY                   TO DTC1-FUNCTION.        <LA5184>*/
		/* CALL 'DATCON1'           USING DTC1-DATCON1-REC.     <LA5184>*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrlnbIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlnbIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlnbIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlnbIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlnbIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlnbIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlnbIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlnbIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlnbIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlnbIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrlnbIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrlnbIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(drypDryprcRecInner.drypDryprcRec);
			sysrSyserrRecInner.sysrStatuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75085600()
	{
		start5610();
	}

protected void start5610()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void releaseSoftlock6000()
	{
		softlock6000();
	}

protected void softlock6000()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(atmodrec.primaryKey);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(sftlockrec.sftlockRec);
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					fatal601();
				case errorProg610:
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatal601()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void errorProg610()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void stop699()
	{
		stopRun();
	}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
