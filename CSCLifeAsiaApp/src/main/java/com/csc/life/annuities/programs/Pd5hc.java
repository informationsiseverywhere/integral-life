/*
 * File: Pd5hc.java
 * Date: 30 August 2009 0:19:15
 * Author: Quipoz Limited
 * 
 * Class transformed from Pd5hc.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.annuities.dataaccess.RegtvstTableDAM;
import com.csc.life.annuities.screens.Sd5hcScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RgpdetpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *  Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 * 
 * REMARKS.
 *  This program is part of the 9405 Annuities Development.  It is
 *  called via T5671 switching when a user is creating, modifying
 *  or enquiring upon a Vesting Annuity proposal.  It can also be
 *  accessed during Vesting Approval in enquiry mode.
 * 
 *  This program controls the Vesting Annuity Payment Details
 *  screen, which is used to modify or enquire upon annuity
 *  payment details held on the Regular Payments Temporary file,
 *  REGT.
 * 
 *  If in enquiry mode all fields except the window select fields
 *  will be protected.
 * 
 *  Display the REGT details on the screen  looking  up  all  of
 *  the descriptions from DESC where appropriate.
 * 
 *  If  the  bank details on REGT are non-blank set a '+' in the
 *  Bank Details indicator field.
 * 
 *  Validation rules.
 *  =================
 * 
 *  Reason  Code.
 *   This is a mandatory field. The item must exist on the Regular
 *   Payment Reason Codes table, T6692.  A window facility is  for
 *   this field.
 * 
 *  Evidence.
 *   This field is free format and optional.
 * 
 *  Payment Method.
 *   This is a mandatory field.  If the entry on  Regular  Payment
 *   Methods  table,  T6694,  for  the  method of payment entered,
 *   indicates that bank details are required an X will be  forced
 *   into  the  Bank  Details selection field to ensure that  bank
 *   details will be set up.
 * 
 *  Payment Currency.
 *   This  is  a mandatory field.  It will  be  held  on the  REGT
 *   created  by  P5329.  It can be overidden and must be a  valid
 *   item on T3629.
 * 
 *  Frequency Code.
 *   This  is a mandatory field.  It will be held on the REGT file
 *   created  by P5329.  It can  only be overridden if the Regular
 *   Claim  Detail  rules  held  on  table  T6696  allow frequency
 *   override. If the Frequency  Code has  changed then the  Total
 *   Sum  Assured  field  must  also be  re-calculated to bring it
 *   into line with the new frequency.
 * 
 *  Payee  Client.
 *   If the entry on the Regular Payment Methods of Payment table,
 *   T6694,  for  the  method of  payment entered indicates that a
 *   payee is required this field  is  mandatory,  otherwise it is
 *   not required and will error if completed.
 * 
 *  Percentage.
 *   If the  percentage and amount  fields  are  blank, obtain the
 *   the  default  percentage  from  table T6696 and calculate the
 *   amount from this.  If the  percentage is changed, recalculate
 *   the amount as a  percentage of the sum assured on the screen.
 *   If both  the percentage  and amount  fields are  changed, the
 *   amount takes precedence and the percentage is recalculated.
 * 
 *  Destination  Key.
 *   If  the  'Contract  Details  Required'  indicator   on  T6694
 *   is 'Y' then a valid contract  number must  be  entered  here.
 *   Otherwise the field must be blank.
 * 
 *  Payment Amount.
 *   This is the amount to be paid for the regular payment record.
 *   The total  payment amount for  all Immediate annuity payments
 *   payable  during the  same period can not be  greater than the
 *   sum assured for the Immediate Annuity component.
 * 
 *  Effective date.
 *   This  is  the  Effective  Date  of  Annuity  payments  and is
 *   shown here for information only.
 * 
 *  First Payment Date.
 *   This  is obtained from the REGT record and is shown here  for
 *   information only.
 * 
 *  Review Date.
 *   This is obtained from the REGT record and is protected if the
 *   coverage has a guaranteed payment period.
 * 
 *  Final Payment Date.
 *   This is optional  and  if  left  blank will  be  set  to  Max
 *   Date. If entered it must not be less than  the First  Payment
 *   date.
 * 
 *  Anniversary Date.
 *   This may be entered by the user  but  if not  then  calculate
 *   it from the effective Date and the Indexation Frequency  from
 *   T6696. The default is Max Date.
 * 
 *  Tables Used.
 *       T3629 - Currency Code Details
 *       Key: CURRCD
 *       T5606 - Regular Benefit Edit Rules
 *       Key: Validation Item Key (from T5671) || Currency Code
 *       T5671 - Generic Program Switching
 *       Key: Transaction Code || CRTABLE
 *       T5688 - Contract Definition
 *       Key: Contract Type
 *       T6692 - Regular Payment Reason Codes
 *       Key: Regular Payment Reason Code
 *       T6694 - Regular Payment Method of Payment
 *       Key: Regular Payment MOP
 *       T6696 - Regular Claim Detail Rules
 *       Key: CRTABLE || Reason Code
 *
 *****************************************************************
 * </pre>
 */
public class Pd5hc extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5)
			.init("PD5HC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2)
			.init("01");
	private String wsaaTotalUsed = "";
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private static final String t3629 = "T3629";
	private static final String t5688 = "T5688";
	private static final String t6694 = "T6694";
	// ILIFE-5330
	/* Contract Header File - Major Alts */
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	/* Client logical file with new fields */
	private RegtvstTableDAM regtvstIO = new RegtvstTableDAM();

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T6694rec t6694rec = new T6694rec();
	private Wssplife wssplife = new Wssplife();
	private Sd5hcScreenVars sv = ScreenProgram
			.getScreenVars(Sd5hcScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO",
			AcblpfDAO.class);
	private List<Acblpf> acblenqListLPAP = null;
	private List<Acblpf> acblenqListLPAN = null;
	private final DescDAO descDao = getApplicationContext().getBean("descDAO",
			DescDAO.class);
	
	Chdrpf chdrpf = new Chdrpf();
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO",
			ChdrpfDAO.class);
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",
			ClntpfDAO.class);
	private Ptrnpf ptrnpf = null;
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",
			PtrnpfDAO.class);
	private Batckey wsaaBatckey1 = new Batckey();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);//ILIFE-3955
	private Td5h7rec td5h7rec = new Td5h7rec();
	private Sftlockrec sftlockrec = new Sftlockrec(); 
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private Regppf regppf;
	private List<Regppf> regppfList; 
	private BigDecimal accAmtLPAP = new BigDecimal(0);
	private BigDecimal accAmtLPAN = new BigDecimal(0);
	private T5645rec t5645rec = new T5645rec();
	private int chdrTranno;
	private List<Chdrpf> chdrBulkUpdtList; 
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private T5688rec t5688rec = new T5688rec();
	private T3629rec t3629rec = new T3629rec();
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private String regtvstrec = "REGTVSTREC"; 
	private RgpdetpfDAO rgpdetpfDAO = getApplicationContext().getBean("rgpdetpfDAO", RgpdetpfDAO.class);//ILIFE-3955
	private static final String chdrmjarec = "CHDRMJAREC";
	private Payreqrec payreqrec2 = new Payreqrec();
	private Subcoderec subcoderec = new Subcoderec();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit1090, checkDestination2060, checkForErrors2080, exit2090, exit4090
	}

	public Pd5hc() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5hc", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams,
				parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea,
				parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams,
				parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea,
				parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	 * ***   Initialise fields for showing on screen.
	 * </pre>
	 */
	protected void initialise1000() {
		try {
			initialise1010();
		} catch (GOTOException e) {
		}
	}

	protected void initialise1010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.refcode.set(SPACES);
		String chdrnum = "";
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf!= null ){
			 chdrnum = chdrpf.getChdrnum();
		}else{
			chdrnum = wsspcomn.chdrChdrnum.toString();
		}
	     	chdrmjaIO.setChdrcoy(wsspcomn.company);
			chdrmjaIO.setChdrnum(chdrnum);
			chdrmjaIO.setFormat(chdrmjarec);
			chdrmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
		
		sv.ownername.set(plainname(chdrmjaIO.getCownnum().toString()));
		regppf = new Regppf(); 
		regppf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		regppf = regpDAO.getRegpRecord(regppf);
		if (regppf != null) {
			 if(isEQ(wsspcomn.sbmaction,"A")){
				 sv.rgpymop.set(regppf.getWdrgpymop());
			 }
			 moveRegppfToRegt();
		}
		String action = wsspcomn.sbmaction.toString();
		if (isNE(action,"E")){
		sv.rgpymopOut[varcom.pr.toInt()].set("Y"); 
		}
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItmfrm(new BigDecimal(getCurrentBusinessDate()));
		itempf.setItmto(new BigDecimal(getCurrentBusinessDate()));
		itempf.setItemtabl("TD5H7");
		itempf.setItemitem(chdrmjaIO.getCnttype().toString());
		itempf.setValidflag("1");
		List<Itempf> itempfList  = itemDAO.findByItemDates(itempf);
		if(itempfList!=null && !itempfList.isEmpty()){
		td5h7rec.td5h7Rec.set(StringUtil.rawToString( itempfList.get(0).getGenarea()));
		}
		sv.ddind.clear();
		sv.intRate.set(td5h7rec.intrate);
		sv.intcalfreq.set(td5h7rec.intcalfreq);
		sv.intcapfreq.set(td5h7rec.intcapfreq);
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cownnum.set(chdrmjaIO.getCownnum());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.currcd.set(chdrmjaIO.getCntcurr());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.register.set(chdrmjaIO.getRegister());
		sv.chdrstatus.set(descDao.getdescData("IT", "T3623",
				chdrmjaIO.getStatcode().toString(),
				wsspcomn.company.toString(), wsspcomn.language.toString())
				.getLongdesc());
		sv.premstatus.set(descDao.getdescData("IT", "T3588",
				chdrmjaIO.getPstatcode().toString(),
				wsspcomn.company.toString(), wsspcomn.language.toString())
				.getShortdesc());
		sv.ctypedes.set(descDao.getdescData("IT", t5688,
				chdrmjaIO.getCnttype().toString(), wsspcomn.company.toString(),
				wsspcomn.language.toString()).getShortdesc());

		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5645","PD5HC");
		if(itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
			acblenqListLPAP = acblpfDAO.getAcblenqRecord(
					wsspcomn.company.toString(), StringUtils.rightPad(chdrmjaIO.getChdrnum().toString(), 16),t5645rec.sacscode[1].toString(),t5645rec.sacstype[1].toString());
	
			acblenqListLPAN =acblpfDAO.getAcblenqRecord(
					wsspcomn.company.toString(), StringUtils.rightPad(chdrmjaIO.getChdrnum().toString(), 16),t5645rec.sacscode[2].toString(),t5645rec.sacstype[2].toString());
			
			if (acblenqListLPAP != null && !acblenqListLPAP.isEmpty()) {
					accAmtLPAP = acblenqListLPAP.get(0).getSacscurbal();
					accAmtLPAP = new BigDecimal(ZERO.toString()).subtract(accAmtLPAP);		            
				
			}
			if (acblenqListLPAN != null &&    !acblenqListLPAN.isEmpty()) {
				accAmtLPAN =  acblenqListLPAN.get(0).getSacscurbal();				
				accAmtLPAN = new BigDecimal(ZERO.toString()).subtract(accAmtLPAN);
			}
			sv.accamnt.set(add(accAmtLPAP,accAmtLPAN));
		} else {
			syserrrec.params.set("t5645:" + wsaaProg);
			fatalError600();
		}
		
		chdrpf = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), chdrmjaIO
				.getChdrnum().toString());
		if (chdrpf != null) {
			sv.lifcnum.set(chdrpf.getCownnum());
			sv.linsname.setLeft(plainname(chdrpf.getCownnum()).toString());
			}
		chdrTranno = chdrmjaIO.tranno.toInt() + 1; 
	}



	protected StringBuilder plainname(String clntnum) {
		/* PLAIN-100 */
		clntpf = clntpfDAO.findClientByClntnum(clntnum);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
		stringVariable1.append(", ");
		stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
		
		return stringVariable1 ;
	}




	/**
	 * <pre>
	 *  END OF CONFNAME **********************************************
	 *     Retrieve screen fields and exit.
	 * </pre>
	 */
	protected void preScreenEdit() {
		preStart();
	}

	protected void preStart() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}

		/* If a lump sum payment is being processed, protect the payment */
		/* amount, the percentage,the frequency and also the last */
		/* payment date. */
	}

	protected void screenEdit2000() {
		try {
			screenIo2010();
			validate2020();					
			checkBankDetails2035();
			checkForErrors();
		} 
		catch (GOTOException e) {
		
		}					
	}

	protected void screenIo2010() {
		/* CALL 'S5222IO' USING SCRN-SCREEN-PARAMS */
		/* S5222-DATA-AREA. */
		/* Screen errors are now handled in the calling program. */
		/* PERFORM 200-SCREEN-ERRORS. */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			checkForErrors();
		}
		if (isNE(scrnparams.statuz, varcom.oK)) {
			wsspcomn.edterror.set("Y");
			if (isNE(scrnparams.statuz, varcom.calc)) {
				scrnparams.errorCode.set(errorsInner.curs);
				checkForErrors();
			}
		}
		wsspcomn.chdrCownnum.set(chdrpf.getCownnum());/* IJTI-1523 */
		wsspcomn.chdrClntpfx.set("CN");
		wsspcomn.chdrClntcoy.set(wsspcomn.fsuco);
	}

	protected void validate2020() {
		if(isEQ(wsspcomn.sbmaction,"E")){
			checkPayment2030();
			if ((sv.rgpymop.toString().equalsIgnoreCase("B"))
					|| (sv.rgpymop.toString().equalsIgnoreCase("C"))) {
			} else {
				sv.ddindErr.set(errorsInner.rrfp);
				checkForErrors();
			}
	
			
			/* Validate fields. */
			
			if ((isNE(sv.ddind, "+") && isNE(sv.ddind, "X") && isNE(sv.ddind,
					SPACES))) {
				sv.ddindErr.set(errorsInner.h118);
				wsspcomn.edterror.set("Y");
			}			
		}
		if (isEQ(wsspcomn.flag, "I")) {
			if ((isEQ(sv.ddind, "X") || isEQ(sv.ddind.getFormData(), "X")  && isEQ(regtvstIO.getBankkey(), SPACES))) {
				sv.ddindErr.set(errorsInner.e493);
				wsspcomn.edterror.set("Y");
				sv.ddind.clear();
				sv.ddind.setFormData("");
			}
			checkForErrors();
		}
	}
	
	protected void checkPayment2030() {
		
		if (isEQ(sv.rgpymop, SPACES) &&  isEQ(wsspcomn.sbmaction,"E")) {
			sv.rgpymopErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			checkForErrors();
		}
		
	}

	protected void checkBankDetails2035()
    {
          
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItmfrm(wsspcomn.currfrom.getbigdata());
		itempf.setItmto(wsspcomn.currfrom.getbigdata());
		itempf.setItemtabl(t6694);
		itempf.setItemitem(sv.rgpymop.toString());
		itempf.setValidflag("1");
		List<Itempf> itempfList  = itemDAO.findByItemDates(itempf);
		if(itempfList!=null && !itempfList.isEmpty()){
			t6694rec.t6694Rec.set(StringUtil.rawToString( itempfList.get(0).getGenarea()));
		}else{
			if(isNE(wsspcomn.sbmaction,"A") && isNE(wsspcomn.flag, "I")){
			 sv.rgpymopErr.set(errorsInner.g529);
             wsspcomn.edterror.set("Y");
             checkForErrors();
			}
		}

           setBankIndicator2200();
           
    }

	protected void checkForErrors() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			 goTo(GotoLabel.exit2090);
		}
	}


	protected void setBankIndicator2200() {		
		if (isNE(sv.ddindErr, SPACES)) {
			return;
		}
		if (isEQ(t6694rec.bankreq, "Y")) {
			if ((isEQ(regtvstIO.getBankkey(), SPACES))
					|| (isEQ(regtvstIO.getBankacckey(), SPACES))) {
				sv.ddind.set("X");
			} else {
				if (isNE(sv.ddind, "X")) {
					sv.ddind.set("+");
				}
			}
		} else {
			if (isEQ(sv.ddind, "X")) {
				sv.ddindErr.set(errorsInner.e570);
				sv.ddind.set(SPACES);
			} else {
				regtvstIO.setBankkey(SPACES);
				regtvstIO.setBankacckey(SPACES);
			}
		}
	}


	protected void update3000() {
		/* UPDATE-DATABASE */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return;
		}
		if (isEQ(wsspcomn.flag, "I") || isEQ(sv.ddind, "X")) {
			return;
		}
		if(isEQ(wsspcomn.sbmaction,"E")){
			updateRegt3100();
		}
		
	}

	protected void updateRegt3100() {
		
		checkPopup();
		if(isEQ(sv.ansrepy, "N") || isEQ(sv.ansrepy, "X") || isEQ(sv.ansrepy, SPACES)) {
			return;
		}
		update3110();		 
	}

	protected void checkPopup() {				
			if(isEQ(wsspcomn.sbmaction , "E"))
				sv.refcode.set("1");
	}	

	protected void update3110() {		
		regppfList = new ArrayList<>();
		updateValidFlagRegppf();
		//insertRegppf();
		insertRegPayDetails();
	    writePtrn3900();
	    updateChdrRecord3020(); 
	    postings360();
	    if(isEQ(t6694rec.payeereq, "Y"))
	    callPayreq380();
	    
	}

	protected void whereNext4000() {
			try {
				if(isEQ(wsspcomn.sbmaction,"E") || isEQ(wsspcomn.sbmaction,"A")){
					nextProgram4000();	
				} else {
					nextProgram4030();
				}
			} catch (GOTOException e) {
		}
	}

	protected void nextProgram4000() {
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1) {
				restoreProgram4100();
			}
		}
		if (isEQ(sv.ddind, "X")) {
			regtvstIO.setFunction(varcom.keeps);
			regtvstIO.setFormat(formatsInner.regtvstrec);
			SmartFileCode.execute(appVars, regtvstIO);
			if (isNE(regtvstIO.getStatuz(), varcom.oK)) {
				 syserrrec.params.set(regtvstIO.getParams());
				 fatalError600();
			}
			popUp4010();
			genssw4020();
			return;
		} else {
			if (isEQ(sv.ddind, "?")) {
				checkBankDetails4400();
			}
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		} else {
			if(isEQ(wsspcomn.sbmaction , "E")){
				if(isEQ(sv.ansrepy, "Y") || isEQ(sv.ansrepy, "N") || isEQ(sv.ansrepy, "X"))
				{					
					unlkSoftLock3270();
					regtvstIO.setFunction(varcom.rlse);
					SmartFileCode.execute(appVars, regtvstIO);
					if (isNE(regtvstIO.getStatuz(), varcom.oK)) {
						 syserrrec.params.set(regtvstIO.getParams());
						 fatalError600();
					}
					wsspcomn.programPtr.add(1);						
				}
				else{
					wsspcomn.nextprog.set(scrnparams.scrname);
				}
				
			}
			else if(isEQ(wsspcomn.sbmaction , "A")){
				wsspcomn.programPtr.add(1);		
			}
			else{
				unlkSoftLock3270();	
				regtvstIO.setFunction(varcom.rlse);
				SmartFileCode.execute(appVars, regtvstIO);
				if (isNE(regtvstIO.getStatuz(), varcom.oK)) {
					 syserrrec.params.set(regtvstIO.getParams());
					 fatalError600();
				}
				wsspcomn.programPtr.add(1);				
			}
		}	
	}

	protected void popUp4010() {
		gensswrec.function.set(SPACES);
		if ((isEQ(sv.ddind, "X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1) {
				saveProgramStack4200();
			}
			sv.ddind.set("?");
			gensswrec.function.set("A");
		}
	}

	protected void genssw4020() {
		if (isEQ(gensswrec.function, SPACES)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz, varcom.oK))
				&& (isNE(gensswrec.statuz, varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1) {
			loadProgramStack4300();
		}
		nextProgram4030();
	}

	protected void nextProgram4030() {
		wsspcomn.programPtr.add(1);
	}

	protected void restoreProgram4100() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void saveProgramStack4200() {
		/* PARA */
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void loadProgramStack4300() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void checkBankDetails4400() {		
		regtvstIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regtvstIO);
		if (isNE(regtvstIO.getStatuz(), varcom.oK)) {
			 syserrrec.params.set(regtvstIO.getParams());
			 fatalError600();
		}
		if ((isEQ(regtvstIO.getBankkey(), SPACES))
				&& (isEQ(regtvstIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		} else {
			sv.ddind.set("+");
		}
	}

	protected void writePtrn3900() {

		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		ptrnpf.setTranno(chdrTranno);
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		wsaaBatckey1.set(wsspcomn.batchkey);
		ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey1.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey1.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
		List<Ptrnpf> list = new ArrayList<Ptrnpf>();
		list.add(ptrnpf);
		if (!ptrnpfDAO.insertPtrnPF(list)) {
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString()
					.concat(chdrmjaIO.getChdrnum().toString()));
			fatalError600();
		}
	}

	/*
	 * Class transformed from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner {
		/* ERRORS */
		private FixedLengthStringData e186 = new FixedLengthStringData(4)
				.init("E186");
		private FixedLengthStringData e304 = new FixedLengthStringData(4)
				.init("E304");
		private FixedLengthStringData e355 = new FixedLengthStringData(4)
				.init("E355");
		private FixedLengthStringData e493 = new FixedLengthStringData(4)
				.init("E493");
		private FixedLengthStringData e570 = new FixedLengthStringData(4)
				.init("E570");
		private FixedLengthStringData g529 = new FixedLengthStringData(4)
				.init("G529");
		private FixedLengthStringData h118 = new FixedLengthStringData(4)
				.init("H118");
		private FixedLengthStringData h093 = new FixedLengthStringData(4)
				.init("H093");
		private FixedLengthStringData curs = new FixedLengthStringData(4)
				.init("CURS");
		private FixedLengthStringData rrfp = new FixedLengthStringData(4)
				.init("RRFP");
	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		private FixedLengthStringData itemrec = new FixedLengthStringData(10)
				.init("ITEMREC   ");
		private FixedLengthStringData itdmrec = new FixedLengthStringData(10)
				.init("ITEMREC   ");
		private FixedLengthStringData regtvstrec = new FixedLengthStringData(10)
				.init("REGTVSTREC");
	}
	
	protected void unlkSoftLock3270(){
		/*    Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		/*  MOVE 'T703'                 TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	} 
	
	protected void updateValidFlagRegppf(){
		regppf.setTranno(chdrTranno);
		regppf.setWdrgpymop(sv.rgpymop.toString());
		if(sv.rgpymop.toString().equalsIgnoreCase("B")){
			 readBankDetailsFromRegt(regppf);
		 }
		regppfList.add(regppf);
		regpDAO.updateRegpWDDet(regppfList);
		regppfList.clear();
	} 
	/*protected void insertRegppf(){
		Regppf insregp = new Regppf (regppf); 
		insregp.setTranno(chdrTranno);
		insregp.setValidflag("1");
		insregp.setWdrgpymop(sv.rgpymop.toString());
		if(sv.rgpymop.toString().equalsIgnoreCase("B")){
			 readBankDetailsFromRegt(insregp);
		 }
 		regppfList.add(insregp);
		regpDAO.insertRegppfRecord(regppfList);
	} */
	
	protected void updateChdrRecord3020(){
		chdrpf = new Chdrpf();
		chdrBulkUpdtList = new ArrayList<>();
		chdrpf.setUniqueNumber(chdrmjaIO.unique_number.toLong());
		chdrpf.setTranno(chdrTranno);
		chdrBulkUpdtList.add(chdrpf);		
		chdrpfDAO.updateChdrTrannoByUniqueNo(chdrBulkUpdtList);
	} 
	
protected void postings360()
	{
		
		lifacmvrec1.lifacmvRec.set(SPACES);
		wsaaRldgacct.set(SPACES);
		wsaaSequenceNo.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		/* Read T1688 for transaction code description.                 */
		descpf=descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if(descpf!=null)
		lifacmvrec1.trandesc.set(descpf.getLongdesc());
		wsaaBatckey.set(wsspcomn.batchkey);
		lifacmvrec1.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.rdocnum.set(regppf.getChdrnum());
		compute(lifacmvrec1.tranno, 0).set(chdrTranno);
		lifacmvrec1.rldgcoy.set(regppf.getChdrcoy());
		lifacmvrec1.origcurr.set(regppf.getCurrcd());
		lifacmvrec1.tranref.set(regppf.getChdrnum());
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.genlcoy.set(regppf.getChdrcoy());
		lifacmvrec1.genlcur.set(regppf.getCurrcd());
		lifacmvrec1.effdate.set(datcon1rec.intDate.toInt());
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(varcom.vrcmUser);
		lifacmvrec1.termid.set(varcom.vrcmTermid);
		lifacmvrec1.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec1.substituteCode[6].set(regppf.getCrtable());
		/* For component level accounting, the whole entity field       */
		/* LIFA-RLDGACCT must be filled.  For contract level accounting */
		/* only the contract header no. is needed in the entity field   */
		/* LIFA-RLDGACCT.                                               */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			wsaaPlan.set(regppf.getPlanSuffix());
			wsaaRldgLife.set(regppf.getLife());
			wsaaRldgCoverage.set(regppf.getCoverage());
			wsaaRldgRider.set(regppf.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
		}
		lifacmvrec1.rldgacct.set(regppf.getChdrnum());
		lifacmvrec1.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec1.glcode.set(t5645rec.glmap[1]);
		lifacmvrec1.glsign.set(t5645rec.sign[1]);
		lifacmvrec1.contot.set(t5645rec.cnttot[1]);
		lifacmvrec1.origamt.set(accAmtLPAP); // lp Ap only  
		lifacmvrec1.acctamt.set(accAmtLPAP);

		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			return ; 
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[2]);
		lifacmvrec1.glcode.set(t5645rec.glmap[2]);
		lifacmvrec1.glsign.set(t5645rec.sign[2]);
		lifacmvrec1.contot.set(t5645rec.cnttot[2]);
		lifacmvrec1.origamt.set(accAmtLPAN);
		lifacmvrec1.acctamt.set(accAmtLPAN);

		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
		}
		
		if(isEQ(sv.rgpymop, "B")){
			lifacmvrec1.sacscode.set(t5645rec.sacscode[3]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[3]);
			lifacmvrec1.glcode.set(t5645rec.glmap[3]);
			lifacmvrec1.glsign.set(t5645rec.sign[3]);
			lifacmvrec1.contot.set(t5645rec.cnttot[3]);
		}else if (isEQ(sv.rgpymop, "C")) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode[4]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[4]);
			lifacmvrec1.glcode.set(t5645rec.glmap[4]);
			lifacmvrec1.glsign.set(t5645rec.sign[4]);
			lifacmvrec1.contot.set(t5645rec.cnttot[4]);
		}
		
		lifacmvrec1.origamt.set(sv.accamnt);   
		lifacmvrec1.acctamt.set(sv.accamnt);
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			return ; 
		}
	}

protected void callPayreq380()
{
	payreqrec2.rec.set(SPACES);
	/* Read table T3629 in order to obtain the bankcode            *   */
	Itempf itempf = new Itempf();
	itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), t3629,regppf.getCurrcd());
	if(itempf != null) {
		t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	payreqrec2.bankcode.set(t3629rec.bankcode);
	payreqrec2.batckey.set(wsspcomn.batchkey);
	payreqrec2.frmRldgacct.set(SPACES);
	payreqrec2.frmRldgacct.set(regppf.getChdrnum());
	subcoderec.codeRec.set(SPACES);
	subcoderec.code[1].set(chdrmjaIO.getCnttype());
	if(isEQ(sv.rgpymop, "B")){
		subcoderec.glcode.set(t5645rec.glmap[3]);
		subcode();
		payreqrec2.glcode.set(subcoderec.glcode);
		payreqrec2.sacscode.set(t5645rec.sacscode[3]);
		payreqrec2.sacstype.set(t5645rec.sacstype[3]);
		payreqrec2.sign.set(t5645rec.sign[3]);
		payreqrec2.cnttot.set(t5645rec.cnttot[3]);
	}
	if(isEQ(sv.rgpymop, "C")){
		subcoderec.glcode.set(t5645rec.glmap[4]);
		subcode();
		payreqrec2.glcode.set(subcoderec.glcode);
		payreqrec2.sacscode.set(t5645rec.sacscode[4]);
		payreqrec2.sacstype.set(t5645rec.sacstype[4]);
		payreqrec2.sign.set(t5645rec.sign[4]);
		payreqrec2.cnttot.set(t5645rec.cnttot[4]);
	}
	payreqrec2.effdate.set(datcon1rec.intDate.toInt());
	payreqrec2.paycurr.set(regppf.getCurrcd());
	payreqrec2.pymt.set(accAmtLPAP);
	payreqrec2.termid.set(varcom.vrcmTermid.toString());
	payreqrec2.user.set(varcom.vrcmUser.toInt());
	payreqrec2.reqntype.set(t6694rec.reqntype);
	payreqrec2.clntcoy.set(regtvstIO.getPaycoy());
	payreqrec2.clntnum.set(regtvstIO.getPayclt());
	if (isNE(regtvstIO.getBankkey(), SPACES)) {
		payreqrec2.bankkey.set(regtvstIO.getBankkey());
		payreqrec2.bankacckey.set(regtvstIO.getBankacckey());
	}
	payreqrec2.zbatctrcde.set(wsaaBatckey.batcBatctrcde);			
	payreqrec2.tranref.set(regppf.getChdrnum());
	payreqrec2.language.set(wsspcomn.language.toString());
	payreqrec2.function.set("REQN");
	callProgram(Payreq.class, payreqrec2.rec);
	if (isNE(payreqrec2.statuz, varcom.oK)) {
		syserrrec.statuz.set(payreqrec2.statuz);
	}
}

protected void subcode()
{
	sbcd100Start();
}

protected void sbcd100Start()
{
	if (isNE(subcoderec.code[1], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[1]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[2], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[2]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[3], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[3]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[4], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[4]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[5], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[5]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
	}
	if (isNE(subcoderec.code[6], SPACES)) {
		subcoderec.substituteCode.set(subcoderec.code[6]);
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
		subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
	}
}

protected void a000CallRounding()
{
	/*A100-CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(wsspcomn.company);
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(regppf.getCurrcd());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
	}
	/*A000-EXIT*/
}
protected void moveRegppfToRegt(){
    regtvstIO.setChdrnum(regppf.getChdrnum());
    regtvstIO.setChdrcoy(regppf.getChdrcoy());
    regtvstIO.setLife(regppf.getLife());
    regtvstIO.setCoverage(regppf.getCoverage());
    regtvstIO.setRider(regppf.getRider());
    regtvstIO.setGlact(regppf.getGlact());
    regtvstIO.setDebcred(regppf.getDebcred());
    regtvstIO.setDestkey(regppf.getDestkey()); 
    regtvstIO.setPaycoy(regppf.getPaycoy());
    regtvstIO.setPayclt(chdrmjaIO.getCownnum());
    regtvstIO.setRgpymop(regppf.getRgpymop());
    regtvstIO.setRegpayfreq(regppf.getRegpayfreq());
    regtvstIO.setCurrcd(regppf.getCurrcd());
    regtvstIO.setPymt(regppf.getPymt());
    regtvstIO.setPrcnt(regppf.getPrcnt());
    regtvstIO.setTotamnt(regppf.getTotamnt());
    regtvstIO.setPayreason(regppf.getPayreason());
    regtvstIO.setClaimevd(regppf.getClaimevd());
    regtvstIO.setCrtdate(regppf.getCrtable());
    regtvstIO.setFirstPaydate(regppf.getFirstPaydate());
    regtvstIO.setRevdte(regppf.getRevdte());
    regtvstIO.setFinalPaydate(regppf.getFinalPaydate());
    regtvstIO.setAnvdate(regppf.getAnvdate());
    regtvstIO.setRgpytype(regppf.getRgpytype());
    regtvstIO.setCrtable(regppf.getCrtable());
    regtvstIO.setPlanSuffix(regppf.getPlanSuffix());
    if(isEQ(wsspcomn.sbmaction,"A")){
    regtvstIO.setBankacckey(regppf.getWdbankacckey());
    regtvstIO.setBankkey(regppf.getWdbankkey());
    
    }
    regtvstIO.setFormat(regtvstrec);
    
    regtvstIO.setFunction(varcom.keeps);
    SmartFileCode.execute(appVars, regtvstIO);
    if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
           regtvstIO.setParams(regtvstIO.getParams());
           fatalError600();
    }
    
}
protected void readBankDetailsFromRegt(Regppf insregp){
	regtvstIO.setFunction("RETRV");
	SmartFileCode.execute(appVars, regtvstIO);
	if (isNE(regtvstIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(regtvstIO.getParams());
		fatalError600();
	}
	// new clouman of method of payment and bank Key and code 
	insregp.setWdbankkey(regtvstIO.getBankkey().toString());
	insregp.setWdbankacckey(regtvstIO.getBankacckey().toString());
} 
public  Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   }

public void insertRegPayDetails(){
	
    List<Rgpdetpf> rgpdetpfList;
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(regppf.getChdrnum());
    rgpdetpfList = rgpdetpfDAO.searchRpdetpfRecord(chdrnumList, regppf.getChdrcoy());
    
    if(rgpdetpfList!= null && !rgpdetpfList.isEmpty()){
    	for(Rgpdetpf Rgpdetpftmp : rgpdetpfList){
    		Rgpdetpftmp.setApcaplamt(new BigDecimal(0));
    		Rgpdetpftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
    		Rgpdetpftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
    		Rgpdetpftmp.setApintamt(BigDecimal.ZERO);
    		Rgpdetpftmp.setValidflag("2");
    	}
     }
    rgpdetpfDAO.updateRegpRecCapDate(rgpdetpfList);
}

}
