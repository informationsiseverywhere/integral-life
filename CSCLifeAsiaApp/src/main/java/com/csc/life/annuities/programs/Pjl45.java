package com.csc.life.annuities.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.annuities.screens.Sjl45ScreenVars;
import com.csc.life.annuities.tablestructures.Tjl45rec;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl45 extends ScreenProgCS{

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl45.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl45");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO",ItempfDAO.class);
	private Itmdkey wsaaItmdkey = new Itmdkey();
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	
	private Tjl45rec tjl45rec = new Tjl45rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl45ScreenVars sv = ScreenProgram.getScreenVars(Sjl45ScreenVars.class);
	Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	
	public Pjl45() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl45", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			LOGGER.info("mainline {}", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			LOGGER.info("mainline {}", e);
		}
	}
	
	@Override
	protected void initialise1000() {

		initialise1010();
    }

	protected void initialise1010()	{
		
			
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItmdkey.set(wsspsmart.itmdkey);
		wsaaItemcoy = wsaaItmdkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItmdkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItmdkey.itemItemtabl.toString();
		Integer wsaaItemfrm = wsaaItmdkey.itemItmfrm.toInt();
		String wsaaItemlang = wsspcomn.language.toString();
		sv.company.set(wsaaItemcoy);
		sv.tabl.set(wsaaItemtabl);
		sv.item.set(wsaaItemitem);
		Descpf descpf=descDAO.getdescData("IT", wsaaItemtabl, wsaaItemitem, wsaaItemcoy, wsaaItemlang);
		if (descpf==null) {
			fatalError600();
		}
		else{
		sv.longdesc.set(descpf.getLongdesc());
		}
		Itempf itempf = itemDAO.findItempfByItemFrmDate("IT", wsaaItemcoy, wsaaItemtabl, wsaaItemitem, wsaaItemfrm);
		
		if (itempf == null) {
			return;
		} else {
			tjl45rec.tjl45Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}

		if (isEQ(itempf.getItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		} else {
			sv.itmfrm.set(itempf.getItmfrm());
		}
		if (isEQ(itempf.getItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		} else {
			sv.itmto.set(itempf.getItmto());
		}
		
		sv.mtoi.set(tjl45rec.mtoi);
			
		
		
		
	}
	
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(Varcom.prot);
		}
	}

	@SuppressWarnings("static-access")
	@Override
	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I") || isNE(sv.itmfrmErr, SPACES)) {
			return;
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	@Override
	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.mtoi, tjl45rec.mtoi)) {
			tjl45rec.mtoi.set(sv.mtoi);
			Itempf itempf = new Itempf();
			itempf.setGenarea(tjl45rec.tjl45Rec.toString().getBytes());
			itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
					.getGENAREAJString(tjl45rec.tjl45Rec.toString().getBytes(), tjl45rec));
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemtabl(wsaaItemtabl);
			itempf.setItemcoy(wsaaItemcoy);
			itempf.setItemitem(wsaaItemitem);
			itempf.setItmfrm(Integer.parseInt(sv.itmfrm.toString()));
			itemDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM, ITMFRM");
		}
	}
	
	@Override
	protected void whereNext4000() {
		wsspcomn.nextprog.set(wsaaProg); 
		wsspcomn.programPtr.add(1);
	}
}

