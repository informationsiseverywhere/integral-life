package com.csc.life.annuities.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: VstlpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:57
 * Class transformed from VSTLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class VstlpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 387+DD.cltaddr.length*5; //vdivisala ILIFE-3212
	public FixedLengthStringData vstlrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData vstlpfRecord = vstlrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(vstlrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(vstlrec);
	public FixedLengthStringData salut = DD.salut.copy().isAPartOf(vstlrec);
	public FixedLengthStringData surname = DD.surname.copy().isAPartOf(vstlrec);
	public FixedLengthStringData givname = DD.givname.copy().isAPartOf(vstlrec);
	public FixedLengthStringData initials = DD.initials.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(vstlrec);
	public FixedLengthStringData crtable01 = DD.crtable.copy().isAPartOf(vstlrec);
	public FixedLengthStringData crtable02 = DD.crtable.copy().isAPartOf(vstlrec);
	public FixedLengthStringData crtable03 = DD.crtable.copy().isAPartOf(vstlrec);
	public FixedLengthStringData crtable04 = DD.crtable.copy().isAPartOf(vstlrec);
	public FixedLengthStringData crtable05 = DD.crtable.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingAmount01 = DD.vstamnt.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingAmount02 = DD.vstamnt.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingAmount03 = DD.vstamnt.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingAmount04 = DD.vstamnt.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingAmount05 = DD.vstamnt.copy().isAPartOf(vstlrec);
	public FixedLengthStringData descrip01 = DD.descrip.copy().isAPartOf(vstlrec);
	public FixedLengthStringData descrip02 = DD.descrip.copy().isAPartOf(vstlrec);
	public FixedLengthStringData descrip03 = DD.descrip.copy().isAPartOf(vstlrec);
	public FixedLengthStringData descrip04 = DD.descrip.copy().isAPartOf(vstlrec);
	public FixedLengthStringData descrip05 = DD.descrip.copy().isAPartOf(vstlrec);
	public FixedLengthStringData currcd01 = DD.currcd.copy().isAPartOf(vstlrec);
	public FixedLengthStringData currcd02 = DD.currcd.copy().isAPartOf(vstlrec);
	public FixedLengthStringData currcd03 = DD.currcd.copy().isAPartOf(vstlrec);
	public FixedLengthStringData currcd04 = DD.currcd.copy().isAPartOf(vstlrec);
	public FixedLengthStringData currcd05 = DD.currcd.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingDate01 = DD.vstdate.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingDate02 = DD.vstdate.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingDate03 = DD.vstdate.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingDate04 = DD.vstdate.copy().isAPartOf(vstlrec);
	public PackedDecimalData vestingDate05 = DD.vstdate.copy().isAPartOf(vstlrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(vstlrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(vstlrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cowncoy = DD.cowncoy.copy().isAPartOf(vstlrec);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(vstlrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public VstlpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for VstlpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public VstlpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for VstlpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public VstlpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for VstlpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public VstlpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("VSTLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"SALUT, " +
							"SURNAME, " +
							"GIVNAME, " +
							"INITIALS, " +
							"CLTADDR01, " +
							"CLTADDR02, " +
							"CLTADDR03, " +
							"CLTADDR04, " +
							"CLTADDR05, " +
							"CLTPCODE, " +
							"CRTABLE01, " +
							"CRTABLE02, " +
							"CRTABLE03, " +
							"CRTABLE04, " +
							"CRTABLE05, " +
							"VSTAMNT01, " +
							"VSTAMNT02, " +
							"VSTAMNT03, " +
							"VSTAMNT04, " +
							"VSTAMNT05, " +
							"DESCRIP01, " +
							"DESCRIP02, " +
							"DESCRIP03, " +
							"DESCRIP04, " +
							"DESCRIP05, " +
							"CURRCD01, " +
							"CURRCD02, " +
							"CURRCD03, " +
							"CURRCD04, " +
							"CURRCD05, " +
							"VSTDATE01, " +
							"VSTDATE02, " +
							"VSTDATE03, " +
							"VSTDATE04, " +
							"VSTDATE05, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"COWNCOY, " +
							"COWNNUM, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     salut,
                                     surname,
                                     givname,
                                     initials,
                                     cltaddr01,
                                     cltaddr02,
                                     cltaddr03,
                                     cltaddr04,
                                     cltaddr05,
                                     cltpcode,
                                     crtable01,
                                     crtable02,
                                     crtable03,
                                     crtable04,
                                     crtable05,
                                     vestingAmount01,
                                     vestingAmount02,
                                     vestingAmount03,
                                     vestingAmount04,
                                     vestingAmount05,
                                     descrip01,
                                     descrip02,
                                     descrip03,
                                     descrip04,
                                     descrip05,
                                     currcd01,
                                     currcd02,
                                     currcd03,
                                     currcd04,
                                     currcd05,
                                     vestingDate01,
                                     vestingDate02,
                                     vestingDate03,
                                     vestingDate04,
                                     vestingDate05,
                                     userProfile,
                                     jobName,
                                     datime,
                                     cowncoy,
                                     cownnum,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		salut.clear();
  		surname.clear();
  		givname.clear();
  		initials.clear();
  		cltaddr01.clear();
  		cltaddr02.clear();
  		cltaddr03.clear();
  		cltaddr04.clear();
  		cltaddr05.clear();
  		cltpcode.clear();
  		crtable01.clear();
  		crtable02.clear();
  		crtable03.clear();
  		crtable04.clear();
  		crtable05.clear();
  		vestingAmount01.clear();
  		vestingAmount02.clear();
  		vestingAmount03.clear();
  		vestingAmount04.clear();
  		vestingAmount05.clear();
  		descrip01.clear();
  		descrip02.clear();
  		descrip03.clear();
  		descrip04.clear();
  		descrip05.clear();
  		currcd01.clear();
  		currcd02.clear();
  		currcd03.clear();
  		currcd04.clear();
  		currcd05.clear();
  		vestingDate01.clear();
  		vestingDate02.clear();
  		vestingDate03.clear();
  		vestingDate04.clear();
  		vestingDate05.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		cowncoy.clear();
  		cownnum.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getVstlrec() {
  		return vstlrec;
	}

	public FixedLengthStringData getVstlpfRecord() {
  		return vstlpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setVstlrec(what);
	}

	public void setVstlrec(Object what) {
  		this.vstlrec.set(what);
	}

	public void setVstlpfRecord(Object what) {
  		this.vstlpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(vstlrec.getLength());
		result.set(vstlrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}