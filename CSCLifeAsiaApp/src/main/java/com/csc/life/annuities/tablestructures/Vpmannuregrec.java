package com.csc.life.annuities.tablestructures;


import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class Vpmannuregrec extends ExternalData {	
    // *******************************
	// Attribute Declarations
	// *******************************
	public FixedLengthStringData statuz = new FixedLengthStringData(8);
	public PackedDecimalData transEffDate = new PackedDecimalData(8, 0);	
	public FixedLengthStringData cnttype = new FixedLengthStringData(3);	
	public FixedLengthStringData callingSys = new FixedLengthStringData(4);	
	public FixedLengthStringData region = new FixedLengthStringData(2);
  	public FixedLengthStringData locale = new FixedLengthStringData(2);
  	public FixedLengthStringData language = new FixedLengthStringData(1);	
	public FixedLengthStringData annpmntopt = new FixedLengthStringData(2);	
	public PackedDecimalData totannfund = new PackedDecimalData(17, 2);	
	public PackedDecimalData annpmnttrm = new PackedDecimalData(3, 0);	
	public FixedLengthStringData annpmntfreq = new FixedLengthStringData(2);
	public PackedDecimalData annpartpmnt = new PackedDecimalData(17, 2);	
	public PackedDecimalData outputannpmntamt = new PackedDecimalData(17, 2);	

	@Override
	public void initialize() {
		COBOLFunctions.initialize(statuz);
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
