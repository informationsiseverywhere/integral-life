package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sjl45
 */
public class Sjl45ScreenVars extends SmartVarModel { 
	private static final long serialVersionUID = 1L; 

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,55);
	public ZonedDecimalData mtoi = DD.minTOIncome.copyToZonedDecimal().isAPartOf(dataFields,60);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize())
														.isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData mtoiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize())
													.isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] mtoiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sjl45screenWritten = new LongData(0);
	public LongData Sjl45protectWritten = new LongData(0);
	
	@Override
	public boolean hasSubfile() {
		return false;
	}


	public Sjl45ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	@Override
	public final void initialiseScreenVars() {
		fieldIndMap.put(mtoiOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = getScrnFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl45screen.class;
		protectRecord = Sjl45protect.class;
	}
		
	public BaseData[] getScrnFields()
	{
		
		return new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, mtoi};
		
	}
	
	public BaseData[][] getscreenOutFields()
	{
		
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, mtoiOut};
		
	}
	
	public BaseData[] getscreenErrFields()
	{
		
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, mtoiErr};
		
	}
	
	public final int getDataAreaSize()
	{
		return 187;
	}
	
	public final int getDataFieldsSize()
	{
		return 75;
	}
	public final int getErrorIndicatorSize()
	{
		return 28;
	}
	public final int getOutputFieldSize()
	{
		return 84;
	}

}
