package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sd5hfScreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5hfScreenVars sv = (Sd5hfScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5hfscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5hfScreenVars screenVars = (Sd5hfScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.rgpytypesd.setClassString("");
		screenVars.regpayfreq.setClassString("");
		screenVars.frqdesc.setClassString("");
		screenVars.destkey.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.cltype.setClassString("");
		screenVars.clmdesc.setClassString("");
		screenVars.claimevd.setClassString("");
		screenVars.rgpymop.setClassString("");
		screenVars.payclt.setClassString("");
		screenVars.payenme.setClassString("");
		screenVars.claimcur.setClassString("");
		screenVars.anvdateDisp.setClassString("");
		screenVars.finalPaydateDisp.setClassString("");
		screenVars.rgpyshort.setClassString("");
		screenVars.clmcurdsc.setClassString("");
		screenVars.currds.setClassString("");
		screenVars.rgpynum.setClassString("");
		screenVars.pymt.setClassString("");
		screenVars.totalamt.setClassString("");
		screenVars.crtdateDisp.setClassString("");
		screenVars.firstPaydateDisp.setClassString("");
		screenVars.prcnt.setClassString("");
		screenVars.vstpay.setClassString("");
		screenVars.revdateDisp.setClassString("");
		screenVars.payoutoption.setClassString("");//fwang3
	}

/**
 * Clear all the variables in Sd5hfscreen
 */
	public static void clear(VarModel pv) {
		Sd5hfScreenVars screenVars = (Sd5hfScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.lifcnum.clear();
		screenVars.ctypedes.clear();
		screenVars.linsname.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.currcd.clear();
		screenVars.rgpytypesd.clear();
		screenVars.regpayfreq.clear();
		screenVars.frqdesc.clear();
		screenVars.destkey.clear();
		screenVars.ddind.clear();
		screenVars.cltype.clear();
		screenVars.clmdesc.clear();
		screenVars.claimevd.clear();
		screenVars.rgpymop.clear();
		screenVars.payclt.clear();
		screenVars.payenme.clear();
		screenVars.claimcur.clear();
		screenVars.anvdateDisp.clear();
		screenVars.anvdate.clear();
		screenVars.finalPaydateDisp.clear();
		screenVars.finalPaydate.clear();
		screenVars.rgpyshort.clear();
		screenVars.clmcurdsc.clear();
		screenVars.currds.clear();
		screenVars.rgpynum.clear();
		screenVars.pymt.clear();
		screenVars.totalamt.clear();
		screenVars.crtdateDisp.clear();
		screenVars.crtdate.clear();
		screenVars.firstPaydateDisp.clear();
		screenVars.firstPaydate.clear();
		screenVars.prcnt.clear();
		screenVars.vstpay.clear();
		screenVars.revdateDisp.clear();
		screenVars.revdate.clear();
		screenVars.payoutoption.clear();//fwang3
	}
}
