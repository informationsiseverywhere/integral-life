	package com.csc.life.annuities.screens;

	import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

	import com.csc.common.DD;
	import com.csc.smart400framework.SmartVarModel;
	import com.quipoz.framework.datatype.BaseData;
	import com.quipoz.framework.datatype.FixedLengthStringData;
	import com.quipoz.framework.datatype.LongData;
	import com.quipoz.framework.datatype.ZonedDecimalData;

	/**
	 * Screen variables for Sjl09
	 */
	public class Sjl09ScreenVars extends SmartVarModel { 
		private static final long serialVersionUID = 1L; 
		
		public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
		public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
		public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
		public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
		public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
		public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
		public FixedLengthStringData wthldngflag = DD.wthldngflag.copy().isAPartOf(dataFields,44);
		public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize())
															.isAPartOf(dataArea, getDataFieldsSize());
		public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
		public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
		public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
		public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
		public FixedLengthStringData wthldngflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
		public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize())
															.isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
		public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
		public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
		public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
		public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
		public FixedLengthStringData[] wthldngflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
		public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


		public LongData Sjl09screenWritten = new LongData(0);
		public LongData Sjl09protectWritten = new LongData(0);
		
			
		
		@Override
		public boolean hasSubfile() {
			return false;
		}


		public Sjl09ScreenVars() {
			super();
			initialiseScreenVars();
		}

		@Override
		public final void initialiseScreenVars() {
			fieldIndMap.put(wthldngflagOut,new String[] {"01","40", "-01",null, null, null, null, null, null, null, null, null});
			screenFields = getScrnFields();
			screenOutFields = getscreenOutFields();
			screenErrFields = getscreenErrFields();
			screenDateFields = new BaseData[] {};
			screenDateErrFields = new BaseData[] {};
			screenDateDispFields = new BaseData[] {};
			
			screenDataArea = dataArea;
			errorInds = errorIndicators;
			screenRecord = Sjl09screen.class;
			protectRecord = Sjl09protect.class;
		}
		
		
		public BaseData[] getScrnFields()
		{
			
			return new BaseData[] {company, tabl, item, longdesc, wthldngflag};
			
		}
		
		public BaseData[][] getscreenOutFields()
		{
			
			return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, wthldngflagOut};
			
		}
		
		public BaseData[] getscreenErrFields()
		{
			
			return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, wthldngflagErr};
			
		}
		
		public final int getDataAreaSize()
		{
			return 125;
		}
		
		public final int getDataFieldsSize()
		{
			return 45;
		}
		public final int getErrorIndicatorSize()
		{
			return 20;
		}
		public final int getOutputFieldSize()
		{
			return 60;
		}

	}
