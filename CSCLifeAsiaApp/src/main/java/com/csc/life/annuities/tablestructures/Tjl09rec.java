package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Tjl09rec extends ExternalData {
	private static final long serialVersionUID = 1L; 
	
	public FixedLengthStringData tjl09Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData wthldngflag = new FixedLengthStringData(1).isAPartOf(tjl09Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(499).isAPartOf(tjl09Rec, 1, FILLER);
	
	public void initialize() {
		COBOLFunctions.initialize(tjl09Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl09Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}
