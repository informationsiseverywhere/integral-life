package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN Date: 30 January 2020 Author: sagrawal35
 */
@SuppressWarnings("unchecked")
public class Sjl11screen extends ScreenRecord {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] { 4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { 1, 22, 3, 79 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sjl11ScreenVars sv = (Sjl11ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl11screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
	}

	public static void clearClassString(VarModel pv) {
		Sjl11ScreenVars screenVars = (Sjl11ScreenVars) pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrm.setClassString("");
		screenVars.itmto.setClassString("");
		screenVars.anntypdesc.setClassString("");
		screenVars.annuitype.setClassString("");
		screenVars.bentypdesc01.setClassString("");
		screenVars.bentypdesc02.setClassString("");
		screenVars.bentypdesc03.setClassString("");
		screenVars.bentypdesc04.setClassString("");
		screenVars.bentypdesc05.setClassString("");
		screenVars.bentypdesc06.setClassString("");
		screenVars.beneftype01.setClassString("");
		screenVars.beneftype02.setClassString("");
		screenVars.beneftype03.setClassString("");
		screenVars.beneftype04.setClassString("");
		screenVars.beneftype05.setClassString("");
		screenVars.beneftype06.setClassString("");
		screenVars.defannpayfreq.setClassString("");	
	}

	/**
	 * Clear all the variables in Sjl11screen
	 */
	public static void clear(VarModel pv) {
		Sjl11ScreenVars screenVars = (Sjl11ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrm.clear();
		screenVars.itmto.clear();
		screenVars.anntypdesc.clear();
		screenVars.annuitype.clear();
		screenVars.bentypdesc01.clear();
		screenVars.bentypdesc02.clear();
		screenVars.bentypdesc03.clear();
		screenVars.bentypdesc04.clear();
		screenVars.bentypdesc05.clear();
		screenVars.bentypdesc06.clear();
		screenVars.beneftype01.clear();
		screenVars.beneftype02.clear();
		screenVars.beneftype03.clear();
		screenVars.beneftype04.clear();
		screenVars.beneftype05.clear();
		screenVars.beneftype06.clear();
		screenVars.defannpayfreq.clear();		
	}
}