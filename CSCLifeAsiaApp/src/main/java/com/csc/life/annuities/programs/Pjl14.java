package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.life.annuities.screens.Sjl14ScreenVars;
import com.csc.life.annuities.tablestructures.Tjl11rec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl14 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl14.class); 
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL14");		
	private Wsspsmart wsspsmart = new Wsspsmart();	
	private boolean annuRegFlag = false;
	/* ERRORS */
	private static final String JL43 = "JL43";
	private static final String F910 = "F910";	
	private static final String E186 = "E186";
	private static final String F917 = "F917";		
	private static final String E070 = "E070";
	private static final String E844 = "E844";
	private static final String H137 = "H137";
	private static final String F259 = "F259";
	private static final String JL55 = "JL55";
	private static final String MA = "MA";
	private static final String AR = "AR";	
	private static final String PT = "PT";	
	private static final String TR = "TR";	
	private Sjl14ScreenVars sv =   getLScreenVars();	
	private Batckey wsaaBatchkey = new Batckey();
	
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	private AnnyregpfDAO annyregpfDAO = getApplicationContext().getBean("annyregpfDAO" , AnnyregpfDAO.class);	
	private Sftlockrec sftlockrec = new Sftlockrec();	
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private static final String TJL11 = "TJL11";
	private Tjl11rec tjl11rec = new Tjl11rec();
	private FixedLengthStringData wsaaAnnyType = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBeneType = new FixedLengthStringData(3);
			
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		EXIT2190,
		EXIT2219,			
		EXIT3090		
    }

	public Pjl14() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl14", AppVars.getInstance(), sv);
	}
	
	protected Sjl14ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sjl14ScreenVars.class);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline {}", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo {}", e);
		}
    }

	@Override
	protected void initialise1000() {
		initialise1010();	
	}

	protected void initialise1010() {
		annuRegFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUOTR008", appVars, "IT");		 
		if(!annuRegFlag){
			return;
		}
		wsspcomn.clntkey.set(SPACES);
		sv.dataArea.set(SPACES);		
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);		
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(E070);
		}		
	}

	@Override
	protected void preScreenEdit() {
		return;
		
	}
	
	@Override
	protected void screenEdit2000()
	{
		if(!annuRegFlag){
			return;
		}
		wsspcomn.edterror.set(Varcom.oK);
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)){
			if (isNE(scrnparams.statuz, "BACH"))
				validateKeys2200();
		if (isNE(sv.errorIndicators, SPACES)) 
			wsspcomn.edterror.set("Y");
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}		
	}
 }
	protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			LOGGER.info("validateAction2100 {}", e);
		}
	}

	protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, Varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.EXIT2190);
		}
	}

	
	protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, Varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

	protected void validateKeys2200() {		
		try {
			validateChdrsel2210();
			validateAnnyData2220();
		}
		catch (GOTOException e){			
			LOGGER.info("validateKeys2200 {}", e);
		}		
	}
	
	protected void validateChdrsel2210(){
		if (isEQ(subprogrec.key1, SPACES)) {
			return;
		}
		if(isEQ(sv.action, "A")){
			if (isEQ(sv.chdrsel, SPACE)) {
				sv.chdrselErr.set(E186);				
			}
			if (isEQ(sv.annuitype, SPACE)) {
				sv.annuitypeErr.set(E186);				
			}
			if (isEQ(sv.beneftype, SPACE)) {
				sv.beneftypeErr.set(E186);				
			}
		}	
		if(isEQ(sv.action, "B") || isEQ(sv.action, "I")){
			if (isEQ(sv.chdrsel, SPACE)) {
				sv.chdrselErr.set(E186);				
			}
			if (isNE(sv.annuitype, SPACE)) {
				sv.annuitypeErr.set(E844);				
			}
			if (isNE(sv.beneftype, SPACE)) {
				sv.beneftypeErr.set(E844);				
			}
		}	
	}
	
	protected void validateAnnyData2220(){
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());			
		if (null == chdrpf) {
			sv.chdrselErr.set(F917);		
			return;	//ILJ-711
		}
		if (isEQ(sv.action, "A") && !(MA.equals(chdrpf.getStatcode().trim()) && MA.equals(chdrpf.getPstcde().trim()))) {
			sv.chdrselErr.set(JL43);			
		}else{
			boolean actionFlag = false;
			if((isEQ(sv.action, "B") || isEQ(sv.action, "I"))){
				actionFlag = true;
			}
			if (actionFlag && !(MA.equals(chdrpf.getStatcode().trim()) && 
					(AR.equals(chdrpf.getPstcde().trim()) || TR.equals(chdrpf.getPstcde().trim())))) {
				sv.chdrselErr.set(H137);				
			}else{
				chdrpfDAO.setCacheObject(chdrpf);
			}		
		}
		//Annuity Create selected
		if(isEQ(sv.action, "A")){
			Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString().trim(), TJL11, chdrpf.getCnttype());
			if (itempf == null) {
				sv.chdrselErr.set(JL43);				
			} else {
				tjl11rec.tjl11Rec.set(itempf.getGenareaString());
				boolean beneftflag = false;
				if(isNE(sv.annuitype, tjl11rec.annuitype) && isEQ(sv.annuitypeErr, SPACES)){
					sv.annuitypeErr.set(JL43);
				}					
				for (int i = 1; i <= 6; i++) {
					if(isNE(tjl11rec.beneftype[i], SPACES) && isEQ(tjl11rec.beneftype[i], sv.beneftype)){							
						beneftflag = true;								
					}
				}
				if(!beneftflag && isEQ(sv.beneftypeErr, SPACES)){
					sv.beneftypeErr.set(JL43);
				}
			}
			wsaaAnnyType.set(sv.annuitype);
			wsaaBeneType.set(sv.beneftype);
		}
		//Annuity Modification selected
		if(isEQ(sv.action, "B")){
			Annyregpf annyregpf = annyregpfDAO.getAnnyregpf(wsspcomn.company.toString(), sv.chdrsel.toString().trim());
			Optional<Annyregpf> isExists = Optional.ofNullable(annyregpf);
			if (isExists.isPresent()) {
				if (null == annyregpf.getChdrnum())
					sv.chdrselErr.set(F259);
				if (PT.equals(annyregpf.getAnnpstcde().trim()))
					sv.chdrselErr.set(JL55);
				if (null != annyregpf.getAnnuitype())
					wsaaAnnyType.set(annyregpf.getAnnuitype());
				if (null != annyregpf.getBeneftype())
					wsaaBeneType.set(annyregpf.getBeneftype());					
			}
		}

		if (isEQ(subprogrec.key1, "Y")) {
			checkRuser2500();
			if (isNE(sdasancrec.statuz, Varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				return;
			}
		}	

	}
	
	protected void checkRuser2500()
	{
		begin2510();
	}

protected void begin2510()
	{
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.statuz.set(Varcom.oK);
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isEQ(sdasancrec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(sdasancrec.statuz);
			syserrrec.params.set(sdasancrec.sancRec);
			fatalError600();
		}
	}

    @Override
	protected void update3000()
	{
		if(!annuRegFlag){
			return;
		}
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateWssp3010();				
				case EXIT3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		wsspcomn.servunit.set(wsaaAnnyType);
		wsspcomn.chdrstcdb.set(wsaaBeneType);
		wsspcomn.chdrChdrnum.set(sv.chdrsel);
		wsspcomn.chdrRpfx.set(tjl11rec.defannpayfreq);
	
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.EXIT3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);

		if (isEQ(scrnparams.action, "I")) {
			wsspcomn.flag.set("I");
		}
		else {
			if (isEQ(scrnparams.action, "A")) {
				wsspcomn.flag.set("A");
			}
			else {
				wsspcomn.flag.set("M");
			}
		}					
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum());		
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, Varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(F910);
		}
	}

protected void batching3080()
{
	if (isEQ(subprogrec.bchrqd, "Y")
	&& isEQ(sv.errorIndicators, SPACES)) {
		updateBatchControl3100();
	}
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
}

protected void updateBatchControl3100()
{
	/*AUTOMATIC-BATCHING*/
	batcdorrec.function.set("AUTO");
	batcdorrec.tranid.set(wsspcomn.tranid);
	batcdorrec.batchkey.set(wsspcomn.batchkey);
	callProgram(Batcdor.class, batcdorrec.batcdorRec);
	if (isNE(batcdorrec.statuz, Varcom.oK)) {
		sv.actionErr.set(batcdorrec.statuz);
	}
	wsspcomn.batchkey.set(batcdorrec.batchkey);
	/*EXIT*/
}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
    @Override
    protected void whereNext4000()
	{
    	if(!annuRegFlag){
			return;
		}
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

}
