/*
 * File: P5233.java
 * Date: 30 August 2009 0:21:15
 * Author: Quipoz Limited
 * 
 * Class transformed from P5233.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.annuities.dataaccess.VstdTableDAM;
import com.csc.life.annuities.screens.S5233ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This is  a  program  which  forms  part  of the  9405  Annuities
* Development.  It is a component of the Vesting Approval process
* and controls the Vesting Approval Confirmation Screen, S5233.
* The details on the screen will be displayed from the Vesting
* Detail (VSTD) file.
*
* From the screen it is possible to enquire on the Regular Payments
* (REGT) records by selecting a VSTD, when S5237, regular payments
* selection screen, is displayed.
*
* On pressing <ENTER> from S5233 the Vesting Approval AT request
* is submitted.
*
*****************************************************************
* </pre>
*/
public class P5233 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5233");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaLineCount = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).setUnsigned();
		/* ERRORS */
	private String e944 = "E944";
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String cltsrec = "CLTSREC";
	private String descrec = "DESCREC";
	private String lifemjarec = "LIFEMJAREC";
	private String vstdrec = "VSTDREC";
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*Vesting Details Logical*/
	private VstdTableDAM vstdIO = new VstdTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5233ScreenVars sv = ScreenProgram.getScreenVars( S5233ScreenVars.class);
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private static final String validflag="1";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		preExit, 
		exit2090, 
		updateErrorIndicators2120, 
		exit3090, 
		exit4090
	}

	public P5233() {
		super();
		screenVars = sv;
		new ScreenModel("S5233", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			retrieveContractHeader1020();
			readLifeDetails1030();
			jointLifeDetails1040();
			contractDetails1050();
			begnVstd1060();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.plansfx.set(ZERO);
		sv.vstpay.set(ZERO);
		sv.vstlump.set(ZERO);
		wsaaTranno.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5233", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

protected void retrieveContractHeader1020()
	{
		chdrmjaIO.setFunction(varcom.retrv);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
	}

protected void readLifeDetails1030()
	{
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1040()
	{
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
		}
		else {
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			cltsIO.setFormat(cltsrec);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(cltsIO.getStatuz());
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
	}

protected void contractDetails1050()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void begnVstd1060()
	{
		vstdIO.setDataArea(SPACES);
		vstdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		vstdIO.setChdrnum(chdrmjaIO.getChdrnum());
		vstdIO.setCoverage(ZERO);
		vstdIO.setLife(ZERO);
		vstdIO.setRider(ZERO);
		vstdIO.setPlanSuffix(ZERO);
		vstdIO.setTranno(ZERO);
		vstdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		vstdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vstdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		vstdIO.setFormat(vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)
		&& isNE(vstdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vstdIO.getParams());
			syserrrec.statuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(vstdIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
			vstdIO.setStatuz(varcom.endp);
		}
		else {
			wsaaTranno.set(vstdIO.getTranno());
		}
		for (wsaaLineCount.set(1); !(isGT(wsaaLineCount,sv.subfilePage)
		|| isEQ(vstdIO.getStatuz(),varcom.endp)); wsaaLineCount.add(1)){
			loadSubfile1100();
		}
	}

protected void loadSubfile1100()
	{
		buildScreen1110();
	}

protected void buildScreen1110()
	{
		sv.plansfx.set(vstdIO.getPlanSuffix());
		sv.life.set(vstdIO.getLife());
		sv.coverage.set(vstdIO.getCoverage());
		sv.rider.set(vstdIO.getRider());
		sv.effdate.set(vstdIO.getEffdate());
		sv.vstpay.set(vstdIO.getVstpay());
		sv.vstlump.set(vstdIO.getVstlump());
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5233", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		vstdIO.setFunction(varcom.nextr);
		vstdIO.setFormat(vstdrec);
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)
		&& isNE(vstdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vstdIO.getParams());
			syserrrec.statuz.set(vstdIO.getStatuz());
			fatalError600();
		}
		if (isNE(vstdIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(vstdIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
			vstdIO.setStatuz(varcom.endp);
		}
		if (isEQ(vstdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			skipOnReturn2010();
			validateScreen2030();
		}
		catch (GOTOException e){
		}
	}

protected void skipOnReturn2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2030()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			for (wsaaLineCount.set(1); !(isGT(wsaaLineCount,sv.subfilePage)
			|| isEQ(vstdIO.getStatuz(),varcom.endp)); wsaaLineCount.add(1)){
				loadSubfile1100();
			}
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		scrnparams.function.set(varcom.srnch);
		processScreen("S5233", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSelect2110();
				}
				case updateErrorIndicators2120: {
					updateErrorIndicators2120();
					readNextModifiedRecord2130();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		if (isNE(sv.select,NUMERIC)) {
			sv.selectErr.set(e944);
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5233", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2130()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5233", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			skipOnReturnOrKill3010();
		}
		catch (GOTOException e){
		}
	}

protected void skipOnReturnOrKill3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		try {
			start4010();
		}
		catch (GOTOException e){
		}
	}

protected void start4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (isEQ(sv.select,"1")) {
				sv.select.set(SPACES);
				scrnparams.function.set(varcom.supd);
				screenio4100();
			}
		}
		else {
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.select,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			chdrmjaIO.setFunction(varcom.keeps);
			chdrmjaio4300();
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			optswchCall4500();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.select,"1")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			vstdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
			vstdIO.setChdrnum(chdrmjaIO.getChdrnum());
			vstdIO.setLife(sv.life);
			vstdIO.setCoverage(sv.coverage);
			vstdIO.setRider(sv.rider);
			vstdIO.setPlanSuffix(sv.plansfx);
			vstdIO.setTranno(wsaaTranno);
			vstdIO.setFunction(varcom.readr);
			vstdio4200();
			vstdIO.setFunction(varcom.keeps);
			vstdio4200();
			chdrmjaIO.setFunction(varcom.keeps);
			chdrmjaio4300();
			
			covrpf=covrpfDAO.getCovrRecord(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString(),sv.life.toString(),vstdIO.getNewcovr().toString(),
					sv.rider.toString(),sv.plansfx.toInt(),validflag);
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}

			optswchCall4500();
		}
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("S5233", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		/*EXIT*/
	}

protected void vstdio4200()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vstdIO.getStatuz());
			syserrrec.params.set(vstdIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void chdrmjaio4300()
	{
		/*CALL*/
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void optswchCall4500()
	{
		call4510();
	}

protected void call4510()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK))
		&& (isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}
}
