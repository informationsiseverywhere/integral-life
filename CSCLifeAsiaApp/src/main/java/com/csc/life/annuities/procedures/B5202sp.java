/*
 * File: B5202sp.java
 * Date: 29 August 2009 21:00:00
 * Author: Quipoz Limited
 * 
 * Class transformed from B5202SP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcddmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.procedures.P5147at;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5510rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Conerr;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Conerrrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Runparmrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*        SINGLE PREMIUM DSS CASH TOP UP.
*
* THIS IS A COPY OF THE ONLINE PROGRAM P5144 WITH A FEW CHANGES
* THE MAIN ONE BEING IT HAS BEEN CONVERTED TO A BATCH SUBROUTINE.
* IT IS CALLED BY B5202 ON APPLY TO A CONTRACT THE PAYMENT
* RECEIVED FROM THE DSS TAPE.
* FOR EASE OF CONVERSION AND MAINTAINANCE OF THE ONLINE FUNCTIONS
* CONSISTENCY,INSTEAD OF FIELDS BEING MOVED IN AND OUT THE SCREEN
* COPYBOOK, A RECORD CALLED B5202SP-DATA-AREA HAS BEEN
* CREATED AS A COPY OF P5144-DATA-AREA.
* THE LAST PART OF THIS PROGRAM, IE. THE CALL TO P5147AT IS TAKEN
* FROM P5147, BUT THE AT MODULE IS CALLED AS A SUBROUTINE.
*
* This function will be used to apply a single premium top-up to
* a component within a contract. The amount (s) will be applied
* to individual components and so it will be up to the user to
* register the correct dissection amount to each coverage if the
* total is being spread across several components.
*
* The user may have selected to apply the top-up to either
* individual policies within the plan or to the whole plan. After
* this the actual component will be selected. If the top-up is
* being applied to individual policies then this screen will be
* invoked once for each selected component.
*
* If the whole plan has been selected, (COVRMJA-PLAN-SUFFIX =
* 0000), then this screen will again be invoked once for each
* component selected but it must loop around within itself and
* display the screen once for each matching COVR record that
* exists within the plan. It will loop around between the 2000
* and 4000 sections until all the COVR records with a key
* matching on Company, Contract Number, Life, Coverage and Rider
* have been processed. The program will read all the COVR records
* from the summary record, if it exists, to the last COVR record
* in the plan for the given rider key. The plan suffix being
* processed will be displayed in the heading portion of the
* screen in order to show to the user which policy is currently
* being processed. When the summary record is being processed
* display the range of policies represented by the summary, ie.
*
*       Policy Number: 0001 - 0006
*
* INITIALISE (1000- SECTION)
*
*
* Perform a RETRV on the COVRMJA I/O module. This will return
* the first COVRMJA record on the selected policy.
*
* If COVRMJA-PLAN-SUFFIX = 0000  then the whole plan is being
* processed and every COVRMJA record whose key matches on the
* passed Company, Contract Number, Life, Coverage and Rider will
* be processed.
*
* If the COVRMJA-PLAN-SUFFIX is not greater than the value of
* CHDRMJA-POLSUM then a 'notional' policy is being processed,
* ie. a policy has been selected that is at present part of the
* summarised portion of the contract. If this is the case then
* the COVRMJA summary record will have been saved by the KEEPS
* command and the values from here will be displayed.
*
* However, when the default values from the corresponding ULNK
* record are displayed if they are amounts and not percentages
* they must be divided by the number of policies summarised,
* (CHDRMJA-POLSUM) before being displayed. Any remainder from
* the division must be shown as belonging to the amounts when
* policy number one is displayed, as the following example will
* serve to illustrate:
*
*     Policies Summarised = 3
*     Amount = $100
*
* For notional policy #1 the calculation will be:
*
*     Value to Display = Amount - (Amount * (POLSUM - 1))
*                                           -------------
*                                             POLSUM
*                      = 100 - 100 * (3 - 1) / 3
*                      = 100 - 100 * 2/3
*                      = $34
*
* For notional policies #2 and #3 the vlaue will simply be
* divided by 3 giving $33 each.
*
* If processing a notional policy remember to use a Plan Suffix
* of 0000 when reading the corresponding ULNK record. The UTRN
* records will however be written with the selected policy number
* and breakout will handle the physical breakout of all the other
* Contract Components later.
*
* The details of the contract being processed will be stored in
* the CHDRMJA I/O module. Retrieve the details user RETRV.
*
* Read the corresponding ULNK record to obtain the existing fund
* split plan. If the fund split is by percentage then display
* the existing funds and percentages from ULNK and their
* Currency Codes from T5515. The user may then use the existing
* split as a default but will be allowed to amend it if required.
*
* Read T5540. If the Available Fund List code is blank then
* there is no list of permitted funds and the user will be
* allowed to enter any valid funds of his choice. If there is
* an available fund list code then all Fund Code fields on the
* screen must be protected.
*
* If the details from the ULNK records were not displayed as
* they were not split by percentages then use the Available
* Fund List code to read T5543 and display the permitted fund
* codes on the screen. Protect all the fund code fields and the
* Fund Split Plan field. Also display each fund currency from
* T5515.
*
* Set the Percent/Amount Indicator to 'P'.
*
*
* UPDATING (3000- SECTION)
*
*
* Write one UTRN for each fund with an amount or percentage. Set
* the following fields on the UTRN:
*
* CHDRCOY    |
* CHDRNUM    |
* LIFE       |
* COVERAGE   |     Full key using the new tranno
* RIDER      |
* PLNSFX     |
* TRANNO     | New Transaction Number (CHDRMJA-TRANNO + 1)
* USER        - PARM information
* VTRFUND     - Fund Code
* UNITSA      - ?
* NDFIND      from T6647, use the allocation indicator
* UNITYP      the BASIS from T5540, add to it the sex and
*             transaction code as the key to T5537 which in
*             turn provides the key to T5536. In this table
*             select the values attributed to frequency 00.
* BATCOY     |
* BATCBRN    |
* BATCACTYR  |
* BATCACTMN  |     Passed from B5202
* BATCTRCDE  |
* BATCBATCH  |
* MONIESDT    - from T6647, Effective Date for Transaction.
* CRTABLE     - CRTABLE Code from COVR.
* CNTCURR     - from CHDR
* CNTAMNT     - the amount of the premium for the particular fund
*               in contract currency * the enhancement factor.
*               Read T6647 for the Enhanced Allocation Basis. Add
*               to this the currency code to form the key to
*               T5545. Use the factor applicable to frequency 00.
* FNDCURR     - Fund Currency from T5515
* SACSCODE    - From T5645 keyed by Program Id. line 1
* SACSTYP     -  "     "     "   "    "      "   "   "
* GENLCDE     -  "     "     "   "    "      "   "   "
* CONTYP      -
* PRCSEQ      - From T6647
* CRCDTE      -
*
* If the entry in T5536 indicates that the amount invested is
* < 100% write a NON INVESTED UTRN as well as the standard UTRN.
*
* The last section will be used to initiate the final processing
* of the on-line portion of Single Premium Top-up.
*
* It will perform the following tasks:
*   - Transfer the softlock to AT
*   - Request the AT function which will complete the processing
*     and call breakout if required.
*
*
*****************************************************************
* </pre>
*/
public class B5202sp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("B5202SP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String h023 = "H023";
		/* FORMATS */
	private static final String pcdtmjarec = "PCDTMJAREC";
	private static final String utrnrec = "UTRNREC";

	private FixedLengthStringData wsaaUlnkFound = new FixedLengthStringData(1).init(SPACES);
	private Validator ulnkFound = new Validator(wsaaUlnkFound, "Y");

	private FixedLengthStringData wsaaUtrnFound = new FixedLengthStringData(1).init(SPACES);
	private Validator utrnFound = new Validator(wsaaUtrnFound, "Y");
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaIfValidFund = new FixedLengthStringData(1).init(SPACES);
	private Validator wsaaValidFund = new Validator(wsaaIfValidFund, "Y");
	private PackedDecimalData wsaaChdrSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInvSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinglePrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaUtrnSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalPerc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPcUnits = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaPcInitUnits = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaPcMaxPeriods = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4);
	private PackedDecimalData wsaaPerc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaCltdob = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaCltage = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5515Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5515Item, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5515Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5536Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5536Allbas = new FixedLengthStringData(4).isAPartOf(wsaaT5536Item, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT5536Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5537Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5537Allbas = new FixedLengthStringData(3).isAPartOf(wsaaT5537Item, 0);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Item, 3);
	private FixedLengthStringData wsaaT5537Trans = new FixedLengthStringData(4).isAPartOf(wsaaT5537Item, 4);

	private FixedLengthStringData wsaaT5540Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5540Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5540Item, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaT5540Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5543Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5543Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5543Item, 0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(4).isAPartOf(wsaaT5543Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Currcode = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 4);

	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaT5645Prog = new FixedLengthStringData(7).isAPartOf(wsaaT5645Item, 0);
	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaT5645Item, 7, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);
	private FixedLengthStringData filler5 = new FixedLengthStringData(1).isAPartOf(wsaaT6647Item, 7, FILLER).init(SPACES);

	private FixedLengthStringData wsaaFndsplItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFndopt = new FixedLengthStringData(4).isAPartOf(wsaaFndsplItem, 0);
	private FixedLengthStringData filler6 = new FixedLengthStringData(4).isAPartOf(wsaaFndsplItem, 4, FILLER).init(SPACES);
		/* WSAA-UNIT-VIRTUAL-FUNDS */
	private FixedLengthStringData[] wsaaUnitVirtualFund = FLSInittedArray(10, 4);
		/* WSAA-CURRCYS */
	private FixedLengthStringData[] wsaaCurrcy = FLSInittedArray(10, 3);

	private FixedLengthStringData wsaaUnitAllPercAmts = new FixedLengthStringData(90);
	private ZonedDecimalData[] wsaaUnitAllPercAmt = ZDArrayPartOfStructure(10, 9, 2, wsaaUnitAllPercAmts, 0);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator wsaaWholePlan = new Validator(wsaaPlanproc, "Y");
	private Validator wsaaSgleComponent = new Validator(wsaaPlanproc, "N");

	private FixedLengthStringData wsaaIfFirstTime = new FixedLengthStringData(1);
	private Validator wsaaFirstTime = new Validator(wsaaIfFirstTime, "Y");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler8 = new FixedLengthStringData(179).isAPartOf(wsaaTransactionRec, 21, FILLER).init(SPACES);

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData index1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaLinkBatchkey = new FixedLengthStringData(22);
	private ZonedDecimalData wsaaLinkAmount = new ZonedDecimalData(13, 2);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PcddmjaTableDAM pcddmjaIO = new PcddmjaTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Conerrrec conerrrec = new Conerrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Atmodrec atmodrec = new Atmodrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5510rec t5510rec = new T5510rec();
	private T5515rec t5515rec = new T5515rec();
	private T5536rec t5536rec = new T5536rec();
	private T5537rec t5537rec = new T5537rec();
	private T5540rec t5540rec = new T5540rec();
	private T5543rec t5543rec = new T5543rec();
	private T5545rec t5545rec = new T5545rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
	private Runparmrec runparmrec = new Runparmrec();
	private B5202spDataAreaInner b5202spDataAreaInner = new B5202spDataAreaInner();
	private TablesInner tablesInner = new TablesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		sacs18000, 
		exit19000, 
		utrnLoop15050, 
		utrnLoop31100, 
		init48500, 
		zeroise51080, 
		exit51090, 
		para54000, 
		gotT553754200, 
		increment54600, 
		increment54850, 
		n000Loop54500, 
		enhanb58200, 
		enhanc58300, 
		enhand58400, 
		enhane58500, 
		enhanf58600, 
		nearExit58800, 
		addPcdt70200, 
		call70300
	}

	public B5202sp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		wsaaLinkAmount = convertAndSetParam(wsaaLinkAmount, parmArray, 2);
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 1);
		wsaaLinkBatchkey = convertAndSetParam(wsaaLinkBatchkey, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main000()
	{
		/*PROCESS*/
		/*    DISPLAY 'B5202SP-LINK-AMOUNT==>' WSAA-LINK-AMOUNT.*/
		/*    DISPLAY 'B5202SP-LINK-BATCH ==>' WSAA-LINK-BATCHKEY.*/
		/*    DISPLAY 'B5202SP-RUNPARM-REC==>' PARM-RUNPARM-REC.*/
		mainline100();
	}

protected void callConerr002()
	{
		/*CONERR*/
		conerrrec.subrname.set(wsaaProg);
		callProgram(Conerr.class, conerrrec.conerrRec);
		/*EXIT*/
	}

protected void systemError005()
	{
		/*DBERROR*/
		conerrrec.sysertyp.set("2");
		callConerr002();
		/*EXIT*/
	}

protected void databaseError006()
	{
		/*DBERROR*/
		conerrrec.sysertyp.set("1");
		conerrrec.syserror.set(conerrrec.statuz);
		callConerr002();
		/*EXIT*/
	}

protected void mainline100()
	{
		singlePremiumTopUp11000();
		updateDatabase30000();
		sftlckCallAt80000();
		exitProgram();
	}

protected void singlePremiumTopUp11000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					singlePremiumTopUpPara11000();
					ulnk13000();
				case sacs18000: 
					sacs18000();
				case exit19000: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void singlePremiumTopUpPara11000()
	{
		b5202spDataAreaInner.b5202spDataArea.set(SPACES);
		varcom.vrcmTranid.set(runparmrec.tranid);
		wsaaBatckey.set(wsaaLinkBatchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaBusinessDate.set(datcon1rec.intDate);
		/*    Initialise Single Premium Top Up working area.*/
		b5202spDataAreaInner.b5202spInstprem.set(ZERO);
		b5202spDataAreaInner.b5202spEffdate.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt01.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt02.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt03.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt04.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt05.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt06.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt07.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt08.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt09.set(ZERO);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt10.set(ZERO);
		b5202spDataAreaInner.b5202spUnitVirtualFund01.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund02.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund03.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund04.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund05.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund06.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund07.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund08.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund09.set(SPACES);
		b5202spDataAreaInner.b5202spUnitVirtualFund10.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy01.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy02.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy03.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy04.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy05.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy06.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy07.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy08.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy09.set(SPACES);
		b5202spDataAreaInner.b5202spCurrcy10.set(SPACES);
		wsaaUnitAllPercAmts.set(ZERO);
		wsaaUlnkFound.set(SPACES);
		/*    Set Single Premium Top Up working area fields*/
		/* RETRIEVE CONTRACT HEADER FOR CONTRACT FOUND EARLIER*/
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrmjaIO.getParams());
			databaseError006();
		}
		if (isNE(chdrmjaIO.getValidflag(),"1")) {
			goTo(GotoLabel.exit19000);
		}
		/*  Now release the CHDRMJA.*/
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrmjaIO.getParams());
			databaseError006();
		}
		/*  The next check is to stop the program abending*/
		/*  when it is passed a blank key. In this case the program*/
		/*  should just go thru without processing.*/
		covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(covrmjaIO.getParams());
			databaseError006();
		}
		/*    DISPLAY 'COVERAGE FOR TOP-UP ==>' COVRMJA-DATA-KEY.*/
		/* If the user has already entered the Top-Up for a component*/
		/* do not allow them to alter it, instead redisplay the*/
		/* component selection screen.*/
		/*  Now release the COVRMJA.*/
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(covrmjaIO.getParams());
			databaseError006();
		}
		utrnExist15000();
		b5202spDataAreaInner.b5202spCoverage.set(covrmjaIO.getCoverage());
		b5202spDataAreaInner.b5202spRider.set(covrmjaIO.getRider());
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		b5202spDataAreaInner.b5202spLife.set(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(lifemjaIO.getParams());
			databaseError006();
		}
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		wsaaCltdob.set(lifemjaIO.getCltdob());
		/*    MOVE LIFEMJA-CHDRCOY         TO CLTS-CLNTCOY.*/
		cltsIO.setClntcoy(runparmrec.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(cltsIO.getParams());
			databaseError006();
		}
		/* CHECK FOR EXISTENCE OF JOINT LIFE DETAILS.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			/* Nothing to do. */
		}
		else {
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			/*          MOVE LIFEMJA-CHDRCOY   TO CLTS-CLNTCOY*/
			cltsIO.setClntcoy(runparmrec.fsuco);
			cltsIO.setFunction("READR");
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				conerrrec.params.set(cltsIO.getParams());
				databaseError006();
			}
		}
		b5202spDataAreaInner.b5202spInstprem.set(ZERO);
		b5202spDataAreaInner.b5202spPercentAmountInd.set("P");
		/*    Get fund split plan*/
		b5202spDataAreaInner.b5202spVirtFundSplitMethod.set(SPACES);
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		wsaaT5540Item.set(SPACES);
		wsaaT5540Crtable.set(covrmjaIO.getCrtable());
		itdmIO.setItemitem(wsaaT5540Item);
		SmartFileCode.execute(appVars, itdmIO);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(),runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
		|| (isNE(itdmIO.getItemitem(),wsaaT5540Item))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.statuz.set(h023);
			databaseError006();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
		b5202spDataAreaInner.b5202spVirtFundSplitMethod.set(t5540rec.fundSplitPlan);
		if (isNE(t5540rec.alfnds,SPACES)) {
			readTableT554359500();
		}
		if (isEQ(b5202spDataAreaInner.b5202spVirtFundSplitMethod, SPACES)) {
			goTo(GotoLabel.sacs18000);
		}
	}

protected void ulnk13000()
	{
		/*    Read ULNK to see if Details already exist*/
		ulnkIO.setDataKey(SPACES);
		ulnkIO.setCoverage(covrmjaIO.getCoverage());
		ulnkIO.setRider(covrmjaIO.getRider());
		ulnkIO.setChdrcoy(covrmjaIO.getChdrcoy());
		ulnkIO.setChdrnum(covrmjaIO.getChdrnum());
		ulnkIO.setLife(covrmjaIO.getLife());
		ulnkIO.setJlife("00");
		/* If the COVRMJA-PLAN-SUFFIX is not greater than the value of*/
		/* CHDRMJA-POLSUM then a 'notional' policy is being processed.*/
		if (isLTE(covrmjaIO.getPlanSuffix(),chdrmjaIO.getPolsum())) {
			ulnkIO.setPlanSuffix(ZERO);
		}
		else {
			ulnkIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		}
		ulnkIO.setFunction("READR");
		SmartFileCode.execute(appVars, ulnkIO);
		/*    Record exists then use dets*/
		/*    or No record so use blank fields.*/
		if (isEQ(ulnkIO.getStatuz(),"MRNF")
		|| isEQ(ulnkIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			conerrrec.params.set(ulnkIO.getParams());
			databaseError006();
		}
		if (isEQ(ulnkIO.getStatuz(),"MRNF")) {
			wsaaUlnkFound.set("N");
			goTo(GotoLabel.exit19000);
		}
		/*    ULNK record found - set up fields*/
		/*    Unless it is an Amount*/
		/*    IF ULNK-PERC-OR-AMNT-IND = 'A'*/
		/*       PERFORM 58000-READ-T5543*/
		/*       GO TO 18000-SACS.*/
		wsaaUlnkFound.set("Y");
		for (x.set(1); !(isGT(x,10)); x.add(1)){
			move14000();
		}
		b5202spDataAreaInner.b5202spPercentAmountInd.set(ulnkIO.getPercOrAmntInd());
	}

	/**
	* <pre>
	*    Read  the sub account balance for  the contract
	* </pre>
	*/
protected void sacs18000()
	{
		wsaaTotalPremium.set(ZERO);
		acblIO.setParams(SPACES);
		acblIO.setRldgacct(ZERO);
		acblIO.setRldgcoy(runparmrec.company);
		acblIO.setRldgacct(chdrmjaIO.getChdrnum());
		acblIO.setOrigcurr(chdrmjaIO.getCntcurr());
		acblIO.setSacscode("LP");
		/* Line 3, item P5144 on table T5645 is read and the right*/
		/* suspense account is chosen.*/
		readT564556000();
		acblIO.setSacstyp(t5645rec.sacstype[3]);
		acblIO.setFunction(varcom.readr);
		/*    DISPLAY 'ACBL-DATA-KEY=' ACBL-DATA-KEY.*/
		SmartFileCode.execute(appVars, acblIO);
		getAppVars().addDiagnostic("ACBL-STATUZ  ="+acblIO.getStatuz());
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(acblIO.getParams());
			databaseError006();
		}
		compute(wsaaChdrSuspense, 2).set(mult(acblIO.getSacscurbal(),-1));
		/*    MOVE  WSAA-CHDR-SUSPENSE    TO B5202SP-TOTPREM.*/
		wsaaCovrKey.set(covrmjaIO.getDataKey());
		if (isEQ(covrmjaIO.getPlanSuffix(),ZERO)) {
			covrmjaIO.setPlanSuffix(9999);
			covrmjaIO.setFunction(varcom.begn);
			wsaaPlanproc.set("Y");
		}
		else {
			wsaaPlanproc.set("N");
			covrmjaIO.setFunction(varcom.readr);
		}
		policyLoad50000();
		b5202spDataAreaInner.b5202spInstprem.set(wsaaLinkAmount);
		b5202spDataAreaInner.b5202spEffdate.set(runparmrec.effdate);
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section.
	* </pre>
	*/
protected void move14000()
	{
		/*MOVE-VALUES*/
		b5202spDataAreaInner.b5202spUnitVirtualFund[x.toInt()].set(ulnkIO.getUalfnd(x));
		wsaaUnitVirtualFund[x.toInt()].set(ulnkIO.getUalfnd(x));
		b5202spDataAreaInner.b5202spUnitAllocPercAmt[x.toInt()].set(ulnkIO.getUalprc(x));
		wsaaUnitAllPercAmt[x.toInt()].set(ulnkIO.getUalprc(x));
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(b5202spDataAreaInner.b5202spUnitVirtualFund[x.toInt()]);
		if (isNE(b5202spDataAreaInner.b5202spUnitVirtualFund[x.toInt()], SPACES)) {
			readT551555000();
			b5202spDataAreaInner.b5202spCurrcy[x.toInt()].set(t5515rec.currcode);
			wsaaCurrcy[x.toInt()].set(t5515rec.currcode);
		}
		/*EXIT*/
	}

protected void utrnExist15000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					utrn15000();
				case utrnLoop15050: 
					utrnLoop15050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void utrn15000()
	{
		/*    Read UTRN to see if Details already exist*/
		wsaaUtrnFound.set("N");
		utrnIO.setDataKey(SPACES);
		utrnIO.setCoverage(covrmjaIO.getCoverage());
		utrnIO.setRider(covrmjaIO.getRider());
		utrnIO.setChdrcoy(covrmjaIO.getChdrcoy());
		utrnIO.setChdrnum(covrmjaIO.getChdrnum());
		utrnIO.setLife(covrmjaIO.getLife());
		utrnIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		utrnIO.setTranno(ZERO);
		utrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
	}

protected void utrnLoop15050()
	{
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)
		&& isNE(utrnIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(utrnIO.getParams());
			databaseError006();
		}
		if (isEQ(utrnIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(utrnIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(utrnIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(utrnIO.getCoverage(),covrmjaIO.getCoverage())
		|| isNE(utrnIO.getLife(),covrmjaIO.getLife())
		|| isNE(utrnIO.getRider(),covrmjaIO.getRider())
		|| isNE(utrnIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())) {
			return ;
		}
		if ((setPrecision(utrnIO.getTranno(), 0)
		&& isEQ(utrnIO.getTranno(),(add(chdrmjaIO.getTranno(),1))))) {
			wsaaUtrnFound.set("Y");
			return ;
		}
		utrnIO.setFunction("NEXTR");
		goTo(GotoLabel.utrnLoop15050);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void updateDatabase30000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update31000();
					setUpUtrn3105();
				case utrnLoop31100: 
					utrnLoop31100();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update31000()
	{
		/* If no PCDT record is to be created then the existing agent is*/
		/* to receive commission, therefore set up current details in*/
		/* PCDT to make AT processing uniform throughout.*/
		pcdtmjaIO.setFunction(varcom.begn);
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		pcdtmjaIO.setLife(b5202spDataAreaInner.b5202spLife);
		pcdtmjaIO.setCoverage(b5202spDataAreaInner.b5202spCoverage);
		pcdtmjaIO.setRider(b5202spDataAreaInner.b5202spRider);
		pcdtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrmjaIO.getChdrcoy(),pcdtmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),pcdtmjaIO.getChdrnum())
		|| isNE(b5202spDataAreaInner.b5202spLife, pcdtmjaIO.getLife())
		|| isNE(b5202spDataAreaInner.b5202spCoverage, pcdtmjaIO.getCoverage())
		|| isNE(b5202spDataAreaInner.b5202spRider, pcdtmjaIO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(),pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(pcdtmjaIO.getParams());
			databaseError006();
		}
		if (isEQ(pcdtmjaIO.getStatuz(),varcom.endp)) {
			defaultPcdt70000();
		}
	}

protected void setUpUtrn3105()
	{
		index1.set(ZERO);
	}

protected void utrnLoop31100()
	{
		index1.add(1);
		if (isGT(index1,10)) {
			return ;
		}
		if (isEQ(b5202spDataAreaInner.b5202spUnitVirtualFund[index1.toInt()], SPACES)) {
			return ;
		}
		readT5537T553654000();
		/* Set WSAA-INV-SINGP to the premium entered for the COVR.*/
		if (isEQ(b5202spDataAreaInner.b5202spPercentAmountInd, "P")) {
			wsaaSinglePrem.set(b5202spDataAreaInner.b5202spInstprem);
			wsaaInvSingp.set(b5202spDataAreaInner.b5202spInstprem);
		}
		else {
			wsaaSinglePrem.set(b5202spDataAreaInner.b5202spUnitAllocPercAmt[index1.toInt()]);
			wsaaInvSingp.set(b5202spDataAreaInner.b5202spUnitAllocPercAmt[index1.toInt()]);
		}
		/*    DISPLAY 'WSAA-INV-SINGP = ' WSAA-INV-SINGP.*/
		/*    DISPLAY 'WSAA-PC-UNITS  = ' WSAA-PC-UNITS.*/
		/*    DISPLAY 'WSAA-PC-INIT-UNITS     = ' WSAA-PC-INIT-UNITS.*/
		/* If UNITS not 100% then adjust WSAA-INV-SINGP and create the*/
		/* uninvested UTRN.*/
		if (isNE(wsaaPcUnits,100)) {
			compute(wsaaInvSingp, 2).set(div(mult(wsaaSinglePrem,wsaaPcUnits),100));
			compute(wsaaUtrnSingp, 2).set(div(mult(wsaaSinglePrem,(sub(100,wsaaPcUnits))),100));
			wsaaUnitType.set(SPACES);
			movesUtrn60000();
		}
		/* Create UTRN for Initial*/
		if (isNE(wsaaPcInitUnits,ZERO)) {
			compute(wsaaUtrnSingp, 2).set(div(mult(wsaaInvSingp,wsaaPcInitUnits),100));
			wsaaUnitType.set("I");
			movesUtrn60000();
		}
		/* If Initial Units not 100% then create Accumulation UTRN.*/
		if (isNE(wsaaPcInitUnits,100)) {
			compute(wsaaUtrnSingp, 2).set(div(mult(wsaaInvSingp,(sub(100,wsaaPcInitUnits))),100));
			wsaaUnitType.set("A");
			movesUtrn60000();
		}
		goTo(GotoLabel.utrnLoop31100);
	}

protected void resetProgramCopybook48000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para48000();
				case init48500: 
					init48500();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para48000()
	{
		wsaaTotalSuspense.add(wsaaTotalPremium);
		b5202spDataAreaInner.b5202spInstprem.set(ZERO);
		b5202spDataAreaInner.b5202spEffdate.set(ZERO);
		sub1.set(ZERO);
		x.set(ZERO);
	}

protected void init48500()
	{
		x.add(1);
		if (isGT(x,10)) {
			x.set(ZERO);
			return ;
		}
		b5202spDataAreaInner.b5202spCurrcy[x.toInt()].set(wsaaCurrcy[x.toInt()]);
		b5202spDataAreaInner.b5202spUnitVirtualFund[x.toInt()].set(wsaaUnitVirtualFund[x.toInt()]);
		b5202spDataAreaInner.b5202spUnitAllocPercAmt[x.toInt()].set(wsaaUnitAllPercAmt[x.toInt()]);
		goTo(GotoLabel.init48500);
	}

protected void policyLoad50000()
	{
		/*LOAD-POLICY*/
		if (wsaaWholePlan.isTrue()) {
			readCovr51500();
			if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
				return ;
			}
		}
		/* The header is set to show which selection is being actioned.*/
		if (wsaaWholePlan.isTrue()) {
			if (isEQ(covrmjaIO.getPlanSuffix(),ZERO)) {
				/* Nothing to do. */
			}
		}
		/*EXIT*/
	}

protected void readItem51000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para51000();
				case zeroise51080: 
					zeroise51080();
				case exit51090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Gets Fund Split Plan from T5510.
	* </pre>
	*/
protected void para51000()
	{
		/*    If Fund Option = spaces, zeroise all numeric fields*/
		/*    for use in Standard Split check later, and that's all!*/
		if (isEQ(wsaaFndopt,SPACES)) {
			for (x.set(1); !(isGT(x,10)); x.add(1)){
				zeroise51080();
			}
			goTo(GotoLabel.exit51090);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t5510);
		itdmIO.setItemitem(wsaaFndsplItem);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(),runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5510))
		|| (isNE(itdmIO.getItemitem(),wsaaFndsplItem))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		else {
			t5510rec.t5510Rec.set(itdmIO.getGenarea());
		}
		/*    If table record not found, zeroise all numeric fields*/
		/*    for use in Standard Split check later!*/
		for (x.set(1); !(isGT(x,10)); x.add(1)){
			zeroise51080();
		}
		goTo(GotoLabel.exit51090);
	}

protected void gotIt51060()
	{
		for (x.set(1); !(isGT(x,10)); x.add(1)){
			move51070();
		}
		goTo(GotoLabel.exit51090);
	}

protected void move51070()
	{
		if (isNE(b5202spDataAreaInner.b5202spUnitVirtualFund[x.toInt()], SPACES)) {
			goTo(GotoLabel.zeroise51080);
		}
		b5202spDataAreaInner.b5202spUnitVirtualFund[x.toInt()].set(t5510rec.unitVirtualFund[x.toInt()]);
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(b5202spDataAreaInner.b5202spUnitVirtualFund[x.toInt()]);
		readT551555000();
		b5202spDataAreaInner.b5202spCurrcy[x.toInt()].set(t5515rec.currcode);
		if (isNE(b5202spDataAreaInner.b5202spPercentAmountInd, "A")) {
			b5202spDataAreaInner.b5202spUnitAllocPercAmt[x.toInt()].set(t5510rec.unitPremPercent[x.toInt()]);
		}
	}

protected void zeroise51080()
	{
		t5510rec.unitPremPercent[x.toInt()].set(ZERO);
		t5510rec.unitVirtualFund[x.toInt()].set(SPACES);
	}

protected void readCovr51500()
	{
		/*READ-COVRMJA*/
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(covrmjaIO.getParams());
			databaseError006();
		}
		if (isNE(covrmjaIO.getChdrcoy(),wsaaCovrChdrcoy)
		|| isNE(covrmjaIO.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrmjaIO.getLife(),wsaaCovrLife)
		|| isNE(covrmjaIO.getCoverage(),wsaaCovrCoverage)
		|| isNE(covrmjaIO.getRider(),wsaaCovrRider)
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void readT664753000()
	{
		para53000();
	}

protected void para53000()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		wsaaT6647Trcde.set(wsaaBatckey.batcBatctrcde);
		wsaaT6647Cnttype.set(chdrmjaIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Item);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(),runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Item))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT5537T553654000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para54000: 
					para54000();
				case gotT553754200: 
					gotT553754200();
				case increment54600: 
					increment54600();
				case increment54850: 
					increment54850();
				case n000Loop54500: 
					n000Loop54500();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para54000()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(tablesInner.t5537);
		wsaaT5537Item.set(SPACES);
		wsaaT5537Allbas.set(t5540rec.allbas);
		wsaaT5537Sex.set(covrmjaIO.getSex());
		wsaaBatckey.set(wsaaLinkBatchkey);
		wsaaT5537Trans.set(wsaaBatckey.batcBatctrcde);
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.gotT553754200);
		}
		/*    If NO record found then try first with the basis, * for*/
		/*    Sex and Transaction code.*/
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaT5537Sex.set("*");
			itemIO.setItemitem(wsaaT5537Item);
			SmartFileCode.execute(appVars, itemIO);
			if ((isNE(itemIO.getStatuz(),varcom.oK))
			&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
				conerrrec.params.set(itemIO.getParams());
				databaseError006();
			}
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.gotT553754200);
		}
		/*    If NO record found then try secondly with the basis, Sex*/
		/*    and '****'  Transaction code.*/
		wsaaT5537Sex.set(covrmjaIO.getSex());
		wsaaT5537Trans.fill("*");
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.gotT553754200);
		}
		/*    If NO record found then try first with the basis, * for*/
		/*    Sex and **** for Transaction code.*/
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaT5537Sex.set("*");
			wsaaT5537Trans.fill("*");
		}
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.gotT553754200);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaT5537Trans.fill("*");
		/* If no match is found, try again with the global sex code*/
		wsaaT5537Sex.set("*");
		/*    When doing a global change and the global data is being*/
		/*    moved to the WSAA-T5537-key make sure that you move the*/
		/*    WSAA-T5537-ITEM to the ITEM-ITEMITEM.*/
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.gotT553754200);
		}
		/* And again if no match is found to prevent system falling over*/
		/* read T5537 with global key.*/
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
	}

protected void gotT553754200()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*    Always calculate the age.*/
		calculateAge59600();
		/*Y-ORDINATE*/
		wsaaZ.set(0);
	}

protected void increment54600()
	{
		wsaaZ.add(1);
		/*    IF WSAA-CLTAGE                  > T5537-TOAGE(WSAA-Z) AND    */
		if (isGT(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])
		&& isLT(wsaaZ,10)) {
			goTo(GotoLabel.increment54600);
		}
		/*    IF WSAA-CLTAGE                  < T5537-TOAGE(WSAA-Z) OR     */
		/*                                    = T5537-TOAGE(WSAA-Z)        */
		if (isLT(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])
		|| isEQ(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])) {
			wsaaY.set(wsaaZ);
		}
		else {
			wsaaT5537Item.set(t5537rec.agecont);
			goTo(GotoLabel.para54000);
		}
		/*X-ORDINATE*/
		wsaaZ.set(0);
	}

protected void increment54850()
	{
		wsaaZ.add(1);
		/*    Always calculate the term.*/
		calculateTerm59700();
		wsaaTerm.set(wsaaRiskCessTerm);
		if (isGT(wsaaTerm,t5537rec.toterm[wsaaZ.toInt()])
		&& isLT(wsaaZ,10)) {
			goTo(GotoLabel.increment54850);
		}
		if (isLT(wsaaTerm,t5537rec.toterm[wsaaZ.toInt()])
		|| isEQ(wsaaTerm,t5537rec.toterm[wsaaZ.toInt()])) {
			wsaaX.set(wsaaZ);
		}
		else {
			wsaaT5537Item.set(t5537rec.trmcont);
			goTo(GotoLabel.para54000);
		}
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY,9));
		wsaaIndex.add(wsaaX);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t5536);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		wsaaT5536Item.set(SPACES);
		wsaaT5536Allbas.set(t5537rec.actAllocBasis[1]);
		itdmIO.setItemitem(wsaaT5536Item);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(),runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5536))
		|| (isNE(itdmIO.getItemitem(),wsaaT5536Item))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
		sub1.set(ZERO);
	}

protected void n000Loop54500()
	{
		sub1.add(1);
		if (isGT(sub1,6)) {
			wsaaPcUnits.set(ZERO);
			wsaaPcInitUnits.set(ZERO);
			return ;
		}
		if (isNE(t5536rec.billfreq[sub1.toInt()],"00")) {
			goTo(GotoLabel.n000Loop54500);
		}
		if (isEQ(sub1,1)) {
			wsaaPcUnits.set(t5536rec.pcUnitsa[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsa[1]);
		}
		if (isEQ(sub1,2)) {
			wsaaPcUnits.set(t5536rec.pcUnitsb[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsb[1]);
		}
		if (isEQ(sub1,3)) {
			wsaaPcUnits.set(t5536rec.pcUnitsc[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsc[1]);
		}
		if (isEQ(sub1,4)) {
			wsaaPcUnits.set(t5536rec.pcUnitsd[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsd[1]);
		}
		if (isEQ(sub1,5)) {
			wsaaPcUnits.set(t5536rec.pcUnitse[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitse[1]);
		}
		if (isEQ(sub1,6)) {
			wsaaPcUnits.set(t5536rec.pcUnitsf[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsf[1]);
		}
	}

protected void readT551555000()
	{
		para55000();
	}

protected void para55000()
	{
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setItemitem(wsaaT5515Item);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(),runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5515))
		|| (isNE(itdmIO.getItemitem(),wsaaT5515Item))) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void readT564556000()
	{
		para56000();
	}

protected void para56000()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		wsaaT5645Item.set(SPACES);
		wsaaT5645Prog.set(wsaaProg);
		itemIO.setItemitem(wsaaT5645Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT554557000()
	{
		para57000();
	}

protected void para57000()
	{
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t5545);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		wsaaT5545Item.set(SPACES);
		wsaaT5545Enhall.set(t6647rec.enhall);
		wsaaT5545Currcode.set(t5515rec.currcode);
		itdmIO.setItemitem(wsaaT5545Item);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		t5545rec.t5545Rec.set(itdmIO.getGenarea());
		wsaaPerc.set(0);
		sub1.set(1);
		while ( !((isGT(sub1,6))
		|| (isNE(wsaaPerc,0)))) {
			matchEnhanceBasis58000();
		}
		
	}

protected void matchEnhanceBasis58000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start58000();
					enhana58100();
				case enhanb58200: 
					enhanb58200();
				case enhanc58300: 
					enhanc58300();
				case enhand58400: 
					enhand58400();
				case enhane58500: 
					enhane58500();
				case enhanf58600: 
					enhanf58600();
				case nearExit58800: 
					nearExit58800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start58000()
	{
		/* First match the billing frequency.                              */
		/* We are only dealing with Single Prems here, so check for '00'   */
		if (isEQ(t5545rec.billfreq[sub1.toInt()],SPACES)) {
			goTo(GotoLabel.nearExit58800);
		}
		if (isNE(t5545rec.billfreq[sub1.toInt()],"00")) {
			goTo(GotoLabel.nearExit58800);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.       
	* There are 5 premiums in each billing frequency set, so          
	* match each premium individually.                                
	* </pre>
	*/
protected void enhana58100()
	{
		if (isNE(sub1,1)) {
			goTo(GotoLabel.enhanb58200);
		}
		if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05,0))) {
			wsaaPerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04,0))) {
				wsaaPerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03,0))) {
					wsaaPerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02,0))) {
						wsaaPerc.set(t5545rec.enhanaPc02);
					}
					else {
						/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-01)         <004>*/
						/*    AND (T5545-ENHANA-PRM-01 NOT = 0)                    <004>*/
						wsaaPerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit58800);
	}

protected void enhanb58200()
	{
		if (isNE(sub1,2)) {
			goTo(GotoLabel.enhanc58300);
		}
		/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-05)         <004>*/
		if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05,0))) {
			wsaaPerc.set(t5545rec.enhanbPc05);
		}
		else {
			/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-04)         <004>*/
			if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04,0))) {
				wsaaPerc.set(t5545rec.enhanbPc04);
			}
			else {
				/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-03)         <004>*/
				if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03,0))) {
					wsaaPerc.set(t5545rec.enhanbPc03);
				}
				else {
					/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-02)         <004>*/
					if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02,0))) {
						wsaaPerc.set(t5545rec.enhanbPc02);
					}
					else {
						/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-01)         <004>*/
						/*    AND (T5545-ENHANB-PRM-01 NOT = 0)                    <004>*/
						wsaaPerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit58800);
	}

protected void enhanc58300()
	{
		if (isNE(sub1,3)) {
			goTo(GotoLabel.enhand58400);
		}
		/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-05)         <004>*/
		if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05,0))) {
			wsaaPerc.set(t5545rec.enhancPc05);
		}
		else {
			/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-04)         <004>*/
			if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04,0))) {
				wsaaPerc.set(t5545rec.enhancPc04);
			}
			else {
				/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-03)         <004>*/
				if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03,0))) {
					wsaaPerc.set(t5545rec.enhancPc03);
				}
				else {
					/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-02)         <004>*/
					if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02,0))) {
						wsaaPerc.set(t5545rec.enhancPc02);
					}
					else {
						/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-01)         <004>*/
						/*    AND (T5545-ENHANC-PRM-01 NOT = 0)                    <004>*/
						wsaaPerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit58800);
	}

protected void enhand58400()
	{
		if (isNE(sub1,4)) {
			goTo(GotoLabel.enhane58500);
		}
		/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-05)         <004>*/
		if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05,0))) {
			wsaaPerc.set(t5545rec.enhandPc05);
		}
		else {
			/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-04)         <004>*/
			if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04,0))) {
				wsaaPerc.set(t5545rec.enhandPc04);
			}
			else {
				/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-03)         <004>*/
				if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03,0))) {
					wsaaPerc.set(t5545rec.enhandPc03);
				}
				else {
					/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-02)         <004>*/
					if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02,0))) {
						wsaaPerc.set(t5545rec.enhandPc02);
					}
					else {
						/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-01)         <004>*/
						/*    AND (T5545-ENHAND-PRM-01 NOT = 0)                    <004>*/
						wsaaPerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit58800);
	}

protected void enhane58500()
	{
		if (isNE(sub1,5)) {
			goTo(GotoLabel.enhanf58600);
		}
		/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-05)         <004>*/
		if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05,0))) {
			wsaaPerc.set(t5545rec.enhanePc05);
		}
		else {
			/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-04)         <004>*/
			if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04,0))) {
				wsaaPerc.set(t5545rec.enhanePc04);
			}
			else {
				/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-03)         <004>*/
				if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03,0))) {
					wsaaPerc.set(t5545rec.enhanePc03);
				}
				else {
					/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-02)         <004>*/
					if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02,0))) {
						wsaaPerc.set(t5545rec.enhanePc02);
					}
					else {
						/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-01)         <004>*/
						/*    AND (T5545-ENHANE-PRM-01 NOT = 0)                    <004>*/
						wsaaPerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit58800);
	}

protected void enhanf58600()
	{
		if (isNE(sub1,6)) {
			goTo(GotoLabel.nearExit58800);
		}
		/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-05)         <004>*/
		if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05,0))) {
			wsaaPerc.set(t5545rec.enhanfPc05);
		}
		else {
			/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-04)         <004>*/
			if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04,0))) {
				wsaaPerc.set(t5545rec.enhanfPc04);
			}
			else {
				/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-03)         <004>*/
				if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03,0))) {
					wsaaPerc.set(t5545rec.enhanfPc03);
				}
				else {
					/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-02)         <004>*/
					if ((isGTE(b5202spDataAreaInner.b5202spInstprem, t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02,0))) {
						wsaaPerc.set(t5545rec.enhanfPc02);
					}
					else {
						/* IF (B5202SP-INSTPREM NOT < T5545-ENHANA-PRM-01)         <004>*/
						/*    AND (T5545-ENHANF-PRM-01 NOT = 0)                    <004>*/
						wsaaPerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void nearExit58800()
	{
		sub1.add(1);
		/*EXIT*/
	}

protected void readTableT554359500()
	{
		start59500();
	}

protected void start59500()
	{
		itemIO.setDataArea(SPACES);
		t5543rec.t5543Rec.set(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(tablesInner.t5543);
		wsaaT5543Item.set(SPACES);
		wsaaT5543Fund.set(t5540rec.alfnds);
		itemIO.setItemitem(wsaaT5543Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			t5543rec.t5543Rec.set(itemIO.getGenarea());
		}
	}

protected void calculateAge59600()
	{
		para59600();
	}

protected void para59600()
	{
		/*    MOVE SPACE                  TO DTC3-DATCON3-REC.             */
		/*    MOVE WSAA-CLTDOB            TO DTC3-INT-DATE-1.              */
		/*    MOVE B5202SP-EFFDATE          TO DTC3-INT-DATE-2.            */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/*    IF DTC3-STATUZ NOT = O-K                                     */
		/*    ROUND UP THE AGE*/
		/*         ADD .99999 DTC3-FREQ-FACTOR    GIVING WSAA-CLTAGE       */
		/*    ELSE                                                         */
		/*         MOVE DTC3-FREQ-FACTOR       TO WSAA-CLTAGE.             */
		/* New routine to calculate Age next/nearest/last birthday         */
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCB");
		agecalcrec.language.set(runparmrec.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.intDate1.set(wsaaCltdob);
		agecalcrec.intDate2.set(b5202spDataAreaInner.b5202spEffdate);
		agecalcrec.company.set(runparmrec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(agecalcrec.statuz);
			systemError005();
		}
		wsaaCltage.set(agecalcrec.agerating);
	}

protected void calculateTerm59700()
	{
		/*PARA*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(b5202spDataAreaInner.b5202spEffdate);
		datcon3rec.intDate2.set(wsaaCltdob);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor,wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		/*EXIT*/
	}

protected void movesUtrn60000()
	{
		moveValues60000();
	}

protected void moveValues60000()
	{
		utrnIO.setStatuz(SPACES);
		utrnIO.setDataKey(SPACES);
		utrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		utrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		utrnIO.setLife(covrmjaIO.getLife());
		utrnIO.setCoverage(covrmjaIO.getCoverage());
		utrnIO.setRider(covrmjaIO.getRider());
		utrnIO.setFormat(utrnrec);
		setPrecision(utrnIO.getTranno(), 0);
		utrnIO.setTranno(add(chdrmjaIO.getTranno(),1));
		utrnIO.setTermid(varcom.vrcmTermid);
		utrnIO.setTransactionDate(varcom.vrcmDate);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		utrnIO.setUser(varcom.vrcmUser);
		utrnIO.setBatccoy(runparmrec.company);
		utrnIO.setBatcbrn(runparmrec.branch);
		wsaaBatckey.set(wsaaLinkBatchkey);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		readT664753000();
		utrnIO.setNowDeferInd(t6647rec.aloind);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		if (isEQ(t6647rec.efdcode,"BD")) {
			utrnIO.setMoniesDate(wsaaBusinessDate);
		}
		else {
			if (isEQ(t6647rec.efdcode,"DD")) {
				utrnIO.setMoniesDate(b5202spDataAreaInner.b5202spEffdate);
			}
			else {
				if (isEQ(t6647rec.efdcode,"LO")) {
					utrnIO.setMoniesDate(b5202spDataAreaInner.b5202spEffdate);
				}
				else {
					utrnIO.setMoniesDate(ZERO);
				}
			}
		}
		if (isEQ(t6647rec.efdcode,"LO")
		&& isGT(wsaaBusinessDate, b5202spDataAreaInner.b5202spEffdate)) {
			utrnIO.setMoniesDate(wsaaBusinessDate);
		}
		utrnIO.setCrtable(covrmjaIO.getCrtable());
		utrnIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		utrnIO.setCntcurr(chdrmjaIO.getCntcurr());
		utrnIO.setContractType(chdrmjaIO.getCnttype());
		utrnIO.setCrComDate(b5202spDataAreaInner.b5202spEffdate);
		utrnIO.setUnitVirtualFund(b5202spDataAreaInner.b5202spUnitVirtualFund[index1.toInt()]);
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(b5202spDataAreaInner.b5202spUnitVirtualFund[index1.toInt()]);
		utrnIO.setFundCurrency(b5202spDataAreaInner.b5202spCurrcy[index1.toInt()]);
		readT564556000();
		/* Check T5688 to see if Component level accounting is             */
		/* required.                                                       */
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItempfx("IT");
		/*MOVE SPACES                 TO WSAA-T5645-ITEM.         <002>*/
		/*MOVE WSAA-PROG              TO WSAA-T5645-PROG.         <002>*/
		/*MOVE WSAA-T5645-ITEM        TO ITDM-ITEMITEM.           <002>*/
		/*MOVE PARM-EFFDATE           TO ITDM-ITMFRM.             <002>*/
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(itdmIO.getStatuz());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),runparmrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			/*MOVE WSAA-T5645-ITEM    TO ITDM-ITEMITEM          <002>*/
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(e308);
			databaseError006();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* If UTRN for Initial or Accumulate use the invested account*/
		/* otherwise use the non invested account. (GL, on table T5645)*/
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			if (isEQ(wsaaUnitType,"I")
			|| isEQ(wsaaUnitType,"A")) {
				utrnIO.setSacscode(t5645rec.sacscode[4]);
				utrnIO.setSacstyp(t5645rec.sacstype[4]);
				utrnIO.setGenlcde(t5645rec.glmap[4]);
			}
			else {
				utrnIO.setSacscode(t5645rec.sacscode[5]);
				utrnIO.setSacstyp(t5645rec.sacstype[5]);
				utrnIO.setGenlcde(t5645rec.glmap[5]);
			}
		}
		else {
			if (isEQ(wsaaUnitType,"I")
			|| isEQ(wsaaUnitType,"A")) {
				utrnIO.setSacscode(t5645rec.sacscode[1]);
				utrnIO.setSacstyp(t5645rec.sacstype[1]);
				utrnIO.setGenlcde(t5645rec.glmap[1]);
			}
			else {
				utrnIO.setSacscode(t5645rec.sacscode[2]);
				utrnIO.setSacstyp(t5645rec.sacstype[2]);
				utrnIO.setGenlcde(t5645rec.glmap[2]);
			}
		}
		/*    IF WSAA-UNIT-TYPE           = 'I' OR 'A'                     */
		/*       MOVE T5645-SACSCODE (1)     TO UTRN-SACSCODE              */
		/*       MOVE T5645-SACSTYPE (1)     TO UTRN-SACSTYP               */
		/*       MOVE T5645-GLMAP (1)        TO UTRN-GENLCDE               */
		/*    ELSE                                                         */
		/*       MOVE T5645-SACSCODE (2)     TO UTRN-SACSCODE              */
		/*       MOVE T5645-SACSTYPE (2)     TO UTRN-SACSTYP               */
		/*       MOVE T5645-GLMAP (2)        TO UTRN-GENLCDE               */
		/*    END-IF.                                                      */
		if (isEQ(wsaaUnitType,"I")) {
			utrnIO.setUnitSubAccount("INIT");
		}
		if (isEQ(wsaaUnitType,"A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		if (isEQ(wsaaUnitType,SPACES)) {
			utrnIO.setUnitSubAccount("NVST");
		}
		readT551555000();
		readT554557000();
		if (isEQ(b5202spDataAreaInner.b5202spPercentAmountInd, "P")) {
			setPrecision(utrnIO.getContractAmount(), 3);
			utrnIO.setContractAmount(div(mult(mult(wsaaUtrnSingp, b5202spDataAreaInner.b5202spUnitAllocPercAmt[index1.toInt()]), wsaaPerc), 10000), true);
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 3);
			utrnIO.setContractAmount(div(mult(wsaaUtrnSingp,wsaaPerc),100), true);
		}
		utrnIO.setUnitType(wsaaUnitType);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setSvp(1);
		utrnIO.setSwitchIndicator(SPACES);
		utrnIO.setFeedbackInd(SPACES);
		utrnIO.setTriggerModule(SPACES);
		utrnIO.setTriggerKey(SPACES);
		utrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)
		&& isNE(utrnIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(utrnIO.getParams());
			databaseError006();
		}
	}

protected void defaultPcdt70000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					moveValues70000();
				case addPcdt70200: 
					addPcdt70200();
				case call70300: 
					call70300();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void moveValues70000()
	{
		/*    MOVE CHDRMJA-CHDRCOY        TO PCDTMJA-CHDRCOY.*/
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		pcdtmjaIO.setLife(b5202spDataAreaInner.b5202spLife);
		pcdtmjaIO.setCoverage(b5202spDataAreaInner.b5202spCoverage);
		pcdtmjaIO.setRider(b5202spDataAreaInner.b5202spRider);
		pcdtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		pcdtmjaIO.setInstprem(b5202spDataAreaInner.b5202spInstprem);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		/*MOVE CHDRMJA-CHDRCOY        TO PCDDMJA-CHDRCOY.*/
		/*MOVE CHDRMJA-CHDRNUM        TO PCDDMJA-CHDRNUM.*/
		/*MOVE BEGN                   TO PCDDMJA-FUNCTION.*/
		/*MOVE ZEROES                 TO WSAA-INDEX.*/
		/*70100-PCDD-LOOP.*/
		/*ADD 1                       TO WSAA-INDEX.*/
		/*IF WSAA-INDEX > 10*/
		/*   GO TO 70300-CALL.*/
		/*MOVE PCDDMJAREC             TO PCDDMJA-FORMAT.*/
		/*CALL 'PCDDMJAIO'            USING PCDDMJA-PARAMS.*/
		/*IF PCDDMJA-CHDRNUM          NOT = CHDRMJA-CHDRNUM OR*/
		/*   PCDDMJA-CHDRCOY          NOT = CHDRMJA-CHDRCOY*/
		/*   MOVE ENDP                TO PCDDMJA-STATUZ.*/
		/*IF PCDDMJA-STATUZ           NOT = O-K*/
		/*AND PCDDMJA-STATUZ          NOT = ENDP*/
		/*   MOVE PCDDMJA-PARAMS      TO CONR-PARAMS*/
		/*   PERFORM 006-DATABASE-ERROR.*/
		/*IF PCDDMJA-STATUZ              = ENDP*/
		/*    GO TO 70200-ADD-PCDT.*/
		pcdtmjaIO.setAgntnum(1, chdrmjaIO.getAgntnum());
		pcdtmjaIO.setSplitc(1, 100);
		wsaaIndex.set(2);
	}

	/**
	* <pre>
	*****MOVE NEXTR                  TO PCDDMJA-FUNCTION.
	*****GO TO 70100-PCDD-LOOP.
	* </pre>
	*/
protected void addPcdt70200()
	{
		if (isGT(wsaaIndex,10)) {
			goTo(GotoLabel.call70300);
		}
		pcdtmjaIO.setAgntnum(wsaaIndex, SPACES);
		pcdtmjaIO.setSplitc(wsaaIndex, ZERO);
		wsaaIndex.add(1);
		goTo(GotoLabel.addPcdt70200);
	}

protected void call70300()
	{
		/*    DISPLAY 'ADD PCDT-DATAKEY==>' PCDTMJA-DATA-KEY.*/
		/*    DISPLAY 'PCDTMJA-TRANNO====>' PCDTMJA-TRANNO.*/
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		/*    DISPLAY 'PCDT-STATUZ=======>' PCDTMJA-STATUZ.*/
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(pcdtmjaIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void sftlckCallAt80000()
	{
		softlock80010();
	}

protected void softlock80010()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(runparmrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(sftlockrec.statuz);
			databaseError006();
		}
		/*  Call the at module ATREQ*/
		/*MOVE SPACES               TO  ATRT-ATREQ-REC.*/
		/*MOVE ZERO                 TO  ATRT-ACCT-YEAR,*/
		/*                              ATRT-ACCT-MONTH.*/
		/*MOVE 'P5147AT'            TO  ATRT-MODULE.*/
		/*MOVE WSAA-BATCKEY         TO  ATRT-BATCH-KEY.*/
		/*MOVE WSAA-PROG            TO  ATRT-REQ-PROG.*/
		/*MOVE PARM-COMPANY         TO  ATRT-COMPANY.*/
		/*MOVE VRCM-USER            TO  ATRT-REQ-USER.*/
		/*MOVE VRCM-TERM            TO  ATRT-REQ-TERM.*/
		/*MOVE PARM-EFFDATE         TO  ATRT-REQ-DATE.*/
		/*MOVE VRCM-TIME            TO  ATRT-REQ-TIME.*/
		/*MOVE PARM-LANGUAGE        TO  ATRT-LANGUAGE.*/
		/*MOVE SPACE                TO  WSAA-PRIMARY-KEY.*/
		/*MOVE CHDRMJA-CHDRNUM      TO  WSAA-PRIMARY-CHDRNUM.*/
		/*MOVE WSAA-PRIMARY-KEY     TO  ATRT-PRIMARY-KEY.*/
		/*MOVE VRCM-DATE            TO WSAA-TRANSACTION-DATE.*/
		/*MOVE VRCM-TIME            TO WSAA-TRANSACTION-TIME.*/
		/*MOVE VRCM-USER            TO WSAA-USER.*/
		/*MOVE VRCM-TERM            TO WSAA-TERMID.*/
		/*MOVE PARM-EFFDATE         TO WSAA-EFFDATE.*/
		/*MOVE WSAA-TRANSACTION-REC  TO ATRT-TRANS-AREA.*/
		/*MOVE '****'        TO ATRT-STATUZ.*/
		/*CALL 'ATREQ'       USING  ATRT-ATREQ-REC.*/
		/*IF ATRT-STATUZ     NOT = O-K*/
		/*   MOVE ATRT-ATREQ-REC    TO CONR-PARAMS*/
		/*   PERFORM 006-DATABASE-ERROR.*/
		atmodrec.atmodRec.set(SPACES);
		atmodrec.batchKey.set(wsaaBatckey);
		atmodrec.company.set(runparmrec.company);
		/*  MOVE VRCM-TERM            TO  ATMD-REQTERM.                  */
		atmodrec.reqterm.set(varcom.vrcmTermid);
		atmodrec.language.set(runparmrec.language);
		wsaaPrimaryKey.set(SPACES);
		wsaaPrimaryChdrnum.set(chdrmjaIO.getChdrnum());
		atmodrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		/*  MOVE VRCM-TERM            TO WSAA-TERMID.                    */
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaEffdate.set(runparmrec.effdate);
		atmodrec.transArea.set(wsaaTransactionRec);
		atmodrec.statuz.set("****");
		callProgram(P5147at.class, atmodrec.atmodRec);
		if (isNE(atmodrec.statuz,varcom.oK)) {
			conerrrec.params.set(atmodrec.atmodRec);
			databaseError006();
		}
	}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
	private FixedLengthStringData t5510 = new FixedLengthStringData(5).init("T5510");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5536 = new FixedLengthStringData(5).init("T5536");
	private FixedLengthStringData t5537 = new FixedLengthStringData(5).init("T5537");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5543 = new FixedLengthStringData(5).init("T5543");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
}
/*
 * Class transformed  from Data Structure B5202SP-DATA-AREA--INNER
 */
private static final class B5202spDataAreaInner { 

	private FixedLengthStringData b5202spDataArea = new FixedLengthStringData(430);
		/* B5202SP-DATA-FIELDS */
	private FixedLengthStringData b5202spChdrnum = new FixedLengthStringData(8).isAPartOf(b5202spDataArea, 0);
	private FixedLengthStringData b5202spChdrstatus = new FixedLengthStringData(10).isAPartOf(b5202spDataArea, 8);
	private FixedLengthStringData b5202spCntcurr = new FixedLengthStringData(3).isAPartOf(b5202spDataArea, 18);
	private FixedLengthStringData b5202spCnttype = new FixedLengthStringData(3).isAPartOf(b5202spDataArea, 21);
	private FixedLengthStringData b5202spComind = new FixedLengthStringData(1).isAPartOf(b5202spDataArea, 24);
	private FixedLengthStringData b5202spCoverage = new FixedLengthStringData(2).isAPartOf(b5202spDataArea, 25);
	private FixedLengthStringData b5202spCrtabdesc = new FixedLengthStringData(30).isAPartOf(b5202spDataArea, 27);
	private FixedLengthStringData b5202spCrtable = new FixedLengthStringData(4).isAPartOf(b5202spDataArea, 57);
	private FixedLengthStringData b5202spCtypedes = new FixedLengthStringData(30).isAPartOf(b5202spDataArea, 61);
	private FixedLengthStringData b5202spCurrcys = new FixedLengthStringData(30).isAPartOf(b5202spDataArea, 91);
	private FixedLengthStringData[] b5202spCurrcy = FLSArrayPartOfStructure(10, 3, b5202spCurrcys, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(b5202spCurrcys, 0, FILLER_REDEFINE);
	private FixedLengthStringData b5202spCurrcy01 = new FixedLengthStringData(3).isAPartOf(filler, 0);
	private FixedLengthStringData b5202spCurrcy02 = new FixedLengthStringData(3).isAPartOf(filler, 3);
	private FixedLengthStringData b5202spCurrcy03 = new FixedLengthStringData(3).isAPartOf(filler, 6);
	private FixedLengthStringData b5202spCurrcy04 = new FixedLengthStringData(3).isAPartOf(filler, 9);
	private FixedLengthStringData b5202spCurrcy05 = new FixedLengthStringData(3).isAPartOf(filler, 12);
	private FixedLengthStringData b5202spCurrcy06 = new FixedLengthStringData(3).isAPartOf(filler, 15);
	private FixedLengthStringData b5202spCurrcy07 = new FixedLengthStringData(3).isAPartOf(filler, 18);
	private FixedLengthStringData b5202spCurrcy08 = new FixedLengthStringData(3).isAPartOf(filler, 21);
	private FixedLengthStringData b5202spCurrcy09 = new FixedLengthStringData(3).isAPartOf(filler, 24);
	private FixedLengthStringData b5202spCurrcy10 = new FixedLengthStringData(3).isAPartOf(filler, 27);
	private ZonedDecimalData b5202spEffdate = new ZonedDecimalData(8, 0).isAPartOf(b5202spDataArea, 121);
	private FixedLengthStringData b5202spVirtFundSplitMethod = new FixedLengthStringData(4).isAPartOf(b5202spDataArea, 129);
	private ZonedDecimalData b5202spInstprem = new ZonedDecimalData(9, 2).isAPartOf(b5202spDataArea, 133);
	private FixedLengthStringData b5202spJlifename = new FixedLengthStringData(47).isAPartOf(b5202spDataArea, 142);
	private FixedLengthStringData b5202spJlife = new FixedLengthStringData(8).isAPartOf(b5202spDataArea, 189);
	private FixedLengthStringData b5202spLife = new FixedLengthStringData(2).isAPartOf(b5202spDataArea, 197);
	private FixedLengthStringData b5202spLifename = new FixedLengthStringData(47).isAPartOf(b5202spDataArea, 199);
	private FixedLengthStringData b5202spLifenum = new FixedLengthStringData(8).isAPartOf(b5202spDataArea, 246);
	private ZonedDecimalData b5202spNumpols = new ZonedDecimalData(4, 0).isAPartOf(b5202spDataArea, 254);
	private FixedLengthStringData b5202spPercentAmountInd = new FixedLengthStringData(1).isAPartOf(b5202spDataArea, 258);
	private ZonedDecimalData b5202spPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(b5202spDataArea, 259);
	private FixedLengthStringData b5202spPremstatus = new FixedLengthStringData(10).isAPartOf(b5202spDataArea, 263);
	private FixedLengthStringData b5202spRegister = new FixedLengthStringData(3).isAPartOf(b5202spDataArea, 273);
	private FixedLengthStringData b5202spRider = new FixedLengthStringData(2).isAPartOf(b5202spDataArea, 276);
	private ZonedDecimalData b5202spSusamt = new ZonedDecimalData(11, 2).isAPartOf(b5202spDataArea, 278);
	private ZonedDecimalData b5202spTotprem = new ZonedDecimalData(11, 2).isAPartOf(b5202spDataArea, 289);
	private FixedLengthStringData b5202spUnitAllocPercAmts = new FixedLengthStringData(90).isAPartOf(b5202spDataArea, 300);
	private ZonedDecimalData[] b5202spUnitAllocPercAmt = ZDArrayPartOfStructure(10, 9, 2, b5202spUnitAllocPercAmts, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(90).isAPartOf(b5202spUnitAllocPercAmts, 0, FILLER_REDEFINE);
	private ZonedDecimalData b5202spUnitAllocPercAmt01 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 0);
	private ZonedDecimalData b5202spUnitAllocPercAmt02 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 9);
	private ZonedDecimalData b5202spUnitAllocPercAmt03 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 18);
	private ZonedDecimalData b5202spUnitAllocPercAmt04 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 27);
	private ZonedDecimalData b5202spUnitAllocPercAmt05 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 36);
	private ZonedDecimalData b5202spUnitAllocPercAmt06 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 45);
	private ZonedDecimalData b5202spUnitAllocPercAmt07 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 54);
	private ZonedDecimalData b5202spUnitAllocPercAmt08 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 63);
	private ZonedDecimalData b5202spUnitAllocPercAmt09 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 72);
	private ZonedDecimalData b5202spUnitAllocPercAmt10 = new ZonedDecimalData(9, 2).isAPartOf(filler1, 81);
	private FixedLengthStringData b5202spUnitVirtualFunds = new FixedLengthStringData(40).isAPartOf(b5202spDataArea, 390);
	private FixedLengthStringData[] b5202spUnitVirtualFund = FLSArrayPartOfStructure(10, 4, b5202spUnitVirtualFunds, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(b5202spUnitVirtualFunds, 0, FILLER_REDEFINE);
	private FixedLengthStringData b5202spUnitVirtualFund01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	private FixedLengthStringData b5202spUnitVirtualFund02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	private FixedLengthStringData b5202spUnitVirtualFund03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	private FixedLengthStringData b5202spUnitVirtualFund04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	private FixedLengthStringData b5202spUnitVirtualFund05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	private FixedLengthStringData b5202spUnitVirtualFund06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	private FixedLengthStringData b5202spUnitVirtualFund07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	private FixedLengthStringData b5202spUnitVirtualFund08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	private FixedLengthStringData b5202spUnitVirtualFund09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	private FixedLengthStringData b5202spUnitVirtualFund10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
}
}
