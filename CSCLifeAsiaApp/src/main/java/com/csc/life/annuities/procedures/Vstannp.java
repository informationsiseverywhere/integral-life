/*
 * File: Vstannp.java
 * Date: 30 August 2009 2:54:27
 * Author: Quipoz Limited
 * 
 * Class transformed from VSTANNP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.annuities.recordstructures.Vstannpcpy;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This subroutine forms part  of  the 9405 Annuities Development.
* It will be called as part  of  the  AT processing to register
* vesting on a deferred annuity and will create accounting
* movements using the VSTANNP entry on T5645.
*
* The only accounting required during vesting  is  an  entry  to
* zeroise the ACBL for reversionary bonus, if any exists for the
* contract.
*      Line No.       Description
*      =======        ===========
*      1              Posting of amount required to zeroise
*                     reversionary bonus.
*
* The  amount to be posted will be obtained from the VSTD record
* for amount type B, reversionary bonus.
*
*****************************************************************
* </pre>
*/
public class Vstannp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VSTANNP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaAmnt = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* TABLES */
	private String t5645 = "T5645";
	private String t1688 = "T1688";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Varcom varcom = new Varcom();
	private Vstannpcpy vstannpcpy = new Vstannpcpy();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endProgram1990, 
		errorProg610
	}

	public Vstannp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vstannpcpy.rec = convertAndSetParam(vstannpcpy.rec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1010();
				}
				case endProgram1990: {
					endProgram1990();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
	{
		wsaaSequenceNo.set(ZERO);
		wsaaAmnt.set(ZERO);
		wsaaRldgacct.set(SPACES);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(vstannpcpy.batckey);
		for (wsaaSub.set(1); !(isGT(wsaaSub,6)); wsaaSub.add(1)){
			if (isEQ(vstannpcpy.type[wsaaSub.toInt()],"B")) {
				wsaaAmnt.set(vstannpcpy.amnt[wsaaSub.toInt()]);
			}
		}
		if (isEQ(wsaaAmnt,ZERO)) {
			goTo(GotoLabel.endProgram1990);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(vstannpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(vstannpcpy.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(vstannpcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
		writePosting2000();
	}

protected void endProgram1990()
	{
		exitProgram();
	}

protected void writePosting2000()
	{
		writeLifacmv2010();
	}

protected void writeLifacmv2010()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(vstannpcpy.chdrcoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.rdocnum.set(vstannpcpy.chdrnum);
		lifacmvrec.tranno.set(vstannpcpy.tranno);
		lifacmvrec.rldgcoy.set(vstannpcpy.chdrcoy);
		lifacmvrec.origcurr.set(vstannpcpy.cntcurr);
		lifacmvrec.tranref.set(vstannpcpy.chdrnum);
		lifacmvrec.trandesc.set(wsaaLongdesc);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.genlcoy.set(vstannpcpy.chdrcoy);
		lifacmvrec.genlcur.set(vstannpcpy.cntcurr);
		lifacmvrec.effdate.set(vstannpcpy.effdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(vstannpcpy.user);
		lifacmvrec.termid.set(vstannpcpy.termid);
		lifacmvrec.substituteCode[1].set(vstannpcpy.cnttype);
		lifacmvrec.substituteCode[6].set(vstannpcpy.crtable);
		wsaaRldgChdrnum.set(vstannpcpy.chdrnum);
		wsaaPlan.set(vstannpcpy.planSuffix);
		wsaaRldgLife.set(vstannpcpy.life);
		wsaaRldgCoverage.set(vstannpcpy.coverage);
		wsaaRldgRider.set(vstannpcpy.rider);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.origamt.set(wsaaAmnt);
		lifacmvrec.acctamt.set(wsaaAmnt);
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		lifacmvrec.contot.set(t5645rec.cnttot[1]);
		postRecord2100();
	}

protected void postRecord2100()
	{
		/*WRITE-LIFACMV*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		vstannpcpy.statuz.set(varcom.bomb);
		exitProgram();
	}
}
