package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5237screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 2, 1, 2, 3}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 19, 6, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5237ScreenVars sv = (S5237ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5237screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5237screensfl, 
			sv.S5237screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5237ScreenVars sv = (S5237ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5237screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5237ScreenVars sv = (S5237ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5237screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5237screensflWritten.gt(0))
		{
			sv.s5237screensfl.setCurrentIndex(0);
			sv.S5237screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5237ScreenVars sv = (S5237ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5237screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5237ScreenVars screenVars = (S5237ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.rgpynum.setFieldName("rgpynum");
				screenVars.pymt.setFieldName("pymt");
				screenVars.rgpytype.setFieldName("rgpytype");
				screenVars.rptldesc.setFieldName("rptldesc");
				screenVars.hrrn.setFieldName("hrrn");
				screenVars.hflag.setFieldName("hflag");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.rgpynum.set(dm.getField("rgpynum"));
			screenVars.pymt.set(dm.getField("pymt"));
			screenVars.rgpytype.set(dm.getField("rgpytype"));
			screenVars.rptldesc.set(dm.getField("rptldesc"));
			screenVars.hrrn.set(dm.getField("hrrn"));
			screenVars.hflag.set(dm.getField("hflag"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5237ScreenVars screenVars = (S5237ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.rgpynum.setFieldName("rgpynum");
				screenVars.pymt.setFieldName("pymt");
				screenVars.rgpytype.setFieldName("rgpytype");
				screenVars.rptldesc.setFieldName("rptldesc");
				screenVars.hrrn.setFieldName("hrrn");
				screenVars.hflag.setFieldName("hflag");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("rgpynum").set(screenVars.rgpynum);
			dm.getField("pymt").set(screenVars.pymt);
			dm.getField("rgpytype").set(screenVars.rgpytype);
			dm.getField("rptldesc").set(screenVars.rptldesc);
			dm.getField("hrrn").set(screenVars.hrrn);
			dm.getField("hflag").set(screenVars.hflag);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5237screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5237ScreenVars screenVars = (S5237ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.rgpynum.clearFormatting();
		screenVars.pymt.clearFormatting();
		screenVars.rgpytype.clearFormatting();
		screenVars.rptldesc.clearFormatting();
		screenVars.hrrn.clearFormatting();
		screenVars.hflag.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5237ScreenVars screenVars = (S5237ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.rgpynum.setClassString("");
		screenVars.pymt.setClassString("");
		screenVars.rgpytype.setClassString("");
		screenVars.rptldesc.setClassString("");
		screenVars.hrrn.setClassString("");
		screenVars.hflag.setClassString("");
	}

/**
 * Clear all the variables in S5237screensfl
 */
	public static void clear(VarModel pv) {
		S5237ScreenVars screenVars = (S5237ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.rgpynum.clear();
		screenVars.pymt.clear();
		screenVars.rgpytype.clear();
		screenVars.rptldesc.clear();
		screenVars.hrrn.clear();
		screenVars.hflag.clear();
	}
}
