package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:11
 * Description:
 * Copybook name: VSTDBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vstdbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vstdbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData vstdbrkKey = new FixedLengthStringData(64).isAPartOf(vstdbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData vstdbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(vstdbrkKey, 0);
  	public FixedLengthStringData vstdbrkChdrnum = new FixedLengthStringData(8).isAPartOf(vstdbrkKey, 1);
  	public PackedDecimalData vstdbrkTranno = new PackedDecimalData(5, 0).isAPartOf(vstdbrkKey, 9);
  	public PackedDecimalData vstdbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(vstdbrkKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(vstdbrkKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vstdbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vstdbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}