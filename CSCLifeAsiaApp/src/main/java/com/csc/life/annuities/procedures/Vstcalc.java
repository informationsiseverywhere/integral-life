/*
 * File: Vstcalc.java
 * Date: 30 August 2009 2:54:31
 * Author: Quipoz Limited
 * 
 * Class transformed from VSTCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.annuities.recordstructures.Vcalcpy;
import com.csc.life.annuities.tablestructures.T6623rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
* This is a subroutine  which  forms  part  of the 9405  Annuities
* Development  and  is  a component  of the  Vesting  Registration
* transaction.  It  is  called  from  P5232, the  Annuity  Details
* screen if any of the four following values are changed: advance,
* arrears, guaranteed period and frequency.
* The subroutine reads T6623 and searches for a table match on the
* aforementioned  values for both  the  old and new  ANNY  values.
* If a match if found for both, the benefit amount is recalculated
* and passed  back to P5232 ( and therefore P5221 ). If a match is
* not  found for either of  the set of values, a status  is passed
* back so that the user is forced to adjust  the values  manually.
*
*****************************************************************
* </pre>
*/
public class Vstcalc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VSTCALC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaOldFactor = new ZonedDecimalData(8, 3);
	private ZonedDecimalData wsaaNewFactor = new ZonedDecimalData(8, 3);
	private ZonedDecimalData wsaaCalcFactor = new ZonedDecimalData(15, 10);
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaOldMatchFound = "";
	private String wsaaNewMatchFound = "";
	private String wsaaNextCheck = "";

	private FixedLengthStringData wsaaItemKey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItemKey, 0);
	private ZonedDecimalData wsaaAge = new ZonedDecimalData(2, 0).isAPartOf(wsaaItemKey, 4).setUnsigned();
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItemKey, 6);
		/* TABLES */
	private String t6623 = "T6623";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6623rec t6623rec = new T6623rec();
	private Varcom varcom = new Varcom();
	private Vcalcpy vcalcpy = new Vcalcpy();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3009, 
		exit3109, 
		errorProg610
	}

	public Vstcalc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vcalcpy.rec = convertAndSetParam(vcalcpy.rec, parmArray, 0);
		try {
			initialize1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialize1000()
	{
		/*INITIALISE*/
		syserrrec.subrname.set(wsaaSubr);
		wsaaOldMatchFound = "N";
		wsaaNewMatchFound = "N";
		wsaaNextCheck = "N";
		wsaaOldFactor.set(ZERO);
		wsaaNewFactor.set(ZERO);
		begnT6623Read2000();
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaOldMatchFound,"Y"))) {
			oldValues3000();
		}
		
		if (isEQ(wsaaOldMatchFound,"Y")) {
			begnT6623Read2000();
			while ( !(isEQ(itemIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaNewMatchFound,"Y"))) {
				newValues3100();
			}
			
		}
		end4000();
	}

protected void begnT6623Read2000()
	{
		begin2010();
	}

protected void begin2010()
	{
		wsaaCrtable.set(vcalcpy.crtable);
		wsaaAge.set(vcalcpy.age);
		wsaaSex.set(vcalcpy.sex);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vcalcpy.chdrcoy);
		itemIO.setItemtabl(t6623);
		itemIO.setItemitem(wsaaItemKey);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMPFX", "ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getItempfx(),"IT")
		|| isNE(itemIO.getItemcoy(),vcalcpy.chdrcoy)
		|| isNE(itemIO.getItemtabl(),t6623)
		|| isNE(itemIO.getItemitem(),wsaaItemKey)
		|| isEQ(itemIO.getStatuz(),varcom.endp)) {
			itemIO.setStatuz(varcom.endp);
		}
		else {
			t6623rec.t6623Rec.set(itemIO.getGenarea());
		}
	}

protected void oldValues3000()
	{
		try {
			checks3000();
		}
		catch (GOTOException e){
		}
	}

protected void checks3000()
	{
		for (wsaaSub.set(1); !((isGT(wsaaSub,10)
		|| isEQ(wsaaOldMatchFound,"Y"))); wsaaSub.add(1)){
			t6623OldMatch3010();
		}
		if (isEQ(wsaaOldMatchFound,"Y")) {
			goTo(GotoLabel.exit3009);
		}
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3009);
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getItempfx(),"IT")
		|| isNE(itemIO.getItemcoy(),vcalcpy.chdrcoy)
		|| isNE(itemIO.getItemtabl(),t6623)
		|| isNE(itemIO.getItemitem(),wsaaItemKey)) {
			itemIO.setStatuz(varcom.endp);
		}
		else {
			t6623rec.t6623Rec.set(itemIO.getGenarea());
		}
	}

protected void t6623OldMatch3010()
	{
		/*CHECK-FOR-MATCH*/
		wsaaNextCheck = "N";
		if ((isEQ(vcalcpy.oldFreqann,t6623rec.freqann[wsaaSub.toInt()]))
		&& (isEQ(vcalcpy.oldGuarperd,t6623rec.guarperd[wsaaSub.toInt()]))) {
			wsaaNextCheck = "Y";
		}
		if (isEQ(wsaaNextCheck,"Y")) {
			if (((isEQ(vcalcpy.oldArrears,SPACES))
			&& (isEQ(t6623rec.arrears[wsaaSub.toInt()],SPACES))
			&& (isNE(vcalcpy.oldAdvance,SPACES))
			&& (isNE(t6623rec.advance[wsaaSub.toInt()],SPACES)))
			|| ((isNE(vcalcpy.oldArrears,SPACES))
			&& (isNE(t6623rec.arrears[wsaaSub.toInt()],SPACES))
			&& (isEQ(vcalcpy.oldAdvance,SPACES))
			&& (isEQ(t6623rec.advance[wsaaSub.toInt()],SPACES)))) {
				wsaaOldFactor.set(t6623rec.comtfact[wsaaSub.toInt()]);
				wsaaOldMatchFound = "Y";
			}
		}
		/*EXIT*/
	}

protected void newValues3100()
	{
		try {
			checks3100();
		}
		catch (GOTOException e){
		}
	}

protected void checks3100()
	{
		for (wsaaSub.set(1); !((isGT(wsaaSub,10)
		|| isEQ(wsaaNewMatchFound,"Y"))); wsaaSub.add(1)){
			t6623NewMatch3110();
		}
		if (isEQ(wsaaNewMatchFound,"Y")) {
			goTo(GotoLabel.exit3109);
		}
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3109);
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getItempfx(),"IT")
		|| isNE(itemIO.getItemcoy(),vcalcpy.chdrcoy)
		|| isNE(itemIO.getItemtabl(),t6623)
		|| isNE(itemIO.getItemitem(),wsaaItemKey)) {
			itemIO.setStatuz(varcom.endp);
		}
		else {
			t6623rec.t6623Rec.set(itemIO.getGenarea());
		}
	}

protected void t6623NewMatch3110()
	{
		/*CHECK-FOR-MATCH*/
		wsaaNextCheck = "N";
		if ((isEQ(vcalcpy.newFreqann,t6623rec.freqann[wsaaSub.toInt()]))
		&& (isEQ(vcalcpy.newGuarperd,t6623rec.guarperd[wsaaSub.toInt()]))) {
			wsaaNextCheck = "Y";
		}
		if (isEQ(wsaaNextCheck,"Y")) {
			if (((isEQ(vcalcpy.newArrears,SPACES))
			&& (isEQ(t6623rec.arrears[wsaaSub.toInt()],SPACES))
			&& (isNE(vcalcpy.newAdvance,SPACES))
			&& (isNE(t6623rec.advance[wsaaSub.toInt()],SPACES)))
			|| ((isNE(vcalcpy.newArrears,SPACES))
			&& (isNE(t6623rec.arrears[wsaaSub.toInt()],SPACES))
			&& (isEQ(vcalcpy.newAdvance,SPACES))
			&& (isEQ(t6623rec.advance[wsaaSub.toInt()],SPACES)))) {
				wsaaNewFactor.set(t6623rec.comtfact[wsaaSub.toInt()]);
				wsaaNewMatchFound = "Y";
			}
		}
		/*EXIT*/
	}

protected void end4000()
	{
		/*ENDS-IT*/
		if (isNE(wsaaOldMatchFound,"Y")
		|| isNE(wsaaNewMatchFound,"Y")) {
			vcalcpy.statuz.set(varcom.mrnf);
		}
		else {
			compute(wsaaCalcFactor, 11).setRounded(div(wsaaOldFactor,wsaaNewFactor));
			compute(vcalcpy.newAmount, 11).setRounded(mult(vcalcpy.oldAmount,wsaaCalcFactor));
			vcalcpy.statuz.set(varcom.oK);
		}
		/*LEAVE*/
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatal600();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatal600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"4")) {
			syserrrec.syserrType.set("3");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		vcalcpy.statuz.set("BOMB");
		exitProgram();
	}
}
