package com.csc.life.annuities.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:10
 * Description:
 * Copybook name: VSTAREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vstarec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData vestingRec = new FixedLengthStringData(196);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(vestingRec, 0);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(vestingRec, 1);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(vestingRec, 9);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(vestingRec, 12);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(vestingRec, 15);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(vestingRec, 17);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(vestingRec, 19);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(vestingRec, 21);
  	public ZonedDecimalData crrcd = new ZonedDecimalData(8, 0).isAPartOf(vestingRec, 25);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(vestingRec, 33);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(vestingRec, 41);
  	public FixedLengthStringData actualValues = new FixedLengthStringData(54).isAPartOf(vestingRec, 42);
  	public PackedDecimalData[] actualVal = PDArrayPartOfStructure(6, 17, 2, actualValues, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(actualValues, 0, FILLER_REDEFINE);
  	public PackedDecimalData actualVal01 = new PackedDecimalData(17, 2).isAPartOf(filler, 0);
  	public PackedDecimalData actualVal02 = new PackedDecimalData(17, 2).isAPartOf(filler, 9);
  	public PackedDecimalData actualVal03 = new PackedDecimalData(17, 2).isAPartOf(filler, 18);
  	public PackedDecimalData actualVal04 = new PackedDecimalData(17, 2).isAPartOf(filler, 27);
  	public PackedDecimalData actualVal05 = new PackedDecimalData(17, 2).isAPartOf(filler, 36);
  	public PackedDecimalData actualVal06 = new PackedDecimalData(17, 2).isAPartOf(filler, 45);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(vestingRec, 96);
  	public PackedDecimalData actvalTotal = new PackedDecimalData(18, 2).isAPartOf(vestingRec, 99);
  	public FixedLengthStringData chdrCurr = new FixedLengthStringData(3).isAPartOf(vestingRec, 109);
  	public FixedLengthStringData chdrType = new FixedLengthStringData(3).isAPartOf(vestingRec, 112);
  	public FixedLengthStringData matCalcMeth = new FixedLengthStringData(4).isAPartOf(vestingRec, 115);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(2).isAPartOf(vestingRec, 119);
  	public FixedLengthStringData types = new FixedLengthStringData(6).isAPartOf(vestingRec, 121);
  	public FixedLengthStringData[] type = FLSArrayPartOfStructure(6, 1, types, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(types, 0, FILLER_REDEFINE);
  	public FixedLengthStringData type01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData type02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData type03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData type04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData type05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData type06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(vestingRec, 127);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(vestingRec, 191);
  	public ZonedDecimalData planSwitch = new ZonedDecimalData(1, 0).isAPartOf(vestingRec, 195).setUnsigned();


	public void initialize() {
		COBOLFunctions.initialize(vestingRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vestingRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}