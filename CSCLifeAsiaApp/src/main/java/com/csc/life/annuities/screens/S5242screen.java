package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5242screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 22, 27, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5242ScreenVars sv = (S5242ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5242screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5242ScreenVars screenVars = (S5242ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.bankdesc.setClassString("");
		screenVars.branchdesc.setClassString("");
		screenVars.bankaccdsc.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.facthous.setClassString("");
		screenVars.numsel.setClassString("");
		screenVars.bankCd.setClassString("");
		screenVars.branchCd.setClassString("");
	}

/**
 * Clear all the variables in S5242screen
 */
	public static void clear(VarModel pv) {
		S5242ScreenVars screenVars = (S5242ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.bankkey.clear();
		screenVars.bankacckey.clear();
		screenVars.bankdesc.clear();
		screenVars.branchdesc.clear();
		screenVars.bankaccdsc.clear();
		screenVars.payrnum.clear();
		screenVars.currcode.clear();
		screenVars.facthous.clear();
		screenVars.numsel.clear();
		screenVars.bankCd.clear();
		screenVars.branchCd.clear();
	}
}
