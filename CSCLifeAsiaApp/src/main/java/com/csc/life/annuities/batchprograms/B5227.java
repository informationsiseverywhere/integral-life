/*
 * File: B5227.java
 * Date: 29 August 2009 21:00:52
 * Author: Quipoz Limited
 *
 * Class transformed from B5227.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.terminationclaims.reports.R5227Report;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*      OVERVIEW  OF THE PROGRAM
*      ========================
*
*
* This is a batch program which forms part of the 9405 Annuities
* Development.  It is called during the Regular Payment          s
* Batch job and its purpose is to produce Certificate of Existence
* Letter triggers and a report listing contracts where these
* letter triggers have been produced.
*
* When the Certificate of Existence Date plus the number of Lead
* Days is equal to or earlier than the effective date
* of the batch run, a LETC record will be created for the
* Certificate of Existence.
*
* A NOTE ON LEAD DAYS: Lead days are set up in the process
* definition screen for B5227, as the third run parameter.
* IT IS IMPORTANT to note that this is set up as a THREE bite
* field:
*    i.e. a lead time of '15' is WRONG
*    INSTEAD TYPE:       '015'
* If this is not done an error may occur.
*
* Each Regular Payment record will be further screened by checking
* its payment status against the allowable codes on T6693.
*
* The Next Certificate of Existence date will be advanced by one
* frequency from T6625.
*
* A report will be produced listing those contracts for which a
* LETC record has been produced.
*
* The letter type for the LETC record will be found by reading the
* Automatic Letters table, T6634, with the contract type and the
* transaction code for this batch process.
*
* The status of the Regular Payment (REGP) record will be updated
* in accordance with the values held on T6693 for the current
* status, coverage code (or ****) and the transaction.
*
*
* INPUTS AND OUTPUTS
* ==================
*
* Inputs:
*
* T6693
* T6625
* T6634
*
* Outputs:
*
* SQL-REGPREC.
*      SQL-CHDRCOY
*      SQL-CHDRNUM
*      .
*      .
*      .    All fields from REGP used
*      .    in program.
*      .
* Control Totals
* ==============
*
* CT01 REGP's selected
* CT02 REGP's updated
* CT03 LETC's written
* CT04 REGP's rejected
*
*****************************************************************
* </pre>
*/
public class B5227 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private R5227Report printerFile = new R5227Report();
	private R5227Report printerFile02 = new R5227Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData printerRec02 = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5227");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final String descrec = "DESCREC";
	private static final String regprec = "REGPREC";
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* TABLES */
	private static final String t1693 = "T1693";
	private static final String t1692 = "T1692";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaOverflowXpt = new FixedLengthStringData(1).init("Y");
	private Validator newXptPageReq = new Validator(wsaaOverflowXpt, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/* Main, standard page headings*/
	private FixedLengthStringData r5227H01 = new FixedLengthStringData(91);
	private FixedLengthStringData r5227h01O = new FixedLengthStringData(91).isAPartOf(r5227H01, 0);
	private FixedLengthStringData rh01Effdate = new FixedLengthStringData(10).isAPartOf(r5227h01O, 0);
	private FixedLengthStringData r5227Jnumb = new FixedLengthStringData(8).isAPartOf(r5227h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5227h01O, 18);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5227h01O, 19);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r5227h01O, 49);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r5227h01O, 59);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r5227h01O, 61);

	private FixedLengthStringData r5227H02 = new FixedLengthStringData(91);
	private FixedLengthStringData r5227h02O = new FixedLengthStringData(91).isAPartOf(r5227H02, 0);
	private FixedLengthStringData rh02Effdate = new FixedLengthStringData(10).isAPartOf(r5227h02O, 0);
	private FixedLengthStringData r537001Jnumb = new FixedLengthStringData(8).isAPartOf(r5227h02O, 10);
	private FixedLengthStringData rh02Company = new FixedLengthStringData(1).isAPartOf(r5227h02O, 18);
	private FixedLengthStringData rh02Companynm = new FixedLengthStringData(30).isAPartOf(r5227h02O, 19);
	private FixedLengthStringData rh02Sdate = new FixedLengthStringData(10).isAPartOf(r5227h02O, 49);
	private FixedLengthStringData rh02Branch = new FixedLengthStringData(2).isAPartOf(r5227h02O, 59);
	private FixedLengthStringData rh02Branchnm = new FixedLengthStringData(30).isAPartOf(r5227h02O, 61);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T6693rec t6693rec = new T6693rec();
	private T6625rec t6625rec = new T6625rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private R5227D01Inner r5227D01Inner = new R5227D01Inner();
	private R5227D02Inner r5227D02Inner = new R5227D02Inner();
	private WsaaFlagsSubsStoresEtcInner wsaaFlagsSubsStoresEtcInner = new WsaaFlagsSubsStoresEtcInner();

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	
	private Map<String, List<Itempf>> t6693Map = null;
	private Map<String, List<Itempf>> t6625Map = null;
	private Map<String, List<Itempf>> tr384Map = null;
	private List<Regppf> updateRegpList = null;
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int intBatchID = 0;
	private int intBatchExtractSize;
	private Iterator<Regppf> iteratorList;
	private Regppf sqlRegppfInner;
	private Map<String, List<Chdrpf>> chdrMap;
	private Map<String, List<Regppf>> regpMap;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2060,
		exit2090
	}

	public B5227() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		wsaaFlagsSubsStoresEtcInner.wsaaLastChdrcoy.set(ZERO);
		wsaaFlagsSubsStoresEtcInner.wsaaLastChdrnum.set(ZERO);
		wsaaFlagsSubsStoresEtcInner.wsaaLastLife.set(ZERO);
		wsaaFlagsSubsStoresEtcInner.wsaaLastCoverage.set(ZERO);
		wsaaFlagsSubsStoresEtcInner.wsaaLastRider.set(ZERO);
		/* Set up Batch Key:*/
		wsaaBatckey.batcKey.set(batcdorrec.batchkey);
		wsaaBatckey.batcBatctrcde.set(bprdIO.getAuthCode());
		printerFile.openOutput();
		printerFile02.openOutput();
		wsspEdterror.set(varcom.oK);
		/* Set-up-Heading-Company.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh02Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
		rh02Companynm.set(descIO.getLongdesc());
		/* Set-up-Heading-Branch.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		/* MOVE BPRD-DEFAULT-BRANCH    TO DESC-DESCITEM.                */
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/* MOVE BPRD-DEFAULT-BRANCH    TO RH01-BRANCH                   */
		rh01Branch.set(bsprIO.getDefaultBranch());
		rh02Branch.set(bsprIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
		rh02Branchnm.set(descIO.getLongdesc());
		/* Set-Up-Heading-dates.*/
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		rh01Effdate.set(datcon1rec.extDate);
		rh02Effdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		rh01Sdate.set(datcon1rec.extDate);
		rh02Sdate.set(datcon1rec.extDate);
		/*  Select-REGP-RECORDS.*/
		/*  Define the query required by declaring a cursor. Only those*/
		/*  records where the CoE date is plus than the effective date*/
		/*  of the batch job are required.*/
		/*  Note that BPRD-BPSYSPAR(3) is the lead time amount defined*/
		/*  on the process definition screen.*/
		wsaaFlagsSubsStoresEtcInner.wsaaTemporaryDate.set(bprdIO.getBpsyspar(3));
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(wsaaFlagsSubsStoresEtcInner.wsaaTempDtNum);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaFlagsSubsStoresEtcInner.wsaaDate.set(datcon2rec.intDate2);
		wsaaFlagsSubsStoresEtcInner.wsaaSqlChdrcoy.set(bsprIO.getCompany());
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		readChunkRecord();
		t6693Map = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), "T6693");
		t6625Map = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), "T6625");
		tr384Map = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), "TR384");
	}

private void readChunkRecord() {
	List<Regppf> regppfList = regppfDAO.searchRegppfRecord(wsaaFlagsSubsStoresEtcInner.wsaaSqlChdrcoy.toString(), wsaaFlagsSubsStoresEtcInner.wsaaDate.toInt(), wsaaFlagsSubsStoresEtcInner.wsaaValidFlag.toString(), getAppVars().getTableNameOverriden("REGPPF"),
			intBatchExtractSize, intBatchID);
	iteratorList = regppfList.iterator();
	if (regppfList != null && !regppfList.isEmpty()) {
		List<String> chdrnumSet = new ArrayList<>();
		for (Regppf r : regppfList) {
			chdrnumSet.add(r.getChdrnum());
		}
		chdrMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumSet);
		regpMap = regppfDAO.searchRegppfByChdrnum(chdrnumSet);
	}
}
protected void readFile2000()
	{
		if (iteratorList != null && iteratorList.hasNext()) {
			sqlRegppfInner = iteratorList.next();
		} else {
			intBatchID++;
			clearList2100();
			readChunkRecord();
			if (iteratorList != null && iteratorList.hasNext()) {
				sqlRegppfInner = iteratorList.next();			
			} else {
				wsspEdterror.set(varcom.endp);
				return ;
			}
		}
	}
private void clearList2100() {
	if (chdrMap != null) {
		chdrMap.clear();
	}
	if(regpMap!=null){
		regpMap.clear();
	}
}
protected void edit2500()
	{
		wsaaFlagsSubsStoresEtcInner.wsaaNewLetter.set("N");
		wsaaFlagsSubsStoresEtcInner.statusNotOk.setTrue();
		wsaaFlagsSubsStoresEtcInner.wsaaRgpystat.set(SPACES);
		ct01Value++;
		checkStatii2510();
		/* If the flag STATUS-OK is not set an Exception report is*/
		/* written and WSSP-EDTERROR is set to SPACES thus preventing*/
		/* 3000- section being called.*/
		/*    If STATUS-OK is true then a further check is required to*/
		/* see if the CHDRCOY, CHDRNUM, LIFE, COVERAGE or RIDER,*/
		/* changes from the previous record, then a new COVR has been*/
		/* found. The WSAA-NEW-LETTER flag is set, used in the 3000*/
		/* section so that a new letter is written.*/
		if (wsaaFlagsSubsStoresEtcInner.statusOk.isTrue()) {
			if (isNE(sqlRegppfInner.getChdrcoy(), wsaaFlagsSubsStoresEtcInner.wsaaLastChdrcoy)
			|| isNE(sqlRegppfInner.getChdrnum(), wsaaFlagsSubsStoresEtcInner.wsaaLastChdrnum)
			|| isNE(sqlRegppfInner.getLife(), wsaaFlagsSubsStoresEtcInner.wsaaLastLife)
			|| isNE(sqlRegppfInner.getCoverage(), wsaaFlagsSubsStoresEtcInner.wsaaLastCoverage)
			|| isNE(sqlRegppfInner.getRider(), wsaaFlagsSubsStoresEtcInner.wsaaLastRider)) {
				wsaaFlagsSubsStoresEtcInner.wsaaNewLetter.set("Y");
			}
			wsspEdterror.set(varcom.oK);
		}
		else {
			writeException2520();
		}

	}


protected void checkStatii2510()
	{
		/* The status of REGP is checked if present on T6693.  This is*/
		/* done twice. The first time with RGPYSTAT & CRTABLE as the*/
		/* item key, and the second (if not found) with **** as the*/
		/* item key. If, after the second check, the item has not been*/
		/* found, then set CT04 and WSSP-EDTERROR to SPACES and exit*/
		/* section.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlRegppfInner.getRgpystat(), SPACES);
		stringVariable1.addExpression(sqlRegppfInner.getCrtable(), SPACES);
		stringVariable1.setStringInto(wsaaFlagsSubsStoresEtcInner.wsaaItemitem);
		if(t6693Map != null && t6693Map.containsKey(wsaaFlagsSubsStoresEtcInner.wsaaItemitem.toString())){
			for(Itempf itdm:t6693Map.get(wsaaFlagsSubsStoresEtcInner.wsaaItemitem.toString())){
				if(itdm.getItmfrm().compareTo(new BigDecimal(sqlRegppfInner.getCertdate())) <= 0){
					t6693rec.t6693Rec.set(StringUtil.rawToString(itdm.getGenarea()));
					t6693Search2540();
					break;
				}
			}
		}else{
			nextStatusCheck2530();
		}
	}


protected void writeException2520()
	{
		r5227D02Inner.rd02Certdate.set(datcon1rec.extDate);
		r5227D02Inner.rd02Chdrnum.set(sqlRegppfInner.getChdrnum());
		r5227D02Inner.rd02Life.set(sqlRegppfInner.getLife());
		r5227D02Inner.rd02Coverage.set(sqlRegppfInner.getCoverage());
		r5227D02Inner.rd02Rider.set(sqlRegppfInner.getRider());
		r5227D02Inner.rd02Rgpynum.set(sqlRegppfInner.getRgpynum());
		r5227D02Inner.rd02Pymt.set(sqlRegppfInner.getPymt());
		r5227D02Inner.rd02Currcd.set(sqlRegppfInner.getCurrcd());
		r5227D02Inner.rd02Prcnt.set(sqlRegppfInner.getPrcnt());
		r5227D02Inner.rd02Rgpytype.set(sqlRegppfInner.getRgpytype());
		r5227D02Inner.rd02Payreason.set(sqlRegppfInner.getPayreason());
		r5227D02Inner.rd02Rgpystat.set(sqlRegppfInner.getRgpystat());
		r5227D02Inner.rd02Regpayfreq.set(sqlRegppfInner.getRegpayfreq());
		if (newXptPageReq.isTrue()) {
			printerFile02.printR5227h02(r5227H02, indicArea);
			wsaaOverflowXpt.set("N");
		}
		printerFile02.printR5227d02(r5227D02Inner.r5227D02, indicArea);
		wsspEdterror.set(SPACES);
		ct04Value++;

	}

protected void nextStatusCheck2530()
 {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlRegppfInner.getRgpystat(), SPACES);
		stringVariable1.addExpression("****", SPACES);
		stringVariable1.setStringInto(wsaaFlagsSubsStoresEtcInner.wsaaItemitem);
		if (t6693Map != null && t6693Map.containsKey(wsaaFlagsSubsStoresEtcInner.wsaaItemitem.toString())) {
			for (Itempf itdm : t6693Map.get(wsaaFlagsSubsStoresEtcInner.wsaaItemitem.toString())) {
				if (itdm.getItmfrm().compareTo(new BigDecimal(sqlRegppfInner.getCertdate())) <= 0) {
					t6693rec.t6693Rec.set(StringUtil.rawToString(itdm.getGenarea()));
					t6693Search2540();
					break;
				}
			}
		} else {
			wsaaFlagsSubsStoresEtcInner.statusNotOk.setTrue();
		}
	}

protected void t6693Search2540()
	{
		/*STARTS*/
		/* Check REGP (SQL) status IS on T6693. If it is not found, set*/
		/* WSAA-EXCEPTION-FLAG to write an exception report in the 3000*/
		/* section.*/
		/* In the 2500 section T6693 gen-area was obtained. Here, the*/
		/* the presence of a valid (correct transaction code) status*/
		/* is checked. If none found, then error.*/
		for (wsaaFlagsSubsStoresEtcInner.wsaaIndex.set(1); !(wsaaFlagsSubsStoresEtcInner.statusOk.isTrue()
		|| isGT(wsaaFlagsSubsStoresEtcInner.wsaaIndex, 12)); wsaaFlagsSubsStoresEtcInner.wsaaIndex.add(1)){
			if (isEQ(bprdIO.getAuthCode(), t6693rec.trcode[wsaaFlagsSubsStoresEtcInner.wsaaIndex.toInt()])) {
				wsaaFlagsSubsStoresEtcInner.wsaaRgpystat.set(t6693rec.rgpystat[wsaaFlagsSubsStoresEtcInner.wsaaIndex.toInt()]);
				wsaaFlagsSubsStoresEtcInner.statusOk.setTrue();
			}
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/* Call T6625 to get frequency.*/
		if (t6625Map != null && t6625Map.containsKey(sqlRegppfInner.getCrtable())) {/* IJTI-1523 */
			for (Itempf itdm : t6625Map.get(sqlRegppfInner.getCrtable())) {/* IJTI-1523 */
				if (itdm.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
					t6625rec.t6625Rec.set(StringUtil.rawToString(itdm.getGenarea()));
					break;
				}
			}
		} else {
			syserrrec.params.set(sqlRegppfInner.getCrtable());
			fatalError600();
		}
	
		datcon1rec.intDate.set(sqlRegppfInner.getCertdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		writeReportLine3100();
		updateRegp3200();
		if (isEQ(wsaaFlagsSubsStoresEtcInner.wsaaNewLetter, "Y")) {
			writeLetrqstLetter3300();
		}
	}

protected void writeReportLine3100()
	{
		r5227D01Inner.rd01Certdate.set(datcon1rec.extDate);
		r5227D01Inner.rd01Chdrnum.set(sqlRegppfInner.getChdrnum());
		r5227D01Inner.rd01Life.set(sqlRegppfInner.getLife());
		r5227D01Inner.rd01Coverage.set(sqlRegppfInner.getCoverage());
		r5227D01Inner.rd01Rider.set(sqlRegppfInner.getRider());
		r5227D01Inner.rd01Rgpynum.set(sqlRegppfInner.getRgpynum());
		r5227D01Inner.rd01Pymt.set(sqlRegppfInner.getPymt());
		r5227D01Inner.rd01Currcd.set(sqlRegppfInner.getCurrcd());
		r5227D01Inner.rd01Prcnt.set(sqlRegppfInner.getPrcnt());
		r5227D01Inner.rd01Rgpytype.set(sqlRegppfInner.getRgpytype());
		r5227D01Inner.rd01Payreason.set(sqlRegppfInner.getPayreason());
		r5227D01Inner.rd01Rgpystat.set(sqlRegppfInner.getRgpystat());
		r5227D01Inner.rd01Regpayfreq.set(sqlRegppfInner.getRegpayfreq());
		if (newPageReq.isTrue()) {
			printerFile.printR5227h01(r5227H01, indicArea);
			wsaaOverflow.set("N");
		}
		printerFile.printR5227d01(r5227D01Inner.r5227D01, indicArea);
	}

protected void updateRegp3200()
 {
		datcon2rec.freqFactor.set(t6625rec.frequency);
		datcon2rec.frequency.set("01");
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(sqlRegppfInner.getCertdate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		Regppf regpIO = null;
		if (regpMap != null && regpMap.containsKey(sqlRegppfInner.getChdrnum())) {
			for (Regppf regp : regpMap.get(sqlRegppfInner.getChdrnum())) {
				if (regp.getChdrcoy().equals(sqlRegppfInner.getChdrcoy())
						&& regp.getLife().equals(sqlRegppfInner.getLife())
						&& regp.getCoverage().equals(sqlRegppfInner.getCoverage())
						&& regp.getRider().equals(sqlRegppfInner.getRider())
						&& regp.getRgpynum() == sqlRegppfInner.getRgpynum()) {
					regpIO = regp;
					break;
				}
			}
		}
		if (regpIO == null) {
			syserrrec.params.set(sqlRegppfInner.getChdrnum());
			fatalError600();
		} 
		//IJTI-320 START
		else {
			regpIO.setRgpystat(wsaaFlagsSubsStoresEtcInner.wsaaRgpystat.toString());
			regpIO.setCertdate(datcon2rec.intDate2.toInt());
		}
		//IJTI-320 END
		if (updateRegpList == null) {
			updateRegpList = new ArrayList<>();
		}
		updateRegpList.add(regpIO);
		ct02Value++;
	}

protected void writeLetrqstLetter3300()
	{
		wsaaFlagsSubsStoresEtcInner.wsaaLastChdrcoy.set(sqlRegppfInner.getChdrcoy());
		wsaaFlagsSubsStoresEtcInner.wsaaLastChdrnum.set(sqlRegppfInner.getChdrnum());
		wsaaFlagsSubsStoresEtcInner.wsaaLastLife.set(sqlRegppfInner.getLife());
		wsaaFlagsSubsStoresEtcInner.wsaaLastCoverage.set(sqlRegppfInner.getCoverage());
		wsaaFlagsSubsStoresEtcInner.wsaaLastRider.set(sqlRegppfInner.getRider());
		/* Get CNTTYPE for use in the read of T6634.*/
		Chdrpf chdrrgpIO = null;
		if(chdrMap != null && chdrMap.containsKey(sqlRegppfInner.getChdrnum())){
			for(Chdrpf c: chdrMap.get(sqlRegppfInner.getChdrnum())){
				if(c.getChdrcoy().toString().equals(sqlRegppfInner.getChdrcoy())){
					chdrrgpIO = c;
					break;
				}
			}
		}		
		if(chdrrgpIO == null){
			syserrrec.params.set(sqlRegppfInner.getChdrnum());
			fatalError600();
		} else {//IJTI-320 START
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrrgpIO.getCnttype(), SPACES);
			stringVariable1.addExpression(bprdIO.getAuthCode(), SPACES);
			if(tr384Map !=null && tr384Map.containsKey(stringVariable1.toString())){
				Itempf itemIO = tr384Map.get(stringVariable1.toString()).get(0);
				tr384rec.tr384Rec.set(StringUtil.rawToString(itemIO.getGenarea()));
			}else{
				syserrrec.params.set(stringVariable1);
				fatalError600();
			}
		}//IJTI-320 END
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
		
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
		/* Setup and Write the Letter.*/
		//IJTI-320 START
		if(chdrrgpIO != null){
			letrqstrec.clntcoy.set(chdrrgpIO.getCowncoy());
			letrqstrec.clntnum.set(chdrrgpIO.getCownnum());
			letrqstrec.branch.set(chdrrgpIO.getCntbranch());
		}
		//IJTI-320 END
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(sqlRegppfInner.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(sqlRegppfInner.getChdrcoy());
		letrqstrec.chdrcoy.set(sqlRegppfInner.getChdrcoy());
		letrqstrec.rdocnum.set(sqlRegppfInner.getChdrnum());
		letrqstrec.chdrnum.set(sqlRegppfInner.getChdrnum());
		/* MOVE CHDRRGP-CHDRNUM        TO LETRQST-OTHER-KEYS.           */
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
		ct03Value++;
	}


protected void commit3500()
 {
		
		if(updateRegpList != null && !updateRegpList.isEmpty()){
			regppfDAO.updateRegpStatRecord(updateRegpList);
			updateRegpList.clear();
		}
		contotrec.totval.set(ct01Value);
		contotrec.totno.set(1);
		callContot001();
		ct01Value = 0;
		contotrec.totval.set(ct02Value);
		contotrec.totno.set(2);
		callContot001();
		ct02Value = 0;
		contotrec.totval.set(ct03Value);
		contotrec.totno.set(3);
		callContot001();
		ct03Value = 0;
		contotrec.totval.set(ct04Value);
		contotrec.totno.set(4);
		callContot001();
		ct04Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor.*/
		/*  Close any open files.*/
		printerFile.close();
		printerFile02.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-FLAGS-SUBS-STORES-ETC--INNER
 */
private static final class WsaaFlagsSubsStoresEtcInner {
	private FixedLengthStringData wsaaNewLetter = new FixedLengthStringData(1);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaTemporaryDate = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaTempDtNums = new FixedLengthStringData(10).isAPartOf(wsaaTemporaryDate, 0, REDEFINE);
	private ZonedDecimalData wsaaTempDtNum = new ZonedDecimalData(3, 0).isAPartOf(wsaaTempDtNums, 0).setUnsigned();
	private FixedLengthStringData wsaaSqlChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaValidFlag = new FixedLengthStringData(1).init("1");
	private FixedLengthStringData wsaaLastChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLastCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLastRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaLastPlanSuffix = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaStatusOk = new FixedLengthStringData(1);
	private Validator statusOk = new Validator(wsaaStatusOk, "Y");
	private Validator statusNotOk = new Validator(wsaaStatusOk, "N");
}
/*
 * Class transformed  from Data Structure R5227-D01--INNER
 */
private static final class R5227D01Inner {

		/* Detail line: add as many detail and total lines as required,
		 using redefines to save WS space where applicable.*/
	private FixedLengthStringData r5227D01 = new FixedLengthStringData(62);
	private FixedLengthStringData r5227d01O = new FixedLengthStringData(62).isAPartOf(r5227D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5227d01O, 0);
	private FixedLengthStringData rd01Life = new FixedLengthStringData(2).isAPartOf(r5227d01O, 8);
	private FixedLengthStringData rd01Coverage = new FixedLengthStringData(2).isAPartOf(r5227d01O, 10);
	private FixedLengthStringData rd01Rider = new FixedLengthStringData(2).isAPartOf(r5227d01O, 12);
	private ZonedDecimalData rd01Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r5227d01O, 14);
	private ZonedDecimalData rd01Pymt = new ZonedDecimalData(17, 2).isAPartOf(r5227d01O, 19);
	private FixedLengthStringData rd01Regpayfreq = new FixedLengthStringData(2).isAPartOf(r5227d01O, 36);
	private FixedLengthStringData rd01Currcd = new FixedLengthStringData(3).isAPartOf(r5227d01O, 38);
	private ZonedDecimalData rd01Prcnt = new ZonedDecimalData(5, 2).isAPartOf(r5227d01O, 41);
	private FixedLengthStringData rd01Rgpytype = new FixedLengthStringData(2).isAPartOf(r5227d01O, 46);
	private FixedLengthStringData rd01Payreason = new FixedLengthStringData(2).isAPartOf(r5227d01O, 48);
	private FixedLengthStringData rd01Rgpystat = new FixedLengthStringData(2).isAPartOf(r5227d01O, 50);
	private FixedLengthStringData rd01Certdate = new FixedLengthStringData(10).isAPartOf(r5227d01O, 52);
}
/*
 * Class transformed  from Data Structure R5227-D02--INNER
 */
private static final class R5227D02Inner {

	private FixedLengthStringData r5227D02 = new FixedLengthStringData(62);
	private FixedLengthStringData r5227d02O = new FixedLengthStringData(62).isAPartOf(r5227D02, 0);
	private FixedLengthStringData rd02Chdrnum = new FixedLengthStringData(8).isAPartOf(r5227d02O, 0);
	private FixedLengthStringData rd02Life = new FixedLengthStringData(2).isAPartOf(r5227d02O, 8);
	private FixedLengthStringData rd02Coverage = new FixedLengthStringData(2).isAPartOf(r5227d02O, 10);
	private FixedLengthStringData rd02Rider = new FixedLengthStringData(2).isAPartOf(r5227d02O, 12);
	private ZonedDecimalData rd02Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r5227d02O, 14);
	private ZonedDecimalData rd02Pymt = new ZonedDecimalData(17, 2).isAPartOf(r5227d02O, 19);
	private FixedLengthStringData rd02Regpayfreq = new FixedLengthStringData(2).isAPartOf(r5227d02O, 36);
	private FixedLengthStringData rd02Currcd = new FixedLengthStringData(3).isAPartOf(r5227d02O, 38);
	private ZonedDecimalData rd02Prcnt = new ZonedDecimalData(5, 2).isAPartOf(r5227d02O, 41);
	private FixedLengthStringData rd02Rgpytype = new FixedLengthStringData(2).isAPartOf(r5227d02O, 46);
	private FixedLengthStringData rd02Payreason = new FixedLengthStringData(2).isAPartOf(r5227d02O, 48);
	private FixedLengthStringData rd02Rgpystat = new FixedLengthStringData(2).isAPartOf(r5227d02O, 50);
	private FixedLengthStringData rd02Certdate = new FixedLengthStringData(10).isAPartOf(r5227d02O, 52);
}
}
