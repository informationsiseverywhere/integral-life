package com.csc.life.annuities.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 05/03/20 05:43
 * @author Quipoz
 */
public class Sjl39screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 18;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {13, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 21, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl39ScreenVars sv = (Sjl39ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sjl39screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sjl39screensfl, 
			sv.Sjl39screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl39ScreenVars sv = (Sjl39ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sjl39screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl39ScreenVars sv = (Sjl39ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sjl39screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sjl39screensflWritten.gt(0))
		{
			sv.sjl39screensfl.setCurrentIndex(0);
			sv.Sjl39screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl39ScreenVars sv = (Sjl39ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sjl39screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl39ScreenVars screenVars = (Sjl39ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.hselect.setFieldName("hselect");
				screenVars.hflag.setFieldName("hflag");				
				screenVars.pmntno.setFieldName("pmntno");				
				screenVars.pmntdateDisp.setFieldName("pmntdateDisp");
				screenVars.annuityamt.setFieldName("annuityamt");
				screenVars.intrate.setFieldName("intrate");
				screenVars.annuitywint.setFieldName("annuitywint");
				screenVars.wholdtax.setFieldName("wholdtax");
				screenVars.paidamt.setFieldName("paidamt");
				screenVars.fillh.setFieldName("fillh");
				screenVars.filll.setFieldName("filll");
				screenVars.pmntstatus.setFieldName("pmntstatus");				
				screenVars.dataloc.setFieldName("dataloc");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.hselect.set(dm.getField("hselect"));
			screenVars.hflag.set(dm.getField("hflag"));
			screenVars.pmntno.set(dm.getField("pmntno"));				
			screenVars.pmntdateDisp.set(dm.getField("pmntdateDisp"));
			screenVars.annuityamt.set(dm.getField("annuityamt"));
			screenVars.intrate.set(dm.getField("intrate"));
			screenVars.annuitywint.set(dm.getField("annuitywint"));
			screenVars.wholdtax.set(dm.getField("wholdtax"));
			screenVars.paidamt.set(dm.getField("paidamt"));
			screenVars.fillh.set(dm.getField("fillh"));
			screenVars.filll.set(dm.getField("filll"));
			screenVars.pmntstatus.set(dm.getField("pmntstatus"));
			screenVars.dataloc.set(dm.getField("dataloc"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl39ScreenVars screenVars = (Sjl39ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.hselect.setFieldName("hselect");
				screenVars.hflag.setFieldName("hflag");
				screenVars.pmntno.setFieldName("pmntno");				
				screenVars.pmntdateDisp.setFieldName("pmntdateDisp");
				screenVars.annuityamt.setFieldName("annuityamt");
				screenVars.intrate.setFieldName("intrate");
				screenVars.annuitywint.setFieldName("annuitywint");
				screenVars.wholdtax.setFieldName("wholdtax");
				screenVars.paidamt.setFieldName("paidamt");
				screenVars.fillh.setFieldName("fillh");
				screenVars.filll.setFieldName("filll");
				screenVars.pmntstatus.setFieldName("pmntstatus");
				screenVars.dataloc.setFieldName("dataloc");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("hselect").set(screenVars.hselect);
			dm.getField("hflag").set(screenVars.hflag);
			dm.getField("pmntno").set(screenVars.pmntno);
			dm.getField("pmntdateDisp").set(screenVars.pmntdateDisp);
			dm.getField("annuityamt").set(screenVars.annuityamt);
			dm.getField("intrate").set(screenVars.intrate);
			dm.getField("annuitywint").set(screenVars.annuitywint);
			dm.getField("wholdtax").set(screenVars.wholdtax);
			dm.getField("fillh").set(screenVars.fillh);
			dm.getField("filll").set(screenVars.filll);
			dm.getField("paidamt").set(screenVars.paidamt);
			dm.getField("pmntstatus").set(screenVars.pmntstatus);
			dm.getField("dataloc").set(screenVars.dataloc);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl39screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl39ScreenVars screenVars = (Sjl39ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.hselect.clearFormatting();
		screenVars.hflag.clearFormatting();
		screenVars.pmntno.clearFormatting();				
		screenVars.pmntdateDisp.clearFormatting();
		screenVars.annuityamt.clearFormatting();
		screenVars.intrate.clearFormatting();
		screenVars.annuitywint.clearFormatting();
		screenVars.wholdtax.clearFormatting();
		screenVars.paidamt.clearFormatting();
		screenVars.fillh.clearFormatting();
		screenVars.filll.clearFormatting();
		screenVars.pmntstatus.clearFormatting();
		screenVars.dataloc.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl39ScreenVars screenVars = (Sjl39ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.hselect.setClassString("");
		screenVars.hflag.setClassString("");
		screenVars.pmntno.setClassString("");				
		screenVars.pmntdateDisp.setClassString("");
		screenVars.annuityamt.setClassString("");
		screenVars.intrate.setClassString("");
		screenVars.annuitywint.setClassString("");
		screenVars.wholdtax.setClassString("");
		screenVars.paidamt.setClassString("");
		screenVars.fillh.setClassString("");
		screenVars.filll.setClassString("");
		screenVars.pmntstatus.setClassString("");
		screenVars.dataloc.setClassString("");
	}

/**
 * Clear all the variables in Sjl39screensfl
 */
	public static void clear(VarModel pv) {
		Sjl39ScreenVars screenVars = (Sjl39ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.hselect.clear();
		screenVars.hflag.clear();		
		screenVars.pmntno.clear();				
		screenVars.pmntdateDisp.clear();
		screenVars.annuityamt.clear();
		screenVars.intrate.clear();
		screenVars.annuitywint.clear();
		screenVars.wholdtax.clear();
		screenVars.paidamt.clear();
		screenVars.fillh.clear();
		screenVars.filll.clear();
		screenVars.pmntstatus.clear();		
		screenVars.dataloc.clear();
	}
}
