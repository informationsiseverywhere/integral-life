package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * Copybook name: Tjl11rec Date: 30 January 2020 Author: sagrawal35
 */
public class Tjl11rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData tjl11Rec = new FixedLengthStringData(372);
	
	public FixedLengthStringData anntypdesc = DD.anntypdesc.copy().isAPartOf(tjl11Rec, 0);
    public FixedLengthStringData annuitype = DD.annuitype.copy().isAPartOf(tjl11Rec, 50);
    
    public FixedLengthStringData bentypsdesc = new FixedLengthStringData(300).isAPartOf(tjl11Rec, 52);
	public FixedLengthStringData[] bentypdesc = FLSArrayPartOfStructure(6, 50, bentypsdesc, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(300).isAPartOf(bentypsdesc, 0, FILLER_REDEFINE);
	public FixedLengthStringData bentypdesc01 = DD.bentypdesc.copy().isAPartOf(filler1, 0);
	public FixedLengthStringData bentypdesc02 = DD.bentypdesc.copy().isAPartOf(filler1, 50);
	public FixedLengthStringData bentypdesc03 = DD.bentypdesc.copy().isAPartOf(filler1, 100);
	public FixedLengthStringData bentypdesc04 = DD.bentypdesc.copy().isAPartOf(filler1, 150);
	public FixedLengthStringData bentypdesc05 = DD.bentypdesc.copy().isAPartOf(filler1, 200);
	public FixedLengthStringData bentypdesc06 = DD.bentypdesc.copy().isAPartOf(filler1, 250);
	
	public FixedLengthStringData beneftypes = new FixedLengthStringData(18).isAPartOf(tjl11Rec, 352);
	public FixedLengthStringData[] beneftype = FLSArrayPartOfStructure(6, 3, beneftypes, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(18).isAPartOf(beneftypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData beneftype01 = DD.beneftype.copy().isAPartOf(filler2, 0);
	public FixedLengthStringData beneftype02 = DD.beneftype.copy().isAPartOf(filler2, 3);
	public FixedLengthStringData beneftype03 = DD.beneftype.copy().isAPartOf(filler2, 6);
	public FixedLengthStringData beneftype04 = DD.beneftype.copy().isAPartOf(filler2, 9);
	public FixedLengthStringData beneftype05 = DD.beneftype.copy().isAPartOf(filler2, 12);
	public FixedLengthStringData beneftype06 = DD.beneftype.copy().isAPartOf(filler2, 15);		
	
	public FixedLengthStringData defannpayfreq = DD.billfreq.copy().isAPartOf(tjl11Rec, 370);		

	public void initialize() {
		COBOLFunctions.initialize(tjl11Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl11Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}
}