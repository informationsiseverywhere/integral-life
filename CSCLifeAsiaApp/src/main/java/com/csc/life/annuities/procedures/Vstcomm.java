/*
 * File: Vstcomm.java
 * Date: 30 August 2009 2:54:33
 * Author: Quipoz Limited
 * 
 * Class transformed from VSTCOMM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.annuities.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.annuities.recordstructures.Vstccpy;
import com.csc.life.annuities.tablestructures.T6623rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This subroutine forms part of the 9405 Annuities Development
* and is a component of the Vesting Registration Transaction.
* It allows the user to input either a percentage to commute     o
* or a lump sum commutation value and then calculates the
* other value.  It will also calculate the reduced benefit
* payable.
* The formulae used in these calculations are as follows:
*
* Lump Sum
* ========
*
*         Lump Sum = (Benefit payable * Percentage/100)
*                     *  Commutation factor
*
*
* Reduced Benefit
* ===============
*
*         Reduced Benefit = Benefit payable * (100-percentage)
*                           / 100
*
* The commutation factor is found on a new table,  Vesting
* Commutation Factors - T6623.  The key to this table is the
* Coverage type (CRTABLE), the age attained at time of vesting of
* the life assured plus the sex code.
* If there is no item on the table for the desired key or there is
* no match for the combination of ANNY-Frequency, ANNY-In advance,
* ANNY-In Arrears and ANNY-guaranteed period then the lump sum or
* percentage returned will be zero and no change will be made to
* the benefit payable figure.  These values can then be updated
* manually if required.
*
* If a commutation (part or whole) of the benefit payable has
* been requested, this subroutine will calculate the lump sum
* payable and the reduced benefit amount for a given commutation
* percentage.  Equally, it will return the commutation percentage
* if a given lump sum has been requested.  This allows subsequent
* validation to ensure that the lump sum requested does not
* represent a commutation percentage greater than the maximum
* value allowed for the coverage in question.
*
*****************************************************************
* </pre>
*/
public class Vstcomm extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VSTCOMM";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-REC */
	private ZonedDecimalData wsaaVstcperc = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaVstlump = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaVstreduce = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAge = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaItemKey = new FixedLengthStringData(7);
	private ZonedDecimalData wsaaTempPercent = new ZonedDecimalData(8, 5);
		/* SUBSCRIPTS-ETC */
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaMatchFound = "";
	private String wsaaNextCheck = "";
		/* TABLES */
	private String t6623 = "T6623";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6623rec t6623rec = new T6623rec();
	private Varcom varcom = new Varcom();
	private Vstccpy vstccpy = new Vstccpy();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2009
	}

	public Vstcomm() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vstccpy.rec = convertAndSetParam(vstccpy.rec, parmArray, 0);
		try {
			control0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control0000()
	{
		/*STARTS*/
		initialize1000();
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			process2000();
		}
		
		end3000();
	}

protected void initialize1000()
	{
		initializeIt1000();
	}

protected void initializeIt1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaMatchFound = "N";
		wsaaNextCheck = "N";
		wsaaVstcperc.set(ZERO);
		wsaaVstlump.set(ZERO);
		wsaaTempPercent.set(ZERO);
		wsaaVstreduce.set(ZERO);
		wsaaVstcperc.set(vstccpy.vstcperc);
		wsaaVstlump.set(vstccpy.vstlump);
		wsaaVstreduce.set(vstccpy.vstreduce);
		wsaaAge.set(vstccpy.age);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(vstccpy.crtable, SPACES));
		stringVariable1.append(delimitedExp(wsaaAge, SPACES));
		stringVariable1.append(delimitedExp(vstccpy.sex, SPACES));
		wsaaItemKey.setLeft(stringVariable1.toString());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vstccpy.company);
		itemIO.setItemtabl(t6623);
		itemIO.setItemitem(wsaaItemKey);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMPFX", "ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getItempfx(),"IT")
		|| isNE(itemIO.getItemcoy(),vstccpy.company)
		|| isNE(itemIO.getItemtabl(),t6623)
		|| isNE(itemIO.getItemitem(),wsaaItemKey)
		|| isEQ(itemIO.getStatuz(),varcom.endp)) {
			itemIO.setStatuz(varcom.endp);
		}
		else {
			t6623rec.t6623Rec.set(itemIO.getGenarea());
		}
	}

protected void process2000()
	{
		try {
			checks2000();
		}
		catch (GOTOException e){
		}
	}

protected void checks2000()
	{
		for (wsaaSub.set(1); !((isGT(wsaaSub,10)
		|| isEQ(wsaaMatchFound,"Y"))); wsaaSub.add(1)){
			t6623Match2100();
		}
		if (isEQ(wsaaMatchFound,"Y")) {
			itemIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2009);
		}
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2009);
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getItempfx(),"IT")
		|| isNE(itemIO.getItemcoy(),vstccpy.company)
		|| isNE(itemIO.getItemtabl(),t6623)
		|| isNE(itemIO.getItemitem(),wsaaItemKey)
		|| isEQ(itemIO.getStatuz(),varcom.endp)) {
			itemIO.setStatuz(varcom.endp);
		}
		else {
			t6623rec.t6623Rec.set(itemIO.getGenarea());
		}
	}

protected void t6623Match2100()
	{
		/*CHECK-FOR-MATCH*/
		wsaaNextCheck = "N";
		if ((isEQ(vstccpy.freqann,t6623rec.freqann[wsaaSub.toInt()]))
		&& (isEQ(vstccpy.guarperd,t6623rec.guarperd[wsaaSub.toInt()]))) {
			wsaaNextCheck = "Y";
		}
		if (isEQ(wsaaNextCheck,"Y")) {
			if (((isEQ(vstccpy.arrears,SPACES))
			&& (isEQ(t6623rec.arrears[wsaaSub.toInt()],SPACES))
			&& (isNE(vstccpy.advance,SPACES))
			&& (isNE(t6623rec.advance[wsaaSub.toInt()],SPACES)))
			|| ((isNE(vstccpy.arrears,SPACES))
			&& (isNE(t6623rec.arrears[wsaaSub.toInt()],SPACES))
			&& (isEQ(vstccpy.advance,SPACES))
			&& (isEQ(t6623rec.advance[wsaaSub.toInt()],SPACES)))) {
				wsaaMatchFound = "Y";
				detailsMatch2110();
			}
		}
		/*EXIT*/
	}

protected void detailsMatch2110()
	{
		/*DO-CALCULATION*/
		if (isEQ(wsaaVstlump,ZERO)) {
			compute(wsaaVstlump, 3).setRounded(mult((mult(vstccpy.vstpay,(div(wsaaVstcperc,100)))),(t6623rec.comtfact[wsaaSub.toInt()])));
		}
		else {
			compute(wsaaVstcperc, 4).setRounded(div((mult(100,wsaaVstlump)),(mult(t6623rec.comtfact[wsaaSub.toInt()],vstccpy.vstpay))));
		}
		compute(wsaaTempPercent, 6).setRounded(div((sub(100,wsaaVstcperc)),100));
		compute(wsaaVstreduce, 6).setRounded(mult(vstccpy.vstpay,wsaaTempPercent));
		/*EXIT*/
	}

protected void end3000()
	{
		/*ENDS-IT*/
		if (isNE(wsaaMatchFound,"Y")) {
			vstccpy.vstlump.set(ZERO);
			vstccpy.vstreduce.set(ZERO);
			vstccpy.vstcperc.set(ZERO);
		}
		else {
			vstccpy.vstlump.set(wsaaVstlump);
			vstccpy.vstreduce.set(wsaaVstreduce);
			vstccpy.vstcperc.set(wsaaVstcperc);
		}
		exitProgram();
	}

protected void fatalError600()
	{
		/*FATAL*/
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("4");
		}
		else {
			syserrrec.syserrType.set("3");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void stop699()
	{
		stopRun();
	}
}
