package com.csc.life.annuities.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Tjl44rec extends ExternalData {
	private static final long serialVersionUID = 1L; 
	
	public FixedLengthStringData tjl44Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData whhtr = new ZonedDecimalData(7, 4).isAPartOf(tjl44Rec, 0);
  	public ZonedDecimalData itmfrm = new ZonedDecimalData(8).isAPartOf(tjl44Rec,4);
	public ZonedDecimalData itmto = new ZonedDecimalData(8).isAPartOf(tjl44Rec,12);
  	public FixedLengthStringData filler = new FixedLengthStringData(480).isAPartOf(tjl44Rec, 20, FILLER);
	
	public void initialize() {
		COBOLFunctions.initialize(tjl44Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl44Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}
