package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5222
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class S5222ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(980/*+8+4+12*/);
	public FixedLengthStringData dataFields = new FixedLengthStringData(436).isAPartOf(dataArea, 0);
	public ZonedDecimalData anvdate = DD.anvdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData claimcur = DD.claimcur.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData claimevd = DD.claimevd.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData clmcurdsc = DD.clmcurdsc.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData clmdesc = DD.clmdesc.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData cltype = DD.cltype.copy().isAPartOf(dataFields,77);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,82);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,141);
	public FixedLengthStringData destkey = DD.destkey.copy().isAPartOf(dataFields,142);
	public ZonedDecimalData finalPaydate = DD.epaydate.copyToZonedDecimal().isAPartOf(dataFields,152);
	public ZonedDecimalData firstPaydate = DD.fpaydate.copyToZonedDecimal().isAPartOf(dataFields,160);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,168);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,186);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,233);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,241);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(dataFields,288);
	public FixedLengthStringData payenme = DD.payenme.copy().isAPartOf(dataFields,296);
	public ZonedDecimalData prcnt = DD.prcnt.copyToZonedDecimal().isAPartOf(dataFields,343);
	public ZonedDecimalData pymt = DD.pymt.copyToZonedDecimal().isAPartOf(dataFields,348);
	public FixedLengthStringData regpayfreq = DD.regpayfreq.copy().isAPartOf(dataFields,365);
	public ZonedDecimalData revdate = DD.revdate.copyToZonedDecimal().isAPartOf(dataFields,367);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(dataFields,375);
	public ZonedDecimalData rgpynum = DD.rgpynum.copyToZonedDecimal().isAPartOf(dataFields,376);
	public FixedLengthStringData rgpyshort = DD.rgpyshort.copy().isAPartOf(dataFields,381);
	public FixedLengthStringData rgpytypesd = DD.rgpytypesd.copy().isAPartOf(dataFields,391);
	public ZonedDecimalData totalamt = DD.totalamt.copyToZonedDecimal().isAPartOf(dataFields,401);
	public ZonedDecimalData vstpay = DD.vstpay.copyToZonedDecimal().isAPartOf(dataFields,419);
	//public FixedLengthStringData payoutoption = DD.payoutopt.copy().isAPartOf(dataFields,419+vstpay.getLength());//fwang3 ICIL-4	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(136).isAPartOf(dataArea, 436/*+payoutoption.length()*/);
	public FixedLengthStringData anvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData claimcurErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData claimevdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData clmcurdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData clmdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData destkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData epaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData fpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData paycltErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData payenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData prcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData pymtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData regpayfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData revdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rgpymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData rgpynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData rgpyshortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rgpytypesdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData totalamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData vstpayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
//	public FixedLengthStringData payoutoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132+4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(408).isAPartOf(dataArea, 572/*+payoutoption.length()+4*/);
	public FixedLengthStringData[] anvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] claimcurOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] claimevdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] clmcurdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] clmdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] destkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] epaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] fpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] paycltOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] payenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] pymtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] regpayfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] revdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rgpymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] rgpynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] rgpyshortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rgpytypesdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] totalamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] vstpayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
//	public FixedLengthStringData[] payoutoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396+12);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData anvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData finalPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData firstPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData revdateDisp = new FixedLengthStringData(10);

	public LongData S5222screenWritten = new LongData(0);
	public LongData S5222protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5222ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(regpayfreqOut,new String[] {"05","24","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(destkeyOut,new String[] {"09","27","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltypeOut,new String[] {"01","20","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimevdOut,new String[] {"02","21","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpymopOut,new String[] {"04","23","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paycltOut,new String[] {"03","22","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimcurOut,new String[] {"08","26","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(anvdateOut,new String[] {"11","29","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(epaydateOut,new String[] {"12","30","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pymtOut,new String[] {"06","25","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totalamtOut,new String[] {"50",null, "-50","-50",null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcntOut,new String[] {"07","31","-07","51",null, null, null, null, null, null, null, null});
		fieldIndMap.put(revdateOut,new String[] {null, null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
//		fieldIndMap.put(payoutoptionOut,new String[] {"75","76","-75",null, null, null, null, null, null, null, null, null});//fwang3
		screenFields = new BaseData[] {chdrnum, cnttype, lifcnum, ctypedes, linsname, cownnum, ownername, occdate, currcd, rgpytypesd, regpayfreq, frqdesc, destkey, ddind, cltype, clmdesc, claimevd, rgpymop, payclt, payenme, claimcur, anvdate, finalPaydate, rgpyshort, clmcurdsc, currds, rgpynum, pymt, totalamt, crtdate, firstPaydate, prcnt, vstpay, revdate/*,payoutoption*/};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, lifcnumOut, ctypedesOut, linsnameOut, cownnumOut, ownernameOut, occdateOut, currcdOut, rgpytypesdOut, regpayfreqOut, frqdescOut, destkeyOut, ddindOut, cltypeOut, clmdescOut, claimevdOut, rgpymopOut, paycltOut, payenmeOut, claimcurOut, anvdateOut, epaydateOut, rgpyshortOut, clmcurdscOut, currdsOut, rgpynumOut, pymtOut, totalamtOut, crtdateOut, fpaydateOut, prcntOut, vstpayOut, revdateOut/*,payoutoptionOut*/};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, lifcnumErr, ctypedesErr, linsnameErr, cownnumErr, ownernameErr, occdateErr, currcdErr, rgpytypesdErr, regpayfreqErr, frqdescErr, destkeyErr, ddindErr, cltypeErr, clmdescErr, claimevdErr, rgpymopErr, paycltErr, payenmeErr, claimcurErr, anvdateErr, epaydateErr, rgpyshortErr, clmcurdscErr, currdsErr, rgpynumErr, pymtErr, totalamtErr, crtdateErr, fpaydateErr, prcntErr, vstpayErr, revdateErr/*,payoutoptionErr*/};
		screenDateFields = new BaseData[] {occdate, anvdate, finalPaydate, crtdate, firstPaydate, revdate};
		screenDateErrFields = new BaseData[] {occdateErr, anvdateErr, epaydateErr, crtdateErr, fpaydateErr, revdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, anvdateDisp, finalPaydateDisp, crtdateDisp, firstPaydateDisp, revdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5222screen.class;
		protectRecord = S5222protect.class;
	}

}
