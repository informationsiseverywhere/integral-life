package com.csc.life.annuities.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: VstdbrkTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:38
 * Class transformed from VSTDBRK.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class VstdbrkTableDAM extends VstdpfTableDAM {

	public VstdbrkTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("VSTDBRK");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "TRANNO, " +
		            "JLIFE, " +
		            "EFFDATE, " +
		            "CRTABLE, " +
		            "VSTLUMP, " +
		            "TYPE01, " +
		            "TYPE02, " +
		            "TYPE03, " +
		            "TYPE04, " +
		            "TYPE05, " +
		            "TYPE06, " +
		            "VSTAMT01, " +
		            "VSTAMT02, " +
		            "VSTAMT03, " +
		            "VSTAMT04, " +
		            "VSTAMT05, " +
		            "VSTAMT06, " +
		            "VSTPAY, " +
		            "VSTPERC, " +
		            "VSTPAYA, " +
		            "ORIGPAY, " +
		            "ELVSTFCT, " +
		            "NEWCOVR, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               tranno,
                               jlife,
                               effdate,
                               crtable,
                               vstlump,
                               fieldType01,
                               fieldType02,
                               fieldType03,
                               fieldType04,
                               fieldType05,
                               fieldType06,
                               vestamt01,
                               vestamt02,
                               vestamt03,
                               vestamt04,
                               vestamt05,
                               vestamt06,
                               vstpay,
                               vstperc,
                               vstpaya,
                               origpay,
                               elvstfct,
                               newcovr,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());
	nonKeyFiller7.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(183);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ getJlife().toInternal()
					+ getEffdate().toInternal()
					+ getCrtable().toInternal()
					+ getVstlump().toInternal()
					+ getFieldType01().toInternal()
					+ getFieldType02().toInternal()
					+ getFieldType03().toInternal()
					+ getFieldType04().toInternal()
					+ getFieldType05().toInternal()
					+ getFieldType06().toInternal()
					+ getVestamt01().toInternal()
					+ getVestamt02().toInternal()
					+ getVestamt03().toInternal()
					+ getVestamt04().toInternal()
					+ getVestamt05().toInternal()
					+ getVestamt06().toInternal()
					+ getVstpay().toInternal()
					+ getVstperc().toInternal()
					+ getVstpaya().toInternal()
					+ getOrigpay().toInternal()
					+ getElvstfct().toInternal()
					+ getNewcovr().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, vstlump);
			what = ExternalData.chop(what, fieldType01);
			what = ExternalData.chop(what, fieldType02);
			what = ExternalData.chop(what, fieldType03);
			what = ExternalData.chop(what, fieldType04);
			what = ExternalData.chop(what, fieldType05);
			what = ExternalData.chop(what, fieldType06);
			what = ExternalData.chop(what, vestamt01);
			what = ExternalData.chop(what, vestamt02);
			what = ExternalData.chop(what, vestamt03);
			what = ExternalData.chop(what, vestamt04);
			what = ExternalData.chop(what, vestamt05);
			what = ExternalData.chop(what, vestamt06);
			what = ExternalData.chop(what, vstpay);
			what = ExternalData.chop(what, vstperc);
			what = ExternalData.chop(what, vstpaya);
			what = ExternalData.chop(what, origpay);
			what = ExternalData.chop(what, elvstfct);
			what = ExternalData.chop(what, newcovr);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getVstlump() {
		return vstlump;
	}
	public void setVstlump(Object what) {
		setVstlump(what, false);
	}
	public void setVstlump(Object what, boolean rounded) {
		if (rounded)
			vstlump.setRounded(what);
		else
			vstlump.set(what);
	}	
	public FixedLengthStringData getFieldType01() {
		return fieldType01;
	}
	public void setFieldType01(Object what) {
		fieldType01.set(what);
	}	
	public FixedLengthStringData getFieldType02() {
		return fieldType02;
	}
	public void setFieldType02(Object what) {
		fieldType02.set(what);
	}	
	public FixedLengthStringData getFieldType03() {
		return fieldType03;
	}
	public void setFieldType03(Object what) {
		fieldType03.set(what);
	}	
	public FixedLengthStringData getFieldType04() {
		return fieldType04;
	}
	public void setFieldType04(Object what) {
		fieldType04.set(what);
	}	
	public FixedLengthStringData getFieldType05() {
		return fieldType05;
	}
	public void setFieldType05(Object what) {
		fieldType05.set(what);
	}	
	public FixedLengthStringData getFieldType06() {
		return fieldType06;
	}
	public void setFieldType06(Object what) {
		fieldType06.set(what);
	}	
	public PackedDecimalData getVestamt01() {
		return vestamt01;
	}
	public void setVestamt01(Object what) {
		setVestamt01(what, false);
	}
	public void setVestamt01(Object what, boolean rounded) {
		if (rounded)
			vestamt01.setRounded(what);
		else
			vestamt01.set(what);
	}	
	public PackedDecimalData getVestamt02() {
		return vestamt02;
	}
	public void setVestamt02(Object what) {
		setVestamt02(what, false);
	}
	public void setVestamt02(Object what, boolean rounded) {
		if (rounded)
			vestamt02.setRounded(what);
		else
			vestamt02.set(what);
	}	
	public PackedDecimalData getVestamt03() {
		return vestamt03;
	}
	public void setVestamt03(Object what) {
		setVestamt03(what, false);
	}
	public void setVestamt03(Object what, boolean rounded) {
		if (rounded)
			vestamt03.setRounded(what);
		else
			vestamt03.set(what);
	}	
	public PackedDecimalData getVestamt04() {
		return vestamt04;
	}
	public void setVestamt04(Object what) {
		setVestamt04(what, false);
	}
	public void setVestamt04(Object what, boolean rounded) {
		if (rounded)
			vestamt04.setRounded(what);
		else
			vestamt04.set(what);
	}	
	public PackedDecimalData getVestamt05() {
		return vestamt05;
	}
	public void setVestamt05(Object what) {
		setVestamt05(what, false);
	}
	public void setVestamt05(Object what, boolean rounded) {
		if (rounded)
			vestamt05.setRounded(what);
		else
			vestamt05.set(what);
	}	
	public PackedDecimalData getVestamt06() {
		return vestamt06;
	}
	public void setVestamt06(Object what) {
		setVestamt06(what, false);
	}
	public void setVestamt06(Object what, boolean rounded) {
		if (rounded)
			vestamt06.setRounded(what);
		else
			vestamt06.set(what);
	}	
	public PackedDecimalData getVstpay() {
		return vstpay;
	}
	public void setVstpay(Object what) {
		setVstpay(what, false);
	}
	public void setVstpay(Object what, boolean rounded) {
		if (rounded)
			vstpay.setRounded(what);
		else
			vstpay.set(what);
	}	
	public PackedDecimalData getVstperc() {
		return vstperc;
	}
	public void setVstperc(Object what) {
		setVstperc(what, false);
	}
	public void setVstperc(Object what, boolean rounded) {
		if (rounded)
			vstperc.setRounded(what);
		else
			vstperc.set(what);
	}	
	public PackedDecimalData getVstpaya() {
		return vstpaya;
	}
	public void setVstpaya(Object what) {
		setVstpaya(what, false);
	}
	public void setVstpaya(Object what, boolean rounded) {
		if (rounded)
			vstpaya.setRounded(what);
		else
			vstpaya.set(what);
	}	
	public PackedDecimalData getOrigpay() {
		return origpay;
	}
	public void setOrigpay(Object what) {
		setOrigpay(what, false);
	}
	public void setOrigpay(Object what, boolean rounded) {
		if (rounded)
			origpay.setRounded(what);
		else
			origpay.set(what);
	}	
	public PackedDecimalData getElvstfct() {
		return elvstfct;
	}
	public void setElvstfct(Object what) {
		setElvstfct(what, false);
	}
	public void setElvstfct(Object what, boolean rounded) {
		if (rounded)
			elvstfct.setRounded(what);
		else
			elvstfct.set(what);
	}	
	public FixedLengthStringData getNewcovr() {
		return newcovr;
	}
	public void setNewcovr(Object what) {
		newcovr.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getVstamts() {
		return new FixedLengthStringData(vestamt01.toInternal()
										+ vestamt02.toInternal()
										+ vestamt03.toInternal()
										+ vestamt04.toInternal()
										+ vestamt05.toInternal()
										+ vestamt06.toInternal());
	}
	public void setVstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getVstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, vestamt01);
		what = ExternalData.chop(what, vestamt02);
		what = ExternalData.chop(what, vestamt03);
		what = ExternalData.chop(what, vestamt04);
		what = ExternalData.chop(what, vestamt05);
		what = ExternalData.chop(what, vestamt06);
	}
	public PackedDecimalData getVstamt(BaseData indx) {
		return getVstamt(indx.toInt());
	}
	public PackedDecimalData getVstamt(int indx) {

		switch (indx) {
			case 1 : return vestamt01;
			case 2 : return vestamt02;
			case 3 : return vestamt03;
			case 4 : return vestamt04;
			case 5 : return vestamt05;
			case 6 : return vestamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setVstamt(BaseData indx, Object what) {
		setVstamt(indx, what, false);
	}
	public void setVstamt(BaseData indx, Object what, boolean rounded) {
		setVstamt(indx.toInt(), what, rounded);
	}
	public void setVstamt(int indx, Object what) {
		setVstamt(indx, what, false);
	}
	public void setVstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setVestamt01(what, rounded);
					 break;
			case 2 : setVestamt02(what, rounded);
					 break;
			case 3 : setVestamt03(what, rounded);
					 break;
			case 4 : setVestamt04(what, rounded);
					 break;
			case 5 : setVestamt05(what, rounded);
					 break;
			case 6 : setVestamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTypes() {
		return new FixedLengthStringData(fieldType01.toInternal()
										+ fieldType02.toInternal()
										+ fieldType03.toInternal()
										+ fieldType04.toInternal()
										+ fieldType05.toInternal()
										+ fieldType06.toInternal());
	}
	public void setTypes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTypes().getLength()).init(obj);
	
		what = ExternalData.chop(what, fieldType01);
		what = ExternalData.chop(what, fieldType02);
		what = ExternalData.chop(what, fieldType03);
		what = ExternalData.chop(what, fieldType04);
		what = ExternalData.chop(what, fieldType05);
		what = ExternalData.chop(what, fieldType06);
	}
	public FixedLengthStringData getType(BaseData indx) {
		return getType(indx.toInt());
	}
	public FixedLengthStringData getType(int indx) {

		switch (indx) {
			case 1 : return fieldType01;
			case 2 : return fieldType02;
			case 3 : return fieldType03;
			case 4 : return fieldType04;
			case 5 : return fieldType05;
			case 6 : return fieldType06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setType(BaseData indx, Object what) {
		setType(indx.toInt(), what);
	}
	public void setType(int indx, Object what) {

		switch (indx) {
			case 1 : setFieldType01(what);
					 break;
			case 2 : setFieldType02(what);
					 break;
			case 3 : setFieldType03(what);
					 break;
			case 4 : setFieldType04(what);
					 break;
			case 5 : setFieldType05(what);
					 break;
			case 6 : setFieldType06(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		jlife.clear();
		effdate.clear();
		crtable.clear();
		vstlump.clear();
		fieldType01.clear();
		fieldType02.clear();
		fieldType03.clear();
		fieldType04.clear();
		fieldType05.clear();
		fieldType06.clear();
		vestamt01.clear();
		vestamt02.clear();
		vestamt03.clear();
		vestamt04.clear();
		vestamt05.clear();
		vestamt06.clear();
		vstpay.clear();
		vstperc.clear();
		vstpaya.clear();
		origpay.clear();
		elvstfct.clear();
		newcovr.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}