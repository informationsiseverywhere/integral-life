package com.csc.life.annuities.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SJL15
 * @version 1.0 generated on 12/02/20 06:38
 * @author Quipoz
 */
public class Sjl15ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1198);
	public FixedLengthStringData dataFields = new FixedLengthStringData(558).isAPartOf(dataArea, 0);	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,41);
	public FixedLengthStringData servbr = DD.servbr.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData cownnum = DD.ownersel.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData cownername = DD.cownername.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,116);
	public FixedLengthStringData cltsex = DD.cltsex.copy().isAPartOf(dataFields,163);             	
	public FixedLengthStringData annuitype = DD.annuitype.copy().isAPartOf(dataFields, 164);
	public FixedLengthStringData beneftype = DD.beneftype.copy().isAPartOf(dataFields, 166);
	public ZonedDecimalData totannufund = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,169);
	
	public FixedLengthStringData annpmntopt = DD.annpmntopt.copy().isAPartOf(dataFields, 186);
	public ZonedDecimalData annupmnttrm = DD.annupmnttrm.copyToZonedDecimal().isAPartOf(dataFields,188);
	public ZonedDecimalData partpmntamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,191);
	public FixedLengthStringData annpmntfreq = DD.annfreq.copy().isAPartOf(dataFields, 208);
	public ZonedDecimalData dfrrdtrm = DD.dfrrdtrm.copyToZonedDecimal().isAPartOf(dataFields,210);
	public ZonedDecimalData dfrrdamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,213);
	public ZonedDecimalData annupmntamt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,230);
	public ZonedDecimalData fundsetupdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,247);
	public ZonedDecimalData pmntstartdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,255);
	public ZonedDecimalData docaccptdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,263);
	public ZonedDecimalData frstpmntdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,271);
	public ZonedDecimalData anniversdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,279);
	public ZonedDecimalData finalpmntdt = DD.fundsetupdt.copyToZonedDecimal().isAPartOf(dataFields,287);
	public FixedLengthStringData annpmntstatus = DD.premstatus_longdesc.copy().isAPartOf(dataFields,295);	
	
	public FixedLengthStringData payee = DD.ownersel.copy().isAPartOf(dataFields,325);
	public FixedLengthStringData payeename = DD.cownername.copy().isAPartOf(dataFields,335);
	public FixedLengthStringData pmntcurr = DD.cntcurr.copy().isAPartOf(dataFields,382);
	public FixedLengthStringData pmntcurrdesc = DD.pmntcurrdesc.copy().isAPartOf(dataFields,385);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(dataFields,435);
	public FixedLengthStringData payeeaccnum = DD.bankacckey.copy().isAPartOf(dataFields,436);//check
	public FixedLengthStringData bankCd = DD.zbankcd.copy().isAPartOf(dataFields,456);			
	public FixedLengthStringData branchCd = DD.zbranchcd.copy().isAPartOf(dataFields,460);	
	public FixedLengthStringData kanabank = DD.zknbank.copy().isAPartOf(dataFields,463);
	public FixedLengthStringData kanabranch = DD.zknbranch.copy().isAPartOf(dataFields,493);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,523);
	public FixedLengthStringData bnkactyp = DD.bnkactyp.copy().isAPartOf(dataFields,553);
	public FixedLengthStringData annpayplan = DD.annpayplan.copy().isAPartOf(dataFields,557);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(160).isAPartOf(dataArea, 558);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData servbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);	
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cltsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData annuitypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData beneftypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData totannufundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	
	public FixedLengthStringData annpmntoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData annupmnttrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData partpmntamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData annpmntfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData dfrrdtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData dfrrdamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData annupmntamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);	
	public FixedLengthStringData fundsetupdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pmntstartdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData docaccptdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData frstpmntdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData anniversdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	
	public FixedLengthStringData payeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);	
	public FixedLengthStringData payeenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData pmntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData pmntcurrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData rgpymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData payeeaccnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData bankCdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);			
	public FixedLengthStringData branchCdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData kanabankErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData kanabranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData bnkactypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData finalpmntdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData annpmntstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData annpayplanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(480).isAPartOf(dataArea, 718);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] servbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cltsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);        	
	public FixedLengthStringData[] annuitypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] beneftypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] totannufundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	
	public FixedLengthStringData[] annpmntoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] annupmnttrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] partpmntamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] annpmntfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] dfrrdtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] dfrrdamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] annupmntamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] fundsetupdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pmntstartdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] docaccptdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] frstpmntdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] anniversdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] finalpmntdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] annpmntstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	
	public FixedLengthStringData[] payeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] payeenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] pmntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] pmntcurrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rgpymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] payeeaccnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] bankCdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);	
	public FixedLengthStringData[] branchCdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);	
	public FixedLengthStringData[] kanabankOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] kanabranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] bnkactypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);	
	public FixedLengthStringData[] annpayplanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);	
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fundsetupdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData pmntstartdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData docaccptdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData frstpmntdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData anniversdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData finalpmntdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData scrndesc = new FixedLengthStringData(30);
	public FixedLengthStringData numsel =  new FixedLengthStringData(10);

	public LongData Sjl15screenWritten = new LongData(0);
	public LongData Sjl15protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sjl15ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(annpmntoptOut,new String[] {"01","40","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annupmnttrmOut,new String[] {"02","41","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(partpmntamtOut,new String[] {"03","42","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annpmntfreqOut,new String[] {"04","43","-04",null, null, null, null, null, null, null, null, null});		
		fieldIndMap.put(dfrrdtrmOut,new String[] {"05","44","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(docaccptdtOut,new String[] {"06","45","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frstpmntdtOut,new String[] {"07","46","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeeOut,new String[] {"08","47", "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeenameOut,new String[] {"09","48", "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pmntcurrOut,new String[] {"10","49", "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pmntcurrdescOut,new String[] {"11","50", "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpymopOut,new String[] {"12","51", "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeeaccnumOut,new String[] {"13","52","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annpmntstatusOut,new String[] {"14","53","-14",null, null, null, null, null, null, null, null, null});
		
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, occdate, servbr, cownnum, cownername, lifenum, lifename, cltsex,
				annuitype, beneftype, totannufund, annpmntopt, annupmnttrm, partpmntamt, annpmntfreq, dfrrdtrm, dfrrdamt, annupmntamt,
				fundsetupdt, pmntstartdt, docaccptdt, frstpmntdt, anniversdt, finalpmntdt, annpmntstatus, payee, payeename, pmntcurr, pmntcurrdesc,
				rgpymop, payeeaccnum, bankCd, branchCd, kanabank, kanabranch, bankaccdsc, bnkactyp, annpayplan};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, occdateOut, servbrOut, cownnumOut, cownernameOut, lifenumOut, lifenameOut, cltsexOut,
			annuitypeOut, beneftypeOut, totannufundOut, annpmntoptOut, annupmnttrmOut, partpmntamtOut, annpmntfreqOut, dfrrdtrmOut, dfrrdamtOut, annupmntamtOut,
			fundsetupdtOut, pmntstartdtOut, docaccptdtOut, frstpmntdtOut, anniversdtOut, finalpmntdtOut, annpmntstatusOut, payeeOut, payeenameOut, pmntcurrOut, pmntcurrdescOut,
			rgpymopOut, payeeaccnumOut, bankCdOut, branchCdOut, kanabankOut, kanabranchOut, bankaccdscOut, bnkactypOut, annpayplanOut};		
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, occdateErr, servbrErr, cownnumErr, cownernameErr, lifenumErr, lifenameErr, cltsexErr,
			annuitypeErr, beneftypeErr, totannufundErr, annpmntoptErr, annupmnttrmErr, partpmntamtErr, annpmntfreqErr, dfrrdtrmErr, dfrrdamtErr, annupmntamtErr,
			fundsetupdtErr, pmntstartdtErr, docaccptdtErr, frstpmntdtErr, anniversdtErr, finalpmntdtErr, annpmntstatusErr, payeeErr, payeenameErr, pmntcurrErr, pmntcurrdescErr,
			rgpymopErr, payeeaccnumErr, bankCdErr, branchCdErr, kanabankErr, kanabranchErr, bankaccdscErr, bnkactypErr, annpayplanErr};
		screenDateFields = new BaseData[]{occdate, fundsetupdt, pmntstartdt, docaccptdt, frstpmntdt, anniversdt, finalpmntdt};
		screenDateErrFields = new BaseData[]{occdateErr, fundsetupdtErr, pmntstartdtErr, docaccptdtErr, frstpmntdtErr, anniversdtErr, finalpmntdtErr};
		screenDateDispFields = new BaseData[]{occdateDisp, fundsetupdtDisp, pmntstartdtDisp, docaccptdtDisp, frstpmntdtDisp, anniversdtDisp, finalpmntdtDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;		
		screenRecord = Sjl15screen.class;
		protectRecord = Sjl15protect.class;
	}

}

