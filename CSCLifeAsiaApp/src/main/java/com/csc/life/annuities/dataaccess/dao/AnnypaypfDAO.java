package com.csc.life.annuities.dataaccess.dao;

import java.util.List;

import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AnnypaypfDAO extends BaseDAO<Annypaypf>{
	
	public Annypaypf getAnnypaypf(String coy,String chdrnum);
	public List<Annypaypf> getRecordbyStat(String coy,String chdrnum, String status);
	public void updateDate(Annypaypf annypaypf);	
	public void updateRecord(Annypaypf annypaypf);
	public void updatewthldngtax(Annypaypf annypaypf);//ILJ-286
	public void updateAnnypayrecord(Annypaypf annypaypf);
	public void insertAnnypaypf(Annypaypf annypaypf);
	public List<Annypaypf> getAnnypaypfRecords(String coy,String chdrnum);
	public Annypaypf getAnnypaypfRecord(String coy,String chdrnum, int pmntno);//ILJ-286
	public void updateAnnypaypfByAnnpmntopt(Annypaypf pf);
	public void deleteAnnypaypfRecord(String chdrcoy, String chdrnum, int tranno);	
	public void updateValidflag(String chdrcoy, String chdrnum);
	public  List<Annypaypf> getAnnypaypfbyEffdt(String chdrcoy,String chdrnum, int effectivedt, int tranno, String batctrcde);
	public void updateValidflg(Annypaypf annypaypf);	
	public void updateFlag(Annypaypf annypaypf);
	public Annypaypf getAnnypaypfRecords(String chdrcoy, String chdrnum, int pmntno , int effectivedt, int tranno, String batctrcde);
	
}


