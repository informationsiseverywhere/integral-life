/******************************************************************************
 * File Name 		: HdivpfDAO.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The DAO Interface for HDIVPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface HdivpfDAO extends BaseDAO<Hdivpf> {

	public List<Hdivpf> searchHdivpfRecord(Hdivpf hdivpf) throws SQLRuntimeException;
	public void bulkUpdateHdivRecords(List<Hdivpf> hdivList);
	public List<Hdivpf> getHdivcshRecords(Hdivpf hdivpf);

/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction*/
	public void insertHdivData(Hdivpf hdivpf);
	public List<Hdivpf> searchHdivdopRecord(Hdivpf hdivpf) throws SQLRuntimeException;
	public void updateHdivdopRecord(Hdivpf hdivpf);
	public List<Hdivpf> searchHdivcshRecord(Hdivpf hdivpf) throws SQLRuntimeException;
//ILIFE-3790 by dpuhawan
	public List<Hdivpf> searchHdivintRecord(Hdivpf hdivpf) throws SQLRuntimeException;
	public boolean insertHdivpf(List<Hdivpf> hdivpfList);
	public Map<String, List<Hdivpf>> getHdivcshMap(String coy, List<String> chdrnumList);
	public Map<String, List<Hdivpf>> getHdivMap(String coy, List<String> chdrnumList); //ILIFE-9080
	public void deleteHdivpfBasedOnTranno(Hdivpf hdivpf);
	public List<Hdivpf> getHdivintRecord(Hdivpf hdivpf);
	public void updateHdivRecord(Hdivpf hdivpf);
	public List<Hdivpf> getHdivdopRecord(Hdivpf hdivpf);
	public List<Hdivpf> searchHdivdop(Hdivpf hdivpf);
}
