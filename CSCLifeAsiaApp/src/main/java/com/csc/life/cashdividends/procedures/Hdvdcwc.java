/*
 * File: Hdvdcwc.java
 * Date: 29 August 2009 22:53:36
 * Author: Quipoz Limited
 *
 * Class transformed from HDVDCWC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivcshTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdvdcwcrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   TRADITIONAL LIFE - CASH DIVIDEND WITHDRAWAL CALCULATION
*
*   This subroutine is called from PH533 via T6598.
*   It is responsible for calculating the accumulated dividend
*   and interest for each component.  The subroutine is called
*   twice for each component, once for calculating the dividend
*   and once for the interest.  HCWC-ENDF is used to control
*   which calculation should take place.
*
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read HCSD for the dividend option and cash dividend method
*   - Depending on HCWC-ENDF, process withdrawal value in 2 steps
*        space - accumulated dividend
*          1   - O/S interest
*
*   Accumulate Dividend>>
*   - Read HDIS for the dividend balance since last capitalisa-
*     tion date
*   - next capitalisation date not > HCWC-EFFECTIVE-DATE,
*     return zero HCWC-ACTUAL-VAL and exit
*   - Accumulate dividend balance since last capitalisation date
*     to total dividend.
*   - Get all withdrawn dividend since last capitalisation date
*     from HDIV via HDIVCSH. Where these values will be -ve in
*     nature, add them to the total dividend
*   - set these values,
*         HCWC-TYPE to 'D'
*         HCWC-DESCRIPTION to 'Dividend'
*         HCWC-NEXT-CAP-DATE to HDIS next capitalisation date
*         HCWC-ACTUAL-VAL to total dividend
*         HCWC-ZDIVOPT to HCSD-ZDIVOPT
*         HCWC-ZCSHDIVMTH to HCSD-ZCSHDIVMTH
*
*   O/S Interest      >>
*   - Read HDIS for the O/S interest to be capitalised
*   - next capitalisation date not > HCWC-EFFECTIVE-DATE,
*     return zero HCWC-ACTUAL-VAL and exit
*   - Accumulate O/S interest into total interest
*   - Set the return values
*         HCWC-TYPE to 'I'
*         HCWC-DESCRIPTION to 'O/S Int.'
*         HCWC-NEXT-CAP-DATE to HDIS next capitalisation date
*         HCWC-ACTUAL-VAL to total interest
*         HCWC-ZDIVOPT to HCSD-ZDIVOPT
*         HCWC-ZCSHDIVMTH to HCSD-ZCSHDIVMTH
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hdvdcwc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HDVDCWC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-ACCUMULATORS */
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotInt = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
	private String h155 = "H155";
	private String hdisrec = "HDISREC";
	private String hcsdrec = "HCSDREC";
	private String hdivcshrec = "HDIVCSHREC";
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Cash Withdraw Trans. Details*/
	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	private Hdvdcwcrec hdvdcwcrec = new Hdvdcwcrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit3090,
		exit4090,
		seExit9090,
		dbExit9190
	}

	public Hdvdcwc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		hdvdcwcrec.withdrawRec = convertAndSetParam(hdvdcwcrec.withdrawRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		main1000();
		return1900();
	}

protected void main1000()
	{
		hdvdcwcrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(hdvdcwcrec.chdrChdrcoy);
		hcsdIO.setChdrnum(hdvdcwcrec.chdrChdrnum);
		hcsdIO.setLife(hdvdcwcrec.lifeLife);
		hcsdIO.setCoverage(hdvdcwcrec.covrCoverage);
		hcsdIO.setRider(hdvdcwcrec.covrRider);
		hcsdIO.setPlanSuffix(hdvdcwcrec.plnsfx);
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);

		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hcsdIO.getStatuz());
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		if (isEQ(hdvdcwcrec.endf,SPACES)) {
			accumDividend3000();
			hdvdcwcrec.endf.set("1");
		}
		else {
			if (isEQ(hdvdcwcrec.endf,"1")) {
				interest4000();
				hdvdcwcrec.statuz.set(varcom.endp);
			}
			else {
				syserrrec.statuz.set(h155);
				systemError9000();
			}
		}
	}

protected void return1900()
	{
		exitProgram();
	}

protected void accumDividend3000()
	{
		try {
			start3100();
		}
		catch (GOTOException e){
		}
	}

protected void start3100()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(hdvdcwcrec.chdrChdrcoy);
		hdisIO.setChdrnum(hdvdcwcrec.chdrChdrnum);
		hdisIO.setLife(hdvdcwcrec.lifeLife);
		hdisIO.setCoverage(hdvdcwcrec.covrCoverage);
		hdisIO.setRider(hdvdcwcrec.covrRider);
		hdisIO.setPlanSuffix(hdvdcwcrec.plnsfx);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		//start ILIFE-1061
		//if (isNE(hdisIO.getStatuz(),varcom.oK)) {
		if (isNE(hdisIO.getStatuz(),varcom.oK) && isNE(hdisIO.getStatuz(),varcom.mrnf)) {
		//end ILIFE-1061
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			dbError9100();
		}
		if (isLTE(hdisIO.getNextCapDate(),hdvdcwcrec.effectiveDate)) {
			hdvdcwcrec.actualVal.set(ZERO);
			goTo(GotoLabel.exit3090);
		}
		wsaaTotDividend.set(hdisIO.getBalSinceLastCap());
		hdivcshIO.setDataArea(SPACES);
		hdivcshIO.setChdrcoy(hdvdcwcrec.chdrChdrcoy);
		hdivcshIO.setChdrnum(hdvdcwcrec.chdrChdrnum);
		hdivcshIO.setLife(hdvdcwcrec.lifeLife);
		hdivcshIO.setCoverage(hdvdcwcrec.covrCoverage);
		hdivcshIO.setRider(hdvdcwcrec.covrRider);
		hdivcshIO.setPlanSuffix(hdvdcwcrec.plnsfx);
		compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
		hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hdivcshIO.getStatuz());
			syserrrec.params.set(hdivcshIO.getParams());
			dbError9100();
		}
		if (isNE(hdivcshIO.getChdrcoy(),hdvdcwcrec.chdrChdrcoy)
		|| isNE(hdivcshIO.getChdrnum(),hdvdcwcrec.chdrChdrnum)
		|| isNE(hdivcshIO.getLife(),hdvdcwcrec.lifeLife)
		|| isNE(hdivcshIO.getCoverage(),hdvdcwcrec.covrCoverage)
		|| isNE(hdivcshIO.getRider(),hdvdcwcrec.covrRider)
		|| isNE(hdivcshIO.getPlanSuffix(),hdvdcwcrec.plnsfx)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			wsaaTotDividend.add(hdivcshIO.getDivdAmount());
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(hdivcshIO.getStatuz());
				syserrrec.params.set(hdivcshIO.getParams());
				dbError9100();
			}
			if (isNE(hdivcshIO.getChdrcoy(),hdvdcwcrec.chdrChdrcoy)
			|| isNE(hdivcshIO.getChdrnum(),hdvdcwcrec.chdrChdrnum)
			|| isNE(hdivcshIO.getLife(),hdvdcwcrec.lifeLife)
			|| isNE(hdivcshIO.getCoverage(),hdvdcwcrec.covrCoverage)
			|| isNE(hdivcshIO.getRider(),hdvdcwcrec.covrRider)
			|| isNE(hdivcshIO.getPlanSuffix(),hdvdcwcrec.plnsfx)) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}

		hdvdcwcrec.nextCapDate.set(hdisIO.getNextCapDate());
		hdvdcwcrec.actualVal.set(wsaaTotDividend);
		hdvdcwcrec.type.set("D");
		hdvdcwcrec.description.set("Dividend");
		hdvdcwcrec.zdivopt.set(hcsdIO.getZdivopt());
		hdvdcwcrec.zcshdivmth.set(hcsdIO.getZcshdivmth());
	}

protected void interest4000()
	{
		try {
			start4100();
		}
		catch (GOTOException e){
		}
	}

protected void start4100()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(hdvdcwcrec.chdrChdrcoy);
		hdisIO.setChdrnum(hdvdcwcrec.chdrChdrnum);
		hdisIO.setLife(hdvdcwcrec.lifeLife);
		hdisIO.setCoverage(hdvdcwcrec.covrCoverage);
		hdisIO.setRider(hdvdcwcrec.covrRider);
		hdisIO.setPlanSuffix(hdvdcwcrec.plnsfx);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		//Start ILIFE-1061
		//if (isNE(hdisIO.getStatuz(),varcom.oK)) {
		if (isNE(hdisIO.getStatuz(),varcom.oK) && isNE(hdisIO.getStatuz(),varcom.mrnf)) {
		//Start ILIFE-1061
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			dbError9100();
		}
		if (isLTE(hdisIO.getNextCapDate(),hdvdcwcrec.effectiveDate)) {
			hdvdcwcrec.actualVal.set(ZERO);
			goTo(GotoLabel.exit4090);
		}
		wsaaTotInt.set(hdisIO.getOsInterest());
		hdvdcwcrec.nextCapDate.set(hdisIO.getNextCapDate());
		hdvdcwcrec.actualVal.set(wsaaTotInt);
		hdvdcwcrec.type.set("I");
		hdvdcwcrec.description.set("O/S Int");
		hdvdcwcrec.zdivopt.set(hcsdIO.getZdivopt());
		hdvdcwcrec.zcshdivmth.set(hcsdIO.getZcshdivmth());
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		hdvdcwcrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		hdvdcwcrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
