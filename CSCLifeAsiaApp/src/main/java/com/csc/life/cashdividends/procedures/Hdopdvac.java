/*
 * File: Hdopdvac.java
 * Date: 29 August 2009 22:53:18
 * Author: Quipoz Limited
 *
 * Class transformed from HDOPDVAC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdvdoprec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*                Cash Dividend Accumulation Option
*                ---------------------------------
*
* This subroutine is called from BH526 via TH500.
*
* It is responsible for keeping the dividend within the office
* to attract further interest.                                   .
*
* The bulk of the processing has already been done in the
* dividend allocation, we only need to update HDIS to ensure it
* is not a premium settlement option.
*
*  PROCESSING.
*  -----------
*
* Invalid the current HDIS and write a new one with new TRANNO.
*
*****************************************************************
*
* </pre>
*/
public class Hdopdvac extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HDOPDVAC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String hdisrec = "HDISREC";
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private Hdvdoprec hdvdoprec = new Hdvdoprec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit870,
		exit970
	}

	public Hdopdvac() {
		super();
	}

public void mainline(Object... parmArray)
	{
		hdvdoprec.dividendRec = convertAndSetParam(hdvdoprec.dividendRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		update300();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		/*INITIALISE*/
		syserrrec.subrname.set(wsaaSubr);
		hdvdoprec.statuz.set(varcom.oK);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		/*EXIT*/
	}

protected void update300()
	{
		updateHdis310();
	}

protected void updateHdis310()
	{
		hdisIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hdisIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hdisIO.setLife(hdvdoprec.lifeLife);
		hdisIO.setCoverage(hdvdoprec.covrCoverage);
		hdisIO.setRider(hdvdoprec.covrRider);
		hdisIO.setPlanSuffix(hdvdoprec.plnsfx);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(hdvdoprec.newTranno);
		hdisIO.setSacscode(SPACES);
		hdisIO.setSacstyp(SPACES);
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
	}

protected void databaseError800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start810();
				}
				case exit870: {
					exit870();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit870);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start910();
				}
				case exit970: {
					exit970();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit970);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
