/******************************************************************************
 * File Name 		: Hdispf.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The Model Class for HDISPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Hdispf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "HDISPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String validflag;
	private Integer tranno;
	private String cntcurr;
	private Integer hdv1stdt;
	private Integer hdvldt;
	private Integer hintldt;
	private Integer hintndt;
	private Integer hcapldt;
	private Integer hcapndt;
	private BigDecimal hdvball;
	private BigDecimal hdvbalc;
	private BigDecimal hintos;
	private Integer hdvsmtdt;
	private Integer hdvsmtno;
	private BigDecimal hdvbalst;
	private String sacscode;
	private String sacstyp;
	private String hintduem;
	private String hintdued;
	private String hcapduem;
	private String hcapdued;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	public Hdispf ( ) {
		//Empty Constructor
	}
 //ILIFE-9444 start
	public Hdispf (Hdispf hdispf ) {
		this.uniqueNumber = hdispf.uniqueNumber;
		this.chdrcoy = hdispf.chdrcoy;
		this.chdrnum = hdispf.chdrnum;
		this.cntcurr = hdispf.cntcurr;
		this.coverage = hdispf.coverage;
		this.rider = hdispf.rider;
		this.life = hdispf.life;
		this.jlife = hdispf.jlife;
		this.plnsfx = hdispf.plnsfx;
		this.validflag = hdispf.validflag;
		this.tranno = hdispf.tranno;
		this.hdv1stdt = hdispf.hdv1stdt;
		this.hdvldt = hdispf.hdvldt;
		this.hintldt = hdispf.hintldt;
		this.hintndt = hdispf.hintndt;
		this.hcapldt = hdispf.hcapldt;
		this.hcapndt = hdispf.hcapndt;
		this.hdvball = hdispf.hdvball;
		this.hdvbalc = hdispf.hdvbalc;
		this.hintos = hdispf.hintos;
		this.hdvsmtdt = hdispf.hdvsmtdt;
		this.hdvsmtno = hdispf.hdvsmtno;
		this.hdvbalst = hdispf.hdvbalst;
		this.sacscode = hdispf.sacscode;
		this.sacstyp = hdispf.sacstyp;
		this.hintduem = hdispf.hintduem;
		this.hintdued = hdispf.hintdued;
		this.hcapduem = hdispf.hcapduem;
		this.hcapdued = hdispf.hcapdued;
		this.usrprf = hdispf.usrprf;
		this.jobnm = hdispf.jobnm;
		this.datime = hdispf.datime;
	}
 //ILIFE-9444 end
	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public String getValidflag(){
		return this.validflag;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public String getCntcurr(){
		return this.cntcurr;
	}
	public Integer getHdv1stdt(){
		return this.hdv1stdt;
	}
	public Integer getHdvldt(){
		return this.hdvldt;
	}
	public Integer getHintldt(){
		return this.hintldt;
	}
	public Integer getHintndt(){
		return this.hintndt;
	}
	public Integer getHcapldt(){
		return this.hcapldt;
	}
	public Integer getHcapndt(){
		return this.hcapndt;
	}
	public BigDecimal getHdvball(){
		return this.hdvball;
	}
	public BigDecimal getHdvbalc(){
		return this.hdvbalc;
	}
	public BigDecimal getHintos(){
		return this.hintos;
	}
	public Integer getHdvsmtdt(){
		return this.hdvsmtdt;
	}
	public Integer getHdvsmtno(){
		return this.hdvsmtno;
	}
	public BigDecimal getHdvbalst(){
		return this.hdvbalst;
	}
	public String getSacscode(){
		return this.sacscode;
	}
	public String getSacstyp(){
		return this.sacstyp;
	}
	public String getHintduem(){
		return this.hintduem;
	}
	public String getHintdued(){
		return this.hintdued;
	}
	public String getHcapduem(){
		return this.hcapduem;
	}
	public String getHcapdued(){
		return this.hcapdued;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setValidflag( String validflag ){
		 this.validflag = validflag;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setCntcurr( String cntcurr ){
		 this.cntcurr = cntcurr;
	}
	public void setHdv1stdt( Integer hdv1stdt ){
		 this.hdv1stdt = hdv1stdt;
	}
	public void setHdvldt( Integer hdvldt ){
		 this.hdvldt = hdvldt;
	}
	public void setHintldt( Integer hintldt ){
		 this.hintldt = hintldt;
	}
	public void setHintndt( Integer hintndt ){
		 this.hintndt = hintndt;
	}
	public void setHcapldt( Integer hcapldt ){
		 this.hcapldt = hcapldt;
	}
	public void setHcapndt( Integer hcapndt ){
		 this.hcapndt = hcapndt;
	}
	public void setHdvball( BigDecimal hdvball ){
		 this.hdvball = hdvball;
	}
	public void setHdvbalc( BigDecimal hdvbalc ){
		 this.hdvbalc = hdvbalc;
	}
	public void setHintos( BigDecimal hintos ){
		 this.hintos = hintos;
	}
	public void setHdvsmtdt( Integer hdvsmtdt ){
		 this.hdvsmtdt = hdvsmtdt;
	}
	public void setHdvsmtno( Integer hdvsmtno ){
		 this.hdvsmtno = hdvsmtno;
	}
	public void setHdvbalst( BigDecimal hdvbalst ){
		 this.hdvbalst = hdvbalst;
	}
	public void setSacscode( String sacscode ){
		 this.sacscode = sacscode;
	}
	public void setSacstyp( String sacstyp ){
		 this.sacstyp = sacstyp;
	}
	public void setHintduem( String hintduem ){
		 this.hintduem = hintduem;
	}
	public void setHintdued( String hintdued ){
		 this.hintdued = hintdued;
	}
	public void setHcapduem( String hcapduem ){
		 this.hcapduem = hcapduem;
	}
	public void setHcapdued( String hcapdued ){
		 this.hcapdued = hcapdued;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("CNTCURR:		");
		output.append(getCntcurr());
		output.append("\r\n");
		output.append("HDV1STDT:		");
		output.append(getHdv1stdt());
		output.append("\r\n");
		output.append("HDVLDT:		");
		output.append(getHdvldt());
		output.append("\r\n");
		output.append("HINTLDT:		");
		output.append(getHintldt());
		output.append("\r\n");
		output.append("HINTNDT:		");
		output.append(getHintndt());
		output.append("\r\n");
		output.append("HCAPLDT:		");
		output.append(getHcapldt());
		output.append("\r\n");
		output.append("HCAPNDT:		");
		output.append(getHcapndt());
		output.append("\r\n");
		output.append("HDVBALL:		");
		output.append(getHdvball());
		output.append("\r\n");
		output.append("HDVBALC:		");
		output.append(getHdvbalc());
		output.append("\r\n");
		output.append("HINTOS:		");
		output.append(getHintos());
		output.append("\r\n");
		output.append("HDVSMTDT:		");
		output.append(getHdvsmtdt());
		output.append("\r\n");
		output.append("HDVSMTNO:		");
		output.append(getHdvsmtno());
		output.append("\r\n");
		output.append("HDVBALST:		");
		output.append(getHdvbalst());
		output.append("\r\n");
		output.append("SACSCODE:		");
		output.append(getSacscode());
		output.append("\r\n");
		output.append("SACSTYP:		");
		output.append(getSacstyp());
		output.append("\r\n");
		output.append("HINTDUEM:		");
		output.append(getHintduem());
		output.append("\r\n");
		output.append("HINTDUED:		");
		output.append(getHintdued());
		output.append("\r\n");
		output.append("HCAPDUEM:		");
		output.append(getHcapduem());
		output.append("\r\n");
		output.append("HCAPDUED:		");
		output.append(getHcapdued());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
