/*
 * File: Bh519.java
 * Date: 29 August 2009 21:28:37
 * Author: Quipoz Limited
 * 
 * Class transformed from BH519.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.cashdividends.dataaccess.HdvxpfTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This batch program is a splitter program responsible for
*   extracting all COVRs that are due for Dividend Allocation.
*
*   The extract records are held in the HDVXPF members created in the
*   prior process.  The HDVXPF member is accessed by the subsequent
*   process driving the actual dividend allocation.
*
*   Initialise
*     - read T5679 for the valid component statii.
*     - set contract number from and to.
*     - calculate last bonus date as BSSC-EFFECTIVE-DATE - 1 yr.
*     - using SQL, extract data from COVRPF and CHDRPF.
*     - define an array to hold HDVX records.
*
*    Read
*     - fetch a block of data into an array for HDVX records.
*
*    Perform    Until End of File
*
*      Edit
*       - dummy
*
*      Update
*       - write HDVX records from array to member(s).
*
*      Read next primary file record
*
*    End Perform
*
*   Close
*     - close SQL
*     - close all opened files.
*
*   Control totals:
*     01 - No. of threads used
*     02 - no. of records extracted
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*                                                                     *
***********************************************************************
* </pre>
*/
public class Bh519 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhdvxpf1rs;
	private java.sql.PreparedStatement sqlhdvxpf1ps;
	private java.sql.Connection sqlhdvxpf1conn;
	private String sqlhdvxpf1 = "";
	private int hdvxpf1LoopIndex = 0;
	private HdvxpfTableDAM hdvxpf = new HdvxpfTableDAM();
	private DiskFileDAM hdvx01 = new DiskFileDAM("HDVX01");
	private DiskFileDAM hdvx02 = new DiskFileDAM("HDVX02");
	private DiskFileDAM hdvx03 = new DiskFileDAM("HDVX03");
	private DiskFileDAM hdvx04 = new DiskFileDAM("HDVX04");
	private DiskFileDAM hdvx05 = new DiskFileDAM("HDVX05");
	private DiskFileDAM hdvx06 = new DiskFileDAM("HDVX06");
	private DiskFileDAM hdvx07 = new DiskFileDAM("HDVX07");
	private DiskFileDAM hdvx08 = new DiskFileDAM("HDVX08");
	private DiskFileDAM hdvx09 = new DiskFileDAM("HDVX09");
	private DiskFileDAM hdvx10 = new DiskFileDAM("HDVX10");
	private DiskFileDAM hdvx11 = new DiskFileDAM("HDVX11");
	private DiskFileDAM hdvx12 = new DiskFileDAM("HDVX12");
	private DiskFileDAM hdvx13 = new DiskFileDAM("HDVX13");
	private DiskFileDAM hdvx14 = new DiskFileDAM("HDVX14");
	private DiskFileDAM hdvx15 = new DiskFileDAM("HDVX15");
	private DiskFileDAM hdvx16 = new DiskFileDAM("HDVX16");
	private DiskFileDAM hdvx17 = new DiskFileDAM("HDVX17");
	private DiskFileDAM hdvx18 = new DiskFileDAM("HDVX18");
	private DiskFileDAM hdvx19 = new DiskFileDAM("HDVX19");
	private DiskFileDAM hdvx20 = new DiskFileDAM("HDVX20");
	private HdvxpfTableDAM hdvxpfData = new HdvxpfTableDAM();
		/*    Change the record length to that of the temporary file.
		    This can be found by doing a DSPFD of the file being
		    duplicated by the CRTTMPF process.*/
	private FixedLengthStringData hdvx01Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx02Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx03Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx04Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx05Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx06Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx07Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx08Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx09Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx10Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx11Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx12Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx13Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx14Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx15Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx16Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx17Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx18Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx19Rec = new FixedLengthStringData(74);
	private FixedLengthStringData hdvx20Rec = new FixedLengthStringData(74);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH519");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaHdvxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHdvxFn, 0, FILLER).init("HDVX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHdvxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHdvxFn, 6).setUnsigned();
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaLastBonusDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat12 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat12 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat12 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat12 = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
//	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private P6671par p6671par = new P6671par();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();
	
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class); 

	public Bh519() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		getT56791300();
		wsaaCompany.set(bsprIO.getCompany());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaLastBonusDate.set(datcon2rec.intDate2);
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		sqlhdvxpf1 = " SELECT  CO.CHDRCOY, CO.CHDRNUM, CO.LIFE, CO.JLIFE, CO.COVERAGE, CO.RIDER, CO.PLNSFX, CO.CRTABLE, CO.PSTATCODE, CO.STATCODE, CO.CRRCD, CO.CBUNST, CO.RCESDTE, CO.SUMINS, CO.CURRFROM, CO.CURRTO, CO.BNUSIN, CH.CNTCURR, CH.TRANNO, CH.PTDATE" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + "  CO,  " + getAppVars().getTableNameOverriden("CHDRPF") + "  CH" +
" WHERE CO.CHDRCOY = ?" +
" AND (CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?)" +
" AND (CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?)" +
" AND CO.VALIDFLAG = '1'" +
" AND CO.BNUSIN = 'D'" +
" AND CO.CBUNST <= ?" +
" AND CH.CHDRCOY = CO.CHDRCOY" +
" AND CH.CHDRNUM = CO.CHDRNUM" +
" AND CH.VALIDFLAG = '1'" +
" AND (CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?)" +
" AND (CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?)" +
" AND CH.CHDRNUM BETWEEN ? AND ?" +
" ORDER BY CO.CHDRCOY, CO.CHDRNUM, CO.LIFE, CO.COVERAGE, CO.RIDER, CO.PLNSFX";
		sqlerrorflag = false;
		try {
			sqlhdvxpf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM(), new com.csc.fsu.general.dataaccess.ChdrpfTableDAM()});
			sqlhdvxpf1ps = getAppVars().prepareStatementEmbeded(sqlhdvxpf1conn, sqlhdvxpf1);
			getAppVars().setDBString(sqlhdvxpf1ps, 1, wsaaCompany);
			getAppVars().setDBString(sqlhdvxpf1ps, 2, wsaaT5679CovRstat01);
			getAppVars().setDBString(sqlhdvxpf1ps, 3, wsaaT5679CovRstat02);
			getAppVars().setDBString(sqlhdvxpf1ps, 4, wsaaT5679CovRstat03);
			getAppVars().setDBString(sqlhdvxpf1ps, 5, wsaaT5679CovRstat04);
			getAppVars().setDBString(sqlhdvxpf1ps, 6, wsaaT5679CovRstat05);
			getAppVars().setDBString(sqlhdvxpf1ps, 7, wsaaT5679CovRstat06);
			getAppVars().setDBString(sqlhdvxpf1ps, 8, wsaaT5679CovRstat07);
			getAppVars().setDBString(sqlhdvxpf1ps, 9, wsaaT5679CovRstat08);
			getAppVars().setDBString(sqlhdvxpf1ps, 10, wsaaT5679CovRstat09);
			getAppVars().setDBString(sqlhdvxpf1ps, 11, wsaaT5679CovRstat10);
			getAppVars().setDBString(sqlhdvxpf1ps, 12, wsaaT5679CovRstat11);
			getAppVars().setDBString(sqlhdvxpf1ps, 13, wsaaT5679CovRstat12);
			getAppVars().setDBString(sqlhdvxpf1ps, 14, wsaaT5679CovPstat01);
			getAppVars().setDBString(sqlhdvxpf1ps, 15, wsaaT5679CovPstat02);
			getAppVars().setDBString(sqlhdvxpf1ps, 16, wsaaT5679CovPstat03);
			getAppVars().setDBString(sqlhdvxpf1ps, 17, wsaaT5679CovPstat04);
			getAppVars().setDBString(sqlhdvxpf1ps, 18, wsaaT5679CovPstat05);
			getAppVars().setDBString(sqlhdvxpf1ps, 19, wsaaT5679CovPstat06);
			getAppVars().setDBString(sqlhdvxpf1ps, 20, wsaaT5679CovPstat07);
			getAppVars().setDBString(sqlhdvxpf1ps, 21, wsaaT5679CovPstat08);
			getAppVars().setDBString(sqlhdvxpf1ps, 22, wsaaT5679CovPstat09);
			getAppVars().setDBString(sqlhdvxpf1ps, 23, wsaaT5679CovPstat10);
			getAppVars().setDBString(sqlhdvxpf1ps, 24, wsaaT5679CovPstat11);
			getAppVars().setDBString(sqlhdvxpf1ps, 25, wsaaT5679CovPstat12);
			getAppVars().setDBNumber(sqlhdvxpf1ps, 26, wsaaLastBonusDate);
			getAppVars().setDBString(sqlhdvxpf1ps, 27, wsaaT5679CnRstat01);
			getAppVars().setDBString(sqlhdvxpf1ps, 28, wsaaT5679CnRstat02);
			getAppVars().setDBString(sqlhdvxpf1ps, 29, wsaaT5679CnRstat03);
			getAppVars().setDBString(sqlhdvxpf1ps, 30, wsaaT5679CnRstat04);
			getAppVars().setDBString(sqlhdvxpf1ps, 31, wsaaT5679CnRstat05);
			getAppVars().setDBString(sqlhdvxpf1ps, 32, wsaaT5679CnRstat06);
			getAppVars().setDBString(sqlhdvxpf1ps, 33, wsaaT5679CnRstat07);
			getAppVars().setDBString(sqlhdvxpf1ps, 34, wsaaT5679CnRstat08);
			getAppVars().setDBString(sqlhdvxpf1ps, 35, wsaaT5679CnRstat09);
			getAppVars().setDBString(sqlhdvxpf1ps, 36, wsaaT5679CnRstat10);
			getAppVars().setDBString(sqlhdvxpf1ps, 37, wsaaT5679CnRstat11);
			getAppVars().setDBString(sqlhdvxpf1ps, 38, wsaaT5679CnRstat12);
			getAppVars().setDBString(sqlhdvxpf1ps, 39, wsaaT5679CnPstat01);
			getAppVars().setDBString(sqlhdvxpf1ps, 40, wsaaT5679CnPstat02);
			getAppVars().setDBString(sqlhdvxpf1ps, 41, wsaaT5679CnPstat03);
			getAppVars().setDBString(sqlhdvxpf1ps, 42, wsaaT5679CnPstat04);
			getAppVars().setDBString(sqlhdvxpf1ps, 43, wsaaT5679CnPstat05);
			getAppVars().setDBString(sqlhdvxpf1ps, 44, wsaaT5679CnPstat06);
			getAppVars().setDBString(sqlhdvxpf1ps, 45, wsaaT5679CnPstat07);
			getAppVars().setDBString(sqlhdvxpf1ps, 46, wsaaT5679CnPstat08);
			getAppVars().setDBString(sqlhdvxpf1ps, 47, wsaaT5679CnPstat09);
			getAppVars().setDBString(sqlhdvxpf1ps, 48, wsaaT5679CnPstat10);
			getAppVars().setDBString(sqlhdvxpf1ps, 49, wsaaT5679CnPstat11);
			getAppVars().setDBString(sqlhdvxpf1ps, 50, wsaaT5679CnPstat12);
			getAppVars().setDBString(sqlhdvxpf1ps, 51, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlhdvxpf1ps, 52, wsaaChdrnumTo);
			sqlhdvxpf1rs = getAppVars().executeQuery(sqlhdvxpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHdvxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HDVX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHdvxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 50)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			hdvx01.openOutput();
		}
		if (isEQ(iz,2)) {
			hdvx02.openOutput();
		}
		if (isEQ(iz,3)) {
			hdvx03.openOutput();
		}
		if (isEQ(iz,4)) {
			hdvx04.openOutput();
		}
		if (isEQ(iz,5)) {
			hdvx05.openOutput();
		}
		if (isEQ(iz,6)) {
			hdvx06.openOutput();
		}
		if (isEQ(iz,7)) {
			hdvx07.openOutput();
		}
		if (isEQ(iz,8)) {
			hdvx08.openOutput();
		}
		if (isEQ(iz,9)) {
			hdvx09.openOutput();
		}
		if (isEQ(iz,10)) {
			hdvx10.openOutput();
		}
		if (isEQ(iz,11)) {
			hdvx11.openOutput();
		}
		if (isEQ(iz,12)) {
			hdvx12.openOutput();
		}
		if (isEQ(iz,13)) {
			hdvx13.openOutput();
		}
		if (isEQ(iz,14)) {
			hdvx14.openOutput();
		}
		if (isEQ(iz,15)) {
			hdvx15.openOutput();
		}
		if (isEQ(iz,16)) {
			hdvx16.openOutput();
		}
		if (isEQ(iz,17)) {
			hdvx17.openOutput();
		}
		if (isEQ(iz,18)) {
			hdvx18.openOutput();
		}
		if (isEQ(iz,19)) {
			hdvx19.openOutput();
		}
		if (isEQ(iz,20)) {
			hdvx20.openOutput();
		}
	}

protected void getT56791300()
	{
		getT56791310();
	}

protected void getT56791310()
	{
		/*itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());*/
		
		List<Itempf> itemList=itempfDAO.findItem("IT", bsprIO.getCompany().toString(), t5679, bprdIO.getAuthCode().toString());
		
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		
		wsaaT5679CovRstat01.set(t5679rec.covRiskStat[1]);
		wsaaT5679CovRstat02.set(t5679rec.covRiskStat[2]);
		wsaaT5679CovRstat03.set(t5679rec.covRiskStat[3]);
		wsaaT5679CovRstat04.set(t5679rec.covRiskStat[4]);
		wsaaT5679CovRstat05.set(t5679rec.covRiskStat[5]);
		wsaaT5679CovRstat06.set(t5679rec.covRiskStat[6]);
		wsaaT5679CovRstat07.set(t5679rec.covRiskStat[7]);
		wsaaT5679CovRstat08.set(t5679rec.covRiskStat[8]);
		wsaaT5679CovRstat09.set(t5679rec.covRiskStat[9]);
		wsaaT5679CovRstat10.set(t5679rec.covRiskStat[10]);
		wsaaT5679CovRstat11.set(t5679rec.covRiskStat[11]);
		wsaaT5679CovRstat12.set(t5679rec.covRiskStat[12]);
		wsaaT5679CovPstat01.set(t5679rec.covPremStat[1]);
		wsaaT5679CovPstat02.set(t5679rec.covPremStat[2]);
		wsaaT5679CovPstat03.set(t5679rec.covPremStat[3]);
		wsaaT5679CovPstat04.set(t5679rec.covPremStat[4]);
		wsaaT5679CovPstat05.set(t5679rec.covPremStat[5]);
		wsaaT5679CovPstat06.set(t5679rec.covPremStat[6]);
		wsaaT5679CovPstat07.set(t5679rec.covPremStat[7]);
		wsaaT5679CovPstat08.set(t5679rec.covPremStat[8]);
		wsaaT5679CovPstat09.set(t5679rec.covPremStat[9]);
		wsaaT5679CovPstat10.set(t5679rec.covPremStat[10]);
		wsaaT5679CovPstat11.set(t5679rec.covPremStat[11]);
		wsaaT5679CovPstat12.set(t5679rec.covPremStat[12]);
		wsaaT5679CnRstat01.set(t5679rec.cnRiskStat[1]);
		wsaaT5679CnRstat02.set(t5679rec.cnRiskStat[2]);
		wsaaT5679CnRstat03.set(t5679rec.cnRiskStat[3]);
		wsaaT5679CnRstat04.set(t5679rec.cnRiskStat[4]);
		wsaaT5679CnRstat05.set(t5679rec.cnRiskStat[5]);
		wsaaT5679CnRstat06.set(t5679rec.cnRiskStat[6]);
		wsaaT5679CnRstat07.set(t5679rec.cnRiskStat[7]);
		wsaaT5679CnRstat08.set(t5679rec.cnRiskStat[8]);
		wsaaT5679CnRstat09.set(t5679rec.cnRiskStat[9]);
		wsaaT5679CnRstat10.set(t5679rec.cnRiskStat[10]);
		wsaaT5679CnRstat11.set(t5679rec.cnRiskStat[11]);
		wsaaT5679CnRstat12.set(t5679rec.cnRiskStat[12]);
		wsaaT5679CnPstat01.set(t5679rec.cnPremStat[1]);
		wsaaT5679CnPstat02.set(t5679rec.cnPremStat[2]);
		wsaaT5679CnPstat03.set(t5679rec.cnPremStat[3]);
		wsaaT5679CnPstat04.set(t5679rec.cnPremStat[4]);
		wsaaT5679CnPstat05.set(t5679rec.cnPremStat[5]);
		wsaaT5679CnPstat06.set(t5679rec.cnPremStat[6]);
		wsaaT5679CnPstat07.set(t5679rec.cnPremStat[7]);
		wsaaT5679CnPstat08.set(t5679rec.cnPremStat[8]);
		wsaaT5679CnPstat09.set(t5679rec.cnPremStat[9]);
		wsaaT5679CnPstat10.set(t5679rec.cnPremStat[10]);
		wsaaT5679CnPstat11.set(t5679rec.cnPremStat[11]);
		wsaaT5679CnPstat12.set(t5679rec.cnPremStat[12]);
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPstatcode[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaStatcode[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaBnusin[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCntcurr[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCrrcd[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCbunst[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaRcesdte[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaSumins[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCurrfrom[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCurrto[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaPtdate[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFiles2010();
		}

protected void readFiles2010()
	{
		/*    Now a block of records is fetched into the array.*/
		/*    Also on the first entry into the program we must set up the*/
		/*    WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (hdvxpf1LoopIndex = 1; isLTE(hdvxpf1LoopIndex,wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlhdvxpf1rs); hdvxpf1LoopIndex++ ){
				getAppVars().getDBObject(sqlhdvxpf1rs, 1, wsaaFetchArrayInner.wsaaChdrcoy[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 2, wsaaFetchArrayInner.wsaaChdrnum[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 3, wsaaFetchArrayInner.wsaaLife[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 4, wsaaFetchArrayInner.wsaaJlife[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 5, wsaaFetchArrayInner.wsaaCoverage[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 6, wsaaFetchArrayInner.wsaaRider[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 7, wsaaFetchArrayInner.wsaaPlnsfx[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 8, wsaaFetchArrayInner.wsaaCrtable[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 9, wsaaFetchArrayInner.wsaaPstatcode[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 10, wsaaFetchArrayInner.wsaaStatcode[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 11, wsaaFetchArrayInner.wsaaCrrcd[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 12, wsaaFetchArrayInner.wsaaCbunst[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 13, wsaaFetchArrayInner.wsaaRcesdte[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 14, wsaaFetchArrayInner.wsaaSumins[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 15, wsaaFetchArrayInner.wsaaCurrfrom[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 16, wsaaFetchArrayInner.wsaaCurrto[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 17, wsaaFetchArrayInner.wsaaBnusin[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 18, wsaaFetchArrayInner.wsaaCntcurr[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 19, wsaaFetchArrayInner.wsaaTranno[hdvxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdvxpf1rs, 20, wsaaFetchArrayInner.wsaaPtdate[hdvxpf1LoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*    If either of the above cases occur, then an SQLCODE = +100*/
		/*    is returned.*/
		/*    The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		else if(isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		} 
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*    If the CHDRNUM being processed is not equal to the previous*/
		/*    one we should move to the next output file member.*/
		/*    The condition is here to allow for checking the last CHDRNUM*/
		/*    of the old block with the first of the new and to move to*/
		/*    the next HDVX member to write to if they have changed.*/
		if (isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3201();
	}

protected void start3201()
	{
		hdvxpfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
		hdvxpfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
		hdvxpfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
		hdvxpfData.jlife.set(wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()]);
		hdvxpfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
		hdvxpfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
		hdvxpfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
		hdvxpfData.crtable.set(wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()]);
		hdvxpfData.pstatcode.set(wsaaFetchArrayInner.wsaaPstatcode[wsaaInd.toInt()]);
		hdvxpfData.statcode.set(wsaaFetchArrayInner.wsaaStatcode[wsaaInd.toInt()]);
		hdvxpfData.crrcd.set(wsaaFetchArrayInner.wsaaCrrcd[wsaaInd.toInt()]);
		hdvxpfData.unitStatementDate.set(wsaaFetchArrayInner.wsaaCbunst[wsaaInd.toInt()]);
		hdvxpfData.riskCessDate.set(wsaaFetchArrayInner.wsaaRcesdte[wsaaInd.toInt()]);
		hdvxpfData.sumins.set(wsaaFetchArrayInner.wsaaSumins[wsaaInd.toInt()]);
		hdvxpfData.currfrom.set(wsaaFetchArrayInner.wsaaCurrfrom[wsaaInd.toInt()]);
		hdvxpfData.currto.set(wsaaFetchArrayInner.wsaaCurrto[wsaaInd.toInt()]);
		hdvxpfData.bonusInd.set(wsaaFetchArrayInner.wsaaBnusin[wsaaInd.toInt()]);
		hdvxpfData.cntcurr.set(wsaaFetchArrayInner.wsaaCntcurr[wsaaInd.toInt()]);
		hdvxpfData.tranno.set(wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()]);
		hdvxpfData.ptdate.set(wsaaFetchArrayInner.wsaaPtdate[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy,1)) {
			hdvx01.write(hdvxpfData);
		}
		if (isEQ(iy,2)) {
			hdvx02.write(hdvxpfData);
		}
		if (isEQ(iy,3)) {
			hdvx03.write(hdvxpfData);
		}
		if (isEQ(iy,4)) {
			hdvx04.write(hdvxpfData);
		}
		if (isEQ(iy,5)) {
			hdvx05.write(hdvxpfData);
		}
		if (isEQ(iy,6)) {
			hdvx06.write(hdvxpfData);
		}
		if (isEQ(iy,7)) {
			hdvx07.write(hdvxpfData);
		}
		if (isEQ(iy,8)) {
			hdvx08.write(hdvxpfData);
		}
		if (isEQ(iy,9)) {
			hdvx09.write(hdvxpfData);
		}
		if (isEQ(iy,10)) {
			hdvx10.write(hdvxpfData);
		}
		if (isEQ(iy,11)) {
			hdvx11.write(hdvxpfData);
		}
		if (isEQ(iy,12)) {
			hdvx12.write(hdvxpfData);
		}
		if (isEQ(iy,13)) {
			hdvx13.write(hdvxpfData);
		}
		if (isEQ(iy,14)) {
			hdvx14.write(hdvxpfData);
		}
		if (isEQ(iy,15)) {
			hdvx15.write(hdvxpfData);
		}
		if (isEQ(iy,16)) {
			hdvx16.write(hdvxpfData);
		}
		if (isEQ(iy,17)) {
			hdvx17.write(hdvxpfData);
		}
		if (isEQ(iy,18)) {
			hdvx18.write(hdvxpfData);
		}
		if (isEQ(iy,19)) {
			hdvx19.write(hdvxpfData);
		}
		if (isEQ(iy,20)) {
			hdvx20.write(hdvxpfData);
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/		
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*    Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlhdvxpf1conn, sqlhdvxpf1ps, sqlhdvxpf1rs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			hdvx01.close();
		}
		if (isEQ(iz,2)) {
			hdvx02.close();
		}
		if (isEQ(iz,3)) {
			hdvx03.close();
		}
		if (isEQ(iz,4)) {
			hdvx04.close();
		}
		if (isEQ(iz,5)) {
			hdvx05.close();
		}
		if (isEQ(iz,6)) {
			hdvx06.close();
		}
		if (isEQ(iz,7)) {
			hdvx07.close();
		}
		if (isEQ(iz,8)) {
			hdvx08.close();
		}
		if (isEQ(iz,9)) {
			hdvx09.close();
		}
		if (isEQ(iz,10)) {
			hdvx10.close();
		}
		if (isEQ(iz,11)) {
			hdvx11.close();
		}
		if (isEQ(iz,12)) {
			hdvx12.close();
		}
		if (isEQ(iz,13)) {
			hdvx13.close();
		}
		if (isEQ(iz,14)) {
			hdvx14.close();
		}
		if (isEQ(iz,15)) {
			hdvx15.close();
		}
		if (isEQ(iz,16)) {
			hdvx16.close();
		}
		if (isEQ(iz,17)) {
			hdvx17.close();
		}
		if (isEQ(iz,18)) {
			hdvx18.close();
		}
		if (isEQ(iz,19)) {
			hdvx19.close();
		}
		if (isEQ(iz,20)) {
			hdvx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaHdvxData = FLSInittedArray (1000, 75);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaHdvxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaHdvxData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaHdvxData, 9);
	private FixedLengthStringData[] wsaaJlife = FLSDArrayPartOfArrayStructure(2, wsaaHdvxData, 11);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaHdvxData, 13);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaHdvxData, 15);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaHdvxData, 17);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaHdvxData, 20);
	private FixedLengthStringData[] wsaaPstatcode = FLSDArrayPartOfArrayStructure(2, wsaaHdvxData, 24);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaHdvxData, 26);
	private PackedDecimalData[] wsaaCrrcd = PDArrayPartOfArrayStructure(8, 0, wsaaHdvxData, 28);
	private PackedDecimalData[] wsaaCbunst = PDArrayPartOfArrayStructure(8, 0, wsaaHdvxData, 33);
	private PackedDecimalData[] wsaaRcesdte = PDArrayPartOfArrayStructure(8, 0, wsaaHdvxData, 38);
	private PackedDecimalData[] wsaaSumins = PDArrayPartOfArrayStructure(17, 2, wsaaHdvxData, 43);
	private PackedDecimalData[] wsaaCurrfrom = PDArrayPartOfArrayStructure(8, 0, wsaaHdvxData, 52);
	private PackedDecimalData[] wsaaCurrto = PDArrayPartOfArrayStructure(8, 0, wsaaHdvxData, 57);
	private FixedLengthStringData[] wsaaBnusin = FLSDArrayPartOfArrayStructure(1, wsaaHdvxData, 62);
	private FixedLengthStringData[] wsaaCntcurr = FLSDArrayPartOfArrayStructure(3, wsaaHdvxData, 63);
	private PackedDecimalData[] wsaaTranno = PDArrayPartOfArrayStructure(5, 0, wsaaHdvxData, 66);
	private PackedDecimalData[] wsaaPtdate = PDArrayPartOfArrayStructure(10, 2, wsaaHdvxData, 69);
}
}
