/*
 * File: Hcvmatp.java
 * Date: 29 August 2009 22:52:35
 * Author: Quipoz Limited
 *
 * Class transformed from HCVMATP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HmtdTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.model.Hmtdpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.dataaccess.MatdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.HmtdpfDAO;
import com.csc.life.terminationclaims.recordstructures.Matpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*         CASH DIVIDEND MATURITY PROCESSING SUBROUTINE.
*
*   This is called from various programs via T6598.
*
*   This subroutine is used as the processing routine for
*   maturity of a traditional cash dividend contract.  It
*   involves writing offset entries to withdraw the cash dividend
*   and interest, writing accounting entries for each type of
*   withdraw within the maturity, repay existing policy loans,
*   place proceeds to contract suspense and change the contract
*   status.
*
*   This program is simply cloned from TRDMATP with addition
*   cash dividend processing to replace the reversionary bonus
*   processing.  The major modifications made in 2000 section.
*
*   Additional files are:
*   HPUA - Paid-up Addition Coverage Details
*   HCSD - Cash Dividend Details
*   HDIV - Dividend Allocation Transaction Details
*   HDIS - Dividend & Interest Summary
*   HMTD - Maturity Additional Details
*
*   This program is an item entry on T6598, the Claim Subroutine
*   method table.
*
*   READR the MATHCLM record for the contract, this contains the
*   contract level details for the maturity including the
*   adjustment amount entered at the time of maturity.
*
*   Do a BEGN read on the maturity Claim Details records
*   (MATDCLM) for the passed key. Accumulate value 'S' field
*   type, 'P' field type, 'D' field type and 'I' field type
*   MATDCLM records in the working storage.
*   (One total will be sufficient for all).
*
*   Do a NEXTR on MATDCLM.
*
*   For each MATDCLM record found do a component posting, and
*   when MATDCLM = ENDP then do the balancing postings.
*
*   Each of the postings will be as follows:-
*
*   If the amounts are not zero, then call LIFACMV to post to
*   the correct maturity accounts. The posting required is
*   defined in the appropriate line number on T5645 table entry
*   for this subroutine. Set up and pass the linkage areas
*   noting some of the values below.
*
*   When all MATD records have been processed for this MATH, then
*   process any loans which may be outstanding.
*   This is done by first calling TOTLOAN to bring
*   the interest on loans for this contract up to date.
*   ACMVs are then required for paying off loans. For each of
*   relevant entries on T5645, call LOANPYMT with the outstanding
*   maturity amount until all loan is 'allocated' or
*   all entries have been processed and some of the maturity
*   value remains.
*   Then write an ACMV record to suspense for the residual of
*   the value to be matured after the above.
*
*
*
*   Additional Processing:
*
*   1000- section
*   - Read T5679
*   - Read HCSD
*
*   2000-PROCESS-MATHCLM
*   - After processing all MATHCLM,
*     if MATHCLM-CURRCD <> contract currency
*     post total maturity value into exchange accounts
*     line 18   LP  CY   payment currency value
*     line 19   LP  CY   contract currency value
*
*   3000-PROCESS-MATDCLM
*   - Read HMTD
*   - Depending on MATDCLM-FIELD-TYPE, process accordingly in 4
*     steps
*          S   - basic cash value
*          P   - paid up addition
*          D   - accumulated dividend
*          I   - O/S interest
*   - After all steps processed, accumulate the maturity values
*     for the MATDCLM by contract currency and payment currency
*
*   END-OF-CONTRACT
*
*
*   Basic Cash Value >>
*   - Invalidate the current HPUAs and write new ones with TRANNO
*     set to MATP-TRANNO, prem and risk set to that on T5679
*   - Post ACMV with line 1 on T5645 (3 for component level acct)
*
*   Paid-up Addition Cash Value >>
*   - Post ACMV with line 2 on T5645 (4 for component level acct)
*
*   Dividend >>
*   - Invalid the current HDIS
*   - Write a new HDIV to denote dividend withdrawal with,
*        DIVD-AMOUNT = HDIS-BAL-SINCE-LAST-CAP * -1
*        DIVD-TYPE   = 'C'
*        INT-CAP-DATE = 99999999
*        EFFDATE, ALLOC-DATE = MATP-EFFDATE
*        TRANNO      = MATDCLM-TRANNO
*        DIVD-RATE   = 0
*   - Write a new HDIV to denote interest withdrawal if <> 0,with
*        DIVD-AMOUNT = HDIS-OS-INTEREST * -1
*        DIVD-TYPE   = 'I'
*   - Write a new HDIS to denote the withdrawal with,
*        TRANNO      = MATP-TRANNO
*        BAL-SINCE-LAST-CAP = 0
*        OS-INTEREST = 0
*   - Post ACMV with line 8 on T5645 where SACSCODE and SACSTYP
*     in HDIS if not blank, should be used.
*
*   Pending Dividend Interest >>
*   - Calculate new interest = HMTD-HACTVAL - ex HDIS-OS-INTERST
*   - transfer existing interest out of interest suspense(LIFACMV)
*       LIFA-ORIGAMT = ex HDIS-OS-INTEREST
*     line  9   LC  IS  ..DVDINTPAY   +   6
*     line 11   LE  IS  ..DVDINTPAY   +   6  component level acct
*   - book new interest since last interest calc date (LIFACMV)
*       LIFA-ORIGAMT = new interest
*     line  5   LC  DI  ..DVDINT      +   7
*     line  6   LE  DI  ..DVDINT      +   7  component level acct
*
*
*   FILE READS.
*   ----------
*
*   Maturity Header file for :
*   - Contract level details.
*   - Adjustment amount entered at time of maturity.
*
*   Maturity Detail file for :
*   - details of individual maturity amounts.
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read T5688 to establish whether contract or component
*     level accounting for this contract type.
*
*   - Read T5645 to get the accounting rules for this
*     subroutine.
*
*   - Read T1688 to get description of the transaction
*     which initiated the maturity.
*
*   - Initial read of MATD file.
*
*   - Read MATH file.
*
*   - Set up ACMV fields which are common to all components on
*     this contract.
*
*   PROCESSING LOOP
*   ---------------
*
*   - Read MATD file sequentially for all maturity records
*     associated with this contract.
*
*   - For each Record :
*
*   - Write an ACMV record.
*     - Fields common to all ACMV's
*
*       LIFA-FUNCTION         - 'PSTW'
*       LIFA-BATCCOY          - Contract Company from linkage
*       LIFA-BATCKEY          - Batch Key details from linkage
*       LIFA-RDOCNUM          - Contract Number from linkage
*       LIFA-TRANNO           - Transaction Number from MATP
*       LIFA-JRNSEQ           - Start from 1, increment by 1
*       LIFA-RLDGCOY          - Contract Company from linkage
*       LIFA-ORIGCURR         - Contract Currency from linkage
*       LIFA-TRANREF          - Contract Number from linkage
*       LIFA-TRANDESC         - Tran. Code description from T1688
*       LIFA-CRATE            - 0
*       LIFA-ACCTAMT          - 0
*       LIFA-GENLCOY          - Contract Company from linkage
*       LIFA-GENLCUR          - Contract Currency from linkage
*       LIFA-POSTYEAR         - Spaces
*       LIFA-POSTMONTH        - Spaces
*       LIFA-EFFDATE          - Effective Mat. Date from linkage
*       LIFA-RCAMT            - 0
*       LIFA-FRCDATE          - Max-date
*       LIFA-TRANSACTION-DATE - Effective Date from linkage
*       LIFA-TRANSACTION-TIME - Time of transaction
*       LIFA-USER             - Transaction User from linkage
*       LIFA-TERMID           - Transaction Term-ID from linkage
*       LIFA-SUBSTITUTE-CODE  - 1 - Contract Type
*                               2 - 5 not set
*     - Fields varying :
*
*       LIFA-SACSCODE         - Account Code from T5645
*       LIFA-RLDGACCT         - If Component Level Accounting :
*                                 Full component Key
*                               If Contract Level Accounting :
*                                 Contract Number
*       LIFA-SACSTYP          - Account Type from T5645
*       LIFA-ORIGAMT          - Amount of posting from MATP rec.
*       LIFA-GLCODE           - General Ledger Code from T5645
*       LIFA-GLSIGN           - General Ledger Sign from T5645
*       LIFA-CONTOT           - Control Total from T5645
*       LIFA-SUBSTITUTE-CODE  - 6 - Component Type
*
*   - Accounting Rules from T5645
*
*     Line 1  : Cash Maturity Value for contract level
*     Line 2  : Paid up Maturity Value for contract level
*     Line 3  : Cash Maturity Value for component level
*     Line 4  : Paid up Maturity Value for component level
*     Line 5  : Dividend Interest at contract level
*     Line 6  : Dividend Interest at component level
*     Line 7  : Adjustment per contract from MATH record
*     Line 8  : Dividend Suspense
*     Line 9  : Interest Suspense at contract level
*     Line 10 :
*     Line 11 : Interest Suspense at component level
*     Line 12 :
*     Line 13 : Payment Suspense
*     Line 14-17: Policy Loan
*     Line 18-19: Currency Ex-change
*
*   - Write ACMV for contract adjustment.
*
*   - Call LOANPYMT using the P2068REC Linkage area for the
*     T5645 entries in the order that they are listed. Each
*     time check to see whether any outstanding maturity
*     remains unallocated.
*
*   - Write balancing ACMV for the total.
*
*   SET EXIT VARIABLES.
*   ------------------
*   set linkage variables as follows :
*
*   To indicate that processing complete :
*
*   - STATUS -        'ENDP'
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hcvmatp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HCVMATP";
	private PackedDecimalData wsaaTodayDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaHdisOsInt = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaHdisRead = new FixedLengthStringData(1).init(SPACES);
	private Validator hdisAlreadyRead = new Validator(wsaaHdisRead, "Y");
	private PackedDecimalData wsaaAdjustmentAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaContractAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaContractHamt = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0);
	private PackedDecimalData wsaaNewInt = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");

	private FixedLengthStringData wsaaT5645 = new FixedLengthStringData(84);
	private FixedLengthStringData[] wsaaStoredT5645 = FLSArrayPartOfStructure(4, 21, wsaaT5645, 0);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	private FixedLengthStringData wsbbT5645 = new FixedLengthStringData(630);
	private FixedLengthStringData[] wsbbStoredT5645 = FLSArrayPartOfStructure(30, 21, wsbbT5645, 0);
	private ZonedDecimalData[] wsbbT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsbbStoredT5645, 0);
	private FixedLengthStringData[] wsbbT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsbbStoredT5645, 2);
	private FixedLengthStringData[] wsbbT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsbbStoredT5645, 16);
	private FixedLengthStringData[] wsbbT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsbbStoredT5645, 18);
	private FixedLengthStringData[] wsbbT5645Sign = FLSDArrayPartOfArrayStructure(1, wsbbStoredT5645, 20);

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaPaidOffFlag = new FixedLengthStringData(1).init("N");
	private Validator noDebts = new Validator(wsaaPaidOffFlag, "Y");
	private Validator inDebt = new Validator(wsaaPaidOffFlag, "N");

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	
	/*ILIFE-3336 Starts*/
	private FixedLengthStringData wsaaPrevCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevLiencd = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPrevTranno = new PackedDecimalData(5);
	private PackedDecimalData wsaaMathPlnsfx = new PackedDecimalData(4);
	private FixedLengthStringData wsaaMathLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaMathChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMathCoy = new FixedLengthStringData(2);
	private PackedDecimalData wsaaMathTranno = new PackedDecimalData(5);
	/*ILIFE-3336 Ends */
	
	/* ERRORS */
	private String e044 = "E044";
	private String e308 = "E308";
	private String mathclmrec = "MATHCLMREC";
	private String matdclmrec = "MATDCLMREC";
	private String hcsdrec = "HCSDREC";
	private String hmtdrec = "HMTDREC";
	private String hdisrec = "HDISREC";
	private String hpuarec = "HPUAREC";
	private String hdivrec = "HDIVREC";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5679 = "T5679";
	private String t5688 = "T5688";
	private Cashedrec cashedrec = new Cashedrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Allocation Trans Details Logica*/
	private HdivTableDAM hdivIO = new HdivTableDAM();
		/*Maturity Additional Details*/
	private HmtdTableDAM hmtdIO = new HmtdTableDAM();
		/*Paid Up Addition Coverage Details Logica*/
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Maturity Detail Record*/
	private MatdclmTableDAM matdclmIO = new MatdclmTableDAM();
		/*Maturity Header Claim File*/
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
	private Matpcpy matpcpy = new Matpcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Totloanrec totloanrec = new Totloanrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
	private PackedDecimalData wsaaNextDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaCurrOsInt = new PackedDecimalData(17, 2).init(0);
	Covrpf covrpf = null;
	Hmtdpf hmtdpf = null;
	private Itempf itempf = null;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	HmtdpfDAO hmtdpfDao = getApplicationContext().getBean("hmtdpfDAO", HmtdpfDAO.class);
	CovrpfDAO covrpfDao = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ZonedDecimalData wsaaSub3 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub4 = new ZonedDecimalData(2, 0).setUnsigned();
	private Th501rec th501rec = new Th501rec();
	private String th501 = "TH501";
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaIssuedAge = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private FixedLengthStringData wsaaMortcls = new FixedLengthStringData(1).isAPartOf(wsaaItem, 6);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItem, 7);
	private boolean cdivFlag = false;
	private Datcon2rec datcon2rec = new Datcon2rec();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	private static final String th527 = "TH527";
	private static final String DVAC = "DVAC";
	private Iterator<Itempf> itempfListIterator;
	private Th527rec th527rec = new Th527rec();
	private PackedDecimalData wsaaAnniversary = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaath527Date = new PackedDecimalData(8, 0).init(0);
	private Hdivdrec hdivdrec = new Hdivdrec();
	private PackedDecimalData wsaaPrevDividend = new PackedDecimalData(18, 3);
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",  HcsdpfDAO.class);
	private List<Hcsdpf> hsdpfList = null;
	private	Hcsdpf hcsdpf = null;
	private int intdividFlag = 0;
	private String t6639 = "T6639";
	private T6639rec t6639rec = new T6639rec();
	private boolean flag = false;
	private boolean endowFlag = false;
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf();
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private static final String PRPRO001 = "PRPRO001";   
	private static final String BTPRO015 = "BTPRO015";

	


	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit0009,
		dbExit9005,
		seExit9105
	}

	public Hcvmatp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		matpcpy.maturityRec = convertAndSetParam(matpcpy.maturityRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			mainlineRoutine0000();
		}
		catch (GOTOException e){
		}
		finally{
			exit0009();
		}
	}

protected void mainlineRoutine0000()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaTranTermid.set(SPACES);
		wsaaTranDate.set(ZERO);
		wsaaTranTime.set(ZERO);
		wsaaTranUser.set(ZERO);
		readFirstMathclm1000();
		if (isEQ(mathclmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit0009);
		}
		wsaaHdisRead.set(SPACES);
		wsaaSequenceNo.set(ZERO);
		wsaaAdjustmentAmt.set(ZERO);
		wsaaContractAmt.set(ZERO);
		wsaaTranno.set(mathclmIO.getTranno());
		wsaaTransTime.set(getCobolTime());
		wsaaBatckey.set(matpcpy.batckey);
		cdivFlag = FeaConfg.isFeatureExist(matpcpy.chdrcoy.toString(), BTPRO015, appVars, "IT");
		endowFlag = FeaConfg.isFeatureExist(matpcpy.chdrcoy.toString(),PRPRO001, appVars, "IT");
		readHcsd1010();
		readTableT56791020();
		readTableT56451100();
		readTableT56881200();
		readTableT16881250();
		getTodayDate1300();
		callTotloan1400();
		setupLifaCommon1500();
		while ( !(isEQ(mathclmIO.getStatuz(),varcom.endp)
		|| isNE(mathclmIO.getChdrcoy(),matpcpy.chdrcoy)
		|| isNE(mathclmIO.getChdrnum(),matpcpy.chdrnum)
		|| isNE(mathclmIO.getTranno(),matpcpy.tranno))) {
			processMathclm2000();
		}

	}

protected void exit0009()
	{
		exitProgram();
	}

protected void readFirstMathclm1000()
	{
		doBegnMathclm1000();
	}

protected void doBegnMathclm1000()
	{
		mathclmIO.setDataArea(SPACES);
		mathclmIO.setChdrcoy(matpcpy.chdrcoy);
		mathclmIO.setChdrnum(matpcpy.chdrnum);
		mathclmIO.setTranno(matpcpy.tranno);
		mathclmIO.setPlanSuffix(matpcpy.planSuffix);
		mathclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mathclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mathclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		mathclmIO.setFormat(mathclmrec);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(),varcom.oK)
		&& isNE(mathclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			syserrrec.statuz.set(mathclmIO.getStatuz());
			dbError9000();
		}
		if (isNE(mathclmIO.getChdrcoy(),matpcpy.chdrcoy)
		|| isNE(mathclmIO.getChdrnum(),matpcpy.chdrnum)
		|| isNE(mathclmIO.getTranno(),matpcpy.tranno)) {
			mathclmIO.setStatuz(varcom.endp);
		}
		/*ILIFE-3336 Starts*/
		wsaaMathPlnsfx.set(mathclmIO.getPlanSuffix());
		wsaaMathLife.set(mathclmIO.getLife());
		wsaaMathChdrnum.set(mathclmIO.getChdrnum());
		wsaaMathCoy.set(mathclmIO.getChdrcoy());
		wsaaMathTranno.set(mathclmIO.getTranno());
		/*ILIFE-3336 Ends*/
	}

protected void readHcsd1010()
	{
		hcsd1010();
	}

protected void hcsd1010()
	{
		hcsdIO.setParams(SPACES);
		hcsdIO.setChdrcoy(matpcpy.chdrcoy);
		hcsdIO.setChdrnum(matpcpy.chdrnum);
		hcsdIO.setLife(matpcpy.life);
		hcsdIO.setCoverage(matpcpy.coverage);
		hcsdIO.setRider(matpcpy.rider);
		hcsdIO.setPlanSuffix(matpcpy.planSuffix);
		hcsdIO.setFunction(varcom.readr);
		hcsdIO.setFormat(hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)
		&& isNE(hcsdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			dbError9000();
		}
	}

protected void readTableT56791020()
	{
		readT56791020();
	}

protected void readT56791020()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(matpcpy.chdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readTableT56451100()
	{
		readT56451100();
	}

protected void readT56451100()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(matpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq("01");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,2)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(add(2,wsaaSub1));
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(add(15,wsaaSub1));
			wsbbT5645Cnttot[wsaaSub2.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsbbT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsbbT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsbbT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsbbT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		itemIO.setItemcoy(matpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(14); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(sub(wsaaSub1,13));
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			wsaaSub2.set(wsaaSub1);
			wsbbT5645Cnttot[wsaaSub2.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsbbT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsbbT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsbbT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsbbT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
	}

protected void readTableT56881200()
	{
		readT56881200();
	}

protected void readT56881200()
	{
		itdmIO.setItemcoy(matpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(matpcpy.cnttype);
		itdmIO.setItmfrm(matpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9000();
		}
		if (isNE(itdmIO.getItemcoy(),matpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),matpcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(matpcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			dbError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaAcctLevel.set(t5688rec.comlvlacc);
	}

protected void readTableT16881250()
	{
		readT16881250();
	}

protected void readT16881250()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matpcpy.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(matpcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set(e044);
			dbError9000();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}

protected void getTodayDate1300()
	{
		/*CALL-DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTodayDate.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void callTotloan1400()
	{
		start1400();
	}

protected void start1400()
	{
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(matpcpy.chdrcoy);
		totloanrec.chdrnum.set(matpcpy.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(matpcpy.effdate);
		totloanrec.tranno.set(matpcpy.tranno);
		totloanrec.batchkey.set(matpcpy.batckey);
		totloanrec.tranTerm.set(matpcpy.termid);
		totloanrec.tranDate.set(matpcpy.date_var);
		totloanrec.tranTime.set(matpcpy.time);
		totloanrec.tranUser.set(matpcpy.user);
		totloanrec.language.set(matpcpy.language);
		totloanrec.postFlag.set("Y");
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError9100();
		}
	}

protected void setupLifaCommon1500()
	{
		setupLifacmv1500();
	}

protected void setupLifacmv1500()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(matpcpy.chdrcoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.rdocnum.set(matpcpy.chdrnum);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.rldgcoy.set(matpcpy.chdrcoy);
		lifacmvrec.origcurr.set(SPACES);
		lifacmvrec.tranref.set(matpcpy.chdrnum);
		lifacmvrec.trandesc.set(wsaaLongdesc);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.genlcoy.set(matpcpy.chdrcoy);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.effdate.set(matpcpy.effdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(wsaaTodayDate);
		lifacmvrec.transactionTime.set(wsaaTransTime);
		lifacmvrec.user.set(matpcpy.user);
		lifacmvrec.termid.set(matpcpy.termid);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.substituteCode[1].set(matpcpy.cnttype);
	}

protected void processMathclm2000()
	{
		processMatdclm2000();
		readNextrMathclm2004();
	}

protected void processMatdclm2000()
	{
		wsaaAdjustmentAmt.set(mathclmIO.getOtheradjst());
		wsaaContractAmt.set(0);
		readFirstMatdclm2100();
		while ( !(isEQ(matdclmIO.getStatuz(),varcom.endp))) {
			processMatdclm3000();
		}

		if (isNE(mathclmIO.getCurrcd(),hmtdIO.getHcnstcur())) {
			lifacmvrec.origcurr.set(hmtdIO.getHcnstcur());
			lifacmvrec.origamt.set(wsaaContractHamt);
			lifacmvrec.sacscode.set(wsbbT5645Sacscode[18]);
			lifacmvrec.sacstyp.set(wsbbT5645Sacstype[18]);
			lifacmvrec.glcode.set(wsbbT5645Glmap[18]);
			lifacmvrec.glsign.set(wsbbT5645Sign[18]);
			lifacmvrec.contot.set(wsbbT5645Cnttot[18]);
			if (isNE(lifacmvrec.origamt,0)) {
				postAcmvRecord7000();
			}
			lifacmvrec.origcurr.set(mathclmIO.getCurrcd());
			lifacmvrec.origamt.set(wsaaContractAmt);
			lifacmvrec.sacscode.set(wsbbT5645Sacscode[19]);
			lifacmvrec.sacstyp.set(wsbbT5645Sacstype[19]);
			lifacmvrec.glcode.set(wsbbT5645Glmap[19]);
			lifacmvrec.glsign.set(wsbbT5645Sign[19]);
			lifacmvrec.contot.set(wsbbT5645Cnttot[19]);
			if (isNE(lifacmvrec.origamt,0)) {
				postAcmvRecord7000();
			}
		}
		if (inDebt.isTrue()) {
			cashedrec.docamt.set(wsaaContractAmt);
			allocateToLoans8000();
		}
		postAdjustmentAmt5000();
		postContractAmt6000();
	}

protected void readNextrMathclm2004()
	{
		mathclmIO.setFormat(mathclmrec);
		mathclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(),varcom.oK)
		&& isNE(mathclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			syserrrec.statuz.set(mathclmIO.getStatuz());
			dbError9000();
		}
		
		/*ILIFE-3336 Starts*/
		if( (isEQ(wsaaMathChdrnum,mathclmIO.getChdrnum())
				&& isEQ(wsaaMathCoy,mathclmIO.getChdrcoy())
				&& isEQ(wsaaMathLife,mathclmIO.getLife())
				&& isEQ(wsaaMathPlnsfx,mathclmIO.getPlanSuffix())
				&& isEQ(wsaaMathTranno,mathclmIO.getTranno())
				&& isNE(mathclmIO.getStatuz(),varcom.endp))){
			        readNextrMathclm2004();
          }
		/*ILIFE-3336 Ends*/
		/*EXIT*/	
	}

protected void readFirstMatdclm2100()
	{
		doBegnMatdclm2100();
	}

protected void doBegnMatdclm2100()
	{
		matdclmIO.setParams(SPACES);
		matdclmIO.setChdrcoy(mathclmIO.getChdrcoy());
		matdclmIO.setChdrnum(mathclmIO.getChdrnum());
		matdclmIO.setTranno(mathclmIO.getTranno());
		matdclmIO.setPlanSuffix(mathclmIO.getPlanSuffix());
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO", "PLNSFX");

		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(),varcom.oK)
		&& isNE(matdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			syserrrec.statuz.set(matdclmIO.getStatuz());
			dbError9000();
		}
		if (isNE(matdclmIO.getChdrcoy(),mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(),mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(),mathclmIO.getTranno())
		|| isNE(matdclmIO.getPlanSuffix(),mathclmIO.getPlanSuffix())) {
			matdclmIO.setStatuz(varcom.endp);
		}
	}

protected void processMatdclm3000()
	{
		postMatdclmRecs3000();
		readNextrMatdclm3006();
	}

protected void postMatdclmRecs3000()
{
		if(cdivFlag && covrpf == null){
			covrpf = covrpfDao.getCovrpfData(matdclmIO.getChdrcoy().toString(), matdclmIO.getChdrnum().toString(), matdclmIO.getLife().toString(), matdclmIO.getCoverage().toString(), matdclmIO.getCrtable().toString());
			calcanniversary();
		}
		hmtdIO.setParams(SPACES);
		hmtdIO.setChdrcoy(matdclmIO.getChdrcoy());
		hmtdIO.setChdrnum(matdclmIO.getChdrnum());
		hmtdIO.setLife(matdclmIO.getLife());
		hmtdIO.setCoverage(matdclmIO.getCoverage());
		hmtdIO.setRider(matdclmIO.getRider());
		hmtdIO.setPlanSuffix(matdclmIO.getPlanSuffix());
		hmtdIO.setTranno(matdclmIO.getTranno());
		hmtdIO.setFieldType(matdclmIO.getFieldType());
		hmtdIO.setFormat(hmtdrec);
		hmtdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hmtdIO);
		if (isNE(hmtdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hmtdIO.getParams());
			syserrrec.statuz.set(hmtdIO.getStatuz());
			dbError9000();
		}
		wsaaContractAmt.add(matdclmIO.getActvalue());
		wsaaContractHamt.add(hmtdIO.getHactval());
		if (componentLevelAccounting.isTrue()) {
			lifacmvrec.substituteCode[6].set(matdclmIO.getCrtable());
			wsaaRldgChdrnum.set(matpcpy.chdrnum);
			wsaaRldgLife.set(matdclmIO.getLife());
			wsaaRldgCoverage.set(matdclmIO.getCoverage());
			wsaaRldgRider.set(matdclmIO.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
		else {
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(matpcpy.chdrnum);
		}
		lifacmvrec.origcurr.set(hmtdIO.getHcnstcur());
		lifacmvrec.origamt.set(hmtdIO.getHactval());
		lifacmvrec.tranno.set(matdclmIO.getTranno());
		postMatdclmRecs3100();
	}

protected void readNextrMatdclm3006()
	{
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(),varcom.oK)
		&& isNE(matdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			syserrrec.statuz.set(matdclmIO.getStatuz());
			dbError9000();
		}
		if (isNE(matdclmIO.getChdrcoy(),mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(),mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(),mathclmIO.getTranno())
		|| isNE(matdclmIO.getPlanSuffix(),mathclmIO.getPlanSuffix())) {
			matdclmIO.setStatuz(varcom.endp);
	       return;
		}
		/*ILIFE-3336 Starts*/
		if( (isEQ(wsaaPrevRider,matdclmIO.getRider())
				&& isEQ(wsaaPrevCoverage,matdclmIO.getCoverage())
				&& isEQ(wsaaPrevLife,matdclmIO.getLife())
				&& isEQ(wsaaPrevLiencd,matdclmIO.getLiencd())
				&& isEQ(wsaaPrevTranno,matdclmIO.getTranno())
				&& isNE(matdclmIO.getStatuz(),varcom.endp))){
			readNextrMatdclm3006();
}
		/*ILIFE-3336 Ends*/
		/*EXIT*/
	}

protected void postMatdclmRecs3100()
	{
	/*ILIFE-3336 Starts*/
	wsaaPrevCoverage.set(matdclmIO.getCoverage());
	wsaaPrevRider.set(matdclmIO.getRider());
	wsaaPrevLife.set(matdclmIO.getLife());
	wsaaPrevLiencd.set(matdclmIO.getLiencd());
	wsaaPrevTranno.set(matdclmIO.getTranno());
	/*ILIFE-3336 Ends*/
		if(cdivFlag){
			if(isEQ(wsaaNextDividend,ZERO))
				calcDividend3000();	
			if(isEQ(wsaaPrevDividend,ZERO)){
			hmtdpf = hmtdpfDao.getHmtdData(matdclmIO.getChdrcoy().toString(), matdclmIO.getChdrnum().toString(), matdclmIO.getLife().toString(), matdclmIO.getCoverage().toString(), matdclmIO.getCrtable().toString(), "D");
			if(hmtdpf != null){
				wsaaPrevDividend.set(hmtdpf.getHactval());
			}	
			}
		}
		basicCashValue3200();
		paidupCashValue3300();
		dividend3400();
		dividendInterest3500();
		if(endowFlag && isEQ(matdclmIO.getFieldType(),"E")){
			zraepf=zraepfDAO.getItemByContractNum(matdclmIO.getChdrnum().toString());
			endowmentposting();
			updateZrae();
		}
		else	
		{
			updateZrae();
		}	
			
	}

protected void basicCashValue3200()
	{
		if (isEQ(matdclmIO.getFieldType(),"S")) {
			hpuaIO.setParams(SPACES);
			hpuaIO.setChdrcoy(matdclmIO.getChdrcoy());
			hpuaIO.setChdrnum(matdclmIO.getChdrnum());
			hpuaIO.setLife(matdclmIO.getLife());
			hpuaIO.setCoverage(matdclmIO.getCoverage());
			hpuaIO.setRider(matdclmIO.getRider());
			hpuaIO.setPlanSuffix(matdclmIO.getPlanSuffix());
			hpuaIO.setPuAddNbr(0);
			hpuaIO.setFormat(hpuarec);
			hpuaIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			hpuaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)
			&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hpuaIO.getParams());
				syserrrec.statuz.set(hpuaIO.getStatuz());
				dbError9000();
			}
			if (isNE(hpuaIO.getChdrcoy(),matdclmIO.getChdrcoy())
			|| isNE(hpuaIO.getChdrnum(),matdclmIO.getChdrnum())
			|| isNE(hpuaIO.getLife(),matdclmIO.getLife())
			|| isNE(hpuaIO.getCoverage(),matdclmIO.getCoverage())
			|| isNE(hpuaIO.getRider(),matdclmIO.getRider())
			|| isNE(hpuaIO.getPlanSuffix(),matdclmIO.getPlanSuffix())) {
				hpuaIO.setStatuz(varcom.endp);
			}
			while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
				hpuaIO.setValidflag("2");
				hpuaIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9000();
				}
				hpuaIO.setValidflag("1");
				hpuaIO.setTranno(matpcpy.tranno);
				hpuaIO.setRstatcode(t5679rec.setCovRiskStat);
				hpuaIO.setPstatcode(t5679rec.setCovPremStat);
				hpuaIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9000();
				}
				setPrecision(hpuaIO.getPuAddNbr(), 0);
				hpuaIO.setPuAddNbr(add(hpuaIO.getPuAddNbr(),1));
				hpuaIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)
				&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9000();
				}
			}

			if (componentLevelAccounting.isTrue()) {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[3]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[3]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[3]);
				lifacmvrec.glsign.set(wsbbT5645Sign[3]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[3]);
			}
			else {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[1]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[1]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[1]);
				lifacmvrec.glsign.set(wsbbT5645Sign[1]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[1]);
			}
			if (isNE(lifacmvrec.origamt,0)) {
				postAcmvRecord7000();
			}
		}
	}

protected void paidupCashValue3300()
	{
		if (isEQ(matdclmIO.getFieldType(),"P")) {
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[4]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[4]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[4]);
				lifacmvrec.glsign.set(wsbbT5645Sign[4]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[4]);
			}
			else {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[2]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[2]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[2]);
				lifacmvrec.glsign.set(wsbbT5645Sign[2]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[2]);
			}
			if (isNE(lifacmvrec.origamt,0)) {
				postAcmvRecord7000();
			}
		}
	}

protected void dividend3400()
	{
		if (isEQ(matdclmIO.getFieldType(),"D")) {
			hdisIO.setParams(SPACES);
			hdisIO.setChdrcoy(matdclmIO.getChdrcoy());
			hdisIO.setChdrnum(matdclmIO.getChdrnum());
			hdisIO.setLife(matdclmIO.getLife());
			hdisIO.setCoverage(matdclmIO.getCoverage());
			hdisIO.setRider(matdclmIO.getRider());
			hdisIO.setPlanSuffix(matdclmIO.getPlanSuffix());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				dbError9000();
			}
			hdisIO.setValidflag("2");
			hdisIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				dbError9000();
			}
			hdivIO.setParams(SPACES);
			hdivIO.setChdrcoy(matdclmIO.getChdrcoy());
			hdivIO.setChdrnum(matdclmIO.getChdrnum());
			hdivIO.setLife(matdclmIO.getLife());
			hdivIO.setCoverage(matdclmIO.getCoverage());
			hdivIO.setRider(matdclmIO.getRider());
			hdivIO.setPlanSuffix(matdclmIO.getPlanSuffix());
			hdivIO.setTranno(matdclmIO.getTranno());
			hdivIO.setEffdate(matpcpy.effdate);
			hdivIO.setDivdAllocDate(matpcpy.effdate);
			hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
			hdivIO.setCntcurr(hdisIO.getCntcurr());
			setPrecision(hdivIO.getDivdAmount(), 2);
			hdivIO.setDivdAmount(mult(hdisIO.getBalSinceLastCap(),-1));
			hdivIO.setDivdRate(ZERO);
			hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
			hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
			hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			hdivIO.setDivdType("C");
			hdivIO.setZdivopt(hcsdIO.getZdivopt());
			hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
			hdivIO.setDivdOptprocTranno(ZERO);
			hdivIO.setDivdCapTranno(ZERO);
			hdivIO.setDivdStmtNo(ZERO);
			hdivIO.setPuAddNbr(ZERO);
			hdivIO.setFormat(hdivrec);
			hdivIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hdivIO);
			if (isNE(hdivIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdivIO.getParams());
				syserrrec.statuz.set(hdivIO.getStatuz());
				dbError9000();
			}
			hdisIO.setValidflag("1");
			hdisIO.setTranno(matpcpy.tranno);
			wsaaHdisRead.set("Y");
			wsaaHdisOsInt.set(hdisIO.getOsInterest());
			hdisIO.setOsInterest(ZERO);
			hdisIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				dbError9000();
			}
			if(cdivFlag){
					compute(wsaaTotDividend, 2).set(sub(hmtdIO.getHactval(),wsaaNextDividend));
					lifacmvrec.origamt.set(wsaaTotDividend);
					matdclmIO.actvalue.set(wsaaTotDividend);
					hmtdIO.setHactval(wsaaTotDividend);
			}
			lifacmvrec.sacscode.set(wsbbT5645Sacscode[8]);
			lifacmvrec.sacstyp.set(wsbbT5645Sacstype[8]);
			lifacmvrec.glcode.set(wsbbT5645Glmap[8]);
			lifacmvrec.glsign.set(wsbbT5645Sign[8]);
			lifacmvrec.contot.set(wsbbT5645Cnttot[8]);
			if (isNE(hdisIO.getSacscode(),SPACES)
			&& isNE(hdisIO.getSacstyp(),SPACES)) {
				lifacmvrec.sacscode.set(hdisIO.getSacscode());
				lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
			}
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(matpcpy.chdrnum);
			if (isNE(lifacmvrec.origamt,0)) {
				postAcmvRecord7000();
			}
			if(cdivFlag){
				if(isNE(wsaaNextDividend,ZERO)){
					wsaaSub3.set(20);
					wsaaSub4.set(4);
					lifacmvrec.origamt.set(wsaaNextDividend);
					setupLifeacmv();
				}
			}
		}
	 }

private void calcDividend3000() {
	
	wsaaNextDividend.set(0);
	if(isGTE(covrpf.getUnitStatementDate(),matpcpy.effdate)){
		return;
	}
	readth527();
	calcDividend();
	if(isNE(hdivdrec.divdAmount,ZERO)){
		wsaaNextDividend.set(hdivdrec.divdAmount);
	}
}

private void readth527() {
	
	wsaaCrtable.set(matpcpy.crtable);
	wsaaIssuedAge.set(covrpf.getAnbAtCcd());
	wsaaMortcls.set(covrpf.getMortcls());		
	wsaaSex.set(covrpf.getSex());

	itempfList = itempfDAO.findBy("IT",matpcpy.chdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
	if(itempfList.size() == 0){ 	
		while(itempfList.size() == 0){
			if (isEQ(subString(wsaaItem, 5, 4),"****")) {
				itdmIO.setItemitem(wsaaItem);
				dbError9000();		
			}
			else {
				if (isNE(subString(wsaaItem, 8, 1),"*")) {
					wsaaItem.setSub1String(8, 1, "*");
				}
				else {
					if (isNE(subString(wsaaItem, 7, 1),"*")) {
						wsaaItem.setSub1String(7, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 5, 2),"**")) {
							wsaaItem.setSub1String(5, 2, "**");
						}
					}
				}
			}
			itempfList = itempfDAO.findBy("IT",matpcpy.chdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
 		}			
 	}
}

private void calcDividend() {
	setuphcsd();
	if(isGT(wsaaAnniversary,matpcpy.effdate)){
		if(isGTE(matpcpy.effdate,covrpf.getUnitStatementDate()) && isLTE(matpcpy.effdate,wsaaAnniversary))
			intdividFlag = 2;
		return;
	}
	itempfListIterator = itempfList.iterator();
	iterateitempfList();
	if(isEQ(th527rec.th527Rec,SPACES)){
		itempfListIterator = itempfList.iterator();
		getLatestFactor();
		if(isNE(th527rec.th527Rec,SPACES)){
			intdividFlag = 1;
			processDividend();
		}
		else{
			return;
		}
	}	
}

protected void setuphcsd(){
	
	hcsdpf = new Hcsdpf();
	hcsdpf.setChdrcoy(matdclmIO.getChdrcoy().toString());
	hcsdpf.setChdrnum(matdclmIO.getChdrnum().toString());	
	hcsdpf.setLife(matdclmIO.getLife().toString());
	hcsdpf.setCoverage(matdclmIO.getCoverage().toString());
	hcsdpf.setRider(matdclmIO.getRider().toString());
	hcsdpf.setPlnsfx(0);		
	hsdpfList = hcsdpfDAO.searchHcsdpfRecord(hcsdpf);
	for(Hcsdpf hcsdobj : hsdpfList){
	if(isEQ(hcsdobj.getValidflag(),"1")){
		itempf = itempfDAO.readItdmpf("IT",matdclmIO.getChdrcoy().toString(),t6639,matdclmIO.getEffdate().toInt(),StringUtils.rightPad(hcsdobj.getZcshdivmth()+covrpf.getPstatcode(), 8));
		if(itempf != null){
			t6639rec.t6639Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else{
			syserrrec.params.set("IT".concat(matdclmIO.getChdrcoy().toString()).concat(t6639).concat(hcsdobj.getZcshdivmth()+covrpf.getPstatcode()));
			systemError9100();
		}
		hcsdpf = hcsdobj;
		break;
	}
  }
}

private void getLatestFactor() {
	
	itempfListIterator = itempfList.iterator();
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
			wsaath527Date.set(itempf.getItmfrm());
			return;
		}else{
			th527rec.th527Rec.set(SPACES);	
			}
		}				
}

private void processDividend() {	
	/* check table has been set up correctly - must have non-zero*/
	/*  risk units otherwise we will be dividing by zero !!*/
	if (isEQ(th527rec.unit,ZERO)) {
		itdmIO.setItemitem(wsaaItem);
		syserrrec.params.set(itdmIO.getParams());
		dbError9000();
	}
	initialize(hdivdrec.dividendRec);
	hdivdrec.crrcd.set(covrpf.getCrrcd());
	hdivdrec.periodTo.set(wsaaAnniversary);
	hdivdrec.periodFrom.set(covrpf.getUnitStatementDate());
	hdivdrec.divdAmount.set(ZERO);
	hdivdrec.sumin.set(covrpf.getSumins());
	hdivdrec.crtable.set(covrpf.getCrtable());
	hdivdrec.issuedAge.set(covrpf.getAnbAtCcd());
	hdivdrec.mortcls.set(covrpf.getMortcls());
	hdivdrec.sex.set(covrpf.getSex());
	hdivdrec.chdrChdrcoy.set(matdclmIO.getChdrcoy());
	hdivdrec.itmfrmTh527.set(wsaath527Date);
	if(isNE(t6639rec.revBonusProg,SPACES)){
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9100();
		}
	}
}

private void iterateitempfList() {
	
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			if((wsaaAnniversary.toInt() >= Integer.parseInt(itempf.getItmfrm().toString()) )
					&& wsaaAnniversary.toInt() <= Integer.parseInt(itempf.getItmto().toString())){
				th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				return;
			}else{
				th527rec.th527Rec.set(SPACES);
			}
		}				
	}	
	
}

protected void setupLifeacmv(){
	
	for (wsaaSub1.set(1); !(isGTE(wsaaSub1,wsaaSub4)) ; wsaaSub1.add(1)){
		lifacmvrec.sacscode.set(wsbbT5645Sacscode[wsaaSub3.toInt()]);
		lifacmvrec.sacstyp.set(wsbbT5645Sacstype[wsaaSub3.toInt()]);
		lifacmvrec.glcode.set(wsbbT5645Glmap[wsaaSub3.toInt()]);
		lifacmvrec.glsign.set(wsbbT5645Sign[wsaaSub3.toInt()]);
		lifacmvrec.contot.set(wsbbT5645Cnttot[wsaaSub3.toInt()]);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(matpcpy.chdrnum);
		if (isNE(lifacmvrec.origamt, 0)) {
			postAcmvRecord7000();
		}
		wsaaSub3.add(1);
	}
}

protected void dividendInterest3500()
	{
		if (isEQ(matdclmIO.getFieldType(),"I")) {
			if(cdivFlag){
				calcOutInst();
			}
			if (hdisAlreadyRead.isTrue()) {
				compute(wsaaNewInt, 2).set(sub(hmtdIO.getHactval(),wsaaHdisOsInt));
				lifacmvrec.origamt.set(wsaaHdisOsInt);
				if(cdivFlag){		
					compute(wsaaNewInt, 2).set(sub(wsaaNewInt,wsaaCurrOsInt));
					hmtdIO.setHactval(sub(hmtdIO.getHactval(),wsaaCurrOsInt));
					matdclmIO.actvalue.set(sub(hmtdIO.getHactval(),wsaaCurrOsInt));
				}
			}
			else {
				hdisIO.setParams(SPACES);
				hdisIO.setChdrcoy(matdclmIO.getChdrcoy());
				hdisIO.setChdrnum(matdclmIO.getChdrnum());
				hdisIO.setLife(matdclmIO.getLife());
				hdisIO.setCoverage(matdclmIO.getCoverage());
				hdisIO.setRider(matdclmIO.getRider());
				hdisIO.setPlanSuffix(matdclmIO.getPlanSuffix());
				hdisIO.setFormat(hdisrec);
				hdisIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, hdisIO);
				if (isNE(hdisIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdisIO.getParams());
					syserrrec.statuz.set(hdisIO.getStatuz());
					dbError9000();
				}
				compute(wsaaNewInt, 2).set(sub(hmtdIO.getHactval(),hdisIO.getOsInterest()));
				lifacmvrec.origamt.set(hdisIO.getOsInterest());
				wsaaHdisOsInt.set(hdisIO.getOsInterest());
				if(cdivFlag){
					compute(wsaaNewInt, 2).set(sub(wsaaNewInt,wsaaCurrOsInt));	
					hmtdIO.setHactval(sub(hmtdIO.getHactval(),wsaaCurrOsInt));
					matdclmIO.actvalue.set(sub(hmtdIO.getHactval(),wsaaCurrOsInt));
				}			
			}
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[11]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[11]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[11]);
				lifacmvrec.glsign.set(wsbbT5645Sign[11]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[11]);
			}
			else {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[9]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[9]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[9]);
				lifacmvrec.glsign.set(wsbbT5645Sign[9]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[9]);
			}
			if (isNE(lifacmvrec.origamt,0)) {
				postAcmvRecord7000();
			}
			lifacmvrec.origamt.set(wsaaNewInt);
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[6]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[6]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[6]);
				lifacmvrec.glsign.set(wsbbT5645Sign[6]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[6]);
			}
			else {
				lifacmvrec.sacscode.set(wsbbT5645Sacscode[5]);
				lifacmvrec.sacstyp.set(wsbbT5645Sacstype[5]);
				lifacmvrec.glcode.set(wsbbT5645Glmap[5]);
				lifacmvrec.glsign.set(wsbbT5645Sign[5]);
				lifacmvrec.contot.set(wsbbT5645Cnttot[5]);
			}
			if (isNE(lifacmvrec.origamt,0)) {
				postAcmvRecord7000();
			}
			if (isNE(hmtdIO.getHactval(),ZERO)) {
				hdivIO.setParams(SPACES);
				hdivIO.setChdrcoy(matdclmIO.getChdrcoy());
				hdivIO.setChdrnum(matdclmIO.getChdrnum());
				hdivIO.setLife(matdclmIO.getLife());
				hdivIO.setCoverage(matdclmIO.getCoverage());
				hdivIO.setRider(matdclmIO.getRider());
				hdivIO.setPlanSuffix(matdclmIO.getPlanSuffix());
				hdivIO.setTranno(matdclmIO.getTranno());
				hdivIO.setEffdate(matpcpy.effdate);
				hdivIO.setDivdAllocDate(matpcpy.effdate);
				hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
				hdivIO.setCntcurr(hdisIO.getCntcurr());
				setPrecision(hdivIO.getDivdAmount(), 2);
				hdivIO.setDivdAmount(mult(hmtdIO.getHactval(),-1));
				hdivIO.setDivdRate(ZERO);
				hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
				hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
				hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
				hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
				hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
				hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
				hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
				hdivIO.setDivdType("I");
				hdivIO.setZdivopt(hcsdIO.getZdivopt());
				hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
				hdivIO.setDivdOptprocTranno(ZERO);
				hdivIO.setDivdCapTranno(ZERO);
				hdivIO.setDivdStmtNo(ZERO);
				hdivIO.setPuAddNbr(ZERO);
				hdivIO.setFormat(hdivrec);
				hdivIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivIO);
				if (isNE(hdivIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivIO.getParams());
					syserrrec.statuz.set(hdivIO.getStatuz());
					dbError9000();
				}
			}
			if (isNE(wsaaNewInt,ZERO)) {
				hdivIO.setDivdType("I");
				hdivIO.setDivdAmount(wsaaNewInt);
				hdivIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivIO);
				if (isNE(hdivIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivIO.getParams());
					syserrrec.statuz.set(hdivIO.getStatuz());
					dbError9000();
				}	
			}
			if(cdivFlag){
				if(isNE(wsaaCurrOsInt,ZERO)){
					wsaaSub3.set(23);
					wsaaSub4.set(5);
					lifacmvrec.origamt.set(wsaaCurrOsInt);
					setupLifeacmv();
				}
			}
		}
	}

	private void endowmentposting() {
		if (zraepf.getChdrnum().length() > 0)
			calculateInterest();

		acblenqListLPAE = acblpfDAO.getAcblenqRecord("2", StringUtils.rightPad(matdclmIO.getChdrnum().toString(), 16),
				wsbbT5645Sacscode[27].toString(), wsbbT5645Sacstype[27].toString());

		acblenqListLPAS = acblpfDAO.getAcblenqRecord("2", StringUtils.rightPad(matdclmIO.getChdrnum().toString(), 16),
				wsbbT5645Sacscode[28].toString(), wsbbT5645Sacstype[28].toString());

		if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
			accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
			accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);
		}
		if (acblenqListLPAS != null && !acblenqListLPAS.isEmpty()) {
			accAmtLPAS = acblenqListLPAS.get(0).getSacscurbal();
			accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
		}

		if (isNE(accAmtLPAE, 0)) {
			lifacmvrec.tranno.set(matdclmIO.getTranno());
			lifacmvrec.rldgacct.set(matdclmIO.getChdrnum());
			lifacmvrec.sacscode.set(wsbbT5645Sacscode[27]);// LP AE
			lifacmvrec.sacstyp.set(wsbbT5645Sacstype[27]);
			lifacmvrec.glcode.set(wsbbT5645Glmap[27]);
			lifacmvrec.glsign.set(wsbbT5645Sign[27]);
			lifacmvrec.contot.set(wsbbT5645Cnttot[27]);
			lifacmvrec.origamt.set(accAmtLPAE);
			lifacmvrec.acctamt.set(accAmtLPAE);
			wsaaSequenceNo.add(1);
			lifacmvrec.jrnseq.set(wsaaSequenceNo);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				return;
			}
			lifacmvrec.sacscode.set(wsbbT5645Sacscode[28]);// LP AS
			lifacmvrec.sacstyp.set(wsbbT5645Sacstype[28]);
			lifacmvrec.glcode.set(wsbbT5645Glmap[28]);
			lifacmvrec.glsign.set(wsbbT5645Sign[28]);
			lifacmvrec.contot.set(wsbbT5645Cnttot[28]);
			accAmtLPAS = add(accAmtLPAS, interestAmount).getbigdata();
			lifacmvrec.origamt.set(accAmtLPAS);
			lifacmvrec.acctamt.set(accAmtLPAS);
			wsaaSequenceNo.add(1);
			lifacmvrec.jrnseq.set(wsaaSequenceNo);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
			}
			lifacmvrec.acctamt.set(ZERO);	//ILB-1265
		}

	}

protected void calculateInterest()
{
	annypyintcalcrec.intcalcRec.set(SPACES);
	annypyintcalcrec.chdrcoy.set(matdclmIO.getChdrcoy());
	annypyintcalcrec.chdrnum.set(matdclmIO.getChdrnum());
	annypyintcalcrec.cnttype.set(matpcpy.cnttype);
	annypyintcalcrec.interestTo.set(matdclmIO.getEffdate());
	annypyintcalcrec.interestFrom.set(zraepf.getAplstintbdte());
	annypyintcalcrec.annPaymt.set(zraepf.getApcaplamt());
	annypyintcalcrec.lastCaplsnDate.set(zraepf.getAplstcapdate());		
	annypyintcalcrec.interestAmount.set(ZERO);
	annypyintcalcrec.currency.set(zraepf.getPaycurr());
	annypyintcalcrec.transaction.set("ENDOWMENT");
	if (isLT(annypyintcalcrec.interestFrom,annypyintcalcrec.interestTo)) {
		callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
		if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annypyintcalcrec.statuz);
			systemError9100();	
		}
	}
	/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 5000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
	if (isNE(annypyintcalcrec.interestAmount, 0)) {
		zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
		callRounding5100();
		annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
		interestAmount.add(annypyintcalcrec.interestAmount);
	}
	
}
protected void callRounding5100()
{
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(matdclmIO.getChdrcoy());
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(zraepf.getPaycurr());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		systemError9100();	
	}
	/*EXIT*/
}

protected void updateZrae()
{
    List<Zraepf> zraepfList;
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(matdclmIO.getChdrnum().toString());
    zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, matdclmIO.getChdrcoy().toString());
    
    if(zraepfList!= null && !zraepfList.isEmpty()){
    	 zraepfDAO.updateValidflag(zraepfList);	
     }
    Zraepf Zraepftmp = new Zraepf();
		if (zraepfList != null && !zraepfList.isEmpty()) {
			Zraepftmp = new Zraepf(zraepfList.get(0));
			Zraepftmp.setApcaplamt(new BigDecimal(0));
			Zraepftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setFlag("2");
			Zraepftmp.setValidflag("1");
			Zraepftmp.setTranno(matdclmIO.getTranno().toInt());
			Zraepftmp.setApintamt(BigDecimal.ZERO);
			Zraepftmp.setTotamnt(0);
			zraepfDAO.insertZraeRecord(Zraepftmp);
		}
}
   



private void calcOutInst() {
	
	if(intdividFlag == 0){
		return;
	}
	if(isEQ(hcsdpf.getZdivopt(),DVAC) && isNE(wsaaPrevDividend,ZERO)){
		itempf = itempfDAO.readItdmpf("IT",matdclmIO.getChdrcoy().toString(),th501,matdclmIO.getEffdate().toInt(),StringUtils.rightPad(hcsdpf.getZcshdivmth(), 8));
		if(itempf != null){
			th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else{
			syserrrec.params.set("IT".concat(matdclmIO.getChdrcoy().toString()).concat(th501).concat(hcsdpf.getZcshdivmth()));
			systemError9100();
		}
	}
	else{
		return;
	}
	initialize(hdvdintrec.divdIntRec);
	hdvdintrec.chdrChdrcoy.set(matdclmIO.getChdrcoy());
	hdvdintrec.chdrChdrnum.set(matdclmIO.getChdrnum());
	hdvdintrec.lifeLife.set(matdclmIO.getLife());
	hdvdintrec.covrCoverage.set(matdclmIO.getCoverage());
	hdvdintrec.covrRider.set(matdclmIO.getRider());
	hdvdintrec.plnsfx.set(ZERO);
	hdvdintrec.cntcurr.set(matdclmIO.getCurrcd());
	hdvdintrec.transcd.set(SPACES);
	hdvdintrec.crtable.set(matdclmIO.getCrtable());
	hdvdintrec.effectiveDate.set(matdclmIO.getEffdate());
	hdvdintrec.tranno.set(ZERO);
	if(intdividFlag == 1){
	hdvdintrec.intFrom.set(wsaaAnniversary);
	}
	else{
	hdvdintrec.intFrom.set(covrpf.getUnitStatementDate());
	}
	hdvdintrec.intTo.set(matdclmIO.getEffdate());
	hdvdintrec.capAmount.set(wsaaPrevDividend);
	hdvdintrec.intDuration.set(ZERO);
	hdvdintrec.intAmount.set(ZERO);
	hdvdintrec.intRate.set(ZERO);
	hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
	if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(hdvdintrec.statuz);
			systemError9100();
		}
		wsaaCurrOsInt.set(hdvdintrec.intAmount);
	}
 }

protected void calcanniversary(){
	
	datcon2rec.intDate1.set(covrpf.getUnitStatementDate());
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		dbError9000();
	}
	else {
		wsaaAnniversary.set(datcon2rec.intDate2);	//CntAnniversaryDate	
	}
}

protected void postAdjustmentAmt5000()
	{
		/*START*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(matpcpy.chdrnum);
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.origamt.set(wsaaAdjustmentAmt);
		if (isNE(lifacmvrec.origamt,ZERO)) {
			postAcmvRecord7000();
		}
		/*EXIT*/
	}

protected void postContractAmt6000()
	{
		/*POST-AMOUNTS*/
		lifacmvrec.sacscode.set(t5645rec.sacscode13);
		lifacmvrec.sacstyp.set(t5645rec.sacstype13);
		lifacmvrec.glcode.set(t5645rec.glmap13);
		lifacmvrec.glsign.set(t5645rec.sign13);
		lifacmvrec.contot.set(t5645rec.cnttot13);
		lifacmvrec.origamt.set(wsaaContractAmt);
		lifacmvrec.origamt.add(wsaaAdjustmentAmt);
		if (isNE(lifacmvrec.origamt,ZERO)) {
			postAcmvRecord7000();
		}
		matpcpy.status.set(varcom.endp);
		/*EXIT*/
	}

protected void postAcmvRecord7000()
	{
		/*START*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError9100();
		}
		/*EXIT*/
	}

protected void allocateToLoans8000()
	{
		start8000();
	}

protected void start8000()
	{
		wsaaGlCompany.set(lifacmvrec.genlcoy);
		wsaaGlCurrency.set(lifacmvrec.genlcur);
		wsaaTranTermid.set(matpcpy.termid);
		wsaaTranUser.set(matpcpy.user);
		wsaaTranTime.set(matpcpy.time);
		wsaaTranDate.set(matpcpy.date_var);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(matpcpy.chdrcoy);
		wsaaTranEntity.set(matpcpy.chdrnum);
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(lifacmvrec.rdocnum);
		cashedrec.doctCompany.set(lifacmvrec.batccoy);
		cashedrec.trandate.set(lifacmvrec.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.transeq.set(wsaaSequenceNo);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(lifacmvrec.origcurr);
		cashedrec.dissrate.set(lifacmvrec.crate);
		cashedrec.trandesc.set(lifacmvrec.trandesc);
		cashedrec.chdrcoy.set(matpcpy.chdrcoy);
		cashedrec.chdrnum.set(matpcpy.chdrnum);
		cashedrec.tranno.set(lifacmvrec.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.language.set(matpcpy.language);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)
		|| isEQ(wsaaContractAmt,0)); wsaaSub1.add(1)){
			loans8200();
		}
		wsaaSequenceNo.set(cashedrec.transeq);
		if (isGT(wsaaContractAmt,0)) {
			wsaaPaidOffFlag.set("Y");
		}
	}

protected void loans8200()
	{
		start8200();
	}

protected void start8200()
	{
		wsaaGlMap.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.origamt.set(wsaaContractAmt);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			systemError9100();
		}
		wsaaContractAmt.set(cashedrec.docamt);
	}

protected void dbError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					chkError9000();
				}
				case dbExit9005: {
					dbExit9005();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void chkError9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9005);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9005()
	{
		matpcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void systemError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					chkError9100();
				}
				case seExit9105: {
					seExit9105();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void chkError9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9105);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9105()
	{
		matpcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
