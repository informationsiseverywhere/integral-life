/*
 * File: Hrevdic.java
 * Date: 29 August 2009 22:55:12
 * Author: Quipoz Limited
 *
 * Class transformed from HREVDIC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisrevTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivricTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*       ----------------------------------------------
*       CASH DIVIDEND INTEREST CAPITALISATION REVERSAL
*       ----------------------------------------------
*
*  This subroutine is called from the Full Contract Reversal
*  program REVGENAT via Table T6661. The parameters passed in
*  is via the REVERSEREC copybook.
*
*  The routine is used to reverse a Cash Dividend Interest
*  Capitalisation.
*
*  All ACMVs that were posted in the forward transaction are
*  reversed.  Transaction records HDIV are reversed in such
*  their capitalised TRANNO = REVE-TRANNO.  Current HDIS
*  records are deleted and validflag '2' records are
*  re-instated.
*  The details of which contract to process originate from
*  the Policy Transaction File (PTRN) and are passed through
*  in the linkage.
*
*  The routine is driven by :
*    - The Company to which this Contract belongs.
*    - The Contract Header Number.
*    - The Transaction Number used in the original
*      cash dividend interest capitalisation transaction.
*
*  PROCESSING
*  ----------
*
*  - Get Today's date.
*
*  - Read the Contract Header details.
*
*  - Get the Transaction Code description from T1688 using
*    Transaction Code passed in through Linkage.
*
*  - Locate HDIV records and delete them. Write new ones with
*    zero capitalised TRANNO.
*
*  - Locate HDIS records and delete the current validflag 1's,
*    re-instate validflag 2 to 1.
*
*  - BEGN on the ACMV file. This will get to the first
*    accounting record which has this Transaction Number.
*
*  - Now find the first record under this number which has the
*    appropriate Transaction Code (the code of the original
*    cash dividend interest capitalisation transaction).
*
*  MAIN PROCESS.
*  -------------
*
*  PROCESS UNTIL HDIVRIC-STATUZ = MRNF
*  - Read hold HDIVRIC and delete it
*  - Post a new HDIV with :
*
*              DIVD-CAP-TRANNO    = 0
*
*  END OF HDIVRIC PROCESSING LOOP.
*
*  BEGH HDISREV.
*  PROCESS UNTIL HDISREV-STATUZ = ENDP
*  - DELTD HDISREV but keep the LAST-STMT-DATE, DIVD-STMT-NO,
*    BAL-AT-STMT-DATE
*  - Read next HDISREV with :
*
*         if this is not a VALIDFLAG 2, something wrong, exit
*         else,WRITD HDIS with
*              VALIDFLAG          = '1'
*              LAST-STMT-NO       = stored
*              LAST-STMT-DATE     = stored
*              BAL-AT-STMT-DATE   = stored
*
*  END OF HDISREV PROCESSING LOOP.
*
*  PROCESS UNTIL ACMV-STATUZ = ENDP
*
*  - Post a new ACMV with :
*
*              ORIGAMT = previous value * -1.
*              TRANSACTION-CODE = Tran Code of the Reversing
*                                 transaction (passed in through
*                                 Linkage).
*              TRANDESC = Description of the reversing Tran Code.
*              TRANSACTION NUMBER = Previous Contract Header
*                                   Tran Number + 1.
*
*  - Find the next ACMV to process by reading the file
*    sequentially until a record of the same transaction code
*    is read.
*
*    If a new Company, Contract Header Number or Transaction
*    Number is encountered, move ENDP to ACMV-STATUZ and
*    effectively exit the processing loop.
*
*  END OF MAIN PROCESSING LOOP.
***********************************************************************
* </pre>
*/
public class Hrevdic extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private PackedDecimalData wsaaStmtNo = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaStmtDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBalance = new PackedDecimalData(17, 2);
	private String hdisrec = "HDISREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String chdrmnarec = "CHDRMNAREC";
	private String hdivricrec = "HDIVRICREC";
	private String hdisrevrec = "HDISREVREC";
		/* TABLES */
	private String t1688 = "T1688";
		/* ERRORS */
	private String hl14 = "HL14";
	private String hl15 = "HL15";
		/*ACCOUNT MOVEMENTS LOGICAL VIEW FOR REVER*/
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Minor Alterations Contract Header*/
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Cash Dividend Alloc Summary (Reversal)*/
	private HdisrevTableDAM hdisrevIO = new HdisrevTableDAM();
		/*Dividend Allocation Trans Details Logica*/
	private HdivTableDAM hdivIO = new HdivTableDAM();
		/*Dividend Interest Capitalisation Reversa*/
	private HdivricTableDAM hdivricIO = new HdivricTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit9090
	}

	public Hrevdic() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline010()
	{
		init010();
		para020();
	}

protected void init010()
	{
		reverserec.statuz.set(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(reverserec.batctrcde);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(reverserec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
	}

protected void para020()
	{
		processHdiv1000();
		processHdis2000();
		procAcmv3000();
		procChdr4000();
		/*EXIT*/
		exitProgram();
	}

protected void processHdiv1000()
	{
		readhHdivric1010();
	}

protected void readhHdivric1010()
	{
		hdivricIO.setStatuz(varcom.oK);
		while ( !(isEQ(hdivricIO.getStatuz(),varcom.mrnf))) {
			hdivricIO.setDataKey(SPACES);
			hdivricIO.setChdrcoy(reverserec.company);
			hdivricIO.setChdrnum(reverserec.chdrnum);
			hdivricIO.setDivdCapTranno(reverserec.tranno);
			hdivricIO.setFormat(hdivricrec);
			hdivricIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hdivricIO);
			if (isNE(hdivricIO.getStatuz(),varcom.oK)
			&& isNE(hdivricIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(hdivricIO.getParams());
				fatalError9000();
			}
			if (isEQ(hdivricIO.getStatuz(),varcom.oK)) {
				hdivricIO.setFunction(varcom.delet);
				SmartFileCode.execute(appVars, hdivricIO);
				if (isNE(hdivricIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivricIO.getParams());
					fatalError9000();
				}
				hdivricIO.setDivdCapTranno(0);
				hdivricIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivricIO);
				if (isNE(hdivricIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivricIO.getParams());
					fatalError9000();
				}
			}
		}

	}

protected void processHdis2000()
	{
		readhHdisrev2010();
	}

protected void readhHdisrev2010()
	{
		hdisrevIO.setDataKey(SPACES);
		hdisrevIO.setChdrcoy(reverserec.company);
		hdisrevIO.setChdrnum(reverserec.chdrnum);
		hdisrevIO.setTranno(reverserec.tranno);
		hdisrevIO.setFormat(hdisrevrec);
		hdisrevIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdisrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, hdisrevIO);
		if (isNE(hdisrevIO.getStatuz(),varcom.oK)
		&& isNE(hdisrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdisrevIO.getParams());
			fatalError9000();
		}
		if (isNE(reverserec.company,hdisrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,hdisrevIO.getChdrnum())
		|| isNE(reverserec.tranno,hdisrevIO.getTranno())) {
			hdisrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdisrevIO.getStatuz(),varcom.endp))) {
			hdisIO.setParams(SPACES);
			hdisIO.setRrn(hdisrevIO.getRrn());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				fatalError9000();
			}
			wsaaStmtNo.set(hdisIO.getDivdStmtNo());
			wsaaStmtDate.set(hdisIO.getDivdStmtDate());
			wsaaBalance.set(hdisIO.getBalAtStmtDate());
			hdisIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				fatalError9000();
			}
			hdisIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)
			&& isNE(hdisIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdisIO.getParams());
				fatalError9000();
			}
			if (isEQ(hdisIO.getStatuz(),varcom.endp)
			|| isNE(hdisIO.getChdrcoy(),hdisrevIO.getChdrcoy())
			|| isNE(hdisIO.getChdrnum(),hdisrevIO.getChdrnum())
			|| isNE(hdisIO.getLife(),hdisrevIO.getLife())
			|| isNE(hdisIO.getCoverage(),hdisrevIO.getCoverage())
			|| isNE(hdisIO.getRider(),hdisrevIO.getRider())
			|| isNE(hdisIO.getPlanSuffix(),hdisrevIO.getPlanSuffix())) {
				hdisrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(hdisIO.getValidflag(),"2")) {
					hdisrevIO.setStatuz(hl15);
					syserrrec.params.set(hdisrevIO.getParams());
					fatalError9000();
				}
				hdisIO.setValidflag("1");
				hdisIO.setDivdStmtNo(wsaaStmtNo);
				hdisIO.setDivdStmtDate(wsaaStmtDate);
				hdisIO.setBalAtStmtDate(wsaaBalance);
				hdisIO.setFormat(hdisrec);
				hdisIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, hdisIO);
				if (isNE(hdisIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdisIO.getParams());
					fatalError9000();
				}
				hdisrevIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hdisrevIO);
				if (isNE(hdisrevIO.getStatuz(),varcom.oK)
				&& isNE(hdisrevIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hdisrevIO.getParams());
					fatalError9000();
				}
				if (isNE(hdisrevIO.getChdrcoy(),reverserec.company)
				|| isNE(hdisrevIO.getChdrnum(),reverserec.chdrnum)
				|| isNE(hdisrevIO.getTranno(),reverserec.tranno)) {
					hdisrevIO.setStatuz(varcom.endp);
				}
			}
		}

	}

protected void procAcmv3000()
	{
		acmvRead3010();
	}

protected void acmvRead3010()
	{
		acmvrevIO.setDataKey(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalError9000();
		}
		if (isEQ(acmvrevIO.getStatuz(),varcom.endp)
		|| isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvRecs3020();
		}

	}

protected void reverseAcmvRecs3020()
	{
		/*START*/
		revAcmv3500();
		acmvrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalError9000();
		}
		if (isNE(acmvrevIO.getRldgcoy(),reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(),reverserec.tranno)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void revAcmv3500()
	{
		acmv1Write3510();
	}

protected void acmv1Write3510()
	{
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.tranno.set(reverserec.newTranno);
		lifacmvrec1.rldgcoy.set(reverserec.company);
		lifacmvrec1.rdocnum.set(reverserec.chdrnum);
		lifacmvrec1.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec1.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec1.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec1.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec1.batccoy.set(reverserec.company);
		lifacmvrec1.batcbrn.set(reverserec.batcbrn);
		lifacmvrec1.batcactyr.set(reverserec.batcactyr);
		lifacmvrec1.batcactmn.set(reverserec.batcactmn);
		lifacmvrec1.batctrcde.set(reverserec.batctrcde);
		lifacmvrec1.batcbatch.set(reverserec.batcbatch);
		lifacmvrec1.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec1.origcurr.set(acmvrevIO.getOrigcurr());
		lifacmvrec1.jrnseq.set(acmvrevIO.getJrnseq());
		setPrecision(acmvrevIO.getOrigamt(), 2);
		acmvrevIO.setOrigamt(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec1.origamt.set(acmvrevIO.getOrigamt());
		lifacmvrec1.rcamt.set(acmvrevIO.getOrigamt());
		lifacmvrec1.tranref.set(acmvrevIO.getTranref());
		lifacmvrec1.trandesc.set(getdescrec.longdesc);
		lifacmvrec1.crate.set(acmvrevIO.getCrate());
		setPrecision(acmvrevIO.getAcctamt(), 2);
		acmvrevIO.setAcctamt(mult(acmvrevIO.getAcctamt(),-1));
		lifacmvrec1.acctamt.set(acmvrevIO.getAcctamt());
		lifacmvrec1.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec1.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.effdate.set(reverserec.effdate1);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(reverserec.transDate);
		lifacmvrec1.transactionTime.set(reverserec.transTime);
		lifacmvrec1.user.set(reverserec.user);
		lifacmvrec1.termid.set(reverserec.termid);
		lifacmvrec1.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
	}

protected void procChdr4000()
	{
		chdrRead4010();
	}

protected void chdrRead4010()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);*/

		chdrlifIO.setFormat(chdrlifrec);
		axesChdr6000();
		if (isNE(chdrlifIO.getValidflag(),"1")) {
			chdrlifIO.setStatuz(hl14);
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		chdrmnaIO.setChdrcoy(chdrlifIO.getChdrcoy());
		chdrmnaIO.setChdrnum(chdrlifIO.getChdrnum());
		chdrmnaIO.setFormat(chdrmnarec);
		chdrmnaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError9000();
		}
		chdrlifIO.setFunction(varcom.deltd);
		chdrlifIO.setFormat(chdrlifrec);
		axesChdr6000();
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		axesChdr6000();
		if (isNE(chdrlifIO.getValidflag(),"2")) {
			chdrlifIO.setStatuz(hl15);
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setCownpfx(chdrmnaIO.getCownpfx());
		chdrlifIO.setCownnum(chdrmnaIO.getCownnum());
		chdrlifIO.setCowncoy(chdrmnaIO.getCowncoy());
		chdrlifIO.setJownnum(chdrmnaIO.getJownnum());
		chdrlifIO.setPayrnum(chdrmnaIO.getPayrnum());
		chdrlifIO.setAsgnpfx(chdrmnaIO.getAsgnpfx());
		chdrlifIO.setAsgncoy(chdrmnaIO.getAsgncoy());
		chdrlifIO.setAsgnnum(chdrmnaIO.getAsgnnum());
		chdrlifIO.setDesppfx(chdrmnaIO.getDesppfx());
		chdrlifIO.setDespcoy(chdrmnaIO.getDespcoy());
		chdrlifIO.setDespnum(chdrmnaIO.getDespnum());
		chdrlifIO.setAgntpfx(chdrmnaIO.getAgntpfx());
		chdrlifIO.setAgntcoy(chdrmnaIO.getAgntcoy());
		chdrlifIO.setAgntnum(chdrmnaIO.getAgntnum());
		chdrlifIO.setBillsupr(chdrmnaIO.getBillsupr());
		chdrlifIO.setBillspfrom(chdrmnaIO.getBillspfrom());
		chdrlifIO.setBillspto(chdrmnaIO.getBillspto());
		chdrlifIO.setNotssupr(chdrmnaIO.getNotssupr());
		chdrlifIO.setNotsspfrom(chdrmnaIO.getNotsspfrom());
		chdrlifIO.setNotsspto(chdrmnaIO.getNotsspto());
		chdrlifIO.setRnwlsupr(chdrmnaIO.getRnwlsupr());
		chdrlifIO.setRnwlspfrom(chdrmnaIO.getRnwlspfrom());
		chdrlifIO.setRnwlspto(chdrmnaIO.getRnwlspto());
		chdrlifIO.setCommsupr(chdrmnaIO.getCommsupr());
		chdrlifIO.setCommspfrom(chdrmnaIO.getCommspfrom());
		chdrlifIO.setCommspto(chdrmnaIO.getCommspto());
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(chdrlifrec);
		axesChdr6000();
	}

protected void axesChdr6000()
	{
		/*START*/
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		try {
			error9010();
		}
		catch (GOTOException e){
		}
		finally{
			exit9090();
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		reverserec.statuz.set(varcom.bomb);
	}

protected void exit9090()
	{
		exitProgram();
	}
}
