package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hpuapf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;

public interface HpuapfDAO extends BaseDAO<Hpuapf> {
	  public List<Hpuapf> selectHpuaRecord(Hpuapf hpuapf);	  
}