package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh534screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 23, 18, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 16, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh534ScreenVars sv = (Sh534ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh534screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh534ScreenVars screenVars = (Sh534ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.submnuprog.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.action.setClassString("");
	}

/**
 * Clear all the variables in Sh534screen
 */
	public static void clear(VarModel pv) {
		Sh534ScreenVars screenVars = (Sh534ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.submnuprog.clear();
		screenVars.chdrsel.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.action.clear();
	}
}
