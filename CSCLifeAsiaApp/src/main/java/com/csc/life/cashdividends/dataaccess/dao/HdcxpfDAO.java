/******************************************************************************
 * File Name 		: HdcxpfDAO.java
 * Author			: sbatra9
 * Creation Date	: 09 July 2020
 * Project			: Integral Life
 * Description		: The DAO Interface for HDCXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdcxpf;

public interface HdcxpfDAO extends BaseDAO<Hdcxpf> {
	public List<Hdcxpf> searchHdcxpfRecord(String tableId, String memName,int batchExtractSize, int batchID);
}