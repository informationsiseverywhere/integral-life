package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdvxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:28
 * Class transformed from HDVXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdvxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 74;
	public FixedLengthStringData hdvxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hdvxpfRecord = hdvxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hdvxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hdvxrec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(hdvxrec);
	public PackedDecimalData unitStatementDate = DD.cbunst.copy().isAPartOf(hdvxrec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(hdvxrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(hdvxrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(hdvxrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(hdvxrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(hdvxrec);
	public PackedDecimalData ptdate = DD.ptdate.copy().isAPartOf(hdvxrec);
	public FixedLengthStringData bonusInd = DD.bnusin.copy().isAPartOf(hdvxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HdvxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HdvxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HdvxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HdvxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdvxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HdvxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdvxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HDVXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"CRTABLE, " +
							"CRRCD, " +
							"CBUNST, " +
							"RCESDTE, " +
							"SUMINS, " +
							"CURRFROM, " +
							"CURRTO, " +
							"CNTCURR, " +
							"TRANNO, " +
							"PSTATCODE, " +
							"STATCODE, " +
							"PTDATE, " +
							"BNUSIN, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     crtable,
                                     crrcd,
                                     unitStatementDate,
                                     riskCessDate,
                                     sumins,
                                     currfrom,
                                     currto,
                                     cntcurr,
                                     tranno,
                                     pstatcode,
                                     statcode,
                                     ptdate,
                                     bonusInd,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		crtable.clear();
  		crrcd.clear();
  		unitStatementDate.clear();
  		riskCessDate.clear();
  		sumins.clear();
  		currfrom.clear();
  		currto.clear();
  		cntcurr.clear();
  		tranno.clear();
  		pstatcode.clear();
  		statcode.clear();
  		ptdate.clear();
  		bonusInd.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHdvxrec() {
  		return hdvxrec;
	}

	public FixedLengthStringData getHdvxpfRecord() {
  		return hdvxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHdvxrec(what);
	}

	public void setHdvxrec(Object what) {
  		this.hdvxrec.set(what);
	}

	public void setHdvxpfRecord(Object what) {
  		this.hdvxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hdvxrec.getLength());
		result.set(hdvxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}