/*
 * File: Hcdicalc.java
 * Date: 29 August 2009 22:51:38
 * Author: Quipoz Limited
 *
 * Class transformed from HCDICALC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th529rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              Cash Dividend Interest Calculation
*              ----------------------------------
*
* This program is a cash dividend calculation subroutine called
* from various programs specified in TH501, by the dividend
* calculation method and the premium status code.
*
* This routine is responsible for returning the dividend interest
* due on the dividend principal for the input period.  The
* calculation is based the interest rate held on TH529, key by
* coverage type.
*
*  CALCULATIONS.
*  -------------
*
* 1. Calculate the interest period.
* 2. Read TH529 with key,
*        HDIA-CRTABLE,
*    If specific item is not located, try again on the generic
*    key '****'.
* 3. Apply the interest rate from TH529 and calculate interest
*        HDIA-INT-AMOUNT =
*           ((HDIA-CAP-AMOUNT * rate * interest period)/ 100
* 4. Return both rate used and rate date to calling program
* 5. Set status to O-K and exit.
*
*****************************************************************
*
* </pre>
*/
public class Hcdicalc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HCDICALC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String g641 = "G641";
		/* FORMATS */
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String th529 = "TH529";
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Th529rec th529rec = new Th529rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit870,
		exit970
	}

	public Hcdicalc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		hdvdintrec.divdIntRec = convertAndSetParam(hdvdintrec.divdIntRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readInterestLinkage300();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		syserrrec.subrname.set(wsaaSubr);
		hdvdintrec.statuz.set(varcom.oK);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(hdvdintrec.intFrom);
		datcon3rec.intDate2.set(hdvdintrec.intTo);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError900();
		}
		hdvdintrec.intDuration.set(datcon3rec.freqFactor);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(hdvdintrec.chdrChdrcoy);
		itdmIO.setItemtabl(th529);
		itdmIO.setItemitem(hdvdintrec.crtable);
		itdmIO.setItmfrm(hdvdintrec.intFrom);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError800();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemtabl(),th529)
		|| isNE(itdmIO.getItemcoy(),hdvdintrec.chdrChdrcoy)
		|| isNE(itdmIO.getItemitem(),hdvdintrec.crtable)) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(hdvdintrec.chdrChdrcoy);
			itdmIO.setItemtabl(th529);
			itdmIO.setItmfrm(hdvdintrec.intFrom);
			itdmIO.setItemitem("****");
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				databaseError800();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),th529)
			|| isNE(itdmIO.getItemcoy(),hdvdintrec.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),"****")) {
				itdmIO.setItemitem("****");
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(g641);
				databaseError800();
			}
		}
		th529rec.th529Rec.set(itdmIO.getGenarea());
	}

protected void readInterestLinkage300()
	{
		/*BEGIN-READING*/
		compute(hdvdintrec.intAmount, 6).setRounded(div((mult(mult(hdvdintrec.capAmount,th529rec.intRate),(div(hdvdintrec.intDuration,365)))),100));
		hdvdintrec.intRate.set(th529rec.intRate);
		hdvdintrec.rateDate.set(itdmIO.getItmfrm());
		/*EXIT*/
	}

protected void databaseError800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start810();
				}
				case exit870: {
					exit870();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit870);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		hdvdintrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start910();
				}
				case exit970: {
					exit970();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit970);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		hdvdintrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
