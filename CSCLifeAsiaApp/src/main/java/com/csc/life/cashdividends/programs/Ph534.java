/*
 * File: Ph534.java
 * Date: 30 August 2009 1:06:09
 * Author: Quipoz Limited
 * 
 * Class transformed from PH534.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //ILIFE-7772
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.cashdividends.screens.Sh534ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
//import com.csc.life.enquiries.programs.P6363.GotoLabel; //ILIFE-7772
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf; //ILIFE-7772
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*     Cash Dividend Withdrawal Sub Menu.
*
* Validation
* ----------
*
*  Key 1 - Contract number (CHDRPTS)
*
*       Y = mandatory, must exist on file.
*            - CHDRPTS  -  Life  new  business  contract  header
*                 logical view.
*            - must be correct status for transaction (T5679).
*            - MUST BE CORRECT BRANCH.
*
*       N = IRREVELANT.
*
*       Blank = irrelevant.
*
*
* Updating
* --------
*
*  Set up WSSP-FLAG:
*       - always "M".                                                (002
*
*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Ph534 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
//	public int numberOfParameters = 0;//ILIFE-7772 this came in Findbugs
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH534");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(0).setUnsigned();
	private String wsaaBsurrOk = "";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6640 = "T6640";
		/* FORMATS */
	private static final String chdrptsrec = "CHDRPTSREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrsurrec = "COVRSURREC";
		/* Dummy user wssp. Replace with required version.
		    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private T6640rec t6640rec = new T6640rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sh534ScreenVars sv = ScreenProgram.getScreenVars( Sh534ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	
	 //ILIFE-7772 start
	protected Chdrpf chdrpf = new Chdrpf();
	//protected Chdrpf chdrenqpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	 //ILIFE-7772 end

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		validateKey22220, 
		exit2290, 
		search2510, 
		exit12090, 
		loopCovrsur2985, 
		exit2989, 
		keeps3070, 
		batching3080, 
		exit3090
	}

	public Ph534() {
		super();
		screenVars = sv;
		new ScreenModel("Sh534", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		sv.effdate.set(varcom.vrcmMaxDate);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'SH534IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SH534-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
				if (isNE(sv.chdrselErr, SPACES)) {
					return ;
				}
				validateEffdate2700();
				if (isEQ(sv.chdrselErr, SPACES)
				&& isEQ(sv.effdateErr, SPACES)) {
					chkValidAction2960();
				}
			}
			else {
				verifyBatchControl2900();
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateKey12210();
				case validateKey22220: 
					validateKey22220();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1, SPACES)) {
			goTo(GotoLabel.validateKey22220);
		}
		 //ILIFE-7772 start
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
	/*	if(chdrpf==null){
			fatalError600();
			return;
		}*/
		if((chdrpf==null) && isEQ(subprogrec.key1, "Y")){
			sv.chdrselErr.set(errorsInner.e544);
		}
		if((chdrpf!=null) && isEQ(subprogrec.key1, "N")){
			sv.chdrselErr.set(errorsInner.f918);
		}
		/*chdrptsIO.setChdrcoy(wsspcomn.company);
		chdrptsIO.setChdrnum(sv.chdrsel);
		chdrptsIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrptsIO);
		}
		else {
			chdrptsIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrptsIO.getStatuz(), varcom.oK)
		&& isNE(chdrptsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrptsIO.getStatuz(), varcom.mrnf)
		&& isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set(errorsInner.e544);
		}
		if (isEQ(chdrptsIO.getStatuz(), varcom.oK)
		&& isEQ(subprogrec.key1, "N")) {
			sv.chdrselErr.set(errorsInner.f918);
		}*/
		 //ILIFE-7772 end 
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2290);
			}
		}
		/*    For transactions on policies,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branch.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrpf.getCntbranch())) {
			sv.chdrselErr.set(errorsInner.e455);
		}
	}

protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2, SPACES)
		|| isEQ(subprogrec.key2, "N")) {
			return ;
		}
		 //ILIFE-7772 start
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
		if(chdrpf==null){
			sv.chdrselErr.set(errorsInner.e544);
		//	fatalError600();
			return;
		}
		/*chdrptsIO.setChdrcoy(wsspcomn.company);
		chdrptsIO.setChdrnum(sv.chdrsel);
		chdrptsIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrptsIO);
		}
		else {
			chdrptsIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrptsIO.getStatuz(), varcom.oK)
		&& isNE(chdrptsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrptsIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.e544);
		}*/
		 //ILIFE-7772 end 
	}

protected void checkStatus2400()
	{
		readStatusTable2410();
	}

protected void readStatusTable2410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		lookForStat2500();
	}

protected void lookForStat2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case search2510: 
					search2510();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search2510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(errorsInner.e767);
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) //ILIFE-7772
			 {
				goTo(GotoLabel.search2510);
			}
		}
		/*EXIT*/
	}

protected void validateEffdate2700()
	{
		/*START*/
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			sv.effdate.set(wsaaToday);
		}
		else {
			if (isGT(sv.effdate, wsaaToday)) {
				sv.effdateErr.set(errorsInner.f073);
			}
			else {
				if (isLT(sv.effdate, chdrptsIO.getOccdate())) {
					sv.effdateErr.set(errorsInner.f616);
				}
			}
		}
		wsspcomn.currfrom.set(sv.effdate);
		/*EXIT*/
	}

	/**
	* <pre>
	*2600-EXIT.                                                  <014>
	*    EXIT.                                                   <014>
	* </pre>
	*/
protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void chkValidAction2960()
	{
		/*INIT*/
		if (isEQ(scrnparams.action, "A")
		|| isEQ(scrnparams.action, "B")) {
			wsaaBsurrOk = "N";
			chkValidBsurr2980();
			if (isEQ(wsaaBsurrOk, "N")) {
				sv.chdrselErr.set(errorsInner.hl22);
			}
		}
		/*EXIT*/
	}

protected void chkValidBsurr2980()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCovrsur2981();
				case loopCovrsur2985: 
					loopCovrsur2985();
					readT66402986();
				case exit2989: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCovrsur2981()
	{
		covrsurIO.setParams(SPACES);
		covrsurIO.setChdrcoy(chdrpf.getChdrcoy().toString()); //ILIFE-7772
		covrsurIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
		covrsurIO.setPlanSuffix(ZERO);
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setFunction(varcom.begn);
	}

protected void loopCovrsur2985()
	{
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		 //ILIFE-7772
		if (isNE(covrsurIO.getChdrcoy(), chdrpf.getChdrcoy().toString())
		|| isNE(covrsurIO.getChdrnum(), chdrpf.getChdrnum())/* IJTI-1523 */
		|| isEQ(covrsurIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2989);
		}
	}

protected void readT66402986()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t6640)
		|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/* No need to give an error if no item found on this table,*/
			/* Traditional Components only exist on this table.*/
			return ;
		}
		t6640rec.t6640Rec.set(itdmIO.getGenarea());
		if (isNE(t6640rec.zdivwdrmth, SPACES)) {
			wsaaBsurrOk = "Y";
			return ;
		}
		covrsurIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loopCovrsur2985);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case keeps3070: 
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		if (isEQ(scrnparams.action, "I")) {
			wsspcomn.flag.set("I");
			 //ILIFE-7772 start
			/*chdrenqpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
			if(chdrenqpf==null){
				fatalError600();
				return;
			}*/
			chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
			if(chdrpf==null){
				fatalError600();
				return;
			}
			/*chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrsel);
			chdrenqIO.setFunction(varcom.reads);
			chdrenqIO.setFormat(chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);*/
			 //ILIFE-7772 end
			goTo(GotoLabel.keeps3070);
		}
		else {
			wsspcomn.flag.set("N");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sv.chdrsel);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		/*    For existing proposals, add one to the transaction number.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			setPrecision(chdrpf.getTranno(), 0); //ILIFE-7772
			chdrpf.setTranno(add(chdrpf.getTranno(), 1).toInt()); //ILIFE-7772
		}
	}

protected void keeps3070()
	{
	 //ILIFE-7772 start
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrptsIO.setFunction("KEEPS");
		chdrptsIO.setFormat(chdrptsrec);
		SmartFileCode.execute(appVars, chdrptsIO);
		if (isNE(chdrptsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}*/
	  //ILIFE-7772 end
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData f918 = new FixedLengthStringData(4).init("F918");
	private FixedLengthStringData hl22 = new FixedLengthStringData(4).init("HL22");
}
}
