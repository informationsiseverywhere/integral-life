package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:44
 * Description:
 * Copybook name: HPUADOPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hpuadopkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hpuadopFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hpuadopKey = new FixedLengthStringData(64).isAPartOf(hpuadopFileKey, 0, REDEFINE);
  	public FixedLengthStringData hpuadopChdrcoy = new FixedLengthStringData(1).isAPartOf(hpuadopKey, 0);
  	public FixedLengthStringData hpuadopChdrnum = new FixedLengthStringData(8).isAPartOf(hpuadopKey, 1);
  	public FixedLengthStringData hpuadopLife = new FixedLengthStringData(2).isAPartOf(hpuadopKey, 9);
  	public FixedLengthStringData hpuadopCoverage = new FixedLengthStringData(2).isAPartOf(hpuadopKey, 11);
  	public FixedLengthStringData hpuadopRider = new FixedLengthStringData(2).isAPartOf(hpuadopKey, 13);
  	public PackedDecimalData hpuadopPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hpuadopKey, 15);
  	public PackedDecimalData hpuadopPuAddNbr = new PackedDecimalData(3, 0).isAPartOf(hpuadopKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(hpuadopKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hpuadopFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hpuadopFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}