package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdcxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:26
 * Class transformed from HDCXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdcxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 55;
	public FixedLengthStringData hdcxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hdcxpfRecord = hdcxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hdcxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hdcxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hdcxrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hdcxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hdcxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hdcxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hdcxrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hdcxrec);
	public PackedDecimalData balSinceLastCap = DD.hdvbalc.copy().isAPartOf(hdcxrec);
	public PackedDecimalData nextCapDate = DD.hcapndt.copy().isAPartOf(hdcxrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hdcxrec);
	public PackedDecimalData osInterest = DD.hintos.copy().isAPartOf(hdcxrec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(hdcxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HdcxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HdcxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HdcxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HdcxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdcxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HdcxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdcxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HDCXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"CRTABLE, " +
							"HDVBALC, " +
							"HCAPNDT, " +
							"TRANNO, " +
							"HINTOS, " +
							"RCESDTE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     crtable,
                                     balSinceLastCap,
                                     nextCapDate,
                                     tranno,
                                     osInterest,
                                     riskCessDate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		crtable.clear();
  		balSinceLastCap.clear();
  		nextCapDate.clear();
  		tranno.clear();
  		osInterest.clear();
  		riskCessDate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHdcxrec() {
  		return hdcxrec;
	}

	public FixedLengthStringData getHdcxpfRecord() {
  		return hdcxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHdcxrec(what);
	}

	public void setHdcxrec(Object what) {
  		this.hdcxrec.set(what);
	}

	public void setHdcxpfRecord(Object what) {
  		this.hdcxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hdcxrec.getLength());
		result.set(hdcxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}