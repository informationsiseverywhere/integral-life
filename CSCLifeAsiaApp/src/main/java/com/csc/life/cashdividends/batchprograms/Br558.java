/*
 * File: Br558.java
 * Date: 29 August 2009 22:20:20
 * Author: Quipoz Limited
 *
 * Class transformed from BR558.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.cashdividends.recordstructures.Pr557par;
import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.life.newbusiness.dataaccess.DivrTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This program converts dividend rates uploaded from PC file
*   to ITEMPF. The format of the PC file must be in the same
*   format as DIVRPF. To create the PC file in the same format
*   as DIVRPF, down load DIVRPF to PC in EXCEL format.
*
*   To add records to the PC file, first open the file in EXCEL,
*   cut and paste data from other EXCEL/LOTUS worksheets into the
*   DIVR format file. To convert the data to ITEMPF dividend rate,
*   upload the EXCEL file (as DATABASE file with replace option)
*   back to DIVRPF and then run the batch schedule(L2UPLDDIV!!).
*
*   This program allows uploading to TH527.                      r
*
*
*   The basic procedure division logic is for reading a Primary fi e
*   and the input details are used to update other files according to
*   certain conditions.
*
*   Initialise
*     - Set up Primary file DIVR key.
*
*    Read
*     - read first primary file record.
*
*    Perform    Until End of File
*
*      Edit
*       - No editing required.
*
*      Update
*       - Check if record already exists on ITEMPF.
*       - If record exists and have the same effective date, update
*         the ITDM record with the details from DIVR.
*       - If record exists but with different effective date, update
*         the existing ITDM record with a end date(effective date -
*         1 day) and then write a new ITDM record with the details
*         from DIVR.
*       - If record does not exist, write a new ITDM and DESC record
*         with details from DIVR.
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
*****************************************************************
* </pre>
*/
public class Br558 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR558");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaScheduleNumber = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaItdmItmto = new ZonedDecimalData(8, 0).init(99999999).setUnsigned();
	private FixedLengthStringData wsaaItdmFunction = new FixedLengthStringData(5).init(SPACES);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String th527 = "TH527";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private DivrTableDAM divrIO = new DivrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Th527rec th527rec = new Th527rec();
	private Pr557par pr557par = new Pr557par();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setup3011, 
		callItdm3013
	}

	public Br558() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		/* Open required files.*/
		pr557par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		/* Move fields to primary file key.*/
		divrIO.setParams(SPACES);
		divrIO.setFunction(varcom.begn);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		/*  Call the I/O module or do a Standard COBOL read on*/
		/*     the primary file.*/
		SmartFileCode.execute(appVars, divrIO);
		if (isNE(divrIO.getStatuz(), varcom.oK)
		&& isNE(divrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(divrIO.getParams());
			fatalError600();
		}
		if (isEQ(divrIO.getStatuz(), varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
				case setup3011: 
					setup3011();
				case callItdm3013: 
					callItdm3013();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		/* Update database records.*/
		itdmIO.setParams(SPACES);
		/* See if the record already exists.*/
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(pr557par.itemtabl);
		itdmIO.setItemitem(divrIO.getItemitem());
		itdmIO.setItmfrm(pr557par.efdate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/* Enf of file ie. record does not exist.*/
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaItdmFunction.set(varcom.writr);
			wsaaDesc.set("Y");
			wsaaItdmItmto.set(varcom.vrcmMaxDate);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.setup3011);
		}
		/* Key break ie. record does not exist.*/
		if (isNE(bsprIO.getCompany(), itdmIO.getItemcoy())
		|| isNE(pr557par.itemtabl, itdmIO.getItemtabl())
		|| isNE(divrIO.getItemitem(), itdmIO.getItemitem())) {
			itdmIO.setFunction(varcom.nextp);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isNE(bsprIO.getCompany(), itdmIO.getItemcoy())
			|| isNE(pr557par.itemtabl, itdmIO.getItemtabl())
			|| isNE(divrIO.getItemitem(), itdmIO.getItemitem())
			|| isEQ(divrIO.getStatuz(), varcom.endp)) {
				wsaaDesc.set("Y");
				wsaaItdmItmto.set(varcom.vrcmMaxDate);
			}
			else {
				datcon2rec.datcon2Rec.set(SPACES);
				datcon2rec.intDate1.set(itdmIO.getItmfrm());
				datcon2rec.freqFactor.set(-1);
				datcon2rec.frequency.set("DY");
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(itdmIO.getParams());
					fatalError600();
				}
				wsaaItdmItmto.set(datcon2rec.intDate2);
				wsaaDesc.set("N");
			}
			wsaaItdmFunction.set(varcom.writr);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.setup3011);
		}
		/* Record exists. Check if the effective date is the same.*/
		/* If the same, rewrite the record with the new details.*/
		/* If not the same, rewrite the existing record with a end date*/
		/* and write a new record with different effective date.*/
		if (isEQ(itdmIO.getItmfrm(), pr557par.efdate)) {
			wsaaDesc.set("N");
			wsaaItdmFunction.set(varcom.rewrt);
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			wsaaItdmItmto.set(itdmIO.getItmto());
			itdmIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(pr557par.efdate);
			datcon2rec.freqFactor.set(-1);
			datcon2rec.frequency.set("DY");
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			itdmIO.setItmto(datcon2rec.intDate2);
			itdmIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			wsaaItdmFunction.set(varcom.writr);
			wsaaDesc.set("N");
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void setup3011()
	{
		/* If record exists(same effective date), need to read and hold*/
		/* the record for rewrite.*/
		if (isEQ(wsaaItdmFunction, varcom.rewrt)) {
			itdmIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(pr557par.itemtabl, th527)) {
			setupTh5274500();
		}
		if (isEQ(wsaaItdmFunction, varcom.rewrt)) {
			itdmIO.setFunction(varcom.rewrt);
			goTo(GotoLabel.callItdm3013);
		}
		/* Write a new ITDM and DESC record.*/
		wsaaScheduleNumber.set(bsscIO.getScheduleNumber());
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(pr557par.itemtabl);
		itdmIO.setItemitem(divrIO.getItemitem());
		itdmIO.setItmfrm(pr557par.efdate);
		itdmIO.setItmto(wsaaItdmItmto);
		itdmIO.setTranid(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(bsscIO.getScheduleName(), SPACES);
		stringVariable1.addExpression(wsaaScheduleNumber, SPACES);
		stringVariable1.setStringInto(itdmIO.getTranid());
		itdmIO.setValidflag("1");
		if (isEQ(pr557par.itemtabl, th527)) {
			itdmIO.setTableprog("PH527");
		}
		itdmIO.setFunction(varcom.writr);
		/* No need to write a DESC if an ITDM record already exists.*/
		if (isEQ(wsaaDesc, "N")) {
			goTo(GotoLabel.callItdm3013);
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(pr557par.itemtabl);
		descIO.setDescitem(divrIO.getItemitem());
		descIO.setTranid(itdmIO.getTranid());
		descIO.setLongdesc(divrIO.getItemitem());
		descIO.setShortdesc(divrIO.getItemitem());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.writr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.dupr)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void callItdm3013()
	{
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/*NEAR-EXIT*/
		divrIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void setupTh5274500()
	{
		/*PARA*/
		th527rec.th527Rec.set(SPACES);
		th527rec.insprms.set(divrIO.getInsprms());
		/*    MOVE DIVR-INSTPR            TO TH527-INSTPR.                 */
		th527rec.instprs.set(divrIO.getInstprs());
		//Ticket ILIFE-142 begins
		/*for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			th527rec.instpr[ix.toInt()].set(ZERO);
		}*/
		//Ticket ILIFE-142 ends
		th527rec.premUnit.set(divrIO.getPremUnit());
		th527rec.unit.set(divrIO.getUnit());
		itdmIO.setGenarea(th527rec.th527Rec);
		/*EXIT*/
	}
}
