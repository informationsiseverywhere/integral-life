/*
 * File: Ph533.java
 * Date: 30 August 2009 1:05:51
 * Author: Quipoz Limited
 *
 * Class transformed from PH533.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdvdcwcrec;
import com.csc.life.cashdividends.screens.Sh533ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.dao.CcfkpfDAO;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.CreditCardUtility;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Cash Dividend Withdrawal
*
* This program is accessed via the Cash Dividend Withdrawal
* Submenu PH534.  It is responsible for initiating the cash
* withdrawal of accumulated dividend and interest.  The
* transaction screen lists all dividend and interest amounts
* attached to the contract components and allows input of
* withdrawal amount against each component.
*
*Initialise
*----------
*
* Read the  Contract  header  (function  RETRV)  and  read  the
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
* and the Bill-to-date.
*
* Note, as this functionality is mainly for Asian countries, plan
* processing will not be included in this scope.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*      - READR  the   life  details  using  LIFEPTS  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*      - READR the  joint-life  details using LIFEPTS (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format Name
*
*      Read the  client  details  record  and  use the relevant
*           copybook in order to format the required names.
*
* Build the Coverage/Rider review details
*
*      N.B. the currency is decided by the withdrawal subroutine
*           and passed back.
*      Default  the currency field at the bottom of the screen.
*
*      For each coverage/rider attached to the policy
*
*           - Check that the component has a valid status for
*                withdrawal by reading T5679. Invalid
*                status components will not call the withdrawal
*                calculation routine and will display a
*                blank line in the subfile. Valid status
*                components will further have their risk
*                cessation dates checked against the
*                effective date. Any components which have
*                risk cessation dates prior to the effective
*                date and which are still in force, will prevent
*                the transaction from continuing.
*
*           - call  the  withdrawal calculation  subroutine  as
*                defined on  T5687,  this  method  is  used  to
*                access  T6598,  which contains the subroutines
*                necessary   for   the  withdrawal calculation
*                processing.
*
* Linkage area passed to the withdrawal calculation subroutine:-
*
*        - company
*        - contract header number
*        - suffix
*        - life number
*        - joint-life number
*        - coverage
*        - rider
*        - crtable
*        - language
*        - actual value
*        - currency
*        - description
*        - type code
*        - status
*        - next capitalisation date
*        - dividend processing option
*        - cash dividend method
*
*      DOWHILE
*         withdrawal calculation subroutine status not = ENDP
*            - load the subfile record
*            - accumulate single currency if applicable
*            - call withdrawal calculation subroutine
*      ENDDO
*
* Policy Loans
*
*      this amount is the total value of loans held against
*           the contract. It is obtained by calling TOTLOAN
*           using this contract header number.
*           Once returned, this figure will be deducted from
*           the Withdrawal Amount. The figure remaining will be
*           displayed in the Net of Withdrawal Amount Debt field.
*           Before deducting the value of loans outstanding
*           from the Surrender Value, any previous Withdrawal
*           records belonging to this transaction and hence
*           unprocessed are read. These records will have
*           already reduced the loan amount outstanding
*           and must be taken into consideration.
*           These records may be in a different currency to
*           the CHDR currency and if so must first be converted.
*
* Net of Withdrawal Amount Debt
*
*      this is the value of loans outstanding after withdrawal
*           has taken place in this contract and taking into
*           account any previous withdrawal for the current
*           transaction.
*
* Total Current Amount
*
*      this amount  is  the  total of the current amounts  from
*           the  coverage/rider  subfile review. This amount is
*           the  sum  of the individual amounts returned by the
*           subroutine. (If all the amounts returned are in the
*           same currency).
*
* Total Withdrawal Amount
*
*      this amount is the total of the withdrawal amounts from
*           the coverage/rider subfile review (ie. the sum of the
*           individual amounts returned  by  the subroutine, if
*           all the amounts returned are  in the same currency)
*           together with the policy loans and manually entered
*           adjustments, initially zero.
*
* The above details are returned  to  the user and if he wishes
* to continue and  perform the Withdrawal routine he enters
* the required data  at  the bottom of the screen, otherwise he
* opts for KILL to exit from this transaction.
*
*Validation
*----------
*
* If  KILL   was entered, then skip the remainder of the
* validation and exit from the program.
*
*
* Withdrawal adjustment reason (optional)
*
*      Validated by the I/O module.
*
* Withdrawal adjustment reason description (optional)
*
*      Check whether an  adjustment  reason was entered or not.
*           If an  adjustment  reason  code  was entered and no
*           adjustment description was entered, then read T5500
*           and output the description found. This is displayed
*           if  CALC  is   pressed  (redisplay).  If  a  reason
*           description was entered, the system will default to
*           use this value  instead  of accessing T5500 for the
*           description.
*
* Currency (optional)
*
*      Validated by the I/O module.
*
*      A blank entry defaults to the contract currency.
*
* Other adjustment amount
*
*      A value may or may not be entered.
*
*      Do not allow withdrawal when no premiums have been paid.
*
*      If CALC is pressed, then  the screen is re-displayed and
*           the adjusted  amount  is  included  with  the final
*           total amount, i.e.  if an adjusted amount exists.
*
*      If the    currency    code     was     changed,     then
*           re-calculate/convert all of the amounts held in the
*           subfile.
*
*Updating
*--------
*
* If the KILL function key was pressed,  skip  the updating and
* exit from the program.
*
* DOWHILE there are records in the subfile
*
*      - write new HDIV records to denote the withdrawal by each
*        coverage/rider level.
*      - write new accounting entries via LIFACMV.  In case of
*        contract currency differs from the withdrawal currency
*        entered, posting to respective accounts is required.
*
* ENDDO
*
*Next Program
*------------
*
* For KILL or 'Enter', add 1 to the program pointer and exit.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Ph533 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH533");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);

	private FixedLengthStringData wsaaPlanSuff = new FixedLengthStringData(4).isAPartOf(wsaaPlanSuffix, 0, REDEFINE);
	private FixedLengthStringData wsaaPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaPlanSuff, 2);
	private PackedDecimalData wsaaDiffce = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDvdInt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaWithdraw = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaCntTotamt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaWdwTotamt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPolicyloan = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaSystemDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSysDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaSystemDate, 0).setUnsigned();
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0).init(0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private FixedLengthStringData wsaaErrorIndicators = new FixedLengthStringData(100);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaBnkkey = new FixedLengthStringData(10);
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3000 = "T3000";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5500 = "T5500";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t6598 = "T6598";
	private static final String t6640 = "T6640";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeptsTableDAM lifeptsIO = new LifeptsTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private BabrTableDAM babrIO = new BabrTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T6598rec t6598rec = new T6598rec();
	private T6640rec t6640rec = new T6640rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Hdvdcwcrec hdvdcwcrec = new Hdvdcwcrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh533ScreenVars sv = ScreenProgram.getScreenVars( Sh533ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	//Cash Dividend
	boolean isCashDividend = false;	
	private Descpf descpf = null;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaBankdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 0);
	private Pyoupf pyoupf = new Pyoupf();
	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO", PyoupfDAO.class);
	private Namadrsrec namadrsrec = new Namadrsrec();
	private final String wsaaLargeName = "LGNMS";
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Clbapf clbapf;
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private Mandpf mandpf;	
	private CcfkpfDAO ccfkpfDAO = getApplicationContext().getBean("ccfkpfDAO", CcfkpfDAO.class);

	protected Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	
 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		continue1030,
		exit1090,
		exit1690,
		read1950,
		exit2090,
		exit3090,
		postAcmv3250,
		postAcmv3350
	}

	public Ph533() {
		super();
		screenVars = sv;
		new ScreenModel("Sh533", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case continue1030:
					continue1030();
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaClntkey.set(SPACES);
		
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		/* Get Today date.                                                 */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/* retrieve the CHDR keeps in submenu*/

		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrptsIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrptsIO);
			if (isNE(chdrptsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrptsIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrptsIO.getChdrcoy().toString(), chdrptsIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		
		
 
		/* Find the PAYR Billing frequency.*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrpf.getChdrcoy());
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		/*    Dummy field initialisation for prototype version.*/
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(chdrpf.getOccdate());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		descIO.setDescitem(chdrpf.getCnttype());
		sv.effdate.set(wsspcomn.currfrom);
		descIO.setDesctabl(t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		}
		else {
			sv.descrip.fill("?");
		}
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		if (isNE(sv.ptdate, sv.btdate)) {
			sv.ptdateErr.set(errorsInner.g008);
			sv.btdateErr.set(errorsInner.g008);
		}
		if (isEQ(chdrpf.getPtdate(), chdrpf.getCcdate())) {
			sv.ptdateErr.set(errorsInner.t065);
		}
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/* Read the life details and the joint life details if they exist*/
		/* and format the names*/
		lifeptsIO.setDataArea(SPACES);
		lifeptsIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeptsIO.setChdrnum(chdrpf.getChdrnum());
		lifeptsIO.setLife("01");
		lifeptsIO.setJlife("00");
		lifeptsIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*lifeptsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeptsIO.setFitKeysSearch("CHDRNUM");*/

		SmartFileCode.execute(appVars, lifeptsIO);
		if (isNE(lifeptsIO.getStatuz(), varcom.oK)
		&& isNE(lifeptsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company, lifeptsIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), lifeptsIO.getChdrnum())
		|| isNE(lifeptsIO.getJlife(), "00")
		&& isNE(lifeptsIO.getJlife(), "  ")
		|| isEQ(lifeptsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		sv.lifcnum.set(lifeptsIO.getLifcnum());
		cltsIO.setClntnum(lifeptsIO.getLifcnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.linsnameErr.set(errorsInner.e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		
		//Cash dividend
		
		if (isNE(chdrpf.getCownnum(),SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspwindow.numsel.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrpf.getCownpfx());
			stringVariable1.addExpression(chdrpf.getCowncoy());
			stringVariable1.addExpression(chdrpf.getCownnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		isCashDividend = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUOTR007", appVars, "IT");	
		if(isCashDividend){	
			sv.configflag.set("Y");
			isCashDividendOn();
		}else{
			sv.configflag.set("N");
		}
				
		/*    look for joint life.*/
		lifeptsIO.setJlife("01");
		lifeptsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeptsIO);
		if ((isNE(lifeptsIO.getStatuz(), varcom.oK))
		&& (isNE(lifeptsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeptsIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifeptsIO.getLifcnum());
		cltsIO.setClntnum(lifeptsIO.getLifcnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.jlinsnameErr.set(errorsInner.e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
		
		
	}

	protected void isCashDividendOn(){
		
		if (isNE(chdrpf.getReqntype().trim(), "4") && isNE(chdrpf.getReqntype().trim(), "C") ) {//IJTI-1410
			sv.bankactkey.set(SPACES);
			sv.crdtcrd.set(SPACES);
			sv.bankkey.set(SPACES);
			
		}
		
		if(((isEQ( chdrpf.getReqntype(),"C") ||isEQ( chdrpf.getReqntype(),"4")) && isNE(chdrpf.getZmandref(),SPACES)))	{
			loadBnkdetailsMendref();
		}else if( isEQ( chdrpf.getReqntype(),"C") ||isEQ( chdrpf.getReqntype(),"4") && isEQ(chdrpf.getZmandref(),SPACES))	{
			loadBnkdetailsChdr();
		}			
	
		
		if (isEQ(chdrpf.getReqntype().trim(), "4")) {//IJTI-1410
			sv.crdtcrd.set(SPACES);	
		}
		
		if (isEQ(chdrpf.getReqntype().trim(), "C")) {//IJTI-1410
			sv.bankactkey.set(SPACES);	
		}
		
		sv.paymthbf.set(chdrpf.getReqntype());
		
		sv.payeeno.set(chdrpf.getCownnum());
		getPayorname();
		sv.payeenme.set(SPACES);
		cltsIO.setClntnum(sv.payeeno);
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.payeenoErr.set(errorsInner.e335);
			sv.payeenme.set(SPACES);
		}
		else {
			plainname();
			sv.payeenme.set(wsspcomn.longconfname);
		}
				
		sv.estMatValueOut[varcom.pr.toInt()].set("Y");
		sv.actvalueOut[varcom.pr.toInt()].set("Y");
		
	}
	
	protected void loadBnkdetailsMendref()	{
		mandpf = mandpfDAO.searchMandpfRecordData(chdrpf.getCowncoy().toString(),chdrpf.getCownnum(),chdrpf.getZmandref(),"MANDPF");//IJTI-1410
		if(mandpf!=null){
			String creditCardInformation = null;
			sv.bankkey.set(mandpf.getBankkey());
			if (isEQ(chdrpf.getReqntype().trim(), "4")) {//IJTI-1410
			sv.bankactkey.set(mandpf.getBankacckey());			
			}else if (isEQ(chdrpf.getReqntype().trim(), "C")) {//IJTI-1410
				
			sv.crdtcrd.set(mandpf.getBankacckey());
			 creditCardInformation=CreditCardUtility.getInstance().getCreditCard(sv.crdtcrd.toString().trim());
			 if(creditCardInformation!=null && !creditCardInformation.isEmpty()){					
				sv.crdtcrd.set(creditCardInformation);					
				}
			}
			
			
		}
		
		//Bank Account description and Bank Description
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		sv.bnkcdedsc.set(babrIO.getBankdesc());		
	
	}
	
protected void loadBnkdetailsChdr(){		
		
		sv.bankkey.set(chdrpf.getBankkey());
		if (isEQ(chdrpf.getReqntype().trim(), "4")) {//IJTI-1410
			sv.bankactkey.set(chdrpf.getBankacckey());
		}else if (isEQ(chdrpf.getReqntype().trim(), "C")) {//IJTI-1410
			sv.crdtcrd.set(chdrpf.getBankacckey());
		}		
		
		//Bank Account description and Bank Description
			babrIO.setBankkey(sv.bankkey);
			babrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, babrIO);
			if (isNE(babrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(babrIO.getParams());
				fatalError600();
			}
			sv.bnkcdedsc.set(babrIO.getBankdesc());
	}
	
	
	protected void getPayorname()	{
		/*A1010-NAMADRS*/		
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(sv.payeeno);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		/*A1090-EXIT*/
	}

protected void continue1030()
	{
		/*  Initialise screen variables.*/
		sv.policyloan.set(ZERO);
		wsaaPolicyloan.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.estimateTotalValue.set(ZERO);
		sv.clamant.set(ZERO);
		/*  Default withdrawal currency to contract currency.*/
		sv.currcd.set(chdrpf.getCntcurr());
		/*  Get the value of any Loans held against this component.*/
		getLoanDetails1900();
		getPolicyDebt1950();
		/* Get new TRANNO.*/
		wsaaNewTranno.set(chdrpf.getTranno());
		/* Dummy subfile initalisation for prototype - replace with SCLR*/
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SH533", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Process all coverage/riders*/
		wsaaDvdInt.set(ZERO);
		wholePlan1300();
		/*  Set total current amount to accumulated withdrawal*/
		sv.estimateTotalValue.set(wsaaDvdInt);
		/*  Store any errors that have been encountered.*/
		wsaaErrorIndicators.set(sv.errorIndicators);
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void wholePlan1300()
	{
		/*READ*/
		covrsurIO.setParams(SPACES);
		covrsurIO.setChdrcoy(chdrpf.getChdrcoy());
		covrsurIO.setChdrnum(chdrpf.getChdrnum());
		covrsurIO.setPlanSuffix(ZERO);
		covrsurIO.setFormat(formatsInner.covrsurrec);
		covrsurIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrpf.getChdrnum())) {
			covrsurIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrsurIO.getStatuz(), varcom.endp))) {
			processComponents1350();
		}

		/*EXIT*/
	}

protected void processComponents1350()
	{
		read1351();
		nextCovrsur1355();
	}

protected void read1351()
	{
		/* Read table T6640 to find the Cash Withdrawal method*/
		obtainWithdrawalMeth1400();
		/* If Cash Withdrawal Method is found, perform calculation*/
		if (isEQ(t6598rec.calcprog, SPACES)) {
			return ;
		}
		/* Set parameters in Withdrawal Calculation Linkage*/
		initialize(hdvdcwcrec.withdrawRec);
		hdvdcwcrec.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		hdvdcwcrec.chdrChdrnum.set(covrsurIO.getChdrnum());
		hdvdcwcrec.lifeLife.set(covrsurIO.getLife());
		hdvdcwcrec.lifeJlife.set(covrsurIO.getJlife());
		hdvdcwcrec.covrCoverage.set(covrsurIO.getCoverage());
		hdvdcwcrec.covrRider.set(covrsurIO.getRider());
		hdvdcwcrec.plnsfx.set(covrsurIO.getPlanSuffix());
		hdvdcwcrec.tranno.set(wsaaNewTranno);
		hdvdcwcrec.cnttype.set(chdrpf.getCnttype());
		hdvdcwcrec.crtable.set(covrsurIO.getCrtable());
		hdvdcwcrec.cntcurr.set(chdrpf.getCntcurr());
		hdvdcwcrec.effectiveDate.set(sv.effdate);
		hdvdcwcrec.crrcd.set(covrsurIO.getCrrcd());
		hdvdcwcrec.language.set(wsspcomn.language);
		hdvdcwcrec.actualVal.set(ZERO);
		hdvdcwcrec.nextCapDate.set(ZERO);
		hdvdcwcrec.endf.set(SPACES);
		hdvdcwcrec.statuz.set(SPACES);
		while ( !(isEQ(hdvdcwcrec.statuz, varcom.endp))) {
			callWithdrawCalcSbr1600();
		}

	}

protected void nextCovrsur1355()
	{
		/* Read the next coverage/rider record.*/
		covrsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrpf.getChdrnum())) {
			covrsurIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void obtainWithdrawalMeth1400()
	{
		read1410();
	}

protected void read1410()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6640)
		|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*    MOVE G588                   TO SCRN-ERROR-CODE*/
			/*    MOVE ITDM-PARAMS            TO SYSR-PARAMS*/
			/*    PERFORM  600-FATAL-ERROR*/
			t6640rec.t6640Rec.set(SPACES);
		}
		else {
			t6640rec.t6640Rec.set(itdmIO.getGenarea());
		}
		/* Get Withdraw Amount Calculation subroutine.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t6640rec.zdivwdrmth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
			
		}
	}

protected void callWithdrawCalcSbr1600()
	{
		try {
			read1610();
			addToSubfile1630();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void read1610()
	{
		/* Call Withdrawal Calculation Subroutine*/
		callProgram(t6598rec.calcprog, hdvdcwcrec.withdrawRec);
		if (isEQ(hdvdcwcrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(hdvdcwcrec.statuz);
			fatalError600();
		}
		if (isNE(hdvdcwcrec.statuz, varcom.oK)
		&& isNE(hdvdcwcrec.statuz, varcom.endp)) {
			syserrrec.statuz.set(hdvdcwcrec.statuz);
			fatalError600();
		}
		if (isEQ(hdvdcwcrec.actualVal, ZERO)) {
			goTo(GotoLabel.exit1690);
		}
		zrdecplrec.amountIn.set(hdvdcwcrec.actualVal);
		zrdecplrec.currency.set(hdvdcwcrec.cntcurr);
		a100CallRounding();
		hdvdcwcrec.actualVal.set(zrdecplrec.amountOut);
		sv.life.set(hdvdcwcrec.lifeLife);
		sv.coverage.set(hdvdcwcrec.covrCoverage);
		sv.hcover.set(hdvdcwcrec.covrCoverage);
		sv.rider.set(hdvdcwcrec.covrRider);
		sv.hrider.set(hdvdcwcrec.covrRider);
		sv.planSuffix.set(hdvdcwcrec.plnsfx);
		sv.hjlife.set(hdvdcwcrec.lifeJlife);
		if (isEQ(hdvdcwcrec.covrRider, "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(hdvdcwcrec.covrRider);
			sv.coverage.set(SPACES);
		}
		sv.hcrtable.set(hdvdcwcrec.crtable);
		sv.cnstcur.set(hdvdcwcrec.cntcurr);
		sv.hcnstcur.set(hdvdcwcrec.cntcurr);
		sv.fieldType.set(hdvdcwcrec.type);					
		sv.estMatValue.set(hdvdcwcrec.actualVal);
		sv.hemv.set(hdvdcwcrec.actualVal);		
		sv.shortds.set(hdvdcwcrec.description);
		sv.nextCapDate.set(hdvdcwcrec.nextCapDate);
		sv.zdivopt.set(hdvdcwcrec.zdivopt);
		sv.zcshdivmth.set(hdvdcwcrec.zcshdivmth);
		if(isCashDividend){
			sv.actvalue.set(sv.estMatValue);				
		}else{
			sv.actvalue.set(ZERO);			
		}		
		sv.zwdv.set(ZERO);
	}

protected void addToSubfile1630()
	{
		/* add Linkage fields to subfile*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SH533", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Accumulate calculated withdrawal into working storage*/
		wsaaDvdInt.add(hdvdcwcrec.actualVal);
	}

protected void getLoanDetails1900()
	{
		start1900();
	}

	/**
	* <pre>
	*  Get the details of all loans currently held against this
	*  Contract.
	*  Note also that TOTLOAN always returns details in the CHDR
	*  currency.
	* </pre>
	*/
protected void start1900()
	{
		/*  Read TOTLOAN to get value of loans for this contract.*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(sv.policyloan, 2).set(add(totloanrec.principal, totloanrec.interest));
		wsaaPolicyloan.set(sv.policyloan);
		wsaaCntcurr.set(chdrpf.getCntcurr());
	}

protected void getPolicyDebt1950()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1950();
				case read1950:
					read1950();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1950()
	{
		sv.tdbtamt.set(ZERO);
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrpf.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrpf.getChdrnum());
		tpoldbtIO.setTranno(ZERO);
		tpoldbtIO.setFormat(formatsInner.tpoldbtrec);
		tpoldbtIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		tpoldbtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tpoldbtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

	}

protected void read1950()
	{
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		&& isNE(tpoldbtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			fatalError600();
		}
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		|| isNE(tpoldbtIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(tpoldbtIO.getChdrnum(), chdrpf.getChdrnum())) {
			return ;
		}
		sv.tdbtamt.add(tpoldbtIO.getTdbtamt());
		tpoldbtIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read1950);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/* Do not allow withdrawal if amount not > 0                       */
		if (isLTE(wsaaDvdInt, 0)) {
			scrnparams.errorCode.set(errorsInner.hl18);
			wsspcomn.edterror.set("Y");
			//commented for testing
			scrnparams.function.set("PROT"); 
		}
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			validateSubfile2060();
			validateSelectionFields2070();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SH533IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SH533-DATA-AREA                         */
		/*                         SH533-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		/*  Pull in any errors from the 1000 section.*/
	
		sv.errorIndicators.set(wsaaErrorIndicators);
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		wsaaScrnStatuz.set(scrnparams.statuz);
		/* Withdrawal Currency must be entered.*/
		if (isEQ(sv.currcd, SPACES)) {
			sv.currcdErr.set(errorsInner.e186);
		}
		/* If Other Adjustment Amount is zero, reason must be blank,*/
		/* also reason must be entered when other adjustment amount <> 0*/
		if (isNE(sv.reasoncd, SPACES)
		&& isEQ(sv.otheradjst, 0)) {
			sv.reasoncdErr.set(errorsInner.e374);
		}
		if (isEQ(sv.reasoncd, SPACES)
		&& isNE(sv.otheradjst, 0)) {
			sv.reasoncdErr.set(errorsInner.e186);
		}
		if (isNE(sv.reasoncd, SPACES)) {
			if (isEQ(sv.resndesc, SPACES)) {
				wsaaScrnStatuz.set("CALC");
				descIO.setDesctabl(t5500);
				descIO.setDescitem(sv.reasoncd);
				findDesc1100();
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.resndesc.set(descIO.getLongdesc());
				}
				else {
					sv.resndesc.fill("?");
				}
			}
		}
		
		
		if(isCashDividend){
			
			if (isNE(sv.bankactkey,SPACES)) {
				clbapf = new Clbapf();
				clbapf.setClntpfx("CN");
				clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
				clbapf.setClntnum(sv.payeeno.toString().trim());
				clbapf.setBankacckey(sv.bankactkey.toString().trim());
				List<? extends Clbapf> clbalist = clbapfDAO.findByBankAccKey(clbapf);
				
				if (clbalist != null && clbalist.size() > 0) {
					for (Clbapf clbaitem : clbalist) {
						wsaaBnkkey.set(clbaitem.getBankkey());	
						sv.bankkey.set(wsaaBnkkey);
					}
					bankBranchDesc();
				}else{
					sv.bankactkeyErr.set(errorsInner.f826);
					wsspcomn.edterror.set("Y");
				}
				
			}
			if (isNE(sv.crdtcrd,SPACES)) {
				
				clbapf = new Clbapf();
				clbapf.setClntpfx("CN");
				clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
				clbapf.setClntnum(sv.payeeno.toString().trim());
				clbapf.setBankacckey(ccfkpfDAO.retreiveReferenceNumber(sv.crdtcrd.toString().trim()));			
				List<? extends Clbapf> clbalist = clbapfDAO.findByBankAccKey(clbapf);
				
				if (clbalist != null && clbalist.size() > 0) {
					for (Clbapf clbaitem : clbalist) {
						wsaaBnkkey.set(clbaitem.getBankkey());//IJTI-1410
						sv.bankkey.set(wsaaBnkkey);
					}
					bankBranchDesc();
				}else{
					sv.crdtcrdErr.set(errorsInner.f826);
					wsspcomn.edterror.set("Y");
				}				
			}
			
			cltsIO.setClntnum(sv.payeeno);
			getClientDetails1200();
			/* Get the confirmation name.*/
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
			|| isNE(cltsIO.getValidflag(), 1)) {
				sv.payeenoErr.set(errorsInner.e335);
				sv.payeenme.set(SPACES);
			}
			else {
				plainname();
				sv.payeenme.set(wsspcomn.longconfname);
			}
			
			if (isNE(chdrpf.getCownnum(), SPACES)) {
				wsaaClntkey.set(wsspcomn.clntkey);
				wsspwindow.numsel.set(wsspcomn.clntkey);
				wsspcomn.clntkey.set(SPACES);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(chdrpf.getCownpfx());
				stringVariable1.addExpression(chdrpf.getCowncoy());
				stringVariable1.addExpression(sv.payeeno);
				stringVariable1.setStringInto(wsspcomn.clntkey);
			}
			
			cltsIO.setParams(SPACES);
			cltsIO.setClntnum(sv.payeeno);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setFunction(varcom.readr);
		
			SmartFileCode.execute(appVars, cltsIO);
			if ((isNE(cltsIO.getStatuz(),varcom.oK))
			&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			
			
			if (isNE(sv.paymthbf, "4") && isNE(sv.paymthbf, "C") ) {
				
				sv.bankactkey.set(SPACES);
				sv.crdtcrd.set(SPACES);
				sv.bankkey.set(SPACES);
				sv.bnkcdedsc.set(SPACES);
			}
			
			if(isEQ(sv.bankactkey,SPACES)  && isEQ(sv.paymthbf, "4")){
				sv.bankkey.set(SPACES);
				sv.bnkcdedsc.set(SPACES);
			}else if(isEQ(sv.crdtcrd,SPACES) && isEQ(sv.paymthbf, "C")){
				sv.bankkey.set(SPACES);
				sv.bnkcdedsc.set(SPACES);
			}
			
		
			if (isEQ(sv.paymthbf, SPACES)) {
				sv.paymthbfErr.set(errorsInner.e186);	
			}
		if (isEQ(sv.paymthbf, "4")) {
			sv.crdtcrd.set(SPACES);
			if (isEQ(sv.bankactkey, SPACES)) {
				sv.bankactkeyErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
			}
			
		}
		
		if (isEQ(sv.paymthbf, "C")) {
			sv.bankactkey.set(SPACES);	
			if (isEQ(sv.crdtcrd, SPACES)) {
				sv.crdtcrdErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
			}
		}
		}
				
	}

protected void validateSubfile2060()
	{
		/*  Validate subfile*/
		wsaaDvdInt.set(ZERO);
		wsaaWithdraw.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("SH533", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile2200();
		}

		/*  Set totals to screen and determine for action allowed*/
		sv.estimateTotalValue.set(wsaaDvdInt);
		sv.clamant.set(wsaaWithdraw);
		/*  Subtract rather than add policy loan amount.*/
		if (isNE(sv.otheradjst, ZERO)) {
			zrdecplrec.amountIn.set(sv.otheradjst);
			zrdecplrec.currency.set(sv.currcd);
			a100CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.otheradjst)) {
				sv.otheradjstErr.set(errorsInner.rfik);
			}
		}
		sv.clamant.add(sv.otheradjst);
		/*  PERFORM 2400-CONVERT-POLICYLOAN.                             */
		if (isNE(sv.currcd, wsaaCntcurr)) {
			if (isEQ(sv.currcd, chdrpf.getCntcurr())) {
				sv.policyloan.set(wsaaPolicyloan);
			}
			else {
				convertPolicyloan2400();
			}
			wsaaCntcurr.set(sv.currcd);
		}
		/*  Check values*/
		if (isLTE(wsaaDvdInt, 0)) {
			sv.estimtotalErr.set(errorsInner.hl18);
		}
		if (isLT(sv.clamant, 0)) {
			sv.clamantErr.set(errorsInner.hl19);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSelectionFields2070()
	{
		if (isEQ(wsaaScrnStatuz, "CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}


protected void bankBranchDesc()
{
	babrIO.setDataKey(SPACES);
	babrIO.setBankkey(wsaaBnkkey.trim());
	babrIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, babrIO);
	if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
		sv.bankkeyErr.set(errorsInner.f906);
	}
	if (isNE(babrIO.getStatuz(),varcom.oK)
	&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(babrIO.getParams());
		fatalError600();
	}
	wsaaBankkey.set(babrIO.getBankdesc());
	sv.bnkcdedsc.set(wsaaBankdesc);
}

protected void readSubfile2200()
	{
		go2250();
		updateErrorIndicators2270();
		readNextRecord2280();
	}

protected void go2250()
	{
		/* Convert the original current amount into withdrawal curreny, if*/
		/* original contract currency is different from withdrawal ccy.*/
		if (isEQ(sv.currcd, wsaaCntcurr)) {
			sv.zwdv.set(sv.actvalue);
		}
		if (isNE(sv.currcd, sv.hcnstcur)
		&& isNE(sv.currcd, SPACES)
		&& isNE(sv.hemv, ZERO)) {
			conlinkrec.amountIn.set(sv.hemv);
			conlinkrec.function.set("SURR");
			callXcvrt2300();
			sv.estMatValue.set(conlinkrec.amountOut);
			conlinkrec.amountIn.set(sv.zwdv);
			conlinkrec.function.set("SURR");
			callXcvrt2300();
			sv.actvalue.set(conlinkrec.amountOut);
			sv.cnstcur.set(sv.currcd);
		}
		if (isEQ(sv.currcd, sv.hcnstcur)
		&& isNE(sv.currcd, wsaaCntcurr)) {			
			sv.estMatValue.set(sv.hemv);
			if(isCashDividend){
				sv.actvalue.set(sv.estMatValue);				
			}else{
				sv.actvalue.set(sv.zwdv);
				
			}
			sv.cnstcur.set(sv.currcd);
		}
		
		
		
		/* Flag error for Current Amount < Withdrawal amount*/
		if (isGT(sv.actvalue, sv.estMatValue)
		&& isGT(sv.actvalue, 0)) {
			sv.actvalueErr.set(errorsInner.hl20);
		}
		if (isNE(sv.actvalue, ZERO)) {
			zrdecplrec.amountIn.set(sv.actvalue);
			zrdecplrec.currency.set(sv.hcnstcur);
			a100CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.actvalue)) {
				sv.actvalueErr.set(errorsInner.rfik);
			}
		}
	}

protected void updateErrorIndicators2270()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SH533", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaDvdInt.add(sv.estMatValue);
		wsaaWithdraw.add(sv.actvalue);
	}

protected void readNextRecord2280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SH533", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callXcvrt2300()
	{
		g02350();
	}

protected void g02350()
	{
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, "****")) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.currcd);
		a100CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void convertPolicyloan2400()
	{
		start2400();
	}

protected void start2400()
	{
		/* Read table T3000 to insure that the currency code entered*/
		/* has conversion rates. If we do not do this, and there are*/
		/* no rates XCVRT will fall over with a System Error.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(t3000);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.currcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.currcdErr.set(errorsInner.h962);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  Convert the Policy Loan into the requested Currency.*/
		/*  Note, the LOAN value as returned from TOTLOAN will always*/
		/*  be in the CHDR currency. Therefore always use that value*/
		/*  and that currency to convert from.*/
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(chdrpf.getCntcurr());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		/*  MOVE SH533-POLICYLOAN       TO CLNK-AMOUNT-IN.               */
		conlinkrec.amountIn.set(wsaaPolicyloan);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.currcd);
		a100CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		sv.policyloan.set(conlinkrec.amountOut);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			if(isCashDividend){
				writePyou();
			}					
			updateDatabase3010();
			prepareForUpdate3060();
			readSubfile3070();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void writePyou()
{	
	pyoupf.setChdrpfx("CH");
	pyoupf.setChdrcoy(wsspcomn.company.toString());
	pyoupf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
	pyoupf.setTranno(chdrpf.getTranno());
	pyoupf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
   	pyoupf.setPayrnum(sv.payeeno.toString().trim());
	pyoupf.setReqntype(sv.paymthbf.toString());
   
	
	if(isNE(sv.bankactkey,SPACE)){
		pyoupf.setBankacckey(sv.bankactkey.toString());
	}else if(isNE(sv.crdtcrd,SPACE)){
		pyoupf.setBankacckey(sv.crdtcrd.toString());
    }else if(isEQ(sv.bankactkey,SPACE)){
    	pyoupf.setBankacckey(sv.crdtcrd.toString());
    }else if(isEQ(sv.crdtcrd,SPACE)){
        pyoupf.setBankacckey(sv.bankactkey.toString());
     }
	
	pyoupf.setBankkey(sv.bankkey.toString());
	pyoupf.setBankdesc(sv.bnkcdedsc.toString());
	pyoupf.setPaymentflag(SPACE);  
	pyoupf.setReqnno(SPACE);
	
	try{
		pyoupfDAO.insertPyoupfRecord(pyoupf);
	}catch(Exception e){		
		e.printStackTrace();
		fatalError600();
	}
	
	
}

/*protected void executeChdrptsIO3020() {
	 SmartFileCode.execute(appVars, ChdrptsIO);
		if (isNE(ChdrptsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ChdrptsIO.getParams());
			fatalError600();
		}
}
*/
protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void prepareForUpdate3060()
	{
		/*  Read T5645 for account posting masks*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read table T5688 to deteremine*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrpf.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.h979);
			scrnparams.errorCode.set(errorsInner.h979);
			fatalError600();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		/*    LIFA- Fields which require initialisation only once.*/
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.rldgcoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.genlcoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.rdocnum.set(sv.chdrnum);
		lifacmvrec.tranref.set(sv.chdrnum);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(sv.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		wsaaJrnseq.set(ZERO);
		lifacmvrec.effdate.set(sv.effdate);
		lifacmvrec.tranno.set(wsaaNewTranno);
		lifacmvrec.trandesc.set(sv.descrip);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
	}

protected void readSubfile3070()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("SH533", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCntTotamt.set(ZERO);
		wsaaWdwTotamt.set(ZERO);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile3100();
		}

		/* Post credit to payment suspense*/
		compute(lifacmvrec.origamt, 2).set(add(sv.otheradjst, wsaaWdwTotamt));
		lifacmvrec.origcurr.set(sv.cnstcur);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.rldgacct.set(hdivIO.getChdrnum());
		lifacmvrec.substituteCode[6].set(SPACES);
		if (isNE(lifacmvrec.origamt, 0)) {
			postAcmvRecord3500();
		}
		/* Post adjustment*/
		lifacmvrec.origamt.set(sv.otheradjst);
		lifacmvrec.origcurr.set(sv.cnstcur);
		lifacmvrec.sacscode.set(t5645rec.sacscode09);
		lifacmvrec.sacstyp.set(t5645rec.sacstype09);
		lifacmvrec.glcode.set(t5645rec.glmap09);
		lifacmvrec.glsign.set(t5645rec.sign09);
		lifacmvrec.contot.set(t5645rec.cnttot09);
		lifacmvrec.rldgacct.set(hdivIO.getChdrnum());
		lifacmvrec.substituteCode[6].set(SPACES);
		if (isNE(lifacmvrec.origamt, 0)) {
			postAcmvRecord3500();
		}
		/* Post equivalence to currency exchange.*/
		if (isNE(sv.currcd, chdrpf.getCntcurr())) {
			lifacmvrec.origcurr.set(chdrpf.getCntcurr());
			lifacmvrec.origamt.set(wsaaCntTotamt);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.contot.set(t5645rec.cnttot07);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(chdrpf.getChdrnum());
			if (isNE(lifacmvrec.origamt, 0)) {
				postAcmvRecord3500();
			}
			/* post the total surrender (before deduction) value to currency*/
			/* exchange account (debit)*/
			lifacmvrec.origcurr.set(sv.currcd);
			lifacmvrec.origamt.set(wsaaWdwTotamt);
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(chdrpf.getChdrnum());
			if (isNE(lifacmvrec.origamt, 0)) {
				postAcmvRecord3500();
			}
		}
		/*    Update existing CHDR as history and write a new one.*/
		chdrmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		chdrmjaIO.setChdrnum(chdrpf.getChdrnum());
		chdrmjaIO.setFunction(varcom.readh);
		procChdrmja3600();
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(sv.effdate);
		chdrmjaIO.setFunction(varcom.rewrt);
		procChdrmja3600();
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrfrom(sv.effdate);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setTranno(wsaaNewTranno);
		chdrmjaIO.setFunction(varcom.writr);
		procChdrmja3600();
		/*    Write a new PTRN.*/
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrcoy(chdrpf.getChdrcoy());
		ptrnIO.setChdrnum(chdrpf.getChdrnum());
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setValidflag("1");
		ptrnIO.setPtrneff(sv.effdate);
		ptrnIO.setTermid(SPACES);
		/* MOVE SH533-EFFDATE      TO PTRN-DATESUB.             <LA5184>*/
		ptrnIO.setDatesub(wsaaToday);
		wsaaSystemDate.set(getCobolDate());
		ptrnIO.setTransactionDate(wsaaSysDate);
		/* MOVE VRCM-TIMEN         TO PTRN-TRANSACTION-TIME.            */
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(999999);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(sv.chdrnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void readSubfile3100()
	{
		writeHdiv3100();
		readNextRecord3180();
	}

protected void writeHdiv3100()
	{
		/* Read HDIS*/
		hdisIO.setDataKey(SPACES);
		hdisIO.setChdrcoy(chdrpf.getChdrcoy());
		hdisIO.setChdrnum(sv.chdrnum);
		hdisIO.setLife(sv.life);
		hdisIO.setCoverage(sv.hcover);
		hdisIO.setRider(sv.hrider);
		hdisIO.setPlanSuffix(sv.planSuffix);
		hdisIO.setFormat(formatsInner.hdisrec);
		hdisIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)
		|| isNE(hdisIO.getValidflag(), "1")) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
		if (isNE(sv.cnstcur, sv.hcnstcur)
		&& isNE(sv.actvalue, ZERO)) {
			withdrawalCurrency3200();
		}
		else {
			if (isEQ(sv.cnstcur, sv.hcnstcur)
			&& isNE(sv.actvalue, ZERO)) {
				contractCurrency3300();
			}
		}
	}

protected void readNextRecord3180()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SH533", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void withdrawalCurrency3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					convert3200();
				case postAcmv3250:
					postAcmv3250();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void convert3200()
	{
		/* If Withdrawal Currency <> Contract Currency,*/
		/* Convert Withdrawal amount into Contract Currency*/
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.cnstcur);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.hcnstcur);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(sv.actvalue);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.hcnstcur);
		a100CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		/*  If Converted Amount <> original current amount, allow discre-*/
		/*  pancy of 1 to assume same amount*/
		compute(wsaaDiffce, 2).set(sub(conlinkrec.amountOut, sv.hemv));
		if (isGTE(wsaaDiffce, -1)
		&& isLTE(wsaaDiffce, 1)) {
			conlinkrec.amountOut.set(sv.hemv);
		}
		/* Write a HDIV to denote the withdrawal*/
		hdivIO.setChdrcoy(chdrpf.getChdrcoy());
		hdivIO.setChdrnum(sv.chdrnum);
		hdivIO.setLife(sv.life);
		hdivIO.setJlife(sv.hjlife);
		hdivIO.setCoverage(sv.hcover);
		hdivIO.setRider(sv.hrider);
		hdivIO.setPlanSuffix(ZERO);
		hdivIO.setTranno(wsaaNewTranno);
		hdivIO.setEffdate(sv.effdate);
		hdivIO.setDivdAllocDate(sv.effdate);
		hdivIO.setDivdIntCapDate(sv.nextCapDate);
		hdivIO.setCntcurr(sv.hcnstcur);
		setPrecision(hdivIO.getDivdAmount(), 2);
		hdivIO.setDivdAmount(mult(conlinkrec.amountOut, -1));
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
		wsaaBatckey.set(wsspcomn.batchkey);
		hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		if (isEQ(sv.fieldType, "D")) {
			hdivIO.setDivdType("C");
		}
		else {
			if (isEQ(sv.fieldType, "I")) {
				hdivIO.setDivdType("I");
			}
		}
		hdivIO.setZdivopt(sv.zdivopt);
		hdivIO.setZcshdivmth(sv.zcshdivmth);
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			syserrrec.statuz.set(hdivIO.getStatuz());
			fatalError600();
		}
		if (isNE(hdivIO.getDivdType(), "I")) {
			goTo(GotoLabel.postAcmv3250);
		}
		/*  Update HDIS by accumulating the interest withdrawn to O/S*/
		/*  interest to be capitalised*/
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(wsaaNewTranno);
		setPrecision(hdisIO.getOsInterest(), 2);
		hdisIO.setOsInterest(add(hdisIO.getOsInterest(), hdivIO.getDivdAmount()));
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
	}

protected void postAcmv3250()
	{
		/*  Post ACMV via LIFACMV*/
		lifacmvrec.origamt.set(conlinkrec.amountOut);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		/*  Post to dividend suspense for type 'D'*/
		/*  and     interest suspense for type 'I'*/
		if (isEQ(sv.fieldType, "D")) {
			wsaaCntTotamt.add(conlinkrec.amountOut);
			wsaaWdwTotamt.add(sv.actvalue);
			lifacmvrec.rldgacct.set(hdivIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			if (isNE(hdisIO.getSacscode(), SPACES)
			&& isNE(hdisIO.getSacstyp(), SPACES)) {
				lifacmvrec.sacscode.set(hdisIO.getSacscode());
				lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
			}
		}
		else {
			if (isEQ(sv.fieldType, "I")) {
				wsaaCntTotamt.add(conlinkrec.amountOut);
				wsaaWdwTotamt.add(sv.actvalue);
				if (isEQ(t5688rec.comlvlacc, "Y")) {
					lifacmvrec.sacscode.set(t5645rec.sacscode06);
					lifacmvrec.sacstyp.set(t5645rec.sacstype06);
					lifacmvrec.glcode.set(t5645rec.glmap06);
					lifacmvrec.glsign.set(t5645rec.sign06);
					lifacmvrec.contot.set(t5645rec.cnttot06);
					wsaaRldgacct.set(SPACES);
					wsaaRldgChdrnum.set(hdivIO.getChdrnum());
					wsaaRldgLife.set(hdivIO.getLife());
					wsaaRldgCoverage.set(hdivIO.getCoverage());
					wsaaRldgRider.set(hdivIO.getRider());
					wsaaPlanSuffix.set(hdivIO.getPlanSuffix());
					wsaaRldgPlanSuffix.set(wsaaPlnsfx);
					lifacmvrec.rldgacct.set(wsaaRldgacct);
					lifacmvrec.substituteCode[6].set(sv.hcrtable);
				}
				else {
					lifacmvrec.sacscode.set(t5645rec.sacscode05);
					lifacmvrec.sacstyp.set(t5645rec.sacstype05);
					lifacmvrec.glcode.set(t5645rec.glmap05);
					lifacmvrec.glsign.set(t5645rec.sign05);
					lifacmvrec.contot.set(t5645rec.cnttot05);
					lifacmvrec.rldgacct.set(hdivIO.getChdrnum());
					lifacmvrec.substituteCode[6].set(SPACES);
				}
			}
		}
		if (isNE(lifacmvrec.origamt, 0)) {
			postAcmvRecord3500();
		}
	}

protected void contractCurrency3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					noConvert3300();
				case postAcmv3350:
					postAcmv3350();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void noConvert3300()
	{
		/* If Withdrawal Currency = Contract Currency, so this.*/
		/* Write a HDIV to denote the withdrawal*/
		hdivIO.setChdrcoy(chdrpf.getChdrcoy());
		hdivIO.setChdrnum(sv.chdrnum);
		hdivIO.setLife(sv.life);
		hdivIO.setJlife(sv.hjlife);
		hdivIO.setCoverage(sv.hcover);
		hdivIO.setRider(sv.hrider);
		hdivIO.setPlanSuffix(ZERO);
		hdivIO.setTranno(wsaaNewTranno);
		hdivIO.setEffdate(sv.effdate);
		hdivIO.setDivdAllocDate(sv.effdate);
		hdivIO.setDivdIntCapDate(sv.nextCapDate);
		hdivIO.setCntcurr(sv.hcnstcur);
		setPrecision(hdivIO.getDivdAmount(), 2);
		hdivIO.setDivdAmount(mult(sv.actvalue, -1));
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
		wsaaBatckey.set(wsspcomn.batchkey);
		hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		if (isEQ(sv.fieldType, "D")) {
			hdivIO.setDivdType("C");
		}
		else {
			if (isEQ(sv.fieldType, "I")) {
				hdivIO.setDivdType("I");
			}
		}
		hdivIO.setZdivopt(sv.zdivopt);
		hdivIO.setZcshdivmth(sv.zcshdivmth);
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			syserrrec.statuz.set(hdivIO.getStatuz());
			fatalError600();
		}
		if (isNE(hdivIO.getDivdType(), "I")) {
			goTo(GotoLabel.postAcmv3350);
		}
		/*  Update HDIS by accumulating the interest withdrawn to O/S*/
		/*  interest to be capitalised*/
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(wsaaNewTranno);
		setPrecision(hdisIO.getOsInterest(), 2);
		hdisIO.setOsInterest(add(hdisIO.getOsInterest(), hdivIO.getDivdAmount()));
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
	}

protected void postAcmv3350()
	{
		/*  Post ACMV via LIFACMV*/
		lifacmvrec.origamt.set(sv.actvalue);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		/*  Post to dividend suspense for type 'D'*/
		/*  and     interest suspense for type 'I'*/
		if (isEQ(sv.fieldType, "D")) {
			wsaaCntTotamt.add(sv.actvalue);
			wsaaWdwTotamt.add(sv.actvalue);
			lifacmvrec.rldgacct.set(hdivIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			if (isNE(hdisIO.getSacscode(), SPACES)
			&& isNE(hdisIO.getSacstyp(), SPACES)) {
				lifacmvrec.sacscode.set(hdisIO.getSacscode());
				lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
			}
		}
		else {
			if (isEQ(sv.fieldType, "I")) {
				wsaaCntTotamt.add(sv.actvalue);
				wsaaWdwTotamt.add(sv.actvalue);
				if (isEQ(t5688rec.comlvlacc, "Y")) {
					lifacmvrec.sacscode.set(t5645rec.sacscode06);
					lifacmvrec.sacstyp.set(t5645rec.sacstype06);
					lifacmvrec.glcode.set(t5645rec.glmap06);
					lifacmvrec.glsign.set(t5645rec.sign06);
					lifacmvrec.contot.set(t5645rec.cnttot06);
					wsaaRldgacct.set(SPACES);
					wsaaRldgChdrnum.set(hdivIO.getChdrnum());
					wsaaRldgLife.set(hdivIO.getLife());
					wsaaRldgCoverage.set(hdivIO.getCoverage());
					wsaaRldgRider.set(hdivIO.getRider());
					wsaaPlanSuffix.set(hdivIO.getPlanSuffix());
					wsaaRldgPlanSuffix.set(wsaaPlnsfx);
					lifacmvrec.rldgacct.set(wsaaRldgacct);
					lifacmvrec.substituteCode[6].set(sv.hcrtable);
				}
				else {
					lifacmvrec.sacscode.set(t5645rec.sacscode05);
					lifacmvrec.sacstyp.set(t5645rec.sacstype05);
					lifacmvrec.glcode.set(t5645rec.glmap05);
					lifacmvrec.glsign.set(t5645rec.sign05);
					lifacmvrec.contot.set(t5645rec.cnttot05);
					lifacmvrec.rldgacct.set(hdivIO.getChdrnum());
					lifacmvrec.substituteCode[6].set(SPACES);
				}
			}
		}
		if (isNE(lifacmvrec.origamt, 0)) {
			postAcmvRecord3500();
		}
	}

protected void postAcmvRecord3500()
	{
		/*POST*/
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void procChdrmja3600()
	{
		/*CHDRMJA*/
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		wsspcomn.confirmationKey.set(sv.chdrnum);
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a100CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A190-EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e374 = new FixedLengthStringData(4).init("E374");
	private FixedLengthStringData g008 = new FixedLengthStringData(4).init("G008");
	private FixedLengthStringData h962 = new FixedLengthStringData(4).init("H962");
	private FixedLengthStringData h979 = new FixedLengthStringData(4).init("H979");
	private FixedLengthStringData hl18 = new FixedLengthStringData(4).init("HL18");
	private FixedLengthStringData hl19 = new FixedLengthStringData(4).init("HL19");
	private FixedLengthStringData hl20 = new FixedLengthStringData(4).init("HL20");
	private FixedLengthStringData t065 = new FixedLengthStringData(4).init("T065");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData f906 = new FixedLengthStringData(4).init("F906");
	private FixedLengthStringData e182 = new FixedLengthStringData(4).init("E182"); 
	private FixedLengthStringData f826 = new FixedLengthStringData(4).init("F826"); 
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335"); 
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData covrsurrec = new FixedLengthStringData(10).init("COVRSURREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData tpoldbtrec = new FixedLengthStringData(10).init("TPOLDBTREC");
}
}
