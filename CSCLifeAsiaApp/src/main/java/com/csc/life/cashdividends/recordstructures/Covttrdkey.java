package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:30
 * Description:
 * Copybook name: COVTTRDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covttrdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covttrdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covttrdKey = new FixedLengthStringData(64).isAPartOf(covttrdFileKey, 0, REDEFINE);
  	public FixedLengthStringData covttrdChdrcoy = new FixedLengthStringData(1).isAPartOf(covttrdKey, 0);
  	public FixedLengthStringData covttrdChdrnum = new FixedLengthStringData(8).isAPartOf(covttrdKey, 1);
  	public FixedLengthStringData covttrdLife = new FixedLengthStringData(2).isAPartOf(covttrdKey, 9);
  	public FixedLengthStringData covttrdCoverage = new FixedLengthStringData(2).isAPartOf(covttrdKey, 11);
  	public FixedLengthStringData covttrdRider = new FixedLengthStringData(2).isAPartOf(covttrdKey, 13);
  	public PackedDecimalData covttrdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covttrdKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covttrdKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covttrdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covttrdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}