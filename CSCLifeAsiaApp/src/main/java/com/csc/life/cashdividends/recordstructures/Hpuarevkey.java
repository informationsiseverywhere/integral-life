package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:45
 * Description:
 * Copybook name: HPUAREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hpuarevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hpuarevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hpuarevKey = new FixedLengthStringData(64).isAPartOf(hpuarevFileKey, 0, REDEFINE);
  	public FixedLengthStringData hpuarevChdrcoy = new FixedLengthStringData(1).isAPartOf(hpuarevKey, 0);
  	public FixedLengthStringData hpuarevChdrnum = new FixedLengthStringData(8).isAPartOf(hpuarevKey, 1);
  	public FixedLengthStringData hpuarevLife = new FixedLengthStringData(2).isAPartOf(hpuarevKey, 9);
  	public FixedLengthStringData hpuarevCoverage = new FixedLengthStringData(2).isAPartOf(hpuarevKey, 11);
  	public FixedLengthStringData hpuarevRider = new FixedLengthStringData(2).isAPartOf(hpuarevKey, 13);
  	public PackedDecimalData hpuarevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hpuarevKey, 15);
  	public PackedDecimalData hpuarevTranno = new PackedDecimalData(5, 0).isAPartOf(hpuarevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(hpuarevKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hpuarevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hpuarevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}