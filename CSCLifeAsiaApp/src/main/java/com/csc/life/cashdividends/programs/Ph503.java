/*
 * File: Ph503.java
 * Date: 30 August 2009 1:03:34
 * Author: Quipoz Limited
 * 
 * Class transformed from PH503.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.general.dataaccess.ClbaddbTableDAM;
import com.csc.life.cashdividends.screens.Sh503ScreenVars;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* PAYEE BANK ACCOUNT DETAILS
*
* This screen is called from PH504 and it is to capture the direct
* credit details for a coverage whose Cash Dividend Option (if
* applicable) is cash withdrawal.  The processing of this option
* involves raising a payment requisition automatically and this
* would required the payment details.  The details captured here
* will first be held on COVTPF and these will be transferred to a
* permanent file upon issue.
*
* 1000-initialize section.
* - Retrieve COVTLNB.
* - Load screen fields:
*   . PAYRNUM, CURRCODE, NUMSEL, BANKKEY, BANKACCKEY from COVTLNB.
*   . FACTHOUS, bank account description from CLBL.
*   . Bank/Branch description from BABR.
*
* 2000-screen-edit section.
* - Set screen function to PROT if inquiry mode.
* - Call screen io.
* - Skip validation if inquiry or kill
* - Skip validation if no details had been entered.
* - Validate:
*   . Both bank/branch and bank account must be entered if either
*     field is entered.
*   . Bank/branch must exist in BABR.
*   . Bank account must exist in CLBL.
*   . Bank account currency must be the same as payment currency.
*   . Bank account holder must be payee.
* - If scrn-statuz = CALC, re-display bank/branch and bank account
*   descriptions if bank account details exists.
*
* 3000-update section.
* - Skip update section if enquiry or kill.
* - Set up COVTLNB fields from screen.
* - Keep COVTLNB.
*
* 4000-where-next section.
* - add 1 to program pointer.
*
*****************************************************************
* </pre>
*/
public class Ph503 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH503");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String e182 = "E182";
	private String f906 = "F906";
	private String g599 = "G599";
	private String g900 = "G900";
	private String z043 = "Z043";
	private String covtlnbrec = "COVTLNBREC";
	private String clbaddbrec = "CLBADDBREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Client Bank Account Details*/
	private ClbaddbTableDAM clbaddbIO = new ClbaddbTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Sh503ScreenVars sv = ScreenProgram.getScreenVars( Sh503ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1190, 
		preExit, 
		exit2090
	}

	public Ph503() {
		super();
		screenVars = sv;
		new ScreenModel("Sh503", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		retrvCovtlnb1020();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
	}

protected void retrvCovtlnb1020()
	{
		covtlnbIO.setFunction("RETRV");
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		covtlnbIO.setFunction("RLSE");
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		sv.payrnum.set(covtlnbIO.getPayclt());
		sv.numsel.set(covtlnbIO.getPayclt());
		sv.currcode.set(covtlnbIO.getPaycurr());
		sv.bankkey.set(covtlnbIO.getBankkey());
		sv.bankacckey.set(covtlnbIO.getBankacckey());
		if (isNE(sv.bankkey,SPACES)
		&& isNE(sv.bankacckey,SPACES)) {
			bankDetails1100();
		}
	}

protected void bankDetails1100()
	{
		try {
			readAccountDets1110();
		}
		catch (GOTOException e){
		}
	}

protected void readAccountDets1110()
	{
		clbaddbIO.setParams(SPACES);
		clbaddbIO.setClntcoy(wsspcomn.fsuco);
		clbaddbIO.setClntnum(sv.payrnum);
		clbaddbIO.setBankkey(sv.bankkey);
		clbaddbIO.setBankacckey(sv.bankacckey);
		clbaddbIO.setFormat(clbaddbrec);
		clbaddbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clbaddbIO);
		if (isNE(clbaddbIO.getStatuz(),varcom.oK)
		&& isNE(clbaddbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clbaddbIO.getParams());
			fatalError600();
		}
		if (isEQ(clbaddbIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(g599);
			sv.bankacckeyErr.set(g599);
			goTo(GotoLabel.exit1190);
		}
		sv.bankaccdsc.set(clbaddbIO.getBankaccdsc());
		sv.facthous.set(clbaddbIO.getFacthous());
		babrIO.setDataKey(SPACES);
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(f906);
		}
		sv.bankdesc.set(subString(babrIO.getBankdesc(), 1, 30));
		sv.branchdesc.set(subString(babrIO.getBankdesc(), 31, 30));
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			inquiryProt2000();
			validate2020();
			redisplayOnCalc2040();
		}
		catch (GOTOException e){
		}
	}

protected void inquiryProt2000()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isEQ(sv.bankkey,SPACES)
		&& isEQ(sv.bankacckey,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.bankkey,SPACES)) {
			sv.bankkeyErr.set(g900);
		}
		if (isEQ(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(e182);
		}
		if (isNE(sv.bankkey,SPACES)
		&& isNE(sv.bankacckey,SPACES)) {
			bankDetails1100();
		}
		if (isNE(clbaddbIO.getCurrcode(),sv.currcode)) {
			sv.bankkeyErr.set(z043);
		}
	}

protected void redisplayOnCalc2040()
	{
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*KEEP-COVTLNB*/
		covtlnbIO.setFacthous(sv.facthous);
		covtlnbIO.setBankkey(sv.bankkey);
		covtlnbIO.setBankacckey(sv.bankacckey);
		covtlnbIO.setFunction("KEEPS");
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
