package com.csc.life.cashdividends.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SH533
 * @version 1.0 generated on 30/08/09 07:02
 * @author Quipoz
 */
public class Sh533ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(1076);
	public FixedLengthStringData dataFields = new FixedLengthStringData(552).isAPartOf(dataArea, 0);
	public ZonedDecimalData clamant = DD.clamant.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,20);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,28);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,45);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,83);
	public ZonedDecimalData tdbtamt = DD.tdbtamt.copyToZonedDecimal().isAPartOf(dataFields,133);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,150);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,158);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,166);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,177);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,207);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,237);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,292);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,300);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,347);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,355);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,402);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,412);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,420);
	//Cash Dividend Start
	public FixedLengthStringData payeeno = DD.payeesel.copy().isAPartOf(dataFields,430);// 10
	public FixedLengthStringData payeenme = DD.payeename.copy().isAPartOf(dataFields,440);//30
	public FixedLengthStringData paymthbf = DD.reqntype.copy().isAPartOf(dataFields,470);//1
	public FixedLengthStringData bankactkey = DD.bankacckey1.copy().isAPartOf(dataFields,471);//20
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,491);//10
	public FixedLengthStringData bnkcdedsc = DD.bnkcdedsc.copy().isAPartOf(dataFields,501);//30
	public FixedLengthStringData configflag = DD.configflag.copy().isAPartOf(dataFields,531);//10
	public FixedLengthStringData crdtcrd = DD.crdtcrd.copy().isAPartOf(dataFields,532); 
	//Cash Dividend End
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(128).isAPartOf(dataArea, 552);
	public FixedLengthStringData clamantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData tdbtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	//Cash Dividend Start
	public FixedLengthStringData payeenoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData payeenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData paymthbfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData bankactkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData bnkcdedscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData configflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData crdtcrdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	//Cash Dividend End
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(396).isAPartOf(dataArea, 680);
	public FixedLengthStringData[] clamantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] tdbtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	//Cash Dividend Start
	public FixedLengthStringData[] payeenoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] payeenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] paymthbfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] bankactkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] bnkcdedscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] configflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] crdtcrdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	//Cash Dividend End
	public FixedLengthStringData subfileArea = new FixedLengthStringData(443);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(121).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public ZonedDecimalData nextCapDate = DD.hcapndt.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(subfileFields,47);
	public FixedLengthStringData hcover = DD.hcover.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,52);
	public ZonedDecimalData hemv = DD.hemv.copyToZonedDecimal().isAPartOf(subfileFields,56);
	public FixedLengthStringData hjlife = DD.hjlife.copy().isAPartOf(subfileFields,73);
	public FixedLengthStringData hrider = DD.hrider.copy().isAPartOf(subfileFields,75);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,77);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(subfileFields,79);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,83);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,85);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(subfileFields,95);
	public FixedLengthStringData zcshdivmth = DD.zcshdivmth.copy().isAPartOf(subfileFields,96);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(subfileFields,100);
	public ZonedDecimalData zwdv = DD.zwdv.copyToZonedDecimal().isAPartOf(subfileFields,104);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(80).isAPartOf(subfileArea, 121);
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hcapndtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hcoverErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hemvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData hjlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData hriderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData typeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData zcshdivmthErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData zdivoptErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 68);
	public FixedLengthStringData zwdvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 72);
	public FixedLengthStringData estMatValueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 76);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(240).isAPartOf(subfileArea, 201);
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hcapndtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hcoverOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hemvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] hjlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] hriderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] typeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] zcshdivmthOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public FixedLengthStringData[] zdivoptOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 204);
	public FixedLengthStringData[] zwdvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 216);
	public FixedLengthStringData[] estMatValueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 228);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 441);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextCapDateDisp = new FixedLengthStringData(10);

	public LongData Sh533screensflWritten = new LongData(0);
	public LongData Sh533screenctlWritten = new LongData(0);
	public LongData Sh533screenWritten = new LongData(0);
	public LongData Sh533protectWritten = new LongData(0);
	public GeneralTable sh533screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sh533screensfl;
	}

	public Sh533ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actvalueOut,new String[] {"42","15", "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ptdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btdateOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcdOut,new String[] {"08","12","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otheradjstOut,new String[] {"07","12","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clamantOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"05","12","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(resndescOut,new String[] {"06","12","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(estMatValueOut,new String[] {"45","16", "-45","47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeenoOut,new String[] {"02","03", "-02","04", null, null, null, null, null, null, null, null});
		fieldIndMap.put(paymthbfOut,new String[] {"14","09", "-14","13", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankactkeyOut,new String[] {"19","21", "-19","20", null, null, null, null, null, null, null, null});
		fieldIndMap.put(crdtcrdOut,new String[] {"22","23", "-22","24", null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		
		
		screenSflFields = new BaseData[] {hemv, zwdv, hcrtable, hcnstcur, hcover, hrider, planSuffix, hjlife, nextCapDate, zdivopt, zcshdivmth, life, coverage, rider, fieldType, shortds, cnstcur, estMatValue, actvalue};
		screenSflOutFields = new BaseData[][] {hemvOut, zwdvOut, hcrtableOut, hcnstcurOut, hcoverOut, hriderOut, plnsfxOut, hjlifeOut, hcapndtOut, zdivoptOut, zcshdivmthOut, lifeOut, coverageOut, riderOut, typeOut, shortdsOut, cnstcurOut, emvOut, actvalueOut,estMatValueOut};
		screenSflErrFields = new BaseData[] {hemvErr, zwdvErr, hcrtableErr, hcnstcurErr, hcoverErr, hriderErr, plnsfxErr, hjlifeErr, hcapndtErr, zdivoptErr, zcshdivmthErr, lifeErr, coverageErr, riderErr, typeErr, shortdsErr, cnstcurErr, emvErr, actvalueErr,estMatValueErr};
		screenSflDateFields = new BaseData[] {nextCapDate};
		screenSflDateErrFields = new BaseData[] {hcapndtErr};
		screenSflDateDispFields = new BaseData[] {nextCapDateDisp};

		screenFields = new BaseData[] {descrip, chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, effdate, policyloan, currcd, otheradjst, estimateTotalValue, clamant, reasoncd, resndesc, tdbtamt,payeeno,payeenme,paymthbf,bankactkey,bankkey,bnkcdedsc,crdtcrd};
		screenOutFields = new BaseData[][] {descripOut, chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, effdateOut, policyloanOut, currcdOut, otheradjstOut, estimtotalOut, clamantOut, reasoncdOut, resndescOut, tdbtamtOut,payeenoOut,payeenmeOut,paymthbfOut,bankactkeyOut,bankkeyOut,bnkcdedscOut,crdtcrdOut};
		screenErrFields = new BaseData[] {descripErr, chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, effdateErr, policyloanErr, currcdErr, otheradjstErr, estimtotalErr, clamantErr, reasoncdErr, resndescErr, tdbtamtErr,payeenoErr,payeenmeErr,paymthbfErr,bankactkeyErr,bankkeyErr,bnkcdedscErr,crdtcrdErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sh533screen.class;
		screenSflRecord = Sh533screensfl.class;
		screenCtlRecord = Sh533screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sh533protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sh533screenctl.lrec.pageSubfile);
	}
}
