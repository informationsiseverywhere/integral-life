/*
 * File: Hcvsurc.java
 * Date: 29 August 2009 22:52:47
 * Author: Quipoz Limited
 *
 * Class transformed from HCVSURC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivcshTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.life.cashdividends.tablestructures.Th532rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   TRADITIONAL LIFE - CASH DIVIDEND SURRENDER CALCULATION
*
*   This subroutine is called from various programs via T6598.
*   It is responsible for returning the cash value, accumulated
*   dividend and O/S interest of a component including its
*   paid-up covers.  If the surrender is effected before the
*   policy anniversary date, the cash value is obtained accord-
*   ing to premium modes as follows
*
*   Monthly:
*   By interpolation across two in-force duration.
*
*         Cash Value = LV + (HV - LV) * D / 365
*
*   Where HV = Cash Value at next anniversary
*         LV = Cash Value at last anniversary
*         D = No. of in-force days from last anniversary
*
*   Yearly:
*   By discount of interest.
*
*         Cash Value = HV / (1 + I  * (1 - D / 365))
*
*   Where HV = Cash Value at next anniversary
*         I = Variable annual interest rate by
*              Cover/Issue Age/Mortality/Sex
*         D = No. of in-force days from last anniversary
*
*   Note that plan processing is NOT part of this scope and
*   hence the subroutine is only catered for plan level process-
*   ing.
*
*   The subroutine is called four times for each component, each
*   time has a different function as follow:
*
*       1/ Cash Value of the basic SI
*       2/ Cash Value of all paid-up SI
*       3/ Accumulated Dividend and capitalised interest
*       4/ O/S interest to be capitalised
*
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read CHDRMJA for billing frequency to determine if pro-rata
*     on cash value is required.
*
*   - Read COVRTRB for coverage details, such as ANB-AT-RCD, SEX,
*     etc.
*
*   - determine last and next anniversary dates
*      last = COVRTRB-UNIT-STATEMENT-DATE
*      next = COVRTRB-UNIT-STATEMENT-DATE + 1 (via DATCON2)
*
*   - Depending on SURC-ENDF, process cash value of SI in 4 steps
*        space - basic cash value
*          2   - paid up addition
*          3   - accumulated dividend
*          4   - O/S interest
*
*   Basic Cash Value >>
*   - work out in force durations base on the last and next
*     anniversary date against COVRTRB-CRRCD, so that the 2
*     durations reference to TH528 and retrieve the cash values,
*     LV and HV.
*   - also work out inforce durations in days, D with last
*     anniversary date against SURC-PTDATE
*   - calculate for monthly case,
*       Surrender value = LV + ((HV - LV) * D / 365)
*   - for yearly case, read TH532 for the interest discount rate
*       Surrender value = LV / (1 + rate * (1 - D / 365))
*   - set SURC-TYPE to 'S' and description 'Cash Value'
*
*   Paid-up Cash Value >>
*   For each HPUA read,
*     - work out in force durations base on the last and next
*       anniversary date against HPUA-CRRCD, so that the 2
*       durations reference to TH528 and retrieve the cash values,
*       LV and HV.
*     - also work out inforce durations in days, D with last
*       anniversary date against SURC-PTDATE
*     - calculate by interpolation only,
*          Surrender value = LV + ((HV - LV) * D / 365)
*   End-for
*   - set SURC-TYPE to 'P' and description 'P/U CshVal'
*
*   Accumulate Dividend>>
*   - Read HDIS for the dividend balance since last capitalisa-
*     tion date, exit section if MRNF
*   - Get all withdrawn dividend since last capitalisation date
*     from HDIV via HDIVCSH. Where these values will be -ve in
*     nature, add them to the dividend balance
*   - set SURC-TYPE to 'D' and description 'Dividend' and
*     surrender value to total dividend balance
*
*   O/S Interest      >>
*   - Get O/S interest to be capitalised from HDIS, exit if MRNF
*   - Read HCSD, with its ZCSHDIVMTH access TH501 for the
*     interest calculation subroutine
*   - Call TH501 interest calculation subr with parameters,
*        INT-FROM = last interest date from HDIS
*        INT-TO   = SURC-PTDATE
*        CAP-AMOUNT = dividend balance since last capitalisa-
*                     tion date
*        TRANNO   = zeroes
*        TRANSCD  = spaces
*   - Add calculated interest to O/S interest
*   - Calculate interest levied on the withdrawn dividend from
*     HDIV via HDIVCSH,
*     For each HDIVCSH read, call TH501 interest calc sbr
*        INT-FROM = greater (HDIS-LAST-INT-DATE, HDIVCSH-PTDATE)
*        INT-TO   = SURC-PTDATE
*        CAP-AMOUNT = HDIVCSH-DIVD-AMOUNT
*        TRANNO   = zeroes
*        TRANSCD  = spaces
*   - These interests are -ve in nature, add to O/S interest
*   - set SURC-TYPE to 'I' and description 'O/S Int' and
*     surrender value to total O/S interest
*
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Hcvsurc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "HCVSURC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-ACCUMULATORS */
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotInt = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotHpua = new PackedDecimalData(18, 5);
	private FixedLengthStringData wsaaTable = new FixedLengthStringData(5);

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaIssuedAge = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private FixedLengthStringData wsaaMortcls = new FixedLengthStringData(1).isAPartOf(wsaaItem, 6);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItem, 7);
	private ZonedDecimalData wsaaAnbAtCcd = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanSuffix = new FixedLengthStringData(4).isAPartOf(wsaaPlnsfx, 0, REDEFINE);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
		/* ERRORS */
	private static final String e031 = "E031";
	private static final String g344 = "G344";
	private static final String h155 = "H155";
	private static final String hl16 = "HL16";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String covrtrbrec = "COVRTRBREC";
	private static final String hdisrec = "HDISREC";
	private static final String hpuarec = "HPUAREC";
	private static final String hcsdrec = "HCSDREC";
	private static final String hdivcshrec = "HDIVCSHREC";
		/* TABLES */
	private static final String th501 = "TH501";
	private static final String th528 = "TH528";
	private static final String th532 = "TH532";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Th501rec th501rec = new Th501rec();
	private Th528rec th528rec = new Th528rec();
	private Th532rec th532rec = new Th532rec();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private WsaaVariablesInner wsaaVariablesInner = new WsaaVariablesInner();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	private boolean cdivFlag = false;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private static final String th527 = "TH527";
	private static final String DVAC = "DVAC";
	private Iterator<Itempf> itempfListIterator;
	private Th527rec th527rec = new Th527rec();
	private PackedDecimalData wsaaAnniversary = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaath527Date = new PackedDecimalData(8, 0).init(0);
	private Hdivdrec hdivdrec = new Hdivdrec();
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",  HcsdpfDAO.class);
	private PackedDecimalData wsaaPrevDividend = new PackedDecimalData(18, 3);
	private List<Hcsdpf> hsdpfList = null;
	private	Hcsdpf hcsdpf = null;
	private Itempf itempf = null;
	private int intdividFlag = 0;
	private String t6639 = "T6639";
	private T6639rec t6639rec = new T6639rec();
	
	private ExternalisedRules er = new ExternalisedRules();
	Vpxsurcrec vpxsurcRec = new Vpxsurcrec();
	private static final String ITEMCOY = "ITEMCOY";
	private static final String OSINT = "O/S Int";
	private static final String VPMSUBOSINT = "VPMSURROSINT";
	
	boolean susur002Permission = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		th528Found8008,
		exit8009,
		th532Found8208,
		exit8209,
		seExit9090,
		dbExit9190
	}

	public Hcvsurc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		main100();
		return190();
	}

protected void main100()
	{
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		chdrmjaIO.setChdrnum(srcalcpy.chdrChdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			dbError9100();
		}
		cdivFlag = FeaConfg.isFeatureExist("2", "BTPRO015", appVars, "IT");
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		covrtrbIO.setChdrnum(srcalcpy.chdrChdrnum);
		covrtrbIO.setLife(srcalcpy.lifeLife);
		covrtrbIO.setCoverage(srcalcpy.covrCoverage);
		covrtrbIO.setRider(srcalcpy.covrRider);
		covrtrbIO.setPlanSuffix(srcalcpy.planSuffix);
		covrtrbIO.setFormat(covrtrbrec);
		covrtrbIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covrtrbIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrtrbIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),srcalcpy.covrRider)
		|| isNE(covrtrbIO.getPlanSuffix(),srcalcpy.planSuffix)
		|| isEQ(covrtrbIO.getStatuz(),varcom.endp)) {
			wsaaPlnsfx.set(srcalcpy.planSuffix);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(srcalcpy.chdrChdrcoy);
			stringVariable1.addExpression(srcalcpy.chdrChdrnum);
			stringVariable1.addExpression(srcalcpy.lifeLife);
			stringVariable1.addExpression(srcalcpy.covrCoverage);
			stringVariable1.addExpression(srcalcpy.covrRider);
			stringVariable1.addExpression(wsaaPlanSuffix);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(g344);
			dbError9100();
		}
		/*    Use COVRTRB-UNIT-STATEMENT-DATE to calculate the next*/
		/*    anniversary date whereas the last anniversary date is itself*/
		wsaaVariablesInner.wsaaLastAnnivDate.set(covrtrbIO.getUnitStatementDate());
		datcon2rec.intDate1.set(covrtrbIO.getUnitStatementDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaVariablesInner.wsaaNextAnnivDate.set(datcon2rec.intDate2);
		/* Process Cash Value of SI*/
		vpxsurcRec = new Vpxsurcrec();
		if (isEQ(srcalcpy.endf,SPACES)) {
			basicCashValue1000();
			srcalcpy.endf.set("2");
		}
		else {
			if (isEQ(srcalcpy.endf,"2")) {
				puaCashValue2000();
				srcalcpy.endf.set("3");
			}
			else {
				if (isEQ(srcalcpy.endf,"3")) {
					if(cdivFlag){
						calcDividend3000();
					}
					accumDividend3000();
					srcalcpy.endf.set("4");
				}
				else {
					if (isEQ(srcalcpy.endf,"4")) {
						if(cdivFlag){
							calcinterest4000();
						}
						interest4000();
						srcalcpy.status.set(varcom.endp);
					}
					else {
						syserrrec.statuz.set(h155);
						systemError9000();
					}
				}
			}
		}
		susur002Permission=FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT");
	}

private void calcinterest4000() {
	
	if(intdividFlag == 0){
		return;
	}
	srcalcpy.actualVal.set(ZERO);
	if(isEQ(hcsdpf.getZdivopt(),DVAC) && isNE(wsaaPrevDividend,ZERO)){
		itempf = itempfDAO.readItdmpf("IT",srcalcpy.chdrChdrcoy.toString(),th501,srcalcpy.effdate.toInt(),StringUtils.rightPad(hcsdpf.getZcshdivmth(), 8));
		if(itempf != null){
			th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else{
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			systemError9000();
		}
	}
	else{
		return;
	}	
	initialize(hdvdintrec.divdIntRec);
	hdvdintrec.chdrChdrcoy.set(srcalcpy.chdrChdrcoy);
	hdvdintrec.chdrChdrnum.set(srcalcpy.chdrChdrnum);
	hdvdintrec.lifeLife.set(srcalcpy.lifeLife);
	hdvdintrec.covrCoverage.set(srcalcpy.covrCoverage);
	hdvdintrec.covrRider.set(srcalcpy.covrRider);
	hdvdintrec.plnsfx.set(ZERO);
	hdvdintrec.cntcurr.set(srcalcpy.currcode);
	hdvdintrec.transcd.set(SPACES);
	hdvdintrec.crtable.set(srcalcpy.crtable);
	hdvdintrec.effectiveDate.set(srcalcpy.effdate);
	hdvdintrec.tranno.set(ZERO);
	if(intdividFlag == 1){
	hdvdintrec.intFrom.set(wsaaAnniversary);
	}
	else{
	hdvdintrec.intFrom.set(covrtrbIO.getUnitStatementDate());
	}
	hdvdintrec.intTo.set(srcalcpy.effdate);
	hdvdintrec.capAmount.set(wsaaPrevDividend);
	hdvdintrec.intDuration.set(ZERO);
	hdvdintrec.intAmount.set(ZERO);
	hdvdintrec.intRate.set(ZERO);
	hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
	
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(VPMSUBOSINT)
			&& er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))) {
		if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
			callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
			if (isNE(hdvdintrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(hdvdintrec.statuz);
				systemError9000();
			}
			wsaaTotInt.add(hdvdintrec.intAmount);
		}
		srcalcpy.actualVal.set(wsaaTotInt);
		if (isLT(srcalcpy.actualVal,ZERO)) {
			srcalcpy.actualVal.set(ZERO);
		}
		srcalcpy.type.set("I");
		srcalcpy.description.set(OSINT);
	}
	else {
		vpxsurcRec.dividendAmt00.set(hdvdintrec.intAmount);
	}
}

private void calcDividend3000() {
	if(isGTE(covrtrbIO.getUnitStatementDate(),srcalcpy.effdate)){
		return;
	}
	readth527();
	calcDividend();
	if(isNE(hdivdrec.divdAmount,ZERO)){
		srcalcpy.actualVal.set(hdivdrec.divdAmount);
		wsaaPrevDividend.set(hdivdrec.divdAmount);
		srcalcpy.type.set("D");
		srcalcpy.description.set("Dividend");
	}
}

private void calcDividend() {
	
	datcon2rec.intDate1.set(covrtrbIO.getUnitStatementDate());
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		dbError9100();
	}
	else {
		wsaaAnniversary.set(datcon2rec.intDate2);	//CntAnniversaryDate
		
	}
	setuphcsd();
	if(isGT(wsaaAnniversary,srcalcpy.effdate)){
		if(isGTE(srcalcpy.effdate,covrtrbIO.getUnitStatementDate()) && isLTE(srcalcpy.effdate,wsaaAnniversary))
			intdividFlag = 2;
		return;
	}
	itempfListIterator = itempfList.iterator();
	iterateitempfList();
	if(isEQ(th527rec.th527Rec,SPACES)){
		itempfListIterator = itempfList.iterator();
		getLatestFactor();
		if(isNE(th527rec.th527Rec,SPACES)){
			intdividFlag = 1;
			processDividend();
		}
		else{
			return;
		}
	}
}

protected void setuphcsd(){
	
	hcsdpf = new Hcsdpf();
	hcsdpf.setChdrcoy(srcalcpy.chdrChdrcoy.toString());
	hcsdpf.setChdrnum(srcalcpy.chdrChdrnum.toString());	
	hcsdpf.setLife(srcalcpy.lifeLife.toString());
	hcsdpf.setCoverage(srcalcpy.covrCoverage.toString());
	hcsdpf.setRider(srcalcpy.covrRider.toString());
	hcsdpf.setPlnsfx(0);		
	hsdpfList = hcsdpfDAO.searchHcsdpfRecord(hcsdpf);
	for(Hcsdpf hcsdobj : hsdpfList){
	if(isEQ(hcsdobj.getValidflag(),"1")){
		itempf = itempfDAO.readItdmpf("IT",srcalcpy.chdrChdrcoy.toString(),t6639,srcalcpy.effdate.toInt(),StringUtils.rightPad(hcsdobj.getZcshdivmth()+covrtrbIO.getPstatcode(), 8));
		if(itempf != null){
			t6639rec.t6639Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else{
			syserrrec.params.set("IT".concat(srcalcpy.chdrChdrcoy.toString()).concat(t6639).concat(hcsdobj.getZcshdivmth()+covrtrbIO.getPstatcode()));
			systemError9000();
		}
		hcsdpf = hcsdobj;
		break;
	}
  }
}

private void iterateitempfList() {
	
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			if((wsaaAnniversary.toInt() >= Integer.parseInt(itempf.getItmfrm().toString()) )
					&& wsaaAnniversary.toInt() <= Integer.parseInt(itempf.getItmto().toString())){
				th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				return;
			}else{
				th527rec.th527Rec.set(SPACES);
			}
		}				
	}	
	
}

private void processDividend() {	
	/* check table has been set up correctly - must have non-zero*/
	/*  risk units otherwise we will be dividing by zero !!*/
	if (isEQ(th527rec.unit,ZERO)) {
		itdmIO.setItemitem(wsaaItem);
		syserrrec.params.set(itdmIO.getParams());
		dbError9100();
	}
	initialize(hdivdrec.dividendRec);
	hdivdrec.crrcd.set(covrtrbIO.getCrrcd());
	hdivdrec.periodTo.set(wsaaAnniversary);
	hdivdrec.periodFrom.set(covrtrbIO.getUnitStatementDate());
	hdivdrec.divdAmount.set(ZERO);
	hdivdrec.sumin.set(covrtrbIO.getSumins());
	hdivdrec.issuedAge.set(covrtrbIO.getAnbAtCcd());
	hdivdrec.crtable.set(covrtrbIO.getCrtable());
	hdivdrec.mortcls.set(covrtrbIO.getMortcls());
	hdivdrec.sex.set(covrtrbIO.getSex());
	hdivdrec.chdrChdrcoy.set(srcalcpy.chdrChdrcoy);
	hdivdrec.itmfrmTh527.set(wsaath527Date);
	if(isNE(t6639rec.revBonusProg,SPACES)){
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
	}
}

protected void getLatestFactor(){
	
	itempfListIterator = itempfList.iterator();
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				wsaath527Date.set(itempf.getItmfrm());
				return;
			}else{
				th527rec.th527Rec.set(SPACES);	
				}
			}			
}

private void readth527() {
	
	wsaaCrtable.set(srcalcpy.crtable);
	wsaaIssuedAge.set(covrtrbIO.getAnbAtCcd());
	wsaaMortcls.set(covrtrbIO.getMortcls());		
	wsaaSex.set(covrtrbIO.getSex());

	itempfList = itempfDAO.findBy("IT",srcalcpy.chdrChdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
	if(itempfList.size() == 0){ 
		
		while(itempfList.size() == 0){
 		
			if (isEQ(subString(wsaaItem, 5, 4),"****")) {
				itdmIO.setItemitem(wsaaItem);
				dbError9100();
				
			}
			else {
				if (isNE(subString(wsaaItem, 8, 1),"*")) {
					wsaaItem.setSub1String(8, 1, "*");
				}
				else {
					if (isNE(subString(wsaaItem, 7, 1),"*")) {
						wsaaItem.setSub1String(7, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 5, 2),"**")) {
							wsaaItem.setSub1String(5, 2, "**");
						}
					}
				}
			}
			itempfList = itempfDAO.findBy("IT",srcalcpy.chdrChdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
 		}
 				
 	}
}

protected void return190()
	{
		exitProgram();
	}

protected void basicCashValue1000()
	{
		start1000();
	}

protected void start1000()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(wsaaVariablesInner.wsaaNextAnnivDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaVariablesInner.wsaaDurNext.set(datcon3rec.freqFactor);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaVariablesInner.wsaaLastAnnivDate);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaVariablesInner.wsaaDayLastToSurr.set(datcon3rec.freqFactor);
		}
		/*    Calculate no. of days between surrender date                 */
		/*    and paid-to-date                                             */
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.effdate);
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaVariablesInner.wsaaDayFromSurr.set(datcon3rec.freqFactor);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaVariablesInner.wsaaLastAnnivDate);
		if(susur002Permission)
		{
		datcon3rec.intDate2.set(srcalcpy.effdate);
		}
		else
		{
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		}
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaVariablesInner.wsaaDayFromLast.set(datcon3rec.freqFactor);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setItemtabl(th528);
		wsaaCrtable.set(srcalcpy.crtable);
		wsaaAnbAtCcd.set(covrtrbIO.getAnbAtCcd());
		wsaaIssuedAge.set(wsaaAnbAtCcd);
		wsaaMortcls.set(covrtrbIO.getMortcls());
		wsaaSex.set(covrtrbIO.getSex());
		itdmIO.setFormat(itemrec);
		itdmIO.setStatuz(varcom.oK);
		processTh5288000();
		/* Match in-force duration in years with TH528 cash values to*/
		/* obtain the corresponding cash value WSAA-CV-NEXT*/
		if (isLTE(wsaaVariablesInner.wsaaDurNext, 99)) {
			wsaaVariablesInner.wsaaCvNext.set(th528rec.insprm[wsaaVariablesInner.wsaaDurNext.toInt()]);
		}
		else {
			/*        MOVE TH528-INSTPR       TO WSAA-CV-NEXT                  */
			compute(wsaaVariablesInner.wsaaCvNext, 0).set(th528rec.instpr[sub(wsaaVariablesInner.wsaaDurNext, 99).toInt()]);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItmfrm(srcalcpy.ptdate);
		itdmIO.setItemtabl(th532);
		wsaaCrtable.set(srcalcpy.crtable);
		wsaaAnbAtCcd.set(covrtrbIO.getAnbAtCcd());
		wsaaIssuedAge.set(wsaaAnbAtCcd);
		wsaaMortcls.set(covrtrbIO.getMortcls());
		wsaaSex.set(covrtrbIO.getSex());
		itdmIO.setFormat(itemrec);
		itdmIO.setStatuz(varcom.oK);
		processTh5328200();
		
		srcalcpy.type.set("S");
		srcalcpy.description.set("Cash Value");
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMSURRBSCSH") 
				&& er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))){
			if (isNE(chdrmjaIO.getBillfreq(),"01")) {
				/*    Calculate in-force duration in years between the last*/
				/*    anniversary date and risk commencement date*/
				wsaaVariablesInner.wsaaCvLast.set(ZERO);
				if (isGT(wsaaVariablesInner.wsaaLastAnnivDate, covrtrbIO.getCrrcd())) {
					datcon3rec.datcon3Rec.set(SPACES);
					datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
					datcon3rec.intDate2.set(wsaaVariablesInner.wsaaLastAnnivDate);
					datcon3rec.frequency.set("01");
					callProgram(Datcon3.class, datcon3rec.datcon3Rec);
					if (isNE(datcon3rec.statuz,varcom.oK)) {
						syserrrec.statuz.set(datcon3rec.statuz);
						syserrrec.params.set(datcon3rec.datcon3Rec);
						systemError9000();
					}
					else {
						wsaaVariablesInner.wsaaDurLast.set(datcon3rec.freqFactor);
					}
					/* Match in-force duration in years with TH528 cash values to*/
					/* obtain the corresponding cash value WSAA-CV-LAST*/
					if (isLTE(wsaaVariablesInner.wsaaDurLast, 99)) {
						wsaaVariablesInner.wsaaCvLast.set(th528rec.insprm[wsaaVariablesInner.wsaaDurLast.toInt()]);
					}
					else {
						/*            MOVE TH528-INSTPR                TO WSAA-CV-LAST     */
						compute(wsaaVariablesInner.wsaaCvNext, 0).set(th528rec.instpr[sub(wsaaVariablesInner.wsaaDurLast, 99).toInt()]);
					}
				}
				/* Calculate surrender value*/
				compute(wsaaVariablesInner.wsaaActualVal, 3).setRounded(div(mult((add(wsaaVariablesInner.wsaaCvLast, div(mult((sub(wsaaVariablesInner.wsaaCvNext, wsaaVariablesInner.wsaaCvLast)), wsaaVariablesInner.wsaaDayFromLast), 365))), covrtrbIO.getSumins()), (mult(th528rec.unit, th528rec.premUnit))));
				if (isGT(wsaaVariablesInner.wsaaDayFromSurr, 0)) {
					compute(srcalcpy.actualVal, 3).setRounded(div(wsaaVariablesInner.wsaaActualVal, (add(1, (div(mult(mult(th532rec.intanny, 0.01), wsaaVariablesInner.wsaaDayFromSurr), 365))))));
				}
				else {
					srcalcpy.actualVal.setRounded(wsaaVariablesInner.wsaaActualVal);
				}
			}
			else {
				/* Calculate surrender value with discount*/
				compute(srcalcpy.actualVal, 3).setRounded(div(mult((div(wsaaVariablesInner.wsaaCvNext, (add(1, mult((div(th532rec.intanny, 100)), (sub(1, div(wsaaVariablesInner.wsaaDayFromLast, 365)))))))), covrtrbIO.getSumins()), (mult(th528rec.unit, th528rec.premUnit))));
			}
		}
		else {
			srcalcpy.billfreq.set(chdrmjaIO.getBillfreq());
			srcalcpy.mortcls.set(wsaaMortcls);
			srcalcpy.sex.set(wsaaSex);
			srcalcpy.age.set(wsaaAnbAtCcd);
			srcalcpy.sumins.set(covrtrbIO.getSumins());
			srcalcpy.intDate1.set(wsaaVariablesInner.wsaaNextAnnivDate);
			srcalcpy.intDate2.set(wsaaVariablesInner.wsaaLastAnnivDate);
			//Externalization call
			callProgram("VPMSURRBSCSH", srcalcpy.surrenderRec);
			if (isEQ(srcalcpy.status,varcom.bomb)) {
				syserrrec.statuz.set(srcalcpy.status);
				syserrrec.subrname.set(wsaaSubr);
				systemError9000();
			}
		}
	}

protected void puaCashValue2000()
	{
		start2000();
	}

protected void start2000()
	{
		hpuaIO.setDataArea(SPACES);
		hpuaIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		hpuaIO.setChdrnum(srcalcpy.chdrChdrnum);
		hpuaIO.setLife(srcalcpy.lifeLife);
		hpuaIO.setCoverage(srcalcpy.covrCoverage);
		hpuaIO.setRider(srcalcpy.covrRider);
		hpuaIO.setPlanSuffix(srcalcpy.planSuffix);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFormat(hpuarec);
		hpuaIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			syserrrec.statuz.set(hpuaIO.getStatuz());
			dbError9100();
		}
		if (isNE(hpuaIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(hpuaIO.getLife(),srcalcpy.lifeLife)
		|| isNE(hpuaIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(hpuaIO.getRider(),srcalcpy.covrRider)
		|| isNE(hpuaIO.getPlanSuffix(),srcalcpy.planSuffix)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		wsaaTotHpua.set(ZERO);
		srcalcpy.type.set("P");
		srcalcpy.description.set("P/U CshVal");
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMSURRPDCSH") 
				&& er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))){
			while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
				calculatePuaCv2100();
				hpuaIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)
				&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9100();
				}
				if (isNE(hpuaIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
				|| isNE(hpuaIO.getChdrnum(),srcalcpy.chdrChdrnum)
				|| isNE(hpuaIO.getLife(),srcalcpy.lifeLife)
				|| isNE(hpuaIO.getCoverage(),srcalcpy.covrCoverage)
				|| isNE(hpuaIO.getRider(),srcalcpy.covrRider)
				|| isNE(hpuaIO.getPlanSuffix(),srcalcpy.planSuffix)) {
					hpuaIO.setStatuz(varcom.endp);
				}
			}
			compute(srcalcpy.actualVal, 6).setRounded(mult(wsaaTotHpua,1));
		}
		else {
			int counter = 0;
			vpxsurcRec.covrsumins00.set(hpuaIO.getSumin());
			vpxsurcRec.age00.set(wsaaAnbAtCcd);
			vpxsurcRec.crtable00.set(hpuaIO.getCrtable());
			vpxsurcRec.covrrcomdate00.set(hpuaIO.getCrrcd());
			srcalcpy.mortcls.set(wsaaMortcls);
			srcalcpy.sex.set(wsaaSex);
			srcalcpy.intDate2.set(wsaaVariablesInner.wsaaLastAnnivDate);
			srcalcpy.intDate1.set(wsaaVariablesInner.wsaaNextAnnivDate);
			while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
				hpuaIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)
				&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9100();
				}
				if (isNE(hpuaIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
				|| isNE(hpuaIO.getChdrnum(),srcalcpy.chdrChdrnum)
				|| isNE(hpuaIO.getLife(),srcalcpy.lifeLife)
				|| isNE(hpuaIO.getCoverage(),srcalcpy.covrCoverage)
				|| isNE(hpuaIO.getRider(),srcalcpy.covrRider)
				|| isNE(hpuaIO.getPlanSuffix(),srcalcpy.planSuffix)) {
					hpuaIO.setStatuz(varcom.endp);
				}
				counter = counter + 1;
				vpxsurcRec = setValues2000(hpuaIO, vpxsurcRec, counter);
			}
			if(isNE(counter,ZERO)) {
				srcalcpy.covrCount.set(counter);
				//Externalization call
				callProgram("VPMSURRPDCSH", srcalcpy.surrenderRec, vpxsurcRec);
				if (isEQ(srcalcpy.status,varcom.bomb)) {
					syserrrec.statuz.set(srcalcpy.status);
					syserrrec.subrname.set(wsaaSubr);
					systemError9000();
				}
			}
			else {
				compute(srcalcpy.actualVal, 6).setRounded(mult(wsaaTotHpua,1));
			}
		}
	}

protected void calculatePuaCv2100()
	{
		start2100();
	}

protected void start2100()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(hpuaIO.getCrrcd());
		datcon3rec.intDate2.set(wsaaVariablesInner.wsaaNextAnnivDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaVariablesInner.wsaaDurNext.set(datcon3rec.freqFactor);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItmfrm(hpuaIO.getCrrcd());
		itdmIO.setItemtabl(th528);
		wsaaCrtable.set(hpuaIO.getCrtable());
		wsaaAnbAtCcd.set(hpuaIO.getAnbAtCcd());
		wsaaIssuedAge.set(wsaaAnbAtCcd);
		wsaaMortcls.set(covrtrbIO.getMortcls());
		wsaaSex.set(covrtrbIO.getSex());
		itdmIO.setFormat(itemrec);
		itdmIO.setStatuz(varcom.oK);
		processTh5288000();
		/* Match in-force duration in years with TH528 cash values to*/
		/* obtain the corresponding cash value WSAA-CV-NEXT*/
		if (isLTE(wsaaVariablesInner.wsaaDurNext, 99)) {
			wsaaVariablesInner.wsaaCvNext.set(th528rec.insprm[wsaaVariablesInner.wsaaDurNext.toInt()]);
		}
		else {
			/*        MOVE TH528-INSTPR                TO WSAA-CV-NEXT         */
			compute(wsaaVariablesInner.wsaaCvNext, 0).set(th528rec.instpr[sub(wsaaVariablesInner.wsaaDurNext, 99).toInt()]);
		}
		/* Paid Up Addition Cash Value calculation is by interpolation*/
		/* always.*/
		/*    Calculate in-force duration in years between the last*/
		/*    anniversary date and risk commencement date*/
		wsaaVariablesInner.wsaaCvLast.set(ZERO);
		if (isGT(wsaaVariablesInner.wsaaLastAnnivDate, hpuaIO.getCrrcd())) {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(hpuaIO.getCrrcd());
			datcon3rec.intDate2.set(wsaaVariablesInner.wsaaLastAnnivDate);
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				systemError9000();
			}
			else {
				wsaaVariablesInner.wsaaDurLast.set(datcon3rec.freqFactor);
			}
			/* Match in-force duration in years with TH528 cash values to*/
			/* obtain the corresponding cash value WSAA-CV-LAST*/
			if (isLTE(wsaaVariablesInner.wsaaDurLast, 99)) {
				wsaaVariablesInner.wsaaCvLast.set(th528rec.insprm[wsaaVariablesInner.wsaaDurLast.toInt()]);
			}
			else {
				/*            MOVE TH528-INSTPR                TO WSAA-CV-LAST     */
				compute(wsaaVariablesInner.wsaaCvNext, 0).set(th528rec.instpr[sub(wsaaVariablesInner.wsaaDurLast, 99).toInt()]);
			}
		}
		/* Calculate surrender value*/
		compute(wsaaVariablesInner.wsaaHpuaCv, 1).setRounded(div(mult((add(wsaaVariablesInner.wsaaCvLast, (div(mult((sub(wsaaVariablesInner.wsaaCvNext, wsaaVariablesInner.wsaaCvLast)), wsaaVariablesInner.wsaaDayLastToSurr), 365)))), hpuaIO.getSumin()), (mult(th528rec.unit, th528rec.premUnit))));
		/* Accumulate the calculated cash value to surrender actual value*/
		wsaaTotHpua.add(wsaaVariablesInner.wsaaHpuaCv);
	}

protected Vpxsurcrec setValues2000(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec, int counter) 
	{
	switch(counter) {
		case 1: {
			setHPUA01(hupaio, vpxsurcrec);
			break;
		}
		case 2: {
			setHPUA02(hupaio, vpxsurcrec);
			break;
		}
		case 3: {
			setHPUA03(hupaio, vpxsurcrec);
			break;
		}
		case 4: {
			setHPUA04(hupaio, vpxsurcrec);
			break;
		}
		case 5: {
			setHPUA05(hupaio, vpxsurcrec);
			break;
		}
		case 6: {
			setHPUA06(hupaio, vpxsurcrec);
			break;
		}
		case 7: {
			setHPUA07(hupaio, vpxsurcrec);
			break;
		}
		case 8: {
			setHPUA08(hupaio, vpxsurcrec);
			break;
		}
		case 9: {
			setHPUA09(hupaio, vpxsurcrec);
			break;
		}
		default : {
			break;
		}
	}
	return vpxsurcrec;
	}

protected void setHPUA01(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age01.set(hupaio.getAnbAtCcd());
	vpxsurcrec.crtable01.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate01.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins01.set(hupaio.getSumin());
}
protected void setHPUA02(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age02.set(hupaio.getAnbAtCcd());
	vpxsurcrec.crtable02.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate02.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins02.set(hupaio.getSumin());
}
protected void setHPUA03(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age03.set(hupaio.getAnbAtCcd());
	vpxsurcrec.crtable03.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate03.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins03.set(hupaio.getSumin());
}
protected void setHPUA04(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age04.set(hupaio.getAnbAtCcd());
	vpxsurcrec.crtable04.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate04.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins04.set(hupaio.getSumin());
}
protected void setHPUA05(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age05.set(wsaaAnbAtCcd);
	vpxsurcrec.crtable05.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate05.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins05.set(hupaio.getSumin());
}
protected void setHPUA06(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age06.set(wsaaAnbAtCcd);
	vpxsurcrec.crtable06.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate06.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins06.set(hupaio.getSumin());
}
protected void setHPUA07(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age07.set(wsaaAnbAtCcd);
	vpxsurcrec.crtable07.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate07.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins07.set(hupaio.getSumin());
}
protected void setHPUA08(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age08.set(wsaaAnbAtCcd);
	vpxsurcrec.crtable08.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate08.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins08.set(hupaio.getSumin());
}
protected void setHPUA09(HpuaTableDAM hupaio,Vpxsurcrec vpxsurcrec){
	vpxsurcrec.age09.set(wsaaAnbAtCcd);
	vpxsurcrec.crtable09.set(hupaio.getCrtable());
	vpxsurcrec.covrrcomdate09.set(hupaio.getCrrcd());
	vpxsurcrec.covrsumins09.set(hupaio.getSumin());
}

protected void accumDividend3000()
	{
			start3100();
		}

protected void start3100()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		hdisIO.setChdrnum(srcalcpy.chdrChdrnum);
		hdisIO.setLife(srcalcpy.lifeLife);
		hdisIO.setCoverage(srcalcpy.covrCoverage);
		hdisIO.setRider(srcalcpy.covrRider);
		hdisIO.setPlanSuffix(srcalcpy.planSuffix);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			dbError9100();
		}
		if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {	
			return ;
		}
		srcalcpy.type.set("D");
		srcalcpy.description.set("Dividend");
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMSURRACDIV") 
				&& er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))){
			wsaaTotDividend.set(hdisIO.getBalSinceLastCap());
			hdivcshIO.setDataArea(SPACES);
			hdivcshIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			hdivcshIO.setChdrnum(srcalcpy.chdrChdrnum);
			hdivcshIO.setLife(srcalcpy.lifeLife);
			hdivcshIO.setCoverage(srcalcpy.covrCoverage);
			hdivcshIO.setRider(srcalcpy.covrRider);
			hdivcshIO.setPlanSuffix(srcalcpy.planSuffix);

		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

			compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
			hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
			hdivcshIO.setFormat(hdivcshrec);
			hdivcshIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(hdivcshIO.getStatuz());
				syserrrec.params.set(hdivcshIO.getParams());
				dbError9100();
			}
			if (isNE(hdivcshIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
			|| isNE(hdivcshIO.getChdrnum(),srcalcpy.chdrChdrnum)
			|| isNE(hdivcshIO.getLife(),srcalcpy.lifeLife)
			|| isNE(hdivcshIO.getCoverage(),srcalcpy.covrCoverage)
			|| isNE(hdivcshIO.getRider(),srcalcpy.covrRider)
			|| isNE(hdivcshIO.getPlanSuffix(),srcalcpy.planSuffix)) {
				hdivcshIO.setStatuz(varcom.endp);
			}
			while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
				wsaaTotDividend.add(hdivcshIO.getDivdAmount());
				hdivcshIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hdivcshIO);
				if (isNE(hdivcshIO.getStatuz(),varcom.oK)
				&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
					syserrrec.statuz.set(hdivcshIO.getStatuz());
					syserrrec.params.set(hdivcshIO.getParams());
					dbError9100();
				}
				if (isNE(hdivcshIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
				|| isNE(hdivcshIO.getChdrnum(),srcalcpy.chdrChdrnum)
				|| isNE(hdivcshIO.getLife(),srcalcpy.lifeLife)
				|| isNE(hdivcshIO.getCoverage(),srcalcpy.covrCoverage)
				|| isNE(hdivcshIO.getRider(),srcalcpy.covrRider)
				|| isNE(hdivcshIO.getPlanSuffix(),srcalcpy.planSuffix)) {
					hdivcshIO.setStatuz(varcom.endp);
				}
			}
			compute(srcalcpy.actualVal, 6).setRounded(mult(wsaaTotHpua,1));
			if(cdivFlag){
				srcalcpy.actualVal.add(wsaaTotDividend);
				wsaaPrevDividend.add(wsaaTotDividend);
			}
			else{
				srcalcpy.actualVal.set(wsaaTotDividend);
			}
		}
		else {
			int counter = 1;
			vpxsurcRec.dividendAmt00.set(hdisIO.getBalSinceLastCap());
			hdivcshIO.setDataArea(SPACES);
			hdivcshIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			hdivcshIO.setChdrnum(srcalcpy.chdrChdrnum);
			hdivcshIO.setLife(srcalcpy.lifeLife);
			hdivcshIO.setCoverage(srcalcpy.covrCoverage);
			hdivcshIO.setRider(srcalcpy.covrRider);
			hdivcshIO.setPlanSuffix(srcalcpy.planSuffix);
			//performance improvement -- Anjali
			hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

			compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
			hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
			hdivcshIO.setFormat(hdivcshrec);
			hdivcshIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(hdivcshIO.getStatuz());
				syserrrec.params.set(hdivcshIO.getParams());
				dbError9100();
			}
			if (isNE(hdivcshIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
			|| isNE(hdivcshIO.getChdrnum(),srcalcpy.chdrChdrnum)
			|| isNE(hdivcshIO.getLife(),srcalcpy.lifeLife)
			|| isNE(hdivcshIO.getCoverage(),srcalcpy.covrCoverage)
			|| isNE(hdivcshIO.getRider(),srcalcpy.covrRider)
			|| isNE(hdivcshIO.getPlanSuffix(),srcalcpy.planSuffix)) {
				hdivcshIO.setStatuz(varcom.endp);
			}
			while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
				setValues3100(counter);
				counter = counter + 1;
				hdivcshIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hdivcshIO);
				if (isNE(hdivcshIO.getStatuz(),varcom.oK)
				&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
					syserrrec.statuz.set(hdivcshIO.getStatuz());
					syserrrec.params.set(hdivcshIO.getParams());
					dbError9100();
				}
				if (isNE(hdivcshIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
				|| isNE(hdivcshIO.getChdrnum(),srcalcpy.chdrChdrnum)
				|| isNE(hdivcshIO.getLife(),srcalcpy.lifeLife)
				|| isNE(hdivcshIO.getCoverage(),srcalcpy.covrCoverage)
				|| isNE(hdivcshIO.getRider(),srcalcpy.covrRider)
				|| isNE(hdivcshIO.getPlanSuffix(),srcalcpy.planSuffix)) {
					hdivcshIO.setStatuz(varcom.endp);
				}
			}
			srcalcpy.covrCount.set(counter);
			//Externalization call
			callProgram("VPMSURRACDIV", srcalcpy.surrenderRec,vpxsurcRec);
			if (isEQ(srcalcpy.status,varcom.bomb)) {
				syserrrec.statuz.set(srcalcpy.status);
				syserrrec.subrname.set(wsaaSubr);
				systemError9000();
			}
		}
	}

protected void setValues3100(int counter)
	{
		switch(counter) {
			case 1: {
				vpxsurcRec.dividendAmt01.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 2: {
				vpxsurcRec.dividendAmt02.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 3: {
				vpxsurcRec.dividendAmt03.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 4: {
				vpxsurcRec.dividendAmt04.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 5: {
				vpxsurcRec.dividendAmt05.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 6: {
				vpxsurcRec.dividendAmt06.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 7: {
				vpxsurcRec.dividendAmt07.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 8: {
				vpxsurcRec.dividendAmt08.set(hdivcshIO.getDivdAmount());
				break;
			}
			case 9: {
				vpxsurcRec.dividendAmt09.set(hdivcshIO.getDivdAmount());
				break;
			}
			default : {
				break;
			}
		}
	}

protected void interest4000()
	{
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(VPMSUBOSINT) 
				&& er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))) {
			start4100();
		}
		else {
			start4000VpmsOn();
		}
	
	}

protected void start4100()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		hdisIO.setChdrnum(srcalcpy.chdrChdrnum);
		hdisIO.setLife(srcalcpy.lifeLife);
		hdisIO.setCoverage(srcalcpy.covrCoverage);
		hdisIO.setRider(srcalcpy.covrRider);
		hdisIO.setPlanSuffix(srcalcpy.planSuffix);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			dbError9100();
		}
		if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		wsaaTotInt.set(hdisIO.getOsInterest());
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		hcsdIO.setChdrnum(srcalcpy.chdrChdrnum);
		hcsdIO.setLife(srcalcpy.lifeLife);
		hcsdIO.setCoverage(srcalcpy.covrCoverage);
		hcsdIO.setRider(srcalcpy.covrRider);
		hcsdIO.setPlanSuffix(srcalcpy.planSuffix);
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hcsdIO.getStatuz());
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItmfrm(hdisIO.getNextIntDate());
		itdmIO.setItemtabl(th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch(ITEMCOY, "ITEMTABL");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),th501)) {
			itdmIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(itdmIO.getParams());
			systemError9000();
		}
		if (isEQ(hcsdIO.getZcshdivmth(),itdmIO.getItemitem())) {
			th501rec.th501Rec.set(itdmIO.getGenarea());
		}
		else {
			itdmIO.setStatuz(e031);
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			systemError9000();
		}
		if (isEQ(th501rec.intCalcSbr,SPACES)) {
			itdmIO.setStatuz(hl16);
			syserrrec.params.set(itdmIO.getParams());
			systemError9000();
		}
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(srcalcpy.chdrChdrcoy);
		hdvdintrec.chdrChdrnum.set(srcalcpy.chdrChdrnum);
		hdvdintrec.lifeLife.set(srcalcpy.lifeLife);
		hdvdintrec.covrCoverage.set(srcalcpy.covrCoverage);
		hdvdintrec.covrRider.set(srcalcpy.covrRider);
		hdvdintrec.plnsfx.set(srcalcpy.planSuffix);
		hdvdintrec.cntcurr.set(srcalcpy.chdrCurr);
		hdvdintrec.transcd.set(SPACES);
		hdvdintrec.crtable.set(srcalcpy.crtable);
		hdvdintrec.effectiveDate.set(srcalcpy.effdate);
		hdvdintrec.tranno.set(ZERO);
		hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
		hdvdintrec.intTo.set(srcalcpy.ptdate);
		hdvdintrec.capAmount.set(hdisIO.getBalSinceLastCap());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(hdvdintrec.statuz);
			systemError9000();
		}
		wsaaTotInt.add(hdvdintrec.intAmount);
		hdivcshIO.setDataArea(SPACES);
		hdivcshIO.setChdrcoy(hdisIO.getChdrcoy());
		hdivcshIO.setChdrnum(hdisIO.getChdrnum());
		hdivcshIO.setLife(hdisIO.getLife());
		hdivcshIO.setCoverage(hdisIO.getCoverage());
		hdivcshIO.setRider(hdisIO.getRider());
		hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
		hdivcshIO.setDivdIntCapDate(hdisIO.getNextIntDate());
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hdivcshIO.getStatuz());
			syserrrec.params.set(hdivcshIO.getParams());
			dbError9100();
		}
		if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
		|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
		|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			initialize(hdvdintrec.divdIntRec);
			hdvdintrec.chdrChdrcoy.set(srcalcpy.chdrChdrcoy);
			hdvdintrec.chdrChdrnum.set(srcalcpy.chdrChdrnum);
			hdvdintrec.lifeLife.set(srcalcpy.lifeLife);
			hdvdintrec.covrCoverage.set(srcalcpy.covrCoverage);
			hdvdintrec.covrRider.set(srcalcpy.covrRider);
			hdvdintrec.plnsfx.set(srcalcpy.planSuffix);
			hdvdintrec.cntcurr.set(srcalcpy.chdrCurr);
			hdvdintrec.transcd.set(SPACES);
			hdvdintrec.crtable.set(srcalcpy.crtable);
			hdvdintrec.effectiveDate.set(srcalcpy.effdate);
			hdvdintrec.tranno.set(ZERO);
			if (isGT(hdivcshIO.getEffdate(),hdisIO.getLastIntDate())) {
				hdvdintrec.intFrom.set(hdivcshIO.getEffdate());
			}
			else {
				hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
			}
			hdvdintrec.intTo.set(srcalcpy.ptdate);
			if(susur002Permission)
			{
				hdvdintrec.intTo.set(srcalcpy.effdate);
			}
			else
			{
				hdvdintrec.intTo.set(srcalcpy.ptdate);
			}
			hdvdintrec.capAmount.set(hdivcshIO.getDivdAmount());
			hdvdintrec.intDuration.set(ZERO);
			hdvdintrec.intAmount.set(ZERO);
			hdvdintrec.intRate.set(ZERO);
			hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
			callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
			if (isNE(hdvdintrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(hdvdintrec.statuz);
				systemError9000();
			}
			wsaaTotInt.add(hdvdintrec.intAmount);
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(hdivcshIO.getStatuz());
				syserrrec.params.set(hdivcshIO.getParams());
				dbError9100();
			}
			if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
			|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
			|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
			|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
			|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
			|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}
		if(cdivFlag){
			srcalcpy.actualVal.add(wsaaTotInt);
		}
		else{
		srcalcpy.actualVal.set(wsaaTotInt);
		}
		srcalcpy.type.set("I");
		srcalcpy.description.set(OSINT);
	}

protected void start4000VpmsOn()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		hdisIO.setChdrnum(srcalcpy.chdrChdrnum);
		hdisIO.setLife(srcalcpy.lifeLife);
		hdisIO.setCoverage(srcalcpy.covrCoverage);
		hdisIO.setRider(srcalcpy.covrRider);
		hdisIO.setPlanSuffix(srcalcpy.planSuffix);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			dbError9100();
		}
		if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		wsaaTotInt.set(hdisIO.getOsInterest());
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		hcsdIO.setChdrnum(srcalcpy.chdrChdrnum);
		hcsdIO.setLife(srcalcpy.lifeLife);
		hcsdIO.setCoverage(srcalcpy.covrCoverage);
		hcsdIO.setRider(srcalcpy.covrRider);
		hcsdIO.setPlanSuffix(srcalcpy.planSuffix);
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hcsdIO.getStatuz());
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItmfrm(hdisIO.getNextIntDate());
		itdmIO.setItemtabl(th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch(ITEMCOY, "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),th501)) {
			itdmIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(itdmIO.getParams());
			systemError9000();
		}
		if (isEQ(hcsdIO.getZcshdivmth(),itdmIO.getItemitem())) {
			th501rec.th501Rec.set(itdmIO.getGenarea());
		}
		else {
			itdmIO.setStatuz(e031);
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			systemError9000();
		}
		if (isEQ(th501rec.intCalcSbr,SPACES)) {
			itdmIO.setStatuz(hl16);
			syserrrec.params.set(itdmIO.getParams());
			systemError9000();
		}
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(srcalcpy.chdrChdrcoy);
		hdvdintrec.chdrChdrnum.set(srcalcpy.chdrChdrnum);
		hdvdintrec.lifeLife.set(srcalcpy.lifeLife);
		hdvdintrec.covrCoverage.set(srcalcpy.covrCoverage);
		hdvdintrec.covrRider.set(srcalcpy.covrRider);
		hdvdintrec.plnsfx.set(srcalcpy.planSuffix);
		hdvdintrec.cntcurr.set(srcalcpy.chdrCurr);
		hdvdintrec.transcd.set(SPACES);
		hdvdintrec.crtable.set(srcalcpy.crtable);
		hdvdintrec.effectiveDate.set(srcalcpy.effdate);
		hdvdintrec.tranno.set(ZERO);
		hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
		hdvdintrec.intTo.set(srcalcpy.ptdate);
		hdvdintrec.capAmount.set(hdisIO.getBalSinceLastCap());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		
		vpxsurcRec.dividendAmt00.set(hdisIO.getBalSinceLastCap());
		vpxsurcRec.covrrcomdate00.set(hdvdintrec.intFrom);
		
		hdivcshIO.setDataArea(SPACES);
		hdivcshIO.setChdrcoy(hdisIO.getChdrcoy());
		hdivcshIO.setChdrnum(hdisIO.getChdrnum());
		hdivcshIO.setLife(hdisIO.getLife());
		hdivcshIO.setCoverage(hdisIO.getCoverage());
		hdivcshIO.setRider(hdisIO.getRider());
		hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
		hdivcshIO.setDivdIntCapDate(hdisIO.getNextIntDate());
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hdivcshIO.getStatuz());
			syserrrec.params.set(hdivcshIO.getParams());
			dbError9100();
		}
		if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
		|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
		|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		int counter = 0;
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			initialize(hdvdintrec.divdIntRec);
			hdvdintrec.chdrChdrcoy.set(srcalcpy.chdrChdrcoy);
			hdvdintrec.chdrChdrnum.set(srcalcpy.chdrChdrnum);
			hdvdintrec.lifeLife.set(srcalcpy.lifeLife);
			hdvdintrec.covrCoverage.set(srcalcpy.covrCoverage);
			hdvdintrec.covrRider.set(srcalcpy.covrRider);
			hdvdintrec.plnsfx.set(srcalcpy.planSuffix);
			hdvdintrec.cntcurr.set(srcalcpy.chdrCurr);
			hdvdintrec.transcd.set(SPACES);
			hdvdintrec.crtable.set(srcalcpy.crtable);
			hdvdintrec.effectiveDate.set(srcalcpy.effdate);
			hdvdintrec.tranno.set(ZERO);
			if (isGT(hdivcshIO.getEffdate(),hdisIO.getLastIntDate())) {
				hdvdintrec.intFrom.set(hdivcshIO.getEffdate());
			}
			else {
				hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
			}
			hdvdintrec.intTo.set(srcalcpy.ptdate);
			if(susur002Permission)
			{
				hdvdintrec.intTo.set(srcalcpy.effdate);
			}
			else
			{
				hdvdintrec.intTo.set(srcalcpy.ptdate);
			}
			hdvdintrec.capAmount.set(hdivcshIO.getDivdAmount());
			hdvdintrec.intDuration.set(ZERO);
			hdvdintrec.intAmount.set(ZERO);
			hdvdintrec.intRate.set(ZERO);
			hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
			counter = counter + 1;
			vpxsurcRec = setDivdAmount4000(hdivcshIO, vpxsurcRec, counter);
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(hdivcshIO.getStatuz());
				syserrrec.params.set(hdivcshIO.getParams());
				dbError9100();
			}
			if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
			|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
			|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
			|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
			|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
			|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}
		srcalcpy.type.set("I");
		srcalcpy.description.set(OSINT);
		srcalcpy.covrCount.set(counter);
		//Externalization call
		callProgram(VPMSUBOSINT, srcalcpy.surrenderRec,vpxsurcRec);
		if (isEQ(srcalcpy.status,varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			syserrrec.subrname.set(wsaaSubr);
			systemError9000();
		}
		srcalcpy.actualVal.add(wsaaTotInt);
	}

protected Vpxsurcrec setDivdAmount4000(HdivcshTableDAM hdivcshio, Vpxsurcrec vpxsurcrec, int counter) 
	{
	switch(counter) {
		case 1: {
			vpxsurcrec.dividendAmt01.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate01.set(hdivcshio.getEffdate());
			break;
		}
		case 2: {
			vpxsurcrec.dividendAmt02.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate02.set(hdivcshio.getEffdate());
			break;
		}
		case 3: {
			vpxsurcrec.dividendAmt03.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate03.set(hdivcshio.getEffdate());
			break;
		}
		case 4: {
			vpxsurcrec.dividendAmt04.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate04.set(hdivcshio.getEffdate());
			break;
		}
		case 5: {
			vpxsurcrec.dividendAmt05.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate05.set(hdivcshio.getEffdate());
			break;
		}
		case 6: {
			vpxsurcrec.dividendAmt06.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate06.set(hdivcshio.getEffdate());
			break;
		}
		case 7: {
			vpxsurcrec.dividendAmt07.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate07.set(hdivcshio.getEffdate());
			break;
		}
		case 8: {
			vpxsurcrec.dividendAmt08.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate08.set(hdivcshio.getEffdate());
			break;
		}
		case 9: {
			vpxsurcrec.dividendAmt09.set(hdivcshio.getDivdAmount());
			vpxsurcrec.covrrcomdate09.set(hdivcshio.getEffdate());
			break;
		}
		default : {
			break;
		}
	}
	return vpxsurcrec;
	}

protected void processTh5288000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start8000();
					allMortcls8003();
					allIssuedAge8004();
				case th528Found8008: 
					th528Found8008();
				case exit8009: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start8000()
	{
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th528Found8008);
		}
		/*ALL-SEX*/
		if (isNE(subString(wsaaItem, 8, 1),"*")) {
			wsaaItem.setSub1String(8, 1, "*");
		}
		itdmIO.setItemtabl(th528);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th528Found8008);
		}
	}

protected void allMortcls8003()
	{
		if (isNE(subString(wsaaItem, 7, 1),"*")) {
			wsaaItem.setSub1String(7, 1, "*");
		}
		itdmIO.setItemtabl(th528);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th528Found8008);
		}
	}

protected void allIssuedAge8004()
	{
		if (isNE(subString(wsaaItem, 5, 2),"**")) {
			wsaaItem.setSub1String(5, 2, "**");
		}
		itdmIO.setItemtabl(th528);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th528Found8008);
		}
		else {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e031);
			systemError9000();
			goTo(GotoLabel.exit8009);
		}
	}

protected void th528Found8008()
	{
		th528rec.th528Rec.set(itdmIO.getGenarea());
	}

protected void readItdm8100()
	{
		readItdmPara8100();
	}

protected void readItdmPara8100()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e031);
			systemError9000();
		}
	}

protected void processTh5328200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start8200();
					allMortcls8203();
					allIssuedAge8204();
					allCrtable8205();
				case th532Found8208: 
					th532Found8208();
				case exit8209: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start8200()
	{
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th532Found8208);
		}
		/*ALL-SEX*/
		if (isNE(subString(wsaaItem, 8, 1),"*")) {
			wsaaItem.setSub1String(8, 1, "*");
		}
		itdmIO.setItemtabl(th532);
		itdmIO.setItmfrm(srcalcpy.ptdate);
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th532Found8208);
		}
	}

protected void allMortcls8203()
	{
		if (isNE(subString(wsaaItem, 7, 1),"*")) {
			wsaaItem.setSub1String(7, 1, "*");
		}
		itdmIO.setItemtabl(th532);
		itdmIO.setItmfrm(srcalcpy.ptdate);
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th532Found8208);
		}
	}

protected void allIssuedAge8204()
	{
		if (isNE(subString(wsaaItem, 5, 2),"**")) {
			wsaaItem.setSub1String(5, 2, "**");
		}
		itdmIO.setItemtabl(th532);
		itdmIO.setItmfrm(srcalcpy.ptdate);
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th532Found8208);
		}
	}

protected void allCrtable8205()
	{
		if (isNE(subString(wsaaItem, 1, 4),"****")) {
			wsaaItem.setSub1String(1, 4, "****");
		}
		itdmIO.setItemtabl(th532);
		itdmIO.setItmfrm(srcalcpy.ptdate);
		readItdm8100();
		if (isEQ(itdmIO.getItemitem(),wsaaItem)) {
			goTo(GotoLabel.th532Found8208);
		}
		else {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e031);
			systemError9000();
			goTo(GotoLabel.exit8209);
		}
	}

protected void th532Found8208()
	{
		th532rec.th532Rec.set(itdmIO.getGenarea());
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-VARIABLES--INNER
 */
private static final class WsaaVariablesInner { 
		/* WSAA-VARIABLES */
	private PackedDecimalData wsaaNextAnnivDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastAnnivDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaDurNext = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaDurLast = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaDayFromLast = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaDayFromSurr = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaDayLastToSurr = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaActualVal = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaCvNext = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaCvLast = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaHpuaCv = new PackedDecimalData(18, 5);
}
}
