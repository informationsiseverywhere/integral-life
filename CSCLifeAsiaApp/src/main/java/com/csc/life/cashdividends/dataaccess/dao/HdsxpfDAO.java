/******************************************************************************
 * File Name 		: HdsxpfDAO.java
 * Author			: sbatra9
 * Creation Date	: 24 June 2020
 * Project			: Integral Life
 * Description		: The DAO Interface for HDSXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdsxpf;

public interface HdsxpfDAO extends BaseDAO<Hdsxpf> {
	public List<Hdsxpf> searchHdsxpfRecord(String tableId, String memName,int batchExtractSize, int batchID);
}