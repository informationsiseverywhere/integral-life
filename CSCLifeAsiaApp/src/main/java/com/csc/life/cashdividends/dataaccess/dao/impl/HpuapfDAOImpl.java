package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.util.List;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.csc.life.cashdividends.dataaccess.dao.HpuapfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hpuapf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HpuapfDAOImpl extends BaseDAOImpl<Hpuapf> implements HpuapfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(HpuapfDAOImpl.class);

    public List<Hpuapf> selectHpuaRecord(Hpuapf hpuaData) {
        StringBuilder sqlCovrSelect1 = new StringBuilder();
        sqlCovrSelect1.append("SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,HPUANBR,VALIDFLAG,TRANNO,ANBCCD,SUMIN,SINGP,CRRCD,RSTATCODE,PSTATCODE,RCESDTE,CRTABLE,HDVPART");
        
        sqlCovrSelect1.append(" FROM HPUA WHERE CHDRCOY = ?  ");
        sqlCovrSelect1.append(" AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
  
        sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, HPUANBR ASC, UNIQUE_NUMBER DESC ");
     
   	
        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
   	 ResultSet sqlcovrpf1rs = null;
        List<Hpuapf> hpuapfList = null;
    	 try {
 			 ps.setInt(1,Integer.parseInt(hpuaData.getChdrcoy()));
 			 ps.setString(2, hpuaData.getChdrnum());
 			 ps.setString(3, hpuaData.getLife());
 			 ps.setString(4, hpuaData.getCoverage());
 			 ps.setString(5, hpuaData.getRider());
 			 ps.setInt(6, hpuaData.getPlanSuffix());
 			 sqlcovrpf1rs = executeQuery(ps);
 			 hpuapfList = new ArrayList<Hpuapf>();
             while (sqlcovrpf1rs.next()) {
                Hpuapf hpuapf = new Hpuapf();
                hpuapf.setChdrcoy(sqlcovrpf1rs.getString(1));
                hpuapf.setChdrnum(sqlcovrpf1rs.getString(2));
                hpuapf.setLife(sqlcovrpf1rs.getString(3));
                hpuapf.setJLife(sqlcovrpf1rs.getString(4));
                hpuapf.setCoverage(sqlcovrpf1rs.getString(5));
                hpuapf.setRider(sqlcovrpf1rs.getString(6));
                hpuapf.setPlanSuffix(sqlcovrpf1rs.getInt(7));
                hpuapf.setPuAddNbr(sqlcovrpf1rs.getInt(8));
                hpuapf.setValidflag(sqlcovrpf1rs.getString(9));
                hpuapf.setTranno(sqlcovrpf1rs.getInt(10));
                hpuapf.setAnbAtCcd(sqlcovrpf1rs.getInt(11));
                hpuapf.setSumin(sqlcovrpf1rs.getInt(12));
                hpuapf.setSingp(sqlcovrpf1rs.getInt(13));
                hpuapf.setCrrcd(sqlcovrpf1rs.getInt(14));
                hpuapf.setRstatcode(sqlcovrpf1rs.getString(15));
                hpuapf.setPstatcode(sqlcovrpf1rs.getString(16));
                hpuapf.setRiskCessDate(sqlcovrpf1rs.getInt(17));
                hpuapf.setCrtable(sqlcovrpf1rs.getString(18));
                hpuapf.setDivdParticipant(sqlcovrpf1rs.getString(19));
                hpuapfList.add(hpuapf);
         
             
            }

        } catch (SQLException e) {
            LOGGER.error("searchCovrRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, sqlcovrpf1rs);
        }
        return hpuapfList;
    }
}