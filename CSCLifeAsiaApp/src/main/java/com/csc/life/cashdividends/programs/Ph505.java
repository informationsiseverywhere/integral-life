/*
 * File: Ph505.java
 * Date: 30 August 2009 1:04:08
 * Author: Quipoz Limited
 * 
 * Class transformed from PH505.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.procedures.Th505pt;
import com.csc.life.cashdividends.screens.Sh505ScreenVars;
import com.csc.life.cashdividends.tablestructures.Th505rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Ph505 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH505");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Th505rec th505rec = new Th505rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh505ScreenVars sv = ScreenProgram.getScreenVars( Sh505ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public Ph505() {
		super();
		screenVars = sv;
		new ScreenModel("Sh505", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th505rec.th505Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		th505rec.ageIssageFrm01.set(ZERO);
		th505rec.ageIssageFrm02.set(ZERO);
		th505rec.ageIssageFrm03.set(ZERO);
		th505rec.ageIssageFrm04.set(ZERO);
		th505rec.ageIssageFrm05.set(ZERO);
		th505rec.ageIssageFrm06.set(ZERO);
		th505rec.ageIssageFrm07.set(ZERO);
		th505rec.ageIssageFrm08.set(ZERO);
		th505rec.ageIssageTo01.set(ZERO);
		th505rec.ageIssageTo02.set(ZERO);
		th505rec.ageIssageTo03.set(ZERO);
		th505rec.ageIssageTo04.set(ZERO);
		th505rec.ageIssageTo05.set(ZERO);
		th505rec.ageIssageTo06.set(ZERO);
		th505rec.ageIssageTo07.set(ZERO);
		th505rec.ageIssageTo08.set(ZERO);
		th505rec.premCessageFrom01.set(ZERO);
		th505rec.premCessageFrom02.set(ZERO);
		th505rec.premCessageFrom03.set(ZERO);
		th505rec.premCessageFrom04.set(ZERO);
		th505rec.premCessageFrom05.set(ZERO);
		th505rec.premCessageFrom06.set(ZERO);
		th505rec.premCessageFrom07.set(ZERO);
		th505rec.premCessageFrom08.set(ZERO);
		th505rec.premCessageTo01.set(ZERO);
		th505rec.premCessageTo02.set(ZERO);
		th505rec.premCessageTo03.set(ZERO);
		th505rec.premCessageTo04.set(ZERO);
		th505rec.premCessageTo05.set(ZERO);
		th505rec.premCessageTo06.set(ZERO);
		th505rec.premCessageTo07.set(ZERO);
		th505rec.premCessageTo08.set(ZERO);
		th505rec.premCesstermFrom01.set(ZERO);
		th505rec.premCesstermFrom02.set(ZERO);
		th505rec.premCesstermFrom03.set(ZERO);
		th505rec.premCesstermFrom04.set(ZERO);
		th505rec.premCesstermFrom05.set(ZERO);
		th505rec.premCesstermFrom06.set(ZERO);
		th505rec.premCesstermFrom07.set(ZERO);
		th505rec.premCesstermFrom08.set(ZERO);
		th505rec.premCesstermTo01.set(ZERO);
		th505rec.premCesstermTo02.set(ZERO);
		th505rec.premCesstermTo03.set(ZERO);
		th505rec.premCesstermTo04.set(ZERO);
		th505rec.premCesstermTo05.set(ZERO);
		th505rec.premCesstermTo06.set(ZERO);
		th505rec.premCesstermTo07.set(ZERO);
		th505rec.premCesstermTo08.set(ZERO);
		th505rec.riskCessageFrom01.set(ZERO);
		th505rec.riskCessageFrom02.set(ZERO);
		th505rec.riskCessageFrom03.set(ZERO);
		th505rec.riskCessageFrom04.set(ZERO);
		th505rec.riskCessageFrom05.set(ZERO);
		th505rec.riskCessageFrom06.set(ZERO);
		th505rec.riskCessageFrom07.set(ZERO);
		th505rec.riskCessageFrom08.set(ZERO);
		th505rec.riskCessageTo01.set(ZERO);
		th505rec.riskCessageTo02.set(ZERO);
		th505rec.riskCessageTo03.set(ZERO);
		th505rec.riskCessageTo04.set(ZERO);
		th505rec.riskCessageTo05.set(ZERO);
		th505rec.riskCessageTo06.set(ZERO);
		th505rec.riskCessageTo07.set(ZERO);
		th505rec.riskCessageTo08.set(ZERO);
		th505rec.riskCesstermFrom01.set(ZERO);
		th505rec.riskCesstermFrom02.set(ZERO);
		th505rec.riskCesstermFrom03.set(ZERO);
		th505rec.riskCesstermFrom04.set(ZERO);
		th505rec.riskCesstermFrom05.set(ZERO);
		th505rec.riskCesstermFrom06.set(ZERO);
		th505rec.riskCesstermFrom07.set(ZERO);
		th505rec.riskCesstermFrom08.set(ZERO);
		th505rec.riskCesstermTo01.set(ZERO);
		th505rec.riskCesstermTo02.set(ZERO);
		th505rec.riskCesstermTo03.set(ZERO);
		th505rec.riskCesstermTo04.set(ZERO);
		th505rec.riskCesstermTo05.set(ZERO);
		th505rec.riskCesstermTo06.set(ZERO);
		th505rec.riskCesstermTo07.set(ZERO);
		th505rec.riskCesstermTo08.set(ZERO);
		th505rec.sumInsMax.set(ZERO);
		th505rec.sumInsMin.set(ZERO);
		th505rec.termIssageFrm01.set(ZERO);
		th505rec.termIssageFrm02.set(ZERO);
		th505rec.termIssageFrm03.set(ZERO);
		th505rec.termIssageFrm04.set(ZERO);
		th505rec.termIssageFrm05.set(ZERO);
		th505rec.termIssageFrm06.set(ZERO);
		th505rec.termIssageFrm07.set(ZERO);
		th505rec.termIssageFrm08.set(ZERO);
		th505rec.termIssageTo01.set(ZERO);
		th505rec.termIssageTo02.set(ZERO);
		th505rec.termIssageTo03.set(ZERO);
		th505rec.termIssageTo04.set(ZERO);
		th505rec.termIssageTo05.set(ZERO);
		th505rec.termIssageTo06.set(ZERO);
		th505rec.termIssageTo07.set(ZERO);
		th505rec.termIssageTo08.set(ZERO);
		th505rec.prmbasis01.set(SPACES);
		th505rec.prmbasis02.set(SPACES);
		th505rec.prmbasis03.set(SPACES);
		th505rec.prmbasis04.set(SPACES);
		th505rec.prmbasis05.set(SPACES);
		th505rec.prmbasis06.set(SPACES);
	}

protected void generalArea1045()
	{
		sv.ageIssageFrms.set(th505rec.ageIssageFrms);
		sv.ageIssageTos.set(th505rec.ageIssageTos);
		sv.eaage.set(th505rec.eaage);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.liencds.set(th505rec.liencds);
		sv.mortclss.set(th505rec.mortclss);
		sv.premCessageFroms.set(th505rec.premCessageFroms);
		sv.premCessageTos.set(th505rec.premCessageTos);
		sv.premCesstermFroms.set(th505rec.premCesstermFroms);
		sv.premCesstermTos.set(th505rec.premCesstermTos);
		sv.riskCessageFroms.set(th505rec.riskCessageFroms);
		sv.riskCessageTos.set(th505rec.riskCessageTos);
		sv.riskCesstermFroms.set(th505rec.riskCesstermFroms);
		sv.riskCesstermTos.set(th505rec.riskCesstermTos);
		sv.specind.set(th505rec.specind);
		sv.sumInsMax.set(th505rec.sumInsMax);
		sv.sumInsMin.set(th505rec.sumInsMin);
		sv.termIssageFrms.set(th505rec.termIssageFrms);
		sv.termIssageTos.set(th505rec.termIssageTos);
		sv.zdefdivopt.set(th505rec.zdefdivopt);
		sv.zdivopts.set(th505rec.zdivopts);
		sv.prmbasiss.set(th505rec.prmbasiss);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
					screenIo2010();
					exit2090();
				}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(th505rec.th505Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.ageIssageFrms,th505rec.ageIssageFrms)) {
			th505rec.ageIssageFrms.set(sv.ageIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ageIssageTos,th505rec.ageIssageTos)) {
			th505rec.ageIssageTos.set(sv.ageIssageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.eaage,th505rec.eaage)) {
			th505rec.eaage.set(sv.eaage);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.liencds,th505rec.liencds)) {
			th505rec.liencds.set(sv.liencds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mortclss,th505rec.mortclss)) {
			th505rec.mortclss.set(sv.mortclss);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageFroms,th505rec.premCessageFroms)) {
			th505rec.premCessageFroms.set(sv.premCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageTos,th505rec.premCessageTos)) {
			th505rec.premCessageTos.set(sv.premCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermFroms,th505rec.premCesstermFroms)) {
			th505rec.premCesstermFroms.set(sv.premCesstermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermTos,th505rec.premCesstermTos)) {
			th505rec.premCesstermTos.set(sv.premCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCessageFroms,th505rec.riskCessageFroms)) {
			th505rec.riskCessageFroms.set(sv.riskCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCessageTos,th505rec.riskCessageTos)) {
			th505rec.riskCessageTos.set(sv.riskCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCesstermFroms,th505rec.riskCesstermFroms)) {
			th505rec.riskCesstermFroms.set(sv.riskCesstermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCesstermTos,th505rec.riskCesstermTos)) {
			th505rec.riskCesstermTos.set(sv.riskCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.specind,th505rec.specind)) {
			th505rec.specind.set(sv.specind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMax,th505rec.sumInsMax)) {
			th505rec.sumInsMax.set(sv.sumInsMax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMin,th505rec.sumInsMin)) {
			th505rec.sumInsMin.set(sv.sumInsMin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageFrms,th505rec.termIssageFrms)) {
			th505rec.termIssageFrms.set(sv.termIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageTos,th505rec.termIssageTos)) {
			th505rec.termIssageTos.set(sv.termIssageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zdefdivopt,th505rec.zdefdivopt)) {
			th505rec.zdefdivopt.set(sv.zdefdivopt);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zdivopts,th505rec.zdivopts)) {
			th505rec.zdivopts.set(sv.zdivopts);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prmbasiss, th505rec.prmbasiss)) {
			th505rec.prmbasiss.set(sv.prmbasiss);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Th505pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
