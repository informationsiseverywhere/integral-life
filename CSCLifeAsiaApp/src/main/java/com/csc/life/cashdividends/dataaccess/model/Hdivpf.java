/******************************************************************************
 * File Name 		: Hdivpf.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The Model Class for HDIVPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Hdivpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "HDIVPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private Integer hpuanbr;
	private Integer tranno;
	private String batccoy;
	private String batcbrn;
	private Integer batcactyr;
	private Integer batcactmn;
	private String batctrcde;
	private String batcbatch;
	private String cntcurr;
	private Integer effdate;
	private Integer hdvaldt;
	private String hdvtyp;
	private BigDecimal hdvamt;
	private BigDecimal hdvrate;
	private Integer hdveffdt;
	private String zdivopt;
	private String zcshdivmth;
	private Integer hdvopttx;
	private Integer hdvcaptx;
	private Integer hincapdt;
	private Integer hdvsmtno;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	public Hdivpf ( ) {}

	public Hdivpf (Hdivpf hdivpf) {
		this.uniqueNumber = hdivpf.uniqueNumber;
		this.chdrcoy = hdivpf.chdrcoy;
		this.chdrnum = hdivpf.chdrnum;
		this.life = hdivpf.life;
		this.jlife = hdivpf.jlife;
		this.coverage = hdivpf.coverage;
		this.rider = hdivpf.rider;
		this.plnsfx = hdivpf.plnsfx;
		this.hpuanbr = hdivpf.hpuanbr;
		this.tranno = hdivpf.tranno;
		this.batccoy = hdivpf.batccoy;
		this.batcbrn = hdivpf.batcbrn;
		this.batcactyr = hdivpf.batcactyr;
		this.batcactmn = hdivpf.batcactmn;
		this.batctrcde = hdivpf.batctrcde;
		this.batcbatch = hdivpf.batcbatch;
		this.cntcurr = hdivpf.cntcurr;
		this.effdate = hdivpf.effdate;
		this.hdvaldt = hdivpf.hdvaldt;
		this.hdvtyp = hdivpf.hdvtyp;
		this.hdvamt = hdivpf.hdvamt;
		this.hdvrate = hdivpf.hdvrate;
		this.hdveffdt = hdivpf.hdveffdt;
		this.zdivopt = hdivpf.zdivopt;
		this.zcshdivmth = hdivpf.zcshdivmth;
		this.hdvopttx = hdivpf.hdvopttx;
		this.hdvcaptx = hdivpf.hdvcaptx;
		this.hincapdt = hdivpf.hincapdt;
		this.hdvsmtno = hdivpf.hdvsmtno;
		this.usrprf = hdivpf.usrprf;
		this.jobnm = hdivpf.jobnm;
		this.datime = hdivpf.datime;
	}
	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public Integer getHpuanbr(){
		return this.hpuanbr;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public String getBatccoy(){
		return this.batccoy;
	}
	public String getBatcbrn(){
		return this.batcbrn;
	}
	public Integer getBatcactyr(){
		return this.batcactyr;
	}
	public Integer getBatcactmn(){
		return this.batcactmn;
	}
	public String getBatctrcde(){
		return this.batctrcde;
	}
	public String getBatcbatch(){
		return this.batcbatch;
	}
	public String getCntcurr(){
		return this.cntcurr;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public Integer getHdvaldt(){
		return this.hdvaldt;
	}
	public String getHdvtyp(){
		return this.hdvtyp;
	}
	public BigDecimal getHdvamt(){
		return this.hdvamt;
	}
	public BigDecimal getHdvrate(){
		return this.hdvrate;
	}
	public Integer getHdveffdt(){
		return this.hdveffdt;
	}
	public String getZdivopt(){
		return this.zdivopt;
	}
	public String getZcshdivmth(){
		return this.zcshdivmth;
	}
	public Integer getHdvopttx(){
		return this.hdvopttx;
	}
	public Integer getHdvcaptx(){
		return this.hdvcaptx;
	}
	public Integer getHincapdt(){
		return this.hincapdt;
	}
	public Integer getHdvsmtno(){
		return this.hdvsmtno;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setHpuanbr( Integer hpuanbr ){
		 this.hpuanbr = hpuanbr;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setBatccoy( String batccoy ){
		 this.batccoy = batccoy;
	}
	public void setBatcbrn( String batcbrn ){
		 this.batcbrn = batcbrn;
	}
	public void setBatcactyr( Integer batcactyr ){
		 this.batcactyr = batcactyr;
	}
	public void setBatcactmn( Integer batcactmn ){
		 this.batcactmn = batcactmn;
	}
	public void setBatctrcde( String batctrcde ){
		 this.batctrcde = batctrcde;
	}
	public void setBatcbatch( String batcbatch ){
		 this.batcbatch = batcbatch;
	}
	public void setCntcurr( String cntcurr ){
		 this.cntcurr = cntcurr;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setHdvaldt( Integer hdvaldt ){
		 this.hdvaldt = hdvaldt;
	}
	public void setHdvtyp( String hdvtyp ){
		 this.hdvtyp = hdvtyp;
	}
	public void setHdvamt( BigDecimal hdvamt ){
		 this.hdvamt = hdvamt;
	}
	public void setHdvrate( BigDecimal hdvrate ){
		 this.hdvrate = hdvrate;
	}
	public void setHdveffdt( Integer hdveffdt ){
		 this.hdveffdt = hdveffdt;
	}
	public void setZdivopt( String zdivopt ){
		 this.zdivopt = zdivopt;
	}
	public void setZcshdivmth( String zcshdivmth ){
		 this.zcshdivmth = zcshdivmth;
	}
	public void setHdvopttx( Integer hdvopttx ){
		 this.hdvopttx = hdvopttx;
	}
	public void setHdvcaptx( Integer hdvcaptx ){
		 this.hdvcaptx = hdvcaptx;
	}
	public void setHincapdt( Integer hincapdt ){
		 this.hincapdt = hincapdt;
	}
	public void setHdvsmtno( Integer hdvsmtno ){
		 this.hdvsmtno = hdvsmtno;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("HPUANBR:		");
		output.append(getHpuanbr());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("BATCCOY:		");
		output.append(getBatccoy());
		output.append("\r\n");
		output.append("BATCBRN:		");
		output.append(getBatcbrn());
		output.append("\r\n");
		output.append("BATCACTYR:		");
		output.append(getBatcactyr());
		output.append("\r\n");
		output.append("BATCACTMN:		");
		output.append(getBatcactmn());
		output.append("\r\n");
		output.append("BATCTRCDE:		");
		output.append(getBatctrcde());
		output.append("\r\n");
		output.append("BATCBATCH:		");
		output.append(getBatcbatch());
		output.append("\r\n");
		output.append("CNTCURR:		");
		output.append(getCntcurr());
		output.append("\r\n");
		output.append("EFFDATE:		");
		output.append(getEffdate());
		output.append("\r\n");
		output.append("HDVALDT:		");
		output.append(getHdvaldt());
		output.append("\r\n");
		output.append("HDVTYP:		");
		output.append(getHdvtyp());
		output.append("\r\n");
		output.append("HDVAMT:		");
		output.append(getHdvamt());
		output.append("\r\n");
		output.append("HDVRATE:		");
		output.append(getHdvrate());
		output.append("\r\n");
		output.append("HDVEFFDT:		");
		output.append(getHdveffdt());
		output.append("\r\n");
		output.append("ZDIVOPT:		");
		output.append(getZdivopt());
		output.append("\r\n");
		output.append("ZCSHDIVMTH:		");
		output.append(getZcshdivmth());
		output.append("\r\n");
		output.append("HDVOPTTX:		");
		output.append(getHdvopttx());
		output.append("\r\n");
		output.append("HDVCAPTX:		");
		output.append(getHdvcaptx());
		output.append("\r\n");
		output.append("HINCAPDT:		");
		output.append(getHincapdt());
		output.append("\r\n");
		output.append("HDVSMTNO:		");
		output.append(getHdvsmtno());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
