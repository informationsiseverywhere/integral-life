package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:45
 * Description:
 * Copybook name: HSUDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hsudkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hsudFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hsudKey = new FixedLengthStringData(64).isAPartOf(hsudFileKey, 0, REDEFINE);
  	public FixedLengthStringData hsudChdrcoy = new FixedLengthStringData(1).isAPartOf(hsudKey, 0);
  	public FixedLengthStringData hsudChdrnum = new FixedLengthStringData(8).isAPartOf(hsudKey, 1);
  	public FixedLengthStringData hsudLife = new FixedLengthStringData(2).isAPartOf(hsudKey, 9);
  	public FixedLengthStringData hsudCoverage = new FixedLengthStringData(2).isAPartOf(hsudKey, 11);
  	public FixedLengthStringData hsudRider = new FixedLengthStringData(2).isAPartOf(hsudKey, 13);
  	public PackedDecimalData hsudPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hsudKey, 15);
  	public PackedDecimalData hsudTranno = new PackedDecimalData(5, 0).isAPartOf(hsudKey, 18);
  	public FixedLengthStringData hsudFieldType = new FixedLengthStringData(1).isAPartOf(hsudKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(hsudKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hsudFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hsudFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}