package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HpuapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:34
 * Class transformed from HPUAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HpuapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 110;
	public FixedLengthStringData hpuarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hpuapfRecord = hpuarec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hpuarec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hpuarec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hpuarec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hpuarec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hpuarec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hpuarec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hpuarec);
	public PackedDecimalData puAddNbr = DD.hpuanbr.copy().isAPartOf(hpuarec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(hpuarec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hpuarec);
	public PackedDecimalData anbAtCcd = DD.anbccd.copy().isAPartOf(hpuarec);
	public PackedDecimalData sumin = DD.sumin.copy().isAPartOf(hpuarec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(hpuarec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(hpuarec);
	public FixedLengthStringData rstatcode = DD.rstatcode.copy().isAPartOf(hpuarec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(hpuarec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(hpuarec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hpuarec);
	public FixedLengthStringData divdParticipant = DD.hdvpart.copy().isAPartOf(hpuarec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hpuarec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hpuarec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hpuarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HpuapfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HpuapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HpuapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HpuapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpuapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HpuapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpuapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HPUAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"HPUANBR, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"ANBCCD, " +
							"SUMIN, " +
							"SINGP, " +
							"CRRCD, " +
							"RSTATCODE, " +
							"PSTATCODE, " +
							"RCESDTE, " +
							"CRTABLE, " +
							"HDVPART, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     puAddNbr,
                                     validflag,
                                     tranno,
                                     anbAtCcd,
                                     sumin,
                                     singp,
                                     crrcd,
                                     rstatcode,
                                     pstatcode,
                                     riskCessDate,
                                     crtable,
                                     divdParticipant,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		puAddNbr.clear();
  		validflag.clear();
  		tranno.clear();
  		anbAtCcd.clear();
  		sumin.clear();
  		singp.clear();
  		crrcd.clear();
  		rstatcode.clear();
  		pstatcode.clear();
  		riskCessDate.clear();
  		crtable.clear();
  		divdParticipant.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHpuarec() {
  		return hpuarec;
	}

	public FixedLengthStringData getHpuapfRecord() {
  		return hpuapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHpuarec(what);
	}

	public void setHpuarec(Object what) {
  		this.hpuarec.set(what);
	}

	public void setHpuapfRecord(Object what) {
  		this.hpuapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hpuarec.getLength());
		result.set(hpuarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}