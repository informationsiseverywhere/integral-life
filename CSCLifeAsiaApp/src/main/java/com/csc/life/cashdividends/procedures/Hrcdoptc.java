/*
 * File: Hrcdoptc.java
 * Date: 29 August 2009 22:54:32
 * Author: Quipoz Limited
 *
 * Class transformed from HRCDOPTC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisgrvTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*  DIVIDEND OPTION CHANGE GENERIC REVERSAL
*
*
*  This subroutine is called from REVCOMPM via T5671 as part of  m
*  the component change reversal.  It reverses the cash dividend
*  option change related processing done in HCDOPTC.
*
*
*  MAIN PROCESSING
*
* - Delete validflag '1' HDIS records and re-instate most recent
*   validflag '2' records
*                                                                 <002>
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hrcdoptc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HRCDOPTC";
		/* FORMATS */
	private String hdisgrvrec = "HDISGRVREC";
	private String hdisrec = "HDISREC";
	private String hl15 = "HL15";
	private Greversrec greversrec = new Greversrec();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend & Interest Generic Reversal*/
	private HdisgrvTableDAM hdisgrvIO = new HdisgrvTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		xxxxErrorProg
	}

	public Hrcdoptc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void xxxxFatalError()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					xxxxFatalErrors();
				}
				case xxxxErrorProg: {
					xxxxErrorProg();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.xxxxErrorProg);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		greversrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		wsaaBatckey.set(greversrec.batckey);
		/*EXIT*/
	}

protected void process2000()
	{
		process2001();
	}

protected void process2001()
	{
		hdisgrvIO.setDataKey(SPACES);
		hdisgrvIO.setChdrcoy(greversrec.chdrcoy);
		hdisgrvIO.setChdrnum(greversrec.chdrnum);
		hdisgrvIO.setLife(greversrec.life);
		hdisgrvIO.setCoverage(greversrec.coverage);
		hdisgrvIO.setRider(greversrec.rider);
		hdisgrvIO.setPlanSuffix(greversrec.planSuffix);
		hdisgrvIO.setTranno(greversrec.tranno);
		hdisgrvIO.setFormat(hdisgrvrec);
		hdisgrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hdisgrvIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, hdisgrvIO);
		if (isNE(hdisgrvIO.getStatuz(),varcom.oK)
		&& isNE(hdisgrvIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdisgrvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(greversrec.chdrcoy,hdisgrvIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hdisgrvIO.getChdrnum())
		|| isNE(greversrec.life,hdisgrvIO.getLife())
		|| isNE(greversrec.coverage,hdisgrvIO.getCoverage())
		|| isNE(greversrec.rider,hdisgrvIO.getRider())
		|| isNE(greversrec.planSuffix,hdisgrvIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hdisgrvIO.getTranno())) {
			hdisgrvIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdisgrvIO.getStatuz(),varcom.endp))) {
			hdisIO.setParams(SPACES);
			hdisIO.setRrn(hdisgrvIO.getRrn());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				xxxxFatalError();
			}
			hdisIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				xxxxFatalError();
			}
			hdisIO.setChdrcoy(hdisgrvIO.getChdrcoy());
			hdisIO.setChdrnum(hdisgrvIO.getChdrnum());
			hdisIO.setLife(hdisgrvIO.getLife());
			hdisIO.setCoverage(hdisgrvIO.getCoverage());
			hdisIO.setRider(hdisgrvIO.getRider());
			hdisIO.setPlanSuffix(hdisgrvIO.getPlanSuffix());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			hdisIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)
			&& isNE(hdisIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdisIO.getParams());
				xxxxFatalError();
			}
			if (isEQ(hdisIO.getStatuz(),varcom.endp)
			|| isNE(hdisIO.getChdrcoy(),hdisgrvIO.getChdrcoy())
			|| isNE(hdisIO.getChdrnum(),hdisgrvIO.getChdrnum())
			|| isNE(hdisIO.getLife(),hdisgrvIO.getLife())
			|| isNE(hdisIO.getCoverage(),hdisgrvIO.getCoverage())
			|| isNE(hdisIO.getRider(),hdisgrvIO.getRider())
			|| isNE(hdisIO.getPlanSuffix(),hdisgrvIO.getPlanSuffix())) {
				hdisgrvIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(hdisIO.getValidflag(),"2")) {
					hdisgrvIO.setStatuz(hl15);
					syserrrec.params.set(hdisgrvIO.getParams());
					xxxxFatalError();
				}
				hdisIO.setValidflag("1");
				hdisIO.setFormat(hdisrec);
				hdisIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, hdisIO);
				if (isNE(hdisIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdisIO.getParams());
					xxxxFatalError();
				}
				hdisgrvIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hdisgrvIO);
				if (isNE(hdisgrvIO.getStatuz(),varcom.oK)
				&& isNE(hdisgrvIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hdisgrvIO.getParams());
					xxxxFatalError();
				}
				if (isNE(hdisgrvIO.getChdrcoy(),greversrec.chdrcoy)
				|| isNE(hdisgrvIO.getChdrnum(),greversrec.chdrnum)
				|| isNE(hdisgrvIO.getLife(),greversrec.life)
				|| isNE(hdisgrvIO.getCoverage(),greversrec.coverage)
				|| isNE(hdisgrvIO.getRider(),greversrec.rider)
				|| isNE(hdisgrvIO.getPlanSuffix(),greversrec.planSuffix)
				|| isNE(hdisgrvIO.getTranno(),greversrec.tranno)) {
					hdisgrvIO.setStatuz(varcom.endp);
				}
			}
		}

	}
}
