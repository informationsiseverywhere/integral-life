/******************************************************************************
 * File Name 		: Hcsdpf.java
 * Author			: smalchi2
 * Creation Date	: 02 January 2017
 * Project			: Integral Life
 * Description		: The Model Class for HCSDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Hcsdpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "HCSDPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String validflag;
	private Integer tranno;
	private Integer effdate;
	private String zdivopt;
	private String zcshdivmth;
	private String paycoy;
	private String payclt;
	private String paymth;
	private String facthous;
	private String bankkey;
	private String bankacckey;
	private String paycurr;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	public Hcsdpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public String getValidflag(){
		return this.validflag;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public String getZdivopt(){
		return this.zdivopt;
	}
	public String getZcshdivmth(){
		return this.zcshdivmth;
	}
	public String getPaycoy(){
		return this.paycoy;
	}
	public String getPayclt(){
		return this.payclt;
	}
	public String getPaymth(){
		return this.paymth;
	}
	public String getFacthous(){
		return this.facthous;
	}
	public String getBankkey(){
		return this.bankkey;
	}
	public String getBankacckey(){
		return this.bankacckey;
	}
	public String getPaycurr(){
		return this.paycurr;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setValidflag( String validflag ){
		 this.validflag = validflag;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setZdivopt( String zdivopt ){
		 this.zdivopt = zdivopt;
	}
	public void setZcshdivmth( String zcshdivmth ){
		 this.zcshdivmth = zcshdivmth;
	}
	public void setPaycoy( String paycoy ){
		 this.paycoy = paycoy;
	}
	public void setPayclt( String payclt ){
		 this.payclt = payclt;
	}
	public void setPaymth( String paymth ){
		 this.paymth = paymth;
	}
	public void setFacthous( String facthous ){
		 this.facthous = facthous;
	}
	public void setBankkey( String bankkey ){
		 this.bankkey = bankkey;
	}
	public void setBankacckey( String bankacckey ){
		 this.bankacckey = bankacckey;
	}
	public void setPaycurr( String paycurr ){
		 this.paycurr = paycurr;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("EFFDATE:		");
		output.append(getEffdate());
		output.append("\r\n");
		output.append("ZDIVOPT:		");
		output.append(getZdivopt());
		output.append("\r\n");
		output.append("ZCSHDIVMTH:		");
		output.append(getZcshdivmth());
		output.append("\r\n");
		output.append("PAYCOY:		");
		output.append(getPaycoy());
		output.append("\r\n");
		output.append("PAYCLT:		");
		output.append(getPayclt());
		output.append("\r\n");
		output.append("PAYMTH:		");
		output.append(getPaymth());
		output.append("\r\n");
		output.append("FACTHOUS:		");
		output.append(getFacthous());
		output.append("\r\n");
		output.append("BANKKEY:		");
		output.append(getBankkey());
		output.append("\r\n");
		output.append("BANKACCKEY:		");
		output.append(getBankacckey());
		output.append("\r\n");
		output.append("PAYCURR:		");
		output.append(getPaycurr());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
