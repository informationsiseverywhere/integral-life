package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdpxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:28
 * Class transformed from HDPXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdpxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 24;
	public FixedLengthStringData hdpxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hdpxpfRecord = hdpxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hdpxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hdpxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hdpxrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hdpxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hdpxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hdpxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hdpxrec);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(hdpxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HdpxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HdpxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HdpxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HdpxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdpxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HdpxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdpxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HDPXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"ZDIVOPT, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     zdivopt,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		zdivopt.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHdpxrec() {
  		return hdpxrec;
	}

	public FixedLengthStringData getHdpxpfRecord() {
  		return hdpxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHdpxrec(what);
	}

	public void setHdpxrec(Object what) {
  		this.hdpxrec.set(what);
	}

	public void setHdpxpfRecord(Object what) {
  		this.hdpxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hdpxrec.getLength());
		result.set(hdpxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}