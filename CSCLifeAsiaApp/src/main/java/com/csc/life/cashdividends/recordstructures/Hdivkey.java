package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:33
 * Description:
 * Copybook name: HDIVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivKey = new FixedLengthStringData(64).isAPartOf(hdivFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivKey, 0);
  	public FixedLengthStringData hdivChdrnum = new FixedLengthStringData(8).isAPartOf(hdivKey, 1);
  	public FixedLengthStringData hdivLife = new FixedLengthStringData(2).isAPartOf(hdivKey, 9);
  	public FixedLengthStringData hdivCoverage = new FixedLengthStringData(2).isAPartOf(hdivKey, 11);
  	public FixedLengthStringData hdivRider = new FixedLengthStringData(2).isAPartOf(hdivKey, 13);
  	public PackedDecimalData hdivPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hdivKey, 15);
  	public PackedDecimalData hdivPuAddNbr = new PackedDecimalData(3, 0).isAPartOf(hdivKey, 18);
  	public PackedDecimalData hdivEffdate = new PackedDecimalData(8, 0).isAPartOf(hdivKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(hdivKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}