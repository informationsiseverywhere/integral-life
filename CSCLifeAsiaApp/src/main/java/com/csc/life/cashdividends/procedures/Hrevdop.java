/*
 * File: Hrevdop.java
 * Date: 29 August 2009 22:55:22
 * Author: Quipoz Limited
 *
 * Class transformed from HREVDOP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisrevTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivropTableDAM;
import com.csc.life.cashdividends.tablestructures.Th500rec;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      -------------------------------------------
*          DIVIDEND OPTION PROCESSING REVERSAL
*      -------------------------------------------
*
*  This subroutine is called from the Full Contract Reversal
*  program REVGENAT via Table T6661. The parameters passed in
*  is via the REVERSEREC copybook.
*
*  The routine is used to reverse the processing done in BH526.
*
*  Transaction records HDIV are reversed.  Current HDIS records
*  if they are not yet printed, can be deleted.  Current HDIS
*  are deleted and validflag '2' records are re-instated.
*  Also re-instate CHDRLIF but keeping all minor alterations
*  made in between the reversal period.
*
*  The routine is driven by :
*    - The Company to which this Contract belongs.
*    - The Contract Header Number.
*    - The Transaction Number used in the original
*      cash dividend option processing transaction.
*
*  PROCESSING
*  ----------
*
*  - Get Today's date.
*
*  - Read the Contract Header details.
*
*  - Get the Transaction Code description from T1688 using
*    Transaction Code passed in through Linkage.
*
*  - Locate HDIV records and delete them via logical HDIVROP.
*
*  - Locate HDIS records and delete the current validflag 1's,
*    re-instate validflag 2 to 1 with logical HDISREV.
*
*  - Reinstate CHDR via CHDRLIF and keeping minor alterations
*    by calling CHDRMNA.
*
*  MAIN PROCESS.
*  -------------
*
*  BEGH HDIVROP.
*  PROCESS UNTIL HDIVROP-STATUZ = ENDP
*  - DELET HDIVROP
*  - Post a new HDIV with :
*
*              Option Processed TRANNO = 0
*
*  END OF HDIVROP PROCESSING LOOP.
*
*  BEGH HDISREV.
*  PROCESS UNTIL HDISREV-STATUZ = ENDP
*  - DELTD HDISREV but keep the LAST-STMT-DATE, DIVD-STMT-NO,
*    BAL-AT-STMT-DATE
*  - Read next HDISREV with :
*
*         if this is not a VALIDFLAG 2, something wrong, exit
*         else,WRITD HDIS with
*              VALIDFLAG          = '1'
*              LAST-STMT-NO       = stored
*              LAST-STMT-DATE     = stored
*              BAL-AT-STMT-DATE   = stored
*
*  END OF HDISREV PROCESSING LOOP.
*
*  UPDATE CHDR.
*  - Read hold CHDRLIF
*  - Read CHDRMNA for the minor alteration details
*  - DELET CHDRLIF
*  - Read CHDRLIF
*  - Set fields in CHDRMNA to CHDRLIF, reset validflag to '1',
*    currfrom and currto date.
*  - REWRT CHDRLIF
*
***********************************************************************
* </pre>
*/
public class Hrevdop extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private PackedDecimalData wsaaStmtNo = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaStmtDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBalance = new PackedDecimalData(17, 2);
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String hcsdrec = "HCSDREC";
	private String hdisrec = "HDISREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String chdrmnarec = "CHDRMNAREC";
	private String hdivroprec = "HDIVROPREC";
	private String hdisrevrec = "HDISREVREC";
		/* ERRORS */
	private String hl14 = "HL14";
	private String hl15 = "HL15";
		/* TABLES */
	private String t1688 = "T1688";
	private String th500 = "TH500";
		/*ACCOUNT MOVEMENTS LOGICAL VIEW FOR REVER*/
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Minor Alterations Contract Header*/
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Cash Dividend Alloc Summary (Reversal)*/
	private HdisrevTableDAM hdisrevIO = new HdisrevTableDAM();
		/*Dividend Option Processing Reversal*/
	private HdivropTableDAM hdivropIO = new HdivropTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private Th500rec th500rec = new Th500rec();
	private Varcom varcom = new Varcom();
	private Itemkey wsaaItemkey = new Itemkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit9090
	}

	public Hrevdop() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline010()
	{
		init010();
		para020();
	}

protected void init010()
	{
		reverserec.statuz.set(varcom.oK);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(reverserec.company);
		wsaaItemkey.itemItemtabl.set(t1688);
		wsaaItemkey.itemItemitem.set(reverserec.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(reverserec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
	}

protected void para020()
	{
		processHdiv1000();
		processHdis2000();
		processChdr4000();
		processAcmv5000();
		/*EXIT*/
		exitProgram();
	}

protected void processHdiv1000()
	{
		readhHdivrop1010();
	}

protected void readhHdivrop1010()
	{
		hdivropIO.setStatuz(varcom.oK);
		while ( !(isEQ(hdivropIO.getStatuz(),varcom.mrnf))) {
			hdivropIO.setDataKey(SPACES);
			hdivropIO.setChdrcoy(reverserec.company);
			hdivropIO.setChdrnum(reverserec.chdrnum);
			hdivropIO.setDivdOptprocTranno(reverserec.tranno);
			hdivropIO.setFormat(hdivroprec);
			hdivropIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hdivropIO);
			if (isNE(hdivropIO.getStatuz(),varcom.oK)
			&& isNE(hdivropIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(hdivropIO.getParams());
				fatalError9000();
			}
			if (isEQ(hdivropIO.getStatuz(),varcom.oK)) {
				hdivropIO.setFunction(varcom.delet);
				SmartFileCode.execute(appVars, hdivropIO);
				if (isNE(hdivropIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivropIO.getParams());
					fatalError9000();
				}
				hdivropIO.setDivdOptprocTranno(0);
				hdivropIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivropIO);
				if (isNE(hdivropIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivropIO.getParams());
					fatalError9000();
				}
			}
		}

	}

protected void processHdis2000()
	{
		readhHdisrev2010();
	}

protected void readhHdisrev2010()
	{
		hdisrevIO.setDataKey(SPACES);
		hdisrevIO.setChdrcoy(reverserec.company);
		hdisrevIO.setChdrnum(reverserec.chdrnum);
		hdisrevIO.setTranno(reverserec.tranno);
		hdisrevIO.setFormat(hdisrevrec);
		hdisrevIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdisrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, hdisrevIO);
		if (isNE(hdisrevIO.getStatuz(),varcom.oK)
		&& isNE(hdisrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdisrevIO.getParams());
			fatalError9000();
		}
		if (isNE(reverserec.company,hdisrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,hdisrevIO.getChdrnum())
		|| isNE(reverserec.tranno,hdisrevIO.getTranno())) {
			hdisrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdisrevIO.getStatuz(),varcom.endp))) {
			readHcsd3000();
			hdisIO.setParams(SPACES);
			hdisIO.setRrn(hdisrevIO.getRrn());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				fatalError9000();
			}
			wsaaStmtNo.set(hdisIO.getDivdStmtNo());
			wsaaStmtDate.set(hdisIO.getDivdStmtDate());
			wsaaBalance.set(hdisIO.getBalAtStmtDate());
			hdisIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				fatalError9000();
			}
			hdisIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)
			&& isNE(hdisIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdisIO.getParams());
				fatalError9000();
			}
			if (isEQ(hdisIO.getStatuz(),varcom.endp)
			|| isNE(hdisIO.getChdrcoy(),hdisrevIO.getChdrcoy())
			|| isNE(hdisIO.getChdrnum(),hdisrevIO.getChdrnum())
			|| isNE(hdisIO.getLife(),hdisrevIO.getLife())
			|| isNE(hdisIO.getCoverage(),hdisrevIO.getCoverage())
			|| isNE(hdisIO.getRider(),hdisrevIO.getRider())
			|| isNE(hdisIO.getPlanSuffix(),reverserec.planSuffix)) {
				hdisrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(hdisIO.getValidflag(),"2")) {
					hdisrevIO.setStatuz(hl15);
					syserrrec.params.set(hdisrevIO.getParams());
					fatalError9000();
				}
				hdisIO.setValidflag("1");
				hdisIO.setDivdStmtNo(wsaaStmtNo);
				hdisIO.setDivdStmtDate(wsaaStmtDate);
				hdisIO.setBalAtStmtDate(wsaaBalance);
				hdisIO.setFormat(hdisrec);
				hdisIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, hdisIO);
				if (isNE(hdisIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdisIO.getParams());
					fatalError9000();
				}
				hdisrevIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hdisrevIO);
				if (isNE(hdisrevIO.getStatuz(),varcom.oK)
				&& isNE(hdisrevIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hdisrevIO.getParams());
					fatalError9000();
				}
				if (isNE(hdisrevIO.getChdrcoy(),reverserec.company)
				|| isNE(hdisrevIO.getChdrnum(),reverserec.chdrnum)
				|| isNE(hdisrevIO.getTranno(),reverserec.tranno)) {
					hdisrevIO.setStatuz(varcom.endp);
				}
			}
		}

	}

protected void readHcsd3000()
	{
		readHcsdPara3000();
	}

protected void readHcsdPara3000()
	{
		hcsdIO.setChdrcoy(reverserec.company);
		hcsdIO.setChdrnum(reverserec.chdrnum);
		hcsdIO.setLife(hdisrevIO.getLife());
		hcsdIO.setCoverage(hdisrevIO.getCoverage());
		hcsdIO.setRider(hdisrevIO.getRider());
		hcsdIO.setPlanSuffix(hdisrevIO.getPlanSuffix());
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			fatalError9000();
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(th500);
		itemIO.setItemitem(hcsdIO.getZdivopt());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		th500rec.th500Rec.set(itemIO.getGenarea());
		if (isNE(th500rec.trevsub,SPACES)) {
			reverserec.life.set(hcsdIO.getLife());
			reverserec.coverage.set(hcsdIO.getCoverage());
			reverserec.rider.set(hcsdIO.getRider());
			reverserec.planSuffix.set(hcsdIO.getPlanSuffix());
			callProgram(th500rec.trevsub, reverserec.reverseRec);
			if (isNE(reverserec.statuz,varcom.oK)) {
				syserrrec.statuz.set(reverserec.statuz);
				fatalError9000();
			}
		}
	}

protected void processChdr4000()
	{
		chdrRead4010();
	}

protected void chdrRead4010()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);*/

		axesChdr6000();
		if (isNE(chdrlifIO.getValidflag(),"1")) {
			chdrlifIO.setStatuz(hl14);
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		chdrmnaIO.setChdrcoy(chdrlifIO.getChdrcoy());
		chdrmnaIO.setChdrnum(chdrlifIO.getChdrnum());
		chdrmnaIO.setFormat(chdrmnarec);
		chdrmnaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError9000();
		}
		chdrlifIO.setFunction(varcom.deltd);
		axesChdr6000();
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		axesChdr6000();
		if (isNE(chdrlifIO.getValidflag(),"2")) {
			chdrlifIO.setStatuz(hl15);
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setCownpfx(chdrmnaIO.getCownpfx());
		chdrlifIO.setCownnum(chdrmnaIO.getCownnum());
		chdrlifIO.setCowncoy(chdrmnaIO.getCowncoy());
		chdrlifIO.setJownnum(chdrmnaIO.getJownnum());
		chdrlifIO.setPayrnum(chdrmnaIO.getPayrnum());
		chdrlifIO.setAsgnpfx(chdrmnaIO.getAsgnpfx());
		chdrlifIO.setAsgncoy(chdrmnaIO.getAsgncoy());
		chdrlifIO.setAsgnnum(chdrmnaIO.getAsgnnum());
		chdrlifIO.setDesppfx(chdrmnaIO.getDesppfx());
		chdrlifIO.setDespcoy(chdrmnaIO.getDespcoy());
		chdrlifIO.setDespnum(chdrmnaIO.getDespnum());
		chdrlifIO.setAgntpfx(chdrmnaIO.getAgntpfx());
		chdrlifIO.setAgntcoy(chdrmnaIO.getAgntcoy());
		chdrlifIO.setAgntnum(chdrmnaIO.getAgntnum());
		chdrlifIO.setBillsupr(chdrmnaIO.getBillsupr());
		chdrlifIO.setBillspfrom(chdrmnaIO.getBillspfrom());
		chdrlifIO.setBillspto(chdrmnaIO.getBillspto());
		chdrlifIO.setNotssupr(chdrmnaIO.getNotssupr());
		chdrlifIO.setNotsspfrom(chdrmnaIO.getNotsspfrom());
		chdrlifIO.setNotsspto(chdrmnaIO.getNotsspto());
		chdrlifIO.setRnwlsupr(chdrmnaIO.getRnwlsupr());
		chdrlifIO.setRnwlspfrom(chdrmnaIO.getRnwlspfrom());
		chdrlifIO.setRnwlspto(chdrmnaIO.getRnwlspto());
		chdrlifIO.setCommsupr(chdrmnaIO.getCommsupr());
		chdrlifIO.setCommspfrom(chdrmnaIO.getCommspfrom());
		chdrlifIO.setCommspto(chdrmnaIO.getCommspto());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.rewrt);
		axesChdr6000();
	}

protected void processAcmv5000()
	{
		acmvRead5010();
	}

protected void acmvRead5010()
	{
		acmvrevIO.setDataKey(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalError9000();
		}
		if (isNE(acmvrevIO.getRldgcoy(),reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(),reverserec.tranno)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvRecs5020();
		}

	}

protected void reverseAcmvRecs5020()
	{
		/*START*/
		revAcmv5500();
		acmvrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalError9000();
		}
		if (isNE(acmvrevIO.getRldgcoy(),reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(),reverserec.tranno)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void revAcmv5500()
	{
		acmv1Write5510();
	}

protected void acmv1Write5510()
	{
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.rldgcoy.set(reverserec.company);
		lifacmvrec.rdocnum.set(reverserec.chdrnum);
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.batccoy.set(reverserec.company);
		lifacmvrec.batcbrn.set(reverserec.batcbrn);
		lifacmvrec.batcactyr.set(reverserec.batcactyr);
		lifacmvrec.batcactmn.set(reverserec.batcactmn);
		lifacmvrec.batctrcde.set(reverserec.batctrcde);
		lifacmvrec.batcbatch.set(reverserec.batcbatch);
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		setPrecision(acmvrevIO.getOrigamt(), 2);
		acmvrevIO.setOrigamt(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.origamt.set(acmvrevIO.getOrigamt());
		lifacmvrec.rcamt.set(acmvrevIO.getOrigamt());
		setPrecision(acmvrevIO.getAcctamt(), 2);
		acmvrevIO.setAcctamt(mult(acmvrevIO.getAcctamt(),-1));
		lifacmvrec.acctamt.set(acmvrevIO.getAcctamt());
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(reverserec.effdate1);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void axesChdr6000()
	{
		/*START*/
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		try {
			error9010();
		}
		catch (GOTOException e){
		}
		finally{
			exit9090();
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		reverserec.statuz.set(varcom.bomb);
	}

protected void exit9090()
	{
		exitProgram();
	}
}
