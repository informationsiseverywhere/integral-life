/*
 * File: Hcvdthp.java
 * Date: 29 August 2009 22:52:14
 * Author: Quipoz Limited
 * 
 * Class transformed from HCVDTHP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivcshTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  HCVDTHP - Cash Dividend Death Claim Processing Subroutine
*  ----------------------------------------------------------
*
*  This subroutine is called from various programs via T6598.
*  It is responsible for processing the death claim of a         e
*  traditional cash dividend contract.
*  This involves writing offset entries to withdraw the cash
*  dividend and interest, writing accounting entries for each
*  type of withdraw within the death claim, repay existing policy
*  loans, place proceeds to contract suspense and change the
*  contract status.
*
*  The following linkage information is passed to this
*  subroutine:-
*             - DEATHREC
*
*  Processing
*  ----------
*
*  From the data passed in the linkage section, check whether the
*  CDTH-FIELD-TYPE is 'S', 'P', 'D', 'I'.
*  Depending on what it is, perform one of the following:-
*
*  If CDTH-FIELD-TYPE = 'S' (sum insured):-
*     post to the sub-accounts-
*     (01),(05) If not Component Level Accounting.
*     (03),(06) If Component Level Accounting.
*
*  If CDTH-FIELD-TYPE = 'P' (Paid up Addition SI):-
*     post to the sub-accounts-
*     (02),(05) If not Component Level Accounting.
*     (04),(06) If Component Level Accounting.
*
*  If CDTH-FIELD-TYPE = 'D' (Accumulated Dividend):-
*     post to the sub-accounts-
*     (12),(05) If not Component Level Accounting.
*     (12),(06) If Component Level Accounting.
*
*  If CDTH-FIELD-TYPE = 'I' (O/S interest):-
*     post to the sub-accounts-
*     (13),(05) If not Component Level Accounting.
*     (15),(06) If Component Level Accounting.
*
*  The sub-account entries are found on T5645, accessed by  this
*  program number.
*
*      If the amounts are not zero, then call 'LIFACMV' ("cash"
*      posting subroutine) to post to the correct account.  The
*      posting required is defined in the appropriate line no. on
*      the T5645 table entry.
*      The linkage area LIFACMVREC will need to be set up for the
*      call to 'LIFACMV'.
*
*  When all records have been processed, make postings for
*  loans.
*  This is done by first calling TOTLOAN to bring
*  the interest on loans for this contract up to date.
*  ACMVs are then required for paying off loans. For each of
*  relevant entries on T5645, call LOANPYMT with the outstanding
*  claim amount until all loan is 'allocated' or
*  all entries have been processed and some of the claim
*  value remains.
*  Then write an ACMV record to suspense for the residual of
*  the value to be claimed after the above.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hcvdthp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "HCVDTHP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaNewInt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHdisOsInt = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaHdisRead = new FixedLengthStringData(1).init(SPACES);
	private Validator hdisAlreadyRead = new Validator(wsaaHdisRead, "Y");
		/* ERRORS */
	private String e308 = "E308";
		/* TABLES */
	private String t5645 = "T5645";
	private String t5679 = "T5679";
	private String t5688 = "T5688";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String hcsdrec = "HCSDREC";
	private String hpuarec = "HPUAREC";
	private String hdisrec = "HDISREC";
	private String hdivrec = "HDIVREC";
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaFirstPost = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaSecondPost = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private ZonedDecimalData wsaaRldgPlansuffix = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgPlanSuffix, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaT5645 = new FixedLengthStringData(105);
	private FixedLengthStringData[] wsaaStoredT5645 = FLSArrayPartOfStructure(5, 21, wsaaT5645, 0);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	private FixedLengthStringData wsbbT5645 = new FixedLengthStringData(630);
	private FixedLengthStringData[] wsbbStoredT5645 = FLSArrayPartOfStructure(30, 21, wsbbT5645, 0);
	private ZonedDecimalData[] wsbbT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsbbStoredT5645, 0);
	private FixedLengthStringData[] wsbbT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsbbStoredT5645, 2);
	private FixedLengthStringData[] wsbbT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsbbStoredT5645, 16);
	private FixedLengthStringData[] wsbbT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsbbStoredT5645, 18);
	private FixedLengthStringData[] wsbbT5645Sign = FLSDArrayPartOfArrayStructure(1, wsbbStoredT5645, 20);

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	private PackedDecimalData wsaaNetVal = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private Cashedrec cashedrec = new Cashedrec();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	protected Dthcpy dthcpy = new Dthcpy();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Allocation Trans Details Logica*/
	private HdivTableDAM hdivIO = new HdivTableDAM();
		/*Paid Up Addition Coverage Details Logica*/
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
	private PackedDecimalData wsaaNextDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCurrOsInt = new PackedDecimalData(17, 2).init(0);
	Payrpf payrpf = null;
	private Itempf itempf = null;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	PayrpfDAO payrpfDao = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ZonedDecimalData wsaaSub3 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub4 = new ZonedDecimalData(2, 0).setUnsigned();
	private Th501rec th501rec = new Th501rec();
	private String hdivcshrec = "HDIVCSHREC";
	private String th501 = "TH501";
	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	private boolean cdivFlag = false;
	private boolean newPostFlag = false;


	public Hcvdthp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		dthcpy.deathRec = convertAndSetParam(dthcpy.deathRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		dthcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaTranTermid.set(SPACES);
		wsaaTranDate.set(ZERO);
		wsaaTranTime.set(ZERO);
		wsaaTranUser.set(ZERO);
		wsaaBatckey.set(dthcpy.batckey);
		cdivFlag = FeaConfg.isFeatureExist("2", "BTPRO015", appVars, "IT");
		readHcsd100();
		readTabT5645200();
		readTabT5679230();
		readTabT5688250();
		lifacmvrec1.origamt.set(dthcpy.actualVal);
		lifacmvrec1.tranno.set(dthcpy.tranno);
		if (isEQ(dthcpy.fieldType,"S")) {
			if (isEQ(t5688rec.comlvlacc,"Y")) {
				wsaaSub2.set(3);
				wsaaSecondPost.set(6);
				customerSpecificSuspAdjOprm();
				componentPosting400();
			}
			else {
				wsaaFirstPost.set(1);
				wsaaSecondPost.set(5);
				customerSpecificSuspAdjOprm();
				contractPosting300();
			}
		}
		else {
			if (isEQ(dthcpy.fieldType,"P")) {
				processHpua1000();
				if (isEQ(t5688rec.comlvlacc,"Y")) {
					wsaaSub2.set(4);
					wsaaSecondPost.set(6);
					componentPosting400();
				}
				else {
					wsaaFirstPost.set(2);
					wsaaSecondPost.set(5);
					contractPosting300();
				}
			}
			else {
				if (isEQ(dthcpy.fieldType,"D")) {
					processHdis2000();
					if(cdivFlag){
						compute(wsaaNextDividend, 2).set(sub(dthcpy.actualVal,wsaaTotDividend));			
						if(isNE(wsaaTotDividend,ZERO)){
							lifacmvrec1.origamt.set(wsaaTotDividend);
							dthcpy.actualVal.set(wsaaTotDividend);
							wsaaFirstPost.set(12);
							wsaaSecondPost.set(5);
							contractPosting300();
						}
						dthcpy.actualVal.set(wsaaNextDividend);
						lifacmvrec1.origamt.set(wsaaNextDividend);
						newPostFlag = true;
						wsaaSub3.set(18);
						wsaaSub4.set(4);
						contractPosting300();
						newPostFlag = false;
					}
					else{
					wsaaFirstPost.set(12);
					wsaaSecondPost.set(5);
					contractPosting300();
					}
				}
				else {
					if (isEQ(dthcpy.fieldType,"I")) {
						interest3000();
						if (isEQ(t5688rec.comlvlacc,"Y")) {
							wsaaSub2.set(15);
							wsaaSecondPost.set(6);
							componentPosting400();
						}
						else {
							wsaaFirstPost.set(13);
							wsaaSecondPost.set(5);
							contractPosting300();
						}
						if (isNE(wsaaNewInt,ZERO)) {
							lifacmvrec1.origamt.set(wsaaNewInt);
							dthcpy.actualVal.set(wsaaNewInt);
							if (isEQ(t5688rec.comlvlacc,"Y")) {
								wsaaSub2.set(17);
								wsaaSecondPost.set(6);
								componentPosting400();
							}
							else {
								wsaaFirstPost.set(16);
								wsaaSecondPost.set(5);
								contractPosting300();
							}
							compute(lifacmvrec1.origamt, 2).set(mult(wsaaNewInt,-1));
							if (isEQ(t5688rec.comlvlacc,"Y")) {
								lifacmvrec1.sacscode.set(wsbbT5645Sacscode[15]);
								lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[15]);
								lifacmvrec1.glcode.set(wsbbT5645Glmap[15]);
								lifacmvrec1.glsign.set(wsbbT5645Sign[15]);
								lifacmvrec1.contot.set(wsbbT5645Cnttot[15]);
							}
							else {
								lifacmvrec1.sacscode.set(wsbbT5645Sacscode[13]);
								lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[13]);
								lifacmvrec1.glcode.set(wsbbT5645Glmap[13]);
								lifacmvrec1.glsign.set(wsbbT5645Sign[13]);
								lifacmvrec1.contot.set(wsbbT5645Cnttot[13]);
							}
							wsaaJrnseq.add(1);
							lifacmvrec1.jrnseq.set(wsaaJrnseq);
							callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
							if (isNE(lifacmvrec1.statuz,varcom.oK)) {
								syserrrec.params.set(lifacmvrec1.lifacmvRec);
								fatalError9000();
							}
						}
						if(cdivFlag){
							if (isNE(wsaaCurrOsInt,ZERO)) {
								lifacmvrec1.origamt.set(wsaaCurrOsInt);
								dthcpy.actualVal.set(wsaaCurrOsInt);
								newPostFlag = true;
								wsaaSub3.set(21);
								wsaaSub4.set(5);
								if (isEQ(t5688rec.comlvlacc,"Y")) {		
									componentPosting400();
								}
								else {
									contractPosting300();
								}
								newPostFlag = false;
							}
							
						}
					}
				}
			}
		}
	}

protected void customerSpecificSuspAdjOprm() {
	
}

protected void exit090()
	{
		exitProgram();
	}

protected void readHcsd100()
	{
		hcsd100();
	}

protected void hcsd100()
	{
		hcsdIO.setParams(SPACES);
		hcsdIO.setChdrcoy(dthcpy.chdrChdrcoy);
		hcsdIO.setChdrnum(dthcpy.chdrChdrnum);
		hcsdIO.setLife(dthcpy.lifeLife);
		hcsdIO.setCoverage(dthcpy.covrCoverage);
		hcsdIO.setRider(dthcpy.covrRider);
		hcsdIO.setPlanSuffix(0);
		hcsdIO.setFunction(varcom.readr);
		hcsdIO.setFormat(hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)
		&& isNE(hcsdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			fatalError9000();
		}
	}

protected void readTabT5645200()
	{
		read231();
	}

protected void read231()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq("01");
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			wsbbT5645Cnttot[add(wsaaSub1,15).toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsbbT5645Glmap[add(wsaaSub1,15).toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsbbT5645Sacscode[add(wsaaSub1,15).toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsbbT5645Sacstype[add(wsaaSub1,15).toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsbbT5645Sign[add(wsaaSub1,15).toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			wsbbT5645Cnttot[wsaaSub1.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsbbT5645Glmap[wsaaSub1.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsbbT5645Sacscode[wsaaSub1.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsbbT5645Sacstype[wsaaSub1.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsbbT5645Sign[wsaaSub1.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		wsaaSub1.set(8);
		for (wsaaSub2.set(1); !(isGT(wsaaSub2,4)); wsaaSub2.add(1)){
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
			wsaaSub1.add(1);
		}
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(dthcpy.chdrChdrcoy);
		descIO.setLanguage(dthcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
	}

protected void readTabT5679230()
	{
		read1231();
	}

protected void read1231()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readTabT5688250()
	{
		read251();
	}

protected void read251()
	{
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(dthcpy.cnttype);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),dthcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(dthcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void contractPosting300()
	{
		/*PARA*/
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(dthcpy.chdrChdrnum);
		writeAcmv500();
		/*EXIT*/
	}

protected void componentPosting400()
	{
		para401();
	}

protected void para401()
	{
		covrenqIO.setChdrcoy(dthcpy.chdrChdrcoy);
		covrenqIO.setChdrnum(dthcpy.chdrChdrnum);
		covrenqIO.setLife(dthcpy.lifeLife);
		covrenqIO.setCoverage(dthcpy.covrCoverage);
		covrenqIO.setRider(dthcpy.covrRider);
		covrenqIO.setPlanSuffix(0);
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError9000();
		}
		lifacmvrec1.substituteCode[6].set(covrenqIO.getCrtable());
		wsaaRldgChdrnum.set(dthcpy.chdrChdrnum);
		wsaaRldgLife.set(dthcpy.lifeLife);
		wsaaRldgCoverage.set(dthcpy.covrCoverage);
		wsaaRldgRider.set(dthcpy.covrRider);
		wsaaRldgPlansuffix.set(covrenqIO.getPlanSuffix());
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		writeAcmv500();
	}

protected void writeAcmv500()
	{
	if (cdivFlag) {
		go505();
	}
	else{
		go501();
	}
		
	}

protected void go505()
{
	if(isEQ(lifacmvrec1.origamt,0) || isEQ(dthcpy.actualVal,0)){
		return;
	}
	lifacmvrec1.batckey.set(wsaaBatckey);
	lifacmvrec1.rdocnum.set(dthcpy.chdrChdrnum);
	lifacmvrec1.tranno.set(dthcpy.tranno);
	lifacmvrec1.trandesc.set(descIO.getLongdesc());
	lifacmvrec1.effdate.set(dthcpy.effdate);
	lifacmvrec1.origcurr.set(dthcpy.currcode);
	lifacmvrec1.genlcur.set(SPACES);
	lifacmvrec1.genlcoy.set(dthcpy.chdrChdrcoy);
	lifacmvrec1.crate.set(ZERO);
	wsaaNetVal.set(lifacmvrec1.origamt);
	wsaaTranTermid.set(dthcpy.termid);
	wsaaTranUser.set(dthcpy.user);
	wsaaTranTime.set(dthcpy.time);
	wsaaTranDate.set(dthcpy.date_var);
	allocateToLoans600();
	lifacmvrec1.rcamt.set(ZERO);
	lifacmvrec1.contot.set(ZERO);
	lifacmvrec1.rcamt.set(ZERO);
	lifacmvrec1.frcdate.set(ZERO);
	lifacmvrec1.transactionDate.set(ZERO);
	lifacmvrec1.transactionTime.set(ZERO);
	lifacmvrec1.user.set(ZERO);
	lifacmvrec1.termid.set(ZERO);
	lifacmvrec1.function.set("PSTW");
	if (!newPostFlag){
	if (isEQ(t5688rec.comlvlacc,"Y")) {
		lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaSub2.toInt()]);
		lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaSub2.toInt()]);
		lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaSub2.toInt()]);
		lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaSub2.toInt()]);
		lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaSub2.toInt()]);
	}
	else {
		lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaFirstPost.toInt()]);
		lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaFirstPost.toInt()]);
		lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaFirstPost.toInt()]);
		lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaFirstPost.toInt()]);
		lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaFirstPost.toInt()]);
	}
	if (isEQ(dthcpy.fieldType,"D")
	&& isNE(hdisIO.getSacscode(),SPACES)
	&& isNE(hdisIO.getSacstyp(),SPACES)) {
		lifacmvrec1.sacscode.set(hdisIO.getSacscode());
		lifacmvrec1.sacstyp.set(hdisIO.getSacstyp());
	}
	}
	lifacmvrec1.rldgcoy.set(dthcpy.chdrChdrcoy);
	lifacmvrec1.acctamt.set(ZERO);
	lifacmvrec1.postyear.set(SPACES);
	lifacmvrec1.postmonth.set(SPACES);
	wsaaTranno.set(dthcpy.tranno);
	lifacmvrec1.tranref.set(wsaaTranno);
	lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec1.substituteCode[1].set(dthcpy.cnttype);
	lifacmvrec1.origamt.set(dthcpy.actualVal);
	lifacmvrec1.termid.set(dthcpy.termid);
	lifacmvrec1.user.set(dthcpy.user);
	lifacmvrec1.transactionTime.set(dthcpy.time);
	lifacmvrec1.transactionDate.set(dthcpy.date_var);
	if (!newPostFlag){
	wsaaJrnseq.add(1);
	lifacmvrec1.jrnseq.set(wsaaJrnseq);
	if (isNE(lifacmvrec1.origamt,0)) {
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
	}
	lifacmvrec1.origamt.set(wsaaNetVal);
	lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaSecondPost.toInt()]);
	lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaSecondPost.toInt()]);
	lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaSecondPost.toInt()]);
	lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaSecondPost.toInt()]);
	lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaSecondPost.toInt()]);
	wsaaJrnseq.add(1);
	lifacmvrec1.jrnseq.set(wsaaJrnseq);
	if (isNE(lifacmvrec1.origamt,0)) {
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
	}
	}
	if (newPostFlag){
		for (wsaaSub1.set(1); !(isGTE(wsaaSub1,wsaaSub4)) ; wsaaSub1.add(1)){
				lifacmvrec1.origamt.set(wsaaNetVal);
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaSub3.toInt()]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaSub3.toInt()]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaSub3.toInt()]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaSub3.toInt()]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaSub3.toInt()]);
				wsaaJrnseq.add(1);
				lifacmvrec1.jrnseq.set(wsaaJrnseq);
				if (isNE(lifacmvrec1.origamt, 0)) {
					callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
					if (isNE(lifacmvrec1.statuz, varcom.oK)) {
						syserrrec.params.set(lifacmvrec1.lifacmvRec);
						fatalError9000();
					}
				}
				wsaaSub3.add(1);
		}
	}
}


protected void go501()
	{
		lifacmvrec1.batckey.set(wsaaBatckey);
		lifacmvrec1.rdocnum.set(dthcpy.chdrChdrnum);
		lifacmvrec1.tranno.set(dthcpy.tranno);
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(dthcpy.effdate);
		lifacmvrec1.origcurr.set(dthcpy.currcode);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec1.crate.set(ZERO);
		wsaaNetVal.set(lifacmvrec1.origamt);
		wsaaTranTermid.set(dthcpy.termid);
		wsaaTranUser.set(dthcpy.user);
		wsaaTranTime.set(dthcpy.time);
		wsaaTranDate.set(dthcpy.date_var);
		allocateToLoans600();
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.termid.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaSub2.toInt()]);
			lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaSub2.toInt()]);
			lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaSub2.toInt()]);
			lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaSub2.toInt()]);
			lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaSub2.toInt()]);
		}
		else {
			lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaFirstPost.toInt()]);
			lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaFirstPost.toInt()]);
			lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaFirstPost.toInt()]);
			lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaFirstPost.toInt()]);
			lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaFirstPost.toInt()]);
		}
		if (isEQ(dthcpy.fieldType,"D")
		&& isNE(hdisIO.getSacscode(),SPACES)
		&& isNE(hdisIO.getSacstyp(),SPACES)) {
			lifacmvrec1.sacscode.set(hdisIO.getSacscode());
			lifacmvrec1.sacstyp.set(hdisIO.getSacstyp());
		}
		lifacmvrec1.rldgcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		wsaaTranno.set(dthcpy.tranno);
		lifacmvrec1.tranref.set(wsaaTranno);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(dthcpy.cnttype);
		lifacmvrec1.origamt.set(dthcpy.actualVal);
		lifacmvrec1.termid.set(dthcpy.termid);
		lifacmvrec1.user.set(dthcpy.user);
		lifacmvrec1.transactionTime.set(dthcpy.time);
		lifacmvrec1.transactionDate.set(dthcpy.date_var);
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		if (isNE(lifacmvrec1.origamt,0)) {
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				fatalError9000();
			}
		}
		lifacmvrec1.origamt.set(wsaaNetVal);
		lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaSecondPost.toInt()]);
		lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaSecondPost.toInt()]);
		lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaSecondPost.toInt()]);
		lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaSecondPost.toInt()]);
		lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaSecondPost.toInt()]);
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		if (isNE(lifacmvrec1.origamt,0)) {
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				fatalError9000();
			}
		}	
	}

protected void allocateToLoans600()
	{
		start600();
	}

protected void start600()
	{
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(lifacmvrec1.batckey);
		cashedrec.doctNumber.set(lifacmvrec1.rdocnum);
		cashedrec.doctCompany.set(lifacmvrec1.batccoy);
		cashedrec.trandate.set(lifacmvrec1.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.transeq.set(wsaaJrnseq);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(lifacmvrec1.origcurr);
		cashedrec.dissrate.set(lifacmvrec1.crate);
		cashedrec.trandesc.set(lifacmvrec1.trandesc);
		cashedrec.genlCompany.set(lifacmvrec1.genlcoy);
		cashedrec.genlCurrency.set(lifacmvrec1.genlcur);
		cashedrec.chdrcoy.set(dthcpy.chdrChdrcoy);
		cashedrec.chdrnum.set(dthcpy.chdrChdrnum);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(dthcpy.chdrChdrcoy);
		wsaaTranEntity.set(dthcpy.chdrChdrnum);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.tranno.set(lifacmvrec1.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.language.set(dthcpy.language);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)
		|| isEQ(wsaaNetVal,0)); wsaaSub1.add(1)){
			loans620();
		}
		wsaaJrnseq.set(cashedrec.transeq);
	}

protected void loans620()
	{
		/*START*/
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		cashedrec.genlAccount.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		cashedrec.origamt.set(wsaaNetVal);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			fatalError9000();
		}
		wsaaNetVal.set(cashedrec.docamt);
		/*EXIT*/
	}

protected void processHpua1000()
	{
		hpua1000();
	}

protected void hpua1000()
	{
		hpuaIO.setParams(SPACES);
		hpuaIO.setChdrcoy(dthcpy.chdrChdrcoy);
		hpuaIO.setChdrnum(dthcpy.chdrChdrnum);
		hpuaIO.setLife(dthcpy.lifeLife);
		hpuaIO.setCoverage(dthcpy.covrCoverage);
		hpuaIO.setRider(dthcpy.covrRider);
		hpuaIO.setPlanSuffix(0);
		hpuaIO.setPuAddNbr(0);
		hpuaIO.setFormat(hpuarec);
		hpuaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			syserrrec.statuz.set(hpuaIO.getStatuz());
			fatalError9000();
		}
		if (isNE(hpuaIO.getChdrcoy(),dthcpy.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(),dthcpy.chdrChdrnum)
		|| isNE(hpuaIO.getLife(),dthcpy.lifeLife)
		|| isNE(hpuaIO.getCoverage(),dthcpy.covrCoverage)
		|| isNE(hpuaIO.getRider(),dthcpy.covrRider)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
			hpuaIO.setValidflag("2");
			hpuaIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hpuaIO.getParams());
				syserrrec.statuz.set(hpuaIO.getStatuz());
				fatalError9000();
			}
			hpuaIO.setValidflag("1");
			hpuaIO.setTranno(dthcpy.tranno);
			hpuaIO.setRstatcode(t5679rec.setCovRiskStat);
			hpuaIO.setPstatcode(t5679rec.setCovPremStat);
			hpuaIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hpuaIO.getParams());
				syserrrec.statuz.set(hpuaIO.getStatuz());
				fatalError9000();
			}
			setPrecision(hpuaIO.getPuAddNbr(), 0);
			hpuaIO.setPuAddNbr(add(hpuaIO.getPuAddNbr(),1));
			hpuaIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)
			&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hpuaIO.getParams());
				syserrrec.statuz.set(hpuaIO.getStatuz());
				fatalError9000();
			}
			if (isNE(hpuaIO.getChdrcoy(),dthcpy.chdrChdrcoy)
			|| isNE(hpuaIO.getChdrnum(),dthcpy.chdrChdrnum)
			|| isNE(hpuaIO.getLife(),dthcpy.lifeLife)
			|| isNE(hpuaIO.getCoverage(),dthcpy.covrCoverage)
			|| isNE(hpuaIO.getRider(),dthcpy.covrRider)) {
				hpuaIO.setStatuz(varcom.endp);
			}
		}
		
	}

protected void processHdis2000()
	{
		hdis2000();
	}

protected void hdis2000()
	{
		hdisIO.setParams(SPACES);
		hdisIO.setChdrcoy(dthcpy.chdrChdrcoy);
		hdisIO.setChdrnum(dthcpy.chdrChdrnum);
		hdisIO.setLife(dthcpy.lifeLife);
		hdisIO.setCoverage(dthcpy.covrCoverage);
		hdisIO.setRider(dthcpy.covrRider);
		hdisIO.setPlanSuffix(0);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hdisIO);
		if(cdivFlag){
			if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
				wsaaHdisOsInt.set(0);
				wsaaHdisRead.set("Y");
				wsaaTotDividend.set(0);
				return;
			}
			else{
				if (isNE(hdisIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdisIO.getParams());
					syserrrec.statuz.set(hdisIO.getStatuz());
					fatalError9000();
				}
			}
		}
		else{
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError9000();
		}
		}
		if(cdivFlag){
			calcPreviousDiv();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError9000();
		}
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(dthcpy.chdrChdrcoy);
		hdivIO.setChdrnum(dthcpy.chdrChdrnum);
		hdivIO.setLife(dthcpy.lifeLife);
		hdivIO.setCoverage(dthcpy.covrCoverage);
		hdivIO.setRider(dthcpy.covrRider);
		hdivIO.setPlanSuffix(hdisIO.getPlanSuffix());
		hdivIO.setTranno(dthcpy.tranno);
		hdivIO.setEffdate(dthcpy.effdate);
		hdivIO.setDivdAllocDate(dthcpy.effdate);
		hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
		hdivIO.setCntcurr(dthcpy.contractCurr);
		setPrecision(hdivIO.getDivdAmount(), 2);
		hdivIO.setDivdAmount(mult(hdisIO.getBalSinceLastCap(),-1));
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
		hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		hdivIO.setDivdType("C");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setFormat(hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			syserrrec.statuz.set(hdivIO.getStatuz());
			fatalError9000();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(dthcpy.tranno);
		wsaaHdisOsInt.set(hdisIO.getOsInterest());
		wsaaHdisRead.set("Y");
		hdisIO.setOsInterest(ZERO);
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError9000();
		}
	}

protected void calcPreviousDiv(){
	
	wsaaTotDividend.set(hdisIO.getBalSinceLastCap());
	hdivcshIO.setDataArea(SPACES);
	hdivcshIO.setChdrcoy(dthcpy.chdrChdrcoy);
	hdivcshIO.setChdrnum(dthcpy.chdrChdrnum);
	hdivcshIO.setLife(dthcpy.lifeLife);
	hdivcshIO.setCoverage(dthcpy.covrCoverage);
	hdivcshIO.setRider(dthcpy.covrRider);
	hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
	compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
	hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
	hdivcshIO.setFormat(hdivcshrec);
	hdivcshIO.setFunction(varcom.begn);

	//performance improvement -- Anjali
	hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

	SmartFileCode.execute(appVars, hdivcshIO);
	if (isNE(hdivcshIO.getStatuz(),varcom.oK)
	&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	if (isNE(hdivcshIO.getChdrcoy(),dthcpy.chdrChdrcoy)
	|| isNE(hdivcshIO.getChdrnum(),dthcpy.chdrChdrnum)
	|| isNE(hdivcshIO.getLife(),dthcpy.lifeLife)
	|| isNE(hdivcshIO.getCoverage(),dthcpy.covrCoverage)
	|| isNE(hdivcshIO.getRider(),dthcpy.covrRider)
	|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
		wsaaTotDividend.add(hdivcshIO.getDivdAmount());
		hdivcshIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		if (isNE(hdivcshIO.getChdrcoy(),dthcpy.chdrChdrcoy)
		|| isNE(hdivcshIO.getChdrnum(),dthcpy.chdrChdrnum)
		|| isNE(hdivcshIO.getLife(),dthcpy.lifeLife)
		|| isNE(hdivcshIO.getCoverage(),dthcpy.covrCoverage)
		|| isNE(hdivcshIO.getRider(),dthcpy.covrRider)
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
	}
}


protected void interest3000()
	{
		if(cdivFlag){
			calcInterest();
		}
		newInt3000();
	}

protected void calcInterest(){
	
	hdisIO.setParams(SPACES);
	hdisIO.setChdrcoy(dthcpy.chdrChdrcoy);
	hdisIO.setChdrnum(dthcpy.chdrChdrnum);
	hdisIO.setLife(dthcpy.lifeLife);
	hdisIO.setCoverage(dthcpy.covrCoverage);
	hdisIO.setRider(dthcpy.covrRider);
	hdisIO.setPlanSuffix(0);
	hdisIO.setFormat(hdisrec);
	hdisIO.setFunction(varcom.readh);
	SmartFileCode.execute(appVars, hdisIO);
	if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
		wsaaHdisOsInt.set(0);
		wsaaHdisRead.set("Y");
		wsaaTotDividend.set(0);
		wsaaNewInt.set(0);
		return;
	}
	else{
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError9000();
		}
	}
	payrpf = payrpfDao.getpayrRecord(dthcpy.chdrChdrcoy.toString(), dthcpy.chdrChdrnum.toString());
	itempf = itempfDAO.readItdmpf("IT",dthcpy.chdrChdrcoy.toString(),th501,dthcpy.effdate.toInt(),StringUtils.rightPad(hcsdIO.getZcshdivmth().toString(), 8));
	if(itempf != null){
		th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else{
		syserrrec.params.set("IT".concat(dthcpy.chdrChdrcoy.toString()).concat(th501).concat(hcsdIO.getZcshdivmth().toString()));
		fatalError9000();
	}
	initialize(hdvdintrec.divdIntRec);
	hdvdintrec.chdrChdrcoy.set(dthcpy.chdrChdrcoy);
	hdvdintrec.chdrChdrnum.set(dthcpy.chdrChdrnum);
	hdvdintrec.lifeLife.set(dthcpy.lifeLife);
	hdvdintrec.covrCoverage.set(dthcpy.covrCoverage);
	hdvdintrec.covrRider.set(dthcpy.covrRider);
	hdvdintrec.plnsfx.set(ZERO);
	hdvdintrec.cntcurr.set(dthcpy.currcode);
	hdvdintrec.transcd.set(SPACES);
	hdvdintrec.crtable.set(dthcpy.crtable);
	hdvdintrec.effectiveDate.set(dthcpy.effdate);
	hdvdintrec.tranno.set(ZERO);
	hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
	hdvdintrec.intTo.set(payrpf.getPtdate());
	hdvdintrec.capAmount.set(hdisIO.getBalSinceLastCap());
	hdvdintrec.intDuration.set(ZERO);
	hdvdintrec.intAmount.set(ZERO);
	hdvdintrec.intRate.set(ZERO);
	hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
	if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(hdvdintrec.statuz);
			fatalError9000();
		}
		wsaaNewInt.add(hdvdintrec.intAmount);
	}
	hdivcshIO.setDataArea(SPACES);
	hdivcshIO.setChdrcoy(hdisIO.getChdrcoy());
	hdivcshIO.setChdrnum(hdisIO.getChdrnum());
	hdivcshIO.setLife(hdisIO.getLife());
	hdivcshIO.setCoverage(hdisIO.getCoverage());
	hdivcshIO.setRider(hdisIO.getRider());
	hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
	hdivcshIO.setDivdIntCapDate(hdisIO.getNextIntDate());
	hdivcshIO.setFormat(hdivcshrec);
	hdivcshIO.setFunction(varcom.begn);

	//performance improvement -- Anjali
	hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

	SmartFileCode.execute(appVars, hdivcshIO);
	if (isNE(hdivcshIO.getStatuz(),varcom.oK)
	&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
	|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
	|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
	|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
	|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
	|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(dthcpy.chdrChdrcoy);
		hdvdintrec.chdrChdrnum.set(dthcpy.chdrChdrnum);
		hdvdintrec.lifeLife.set(dthcpy.lifeLife);
		hdvdintrec.covrCoverage.set(dthcpy.covrCoverage);
		hdvdintrec.covrRider.set(dthcpy.covrRider);
		hdvdintrec.plnsfx.set(ZERO);
		hdvdintrec.cntcurr.set(dthcpy.currcode);
		hdvdintrec.transcd.set(SPACES);
		hdvdintrec.crtable.set(dthcpy.crtable);
		hdvdintrec.effectiveDate.set(dthcpy.effdate);
		hdvdintrec.tranno.set(ZERO);
		if (isGT(hdivcshIO.getEffdate(),hdisIO.getLastIntDate())) {
			hdvdintrec.intFrom.set(hdivcshIO.getEffdate());
		}
		else {
			hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
		}
		hdvdintrec.intTo.set(payrpf.getPtdate());
		hdvdintrec.capAmount.set(hdivcshIO.getDivdAmount());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
			callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
			if (isNE(hdvdintrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(hdvdintrec.statuz);
				fatalError9000();
			}
			wsaaNewInt.add(hdvdintrec.intAmount);
		}
		hdivcshIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
		|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
		|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
	}
}

protected void newInt3000()
	{
		if (hdisAlreadyRead.isTrue()) {
			if(cdivFlag){
				compute(wsaaCurrOsInt, 2).set(sub(dthcpy.actualVal,wsaaHdisOsInt));
				compute(wsaaCurrOsInt, 2).set(sub(wsaaCurrOsInt,wsaaNewInt));
				compute(dthcpy.actualVal, 2).set(sub(dthcpy.actualVal,wsaaCurrOsInt));
				lifacmvrec1.origamt.set(wsaaHdisOsInt);
			}
			else{
			compute(wsaaNewInt, 2).set(sub(dthcpy.actualVal,wsaaHdisOsInt));
			lifacmvrec1.origamt.set(wsaaHdisOsInt);
			}
		}
		else {
			hdisIO.setParams(SPACES);
			hdisIO.setChdrcoy(dthcpy.chdrChdrcoy);
			hdisIO.setChdrnum(dthcpy.chdrChdrnum);
			hdisIO.setLife(dthcpy.lifeLife);
			hdisIO.setCoverage(dthcpy.covrCoverage);
			hdisIO.setRider(dthcpy.covrRider);
			hdisIO.setPlanSuffix(0);
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				fatalError9000();
			}
			if(cdivFlag){
				compute(wsaaCurrOsInt, 2).set(sub(dthcpy.actualVal,wsaaHdisOsInt));
				compute(wsaaCurrOsInt, 2).set(sub(wsaaCurrOsInt,wsaaNewInt));
				compute(dthcpy.actualVal, 2).set(sub(dthcpy.actualVal,wsaaCurrOsInt));
				lifacmvrec1.origamt.set(wsaaHdisOsInt);
			}
			else{
			compute(wsaaNewInt, 2).set(sub(dthcpy.actualVal,hdisIO.getOsInterest()));
			lifacmvrec1.origamt.set(hdisIO.getOsInterest());
			}
		}
		if (isNE(dthcpy.actualVal,ZERO)) {
			hdivIO.setParams(SPACES);
			hdivIO.setChdrcoy(dthcpy.chdrChdrcoy);
			hdivIO.setChdrnum(dthcpy.chdrChdrnum);
			hdivIO.setLife(dthcpy.lifeLife);
			hdivIO.setCoverage(dthcpy.covrCoverage);
			hdivIO.setRider(dthcpy.covrRider);
			hdivIO.setPlanSuffix(0);
			hdivIO.setTranno(dthcpy.tranno);
			hdivIO.setEffdate(dthcpy.effdate);
			hdivIO.setDivdAllocDate(dthcpy.effdate);
			hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
			hdivIO.setCntcurr(hdisIO.getCntcurr());
			setPrecision(hdivIO.getDivdAmount(), 2);
			hdivIO.setDivdAmount(mult(dthcpy.actualVal,-1));
			hdivIO.setDivdRate(ZERO);
			hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
			hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
			hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			hdivIO.setDivdType("I");
			hdivIO.setZdivopt(hcsdIO.getZdivopt());
			hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
			hdivIO.setDivdOptprocTranno(ZERO);
			hdivIO.setDivdCapTranno(ZERO);
			hdivIO.setDivdStmtNo(ZERO);
			hdivIO.setPuAddNbr(ZERO);
			hdivIO.setFormat(hdivrec);
			hdivIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hdivIO);
			if (isNE(hdivIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdivIO.getParams());
				syserrrec.statuz.set(hdivIO.getStatuz());
				fatalError9000();
			}
		}
		if (isNE(wsaaNewInt,ZERO)) {
			hdivIO.setDivdType("I");
			hdivIO.setDivdAmount(wsaaNewInt);
			hdivIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hdivIO);
			if (isNE(hdivIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdivIO.getParams());
				syserrrec.statuz.set(hdivIO.getStatuz());
				fatalError9000();
			}
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		dthcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
