/******************************************************************************
 * File Name 		: Hdpxpf.java
 * Author			: sbatra9
 * Creation Date	: 22 June 2020
 * Project			: Integral Life
 * Description		: The Model Class for HDPXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.model;


public class Hdpxpf {
	
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String  life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String zdivopt;
	
	public Hdpxpf() {
		//Empty Constructor
	}
	
	public Hdpxpf(Hdpxpf hdpxpf){
		this.chdrcoy=hdpxpf.chdrcoy;
    	this.chdrnum=hdpxpf.chdrnum;
    	this.life=hdpxpf.life;
    	this.jlife=hdpxpf.jlife;
    	this.coverage=hdpxpf.coverage;
    	this.rider=hdpxpf.rider;
    	this.planSuffix=hdpxpf.planSuffix;
    	this.zdivopt=hdpxpf.zdivopt;
	}
	
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    
    public String getChdrnum() {
        return chdrnum;
    }
    
    public void setLife(String life) {
        this.life = life;
    }
    
    public String getLife() {
        return life;
    }
    
    public void setJLife(String jlife) {
        this.jlife = jlife;
    }
    
    public String getJLife() {
        return jlife;
    }
    
    public String getCoverage() {
        return coverage;
    }
    
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    
    public String getRider() {
        return rider;
    }
    
    public void setRider(String rider) {
        this.rider = rider;
    }
    
    public int getPlanSuffix() {
        return planSuffix;
    }
    
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    
    public String getZdivopt() {
		return zdivopt;
	}
	public void setZdivopt(String zdivopt) {
		this.zdivopt = zdivopt;
	}
	
	// ToString method
	public String toString(){
		StringBuilder output = new StringBuilder();
		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJLife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlanSuffix());
		output.append("\r\n");
		output.append("ZDIVOPT:		");
		output.append(getZdivopt());
		output.append("\r\n");
		return output.toString();
	}
}