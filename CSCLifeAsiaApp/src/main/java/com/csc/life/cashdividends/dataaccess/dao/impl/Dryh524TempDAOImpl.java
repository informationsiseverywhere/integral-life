package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.cashdividends.dataaccess.dao.Dryh524TempDAO;
import com.csc.life.cashdividends.dataaccess.model.Dryh524DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;

public class Dryh524TempDAOImpl extends BaseDAOImpl<Dryh524DTO> implements Dryh524TempDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Dryh524TempDAOImpl.class);
	public List<Dryh524DTO> getHdisHcsdRecords(String chdrcoy, String chdrnum) {
		List<Dryh524DTO> dryh524List = new ArrayList<Dryh524DTO>();
		Dryh524DTO dryh524; 
		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append(" HDIS.UNIQUE_NUMBER,HDIS.CHDRCOY,HDIS.CHDRNUM,HDIS.HINTNDT,HDIS.HCAPLDT,HCAPNDT,HDIS.TRANNO, HDIS.LIFE, HDIS.COVERAGE, HDIS.RIDER, HDIS.PLNSFX, HINTOS, HCSD.UNIQUE_NUMBER AS HCSDUNIQUENUMBER, ZCSHDIVMTH FROM HDISPF HDIS ");
		sql.append(" LEFT JOIN HCSDPF HCSD ON HDIS.CHDRCOY=HCSD.CHDRCOY AND HDIS.CHDRNUM=HCSD.CHDRNUM AND HDIS.LIFE=HCSD.LIFE");
		sql.append(" AND HDIS.COVERAGE=HCSD.COVERAGE AND HDIS.RIDER=HCSD.RIDER AND HDIS.PLNSFX=HCSD.PLNSFX");
		sql.append(" WHERE HDIS.VALIDFLAG=? AND HDIS.CHDRCOY=? AND HDIS.CHDRNUM=?");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, "1");
			ps.setString(2, chdrcoy);
			ps.setString(3, chdrnum);
			rs = ps.executeQuery();
			while(rs.next()) {
				dryh524 = new Dryh524DTO();
				dryh524.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				dryh524.setChdrcoy(rs.getString("CHDRCOY"));
				dryh524.setChdrnum(rs.getString("CHDRNUM"));
				dryh524.setHintndt(rs.getInt("HINTNDT"));
				dryh524.setHcapndt(rs.getInt("HCAPNDT"));
				dryh524.setHcapldt(rs.getInt("HCAPLDT"));
				dryh524.setHintos(rs.getBigDecimal("HINTOS"));
				dryh524.setTranno(rs.getInt("TRANNO"));
				dryh524.setLife(rs.getString("LIFE"));
				dryh524.setCoverage(rs.getString("COVERAGE"));
				dryh524.setRider(rs.getString("RIDER"));
				dryh524.setPlnsfx(rs.getInt("PLNSFX"));
				dryh524.setZcshdivmth(rs.getString("ZCSHDIVMTH"));
				dryh524.setHcsdUniqueNumber(rs.getLong("HCSDUNIQUENUMBER"));
				dryh524List.add(dryh524);
			}
		}
		catch(SQLException e) {
			LOGGER.error("Exception in method : getHdisHcsdRecords()" ,e);
			
		}
		finally {
			close(ps,rs);
		}
		return dryh524List;
	}
}
