package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh533screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 8;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {42, 10, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 17, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh533ScreenVars sv = (Sh533ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sh533screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sh533screensfl, 
			sv.Sh533screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sh533ScreenVars sv = (Sh533ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sh533screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sh533ScreenVars sv = (Sh533ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sh533screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sh533screensflWritten.gt(0))
		{
			sv.sh533screensfl.setCurrentIndex(0);
			sv.Sh533screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sh533ScreenVars sv = (Sh533ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sh533screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh533ScreenVars screenVars = (Sh533ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hemv.setFieldName("hemv");
				screenVars.zwdv.setFieldName("zwdv");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hrider.setFieldName("hrider");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.nextCapDateDisp.setFieldName("nextCapDateDisp");
				screenVars.zdivopt.setFieldName("zdivopt");
				screenVars.zcshdivmth.setFieldName("zcshdivmth");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hemv.set(dm.getField("hemv"));
			screenVars.zwdv.set(dm.getField("zwdv"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.hcnstcur.set(dm.getField("hcnstcur"));
			screenVars.hcover.set(dm.getField("hcover"));
			screenVars.hrider.set(dm.getField("hrider"));
			screenVars.planSuffix.set(dm.getField("planSuffix"));
			screenVars.hjlife.set(dm.getField("hjlife"));
			screenVars.nextCapDateDisp.set(dm.getField("nextCapDateDisp"));
			screenVars.zdivopt.set(dm.getField("zdivopt"));
			screenVars.zcshdivmth.set(dm.getField("zcshdivmth"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.fieldType.set(dm.getField("fieldType"));
			screenVars.shortds.set(dm.getField("shortds"));
			screenVars.cnstcur.set(dm.getField("cnstcur"));
			screenVars.estMatValue.set(dm.getField("estMatValue"));
			screenVars.actvalue.set(dm.getField("actvalue"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh533ScreenVars screenVars = (Sh533ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hemv.setFieldName("hemv");
				screenVars.zwdv.setFieldName("zwdv");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hrider.setFieldName("hrider");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.nextCapDateDisp.setFieldName("nextCapDateDisp");
				screenVars.zdivopt.setFieldName("zdivopt");
				screenVars.zcshdivmth.setFieldName("zcshdivmth");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hemv").set(screenVars.hemv);
			dm.getField("zwdv").set(screenVars.zwdv);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("hcnstcur").set(screenVars.hcnstcur);
			dm.getField("hcover").set(screenVars.hcover);
			dm.getField("hrider").set(screenVars.hrider);
			dm.getField("planSuffix").set(screenVars.planSuffix);
			dm.getField("hjlife").set(screenVars.hjlife);
			dm.getField("nextCapDateDisp").set(screenVars.nextCapDateDisp);
			dm.getField("zdivopt").set(screenVars.zdivopt);
			dm.getField("zcshdivmth").set(screenVars.zcshdivmth);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("fieldType").set(screenVars.fieldType);
			dm.getField("shortds").set(screenVars.shortds);
			dm.getField("cnstcur").set(screenVars.cnstcur);
			dm.getField("estMatValue").set(screenVars.estMatValue);
			dm.getField("actvalue").set(screenVars.actvalue);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sh533screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sh533ScreenVars screenVars = (Sh533ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hemv.clearFormatting();
		screenVars.zwdv.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.hcnstcur.clearFormatting();
		screenVars.hcover.clearFormatting();
		screenVars.hrider.clearFormatting();
		screenVars.planSuffix.clearFormatting();
		screenVars.hjlife.clearFormatting();
		screenVars.nextCapDateDisp.clearFormatting();
		screenVars.zdivopt.clearFormatting();
		screenVars.zcshdivmth.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.fieldType.clearFormatting();
		screenVars.shortds.clearFormatting();
		screenVars.cnstcur.clearFormatting();
		screenVars.estMatValue.clearFormatting();
		screenVars.actvalue.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sh533ScreenVars screenVars = (Sh533ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hemv.setClassString("");
		screenVars.zwdv.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.hcnstcur.setClassString("");
		screenVars.hcover.setClassString("");
		screenVars.hrider.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.hjlife.setClassString("");
		screenVars.nextCapDateDisp.setClassString("");
		screenVars.zdivopt.setClassString("");
		screenVars.zcshdivmth.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.fieldType.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.cnstcur.setClassString("");
		screenVars.estMatValue.setClassString("");
		screenVars.actvalue.setClassString("");
	}

/**
 * Clear all the variables in Sh533screensfl
 */
	public static void clear(VarModel pv) {
		Sh533ScreenVars screenVars = (Sh533ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hemv.clear();
		screenVars.zwdv.clear();
		screenVars.hcrtable.clear();
		screenVars.hcnstcur.clear();
		screenVars.hcover.clear();
		screenVars.hrider.clear();
		screenVars.planSuffix.clear();
		screenVars.hjlife.clear();
		screenVars.nextCapDateDisp.clear();
		screenVars.nextCapDate.clear();
		screenVars.zdivopt.clear();
		screenVars.zcshdivmth.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.fieldType.clear();
		screenVars.shortds.clear();
		screenVars.cnstcur.clear();
		screenVars.estMatValue.clear();
		screenVars.actvalue.clear();
	}
}
