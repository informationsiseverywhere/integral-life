package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:33
 * Description:
 * Copybook name: HDIVREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivrevKey = new FixedLengthStringData(64).isAPartOf(hdivrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivrevChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivrevKey, 0);
  	public FixedLengthStringData hdivrevChdrnum = new FixedLengthStringData(8).isAPartOf(hdivrevKey, 1);
  	public PackedDecimalData hdivrevTranno = new PackedDecimalData(5, 0).isAPartOf(hdivrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(hdivrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}