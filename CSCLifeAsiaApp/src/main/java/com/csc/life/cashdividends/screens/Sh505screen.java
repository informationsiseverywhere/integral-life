package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh505screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh505ScreenVars sv = (Sh505ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh505screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh505ScreenVars screenVars = (Sh505ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.sumInsMin.setClassString("");
		screenVars.sumInsMax.setClassString("");
		screenVars.ageIssageFrm01.setClassString("");
		screenVars.ageIssageTo01.setClassString("");
		screenVars.ageIssageFrm02.setClassString("");
		screenVars.ageIssageTo02.setClassString("");
		screenVars.ageIssageFrm03.setClassString("");
		screenVars.ageIssageTo03.setClassString("");
		screenVars.ageIssageFrm04.setClassString("");
		screenVars.ageIssageTo04.setClassString("");
		screenVars.ageIssageFrm05.setClassString("");
		screenVars.ageIssageTo05.setClassString("");
		screenVars.ageIssageFrm06.setClassString("");
		screenVars.ageIssageTo06.setClassString("");
		screenVars.ageIssageFrm07.setClassString("");
		screenVars.ageIssageTo07.setClassString("");
		screenVars.ageIssageFrm08.setClassString("");
		screenVars.ageIssageTo08.setClassString("");
		screenVars.riskCessageFrom01.setClassString("");
		screenVars.riskCessageTo01.setClassString("");
		screenVars.riskCessageFrom02.setClassString("");
		screenVars.riskCessageTo02.setClassString("");
		screenVars.riskCessageFrom03.setClassString("");
		screenVars.riskCessageTo03.setClassString("");
		screenVars.riskCessageFrom04.setClassString("");
		screenVars.riskCessageTo04.setClassString("");
		screenVars.riskCessageFrom05.setClassString("");
		screenVars.riskCessageTo05.setClassString("");
		screenVars.riskCessageFrom06.setClassString("");
		screenVars.riskCessageTo06.setClassString("");
		screenVars.riskCessageFrom07.setClassString("");
		screenVars.riskCessageTo07.setClassString("");
		screenVars.riskCessageFrom08.setClassString("");
		screenVars.riskCessageTo08.setClassString("");
		screenVars.premCessageFrom01.setClassString("");
		screenVars.premCessageTo01.setClassString("");
		screenVars.premCessageFrom02.setClassString("");
		screenVars.premCessageTo02.setClassString("");
		screenVars.premCessageFrom03.setClassString("");
		screenVars.premCessageTo03.setClassString("");
		screenVars.premCessageFrom04.setClassString("");
		screenVars.premCessageTo04.setClassString("");
		screenVars.premCessageFrom05.setClassString("");
		screenVars.premCessageTo05.setClassString("");
		screenVars.premCessageFrom06.setClassString("");
		screenVars.premCessageTo06.setClassString("");
		screenVars.premCessageFrom07.setClassString("");
		screenVars.premCessageTo07.setClassString("");
		screenVars.premCessageFrom08.setClassString("");
		screenVars.premCessageTo08.setClassString("");
		screenVars.eaage.setClassString("");
		screenVars.termIssageFrm01.setClassString("");
		screenVars.termIssageTo01.setClassString("");
		screenVars.termIssageFrm02.setClassString("");
		screenVars.termIssageTo02.setClassString("");
		screenVars.termIssageFrm03.setClassString("");
		screenVars.termIssageTo03.setClassString("");
		screenVars.termIssageFrm04.setClassString("");
		screenVars.termIssageTo04.setClassString("");
		screenVars.termIssageFrm05.setClassString("");
		screenVars.termIssageTo05.setClassString("");
		screenVars.termIssageFrm06.setClassString("");
		screenVars.termIssageTo06.setClassString("");
		screenVars.termIssageFrm07.setClassString("");
		screenVars.termIssageTo07.setClassString("");
		screenVars.termIssageFrm08.setClassString("");
		screenVars.termIssageTo08.setClassString("");
		screenVars.riskCesstermFrom01.setClassString("");
		screenVars.riskCesstermTo01.setClassString("");
		screenVars.riskCesstermFrom02.setClassString("");
		screenVars.riskCesstermTo02.setClassString("");
		screenVars.riskCesstermFrom03.setClassString("");
		screenVars.riskCesstermTo03.setClassString("");
		screenVars.riskCesstermFrom04.setClassString("");
		screenVars.riskCesstermTo04.setClassString("");
		screenVars.riskCesstermFrom05.setClassString("");
		screenVars.riskCesstermTo05.setClassString("");
		screenVars.riskCesstermFrom06.setClassString("");
		screenVars.riskCesstermTo06.setClassString("");
		screenVars.riskCesstermFrom07.setClassString("");
		screenVars.riskCesstermTo07.setClassString("");
		screenVars.riskCesstermFrom08.setClassString("");
		screenVars.riskCesstermTo08.setClassString("");
		screenVars.premCesstermFrom01.setClassString("");
		screenVars.premCesstermTo01.setClassString("");
		screenVars.premCesstermFrom02.setClassString("");
		screenVars.premCesstermTo02.setClassString("");
		screenVars.premCesstermFrom03.setClassString("");
		screenVars.premCesstermTo03.setClassString("");
		screenVars.premCesstermFrom04.setClassString("");
		screenVars.premCesstermTo04.setClassString("");
		screenVars.premCesstermFrom05.setClassString("");
		screenVars.premCesstermTo05.setClassString("");
		screenVars.premCesstermFrom06.setClassString("");
		screenVars.premCesstermTo06.setClassString("");
		screenVars.premCesstermFrom07.setClassString("");
		screenVars.premCesstermTo07.setClassString("");
		screenVars.premCesstermFrom08.setClassString("");
		screenVars.premCesstermTo08.setClassString("");
		screenVars.mortcls01.setClassString("");
		screenVars.mortcls02.setClassString("");
		screenVars.mortcls03.setClassString("");
		screenVars.mortcls04.setClassString("");
		screenVars.mortcls05.setClassString("");
		screenVars.mortcls06.setClassString("");
		screenVars.liencd01.setClassString("");
		screenVars.liencd02.setClassString("");
		screenVars.liencd03.setClassString("");
		screenVars.liencd04.setClassString("");
		screenVars.liencd05.setClassString("");
		screenVars.liencd06.setClassString("");
		screenVars.specind.setClassString("");
		screenVars.zdivopt01.setClassString("");
		screenVars.zdivopt02.setClassString("");
		screenVars.zdivopt03.setClassString("");
		screenVars.zdivopt04.setClassString("");
		screenVars.zdivopt05.setClassString("");
		screenVars.zdivopt06.setClassString("");
		screenVars.zdivopt07.setClassString("");
		screenVars.zdefdivopt.setClassString("");
		screenVars.prmbasis01.setClassString("");
		screenVars.prmbasis02.setClassString("");
		screenVars.prmbasis03.setClassString("");
		screenVars.prmbasis04.setClassString("");
		screenVars.prmbasis05.setClassString("");
		screenVars.prmbasis06.setClassString("");
	}

/**
 * Clear all the variables in Sh505screen
 */
	public static void clear(VarModel pv) {
		Sh505ScreenVars screenVars = (Sh505ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.sumInsMin.clear();
		screenVars.sumInsMax.clear();
		screenVars.ageIssageFrm01.clear();
		screenVars.ageIssageTo01.clear();
		screenVars.ageIssageFrm02.clear();
		screenVars.ageIssageTo02.clear();
		screenVars.ageIssageFrm03.clear();
		screenVars.ageIssageTo03.clear();
		screenVars.ageIssageFrm04.clear();
		screenVars.ageIssageTo04.clear();
		screenVars.ageIssageFrm05.clear();
		screenVars.ageIssageTo05.clear();
		screenVars.ageIssageFrm06.clear();
		screenVars.ageIssageTo06.clear();
		screenVars.ageIssageFrm07.clear();
		screenVars.ageIssageTo07.clear();
		screenVars.ageIssageFrm08.clear();
		screenVars.ageIssageTo08.clear();
		screenVars.riskCessageFrom01.clear();
		screenVars.riskCessageTo01.clear();
		screenVars.riskCessageFrom02.clear();
		screenVars.riskCessageTo02.clear();
		screenVars.riskCessageFrom03.clear();
		screenVars.riskCessageTo03.clear();
		screenVars.riskCessageFrom04.clear();
		screenVars.riskCessageTo04.clear();
		screenVars.riskCessageFrom05.clear();
		screenVars.riskCessageTo05.clear();
		screenVars.riskCessageFrom06.clear();
		screenVars.riskCessageTo06.clear();
		screenVars.riskCessageFrom07.clear();
		screenVars.riskCessageTo07.clear();
		screenVars.riskCessageFrom08.clear();
		screenVars.riskCessageTo08.clear();
		screenVars.premCessageFrom01.clear();
		screenVars.premCessageTo01.clear();
		screenVars.premCessageFrom02.clear();
		screenVars.premCessageTo02.clear();
		screenVars.premCessageFrom03.clear();
		screenVars.premCessageTo03.clear();
		screenVars.premCessageFrom04.clear();
		screenVars.premCessageTo04.clear();
		screenVars.premCessageFrom05.clear();
		screenVars.premCessageTo05.clear();
		screenVars.premCessageFrom06.clear();
		screenVars.premCessageTo06.clear();
		screenVars.premCessageFrom07.clear();
		screenVars.premCessageTo07.clear();
		screenVars.premCessageFrom08.clear();
		screenVars.premCessageTo08.clear();
		screenVars.eaage.clear();
		screenVars.termIssageFrm01.clear();
		screenVars.termIssageTo01.clear();
		screenVars.termIssageFrm02.clear();
		screenVars.termIssageTo02.clear();
		screenVars.termIssageFrm03.clear();
		screenVars.termIssageTo03.clear();
		screenVars.termIssageFrm04.clear();
		screenVars.termIssageTo04.clear();
		screenVars.termIssageFrm05.clear();
		screenVars.termIssageTo05.clear();
		screenVars.termIssageFrm06.clear();
		screenVars.termIssageTo06.clear();
		screenVars.termIssageFrm07.clear();
		screenVars.termIssageTo07.clear();
		screenVars.termIssageFrm08.clear();
		screenVars.termIssageTo08.clear();
		screenVars.riskCesstermFrom01.clear();
		screenVars.riskCesstermTo01.clear();
		screenVars.riskCesstermFrom02.clear();
		screenVars.riskCesstermTo02.clear();
		screenVars.riskCesstermFrom03.clear();
		screenVars.riskCesstermTo03.clear();
		screenVars.riskCesstermFrom04.clear();
		screenVars.riskCesstermTo04.clear();
		screenVars.riskCesstermFrom05.clear();
		screenVars.riskCesstermTo05.clear();
		screenVars.riskCesstermFrom06.clear();
		screenVars.riskCesstermTo06.clear();
		screenVars.riskCesstermFrom07.clear();
		screenVars.riskCesstermTo07.clear();
		screenVars.riskCesstermFrom08.clear();
		screenVars.riskCesstermTo08.clear();
		screenVars.premCesstermFrom01.clear();
		screenVars.premCesstermTo01.clear();
		screenVars.premCesstermFrom02.clear();
		screenVars.premCesstermTo02.clear();
		screenVars.premCesstermFrom03.clear();
		screenVars.premCesstermTo03.clear();
		screenVars.premCesstermFrom04.clear();
		screenVars.premCesstermTo04.clear();
		screenVars.premCesstermFrom05.clear();
		screenVars.premCesstermTo05.clear();
		screenVars.premCesstermFrom06.clear();
		screenVars.premCesstermTo06.clear();
		screenVars.premCesstermFrom07.clear();
		screenVars.premCesstermTo07.clear();
		screenVars.premCesstermFrom08.clear();
		screenVars.premCesstermTo08.clear();
		screenVars.mortcls01.clear();
		screenVars.mortcls02.clear();
		screenVars.mortcls03.clear();
		screenVars.mortcls04.clear();
		screenVars.mortcls05.clear();
		screenVars.mortcls06.clear();
		screenVars.liencd01.clear();
		screenVars.liencd02.clear();
		screenVars.liencd03.clear();
		screenVars.liencd04.clear();
		screenVars.liencd05.clear();
		screenVars.liencd06.clear();
		screenVars.specind.clear();
		screenVars.zdivopt01.clear();
		screenVars.zdivopt02.clear();
		screenVars.zdivopt03.clear();
		screenVars.zdivopt04.clear();
		screenVars.zdivopt05.clear();
		screenVars.zdivopt06.clear();
		screenVars.zdivopt07.clear();
		screenVars.zdefdivopt.clear();
		screenVars.prmbasis01.clear();
		screenVars.prmbasis02.clear();
		screenVars.prmbasis03.clear();
		screenVars.prmbasis04.clear();
		screenVars.prmbasis05.clear();
		screenVars.prmbasis06.clear();
	}
}
