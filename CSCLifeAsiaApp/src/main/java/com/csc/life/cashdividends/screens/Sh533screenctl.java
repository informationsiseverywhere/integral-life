package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh533screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sh533screensfl";
		lrec.subfileClass = Sh533screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 8;
		lrec.pageSubfile = 7;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh533ScreenVars sv = (Sh533ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh533screenctlWritten, sv.Sh533screensflWritten, av, sv.sh533screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh533ScreenVars screenVars = (Sh533ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		
		screenVars.payeeno.setClassString("");
		screenVars.payeenme.setClassString("");
		screenVars.paymthbf.setClassString("");
		screenVars.bankactkey.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bnkcdedsc.setClassString("");
		screenVars.configflag.setClassString("");
		screenVars.crdtcrd.setClassString("");
	}

/**
 * Clear all the variables in Sh533screenctl
 */
	public static void clear(VarModel pv) {
		Sh533ScreenVars screenVars = (Sh533ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.descrip.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		
		screenVars.payeeno.clear();
		screenVars.payeenme.clear();
		screenVars.paymthbf.clear();
		screenVars.bankactkey.clear();
		screenVars.bankkey.clear();
		screenVars.bnkcdedsc.clear();
		screenVars.configflag.clear();
		screenVars.crdtcrd.clear();
	}
}
