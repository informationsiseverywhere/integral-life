package com.csc.life.cashdividends.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:41
 * Description:
 * Copybook name: TH505REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th505rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th505Rec = new FixedLengthStringData(506);
  	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(24).isAPartOf(th505Rec, 0);
  	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(8, 3, 0, ageIssageFrms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData ageIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData ageIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData ageIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData ageIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData ageIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData ageIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData ageIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(24).isAPartOf(th505Rec, 24);
  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(8, 3, 0, ageIssageTos, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public FixedLengthStringData eaage = new FixedLengthStringData(1).isAPartOf(th505Rec, 48);
  	public FixedLengthStringData liencds = new FixedLengthStringData(12).isAPartOf(th505Rec, 49);
  	public FixedLengthStringData[] liencd = FLSArrayPartOfStructure(6, 2, liencds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(liencds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData liencd01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData liencd02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData liencd03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData liencd04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData liencd05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData liencd06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData mortclss = new FixedLengthStringData(6).isAPartOf(th505Rec, 61);
  	public FixedLengthStringData[] mortcls = FLSArrayPartOfStructure(6, 1, mortclss, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(6).isAPartOf(mortclss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mortcls01 = new FixedLengthStringData(1).isAPartOf(filler3, 0);
  	public FixedLengthStringData mortcls02 = new FixedLengthStringData(1).isAPartOf(filler3, 1);
  	public FixedLengthStringData mortcls03 = new FixedLengthStringData(1).isAPartOf(filler3, 2);
  	public FixedLengthStringData mortcls04 = new FixedLengthStringData(1).isAPartOf(filler3, 3);
  	public FixedLengthStringData mortcls05 = new FixedLengthStringData(1).isAPartOf(filler3, 4);
  	public FixedLengthStringData mortcls06 = new FixedLengthStringData(1).isAPartOf(filler3, 5);
  	public FixedLengthStringData premCessageFroms = new FixedLengthStringData(24).isAPartOf(th505Rec, 67);
  	public ZonedDecimalData[] premCessageFrom = ZDArrayPartOfStructure(8, 3, 0, premCessageFroms, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(premCessageFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData premCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 3);
  	public ZonedDecimalData premCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 6);
  	public ZonedDecimalData premCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 9);
  	public ZonedDecimalData premCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 12);
  	public ZonedDecimalData premCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 15);
  	public ZonedDecimalData premCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 18);
  	public ZonedDecimalData premCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 21);
  	public FixedLengthStringData premCessageTos = new FixedLengthStringData(24).isAPartOf(th505Rec, 91);
  	public ZonedDecimalData[] premCessageTo = ZDArrayPartOfStructure(8, 3, 0, premCessageTos, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(premCessageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData premCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 3);
  	public ZonedDecimalData premCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 6);
  	public ZonedDecimalData premCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 9);
  	public ZonedDecimalData premCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 12);
  	public ZonedDecimalData premCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 15);
  	public ZonedDecimalData premCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 18);
  	public ZonedDecimalData premCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 21);
  	public FixedLengthStringData premCesstermFroms = new FixedLengthStringData(24).isAPartOf(th505Rec, 115);
  	public ZonedDecimalData[] premCesstermFrom = ZDArrayPartOfStructure(8, 3, 0, premCesstermFroms, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(premCesstermFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCesstermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 0);
  	public ZonedDecimalData premCesstermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 3);
  	public ZonedDecimalData premCesstermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 6);
  	public ZonedDecimalData premCesstermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 9);
  	public ZonedDecimalData premCesstermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 12);
  	public ZonedDecimalData premCesstermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 15);
  	public ZonedDecimalData premCesstermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 18);
  	public ZonedDecimalData premCesstermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 21);
  	public FixedLengthStringData premCesstermTos = new FixedLengthStringData(24).isAPartOf(th505Rec, 139);
  	public ZonedDecimalData[] premCesstermTo = ZDArrayPartOfStructure(8, 3, 0, premCesstermTos, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(24).isAPartOf(premCesstermTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 0);
  	public ZonedDecimalData premCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 3);
  	public ZonedDecimalData premCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 6);
  	public ZonedDecimalData premCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 9);
  	public ZonedDecimalData premCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 12);
  	public ZonedDecimalData premCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 15);
  	public ZonedDecimalData premCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 18);
  	public ZonedDecimalData premCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 21);
  	public FixedLengthStringData riskCessageFroms = new FixedLengthStringData(24).isAPartOf(th505Rec, 163);
  	public ZonedDecimalData[] riskCessageFrom = ZDArrayPartOfStructure(8, 3, 0, riskCessageFroms, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(riskCessageFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 0);
  	public ZonedDecimalData riskCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 3);
  	public ZonedDecimalData riskCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 6);
  	public ZonedDecimalData riskCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 9);
  	public ZonedDecimalData riskCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 12);
  	public ZonedDecimalData riskCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 15);
  	public ZonedDecimalData riskCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 18);
  	public ZonedDecimalData riskCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 21);
  	public FixedLengthStringData riskCessageTos = new FixedLengthStringData(24).isAPartOf(th505Rec, 187);
  	public ZonedDecimalData[] riskCessageTo = ZDArrayPartOfStructure(8, 3, 0, riskCessageTos, 0);
  	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(riskCessageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 0);
  	public ZonedDecimalData riskCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 3);
  	public ZonedDecimalData riskCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 6);
  	public ZonedDecimalData riskCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 9);
  	public ZonedDecimalData riskCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 12);
  	public ZonedDecimalData riskCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 15);
  	public ZonedDecimalData riskCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 18);
  	public ZonedDecimalData riskCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 21);
  	public FixedLengthStringData riskCesstermFroms = new FixedLengthStringData(24).isAPartOf(th505Rec, 211);
  	public ZonedDecimalData[] riskCesstermFrom = ZDArrayPartOfStructure(8, 3, 0, riskCesstermFroms, 0);
  	public FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(riskCesstermFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCesstermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 0);
  	public ZonedDecimalData riskCesstermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 3);
  	public ZonedDecimalData riskCesstermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 6);
  	public ZonedDecimalData riskCesstermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 9);
  	public ZonedDecimalData riskCesstermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 12);
  	public ZonedDecimalData riskCesstermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 15);
  	public ZonedDecimalData riskCesstermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 18);
  	public ZonedDecimalData riskCesstermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 21);
  	public FixedLengthStringData riskCesstermTos = new FixedLengthStringData(24).isAPartOf(th505Rec, 235);
  	public ZonedDecimalData[] riskCesstermTo = ZDArrayPartOfStructure(8, 3, 0, riskCesstermTos, 0);
  	public FixedLengthStringData filler11 = new FixedLengthStringData(24).isAPartOf(riskCesstermTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 0);
  	public ZonedDecimalData riskCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 3);
  	public ZonedDecimalData riskCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 6);
  	public ZonedDecimalData riskCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 9);
  	public ZonedDecimalData riskCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 12);
  	public ZonedDecimalData riskCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 15);
  	public ZonedDecimalData riskCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 18);
  	public ZonedDecimalData riskCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 21);
  	public FixedLengthStringData specind = new FixedLengthStringData(1).isAPartOf(th505Rec, 259);
  	public ZonedDecimalData sumInsMax = new ZonedDecimalData(15, 0).isAPartOf(th505Rec, 260);
  	public ZonedDecimalData sumInsMin = new ZonedDecimalData(15, 0).isAPartOf(th505Rec, 275);
  	public FixedLengthStringData termIssageFrms = new FixedLengthStringData(24).isAPartOf(th505Rec, 290);
  	public ZonedDecimalData[] termIssageFrm = ZDArrayPartOfStructure(8, 3, 0, termIssageFrms, 0);
  	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(termIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData termIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 0);
  	public ZonedDecimalData termIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 3);
  	public ZonedDecimalData termIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 6);
  	public ZonedDecimalData termIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 9);
  	public ZonedDecimalData termIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 12);
  	public ZonedDecimalData termIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 15);
  	public ZonedDecimalData termIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 18);
  	public ZonedDecimalData termIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 21);
  	public FixedLengthStringData termIssageTos = new FixedLengthStringData(24).isAPartOf(th505Rec, 314);
  	public ZonedDecimalData[] termIssageTo = ZDArrayPartOfStructure(8, 3, 0, termIssageTos, 0);
  	public FixedLengthStringData filler13 = new FixedLengthStringData(24).isAPartOf(termIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData termIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 0);
  	public ZonedDecimalData termIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 3);
  	public ZonedDecimalData termIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 6);
  	public ZonedDecimalData termIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 9);
  	public ZonedDecimalData termIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 12);
  	public ZonedDecimalData termIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 15);
  	public ZonedDecimalData termIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 18);
  	public ZonedDecimalData termIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 21);
  	public FixedLengthStringData zdefdivopt = new FixedLengthStringData(4).isAPartOf(th505Rec, 338);
  	public FixedLengthStringData zdivopts = new FixedLengthStringData(28).isAPartOf(th505Rec, 342);
  	public FixedLengthStringData[] zdivopt = FLSArrayPartOfStructure(7, 4, zdivopts, 0);
  	public FixedLengthStringData filler14 = new FixedLengthStringData(28).isAPartOf(zdivopts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zdivopt01 = new FixedLengthStringData(4).isAPartOf(filler14, 0);
  	public FixedLengthStringData zdivopt02 = new FixedLengthStringData(4).isAPartOf(filler14, 4);
  	public FixedLengthStringData zdivopt03 = new FixedLengthStringData(4).isAPartOf(filler14, 8);
  	public FixedLengthStringData zdivopt04 = new FixedLengthStringData(4).isAPartOf(filler14, 12);
  	public FixedLengthStringData zdivopt05 = new FixedLengthStringData(4).isAPartOf(filler14, 16);
  	public FixedLengthStringData zdivopt06 = new FixedLengthStringData(4).isAPartOf(filler14, 20);
  	public FixedLengthStringData zdivopt07 = new FixedLengthStringData(4).isAPartOf(filler14, 24);
  	
  	public FixedLengthStringData prmbasiss = new FixedLengthStringData(6).isAPartOf(th505Rec,370);
	public FixedLengthStringData[] prmbasis = FLSArrayPartOfStructure(6, 1, prmbasiss, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(prmbasiss, 0, FILLER_REDEFINE);
	public FixedLengthStringData prmbasis01 = new FixedLengthStringData(1).isAPartOf(filler15,0);
	public FixedLengthStringData prmbasis02 = new FixedLengthStringData(1).isAPartOf(filler15,1);
	public FixedLengthStringData prmbasis03 = new FixedLengthStringData(1).isAPartOf(filler15,2);
	public FixedLengthStringData prmbasis04 = new FixedLengthStringData(1).isAPartOf(filler15,3);
	public FixedLengthStringData prmbasis05 = new FixedLengthStringData(1).isAPartOf(filler15,4);
	public FixedLengthStringData prmbasis06 = new FixedLengthStringData(1).isAPartOf(filler15,5);

	
  	public FixedLengthStringData filler16 = new FixedLengthStringData(130).isAPartOf(th505Rec, 376, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th505Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th505Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}