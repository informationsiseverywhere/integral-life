/******************************************************************************
 * File Name 		: Hdsxpf.java
 * Author			: sbatra9
 * Creation Date	: 24 June 2020
 * Project			: Integral Life
 * Description		: The Model Class for HDSXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.model;

import java.util.Date;

public class Hdsxpf {

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int plnsfx;
	private String crtable;
	private int hdvbalc;
	private int hintndt;
	private int tranno;
	private int hintos;
	private int rcesdte;
	private String usrprf;
	private String jobnm;
	private Date datime;
	// Constructor
	public Hdsxpf ( ) {
		//Empty Constructor
	}
	public Hdsxpf(Hdsxpf hdsxpf){
		this.chdrcoy=hdsxpf.chdrcoy;
    	this.chdrnum=hdsxpf.chdrnum;
    	this.life=hdsxpf.life;
    	this.jlife=hdsxpf.jlife;
    	this.coverage=hdsxpf.coverage;
    	this.rider=hdsxpf.rider;
    	this.plnsfx=hdsxpf.plnsfx;
    	this.crtable=hdsxpf.crtable;
    	this.hdvbalc=hdsxpf.hdvbalc;
    	this.hintndt=hdsxpf.hintndt;
    	this.tranno=hdsxpf.tranno;
    	this.hintos=hdsxpf.hintos;
    	this.rcesdte=hdsxpf.rcesdte;
    	this.usrprf=hdsxpf.usrprf;
    	this.jobnm=hdsxpf.jobnm;
    	this.datime=hdsxpf.datime;
	}
	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public int getPlnsfx(){
		return this.plnsfx;
	}

	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());
	}
	public String getCrtable() {
		return crtable;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public int getTranno() {
		return tranno;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	
	public int getHdvbalc() {
		return hdvbalc;
	}

	public void setHdvbalc(int hdvbalc) {
		this.hdvbalc = hdvbalc;
	}

	public int getHintndt() {
		return hintndt;
	}

	public void setHintndt(int hintndt) {
		this.hintndt = hintndt;
	}

	public int getHintos() {
		return hintos;
	}

	public void setHintos(int hintos) {
		this.hintos = hintos;
	}

	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	
	// ToString method
		public String toString(){

			StringBuilder output = new StringBuilder();
			output.append("UNIQUE_NUMBER:		");
			output.append(getUniqueNumber());
			output.append("\r\n");
			output.append("CHDRCOY:		");
			output.append(getChdrcoy());
			output.append("\r\n");
			output.append("CHDRNUM:		");
			output.append(getChdrnum());
			output.append("\r\n");
			output.append("LIFE:		");
			output.append(getLife());
			output.append("\r\n");
			output.append("JLIFE:		");
			output.append(getJlife());
			output.append("\r\n");
			output.append("COVERAGE:		");
			output.append(getCoverage());
			output.append("\r\n");
			output.append("RIDER:		");
			output.append(getRider());
			output.append("\r\n");
			output.append("PLNSFX:		");
			output.append(getPlnsfx());
			output.append("\r\n");
			output.append("CRTABLE:		");
			output.append(getCrtable());
			output.append("\r\n");
			output.append("HDVBALC:     ");
			output.append(getHdvbalc());
			output.append("\r\n");
			output.append("HINTNDT:     ");
			output.append(getHintndt());
			output.append("\r\n");
			output.append("TRANNO:		");
			output.append(getTranno());
			output.append("\r\n");
			output.append("HINTOS:		");
			output.append(getHintos());
			output.append("\r\n");
			output.append("RCESDTE:		");
			output.append(getRcesdte());
			output.append("\r\n");
			output.append("USRPRF:		");
			output.append(getUsrprf());
			output.append("\r\n");
			output.append("JOBNM:		");
			output.append(getJobnm());
			output.append("\r\n");
			output.append("DATIME:		");
			output.append(getDatime());
			output.append("\r\n");
			return output.toString();
		}
}
