package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HsudpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:36
 * Class transformed from HSUDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HsudpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 95;
	public FixedLengthStringData hsudrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hsudpfRecord = hsudrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hsudrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hsudrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hsudrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hsudrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hsudrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hsudrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hsudrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hsudrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hsudrec);
	public PackedDecimalData hactval = DD.hactval.copy().isAPartOf(hsudrec);
	public PackedDecimalData hemv = DD.hemv.copy().isAPartOf(hsudrec);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(hsudrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(hsudrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hsudrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hsudrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hsudrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HsudpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HsudpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HsudpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HsudpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HsudpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HsudpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HsudpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HSUDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"PLNSFX, " +
							"HACTVAL, " +
							"HEMV, " +
							"HCNSTCUR, " +
							"TYPE_T, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     crtable,
                                     planSuffix,
                                     hactval,
                                     hemv,
                                     hcnstcur,
                                     fieldType,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		planSuffix.clear();
  		hactval.clear();
  		hemv.clear();
  		hcnstcur.clear();
  		fieldType.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHsudrec() {
  		return hsudrec;
	}

	public FixedLengthStringData getHsudpfRecord() {
  		return hsudpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHsudrec(what);
	}

	public void setHsudrec(Object what) {
  		this.hsudrec.set(what);
	}

	public void setHsudpfRecord(Object what) {
  		this.hsudpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hsudrec.getLength());
		result.set(hsudrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}