/*
 * File: Hdoppuad.java
 * Date: 29 August 2009 22:53:26
 * Author: Quipoz Limited
 * 
 * Class transformed from HDOPPUAD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuadopTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdvdoprec;
import com.csc.life.cashdividends.tablestructures.Th530rec;
import com.csc.life.cashdividends.tablestructures.Th531rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              Cash Dividend Paid Up Addition
*              ------------------------------
*
* This subroutine is called from BH526 via TH500.
*
* It is responsible for withdrawing the cash dividend allocated
* for the current period to purchase a Paid-up Addition cover.
*
* The processing is based on two new tables, TH530 and TH531.
* The paid up addition cover is set up in the form of a HPUA
* record which attaches to the original participating coverage.
*
*  PROCESSING.
*  -----------
*
* 1. Read relevant tables, T5645, T5679, T5688.
* 2. Read HCSD(Cash Dividend Option).
* 3. Read and hold COVR(Coverage/Rider Details).
* 4. Obtain a new issue age from AGECALC with LIFELNB information.
* 5. Read TH531 for paid up addition cover definition.
* 6. Get SI factor ,
*    - calculate term to maturity with DATCON3
*           COVR-RISK-CESS-DATE - HDOP-HDIV-EFFDATE
*      round up to nearest year
*    - read TH530 with key
*           COVR-CRTABLE/Term to maturity/COVR-MORTCLS/COVR-SEX
*      read generic by substituting components with '*' if needed
*    - search TH530 with the issue age for the correct rate
* 7. Calculate SI,
*      HDOP-DIVD-AMOUNT * ((Risk unit * Prem unit) / Rate)
* 8. Obtain the next paid up number by reading the first HPUADOP
*    and set new PUA-NO as HPUADOP-PU-ADD-NBR + 1.
* 9. Write a HPUA record for the new paid-up addition cover.
* 10.Write a HDIV to denote a withdrawal has been taken place.
* 11.Update HDIS for the same reason, by invalid the current one
*    and write a new one.
* 12.Update COVR with the new total SI, by invalid the current
*    one and write a new one with,
*      COVR-VAR-SUM-INSURED = COVR-SUMINS + HPUA-SUMIN
*    if COVR-VAR-SUM-INSURED is not zeroes, simply add HPUA-SUMIN
*    to it.
* 13.Perform account posting for the purchase of PUA. Write double
*    entry to post to receivable and premium income.
*
*
*****************************************************************
*
* </pre>
*/
public class Hdoppuad extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "HDOPPUAD";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private PackedDecimalData wsaaSi = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaNewPuaNo = new ZonedDecimalData(5, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTermToMature = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaTerm = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private FixedLengthStringData wsaaMortcls = new FixedLengthStringData(1).isAPartOf(wsaaItem, 6);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItem, 7);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0);
		/* ERRORS */
	private static final String g549 = "G549";
	private static final String g641 = "G641";
	private static final String itemrec = "ITEMREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String hpuadoprec = "HPUADOPREC";
	private static final String hcsdrec = "HCSDREC";
	private static final String covrrec = "COVRREC";
	private static final String hdivrec = "HDIVREC";
	private static final String hdisrec = "HDISREC";
	private static final String hpuarec = "HPUAREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t1693 = "T1693";
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String th530 = "TH530";
	private static final String th531 = "TH531";
	private CovrTableDAM covrIO = new CovrTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private HpuadopTableDAM hpuadopIO = new HpuadopTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T1693rec t1693rec = new T1693rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Th530rec th530rec = new Th530rec();
	private Th531rec th531rec = new Th531rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Hdvdoprec hdvdoprec = new Hdvdoprec();

	public Hdoppuad() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		hdvdoprec.dividendRec = convertAndSetParam(hdvdoprec.dividendRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		calculatePaidupSi300();
		update400();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		syserrrec.subrname.set(wsaaSubr);
		hdvdoprec.statuz.set(varcom.oK);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		/* Read T5645 to obtain ledger codes for account posting*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.batccoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			databaseError800();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T5679 for filtering COVR with irrelevant statii*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.batccoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(hdvdoprec.batctrcde);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			databaseError800();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Read T5688 for contract/component level account posting*/
		itdmIO.setItemcoy(hdvdoprec.batccoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(hdvdoprec.cnttype);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),hdvdoprec.batccoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),hdvdoprec.cnttype)) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemtabl(t5688);
			itdmIO.setItemitem(hdvdoprec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Read HCSD for Cash Dividend option and method details*/
		hcsdIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hcsdIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hcsdIO.setLife(hdvdoprec.lifeLife);
		hcsdIO.setCoverage(hdvdoprec.covrCoverage);
		hcsdIO.setRider(hdvdoprec.covrRider);
		hcsdIO.setPlanSuffix(hdvdoprec.plnsfx);
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			databaseError800();
		}
		/* Read hold COVR for Coverage details*/
		covrIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		covrIO.setChdrnum(hdvdoprec.chdrChdrnum);
		covrIO.setLife(hdvdoprec.lifeLife);
		covrIO.setCoverage(hdvdoprec.covrCoverage);
		covrIO.setRider(hdvdoprec.covrRider);
		covrIO.setPlanSuffix(hdvdoprec.plnsfx);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			databaseError800();
		}
	}

protected void calculatePaidupSi300()
	{
			calculate310();
		}

protected void calculate310()
	{
		/* Obtain new issue age via AGECALC*/
		/* First,*/
		/* Read T1693 in company 0 for the FSU company*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(hdvdoprec.chdrChdrcoy);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setItemitem(wsaaSubr);
			syserrrec.params.set(itemIO.getParams());
			databaseError800();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		/* Second,*/
		/* Read LIFELNB to obtain the client details*/
		lifelnbIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		lifelnbIO.setChdrnum(hdvdoprec.chdrChdrnum);
		lifelnbIO.setLife(hdvdoprec.lifeLife);
		lifelnbIO.setJlife(hdvdoprec.lifeJlife);
		if (isEQ(hdvdoprec.lifeJlife,SPACES)) {
			lifelnbIO.setJlife("00");
		}
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			databaseError800();
		}
		/* Set up AGECALC to calculate issue age*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.statuz.set(varcom.oK);
		agecalcrec.language.set(hdvdoprec.language);
		agecalcrec.cnttype.set(hdvdoprec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(hdvdoprec.hdivEffdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError900();
		}
		/* Read TH531 for paid up cover definition*/
		itdmIO.setItemcoy(hdvdoprec.batccoy);
		itdmIO.setItemtabl(th531);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),hdvdoprec.batccoy)
		|| isNE(itdmIO.getItemtabl(),th531)
		|| isNE(itdmIO.getItemitem(),covrIO.getCrtable())) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemtabl(th531);
			itdmIO.setItemitem(covrIO.getCrtable());
			itdmIO.setItmfrm(covrIO.getCrrcd());
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		th531rec.th531Rec.set(itdmIO.getGenarea());
		/*    Get SI Factor*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrIO.getRiskCessDate());
		datcon3rec.intDate2.set(hdvdoprec.hdivEffdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			compute(wsaaTermToMature, 6).setRounded(mult(1,datcon3rec.freqFactor));
		}
		/*    Read TH530 to locate the applicable rate for calculating SI*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(hdvdoprec.chdrChdrcoy);
		itdmIO.setItemtabl(th530);
		itdmIO.setItmfrm(covrIO.getCrrcd());
		wsaaCrtable.set(covrIO.getCrtable());
		wsaaTerm.set(wsaaTermToMature);
		wsaaMortcls.set(covrIO.getMortcls());
		wsaaSex.set(covrIO.getSex());
		itdmIO.setFormat(itemrec);
		itdmIO.setStatuz(SPACES);
		while ( !((isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemitem(),wsaaItem)))) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(hdvdoprec.chdrChdrcoy);
			itdmIO.setItemtabl(th530);
			itdmIO.setItmfrm(covrIO.getCrrcd());
			itdmIO.setItemitem(wsaaItem);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				databaseError800();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),th530)
			|| isNE(itdmIO.getItemcoy(),hdvdoprec.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaItem)) {
				if (isEQ(subString(wsaaItem, 5, 4),"****")) {
					itdmIO.setItemitem(wsaaItem);
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(g641);
					databaseError800();
				}
				else {
					if (isNE(subString(wsaaItem, 8, 1),"*")) {
						wsaaItem.setSub1String(8, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 7, 1),"*")) {
							wsaaItem.setSub1String(7, 1, "*");
						}
						else {
							if (isNE(subString(wsaaItem, 5, 2),"**")) {
								wsaaItem.setSub1String(5, 2, "**");
							}
						}
					}
				}
			}
			else {
				th530rec.th530Rec.set(itdmIO.getGenarea());
			}
		}
		
		/* Search for the rate in TH530 with AGEC issue age*/
		/* Firstly,*/
		/* check table has been set up correctly - must have non-zero*/
		/*  risk units otherwise we will be dividing by zero !!*/
		if (isEQ(th530rec.unit,ZERO)) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g549);
			databaseError800();
		}
		/* Calculate SI*/
		if (isEQ(agecalcrec.agerating,0)) {
			compute(wsaaSi, 3).setRounded(mult(hdvdoprec.divdAmount,(div((mult(th530rec.unit,th530rec.premUnit)),th530rec.insprem))));
			zrdecplrec.amountIn.set(wsaaSi);
			a000CallRounding();
			wsaaSi.set(zrdecplrec.amountOut);
			return ;
		}
		if (isLT(agecalcrec.agerating,100)) {
			compute(wsaaSi, 3).setRounded(mult(hdvdoprec.divdAmount,(div((mult(th530rec.unit,th530rec.premUnit)),th530rec.insprm[agecalcrec.agerating.toInt()]))));
		}
		else {
			compute(wsaaIndex, 0).set(sub(agecalcrec.agerating, 99));
			compute(wsaaSi, 3).setRounded(mult(hdvdoprec.divdAmount, (div((mult(th530rec.unit, th530rec.premUnit)), th530rec.instpr[wsaaIndex.toInt()]))));
		}
		zrdecplrec.amountIn.set(wsaaSi);
		a000CallRounding();
		wsaaSi.set(zrdecplrec.amountOut);
	}

protected void update400()
	{
		prepareUpdate410();
	}

protected void prepareUpdate410()
	{
		/* Obtain the next paid up no.*/
		hpuadopIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hpuadopIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hpuadopIO.setLife(hdvdoprec.lifeLife);
		hpuadopIO.setCoverage(hdvdoprec.covrCoverage);
		hpuadopIO.setRider(hdvdoprec.covrRider);
		hpuadopIO.setPlanSuffix(hdvdoprec.plnsfx);
		hpuadopIO.setPuAddNbr(99999);
		hpuadopIO.setFormat(hpuadoprec);
		hpuadopIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hpuadopIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuadopIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, hpuadopIO);
		if (isNE(hpuadopIO.getStatuz(),varcom.oK)
		&& isNE(hpuadopIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuadopIO.getParams());
			databaseError800();
		}
		if (isEQ(hpuadopIO.getStatuz(),varcom.endp)
		|| isNE(hpuadopIO.getChdrcoy(),hdvdoprec.chdrChdrcoy)
		|| isNE(hpuadopIO.getChdrnum(),hdvdoprec.chdrChdrnum)
		|| isNE(hpuadopIO.getLife(),hdvdoprec.lifeLife)
		|| isNE(hpuadopIO.getCoverage(),hdvdoprec.covrCoverage)
		|| isNE(hpuadopIO.getRider(),hdvdoprec.covrRider)
		|| isNE(hpuadopIO.getPlanSuffix(),hdvdoprec.plnsfx)) {
			wsaaNewPuaNo.set(1);
		}
		else {
			compute(wsaaNewPuaNo, 0).set(add(1,hpuadopIO.getPuAddNbr()));
		}
		/* Create a paid up addition cover*/
		hpuaIO.setDataArea(SPACES);
		hpuaIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hpuaIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hpuaIO.setLife(hdvdoprec.lifeLife);
		hpuaIO.setCoverage(hdvdoprec.covrCoverage);
		hpuaIO.setRider(hdvdoprec.covrRider);
		hpuaIO.setPlanSuffix(hdvdoprec.plnsfx);
		hpuaIO.setJlife(hdvdoprec.lifeJlife);
		hpuaIO.setPuAddNbr(wsaaNewPuaNo);
		hpuaIO.setValidflag("1");
		hpuaIO.setTranno(hdvdoprec.newTranno);
		hpuaIO.setAnbAtCcd(agecalcrec.agerating);
		hpuaIO.setSumin(wsaaSi);
		hpuaIO.setSingp(hdvdoprec.divdAmount);
		hpuaIO.setCrrcd(hdvdoprec.hdivEffdate);
		hpuaIO.setRstatcode(t5679rec.setCovRiskStat);
		hpuaIO.setPstatcode(t5679rec.setSngpCovStat);
		hpuaIO.setRiskCessDate(covrIO.getRiskCessDate());
		hpuaIO.setCrtable(th531rec.puCvCode);
		hpuaIO.setDivdParticipant(th531rec.divdParticipant);
		hpuaIO.setFormat(hpuarec);
		hpuaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hpuaIO.getParams());
			databaseError800();
		}
		/* Write a HDIV record to denote the withdrawal*/
		hdivIO.setDataArea(SPACES);
		hdivIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hdivIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hdivIO.setLife(hdvdoprec.lifeLife);
		hdivIO.setCoverage(hdvdoprec.covrCoverage);
		hdivIO.setRider(hdvdoprec.covrRider);
		hdivIO.setPlanSuffix(hdvdoprec.plnsfx);
		hdivIO.setJlife(hdvdoprec.lifeJlife);
		hdivIO.setTranno(hdvdoprec.newTranno);
		hdivIO.setEffdate(hdvdoprec.hdivEffdate);
		hdivIO.setDivdAllocDate(hdvdoprec.effectiveDate);
		hdivIO.setDivdIntCapDate(varcom.vrcmMaxDate);
		hdivIO.setCntcurr(hdvdoprec.cntcurr);
		setPrecision(hdivIO.getDivdAmount(), 2);
		hdivIO.setDivdAmount(mult(hdvdoprec.divdAmount,-1));
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
		hdivIO.setBatccoy(hdvdoprec.batccoy);
		hdivIO.setBatcbrn(hdvdoprec.batcbrn);
		hdivIO.setBatcactyr(hdvdoprec.batcactyr);
		hdivIO.setBatcactmn(hdvdoprec.batcactmn);
		hdivIO.setBatctrcde(hdvdoprec.batctrcde);
		hdivIO.setBatcbatch(hdvdoprec.batcbatch);
		hdivIO.setDivdType("W");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setFormat(hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			databaseError800();
		}
		/*  Now update HDIS to denote withdrawal*/
		hdisIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hdisIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hdisIO.setLife(hdvdoprec.lifeLife);
		hdisIO.setCoverage(hdvdoprec.covrCoverage);
		hdisIO.setRider(hdvdoprec.covrRider);
		hdisIO.setPlanSuffix(hdvdoprec.plnsfx);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(hdvdoprec.newTranno);
		setPrecision(hdisIO.getBalSinceLastCap(), 2);
		hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(),(mult(hdvdoprec.divdAmount,-1))));
		hdisIO.setSacscode(SPACES);
		hdisIO.setSacstyp(SPACES);
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		/* Update COVR with the new SI calculated*/
		covrIO.setValidflag("2");
		covrIO.setCurrto(hdvdoprec.effectiveDate);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			databaseError800();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setCurrfrom(hdvdoprec.effectiveDate);
		covrIO.setTranno(hdvdoprec.newTranno);
		if (isEQ(covrIO.getVarSumInsured(),ZERO)) {
			setPrecision(covrIO.getVarSumInsured(), 2);
			covrIO.setVarSumInsured(add(covrIO.getSumins(),hpuaIO.getSumin()));
		}
		else {
			setPrecision(covrIO.getVarSumInsured(), 2);
			covrIO.setVarSumInsured(add(covrIO.getVarSumInsured(),hpuaIO.getSumin()));
		}
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			databaseError800();
		}
		/* Get ready to post accounting LIFACMV*/
		/*    Fields which require initialisation only once.*/
		wsaaJrnseq.set(ZERO);
		lifacmvrec.batccoy.set(hdvdoprec.batccoy);
		lifacmvrec.batcbrn.set(hdvdoprec.batcbrn);
		lifacmvrec.batcactyr.set(hdvdoprec.batcactyr);
		lifacmvrec.batcactmn.set(hdvdoprec.batcactmn);
		lifacmvrec.batctrcde.set(hdvdoprec.batctrcde);
		lifacmvrec.batcbatch.set(hdvdoprec.batcbatch);
		lifacmvrec.rldgcoy.set(hdvdoprec.batccoy);
		lifacmvrec.genlcoy.set(hdvdoprec.batccoy);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.rdocnum.set(hdvdoprec.chdrChdrnum);
		lifacmvrec.tranref.set(hdvdoprec.chdrChdrnum);
		lifacmvrec.tranno.set(hdvdoprec.newTranno);
		lifacmvrec.effdate.set(hdvdoprec.effectiveDate);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(hdvdoprec.effectiveDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.substituteCode[1].set(hdvdoprec.cnttype);
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.substituteCode[6].set(hdvdoprec.crtable);
		}
		else {
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.chdrChdrcoy);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(hdvdoprec.batctrcde);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(hdvdoprec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
		/* Post receivable for the purchase of PUA*/
		lifacmvrec.origamt.set(hdvdoprec.divdAmount);
		lifacmvrec.origcurr.set(hdvdoprec.cntcurr);
		wsaaRldgacct.set(SPACES);
		wsaaRldgacct.set(hdvdoprec.chdrChdrnum);
		wsaaSub1.set(1);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError900();
		}
		/* Account posting for premium income*/
		if (componLevelAccounted.isTrue()) {
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(hdvdoprec.chdrChdrnum);
			wsaaPlan.set(hdvdoprec.plnsfx);
			wsaaRldgLife.set(hdvdoprec.lifeLife);
			wsaaRldgCoverage.set(hdvdoprec.covrCoverage);
			wsaaRldgRider.set(hdvdoprec.covrRider);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			wsaaSub2.set(4);
		}
		else {
			wsaaRldgacct.set(SPACES);
			wsaaRldgacct.set(hdvdoprec.chdrChdrnum);
			wsaaSub2.set(2);
		}
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub2.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub2.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub2.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub2.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub2.toInt()]);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError900();
		}
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(hdvdoprec.batccoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(hdvdoprec.cntcurr);
		zrdecplrec.batctrcde.set(hdvdoprec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError900();
		}
		/*A090-EXIT*/
	}
protected void databaseError800()
	{
					start810();
					exit870();
				}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
					start910();
					exit970();
				}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
