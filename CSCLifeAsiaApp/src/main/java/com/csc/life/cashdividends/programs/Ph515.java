/*
 * File: Ph515.java
 * Date: 30 August 2009 1:04:45
 * Author: Quipoz Limited
 * 
 * Class transformed from PH515.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.ClbaddbTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.screens.Sh515ScreenVars;
import com.csc.life.cashdividends.tablestructures.Th500rec;
import com.csc.life.cashdividends.tablestructures.Th505rec;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcddmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgwTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Term (Par with Dividend) Component Change Generic
* -------------------------------------------------
* This screen/program is used to capture any changes to the
* coverage/rider details for traditional cash dividend
* component.
*
*  Initialise.
*  -----------
*
*  Skip this section  if    returning    from    an    optional
*  selection (current stack position action flag = '*').
*
*  Read  CHDRLIF  (RETRV)  in    order  to  obtain the contract
*  header information.
*
*  The key for Coverage/rider  to    be    worked  on  will  be
*  available  from  COVR.   This is obtained by using the RETRV
*  function.
*
*  Firstly  we  need  to check  if  there  has  already    been
*  an   alteration   to   this     coverage/rider  within  this
*  transaction. If this  is   the  case  there  will    be    a
*  record      present     on    the  temporary  coverage/rider
*  transaction  file    COVTPF.    Read  this  file  using  the
*  logical  view    COVTMAJ  and  the  key  from  the retrieved
*  coverage/rider record. If  a  record  is  found  go  to  the
*  SET-UP-SCREEN section.
*
*  Compare  the  plan  suffix  of  the  retrieved  record  with
*  the contract header no of policies  in  plan    field.    If
*  the  plan  suffix  is '00' go to SET-UP-COVT.  If it is less
*  than or equal to this field the we need  to  'breakout'  the
*  amount fields as follows:
*
*  Divide the COVR sum assured  field  by  the CHDR no of
*                policies in plan.
*  Divide  the COVR premium  field  by  the  CHDR  no  of
*                policies in plan.
*
*  ******* SET-UP-COVT.
*
*  Move    the  appropriate fields  from  the  COVR  record  to
*  the COVTMAJ record.
*
*  ******* SET-UP-SCREEN.
*
*  Read  the  contract definition  details  from   T5688    for
*  the  contract  type  held on  CHDRLIF.  Access  the  version
*  of this item for the original commencement date of the risk.
*
*  Read  the  general  coverage/rider  details from  T5687  and
*  the  traditional/term    edit    rules  from  TH505  for the
*  coverage type held  on  COVTMAJ.  Access  the    version  of
*  these item for the original commencement date of the risk.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain the life assured  and joint-life details (if any)
*  do the following;-
*
*       - read the life  details  using  LIFEMAJ  (life  number
*       from  COVTMAJ,  joint  life number  '00').  Look up the
*       name from the client details (CLTS)  and  format  as  a
*       "confirmation name".
*
*       -  read  the  joint    life details using LIFEMAJ (life
*       number from COVTMAJ,  joint  life  number  '01').    If
*       found,  look  up  the  name  from  the  client  details
*       (CLTS) and format as a "confirmation name".
*
*  To determine which premium  calculation    method    to  use
*  decide  whether  or  not  it  is  a   Single  or  Joint-life
*  case  (it is a joint life case, the  joint    life    record
*  was  found  above). Then:
*
*       -  if  it  is a single-life case use, the single-method
*       from T5687. The age to be used  for validation will  be
*       the age of the main life.
*
*       -  if  the joint-life  indicator (from T5687) is blank,
*       and  if  it  is  a    Joint-life    case,    use    the
*       joint-method  from  T5687.  The  age  to  be  used  for
*       validation will be the age of the main life.
*
*       -  if the Joint-life  indicator  is  'N',   then    use
*       the  Single-method.    But,  if  there  is a joint-life
*       (this must be a rider to have got  this  far)    prompt
*       for  the  joint  life  indicator  to  determine   which
*       life  the rider is to attach to.  In all  other  cases,
*       the  joint  life  indicator should be non-displayed and
*       protected.  The  age  to  be used for  validation  will
*       be  the    age  of the main or joint life, depending on
*       the one selected.
*
*       - use the premium-method  selected  from   T5687,    if
*       not  blank, to access T5675.  This gives the subroutine
*       to use for the calculation.
*
*      COVERAGE/RIDER DETAILS
*
*  The fields to be displayed  on  the  screen  are  determined
*  from the entry in table TH505 read earlier as follows:
*
*       -  if  the  maximum and minimum sum insured amounts are
*       both zero, non-display  and  protect  the  sum  insured
*       amount.    If  the  minimum and maximum  are  both  the
*       same, display the  amount  protected.  Otherwise  allow
*       input.
*
*       -    if    all    the   valid   mortality  classes  are
*       blank, non-display and protect this   field.  If  there
*       is  only one mortality class, display  and  protect  it
*       (no validation will be required).
*
*       - if all the valid  lien codes are  blank,  non-display
*       and  protect this field. Otherwise, this is an optional
*       field.
*
*       - if the cessation  AGE  section  on  TH505  is  blank,
*       protect the two age related fields.
*
*       -    if  the  cessation    TERM  section  on  TH505  is
*       blank, protect the two term related fields.
*
*       - using the age next  birthday  (ANB  at   RCD)    from
*       the  applicable  life (see above), look up Issue Age on
*       the AGE and TERM sections.  If the  age fits into  only
*       one  "slot"  in  one  of these  sections,  and the risk
*       cessation limits are the same, default   and    protect
*       the    risk  cessation fields.  Also  do the  same  for
*       the  premium   cessation  details.      In  this  case,
*       also    calculate    the    risk  and premium cessation
*       dates.
*
*       -  please note that it  is  possible  for   the    risk
*       and  premium  cessation  dates    to   be  overwritten,
*       i.e.  even after they are calculated.
*
*      OPTIONS AND EXTRAS
*
*  If options and extras are  not   allowed    (as  defined  by
*  TH505) non-display and protect the fields.
*
*  Otherwise,  read  the  options  and  extras  details for the
*  current coverage/rider.   If any records  exist,    put    a
*  '+'    in   the Options/Extras indicator (to show that there
*  are some).
*
*  If options and extras  are  not  allowed,  then  non-display
*  and  protect  the  special  terms    narrative  clause  code
*  fields. If special terms are present,    then    obtain  the
*  clause  codes  from  the  coverage  record and output to the
*  screen.
*
* REASSURANCE
*
* If Reassurance is not allowed ( as defined by T5687 ), then
* non-display and protect the field ( SH515-RATYPIND ).
*
* Otherwise,  read  the  Reassurance  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the  Reassurance  indicator (to show that there are some).
*
*  Finally after setting up  all  of  the    screen  attributes
*  move over the relevant fields from COVTMAJ where applicable.
*
*  Validation.
*  -----------
*
*  Skip  this  section  if    returning    from    an  optional
*  selection (current stack position action flag = '*').
*
*  If CF12 (KILL) is requested then skip the validation.
*
*  Before the premium amount  is  calculated,  the screen  must
*  be  valid.    So  all  editing    is  completed  before  the
*  premium is calculated.
*
*  Table TH505 (previously read)  is    used    to  obtain  the
*  editing  rules.    Edit  the  screen according  to the rules
*  defined by the help. In particular:-
*
*       1) Check the  sum-assured,  if   applicable,    against
*       the limits.
*
*       2)  Check  the  consistency  of   the risk age and term
*       fields and premium age and term  fields.  Either,  risk
*       age  and  and  premium age must be used  or  risk  term
*       and premium term must  be  used.    They  must  not  be
*       combined.  Note that these only need validating if they
*       were not defaulted.
*
*       3) Mortality-Class, if the  mortality    class  appears
*       on  a  coverage/rider  screen  it is a compulsory field
*       because  it  will    be  used  in    calculating    the
*       premium    amount.    The  mortality class entered must
*       one  of the ones in the edit rules table.
*
*  Calculate the following:-
*
*       - risk cessation date
*
*       - premium cessation date
*
*       - note, the risk and premium  cessation  dates  may  be
*       overwritten by the user.
*
*  OPTIONS AND EXTRAS
*
*  If  options/extras  already  exist, there  will  be a '+' in
*  this field.  A request to access  the  details  is  made  by
*  entering  'X'.    No other values  (other  than  blank)  are
*  allowed.  If  options  and  extras  are  requested,  DO  NOT
*  CALCULATE THE PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance  already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
*  PREMIUM CALCULATION.
*  --------------------
*
*  The    premium   amount is  required  on  all  products  and
*  all validating  must  be   successfully  completed    before
*  it    is  calculated.  If  there  is    no   premium  method
*  defined (i.e the relevant code  was  blank),    the  premium
*  amount  must  be  entered.  Otherwise,  it  is  optional and
*  always calculated.
*
*  To  calculate  it,    call    the    relevant    calculation
*  subroutine worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       -  Joint life number (if the screen indicator is set to
*       'J' ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. term cessation date)
*       - Currency
*       - Sum insured
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date of transaction)
*
*  Subroutine may look up:
*
*       - Life and Joint-life details
*       - Options/Extras
*
*  Having calculated it, the    entered    value,  if  any,  is
*  compared  with  it  to check that it  is  within  acceptable
*  limits of the automatically  calculated  figure.    If    it
*  is    less    than    the  amount    calculated  and  within
*  tolerance  then  the  manually entered  amount  is  allowed.
*  If    the    entered  value  exceeds the calculated one, the
*  calculated value is used.
*
*  To check the tolerance  amount,  read  the  tolerance  limit
*  from  T5667  (item  is  transaction  and  coverage  code, if
*  not found, item and '****'). Although this  is    a    dated
*  table, just read the latest one (using ITEM).
*
*      If 'CALC' was entered then re-display the screen.
*
*  Updating.
*  ---------
*
*  Updating  occurs  if  any of the  fields  on the screen have
*  been amended from the original COVTMAJ details.
*
*  If the 'KILL' function key was pressed skip the updating.
*
*  Update the COVTMAJ record with the details from  the  screen
*  and write/rewrite the record to the database.
*
*  IF no Coverage/rider details have been changed, but
*    Reassurance or Options/Extras details have been amended,
*    we force the program to produce a COVT record so that the
*    Component change AT module is executed, thus writing a PTRN
*    indicating that something has happened to the Coverage/Rider
*    details.
*
*  Next Program.
*  -------------
*
*  The    first  thing  to   consider   is   the   handling  of
*  an options/extras request. If  the    indicator  is  'X',  a
*  request  to  visit options and extras has been made. In this
*  case:
*
*       - change the options/extras request indicator to '?',
*
*       - save the next 8 programs from the program stack,
*
*       - call GENSSWCH with and  action  of  'A'  to  retrieve
*       the  program  switching  required, and move them to the
*       stack,
*
*       - set the current stack "action" to '*',
*
*       - add one to the program pointer and exit.
*
*  On return from this  request,  the  current  stack  "action"
*  will  be    '*' and the  options/extras  indicator  will  be
*  '?'.  To handle the return from options and extras:
*
*       -  calculate  the  premium  as  described   above    in
*       the  'Validation'  section,  and  check    that  it  is
*       within the tolerance limit,
*
*       -  blank  out  the  stack   "action",    restore    the
*       saved programs to the program stack,
*
*       -  if  the  tolerance   checking  results  in  an error
*       being detected, set WSSP-NEXTPROG  to    the    current
*       screen name (thus returning to re-display the screen).
*
* If Reassurance has been requested (the indicator = 'X'), then..
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with and  action  of 'B' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance  indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
*  If  control  is passed to this  part  of the 4000 section on
*  the way  out  of the program,  ie.   after    screen    I/O,
*  then    the  current  stack  position  action  flag  will be
*  blank.  If the 4000 section  is  being    performed    after
*  returning      from  processing  another  program  then  the
*  current  stack  position action flag will be '*'.
*
*  If 'Enter' has been pressed  add 1 to  the  program  pointer
*  and exit.
*
*  If  'KILL'  has  been requested, (CF12), then move spaces to
*  the current program entry in the program stack and exit.
*
*****************************************************************
* </pre>
*/
public class Ph515 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH515");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaWorkingAnb = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaPovrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPovrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaPcestermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcestermStore = new ZonedDecimalData(3, 0);
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private PackedDecimalData wsaaCalcPrem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private String wsaaPovrModified = "N";

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");
	private FixedLengthStringData keyCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData keyAgeterm = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData keyMortcls = new FixedLengthStringData(1);
	private FixedLengthStringData keySex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator policyLevel = new Validator(wsaaPlanproc, "N");
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	private Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
	private Validator compApp = new Validator(wsaaFlag, "S", "P");
	private Validator revComp = new Validator(wsaaFlag, "R");
	private Validator addLife = new Validator(wsaaFlag, "D");
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaStoreSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNextSeqnbr = new PackedDecimalData(3, 0).init(999);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaOldPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(17, 0);
	private ZonedDecimalData wsaaPreviousSumins = new ZonedDecimalData(17, 0).init(ZERO);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSuminsDiff = new PackedDecimalData(17, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaOldMortcls = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaIfSuminsChanged = new FixedLengthStringData(1);
	private Validator wsaaSuminsChanged = new Validator(wsaaIfSuminsChanged, "I", "D");
	private Validator wsaaSuminsIncreased = new Validator(wsaaIfSuminsChanged, "I");
	private Validator wsaaSuminsDecreased = new Validator(wsaaIfSuminsChanged, "D");

	private FixedLengthStringData wsaaIfPremChanged = new FixedLengthStringData(1);
	private Validator wsaaPremChanged = new Validator(wsaaIfPremChanged, "I", "D");
	private Validator wsaaPremIncreased = new Validator(wsaaIfPremChanged, "I");
	private Validator wsaaPremDecreased = new Validator(wsaaIfPremChanged, "D");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaOptext = new FixedLengthStringData(1);
	private Validator optextYes = new Validator(wsaaOptext, "Y");
	private Validator optextNo = new Validator(wsaaOptext, "N");

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler4, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);

		/* Plan selection.*/
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private FixedLengthStringData filler6 = new FixedLengthStringData(49).isAPartOf(wsaaCovrKey, 15, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private String wsaaFirstTime = "";
		/* WSBB-JOINT-LIFE-DETS */
	private PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreZbinstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreSingp = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreInstprem = new ZonedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsccSingPremFlag = new FixedLengthStringData(1);
	private Validator singlePremium = new Validator(wsccSingPremFlag, "Y");
	private FixedLengthStringData wsaaLextUpdates = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	/* BRD-306 START */
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTempPrev = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaPrevDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDue = new ZonedDecimalData(8, 0).setUnsigned();
	/* BRD-306 END */

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client Bank Account Details*/
	private ClbaddbTableDAM clbaddbIO = new ClbaddbTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Cover/Rider Logical View*/
	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Extras & Options Reversals logical*/
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Mandate Logical File*/
	private MandTableDAM mandIO = new MandTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*PCDD for Major Alts*/
	private PcddmjaTableDAM pcddmjaIO = new PcddmjaTableDAM();
		/*Temporary Commission Split*/
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
		/*Premium breakdown additions to the covR.*/
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Gensswrec gensswrec = new Gensswrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Freqcpy freqcpy = new Freqcpy();
	private T2240rec t2240rec = new T2240rec();
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T5690rec t5690rec = new T5690rec();
	private T6005rec t6005rec = new T6005rec();
	private Th500rec th500rec = new Th500rec();
	private Th505rec th505rec = new Th505rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Th528rec th528rec = new Th528rec();// fwang3 ICIL-560
	private Wssplife wssplife = new Wssplife();
	private Sh515ScreenVars sv = ScreenProgram.getScreenVars( Sh515ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private String occuptationCode="";
	private boolean loadingFlag = false;/* BRD-306 */
	private boolean prmbasisFlag=false;
	private boolean dialdownFlag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
	private PackedDecimalData wsaaCashValArr = new PackedDecimalData(17, 2);
	/*
	 * fwang3 ICIL-560
	 */
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private boolean hasCashValue = false;
	private PackedDecimalData wsaaHoldSumin = new PackedDecimalData(15, 0);
	private boolean chinaLocalPermission;
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList = new ArrayList<Covrpf>();
	
	//ICIL-1310 Starts
	private boolean occupationFlag = false;
	private String occup = null;
	private String occupClass =null;
	private Itempf itempf = null;
	private T3644rec t3644rec = new T3644rec();
	private Ta610rec ta610rec = new Ta610rec();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private String occupFeature = "NBPRP056";
	private String itemPFX = "IT";
	//ICIL-1310 End
	private List<Itempf> ta610List = new ArrayList<Itempf>();	//ICIL-1494
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();	//ICIL-1494
	//ILJ-47 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-47 End
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setScreen1060, 
		exit1090, 
		exit1190, 
		preExit, 
		exit2090, 
		checkSuminCont2120, 
		checkRcessFields2120, 
		validDate2140, 
		datesCont2140, 
		ageAnniversary2140, 
		check2140, 
		checkOccurance2140, 
		checkTermFields2150, 
		checkComplete2160, 
		checkMortcls2170, 
		loop2175, 
		checkLiencd2180, 
		loop2185, 
		checkMore2190, 
		cont, 
		exit2290, 
		mop2410, 
		updatePcdt3060, 
		exit3090, 
		exit3290, 
		exit50z0, 
		exit5290, 
		exit5390, 
		covtExist6025, 
		itemCall7023, 
		a250CallTaxSubr, 
		a290Exit
	}

	public Ph515() {
		super();
		screenVars = sv;
		new ScreenModel("Sh515", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					contractHeader1020();
					contractCoverage1030();
					headerToScreen1040();
					lifeDetails1050();
				case setScreen1060: 
					setScreen1060();
					commisionSplit1070();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/* If returning from a multiple selection then go to exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		occupationFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), occupFeature, appVars, itemPFX);	//ICIL-1310		
		/* Move WSSP key details to working storage for later updates*/
		/*   and table reads.*/
		/* Move WSSP FLAG to working storage for later program condition*/
		/*   checks.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaFlag.set(wsspcomn.flag);
		/* Intialise the Screen.*/
		
		sv.dataArea.set(SPACES);
		sv.anbAtCcd.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		prmbasisFlag=false;
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		sv.adjustageamt.set(0);
		/*BRD-306 END */
		//ILIFE-3975
		sv.dialdownoption.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.sumin.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.comind.set(SPACES);
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		wsaaPlanSuffix.set(ZERO);
		wsaaSumin.set(ZERO);
		wsaaOldPrem.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaPremDiff.set(ZERO);
		wsaaSuminsDiff.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaIndex.set(ZERO);
		sv.cashvalarer.set(ZERO);//ICIL-299
		wsaaCashValArr.set(ZERO);//ICIL-299
		wsaaFirstTaxCalc.set("Y");
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);	//ICIL-1494
		}
		//ILJ-47 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{
			sv.rcessageOut[varcom.nd.toInt()].set("Y");			
		}
		//ILJ-47 End
	}

protected void contractHeader1020()
	{
		/* Retrieve contract header information.*/
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
	chdrpf = chdrpfDAO.getCacheObject(chdrpf);
	if(null==chdrpf) {
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		else {
			chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
			if(null==chdrpf) {
				fatalError600();
			}
			else {
				chdrpfDAO.setCacheObject(chdrpf);
			}
		}
	}
		/*BRD-306 START */
		sv.cnttype.set(chdrpf.getCnttype());/* IJTI-1386 */
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());/* IJTI-1386 */
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		sv.effdate.set(chdrpf.getPtdate());
		loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
	
	}

protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covtmjaIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtmjaIO.getChdrnum().toString());
	rcvdPFObject.setLife(covtmjaIO.getLife().toString());
	rcvdPFObject.setCoverage(covtmjaIO.getCoverage().toString());
	rcvdPFObject.setRider(covtmjaIO.getRider().toString());
	rcvdPFObject.setCrtable(wsaaCrtable.toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}
protected void getNextEffdate1400()
{
	/*GET-EFFDATE*/
	datcon2rec.freqFactor.set(1);
	datcon2rec.frequency.set(chdrpf.getBillfreq());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz,varcom.bomb)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	wsaaTempPrev.set(datcon2rec.intDate1);
	datcon2rec.intDate1.set(datcon2rec.intDate2);
	wsaaFreq.add(1);
	/*EXIT*/
}
	/**
	 * @author fwang3
	 * ICIL-560
	 * @param items
	 */
	private void loadTh528(List<Itempf> items) {
		String item4parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "" + covrpf.getMortcls()	+ "" + covrpf.getSex();
		String item2parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "**";
		for (Itempf item : items) {
			if (covrpf.getCrrcd() == item.getItmfrm().intValue()) {
				if (item.getItemitem().equals(item4parts)) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
				if (item.getItemitem().equals(item2parts)) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
				if (item.getItemitem().equals(covrpf.getCrtable() + "****")) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
			}
		}
	}
	
	/**
	 * fwang3 ICIL-560
	 */
	private void processCashValue() {
		chinaLocalPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM007", appVars, "IT");
		if(chinaLocalPermission) {
			List<Itempf> items = itemDAO.getItemsbyItemcodeLikeOperator("IT", wsspcomn.company.toString(), tablesInner.th528.toString(), covrpf.getCrtable());/* IJTI-1386 */
			if (items.isEmpty()) {
				sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
				hasCashValue = false;
			} else {
				loadTh528(items);
				hasCashValue = true;
			}
		} else {
			sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
		}
	}
	
protected void contractCoverage1030()
	{
		/* Retrieve the coverage details saved from either p5131 (modify/*/
		/*   enquiry) or p5128 (add).*/
	covrpf = covrpfDAO.getCacheObject(covrpf);
	if(null==covrpf) {
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
	}
	else {
		covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
				covrpf.getRider(),covrmjaIO.getPlanSuffix().toInt(),covrpf.getValidflag());/* IJTI-1386 */
		if(null==covrpf) {
			fatalError600();
		}
	else {
		covrpfDAO.setCacheObject(covrpf);
		}
	}
}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		processCashValue();//fwang3 ICIL-560
		/* Read T2240 for age definition.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRMJA-CNTTYPE        TO ITEM-ITEMITEM.                */
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM                 */
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				} //MTL002
			}
		}
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());/* IJTI-1386 */
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		/*    MOVE 'N'                    TO SH515-ZAGELIT-OUT (HI).*/
		/* Read the PAYR record to get the Billing Details.*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		payrIO.setPayrseqno(covrpf.getPayrseqno());
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		wsaaCrtable.set(covrpf.getCrtable());
		/* Release the coverage details for processing of Whole Plan.*/
		/*   i.e. If entering on an action of Component ADD then the*/
		/*        Component to be added must exist for all policies*/
		/*        within the plan.*/
		/*   i.e. If entering on an action of Component Modify/Enquiry*/
		/*        and Whole Plan selected then we will process the*/
		/*        component for all policies within the plan.*/
		if (isEQ(wsaaPlanSuffix,ZERO)) {
			covrpf = covrpfDAO.getCacheObject(covrpf);
		}
		/*    If modify of Component requested and the component*/
		/*    in question is a Single premium Component then we move 'y'*/
		/*    to comp enquiry and spaces to comp modify and an error*/
		/*    informing the user that single premium component cannot*/
		/*    be modified.*/
		if (modifyComp.isTrue()
		&& isEQ(payrIO.getBillfreq(),ZERO)) {
			wsaaFlag.set("I");
			wsccSingPremFlag.set("Y");
		}
	}

protected void headerToScreen1040()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1100();
	}

protected void lifeDetails1050()
	{
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		/* PERFORM 1200-CALC-AGE.                                       */
		if (addComp.isTrue()) {
			calcAge1200();
			wsaaAnbAtCcd.set(wsaaWorkingAnb);
		}
		else {
			wsaaAnbAtCcd.set(covrpf.getAnbAtCcd());
		}
		/* MOVE WSAA-WORKING-ANB       TO WSAA-ANB-AT-CCD.              */
		wsaaCltdob.set(lifemjaIO.getCltdob());
		wsaaSex.set(lifemjaIO.getCltsex());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		if(isNE(cltsIO.getStatcode(), SPACES)&& (incomeProtectionflag||premiumflag)){
			occuptationCode=cltsIO.getStatcode().toString();
		}
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbAnbAtCcd.set(0);
			wsaaLifeind.set("S");
			wsbbSex.set(SPACES);
			goTo(GotoLabel.setScreen1060);
		}
		else {
			wsaaLifeind.set("J");
		}
		if (addComp.isTrue()) {
			calcAge1200();
			wsbbAnbAtCcd.set(wsaaWorkingAnb);
		}
		else {
			wsbbAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		wsbbCltdob.set(lifemjaIO.getCltdob());
		wsbbSex.set(lifemjaIO.getCltsex());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void setScreen1060()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());/* IJTI-1386 */
		sv.numpols.set(chdrpf.getPolinc().toString());
		sv.life.set(lifemjaIO.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		sv.currcd.set(payrIO.getCntcurr());
		if (isEQ(sv.paycurr,SPACES)) {
			sv.paycurr.set(payrIO.getCntcurr());
		}
		//wsaaCovrKey.set(covrmjaIO.getDataKey());
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)
		&& !addComp.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			covrpf.setPlanSuffix(9999);
			covtmjaIO.setPlanSuffix(9999);
			//covrmjaIO.setFunction(varcom.begn);
			covrpfList=covrpfDAO.selectCovrRecord(covrpf);
			covtmjaIO.setFunction(varcom.begn);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanproc.set("Y");
		}
		else {
			wsaaPlanproc.set("N");
			//covtmjaIO.setDataKey(c.getDataKey());
			covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
			covtmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		dialdownFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag || premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends
		exclFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
		if (modifyComp.isTrue()
		&& isNE(covrpf.getCpiDate(),ZERO)
		&& isNE(covrpf.getCpiDate(),varcom.vrcmMaxDate)) {
			scrnparams.errorCode.set(errorsInner.f862);
		}
		

		policyLoad5000();
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getCrtable(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());/* IJTI-1386 */
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}


protected void commisionSplit1070()
	{
		checkPcdt5700();
		if (isNE(tr52drec.txcode, SPACES)
		&& isEQ(t5687rec.bbmeth, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("N");
			sv.linstamtOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void loadHeading1100()
	{
		try {
			loadScreen1110();
		}
		catch (GOTOException e){
		}
	}

protected void loadScreen1110()
	{
		wsaaHeading.set(SPACES);
		for (wsaaX.set(30); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		wsaaZ.set(0);
		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1130();
		}
		wsaaX.add(1);
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1190);
	}

protected void moveChar1130()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void calcAge1200()
	{
		init1210();
	}

protected void init1210()
	{
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());/* IJTI-1386 */
		agecalcrec.intDate1.set(lifemjaIO.getCltdob());
		agecalcrec.intDate2.set(payrIO.getBtdate());
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaWorkingAnb.set(agecalcrec.agerating);
	}

protected void setupBonus1300()
	{
			para1300();
		}

protected void para1300()
	{
		sv.bappmethOut[varcom.pr.toInt()].set("N");
		sv.bappmethOut[varcom.nd.toInt()].set("N");
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemitem(covtmjaIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(itemIO.getGenarea());
		if (isEQ(t6005rec.ind,"1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (singlePremium.isTrue()) {
			scrnparams.errorCode.set(errorsInner.f008);
		}
		wsaaOldSumins.set(sv.sumin);
		wsaaOldPrem.set(sv.instPrem);
		readPovr8000();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(),wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(),wsaaPovrLife)
		|| isNE(povrIO.getCoverage(),wsaaPovrCoverage)
		|| isNE(povrIO.getRider(),wsaaPovrRider)) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()],"Y")
		&& (isNE(sv.pcesdteErr,SPACES)
		|| isNE(sv.rcesdteErr,SPACES))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			a200CheckCalcTax();
		}
	}

protected void callScreenIo2010()
	{
		/* Display the screen.                                             */
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2001();
			if(occupationFlag) checkOccupation();	//ICIL-1310
			cont2030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'SH515IO' USING        SCRN-SCREEN-PARAMS               */
		/*                                SH515-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF12 (KILL) is requested*/
		/* then skip the remainder of the validation.*/
		wsaaIfSuminsChanged.set("N");
		wsaaIfPremChanged.set("N");
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* Check the situation where the date fields are protected but*/
		/* age and term fields differ from the last call to the premium*/
		/* calculation routine. Blank the dates if ages or terms have*/
		/* changed hence ensuring a re-calculation of dates & premium.*/
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()],"Y")
		&& (isNE(sv.premCessAge,wsaaPcesageStore)
		|| isNE(sv.riskCessAge,wsaaRcesageStore)
		|| isNE(sv.premCessTerm,wsaaPcestermStore)
		|| isNE(sv.riskCessTerm,wsaaRcestermStore))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
		}
		/*VALIDATE*/
		/* If  in  enquiry  mode,  skip  all field validation EXCEPT the*/
		/* options/extras indicator.*/
		/*    IF NOT COMP-ENQUIRY                                          */
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			editCoverage2100();
			editPayeeDetails2400();
		}
	}

	//ICIL-1310 Starts
	protected void checkOccupation()	{
		occup = lifepfDAO.getOccRecord(wsspcomn.company.toString(), sv.chdrnum.toString().trim(), sv.life.toString().trim());
		if (occup != null && !occup.trim().equals("")) {
			itempf = new Itempf();
			itempf.setItempfx(itemPFX);
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.ta610.toString());
			itempf.setItemitem(covrpf.getCrtable()); //IJTI-1793
			itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
			itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
			ta610List = itemDAO.findByItemDates(itempf);	//ICIL-1494
			if (ta610List.size()>0 && ta610List.get(0).getGenarea()!=null) {
				ta610rec.tA610Rec.set(StringUtil.rawToString(ta610List.get(0).getGenarea()));
				for (int i = 1; i <= 10; i++) {
					if (isEQ(ta610rec.occclassdes[i], occup)) {
						scrnparams.errorCode.set(errorsInner.rrsu);
						return;
					}
				}
				getOccupationClass();
				if(occupClass!=null) {
					for (int i = 1; i <= 10; i++) {
						if (isEQ(ta610rec.occcode[i], occupClass)) {
							scrnparams.errorCode.set(errorsInner.rrsu);
							return;
						}
					}
				}
			}
		}
	}
	protected void getOccupationClass() {
		itempf = new Itempf();
		itempf.setItempfx(itemPFX);
		itempf.setItemcoy(wsspcomn.fsuco.toString());
		itempf.setItemtabl(tablesInner.t3644.toString());
		itempf.setItemitem(occup.trim());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf != null) {
			t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			occupClass = t3644rec.occupationClass.toString();
		}
	}
	//ICIL-1310 End

protected void cont2030()
	{
		/* If  options/extras already exist, there will be a '+' in this*/
		/*   field. A request  to access the details is made by entering*/
		/*   'X'. No other  values  (other  than  blank) are allowed. If*/
		/*   options and extras  are  requested,  DO  NOT  CALCULATE THE*/
		/*   PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind,"+")
		&& isNE(sv.optextind,"X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		if (isNE(sv.comind, " ")
		&& isNE(sv.comind,"+")
		&& isNE(sv.comind,"X")) {
			sv.comindErr.set(errorsInner.g620);
		}
		/* Check the bank account details indicator.*/
		if (isNE(sv.bankaccreq, " ")
		&& isNE(sv.bankaccreq,"+")
		&& isNE(sv.bankaccreq,"X")) {
			sv.bankaccreqErr.set(errorsInner.g620);
		}
		/* Check the tax details indicator.                                */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/* Check the premium breakdown indicator.*/
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind,"+")
		&& isNE(sv.pbind,"X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		if (isNE(sv.sumin,wsaaOldSumins)) {
			compute(wsaaSuminsDiff, 0).set(sub(sv.sumin,wsaaOldSumins));
			if (isLT(wsaaSuminsDiff,0)) {
				compute(wsaaSuminsDiff, 0).set(mult(wsaaSuminsDiff,-1));
				wsaaIfSuminsChanged.set("D");
			}
			else {
				wsaaIfSuminsChanged.set("I");
			}
		}
		if (isNE(sv.instPrem,wsaaOldPrem)) {
			compute(wsaaPremDiff, 2).set(sub(sv.instPrem,wsaaOldPrem));
			if (isLT(wsaaPremDiff,0)) {
				compute(wsaaPremDiff, 2).set(mult(wsaaPremDiff,-1));
				wsaaIfPremChanged.set("D");
			}
			else {
				wsaaIfPremChanged.set("I");
			}
		}
		processCashValueInArrears();// fwang3 ICIL-560
		/* If everything else is O-K calculate the Installment Premium.*/
		if (isNE(sv.optextind,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& addComp.isTrue()) {
			calcPremium2200();
		}
		if (isNE(sv.optextind,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& isNE(sv.comind,"X")
		&& modifyComp.isTrue()) {
			calcPremium2200();
		}
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(errorsInner.g620);
				}
		
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* If 'CALC' was entered then re-display the screen.*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

/**
 * fwang3 ICIL-560
 */
private void processCashValueInArrears() {
	// fwang3 ICIL-560 judge if the component has cash value and than code is come from submenu
	if (chinaLocalPermission && isEQ(wsaaFlag, "M") && hasCashValue && isEQ("T555", wsaaBatckey.batcBatctrcde)) {
		if (isLT(sv.sumin, wsaaCashValArr)) {
			sv.suminErr.set(errorsInner.rrj4);// ICIL-299
		}
		sv.cashvalarer.set(ZERO);
		if (isGT(sv.sumin, wsaaHoldSumin)) {
			int years = DateUtils.calYears(chdrpf.getOccdate().toString(), chdrpf.getPtdate().toString());
			PackedDecimalData data = new PackedDecimalData(11, 5).init(100000);
			compute(sv.cashvalarer,2).setRounded(mult(div(th528rec.insprm[years + 1], data), sub(sv.sumin, wsaaHoldSumin)));//fix ICIL-706
		}
	}
}
	
	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void editCoverage2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkSumin2110();
				case checkSuminCont2120: 
					checkSuminCont2120();
				case checkRcessFields2120: 
					checkRcessFields2120();
					checkPcessFields2130();
					checkAgeTerm2140();
				case validDate2140: 
					validDate2140();
				case datesCont2140: 
					datesCont2140();
				case ageAnniversary2140: 
					ageAnniversary2140();
				case check2140: 
					check2140();
				case checkOccurance2140: 
					checkOccurance2140();
				case checkTermFields2150: 
					checkTermFields2150();
				case checkComplete2160: 
					checkComplete2160();
				case checkMortcls2170: 
					checkMortcls2170();
				case loop2175: 
					loop2175();
				case checkLiencd2180: 
					checkLiencd2180();
				case loop2185: 
					loop2185();
				case checkMore2190: 
					checkMore2190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkSumin2110()
	{
		/* Check sum insured ranges.*/
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)) {
			goTo(GotoLabel.checkRcessFields2120);
		}
		if (isEQ(sv.sumin,covrpf.getSumins())) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkSuminCont2120);
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin,chdrpf.getPolinc()),chdrpf.getPolsum())));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
	}

protected void checkSuminCont2120()
	{
		if (isLT(wsaaSumin,th505rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin,th505rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

protected void checkRcessFields2120()
	{
		if (isEQ(sv.select,"J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent,SPACES)
		&& jointlif.isTrue()) {
			checkDefaults5300();
		}
		if (isGT(sv.riskCessAge,0)
		&& isGT(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(th505rec.eaage,SPACES)
		&& isEQ(sv.riskCessAge,0)
		&& isEQ(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2130()
	{
		if (isGT(sv.premCessAge,0)
		&& isGT(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)
		&& isEQ(sv.rcessageErr,SPACES)
		&& isEQ(sv.rcesstrmErr,SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(th505rec.eaage,SPACES)
		&& isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2140()
	{
		if ((isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))
		|| (isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if ((isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))
		|| (isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		/* To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (TH505).*/
		if (isEQ(sv.riskCessDate,varcom.vrcmMaxDate)) {
			riskCessDate5400();
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			premCessDate5450();
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate,sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isEQ(sv.rider,"00")) {
			goTo(GotoLabel.datesCont2140);
		}
		wsaaStorePlanSuffix.set(covrpf.getPlanSuffix());
		/* Read the Temporary COVT file for the Coverage record which must*/
		/* have been written before a rider can be selected.*/
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(covrpf.getCoverage());
		covtmjaIO.setRider("00");
		covtmjaIO.setSeqnbr(ZERO);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(),covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(),"00")) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.validDate2140);
		}
		if (modifyComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(),ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGT(chdrpf.getPolsum(),covrpf.getPlanSuffix())
				|| isEQ(chdrpf.getPolsum(),covrpf.getPlanSuffix())) {
					covrpf.setPlanSuffix(0);
				}
			}
		}
		if (addComp.isTrue()) {
			covrpf.setPlanSuffix(0);
			if (isEQ(chdrpf.getPolsum(),ZERO)) {
				if (isEQ(chdrpf.getPolinc(),1)) {
					covrpf.setPlanSuffix(0);
				}
				else {
					covrpf.setPlanSuffix(1);
				}
			}
			else {
				covrpf.setPlanSuffix(0);
			}
		}
		covrpf.setRider("00");
		covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
		covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
	}

protected void validDate2140()
	{
		if (isGT(sv.riskCessDate,covtmjaIO.getRiskCessDate())) {
			sv.rcesdteErr.set(errorsInner.h033);
		}
		if (isGT(sv.premCessDate,covtmjaIO.getRiskCessDate())) {
			sv.pcesdteErr.set(errorsInner.h044);
		}
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(sv.life);
		covtmjaIO.setCoverage(sv.coverage);
		covtmjaIO.setRider(sv.rider);
		covtmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		List<String> tempList = new ArrayList<String>();
		tempList.add(covrpf.getChdrnum());
		covrpfList = covrpfDAO.searchCovrRecord(covrpf.getChdrcoy(), tempList);
		tempList.clear();
		if(!(covrpfList.isEmpty())) {
			covrpf = covrpfList.get(0);
		}
	}

protected void datesCont2140()
	{
		if (isEQ(sv.rider,"00")
		&& modifyComp.isTrue()) {
			covrrgwIO.setParams(SPACES);
			covrrgwIO.setChdrcoy(covrpf.getChdrcoy());
			covrrgwIO.setChdrnum(covrpf.getChdrnum());
			covrrgwIO.setLife(covrpf.getLife());
			covrrgwIO.setCoverage(covrpf.getCoverage());
			covrrgwIO.setRider(covrpf.getRider());
			covrrgwIO.setPlanSuffix(covrpf.getPlanSuffix());
			covrrgwIO.setFormat(formatsInner.covrrgwrec);
			covrrgwIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, covrrgwIO);
			if (isNE(covrrgwIO.getStatuz(),varcom.oK)
			&& isNE(covrrgwIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrrgwIO.getParams());
				syserrrec.statuz.set(covrrgwIO.getStatuz());
				fatalError600();
			}
			while ( !(isEQ(covrrgwIO.getStatuz(),varcom.endp))) {
				riderCessation2140();
			}
			
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if (isEQ(th505rec.eaage,"A")) {
			goTo(GotoLabel.ageAnniversary2140);
		}
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		goTo(GotoLabel.check2140);
	}

protected void ageAnniversary2140()
	{
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			compute(wszzRiskCessAge, 5).set(add(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			compute(wszzPremCessAge, 5).set(add(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
	}

protected void check2140()
	{
		if (addComp.isTrue()) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
		}
		else {
			datcon3rec.intDate1.set(covrpf.getCrrcd());
		}
		if (isEQ(sv.riskCessTerm,ZERO)) {
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm,ZERO)) {
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		/* Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
	}

protected void checkOccurance2140()
	{
		x.add(1);
		if (isGT(x,wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2160);
		}
		if ((isEQ(th505rec.ageIssageFrm[x.toInt()],0)
		&& isEQ(th505rec.ageIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,th505rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,th505rec.ageIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkTermFields2150);
		}
		if (isGTE(wszzRiskCessAge,th505rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge,th505rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge,th505rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge,th505rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2150()
	{
		if ((isEQ(th505rec.termIssageFrm[x.toInt()],0)
		&& isEQ(th505rec.termIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,th505rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,th505rec.termIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkOccurance2140);
		}
		if (isGTE(wszzRiskCessTerm,th505rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm,th505rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm,th505rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm,th505rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2140);
	}

protected void checkComplete2160()
	{
		if (isNE(sv.rcesstrmErr,SPACES)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr,SPACES)
		&& isEQ(sv.premCessTerm,ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr,SPACES)
		&& isEQ(sv.riskCessAge,ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr,SPACES)
		&& isEQ(sv.premCessAge,ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2170()
	{
		if (isEQ(sv.mortclsOut[varcom.pr.toInt()],"Y")) {
			goTo(GotoLabel.checkLiencd2180);
		}
		x.set(0);
	}

protected void loop2175()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2180);
		}
		if (isEQ(th505rec.mortcls[x.toInt()],SPACES)
		|| isNE(th505rec.mortcls[x.toInt()],sv.mortcls)) {
			goTo(GotoLabel.loop2175);
		}
	}

protected void checkLiencd2180()
	{
		if (isEQ(sv.liencdOut[varcom.pr.toInt()],"Y")
		|| isEQ(sv.liencd,SPACES)) {
			goTo(GotoLabel.checkMore2190);
		}
		x.set(0);
	}

protected void loop2185()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2190);
		}
		if (isEQ(th505rec.liencd[x.toInt()],SPACES)
		|| isNE(th505rec.liencd[x.toInt()],sv.liencd)) {
			goTo(GotoLabel.loop2185);
		}
	}

protected void checkMore2190()
	{
		if (isNE(sv.bappmeth,SPACES)
		&& isNE(sv.bappmeth,t6005rec.bappmeth01)
		&& isNE(sv.bappmeth,t6005rec.bappmeth02)
		&& isNE(sv.bappmeth,t6005rec.bappmeth03)
		&& isNE(sv.bappmeth,t6005rec.bappmeth04)
		&& isNE(sv.bappmeth,t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			if (isNE(sv.select,SPACES)
			&& isNE(sv.select,"J")
			&& isNE(sv.select,"L")) {
				sv.selectErr.set(errorsInner.h039);
			}
		}
		/*Z0-EXIT*/
		if(incomeProtectionflag || premiumflag){
			checkIPPmandatory();
			checkIPPfields();
		}
	}

protected void riderCessation2140()
	{
			para2140();
		}

protected void para2140()
	{
		if (isNE(covrrgwIO.getChdrcoy(),wsaaCovrChdrcoy)
		|| isNE(covrrgwIO.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrrgwIO.getLife(),wsaaCovrLife)
		|| isNE(covrrgwIO.getCoverage(),wsaaCovrCoverage)) {
			covrrgwIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(covrrgwIO.getRiskCessDate(),sv.riskCessDate)) {
			sv.rcesdteErr.set(errorsInner.h033);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isGT(covrrgwIO.getPremCessDate(),sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.h044);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrrgwIO.getStatuz(),varcom.endp)) {
			return ;
		}
		covrrgwIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrrgwIO);
		if (isNE(covrrgwIO.getStatuz(),varcom.oK)
		&& isNE(covrrgwIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrrgwIO.getParams());
			syserrrec.statuz.set(covrrgwIO.getStatuz());
			fatalError600();
		}
	}

protected void calcPremium2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2210();
					callSubr2220();
					adjustPrem2225();
				case cont: 
					cont();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2210()
	{
		if (isNE(t5687rec.bbmeth,SPACES)) {
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(t5675rec.premsubr,SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		if (premReqd.isTrue()) {
			if (isEQ(sv.instPrem,0)) {
				sv.instprmErr.set(errorsInner.g818);
			}
			goTo(GotoLabel.exit2290);
		}
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (wsaaPremChanged.isTrue()
			&& modifyComp.isTrue()) {
				wsaaOldPrem.set(sv.instPrem);
				wsaaPremStatuz.set("U");
				sv.instprmErr.set(errorsInner.f404);
				goTo(GotoLabel.exit2290);
			}
		}
		if (modifyComp.isTrue()
		&& userPremEntered.isTrue()
		&& premReqd.isTrue()) {
			if (isGT(sv.instPrem,0)) {
				goTo(GotoLabel.exit2290);
			}
		}
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (!wsaaPremChanged.isTrue()) {
				goTo(GotoLabel.exit2290);
			}
		}
	}

protected void callSubr2220()
	{
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy().toString());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select,"J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(chdrpf.getOccdate());
		premiumrec.termdate.set(sv.premCessDate);
		premiumrec.currcode.set(payrIO.getCntcurr());
		if (isNE(wsaaSumin,ZERO)) {
		}
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.cnttype.set(chdrpf.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.benCessTerm.set(ZERO);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		if (modifyComp.isTrue()
		&& isEQ(sv.pbindOut[varcom.nd.toInt()],"N")) {
			premiumrec.function.set("MALT");
			if (isNE(wsaaPovrModified,"Y")) {
				vf2MaltPovr8300();
				wsaaPovrModified = "Y";
			}
		}
		premiumrec.language.set(wsspcomn.language);
		
		//ILIFE-3975
		if(isNE(sv.dialdownoption,SPACES)){
			if(sv.dialdownoption.toString().startsWith("0"))
				premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
			else
				premiumrec.dialdownoption.set(sv.dialdownoption);
		}
		else
			premiumrec.dialdownoption.set("100");

		//ILIFE-3975 end
		
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//Ticket #ILIFE-2005 end
		
		if (isEQ(premiumrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		wsaaPcesageStore.set(sv.premCessAge);
		wsaaRcesageStore.set(sv.riskCessAge);
		wsaaPcestermStore.set(sv.premCessTerm);
		wsaaRcestermStore.set(sv.riskCessTerm);
	}

protected void adjustPrem2225()
	{
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		if (isLTE(payrIO.getMandref(),SPACES)) {
			goTo(GotoLabel.cont);
		}
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		mandIO.setPayrcoy(clrfIO.getClntcoy());
		mandIO.setPayrnum(clrfIO.getClntnum());
		mandIO.setMandref(payrIO.getMandref());
		mandIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		if (isEQ(mandIO.getMandAmt(),ZERO)) {
			goTo(GotoLabel.cont);
		}
		if (isGT(payrIO.getBillfreq(),0)) {
			compute(wsaaCalcPrem, 2).set(sub(covrpf.getInstprem(),(sub(payrIO.getSinstamt06(),sv.instPrem))));
		}
		if (isNE(mandIO.getMandAmt(),wsaaCalcPrem)) {
			sv.instprmErr.set(errorsInner.ev01);
			wsspcomn.edterror.set("Y");
		}
	}

protected void cont()
	{
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		/*BRD-306 END */
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		if (isEQ(sv.instPrem,0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		if (isGTE(sv.instPrem,premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem,sv.instPrem));
		sv.instprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub,11)
		|| isEQ(sv.instprmErr,SPACES)); wsaaSub.add(1)){
			searchForTolerance2240();
		}
		if (isEQ(sv.instprmErr,SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem,sv.zlinstprem));
		}
		goTo(GotoLabel.exit2290);
	}

protected void searchForTolerance2240()
	{
		if (isEQ(payrIO.getBillfreq(),t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtol[wsaaSub.toInt()])),100));
			if (isLTE(wsaaDiff,wsaaTol)
			&& isLTE(wsaaDiff,t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()],0)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtoln[wsaaSub.toInt()])),100));
				if (isLTE(wsaaDiff,wsaaTol)
				&& isLTE(wsaaDiff,t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.instprmErr.set(SPACES);
				}
			}
		}
	}

protected void callDatcon32300()
	{
		/*CALL*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void editPayeeDetails2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					editPayeeDet2400();
				case mop2410: 
					mop2410();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void editPayeeDet2400()
	{
		if (isEQ(sv.zdivopt,SPACES)) {
			if (isNE(th505rec.zdefdivopt,SPACES)) {
				sv.zdivopt.set(th505rec.zdefdivopt);
			}
		}
		else {
			for (wsaaIx.set(1); !(isGT(wsaaIx,7)
			|| isEQ(sv.zdivopt,th505rec.zdivopt[wsaaIx.toInt()])
			|| isNE(sv.zdivoptErr,SPACES)); wsaaIx.add(1)){
				if (isNE(sv.zdivopt,th505rec.zdivopt[wsaaIx.toInt()])
				&& isEQ(wsaaIx,7)) {
					sv.zdivoptErr.set(errorsInner.z042);
				}
			}
		}
		if (isEQ(sv.zdivopt,SPACES)) {
			goTo(GotoLabel.mop2410);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.th500);
		itemIO.setItemitem(sv.zdivopt);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.zdivoptErr.set(errorsInner.z038);
			goTo(GotoLabel.mop2410);
		}
		th500rec.th500Rec.set(itemIO.getGenarea());
		if (isEQ(th500rec.payeereq,"Y")) {
			if (isEQ(sv.payeesel,SPACES)) {
				sv.payeeselErr.set(errorsInner.e133);
			}
			if (isEQ(sv.paymth,SPACES)) {
				sv.paymthErr.set(errorsInner.f020);
			}
			if (isEQ(sv.paycurr,SPACES)) {
				sv.paycurrErr.set(errorsInner.h960);
			}
		}
	}

protected void mop2410()
	{
		if (isNE(covtmjaIO.getBankkey(),SPACES)
		&& isNE(covtmjaIO.getBankacckey(),SPACES)) {
			clbaddbIO.setParams(SPACES);
			clbaddbIO.setClntcoy(covtmjaIO.getPaycoy());
			clbaddbIO.setClntnum(covtmjaIO.getPayclt());
			clbaddbIO.setBankkey(covtmjaIO.getBankkey());
			clbaddbIO.setBankacckey(covtmjaIO.getBankacckey());
			clbaddbIO.setFunction(varcom.readr);
			clbaddbIO.setFormat(formatsInner.clbaddbrec);
			SmartFileCode.execute(appVars, clbaddbIO);
			if (isNE(clbaddbIO.getStatuz(),varcom.oK)
			&& isNE(clbaddbIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(clbaddbIO.getParams());
				fatalError600();
			}
			if (isEQ(clbaddbIO.getStatuz(),varcom.mrnf)) {
				sv.bankaccreqErr.set(errorsInner.f826);
				sv.bankaccreq.set("X");
			}
			if (isNE(clbaddbIO.getCurrcode(),sv.paycurr)) {
				sv.paycurrErr.set(errorsInner.z043);
			}
		}
		if (isEQ(sv.paymth,SPACES)) {
			sv.pymdesc.set(SPACES);
			return ;
		}
		/*  payment method must exist in T5690.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5690);
		itemIO.setItemitem(sv.paymth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.paymthErr.set(errorsInner.f177);
			return ;
		}
		t5690rec.t5690Rec.set(itemIO.getGenarea());
		if (isEQ(t5690rec.bankreq,"Y")) {
			if (isNE(sv.bankaccreq,"+")
			&& isNE(sv.bankaccreq,"X")) {
				sv.bankaccreq.set("X");
			}
		}
		else {
			if (isEQ(sv.bankaccreq,"+")
			|| isEQ(sv.bankaccreq,"X")) {
				sv.bankaccreqErr.set(errorsInner.hl52);
				/*****         MOVE HL30           TO SH515-BANKACCREQ-ERR          */
			}
		}
		/*  Get the pay method description from DESCPF*/
		mopDescription6300();
		/*  Re-display payee name from client file.*/
		if (isNE(sv.payeesel,SPACES)) {
			cltsIO.setDataKey(SPACES);
			cltsIO.setClntnum(sv.payeesel);
			payeeName6200();
		}
	}

protected void adjustSuminsPrem2800()
	{
		/*START*/
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(premiumrec.sumin, 3).setRounded((div(mult(premiumrec.sumin,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		sv.sumin.set(premiumrec.sumin);
		sv.instPrem.set(premiumrec.calcPrem);
		wsaaOldPrem.set(sv.instPrem);
		wsaaOldSumins.set(sv.sumin);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					if(incomeProtectionflag || premiumflag || dialdownFlag)
					{
						insertAndUpdateRcvdpf();
					}
					updateDatabase3010();
				case updatePcdt3060: 
					updatePcdt3060();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
//updateExcl() removed as a part of PINNACLE-2855

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		/* If termination of processing or enquiry go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3090);
		}
		if (compEnquiry.isTrue()) {
			if (compEnquiry.isTrue()
			|| revComp.isTrue()) {
				goTo(GotoLabel.exit3090);
			}
		}
		if (isEQ(sv.pbind,"X")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.bankaccreq,"X")) {
			goTo(GotoLabel.exit3090);
		}
		if (compApp.isTrue()) {
			goTo(GotoLabel.updatePcdt3060);
		}
		setupCovtmja3100();
		if (isEQ(wsaaLextUpdates,"Y")) {
			wsaaUpdateFlag.set("Y");
		}
		updateCovtmja3200();
		
		//goTo(GotoLabel.exit3090);
		if (isEQ(wsaaUpdateFlag,"N")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
if(rcvdPFObject!=null){
	rcvdPFObject.setChdrcoy(covtmjaIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtmjaIO.getChdrnum().toString());
	rcvdPFObject.setCoverage(covtmjaIO.getCoverage().toString());
	rcvdPFObject.setCrtable(covtmjaIO.getCrtable().toString());
	rcvdPFObject.setLife(covtmjaIO.getLife().toString());
	rcvdPFObject.setRider(covtmjaIO.getRider().toString());
	
	if(rcvdPFObject.getPrmbasis()!=null){
		if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getDialdownoption()!=null){
		//BRD-NBP-011 starts
		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			rcvdUpdateFlag=true;
		}
	}
		//BRD-NBP-011 ends
		if(rcvdUpdateFlag){
			rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
		}
	}else{ 
		rcvdPFObject=new Rcvdpf();
		rcvdPFObject.setChdrcoy(covtmjaIO.getChdrcoy().toString());
		rcvdPFObject.setChdrnum(covtmjaIO.getChdrnum().toString());
		rcvdPFObject.setCoverage(covtmjaIO.getCoverage().toString());
		rcvdPFObject.setCrtable(covtmjaIO.getCrtable().toString());
		rcvdPFObject.setLife(covtmjaIO.getLife().toString());
		rcvdPFObject.setRider(covtmjaIO.getRider().toString());
	
		if(isNE(sv.prmbasis,SPACES)){
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		}
		//BRD-NBP-011 starts
		if(isNE(sv.dialdownoption,SPACES)){
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
		}
		//BRD-NBP-011 ends
		rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
	}
	
}

protected void updatePcdt3060()
	{
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy().toString());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrpf.getChdrcoy(),pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),pcdtmjaIO.getChdrnum())
		|| isNE(sv.life,pcdtmjaIO.getLife())
		|| isNE(sv.coverage,pcdtmjaIO.getCoverage())
		|| isNE(sv.rider,pcdtmjaIO.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(),pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(),varcom.endp)) {
			defaultPcdt3800();
		}
	}

protected void setupCovtmja3100()
	{
		updateReqd3110();
	}

protected void updateReqd3110()
	{
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate,covtmjaIO.getRiskCessDate())) {
			covtmjaIO.setRiskCessDate(sv.riskCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate,covtmjaIO.getPremCessDate())) {
			covtmjaIO.setPremCessDate(sv.premCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge,covtmjaIO.getRiskCessAge())) {
			covtmjaIO.setRiskCessAge(sv.riskCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge,covtmjaIO.getPremCessAge())) {
			covtmjaIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm,covtmjaIO.getRiskCessTerm())) {
			covtmjaIO.setRiskCessTerm(sv.riskCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm,covtmjaIO.getPremCessTerm())) {
			covtmjaIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin,covtmjaIO.getSumins())) {
			covtmjaIO.setSumins(sv.sumin);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zbinstprem,covtmjaIO.getZbinstprem())) {
			covtmjaIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem,covtmjaIO.getZlinstprem())) {
			covtmjaIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtmjaIO.setLoadper(sv.loadper);
			covtmjaIO.setRateadj(sv.rateadj);
			covtmjaIO.setFltmort(sv.fltmort);
			covtmjaIO.setPremadj(sv.premadj);
			covtmjaIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		if (isEQ(payrIO.getBillfreq(),"00")) {
			if (isNE(sv.instPrem,covtmjaIO.getSingp())) {
				covtmjaIO.setSingp(sv.instPrem);
				covtmjaIO.setInstprem(ZERO);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(sv.instPrem,covtmjaIO.getInstprem())) {
				covtmjaIO.setInstprem(sv.instPrem);
				covtmjaIO.setSingp(ZERO);
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.mortcls,covtmjaIO.getMortcls())) {
			covtmjaIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd,covtmjaIO.getLiencd())) {
			covtmjaIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		if ((isEQ(sv.select,"J")
		&& (isEQ(covtmjaIO.getJlife(),"00")
		|| isEQ(covtmjaIO.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select,"L"))
		&& isEQ(covtmjaIO.getJlife(),"01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select,"J")) {
				covtmjaIO.setJlife("01");
			}
			else {
				covtmjaIO.setJlife("00");
			}
		}
		if (isNE(payrIO.getBillfreq(),covtmjaIO.getBillfreq())) {
			covtmjaIO.setBillfreq(payrIO.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillchnl(),covtmjaIO.getBillchnl())) {
			covtmjaIO.setBillchnl(payrIO.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd,covtmjaIO.getAnbccd(1))) {
			covtmjaIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtmjaIO.getSex(1))) {
			covtmjaIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd,covtmjaIO.getAnbccd(2))) {
			covtmjaIO.setAnbccd(2, wsbbAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtmjaIO.getSex(2))) {
			covtmjaIO.setSex(2, wsbbSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth,covtmjaIO.getBappmeth())) {
			covtmjaIO.setBappmeth(sv.bappmeth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zdivopt,covtmjaIO.getZdivopt())) {
			covtmjaIO.setZdivopt(sv.zdivopt);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.payeesel,covtmjaIO.getPayclt())) {
			covtmjaIO.setPaycoy(wsspcomn.fsuco);
			covtmjaIO.setPayclt(sv.payeesel);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.paymth,covtmjaIO.getPaymth())) {
			covtmjaIO.setPaymth(sv.paymth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.paycurr,covtmjaIO.getPaycurr())) {
			covtmjaIO.setPaycurr(sv.paycurr);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			if (isNE(covtmjaIO.getZdivopt(),SPACES)
			|| isNE(covtmjaIO.getPaycoy(),SPACES)
			|| isNE(covtmjaIO.getPayclt(),SPACES)
			|| isNE(covtmjaIO.getPaymth(),SPACES)
			|| isNE(covtmjaIO.getBankkey(),SPACES)
			|| isNE(covtmjaIO.getBankacckey(),SPACES)
			|| isNE(covtmjaIO.getPaycurr(),SPACES)
			|| isNE(covtmjaIO.getFacthous(),SPACES)) {
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(covtmjaIO.getZdivopt(),hcsdIO.getZdivopt())
			|| isNE(covtmjaIO.getPaycoy(),hcsdIO.getPaycoy())
			|| isNE(covtmjaIO.getPayclt(),hcsdIO.getPayclt())
			|| isNE(covtmjaIO.getPaymth(),hcsdIO.getPaymth())
			|| isNE(covtmjaIO.getBankkey(),hcsdIO.getBankkey())
			|| isNE(covtmjaIO.getBankacckey(),hcsdIO.getBankacckey())
			|| isNE(covtmjaIO.getPaycurr(),hcsdIO.getPaycurr())
			|| isNE(covtmjaIO.getFacthous(),hcsdIO.getFacthous())) {
				wsaaUpdateFlag.set("Y");
			}
		}
	}

protected void updateCovtmja3200()
	{
		try {
			noUpdate3210();
			update3220();
		}
		catch (GOTOException e){
		}
	}

protected void noUpdate3210()
	{
		if (isEQ(wsaaUpdateFlag,"N")
		&& isNE(sv.optextind,"X")) {
			goTo(GotoLabel.exit3290);
		}
	}

protected void update3220()
	{
		covtmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		covtmjaIO.setTermid(varcom.vrcmTermid);
		covtmjaIO.setUser(varcom.vrcmUser);
		covtmjaIO.setTransactionDate(varcom.vrcmDate);
		covtmjaIO.setTransactionTime(varcom.vrcmTime);
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(wsaaCovrCoverage);
		covtmjaIO.setEffdate(payrIO.getBtdate());
		covtmjaIO.setRider(wsaaCovrRider);
		covtmjaIO.setPayrseqno(1);
		covtmjaIO.setBenCessAge(ZERO);
		covtmjaIO.setBenCessTerm(ZERO);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		if (addComp.isTrue()) {
			covtmjaIO.setPlanSuffix(0);
			covtmjaIO.setSeqnbr(0);
		}
		else {
			covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
			covtmjaIO.setSeqnbr(wsaaNextSeqnbr);
		}
		wsaaNextSeqnbr.subtract(1);
		covtmjaIO.setCrtable(wsaaCrtable);
		covtmjaIO.setReserveUnitsInd(SPACES);
		covtmjaIO.setReserveUnitsDate(ZERO);
		covtmjaIO.setPolinc(ZERO);
		covtmjaIO.setFunction(varcom.updat);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
	}

protected void defaultPcdt3800()
	{
		moveValues3800();
		call3830();
	}

protected void moveValues3800()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		for (wsaaIndex.set(1); !(isLT(wsaaIndex,10)); wsaaIndex.add(1)){
			pcdtmjaIO.setSplitc(wsaaIndex, 0);
		}
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy().toString());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setInstprem(ZERO);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		wsaaIndex.set(1);
	}

protected void call3830()
	{
		pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, 100);
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(sv.bankaccreq,"X")) {
			bankaccExe4b00();
			return ;
		}
		else {
			if (isEQ(sv.bankaccreq,"?")) {
				bankaccRet4c00();
				return ;
			}
		}
		if (isEQ(sv.pbind,"X")) {
			pbindExe8100();
			return ;
		}
		else {
			if (isEQ(sv.pbind,"?")) {
				pbindRet8200();
				return ;
			}
		}
		if (isEQ(sv.optextind,"X")) {
			optionsExe4500();
			return ;
		}
		else {
			if (isEQ(sv.optextind,"?")) {
				optionsRet4600();
				return ;
			}
		}
		if (isEQ(sv.comind,"X")) {
			commissionExe4900();
			return ;
		}
		else {
			if (isEQ(sv.comind,"?")) {
				commissionRet4a00();
				return ;
			}
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe8500();
			return ;
		}
		else {
			if (isEQ(sv.taxind, "?")) {
				taxRet8600();
				return ;
			}
		}
		if (isEQ(sv.exclind, "X")) {
			exclExe6000();
			return ;
		}
		else {
			if (isEQ(sv.exclind, "?")) {
				exclRet6100();
				return ;
			}
		}
		if (planLevel.isTrue()) {
			clearScreen4700();
			policyLoad5000();
			if (covrpf != null) {
				wsspcomn.nextprog.set(wsaaProg);
				wayout4800();
			}
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4800();
		}
	}
protected void exclExe6000(){
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
		wsspcomn.crtable.set(wsaaCrtable);
		
		
		if(isNE(wsspcomn.flag,"I")){
		wsspcomn.flag.set("X");
		wsspcomn.cmode.set("IF");}
		else
			wsspcomn.cmode.set("IFE");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),wsaaCrtable.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());/* IJTI-1386 */
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}

protected void gensww4210()
	{
			para4211();
		}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextrevIO.getStatuz(),varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void optionsExe4500()
	{
		keepCovr4510();
	}

protected void keepCovr4510()
	{
		covrpfDAO.setCacheObject(covrpf);
		sv.optextind.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		para4600();
	}

protected void para4600()
	{
		wsaaOptext.set("Y");
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			calcPremium2200();
		}
		wsaaOptext.set("N");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		/* Blank out the stack "action".*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		checkLext5600();
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy().toString());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());/* IJTI-1386 */
		wsaaPovrLife.set(covtmjaIO.getLife());
		wsaaPovrCoverage.set(covtmjaIO.getCoverage());
		wsaaPovrRider.set(covtmjaIO.getRider());
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void clearScreen4700()
	{
		/*PARA*/
		sv.premCessDate.set(ZERO);
		sv.riskCessDate.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.liencd.set(SPACES);
		sv.mortcls.set(SPACES);
		sv.optextind.set(SPACES);
		sv.select.set(SPACES);
		sv.comind.set(SPACES);
		sv.statFund.set(SPACES);
		sv.statSect.set(SPACES);
		sv.bappmeth.set(SPACES);
		sv.statSubsect.set(SPACES);
		/*EXIT*/
	}

protected void wayout4800()
	{
		/*PROGRAM-EXIT*/
		/* If control is passed to this  part of the 4000 section on the*/
		/*   way out of the program, i.e. after  screen  I/O,  then  the*/
		/*   current stack position action  flag will be  blank.  If the*/
		/*   4000-section  is  being   performed  after  returning  from*/
		/*   processing another program then  the current stack position*/
		/*   action flag will be '*'.*/
		/* If 'KILL' has been requested, (CF12), then move spaces to the*/
		/*   current program entry in the program stack and exit.*/
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/* Add 1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void commissionExe4900()
	{
		keepCovr4910();
	}

protected void keepCovr4910()
	{
		/* Keep the COVRMJA record if Whole Plan selected or Add component*/
		/*   as this will have been released within the 1000-section to*/
		/*   enable all policies to be processed.*/
		wsspcomn.tranrate.set(sv.instPrem);
		wssplife.effdate.set(payrIO.getBtdate());
		covrpfDAO.setCacheObject(covrpf);
		sv.comind.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void commissionRet4a00()
	{
		/*A00-PARA*/
		calcPremium5200();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restore4610();
		}
		/* Blank out the stack "action".*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		checkPcdt5700();
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*A90-EXIT*/
	}

protected void bankaccExe4b00()
	{
		bankAccExe4b00();
	}

protected void bankAccExe4b00()
	{
		sv.bankaccreq.set("?");
		covtmjaIO.setZdivopt(sv.zdivopt);
		covtmjaIO.setPaycoy(wsspcomn.fsuco);
		covtmjaIO.setPayclt(sv.payeesel);
		covtmjaIO.setPaymth(sv.paymth);
		covtmjaIO.setPaycurr(sv.paycurr);
		covtmjaIO.setFunction("KEEPS");
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			save4510();
		}
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void bankaccRet4c00()
	{
		bankAccRet4c00();
		programStack4c10();
	}

protected void bankAccRet4c00()
	{
		covtmjaIO.setFunction("RETRV");
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covtmjaIO.getBankkey(),SPACES)
		&& isEQ(covtmjaIO.getBankacckey(),SPACES)) {
			sv.bankaccreq.set(SPACES);
		}
		else {
			sv.bankaccreq.set("+");
		}
	}

protected void programStack4c10()
	{
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*C90-EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			loadPolicy5010();
			readCovt5020();
			fieldsToScreen5080();
			mortalityLien5090();
			ageProcessing50a0();
			optionsExtras50b0();
			enquiryProtect50c0();
		}
		catch (GOTOException e){
		}
	}

protected void loadPolicy5010()
	{
		wsaaOldPrem.set(0);
		wsaaOldSumins.set(0);
		if (planLevel.isTrue()) {
			readCovr5100();
			for (Covrpf covr : covrpfList) {
				if (covr == null) {
					goTo(GotoLabel.exit50z0);
				}
				else {
					covtmjaIO.setFunction(varcom.readr);
					if (addComp.isTrue()) {
						covtmjaIO.setCoverage(wsaaCovrCoverage);
						covtmjaIO.setRider(wsaaCovrRider);
						covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
					}
					else {
						covtmjaIO.setDataKey(wsaaCovrKey);
						if (isNE(covr.getPlanSuffix(),ZERO)) {
							covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
						}
					}
				}
				covrpf.setPlanSuffix(covr.getPlanSuffix());
			}
		}
		wsaaOldSumins.set(covrpf.getSumins());
		wsaaOldPrem.set(covrpf.getInstprem());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			sv.planSuffix.set(chdrpf.getPolsum());
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
	}

protected void readCovt5020()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.mrnf)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(),covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(),covrpf.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(),covrpf.getPlanSuffix())
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)
		|| isEQ(covtmjaIO.getStatuz(),varcom.mrnf)) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (!addComp.isTrue()) {
			recordToScreen6000();
			goTo(GotoLabel.exit50z0);
		}
		else {
			if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)
			|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
				covtmjaIO.setNonKey(SPACES);
				covtmjaIO.setAnbccd(1, ZERO);
				covtmjaIO.setAnbccd(2, ZERO);
				covtmjaIO.setSingp(ZERO);
				covtmjaIO.setPlanSuffix(ZERO);
				covtmjaIO.setInstprem(ZERO);
				covtmjaIO.setZbinstprem(ZERO);
				covtmjaIO.setZlinstprem(ZERO);
				covtmjaIO.setPremCessAge(ZERO);
				covtmjaIO.setPremCessTerm(ZERO);
				covtmjaIO.setRiskCessAge(ZERO);
				covtmjaIO.setRiskCessTerm(ZERO);
				covtmjaIO.setBenCessAge(ZERO);
				covtmjaIO.setBenCessTerm(ZERO);
				covtmjaIO.setReserveUnitsDate(ZERO);
				covtmjaIO.setSumins(ZERO);
				covtmjaIO.setEffdate(payrIO.getBtdate());
				covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setPolinc(chdrpf.getPolinc());
				covtmjaIO.setCrtable(wsaaCrtable);
				tableLoads7000();
			}
			else {
				tableLoads7000();
			}
		}
		if (addComp.isTrue()) {
			covtmjaIO.setZdivopt(th505rec.zdefdivopt);
			sv.zdivopt.set(th505rec.zdefdivopt);
			covtmjaIO.setPayclt(lifemjaIO.getLifcnum());
			sv.payeesel.set(lifemjaIO.getLifcnum());
			covtmjaIO.setPaycoy(wsspcomn.fsuco);
		}
		if (isEQ(covtmjaIO.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
	}

protected void fieldsToScreen5080()
	{
		sv.planSuffix.set(chdrpf.getPolinc());
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premCessDate.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.riskCessDate.set(covtmjaIO.getRiskCessDate());
		sv.riskCessAge.set(covtmjaIO.getRiskCessAge());
		sv.riskCessTerm.set(covtmjaIO.getRiskCessTerm());
		sv.instPrem.set(covtmjaIO.getInstprem());
		sv.zbinstprem.set(covtmjaIO.getZbinstprem());
		sv.zlinstprem.set(covtmjaIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtmjaIO.getLoadper());
		sv.rateadj.set(covtmjaIO.getRateadj());
		sv.fltmort.set(covtmjaIO.fltmort);
		sv.premadj.set(covtmjaIO.getPremadj());
		sv.adjustageamt.set(covtmjaIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covtmjaIO.getSumins());
		setupBonus1300();
		sv.bappmeth.set(covtmjaIO.getBappmeth());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		if (isEQ(th505rec.sumInsMax,0)
		&& isEQ(th505rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)
		&& isNE(th505rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(th505rec.sumInsMax);
		}
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560 
		}
	}

protected void mortalityLien5090()
	{
		if (isEQ(th505rec.mortclss,SPACES)) {
			sv.mortcls.set(SPACES);
			sv.mortclsOut[varcom.nd.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		
		if (isNE(th505rec.mortcls01,SPACES)
		&& isEQ(th505rec.mortcls02,SPACES)
		&& isEQ(th505rec.mortcls03,SPACES)
		&& isEQ(th505rec.mortcls04,SPACES)
		&& isEQ(th505rec.mortcls05,SPACES)
		&& isEQ(th505rec.mortcls06,SPACES)) {
			sv.mortcls.set(th505rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(th505rec.liencds,SPACES)) {
			sv.liencd.set(SPACES);
			//sv.liencdOut[varcom.nd.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(th505rec.liencd01,SPACES)
		&& isEQ(th505rec.liencd02,SPACES)
		&& isEQ(th505rec.liencd03,SPACES)
		&& isEQ(th505rec.liencd04,SPACES)
		&& isEQ(th505rec.liencd05,SPACES)
		&& isEQ(th505rec.liencd06,SPACES)) {
			sv.liencd.set(th505rec.liencd01);
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void ageProcessing50a0()
	{
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (isEQ(covtmjaIO.getJlife(),"01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults5300();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& isNE(th505rec.eaage,SPACES)) {
			riskCessDate5400();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& isNE(th505rec.eaage,SPACES)) {
			premCessDate5450();
		}
		if (isNE(th505rec.eaage,SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void optionsExtras50b0()
	{
		if (isEQ(th505rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
		/*B2-PREMIUM-BREAKDOWN*/
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy().toString());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());/* IJTI-1386 */
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
	}

protected void enquiryProtect50c0()
	{
		if (compEnquiry.isTrue()) {
			if (compEnquiry.isTrue()
			|| compApp.isTrue()
			|| revComp.isTrue()) {
				sv.zdivoptOut[varcom.pr.toInt()].set("Y");
				sv.suminOut[varcom.pr.toInt()].set("Y");
				sv.pcessageOut[varcom.pr.toInt()].set("Y");
				sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
				sv.rcessageOut[varcom.pr.toInt()].set("Y");
				sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
				sv.mortclsOut[varcom.pr.toInt()].set("Y");
				sv.liencdOut[varcom.pr.toInt()].set("Y");
				sv.instprmOut[varcom.pr.toInt()].set("Y");
				sv.selectOut[varcom.pr.toInt()].set("Y");
				sv.bappmethOut[varcom.pr.toInt()].set("Y");
				sv.rcesdteOut[varcom.pr.toInt()].set("Y");
				sv.prmbasisOut[varcom.pr.toInt()].set("Y");
				sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
			}
		}
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{	
		if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
				sv.prmbasisErr.set(errorsInner.e186);
	}
}
protected void checkIPPfields()
{
	boolean th505Flag=false;
	
	for(int counter=1; counter<th505rec.prmbasis.length;counter++){
		if(isNE(sv.prmbasis,SPACES)){
			if (isEQ(th505rec.prmbasis[counter], sv.prmbasis)){
				th505Flag=true;
				break;
			}
		}
	}
	if(!th505Flag && isNE(sv.prmbasis,SPACES)){
		sv.prmbasisErr.set("RFV1");// new error code
	}
	
}


protected void readCovr5100()
	{
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
			if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
					|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
					|| isNE(covr.getLife(),wsaaCovrLife)
					|| isNE(covr.getCoverage(),wsaaCovrCoverage)
					|| isNE(covr.getRider(),wsaaCovrRider)) {
						return;
					}
		}
	}

protected void calcPremium5200()
	{
		try {
			calcPremium5210();
			methodTable5220();
		}
		catch (GOTOException e){
		}
	}

protected void calcPremium5210()
	{
		wsaaPremStatuz.set("N");
		if (isNE(t5687rec.bbmeth,SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit5290);
		}
	}

protected void methodTable5220()
	{
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/* ILIFE-3142 End */
			t5675rec.t5675Rec.set(itemIO.getGenarea());
		}
	}

protected void checkDefaults5300()
	{
		try {
			searchTable5310();
		}
		catch (GOTOException e){
		}
	}

protected void searchTable5310()
	{
		sub1.set(1);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
		while ( !(isGT(sub1,wsaaMaxOcc))) {
			nextColumn5320();
		}
		
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		goTo(GotoLabel.exit5390);
	}

protected void nextColumn5320()
	{
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.ageIssageTo[sub1.toInt()])
			&& isEQ(th505rec.riskCessageFrom[sub1.toInt()],th505rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(th505rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.ageIssageTo[sub1.toInt()])
			&& isEQ(th505rec.premCessageFrom[sub1.toInt()],th505rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(th505rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.termIssageTo[sub1.toInt()])
			&& isEQ(th505rec.riskCesstermFrom[sub1.toInt()],th505rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(th505rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.termIssageTo[sub1.toInt()])
			&& isEQ(th505rec.premCesstermFrom[sub1.toInt()],th505rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(th505rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		sub1.add(1);
	}

protected void riskCessDate5400()
	{
			riskCessDatePara5400();
		}

protected void riskCessDatePara5400()
	{
		if (isEQ(sv.riskCessAge,ZERO)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.riskCessAge,0),true)
		&& isEQ(isGT(sv.riskCessTerm,0),false)){
			if (isEQ(th505rec.eaage,"A")) {
				if (addComp.isTrue()) {
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge,wszzAnbAtCcd));
			}
			if (isEQ(th505rec.eaage,"E")
			|| isEQ(th505rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.riskCessAge);
			}
		}
		else if (isEQ(isGT(sv.riskCessAge,0),false)
		&& isEQ(isGT(sv.riskCessTerm,0),true)){
			if (isEQ(th505rec.eaage,"A")
			|| isEQ(th505rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.riskCessTerm);
			}
			if (isEQ(th505rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.riskCessTerm,wszzAnbAtCcd),1));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.riskCessDate.set(datcon2rec.intDate2);
		}
	}

protected void premCessDate5450()
	{
			premCessDatePara5450();
		}

protected void premCessDatePara5450()
	{
		if (isEQ(sv.premCessAge,ZERO)
		&& isEQ(sv.premCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.premCessAge,0),true)
		&& isEQ(isGT(sv.premCessTerm,0),false)){
			if (isEQ(th505rec.eaage,"A")) {
				if (addComp.isTrue()) {
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge,wszzAnbAtCcd));
			}
			if (isEQ(th505rec.eaage,"E")
			|| isEQ(th505rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.premCessAge);
			}
		}
		else if (isEQ(isGT(sv.premCessAge,0),false)
		&& isEQ(isGT(sv.premCessTerm,0),true)){
			if (isEQ(th505rec.eaage,"A")
			|| isEQ(th505rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.premCessTerm);
			}
			if (isEQ(th505rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.premCessTerm,wszzAnbAtCcd),1));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.premCessDate.set(datcon2rec.intDate2);
		}
	}

protected void callDatcon25500()
	{
		/*CALL*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkLext5600()
	{
		readLext5610();
	}

protected void readLext5610()
	{
		wsaaLextUpdates.set("N");
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(chdrpf.getChdrcoy().toString());
		lextrevIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		lextrevIO.setLife(covtmjaIO.getLife());
		lextrevIO.setCoverage(covtmjaIO.getCoverage());
		lextrevIO.setRider(covtmjaIO.getRider());
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(),varcom.oK)
		&& isNE(lextrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy().toString(),lextrevIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),lextrevIO.getChdrnum())/* IJTI-1386 */
		|| isNE(covtmjaIO.getLife(),lextrevIO.getLife())
		|| isNE(covtmjaIO.getCoverage(),lextrevIO.getCoverage())
		|| isNE(covtmjaIO.getRider(),lextrevIO.getRider())) {
			lextrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextrevIO.getStatuz(),varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
		if (isNE(lextrevIO.getStatuz(),varcom.endp)) {
			if (isGT(lextrevIO.getTranno(),chdrpf.getTranno())) {
				wsaaLextUpdates.set("Y");
				lextrevIO.setStatuz(varcom.endp);
			}
		}
		while ( !(isEQ(lextrevIO.getStatuz(),varcom.endp))) {
			lextrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lextrevIO);
			if (isNE(lextrevIO.getStatuz(),varcom.oK)
			&& isNE(lextrevIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(lextrevIO.getParams());
				syserrrec.statuz.set(lextrevIO.getStatuz());
				fatalError600();
			}
			if (isNE(chdrpf.getChdrcoy(),lextrevIO.getChdrcoy())
			|| isNE(chdrpf.getChdrnum(),lextrevIO.getChdrnum())
			|| isNE(covtmjaIO.getLife(),lextrevIO.getLife())
			|| isNE(covtmjaIO.getCoverage(),lextrevIO.getCoverage())
			|| isNE(covtmjaIO.getRider(),lextrevIO.getRider())) {
				lextrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isGT(lextrevIO.getTranno(),chdrpf.getTranno())) {
					wsaaLextUpdates.set("Y");
					lextrevIO.setStatuz(varcom.endp);
				}
			}
		}
		
	}

protected void checkPcdt5700()
	{
		readPcdt5710();
	}

protected void readPcdt5710()
	{
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		pcdtmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		pcdtmjaIO.setLife(covtmjaIO.getLife());
		pcdtmjaIO.setCoverage(covtmjaIO.getCoverage());
		pcdtmjaIO.setRider(covtmjaIO.getRider());
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),pcdtmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),pcdtmjaIO.getChdrnum())
		|| isNE(covtmjaIO.getLife(),pcdtmjaIO.getLife())
		|| isNE(covtmjaIO.getCoverage(),pcdtmjaIO.getCoverage())
		|| isNE(covtmjaIO.getRider(),pcdtmjaIO.getRider())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(pcdtmjaIO.getStatuz(),varcom.endp)) {
			sv.comind.set(" ");
		}
		else {
			sv.comind.set("+");
		}
	}

protected void recordToScreen6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covrRecord6010();
					fieldsToScreen6020();
				case covtExist6025: 
					covtExist6025();
					enquiryProtect6030();
					optionsExtras603a();
					premiumBreakdown603c();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covrRecord6010()
	{
		cashDivDet6100();
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			covtmjaIO.setSex01(wsaaSex);
			covtmjaIO.setSex02(wsbbSex);
			covtmjaIO.setAnbAtCcd01(wsaaAnbAtCcd);
			covtmjaIO.setAnbAtCcd02(wsbbAnbAtCcd);
			covtmjaIO.setCrtable(covrpf.getCrtable());
			covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
			covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
			covtmjaIO.setRiskCessAge(covrpf.getRiskCessAge());
			covtmjaIO.setPremCessAge(covrpf.getPremCessAge());
			covtmjaIO.setRiskCessTerm(covrpf.getRiskCessTerm());
			covtmjaIO.setPremCessTerm(covrpf.getPremCessTerm());
			covtmjaIO.setReserveUnitsDate(covrpf.getReserveUnitsDate());
			covtmjaIO.setReserveUnitsInd(covrpf.getReserveUnitsInd());
			covtmjaIO.setSumins(covrpf.getSumins());
			covtmjaIO.setInstprem(covrpf.getInstprem());
			covtmjaIO.setZbinstprem(covrpf.getZbinstprem());
			covtmjaIO.setZlinstprem(covrpf.getZlinstprem());
			covtmjaIO.setMortcls(covrpf.getMortcls());
			covtmjaIO.setLiencd(covrpf.getLiencd());
			covtmjaIO.setJlife(covrpf.getJlife());
			covtmjaIO.setEffdate(covrpf.getCrrcd());
			covtmjaIO.setBillfreq(payrIO.getBillfreq());
			covtmjaIO.setBillchnl(payrIO.getBillchnl());
			covtmjaIO.setSingp(covrpf.getSingp());
			sv.statFund.set(covrpf.getStatFund());
			sv.statSect.set(covrpf.getStatSect());
			covtmjaIO.setPayrseqno(covrpf.getPayrseqno());
			covtmjaIO.setBappmeth(covrpf.getBappmeth());
			sv.statSubsect.set(covrpf.getStatSubsect());
			covtmjaIO.setPolinc(chdrpf.getPolinc());
			if (isEQ(hcsdIO.getStatuz(),varcom.mrnf)) {
				covtmjaIO.setZdivopt(SPACES);
				covtmjaIO.setPaycoy(SPACES);
				covtmjaIO.setPayclt(SPACES);
				covtmjaIO.setPaymth(SPACES);
				covtmjaIO.setBankkey(SPACES);
				covtmjaIO.setBankacckey(SPACES);
				covtmjaIO.setPaycurr(SPACES);
				covtmjaIO.setFacthous(SPACES);
			}
			else {
				covtmjaIO.setZdivopt(hcsdIO.getZdivopt());
				covtmjaIO.setPaycoy(hcsdIO.getPaycoy());
				covtmjaIO.setPayclt(hcsdIO.getPayclt());
				covtmjaIO.setPaymth(hcsdIO.getPaymth());
				covtmjaIO.setBankkey(hcsdIO.getBankkey());
				covtmjaIO.setBankacckey(hcsdIO.getBankacckey());
				covtmjaIO.setPaycurr(hcsdIO.getPaycurr());
				covtmjaIO.setFacthous(hcsdIO.getFacthous());
			}
		}
	}

protected void fieldsToScreen6020()
	{
		if (isEQ(covtmjaIO.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.covtExist6025); 
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(sv.sumin, 2).set((sub(covtmjaIO.getSumins(),(div(mult(covtmjaIO.getSumins(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.instPrem, 2).set((sub(covtmjaIO.getInstprem(),(div(mult(covtmjaIO.getInstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zbinstprem, 2).set((sub(covtmjaIO.getZbinstprem(),(div(mult(covtmjaIO.getZbinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zlinstprem, 2).set((sub(covtmjaIO.getZlinstprem(),(div(mult(covtmjaIO.getZlinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(sv.sumin, 3).setRounded(div(covtmjaIO.getSumins(),chdrpf.getPolsum()));
				compute(sv.instPrem, 3).setRounded(div(covtmjaIO.getInstprem(),chdrpf.getPolsum()));
				compute(sv.zbinstprem, 3).setRounded(div(covtmjaIO.getZbinstprem(),chdrpf.getPolsum()));
				compute(sv.zlinstprem, 3).setRounded(div(covtmjaIO.getZlinstprem(),chdrpf.getPolsum()));
			}
		}
		else {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.fltmort);
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */

			sv.instPrem.set(covtmjaIO.getInstprem());
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(wsaaOldSumins, 0).set((sub(wsaaOldSumins,(div(mult(wsaaOldSumins,(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(wsaaOldPrem, 2).set((sub(wsaaOldPrem,(div(mult(wsaaOldPrem,(sub(chdrpf.getPolsum(),1))),chdrmjaIO.getPolsum())))));
			}
			else {
				compute(wsaaOldSumins, 1).setRounded(div(wsaaOldSumins,chdrpf.getPolsum()));
				compute(wsaaOldPrem, 3).setRounded(div(wsaaOldPrem,chdrpf.getPolsum()));
			}
		}
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560 
		}
	}

protected void covtExist6025()
	{
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)) {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.fltmort);
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */
			sv.instPrem.set(covtmjaIO.getInstprem());
		}
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premCessDate.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.riskCessDate.set(covtmjaIO.getRiskCessDate());
		sv.riskCessAge.set(covtmjaIO.getRiskCessAge());
		sv.riskCessTerm.set(covtmjaIO.getRiskCessTerm());
		sv.zdivopt.set(covtmjaIO.getZdivopt());
		sv.paymth.set(covtmjaIO.getPaymth());
		sv.paycurr.set(covtmjaIO.getPaycurr());
		if (isNE(covtmjaIO.getBankkey(),SPACES)) {
			sv.bankaccreq.set("+");
		}
		else {
			sv.bankaccreq.set(SPACES);
		}
		if (isNE(covtmjaIO.getPayclt(),SPACES)) {
			cltsIO.setDataKey(SPACES);
			cltsIO.setClntnum(covtmjaIO.getPayclt());
			payeeName6200();
			sv.payeesel.set(covtmjaIO.getPayclt());
		}
		if (isNE(sv.paymth,SPACES)) {
			mopDescription6300();
		}
		setupBonus1300();
		sv.bappmeth.set(covtmjaIO.getBappmeth());
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560
		}
	}

protected void enquiryProtect6030()
	{
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.zdivoptOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
		if (!addComp.isTrue()) {
			tableLoads7000();
		}
		if (isEQ(th505rec.sumInsMax,0)
		&& isEQ(th505rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)
		&& isNE(th505rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(th505rec.sumInsMax);
		}
	}

protected void optionsExtras603a()
	{
		if (isEQ(th505rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
	}

protected void premiumBreakdown603c()
	{
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/*EXIT*/
	}

protected void cashDivDet6100()
	{
		cashDiv6100();
	}

protected void cashDiv6100()
	{
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(covrpf.getChdrcoy());/* IJTI-1386 */
		hcsdIO.setChdrnum(covrpf.getChdrnum());/* IJTI-1386 */
		hcsdIO.setLife(covrpf.getLife());
		hcsdIO.setCoverage(covrpf.getCoverage());
		hcsdIO.setRider(covrpf.getRider());
		hcsdIO.setPlanSuffix(covrpf.getPlanSuffix());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)
		&& isNE(hcsdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			fatalError600();
		}
	}

protected void payeeName6200()
	{
		/*PAYEE*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.payeenme.set(wsspcomn.longconfname);
		/*EXIT*/
	}

protected void mopDescription6300()
	{
		mopDesc6300();
	}

protected void mopDesc6300()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5690);
		descIO.setDescitem(sv.paymth);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pymdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pymdesc.fill("?");
		}
	}

protected void tableLoads7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t5687Load7010();
					t5671Load7020();
				case itemCall7023: 
					itemCall7023();
					editRules7030();
					th505Load7040();
					t5667Load7045();
					t5675Load7050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t5687Load7010()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void t5671Load7020()
	{
		/* Coverage/Rider Switching.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(wsaaCrtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
	}

protected void itemCall7023()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		else {
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				wsbbCrtable.set("****");
				itemIO.setItemitem(wsbbTranCrtable);
				goTo(GotoLabel.itemCall7023);
			}
			else {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
	}

protected void editRules7030()
	{
		if (isEQ(t5671rec.pgm[1],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
	}

protected void th505Load7040()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th505);
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(tablesInner.th505, itdmIO.getItemtabl())
		|| isNE(wsbbTranCurrency,itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			th505rec.th505Rec.set(itdmIO.getGenarea());
		}
		else {
			th505rec.th505Rec.set(SPACES);
			th505rec.ageIssageFrms.fill("0");
			th505rec.ageIssageTos.fill("0");
			th505rec.termIssageFrms.fill("0");
			th505rec.termIssageTos.fill("0");
			th505rec.premCessageFroms.fill("0");
			th505rec.premCessageTos.fill("0");
			th505rec.premCesstermFroms.fill("0");
			th505rec.premCesstermTos.fill("0");
			th505rec.riskCessageFroms.fill("0");
			th505rec.riskCessageTos.fill("0");
			th505rec.riskCesstermFroms.fill("0");
			th505rec.riskCesstermTos.fill("0");
			th505rec.sumInsMax.set(ZERO);
			th505rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode,SPACES)) {
				scrnparams.errorCode.set(errorsInner.f335);
			}
		}
		if(isEQ(th505rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
}

protected void t5667Load7045()
	{
		itemIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
	}

protected void t5675Load7050()
	{
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			calcPremium5200();
		}
		/*EXIT*/
	}

protected void readPovr8000()
	{
		start8010();
	}

protected void start8010()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)
		&& isNE(povrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void pbindExe8100()
	{
		start8110();
	}

protected void start8110()
	{
		readPovr8000();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(),wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(),wsaaPovrLife)
		|| isNE(povrIO.getCoverage(),wsaaPovrCoverage)
		|| isNE(povrIO.getRider(),wsaaPovrRider)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		sv.pbind.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet8200()
	{
		start8210();
	}

protected void start8210()
	{
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		sv.pbind.set("+");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void vf2MaltPovr8300()
	{
		start8310();
	}

protected void start8310()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setValidflag("2");
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void taxExe8500()
	{
		start8510();
	}

protected void start8510()
	{
		/* Keep the CHDR/COVT record .......                               */
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction("KEEPS");
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		wsaaStoreZbinstprem.set(covtmjaIO.getZbinstprem());
		wsaaStoreSingp.set(covtmjaIO.getSingp());
		wsaaStoreInstprem.set(covtmjaIO.getInstprem());
		covtmjaIO.setZbinstprem(sv.zbinstprem);
		if (isEQ(payrIO.getBillfreq(), "00")) {
			covtmjaIO.setSingp(sv.instPrem);
			covtmjaIO.setInstprem(0);
		}
		else {
			covtmjaIO.setInstprem(sv.instPrem);
			covtmjaIO.setSingp(0);
		}
		covtmjaIO.setFunction("KEEPS");
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		wssplife.effdate.set(payrIO.getBtdate());
		/* The first thing to consider is the handling of an Options/      */
		/*   Extras request. If the indicator is 'X', a request to visit   */
		/*   options and extras has been made. In this case:               */
		/*    - change the options/extras request indicator to '?',        */
		sv.taxind.set("?");
		/* Save the next 8 programs from the program stack.                */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			save4510();
		}
		/* Call GENSSWCH with and action of 'A' to retrieve the program    */
		/*   switching required, and move them to the stack.               */
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.                          */
		/* Add one to the program pointer and exit.                        */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet8600()
	{
		/*START*/
		sv.taxind.set("+");
		covtmjaIO.setZbinstprem(wsaaStoreZbinstprem);
		covtmjaIO.setSingp(wsaaStoreSingp);
		covtmjaIO.setInstprem(wsaaStoreInstprem);
		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restore4610();
		}
		/* Blank out the stack "action".                                   */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/* Set WSSP-NEXTPROG to the current screen name (thus returning    */
		/*    to re-display the screen).                                   */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void a200CheckCalcTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a210Start();
				case a250CallTaxSubr: 
					a250CallTaxSubr();
				case a290Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210Start()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.a290Exit);
		}
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.a250CallTaxSubr);
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covtmjaIO.getCrtable());
		a300ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void a250CallTaxSubr()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covtmjaIO.getLife());
			txcalcrec.coverage.set(covtmjaIO.getCoverage());
			txcalcrec.rider.set(covtmjaIO.getRider());
			txcalcrec.planSuffix.set(covtmjaIO.getPlanSuffix());
			txcalcrec.crtable.set(covtmjaIO.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());/* IJTI-1386 */
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.instPrem);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrpf.getPtdate());
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(sv.instPrem);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
			}
		}
	}

protected void a300ReadTr52e()
	{
		a310Start();
	}

protected void a310Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrpf.getPtdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}
/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e133 = new FixedLengthStringData(4).init("E133");
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData hl52 = new FixedLengthStringData(4).init("HL52");
	private FixedLengthStringData ev01 = new FixedLengthStringData(4).init("EV01");
	private FixedLengthStringData f008 = new FixedLengthStringData(4).init("F008");
	private FixedLengthStringData f020 = new FixedLengthStringData(4).init("F020");
	private FixedLengthStringData f177 = new FixedLengthStringData(4).init("F177");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f335 = new FixedLengthStringData(4).init("F335");
	private FixedLengthStringData f404 = new FixedLengthStringData(4).init("F404");
	private FixedLengthStringData f826 = new FixedLengthStringData(4).init("F826");
	private FixedLengthStringData f862 = new FixedLengthStringData(4).init("F862");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h960 = new FixedLengthStringData(4).init("H960");
	private FixedLengthStringData z038 = new FixedLengthStringData(4).init("Z038");
	private FixedLengthStringData z042 = new FixedLengthStringData(4).init("Z042");
	private FixedLengthStringData z043 = new FixedLengthStringData(4).init("Z043");
	private FixedLengthStringData rrj4 = new FixedLengthStringData(4).init("RRJ4");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");	//ICIL-1310
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5690 = new FixedLengthStringData(5).init("T5690");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData th500 = new FixedLengthStringData(5).init("TH500");
	private FixedLengthStringData th505 = new FixedLengthStringData(5).init("TH505");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData th528 = new FixedLengthStringData(5).init("TH528");//fwang3 ICIL-560
	private FixedLengthStringData ta610 = new FixedLengthStringData(5).init("TA610");	//ICIL-1310
	private FixedLengthStringData t3644 = new FixedLengthStringData(5).init("T3644");	//ICIL-1310
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData clbaddbrec = new FixedLengthStringData(10).init("CLBADDBREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC   ");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC   ");
	private FixedLengthStringData covrrgwrec = new FixedLengthStringData(10).init("COVRRGWREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
}
}
