/*
 * File: Ph537.java
 * Date: 30 August 2009 1:06:28
 * Author: Quipoz Limited
 * 
 * Class transformed from PH537.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivcshTableDAM;
import com.csc.life.cashdividends.screens.Sh537ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*    Cash Dividend Allocation Inquiry
*
*    This program is aim to provide an Enquiry which display
*    Cash Dividend Allocation Details attached of the cash
*    dividend coverage.
*
* Initialise.
* -----------
*
* Read the  CHDRENQ contract  (function  RETRV)  and  read  the
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured, etc.
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format the Ownername and the subsequent Life name by first
*  reading the respective Client (CLTS) record and use the
*  CONFNAME procedure division copybook to format the required
*  names.
*
* LIFE ASSURED DETAILS
*
* The Life Client numbers can be obtained from the
* first Coverage record read.
* To obtain the life assured details (if any) do the following:-
*
*      - READR  the   life  details  using  LIFExxx  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*
* Cash Dividend Allocation Inquiry
*
* Obtain from HDIS and HDIVCSH.
*
* Validate.
* ---------
*
* Next Program.
* -------------
*    Add 1 to program pointer
*
* TABLES USED.
* ------------
*
* T3588 - Contract Premium statii
* T3623 - Contract Risk statii
* T5687 - Coverage/Rider Details
* T5688 - Contract Types
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Ph537 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH537");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
		/* FORMATS */
	private String descrec = "DESCREC";
	private String cltsrec = "CLTSREC";
	private String hdisrec = "HDISREC";
	private String hdivcshrec = "HDIVCSHREC";
	private String chdrenqrec = "CHDRENQREC";
	private String covrenqrec = "COVRENQREC";
	private String lifeenqrec = "LIFEENQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		
			//ILIFE-4325 changes started
		/*Contract Enquiry - Contract Header.*/
	//private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrenqIO = new Chdrpf();
	
		/*Contract Enquiry - Coverage Details.*/
	//private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrenqIO = new Covrpf();
			//ILIFE-4325 changes ended
	
	/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Cash Withdraw Trans. Details*/
	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sh537ScreenVars sv = ScreenProgram.getScreenVars( Sh537ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1790, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Ph537() {
		super();
		screenVars = sv;
		new ScreenModel("Sh537", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
	/*	sv.dataArea.set(SPACES);
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.currcd.set(chdrenqIO.getCntcurr());
		sv.cownnum.set(chdrenqIO.getCownnum());
		covrenqIO.setParams(SPACES);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}*/
			//ILIFE-4325 modification started
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(cntDteFlag)	{
					sv.iljCntDteFlag.set("Y");
				} else {
					sv.iljCntDteFlag.set("N");
				}
		//ILJ-49 End 
		chdrenqIO = chdrDao.getCacheObject(chdrenqIO);
		
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.currcd.set(chdrenqIO.getCntcurr());
		sv.cownnum.set(chdrenqIO.getCownnum());
		
		covrenqIO = covrDao.getCacheObject(covrenqIO);
		//ILIFE-4325 modification ended
		sv.life.set(covrenqIO.getLife());
		sv.coverage.set(covrenqIO.getCoverage());
		sv.rider.set(covrenqIO.getRider());
		sv.crtable.set(covrenqIO.getCrtable());
		sv.occdate.set(covrenqIO.getCrrcd());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covrenqIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(chdrenqIO.getChdrcoy());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(chdrenqIO.getChdrcoy());
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(chdrenqIO.getChdrcoy());
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstcde());			//ILIFE-4325
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntcoy(chdrenqIO.getCowncoy());
		cltsIO.setClntnum(chdrenqIO.getCownnum());
		readClts1300();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),"1")) {
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		readLife1400();
		if (isEQ(lifeenqIO.getStatuz(),varcom.mrnf)
		|| isNE(lifeenqIO.getValidflag(),"1")) {
			sv.linsname.set(SPACES);
		}
		else {
			sv.lifcnum.set(lifeenqIO.getLifcnum());
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		hdisIO.setDataKey(SPACES);
		hdisIO.setChdrcoy(covrenqIO.getChdrcoy());
		hdisIO.setChdrnum(covrenqIO.getChdrnum());
		hdisIO.setLife(covrenqIO.getLife());
		hdisIO.setCoverage(covrenqIO.getCoverage());
		hdisIO.setRider(covrenqIO.getRider());
		hdisIO.setPlanSuffix(covrenqIO.getPlanSuffix());
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdisIO.getParams());
			fatalError600();
		}
		sv.firstDivdDate.set(hdisIO.getFirstDivdDate());
		sv.lastDivdDate.set(hdisIO.getLastDivdDate());
		sv.lastIntDate.set(hdisIO.getLastIntDate());
		sv.nextIntDate.set(hdisIO.getNextIntDate());
		sv.lastCapDate.set(hdisIO.getLastCapDate());
		sv.nextCapDate.set(hdisIO.getNextCapDate());
		sv.divdStmtDate.set(hdisIO.getDivdStmtDate());
		sv.divdStmtNo.set(hdisIO.getDivdStmtNo());
		sv.tamt01.set(hdisIO.getBalAtLastDivd());
		sv.tamt02.set(hdisIO.getOsInterest());
		sv.tamt04.set(hdisIO.getBalAtStmtDate());
		hdivcshIO.setDataKey(SPACES);
		hdivcshIO.setChdrcoy(covrenqIO.getChdrcoy());
		hdivcshIO.setChdrnum(covrenqIO.getChdrnum());
		hdivcshIO.setLife(covrenqIO.getLife());
		hdivcshIO.setCoverage(covrenqIO.getCoverage());
		hdivcshIO.setRider(covrenqIO.getRider());
		hdivcshIO.setPlanSuffix(covrenqIO.getPlanSuffix());
		compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
		hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdivcshIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),hdivcshIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),hdivcshIO.getChdrnum())
		|| isNE(covrenqIO.getLife(),hdivcshIO.getLife())
		|| isNE(covrenqIO.getCoverage(),hdivcshIO.getCoverage())
		|| isNE(covrenqIO.getRider(),hdivcshIO.getRider())
		|| isNE(covrenqIO.getPlanSuffix(),hdivcshIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			setPrecision(hdisIO.getBalSinceLastCap(), 2);
			hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(),hdivcshIO.getDivdAmount()));
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdivcshIO.getParams());
				fatalError600();
			}
			if (isNE(covrenqIO.getChdrcoy(),hdivcshIO.getChdrcoy())
			|| isNE(covrenqIO.getChdrnum(),hdivcshIO.getChdrnum())
			|| isNE(covrenqIO.getLife(),hdivcshIO.getLife())
			|| isNE(covrenqIO.getCoverage(),hdivcshIO.getCoverage())
			|| isNE(covrenqIO.getRider(),hdivcshIO.getRider())
			|| isNE(covrenqIO.getPlanSuffix(),hdivcshIO.getPlanSuffix())) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}
		
		sv.tamt03.set(hdisIO.getBalSinceLastCap());
	}

protected void readClts1300()
	{
		/*START*/
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readLife1400()
	{
		/*START*/
		lifeenqIO.setFormat(lifeenqrec);
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		for (wsaaX.set(30); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()],SPACES)); wsaaX.add(-1));
		compute(wsaaY, 0).set(sub(30,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		wsaaZ.set(0);
		
		//Modified by Peter Zhang fix bug #739
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		//end
		
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		wsaaX.add(1);
		
		//Modified by Peter Zhang fix bug #739
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		//end
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
