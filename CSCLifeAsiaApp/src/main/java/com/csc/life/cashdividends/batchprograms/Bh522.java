/*
 * File: Bh522.java
 * Date: 29 August 2009 21:29:49
 * Author: Quipoz Limited
 *
 * Class transformed from BH522.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap; //ILIFE-9080
import java.util.Iterator; //ILIFE-9080
import java.util.List;
import java.util.Map; //ILIFE-9080

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Varcom; //ILIFE-9080
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

//ILB-566 starts
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdispfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.dataaccess.model.Hdsxpf; //ILIFE-9080
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdsxpfDAO; //ILIFE-9080
import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
//ILB-566 ends


/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This batch program process the HDIS records extracted
*   in the previous program BH521 based on the Next Interest
*   Date(HINTNDT).
*   Dividend Interest is calculated and allocated to respective
*   HDIS while the subroutine defined is in TH501.  Then these
*   dividend files will be affected,
*        HDIS - dividend transaction summary
*        HDIV - dividend transaction details
*
*
*   PROCESSING
*   ==========
*
*   Initialise
*
*  - Check the restart method is 3.
*  - Find the name of the HDSX temporary file. HDSX is a temporary
*    file which has the name of "HDSX" + first two characters of
*    param 4 + the last four digits of the job number.
*  - Open HDSXPF as input.
*  - Pre-load constantly referenced Tables into working storage.
*    These are: T5688, TH501, T5645.
*  - Set up the general fields in LIFACMV.
*
*  Read.
*
*  - Read a HDSXPF.
*  - Check end of file and end processing
*
*  Edit.
*
*  - Softlock the contract if this HDSX is for a new contract.
*
*  Update.
*
*  - Read HCSD(Cash dividend details)
*  - Read TH501 with cash dividend method in HCSD and effective
*    date = NEXT INTEREST DATE
*  - Read and hold the CHDRLIF(Contract header)
*  - set NEW-TRANNO to CHDRLIF-TRANNO + 1
*  - Read HDIS(Dividend & Interest Summary)
*  - If interest allocation routine in TH501 is not blank,
*    set up the linkage for the new copy book HDVDINTREC to
*    call the dividend interest calc subr.
*  - Accumulate HDIA-INT-AMOUNT into a working field
*  - Calculate interest levied on the withdrawn dividend,
*    by looping through each HDIVCSH from next interest date     .
*    of HDSXPF
*         set up linkage in HDVDINTREC with data in HDIVCSH
*         call the same dividend interest calc subr
*         add HDIA-INT-AMOUNT returned to same working field,
*         note these HDIA-INT-AMOUNT values are expected -ve
*            also apply the fix DD & MM in TH501
*            ensure the DD for the new month is valid
*  - For non zero interest amount accumulated, prepare to write
*    a HDIV record and update the HDIS of it by invalid the
*    existing one and write a new one.
*  - if dividend amount is not zero, write accounting entries,
*         look for the posting level in T5688 for CNTTYPE
*         set up fields in LIFACMV linkage
*         map for the relevant GL details in T5645
*         call LIFACMV twice to post the dividend expense and
*         its opposite entry for payable.
*  - various file updates for transaction history, in CHDRLIF,
*    and PTRN.
*  - Repeat 3000 section, until HDIS next interest date has
*    gone pass the job effective date BSSC-EFFECTIVE-DATE
*
*  Finalise.
*
*  - Close HDSXPF
*  - Remove the override
*  - Set the parameter statuz to O-K
*
*
*   Control totals:
*     01 - No. of HDSX records read
*     02 - No. of Cover with no Dividend
*     03 - No. of HDSX records locked
*     04 - No. of HDSX records processed
*     05 - Total value of Interest
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Bh522 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH522");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaHdsxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHdsxFn, 0, FILLER).init("HDSX");
	private FixedLengthStringData wsaaHdsxRunid = new FixedLengthStringData(2).isAPartOf(wsaaHdsxFn, 4);
	private ZonedDecimalData wsaaHdsxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHdsxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaSystemDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSysDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaSystemDate, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaIntAmount = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaLastTime = new FixedLengthStringData(1);
	private Validator loopLastTime = new Validator(wsaaLastTime, "Y");

	private FixedLengthStringData wsaaT5645Array = new FixedLengthStringData(21000);
	private FixedLengthStringData[] wsaaT5645Data = FLSArrayPartOfStructure(1000, 21, wsaaT5645Array, 0);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5688Array = new FixedLengthStringData(9000);
	private FixedLengthStringData[] wsaaT5688Rec = FLSArrayPartOfStructure(1000, 9, wsaaT5688Array, 0);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(8, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Key, 3);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 8);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
	//private static final int wsaaT5688Size = 100;
	//private static final int wsaaTh501Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;
	private static final int wsaaTh501Size = 1000;
	private PackedDecimalData wsaaNextIntDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaHdisDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaHdisDd = new FixedLengthStringData(2).isAPartOf(wsaaHdisDate, 6);
		/* TABLES */
	private static final String th501 = "TH501";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String h791 = "H791";
	private static final String hl16 = "HL16";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

	private Getdescrec getdescrec = new Getdescrec();
	private Th501rec th501rec = new Th501rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private WsaaTh501ArrayInner wsaaTh501ArrayInner = new WsaaTh501ArrayInner();
	
	private List<Itempf> t5645List = null;
    private List<Itempf> t5688List = null;
    private List<Itempf> th501List = null;
    private Itempf itempf = new Itempf();
    List<Itempf> itempfList = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);

    //ILB-566
	private SftlockUtil sftlockUtil = new SftlockUtil();
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private Chdrpf chdrpf = new Chdrpf();
    private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",HcsdpfDAO.class);
    private Hcsdpf hcsdpf = new Hcsdpf();
    private HdivpfDAO hdivpfDAO = getApplicationContext().getBean("hdivpfDAO",HdivpfDAO.class);
    private Hdivpf hdivpf = new Hdivpf();
    private Itdmpf itdmpf = new Itdmpf();
    private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
    private Ptrnpf ptrnpf = new Ptrnpf();
    private HdispfDAO hdispfDAO = getApplicationContext().getBean("hdispfDAO",HdispfDAO.class);
    private Hdispf hdispf = new Hdispf();
    private ZonedDecimalData wsaaNextIntDatecal = new ZonedDecimalData(8, 0).setUnsigned();
    private FixedLengthStringData wsaaNextIntDateCal = new FixedLengthStringData(8).isAPartOf(wsaaNextIntDatecal, 0, REDEFINE);
	private ZonedDecimalData wsaaNextintYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNextIntDateCal, 0).setUnsigned();
	private ZonedDecimalData wsaaNextintMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntDateCal, 4).setUnsigned();
	private ZonedDecimalData wsaaNextintDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntDateCal, 6).setUnsigned();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private FixedLengthStringData wsaaLastdayOfMonths = new FixedLengthStringData(24).init("312831303130313130313031");

	private FixedLengthStringData wsaaLastddOfMonths = new FixedLengthStringData(24).isAPartOf(wsaaLastdayOfMonths, 0, REDEFINE);
	private ZonedDecimalData[] wsaaLastdd = ZDArrayPartOfStructure(12, 2, 0, wsaaLastddOfMonths, 0, UNSIGNED_TRUE);
	
    
    //ILIFE-9080 start    
    private int ct01Value;
    private int ct02Value;
    private int ct03Value;
	private int ct04Value;
	private BigDecimal ct05Value = BigDecimal.ZERO;
	private int batchID;
	private int batchExtractSize;
	private Hdsxpf hdsxpf; 
	private HdsxpfDAO hdsxpfDAO = getApplicationContext().getBean("hdsxpfDAO", HdsxpfDAO.class);
	private Iterator<Hdsxpf> iter;
    private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<>();
    private List<Ptrnpf> ptrnpfList = new ArrayList<>();
    private List<Chdrpf> chdrpfListUpdt = new ArrayList<>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<>();
	private Map<String, List<Hcsdpf>> hcsdListMap = new HashMap<>();
	private Map<String, List<Hdispf>> hdisListMap = new HashMap<>();
	private Map<String, List<Hdivpf>> hdivcshListMap = new HashMap<>();
	private List<Hdivpf> hdivpfList = new ArrayList<>();
	private List<Hdispf> hdisList = new ArrayList<>();
 //ILIFE-9080 end
	private List<Hdispf> hdisupdateList = new ArrayList<>();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		loop3020
	}

	public Bh522() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
 //ILIFE-9080 start		
		wsaaHdsxRunid.set(bprdIO.getSystemParam04());
		wsaaHdsxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
        if(wsspEdterror.equals(varcom.endp)){
        	return;
        }
  //ILIFE-9080 end
		wsaaSystemDate.set(getCobolDate());
		
		t5645List=itempfDAO.findItem("IT", bsprIO.getCompany().toString(), t5645, wsaaProg.toString());
		t5688List=itempfDAO.findItem(bsprIO.getCompany().toString(), t5688, varcom.vrcmMaxDate.toInt());
		th501List=itempfDAO.findItem(bsprIO.getCompany().toString(), th501, varcom.vrcmMaxDate.toInt());
		
		if (t5645List.size() > wsaaT5645Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(t5645);
            fatalError600();
        }
		loadT56451200(t5645List);
		
		if (t5688List.size() > wsaaT5688Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(t5688);
            fatalError600();
        }
		loadT56881300(t5688List);
		
		if (th501List.size() > wsaaTh501Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(th501);
            fatalError600();
        }
			loadTh5011500(th501List);
		
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		itempf.setItempfx("IT");
		itempf.setItemcoy(bsprIO.getCompany().toString());
		itempf.setItemtabl(t1688);
		itempf.setItemitem(bprdIO.getAuthCode().toString());
		getdescrec.itemkey.set(itempf.getItempfx().concat(bsprIO.getCompany().toString()).concat(t1688).concat(bprdIO.getAuthCode().toString()));
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
	}
 //ILIFE-9080 start
private void readChunkRecord() {
	ct01Value = 0;
	ct02Value = 0;
	ct03Value = 0;
	ct04Value = 0;
	ct05Value = BigDecimal.ZERO;
	
	List<Hdsxpf> hdsxpfList = hdsxpfDAO.searchHdsxpfRecord(wsaaHdsxFn.toString(), wsaaThreadMember.toString(), 
			batchExtractSize, batchID);
	iter= hdsxpfList.iterator();
	if (!hdsxpfList.isEmpty()) {
	List<String> chdrnumList = new ArrayList<>(hdsxpfList.size());
	for (Hdsxpf p : hdsxpfList) {
		chdrnumList.add(p.getChdrnum());
	}
	String coy = bsprIO.getCompany().toString();
	chdrpfMap = chdrpfDAO.searchChdrpf(coy, chdrnumList);
	hcsdListMap = hcsdpfDAO.getHcsdMap(coy, chdrnumList); 
	hdisListMap = hdispfDAO.getHdisMap(coy, chdrnumList);
	hdivcshListMap = hdivpfDAO.getHdivMap(coy, chdrnumList);
	}
}
 //ILIFE-9080 end


protected void loadT56451200(List<Itempf> itemList)
{
       int i = 1;
       int x = 1;
       for (Itempf itempf : itemList) {
           	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
         //  	ix.set(1);
			while ( !(isGT(x,15))) {
			wsaaT5645Cnttot[i].set(t5645rec.cnttot[x]);
			wsaaT5645Glmap[i].set(t5645rec.glmap[x]);
			wsaaT5645Sacscode[i].set(t5645rec.sacscode[x]);
			wsaaT5645Sacstype[i].set(t5645rec.sacstype[x]);
			wsaaT5645Sign[i].set(t5645rec.sign[x]);
			/*ix.add(1);
			iy.add(1);*/
			x++;
           i++;
		 }
       }
   }

protected void loadT56881300(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
           t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
           wsaaT5688Cnttype[i].set(itempf.getItemitem());
           wsaaT5688Itmfrm[i].set(itempf.getItmfrm());
           wsaaT5688Comlvlacc[i].set(t5688rec.comlvlacc);
           i++;
       }
   }

protected void loadTh5011500(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
    	   	th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
           	wsaaTh501ArrayInner.wsaaTh501Divdmth[i].set(itempf.getItemitem());
	   		wsaaTh501ArrayInner.wsaaTh501Itmfrm[i].set(itempf.getItmfrm());
	   		wsaaTh501ArrayInner.wsaaTh501IntFq[i].set(th501rec.freqcy02);
	   		wsaaTh501ArrayInner.wsaaTh501Calcprog[i].set(th501rec.intCalcSbr);
	   		wsaaTh501ArrayInner.wsaaTh501CapFq[i].set(th501rec.freqcy01);
	   		wsaaTh501ArrayInner.wsaaTh501CapDd[i].set(th501rec.hfixdd01);
	   		wsaaTh501ArrayInner.wsaaTh501IntDd[i].set(th501rec.hfixdd02);
	   		wsaaTh501ArrayInner.wsaaTh501CapMm[i].set(th501rec.hfixmm01);
	   		wsaaTh501ArrayInner.wsaaTh501IntMm[i].set(th501rec.hfixmm02);
           i++;
       }
   }


protected void readFile2000()
	{
		/*READ-FILE*/
 //ILIFE-9080 start
		if (!iter.hasNext()) {
			batchID++;
			readChunkRecord();
			if (!iter.hasNext()) {
				wsspEdterror.set(Varcom.endp);
				return;
			}
			
		}
		this.hdsxpf = iter.next();
		ct01Value++;
 //ILIFE-9080 end
		/*EXIT*/
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		wsspEdterror.set(SPACES);
 //ILIFE-9080 start
		if (isEQ(hdsxpf.getChdrnum(),wsaaALockedChdrnum)) {
			ct03Value++;
			return ;
		}
		if (isNE(hdsxpf.getChdrnum(),lastChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz,"LOCK")) {
				wsaaALockedChdrnum.set(hdsxpf.getChdrnum());
				return ;
			}
			else {
				wsaaALockedChdrnum.set(SPACES);
			}
			lastChdrnum.set(hdsxpf.getChdrnum());
			wsaaJrnseq.set(ZERO);
		}
		/*    Skip processing this HDSX if its dividend balance is zero*/
		if (isEQ(hdsxpf.getHdvbalc(),0)) {
			ct02Value++;
 //ILIFE-9080 end
			return ;
		}
		wsspEdterror.set(varcom.oK);
	}

protected void softlockPolicy2600()
	{
		softlockPolicy2610();
	}

protected void softlockPolicy2610()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(hdsxpf.getChdrcoy()); //ILIFE-9080
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(hdsxpf.getChdrnum()); //ILIFE-9080
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hdsxpf.getChdrcoy()); //ILIFE-9080
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(hdsxpf.getChdrnum()); //ILIFE-9080
			stringVariable1.setStringInto(conlogrec.params);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++; //ILIFE-9080
		}
		else {
			if (isNE(sftlockrec.statuz,varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
		
		SftlockRecBean sftlockRecBean = new SftlockRecBean();
    	sftlockRecBean.setFunction("LOCK");
    	sftlockRecBean.setCompany(hdsxpf.getChdrcoy()); //ILIFE-9080
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(hdsxpf.getChdrnum()); //ILIFE-9080
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (isEQ(sftlockRecBean.getStatuz(),"LOCK")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(hdsxpf.getChdrcoy()); //ILIFE-9080
			stringVariable1.append('|');
			stringVariable1.append(hdsxpf.getChdrnum()); //ILIFE-9080
			conlogrec.params.set(stringVariable1.toString());
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++; //ILIFE-9080
		}
    	else {
    	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
    		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
    			syserrrec.statuz.set(sftlockRecBean.getStatuz());
    			fatalError600();
    		}
    	}
		
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update3010();
				case loop3020:
					loop3020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
 //ILIFE-9080 starr	
		String chdrnum = hdsxpf.getChdrnum();
	    if (hcsdListMap != null && hcsdListMap.containsKey(chdrnum)) {
	        List<Hcsdpf> hcsdpfList = hcsdListMap.get(chdrnum);
	        for (Iterator<Hcsdpf> iterator = hcsdpfList.iterator(); iterator.hasNext(); ){
	        	Hcsdpf hcsd = iterator.next();
	        	if (hcsd.getLife().equals(hdsxpf.getLife())
	        		&&  hcsd.getCoverage().equals(hdsxpf.getCoverage())
	        		&& 	hcsd.getRider().equals(hdsxpf.getRider())
	        		&& Integer.compare(hcsd.getPlnsfx(),hdsxpf.getPlnsfx()) == 0 ) {
	        		hcsdpf = hcsd;
	        		break;
	        			
	        	}
	        }
	    }	
	    if(hcsdpf == null) {
	    	syserrrec.params.set(hdsxpf.getChdrnum());
			fatalError600();
	    }
		
 //ILIFE-9080 end
		/*  Set last time loop flag to blank.*/
		wsaaLastTime.set(SPACES);
	}

protected void loop3020()
	{	
 //ILIFE-9080 start
		   String chdrnum = hdsxpf.getChdrnum();
		   if (hdisListMap != null && hdisListMap.containsKey(chdrnum)) {
		        List<Hdispf> hdispfList = hdisListMap.get(chdrnum);
		        for (Iterator<Hdispf> iterator = hdispfList.iterator(); iterator.hasNext(); ){
		        	Hdispf hdis = iterator.next();
		        	if (hdis.getLife().equals(hdsxpf.getLife())
		            		&&  hdis.getCoverage().equals(hdsxpf.getCoverage())
		            		&& 	hdis.getRider().equals(hdsxpf.getRider())
		            		&& Integer.compare(hdis.getPlnsfx(),hdsxpf.getPlnsfx()) == 0 ) {
		        			hdispf = hdis;
		            			break;
		            			
		            	}
		        }
		   }
		   if(!(hdisList.isEmpty())) {
	        	int maxtranno = 0;
				for(Hdispf h : hdisList) {
					if(h.getChdrnum().equals(lastChdrnum.toString())) {
						if(maxtranno < h.getTranno()) {
							maxtranno = h.getTranno();
							hdispf = h;
						}
					}
				}
	        }
	 //ILIFE-9080 end
		/*    Do not allocate interest if next interest date > next*/
		/*    capitalisation date.*/
		if (isGT(hdsxpf.getHintndt(),hdispf.getHcapndt())
		&& isEQ(hdispf.getHintldt(),hdispf.getHcapndt())) {
			return ;
		}
		/*    Allocate interest for partial month leading to*/
		/*    the next capitalisation date.*/
		wsaaNextIntDate.set(varcom.vrcmMaxDate);
		if (isGT(hdsxpf.getHintndt(),hdispf.getHcapndt())) {
			wsaaNextIntDate.set(hdsxpf.getHintndt());
			hdsxpf.setHintndt(hdispf.getHcapndt());
		}
		/*    Match for the TH501 item with the Cash Dividend method held*/
		/*    on HCSD and Next Interest Date from array.*/
		for (ix.set(1); !(isGT(ix,wsaaTh501Size)
		|| isEQ(wsaaTh501ArrayInner.wsaaTh501Divdmth[ix.toInt()], SPACES)
		|| (isEQ(wsaaTh501ArrayInner.wsaaTh501Divdmth[ix.toInt()], hcsdpf.getZcshdivmth())
		&& isLTE(wsaaTh501ArrayInner.wsaaTh501Itmfrm[ix.toInt()], hdsxpf.getHintndt()))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaTh501Size)
		|| isEQ(wsaaTh501ArrayInner.wsaaTh501Divdmth[ix.toInt()], SPACES)) {
			//itdmIO.setFunction(varcom.begn);
			//itdmIO.setStatuz(varcom.endp);
			itdmpf.setItemcoy(bsprIO.getCompany().toString());
			itdmpf.setItemtabl(th501);
			itdmpf.setItemitem(hcsdpf.getZcshdivmth());
			itdmpf.setItmfrm(hdsxpf.getHintndt());
			fatalError600();
		}
		if (isEQ(wsaaTh501ArrayInner.wsaaTh501Calcprog[ix.toInt()], SPACES)) {
			fatalError600();
		}
		
	 //ILIFE-9080 start
		if(chdrpfMap!=null &&chdrpfMap.containsKey(hdsxpf.getChdrnum())){
			for(Chdrpf c:chdrpfMap.get(hdsxpf.getChdrnum())){
				if(c.getChdrcoy().toString().equals(hdsxpf.getChdrcoy())){
					chdrpf = c;
					break;
				}
			}
		}
		if(!(chdrpfListInst.isEmpty())) {
			int maxtranno = 0;
			for(Chdrpf c : chdrpfListInst) {
				if(c.getChdrnum().equals(lastChdrnum.toString())) {
					if(maxtranno < c.getTranno()) {
						maxtranno = c.getTranno();
						chdrpf = c;
					}
				}
			}
		}
		if(chdrpf == null){
			syserrrec.params.set(hdsxpf.getChdrnum());
			fatalError600();
		}
		compute(wsaaNewTranno, 0).set(add(chdrpf.getTranno(),1));
		
	    List<Hdispf> hdispfList = hdisListMap.get(chdrnum);
        for (Iterator<Hdispf> iterator = hdispfList.iterator(); iterator.hasNext(); ){
        	Hdispf hdis = iterator.next();
        	if (hdis.getLife().equals(hdsxpf.getLife())
            		&&  hdis.getCoverage().equals(hdsxpf.getCoverage())
            		&& 	hdis.getRider().equals(hdsxpf.getRider())
            		&& Integer.compare(hdis.getPlnsfx(),hdsxpf.getPlnsfx()) == 0 ) {
        			hdispf = hdis;
            			break;
            	}
        }
        if(!(hdisList.isEmpty())) {
        	int maxtranno = 0;
			for(Hdispf h : hdisList) {
				if(h.getChdrnum().equals(lastChdrnum.toString())) {
					if(maxtranno < h.getTranno()) {
						maxtranno = h.getTranno();
						hdispf = h;
					}
				}
			}
        }
		
 //ILIFE-9080 end
		/*    Initialise the accumulate field for interest*/
		wsaaIntAmount.set(0);
		/*    Prepare to call interest allocation subroutine*/
		initialize(hdvdintrec.divdIntRec);
  //ILIFE-9080 start
		hdvdintrec.chdrChdrcoy.set(hdsxpf.getChdrcoy());
		hdvdintrec.chdrChdrnum.set(hdsxpf.getChdrnum());
		hdvdintrec.lifeLife.set(hdsxpf.getLife());
		hdvdintrec.covrCoverage.set(hdsxpf.getCoverage());
		hdvdintrec.covrRider.set(hdsxpf.getRider());
		hdvdintrec.plnsfx.set(hdsxpf.getPlnsfx());
		hdvdintrec.cntcurr.set(chdrpf.getCntcurr());
		hdvdintrec.transcd.set(bprdIO.getAuthCode());
		hdvdintrec.crtable.set(hdsxpf.getCrtable());
		hdvdintrec.effectiveDate.set(bsscIO.getEffectiveDate());
		hdvdintrec.tranno.set(wsaaNewTranno);
		hdvdintrec.intFrom.set(hdispf.getHintldt());
		hdvdintrec.intTo.set(hdsxpf.getHintndt());
		hdvdintrec.capAmount.set(hdsxpf.getHdvbalc());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Interest Allocation Subroutine*/
		callProgram(wsaaTh501ArrayInner.wsaaTh501Calcprog[ix.toInt()], hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdvdintrec.divdIntRec);
			syserrrec.statuz.set(hdvdintrec.statuz);
			fatalError600();
		}
		/* MOVE HDIA-INT-AMOUNT         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO HDIA-INT-AMOUNT.             */
		if (isNE(hdvdintrec.intAmount, 0)) {
			zrdecplrec.amountIn.set(hdvdintrec.intAmount);
			callRounding8000();
			hdvdintrec.intAmount.set(zrdecplrec.amountOut);
		}
		/*    Accumulate interest amount in a working field*/
		wsaaIntAmount.add(hdvdintrec.intAmount);
		/*    Read through HDIVCSH from next interest date of HDSX,*/
		/*    calculate interest on the withdrawn dividend, accumulate*/
		/*    to the total interest allocated in this run*/
		
		  if (hdivcshListMap != null && hdivcshListMap.containsKey(chdrnum)) {
		        List<Hdivpf> hdivpfList = hdivcshListMap.get(chdrnum);
		        for (Iterator<Hdivpf> iterator = hdivpfList.iterator(); iterator.hasNext(); ){
		        	Hdivpf hdiv = iterator.next();
		        	if (hdiv.getLife().equals(hdsxpf.getLife())
		        		&&  hdiv.getCoverage().equals(hdsxpf.getCoverage())
		        		&& 	hdiv.getRider().equals(hdsxpf.getRider())
		        		&& Integer.compare(hdiv.getPlnsfx(),hdsxpf.getPlnsfx()) == 0 
		        		&& (Integer.compare(hdiv.getHincapdt(),hdispf.getHcapldt() + 1) == 0 
		        		|| Integer.compare(hdiv.getHincapdt(),hdispf.getHcapldt() + 1) < 0)) {

		        		initialize(hdvdintrec.divdIntRec);
		    			hdvdintrec.chdrChdrcoy.set(hdsxpf.getChdrcoy());
		    			hdvdintrec.chdrChdrnum.set(hdsxpf.getChdrnum());
		    			hdvdintrec.lifeLife.set(hdsxpf.getLife());
		    			hdvdintrec.covrCoverage.set(hdsxpf.getCoverage());
		    			hdvdintrec.covrRider.set(hdsxpf.getRider());
		    			hdvdintrec.plnsfx.set(hdsxpf.getPlnsfx());
		    			hdvdintrec.cntcurr.set(chdrpf.getCntcurr());
		    			hdvdintrec.transcd.set(bprdIO.getAuthCode());
		    			hdvdintrec.crtable.set(hdsxpf.getCrtable());
		    			hdvdintrec.effectiveDate.set(bsscIO.getEffectiveDate());
		    			hdvdintrec.tranno.set(wsaaNewTranno);
		    			if (isGT(hdispf.getHintldt(),hdiv.getEffdate())) {
		    				hdvdintrec.intFrom.set(hdispf.getHintldt());
		    			}
		    			else {
		    				hdvdintrec.intFrom.set(hdiv.getEffdate());
		    			}
		    			hdvdintrec.intTo.set(hdsxpf.getHintndt());
		    			hdvdintrec.capAmount.set(hdiv.getHdvamt());
		    			hdvdintrec.intDuration.set(ZERO);
		    			hdvdintrec.intAmount.set(ZERO);
		    			hdvdintrec.intRate.set(ZERO);
		    			hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		    			/*    Call Interest Allocation Subroutine*/
		    			callProgram(wsaaTh501ArrayInner.wsaaTh501Calcprog[ix.toInt()], hdvdintrec.divdIntRec);
		    			if (isNE(hdvdintrec.statuz,varcom.oK)) {
		    				syserrrec.params.set(hdvdintrec.divdIntRec);
		    				syserrrec.statuz.set(hdvdintrec.statuz);
		    				fatalError600();
		    			}
		    			if (isNE(hdvdintrec.intAmount, 0)) {
		    				zrdecplrec.amountIn.set(hdvdintrec.intAmount);
		    				callRounding8000();
		    				hdvdintrec.intAmount.set(zrdecplrec.amountOut);
		    			}
		    			wsaaIntAmount.add(hdvdintrec.intAmount);
		        	}
		        }
		    }	
		  
		 //ILIFE-9080 end
		/*    When the accumulated interest amount is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history).*/
		if (isNE(wsaaIntAmount,0)) {
			Hdivpf hdivpf = new Hdivpf();
 //ILIFE-9080 start
			hdivpf.setChdrcoy(hdsxpf.getChdrcoy());
			hdivpf.setChdrnum(hdsxpf.getChdrnum());
			hdivpf.setLife(hdsxpf.getLife());
			hdivpf.setJlife(hdsxpf.getJlife());
			hdivpf.setCoverage(hdsxpf.getCoverage());
			hdivpf.setRider(hdsxpf.getRider());
			hdivpf.setPlnsfx(hdsxpf.getPlnsfx());
			hdivpf.setTranno(wsaaNewTranno.toInt());
			/*     MOVE HDIS-NEXT-INT-DATE      TO HDIV-EFFDATE*/
			hdivpf.setEffdate(hdsxpf.getHdvbalc());
			hdivpf.setHdvaldt(bsscIO.getEffectiveDate().toInt());
			hdivpf.setHincapdt(hdispf.getHcapndt());
			hdivpf.setCntcurr(chdrpf.getCntcurr());
			hdivpf.setHdvamt(wsaaIntAmount.getbigdata());
			hdivpf.setHdvrate(new BigDecimal(hdvdintrec.intRate.toInt()));
			hdivpf.setHdveffdt(hdvdintrec.rateDate.toInt());
			hdivpf.setBatccoy(batcdorrec.company.toString());
			hdivpf.setBatcbrn(batcdorrec.branch.toString());
			hdivpf.setBatcactyr(batcdorrec.actyear.toInt());
			hdivpf.setBatcactmn(batcdorrec.actmonth.toInt());
			hdivpf.setBatctrcde(batcdorrec.trcde.toString());
			hdivpf.setBatcbatch(batcdorrec.batch.toString());
			hdivpf.setHdvtyp("I");
			hdivpf.setZdivopt(hcsdpf.getZdivopt());
			hdivpf.setZcshdivmth(hcsdpf.getZcshdivmth());
			hdivpf.setHdvopttx(0);
			hdivpf.setHdvcaptx(0);
			hdivpf.setHdvsmtno(0);
			hdivpf.setHpuanbr(0);
			if(hdivpfList==null){
				hdivpfList = new ArrayList<>(); //ILIFE-9080
	        }
			hdivpfList.add(hdivpf);
			
		}
		Hdispf hdispfupdt = new Hdispf(hdispf);
		hdispfupdt.setValidflag("2");
		hdisupdateList.add(hdispfupdt);
		//hdispfDAO.updateHdisValidflag(hdispf);
		Hdispf hdispfinst = new Hdispf(hdispf);
		hdispfinst.setValidflag("1");
		hdispfinst.setTranno(wsaaNewTranno.toInt());
		hdispfinst.setHintldt(hdsxpf.getHintndt());
		if (!loopLastTime.isTrue()) {
			if (isNE(wsaaNextIntDate,varcom.vrcmMaxDate)) {
				hdispfinst.setHintndt(wsaaNextIntDate.toInt());
			}
			else {
				datcon4rec.intDate1.set(hdispf.getHintndt());
				wsaaHdisDate.set(hdispf.getHintndt());
				datcon4rec.frequency.set(wsaaTh501ArrayInner.wsaaTh501IntFq[ix.toInt()]);
				datcon4rec.freqFactor.set(1);
				datcon4rec.billmonth.set(hdispf.getHintduem());
				datcon4rec.billday.set(wsaaHdisDd);
//ILIFE-9444, suggested as per ILIFE-9256
				if(hdispf.getHintduem().equals("02")) {
					datcon4rec.billday.set(28);
				}
//ILIFE-9444 end
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					fatalError600();
				}
				wsaaNextIntDateCal.set(datcon4rec.intDate2);
				if (isNE(wsaaTh501ArrayInner.wsaaTh501IntDd[ix.toInt()], SPACES)) {
					wsaaNextintDay.set(wsaaTh501ArrayInner.wsaaTh501IntDd[ix.toInt()]);
				}
				if (isNE(wsaaTh501ArrayInner.wsaaTh501IntMm[ix.toInt()], SPACES)) {
					if (isGT(wsaaTh501ArrayInner.wsaaTh501IntMm[ix.toInt()], wsaaNextintMth)) {
						wsaaNextintYear.subtract(1);
					}
					wsaaNextintMth.set(wsaaTh501ArrayInner.wsaaTh501IntMm[ix.toInt()]);
				}
				datcon1rec.intDate.set(wsaaNextIntDateCal);
				datcon1rec.function.set("CONV");
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				if (isNE(datcon1rec.statuz,varcom.oK)) {
					wsaaNextintDay.set(wsaaLastdd[wsaaNextintMth.toInt()]);
					if (isEQ(wsaaNextintMth,2)
					&& (setPrecision(wsaaNextintYear, 0)
					&& isEQ((mult((div(wsaaNextintYear,4)),4)),wsaaNextintYear))
					&& isNE(wsaaNextintYear,1900)) {
						wsaaNextintDay.add(1);
					}
				}
				//ILIFE-9080, modified condition as ILIFE-9256, due to conflict condition got misplaced.
				if (isGT(wsaaNextIntDateCal,hdsxpf.getRcesdte())) {
					hdispfinst.setHintndt(hdsxpf.getRcesdte());
				}
				//ILIFE-9080 end
				else {
					hdispfinst.setHintndt(wsaaNextIntDateCal.toInt());
				}
			}
		}
		setPrecision(hdispf.getHintos(), 2);
		hdispfinst.setHintos(add(hdispf.getHintos(),wsaaIntAmount.getbigdata()).getbigdata());
		if(hdisList==null){
			   hdisList = new ArrayList<>(); //ILIFE-9080
	        }
//ILIFE-9444 start
		//Hdispf hdispftemp = new Hdispf(hdispf);
		hdisList.add(hdispfinst);
//ILIFE-9444 end		
		if (isNE(wsaaIntAmount,0)) {
			writeAccounting3200();
		}

		Chdrpf pf = new Chdrpf(chdrpf);
		pf.setUniqueNumber(chdrpf.getUniqueNumber());
		pf.setValidflag("2".charAt(0));
		pf.setCurrto(bsscIO.getEffectiveDate().toInt());
		if(chdrpfListUpdt==null){
			chdrpfListUpdt = new ArrayList<>();
		}
		this.chdrpfListUpdt.add(pf);
		Chdrpf chdrpfinst = new Chdrpf(chdrpf);
		chdrpfinst.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrpfinst.setValidflag("1".charAt(0));
		chdrpfinst.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		chdrpfinst.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpfinst.setTranno(wsaaNewTranno.toInt());
		if(chdrpfListInst==null){
			chdrpfListInst = new ArrayList<>();
		}
		this.chdrpfListInst.add(chdrpfinst);

		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setChdrcoy(hdsxpf.getChdrcoy());
		ptrnpf.setChdrnum(hdsxpf.getChdrnum());
		ptrnpf.setTranno(wsaaNewTranno.toInt());
		ptrnpf.setValidflag("1");
		ptrnpf.setPtrneff(hdsxpf.getHintndt());
		ptrnpf.setTermid(" ");
		ptrnpf.setTrdt(wsaaSysDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTimen.toInt());
		ptrnpf.setUserT(999999);
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		
		if(ptrnpfList == null){
			ptrnpfList = new ArrayList<>();
		}
		ptrnpfList.add(ptrnpf);
		
		if (loopLastTime.isTrue()) {
			return ;
		}
		else {
			if (isEQ(hdispfinst.getHintndt(),hdsxpf.getRcesdte())) {
				wsaaLastTime.set("Y");
			}
		}
		if (isLTE(hdispfinst.getHintndt(),bsscIO.getEffectiveDate())) {
			hdsxpf.setHintndt(hdispfinst.getHintndt());
			goTo(GotoLabel.loop3020);
		}
 //ILIFE-9080 end
	}

protected void writeAccounting3200()
	{
		accounting3210();
	}

protected void accounting3210()
	{
		/*    Determine contract or component level accounting in T5688*/
		for (ix.set(1); !(isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)
		|| (isEQ(wsaaT5688Cnttype[ix.toInt()],chdrpf.getCnttype())
		&& isLTE(wsaaT5688Itmfrm[ix.toInt()],chdrpf.getOccdate()))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)) {
			//itdmIO.setFunction(varcom.begn);
			//itdmIO.setStatuz(varcom.endp);
			itdmpf.setItemcoy(bsprIO.getCompany().toString());
			itdmpf.setItemtabl(t5688);
			itdmpf.setItemitem(chdrpf.getCnttype());
			itdmpf.setItmfrm(chdrpf.getOccdate());
			//syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		wsaaComponLevelAccounted.set(wsaaT5688Comlvlacc[ix.toInt()]);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(wsaaIntAmount);
		wsaaJrnseq.add(1);
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(3);
			lifacmvrec.substituteCode[6].set(hdsxpf.getCrtable()); //ILIFE-9080
			x1000CallLifacmv();
		}
		else {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(SPACES);
			x1000CallLifacmv();
		}
		wsaaJrnseq.add(1);
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(4);
			lifacmvrec.substituteCode[6].set(hdsxpf.getCrtable()); //ILIFE-9080
			x1000CallLifacmv();
		}
		else {
			t5645Ix.set(2);
			lifacmvrec.substituteCode[6].set(SPACES);
			x1000CallLifacmv();
		}
		ct04Value++; //ILIFE-9080
		ct05Value = ct05Value.add(wsaaIntAmount.getbigdata()); //ILIFE-9080
	}

protected void commit3500()
	{
		/*COMMIT*/
 //ILIFE-9080 start
	commitControlTotals();
		
	if (chdrpfListInst != null && !chdrpfListInst.isEmpty()) {
		chdrpfDAO.insertChdrValidRecord(chdrpfListInst);
		chdrpfListInst.clear();
	}
	
	if (chdrpfListUpdt != null && !chdrpfListUpdt.isEmpty()) {
		chdrpfDAO.updateChdrRecordByTranno(chdrpfListUpdt);
		chdrpfListUpdt.clear();
	}
		
	if (ptrnpfList != null && !ptrnpfList.isEmpty()) {
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
	}
	
	if (hdivpfList != null && !hdivpfList.isEmpty()) {
	   	hdivpfDAO.insertHdivpf(hdivpfList);
	   	hdivpfList.clear();
	}
	 
	if( hdisList != null && !hdisList.isEmpty()) {
	   	hdispfDAO.insertHdisRecord(hdisList); //ILIFE-9444
	   	hdisList.clear();
	 }
	 //ILIFE-9080 end	
	
	if( hdisupdateList != null && !hdisupdateList.isEmpty()) {
    	hdispfDAO.updateHdisRecord(hdisupdateList);
    	hdisupdateList.clear();
	 }
		/*EXIT*/
	}
 //ILIFE-9080 start
private void commitControlTotals(){ 
	
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Value);
	callContot001();
	
	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Value);
	callContot001();
	
	contotrec.totno.set(ct03);
	contotrec.totval.set(ct03Value);
	callContot001();
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Value);
	callContot001();
	
	contotrec.totno.set(ct05);
	contotrec.totval.set(ct05Value);
	callContot001();
	
}
 //ILIFE-9080 end
protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
 //ILIFE-9080 start
		if (chdrpfMap != null) {
			chdrpfMap.clear();
		}
		if (hcsdListMap != null) {
			hcsdListMap.clear();
		}
		
		if (hdivcshListMap != null) {
			hdivcshListMap.clear();
		}
		
		if (hdisListMap != null) {
			hdisListMap.clear();
		}
		
		hdisList.clear();
		hdisList=null;
		
		hdivpfList.clear();
		hdivpfList=null;
		
		chdrpfListInst.clear();
		chdrpfListInst=null;
		
		chdrpfListUpdt.clear();
		chdrpfListUpdt= null;
		
		ptrnpfList.clear();
		ptrnpfList=null;
	
 //ILIFE-9080 end
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}


protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(hdvdintrec.cntcurr);
		zrdecplrec.batctrcde.set(hdvdintrec.transcd);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void x1000CallLifacmv()
	{
			x1010Call();
		}

protected void x1010Call()
	{
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(hdsxpf.getPlnsfx()); //ILIFE-9080
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hdsxpf.getChdrnum()); //ILIFE-9080
			stringVariable1.addExpression(hdsxpf.getLife()); //ILIFE-9080
			stringVariable1.addExpression(hdsxpf.getCoverage()); //ILIFE-9080
			stringVariable1.addExpression(hdsxpf.getRider()); //ILIFE-9080
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
		}
		else {
			lifacmvrec.rldgacct.set(hdsxpf.getChdrnum()); //ILIFE-9080
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		lifacmvrec.rdocnum.set(hdsxpf.getChdrnum()); //ILIFE-9080
		lifacmvrec.tranref.set(hdsxpf.getChdrnum()); //ILIFE-9080
		lifacmvrec.tranno.set(wsaaNewTranno);
		lifacmvrec.effdate.set(hdsxpf.getHintndt()); //ILIFE-9080
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure WSAA-TH501-ARRAY--INNER
 */
private static final class WsaaTh501ArrayInner {

	private FixedLengthStringData wsaaTh501Array = new FixedLengthStringData(31000);
	private FixedLengthStringData[] wsaaTh501Rec = FLSArrayPartOfStructure(1000, 31, wsaaTh501Array, 0);
	private FixedLengthStringData[] wsaaTh501Key = FLSDArrayPartOfArrayStructure(9, wsaaTh501Rec, 0);
	private FixedLengthStringData[] wsaaTh501Divdmth = FLSDArrayPartOfArrayStructure(4, wsaaTh501Key, 0, SPACES);
	private PackedDecimalData[] wsaaTh501Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaTh501Key, 4);
	private FixedLengthStringData[] wsaaTh501Data = FLSDArrayPartOfArrayStructure(22, wsaaTh501Rec, 9);
	private FixedLengthStringData[] wsaaTh501CapFq = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 0);
	private FixedLengthStringData[] wsaaTh501IntFq = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 2);
	private FixedLengthStringData[] wsaaTh501CapDd = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 4);
	private FixedLengthStringData[] wsaaTh501IntDd = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 6);
	private FixedLengthStringData[] wsaaTh501CapMm = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 8);
	private FixedLengthStringData[] wsaaTh501IntMm = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 10);
	private FixedLengthStringData[] wsaaTh501Calcprog = FLSDArrayPartOfArrayStructure(10, wsaaTh501Data, 12);
}
}
