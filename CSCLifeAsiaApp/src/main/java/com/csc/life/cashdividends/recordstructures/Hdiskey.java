package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:30
 * Description:
 * Copybook name: HDISKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdiskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdisFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdisKey = new FixedLengthStringData(64).isAPartOf(hdisFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdisChdrcoy = new FixedLengthStringData(1).isAPartOf(hdisKey, 0);
  	public FixedLengthStringData hdisChdrnum = new FixedLengthStringData(8).isAPartOf(hdisKey, 1);
  	public FixedLengthStringData hdisLife = new FixedLengthStringData(2).isAPartOf(hdisKey, 9);
  	public FixedLengthStringData hdisCoverage = new FixedLengthStringData(2).isAPartOf(hdisKey, 11);
  	public FixedLengthStringData hdisRider = new FixedLengthStringData(2).isAPartOf(hdisKey, 13);
  	public PackedDecimalData hdisPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hdisKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(hdisKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdisFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdisFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}