/******************************************************************************
 * File Name 		: HcsdpfDAO.java
 * Author			: smalchi2
 * Creation Date	: 02 January 2017
 * Project			: Integral Life
 * Description		: The DAO Interface for HCSDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface HcsdpfDAO extends BaseDAO<Hcsdpf> {

	public List<Hcsdpf> searchHcsdpfRecord(Hcsdpf hcsdpf) throws SQLRuntimeException;
	public Map<String, Hcsdpf> searchHcsdRecord(String coy, List<String> chdrnumList);
	public Map<String, List<Hcsdpf>> getHcsdMap(String coy, List<String> chdrnumList);
    public Hcsdpf getHcsdRecordByCoyAndNum(String chdrcoy, String chdrnum);
    public List<Hcsdpf> searchHcsdRecord(Hcsdpf Hcsd);
	//public void insertIntoHcsdpf(Hcsdpf hcsdpf) throws SQLRuntimeException;

//	public void deleteHcsdpf(Hcsdpf hcsdpf) throws SQLRuntimeException;

}
