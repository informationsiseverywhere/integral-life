package com.csc.life.cashdividends.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdivrevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:40
 * Class transformed from HDIVREV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdivrevTableDAM extends HdivpfTableDAM {

	public HdivrevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HDIVREV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "HPUANBR, " +
		            "TRANNO, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "CNTCURR, " +
		            "EFFDATE, " +
		            "HDVALDT, " +
		            "HDVTYP, " +
		            "HDVAMT, " +
		            "HDVRATE, " +
		            "HDVEFFDT, " +
		            "ZDIVOPT, " +
		            "ZCSHDIVMTH, " +
		            "HDVOPTTX, " +
		            "HDVCAPTX, " +
		            "HINCAPDT, " +
		            "HDVSMTNO, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               coverage,
                               rider,
                               planSuffix,
                               puAddNbr,
                               tranno,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               cntcurr,
                               effdate,
                               divdAllocDate,
                               divdType,
                               divdAmount,
                               divdRate,
                               divdRtEffdt,
                               zdivopt,
                               zcshdivmth,
                               divdOptprocTranno,
                               divdCapTranno,
                               divdIntCapDate,
                               divdStmtNo,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(52);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller90.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(143);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getPuAddNbr().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getCntcurr().toInternal()
					+ getEffdate().toInternal()
					+ getDivdAllocDate().toInternal()
					+ getDivdType().toInternal()
					+ getDivdAmount().toInternal()
					+ getDivdRate().toInternal()
					+ getDivdRtEffdt().toInternal()
					+ getZdivopt().toInternal()
					+ getZcshdivmth().toInternal()
					+ getDivdOptprocTranno().toInternal()
					+ getDivdCapTranno().toInternal()
					+ getDivdIntCapDate().toInternal()
					+ getDivdStmtNo().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, puAddNbr);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, divdAllocDate);
			what = ExternalData.chop(what, divdType);
			what = ExternalData.chop(what, divdAmount);
			what = ExternalData.chop(what, divdRate);
			what = ExternalData.chop(what, divdRtEffdt);
			what = ExternalData.chop(what, zdivopt);
			what = ExternalData.chop(what, zcshdivmth);
			what = ExternalData.chop(what, divdOptprocTranno);
			what = ExternalData.chop(what, divdCapTranno);
			what = ExternalData.chop(what, divdIntCapDate);
			what = ExternalData.chop(what, divdStmtNo);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public PackedDecimalData getPuAddNbr() {
		return puAddNbr;
	}
	public void setPuAddNbr(Object what) {
		setPuAddNbr(what, false);
	}
	public void setPuAddNbr(Object what, boolean rounded) {
		if (rounded)
			puAddNbr.setRounded(what);
		else
			puAddNbr.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getDivdAllocDate() {
		return divdAllocDate;
	}
	public void setDivdAllocDate(Object what) {
		setDivdAllocDate(what, false);
	}
	public void setDivdAllocDate(Object what, boolean rounded) {
		if (rounded)
			divdAllocDate.setRounded(what);
		else
			divdAllocDate.set(what);
	}	
	public FixedLengthStringData getDivdType() {
		return divdType;
	}
	public void setDivdType(Object what) {
		divdType.set(what);
	}	
	public PackedDecimalData getDivdAmount() {
		return divdAmount;
	}
	public void setDivdAmount(Object what) {
		setDivdAmount(what, false);
	}
	public void setDivdAmount(Object what, boolean rounded) {
		if (rounded)
			divdAmount.setRounded(what);
		else
			divdAmount.set(what);
	}	
	public PackedDecimalData getDivdRate() {
		return divdRate;
	}
	public void setDivdRate(Object what) {
		setDivdRate(what, false);
	}
	public void setDivdRate(Object what, boolean rounded) {
		if (rounded)
			divdRate.setRounded(what);
		else
			divdRate.set(what);
	}	
	public PackedDecimalData getDivdRtEffdt() {
		return divdRtEffdt;
	}
	public void setDivdRtEffdt(Object what) {
		setDivdRtEffdt(what, false);
	}
	public void setDivdRtEffdt(Object what, boolean rounded) {
		if (rounded)
			divdRtEffdt.setRounded(what);
		else
			divdRtEffdt.set(what);
	}	
	public FixedLengthStringData getZdivopt() {
		return zdivopt;
	}
	public void setZdivopt(Object what) {
		zdivopt.set(what);
	}	
	public FixedLengthStringData getZcshdivmth() {
		return zcshdivmth;
	}
	public void setZcshdivmth(Object what) {
		zcshdivmth.set(what);
	}	
	public PackedDecimalData getDivdOptprocTranno() {
		return divdOptprocTranno;
	}
	public void setDivdOptprocTranno(Object what) {
		setDivdOptprocTranno(what, false);
	}
	public void setDivdOptprocTranno(Object what, boolean rounded) {
		if (rounded)
			divdOptprocTranno.setRounded(what);
		else
			divdOptprocTranno.set(what);
	}	
	public PackedDecimalData getDivdCapTranno() {
		return divdCapTranno;
	}
	public void setDivdCapTranno(Object what) {
		setDivdCapTranno(what, false);
	}
	public void setDivdCapTranno(Object what, boolean rounded) {
		if (rounded)
			divdCapTranno.setRounded(what);
		else
			divdCapTranno.set(what);
	}	
	public PackedDecimalData getDivdIntCapDate() {
		return divdIntCapDate;
	}
	public void setDivdIntCapDate(Object what) {
		setDivdIntCapDate(what, false);
	}
	public void setDivdIntCapDate(Object what, boolean rounded) {
		if (rounded)
			divdIntCapDate.setRounded(what);
		else
			divdIntCapDate.set(what);
	}	
	public PackedDecimalData getDivdStmtNo() {
		return divdStmtNo;
	}
	public void setDivdStmtNo(Object what) {
		setDivdStmtNo(what, false);
	}
	public void setDivdStmtNo(Object what, boolean rounded) {
		if (rounded)
			divdStmtNo.setRounded(what);
		else
			divdStmtNo.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		life.clear();
		jlife.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		puAddNbr.clear();
		nonKeyFiller90.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		cntcurr.clear();
		effdate.clear();
		divdAllocDate.clear();
		divdType.clear();
		divdAmount.clear();
		divdRate.clear();
		divdRtEffdt.clear();
		zdivopt.clear();
		zcshdivmth.clear();
		divdOptprocTranno.clear();
		divdCapTranno.clear();
		divdIntCapDate.clear();
		divdStmtNo.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}