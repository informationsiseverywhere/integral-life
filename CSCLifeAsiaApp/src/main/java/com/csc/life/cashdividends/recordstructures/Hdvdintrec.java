package com.csc.life.cashdividends.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:34
 * Description:
 * Copybook name: HDVDINTREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdvdintrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData divdIntRec = new FixedLengthStringData(87);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(divdIntRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(divdIntRec, 5);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(divdIntRec, 9);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(divdIntRec, 10);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(divdIntRec, 18);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(divdIntRec, 20);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(divdIntRec, 22);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(divdIntRec, 24);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(divdIntRec, 27);
  	public FixedLengthStringData transcd = new FixedLengthStringData(4).isAPartOf(divdIntRec, 30);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(divdIntRec, 34);
  	public PackedDecimalData effectiveDate = new PackedDecimalData(8, 0).isAPartOf(divdIntRec, 38);
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(divdIntRec, 43).setUnsigned();
  	public PackedDecimalData capAmount = new PackedDecimalData(13, 2).isAPartOf(divdIntRec, 48);
  	public PackedDecimalData intFrom = new PackedDecimalData(8, 0).isAPartOf(divdIntRec, 55);
  	public PackedDecimalData intTo = new PackedDecimalData(8, 0).isAPartOf(divdIntRec, 60);
  	public PackedDecimalData intDuration = new PackedDecimalData(11, 5).isAPartOf(divdIntRec, 65);
  	public PackedDecimalData intAmount = new PackedDecimalData(11, 2).isAPartOf(divdIntRec, 71);
  	public PackedDecimalData intRate = new PackedDecimalData(8, 5).isAPartOf(divdIntRec, 77);
  	public PackedDecimalData rateDate = new PackedDecimalData(8, 0).isAPartOf(divdIntRec, 82);


	public void initialize() {
		COBOLFunctions.initialize(divdIntRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		divdIntRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}