package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:33
 * Description:
 * Copybook name: HDIVROPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivropkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivropFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivropKey = new FixedLengthStringData(64).isAPartOf(hdivropFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivropChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivropKey, 0);
  	public FixedLengthStringData hdivropChdrnum = new FixedLengthStringData(8).isAPartOf(hdivropKey, 1);
  	public PackedDecimalData hdivropDivdOptprocTranno = new PackedDecimalData(5, 0).isAPartOf(hdivropKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(hdivropKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivropFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivropFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}