/*
 * File: Th527pt.java
 * Date: 30 August 2009 2:34:19
 * Author: Quipoz Limited
 * 
 * Class transformed from TH527PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH527.
*
*
*****************************************************************
* </pre>
*/
public class Th527pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(42).isAPartOf(wsaaPrtLine001, 34, FILLER).init("Cash Dividend                        SH527");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(71);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);
	private FixedLengthStringData filler10 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 45, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 50, FILLER).init("Risk Unit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(9, 0).isAPartOf(wsaaPrtLine003, 62).setPattern("ZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler12 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine004, 0, FILLER).init("Yr Inforce  1      2      3      4      5      6      7 8      9     10");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler13 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init("   0+");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(79);
	private FixedLengthStringData filler23 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  10+");
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(79);
	private FixedLengthStringData filler33 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  20+");
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(79);
	private FixedLengthStringData filler43 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  30+");
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(79);
	private FixedLengthStringData filler53 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  40+");
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(79);
	private FixedLengthStringData filler63 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  50+");
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(79);
	private FixedLengthStringData filler73 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  60+");
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(79);
	private FixedLengthStringData filler83 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 0, FILLER).init("  70+");
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(79);
	private FixedLengthStringData filler93 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 0, FILLER).init("  80+");
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(79);
	private FixedLengthStringData filler103 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 0, FILLER).init("  90+");
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(43);
	private FixedLengthStringData filler113 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler114 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 29, FILLER).init("Prem Unit:");
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 40).setPattern("ZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th527rec th527rec = new Th527rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th527pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th527rec.th527Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(th527rec.unit);
		fieldNo008.set(th527rec.insprm01);
		fieldNo009.set(th527rec.insprm02);
		fieldNo010.set(th527rec.insprm03);
		fieldNo011.set(th527rec.insprm04);
		fieldNo012.set(th527rec.insprm05);
		fieldNo013.set(th527rec.insprm06);
		fieldNo014.set(th527rec.insprm07);
		fieldNo015.set(th527rec.insprm08);
		fieldNo016.set(th527rec.insprm09);
		fieldNo017.set(th527rec.insprm10);
		fieldNo018.set(th527rec.insprm11);
		fieldNo019.set(th527rec.insprm12);
		fieldNo020.set(th527rec.insprm13);
		fieldNo021.set(th527rec.insprm14);
		fieldNo022.set(th527rec.insprm15);
		fieldNo023.set(th527rec.insprm16);
		fieldNo024.set(th527rec.insprm17);
		fieldNo025.set(th527rec.insprm18);
		fieldNo026.set(th527rec.insprm19);
		fieldNo027.set(th527rec.insprm20);
		fieldNo028.set(th527rec.insprm21);
		fieldNo029.set(th527rec.insprm22);
		fieldNo030.set(th527rec.insprm23);
		fieldNo031.set(th527rec.insprm24);
		fieldNo032.set(th527rec.insprm25);
		fieldNo033.set(th527rec.insprm26);
		fieldNo034.set(th527rec.insprm27);
		fieldNo035.set(th527rec.insprm28);
		fieldNo036.set(th527rec.insprm29);
		fieldNo037.set(th527rec.insprm30);
		fieldNo038.set(th527rec.insprm31);
		fieldNo039.set(th527rec.insprm32);
		fieldNo040.set(th527rec.insprm33);
		fieldNo041.set(th527rec.insprm34);
		fieldNo042.set(th527rec.insprm35);
		fieldNo043.set(th527rec.insprm36);
		fieldNo044.set(th527rec.insprm37);
		fieldNo045.set(th527rec.insprm38);
		fieldNo046.set(th527rec.insprm39);
		fieldNo047.set(th527rec.insprm40);
		fieldNo048.set(th527rec.insprm41);
		fieldNo049.set(th527rec.insprm42);
		fieldNo050.set(th527rec.insprm43);
		fieldNo051.set(th527rec.insprm44);
		fieldNo052.set(th527rec.insprm45);
		fieldNo053.set(th527rec.insprm46);
		fieldNo054.set(th527rec.insprm47);
		fieldNo055.set(th527rec.insprm48);
		fieldNo056.set(th527rec.insprm49);
		fieldNo057.set(th527rec.insprm50);
		fieldNo058.set(th527rec.insprm51);
		fieldNo059.set(th527rec.insprm52);
		fieldNo060.set(th527rec.insprm53);
		fieldNo061.set(th527rec.insprm54);
		fieldNo062.set(th527rec.insprm55);
		fieldNo063.set(th527rec.insprm56);
		fieldNo064.set(th527rec.insprm57);
		fieldNo065.set(th527rec.insprm58);
		fieldNo066.set(th527rec.insprm59);
		fieldNo067.set(th527rec.insprm60);
		fieldNo068.set(th527rec.insprm61);
		fieldNo069.set(th527rec.insprm62);
		fieldNo070.set(th527rec.insprm63);
		fieldNo071.set(th527rec.insprm64);
		fieldNo072.set(th527rec.insprm65);
		fieldNo073.set(th527rec.insprm66);
		fieldNo074.set(th527rec.insprm67);
		fieldNo075.set(th527rec.insprm68);
		fieldNo076.set(th527rec.insprm69);
		fieldNo077.set(th527rec.insprm70);
		fieldNo078.set(th527rec.insprm71);
		fieldNo079.set(th527rec.insprm72);
		fieldNo080.set(th527rec.insprm73);
		fieldNo081.set(th527rec.insprm74);
		fieldNo082.set(th527rec.insprm75);
		fieldNo083.set(th527rec.insprm76);
		fieldNo084.set(th527rec.insprm77);
		fieldNo085.set(th527rec.insprm78);
		fieldNo086.set(th527rec.insprm79);
		fieldNo087.set(th527rec.insprm80);
		fieldNo088.set(th527rec.insprm81);
		fieldNo089.set(th527rec.insprm82);
		fieldNo090.set(th527rec.insprm83);
		fieldNo091.set(th527rec.insprm84);
		fieldNo092.set(th527rec.insprm85);
		fieldNo093.set(th527rec.insprm86);
		fieldNo094.set(th527rec.insprm87);
		fieldNo095.set(th527rec.insprm88);
		fieldNo096.set(th527rec.insprm89);
		fieldNo097.set(th527rec.insprm90);
		fieldNo098.set(th527rec.insprm91);
		fieldNo099.set(th527rec.insprm92);
		fieldNo100.set(th527rec.insprm93);
		fieldNo101.set(th527rec.insprm94);
		fieldNo102.set(th527rec.insprm95);
		fieldNo103.set(th527rec.insprm96);
		fieldNo104.set(th527rec.insprm97);
		fieldNo105.set(th527rec.insprm98);
		fieldNo106.set(th527rec.insprm99);
		fieldNo107.set(th527rec.instpr);
		fieldNo108.set(th527rec.premUnit);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
