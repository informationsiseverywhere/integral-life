package com.csc.life.cashdividends.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:55
 * Description:
 * Copybook name: TH529REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th529rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th529Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData intRate = new ZonedDecimalData(8, 5).isAPartOf(th529Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(492).isAPartOf(th529Rec, 8, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th529Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th529Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}