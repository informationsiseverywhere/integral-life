/*
 * File: Hdopcswd.java
 * Date: 29 August 2009 22:53:11
 * Author: Quipoz Limited
 *
 * Class transformed from HDOPCSWD.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.anticipatedendowment.procedures.Zrgetusr;
import com.csc.life.anticipatedendowment.recordstructures.Zrgtusrrec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdvdoprec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              Cash Dividend Withdrawal
*              ------------------------
*
* This subroutine is called from BH526 via TH500.
*
* This routine is responsible for withdrawing the cash dividend
* allocated for the current period and create a payment
* requisition based on payee details held on HCSDPF.  Once the
* transaction has successfully put throughm the following
* postings will be created.
*
*       Dividend = 100   Contract Currency=USD
*                        Payment  Currency=HKD
*       Contract level accounting    USD 1 = HKD 7.72
*
*       HKD ledger:
*                                        CR   DR
*       Currency Exchange                    772    line 7
*       Payment Suspense                772         line 3
*
*       Payment Suspense  (via CHQPRN)  772         line 5
*       Bank              (via CHQPRN)  772         T3698
*
*       USD ledger:
*                                        CR   DR
*       Dividend Payable                     100    line 1
*       Currency Exchange               100         line 8
*
*  PROGRAM LOGIC
*  -------------
*
* 1. Read HCSD.
* 2. Create payment requisition and its accounting entries
*    If payment currency not = contract currency
*        call XCVRT to convert dividend amount into payment curr
*        then write ACMV for currency conversion
* 3. Utilise copybook SUBCODE to perform GL substitution based on
*    accounting method specified on T5688 and accounting rules on
*    T5645(line 5 or 6)
* 4. Read T3629 with HCSD-PAYCURR as item key
* 5. Create payment requisition via PAYREQ
* 6. Write a HDIV to denote the withdrawal has been taken place
* 7. Update HDIS summary by invalid the current HDIS and write
*    a new one
* 8. Write accounting records for the transer of dividend to
*    payment suspense.
* 9. set statuz to O-K and return.
*
*****************************************************************
*
* </pre>
*/
public class Hdopcswd extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "HDOPCSWD";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String g641 = "G641";
	private static final String hl63 = "HL63";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String hcsdrec = "HCSDREC";
	private static final String hdivrec = "HDIVREC";
	private static final String hdisrec = "HDISREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t5690 = "T5690";
	private static final String t3629 = "T3629";
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T5690rec t5690rec = new T5690rec();
	private T3629rec t3629rec = new T3629rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Payreqrec payreqrec = new Payreqrec();
	private Zrgtusrrec zrgtusrrec = new Zrgtusrrec();
	private Subcoderec subcoderec = new Subcoderec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Hdvdoprec hdvdoprec = new Hdvdoprec();
	boolean isCashDividend = false;
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private static final String wsaaAutoCheque = "1";
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private Mandpf mandpf;	

	public Hdopcswd() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		hdvdoprec.dividendRec = convertAndSetParam(hdvdoprec.dividendRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		getUserNumber400();
		initialise200();
		postPayment300();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{			
		isCashDividend = FeaConfg.isFeatureExist("2", "SUOTR007", appVars, "IT");
		chdrpf = chdrpfDAO.getChdrpf(hdvdoprec.chdrChdrcoy.toString(), hdvdoprec.chdrChdrnum.toString());
		
		syserrrec.subrname.set(wsaaSubr);
		hdvdoprec.statuz.set(varcom.oK);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		/*    Read T5645*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.batccoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError800();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*    Read T5688*/
		itdmIO.setItemcoy(hdvdoprec.batccoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(hdvdoprec.cnttype);
		itdmIO.setItmfrm(hdvdoprec.effectiveDate);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		if (isNE(itdmIO.getItemcoy(), hdvdoprec.batccoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),hdvdoprec.cnttype)) {
			itdmIO.setStatuz(e308);
			itdmIO.setItemitem(hdvdoprec.cnttype);
			itdmIO.setItmfrm(hdvdoprec.effectiveDate);
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaComponLevelAccounted.set(t5688rec.comlvlacc);
		/*    Read HCSD*/
		hcsdIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hcsdIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hcsdIO.setLife(hdvdoprec.lifeLife);
		hcsdIO.setCoverage(hdvdoprec.covrCoverage);
		hcsdIO.setRider(hdvdoprec.covrRider);
		hcsdIO.setPlanSuffix(hdvdoprec.plnsfx);
		hcsdIO.setFunction(varcom.readr);
		hcsdIO.setFormat(hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			databaseError800();
		}
		/*    Fields which require initialisation only once.*/
		wsaaJrnseq.set(ZERO);
		lifacmvrec.batccoy.set(hdvdoprec.batccoy);
		lifacmvrec.batcbrn.set(hdvdoprec.batcbrn);
		lifacmvrec.batcactyr.set(hdvdoprec.batcactyr);
		lifacmvrec.batcactmn.set(hdvdoprec.batcactmn);
		lifacmvrec.batctrcde.set(hdvdoprec.batctrcde);
		lifacmvrec.batcbatch.set(hdvdoprec.batcbatch);
		lifacmvrec.rldgcoy.set(hdvdoprec.batccoy);
		lifacmvrec.genlcoy.set(hdvdoprec.batccoy);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.rdocnum.set(hdvdoprec.chdrChdrnum);
		lifacmvrec.tranref.set(hdvdoprec.chdrChdrnum);
		lifacmvrec.tranno.set(hdvdoprec.newTranno);
		lifacmvrec.effdate.set(hdvdoprec.effectiveDate);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(hdvdoprec.effectiveDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.substituteCode[1].set(hdvdoprec.cnttype);
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.substituteCode[6].set(hdvdoprec.crtable);
		}
		else {
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.chdrChdrcoy);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(hdvdoprec.batctrcde);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(hdvdoprec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
	}

protected void postPayment300()
	{
		post310();
	}

protected void post310()
	{
		/*  Read T3629 with HDOP-CNTTYPE for product dependent bankcode*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.chdrChdrcoy);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(hcsdIO.getPaycurr());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setStatuz(g641);
			syserrrec.params.set(itemIO.getParams());
			databaseError800();
		}
		else {
			t3629rec.t3629Rec.set(itemIO.getGenarea());
			if (isEQ(t3629rec.bankcode,SPACES)) {
				itemIO.setStatuz(hl63);
				syserrrec.params.set(itemIO.getParams());
				databaseError800();
			}
		}
		/*  Post for currency conversion if contract currency not =*/
		/*  payment currency*/
		if (isNE(hdvdoprec.cntcurr,hcsdIO.getPaycurr())) {
			conlinkrec.amountIn.set(hdvdoprec.divdAmount);
			conlinkrec.currIn.set(hdvdoprec.cntcurr);
			conlinkrec.currOut.set(hcsdIO.getPaycurr());
			conlinkrec.cashdate.set(hdvdoprec.effectiveDate);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(hdvdoprec.batccoy);
			conlinkrec.function.set("CVRT");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(conlinkrec.clnk002Rec);
				syserrrec.statuz.set(conlinkrec.statuz);
				systemError900();
			}
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(conlinkrec.amountIn);
			lifacmvrec.origcurr.set(hdvdoprec.cntcurr);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.contot.set(t5645rec.cnttot07);
			lifacmvrec.function.set("PSTW");
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				systemError900();
			}
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(conlinkrec.amountOut);
			lifacmvrec.origcurr.set(hcsdIO.getPaycurr());
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
			lifacmvrec.function.set("PSTW");
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				systemError900();
			}
		}
		/*  Write a HDIV to denote the withdraw has been taken place*/
		hdivIO.setDataArea(SPACES);
		hdivIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hdivIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hdivIO.setLife(hdvdoprec.lifeLife);
		hdivIO.setCoverage(hdvdoprec.covrCoverage);
		hdivIO.setRider(hdvdoprec.covrRider);
		hdivIO.setPlanSuffix(hdvdoprec.plnsfx);
		hdivIO.setJlife(hdvdoprec.lifeJlife);
		hdivIO.setTranno(hdvdoprec.newTranno);
		hdivIO.setEffdate(hdvdoprec.hdivEffdate);
		hdivIO.setDivdAllocDate(hdvdoprec.effectiveDate);
		hdivIO.setDivdIntCapDate(varcom.vrcmMaxDate);
		hdivIO.setCntcurr(hdvdoprec.cntcurr);
		setPrecision(hdivIO.getDivdAmount(), 2);
		hdivIO.setDivdAmount(mult(hdvdoprec.divdAmount,-1));
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
		hdivIO.setBatccoy(hdvdoprec.batccoy);
		hdivIO.setBatcbrn(hdvdoprec.batcbrn);
		hdivIO.setBatcactyr(hdvdoprec.batcactyr);
		hdivIO.setBatcactmn(hdvdoprec.batcactmn);
		hdivIO.setBatctrcde(hdvdoprec.batctrcde);
		hdivIO.setBatcbatch(hdvdoprec.batcbatch);
		hdivIO.setDivdType("W");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setFormat(hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			databaseError800();
		}
		/*  Now update HDIS to denote withdrawal*/
		hdisIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hdisIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hdisIO.setLife(hdvdoprec.lifeLife);
		hdisIO.setCoverage(hdvdoprec.covrCoverage);
		hdisIO.setRider(hdvdoprec.covrRider);
		hdisIO.setPlanSuffix(hdvdoprec.plnsfx);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(hdvdoprec.newTranno);
		setPrecision(hdisIO.getBalSinceLastCap(), 2);
		hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(),(mult(hdvdoprec.divdAmount,-1))));
		hdisIO.setSacscode(SPACES);
		hdisIO.setSacstyp(SPACES);
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		/* Account posting for the transfer of dividend to payment*/
		/* suspense.*/
		/* IF  HCSD-PAYCURR        NOT = HDOP-CNTCURR*/
		/*     MOVE CLNK-AMOUNT-OUT    TO LIFA-ORIGAMT*/
		/* ELSE*/
		/*     MOVE HDOP-DIVD-AMOUNT   TO LIFA-ORIGAMT*/
		/* END-IF.*/
		/* MOVE HCSD-PAYCURR           TO LIFA-ORIGCURR.*/
		wsaaRldgacct.set(SPACES);
		wsaaRldgacct.set(hdvdoprec.chdrChdrnum);
		wsaaSub1.set(1);
		wsaaSub2.set(3);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		lifacmvrec.origamt.set(hdvdoprec.divdAmount);
		lifacmvrec.origcurr.set(hdvdoprec.cntcurr);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError900();
		}
		/* Account posting for payment suspense*/
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub2.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub2.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub2.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub2.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub2.toInt()]);
		if (isNE(hcsdIO.getPaycurr(),hdvdoprec.cntcurr)) {
			lifacmvrec.origamt.set(conlinkrec.amountOut);
		}
		else {
			lifacmvrec.origamt.set(hdvdoprec.divdAmount);
		}
		lifacmvrec.origcurr.set(hcsdIO.getPaycurr());
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError900();
		}
		/* Read ACBL with the subaccount code & type defined in line 03*/
		/* to check if there is any value in the payment suspense and*/
		/* determine to write cheque payments*/
		/* MOVE SPACES                 TO ACBL-DATA-AREA.*/
		/* MOVE HDOP-CHDR-CHDRCOY      TO ACBL-RLDGCOY.*/
		/* MOVE T5645-SACSCODE-03      TO ACBL-SACSCODE.*/
		/* MOVE HDOP-CHDR-CHDRNUM      TO ACBL-RLDGACCT.*/
		/* MOVE HDOP-CNTCURR           TO ACBL-ORIGCURR.*/
		/* MOVE T5645-SACSTYPE-03      TO ACBL-SACSTYP.*/
		/* MOVE ACBLREC                TO ACBL-FORMAT.*/
		/* MOVE READR                  TO ACBL-FUNCTION.*/
		/* CALL 'ACBLIO'  USING ACBL-PARAMS.*/
		/* IF  ACBL-STATUZ             NOT = O-K*/
		/*     MOVE ACBL-PARAMS        TO SYSR-PARAMS*/
		/*     MOVE ACBL-STATUZ        TO SYSR-STATUZ*/
		/*     PERFORM 800-DATABASE-ERROR*/
		/* END-IF.*/
		/*    Read T3695*/
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.*/
		/* MOVE HDOP-BATCCOY           TO ITEM-ITEMCOY.*/
		/* MOVE T3695                  TO ITEM-ITEMTABL.*/
		/* MOVE ACBL-SACSTYP           TO ITEM-ITEMITEM.*/
		/* MOVE SPACES                 TO ITEM-ITEMSEQ.*/
		/* MOVE READR                  TO ITEM-FUNCTION.*/
		/* MOVE ITEMREC                TO ITEM-FORMAT.*/
		/* CALL 'ITEMIO'  USING ITEM-PARAMS.*/
		/* IF  ITEM-STATUZ             NOT = O-K*/
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ*/
		/*     PERFORM 800-DATABASE-ERROR*/
		/* END-IF.*/
		/* MOVE ITEM-GENAREA           TO T3695-T3695-REC.*/
		/* IF  T3695-SIGN              = '-'*/
		/*     COMPUTE ACBL-SACSCURBAL = ACBL-SACSCURBAL * -1*/
		/* END-IF.*/
		/* IF  ACBL-SACSCURBAL         NOT < HDOP-DIVD-AMOUNT*/
		/*     PERFORM 350-CALL-PAYREQ*/
		/* END-IF.*/
		callPayreq350();
	}

	/**
	* <pre>
	* copy in new procedure division copybook *
	* </pre>
	*/
protected void subcode()
	{
		sbcd100Start();
	}

protected void sbcd100Start()
	{
		if (isNE(subcoderec.code[1],SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[1]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[2],SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[2]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[3],SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[3]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[4],SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[4]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[5],SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[5]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[6],SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[6]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
		}
	}

protected void callPayreq350()
	{
		payreq350();
	}

protected void payreq350()
	{
		/*  Prepare to call PAYREQ for requisition create*/
		payreqrec.rec.set(SPACES);
		payreqrec.bankcode.set(t3629rec.bankcode);
		payreqrec.batckey.set(lifacmvrec.batckey);
		/*        Check for Component level accounting & act accordingly*/
		payreqrec.frmRldgacct.set(SPACES);
		payreqrec.frmRldgacct.set(hdvdoprec.chdrChdrnum);
		/* Use new procedure division copybook SUBCODE to perform the*/
		/*  substitution in the GLCODE field before it is passed to*/
		/*  PAYREQ and written to the PREQ file.*/
		subcoderec.codeRec.set(SPACES);
		subcoderec.code[1].set(hdvdoprec.cnttype);
		subcoderec.glcode.set(t5645rec.glmap05);
		subcode();
		payreqrec.glcode.set(subcoderec.glcode);
		payreqrec.sacscode.set(t5645rec.sacscode05);
		payreqrec.sacstype.set(t5645rec.sacstype05);
		payreqrec.sign.set(t5645rec.sign05);
		payreqrec.cnttot.set(t5645rec.cnttot05);
		payreqrec.effdate.set(hdvdoprec.effectiveDate);
		if(isCashDividend){
			payreqrec.paycurr.set(chdrpf.getCntcurr());
		}else{
			payreqrec.paycurr.set(hcsdIO.getPaycurr());
		}
		
		if (isNE(hcsdIO.getPaycurr(),hdvdoprec.cntcurr)) {
			payreqrec.pymt.set(conlinkrec.amountOut);
		}
		else {
			payreqrec.pymt.set(hdvdoprec.divdAmount);
		}
		payreqrec.termid.set(SPACES);
		payreqrec.user.set(zrgtusrrec.usernum);
		/* Read T5690 to determine the payment type for this pay method*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.batccoy);
		itemIO.setItemtabl(t5690);
		itemIO.setItemitem(hcsdIO.getPaymth());
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError800();
		}
		else {
			t5690rec.t5690Rec.set(itemIO.getGenarea());
		}
		if(isCashDividend && chdrpf!=null){
			if(isNE(chdrpf.getZmandref(),SPACES)){
				mandpf = mandpfDAO.searchMandpfRecordData(chdrpf.getCowncoy().toString(),
						chdrpf.getCownnum(), chdrpf.getZmandref(), "MANDPF");/* IJTI-1523 */
				if(mandpf!=null && isNE(chdrpf.getReqntype(),SPACES)){
					payreqrec.bankkey.set(mandpf.getBankkey());
					payreqrec.bankacckey.set(mandpf.getBankacckey());
					payreqrec.reqntype.set(chdrpf.getReqntype());
				}
				else{
					payreqrec.bankkey.set(SPACES);
					payreqrec.bankacckey.set(SPACES);
					payreqrec.reqntype.set(wsaaAutoCheque);
				}
			}
			else{
				payreqrec.bankkey.set(SPACES);
				payreqrec.bankacckey.set(SPACES);
				payreqrec.reqntype.set(wsaaAutoCheque);
			}
			payreqrec.clntcoy.set(chdrpf.getCowncoy());
			payreqrec.clntnum.set(chdrpf.getCownnum());
			
		}else{
			payreqrec.reqntype.set(t5690rec.paymentMethod);
			payreqrec.clntcoy.set(hcsdIO.getPaycoy());
			payreqrec.clntnum.set(hcsdIO.getPayclt());
			payreqrec.bankkey.set(hcsdIO.getBankkey());
			payreqrec.bankacckey.set(hcsdIO.getBankacckey());
		}				
			
		payreqrec.tranref.set(payreqrec.frmRldgacct);
		payreqrec.trandesc.set(lifacmvrec.trandesc);
		payreqrec.language.set(hdvdoprec.language);
		payreqrec.zbatctrcde.set(hdvdoprec.batctrcde); 
		payreqrec.function.set("REQN");
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(payreqrec.rec);
			syserrrec.statuz.set(payreqrec.statuz);
			systemError900();
		}
	}

protected void getUserNumber400()
	{
		/*START*/
		/* Using ZRGETUSR sub-routine, get the User Number which is*/
		/* not available from the new batch programs.*/
		zrgtusrrec.rec.set(SPACES);
		zrgtusrrec.userid.set(hdvdoprec.userName);
		zrgtusrrec.function.set("USID");
		zrgtusrrec.usernum.set(0);
		callProgram(Zrgetusr.class, zrgtusrrec.rec);
		if (isNE(zrgtusrrec.statuz,varcom.oK)) {
			syserrrec.params.set(zrgtusrrec.rec);
			syserrrec.statuz.set(zrgtusrrec.statuz);
			systemError900();
		}
		/*EXIT*/
	}

protected void databaseError800()
	{
					start810();
					exit870();
				}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
					start910();
					exit970();
				}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(hdvdoprec.batccoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(hcsdIO.getPaycurr());
		zrdecplrec.batctrcde.set(hdvdoprec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError900();
		}
		/*A900-EXIT*/
	}
}
