package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:29
 * Description:
 * Copybook name: HCSDMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hcsdmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hcsdmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hcsdmjaKey = new FixedLengthStringData(64).isAPartOf(hcsdmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData hcsdmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(hcsdmjaKey, 0);
  	public FixedLengthStringData hcsdmjaChdrnum = new FixedLengthStringData(8).isAPartOf(hcsdmjaKey, 1);
  	public FixedLengthStringData hcsdmjaLife = new FixedLengthStringData(2).isAPartOf(hcsdmjaKey, 9);
  	public FixedLengthStringData hcsdmjaCoverage = new FixedLengthStringData(2).isAPartOf(hcsdmjaKey, 11);
  	public FixedLengthStringData hcsdmjaRider = new FixedLengthStringData(2).isAPartOf(hcsdmjaKey, 13);
  	public PackedDecimalData hcsdmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hcsdmjaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(hcsdmjaKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hcsdmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hcsdmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}