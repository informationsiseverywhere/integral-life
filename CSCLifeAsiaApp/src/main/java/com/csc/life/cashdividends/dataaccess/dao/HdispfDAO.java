/******************************************************************************
 * File Name 		: HdispfDAO.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The DAO Interface for HDISPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface HdispfDAO extends BaseDAO<Hdispf> {

	public List<Hdispf> searchHdispfRecord(Hdispf hdispf) throws SQLRuntimeException;

	public void insertIntoHdispf(Hdispf hdispf) throws SQLRuntimeException;

	/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction*/
	public List<Hdispf>	readHdispfData(Hdispf hdisModel);	
	public Map<String, Hdispf> searchHdispfRecord(String coy, List<String> chdrnumList);
	public Map<String, List<Hdispf>> getHdisMap(String coy, List<String> chdrnumList);
	public boolean updateHdisValidflag(Hdispf hdispf);
	public void insertHdisValidRecord(List<Hdispf> hdisBulkOpList);
	public boolean updateHdisRecords(Hdispf hdispf);
	public void insertHdisRecords(List<Hdispf> hdisBulkOpList);
	public void insertAllRecords(List<Hdispf> hdisList);
	public void insertHdisRecord(List<Hdispf> hdispfList);
	public List<Hdispf> searchHdisRecord(Hdispf hdis);
	public void updateHdisRecord(List<Hdispf> hdispfupdateList);
	
}
