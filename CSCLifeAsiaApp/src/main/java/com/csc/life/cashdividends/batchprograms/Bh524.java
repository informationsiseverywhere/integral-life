/*
 * File: Bh524.java
 * Date: 29 August 2009 21:30:36
 * Author: Quipoz Limited
 *
 * Class transformed from BH524.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdcxpfDAO; //ILIFE-9080
import com.csc.life.cashdividends.dataaccess.dao.HdispfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.dataaccess.model.Hdcxpf; //ILIFE-9080
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.LifacmvPojo;
import com.csc.life.productdefinition.procedures.LifacmvUtils;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.procedures.Conlog;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom; //ILIFE-9080
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This batch program process the HDIS records extracted
*   in the previous program BH523 based on the Next Capitalisation
*   Date(HCAPNDT).
*   When Dividend Interest is capitalisated, these files will be
*   affected,
*        HDIS - dividend transaction summary
*        HDIV - dividend transaction details
*
*
*   PROCESSING
*   ==========
*
*   Initialise
*
*  - Check the restart method is 3.
*  - Find the name of the HDCX temporary file. HDCX is a temporary
*    file which has the name of "HDCX" + first two characters of
*    param 4 + the last four digits of the job number.
*  - Open HDCXPF as input.
*  - Pre-load constantly referenced Tables into working storage.
*    These are: T5688, TH501, T5645.
*  - Set up the general fields in LIFACMV.
*
*  Read.
*
*  - Read a HDCXPF.
*  - Check end of file and end processing
*
*  Edit.
*
*  - Softlock the contract if this HDCX is for a new contract.
*
*  Update.
*
*  - Read HCSD(Cash dividend details)
*  - Read TH501 with cash dividend method in HCSD and effective
*    date = NEXT CAPITALISATION DATE
*  - Read all HDIVINT via the keys from HDCX, and accumulate the
*    dividend amount into a working field.
*  - check if the total interest accumulated equals to the O/S
*    interest to be capitalised, if not skip processing on this
*    HDCX and log a message
*  - Read and hold the CHDRLIF(Contract header)
*  - Set NEW-TRANNO to CHDRLIF-TRANNO + 1
*  - Update related HDIV records, setting capitalised TRANNO to
*    NEW-TRANNO
*  - Invalid the current HDIS(Dividend & Interest Summary) and
*    write a new one with its O/S interest accumulated to its
*    dividend balance since last capitalisation date.  Re-set the
*    O/S interest value, set next cap date to last cap date and
*    calculate the new next cap date with the CAP DUE MM & DD and
*    TH501-FREQ for capitalisation using DATCON4
*  - if capitalisation amount is not zero, write accounting entries,
*         look for the posting level in T5688 for CNTTYPE
*         set up fields in LIFACMV linkage
*         map for the relevant GL details in T5645
*         call LIFACMV twice to post the dividend expense and
*         its opposite entry for payable.
*  - various file updates for transaction history, in CHDRLIF
*    and PTRN.
*
*  Finalise.
*
*  - Close HDCXPF
*  - Remove the override
*  - Set the parameter statuz to O-K
*
*
*   Control totals:
*     01 - No. of HDCX records read
*     02 - No. of Cover with no O/S interest
*     03 - No. of HDCX records locked
*     04 - No. of HDCX records processed
*     05 - No. of unreconciled HDCX
*     06 - Total Value of Interest Capitalised
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Bh524 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();


	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH524");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaConlMsg = new FixedLengthStringData(71);
	private FixedLengthStringData wsaaHdisKey = new FixedLengthStringData(20).isAPartOf(wsaaConlMsg, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(wsaaConlMsg, 20, FILLER).init("Total interest to capitalise does not reconcile O/S");
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaHdcxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaHdcxFn, 0, FILLER).init("HDCX");
	private FixedLengthStringData wsaaHdcxRunid = new FixedLengthStringData(2).isAPartOf(wsaaHdcxFn, 4);
	private ZonedDecimalData wsaaHdcxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHdcxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaSystemDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSysDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaSystemDate, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaCapInt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaT5645Array = new FixedLengthStringData(21000);
	private FixedLengthStringData[] wsaaT5645Data = FLSArrayPartOfStructure(1000, 21, wsaaT5645Array, 0);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	//private int wsaaT5645Size = 45;
	//ILIFE-2605 fixed--Array size increased
	private int wsaaT5645Size = 1000;
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5688Array = new FixedLengthStringData(9000);
	private FixedLengthStringData[] wsaaT5688Rec = FLSArrayPartOfStructure(1000, 9, wsaaT5688Array, 0);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(8, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Key, 3);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 8);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
	//private int wsaaT5688Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private int wsaaT5688Size = 1000;

	private FixedLengthStringData wsaaTh501Array = new FixedLengthStringData(28000);
	private FixedLengthStringData[] wsaaTh501Rec = FLSArrayPartOfStructure(1000, 28, wsaaTh501Array, 0);
	private FixedLengthStringData[] wsaaTh501Key = FLSDArrayPartOfArrayStructure(9, wsaaTh501Rec, 0);
	private FixedLengthStringData[] wsaaTh501Divdmth = FLSDArrayPartOfArrayStructure(4, wsaaTh501Key, 0, SPACES);
	private PackedDecimalData[] wsaaTh501Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaTh501Key, 4);
	private FixedLengthStringData[] wsaaTh501Data = FLSDArrayPartOfArrayStructure(19, wsaaTh501Rec, 9);
	private FixedLengthStringData[] wsaaTh501CapFq = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 0);
	private FixedLengthStringData[] wsaaTh501IntFq = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 2);
	private FixedLengthStringData[] wsaaTh501CapDd = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 4);
	private FixedLengthStringData[] wsaaTh501IntDd = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 6);
	private FixedLengthStringData[] wsaaTh501CapMm = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 8);
	private FixedLengthStringData[] wsaaTh501IntMm = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 10);
	private FixedLengthStringData[] wsaaTh501Calcprog = FLSDArrayPartOfArrayStructure(7, wsaaTh501Data, 12);
	//private int wsaaTh501Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private int wsaaTh501Size = 1000;
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaHdisDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaHdisDd = new FixedLengthStringData(2).isAPartOf(wsaaHdisDate, 6);
		/* TABLES */
	private String th501 = "TH501";
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5688 = "T5688";
		/* ERRORS */
	private String ivrm = "IVRM";
	private String h791 = "H791";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Header Life Fields*/
//	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Getdescrec getdescrec = new Getdescrec();
		/*Cash Dividend Details Logical*/
//	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
//	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Allocation Trans Details Logica*/
//	private HdivTableDAM hdivIO = new HdivTableDAM();
		/*Dividend Cash Withdraw Trans. Details*/
//	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
		/*O/S Dividend Interest Transaction Detail*/
//	private HdivintTableDAM hdivintIO = new HdivintTableDAM();
		/*Table items, date - maintenance view*/
//	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
//	private ItemTableDAM itemIO = new ItemTableDAM();
//	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Policy transaction history logical file*/
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Th501rec th501rec = new Th501rec();
	
	private List<Itempf> t5645List = null;
    private List<Itempf> t5688List = null;
 //   private List<Itempf> t6639List = null;
    private List<Itempf> th501List = null;
    private Itempf itempf = new Itempf();
    List<Itempf> itempfList = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private LifacmvUtils lifacmvUtils = getApplicationContext().getBean("lifacmvUtils", LifacmvUtils.class);
	private LifacmvPojo lifacmvPojo = new LifacmvPojo();
	private Syserrrec syserrrec = new Syserrrec();
	private Syserr syserr = new Syserr();
	private static final Logger LOGGER = LoggerFactory.getLogger(Bh524.class);
	private	Hcsdpf hcsdpf = new Hcsdpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private Ptrnpf ptrnpf = new Ptrnpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private List<Ptrnpf> ptrnpfList = new ArrayList<>(); //ILIFE-9080
	private SftlockUtil sftlockUtil = new SftlockUtil();
	SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private HdivpfDAO hdivpfDao = getApplicationContext().getBean("hdivpfDAO",HdivpfDAO.class);
	//private List<Hdivpf> hdivpfList = new ArrayList<>(); //ILIFE-9080
	private String username;
	private List<Hdispf> hdispfList = new ArrayList<>();
	private Hdispf hdispf = new Hdispf();
	private HdispfDAO hdispfDAO = getApplicationContext().getBean("hdispfDAO", HdispfDAO.class); 
	private Hdivpf hdivpf = new Hdivpf();
	//ILIFE-9080 start
    private Iterator<Hdcxpf> iter;
	private int batchID;
	private int batchExtractSize;
	private Hdcxpf hdcxpf; 
	private HdcxpfDAO hdcxpfDAO = getApplicationContext().getBean("hdcxpfDAO", HdcxpfDAO.class);
	private int ct01Value;
	private int ct02Value;
	private int ct03Value;
	private int ct04Value;
	private int ct05Value;
	private BigDecimal ct06Value = BigDecimal.ZERO;
	private Map<String, List<Hcsdpf>> hcsdListMap = new HashMap<>();
	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<>();
	private Map<String, List<Hdispf>> hdisListMap = new HashMap<>();
	private Map<String, List<Hdivpf>> hdivListMap = new HashMap<>();
	List<Hdivpf> hdivpfUpdateList = new LinkedList<>();
	private Map<String, List<Hdivpf>> hdivcshListMap = new HashMap<>();
    private List<Chdrpf> chdrpfListUpdt = new ArrayList<>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<>();
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",HcsdpfDAO.class); //ILIFE-9444
 	//ILIFE-9080 end
	private List<Hdispf> hdisupdateList = new ArrayList<>();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1209,
		exit1309,
		exit1509,
		exit2090,
		exit2590,
		exit3090,
		x1090Exit
	}

	public Bh524() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		username = (String) ThreadLocalStore.get(ThreadLocalStore.USER);
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		
		 //ILIFE-9080 start
		wsspEdterror.set(varcom.oK);
		wsaaHdcxRunid.set(bprdIO.getSystemParam04());
		wsaaHdcxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
        if(wsspEdterror.equals(varcom.endp)){
        	return;
        }
 		//ILIFE-9080 end      
		wsaaSystemDate.set(getCobolDate());
		
		t5645List=itempfDAO.findItem("IT", bsprIO.getCompany().toString(), t5645, wsaaProg.toString());
		t5688List=itempfDAO.findItem(bsprIO.getCompany().toString(), t5688, varcom.vrcmMaxDate.toInt());
		th501List=itempfDAO.findItem(bsprIO.getCompany().toString(), th501, varcom.vrcmMaxDate.toInt());
		
		if (t5645List.size() > wsaaT5645Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(t5645);
            fatalError600();
        }
		loadT56451200(t5645List);
		
		if (t5688List.size() > wsaaT5688Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(t5688);
            fatalError600();
        }
		loadT56881300(t5688List);
		
		if (th501List.size() > wsaaTh501Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(th501);
            fatalError600();
        }
			loadTh5011500(th501List);
			
		lifacmvPojo.setBatccoy(batcdorrec.company.toString());
		lifacmvPojo.setBatcbrn(batcdorrec.branch.toString());
		lifacmvPojo.setBatcactyr(batcdorrec.actyear.toInt());
		lifacmvPojo.setBatcactmn(batcdorrec.actmonth.toInt());
		lifacmvPojo.setBatctrcde(batcdorrec.trcde.toString());
		lifacmvPojo.setBatcbatch(batcdorrec.batch.toString());
		lifacmvPojo.setRldgcoy(batcdorrec.company.toString());
		lifacmvPojo.setGenlcoy(batcdorrec.company.toString());
		lifacmvPojo.setPostyear(SPACES.stringValue());
		lifacmvPojo.setPostmonth(SPACES.stringValue());
		lifacmvPojo.setFrcdate(varcom.vrcmMaxDate.toInt());
		lifacmvPojo.setCrate(BigDecimal.ZERO);
		lifacmvPojo.setAcctamt(BigDecimal.ZERO);
		lifacmvPojo.setRcamt(ZERO.intValue());
		lifacmvPojo.setContot(ZERO.intValue());
		lifacmvPojo.setRcamt(ZERO.intValue());
		lifacmvPojo.setTermid(SPACES.stringValue());
		lifacmvPojo.setTransactionDate(bsscIO.getEffectiveDate().toInt());
		lifacmvPojo.setTransactionTime(varcom.vrcmTime.toInt());
		lifacmvPojo.setUser(ZERO.intValue());
		
		itempf.setItempfx("IT");
		itempf.setItemcoy(bsprIO.getCompany().toString());
		itempf.setItemtabl(t1688);
		itempf.setItemitem(bprdIO.getAuthCode().toString());
		getdescrec.itemkey.set(itempf.getItempfx().concat(bsprIO.getCompany().toString()).concat(t1688).concat(bprdIO.getAuthCode().toString()));
		
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvPojo.setTrandesc(SPACES.stringValue());
		}
		else {
			lifacmvPojo.setTrandesc(getdescrec.longdesc.toString());
		}
	}

//ILIFE-9080 start
private void readChunkRecord() {
	ct01Value = 0;
	ct02Value = 0;
	ct03Value = 0;
	ct04Value = 0;
	ct05Value = 0;
	ct06Value = BigDecimal.ZERO;
	
	List<Hdcxpf> hdcxpfList = hdcxpfDAO.searchHdcxpfRecord(wsaaHdcxFn.toString(), wsaaThreadMember.toString(), batchExtractSize, batchID);
	iter= hdcxpfList.iterator();
	if (!hdcxpfList.isEmpty()) {
	List<String> chdrnumList = new ArrayList<>(hdcxpfList.size());
	for (Hdcxpf p : hdcxpfList) {
		chdrnumList.add(p.getChdrnum());
	}
	String coy = bsprIO.getCompany().toString();
	chdrpfMap = chdrpfDAO.searchChdrpf(coy, chdrnumList);
	hdisListMap = hdispfDAO.getHdisMap(coy, chdrnumList);
	hdivListMap = hdivpfDao.getHdivMap(coy, chdrnumList);
	hdivcshListMap = hdivpfDao.getHdivMap(coy, chdrnumList);
	hcsdListMap = hcsdpfDAO.getHcsdMap(coy, chdrnumList);  //ILIFE-9444
	}
}
//ILIFE-9080 end
protected void loadT56451200(List<Itempf> itemList)
{
       int i = 1;
       int x = 1;
       for (Itempf itempf : itemList) {
           	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
         //  	ix.set(1);
			while ( !(isGT(x,15))) {
			wsaaT5645Cnttot[i].set(t5645rec.cnttot[x]);
			wsaaT5645Glmap[i].set(t5645rec.glmap[x]);
			wsaaT5645Sacscode[i].set(t5645rec.sacscode[x]);
			wsaaT5645Sacstype[i].set(t5645rec.sacstype[x]);
			wsaaT5645Sign[i].set(t5645rec.sign[x]);
			x++;
           i++;
		 }
       }
   }



protected void loadT56881300(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
           t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
           wsaaT5688Cnttype[i].set(itempf.getItemitem());
           wsaaT5688Itmfrm[i].set(itempf.getItmfrm());
           wsaaT5688Comlvlacc[i].set(t5688rec.comlvlacc);
           i++;
       }
   }

protected void loadTh5011500(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
    	   	th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
    	   	wsaaTh501Divdmth[i].set(itempf.getItemitem());
    	   	wsaaTh501Itmfrm[i].set(itempf.getItmfrm());
	   		wsaaTh501CapFq[i].set(th501rec.freqcy01);
           i++;
       }
   }

protected void readFile2000()
	{
	 //ILIFE-9080 start
	if (!iter.hasNext()) {
		batchID++;
		readChunkRecord();
		if (!iter.hasNext()) {
			wsspEdterror.set(Varcom.endp);
			return;
		}
	}
	 this.hdcxpf = iter.next();
	 ct01Value++;
//ILIFE-9080
	}

protected void edit2500()
	{
	//ILIFE-9080 start
	wsspEdterror.set(SPACES);
	if (isEQ(hdcxpf.getChdrnum(),wsaaALockedChdrnum)) {
		ct03Value++;
		return;
	}
	if (isNE(hdcxpf.getChdrnum(),lastChdrnum)) {
		softlockPolicy2600();
		if (isEQ(sftlockRecBean.getStatuz(),"LOCK")) {
			wsaaALockedChdrnum.set(hdcxpf.getChdrnum());
			return;
		}
		else {
			wsaaALockedChdrnum.set(SPACES);
		}
		lastChdrnum.set(hdcxpf.getChdrnum());
		wsaaJrnseq.set(ZERO);
	}
	if (isEQ(hdcxpf.getHintos(),0)) {
		ct02Value++;
		return;
	}
	wsspEdterror.set(varcom.oK);
	//ILIFE-9080 end
	}

protected void softlockPolicy2600()
	{
		softlockPolicy2610();
	}

protected void softlockPolicy2610()
	{
    	sftlockRecBean.setFunction("LOCK");
    	sftlockRecBean.setCompany(hdcxpf.getChdrcoy());
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(hdcxpf.getChdrnum());
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (isEQ(sftlockRecBean.getStatuz(),"LOCK")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(hdcxpf.getChdrcoy());
			stringVariable1.append('|');
			stringVariable1.append(hdcxpf.getChdrnum());
			conlogrec.params.setLeft(stringVariable1.toString());
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++;
		}
    	else {
    	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
    		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
    			syserrrec.statuz.set(sftlockRecBean.getStatuz());
    			fatalError600();
    		}
    	}
	}

protected void update3000()
	{
		try {
			update3010();
		}
		catch (GOTOException e){
		}
	}

protected void update3010()
	{
		String chdrnum = hdcxpf.getChdrnum();
	    if (hcsdListMap != null && hcsdListMap.containsKey(chdrnum)) {
	        List<Hcsdpf> hcsdpfList = hcsdListMap.get(chdrnum);
	        for (Iterator<Hcsdpf> iterator = hcsdpfList.iterator(); iterator.hasNext(); ){
	        	Hcsdpf hcsd = iterator.next();
	        	if (hcsd.getLife().equals(hdcxpf.getLife())
	        		&&  hcsd.getCoverage().equals(hdcxpf.getCoverage())
	        		&& 	hcsd.getRider().equals(hdcxpf.getRider())
	        		&& Integer.compare(hcsd.getPlnsfx(),hdcxpf.getPlnsfx()) == 0 ) {
	        		hcsdpf = hcsd;
	        		break;
	        	}
	        }
	    }	
	    if(hcsdpf == null) {
	    	syserrrec.params.set(hdcxpf.getChdrnum());
			fatalError600();
	    }
	    
		for (ix.set(1); !(isGT(ix,wsaaTh501Size)
		|| isEQ(wsaaTh501Divdmth[ix.toInt()],SPACES)
		|| (isEQ(wsaaTh501Divdmth[ix.toInt()],hcsdpf.getZcshdivmth())
		&& isLTE(wsaaTh501Itmfrm[ix.toInt()],hdcxpf.getHcapndt()))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaTh501Size)
		|| isEQ(wsaaTh501Divdmth[ix.toInt()],SPACES)) {
			itempf = itempfDAO.findItemByItdm(bsprIO.getCompany().toString(), th501, hcsdpf.getZcshdivmth(), hdcxpf.getHcapndt());
		}
		wsaaCapInt.set(0);
		List<Hdivpf> hdivpfList = hdivListMap.get(chdrnum);
        for (Iterator<Hdivpf> iterator = hdivpfList.iterator(); iterator.hasNext(); ){
        	Hdivpf hdiv = iterator.next();
        	if (hdiv.getLife().equals(hdcxpf.getLife())
            		&&  hdiv.getCoverage().equals(hdcxpf.getCoverage())
            		&&  hdiv.getRider().equals(hdcxpf.getRider())
            		&&  Integer.compare(hdiv.getPlnsfx(),hdcxpf.getPlnsfx()) == 0
            		&&  hdiv.getHincapdt().equals(hdcxpf.getHcapndt())
            		&& hdiv.getHdvtyp().equals("I")
            		&& hdiv.getHdvcaptx().equals(0)) {
        			hdivpf = hdiv;
        			wsaaCapInt.add(hdiv.getHdvamt().doubleValue());
            			
		}
        }
		if (isNE(wsaaCapInt,hdcxpf.getHintos())) {
			ct05Value++;
			wsaaConlMsg.set(SPACES);
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(hdcxpf.getChdrcoy());
			stringVariable1.append('|');
			stringVariable1.append(hdcxpf.getChdrnum());
			stringVariable1.append('|');
			stringVariable1.append(hdcxpf.getLife());
			stringVariable1.append('|');
			stringVariable1.append(hdcxpf.getCoverage());
			stringVariable1.append('|');
			stringVariable1.append(hdcxpf.getRider());
			wsaaHdisKey.setLeft(stringVariable1.toString());
			conlogrec.message.set(wsaaConlMsg);
			callProgram(Conlog.class, conlogrec.conlogRec);
			goTo(GotoLabel.exit3090);
		}
		 //ILIFE-9080 start
		if(chdrpfMap!=null &&chdrpfMap.containsKey(hdcxpf.getChdrnum())){
			for(Chdrpf c:chdrpfMap.get(hdcxpf.getChdrnum())){
				if(c.getChdrcoy().toString().equals(hdcxpf.getChdrcoy())){
					chdrpf = c;
					break;
				}
			}
		}
		if(!(chdrpfListInst.isEmpty())) {
			int maxtranno = 0;
			for(Chdrpf c : chdrpfListInst) {
				if(c.getChdrnum().equals(lastChdrnum.toString())) {
					if(maxtranno < c.getTranno()) {
						maxtranno = c.getTranno();
						chdrpf = c;
					}
				}
			}
		}
		if(chdrpf == null){
			syserrrec.params.set(hdcxpf.getChdrnum());
			fatalError600();
		}
		compute(wsaaNewTranno, 0).set(add(chdrpf.getTranno(),1));
		if(!(hdivpfList.isEmpty())) {
			//ILIFE-3790 end
			for(Hdivpf hdiv:hdivpfList) {
				if(hdiv.getHdvtyp().equals("I")) {
					hdiv.setUsrprf(username);
					hdiv.setJobnm(username);
					hdiv.setTranno(wsaaNewTranno.toInt());
					hdivpfUpdateList.add(hdiv);
				}
			}
		}

	 //ILIFE-9080 start
	   if (hdisListMap != null && hdisListMap.containsKey(chdrnum)) {
	        List<Hdispf> hdispfList = hdisListMap.get(chdrnum);
	        for (Iterator<Hdispf> iterator = hdispfList.iterator(); iterator.hasNext(); ){
	        	Hdispf hdis = iterator.next();
	        	if (hdis.getLife().equals(hdcxpf.getLife())
	            		&&  hdis.getCoverage().equals(hdcxpf.getCoverage())
	            		&& 	hdis.getRider().equals(hdcxpf.getRider())
	            		&& Integer.compare(hdis.getPlnsfx(),hdcxpf.getPlnsfx()) == 0 ) {
	        			hdispf = hdis;
	            			break;
	        	}
	        }
	   }
	   if(!(hdispfList.isEmpty())) {
       	int maxtranno = 0;
			for(Hdispf h : hdispfList) {
				if(h.getChdrnum().equals(lastChdrnum.toString())) {
					if(maxtranno < h.getTranno()) {
						maxtranno = h.getTranno();
						hdispf = h;
					}
				}
			}
       }
	   if(hdispf == null) {
		   syserrrec.params.set(hdcxpf.getChdrnum());
		   fatalError600();
	   }

		wsaaTotDividend.set(ZERO);
	
		if (hdivcshListMap != null && hdivcshListMap.containsKey(chdrnum)) {
	        List<Hdivpf> hdivcshList = hdivcshListMap.get(chdrnum);
	        for (Iterator<Hdivpf> iterator = hdivpfList.iterator(); iterator.hasNext(); ){
	        	Hdivpf hdivcsh = iterator.next();
	        	if (hdivcsh.getLife().equals(hdcxpf.getLife())
	        		&&  hdivcsh.getCoverage().equals(hdcxpf.getCoverage())
	        		&& 	hdivcsh.getRider().equals(hdcxpf.getRider())
	        		&& Integer.compare(hdivcsh.getPlnsfx(),hdcxpf.getPlnsfx()) == 0 
	        		&& Integer.compare(hdivcsh.getHincapdt(),hdispf.getHcapldt() + 1) == 0
	        		&& hdivcsh.getHdvtyp().equals("C")) {
	        		wsaaTotDividend.add(hdivcsh.getHdvamt().doubleValue());
	        	}
	        }
		}
		//ILIFE-9080 end

		Hdispf hdispfupdt = new Hdispf(hdispf);
		hdispfupdt.setValidflag("2");
		hdisupdateList.add(hdispfupdt);
		//hdispfDAO.updateHdisValidflag(hdispf);
		Hdispf hdispfinst = new Hdispf(hdispf);
		hdispfinst.setValidflag("1");
		hdispfinst.setTranno(wsaaNewTranno.toInt());
		setPrecision(hdispfinst.getHdvbalc(), 2);
		hdispfinst.setHdvbalc(add(add(hdispf.getHdvbalc(),hdispf.getHintos()),wsaaTotDividend).getbigdata());
		//setPrecision(hdispfinst.getHdvbalc(), 2);
		//hdispfinst.setHdvbalc(add(hdispf.getHdvbalc(),wsaaTotDividend).getbigdata());
		hdispfinst.setHintos(BigDecimal.ZERO);
		hdispfinst.setHcapldt(hdispf.getHcapndt());
		if (isNE(hdispf.getHcapndt(),hdcxpf.getRcesdte())) {
			datcon4rec.intDate1.set(hdispf.getHcapndt());
			wsaaHdisDate.set(hdispf.getHcapndt());
			datcon4rec.frequency.set(wsaaTh501CapFq[ix.toInt()]);
			datcon4rec.freqFactor.set(1);
			datcon4rec.billmonth.set(hdispf.getHcapduem());
			datcon4rec.billday.set(wsaaHdisDd);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				fatalError600();
			}
			if (isGT(datcon4rec.intDate2,hdcxpf.getRcesdte())) {
				hdispfinst.setHcapndt((hdcxpf.getRcesdte()));
			}
			else {
				hdispfinst.setHcapndt(datcon4rec.intDate2.toInt());
			}
		}
		if(hdispfList == null) {
			hdispfList = new ArrayList<>();
		}		
		hdispfList.add(hdispfinst);
		
		if (isNE(wsaaCapInt,0)) {
			writeAccounting3200();
		}
		 //ILIFE-9080 start
		Chdrpf pf = new Chdrpf(chdrpf);
		pf.setUniqueNumber(chdrpf.getUniqueNumber());
		pf.setValidflag("2".charAt(0));
		pf.setCurrto(bsscIO.getEffectiveDate().toInt());
		if(chdrpfListUpdt==null){
			chdrpfListUpdt = new ArrayList<>();
		}
		this.chdrpfListUpdt.add(pf);
		Chdrpf chdrpfinst = new Chdrpf(chdrpf);
		chdrpfinst.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrpfinst.setValidflag("1".charAt(0));
		chdrpfinst.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		chdrpfinst.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpfinst.setTranno(wsaaNewTranno.toInt());
		if(chdrpfListInst==null){
			chdrpfListInst = new ArrayList<>();
		}
		this.chdrpfListInst.add(chdrpfinst);
		
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setChdrcoy(hdcxpf.getChdrcoy());
		ptrnpf.setChdrnum(hdcxpf.getChdrnum());
		ptrnpf.setTranno(wsaaNewTranno.toInt());
		ptrnpf.setValidflag("1");
		ptrnpf.setPtrneff(hdcxpf.getHcapndt());
		ptrnpf.setTermid(SPACES.stringValue());
		ptrnpf.setTrdt(wsaaSysDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTimen.toInt());
		ptrnpf.setUserT(999999);
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		if(ptrnpfList == null){
			ptrnpfList = new ArrayList<>();
		}
		ptrnpfList.add(ptrnpf);
		 //ILIFE-9080 end
	}

protected void writeAccounting3200()
	{
		accounting3210();
	}

protected void accounting3210()
	{
		for (ix.set(1); !(isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)
		|| (isEQ(wsaaT5688Cnttype[ix.toInt()],chdrpf.getCnttype())
		&& isLTE(wsaaT5688Itmfrm[ix.toInt()],chdrpf.getOccdate()))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)) {
			
			itempf = itempfDAO.findItemByItdm(bsprIO.getCompany().toString(), t5688, chdrpf.getCnttype(), chdrpf.getOccdate());//IJTI-1485
		}
		wsaaComponLevelAccounted.set(wsaaT5688Comlvlacc[ix.toInt()]);
		lifacmvPojo.setOrigcurr(chdrpf.getCntcurr());//IJTI-1485
		lifacmvPojo.setOrigamt(wsaaCapInt.getbigdata());
		wsaaJrnseq.add(1);
		if (!componLevelAccounted.isTrue()) {
			t5645Ix.set(1);
		//	lifacmvPojo.substituteCode[6].set(SPACES);
			String[] substituteCodes = new String[6];
	        substituteCodes[0] = SPACES.stringValue();
	        lifacmvPojo.setSubstituteCodes(substituteCodes);
			lifacmvPojo.setRldgacct(SPACES.stringValue());
			wsaaPlan.set(hdcxpf.getPlnsfx()); //ILIFE-9080
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(hdcxpf.getChdrnum()); //ILIFE-9080
			stringVariable1.append(hdcxpf.getLife()); //ILIFE-9080
			stringVariable1.append(hdcxpf.getCoverage()); //ILIFE-9080
			stringVariable1.append(hdcxpf.getRider()); //ILIFE-9080
			stringVariable1.append(wsaaPlan);
			lifacmvPojo.setRldgacct(stringVariable1.toString());
		}
		else {
			t5645Ix.set(3);
		//	lifacmvPojo.substituteCode[6].set(hdcxpfRec.crtable);
			String[] substituteCodes = new String[6];
	        substituteCodes[0] = hdcxpf.getCrtable();
	        lifacmvPojo.setSubstituteCodes(substituteCodes);
			lifacmvPojo.setRldgacct(hdcxpf.getChdrnum());
		}
		lifacmvPojo.setSacscode(wsaaT5645Sacscode[t5645Ix.toInt()].toString());
		lifacmvPojo.setSacstyp(wsaaT5645Sacstype[t5645Ix.toInt()].toString());
		lifacmvPojo.setGlcode(wsaaT5645Glmap[t5645Ix.toInt()].toString());
		lifacmvPojo.setGlsign(wsaaT5645Sign[t5645Ix.toInt()].toString());
		lifacmvPojo.setContot(wsaaT5645Cnttot[t5645Ix.toInt()].toInt());
		x1000CallLifacmv();
		wsaaJrnseq.add(1);
		t5645Ix.set(2);
		lifacmvPojo.setSacscode(wsaaT5645Sacscode[t5645Ix.toInt()].toString());
		lifacmvPojo.setSacstyp(wsaaT5645Sacstype[t5645Ix.toInt()].toString());
		lifacmvPojo.setGlcode(wsaaT5645Glmap[t5645Ix.toInt()].toString());
		lifacmvPojo.setGlsign(wsaaT5645Sign[t5645Ix.toInt()].toString());
		lifacmvPojo.setContot(wsaaT5645Cnttot[t5645Ix.toInt()].toInt());
		if (isNE(hdispf.getSacscode(),SPACES)
		&& isNE(hdispf.getSacstyp(),SPACES)) {
			lifacmvPojo.setSacscode(hdispf.getSacscode());//IJTI-1485
			lifacmvPojo.setSacstyp(hdispf.getSacstyp());//IJTI-1485
		}
	//	lifacmvPojo.substituteCode[6].set(SPACES);
		String[] substituteCodes = new String[6];
        substituteCodes[0] = SPACES.stringValue();
        lifacmvPojo.setSubstituteCodes(substituteCodes);
		lifacmvPojo.setRldgacct(hdcxpf.getChdrnum());
		x1000CallLifacmv();
 //ILIFE-9080 start
		ct04Value++;
		ct06Value = ct06Value.add(wsaaCapInt.getbigdata());
 //ILIFE-9080 end
		
	}

protected void commit3500()
	{
		/*COMMIT*/
	 //ILIFE-9080 start
	commitControlTotals();
	
	if( hdivpfUpdateList != null && !hdivpfUpdateList.isEmpty()) {
		hdivpfDao.bulkUpdateHdivRecords(hdivpfUpdateList);
		hdivpfUpdateList.clear();
	}
		
	if( hdispfList != null && !hdispfList.isEmpty()) {
	   	hdispfDAO.insertHdisValidRecord(hdispfList);
	   	hdispfList.clear();
	 }
	
	if( hdisupdateList != null && !hdisupdateList.isEmpty()) {
    	hdispfDAO.updateHdisRecord(hdisupdateList);
    	hdisupdateList.clear();
	 }
	
	if (chdrpfListInst != null && !chdrpfListInst.isEmpty()) {
		chdrpfDAO.insertChdrValidRecord(chdrpfListInst);
		chdrpfListInst.clear();
	}
	
	if (chdrpfListUpdt != null && !chdrpfListUpdt.isEmpty()) {
		chdrpfDAO.updateChdrRecordByTranno(chdrpfListUpdt);
		chdrpfListUpdt.clear();
	}
		
	if (ptrnpfList != null && !ptrnpfList.isEmpty()) {
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
	}
	 //ILIFE-9080 end
		/*EXIT*/
	}

//ILIFE-9080 start
private void commitControlTotals(){ 
	
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Value);
	callContot001();
	
	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Value);
	callContot001();
	
	contotrec.totno.set(ct03);
	contotrec.totval.set(ct03Value);
	callContot001();
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Value);
	callContot001();
	
	contotrec.totno.set(ct05);
	contotrec.totval.set(ct05Value);
	callContot001();
	
	contotrec.totno.set(ct06);
	contotrec.totval.set(ct06Value);
	callContot001();
	
}
//ILIFE-9080 end

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
	
	//ILIFE-9080 start
		if (chdrpfMap != null) {
			chdrpfMap.clear();
		}
		if (hcsdListMap != null) {
			hcsdListMap.clear();
		}
		
		if (hdivcshListMap != null) {
			hdivcshListMap.clear();
		}
		
		if (hdisListMap != null) {
			hdisListMap.clear();
		}
		hdispfList.clear();
		hdispfList=null;
		
		/*hdivpfList.clear();
		hdivpfList=null;*/
		
		chdrpfListInst.clear();
		chdrpfListInst=null;
		
		chdrpfListUpdt.clear();
		chdrpfListUpdt= null;
		
		ptrnpfList.clear();
		ptrnpfList=null;
		
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}


protected void x1000CallLifacmv()
	{
		if (isEQ(lifacmvPojo.getOrigamt(),ZERO)) {
			goTo(GotoLabel.x1090Exit);
		}
		lifacmvPojo.setFunction("PSTW");
		lifacmvPojo.setJrnseq(wsaaJrnseq.toInt());
		lifacmvPojo.setRdocnum(hdcxpf.getChdrnum());
		lifacmvPojo.setTranref(hdcxpf.getChdrnum());
		lifacmvPojo.setTranno(wsaaNewTranno.toInt());
		lifacmvPojo.setEffdate(hdcxpf.getHcapndt());
		String[] substituteCodes = new String[6];
	    substituteCodes[0] = chdrpf.getCnttype();//IJTI-1485
	    lifacmvPojo.setSubstituteCodes(substituteCodes);
		lifacmvUtils.calcLifacmv(lifacmvPojo);
		if (isNE(lifacmvPojo.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifacmvPojo);
			syserrrec.statuz.set(lifacmvPojo.getStatuz());
			databaseError99500(lifacmvPojo,"2");
		}
	}

protected void databaseError99500(LifacmvPojo lifacmvPojo,String syserrType){
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set(syserrType);
	syserr.mainline(new Object[] { syserrrec.syserrRec });
	lifacmvPojo.setStatuz("BOMB");
	LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
	throw new RuntimeException(new COBOLExitProgramException());
}
}
