package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:30
 * Description:
 * Copybook name: HDISGRVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdisgrvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdisgrvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdisgrvKey = new FixedLengthStringData(64).isAPartOf(hdisgrvFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdisgrvChdrcoy = new FixedLengthStringData(1).isAPartOf(hdisgrvKey, 0);
  	public FixedLengthStringData hdisgrvChdrnum = new FixedLengthStringData(8).isAPartOf(hdisgrvKey, 1);
  	public FixedLengthStringData hdisgrvLife = new FixedLengthStringData(2).isAPartOf(hdisgrvKey, 9);
  	public FixedLengthStringData hdisgrvCoverage = new FixedLengthStringData(2).isAPartOf(hdisgrvKey, 11);
  	public FixedLengthStringData hdisgrvRider = new FixedLengthStringData(2).isAPartOf(hdisgrvKey, 13);
  	public PackedDecimalData hdisgrvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hdisgrvKey, 15);
  	public PackedDecimalData hdisgrvTranno = new PackedDecimalData(5, 0).isAPartOf(hdisgrvKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(hdisgrvKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdisgrvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdisgrvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}