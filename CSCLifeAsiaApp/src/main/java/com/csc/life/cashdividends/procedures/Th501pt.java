/*
 * File: Th501pt.java
 * Date: 30 August 2009 2:33:45
 * Author: Quipoz Limited
 * 
 * Class transformed from TH501PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH501.
*
*
*****************************************************************
* </pre>
*/
public class Th501pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Cash Dividend Methods                    SH501");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(58);
	private FixedLengthStringData filler9 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine004, 11, FILLER).init("Capitalisation:            Interest Allocation:");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(59);
	private FixedLengthStringData filler11 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 18, FILLER).init("Freq:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 30);
	private FixedLengthStringData filler13 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 45, FILLER).init("Freq:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 57);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(59);
	private FixedLengthStringData filler15 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 18, FILLER).init("Fixed DD:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 30);
	private FixedLengthStringData filler17 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 45, FILLER).init("Fixed DD:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 57);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(59);
	private FixedLengthStringData filler19 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 18, FILLER).init("Fixed MM:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 30);
	private FixedLengthStringData filler21 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 45, FILLER).init("Fixed MM:");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 57);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(55);
	private FixedLengthStringData filler23 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine008, 11, FILLER).init("Interest Calculation Subroutine:");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 45);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(31);
	private FixedLengthStringData filler25 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine009, 11, FILLER).init("Dividend Allocation:");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(59);
	private FixedLengthStringData filler27 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine010, 18, FILLER).init("At due date:");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 58);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(59);
	private FixedLengthStringData filler29 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine011, 18, FILLER).init("On receipt of full prem:");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 58);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(59);
	private FixedLengthStringData filler31 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine012, 18, FILLER).init("On receipt of full prem + 1 instalment:");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 58);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(28);
	private FixedLengthStringData filler33 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th501rec th501rec = new Th501rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th501pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th501rec.th501Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(th501rec.freqcy01);
		fieldNo008.set(th501rec.freqcy02);
		fieldNo009.set(th501rec.hfixdd01);
		fieldNo010.set(th501rec.hfixdd02);
		fieldNo011.set(th501rec.hfixmm01);
		fieldNo012.set(th501rec.hfixmm02);
		fieldNo013.set(th501rec.intCalcSbr);
		fieldNo014.set(th501rec.ind01);
		fieldNo015.set(th501rec.ind02);
		fieldNo016.set(th501rec.ind03);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
