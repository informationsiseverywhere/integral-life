/******************************************************************************
 * File Name 		: Hdsxpf.java
 * Author			: sbatra9
 * Creation Date	: 24 June 2020
 * Project			: Integral Life
 * Description		: The Model Class for HDSXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.csc.life.cashdividends.dataaccess.dao.HdsxpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdsxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HdsxpfDAOImpl extends BaseDAOImpl<Hdsxpf> implements HdsxpfDAO{
    private static final Logger LOGGER = LoggerFactory.getLogger(HdsxpfDAOImpl.class);

    public List<Hdsxpf> searchHdsxpfRecord(String tableId, String memName,
			int batchExtractSize, int batchID) {
    	
    	StringBuilder  sqlStr = new StringBuilder();
    	sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.CHDRCOY, TE.CHDRNUM)-1)/?) ROWNM, TE.* FROM ");
        sqlStr.append(tableId);
        sqlStr.append(" TE");
        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
 		List<Hdsxpf> pfList = new ArrayList<>();
 		ResultSet rs = null;

 		try {
 			 ps.setInt(1, batchExtractSize);
        	 ps.setString(2, memName);
        	 ps.setInt(3, batchID);
        	 rs = ps.executeQuery();
             while (rs.next()) {
                Hdsxpf hdsxpf = new Hdsxpf();
                hdsxpf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
                hdsxpf.setChdrcoy(rs.getString("CHDRCOY"));
                hdsxpf.setChdrnum(rs.getString("CHDRNUM"));
                hdsxpf.setLife(rs.getString("LIFE"));
                hdsxpf.setJlife(rs.getString("JLIFE"));
                hdsxpf.setCoverage(rs.getString("COVERAGE"));
                hdsxpf.setRider(rs.getString("RIDER"));
                hdsxpf.setPlnsfx(rs.getInt("PLNSFX"));
                hdsxpf.setCrtable(rs.getString("CRTABLE"));
                hdsxpf.setHdvbalc(rs.getInt("HDVBALC"));
                hdsxpf.setHintndt(rs.getInt("HINTNDT"));
                hdsxpf.setHintos(rs.getInt("HINTOS"));
                hdsxpf.setRcesdte(rs.getInt("RCESDTE"));
            	pfList.add(hdsxpf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchHdsxpfRecord()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pfList;
    }

}