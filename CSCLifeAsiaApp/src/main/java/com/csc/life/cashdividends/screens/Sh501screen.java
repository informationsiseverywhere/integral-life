package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh501screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh501ScreenVars sv = (Sh501ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh501screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh501ScreenVars screenVars = (Sh501ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.freqcy01.setClassString("");
		screenVars.freqcy02.setClassString("");
		screenVars.hfixdd01.setClassString("");
		screenVars.hfixdd02.setClassString("");
		screenVars.hfixmm01.setClassString("");
		screenVars.hfixmm02.setClassString("");
		screenVars.intCalcSbr.setClassString("");
		screenVars.ind01.setClassString("");
		screenVars.ind02.setClassString("");
		screenVars.ind03.setClassString("");
	}

/**
 * Clear all the variables in Sh501screen
 */
	public static void clear(VarModel pv) {
		Sh501ScreenVars screenVars = (Sh501ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.freqcy01.clear();
		screenVars.freqcy02.clear();
		screenVars.hfixdd01.clear();
		screenVars.hfixdd02.clear();
		screenVars.hfixmm01.clear();
		screenVars.hfixmm02.clear();
		screenVars.intCalcSbr.clear();
		screenVars.ind01.clear();
		screenVars.ind02.clear();
		screenVars.ind03.clear();
	}
}
