package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh537screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh537ScreenVars sv = (Sh537ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh537screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh537ScreenVars screenVars = (Sh537ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.firstDivdDateDisp.setClassString("");
		screenVars.lastDivdDateDisp.setClassString("");
		screenVars.tamt01.setClassString("");
		screenVars.lastIntDateDisp.setClassString("");
		screenVars.nextIntDateDisp.setClassString("");
		screenVars.tamt02.setClassString("");
		screenVars.lastCapDateDisp.setClassString("");
		screenVars.nextCapDateDisp.setClassString("");
		screenVars.tamt03.setClassString("");
		screenVars.divdStmtDateDisp.setClassString("");
		screenVars.divdStmtNo.setClassString("");
		screenVars.tamt04.setClassString("");
	}

/**
 * Clear all the variables in Sh537screen
 */
	public static void clear(VarModel pv) {
		Sh537ScreenVars screenVars = (Sh537ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.currcd.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.crtable.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.firstDivdDateDisp.clear();
		screenVars.firstDivdDate.clear();
		screenVars.lastDivdDateDisp.clear();
		screenVars.lastDivdDate.clear();
		screenVars.tamt01.clear();
		screenVars.lastIntDateDisp.clear();
		screenVars.lastIntDate.clear();
		screenVars.nextIntDateDisp.clear();
		screenVars.nextIntDate.clear();
		screenVars.tamt02.clear();
		screenVars.lastCapDateDisp.clear();
		screenVars.lastCapDate.clear();
		screenVars.nextCapDateDisp.clear();
		screenVars.nextCapDate.clear();
		screenVars.tamt03.clear();
		screenVars.divdStmtDateDisp.clear();
		screenVars.divdStmtDate.clear();
		screenVars.divdStmtNo.clear();
		screenVars.tamt04.clear();
	}
}
