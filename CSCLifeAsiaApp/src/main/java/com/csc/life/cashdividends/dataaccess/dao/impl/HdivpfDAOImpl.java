/******************************************************************************
 * File Name 		: HdivpfDAOImpl.java
 * Author			: smalchi2
 * Creation Date	: 02 January 2017
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for HDIVPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HdivpfDAOImpl extends BaseDAOImpl<Hdivpf> implements HdivpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HdivpfDAOImpl.class);

	public List<Hdivpf> searchHdivpfRecord(Hdivpf hdivpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, ");
		sqlSelect.append("BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ");
		sqlSelect.append("CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, ");
		sqlSelect.append("HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, ");
		sqlSelect.append("HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM HDIVPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND HINCAPDT = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, HINCAPDT ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());
			psHdivpfSelect.setString(3, hdivpf.getLife());
			psHdivpfSelect.setString(4, hdivpf.getCoverage());
			psHdivpfSelect.setString(5, hdivpf.getRider());
			psHdivpfSelect.setInt(6, hdivpf.getPlnsfx());
			psHdivpfSelect.setInt(7, hdivpf.getHincapdt());

			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();

				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getInt("UNIQUE_NUMBER"));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString("CHDRCOY"));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString("CHDRNUM"));
				hdivpfnew.setLife(sqlHdivpfRs.getString("LIFE"));
				hdivpfnew.setJlife(sqlHdivpfRs.getString("JLIFE"));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString("COVERAGE"));
				hdivpfnew.setRider(sqlHdivpfRs.getString("RIDER"));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt("PLNSFX"));
				hdivpfnew.setHpuanbr(sqlHdivpfRs.getInt("HPUANBR"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt("TRANNO"));
				hdivpfnew.setBatccoy(sqlHdivpfRs.getString("BATCCOY"));
				hdivpfnew.setBatcbrn(sqlHdivpfRs.getString("BATCBRN"));
				hdivpfnew.setBatcactyr(sqlHdivpfRs.getInt("BATCACTYR"));
				hdivpfnew.setBatcactmn(sqlHdivpfRs.getInt("BATCACTMN"));
				hdivpfnew.setBatctrcde(sqlHdivpfRs.getString("BATCTRCDE"));
				hdivpfnew.setBatcbatch(sqlHdivpfRs.getString("BATCBATCH"));
				hdivpfnew.setCntcurr(sqlHdivpfRs.getString("CNTCURR"));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt("EFFDATE"));
				hdivpfnew.setHdvaldt(sqlHdivpfRs.getInt("HDVALDT"));
				hdivpfnew.setHdvtyp(sqlHdivpfRs.getString("HDVTYP"));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setHdvrate(sqlHdivpfRs.getBigDecimal("HDVRATE"));
				hdivpfnew.setHdveffdt(sqlHdivpfRs.getInt("HDVEFFDT"));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString("ZDIVOPT"));
				hdivpfnew.setZcshdivmth(sqlHdivpfRs.getString("ZCSHDIVMTH"));
				hdivpfnew.setHdvopttx(sqlHdivpfRs.getInt("HDVOPTTX"));
				hdivpfnew.setHdvcaptx(sqlHdivpfRs.getInt("HDVCAPTX"));
				hdivpfnew.setHincapdt(sqlHdivpfRs.getInt("HINCAPDT"));
				hdivpfnew.setHdvsmtno(sqlHdivpfRs.getInt("HDVSMTNO"));
				hdivpfnew.setUsrprf(sqlHdivpfRs.getString("USRPRF"));
				hdivpfnew.setJobnm(sqlHdivpfRs.getString("JOBNM"));
				hdivpfnew.setDatime(sqlHdivpfRs.getDate("DATIME"));

				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHdivpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}
	
	/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction*/
	public void insertHdivData(Hdivpf hdivpf){
		
		 
        String SQL_HDIV_INSERT = "INSERT INTO HDIVPF (CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,HPUANBR,TRANNO,BATCCOY,BATCBRN,"
        		+ "BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CNTCURR,EFFDATE,HDVALDT,HDVTYP,HDVAMT,HDVRATE,HDVEFFDT,ZDIVOPT,ZCSHDIVMTH,HDVOPTTX,"
        		+ "HDVCAPTX,HINCAPDT,HDVSMTNO,USRPRF,JOBNM,DATIME)" //ILIFE-9080
        		+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
   
        PreparedStatement psHdivInsert = getPrepareStatement(SQL_HDIV_INSERT);
        try {
            
        	psHdivInsert.setString(1, hdivpf.getChdrcoy());
        	psHdivInsert.setString(2,hdivpf.getChdrnum() );
        	psHdivInsert.setString(3,hdivpf.getLife());
        	psHdivInsert.setString(4, hdivpf.getJlife());
        	psHdivInsert.setString(5,hdivpf.getCoverage());
        	psHdivInsert.setString(6, hdivpf.getRider());
        	psHdivInsert.setInt(7, hdivpf.getPlnsfx());
        	psHdivInsert.setInt(8,hdivpf.getHpuanbr());
        	psHdivInsert.setInt(9, hdivpf.getTranno() );
        	psHdivInsert.setString(10,hdivpf.getBatccoy());
        	psHdivInsert.setString(11,hdivpf.getBatcbrn());
        	psHdivInsert.setInt(12, hdivpf.getBatcactyr());
        	psHdivInsert.setInt(13,hdivpf.getBatcactmn());
        	psHdivInsert.setString(14,hdivpf.getBatctrcde());
        	psHdivInsert.setString(15, hdivpf.getBatcbatch() );
        	psHdivInsert.setString(16,hdivpf.getCntcurr());
        	psHdivInsert.setInt(17,hdivpf.getEffdate());
        	psHdivInsert.setInt(18, hdivpf.getHdvaldt());
        	psHdivInsert.setString(19,hdivpf.getHdvtyp());
        	psHdivInsert.setBigDecimal(20, hdivpf.getHdvamt() );
        	psHdivInsert.setBigDecimal(21,hdivpf.getHdvrate());
        	psHdivInsert.setInt(22,hdivpf.getHdveffdt());
        	psHdivInsert.setString(23, hdivpf.getZdivopt());
        	psHdivInsert.setString(24,hdivpf.getZcshdivmth());
        	psHdivInsert.setInt(25, hdivpf.getHdvopttx() );
        	psHdivInsert.setInt(26,hdivpf.getHdvcaptx());
        	psHdivInsert.setInt(27,hdivpf.getHincapdt());
        	psHdivInsert.setInt(28, hdivpf.getHdvsmtno());
        	psHdivInsert.setString(29, this.getUsrprf()); //ILIFE-9080
        	psHdivInsert.setString(30, this.getJobnm()); //ILIFE-9080
        	psHdivInsert.setTimestamp(31, new Timestamp(System.currentTimeMillis())); //ILIFE-9080
        	psHdivInsert.execute();
        } catch (SQLException e) {
            LOGGER.error("insertHdivData()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psHdivInsert, null);
        }
    
	}
	
	public List<Hdivpf> searchHdivdopRecord(Hdivpf hdivpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, EFFDATE, PLNSFX, RIDER, JLIFE, HDVAMT, TRANNO, ZDIVOPT ");
		//sqlSelect.append(" ");
		sqlSelect.append("FROM HDIVDOP WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		
		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());


			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();
				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getLong(1));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString(2));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString(3));
				hdivpfnew.setLife(sqlHdivpfRs.getString(4));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString(5));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt(6));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt(7));
				hdivpfnew.setRider(sqlHdivpfRs.getString(8));
				hdivpfnew.setJlife(sqlHdivpfRs.getString(9));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt(11));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString(12));	
				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHdivpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}
	
	
	@Override
	public void updateHdivdopRecord(Hdivpf hdivpf) {
		
		StringBuilder sql = new StringBuilder("UPDATE HDIVDOP SET HDVOPTTX=?");
		sql.append(" WHERE UNIQUE_NUMBER=? AND CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?");
		PreparedStatement ps = null;
		try{
			
			ps = getPrepareStatement(sql.toString());
			ps.setInt(1, hdivpf.getHdvopttx());
			ps.setLong(2, hdivpf.getUniqueNumber());
			ps.setString(3, hdivpf.getChdrcoy());
			ps.setString(4, hdivpf.getChdrnum());
			ps.setString(5, hdivpf.getLife());
			ps.setString(6, hdivpf.getCoverage());
			ps.setString(7, hdivpf.getRider());
			ps.setInt(8, hdivpf.getPlnsfx());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.error("updateHdivdopRecord()", e);	
			throw new SQLRuntimeException(e);
		} finally
		{
			close(ps,null);			
		}	

	}
		
	public void bulkUpdateHdivRecords(List<Hdivpf> hdivList) {
		StringBuilder sqlUpdate = new StringBuilder();
		sqlUpdate.append("UPDATE VM1DTA.HDIVPF set CHDRCOY=?, CHDRNUM=?, LIFE=?, JLIFE=?, COVERAGE=?, RIDER=?, PLNSFX=?, HPUANBR=?, TRANNO=?, BATCCOY=?, BATCBRN=?,");
		sqlUpdate.append(" BATCACTYR=?, BATCACTMN=?, BATCTRCDE=?, BATCBATCH=?, CNTCURR=?, EFFDATE=?, HDVALDT=?, HDVTYP=?, HDVAMT=?, HDVRATE=?, HDVEFFDT=?, ZDIVOPT=?,");
		sqlUpdate.append(" ZCSHDIVMTH=?, HDVOPTTX=?, HDVCAPTX=?, HINCAPDT=?, HDVSMTNO=?, USRPRF=?, JOBNM=?, DATIME=? "); //ILIFE-3790 by dpuhawan
		sqlUpdate.append(" WHERE UNIQUE_NUMBER=?"); //ILIFE-3790 by dpuhawan
		PreparedStatement psHdivpfUpdate = null;
		try {
			psHdivpfUpdate = getPrepareStatement(sqlUpdate.toString());
			for(Hdivpf hdiv:hdivList) {
				psHdivpfUpdate.setString(1, hdiv.getChdrcoy());
				psHdivpfUpdate.setString(2, hdiv.getChdrnum());
				psHdivpfUpdate.setString(3, hdiv.getLife());
				psHdivpfUpdate.setString(4, hdiv.getJlife());
				psHdivpfUpdate.setString(5, hdiv.getCoverage());
				psHdivpfUpdate.setString(6, hdiv.getRider());
				psHdivpfUpdate.setInt(7, hdiv.getPlnsfx());
				psHdivpfUpdate.setInt(8, hdiv.getHpuanbr());
				psHdivpfUpdate.setInt(9, hdiv.getTranno());
				psHdivpfUpdate.setString(10, hdiv.getBatccoy());
				psHdivpfUpdate.setString(11, hdiv.getBatcbrn());
				psHdivpfUpdate.setInt(12, hdiv.getBatcactyr());
				psHdivpfUpdate.setInt(13, hdiv.getBatcactmn());
				psHdivpfUpdate.setString(14, hdiv.getBatctrcde());
				psHdivpfUpdate.setString(15, hdiv.getBatcbatch());
				psHdivpfUpdate.setString(16, hdiv.getCntcurr());
				psHdivpfUpdate.setInt(17, hdiv.getEffdate());
				psHdivpfUpdate.setInt(18, hdiv.getHdvaldt());
				psHdivpfUpdate.setString(19, hdiv.getHdvtyp());
				psHdivpfUpdate.setBigDecimal(20, hdiv.getHdvamt());
				psHdivpfUpdate.setBigDecimal(21, hdiv.getHdvrate());
				psHdivpfUpdate.setInt(22, hdiv.getHdveffdt());
				psHdivpfUpdate.setString(23, hdiv.getZdivopt());
				psHdivpfUpdate.setString(24, hdiv.getZcshdivmth());
				psHdivpfUpdate.setInt(25, hdiv.getHdvopttx());
				psHdivpfUpdate.setInt(26, hdiv.getHdvcaptx());
				psHdivpfUpdate.setInt(27, hdiv.getHincapdt());
				psHdivpfUpdate.setInt(28, hdiv.getHdvsmtno());
				psHdivpfUpdate.setString(29, hdiv.getUsrprf());
				psHdivpfUpdate.setString(30, hdiv.getJobnm());
				//ILIFE-3790 Start by dpuhawan
				//psHdivpfUpdate.setString(31, "GETDATE()");
				psHdivpfUpdate.setTimestamp(31, new Timestamp(System.currentTimeMillis()));
				psHdivpfUpdate.setLong(32,hdiv.getUniqueNumber());
				//ILIFE-3790 end
				psHdivpfUpdate.addBatch();
			}
			psHdivpfUpdate.executeBatch();
		}
		catch (SQLException e) {
			LOGGER.error("bulkUpdateHdivRecords()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfUpdate, null);
		}
	}
	public List<Hdivpf> getHdivcshRecords(Hdivpf hdivpf) {
		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, ");
		sqlSelect.append("BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ");
		sqlSelect.append("CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, ");
		sqlSelect.append("HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, ");
		sqlSelect.append("HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM HDIVPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND HINCAPDT = ? AND HDVTYP = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, HINCAPDT ASC, UNIQUE_NUMBER DESC");
		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());
			psHdivpfSelect.setString(3, hdivpf.getLife());
			psHdivpfSelect.setString(4, hdivpf.getCoverage());
			psHdivpfSelect.setString(5, hdivpf.getRider());
			psHdivpfSelect.setInt(6, hdivpf.getPlnsfx());
			psHdivpfSelect.setInt(7, hdivpf.getHincapdt());
			psHdivpfSelect.setString(8, "C");
			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();

				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getInt("UNIQUE_NUMBER"));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString("CHDRCOY"));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString("CHDRNUM"));
				hdivpfnew.setLife(sqlHdivpfRs.getString("LIFE"));
				hdivpfnew.setJlife(sqlHdivpfRs.getString("JLIFE"));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString("COVERAGE"));
				hdivpfnew.setRider(sqlHdivpfRs.getString("RIDER"));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt("PLNSFX"));
				hdivpfnew.setHpuanbr(sqlHdivpfRs.getInt("HPUANBR"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt("TRANNO"));
				hdivpfnew.setBatccoy(sqlHdivpfRs.getString("BATCCOY"));
				hdivpfnew.setBatcbrn(sqlHdivpfRs.getString("BATCBRN"));
				hdivpfnew.setBatcactyr(sqlHdivpfRs.getInt("BATCACTYR"));
				hdivpfnew.setBatcactmn(sqlHdivpfRs.getInt("BATCACTMN"));
				hdivpfnew.setBatctrcde(sqlHdivpfRs.getString("BATCTRCDE"));
				hdivpfnew.setBatcbatch(sqlHdivpfRs.getString("BATCBATCH"));
				hdivpfnew.setCntcurr(sqlHdivpfRs.getString("CNTCURR"));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt("EFFDATE"));
				hdivpfnew.setHdvaldt(sqlHdivpfRs.getInt("HDVALDT"));
				hdivpfnew.setHdvtyp(sqlHdivpfRs.getString("HDVTYP"));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setHdvrate(sqlHdivpfRs.getBigDecimal("HDVRATE"));
				hdivpfnew.setHdveffdt(sqlHdivpfRs.getInt("HDVEFFDT"));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString("ZDIVOPT"));
				hdivpfnew.setZcshdivmth(sqlHdivpfRs.getString("ZCSHDIVMTH"));
				hdivpfnew.setHdvopttx(sqlHdivpfRs.getInt("HDVOPTTX"));
				hdivpfnew.setHdvcaptx(sqlHdivpfRs.getInt("HDVCAPTX"));
				hdivpfnew.setHincapdt(sqlHdivpfRs.getInt("HINCAPDT"));
				hdivpfnew.setHdvsmtno(sqlHdivpfRs.getInt("HDVSMTNO"));
				hdivpfnew.setUsrprf(sqlHdivpfRs.getString("USRPRF"));
				hdivpfnew.setJobnm(sqlHdivpfRs.getString("JOBNM"));
				hdivpfnew.setDatime(sqlHdivpfRs.getDate("DATIME"));

				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("getHdivcshRecords()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}
	
	public List<Hdivpf> searchHdivcshRecord(Hdivpf hdivpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, ");
		sqlSelect.append("BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ");
		sqlSelect.append("CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, ");
		sqlSelect.append("HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, ");
		sqlSelect.append("HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM HDIVPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, HINCAPDT ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());
			psHdivpfSelect.setString(3, hdivpf.getLife());
			psHdivpfSelect.setString(4, hdivpf.getCoverage());
			psHdivpfSelect.setString(5, hdivpf.getRider());
			psHdivpfSelect.setInt(6, hdivpf.getPlnsfx());
		//	psHdivpfSelect.setInt(7, hdivpf.getHincapdt());

			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();

				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getInt("UNIQUE_NUMBER"));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString("CHDRCOY"));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString("CHDRNUM"));
				hdivpfnew.setLife(sqlHdivpfRs.getString("LIFE"));
				hdivpfnew.setJlife(sqlHdivpfRs.getString("JLIFE"));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString("COVERAGE"));
				hdivpfnew.setRider(sqlHdivpfRs.getString("RIDER"));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt("PLNSFX"));
				hdivpfnew.setHpuanbr(sqlHdivpfRs.getInt("HPUANBR"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt("TRANNO"));
				hdivpfnew.setBatccoy(sqlHdivpfRs.getString("BATCCOY"));
				hdivpfnew.setBatcbrn(sqlHdivpfRs.getString("BATCBRN"));
				hdivpfnew.setBatcactyr(sqlHdivpfRs.getInt("BATCACTYR"));
				hdivpfnew.setBatcactmn(sqlHdivpfRs.getInt("BATCACTMN"));
				hdivpfnew.setBatctrcde(sqlHdivpfRs.getString("BATCTRCDE"));
				hdivpfnew.setBatcbatch(sqlHdivpfRs.getString("BATCBATCH"));
				hdivpfnew.setCntcurr(sqlHdivpfRs.getString("CNTCURR"));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt("EFFDATE"));
				hdivpfnew.setHdvaldt(sqlHdivpfRs.getInt("HDVALDT"));
				hdivpfnew.setHdvtyp(sqlHdivpfRs.getString("HDVTYP"));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setHdvrate(sqlHdivpfRs.getBigDecimal("HDVRATE"));
				hdivpfnew.setHdveffdt(sqlHdivpfRs.getInt("HDVEFFDT"));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString("ZDIVOPT"));
				hdivpfnew.setZcshdivmth(sqlHdivpfRs.getString("ZCSHDIVMTH"));
				hdivpfnew.setHdvopttx(sqlHdivpfRs.getInt("HDVOPTTX"));
				hdivpfnew.setHdvcaptx(sqlHdivpfRs.getInt("HDVCAPTX"));
				hdivpfnew.setHincapdt(sqlHdivpfRs.getInt("HINCAPDT"));
				hdivpfnew.setHdvsmtno(sqlHdivpfRs.getInt("HDVSMTNO"));
				hdivpfnew.setUsrprf(sqlHdivpfRs.getString("USRPRF"));
				hdivpfnew.setJobnm(sqlHdivpfRs.getString("JOBNM"));
				hdivpfnew.setDatime(sqlHdivpfRs.getDate("DATIME"));

				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHdivpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}
	//ILIFE-3790 Start by dpuhawan
	public List<Hdivpf> searchHdivintRecord(Hdivpf hdivpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, ");
		sqlSelect.append("BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ");
		sqlSelect.append("CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, ");
		sqlSelect.append("HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, ");
		sqlSelect.append("HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM HDIVPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND HINCAPDT = ? AND HDVTYP = 'I' AND HDVCAPTX = 0");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, HINCAPDT ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());
			psHdivpfSelect.setString(3, hdivpf.getLife());
			psHdivpfSelect.setString(4, hdivpf.getCoverage());
			psHdivpfSelect.setString(5, hdivpf.getRider());
			psHdivpfSelect.setInt(6, hdivpf.getPlnsfx());
			psHdivpfSelect.setInt(7, hdivpf.getHincapdt());

			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();

				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getInt("UNIQUE_NUMBER"));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString("CHDRCOY"));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString("CHDRNUM"));
				hdivpfnew.setLife(sqlHdivpfRs.getString("LIFE"));
				hdivpfnew.setJlife(sqlHdivpfRs.getString("JLIFE"));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString("COVERAGE"));
				hdivpfnew.setRider(sqlHdivpfRs.getString("RIDER"));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt("PLNSFX"));
				hdivpfnew.setHpuanbr(sqlHdivpfRs.getInt("HPUANBR"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt("TRANNO"));
				hdivpfnew.setBatccoy(sqlHdivpfRs.getString("BATCCOY"));
				hdivpfnew.setBatcbrn(sqlHdivpfRs.getString("BATCBRN"));
				hdivpfnew.setBatcactyr(sqlHdivpfRs.getInt("BATCACTYR"));
				hdivpfnew.setBatcactmn(sqlHdivpfRs.getInt("BATCACTMN"));
				hdivpfnew.setBatctrcde(sqlHdivpfRs.getString("BATCTRCDE"));
				hdivpfnew.setBatcbatch(sqlHdivpfRs.getString("BATCBATCH"));
				hdivpfnew.setCntcurr(sqlHdivpfRs.getString("CNTCURR"));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt("EFFDATE"));
				hdivpfnew.setHdvaldt(sqlHdivpfRs.getInt("HDVALDT"));
				hdivpfnew.setHdvtyp(sqlHdivpfRs.getString("HDVTYP"));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setHdvrate(sqlHdivpfRs.getBigDecimal("HDVRATE"));
				hdivpfnew.setHdveffdt(sqlHdivpfRs.getInt("HDVEFFDT"));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString("ZDIVOPT"));
				hdivpfnew.setZcshdivmth(sqlHdivpfRs.getString("ZCSHDIVMTH"));
				hdivpfnew.setHdvopttx(sqlHdivpfRs.getInt("HDVOPTTX"));
				hdivpfnew.setHdvcaptx(sqlHdivpfRs.getInt("HDVCAPTX"));
				hdivpfnew.setHincapdt(sqlHdivpfRs.getInt("HINCAPDT"));
				hdivpfnew.setHdvsmtno(sqlHdivpfRs.getInt("HDVSMTNO"));
				hdivpfnew.setUsrprf(sqlHdivpfRs.getString("USRPRF"));
				hdivpfnew.setJobnm(sqlHdivpfRs.getString("JOBNM"));
				hdivpfnew.setDatime(sqlHdivpfRs.getDate("DATIME"));

				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHdivintRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}	
	
	//ILIFE-3790 End

	public boolean insertHdivpf(List<Hdivpf> hdivpfList) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO VM1DTA.HDIVPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, HPUANBR, "
				+ "TRANNO, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, CNTCURR, EFFDATE, HDVALDT,"
				+ " HDVTYP, HDVAMT, HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, DATIME)  "); 
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			int seq;
            for (Hdivpf hdiv : hdivpfList) {		
            	seq=0;
				ps.setString(++seq, hdiv.getChdrcoy());
			    ps.setString(++seq, hdiv.getChdrnum());
			    ps.setString(++seq, hdiv.getLife());
			    ps.setString(++seq, hdiv.getJlife());			    
				ps.setString(++seq, hdiv.getCoverage());
				ps.setString(++seq, hdiv.getRider());				
			    ps.setInt(++seq, hdiv.getPlnsfx());
			    ps.setInt(++seq, hdiv.getHpuanbr()); 
				ps.setInt(++seq, hdiv.getTranno());				
			    ps.setString(++seq, hdiv.getBatccoy());		
				ps.setString(++seq, hdiv.getBatcbrn());		
			    ps.setInt(++seq, hdiv.getBatcactyr());
			    ps.setInt(++seq, hdiv.getBatcactmn());			    
			    ps.setString(++seq, hdiv.getBatctrcde());		
			    ps.setString(++seq, hdiv.getBatcbatch());
			    ps.setString(++seq, hdiv.getCntcurr());
			    ps.setInt(++seq, hdiv.getEffdate());
			    ps.setInt(++seq, hdiv.getHdvaldt());
			    ps.setString(++seq, hdiv.getHdvtyp());
			    ps.setBigDecimal(++seq, hdiv.getHdvamt());
			    ps.setBigDecimal(++seq, hdiv.getHdvrate());
			    ps.setInt(++seq, hdiv.getHdveffdt());
			    ps.setString(++seq, hdiv.getZdivopt());
			    ps.setString(++seq, hdiv.getZcshdivmth());
			    ps.setInt(++seq, hdiv.getHdvopttx());
			    ps.setInt(++seq, hdiv.getHdvcaptx());
			    ps.setInt(++seq, hdiv.getHincapdt());
			    ps.setInt(++seq, hdiv.getHdvsmtno()); 
			    ps.setString(++seq, this.getUsrprf());
			    ps.setString(++seq, this.getJobnm());
			    ps.setTimestamp(++seq, new Timestamp(System.currentTimeMillis()));		    		    			    		    		    
			    ps.addBatch();
            }		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("insertHdivPF()", e);	
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return isInsertSuccessful;
	}			
	
    public Map<String, List<Hdivpf>> getHdivcshMap(String coy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, HPUANBR, "
        		+ "TRANNO, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, CNTCURR, EFFDATE, "
        		+ "HDVALDT, HDVTYP, HDVAMT, HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX,"
        		+ " HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, DATIME  ");
        sb.append("FROM VM1DTA.HDIVPF WHERE HDVTYP = 'C' AND CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, HINCAPDT, UNIQUE_NUMBER DESC ");            
  //      LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Hdivpf>> hdivcshMap = new HashMap<String, List<Hdivpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
            	Hdivpf hdivpf = new Hdivpf();
                hdivpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                hdivpf.setChdrcoy(rs.getString("CHDRCOY"));
                hdivpf.setChdrnum(rs.getString("CHDRNUM"));
                hdivpf.setLife(rs.getString("LIFE"));                
                hdivpf.setJlife(rs.getString("JLIFE"));                
                hdivpf.setCoverage(rs.getString("COVERAGE"));
                hdivpf.setRider(rs.getString("RIDER"));        
                hdivpf.setPlnsfx(rs.getInt("PLNSFX"));
                hdivpf.setHpuanbr(rs.getInt("HPUANBR"));
                hdivpf.setTranno(rs.getInt("TRANNO"));
                hdivpf.setBatccoy(rs.getString("BATCCOY"));
                hdivpf.setBatcbrn(rs.getString("BATCBRN"));
                hdivpf.setBatcactyr(rs.getInt("BATCACTYR"));
                hdivpf.setBatcactmn(rs.getInt("BATCACTMN"));
                hdivpf.setBatctrcde(rs.getString("BATCTRCDE")); 
                hdivpf.setBatcbatch(rs.getString("BATCBATCH"));
                hdivpf.setCntcurr(rs.getString("CNTCURR"));
                hdivpf.setEffdate(rs.getInt("EFFDATE"));
                hdivpf.setHdvaldt(rs.getInt("HDVALDT"));
                hdivpf.setHdvtyp(rs.getString("HDVTYP"));
                hdivpf.setHdvamt(rs.getBigDecimal("HDVAMT"));
                hdivpf.setHdvrate(rs.getBigDecimal("HDVRATE"));
                hdivpf.setHdveffdt(rs.getInt("HDVEFFDT"));
                hdivpf.setZdivopt(rs.getString("ZDIVOPT"));
                hdivpf.setZcshdivmth(rs.getString("ZCSHDIVMTH"));       
                hdivpf.setHdvopttx(rs.getInt("HDVOPTTX"));
                hdivpf.setHdvcaptx(rs.getInt("HDVCAPTX"));
                hdivpf.setHincapdt(rs.getInt("HINCAPDT"));
                hdivpf.setHdvsmtno(rs.getInt("HDVSMTNO"));
				hdivpf.setUsrprf(rs.getString("USRPRF"));
				hdivpf.setJobnm(rs.getString("JOBNM"));                
				hdivpf.setDatime(rs.getDate("DATIME"));                 

                if (hdivcshMap.containsKey(hdivpf.getChdrnum())) {
                	hdivcshMap.get(hdivpf.getChdrnum()).add(hdivpf);
                } else {
                    List<Hdivpf> hdivcshList = new ArrayList<Hdivpf>();
                    hdivcshList.add(hdivpf);
                    hdivcshMap.put(hdivpf.getChdrnum(), hdivcshList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("getHdivcshMap()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return hdivcshMap;

    }	
    
    public void deleteHdivpfBasedOnTranno(Hdivpf hdivpf){
    	StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM VM1DTA.HDIVPF WHERE CHDRCOY=? AND CHDRNUM=? AND BATCTRCDE=?");
        LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        try {
            ps.setInt(1, Integer.parseInt(hdivpf.getChdrcoy()));
		    ps.setString(2, hdivpf.getChdrnum());
		    ps.setString(3, hdivpf.getBatctrcde());
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("deleteHdivpfBasedOnTranno()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    }
    
    public List<Hdivpf> getHdivintRecord(Hdivpf hdivpf){

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, ");
		sqlSelect.append("BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ");
		sqlSelect.append("CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, ");
		sqlSelect.append("HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, ");
		sqlSelect.append("HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM HDIVPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND BATCTRCDE=? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, HINCAPDT ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());
			psHdivpfSelect.setString(3, hdivpf.getBatctrcde());

			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();

				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getInt("UNIQUE_NUMBER"));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString("CHDRCOY"));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString("CHDRNUM"));
				hdivpfnew.setLife(sqlHdivpfRs.getString("LIFE"));
				hdivpfnew.setJlife(sqlHdivpfRs.getString("JLIFE"));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString("COVERAGE"));
				hdivpfnew.setRider(sqlHdivpfRs.getString("RIDER"));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt("PLNSFX"));
				hdivpfnew.setHpuanbr(sqlHdivpfRs.getInt("HPUANBR"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt("TRANNO"));
				hdivpfnew.setBatccoy(sqlHdivpfRs.getString("BATCCOY"));
				hdivpfnew.setBatcbrn(sqlHdivpfRs.getString("BATCBRN"));
				hdivpfnew.setBatcactyr(sqlHdivpfRs.getInt("BATCACTYR"));
				hdivpfnew.setBatcactmn(sqlHdivpfRs.getInt("BATCACTMN"));
				hdivpfnew.setBatctrcde(sqlHdivpfRs.getString("BATCTRCDE"));
				hdivpfnew.setBatcbatch(sqlHdivpfRs.getString("BATCBATCH"));
				hdivpfnew.setCntcurr(sqlHdivpfRs.getString("CNTCURR"));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt("EFFDATE"));
				hdivpfnew.setHdvaldt(sqlHdivpfRs.getInt("HDVALDT"));
				hdivpfnew.setHdvtyp(sqlHdivpfRs.getString("HDVTYP"));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setHdvrate(sqlHdivpfRs.getBigDecimal("HDVRATE"));
				hdivpfnew.setHdveffdt(sqlHdivpfRs.getInt("HDVEFFDT"));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString("ZDIVOPT"));
				hdivpfnew.setZcshdivmth(sqlHdivpfRs.getString("ZCSHDIVMTH"));
				hdivpfnew.setHdvopttx(sqlHdivpfRs.getInt("HDVOPTTX"));
				hdivpfnew.setHdvcaptx(sqlHdivpfRs.getInt("HDVCAPTX"));
				hdivpfnew.setHincapdt(sqlHdivpfRs.getInt("HINCAPDT"));
				hdivpfnew.setHdvsmtno(sqlHdivpfRs.getInt("HDVSMTNO"));
				hdivpfnew.setUsrprf(sqlHdivpfRs.getString("USRPRF"));
				hdivpfnew.setJobnm(sqlHdivpfRs.getString("JOBNM"));
				hdivpfnew.setDatime(sqlHdivpfRs.getDate("DATIME"));

				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("getHdivintRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}
    
    @Override
	public void updateHdivRecord(Hdivpf hdivpf) {
		
		StringBuilder sql = new StringBuilder("UPDATE HDIVPF SET Tranno=? , HDVAMT=? ");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND UNIQUE_NUMBER=? ");
		PreparedStatement ps = null;
		try{
			
			ps = getPrepareStatement(sql.toString());
			ps.setInt(1, hdivpf.getTranno());
			ps.setBigDecimal(2, hdivpf.getHdvamt());
			ps.setString(3, hdivpf.getChdrcoy());
			ps.setString(4, hdivpf.getChdrnum());
			ps.setString(5, hdivpf.getLife());
			ps.setString(6, hdivpf.getCoverage());
			ps.setString(7, hdivpf.getRider());
			ps.setInt(8, hdivpf.getPlnsfx());
			ps.setLong(9, hdivpf.getUniqueNumber());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.error("updateHdivRecord()", e);	
			throw new SQLRuntimeException(e);
		} finally
		{
			close(ps,null);			
		}	

	}

	@Override
	public List<Hdivpf> getHdivdopRecord(Hdivpf hdivpf) {
		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY,"
				+ " BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, HDVRATE, HDVEFFDT,"
				+ "ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, DATIME ");
		//sqlSelect.append(" ");
		sqlSelect.append("FROM HDIVDOP WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND  HDVTYP = 'D' AND HDVOPTTX = 0 ");
		sqlSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		
		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());
			psHdivpfSelect.setString(3, hdivpf.getLife());
			psHdivpfSelect.setString(4, hdivpf.getCoverage());
			psHdivpfSelect.setString(5, hdivpf.getRider());
			psHdivpfSelect.setInt(6, hdivpf.getPlnsfx());


			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();
				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getInt("UNIQUE_NUMBER"));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString("CHDRCOY"));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString("CHDRNUM"));
				hdivpfnew.setLife(sqlHdivpfRs.getString("LIFE"));
				hdivpfnew.setJlife(sqlHdivpfRs.getString("JLIFE"));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString("COVERAGE"));
				hdivpfnew.setRider(sqlHdivpfRs.getString("RIDER"));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt("PLNSFX"));
				hdivpfnew.setHpuanbr(sqlHdivpfRs.getInt("HPUANBR"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt("TRANNO"));
				hdivpfnew.setBatccoy(sqlHdivpfRs.getString("BATCCOY"));
				hdivpfnew.setBatcbrn(sqlHdivpfRs.getString("BATCBRN"));
				hdivpfnew.setBatcactyr(sqlHdivpfRs.getInt("BATCACTYR"));
				hdivpfnew.setBatcactmn(sqlHdivpfRs.getInt("BATCACTMN"));
				hdivpfnew.setBatctrcde(sqlHdivpfRs.getString("BATCTRCDE"));
				hdivpfnew.setBatcbatch(sqlHdivpfRs.getString("BATCBATCH"));
				hdivpfnew.setCntcurr(sqlHdivpfRs.getString("CNTCURR"));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt("EFFDATE"));
				hdivpfnew.setHdvaldt(sqlHdivpfRs.getInt("HDVALDT"));
				hdivpfnew.setHdvtyp(sqlHdivpfRs.getString("HDVTYP"));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setHdvrate(sqlHdivpfRs.getBigDecimal("HDVRATE"));
				hdivpfnew.setHdveffdt(sqlHdivpfRs.getInt("HDVEFFDT"));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString("ZDIVOPT"));
				hdivpfnew.setZcshdivmth(sqlHdivpfRs.getString("ZCSHDIVMTH"));
				hdivpfnew.setHdvopttx(sqlHdivpfRs.getInt("HDVOPTTX"));
				hdivpfnew.setHdvcaptx(sqlHdivpfRs.getInt("HDVCAPTX"));
				hdivpfnew.setHincapdt(sqlHdivpfRs.getInt("HINCAPDT"));
				hdivpfnew.setHdvsmtno(sqlHdivpfRs.getInt("HDVSMTNO"));
				hdivpfnew.setUsrprf(sqlHdivpfRs.getString("USRPRF"));
				hdivpfnew.setJobnm(sqlHdivpfRs.getString("JOBNM"));
				hdivpfnew.setDatime(sqlHdivpfRs.getDate("DATIME"));
				
				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("getHdivdopRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}
	
	public List<Hdivpf> searchHdivdop(Hdivpf hdivpf) {

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, ");
		sqlSelect.append("BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ");
		sqlSelect.append("CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, ");
		sqlSelect.append("HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, ");
		sqlSelect.append("HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM HDIVPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC,UNIQUE_NUMBER DESC");


		PreparedStatement psHdivpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdivpfRs = null;
		List<Hdivpf> outputList = new ArrayList<Hdivpf>();

		try {

			psHdivpfSelect.setString(1, hdivpf.getChdrcoy());
			psHdivpfSelect.setString(2, hdivpf.getChdrnum());
			psHdivpfSelect.setString(3, hdivpf.getLife());
			psHdivpfSelect.setString(4, hdivpf.getCoverage());
			psHdivpfSelect.setString(5, hdivpf.getRider());
			psHdivpfSelect.setInt(6, hdivpf.getPlnsfx());

			sqlHdivpfRs = executeQuery(psHdivpfSelect);

			while (sqlHdivpfRs.next()) {

				Hdivpf hdivpfnew = new Hdivpf();

				hdivpfnew.setUniqueNumber(sqlHdivpfRs.getInt("UNIQUE_NUMBER"));
				hdivpfnew.setChdrcoy(sqlHdivpfRs.getString("CHDRCOY"));
				hdivpfnew.setChdrnum(sqlHdivpfRs.getString("CHDRNUM"));
				hdivpfnew.setLife(sqlHdivpfRs.getString("LIFE"));
				hdivpfnew.setJlife(sqlHdivpfRs.getString("JLIFE"));
				hdivpfnew.setCoverage(sqlHdivpfRs.getString("COVERAGE"));
				hdivpfnew.setRider(sqlHdivpfRs.getString("RIDER"));
				hdivpfnew.setPlnsfx(sqlHdivpfRs.getInt("PLNSFX"));
				hdivpfnew.setHpuanbr(sqlHdivpfRs.getInt("HPUANBR"));
				hdivpfnew.setTranno(sqlHdivpfRs.getInt("TRANNO"));
				hdivpfnew.setBatccoy(sqlHdivpfRs.getString("BATCCOY"));
				hdivpfnew.setBatcbrn(sqlHdivpfRs.getString("BATCBRN"));
				hdivpfnew.setBatcactyr(sqlHdivpfRs.getInt("BATCACTYR"));
				hdivpfnew.setBatcactmn(sqlHdivpfRs.getInt("BATCACTMN"));
				hdivpfnew.setBatctrcde(sqlHdivpfRs.getString("BATCTRCDE"));
				hdivpfnew.setBatcbatch(sqlHdivpfRs.getString("BATCBATCH"));
				hdivpfnew.setCntcurr(sqlHdivpfRs.getString("CNTCURR"));
				hdivpfnew.setEffdate(sqlHdivpfRs.getInt("EFFDATE"));
				hdivpfnew.setHdvaldt(sqlHdivpfRs.getInt("HDVALDT"));
				hdivpfnew.setHdvtyp(sqlHdivpfRs.getString("HDVTYP"));
				hdivpfnew.setHdvamt(sqlHdivpfRs.getBigDecimal("HDVAMT"));
				hdivpfnew.setHdvrate(sqlHdivpfRs.getBigDecimal("HDVRATE"));
				hdivpfnew.setHdveffdt(sqlHdivpfRs.getInt("HDVEFFDT"));
				hdivpfnew.setZdivopt(sqlHdivpfRs.getString("ZDIVOPT"));
				hdivpfnew.setZcshdivmth(sqlHdivpfRs.getString("ZCSHDIVMTH"));
				hdivpfnew.setHdvopttx(sqlHdivpfRs.getInt("HDVOPTTX"));
				hdivpfnew.setHdvcaptx(sqlHdivpfRs.getInt("HDVCAPTX"));
				hdivpfnew.setHincapdt(sqlHdivpfRs.getInt("HINCAPDT"));
				hdivpfnew.setHdvsmtno(sqlHdivpfRs.getInt("HDVSMTNO"));
				hdivpfnew.setUsrprf(sqlHdivpfRs.getString("USRPRF"));
				hdivpfnew.setJobnm(sqlHdivpfRs.getString("JOBNM"));
				hdivpfnew.setDatime(sqlHdivpfRs.getDate("DATIME"));

				outputList.add(hdivpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHdivdop()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdivpfSelect, sqlHdivpfRs);
		}

		return outputList;
	}
 //ILIFE-9080 start
	@Override
	public Map<String, List<Hdivpf>> getHdivMap(String coy, List<String> chdrnumList) {
		StringBuilder sb = new StringBuilder();
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, HPUANBR, TRANNO, "
        		+ "BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, CNTCURR, EFFDATE, HDVALDT, HDVTYP, "
        		+ "HDVAMT, HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, DATIME  ");
        sb.append("FROM VM1DTA.HDIVPF WHERE CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, HINCAPDT, UNIQUE_NUMBER DESC ");            
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Hdivpf>> hdivMap = new HashMap<String, List<Hdivpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
            	Hdivpf hdivpf = new Hdivpf();
                hdivpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                hdivpf.setChdrcoy(rs.getString("CHDRCOY"));
                hdivpf.setChdrnum(rs.getString("CHDRNUM"));
                hdivpf.setLife(rs.getString("LIFE"));                
                hdivpf.setJlife(rs.getString("JLIFE"));                
                hdivpf.setCoverage(rs.getString("COVERAGE"));
                hdivpf.setRider(rs.getString("RIDER"));        
                hdivpf.setPlnsfx(rs.getInt("PLNSFX"));
                hdivpf.setHpuanbr(rs.getInt("HPUANBR"));
                hdivpf.setTranno(rs.getInt("TRANNO"));
                hdivpf.setBatccoy(rs.getString("BATCCOY"));
                hdivpf.setBatcbrn(rs.getString("BATCBRN"));
                hdivpf.setBatcactyr(rs.getInt("BATCACTYR"));
                hdivpf.setBatcactmn(rs.getInt("BATCACTMN"));
                hdivpf.setBatctrcde(rs.getString("BATCTRCDE")); 
                hdivpf.setBatcbatch(rs.getString("BATCBATCH"));
                hdivpf.setCntcurr(rs.getString("CNTCURR"));
                hdivpf.setEffdate(rs.getInt("EFFDATE"));
                hdivpf.setHdvaldt(rs.getInt("HDVALDT"));
                hdivpf.setHdvtyp(rs.getString("HDVTYP"));
                hdivpf.setHdvamt(rs.getBigDecimal("HDVAMT"));
                hdivpf.setHdvrate(rs.getBigDecimal("HDVRATE"));
                hdivpf.setHdveffdt(rs.getInt("HDVEFFDT"));
                hdivpf.setZdivopt(rs.getString("ZDIVOPT"));
                hdivpf.setZcshdivmth(rs.getString("ZCSHDIVMTH"));       
                hdivpf.setHdvopttx(rs.getInt("HDVOPTTX"));
                hdivpf.setHdvcaptx(rs.getInt("HDVCAPTX"));
                hdivpf.setHincapdt(rs.getInt("HINCAPDT"));
                hdivpf.setHdvsmtno(rs.getInt("HDVSMTNO"));
				hdivpf.setUsrprf(rs.getString("USRPRF"));
				hdivpf.setJobnm(rs.getString("JOBNM"));                
				hdivpf.setDatime(rs.getDate("DATIME"));                 

                if (hdivMap.containsKey(hdivpf.getChdrnum())) {
                	hdivMap.get(hdivpf.getChdrnum()).add(hdivpf);
                } else {
                    List<Hdivpf> hdivList = new ArrayList<Hdivpf>();
                    hdivList.add(hdivpf);
                    hdivMap.put(hdivpf.getChdrnum(), hdivList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("getHdivMap()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return hdivMap;
 //ILIFE-9080 end
	}	
}
