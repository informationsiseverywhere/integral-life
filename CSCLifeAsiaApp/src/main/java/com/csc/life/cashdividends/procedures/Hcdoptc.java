/*
 * File: Hcdoptc.java
 * Date: 29 August 2009 22:51:40
 * Author: Quipoz Limited
 *
 * Class transformed from HCDOPTC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.dataaccess.HcsdmjaTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.newbusiness.tablestructures.Th535rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* OVERVIEW
* ========
*
* This program is accessed via P5132AT via T5671 during component
* change processing.  It is responsible determining ths change
* and call the appropriate subroutine obtained from TH535 to
* carry out the processing.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hcdoptc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaNewOption = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);
		/* ERRORS */
	private String hl21 = "HL21";
		/* FORMATS */
	private String hcsdmjarec = "HCSDMJAREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String th535 = "TH535";
		/*Cash Dividend Details - Major Change*/
	private HcsdmjaTableDAM hcsdmjaIO = new HcsdmjaTableDAM();
	private Isuallrec isuallrec = new Isuallrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Th535rec th535rec = new Th535rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		noHcsd0001,
		exit602
	}

	public Hcdoptc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					starts0000();
				}
				case noHcsd0001: {
					noHcsd0001();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void starts0000()
	{
		isuallrec.statuz.set(varcom.oK);
		initialize1000();
		if (isEQ(hcsdmjaIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.noHcsd0001);
		}
		wsaaNewOption.set(hcsdmjaIO.getZdivopt());
		wsaaNewTranno.set(hcsdmjaIO.getTranno());
		readNextHcsd2000();
		if (isEQ(hcsdmjaIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.noHcsd0001);
		}
		if (isEQ(hcsdmjaIO.getZdivopt(),wsaaNewOption)) {
			goTo(GotoLabel.noHcsd0001);
		}
		findOptchgSbr3000();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.noHcsd0001);
		}
		th535rec.th535Rec.set(itemIO.getGenarea());
		if (isEQ(th535rec.optionChange,SPACES)) {
			isuallrec.statuz.set(hl21);
			fatalError600();
		}
		isuallrec.newTranno.set(wsaaNewTranno);
		callProgram(th535rec.optionChange, isuallrec.isuallRec);
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.params.set(isuallrec.isuallRec);
			syserrrec.statuz.set(isuallrec.statuz);
			fatalError600();
		}
		isuallrec.newTranno.set(ZERO);
	}

protected void noHcsd0001()
	{
		exitProgram();
	}

protected void initialize1000()
	{
		starts1000();
	}

protected void starts1000()
	{
		hcsdmjaIO.setDataArea(SPACES);
		hcsdmjaIO.setChdrnum(isuallrec.chdrnum);
		hcsdmjaIO.setChdrcoy(isuallrec.company);
		hcsdmjaIO.setLife(isuallrec.life);
		hcsdmjaIO.setCoverage(isuallrec.coverage);
		hcsdmjaIO.setRider(isuallrec.rider);
		hcsdmjaIO.setPlanSuffix(isuallrec.planSuffix);
		hcsdmjaIO.setFormat(hcsdmjarec);
		hcsdmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hcsdmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, hcsdmjaIO);
		if (isNE(hcsdmjaIO.getStatuz(),varcom.oK)
		&& isNE(hcsdmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hcsdmjaIO.getParams());
			syserrrec.statuz.set(hcsdmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(hcsdmjaIO.getChdrcoy(),isuallrec.company)
		|| isNE(hcsdmjaIO.getChdrnum(),isuallrec.chdrnum)
		|| isNE(hcsdmjaIO.getLife(),isuallrec.life)
		|| isNE(hcsdmjaIO.getCoverage(),isuallrec.coverage)
		|| isNE(hcsdmjaIO.getRider(),isuallrec.rider)
		|| isNE(hcsdmjaIO.getPlanSuffix(),isuallrec.planSuffix)) {
			hcsdmjaIO.setStatuz(varcom.endp);
		}
	}

protected void readNextHcsd2000()
	{
		/*STARTS*/
		hcsdmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hcsdmjaIO);
		if (isNE(hcsdmjaIO.getStatuz(),varcom.oK)
		&& isNE(hcsdmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hcsdmjaIO.getParams());
			syserrrec.statuz.set(hcsdmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(hcsdmjaIO.getChdrcoy(),isuallrec.company)
		|| isNE(hcsdmjaIO.getChdrnum(),isuallrec.chdrnum)
		|| isNE(hcsdmjaIO.getLife(),isuallrec.life)
		|| isNE(hcsdmjaIO.getCoverage(),isuallrec.coverage)
		|| isNE(hcsdmjaIO.getRider(),isuallrec.rider)
		|| isNE(hcsdmjaIO.getPlanSuffix(),isuallrec.planSuffix)) {
			hcsdmjaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void findOptchgSbr3000()
	{
		starts3000();
	}

protected void starts3000()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hcsdmjaIO.getChdrcoy());
		itemIO.setItemtabl(th535);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(hcsdmjaIO.getZdivopt().toString());
		stringVariable1.append(wsaaNewOption.toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error600();
				}
				case exit602: {
					exit602();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit602);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		isuallrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
