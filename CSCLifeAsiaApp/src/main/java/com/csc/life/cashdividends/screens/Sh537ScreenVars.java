package com.csc.life.cashdividends.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH537
 * @version 1.0 generated on 30/08/09 07:03
 * @author Quipoz
 */
public class Sh537ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(803);
	public FixedLengthStringData dataFields = new FixedLengthStringData(355).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,85);
	public ZonedDecimalData lastCapDate = DD.hcapldt.copyToZonedDecimal().isAPartOf(dataFields,88);
	public ZonedDecimalData nextCapDate = DD.hcapndt.copyToZonedDecimal().isAPartOf(dataFields,96);
	public ZonedDecimalData lastDivdDate = DD.hdvldt.copyToZonedDecimal().isAPartOf(dataFields,104);
	public ZonedDecimalData divdStmtDate = DD.hdvsmtdt.copyToZonedDecimal().isAPartOf(dataFields,112);
	public ZonedDecimalData divdStmtNo = DD.hdvsmtno.copyToZonedDecimal().isAPartOf(dataFields,120);
	public ZonedDecimalData firstDivdDate = DD.hdv1stdt.copyToZonedDecimal().isAPartOf(dataFields,125);
	public ZonedDecimalData lastIntDate = DD.hintldt.copyToZonedDecimal().isAPartOf(dataFields,133);
	public ZonedDecimalData nextIntDate = DD.hintndt.copyToZonedDecimal().isAPartOf(dataFields,141);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,149);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,157);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,159);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,206);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,261);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,271);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,273);
	public FixedLengthStringData tamts = new FixedLengthStringData(72).isAPartOf(dataFields, 283);
	public ZonedDecimalData[] tamt = ZDArrayPartOfStructure(4, 18, 2, tamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(72).isAPartOf(tamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData tamt01 = DD.tamt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData tamt02 = DD.tamt.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData tamt03 = DD.tamt.copyToZonedDecimal().isAPartOf(filler,36);
	public ZonedDecimalData tamt04 = DD.tamt.copyToZonedDecimal().isAPartOf(filler,54);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(112).isAPartOf(dataArea, 355);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData hcapldtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData hcapndtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData hdvldtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData hdvsmtdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData hdvsmtnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData hdv1stdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData hintldtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData hintndtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData tamtsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData[] tamtErr = FLSArrayPartOfStructure(4, 4, tamtsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(tamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData tamt01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData tamt02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData tamt03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData tamt04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(336).isAPartOf(dataArea, 467);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] hcapldtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] hcapndtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] hdvldtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] hdvsmtdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] hdvsmtnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] hdv1stdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] hintldtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] hintndtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData tamtsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 288);
	public FixedLengthStringData[] tamtOut = FLSArrayPartOfStructure(4, 12, tamtsOut, 0);
	public FixedLengthStringData[][] tamtO = FLSDArrayPartOfArrayStructure(12, 1, tamtOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(tamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] tamt01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] tamt02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] tamt03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] tamt04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData lastCapDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextCapDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastDivdDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData divdStmtDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData firstDivdDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastIntDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextIntDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public LongData Sh537screenWritten = new LongData(0);
	public LongData Sh537protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh537ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, currcd, life, coverage, rider, crtable, occdate, rstate, pstate, cownnum, ownername, lifcnum, linsname, firstDivdDate, lastDivdDate, tamt01, lastIntDate, nextIntDate, tamt02, lastCapDate, nextCapDate, tamt03, divdStmtDate, divdStmtNo, tamt04};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, currcdOut, lifeOut, coverageOut, riderOut, crtableOut, occdateOut, rstateOut, pstateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, hdv1stdtOut, hdvldtOut, tamt01Out, hintldtOut, hintndtOut, tamt02Out, hcapldtOut, hcapndtOut, tamt03Out, hdvsmtdtOut, hdvsmtnoOut, tamt04Out};
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, currcdErr, lifeErr, coverageErr, riderErr, crtableErr, occdateErr, rstateErr, pstateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, hdv1stdtErr, hdvldtErr, tamt01Err, hintldtErr, hintndtErr, tamt02Err, hcapldtErr, hcapndtErr, tamt03Err, hdvsmtdtErr, hdvsmtnoErr, tamt04Err};
		screenDateFields = new BaseData[] {occdate, firstDivdDate, lastDivdDate, lastIntDate, nextIntDate, lastCapDate, nextCapDate, divdStmtDate};
		screenDateErrFields = new BaseData[] {occdateErr, hdv1stdtErr, hdvldtErr, hintldtErr, hintndtErr, hcapldtErr, hcapndtErr, hdvsmtdtErr};
		screenDateDispFields = new BaseData[] {occdateDisp, firstDivdDateDisp, lastDivdDateDisp, lastIntDateDisp, nextIntDateDisp, lastCapDateDisp, nextCapDateDisp, divdStmtDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh537screen.class;
		protectRecord = Sh537protect.class;
	}

}
