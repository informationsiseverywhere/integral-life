/*
 * File: Bh525.java
 * Date: 29 August 2009 21:31:05
 * Author: Quipoz Limited
 * 
 * Class transformed from BH525.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.cashdividends.dataaccess.HdpxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This batch program is a splitter program responsible for
*   extracting all HDIVs that are due for Dividend Option
*   Processing based on the Option Processed TRANNO field.
*
*   The extract records are held in the HDPXPF members created
*   in the prior process.  If a component has one or more paid up
*   addition covers which also attracts cash dividend, only one
*   extract record is written for this component as the dividend
*   option is processed at component level ie. summarised by
*   component.  The HDPXPF members are then accessed by the
*   subsequent program to drive the actual option processing.
*
*   Initialise
*     - read T5679 for the valid component statii.
*     - set contract number from and to.
*     - using SQL, extract data from COVRPF and CHDRPF.
*     - define an array to hold HDPX records.
*
*    Read
*     - fetch a block of data into an array for HDPX records.
*
*    Perform    Until End of File
*
*      Edit
*       - dummy
*
*      Update
*       - write HDPX records from array to member(s).
*
*      Read next primary file record
*
*    End Perform
*
*   Close
*     - close SQL
*     - close all opened files.
*
*   Control totals:
*     01 - No. of threads used
*     02 - no. of records extracted
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*
***********************************************************************
* </pre>
*/
public class Bh525 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhdpxpf1rs;
	private java.sql.PreparedStatement sqlhdpxpf1ps;
	private java.sql.Connection sqlhdpxpf1conn;
	private String sqlhdpxpf1 = "";
	private int hdpxpf1LoopIndex = 0;
	private HdpxpfTableDAM hdpxpf = new HdpxpfTableDAM();
	private DiskFileDAM hdpx01 = new DiskFileDAM("HDPX01");
	private DiskFileDAM hdpx02 = new DiskFileDAM("HDPX02");
	private DiskFileDAM hdpx03 = new DiskFileDAM("HDPX03");
	private DiskFileDAM hdpx04 = new DiskFileDAM("HDPX04");
	private DiskFileDAM hdpx05 = new DiskFileDAM("HDPX05");
	private DiskFileDAM hdpx06 = new DiskFileDAM("HDPX06");
	private DiskFileDAM hdpx07 = new DiskFileDAM("HDPX07");
	private DiskFileDAM hdpx08 = new DiskFileDAM("HDPX08");
	private DiskFileDAM hdpx09 = new DiskFileDAM("HDPX09");
	private DiskFileDAM hdpx10 = new DiskFileDAM("HDPX10");
	private DiskFileDAM hdpx11 = new DiskFileDAM("HDPX11");
	private DiskFileDAM hdpx12 = new DiskFileDAM("HDPX12");
	private DiskFileDAM hdpx13 = new DiskFileDAM("HDPX13");
	private DiskFileDAM hdpx14 = new DiskFileDAM("HDPX14");
	private DiskFileDAM hdpx15 = new DiskFileDAM("HDPX15");
	private DiskFileDAM hdpx16 = new DiskFileDAM("HDPX16");
	private DiskFileDAM hdpx17 = new DiskFileDAM("HDPX17");
	private DiskFileDAM hdpx18 = new DiskFileDAM("HDPX18");
	private DiskFileDAM hdpx19 = new DiskFileDAM("HDPX19");
	private DiskFileDAM hdpx20 = new DiskFileDAM("HDPX20");
	private HdpxpfTableDAM hdpxpfData = new HdpxpfTableDAM();
		/*    Change the record length to that of the temporary file.
		    This can be found by doing a DSPFD of the file being
		    duplicated by the CRTTMPF process.*/
	private FixedLengthStringData hdpx01Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx02Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx03Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx04Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx05Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx06Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx07Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx08Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx09Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx10Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx11Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx12Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx13Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx14Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx15Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx16Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx17Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx18Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx19Rec = new FixedLengthStringData(32);
	private FixedLengthStringData hdpx20Rec = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH519");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

		/*    HDPX member parameters*/
	private FixedLengthStringData wsaaHdpxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHdpxFn, 0, FILLER).init("HDPX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHdpxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHdpxFn, 6).setUnsigned();
		/*    Host variables*/
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*    Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*    Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaHdpxData = FLSInittedArray (1000, 24);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaHdpxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaHdpxData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaHdpxData, 9);
	private FixedLengthStringData[] wsaaJlife = FLSDArrayPartOfArrayStructure(2, wsaaHdpxData, 11);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaHdpxData, 13);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaHdpxData, 15);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaHdpxData, 17);
	private FixedLengthStringData[] wsaaDivdOption = FLSDArrayPartOfArrayStructure(4, wsaaHdpxData, 20);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
//	private ItemTableDAM itemIO = new ItemTableDAM();
	private P6671par p6671par = new P6671par();

	public Bh525() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/**    The  restart section is being handled in section 1100.*/
		/**    This is because the code is applicable to every run,*/
		/**    not just restarts.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check that the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Do ovrdbf for each temporary file member. Note that we have*/
		/*    allowed for a maximum of 20.*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Open the required number of temporary files, depending*/
		/*    on IZ and determined by the number of threads specified*/
		/*    for the subsequent process.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		sqlhdpxpf1 = " SELECT DISTINCT CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, ZDIVOPT" +
" FROM   " + getAppVars().getTableNameOverriden("HDIVPF") + " " +
" WHERE CHDRCOY = ?" +
" AND CHDRNUM BETWEEN ? AND ?" +
" AND HDVTYP = 'D'" +
" AND HDVOPTTX = 0" +
" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX";
		sqlerrorflag = false;
		try {
			sqlhdpxpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.cashdividends.dataaccess.HdivpfTableDAM());
			sqlhdpxpf1ps = getAppVars().prepareStatementEmbeded(sqlhdpxpf1conn, sqlhdpxpf1, "HDIVPF");
			getAppVars().setDBString(sqlhdpxpf1ps, 1, wsaaCompany);
			getAppVars().setDBString(sqlhdpxpf1ps, 2, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlhdpxpf1ps, 3, wsaaChdrnumTo);
			sqlhdpxpf1rs = getAppVars().executeQuery(sqlhdpxpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as*/
		/*    the array is indexed).*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHdpxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HDPX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHdpxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 50)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			hdpx01.openOutput();
		}
		if (isEQ(iz, 2)) {
			hdpx02.openOutput();
		}
		if (isEQ(iz, 3)) {
			hdpx03.openOutput();
		}
		if (isEQ(iz, 4)) {
			hdpx04.openOutput();
		}
		if (isEQ(iz, 5)) {
			hdpx05.openOutput();
		}
		if (isEQ(iz, 6)) {
			hdpx06.openOutput();
		}
		if (isEQ(iz, 7)) {
			hdpx07.openOutput();
		}
		if (isEQ(iz, 8)) {
			hdpx08.openOutput();
		}
		if (isEQ(iz, 9)) {
			hdpx09.openOutput();
		}
		if (isEQ(iz, 10)) {
			hdpx10.openOutput();
		}
		if (isEQ(iz, 11)) {
			hdpx11.openOutput();
		}
		if (isEQ(iz, 12)) {
			hdpx12.openOutput();
		}
		if (isEQ(iz, 13)) {
			hdpx13.openOutput();
		}
		if (isEQ(iz, 14)) {
			hdpx14.openOutput();
		}
		if (isEQ(iz, 15)) {
			hdpx15.openOutput();
		}
		if (isEQ(iz, 16)) {
			hdpx16.openOutput();
		}
		if (isEQ(iz, 17)) {
			hdpx17.openOutput();
		}
		if (isEQ(iz, 18)) {
			hdpx18.openOutput();
		}
		if (isEQ(iz, 19)) {
			hdpx19.openOutput();
		}
		if (isEQ(iz, 20)) {
			hdpx20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaJlife[wsaaInd.toInt()].set(SPACES);
		wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaDivdOption[wsaaInd.toInt()].set(SPACES);
		wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{
		readFiles2010();
	}

protected void readFiles2010()
	{
		/*    Now a block of records is fetched into the array.*/
		/*    Also on the first entry into the program we must set up the*/
		/*    WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (hdpxpf1LoopIndex = 1; isLTE(hdpxpf1LoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlhdpxpf1rs); hdpxpf1LoopIndex++ ){
				getAppVars().getDBObject(sqlhdpxpf1rs, 1, wsaaChdrcoy[hdpxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdpxpf1rs, 2, wsaaChdrnum[hdpxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdpxpf1rs, 3, wsaaLife[hdpxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdpxpf1rs, 4, wsaaJlife[hdpxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdpxpf1rs, 5, wsaaCoverage[hdpxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdpxpf1rs, 6, wsaaRider[hdpxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdpxpf1rs, 7, wsaaPlnsfx[hdpxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdpxpf1rs, 8, wsaaDivdOption[hdpxpf1LoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*    If either of the above cases occur, then an SQLCODE = +100*/
		/*    is returned.*/
		/*    The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		} 
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*    Re-initialise the block for next fetch and point to the*/
		/*    first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*    If the CHDRNUM being processed is not equal to the previous*/
		/*    one we should move to the next output file member.*/
		/*    The condition is here to allow for checking the last CHDRNUM*/
		/*    of the old block with the first of the new and to move to*/
		/*    the next HDPX member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*    Load from storage all HDPX data for the same contract until*/
		/*    the CHDRNUM on HDPX has changed or until the end of an*/
		/*    incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3201();
	}

protected void start3201()
	{
		hdpxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		hdpxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		hdpxpfData.life.set(wsaaLife[wsaaInd.toInt()]);
		hdpxpfData.jlife.set(wsaaJlife[wsaaInd.toInt()]);
		hdpxpfData.coverage.set(wsaaCoverage[wsaaInd.toInt()]);
		hdpxpfData.rider.set(wsaaRider[wsaaInd.toInt()]);
		hdpxpfData.planSuffix.set(wsaaPlnsfx[wsaaInd.toInt()]);
		hdpxpfData.zdivopt.set(wsaaDivdOption[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy, 1)) {
			hdpx01.write(hdpxpfData);
		}
		if (isEQ(iy, 2)) {
			hdpx02.write(hdpxpfData);
		}
		if (isEQ(iy, 3)) {
			hdpx03.write(hdpxpfData);
		}
		if (isEQ(iy, 4)) {
			hdpx04.write(hdpxpfData);
		}
		if (isEQ(iy, 5)) {
			hdpx05.write(hdpxpfData);
		}
		if (isEQ(iy, 6)) {
			hdpx06.write(hdpxpfData);
		}
		if (isEQ(iy, 7)) {
			hdpx07.write(hdpxpfData);
		}
		if (isEQ(iy, 8)) {
			hdpx08.write(hdpxpfData);
		}
		if (isEQ(iy, 9)) {
			hdpx09.write(hdpxpfData);
		}
		if (isEQ(iy, 10)) {
			hdpx10.write(hdpxpfData);
		}
		if (isEQ(iy, 11)) {
			hdpx11.write(hdpxpfData);
		}
		if (isEQ(iy, 12)) {
			hdpx12.write(hdpxpfData);
		}
		if (isEQ(iy, 13)) {
			hdpx13.write(hdpxpfData);
		}
		if (isEQ(iy, 14)) {
			hdpx14.write(hdpxpfData);
		}
		if (isEQ(iy, 15)) {
			hdpx15.write(hdpxpfData);
		}
		if (isEQ(iy, 16)) {
			hdpx16.write(hdpxpfData);
		}
		if (isEQ(iy, 17)) {
			hdpx17.write(hdpxpfData);
		}
		if (isEQ(iy, 18)) {
			hdpx18.write(hdpxpfData);
		}
		if (isEQ(iy, 19)) {
			hdpx19.write(hdpxpfData);
		}
		if (isEQ(iy, 20)) {
			hdpx20.write(hdpxpfData);
		}
		/*    Log the number of extracted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*    Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/		
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*    Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlhdpxpf1conn, sqlhdpxpf1ps, sqlhdpxpf1rs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			hdpx01.close();
		}
		if (isEQ(iz, 2)) {
			hdpx02.close();
		}
		if (isEQ(iz, 3)) {
			hdpx03.close();
		}
		if (isEQ(iz, 4)) {
			hdpx04.close();
		}
		if (isEQ(iz, 5)) {
			hdpx05.close();
		}
		if (isEQ(iz, 6)) {
			hdpx06.close();
		}
		if (isEQ(iz, 7)) {
			hdpx07.close();
		}
		if (isEQ(iz, 8)) {
			hdpx08.close();
		}
		if (isEQ(iz, 9)) {
			hdpx09.close();
		}
		if (isEQ(iz, 10)) {
			hdpx10.close();
		}
		if (isEQ(iz, 11)) {
			hdpx11.close();
		}
		if (isEQ(iz, 12)) {
			hdpx12.close();
		}
		if (isEQ(iz, 13)) {
			hdpx13.close();
		}
		if (isEQ(iz, 14)) {
			hdpx14.close();
		}
		if (isEQ(iz, 15)) {
			hdpx15.close();
		}
		if (isEQ(iz, 16)) {
			hdpx16.close();
		}
		if (isEQ(iz, 17)) {
			hdpx17.close();
		}
		if (isEQ(iz, 18)) {
			hdpx18.close();
		}
		if (isEQ(iz, 19)) {
			hdpx19.close();
		}
		if (isEQ(iz, 20)) {
			hdpx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
