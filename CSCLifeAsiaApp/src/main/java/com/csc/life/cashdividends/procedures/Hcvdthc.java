/*
 * File: Hcvdthc.java
 * Date: 29 August 2009 22:52:08
 * Author: Quipoz Limited
 *
 * Class transformed from HCVDTHC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivcshTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.quipoz.COBOLFramework.util.StringUtil;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
/**
* <pre>
*
*REMARKS.
*
*   TRADITIONAL LIFE - CASH DIVIDEND DEATH CLAIM CALCULATION
*
*   This subroutine is called from various programs via T6598.
*   It is responsible for returning the death claim amount which
*   consists of basic SI, paid up SI, accumulated dividend and
*   O/S interest of a component.
*
*   Note that plan processing is NOT part of this scope and
*   hence the subroutine is only catered for plan level process-
*   ing.
*
*   The subroutine is called four times for each component, each
*   time has a different function as follow:
*
*       1/ Basic SI
*       2/ Paid-up SI
*       3/ Accumulated Dividend and capitalised interest
*       4/ O/S interest to be capitalised
*
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read COVRTRB for coverage details, such as ANB-AT-RCD, SEX,
*     etc.
*
*   - determine last and next anniversary dates
*      last = COVRTRB-UNIT-STATEMENT-DATE
*      next = COVRTRB-UNIT-STATEMENT-DATE + 1 (via DATCON2)
*
*   - Depending on CDTH-ENDF, process cash value of SI in 4 steps
*        space - basic SI
*          2   - paid up addition SI
*          3   - accumulated dividend
*          4   - O/S interest
*
*   Basic SI >>
*   - claim amount = sum assured,
*   - set description 'Basic SI'
*
*   Paid-up Addition SI >>
*   For each HPUA read,
*          Add HPUA SI to give claim amount
*   End-for
*   - set description 'PUA SI'
*
*   Accumulate Dividend>>
*   - Read HDIS for the dividend balance since last capitalisa-
*     tion date
*   - Get all withdrawn dividend since last capitalisation date
*     from HDIV via HDIVCSH. Where these values will be -ve in
*     nature, add them to the dividend balance
*   - set description 'Dividend' and claim amount to total
*     dividend balance
*
*   O/S Interest      >>
*   - Get O/S interest to be capitalised from HDIS
*   - Read HCSD, with its ZCSHDIVMTH access TH501 for the
*     interest calculation subroutine
*   - Call TH501 interest calculation subr with parameters,
*        INT-FROM = last interest date from HDIS
*        INT-TO   = CDTH-EFFDATE
*        CAP-AMOUNT = dividend balance since last capitalisa-
*                     tion date
*        TRANNO   = zeroes
*        TRANSCD  = spaces
*   - Add calculated interest to O/S interest
*   - Calculate interest levied on the withdrawn dividend from
*     HDIV via HDIVCSH,
*     For each HDIVCSH read, call TH501 interest calc sbr
*        INT-FROM = greater (HDIS-LAST-INT-DATE, HDIVCSH-EFFDATE)
*        INT-TO   = CDTH-EFFDATE
*        CAP-AMOUNT = HDIVCSH-DIVD-AMOUNT
*        TRANNO   = zeroes
*        TRANSCD  = spaces
*   - These interests are -ve in nature, add to O/S interest
*   - set description 'O/S Int' and claim amount to total O/S
*     interest
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hcvdthc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HCVDTHC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-VARIABLES */
	private PackedDecimalData wsaaNextAnnivDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastAnnivDate = new PackedDecimalData(8, 0);
		/* WSAA-ACCUMULATORS */
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotInt = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotHpua = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
		/* ERRORS */
	private String g344 = "G344";
	private String g563 = "G563";
	private String h155 = "H155";
	private String hl16 = "HL16";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String covrtrbrec = "COVRTRBREC";
	private String hdisrec = "HDISREC";
	private String hpuarec = "HPUAREC";
	private String hcsdrec = "HCSDREC";
	private String hdivcshrec = "HDIVCSHREC";
	private String payrrec = "PAYRREC";
		/* TABLES */
	private String th501 = "TH501";
		/*COVR layout for trad. reversionary bonus*/
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Deathrec deathrec = new Deathrec();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Cash Withdraw Trans. Details*/
	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
		/*Paid Up Addition Coverage Details Logica*/
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Th501rec th501rec = new Th501rec();
	private Varcom varcom = new Varcom();
	private boolean cdivFlag = false;
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaIssuedAge = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private FixedLengthStringData wsaaMortcls = new FixedLengthStringData(1).isAPartOf(wsaaItem, 6);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItem, 7);
	List<Itempf> itempfList = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private static final String th527 = "TH527";
	private static final String DVAC = "DVAC";
	private Iterator<Itempf> itempfListIterator;
	private Th527rec th527rec = new Th527rec();
	private PackedDecimalData wsaaAnniversary = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaath527Date = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaPrevDividend = new PackedDecimalData(18, 3);
	private Hdivdrec hdivdrec = new Hdivdrec();
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",  HcsdpfDAO.class);
	private List<Hcsdpf> hsdpfList = null;
	private	Hcsdpf hcsdpf = null;
	private Itempf itempf = null;
	private int intdividFlag = 0;
	private String t6639 = "T6639";
	private T6639rec t6639rec = new T6639rec();
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit3090,
		exit4090,
		seExit9090,
		dbExit9190
	}

	public Hcvdthc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		main1000();
		return1900();
	}

protected void main1000()
	{
		deathrec.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(deathrec.chdrChdrcoy);
		payrIO.setChdrnum(deathrec.chdrChdrnum);
		payrIO.setPayrseqno(0);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			dbError9100();
		}
		if (isNE(payrIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(payrIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isEQ(payrIO.getStatuz(),varcom.endp)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(deathrec.chdrChdrcoy.toString());
			stringVariable1.append(deathrec.chdrChdrnum.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(g563);
			dbError9100();
		}
		cdivFlag = FeaConfg.isFeatureExist("2", "BTPRO015", appVars, "IT");
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(deathrec.chdrChdrcoy);
		covrtrbIO.setChdrnum(deathrec.chdrChdrnum);
		covrtrbIO.setLife(deathrec.lifeLife);
		covrtrbIO.setCoverage(deathrec.covrCoverage);
		covrtrbIO.setRider(deathrec.covrRider);
		covrtrbIO.setPlanSuffix(0);
		covrtrbIO.setFormat(covrtrbrec);
		covrtrbIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covrtrbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrtrbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrtrbIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),deathrec.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(covrtrbIO.getRider(),deathrec.covrRider)
		|| isEQ(covrtrbIO.getStatuz(),varcom.endp)) {
			StringBuilder stringVariable2 = new StringBuilder();
			stringVariable2.append(deathrec.chdrChdrcoy.toString());
			stringVariable2.append(deathrec.chdrChdrnum.toString());
			stringVariable2.append(deathrec.lifeLife.toString());
			stringVariable2.append(deathrec.covrCoverage.toString());
			stringVariable2.append(deathrec.covrRider.toString());
			syserrrec.params.setLeft(stringVariable2.toString());
			syserrrec.statuz.set(g344);
			dbError9100();
		}
		datcon2rec.intDate1.set(covrtrbIO.getUnitStatementDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(deathrec.fieldType,SPACES)) {
			basicSi1000();
			deathrec.fieldType.set("S");
		}
		else {
			if (isEQ(deathrec.fieldType,"S")) {
				puaSi2000();
				deathrec.fieldType.set("P");
			}
			else {
				if (isEQ(deathrec.fieldType,"P")) {
					if(cdivFlag){
						calcDividend3000();
					}
					accumDividend3000();
					deathrec.fieldType.set("D");
				}
				else {
					if (isEQ(deathrec.fieldType,"D")) {
						if(cdivFlag){
							calcinterest4000();
						}
						interest4000();
						deathrec.fieldType.set("I");
					}
					else {
						if (isEQ(deathrec.fieldType,"I")) {
							deathrec.status.set(varcom.endp);
						}
						else {
							syserrrec.statuz.set(h155);
							systemError9000();
						}
					}
				}
			}
		}
	}

protected void return1900()
	{
		exitProgram();
	}

protected void basicSi1000()
	{
		/*START*/
		deathrec.actualVal.set(covrtrbIO.getSumins());
		deathrec.description.set("Basic SI");
		/*EXIT*/
	}

protected void puaSi2000()
	{
		start2000();
	}

protected void start2000()
	{
		hpuaIO.setDataArea(SPACES);
		hpuaIO.setChdrcoy(deathrec.chdrChdrcoy);
		hpuaIO.setChdrnum(deathrec.chdrChdrnum);
		hpuaIO.setLife(deathrec.lifeLife);
		hpuaIO.setCoverage(deathrec.covrCoverage);
		hpuaIO.setRider(deathrec.covrRider);
		hpuaIO.setPlanSuffix(0);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFormat(hpuarec);
		hpuaIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		if (isNE(hpuaIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(hpuaIO.getLife(),deathrec.lifeLife)
		|| isNE(hpuaIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(hpuaIO.getRider(),deathrec.covrRider)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		wsaaTotHpua.set(ZERO);
		while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
			if (isEQ(hpuaIO.getValidflag(),"1")) {
				wsaaTotHpua.add(hpuaIO.getSumin());
			}
			hpuaIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)
			&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
				hpuaIO.setStatuz(varcom.endp);
			}
			if (isNE(hpuaIO.getChdrcoy(),deathrec.chdrChdrcoy)
			|| isNE(hpuaIO.getChdrnum(),deathrec.chdrChdrnum)
			|| isNE(hpuaIO.getLife(),deathrec.lifeLife)
			|| isNE(hpuaIO.getCoverage(),deathrec.covrCoverage)
			|| isNE(hpuaIO.getRider(),deathrec.covrRider)) {
				hpuaIO.setStatuz(varcom.endp);
			}
		}

		compute(deathrec.actualVal, 6).setRounded(mult(wsaaTotHpua,1));
		deathrec.description.set("PUA SI");
	}

protected void calcDividend3000(){
	
	if(isGTE(covrtrbIO.getUnitStatementDate(),deathrec.effdate)){
		return;
	}
	readth527();
	calcDividend();
	if(isNE(hdivdrec.divdAmount,ZERO)){
		deathrec.actualVal.set(hdivdrec.divdAmount);
		wsaaPrevDividend.set(hdivdrec.divdAmount);
		deathrec.description.set("Dividend");
	}
}

protected void accumDividend3000()
	{
		try {
			start3100();
		}
		catch (GOTOException e){
		}
	}

protected void start3100()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(deathrec.chdrChdrcoy);
		hdisIO.setChdrnum(deathrec.chdrChdrnum);
		hdisIO.setLife(deathrec.lifeLife);
		hdisIO.setCoverage(deathrec.covrCoverage);
		hdisIO.setRider(deathrec.covrRider);
		hdisIO.setPlanSuffix(0);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			hdisIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
		wsaaTotDividend.set(hdisIO.getBalSinceLastCap());
		if(cdivFlag)
			wsaaPrevDividend.add(wsaaTotDividend);
		hdivcshIO.setDataArea(SPACES);
		hdivcshIO.setChdrcoy(deathrec.chdrChdrcoy);
		hdivcshIO.setChdrnum(deathrec.chdrChdrnum);
		hdivcshIO.setLife(deathrec.lifeLife);
		hdivcshIO.setCoverage(deathrec.covrCoverage);
		hdivcshIO.setRider(deathrec.covrRider);
		hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
		compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
		hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		if (isNE(hdivcshIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(hdivcshIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(hdivcshIO.getLife(),deathrec.lifeLife)
		|| isNE(hdivcshIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(hdivcshIO.getRider(),deathrec.covrRider)
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			wsaaTotDividend.add(hdivcshIO.getDivdAmount());
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				hdivcshIO.setStatuz(varcom.endp);
			}
			if (isNE(hdivcshIO.getChdrcoy(),deathrec.chdrChdrcoy)
			|| isNE(hdivcshIO.getChdrnum(),deathrec.chdrChdrnum)
			|| isNE(hdivcshIO.getLife(),deathrec.lifeLife)
			|| isNE(hdivcshIO.getCoverage(),deathrec.covrCoverage)
			|| isNE(hdivcshIO.getRider(),deathrec.covrRider)
			|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}
		if(cdivFlag){
			deathrec.actualVal.add(wsaaTotDividend);	
		}
		else{
			deathrec.actualVal.set(wsaaTotDividend);
		}
		deathrec.description.set("Dividend");
		
	}

protected void calcDividend(){
	
	datcon2rec.intDate1.set(covrtrbIO.getUnitStatementDate());
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		dbError9100();
	}
	else {
		wsaaAnniversary.set(datcon2rec.intDate2); //CntAnniversaryDate	
	}		
	setuphcsd();
	if(isGT(wsaaAnniversary,deathrec.effdate)){
		if(isGTE(deathrec.effdate,covrtrbIO.getUnitStatementDate()) && isLTE(deathrec.effdate,wsaaAnniversary))
			intdividFlag = 2;
		return;
	}
	itempfListIterator = itempfList.iterator();
	iterateitempfList();
	if(isEQ(th527rec.th527Rec,SPACES)){
		itempfListIterator = itempfList.iterator();
		getLatestFactor();
		if(isNE(th527rec.th527Rec,SPACES)){
			intdividFlag = 1;
			processDividend();
		}
		else{
			return;
		}
	}
}

protected void setuphcsd(){
	
	hcsdpf = new Hcsdpf();
	hcsdpf.setChdrcoy(deathrec.chdrChdrcoy.toString());
	hcsdpf.setChdrnum(deathrec.chdrChdrnum.toString());	
	hcsdpf.setLife(deathrec.lifeLife.toString());
	hcsdpf.setCoverage(deathrec.covrCoverage.toString());
	hcsdpf.setRider(deathrec.covrRider.toString());
	hcsdpf.setPlnsfx(0);		
	hsdpfList = hcsdpfDAO.searchHcsdpfRecord(hcsdpf);
	for(Hcsdpf hcsdobj : hsdpfList){
	if(isEQ(hcsdobj.getValidflag(),"1")){
		itempf = itempfDAO.readItdmpf("IT",deathrec.chdrChdrcoy.toString(),t6639,deathrec.effdate.toInt(),StringUtils.rightPad(hcsdobj.getZcshdivmth()+covrtrbIO.getPstatcode(), 8));
		if(itempf != null){
			t6639rec.t6639Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else{
			syserrrec.params.set("IT".concat(deathrec.chdrChdrcoy.toString()).concat(t6639).concat(hcsdobj.getZcshdivmth()+covrtrbIO.getPstatcode()));
			systemError9000();
		}
		hcsdpf = hcsdobj;
		break;
	}
  }
}
	
protected void getLatestFactor(){
	
	itempfListIterator = itempfList.iterator();
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
			wsaath527Date.set(itempf.getItmfrm());
			return;
		}else{
			th527rec.th527Rec.set(SPACES);	
			}
		}				
}

protected void processDividend(){	
	/* check table has been set up correctly - must have non-zero*/
	/*  risk units otherwise we will be dividing by zero !!*/
	if (isEQ(th527rec.unit,ZERO)) {
		itdmIO.setItemitem(wsaaItem);
		syserrrec.params.set(itdmIO.getParams());
		dbError9100();
	}
	initialize(hdivdrec.dividendRec);
	hdivdrec.crrcd.set(covrtrbIO.getCrrcd());
	hdivdrec.periodTo.set(wsaaAnniversary);
	hdivdrec.periodFrom.set(covrtrbIO.getUnitStatementDate());
	hdivdrec.divdAmount.set(ZERO);
	hdivdrec.sumin.set(covrtrbIO.getSumins());
	hdivdrec.crtable.set(covrtrbIO.getCrtable());
	hdivdrec.issuedAge.set(covrtrbIO.getAnbAtCcd());
	hdivdrec.mortcls.set(covrtrbIO.getMortcls());
	hdivdrec.sex.set(covrtrbIO.getSex());
	hdivdrec.chdrChdrcoy.set(deathrec.chdrChdrcoy);
	hdivdrec.itmfrmTh527.set(wsaath527Date);
	if(isNE(t6639rec.revBonusProg,SPACES)){
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
	}
}

protected void readth527(){
	
		wsaaCrtable.set(deathrec.crtable);
		wsaaIssuedAge.set(covrtrbIO.getAnbAtCcd());
		wsaaMortcls.set(covrtrbIO.getMortcls());		
		wsaaSex.set(covrtrbIO.getSex());
	
		itempfList = itempfDAO.findBy("IT",deathrec.chdrChdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
		if(itempfList.size() == 0){ 
			
			while(itempfList.size() == 0){
	 		
				if (isEQ(subString(wsaaItem, 5, 4),"****")) {
					itdmIO.setItemitem(wsaaItem);
					dbError9100();
					
				}
				else {
					if (isNE(subString(wsaaItem, 8, 1),"*")) {
						wsaaItem.setSub1String(8, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 7, 1),"*")) {
							wsaaItem.setSub1String(7, 1, "*");
						}
						else {
							if (isNE(subString(wsaaItem, 5, 2),"**")) {
								wsaaItem.setSub1String(5, 2, "**");
							}
						}
					}
				}
				itempfList = itempfDAO.findBy("IT",deathrec.chdrChdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
	 		}
	 				
	 	}
}

protected void iterateitempfList(){
	
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			if((wsaaAnniversary.toInt() >= Integer.parseInt(itempf.getItmfrm().toString()) )
					&& wsaaAnniversary.toInt() <= Integer.parseInt(itempf.getItmto().toString())){
				th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				return;
			}else{
				th527rec.th527Rec.set(SPACES);
			}
		}				
	}	
}

protected void calcinterest4000(){
	
		if(intdividFlag == 0){
			return;
		}
		deathrec.actualVal.set(ZERO);
		
		if(isEQ(hcsdpf.getZdivopt(),DVAC) && isNE(wsaaPrevDividend,ZERO)){
			itempf = itempfDAO.readItdmpf("IT",deathrec.chdrChdrcoy.toString(),th501,deathrec.effdate.toInt(),StringUtils.rightPad(hcsdpf.getZcshdivmth(), 8));
			if(itempf != null){
				th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			else{
				syserrrec.params.set("IT".concat(deathrec.chdrChdrcoy.toString()).concat(th501).concat(hcsdpf.getZcshdivmth()));
				systemError9000();
			}
		}
		else{
			return;
		}
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(deathrec.chdrChdrcoy);
		hdvdintrec.chdrChdrnum.set(deathrec.chdrChdrnum);
		hdvdintrec.lifeLife.set(deathrec.lifeLife);
		hdvdintrec.covrCoverage.set(deathrec.covrCoverage);
		hdvdintrec.covrRider.set(deathrec.covrRider);
		hdvdintrec.plnsfx.set(ZERO);
		hdvdintrec.cntcurr.set(deathrec.currcode);
		hdvdintrec.transcd.set(SPACES);
		hdvdintrec.crtable.set(deathrec.crtable);
		hdvdintrec.effectiveDate.set(deathrec.effdate);
		hdvdintrec.tranno.set(ZERO);
		if(intdividFlag == 1){
		hdvdintrec.intFrom.set(wsaaAnniversary);
		}
		else{
		hdvdintrec.intFrom.set(covrtrbIO.getUnitStatementDate());
		}
		hdvdintrec.intTo.set(deathrec.effdate);
		hdvdintrec.capAmount.set(wsaaPrevDividend);
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
			callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
			if (isNE(hdvdintrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(hdvdintrec.statuz);
				systemError9000();
			}
			wsaaTotInt.add(hdvdintrec.intAmount);
		}

	deathrec.actualVal.set(wsaaTotInt);
	if (isLT(deathrec.actualVal,ZERO)) {
		deathrec.actualVal.set(ZERO);
	}
	deathrec.description.set("O/S Int");
}

protected void interest4000()
	{
		try {
			start4100();
		}
		catch (GOTOException e){
		}
	}

protected void start4100()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(deathrec.chdrChdrcoy);
		hdisIO.setChdrnum(deathrec.chdrChdrnum);
		hdisIO.setLife(deathrec.lifeLife);
		hdisIO.setCoverage(deathrec.covrCoverage);
		hdisIO.setRider(deathrec.covrRider);
		hdisIO.setPlanSuffix(0);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			hdisIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit4090);
		}
		wsaaTotInt.set(hdisIO.getOsInterest());
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(deathrec.chdrChdrcoy);
		hcsdIO.setChdrnum(deathrec.chdrChdrnum);
		hcsdIO.setLife(deathrec.lifeLife);
		hcsdIO.setCoverage(deathrec.covrCoverage);
		hcsdIO.setRider(deathrec.covrRider);
		hcsdIO.setPlanSuffix(hdisIO.getPlanSuffix());
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hcsdIO.getStatuz());
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(deathrec.chdrChdrcoy);
		itdmIO.setItmfrm(hdisIO.getNextIntDate());
		itdmIO.setItemtabl(th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		 && isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),deathrec.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),th501)) {
			itdmIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(itdmIO.getParams());
			systemError9000();
		}
		th501rec.th501Rec.set(itdmIO.getGenarea());
		if (isEQ(th501rec.intCalcSbr,SPACES)) {
			itdmIO.setStatuz(hl16);
			syserrrec.params.set(itdmIO.getParams());
			systemError9000();
		}
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(deathrec.chdrChdrcoy);
		hdvdintrec.chdrChdrnum.set(deathrec.chdrChdrnum);
		hdvdintrec.lifeLife.set(deathrec.lifeLife);
		hdvdintrec.covrCoverage.set(deathrec.covrCoverage);
		hdvdintrec.covrRider.set(deathrec.covrRider);
		hdvdintrec.plnsfx.set(ZERO);
		hdvdintrec.cntcurr.set(deathrec.currcode);
		hdvdintrec.transcd.set(SPACES);
		hdvdintrec.crtable.set(deathrec.crtable);
		hdvdintrec.effectiveDate.set(deathrec.effdate);
		hdvdintrec.tranno.set(ZERO);
		hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
		hdvdintrec.intTo.set(payrIO.getPtdate());
		hdvdintrec.capAmount.set(hdisIO.getBalSinceLastCap());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(hdvdintrec.statuz);
			systemError9000();
		}
		wsaaTotInt.add(hdvdintrec.intAmount);
		hdivcshIO.setDataArea(SPACES);
		hdivcshIO.setChdrcoy(hdisIO.getChdrcoy());
		hdivcshIO.setChdrnum(hdisIO.getChdrnum());
		hdivcshIO.setLife(hdisIO.getLife());
		hdivcshIO.setCoverage(hdisIO.getCoverage());
		hdivcshIO.setRider(hdisIO.getRider());
		hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
		hdivcshIO.setDivdIntCapDate(hdisIO.getNextIntDate());
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
		|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
		|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			initialize(hdvdintrec.divdIntRec);
			hdvdintrec.chdrChdrcoy.set(deathrec.chdrChdrcoy);
			hdvdintrec.chdrChdrnum.set(deathrec.chdrChdrnum);
			hdvdintrec.lifeLife.set(deathrec.lifeLife);
			hdvdintrec.covrCoverage.set(deathrec.covrCoverage);
			hdvdintrec.covrRider.set(deathrec.covrRider);
			hdvdintrec.plnsfx.set(ZERO);
			hdvdintrec.cntcurr.set(deathrec.currcode);
			hdvdintrec.transcd.set(SPACES);
			hdvdintrec.crtable.set(deathrec.crtable);
			hdvdintrec.effectiveDate.set(deathrec.effdate);
			hdvdintrec.tranno.set(ZERO);
			if (isGT(hdivcshIO.getEffdate(),hdisIO.getLastIntDate())) {
				hdvdintrec.intFrom.set(hdivcshIO.getEffdate());
			}
			else {
				hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
			}
			hdvdintrec.intTo.set(payrIO.getPtdate());
			hdvdintrec.capAmount.set(hdivcshIO.getDivdAmount());
			hdvdintrec.intDuration.set(ZERO);
			hdvdintrec.intAmount.set(ZERO);
			hdvdintrec.intRate.set(ZERO);
			hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
			if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
				callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
				if (isNE(hdvdintrec.statuz,varcom.oK)) {
					syserrrec.statuz.set(hdvdintrec.statuz);
					systemError9000();
				}
				wsaaTotInt.add(hdvdintrec.intAmount);
			}
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				hdivcshIO.setStatuz(varcom.endp);
			}
			if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
			|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
			|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
			|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
			|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
			|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}
		if(cdivFlag){
		deathrec.actualVal.add(wsaaTotInt);
		}
		else{
		deathrec.actualVal.set(wsaaTotInt);
		}
		if (isLT(deathrec.actualVal,ZERO)) {
			deathrec.actualVal.set(ZERO);
		}
		deathrec.description.set("O/S Int");
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		deathrec.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		deathrec.status.set(varcom.bomb);
		exitProgram();
	}
}
