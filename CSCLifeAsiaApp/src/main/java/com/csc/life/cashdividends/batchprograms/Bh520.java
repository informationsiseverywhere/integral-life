/*
 * File: Bh520.java
 * Date: 29 August 2009 21:28:54
 * Author: Quipoz Limited
 *
 * Class transformed from BH520.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap; //ILIFE-9080
import java.util.Iterator;
import java.util.List;
import java.util.Map; //ILIFE-9080

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Varcom; //ILIFE-9080
//ILB-566 starts
import com.csc.smart.procedures.SftlockUtil;
import com.csc.fsu.agents.dataaccess.model.Agntpf;

import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.life.cashdividends.dataaccess.dao.HdispfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.dataaccess.model.Hdvxpf; //ILIFE-9080
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdvxpfDAO; //ILIFE-9080
import com.csc.life.cashdividends.dataaccess.model.Hpuapf;
import com.csc.life.cashdividends.dataaccess.dao.HpuapfDAO;
import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
//ILB-566 ends

import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This batch program process the coverage records extracted
*   in the previous program BH519 based on the Unit Statement
*   Date(CBUNST) and Bonus indicator.
*   Dividend is calculated and allocated to each eligible
*   coverage by the subroutine defined in T6639.  Then these
*   dividend transaction details are written to 2 files,
*        HDIS - dividend transaction summary
*        HDIV - dividend transaction details
*
*
*   PROCESSING
*   ==========
*
*   Initialise
*
*  - Check the restart method is 3.
*  - Find the name of the HDVX temporary file. HDVX is a temporary
*    file which has the name of "HDVX" + first two characters of
*    param 4 + the last four digits of the job number.
*  - Open HDVXPF as input.
*  - Pre-load constantly referenced Tables into working storage.
*    These are: T5688, TH501, T6639, T5645.
*  - Set up the general fields in LIFACMV.
*
*  Read.
*
*  - Read a HDVXPF.
*  - Check end of file and end processing
*
*  Edit.
*
*  - Softlock the contract if this HDVX is for a new contract.
*
*  Update.
*
*  - Calculate the new bonus date by adding one year to its
*      CBUNST
*  - Read HCSD(Cash dividend details)
*  - Read TH501 with cash dividend method in HCSD and effective
*    date = new bonus date
*  - If dividend alloc on receipt of full premium
*       but current year full premium not yet received,
*       then skip this HDSX
*    If dividend alloc on receipt of full premium + 1 instalment
*       but current year full premium not yet received,
*       then skip this HDSX
*  - Read and hold the CHDRLIF(Contract header)
*  - Read and hold the COVRBON(Coverage/Rider details for trad.
*    business bonus processing)
*  - Read HDIS(Dividend & Interest Summary)
*  - Calculate new interest allocation date,
*         read HDIS,                                             .
*         if O-K
*            set next interest alloc date =
*                HDIS next interest date + 1 Freq (TH501)
*         else
*            set next interest alloc date =
*                new bonus date + 1 freq (TH501)
*            also apply the fix DD & MM in TH501
*            ensure the DD for the new month is valid
*  - Calculate new interest capitalisation date,
*         read HDIS,                                             .
*         if O-K
*            set next interest cap. date =
*                HDIS next cap. date + 1 freq (TH501)
*         else
*            set next interest cap. date =
*                new bonus date + 1 freq (TH501)
*            also apply the fix DD & MM in TH501
*            ensure the DD for the new month is valid
*  - Read T6639 with cash dividend method from HCSD and
*    PSTATCODE from HDVXPF, to look for the additional bonus
*    program T6639-ADD-BONUS-PROG as the dividend calc subr.
*  - Set up the linkage for the new copy book HDIVDREC to call
*    the dividend calc subr.
*  alloc update start>>
*  - For non zero dividend amount returned from dividend calc
*    subr, prepare to write a HDIV record and update the HDIS
*    of it by invalid the existing one and write a new one.
*  - If HDIS is not already found, set up and write a new one.
*  <<alloc update end
*  - Allocate a dividend to Paid-up Additions,
*         set up to read related HUPA records,
*         for each read, if its cash dividend method not = space
*         search for its T6639-ADD-BONUS-PROG with
*             key = cash dividend method + PSTATCODE in HDVXPF
*         set up dividend allocation linkage in HDIVDREC
*         perform alloc update as mentioned
*  - if dividend amount is not zero, write accounting entries,
*         look for the posting level in T5688 for CNTTYPE
*         set up fields in LIFACMV linkage
*         map for the relevant GL details in T5645
*         call LIFACMV twice to post the dividend expense and
*         its opposite entry for payable.
*  - various file updates for transaction history, in CHDRLIF,
*    COVRBON and PTRN.
*
*  Finalise.
*
*  - Close HDVXPF
*  - Remove the override
*  - Set the parameter statuz to O-K
*
*
*   Control totals:
*     01 - No. of HDVX records read
*     02 - No. of contracts with O/S prem
*     03 - No. of HDVX records locked
*     04 - No. of HDVX records processed
*     05 - Total value of Dividend
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Bh520 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH520");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaHdvxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHdvxFn, 0, FILLER).init("HDVX");
	private FixedLengthStringData wsaaHdvxRunid = new FixedLengthStringData(2).isAPartOf(wsaaHdvxFn, 4);
	private ZonedDecimalData wsaaHdvxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHdvxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaSystemDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSysDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaSystemDate, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaNewBonusDate = new PackedDecimalData(8, 0).init(0);
	private ZonedDecimalData wsaaNextIntDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNextIntdate = new FixedLengthStringData(8).isAPartOf(wsaaNextIntDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNextintYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNextIntdate, 0).setUnsigned();
	private ZonedDecimalData wsaaNextintMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 4).setUnsigned();
	private ZonedDecimalData wsaaNextintDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 6).setUnsigned();
	private ZonedDecimalData wsaaNextCapDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNextCapdate = new FixedLengthStringData(8).isAPartOf(wsaaNextCapDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNextcapYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNextCapdate, 0).setUnsigned();
	private ZonedDecimalData wsaaNextcapMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextCapdate, 4).setUnsigned();
	private ZonedDecimalData wsaaNextcapDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextCapdate, 6).setUnsigned();
	private FixedLengthStringData wsaaLastdayOfMonths = new FixedLengthStringData(24).init("312831303130313130313031");

	private FixedLengthStringData wsaaLastddOfMonths = new FixedLengthStringData(24).isAPartOf(wsaaLastdayOfMonths, 0, REDEFINE);
	private ZonedDecimalData[] wsaaLastdd = ZDArrayPartOfStructure(12, 2, 0, wsaaLastddOfMonths, 0, UNSIGNED_TRUE);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaDivdAmount = new PackedDecimalData(11, 2).init(0);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaT5645Array = new FixedLengthStringData(21000);
	private FixedLengthStringData[] wsaaT5645Data = FLSArrayPartOfStructure(1000, 21, wsaaT5645Array, 0);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5688Array = new FixedLengthStringData(9000);
	private FixedLengthStringData[] wsaaT5688Rec = FLSArrayPartOfStructure(1000, 9, wsaaT5688Array, 0);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(8, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Key, 3);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 8);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
	//private static final int wsaaT5688Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;

	private FixedLengthStringData wsaaT6639Array = new FixedLengthStringData(19000);
	private FixedLengthStringData[] wsaaT6639Rec = FLSArrayPartOfStructure(1000, 19, wsaaT6639Array, 0);
	private FixedLengthStringData[] wsaaT6639Key = FLSDArrayPartOfArrayStructure(11, wsaaT6639Rec, 0);
	private FixedLengthStringData[] wsaaT6639Divdmth = FLSDArrayPartOfArrayStructure(4, wsaaT6639Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT6639Pstcd = FLSDArrayPartOfArrayStructure(2, wsaaT6639Key, 4, SPACES);
	private PackedDecimalData[] wsaaT6639Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6639Key, 6);
	private FixedLengthStringData[] wsaaT6639Data = FLSDArrayPartOfArrayStructure(8, wsaaT6639Rec, 11);
	private FixedLengthStringData[] wsaaT6639Bonusprog = FLSDArrayPartOfArrayStructure(8, wsaaT6639Data, 0);
	//private static final int wsaaT6639Size = 50;
	//private static final int wsaaTh501Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT6639Size = 1000;
	private static final int wsaaTh501Size = 1000;

	private FixedLengthStringData wsaaHdisDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaHdisDd = new FixedLengthStringData(2).isAPartOf(wsaaHdisDate, 6);
		/* TABLES */
	private static final String th501 = "TH501";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t6639 = "T6639";
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String h791 = "H791";
	private static final String hl17 = "HL17";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

	private Th501rec th501rec = new Th501rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T6639rec t6639rec = new T6639rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Hdivdrec hdivdrec = new Hdivdrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private WsaaTh501ArrayInner wsaaTh501ArrayInner = new WsaaTh501ArrayInner();
	
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaIssuedAge = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private FixedLengthStringData wsaaMortcls = new FixedLengthStringData(1).isAPartOf(wsaaItem, 6);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItem, 7);
	List<Itempf> itempfList = new ArrayList<>();
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);

	private static final String th527 = "TH527";
	boolean cdivFlag =false;
	private Iterator<Itempf> itempfListIterator;
	private Th527rec th527rec = new Th527rec();
    
    private List<Itempf> t5645List = null;
    private List<Itempf> t5688List = null;
    private List<Itempf> t6639List = null;
    private List<Itempf> th501List = null;
    private Itempf itempf = new Itempf();
    
    //ILB-566
    private SftlockUtil sftlockUtil = new SftlockUtil();
    private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO",AgntpfDAO.class);
    private Agntpf agntpf = new Agntpf();
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private Chdrpf chdrpf = new Chdrpf();
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
    private Covrpf covrpf = new Covrpf();
    private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",HcsdpfDAO.class);
    private Hcsdpf hcsdpf = new Hcsdpf();
    private HdispfDAO hdispfDAO = getApplicationContext().getBean("hdispfDAO",HdispfDAO.class);
    private Hdispf hdispf = new Hdispf();
    private List<Hdispf> hdispfList = new ArrayList<>();
    private HdivpfDAO hdivpfDAO = getApplicationContext().getBean("hdivpfDAO",HdivpfDAO.class);
    private Hdivpf hdivpf = new Hdivpf();
    private HpuapfDAO hpuapfDAO = getApplicationContext().getBean("hpuapfDAO",HpuapfDAO.class);
    private Hpuapf hpuapf = new Hpuapf();
    private List<Hpuapf> hpuapfList = new ArrayList<>();
    private Itdmpf itdmpf = new Itdmpf();
    private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
    private Ptrnpf ptrnpf = new Ptrnpf();
    private UstmData ustm = new UstmData();

 	//ILIFE-9080 start
    private Iterator<Hdvxpf> iter;
    private int batchID;
    private int batchExtractSize;
    private Hdvxpf hdvxpf; 
    private HdvxpfDAO hdvxpfDAO = getApplicationContext().getBean("hdvxpfDAO", HdvxpfDAO.class);
    private int ct01Value;
    private int ct02Value;
    private int ct03Value;
	private int ct04Value;
	private BigDecimal ct05Value = BigDecimal.ZERO;
    private int ct06Value;
	
	
    private Map<String, List<Covrpf>> covrpfMap = new HashMap<>();
    private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<>();
    private Map<String, List<Hdispf>> hdisListMap = new HashMap<>();
	private Map<String, List<Hcsdpf>> hcsdListMap = new HashMap<>();
	
	private Covrpf covrIO = new Covrpf();
	private List<Chdrpf> chdrpfListUpdt = new ArrayList<>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<>();
	private List<Covrpf> covrpfListUpdt = new ArrayList<>();
	private List<Covrpf> covrpfListInst = new ArrayList<>();
	private List<Ptrnpf> ptrnpfList = new ArrayList<>();
	private List<Hdivpf> hdivpfList = new ArrayList<>();
	private List<Hdispf> hdisList = new ArrayList<>();
	private List<Hdispf> hdisinsertallList = new ArrayList<>();
	//ILIFE-9080 end 
	private List<Hdispf> hdisupdateList = new ArrayList<>();

	public Bh520() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
 //ILIFE-9080 start
		wsspEdterror.set(varcom.oK);
		wsaaHdvxRunid.set(bprdIO.getSystemParam04());
		wsaaHdvxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
        if(wsspEdterror.equals(varcom.endp)){
        	return;
        }
 //ILIFE-9080 end        

		/*    Set system date.*/
		wsaaSystemDate.set(getCobolDate());
		/*    Load All Accounting Rules.*/
		
		t5645List=itempfDAO.findItem("IT", bsprIO.getCompany().toString(), t5645, wsaaProg.toString());
		t5688List=itempfDAO.findItem(bsprIO.getCompany().toString(), t5688, varcom.vrcmMaxDate.toInt());
		t6639List=itempfDAO.findItem(bsprIO.getCompany().toString(), t6639, varcom.vrcmMaxDate.toInt());
		th501List=itempfDAO.findItem(bsprIO.getCompany().toString(), th501, varcom.vrcmMaxDate.toInt());
		
		if (t5645List.size() > wsaaT5645Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5645");
            fatalError600();
        }
		loadT56451200(t5645List);
		
		if (t5688List.size() > wsaaT5688Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(t5688);
            fatalError600();
        }
		loadT56881300(t5688List);
		
		if (t6639List.size() > wsaaT5688Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(t6639);
            fatalError600();
        }
		loadT66391400(t6639List);
		
		if (th501List.size() > wsaaTh501Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set(th501);
            fatalError600();
        }
			loadTh5011500(th501List);
		
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		
		itempf.setItempfx("IT");
		itempf.setItemcoy(bsprIO.getCompany().toString());
		itempf.setItemtabl(t1688);
		itempf.setItemitem(bprdIO.getAuthCode().toString());
		getdescrec.itemkey.set(itempf.getItempfx().concat(bsprIO.getCompany().toString()).concat(t1688).concat(bprdIO.getAuthCode().toString()));
		
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
		cdivFlag = FeaConfg.isFeatureExist("2","BTPRO015",appVars,"IT");
	}
 //ILIFE-9080 start
private void readChunkRecord() {
	ct01Value = 0;
	ct02Value = 0;
	ct03Value = 0;
	ct04Value = 0;
	ct05Value = BigDecimal.ZERO;
	ct06Value = 0;
	
	List<Hdvxpf> hdvxpfList = hdvxpfDAO.searchHdvxpfRecord(wsaaHdvxFn.toString(), wsaaThreadMember.toString(),
			batchExtractSize, batchID);
	iter= hdvxpfList.iterator();
	if (!hdvxpfList.isEmpty()) {
	List<String> chdrnumList = new ArrayList<>(hdvxpfList.size());
	for (Hdvxpf p : hdvxpfList) {
		chdrnumList.add(p.getChdrnum());
	}
	String coy = bsprIO.getCompany().toString();
	covrpfMap = covrpfDAO.searchCovrMap(coy, chdrnumList);
	chdrpfMap = chdrpfDAO.searchChdrpf(coy, chdrnumList);
	hdisListMap = hdispfDAO.getHdisMap(coy, chdrnumList);
	hcsdListMap = hcsdpfDAO.getHcsdMap(coy, chdrnumList); 
	}
}
 //ILIFE-9080 end


protected void loadT56451200(List<Itempf> itemList)
{
       int i = 1;
       int x = 1;
       for (Itempf itempf : itemList) {
           	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
         //  	ix.set(1);
			while ( !(isGT(x,15))) {
			wsaaT5645Cnttot[i].set(t5645rec.cnttot[x]);
			wsaaT5645Glmap[i].set(t5645rec.glmap[x]);
			wsaaT5645Sacscode[i].set(t5645rec.sacscode[x]);
			wsaaT5645Sacstype[i].set(t5645rec.sacstype[x]);
			wsaaT5645Sign[i].set(t5645rec.sign[x]);
			/*ix.add(1);
			iy.add(1);*/
			x++;
           i++;
		 }
       }
   }

protected void loadT56881300(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
           t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
           wsaaT5688Cnttype[i].set(itempf.getItemitem());
           wsaaT5688Itmfrm[i].set(itempf.getItmfrm());
           wsaaT5688Comlvlacc[i].set(t5688rec.comlvlacc);
           i++;
       }
   }


protected void loadT66391400(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
    	   	t6639rec.t6639Rec.set(StringUtil.rawToString(itempf.getGenarea()));
    		wsaaT6639Divdmth[i].set(subString(itempf.getItemitem(), 1, 4));
    		wsaaT6639Pstcd[i].set(subString(itempf.getItemitem(), 5, 2));
    		wsaaT6639Itmfrm[i].set(itempf.getItmfrm());
    		wsaaT6639Bonusprog[i].set(t6639rec.revBonusProg);
           i++;
       }
   }

protected void loadTh5011500(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
    	   	th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
           	wsaaTh501ArrayInner.wsaaTh501Divdmth[i].set(itempf.getItemitem());
	   		wsaaTh501ArrayInner.wsaaTh501Itmfrm[i].set(itempf.getItmfrm());
	   		wsaaTh501ArrayInner.wsaaTh501CapFq[i].set(th501rec.freqcy01);
	   		wsaaTh501ArrayInner.wsaaTh501IntFq[i].set(th501rec.freqcy02);
	   		wsaaTh501ArrayInner.wsaaTh501CapDd[i].set(th501rec.hfixdd01);
	   		wsaaTh501ArrayInner.wsaaTh501IntDd[i].set(th501rec.hfixdd02);
	   		wsaaTh501ArrayInner.wsaaTh501CapMm[i].set(th501rec.hfixmm01);
	   		wsaaTh501ArrayInner.wsaaTh501IntMm[i].set(th501rec.hfixmm02);
           i++;
       }
   }


protected void readFile2000()
	{
		/*READ-FILE*/
 //ILIFE-9080 start
		if (!iter.hasNext()) {
			batchID++;
			readChunkRecord();
			if (!iter.hasNext()) {
				wsspEdterror.set(Varcom.endp);
				return;
			}
			
		}
		 this.hdvxpf = iter.next();
		 ct01Value++;
 //ILIFE-9080
		/*EXIT*/
	}

protected void edit2500()
	{
 //ILIFE-9080 start
			wsspEdterror.set(SPACES);
			if (isEQ(hdvxpf.getChdrnum(),wsaaALockedChdrnum)) {
				ct03Value++;
				return ;
			}
			if (isNE(hdvxpf.getChdrnum(),lastChdrnum)) {
				softlockPolicy2600();
				if (isEQ(sftlockrec.statuz,"LOCK")) {
					wsaaALockedChdrnum.set(hdvxpf.getChdrnum());
					return ;
				}
				else {
					wsaaALockedChdrnum.set(SPACES);
				}
				lastChdrnum.set(hdvxpf.getChdrnum());
				wsaaJrnseq.set(ZERO);
			}
			wsspEdterror.set(varcom.oK);
 //ILIFE-9080 end
		}



protected void softlockPolicy2600()
	{
		softlockPolicy2610();
	}

protected void softlockPolicy2610()
	{
		SftlockRecBean sftlockRecBean = new SftlockRecBean();
    	sftlockRecBean.setFunction("LOCK");
    	sftlockRecBean.setCompany(hdvxpf.getChdrcoy()); //ILIFE-9080
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(hdvxpf.getChdrnum()); //ILIFE-9080
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (isEQ(sftlockRecBean.getStatuz(),"LOCK")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(hdvxpf.getChdrcoy()); //ILIFE-9080
			stringVariable1.append('|');
			stringVariable1.append(hdvxpf.getChdrnum()); //ILIFE-9080
			conlogrec.params.set(stringVariable1.toString());
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++; //ILIFE-9080
		
		}
    	else {
    	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
    		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
    			syserrrec.statuz.set(sftlockRecBean.getStatuz());
    			fatalError600();
    		}
    	}
	}

protected void update3000()
	{			
		if (cdivFlag) {			
			wsaaNewBonusDate.set(hdvxpf.getCbunst()); //ILIFE-9080
			while (isGT(bsscIO.getEffectiveDate(),wsaaNewBonusDate)){
					datcon2rec.intDate1.set(wsaaNewBonusDate);// CBUNST Date
				update3010();			
			}
		} else {
			datcon2rec.intDate1.set(hdvxpf.getCbunst());// CBUNST Date  //ILIFE-9080
			update3010();
		}		
		}

protected void update3010()
	{
		/*    Calculate new Bonus Date by adding 1 year to the CBUNST*/
		/*    of HDVXPF*/
		//datcon2rec.intDate1.set(hdvxpfRec.unitStatementDate);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNewBonusDate.set(datcon2rec.intDate2);
 //ILIFE-9080 start
		covrIO = null;
		if (covrpfMap != null && covrpfMap.containsKey(hdvxpf.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(hdvxpf.getChdrnum())) {
				if (c.getLife().equals(hdvxpf.getLife())
						&& c.getCoverage().equals(hdvxpf.getCoverage())
						&& c.getRider().equals(hdvxpf.getRider()) 
						&& c.getPlanSuffix() == hdvxpf.getPlnsfx()) {
					covrIO = c;
					break;
				}
			}
		}
		if(!(covrpfListInst.isEmpty())) {
			int maxtranno = 0;
			for(Covrpf c : covrpfListInst) {
				if(c.getChdrnum().equals(lastChdrnum.toString())) {
					if(maxtranno < c.getTranno()) {
						maxtranno = c.getTranno();
						covrIO = c;
					}
				}
			}
		}
		if (covrIO == null) {
			syserrrec.params.set(hdvxpf.getChdrnum());
			fatalError600();
		}
		
		if(cdivFlag){
			readth527();
			if(isEQ(th527rec.th527Rec, SPACES)) {	
				ct06Value++;
				return ;		
			}	
		}
		
		//change to map for all other programs 
		
		String chdrnum = hdvxpf.getChdrnum();
	    if (hcsdListMap != null && hcsdListMap.containsKey(chdrnum)) {
	        List<Hcsdpf> hcsdpfList = hcsdListMap.get(chdrnum);
	        for (Iterator<Hcsdpf> iterator = hcsdpfList.iterator(); iterator.hasNext(); ){
	        	Hcsdpf hcsd = iterator.next();
	        	if (hcsd.getLife().equals(hdvxpf.getLife())
	        		&&  hcsd.getCoverage().equals(hdvxpf.getCoverage())
	        		&& 	hcsd.getRider().equals(hdvxpf.getRider())
	        		&& Integer.compare(hcsd.getPlnsfx(),hdvxpf.getPlnsfx()) == 0 ) {
	        		hcsdpf = hcsd;
	        		break;
	        			
	        	}
	        }
	    }	
	    if(hcsdpf == null) {
	    	syserrrec.params.set(hdvxpf.getChdrnum());
			fatalError600();
	    }
		
 //ILIFE-9080 end

		/*    Match for the TH501 item with the Cash Dividend method held*/
		/*    on HCSD and WSAA-NEW-BONUS-DATE from array.*/
		for (ix.set(1); !(isGT(ix,wsaaTh501Size)
		|| isEQ(wsaaTh501ArrayInner.wsaaTh501Divdmth[ix.toInt()], SPACES)
		|| (isEQ(wsaaTh501ArrayInner.wsaaTh501Divdmth[ix.toInt()], hcsdpf.getZcshdivmth())
		&& isLTE(wsaaTh501ArrayInner.wsaaTh501Itmfrm[ix.toInt()], wsaaNewBonusDate))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaTh501Size)
		|| isEQ(wsaaTh501ArrayInner.wsaaTh501Divdmth[ix.toInt()], SPACES)) {
			itdmpf.setItemcoy(bsprIO.getCompany().toString());
			itdmpf.setItemtabl(th501);
			itdmpf.setItemitem(hcsdpf.getZcshdivmth());
			itdmpf.setItmfrm(wsaaNewBonusDate.toInt());
			fatalError600();
		}
		/*    Determined by the dividend allocation indicators in TH501,*/
		/*      TH501-IND-01  =  at due date*/
		/*      TH501-IND-02  =  on receipt of full premium*/
		/*      TH501-IND-03  =  on receipt of full premium + 1 instal.*/
		/*    Check if full premium has to be paid to allocate dividend or*/
		/*    skip process this HDVX record.*/
		if ((isEQ(th501rec.ind02,"X")
		&& isGT(wsaaNewBonusDate,hdvxpf.getPtdate()))
		|| (isEQ(th501rec.ind03,"X")
		&& isLTE(hdvxpf.getPtdate(),wsaaNewBonusDate))) {
			ct02Value++;
			return ;
		}

 //ILIFE-9080 start		
		//for loop not required since valid flag check is done
		if(chdrpfMap!=null &&chdrpfMap.containsKey(hdvxpf.getChdrnum())){
			for(Chdrpf c:chdrpfMap.get(hdvxpf.getChdrnum())){
				if(c.getChdrcoy().toString().equals(hdvxpf.getChdrcoy())){
					chdrpf = c;
					break;
				}
			}
		}
		if(chdrpf == null){
			syserrrec.params.set(hdvxpf.getChdrnum());
			fatalError600();
		}
		
		compute(wsaaNewTranno, 0).set(add(chdrpf.getTranno(),1));
	    if (hdisListMap != null && hdisListMap.containsKey(chdrnum)) {
	        List<Hdispf> hdispfList = hdisListMap.get(chdrnum);
	        for (Iterator<Hdispf> iterator = hdispfList.iterator(); iterator.hasNext(); ){
	        	Hdispf hdis = iterator.next();
	        	if (hdis.getLife().equals(hdvxpf.getLife())
	            		&&  hdis.getCoverage().equals(hdvxpf.getCoverage())
	            		&& 	hdis.getRider().equals(hdvxpf.getRider())
	            		&& Integer.compare(hdis.getPlnsfx(),hdvxpf.getPlnsfx()) == 0 ) {
	        			hdispf = hdis;
	            			break;
	            	}
	        }
	        
	 //ILIFE-9080 end

		/*    Calculate the Next Interest Allocation Date*/
			datcon4rec.intDate1.set(hdispf.getHintndt());
			wsaaHdisDate.set(hdispf.getHintndt());
			datcon4rec.frequency.set(wsaaTh501ArrayInner.wsaaTh501IntFq[ix.toInt()]);
			datcon4rec.freqFactor.set(1);
			datcon4rec.billmonth.set(hdispf.getHintduem());
			/*     MOVE HDIS-INT-DUE-DD    TO DTC4-BILLDAY                  */
			datcon4rec.billday.set(wsaaHdisDd);
			//ILIFE-9444, suggested as per ILIFE-9256
			if(hdispf.getHintduem().equals("02")) {
				datcon4rec.billday.set(28);
			}
			//ILIFE-9444 end
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				fatalError600();
			}
			wsaaNextIntDate.set(datcon4rec.intDate2);
 //ILIFE-9080 start
			/*    Calculate the Next Interest Capitalisation Date*/
			
			datcon4rec.intDate1.set(hdispf.getHcapndt());
			wsaaHdisDate.set(hdispf.getHcapndt());
			datcon4rec.frequency.set(wsaaTh501ArrayInner.wsaaTh501CapFq[ix.toInt()]);
			datcon4rec.freqFactor.set(1);
			datcon4rec.billmonth.set(hdispf.getHcapduem());
			/*     MOVE HDIS-CAP-DUE-DD    TO DTC4-BILLDAY                  */
			datcon4rec.billday.set(wsaaHdisDd);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				fatalError600();
			}
			wsaaNextCapDate.set(datcon4rec.intDate2);
 //ILIFE-9080 end
		}
		else {
			hdispf = new Hdispf();
			datcon2rec.intDate1.set(wsaaNewBonusDate);
			datcon2rec.frequency.set(wsaaTh501ArrayInner.wsaaTh501IntFq[ix.toInt()]);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaNextIntDate.set(datcon2rec.intDate2);
			if (isNE(wsaaTh501ArrayInner.wsaaTh501IntDd[ix.toInt()], SPACES)) {
				wsaaNextintDay.set(wsaaTh501ArrayInner.wsaaTh501IntDd[ix.toInt()]);
			}
			if (isNE(wsaaTh501ArrayInner.wsaaTh501IntMm[ix.toInt()], SPACES)) {
				if (isGT(wsaaTh501ArrayInner.wsaaTh501IntMm[ix.toInt()], wsaaNextintMth)) {
					wsaaNextintYear.subtract(1);
				}
				wsaaNextintMth.set(wsaaTh501ArrayInner.wsaaTh501IntMm[ix.toInt()]);
			}
			/*    Ensure this Next Interest Allocation date is valid*/
			datcon1rec.intDate.set(wsaaNextIntDate);
			datcon1rec.function.set("CONV");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				wsaaNextintDay.set(wsaaLastdd[wsaaNextintMth.toInt()]);
				if (isEQ(wsaaNextintMth,2)
				&& (setPrecision(wsaaNextintYear, 0)
				&& isEQ((mult((div(wsaaNextintYear,4)),4)),wsaaNextintYear))
				&& isNE(wsaaNextintYear,1900)) {
					wsaaNextintDay.add(1);
				}
			}
			
 //ILIFE-9080 start
			datcon2rec.intDate1.set(wsaaNewBonusDate);
			datcon2rec.frequency.set(wsaaTh501ArrayInner.wsaaTh501CapFq[ix.toInt()]);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaNextCapDate.set(datcon2rec.intDate2);
			if (isNE(wsaaTh501ArrayInner.wsaaTh501CapDd[ix.toInt()], SPACES)) {
				wsaaNextcapDay.set(wsaaTh501ArrayInner.wsaaTh501CapDd[ix.toInt()]);
			}
			if (isNE(wsaaTh501ArrayInner.wsaaTh501CapMm[ix.toInt()], SPACES)) {
				if (isGT(wsaaTh501ArrayInner.wsaaTh501CapMm[ix.toInt()], wsaaNextcapMth)) {
					wsaaNextcapYear.subtract(1);
				}
				wsaaNextcapMth.set(wsaaTh501ArrayInner.wsaaTh501CapMm[ix.toInt()]);
			}
			/*    Ensure this Next Interest Capitalisation date is valid*/
			datcon1rec.intDate.set(wsaaNextCapDate);
			datcon1rec.function.set("CONV");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				wsaaNextcapDay.set(wsaaLastdd[wsaaNextcapMth.toInt()]);
				if (isEQ(wsaaNextcapMth,2)
				&& (setPrecision(wsaaNextcapYear, 0)
				&& isEQ((mult((div(wsaaNextcapYear,4)),4)),wsaaNextcapYear))
				&& isNE(wsaaNextcapYear,1900)) {
					wsaaNextcapDay.add(1);
				}
			}
		
		}
 //ILIFE-9080 end
		/*    Find dividend allocation subroutine from T6639.*/
		for (ix.set(1); !(isGT(ix,wsaaT6639Size)
		|| isEQ(wsaaT6639Divdmth[ix.toInt()],SPACES)
		|| (isEQ(wsaaT6639Divdmth[ix.toInt()],hcsdpf.getZcshdivmth())
		&& isEQ(wsaaT6639Pstcd[ix.toInt()],hdvxpf.getPstatcode())
		&& isLTE(wsaaT6639Itmfrm[ix.toInt()],wsaaNewBonusDate))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaT6639Size)
		|| isEQ(wsaaT6639Divdmth[ix.toInt()],SPACES)
		|| isEQ(wsaaT6639Bonusprog[ix.toInt()],SPACES)) {
			itdmpf.setItemcoy(bsprIO.getCompany().toString());
			itdmpf.setItemtabl(t6639);
			itdmpf.setItemitem(hcsdpf.getZcshdivmth() + hdvxpf.getPstatcode());
			itdmpf.setItmfrm(wsaaNewBonusDate.toInt());
			fatalError600();
		}
		/*    Check if dividend allocation subroutine is defined*/
		if (isEQ(wsaaT6639Bonusprog[ix.toInt()],SPACES)) {
			fatalError600();
		}
		/*    Initialise the accumulate field for dividend amount*/
		wsaaDivdAmount.set(0);
		/*    Prepare to call cash dividend allocation subroutine*/
		initialize(hdivdrec.dividendRec);
 //ILIFE-9080 start
		hdivdrec.chdrChdrcoy.set(hdvxpf.getChdrcoy());
		hdivdrec.chdrChdrnum.set(hdvxpf.getChdrnum());
		hdivdrec.lifeLife.set(hdvxpf.getLife());
		hdivdrec.covrCoverage.set(hdvxpf.getCoverage());
		hdivdrec.covrRider.set(hdvxpf.getRider());
		hdivdrec.plnsfx.set(hdvxpf.getPlnsfx());
		hdivdrec.cntcurr.set(hdvxpf.getCntcurr());
		hdivdrec.cnttype.set(chdrpf.getCnttype());
		hdivdrec.transcd.set(bprdIO.getAuthCode());
		hdivdrec.premStatus.set(hdvxpf.getPstatcode());
		hdivdrec.crtable.set(hdvxpf.getCrtable());
		hdivdrec.sumin.set(hdvxpf.getSumins());
		hdivdrec.effectiveDate.set(bsscIO.getEffectiveDate());
		hdivdrec.ptdate.set(hdvxpf.getPtdate());
		hdivdrec.allocMethod.set(hdvxpf.getBnusin());
		hdivdrec.mortcls.set(covrIO.getMortcls());
		hdivdrec.sex.set(covrIO.getSex());
		hdivdrec.issuedAge.set(covrIO.getAnbAtCcd());
		hdivdrec.tranno.set(wsaaNewTranno);
		hdivdrec.divdCalcMethod.set(hcsdpf.getZcshdivmth());
		hdivdrec.periodFrom.set(hdvxpf.getCbunst());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(hdvxpf.getCrrcd());
		hdivdrec.cessDate.set(hdvxpf.getRcesdte());
 //ILIFE-9080 end
		wsaaBillfreq.set(chdrpf.getBillfreq());
		compute(hdivdrec.annualisedPrem, 3).setRounded(mult(covrIO.getInstprem(),wsaaBillfreq9));
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Allocation Subroutine*/
		callProgram(wsaaT6639Bonusprog[ix.toInt()], hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			fatalError600();
		}
		/* MOVE HDVD-DIVD-AMOUNT        TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO HDVD-DIVD-AMOUNT.            */
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history) if exists or create one.*/
		if (isNE(hdivdrec.divdAmount,0)) {
			zrdecplrec.amountIn.set(hdivdrec.divdAmount);
			callRounding8000();
			hdivdrec.divdAmount.set(zrdecplrec.amountOut);
			wsaaDivdAmount.add(hdivdrec.divdAmount);
 //ILIFE-9080 start
			Hdivpf hdivpf = new Hdivpf();
			hdivpf.setChdrcoy(hdvxpf.getChdrcoy());
			hdivpf.setChdrnum(hdvxpf.getChdrnum());
			hdivpf.setLife(hdvxpf.getLife());
			hdivpf.setJlife(hdvxpf.getJlife());
			hdivpf.setCoverage(hdvxpf.getCoverage());
			hdivpf.setRider(hdvxpf.getRider());
			hdivpf.setPlnsfx(hdvxpf.getPlnsfx());
			hdivpf.setTranno(wsaaNewTranno.toInt());
			hdivpf.setEffdate(wsaaNewBonusDate.toInt());
			hdivpf.setHdvaldt(bsscIO.getEffectiveDate().toInt());
			hdivpf.setHincapdt(wsaaNextCapDate.toInt());
			hdivpf.setCntcurr(hdvxpf.getCntcurr());
			hdivpf.setHdvamt(hdivdrec.divdAmount.getbigdata());
			hdivpf.setHdvrate(hdivdrec.divdRate.getbigdata());
			hdivpf.setHdveffdt(hdivdrec.rateDate.toInt());
			hdivpf.setBatccoy(batcdorrec.company.toString());
			hdivpf.setBatcbrn(batcdorrec.branch.toString());
			hdivpf.setBatcactyr(batcdorrec.actyear.toInt());
			hdivpf.setBatcactmn(batcdorrec.actmonth.toInt());
			hdivpf.setBatctrcde(batcdorrec.trcde.toString());
			hdivpf.setBatcbatch(batcdorrec.batch.toString());
			hdivpf.setHdvtyp("D");
			hdivpf.setZdivopt(hcsdpf.getZdivopt());
			hdivpf.setZcshdivmth(hcsdpf.getZcshdivmth());
			hdivpf.setHdvopttx(0);
			hdivpf.setHdvcaptx(0);
			hdivpf.setHdvsmtno(0);
			hdivpf.setHpuanbr(0);
			if (hdivpfList == null) {
				hdivpfList = new ArrayList<>();
	        }
			hdivpfList.add(hdivpf);
 //ILIFE-9080 end
			
			hdispf.setChdrcoy(hdivpf.getChdrcoy());
			hdispf.setChdrnum(hdivpf.getChdrnum());
			hdispf.setLife(hdivpf.getLife());
			hdispf.setCoverage(hdivpf.getCoverage());
			hdispf.setRider(hdivpf.getRider());
			hdispf.setPlnsfx(hdivpf.getPlnsfx());
			hdispfList = hdispfDAO.searchHdisRecord(hdispf);
			if (!hdispfList.isEmpty()) {
				for(Hdispf hdis : hdispfList) {
					hdispf = hdis;
				}
			}
			if (hdispfList.isEmpty()) {
				hdispf.setJlife(hdivpf.getJlife());
				hdispf.setCntcurr(hdivpf.getCntcurr());
				hdispf.setValidflag("1");
				hdispf.setTranno(wsaaNewTranno.toInt());
				hdispf.setHdv1stdt(wsaaNewBonusDate.toInt());
				hdispf.setHdvldt(wsaaNewBonusDate.toInt());
				hdispf.setHintldt(wsaaNewBonusDate.toInt());
				hdispf.setHcapldt(wsaaNewBonusDate.toInt());
				hdispf.setHintndt(wsaaNextIntDate.toInt());
				hdispf.setHcapndt(wsaaNextCapDate.toInt());
				hdispf.setHdvball(new BigDecimal(hdivdrec.divdAmount.toInt()));
				hdispf.setHdvbalc(new BigDecimal(hdivdrec.divdAmount.toInt()));
				hdispf.setHintos(new BigDecimal(0));
				hdispf.setHdvsmtno(0);
				hdispf.setHdvbalst(new BigDecimal(0));
				hdispf.setHdvsmtdt(varcom.vrcmMaxDate.toInt());
				/*    Set the due month and day for interest allocation and*/
				/*    capitalisation.  Since the due month and due day must be*/
				/*    compatible, say 06 and 31, then make them 05 and 31 instead.*/
				/*    Note: due month and day on HDIS once written is permanent.*/
				if (isNE(wsaaTh501ArrayInner.wsaaTh501IntDd[ix.toInt()], SPACES)) {
					hdispf.setHintdued(wsaaTh501ArrayInner.wsaaTh501IntDd[ix.toInt()].toString());
				}
				else {
					hdispf.setHintdued(wsaaNextintDay.toString());
				}
				if (isLTE(hdispf.getHintdued(),wsaaLastdd[wsaaNextintMth.toInt()])
				|| (isEQ(wsaaNextintMth,2)
				&& isEQ(hdispf.getHintdued(),29))) {
					hdispf.setHintduem(wsaaNextintMth.toString());
				}
				else {
					wsaaNextintMth.subtract(1);
					hdispf.setHintduem(wsaaNextintMth.toString());
					if (isEQ(wsaaNextintMth,0)) {
						hdispf.setHintduem("12");
					}
				}
				if (isNE(wsaaTh501ArrayInner.wsaaTh501CapDd[ix.toInt()], SPACES)) {
					hdispf.setHcapdued(wsaaTh501ArrayInner.wsaaTh501CapDd[ix.toInt()].toString());
				}
				else {
					hdispf.setHcapdued(wsaaNextcapDay.toString());
				}
				if (isLTE(hdispf.getHcapdued(),wsaaLastdd[wsaaNextcapMth.toInt()])
				|| (isEQ(wsaaNextcapMth,2)
				&& isEQ(hdispf.getHcapdued(),29))) {
					hdispf.setHcapduem(wsaaNextcapMth.toString());
				}
				else {
					wsaaNextcapMth.subtract(1);
					hdispf.setHcapduem(wsaaNextcapMth.toString());
					if (isEQ(wsaaNextcapMth,0)) {
						hdispf.setHcapduem("12");
					}
				}
				if(hdisinsertallList==null){
					hdisinsertallList = new ArrayList<>(); //ILIFE-9080
				 }
				hdisinsertallList.add(hdispf); //ILIFE-9080
			}
			else {
				Hdispf hdispfupdt = new Hdispf(hdispf);
				hdispfupdt.setValidflag("2");
				hdisupdateList.add(hdispfupdt);
				//hdispfDAO.updateHdisValidflag(hdispf);
				Hdispf hdispfinst = new Hdispf(hdispf);
				hdispfinst.setValidflag("1");
				hdispfinst.setTranno(wsaaNewTranno.toInt());
				hdispfinst.setHdvldt(wsaaNewBonusDate.toInt());
				setPrecision(hdispfinst.getHdvball(), 2);
				hdispfinst.setHdvball(add(hdispf.getHdvball(),hdivdrec.divdAmount).getbigdata());
				setPrecision(hdispfinst.getHdvbalc(), 2);
				hdispfinst.setHdvbalc(add(hdispf.getHdvbalc(),hdivdrec.divdAmount).getbigdata());
			    if(hdisList==null){
				   hdisList = new ArrayList<>(); //ILIFE-9080
		        }
				hdisList.add(hdispfinst);
			}
		}
		/*    Allocate Dividend to Paid-up Additions*/
		hpuapf.setChdrcoy(hdivdrec.chdrChdrcoy.toString());
		hpuapf.setChdrnum(hdivdrec.chdrChdrnum.toString());
		hpuapf.setLife(hdivdrec.lifeLife.toString());
		hpuapf.setCoverage(hdivdrec.covrCoverage.toString());
		hpuapf.setRider(hdivdrec.covrRider.toString());
		hpuapf.setPlanSuffix(hdivdrec.plnsfx.toInt());
		hpuapf.setPuAddNbr(0);
		hpuapfList = hpuapfDAO.selectHpuaRecord(hpuapf);
		if ((!hpuapfList.isEmpty() && hpuapfList != null)
			|| isNE(hpuapf.getChdrcoy(),hdivdrec.chdrChdrcoy)
			|| isNE(hpuapf.getChdrnum(),hdivdrec.chdrChdrnum)
			|| isNE(hpuapf.getLife(),hdivdrec.lifeLife)
			|| isNE(hpuapf.getCoverage(),hdivdrec.covrCoverage)
			|| isNE(hpuapf.getRider(),hdivdrec.covrRider)
			|| isNE(hpuapf.getPlanSuffix(),hdivdrec.plnsfx)){
			for (Hpuapf hpua : hpuapfList) {
				dividendPaidup3100(hpua);
			}
		}	

		if (isNE(wsaaDivdAmount,0)) {
			writeAccounting3200();
		}
 //ILIFE-9080 start
		Covrpf covrpf = new Covrpf(covrIO);
		covrpf.setUniqueNumber(covrIO.getUniqueNumber());
		covrpf.setValidflag("2");
		covrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
		if(covrpfListUpdt==null){
			covrpfListUpdt = new ArrayList<>();
		}
		this.covrpfListUpdt.add(covrpf);
		Covrpf covrpfinst = new Covrpf(covrIO);
		covrpfinst.setValidflag("1");
		covrpfinst.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		covrpfinst.setCurrto(varcom.vrcmMaxDate.toInt());
		covrpfinst.setUnitStatementDate(wsaaNewBonusDate.toInt());
		covrpfinst.setTranno(wsaaNewTranno.toInt());
		if(covrpfListInst==null){
			covrpfListInst = new ArrayList<>();
		}
		this.covrpfListInst.add(covrpfinst);
		
		Chdrpf pf = new Chdrpf(chdrpf);
		pf.setUniqueNumber(chdrpf.getUniqueNumber());
		pf.setValidflag("2".charAt(0));
		pf.setCurrto(bsscIO.getEffectiveDate().toInt());
		if(chdrpfListUpdt==null){
			chdrpfListUpdt = new ArrayList<>();
		}
		this.chdrpfListUpdt.add(pf);
		Chdrpf chdrpfinst = new Chdrpf(chdrpf);
		chdrpfinst.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrpfinst.setValidflag("1".charAt(0));
		chdrpfinst.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		chdrpfinst.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpfinst.setTranno(wsaaNewTranno.toInt());
		if(chdrpfListInst==null){
			chdrpfListInst = new ArrayList<>();
		}
		this.chdrpfListInst.add(chdrpfinst);
		
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setChdrcoy(hdvxpf.getChdrcoy()); //ILIFE-9080
		ptrnpf.setChdrnum(hdvxpf.getChdrnum()); //ILIFE-9080
		ptrnpf.setTranno(wsaaNewTranno.toInt());
		ptrnpf.setValidflag("1");
		ptrnpf.setPtrneff(wsaaNewBonusDate.toInt());
		ptrnpf.setTermid(" ");
		ptrnpf.setTrdt(wsaaSysDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTimen.toInt());
		ptrnpf.setUserT(999999);
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		if(ptrnpfList == null){
			ptrnpfList = new ArrayList<>();
		}
		ptrnpfList.add(ptrnpf);
		
		 //ILIFE-9080 end
		agntpf = agntpfDAO.getClientData(fsupfxcpy.agnt.toString(), chdrpf.getAgntcoy().toString(), chdrpf.getAgntnum());
		if (agntpf == null) {
			fatalError600();
		}
		
		ustm.setClntpfx(fsupfxcpy.clnt.toString());
		ustm.setClntcoy(agntpf.getClntcoy());
		ustm.setClntnum(agntpf.getClntnum());
	}

 	protected void readth527()
 	{
 		
 		wsaaCrtable.set(hdvxpf.getCrtable()); //ILIFE-9080
		wsaaIssuedAge.set(covrIO.getAnbAtCcd());
		wsaaMortcls.set(covrIO.getMortcls());		
		wsaaSex.set(covrIO.getSex());
		
		itempfList = itempfDAO.findBy("IT",hdvxpf.getChdrcoy(),th527,wsaaItem.toString().trim(),SPACES.toString()); //ILIFE-9080

		if(itempfList.isEmpty()){ 
					
			while(itempfList.isEmpty()){
	 		
				if (isEQ(subString(wsaaItem, 5, 4),"****")) {
					itdmpf.setItemitem(wsaaItem.toString());
					fatalError600();
				}
				else {
					if (isNE(subString(wsaaItem, 8, 1),"*")) {
						wsaaItem.setSub1String(8, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 7, 1),"*")) {
							wsaaItem.setSub1String(7, 1, "*");
						}
						else {
							if (isNE(subString(wsaaItem, 5, 2),"**")) {
								wsaaItem.setSub1String(5, 2, "**");
							}
						}
					}
				}
				itempfList = itempfDAO.findBy("IT",hdvxpf.getChdrcoy(),th527,wsaaItem.toString().trim(),SPACES.toString()); //ILIFE-9080
	 		}
	 				
	 	}			
		itempfListIterator = itempfList.iterator();
		while (itempfListIterator.hasNext()) {
			itempf = new Itempf(); //ILIFE-9080
			itempf = itempfListIterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((wsaaNewBonusDate.toInt() >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& wsaaNewBonusDate.toInt() <= Integer.parseInt(itempf.getItmto().toString())){
					th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
					return;
				}else{
					th527rec.th527Rec.set(SPACES);
				}
			}				
		}		
		}
protected void dividendPaidup3100(Hpuapf hpuapf)
	{
					dividend3110(hpuapf);
					//nextHpua3180();
				}

protected void dividend3110(Hpuapf hpuapf)
	{
		if (isNE(hpuapf.getDivdParticipant(),"Y")) {
			return ;
		}
		for (ix.set(1); !(isGT(ix,wsaaT6639Size)
		|| isEQ(wsaaT6639Divdmth[ix.toInt()],SPACES)
		|| (isEQ(wsaaT6639Divdmth[ix.toInt()],hcsdpf.getZcshdivmth())
		&& isEQ(wsaaT6639Pstcd[ix.toInt()],hpuapf.getPstatcode())
		&& isLTE(wsaaT6639Itmfrm[ix.toInt()],wsaaNewBonusDate))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaT6639Size)
		|| isEQ(wsaaT6639Divdmth[ix.toInt()],SPACES)) {
			/*     MOVE BEGN               TO ITDM-FUNCTION*/
			/*     MOVE ENDP               TO ITDM-STATUZ*/
			/*     MOVE BSPR-COMPANY       TO ITDM-ITEMCOY*/
			/*     MOVE T6639              TO ITDM-ITEMTABL*/
			/*     STRING HCSD-ZCSHDIVMTH, HPUA-PSTATCODE*/
			/*                             DELIMITED BY SIZE*/
			/*                             INTO ITDM-ITEMITEM*/
			/*     MOVE WSAA-NEW-BONUS-DATE TO ITDM-ITMFRM*/
			/*     MOVE ITDM-PARAMS        TO SYSR-PARAMS*/
			/*     PERFORM 600-FATAL-ERROR*/
			return ;
		}
		if (isEQ(wsaaT6639Bonusprog[ix.toInt()],SPACES)) {
		//	itdmIO.setStatuz(hl17);
			//syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		initialize(hdivdrec.dividendRec);
 //ILIFE-9080 start
		hdivdrec.chdrChdrcoy.set(hdvxpf.getChdrcoy());
		hdivdrec.chdrChdrnum.set(hdvxpf.getChdrnum());
		hdivdrec.lifeLife.set(hdvxpf.getLife());
		hdivdrec.covrCoverage.set(hdvxpf.getCoverage());
		hdivdrec.covrRider.set(hdvxpf.getRider());
		hdivdrec.plnsfx.set(hdvxpf.getPlnsfx());
		hdivdrec.cntcurr.set(hdvxpf.getCntcurr());
		hdivdrec.cnttype.set(chdrpf.getCnttype());
		hdivdrec.transcd.set(bprdIO.getAuthCode());
		hdivdrec.premStatus.set(hpuapf.getPstatcode());
		hdivdrec.crtable.set(hpuapf.getCrtable());
		hdivdrec.sumin.set(hpuapf.getSumin());
		hdivdrec.effectiveDate.set(bsscIO.getEffectiveDate());
		hdivdrec.ptdate.set(hdvxpf.getPtdate());
		hdivdrec.allocMethod.set(hdvxpf.getBnusin());
		hdivdrec.mortcls.set(covrIO.getMortcls());
		hdivdrec.sex.set(covrIO.getSex());
		hdivdrec.issuedAge.set(hpuapf.getAnbAtCcd());
		hdivdrec.tranno.set(wsaaNewTranno);
		hdivdrec.divdCalcMethod.set(hcsdpf.getZcshdivmth());
		hdivdrec.periodFrom.set(hdvxpf.getCbunst());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(hpuapf.getCrrcd());
		hdivdrec.cessDate.set(hpuapf.getRiskCessDate());
		hdivdrec.annualisedPrem.set(hpuapf.getSingp());
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		callProgram(wsaaT6639Bonusprog[ix.toInt()], hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			fatalError600();
		}
		if (isNE(hdivdrec.divdAmount,0)) {
			zrdecplrec.amountIn.set(hdivdrec.divdAmount);
			callRounding8000();
			hdivdrec.divdAmount.set(zrdecplrec.amountOut);
			Hdivpf hdivpf = new Hdivpf();
			wsaaDivdAmount.add(hdivdrec.divdAmount);
			hdivpf.setChdrcoy(hdvxpf.getChdrcoy());
			hdivpf.setChdrnum(hdvxpf.getChdrnum());
			hdivpf.setLife(hdvxpf.getLife());
			hdivpf.setJlife(hdvxpf.getJlife());
			hdivpf.setCoverage(hdvxpf.getCoverage());
			hdivpf.setRider(hdvxpf.getRider());
			hdivpf.setPlnsfx(hdvxpf.getPlnsfx());
			hdivpf.setTranno(wsaaNewTranno.toInt());
			hdivpf.setEffdate(wsaaNewBonusDate.toInt());
			hdivpf.setHdvaldt(bsscIO.getEffectiveDate().toInt());
			hdivpf.setHincapdt(wsaaNextCapDate.toInt());
			hdivpf.setCntcurr(hdvxpf.getCntcurr());
			hdivpf.setHdvamt(hdivdrec.divdAmount.getbigdata());
			hdivpf.setHdvrate(hdivdrec.divdRate.getbigdata());
			hdivpf.setHdveffdt(hdivdrec.rateDate.toInt());
			hdivpf.setBatccoy(batcdorrec.company.toString());
			hdivpf.setBatcbrn(batcdorrec.branch.toString());
			hdivpf.setBatcactyr(batcdorrec.actyear.toInt());
			hdivpf.setBatcactmn(batcdorrec.actmonth.toInt());
			hdivpf.setBatctrcde(batcdorrec.trcde.toString());
			hdivpf.setBatcbatch(batcdorrec.batch.toString());
			hdivpf.setHdvtyp("D");
			hdivpf.setZdivopt(hcsdpf.getZdivopt());
			hdivpf.setZcshdivmth(hcsdpf.getZcshdivmth());
			hdivpf.setHdvopttx(0);
			hdivpf.setHdvcaptx(0);
			hdivpf.setHdvsmtno(0);
			hdivpf.setHpuanbr(0);
			if(hdivpfList==null){
				hdivpfList = new ArrayList<>(); //ILIFE-9080
		        }
			hdivpfList.add(hdivpf);
			
			hdispf.setChdrcoy(hdivpf.getChdrcoy());
			hdispf.setChdrnum(hdivpf.getChdrnum());
			hdispf.setLife(hdivpf.getLife());
			hdispf.setCoverage(hdivpf.getCoverage());
			hdispf.setRider(hdivpf.getRider());
			hdispf.setPlnsfx(hdivpf.getPlnsfx());
			hdispfList = hdispfDAO.searchHdisRecord(hdispf);
			if(hdispfList.isEmpty()) {
				fatalError600();
			}
			if (!hdispfList.isEmpty()) {
				for(Hdispf hdis : hdispfList) {
					hdispf = hdis;
				}
			}
			if(!(hdisList.isEmpty())) {
	        	int maxtranno = 0;
				for(Hdispf h : hdisList) {
					if(h.getChdrnum().equals(lastChdrnum.toString())) {
						if(maxtranno < h.getTranno()) {
							maxtranno = h.getTranno();
							hdispf = h;
						}
					}
				}
	        }
			if (isEQ(hdispf.getTranno(),wsaaNewTranno)) {
				Hdispf hdispfupdt = new Hdispf(hdispf);
				setPrecision(hdispfupdt.getHdvball(), 2);
				hdispfupdt.setHdvball(add(hdispf.getHdvball(),hdivdrec.divdAmount).getbigdata());
				setPrecision(hdispfupdt.getHdvbalc(), 2);
				hdispfupdt.setHdvbalc(add(hdispf.getHdvbalc(),hdivdrec.divdAmount).getbigdata());
				hdisupdateList.add(hdispfupdt);
				//hdispfDAO.updateHdisRecords(hdispfupdt);
			}
			else {
				Hdispf hdispfupdt = new Hdispf(hdispf);
				hdispfupdt.setValidflag("2");
				hdisupdateList.add(hdispfupdt);
				Hdispf hdispfinst = new Hdispf(hdispf);
				hdispfinst.setValidflag("1");
				hdispfinst.setTranno(wsaaNewTranno.toInt());
				hdispfinst.setHdvldt(wsaaNewBonusDate.toInt());
				setPrecision(hdispfinst.getHdvball(), 2);
				hdispfinst.setHdvball(add(hdispf.getHdvball(),hdivdrec.divdAmount).getbigdata());
				setPrecision(hdispfinst.getHdvbalc(), 2);
				hdispfinst.setHdvbalc(add(hdispf.getHdvbalc(),hdivdrec.divdAmount).getbigdata());
				if (hdisList == null) {
					 hdisList = new ArrayList<>();
		        }
				hdisList.add(hdispfinst);
 //ILIFE-9080 end
				
			}
		}
	}

protected void writeAccounting3200()
	{
		accounting3210();
	}

protected void accounting3210()
	{
		for (ix.set(1); !(isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)
		|| (isEQ(wsaaT5688Cnttype[ix.toInt()],chdrpf.getCnttype())
		&& isLTE(wsaaT5688Itmfrm[ix.toInt()],chdrpf.getOccdate()))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)) {
			//itdmIO.setFunction(varcom.begn);
			//itdmIO.setStatuz(varcom.endp);
			itdmpf.setItemcoy(bsprIO.getCompany().toString());
			itdmpf.setItemtabl(t5688);
			itdmpf.setItemitem(chdrpf.getCnttype());
			itdmpf.setItmfrm(chdrpf.getOccdate());
			//syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		wsaaComponLevelAccounted.set(wsaaT5688Comlvlacc[ix.toInt()]);
		lifacmvrec.origcurr.set(hdvxpf.getCntcurr());
		lifacmvrec.origamt.set(wsaaDivdAmount);
		wsaaJrnseq.add(1);
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(3);
			lifacmvrec.substituteCode[6].set(hdvxpf.getCrtable());
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(hdvxpf.getPlnsfx());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hdvxpf.getChdrnum());
			stringVariable1.addExpression(hdvxpf.getLife());
			stringVariable1.addExpression(hdvxpf.getCoverage());
			stringVariable1.addExpression(hdvxpf.getRider());
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
			x1000CallLifacmv();
		}
		else {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(hdvxpf.getChdrnum()); //ILIFE-9080
			x1000CallLifacmv();
		}
		wsaaJrnseq.add(1);
		t5645Ix.set(2);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(hdvxpf.getChdrnum()); //ILIFE-9080
		x1000CallLifacmv();
		ct04Value++; //ILIFE-9080
		ct05Value = ct05Value.add(wsaaDivdAmount.getbigdata()); //ILIFE-9080
		
	}

protected void commit3500()
	{
		/*COMMIT*/
	 //ILIFE-9080 start
	commitControlTotals();
	if (covrpfListInst != null && !covrpfListInst.isEmpty()) {
		covrpfDAO.insertCovrBulk(covrpfListInst); //ILB-1080
		covrpfListInst.clear();
	}
	
	if (covrpfListUpdt != null && !covrpfListUpdt.isEmpty()) {
		covrpfDAO.updateCovrValidFlagByTranno(covrpfListUpdt);
		covrpfListUpdt.clear();
	}
	
	if (chdrpfListInst != null && !chdrpfListInst.isEmpty()) {
		chdrpfDAO.insertChdrValidRecord(chdrpfListInst);
		chdrpfListInst.clear();
	}
	
	if (chdrpfListUpdt != null && !chdrpfListUpdt.isEmpty()) {
		chdrpfDAO.updateChdrRecordByTranno(chdrpfListUpdt);
		chdrpfListUpdt.clear();
	}
	
	if (ptrnpfList != null && !ptrnpfList.isEmpty()) {
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
	}
	
    if (hdivpfList != null && !hdivpfList.isEmpty()) {
    	hdivpfDAO.insertHdivpf(hdivpfList);
    	hdivpfList.clear();
    }
    
    if( hdisList != null && !hdisList.isEmpty()) {
    	hdispfDAO.insertHdisRecords(hdisList);
    	hdisList.clear();
    }
    
    if( hdisinsertallList != null && !hdisinsertallList.isEmpty()) {
    	hdispfDAO.insertAllRecords(hdisinsertallList);
    	hdisinsertallList.clear();
    }
    
    if( hdisupdateList != null && !hdisupdateList.isEmpty()) {
    	hdispfDAO.updateHdisRecord(hdisupdateList);
    	hdisupdateList.clear();
    }
    
    
     //ILIFE-9080 end
		/*EXIT*/
	}
 //ILIFE-9080 start
private void commitControlTotals(){ 
	
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Value);
	callContot001();
	
	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Value);
	callContot001();
	
	contotrec.totno.set(ct03);
	contotrec.totval.set(ct03Value);
	callContot001();
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Value);
	callContot001();
	
	contotrec.totno.set(ct05);
	contotrec.totval.set(ct05Value);
	callContot001();
	
	contotrec.totno.set(ct06);
	contotrec.totval.set(ct06Value);
	callContot001();
	
}
 //ILIFE-9080 end
protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
	 //ILIFE-9080 start
		if (chdrpfMap != null) {
			chdrpfMap.clear();
		}
		
		if (covrpfMap != null) {
			covrpfMap.clear();
		}
		
		if (hdisListMap != null) {
			hdisListMap.clear();
		}
		
		if (hcsdListMap != null) {
			hcsdListMap.clear();
		}
		
		covrpfListUpdt.clear();
		covrpfListUpdt=null;
		
		covrpfListInst.clear();
		covrpfListInst=null;
		
		chdrpfListInst.clear();
		chdrpfListInst=null;
		
		chdrpfListUpdt.clear();
		chdrpfListUpdt=null;
		
		ptrnpfList.clear();
		ptrnpfList=null;
		
		hdivpfList.clear();
		hdivpfList=null;
		
		hdisinsertallList.clear();
		hdisinsertallList=null;
		
		hdisList.clear();
		hdisList=null;
	 //ILIFE-9080 end
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(hdivdrec.cntcurr);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void x1000CallLifacmv()
	{
			x1010Call();
		}

protected void x1010Call()
	{
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		/*    IF  COMPON-LEVEL-ACCOUNTED                                   */
		/*       MOVE SPACES              TO LIFA-RLDGACCT                 */
		/*       MOVE PLNSFX OF HDVXPF-REC TO WSAA-PLAN                    */
		/*       STRING CHDRNUM  OF HDVXPF-REC                             */
		/*              LIFE     OF HDVXPF-REC                             */
		/*              COVERAGE OF HDVXPF-REC                             */
		/*              RIDER    OF HDVXPF-REC                             */
		/*              WSAA-PLAN                                          */
		/*              DELIMITED BY SIZE INTO LIFA-RLDGACCT               */
		/*    ELSE                                                         */
		/*       MOVE CHDRNUM OF HDVXPF-REC TO LIFA-RLDGACCT               */
		/*    END-IF.                                                      */
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		lifacmvrec.rdocnum.set(hdvxpf.getChdrnum()); //ILIFE-9080
		lifacmvrec.tranref.set(hdvxpf.getChdrnum()); //ILIFE-9080
		lifacmvrec.tranno.set(wsaaNewTranno);
		lifacmvrec.effdate.set(wsaaNewBonusDate);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure WSAA-TH501-ARRAY--INNER
 */
private static final class WsaaTh501ArrayInner {

	private FixedLengthStringData wsaaTh501Array = new FixedLengthStringData(31000);
	private FixedLengthStringData[] wsaaTh501Rec = FLSArrayPartOfStructure(1000, 31, wsaaTh501Array, 0);
	private FixedLengthStringData[] wsaaTh501Key = FLSDArrayPartOfArrayStructure(9, wsaaTh501Rec, 0);
	private FixedLengthStringData[] wsaaTh501Divdmth = FLSDArrayPartOfArrayStructure(4, wsaaTh501Key, 0, SPACES);
	private PackedDecimalData[] wsaaTh501Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaTh501Key, 4);
	private FixedLengthStringData[] wsaaTh501Data = FLSDArrayPartOfArrayStructure(22, wsaaTh501Rec, 9);
	private FixedLengthStringData[] wsaaTh501CapFq = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 0);
	private FixedLengthStringData[] wsaaTh501IntFq = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 2);
	private FixedLengthStringData[] wsaaTh501CapDd = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 4);
	private FixedLengthStringData[] wsaaTh501IntDd = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 6);
	private FixedLengthStringData[] wsaaTh501CapMm = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 8);
	private FixedLengthStringData[] wsaaTh501IntMm = FLSDArrayPartOfArrayStructure(2, wsaaTh501Data, 10);
	private FixedLengthStringData[] wsaaTh501Calcprog = FLSDArrayPartOfArrayStructure(10, wsaaTh501Data, 12);
}
}
