package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:32
 * Description:
 * Copybook name: HDIVINTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivintkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivintFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivintKey = new FixedLengthStringData(64).isAPartOf(hdivintFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivintChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivintKey, 0);
  	public FixedLengthStringData hdivintChdrnum = new FixedLengthStringData(8).isAPartOf(hdivintKey, 1);
  	public FixedLengthStringData hdivintLife = new FixedLengthStringData(2).isAPartOf(hdivintKey, 9);
  	public FixedLengthStringData hdivintCoverage = new FixedLengthStringData(2).isAPartOf(hdivintKey, 11);
  	public FixedLengthStringData hdivintRider = new FixedLengthStringData(2).isAPartOf(hdivintKey, 13);
  	public PackedDecimalData hdivintPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hdivintKey, 15);
  	public PackedDecimalData hdivintDivdIntCapDate = new PackedDecimalData(8, 0).isAPartOf(hdivintKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(hdivintKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivintFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivintFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}