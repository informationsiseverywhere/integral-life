package com.csc.life.cashdividends.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH504
 * @version 1.0 generated on 30/08/09 07:01
 * @author Quipoz
 */
public class Sh504ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1431);
	public FixedLengthStringData dataFields = new FixedLengthStringData(539).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bankaccreq = DD.bankaccreq.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,48);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,51);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,182);
	public ZonedDecimalData numapp = DD.numapp.copyToZonedDecimal().isAPartOf(dataFields,183);
	public ZonedDecimalData numavail = DD.numavail.copyToZonedDecimal().isAPartOf(dataFields,187);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,191);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(dataFields,192);
	public FixedLengthStringData payeesel = DD.payeesel.copy().isAPartOf(dataFields,195);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,207);
	public ZonedDecimalData premCessDate = DD.pcesdte.copyToZonedDecimal().isAPartOf(dataFields,208);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,216);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,219);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,222);
	public FixedLengthStringData pymdesc = DD.pymdesc.copy().isAPartOf(dataFields,226);
	public FixedLengthStringData ratypind = DD.ratypind.copy().isAPartOf(dataFields,256);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,257);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,265);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,268);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,271);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,273);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,274);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,277);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,281);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,296);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,313);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,314);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,327);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(dataFields,344);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,348);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 365);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 368);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 398);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 415);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 432);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 449);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 466); 
	public FixedLengthStringData payeenme = DD.payeenme.copy().isAPartOf(dataFields,483);
	/*BRD-306 END */
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,533);//ILIFE-3421
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,534);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,537);
	public FixedLengthStringData aepaydet = DD.optextind.copy().isAPartOf(dataFields,538);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(220).isAPartOf(dataArea, 539);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankaccreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData numappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData numavailErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData paycurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData payeeselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData paymthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData pcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData pymdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData ratypindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData zdivoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData payeenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	/*BRD-306 END */
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);//ILIFE-3421
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData aepaydetErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(672).isAPartOf(dataArea, 759);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankaccreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] numappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] numavailOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] paycurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] payeeselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] paymthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] pcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] pymdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] ratypindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] zdivoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] payeenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
/*BRD-306 END */
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);//ILIFE-3421
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] aepaydetOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] riskCessAgeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);//ILJ-43
//	public FixedLengthStringData[] riskCessDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);//ILJ-43
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData premCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);

	public LongData Sh504screenWritten = new LongData(0);
	public LongData Sh504protectWritten = new LongData(0);
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return false;
	}


	public Sh504ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"14","02","-14","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcessageOut,new String[] {"15","03","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"17","41","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"19","06","-19","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"20","08","-20","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {"36","37","-36","37",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"21","09","-21","09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"22","31","-22","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratypindOut,new String[] {"23","40","-01","40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"33","34","-33","34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(polincOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"24","26","-23","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numavailOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numappOut,new String[] {"25","11","-24","10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdivoptOut,new String[] {"42","35","-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeeselOut,new String[] {"43","35","-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paymthOut,new String[] {"44","35","-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paycurrOut,new String[] {"45","35","-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zlinstpremOut,new String[] {null, null, null, "32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {"47", "74", "-47", "46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"47","48","-47","47",null, null, null, null, null, null, null, null});
		
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "56", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "57", null, null, null, null, null, null, null, null});
        /*BRD-306 END */
		fieldIndMap.put(prmbasisOut,new String[] {"59", "60", "-59", "58", null, null, null, null, null, null, null, null});//ILIFE-3421
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "73", null, null, null, null, null, null, null, null});
		fieldIndMap.put(aepaydetOut,new String[] {null, null,null, "75", null, null, null, null, null, null, null, null});
		//fieldIndMap.put(riskCessAgeOut,new String[] {null, null, null, "123", null, null, null, null, null, null, null, null});//ILJ-43
	//	fieldIndMap.put(riskCessDateOut,new String[] {null, null, null, "124", null, null, null, null, null, null, null, null});//ILJ-43

		screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, sumin, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, mortcls, liencd, bappmeth, optextind, instPrem, ratypind, pbind, polinc, select, numavail, numapp, zdivopt, payeesel, paymth, pymdesc, paycurr, bankaccreq, zlinstprem, taxamt, taxind, loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt,payeenme, prmbasis, dialdownoption,exclind,aepaydet};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, suminOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, mortclsOut, liencdOut, bappmethOut, optextindOut, instprmOut, ratypindOut, pbindOut, polincOut, selectOut, numavailOut, numappOut, zdivoptOut, payeeselOut, paymthOut, pymdescOut, paycurrOut, bankaccreqOut, zlinstpremOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut, adjustageamtOut,payeenmeOut,prmbasisOut, dialdownoptionOut,exclindOut,aepaydetOut,riskCessAgeOut};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, suminErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, mortclsErr, liencdErr, bappmethErr, optextindErr, instprmErr, ratypindErr, pbindErr, polincErr, selectErr, numavailErr, numappErr, zdivoptErr, payeeselErr, paymthErr, pymdescErr, paycurrErr, bankaccreqErr, zlinstpremErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr, adjustageamtErr,payeenmeErr,prmbasisErr, dialdownoptionErr,exclindErr,aepaydetErr};		screenDateFields = new BaseData[] {riskCessDate, premCessDate};
		screenDateErrFields = new BaseData[] {rcesdteErr, pcesdteErr};
		screenDateDispFields = new BaseData[] {riskCessDateDisp, premCessDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh504screen.class;
		protectRecord = Sh504protect.class;
	}

}