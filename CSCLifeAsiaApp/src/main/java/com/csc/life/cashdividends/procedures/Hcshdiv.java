/*
 * File: Hcshdiv.java
 * Date: 29 August 2009 22:52:05
 * Author: Quipoz Limited
 * 
 * Class transformed from HCSHDIV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              Cash Dividend Calculation
*              -------------------------
*
* This program is a cash dividend calculation subroutine called
* from various programs specified in T6639, by the dividend
* calculation method and the premium status code.
*
* This routine is responsible for returning the cash dividend due
* for the input period.  The calculation is based on rates held
* on TH527, cash dividend method.
*
*  CALCULATIONS.
*  -------------
*
* 1. Calculate the inforce duration and round up to nearest year
* 2. Calculate the dividend period.
* 3. Read TH527 with key,
*        HDVD-CRTABLE,
*        HDVD-ISSUED-AGE,
*        HDVD-MORTCLS,
*        HDVD-SEX
*    If specific item is not located, try again on generic keys
*    on sex, mortcls and issue age by substituting each of them.
* 4. Apply the appropriate rate from TH527 and calculate dividend
*        HDVD-DIVD-AMOUNT =
*           ((HDVD-SUMIN * rate * HDVD-DIVD-DURATION)/
*             Risk Unit ) / Premium Unit
* 5. Set status to O-K and exit.
*
*****************************************************************
*
* </pre>
*/
public class Hcshdiv extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "HCSHDIV";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaIssuedAge = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private FixedLengthStringData wsaaMortcls = new FixedLengthStringData(1).isAPartOf(wsaaItem, 6);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItem, 7);
		/* ERRORS */
	private static final String g549 = "G549";
	private static final String g641 = "G641";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String th527 = "TH527";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Th527rec th527rec = new Th527rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Hdivdrec hdivdrec = new Hdivdrec();
	private boolean cdivFlag = false;

	public Hcshdiv() {
		super();
	}

public void mainline(Object... parmArray)
	{
		hdivdrec.dividendRec = convertAndSetParam(hdivdrec.dividendRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readDividendLinkage300();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		syserrrec.subrname.set(wsaaSubr);
		hdivdrec.statuz.set(varcom.oK);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(hdivdrec.crrcd);
		datcon3rec.intDate2.set(hdivdrec.periodTo);
		datcon3rec.frequency.set("01");
		cdivFlag = FeaConfg.isFeatureExist("2", "BTPRO015", appVars, "IT");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			compute(hdivdrec.termInForce, 6).setRounded(mult(1,datcon3rec.freqFactor));
		}
		datcon3rec.datcon3Rec.set(SPACES);
		if (isGT(hdivdrec.crrcd,hdivdrec.periodFrom)) {
			datcon3rec.intDate1.set(hdivdrec.crrcd);
		}
		else {
			datcon3rec.intDate1.set(hdivdrec.periodFrom);
		}
		datcon3rec.intDate2.set(hdivdrec.periodTo);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			hdivdrec.divdDuration.set(datcon3rec.freqFactor);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(hdivdrec.chdrChdrcoy);
		if (!cdivFlag) {
			itdmIO.setItmfrm(hdivdrec.crrcd);
		} else {
			itdmIO.setItmfrm(hdivdrec.periodTo);
			if(isEQ(hdivdrec.divdDuration,ZERO))
				return;
		}
		itdmIO.setItemtabl(th527);
		wsaaCrtable.set(hdivdrec.crtable);
		wsaaIssuedAge.set(hdivdrec.issuedAge);
		wsaaMortcls.set(hdivdrec.mortcls);
		wsaaSex.set(hdivdrec.sex);
		itdmIO.setFormat(itemrec);
		itdmIO.setStatuz(SPACES);
		while ( !((isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemitem(),wsaaItem)))) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(hdivdrec.chdrChdrcoy);
			if(!cdivFlag){
			itdmIO.setItmfrm(hdivdrec.crrcd);
			}else{
				if(isNE(hdivdrec.itmfrmTh527,ZERO))
					itdmIO.setItmfrm(hdivdrec.itmfrmTh527);	
				else
					itdmIO.setItmfrm(hdivdrec.periodTo);	
			}
			itdmIO.setItemtabl(th527);
			itdmIO.setItemitem(wsaaItem);
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
//			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				databaseError800();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),th527)
			|| isNE(itdmIO.getItemcoy(),hdivdrec.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaItem)) {
				if (isEQ(subString(wsaaItem, 5, 4),"****")) {
					itdmIO.setItemitem(wsaaItem);
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(g641);
					databaseError800();
				}
				else {
					if (isNE(subString(wsaaItem, 8, 1),"*")) {
						wsaaItem.setSub1String(8, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 7, 1),"*")) {
							wsaaItem.setSub1String(7, 1, "*");
						}
						else {
							if (isNE(subString(wsaaItem, 5, 2),"**")) {
								wsaaItem.setSub1String(5, 2, "**");
							}
						}
					}
				}
			}
			else {
				th527rec.th527Rec.set(itdmIO.getGenarea());
			}
		}
		
		hdivdrec.rateDate.set(itdmIO.getItmfrm());
	}

protected void readDividendLinkage300()
	{
		beginReading310();
	}

protected void beginReading310()
	{
		/* check table has been set up correctly - must have non-zero*/
		/*  risk units otherwise we will be dividing by zero !!*/
		if (isEQ(th527rec.unit,ZERO)) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g549);
			databaseError800();
		}
		if (isLT(hdivdrec.termInForce,100)) {
			compute(hdivdrec.divdAmount, 6).setRounded(div((div((mult(mult(hdivdrec.sumin,th527rec.insprm[hdivdrec.termInForce.toInt()]),hdivdrec.divdDuration)),th527rec.unit)),th527rec.premUnit));
			compute(hdivdrec.divdRate, 6).setRounded(mult((div((div(th527rec.insprm[hdivdrec.termInForce.toInt()],th527rec.unit)),th527rec.premUnit)),100));
		}
		else {
			if (isLT(hdivdrec.termInForce, 111)) {
				compute(hdivdrec.divdAmount, 6).setRounded(div((div((mult(mult(hdivdrec.sumin, th527rec.instpr[sub(hdivdrec.termInForce, 99).toInt()]), hdivdrec.divdDuration)), th527rec.unit)), th527rec.premUnit));
				compute(hdivdrec.divdRate, 6).setRounded(mult((div((div(th527rec.instpr[sub(hdivdrec.termInForce, 99).toInt()], th527rec.unit)), th527rec.premUnit)), 100));
				/***   ((TH527-INSTPR / TH527-UNIT) / TH527-PREM-UNIT) * 100<S19FIX>*/
			}
			else {
				compute(hdivdrec.divdAmount, 6).setRounded(div((div((mult(mult(hdivdrec.sumin, th527rec.instpr[11]), hdivdrec.divdDuration)), th527rec.unit)), th527rec.premUnit));
				compute(hdivdrec.divdRate, 6).setRounded(mult((div((div(th527rec.instpr[11], th527rec.unit)), th527rec.premUnit)), 100));
				/****       ((TH527-INSTPR / TH527-UNIT) / TH527-PREM-UNIT) * 100   */
			}
		}
		/*EXIT*/
	}

protected void databaseError800()
	{
					start810();
					exit870();
				}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		hdivdrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
					start910();
					exit970();
				}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		hdivdrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
