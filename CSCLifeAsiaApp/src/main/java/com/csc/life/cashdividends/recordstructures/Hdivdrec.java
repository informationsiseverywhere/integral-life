package com.csc.life.cashdividends.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:31
 * Description:
 * Copybook name: HDIVDREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivdrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dividendRec = new FixedLengthStringData(134);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(dividendRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(dividendRec, 5);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(dividendRec, 9);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(dividendRec, 10);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(dividendRec, 18);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(dividendRec, 20);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(dividendRec, 22);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(dividendRec, 24);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(dividendRec, 27);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(dividendRec, 30);
  	public FixedLengthStringData transcd = new FixedLengthStringData(4).isAPartOf(dividendRec, 33);
  	public FixedLengthStringData premStatus = new FixedLengthStringData(2).isAPartOf(dividendRec, 37);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(dividendRec, 39);
  	public PackedDecimalData sumin = new PackedDecimalData(17, 2).isAPartOf(dividendRec, 43);
  	public PackedDecimalData effectiveDate = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 52);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 57);
  	public FixedLengthStringData allocMethod = new FixedLengthStringData(1).isAPartOf(dividendRec, 62);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(dividendRec, 63);
  	public FixedLengthStringData sex = new FixedLengthStringData(1).isAPartOf(dividendRec, 64);
  	public ZonedDecimalData issuedAge = new ZonedDecimalData(2, 0).isAPartOf(dividendRec, 65).setUnsigned();
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(dividendRec, 67).setUnsigned();
  	public FixedLengthStringData divdCalcMethod = new FixedLengthStringData(4).isAPartOf(dividendRec, 72);
  	public PackedDecimalData periodFrom = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 76);
  	public PackedDecimalData periodTo = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 81);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 86);
  	public PackedDecimalData cessDate = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 91);
  	public PackedDecimalData annualisedPrem = new PackedDecimalData(11, 2).isAPartOf(dividendRec, 96);
  	public PackedDecimalData divdDuration = new PackedDecimalData(11, 5).isAPartOf(dividendRec, 102);
  	public PackedDecimalData termInForce = new PackedDecimalData(3, 0).isAPartOf(dividendRec, 108);
  	public PackedDecimalData divdAmount = new PackedDecimalData(11, 2).isAPartOf(dividendRec, 110);
  	public PackedDecimalData divdRate = new PackedDecimalData(8, 5).isAPartOf(dividendRec, 116);
  	public PackedDecimalData rateDate = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 121);
  	public PackedDecimalData itmfrmTh527 = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 129);


	public void initialize() {
		COBOLFunctions.initialize(dividendRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		dividendRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}