/******************************************************************************
 * File Name 		: Hdpxpf.java
 * Author			: sbatra9
 * Creation Date	: 22 June 2020
 * Project			: Integral Life
 * Description		: The Model Class for HDPXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.csc.life.cashdividends.dataaccess.dao.HdpxpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdpxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HdpxpfDAOImpl extends BaseDAOImpl<Hdpxpf> implements HdpxpfDAO{
    private static final Logger LOGGER = LoggerFactory.getLogger(HdpxpfDAOImpl.class);

    public List<Hdpxpf> searchHdpxpfRecord(String tableId, String memName,
			int batchExtractSize, int batchID) {
    	
    	StringBuilder  sqlStr = new StringBuilder();
    	sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.CHDRCOY, TE.CHDRNUM)-1)/?) ROWNM, TE.* FROM ");
        sqlStr.append(tableId);
        sqlStr.append(" TE");
        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
 		List<Hdpxpf> pfList = new ArrayList<>();
 		ResultSet rs = null;

 		try {
 			 ps.setInt(1, batchExtractSize);
        	 ps.setString(2, memName);
        	 ps.setInt(3, batchID);
        	 rs = ps.executeQuery();
             while (rs.next()) {
                Hdpxpf hdpxpf = new Hdpxpf();
                hdpxpf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
                hdpxpf.setChdrcoy(rs.getString("CHDRCOY"));
                hdpxpf.setChdrnum(rs.getString("CHDRNUM"));
                hdpxpf.setLife(rs.getString("LIFE"));
                hdpxpf.setJLife(rs.getString("JLIFE"));
                hdpxpf.setCoverage(rs.getString("COVERAGE"));
                hdpxpf.setRider(rs.getString("RIDER"));
                hdpxpf.setPlanSuffix(rs.getInt("PLNSFX"));
                hdpxpf.setZdivopt(rs.getString("ZDIVOPT"));
            	pfList.add(hdpxpf);
             
            }

        } catch (SQLException e) {
            LOGGER.error("searchHdpxpfRecord()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pfList;
    }
	
}