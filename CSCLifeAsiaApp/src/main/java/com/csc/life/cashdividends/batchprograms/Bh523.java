/*
 * File: Bh523.java
 * Date: 29 August 2009 21:30:18
 * Author: Quipoz Limited
 * 
 * Class transformed from BH523.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.cashdividends.dataaccess.HdcxpfTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This batch program is a splitter program responsible for
*   extracting all HDISs that are due for Interest Capitalisation.
*
*   The extract records are held in the HDCXPF members created in the
*   prior process.  The HDCXPF member is accessed by the subsequent
*   process driving the actual interest capitalisation.
*
*   Initialise
*     - read T5679 for the valid component statii.
*     - set contract number from and to.
*     - using SQL, extract data from HDISPF, COVRPF and CHDRPF.
*     - define an array to hold HDCX records.
*
*    Read
*     - fetch a block of data into an array for HDCX records.
*
*    Perform    Until End of File
*
*      Edit
*       - dummy
*
*      Update
*       - write HDCX records from array to member(s).
*
*      Read next primary file record
*
*    End Perform
*
*   Close
*     - close SQL
*     - close all opened files.
*
*   Control totals:
*     01 - No. of threads used
*     02 - no. of records extracted
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*                                                                     *
***********************************************************************
* </pre>
*/
public class Bh523 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhdcxpf1rs;
	private java.sql.PreparedStatement sqlhdcxpf1ps;
	private java.sql.Connection sqlhdcxpf1conn = null;
	private String sqlhdcxpf1 = "";
	private int hdcxpf1LoopIndex = 0;
	private HdcxpfTableDAM hdcxpf = new HdcxpfTableDAM();
	private DiskFileDAM hdcx01 = new DiskFileDAM("HDCX01");
	private DiskFileDAM hdcx02 = new DiskFileDAM("HDCX02");
	private DiskFileDAM hdcx03 = new DiskFileDAM("HDCX03");
	private DiskFileDAM hdcx04 = new DiskFileDAM("HDCX04");
	private DiskFileDAM hdcx05 = new DiskFileDAM("HDCX05");
	private DiskFileDAM hdcx06 = new DiskFileDAM("HDCX06");
	private DiskFileDAM hdcx07 = new DiskFileDAM("HDCX07");
	private DiskFileDAM hdcx08 = new DiskFileDAM("HDCX08");
	private DiskFileDAM hdcx09 = new DiskFileDAM("HDCX09");
	private DiskFileDAM hdcx10 = new DiskFileDAM("HDCX10");
	private DiskFileDAM hdcx11 = new DiskFileDAM("HDCX11");
	private DiskFileDAM hdcx12 = new DiskFileDAM("HDCX12");
	private DiskFileDAM hdcx13 = new DiskFileDAM("HDCX13");
	private DiskFileDAM hdcx14 = new DiskFileDAM("HDCX14");
	private DiskFileDAM hdcx15 = new DiskFileDAM("HDCX15");
	private DiskFileDAM hdcx16 = new DiskFileDAM("HDCX16");
	private DiskFileDAM hdcx17 = new DiskFileDAM("HDCX17");
	private DiskFileDAM hdcx18 = new DiskFileDAM("HDCX18");
	private DiskFileDAM hdcx19 = new DiskFileDAM("HDCX19");
	private DiskFileDAM hdcx20 = new DiskFileDAM("HDCX20");
	private HdcxpfTableDAM hdcxpfData = new HdcxpfTableDAM();
		/*    Change the record length to that of the temporary file.
		    This can be found by doing a DSPFD of the file being
		    duplicated by the CRTTMPF process.*/
	private FixedLengthStringData hdcx01Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx02Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx03Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx04Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx05Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx06Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx07Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx08Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx09Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx10Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx11Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx12Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx13Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx14Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx15Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx16Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx17Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx18Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx19Rec = new FixedLengthStringData(55);
	private FixedLengthStringData hdcx20Rec = new FixedLengthStringData(55);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH523");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

		/*    HDCX member parameters*/
	private FixedLengthStringData wsaaHdcxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHdcxFn, 0, FILLER).init("HDCX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHdcxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHdcxFn, 6).setUnsigned();
		/*    Host variables*/
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovRstat12 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CovPstat12 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnRstat12 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat01 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat02 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat03 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat04 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat05 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat06 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat07 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat08 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat09 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat10 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat11 = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT5679CnPstat12 = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String itemrec = "ITEMREC";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
//	private ItemTableDAM itemIO = new ItemTableDAM();
	private P6671par p6671par = new P6671par();
	private T5679rec t5679rec = new T5679rec();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();
	
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class); 

	public Bh523() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		getT56791300();
		wsaaCompany.set(bsprIO.getCompany());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		sqlhdcxpf1 = " SELECT  HD.CHDRCOY, HD.CHDRNUM, HD.LIFE, HD.JLIFE, HD.COVERAGE, HD.RIDER, HD.PLNSFX, CO.CRTABLE, HD.HDVBALC, HD.HCAPNDT, HD.TRANNO, HD.HINTOS, CO.RCESDTE" +
" FROM   " + getAppVars().getTableNameOverriden("HDIS") + "  HD,  " + getAppVars().getTableNameOverriden("COVRPF") + "  CO,  " + getAppVars().getTableNameOverriden("CHDRPF") + "  CH" +
" WHERE HD.CHDRCOY = ?" +
" AND HD.VALIDFLAG = '1'" +
" AND HD.CHDRNUM BETWEEN ? AND ?" +
" AND HD.HCAPNDT <= ?" +
" AND HD.HCAPNDT <= HD.HINTNDT" +
" AND CO.VALIDFLAG = '1'" +
" AND CH.VALIDFLAG = '1'" +
" AND (CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?" +
" OR CO.STATCODE = ?)" +
" AND (CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?" +
" OR CO.PSTATCODE = ?)" +
" AND (CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?" +
" OR CH.STATCODE = ?)" +
" AND (CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?" +
" OR CH.PSTCDE = ?)" +
" AND CH.CHDRCOY = CO.CHDRCOY" +
" AND CH.CHDRNUM = CO.CHDRNUM" +
" AND HD.CHDRCOY = CO.CHDRCOY" +
" AND HD.CHDRNUM = CO.CHDRNUM" +
" AND HD.LIFE = CO.LIFE" +
" AND HD.COVERAGE = CO.COVERAGE" +
" AND HD.RIDER = CO.RIDER" +
" AND HD.PLNSFX = CO.PLNSFX" +
" ORDER BY HD.CHDRCOY, HD.CHDRNUM, HD.LIFE, HD.COVERAGE, HD.RIDER, HD.PLNSFX";
		sqlerrorflag = false;
		try {
			sqlhdcxpf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.cashdividends.dataaccess.HdisTableDAM(), new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM(), new com.csc.fsu.general.dataaccess.ChdrpfTableDAM()});
			sqlhdcxpf1ps = getAppVars().prepareStatementEmbeded(sqlhdcxpf1conn, sqlhdcxpf1);
			getAppVars().setDBString(sqlhdcxpf1ps, 1, wsaaCompany);
			getAppVars().setDBString(sqlhdcxpf1ps, 2, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlhdcxpf1ps, 3, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqlhdcxpf1ps, 4, wsaaEffectiveDate);
			getAppVars().setDBString(sqlhdcxpf1ps, 5, wsaaT5679CovRstat01);
			getAppVars().setDBString(sqlhdcxpf1ps, 6, wsaaT5679CovRstat02);
			getAppVars().setDBString(sqlhdcxpf1ps, 7, wsaaT5679CovRstat03);
			getAppVars().setDBString(sqlhdcxpf1ps, 8, wsaaT5679CovRstat04);
			getAppVars().setDBString(sqlhdcxpf1ps, 9, wsaaT5679CovRstat05);
			getAppVars().setDBString(sqlhdcxpf1ps, 10, wsaaT5679CovRstat06);
			getAppVars().setDBString(sqlhdcxpf1ps, 11, wsaaT5679CovRstat07);
			getAppVars().setDBString(sqlhdcxpf1ps, 12, wsaaT5679CovRstat08);
			getAppVars().setDBString(sqlhdcxpf1ps, 13, wsaaT5679CovRstat09);
			getAppVars().setDBString(sqlhdcxpf1ps, 14, wsaaT5679CovRstat10);
			getAppVars().setDBString(sqlhdcxpf1ps, 15, wsaaT5679CovRstat11);
			getAppVars().setDBString(sqlhdcxpf1ps, 16, wsaaT5679CovRstat12);
			getAppVars().setDBString(sqlhdcxpf1ps, 17, wsaaT5679CovPstat01);
			getAppVars().setDBString(sqlhdcxpf1ps, 18, wsaaT5679CovPstat02);
			getAppVars().setDBString(sqlhdcxpf1ps, 19, wsaaT5679CovPstat03);
			getAppVars().setDBString(sqlhdcxpf1ps, 20, wsaaT5679CovPstat04);
			getAppVars().setDBString(sqlhdcxpf1ps, 21, wsaaT5679CovPstat05);
			getAppVars().setDBString(sqlhdcxpf1ps, 22, wsaaT5679CovPstat06);
			getAppVars().setDBString(sqlhdcxpf1ps, 23, wsaaT5679CovPstat07);
			getAppVars().setDBString(sqlhdcxpf1ps, 24, wsaaT5679CovPstat08);
			getAppVars().setDBString(sqlhdcxpf1ps, 25, wsaaT5679CovPstat09);
			getAppVars().setDBString(sqlhdcxpf1ps, 26, wsaaT5679CovPstat10);
			getAppVars().setDBString(sqlhdcxpf1ps, 27, wsaaT5679CovPstat11);
			getAppVars().setDBString(sqlhdcxpf1ps, 28, wsaaT5679CovPstat12);
			getAppVars().setDBString(sqlhdcxpf1ps, 29, wsaaT5679CnRstat01);
			getAppVars().setDBString(sqlhdcxpf1ps, 30, wsaaT5679CnRstat02);
			getAppVars().setDBString(sqlhdcxpf1ps, 31, wsaaT5679CnRstat03);
			getAppVars().setDBString(sqlhdcxpf1ps, 32, wsaaT5679CnRstat04);
			getAppVars().setDBString(sqlhdcxpf1ps, 33, wsaaT5679CnRstat05);
			getAppVars().setDBString(sqlhdcxpf1ps, 34, wsaaT5679CnRstat06);
			getAppVars().setDBString(sqlhdcxpf1ps, 35, wsaaT5679CnRstat07);
			getAppVars().setDBString(sqlhdcxpf1ps, 36, wsaaT5679CnRstat08);
			getAppVars().setDBString(sqlhdcxpf1ps, 37, wsaaT5679CnRstat09);
			getAppVars().setDBString(sqlhdcxpf1ps, 38, wsaaT5679CnRstat10);
			getAppVars().setDBString(sqlhdcxpf1ps, 39, wsaaT5679CnRstat11);
			getAppVars().setDBString(sqlhdcxpf1ps, 40, wsaaT5679CnRstat12);
			getAppVars().setDBString(sqlhdcxpf1ps, 41, wsaaT5679CnPstat01);
			getAppVars().setDBString(sqlhdcxpf1ps, 42, wsaaT5679CnPstat02);
			getAppVars().setDBString(sqlhdcxpf1ps, 43, wsaaT5679CnPstat03);
			getAppVars().setDBString(sqlhdcxpf1ps, 44, wsaaT5679CnPstat04);
			getAppVars().setDBString(sqlhdcxpf1ps, 45, wsaaT5679CnPstat05);
			getAppVars().setDBString(sqlhdcxpf1ps, 46, wsaaT5679CnPstat06);
			getAppVars().setDBString(sqlhdcxpf1ps, 47, wsaaT5679CnPstat08);
			getAppVars().setDBString(sqlhdcxpf1ps, 48, wsaaT5679CnPstat09);
			getAppVars().setDBString(sqlhdcxpf1ps, 49, wsaaT5679CnPstat10);
			getAppVars().setDBString(sqlhdcxpf1ps, 50, wsaaT5679CnPstat11);
			getAppVars().setDBString(sqlhdcxpf1ps, 51, wsaaT5679CnPstat12);
			sqlhdcxpf1rs = getAppVars().executeQuery(sqlhdcxpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as*/
		/*    the array is indexed).*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHdcxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HDCX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHdcxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 50)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz,1)) {
			hdcx01.openOutput();
		}
		if (isEQ(iz,2)) {
			hdcx02.openOutput();
		}
		if (isEQ(iz,3)) {
			hdcx03.openOutput();
		}
		if (isEQ(iz,4)) {
			hdcx04.openOutput();
		}
		if (isEQ(iz,5)) {
			hdcx05.openOutput();
		}
		if (isEQ(iz,6)) {
			hdcx06.openOutput();
		}
		if (isEQ(iz,7)) {
			hdcx07.openOutput();
		}
		if (isEQ(iz,8)) {
			hdcx08.openOutput();
		}
		if (isEQ(iz,9)) {
			hdcx09.openOutput();
		}
		if (isEQ(iz,10)) {
			hdcx10.openOutput();
		}
		if (isEQ(iz,11)) {
			hdcx11.openOutput();
		}
		if (isEQ(iz,12)) {
			hdcx12.openOutput();
		}
		if (isEQ(iz,13)) {
			hdcx13.openOutput();
		}
		if (isEQ(iz,14)) {
			hdcx14.openOutput();
		}
		if (isEQ(iz,15)) {
			hdcx15.openOutput();
		}
		if (isEQ(iz,16)) {
			hdcx16.openOutput();
		}
		if (isEQ(iz,17)) {
			hdcx17.openOutput();
		}
		if (isEQ(iz,18)) {
			hdcx18.openOutput();
		}
		if (isEQ(iz,19)) {
			hdcx19.openOutput();
		}
		if (isEQ(iz,20)) {
			hdcx20.openOutput();
		}
	}

protected void getT56791300()
	{
		getT56791310();
	}

protected void getT56791310()
	{
		/*itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());*/
		
		List<Itempf> itemList=itempfDAO.findItem("IT", bsprIO.getCompany().toString(), t5679, bprdIO.getAuthCode().toString());
		
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		wsaaT5679CovRstat01.set(t5679rec.covRiskStat[1]);
		wsaaT5679CovRstat02.set(t5679rec.covRiskStat[2]);
		wsaaT5679CovRstat03.set(t5679rec.covRiskStat[3]);
		wsaaT5679CovRstat04.set(t5679rec.covRiskStat[4]);
		wsaaT5679CovRstat05.set(t5679rec.covRiskStat[5]);
		wsaaT5679CovRstat06.set(t5679rec.covRiskStat[6]);
		wsaaT5679CovRstat07.set(t5679rec.covRiskStat[7]);
		wsaaT5679CovRstat08.set(t5679rec.covRiskStat[8]);
		wsaaT5679CovRstat09.set(t5679rec.covRiskStat[9]);
		wsaaT5679CovRstat10.set(t5679rec.covRiskStat[10]);
		wsaaT5679CovRstat11.set(t5679rec.covRiskStat[11]);
		wsaaT5679CovRstat12.set(t5679rec.covRiskStat[12]);
		wsaaT5679CovPstat01.set(t5679rec.covPremStat[1]);
		wsaaT5679CovPstat02.set(t5679rec.covPremStat[2]);
		wsaaT5679CovPstat03.set(t5679rec.covPremStat[3]);
		wsaaT5679CovPstat04.set(t5679rec.covPremStat[4]);
		wsaaT5679CovPstat05.set(t5679rec.covPremStat[5]);
		wsaaT5679CovPstat06.set(t5679rec.covPremStat[6]);
		wsaaT5679CovPstat07.set(t5679rec.covPremStat[7]);
		wsaaT5679CovPstat08.set(t5679rec.covPremStat[8]);
		wsaaT5679CovPstat09.set(t5679rec.covPremStat[9]);
		wsaaT5679CovPstat10.set(t5679rec.covPremStat[10]);
		wsaaT5679CovPstat11.set(t5679rec.covPremStat[11]);
		wsaaT5679CovPstat12.set(t5679rec.covPremStat[12]);
		wsaaT5679CnRstat01.set(t5679rec.cnRiskStat[1]);
		wsaaT5679CnRstat02.set(t5679rec.cnRiskStat[2]);
		wsaaT5679CnRstat03.set(t5679rec.cnRiskStat[3]);
		wsaaT5679CnRstat04.set(t5679rec.cnRiskStat[4]);
		wsaaT5679CnRstat05.set(t5679rec.cnRiskStat[5]);
		wsaaT5679CnRstat06.set(t5679rec.cnRiskStat[6]);
		wsaaT5679CnRstat07.set(t5679rec.cnRiskStat[7]);
		wsaaT5679CnRstat08.set(t5679rec.cnRiskStat[8]);
		wsaaT5679CnRstat09.set(t5679rec.cnRiskStat[9]);
		wsaaT5679CnRstat10.set(t5679rec.cnRiskStat[10]);
		wsaaT5679CnRstat11.set(t5679rec.cnRiskStat[11]);
		wsaaT5679CnRstat12.set(t5679rec.cnRiskStat[12]);
		wsaaT5679CnPstat01.set(t5679rec.cnPremStat[1]);
		wsaaT5679CnPstat02.set(t5679rec.cnPremStat[2]);
		wsaaT5679CnPstat03.set(t5679rec.cnPremStat[3]);
		wsaaT5679CnPstat04.set(t5679rec.cnPremStat[4]);
		wsaaT5679CnPstat05.set(t5679rec.cnPremStat[5]);
		wsaaT5679CnPstat06.set(t5679rec.cnPremStat[6]);
		wsaaT5679CnPstat08.set(t5679rec.cnPremStat[8]);
		wsaaT5679CnPstat09.set(t5679rec.cnPremStat[9]);
		wsaaT5679CnPstat10.set(t5679rec.cnPremStat[10]);
		wsaaT5679CnPstat11.set(t5679rec.cnPremStat[11]);
		wsaaT5679CnPstat12.set(t5679rec.cnPremStat[12]);
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaBalSinceLastCap[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaNextCapDate[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaOsInterest[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaRiskCessDate[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFiles2010();
		}

protected void readFiles2010()
	{
		/*    Now a block of records is fetched into the array.*/
		/*    Also on the first entry into the program we must set up the*/
		/*    WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (hdcxpf1LoopIndex = 1; isLTE(hdcxpf1LoopIndex,wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlhdcxpf1rs); hdcxpf1LoopIndex++ ){
				getAppVars().getDBObject(sqlhdcxpf1rs, 1, wsaaFetchArrayInner.wsaaChdrcoy[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 2, wsaaFetchArrayInner.wsaaChdrnum[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 3, wsaaFetchArrayInner.wsaaLife[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 4, wsaaFetchArrayInner.wsaaJlife[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 5, wsaaFetchArrayInner.wsaaCoverage[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 6, wsaaFetchArrayInner.wsaaRider[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 7, wsaaFetchArrayInner.wsaaPlnsfx[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 8, wsaaFetchArrayInner.wsaaCrtable[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 9, wsaaFetchArrayInner.wsaaBalSinceLastCap[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 10, wsaaFetchArrayInner.wsaaNextCapDate[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 11, wsaaFetchArrayInner.wsaaTranno[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 12, wsaaFetchArrayInner.wsaaOsInterest[hdcxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhdcxpf1rs, 13, wsaaFetchArrayInner.wsaaRiskCessDate[hdcxpf1LoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*    If either of the above cases occur, then an SQLCODE = +100*/
		/*    is returned.*/
		/*    The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		else if(isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		} 
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*    Re-initialise the block for next fetch and point to the*/
		/*    first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*    If the CHDRNUM being processed is not equal to the previous*/
		/*    one we should move to the next output file member.*/
		/*    The condition is here to allow for checking the last CHDRNUM*/
		/*    of the old block with the first of the new and to move to*/
		/*    the next HDCX member to write to if they have changed.*/
		if (isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*    Load from storage all HDCX data for the same contract until*/
		/*    the CHDRNUM on HDCX has changed or until the end of an*/
		/*    incomplete block is reached.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3201();
	}

protected void start3201()
	{
		hdcxpfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
		hdcxpfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
		hdcxpfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
		hdcxpfData.jlife.set(wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()]);
		hdcxpfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
		hdcxpfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
		hdcxpfData.crtable.set(wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()]);
		hdcxpfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
		hdcxpfData.balSinceLastCap.set(wsaaFetchArrayInner.wsaaBalSinceLastCap[wsaaInd.toInt()]);
		hdcxpfData.nextCapDate.set(wsaaFetchArrayInner.wsaaNextCapDate[wsaaInd.toInt()]);
		hdcxpfData.tranno.set(wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()]);
		hdcxpfData.osInterest.set(wsaaFetchArrayInner.wsaaOsInterest[wsaaInd.toInt()]);
		hdcxpfData.riskCessDate.set(wsaaFetchArrayInner.wsaaRiskCessDate[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy,1)) {
			hdcx01.write(hdcxpfData);
		}
		if (isEQ(iy,2)) {
			hdcx02.write(hdcxpfData);
		}
		if (isEQ(iy,3)) {
			hdcx03.write(hdcxpfData);
		}
		if (isEQ(iy,4)) {
			hdcx04.write(hdcxpfData);
		}
		if (isEQ(iy,5)) {
			hdcx05.write(hdcxpfData);
		}
		if (isEQ(iy,6)) {
			hdcx06.write(hdcxpfData);
		}
		if (isEQ(iy,7)) {
			hdcx07.write(hdcxpfData);
		}
		if (isEQ(iy,8)) {
			hdcx08.write(hdcxpfData);
		}
		if (isEQ(iy,9)) {
			hdcx09.write(hdcxpfData);
		}
		if (isEQ(iy,10)) {
			hdcx10.write(hdcxpfData);
		}
		if (isEQ(iy,11)) {
			hdcx11.write(hdcxpfData);
		}
		if (isEQ(iy,12)) {
			hdcx12.write(hdcxpfData);
		}
		if (isEQ(iy,13)) {
			hdcx13.write(hdcxpfData);
		}
		if (isEQ(iy,14)) {
			hdcx14.write(hdcxpfData);
		}
		if (isEQ(iy,15)) {
			hdcx15.write(hdcxpfData);
		}
		if (isEQ(iy,16)) {
			hdcx16.write(hdcxpfData);
		}
		if (isEQ(iy,17)) {
			hdcx17.write(hdcxpfData);
		}
		if (isEQ(iy,18)) {
			hdcx18.write(hdcxpfData);
		}
		if (isEQ(iy,19)) {
			hdcx19.write(hdcxpfData);
		}
		if (isEQ(iy,20)) {
			hdcx20.write(hdcxpfData);
		}
		/*    Log the number of extracted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*    Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/		
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*    Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlhdcxpf1conn, sqlhdcxpf1ps, sqlhdcxpf1rs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			hdcx01.close();
		}
		if (isEQ(iz,2)) {
			hdcx02.close();
		}
		if (isEQ(iz,3)) {
			hdcx03.close();
		}
		if (isEQ(iz,4)) {
			hdcx04.close();
		}
		if (isEQ(iz,5)) {
			hdcx05.close();
		}
		if (isEQ(iz,6)) {
			hdcx06.close();
		}
		if (isEQ(iz,7)) {
			hdcx07.close();
		}
		if (isEQ(iz,8)) {
			hdcx08.close();
		}
		if (isEQ(iz,9)) {
			hdcx09.close();
		}
		if (isEQ(iz,10)) {
			hdcx10.close();
		}
		if (isEQ(iz,11)) {
			hdcx11.close();
		}
		if (isEQ(iz,12)) {
			hdcx12.close();
		}
		if (isEQ(iz,13)) {
			hdcx13.close();
		}
		if (isEQ(iz,14)) {
			hdcx14.close();
		}
		if (isEQ(iz,15)) {
			hdcx15.close();
		}
		if (isEQ(iz,16)) {
			hdcx16.close();
		}
		if (isEQ(iz,17)) {
			hdcx17.close();
		}
		if (isEQ(iz,18)) {
			hdcx18.close();
		}
		if (isEQ(iz,19)) {
			hdcx19.close();
		}
		if (isEQ(iz,20)) {
			hdcx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaHdcxData = FLSInittedArray (1000, 55);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaHdcxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaHdcxData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaHdcxData, 9);
	private FixedLengthStringData[] wsaaJlife = FLSDArrayPartOfArrayStructure(2, wsaaHdcxData, 11);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaHdcxData, 13);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaHdcxData, 15);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaHdcxData, 17);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaHdcxData, 20);
	private PackedDecimalData[] wsaaBalSinceLastCap = PDArrayPartOfArrayStructure(17, 2, wsaaHdcxData, 24);
	private PackedDecimalData[] wsaaNextCapDate = PDArrayPartOfArrayStructure(8, 0, wsaaHdcxData, 33);
	private PackedDecimalData[] wsaaTranno = PDArrayPartOfArrayStructure(5, 0, wsaaHdcxData, 38);
	private PackedDecimalData[] wsaaOsInterest = PDArrayPartOfArrayStructure(17, 2, wsaaHdcxData, 41);
	private PackedDecimalData[] wsaaRiskCessDate = PDArrayPartOfArrayStructure(8, 0, wsaaHdcxData, 50);
}
}
