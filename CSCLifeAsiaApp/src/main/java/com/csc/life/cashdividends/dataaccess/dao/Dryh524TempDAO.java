package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;

import com.csc.life.cashdividends.dataaccess.model.Dryh524DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Dryh524TempDAO extends BaseDAO<Dryh524DTO> {

	public List<Dryh524DTO> getHdisHcsdRecords(String chdrcoy, String chdrnum); 
}
