/******************************************************************************
 * File Name 		: HdvxpfDAO.java
 * Author			: sbatra9
 * Creation Date	: 23 June 2020
 * Project			: Integral Life
 * Description		: The DAO Interface for HDVXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdvxpf;

public interface HdvxpfDAO extends BaseDAO<Hdvxpf> {
	public List<Hdvxpf> searchHdvxpfRecord(String tableId, String memName,int batchExtractSize, int batchID);
}