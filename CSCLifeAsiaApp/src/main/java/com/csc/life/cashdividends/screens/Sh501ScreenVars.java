package com.csc.life.cashdividends.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH501
 * @version 1.0 generated on 30/08/09 07:00
 * @author Quipoz
 */
public class Sh501ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(341);
	public FixedLengthStringData dataFields = new FixedLengthStringData(85).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData freqcys = new FixedLengthStringData(4).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(2, 2, freqcys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(freqcys, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01 = DD.freqcy.copy().isAPartOf(filler,0);
	public FixedLengthStringData freqcy02 = DD.freqcy.copy().isAPartOf(filler,2);
	public FixedLengthStringData hfixdds = new FixedLengthStringData(4).isAPartOf(dataFields, 5);
	public FixedLengthStringData[] hfixdd = FLSArrayPartOfStructure(2, 2, hfixdds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(hfixdds, 0, FILLER_REDEFINE);
	public FixedLengthStringData hfixdd01 = DD.hfixdd.copy().isAPartOf(filler1,0);
	public FixedLengthStringData hfixdd02 = DD.hfixdd.copy().isAPartOf(filler1,2);
	public FixedLengthStringData hfixmms = new FixedLengthStringData(4).isAPartOf(dataFields, 9);
	public FixedLengthStringData[] hfixmm = FLSArrayPartOfStructure(2, 2, hfixmms, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(hfixmms, 0, FILLER_REDEFINE);
	public FixedLengthStringData hfixmm01 = DD.hfixmm.copy().isAPartOf(filler2,0);
	public FixedLengthStringData hfixmm02 = DD.hfixmm.copy().isAPartOf(filler2,2);
	public FixedLengthStringData intCalcSbr = DD.hintcalc.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData inds = new FixedLengthStringData(3).isAPartOf(dataFields, 23);
	public FixedLengthStringData[] ind = FLSArrayPartOfStructure(3, 1, inds, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(3).isAPartOf(inds, 0, FILLER_REDEFINE);
	public FixedLengthStringData ind01 = DD.ind.copy().isAPartOf(filler3,0);
	public FixedLengthStringData ind02 = DD.ind.copy().isAPartOf(filler3,1);
	public FixedLengthStringData ind03 = DD.ind.copy().isAPartOf(filler3,2);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,26);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,34);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,42);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 85);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData freqcysErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] freqcyErr = FLSArrayPartOfStructure(2, 4, freqcysErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(freqcysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData freqcy02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData hfixddsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] hfixddErr = FLSArrayPartOfStructure(2, 4, hfixddsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(hfixddsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hfixdd01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData hfixdd02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData hfixmmsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] hfixmmErr = FLSArrayPartOfStructure(2, 4, hfixmmsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(8).isAPartOf(hfixmmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hfixmm01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData hfixmm02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData hintcalcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData indsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] indErr = FLSArrayPartOfStructure(3, 4, indsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(12).isAPartOf(indsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ind01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData ind02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData ind03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 149);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData freqcysOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] freqcyOut = FLSArrayPartOfStructure(2, 12, freqcysOut, 0);
	public FixedLengthStringData[][] freqcyO = FLSDArrayPartOfArrayStructure(12, 1, freqcyOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(freqcysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] freqcy01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] freqcy02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData hfixddsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] hfixddOut = FLSArrayPartOfStructure(2, 12, hfixddsOut, 0);
	public FixedLengthStringData[][] hfixddO = FLSDArrayPartOfArrayStructure(12, 1, hfixddOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(hfixddsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hfixdd01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] hfixdd02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData hfixmmsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] hfixmmOut = FLSArrayPartOfStructure(2, 12, hfixmmsOut, 0);
	public FixedLengthStringData[][] hfixmmO = FLSDArrayPartOfArrayStructure(12, 1, hfixmmOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(hfixmmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hfixmm01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] hfixmm02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] hintcalcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData indsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(3, 12, indsOut, 0);
	public FixedLengthStringData[][] indO = FLSDArrayPartOfArrayStructure(12, 1, indOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(36).isAPartOf(indsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ind01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] ind02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] ind03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sh501screenWritten = new LongData(0);
	public LongData Sh501protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh501ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(freqcy01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hfixdd01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hfixdd02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hfixmm01Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hfixmm02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hintcalcOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind01Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind02Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind03Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, freqcy01, freqcy02, hfixdd01, hfixdd02, hfixmm01, hfixmm02, intCalcSbr, ind01, ind02, ind03};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, freqcy01Out, freqcy02Out, hfixdd01Out, hfixdd02Out, hfixmm01Out, hfixmm02Out, hintcalcOut, ind01Out, ind02Out, ind03Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, freqcy01Err, freqcy02Err, hfixdd01Err, hfixdd02Err, hfixmm01Err, hfixmm02Err, hintcalcErr, ind01Err, ind02Err, ind03Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh501screen.class;
		protectRecord = Sh501protect.class;
	}

}
