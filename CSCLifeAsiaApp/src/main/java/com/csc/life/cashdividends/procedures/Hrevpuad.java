/*
 * File: Hrevpuad.java
 * Date: 29 August 2009 22:55:30
 * Author: Quipoz Limited
 *
 * Class transformed from HREVPUAD.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.dataaccess.HdivrevTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuarevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*       ------------------------------------------
*           DIVIDEND PAID UP ADDITION REVERSAL
*       ------------------------------------------
*
*  This subroutine is called from the Full Contract Reversal
*  program REVGENAT via Table T6661. The parameters passed in
*  is via the REVERSEREC copybook.
*
*  The routine is used to reverse processing done in HDOPPUAD.
*
*  All ACMVs that were posted in the forward transaction are
*  reversed.  Transaction records HDIV are reversed in such
*  that the matching HDIV TRANNO are deleted. Same applies to
*  HPUA records.
*
*  The routine is driven by :
*    - The Company to which this Contract belongs.
*    - The Contract Header Number.
*    - The Transaction Number used in the original
*      dividend paid-up addition transaction.
*
*  PROCESSING
*  ----------
*
*  - Get Today's date.
*
*  - Locate HDIV records and delete them.
*
*  - Locate HPUA records and delete them.
*
*  - BEGN on the ACMV file with ACMVREV. This will get to the
*    first accounting record which has this Transaction Number.
*
*  MAIN PROCESS.
*  -------------
*
*  PROCESS UNTIL HDIVREV-STATUZ = MRNF
*  - Read hold HDIVREV and delete it
*
*  END OF HDIVREV PROCESSING LOOP.
*
*  PROCESS UNTIL HPUAREV-STATUZ = MRNF
*  - Read hold HPUAREV and delete it
*
*  END OF HDIVREV PROCESSING LOOP.
*
*  END OF MAIN PROCESSING LOOP.
*
***********************************************************************
* </pre>
*/
public class Hrevpuad extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
		/* FORMATS */
	private String hdivrevrec = "HDIVREVREC";
	private String hpuarevrec = "HPUAREVREC";
	private String covrrec = "COVRREC";
		/* ERRORS */
	private String hl14 = "HL14";
	private String hl15 = "HL15";
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Cash Dividend Alloc Transaction(Reversal*/
	private HdivrevTableDAM hdivrevIO = new HdivrevTableDAM();
		/*Dividend Paid Up Addition Reversal*/
	private HpuarevTableDAM hpuarevIO = new HpuarevTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit9090
	}

	public Hrevpuad() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline010()
	{
		/*INIT*/
		reverserec.statuz.set(varcom.oK);
		/*PARA*/
		processHdiv1000();
		processHpua2000();
		processCovr3000();
		/*EXIT*/
		exitProgram();
	}

protected void processHdiv1000()
	{
		readhHdivrev1010();
	}

protected void readhHdivrev1010()
	{
		hdivrevIO.setStatuz(varcom.oK);
		while ( !(isEQ(hdivrevIO.getStatuz(),varcom.mrnf))) {
			hdivrevIO.setDataKey(SPACES);
			hdivrevIO.setChdrcoy(reverserec.company);
			hdivrevIO.setChdrnum(reverserec.chdrnum);
			hdivrevIO.setTranno(reverserec.tranno);
			hdivrevIO.setFormat(hdivrevrec);
			hdivrevIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hdivrevIO);
			if (isNE(hdivrevIO.getStatuz(),varcom.oK)
			&& isNE(hdivrevIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(hdivrevIO.getParams());
				fatalError9000();
			}
			if (isEQ(hdivrevIO.getStatuz(),varcom.oK)) {
				hdivrevIO.setFunction(varcom.delet);
				SmartFileCode.execute(appVars, hdivrevIO);
				if (isNE(hdivrevIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivrevIO.getParams());
					fatalError9000();
				}
			}
		}

	}

protected void processHpua2000()
	{
		readhHpuarev2010();
	}

protected void readhHpuarev2010()
	{
		hpuarevIO.setStatuz(varcom.oK);
		while ( !(isEQ(hpuarevIO.getStatuz(),varcom.mrnf))) {
			hpuarevIO.setDataKey(SPACES);
			hpuarevIO.setChdrcoy(reverserec.company);
			hpuarevIO.setChdrnum(reverserec.chdrnum);
			hpuarevIO.setLife(reverserec.life);
			hpuarevIO.setCoverage(reverserec.coverage);
			hpuarevIO.setRider(reverserec.rider);
			hpuarevIO.setPlanSuffix(reverserec.planSuffix);
			hpuarevIO.setTranno(reverserec.tranno);
			hpuarevIO.setFormat(hpuarevrec);
			hpuarevIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hpuarevIO);
			if (isNE(hpuarevIO.getStatuz(),varcom.oK)
			&& isNE(hpuarevIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(hpuarevIO.getParams());
				fatalError9000();
			}
			if (isEQ(hpuarevIO.getStatuz(),varcom.oK)) {
				hpuarevIO.setFunction(varcom.delet);
				SmartFileCode.execute(appVars, hpuarevIO);
				if (isNE(hpuarevIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hpuarevIO.getParams());
					fatalError9000();
				}
			}
		}

	}

protected void processCovr3000()
	{
		covrRead3010();
	}

protected void covrRead3010()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setLife(reverserec.life);
		covrIO.setCoverage(reverserec.coverage);
		covrIO.setRider(reverserec.rider);
		covrIO.setPlanSuffix(reverserec.planSuffix);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrIO.getValidflag(),"1")) {
			covrIO.setStatuz(hl14);
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		covrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrIO.getStatuz());
			fatalError9000();
		}
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrIO.getValidflag(),"2")) {
			covrIO.setStatuz(hl15);
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getStatuz());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		try {
			error9010();
		}
		catch (GOTOException e){
		}
		finally{
			exit9090();
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		reverserec.statuz.set(varcom.bomb);
	}

protected void exit9090()
	{
		exitProgram();
	}
}
