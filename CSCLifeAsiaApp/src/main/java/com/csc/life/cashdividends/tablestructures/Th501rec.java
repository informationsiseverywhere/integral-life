package com.csc.life.cashdividends.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:39
 * Description:
 * Copybook name: TH501REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th501rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th501Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData freqcys = new FixedLengthStringData(4).isAPartOf(th501Rec, 0);
  	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(2, 2, freqcys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(freqcys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freqcy01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData freqcy02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData hfixdds = new FixedLengthStringData(4).isAPartOf(th501Rec, 4);
  	public FixedLengthStringData[] hfixdd = FLSArrayPartOfStructure(2, 2, hfixdds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(hfixdds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData hfixdd01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData hfixdd02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData hfixmms = new FixedLengthStringData(4).isAPartOf(th501Rec, 8);
  	public FixedLengthStringData[] hfixmm = FLSArrayPartOfStructure(2, 2, hfixmms, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(hfixmms, 0, FILLER_REDEFINE);
  	public FixedLengthStringData hfixmm01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData hfixmm02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData intCalcSbr = new FixedLengthStringData(10).isAPartOf(th501Rec, 12);
  	public FixedLengthStringData inds = new FixedLengthStringData(3).isAPartOf(th501Rec, 22);
  	public FixedLengthStringData[] ind = FLSArrayPartOfStructure(3, 1, inds, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(3).isAPartOf(inds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ind01 = new FixedLengthStringData(1).isAPartOf(filler3, 0);
  	public FixedLengthStringData ind02 = new FixedLengthStringData(1).isAPartOf(filler3, 1);
  	public FixedLengthStringData ind03 = new FixedLengthStringData(1).isAPartOf(filler3, 2);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(475).isAPartOf(th501Rec, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th501Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th501Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}