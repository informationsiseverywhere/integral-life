package com.csc.life.cashdividends.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:34
 * Description:
 * Copybook name: HDVDCWCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdvdcwcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData withdrawRec = new FixedLengthStringData(100);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(withdrawRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(withdrawRec, 5);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(withdrawRec, 9);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(withdrawRec, 10);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(withdrawRec, 18);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(withdrawRec, 20);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(withdrawRec, 22);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(withdrawRec, 24);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(withdrawRec, 26);
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(withdrawRec, 29).setUnsigned();
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(withdrawRec, 34);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(withdrawRec, 37);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(withdrawRec, 41);
  	public PackedDecimalData effectiveDate = new PackedDecimalData(8, 0).isAPartOf(withdrawRec, 44);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(withdrawRec, 49);
  	public FixedLengthStringData type = new FixedLengthStringData(1).isAPartOf(withdrawRec, 54);
  	public FixedLengthStringData description = new FixedLengthStringData(10).isAPartOf(withdrawRec, 55);
  	public FixedLengthStringData endf = new FixedLengthStringData(1).isAPartOf(withdrawRec, 65);
  	public ZonedDecimalData actualVal = new ZonedDecimalData(17, 2).isAPartOf(withdrawRec, 66);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(withdrawRec, 83);
  	public ZonedDecimalData nextCapDate = new ZonedDecimalData(8, 0).isAPartOf(withdrawRec, 84).setUnsigned();
  	public FixedLengthStringData zdivopt = new FixedLengthStringData(4).isAPartOf(withdrawRec, 92);
  	public FixedLengthStringData zcshdivmth = new FixedLengthStringData(4).isAPartOf(withdrawRec, 96);


	public void initialize() {
		COBOLFunctions.initialize(withdrawRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		withdrawRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}