package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:32
 * Description:
 * Copybook name: HDIVGRVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivgrvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivgrvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivgrvKey = new FixedLengthStringData(64).isAPartOf(hdivgrvFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivgrvChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivgrvKey, 0);
  	public FixedLengthStringData hdivgrvChdrnum = new FixedLengthStringData(8).isAPartOf(hdivgrvKey, 1);
  	public FixedLengthStringData hdivgrvLife = new FixedLengthStringData(2).isAPartOf(hdivgrvKey, 9);
  	public FixedLengthStringData hdivgrvCoverage = new FixedLengthStringData(2).isAPartOf(hdivgrvKey, 11);
  	public FixedLengthStringData hdivgrvRider = new FixedLengthStringData(2).isAPartOf(hdivgrvKey, 13);
  	public PackedDecimalData hdivgrvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hdivgrvKey, 15);
  	public PackedDecimalData hdivgrvTranno = new PackedDecimalData(5, 0).isAPartOf(hdivgrvKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(hdivgrvKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivgrvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivgrvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}