/******************************************************************************
 * File Name 		: Hdpxpf.java
 * Author			: sbatra9
 * Creation Date	: 22 June 2020
 * Project			: Integral Life
 * Description		: The Model Class for HDPXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.csc.life.cashdividends.dataaccess.dao.HdvxpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdvxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HdvxpfDAOImpl extends BaseDAOImpl<Hdvxpf> implements HdvxpfDAO{
    private static final Logger LOGGER = LoggerFactory.getLogger(HdvxpfDAOImpl.class);

    public List<Hdvxpf> searchHdvxpfRecord(String tableId, String memName,
			int batchExtractSize, int batchID) {
    	
    	StringBuilder  sqlStr = new StringBuilder();
    	sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.CHDRCOY, TE.CHDRNUM)-1)/?) ROWNM, TE.* FROM ");
        sqlStr.append(tableId);
        sqlStr.append(" TE");
        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
 		List<Hdvxpf> pfList = new ArrayList<>();
 		ResultSet rs = null;

 		try {
 			 ps.setInt(1, batchExtractSize);
        	 ps.setString(2, memName);
        	 ps.setInt(3, batchID);
        	 rs = ps.executeQuery();
             while (rs.next()) {
                Hdvxpf hdvxpf = new Hdvxpf();
                hdvxpf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
                hdvxpf.setChdrcoy(rs.getString("CHDRCOY"));
                hdvxpf.setChdrnum(rs.getString("CHDRNUM"));
                hdvxpf.setLife(rs.getString("LIFE"));
                hdvxpf.setJlife(rs.getString("JLIFE"));
                hdvxpf.setCoverage(rs.getString("COVERAGE"));
                hdvxpf.setRider(rs.getString("RIDER"));
                hdvxpf.setPlnsfx(rs.getInt("PLNSFX"));
                hdvxpf.setCrtable(rs.getString("CRTABLE"));
                hdvxpf.setCrrcd(rs.getInt("CRRCD"));
                hdvxpf.setCbunst(rs.getInt("CBUNST"));
                hdvxpf.setRcesdte(rs.getInt("RCESDTE"));
                hdvxpf.setSumins(rs.getInt("SUMINS"));
                hdvxpf.setCurrfrom(rs.getInt("CURRFROM"));
                hdvxpf.setCurrto(rs.getInt("CURRTO"));
                hdvxpf.setCntcurr(rs.getString("CNTCURR"));
                hdvxpf.setTranno(rs.getInt("TRANNO"));
                hdvxpf.setPstatcode(rs.getString("PSTATCODE"));
                hdvxpf.setStatcode(rs.getString("STATCODE"));
                hdvxpf.setPtdate(rs.getInt("PTDATE"));
                hdvxpf.setBnusin(rs.getString("BNUSIN"));
            	pfList.add(hdvxpf);
             
            }

        } catch (SQLException e) {
            LOGGER.error("searchHdvxpfRecord()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pfList;
    }

}