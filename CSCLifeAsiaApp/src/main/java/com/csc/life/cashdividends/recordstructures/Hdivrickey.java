package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:33
 * Description:
 * Copybook name: HDIVRICKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivrickey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivricFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivricKey = new FixedLengthStringData(64).isAPartOf(hdivricFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivricChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivricKey, 0);
  	public FixedLengthStringData hdivricChdrnum = new FixedLengthStringData(8).isAPartOf(hdivricKey, 1);
  	public PackedDecimalData hdivricDivdCapTranno = new PackedDecimalData(5, 0).isAPartOf(hdivricKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(hdivricKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivricFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivricFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}