/*
 * File: Hcdoptc1.java
 * Date: 29 August 2009 22:51:43
 * Author: Quipoz Limited
 *
 * Class transformed from HCDOPTC1.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   OVERVIEW
*   ========
*
*   This program is accessed via HCDOPTC via TH535 during
*   component change processing.  It is responsible for carrying
*   out the following option change processing.
*
*   From DVAC to PMST
*   -  Transfer suspense from LC DS to LP DS
*   -  Update HDIS with subaccount code and type set to LP DS
*      respectively
*
*
*   Processing
*   - Read T5645 for sub-account code and type combinations
*   - Read HDIS
*   - Read ACBL for LC DS
*   - If ACBL balance not = 0
*     write ACMV for LP DS via LIFACMV
*     invalid current HDIS, write a new one with subaccount
*     code and type LP DS.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hcdoptc1 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HCDOPTC1";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0).init(0);
	private String acblrec = "ACBLREC";
	private String hdisrec = "HDISREC";
	private String chdrenqrec = "CHDRENQREC";
		/* TABLES */
	private String t3695 = "T3695";
	private String t5645 = "T5645";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private Isuallrec isuallrec = new Isuallrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		exit602
	}

	public Hcdoptc1() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		try {
			starts1000();
		}
		catch (GOTOException e){
		}
		finally{
			exit1090();
		}
	}

protected void starts1000()
	{
		isuallrec.statuz.set(varcom.oK);
		chdrenqIO.setDataArea(SPACES);
		chdrenqIO.setChdrcoy(isuallrec.company);
		chdrenqIO.setChdrnum(isuallrec.chdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrnum(isuallrec.chdrnum);
		hdisIO.setChdrcoy(isuallrec.company);
		hdisIO.setLife(isuallrec.life);
		hdisIO.setCoverage(isuallrec.coverage);
		hdisIO.setRider(isuallrec.rider);
		hdisIO.setPlanSuffix(isuallrec.planSuffix);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit1090);
		}
		acblIO.setDataArea(SPACES);
		acblIO.setRldgcoy(hdisIO.getChdrcoy());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setRldgacct(hdisIO.getChdrnum());
		acblIO.setOrigcurr(hdisIO.getCntcurr());
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFormat(acblrec);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(acblIO.getSacscurbal(),0)) {
			goTo(GotoLabel.exit1090);
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		wsaaJrnseq.set(ZERO);
		lifacmvrec.batccoy.set(isuallrec.batccoy);
		lifacmvrec.batcbrn.set(isuallrec.batcbrn);
		lifacmvrec.batcactyr.set(isuallrec.batcactyr);
		lifacmvrec.batcactmn.set(isuallrec.batcactmn);
		lifacmvrec.batctrcde.set(isuallrec.batctrcde);
		lifacmvrec.batcbatch.set(isuallrec.batcbatch);
		lifacmvrec.rldgcoy.set(isuallrec.batccoy);
		lifacmvrec.genlcoy.set(isuallrec.batccoy);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(isuallrec.termid);
		lifacmvrec.transactionDate.set(isuallrec.transactionDate);
		lifacmvrec.transactionTime.set(isuallrec.transactionTime);
		lifacmvrec.user.set(isuallrec.user);
		lifacmvrec.trandesc.set("Option Change");
		lifacmvrec.origcurr.set(acblIO.getOrigcurr());
		lifacmvrec.origamt.set(acblIO.getSacscurbal());
		if (isEQ(t3695rec.sign,"-")) {
			compute(lifacmvrec.origamt, 2).set(mult(lifacmvrec.origamt,-1));
		}
		lifacmvrec.function.set("PSTW");
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.rldgacct.set(hdisIO.getChdrnum());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.rdocnum.set(hdisIO.getChdrnum());
		lifacmvrec.tranref.set(hdisIO.getChdrnum());
		lifacmvrec.tranno.set(isuallrec.newTranno);
		lifacmvrec.effdate.set(isuallrec.effdate);
		lifacmvrec.substituteCode[1].set(chdrenqIO.getCnttype());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(isuallrec.newTranno);
		hdisIO.setSacscode(lifacmvrec.sacscode);
		hdisIO.setSacstyp(lifacmvrec.sacstyp);
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			fatalError600();
		}
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error600();
				}
				case exit602: {
					exit602();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit602);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		isuallrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
