package com.csc.life.cashdividends.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:59
 * Description:
 * Copybook name: TH532REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th532rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th532Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData intanny = new ZonedDecimalData(5, 2).isAPartOf(th532Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(495).isAPartOf(th532Rec, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th532Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th532Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}