package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:31
 * Description:
 * Copybook name: HDIVDOPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivdopkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivdopFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivdopKey = new FixedLengthStringData(64).isAPartOf(hdivdopFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivdopChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivdopKey, 0);
  	public FixedLengthStringData hdivdopChdrnum = new FixedLengthStringData(8).isAPartOf(hdivdopKey, 1);
  	public FixedLengthStringData hdivdopLife = new FixedLengthStringData(2).isAPartOf(hdivdopKey, 9);
  	public FixedLengthStringData hdivdopCoverage = new FixedLengthStringData(2).isAPartOf(hdivdopKey, 11);
  	public FixedLengthStringData hdivdopRider = new FixedLengthStringData(2).isAPartOf(hdivdopKey, 13);
  	public PackedDecimalData hdivdopPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hdivdopKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(hdivdopKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivdopFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivdopFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}