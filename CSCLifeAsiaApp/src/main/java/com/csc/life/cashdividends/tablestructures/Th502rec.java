package com.csc.life.cashdividends.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:39
 * Description:
 * Copybook name: TH502REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th502rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th502Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zconannalc = new FixedLengthStringData(1).isAPartOf(th502Rec, 0);
  	public FixedLengthStringData zcoyannalc = new FixedLengthStringData(1).isAPartOf(th502Rec, 1);
  	public FixedLengthStringData zcshdivalc = new FixedLengthStringData(1).isAPartOf(th502Rec, 2);
  	public FixedLengthStringData zrevbonalc = new FixedLengthStringData(1).isAPartOf(th502Rec, 3);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(th502Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th502Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th502Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}