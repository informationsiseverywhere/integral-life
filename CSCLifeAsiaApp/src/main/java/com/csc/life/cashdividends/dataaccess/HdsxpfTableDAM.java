package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdsxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:28
 * Class transformed from HDSXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdsxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 55;
	public FixedLengthStringData hdsxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hdsxpfRecord = hdsxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hdsxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hdsxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hdsxrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hdsxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hdsxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hdsxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hdsxrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hdsxrec);
	public PackedDecimalData balSinceLastCap = DD.hdvbalc.copy().isAPartOf(hdsxrec);
	public PackedDecimalData nextIntDate = DD.hintndt.copy().isAPartOf(hdsxrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hdsxrec);
	public PackedDecimalData osInterest = DD.hintos.copy().isAPartOf(hdsxrec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(hdsxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HdsxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HdsxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HdsxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HdsxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdsxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HdsxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdsxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HDSXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"CRTABLE, " +
							"HDVBALC, " +
							"HINTNDT, " +
							"TRANNO, " +
							"HINTOS, " +
							"RCESDTE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     crtable,
                                     balSinceLastCap,
                                     nextIntDate,
                                     tranno,
                                     osInterest,
                                     riskCessDate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		crtable.clear();
  		balSinceLastCap.clear();
  		nextIntDate.clear();
  		tranno.clear();
  		osInterest.clear();
  		riskCessDate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHdsxrec() {
  		return hdsxrec;
	}

	public FixedLengthStringData getHdsxpfRecord() {
  		return hdsxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHdsxrec(what);
	}

	public void setHdsxrec(Object what) {
  		this.hdsxrec.set(what);
	}

	public void setHdsxpfRecord(Object what) {
  		this.hdsxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hdsxrec.getLength());
		result.set(hdsxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}