package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh504screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh504ScreenVars sv = (Sh504ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh504screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh504ScreenVars screenVars = (Sh504ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.zbinstprem.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.riskCessAge.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.premCessDateDisp.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.bappmeth.setClassString("");
		screenVars.optextind.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.ratypind.setClassString("");
		screenVars.pbind.setClassString("");
		screenVars.polinc.setClassString("");
		screenVars.select.setClassString("");
		screenVars.numavail.setClassString("");
		screenVars.numapp.setClassString("");
		screenVars.zdivopt.setClassString("");
		screenVars.payeesel.setClassString("");
		screenVars.paymth.setClassString("");
		screenVars.pymdesc.setClassString("");
		screenVars.paycurr.setClassString("");
		screenVars.bankaccreq.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.taxind.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		screenVars.payeenme.setClassString("");
		screenVars.prmbasis.setClassString("");/*ILIFE-3421*/
	screenVars.dialdownoption.setClassString("");//BRD-NBP-011
	screenVars.exclind.setClassString("");
	screenVars.aepaydet.setClassString("");}

/**
 * Clear all the variables in Sh504screen
 */
	public static void clear(VarModel pv) {
		Sh504ScreenVars screenVars = (Sh504ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.zbinstprem.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.sumin.clear();
		screenVars.currcd.clear();
		screenVars.riskCessAge.clear();
		screenVars.riskCessTerm.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		screenVars.premCessDateDisp.clear();
		screenVars.premCessDate.clear();
		screenVars.mortcls.clear();
		screenVars.liencd.clear();
		screenVars.bappmeth.clear();
		screenVars.optextind.clear();
		screenVars.instPrem.clear();
		screenVars.ratypind.clear();
		screenVars.pbind.clear();
		screenVars.polinc.clear();
		screenVars.select.clear();
		screenVars.numavail.clear();
		screenVars.numapp.clear();
		screenVars.zdivopt.clear();
		screenVars.payeesel.clear();
		screenVars.paymth.clear();
		screenVars.pymdesc.clear();
		screenVars.paycurr.clear();
		screenVars.bankaccreq.clear();
		screenVars.zlinstprem.clear();
		screenVars.taxamt.clear();
		screenVars.taxind.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		screenVars.payeenme.clear();
		screenVars.prmbasis.clear();/*ILIFE-3421*/
		screenVars.dialdownoption.clear(); //BRD-NBP-011
		screenVars.exclind.clear();	
		screenVars.aepaydet.clear();	}
}