package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HmtdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:32
 * Class transformed from HMTDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HmtdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 95;
	public FixedLengthStringData hmtdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hmtdpfRecord = hmtdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hmtdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hmtdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hmtdrec);
	public PackedDecimalData hactval = DD.hactval.copy().isAPartOf(hmtdrec);
	public PackedDecimalData hemv = DD.hemv.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hmtdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hmtdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HmtdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HmtdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HmtdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HmtdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HmtdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HmtdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HmtdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HMTDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"PLNSFX, " +
							"HACTVAL, " +
							"HEMV, " +
							"HCNSTCUR, " +
							"TYPE_T, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     crtable,
                                     planSuffix,
                                     hactval,
                                     hemv,
                                     hcnstcur,
                                     fieldType,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		planSuffix.clear();
  		hactval.clear();
  		hemv.clear();
  		hcnstcur.clear();
  		fieldType.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHmtdrec() {
  		return hmtdrec;
	}

	public FixedLengthStringData getHmtdpfRecord() {
  		return hmtdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHmtdrec(what);
	}

	public void setHmtdrec(Object what) {
  		this.hmtdrec.set(what);
	}

	public void setHmtdpfRecord(Object what) {
  		this.hmtdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hmtdrec.getLength());
		result.set(hmtdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}