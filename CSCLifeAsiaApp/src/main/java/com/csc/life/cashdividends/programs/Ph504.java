/*
 * File: Ph504.java
 * Date: 30 August 2009 1:03:41
 * Author: Quipoz Limited
 *
 * Class transformed from PH504.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.ClbaddbTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.cashdividends.screens.Sh504ScreenVars;
import com.csc.life.cashdividends.tablestructures.Th500rec;
import com.csc.life.cashdividends.tablestructures.Th505rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovttrmTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.underwriting.dataaccess.UndcTableDAM;
import com.csc.life.underwriting.tablestructures.T6768rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* TERM (PAR WITH DIVIDEND) COVERAGE GENERIC SCREEN
*
* It will capture details of Term Based Life Coverage and Riders
* with Cash Dividend Processing.
*
***********************************************************************
* </pre>
*/
public class Ph504 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH504");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaKillFlag = new FixedLengthStringData(1);
	private Validator forcedKill = new Validator(wsaaKillFlag, "Y");

	private FixedLengthStringData wsaaCtrltime = new FixedLengthStringData(1);
	private Validator firsttime = new Validator(wsaaCtrltime, "Y");
	private Validator nonfirst = new Validator(wsaaCtrltime, "N");
	private String premReqd = "N";
	private FixedLengthStringData wsaaUndwrule = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(5).init(SPACES);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator nonplan = new Validator(wsaaPlanproc, "N");
	private Validator plan = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	private Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaYaFlag = new FixedLengthStringData(1);
	private Validator yaFound = new Validator(wsaaYaFlag, "Y");

	private FixedLengthStringData wsaaXaFlag = new FixedLengthStringData(1);
	private Validator xaFound = new Validator(wsaaXaFlag, "Y");

	private FixedLengthStringData wsaaT6768Found = new FixedLengthStringData(1);
	private Validator t6768NotFound = new Validator(wsaaT6768Found, "N");

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");
	private PackedDecimalData wsaaNumavail = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaWorkCredit = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSBB-JOINT-LIFE-DETS */
	private PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);

	private FixedLengthStringData wsaaT6768Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6768Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6768Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT6768Curr = new FixedLengthStringData(3).isAPartOf(wsaaT6768Key, 4).init(SPACES);
	private FixedLengthStringData wsaaT6768Sex = new FixedLengthStringData(1).isAPartOf(wsaaT6768Key, 7).init(SPACES);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);

	private FixedLengthStringData wsaaUnderwritingReqd = new FixedLengthStringData(1).init("N");
	private Validator underwritingReqd = new Validator(wsaaUnderwritingReqd, "Y");
	private ZonedDecimalData wsaaAge00Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAge01Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAdjustedAge = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaUndwAge = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaXa = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaYa = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZa = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaSub00 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub01 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaCovtlnbParams = new FixedLengthStringData(219);
	private FixedLengthStringData wsaaCovtlnbDataKey = new FixedLengthStringData(64).isAPartOf(wsaaCovtlnbParams, 49);
	private FixedLengthStringData wsaaCovtlnbCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovtlnbDataKey, 11);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClbaddbTableDAM clbaddbIO = new ClbaddbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
		/*Coverage transactions - term*/
	private CovttrmTableDAM covttrmIO = new CovttrmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Premium breakdown additions to the covR.*/
	private PovrTableDAM povrIO = new PovrTableDAM();
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private UndcTableDAM undcIO = new UndcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Gensswrec gensswrec = new Gensswrec();
	private Premiumrec premiumrec = new Premiumrec();
	private T2240rec t2240rec = new T2240rec();
	private T5585rec t5585rec = new T5585rec();
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T5690rec t5690rec = new T5690rec();
	private T6005rec t6005rec = new T6005rec();
	private T6768rec t6768rec = new T6768rec();
	private Th500rec th500rec = new Th500rec();
	private Th505rec th505rec = new Th505rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Wssplife wssplife = new Wssplife();
	private Sh504ScreenVars sv = ScreenProgram.getScreenVars( Sh504ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private boolean loadingFlag = false;/* BRD-306 */
	private boolean premiumflag = false;//ILIFE-3421 starts
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();//ILIFE-3421 ends
	private String occuptationCode="";
	private boolean prmbasisFlag=false;
	private boolean dialdownFlag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	boolean isEndMat = false;
	private String aepaydet = "-";
	private static final String chdrenqrec = "CHDRENQREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private static final String TA66 = "TA66";
	private List<Itempf> itempfListtr529;
	private List<Itempf> itempfListtr530;
	private static final String TR530 = "TR530";
	private static final String itemcoy = "2";
	private static final String TR529 = "TR529";
  	private Itempf itempf = new Itempf();
  	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
  	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	  	
  	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	private int fupno = 0;
	private List<Fluppf> fluplnbList = new ArrayList<Fluppf>();
	private T5661rec t5661rec = new T5661rec();
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private Ta610rec ta610rec = new Ta610rec();
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	boolean NBPRP056Permission  = false;
	private T3644rec t3644rec = new T3644rec();
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	private static final String t5661 = "T5661";
	private static final String NBPRP056="NBPRP056";
	private List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> ta610List = new ArrayList<Itempf>();	//ICIL-1494
	//ILJ-43
	private boolean contDtCalcFlag = false;
	private String cntDteFeature = "NBPRP113";
	//end
  	
	private boolean exclFlag = false;	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		cont1010,
		cont1012,
		cont1015,
		premmeth1020,
		cont1030,
		exit1090,
		riskCessTerm1310,
		exit1340,
		premCessTerm1360,
		exit1390,
		exit1590,
		nextColumn1620,
		moveDefaults1650,
		preExit,
		redisplay2080,
		exit2090,
		checkRcessFields2230,
		ageAnniversary2241,
		term2242,
		termExact2243,
		check2244,
		checkOccurance2245,
		checkTermFields2250,
		checkComplete2255,
		checkMortcls2260,
		loop2265,
		checkLiencd2270,
		loop2275,
		checkMore2280,
		mop2286,
		exit2290,
		calc2310,
		exit2390,
		exit3090,
		exit3790,
		adjust3b10,
		adjust3c10,
		go3d10,
		cont4710,
		cont4715,
		cont4717,
		cont4720,
		exit4790,
		rolu4805,
		cont4810,
		cont4820,
		readCovttrm4830,
		cont4835,
		cont4837,
		cont4840,
		exit4890, 
		a250CallTaxSubr, 
		a290Exit
	}

	public Ph504() {
		super();
		screenVars = sv;
		new ScreenModel("Sh504", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {		
			//TMLII-268
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			//TMLII-268
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case cont1010: 
					cont1010();
					plan1010();
				case cont1012: 
					cont1012();
				case cont1015: 
					cont1015();
				case premmeth1020: 
					premmeth1020();
				case cont1030: 
					cont1030();
					cont1060();
					prot1070();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");//ilj-43
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (isNE(wsspcomn.flag, "I")) {  //MTL130
				 sv.instPrem.set(0);		//MTL130
				 calcPremium2300();			//MTL130
				 sv.instprmErr.set(" ");    //MTL130
			}
			goTo(GotoLabel.exit1090);
		}
		
		sv.dataArea.set(SPACES);
		//ilj-43	
		if(!contDtCalcFlag) {
			//sv.riskCessAgeOut[varcom.nd.toInt()].set("Y");
			sv.contDtCalcScreenflag.set("N");
			
		}
	//end
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}
		premiumrec.premiumRec.set(SPACES);
		initialize(premiumrec.premiumRec);
		wsaaBatckey.set(wsspcomn.batchkey);
		wssplife.fupno.set(ZERO);
		/*    Dummy field initilisation for prototype version.*/
		sv.anbAtCcd.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		prmbasisFlag=false;
		sv.dialdownoption.set(SPACES);
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.adjustageamt.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		/*BRD-306 END */
		sv.prmbasis.set(SPACES);
		sv.numapp.set(ZERO);
		sv.numavail.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.polinc.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.sumin.set(ZERO);
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		wsaaCtrltime.set("N");
		wsaaFirstTaxCalc.set("Y");
		/* Move ZERO to WSSP area to be used in linkage*/
		wssplife.bigAmt.set(ZERO);
		wssplife.occdate.set(ZERO);
		/* Read CHDRLNB (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*BRD-306 START */
		sv.cnttype.set(chdrlnbIO.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*BRD-306 END */
		isEndMat = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "PRPRO001", appVars, "IT");	
		if(!isEndMat){
			sv.aepaydetOut[varcom.nd.toInt()].set("Y");
			aepaydet = "+";
		}
		
		
		
		/* Read TR675 to see whether Underwriting is required for product  */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr675);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr675)
		|| isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaUnderwritingReqd.set("N");
		}
		else {
			wsaaUnderwritingReqd.set("Y");
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRLNB-CNTTYPE        TO ITEM-ITEMITEM.                */
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*     MOVE '***'              TO ITEM-ITEMITEM                 */
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else {
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				} //MTL002
			}
		}
		sv.zagelitOut[varcom.hi.toInt()].set("N");
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		/* Read the PAYR record to get the Billing Details.*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getPolinc(),0)) {
			wsaaPlanproc.set("N");
		}
		else {
			wsaaPlanproc.set("Y");
		}
		if (nonplan.isTrue()) {
			sv.polinc.set(1);
			sv.numavail.set(1);
			sv.numapp.set(1);
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		if (isEQ(covtlnbIO.getRider(),"00")) {
			goTo(GotoLabel.cont1010);
		}
		wsaaCovtlnbParams.set(covtlnbIO.getParams());
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setDataKey(wsaaCovtlnbDataKey);
		covtlnbIO.setRider(ZERO);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.endp)
		&& isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isNE(covtlnbIO.getCoverage(),wsaaCovtlnbCoverage)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			wsaaKillFlag.set("Y");
			goTo(GotoLabel.exit1090);
		}
		covtlnbIO.setParams(wsaaCovtlnbParams);
	}

protected void cont1010()
	{
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setSeqnbr(ZERO);
		covttrmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covttrmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covttrmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(),varcom.oK)
		&& isNE(covttrmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(),covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(),covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
		if (isEQ(covttrmIO.getStatuz(),varcom.endp)) {
			covttrmIO.setNonKey(SPACES);
			covttrmIO.setAnbccd(1, 0);
			covttrmIO.setAnbccd(2, 0);
			covttrmIO.setSingp(0);
			covttrmIO.setInstprem(0);
			covttrmIO.setZbinstprem(0);
			covttrmIO.setZlinstprem(0);
			covttrmIO.setNumapp(0);
			covttrmIO.setPremCessAge(0);
			covttrmIO.setPremCessTerm(0);
			covttrmIO.setPolinc(0);
			covttrmIO.setRiskCessAge(0);
			covttrmIO.setRiskCessTerm(0);
			covttrmIO.setSumins(0);
			covttrmIO.setBenCessAge(0);
			covttrmIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
		}
	}
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covtlnbIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtlnbIO.getChdrnum().toString());
	rcvdPFObject.setLife(covtlnbIO.getLife().toString());
	rcvdPFObject.setCoverage(covtlnbIO.getCoverage().toString());
	rcvdPFObject.setRider(covtlnbIO.getRider().toString());
	rcvdPFObject.setCrtable(covtlnbIO.getCrtable().toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}

protected void plan1010()
	{
		if (firsttime.isTrue()) {
			covttrmIO.setPolinc(ZERO);
			covttrmIO.setNumapp(ZERO);
			covttrmIO.setSeqnbr(1);
		}
		if(isEndMat)
		{
			itempfListtr530=itempfDAO.getAllItemitem("IT",itemcoy, TR530, covtlnbIO.getCrtable().toString());/* IJTI-1523 */
			itempfListtr529=itempfDAO.getAllItemitem("IT",itemcoy, TR529, covtlnbIO.getCrtable().toString());/* IJTI-1523 */
			
			if ((itempfListtr530.size()==0) && (itempfListtr529.size()==0) )  {
				
				sv.aepaydetOut[varcom.nd.toInt()].set("Y");
				aepaydet = "+";
			}
		}
		if (plan.isTrue()) {
			compute(wsaaCredit, 0).set((sub(chdrlnbIO.getPolinc(),covttrmIO.getPolinc())));
		}
		if (isEQ(chdrlnbIO.getPolinc(),1)
		&& (isEQ(covttrmIO.getPolinc(),0)
		|| isEQ(covttrmIO.getPolinc(),1))) {
			wsaaPlanproc.set("N");
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
		wsaaNumavail.set(chdrlnbIO.getPolinc());
		/* BRD-306 starts */
		loadingFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		/* BRD-306 ends */
		dialdownFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		/*ILIFE-3421 starts*/
		premiumflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		/*ILIFE-3421 ends*/
	//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{	
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends
		exclFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
			
		}
		else{
			checkExcl();
		}
		if (firsttime.isTrue()) {
			covttrmIO.setNumapp(wsaaNumavail);
			goTo(GotoLabel.cont1012);
		}
		sv.riskCessDate.set(covttrmIO.getRiskCessDate());
		sv.premCessDate.set(covttrmIO.getPremCessDate());
		sv.riskCessAge.set(covttrmIO.getRiskCessAge());
		sv.premCessAge.set(covttrmIO.getPremCessAge());
		sv.riskCessTerm.set(covttrmIO.getRiskCessTerm());
		sv.premCessTerm.set(covttrmIO.getPremCessTerm());
		sv.zdivopt.set(covttrmIO.getZdivopt());
		sv.payeesel.set(covttrmIO.getPayclt());
		
		if (isNE(sv.payeesel,SPACES)) {
			cltsIO.setDataKey(SPACES);
			cltsIO.setClntnum(sv.payeesel);
			payeeName1200();
		}
		sv.paymth.set(covttrmIO.getPaymth());
		sv.paycurr.set(covttrmIO.getPaycurr());
		if (isNE(covttrmIO.getBankacckey(),SPACES)) {
			sv.bankaccreq.set("+");
		}
		if (isNE(covttrmIO.getSingp(),ZERO)) {
			sv.instPrem.set(covttrmIO.getSingp());
			if (isEQ(covttrmIO.getInstprem(),0)) {
				sv.zbinstprem.set(covttrmIO.getZbinstprem());
				sv.zlinstprem.set(covttrmIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covttrmIO.getZbinstprem());
			sv.zlinstprem.set(covttrmIO.getZlinstprem());
			sv.instPrem.set(covttrmIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covttrmIO.getLoadper());
		sv.rateadj.set(covttrmIO.getRateadj());
		sv.fltmort.set(covttrmIO.fltmort);
		sv.premadj.set(covttrmIO.getPremadj());
		sv.adjustageamt.set(covttrmIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covttrmIO.getSumins());
		sv.mortcls.set(covttrmIO.getMortcls());
		sv.liencd.set(covttrmIO.getLiencd());
		if (isEQ(covttrmIO.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set(SPACES);
		}
		sv.bappmeth.set(covttrmIO.getBappmeth());
		if (isEQ(sv.paymth,SPACES)) {
			sv.pymdesc.set(SPACES);
			goTo(GotoLabel.cont1012);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5690);
		descIO.setDescitem(covttrmIO.getPaymth());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pymdesc.fill("?");
		}
		else {
			sv.pymdesc.set(descIO.getLongdesc());
		}
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(),covtlnbIO.getCrtable().toString(),covtlnbIO.getLife().toString(),covtlnbIO.getCoverage().toString(),covtlnbIO.getRider().toString());
	if (exclpf==null) {
		sv.exclind.set(" ");
		
	}
	else {
		sv.exclind.set("+");
		
	}
}

protected void cont1012()
	{
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covttrmIO.getNumapp());
		/* Read  the  contract  definition  details  from  T5688 for the*/
		/* contract  type  held  on  CHDRLNB. Access the version of this*/
		/* item for the original commencement date of the risk.*/
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Read  the  general  coverage/rider details from T5687 and the*/
		/* traditional/term  edit rules from TH505 for the coverage type   */
		/* held  on  COVTLNB.  Access  the version of this  item for the*/
		/* original commencement date of the risk.*/
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(covtlnbIO.getCrtable(),covtlnbIO.getCrtable())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		checkRacd1100();
		setupBonus1200();
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covtlnbIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1500();
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtlnbIO.getCrtable());
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		itdmIO.setItemtabl(tablesInner.th505);
		if (isEQ(t5671rec.pgm[1],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.th505)
		|| isNE(itdmIO.getItemitem(),wsbbTranCurrency)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			th505rec.th505Rec.set(itdmIO.getGenarea());
		}
		else {
			th505rec.th505Rec.set(SPACES);
			th505rec.ageIssageFrms.fill("0");
			th505rec.ageIssageTos.fill("0");
			th505rec.termIssageFrms.fill("0");
			th505rec.termIssageTos.fill("0");
			th505rec.premCessageFroms.fill("0");
			th505rec.premCessageTos.fill("0");
			th505rec.premCesstermFroms.fill("0");
			th505rec.premCesstermTos.fill("0");
			th505rec.riskCessageFroms.fill("0");
			th505rec.riskCessageTos.fill("0");
			th505rec.riskCesstermFroms.fill("0");
			th505rec.riskCesstermTos.fill("0");
			th505rec.sumInsMax.set(ZERO);
			th505rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode,SPACES)) {
				scrnparams.errorCode.set(errorsInner.hl52);
			}
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
		if (firsttime.isTrue()) {
			dividendOption1800();
			if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
				sv.zdivopt.set(th505rec.zdefdivopt);
			}
			sv.payeesel.set(chdrlnbIO.getCownnum());
			if (isNE(sv.payeesel,SPACES)) {
				cltsIO.setDataKey(SPACES);
				cltsIO.setClntnum(sv.payeesel);
				payeeName1200();
			}
		}
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*    Save Main Life details within Working Storage for later use.*/
		wsaaAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaCltdob.set(lifelnbIO.getCltdob());
		wsaaSex.set(lifelnbIO.getCltsex());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbSex.set(SPACES);
			wsbbAnbAtCcd.set(0);
			goTo(GotoLabel.cont1015);
		}
		wsbbAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsbbCltdob.set(lifelnbIO.getCltdob());
		wsbbSex.set(lifelnbIO.getCltsex());
		/*    Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifelnbIO.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
		if(isNE(cltsIO.getStatcode(), SPACES)&& premiumflag){
			occuptationCode=cltsIO.getStatcode().toString();
		}
	}

protected void cont1015()
	{
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			wsaaLifeind.set("S");
		}
		else {
			wsaaLifeind.set("J");
		}
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		if (isEQ(t5687rec.jlifePresent,SPACES)) {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		itemIO.setItemitem(t5687rec.premmeth);
		if (nonfirst.isTrue()) {
			if (isEQ(covttrmIO.getJlife(),"01")) {
				sv.select.set("J");
			}
			else {
				sv.select.set("L");
			}
		}
	}

protected void premmeth1020()
	{
		premReqd = "N";
		if (isNE(t5687rec.bbmeth,SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.cont1030);
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/* ILIFE-3142 End*/
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void cont1030()
	{
		if (isEQ(sv.paycurr,SPACES)) {
			sv.paycurr.set(payrIO.getCntcurr());
		}
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		sv.chdrnum.set(lifelnbIO.getChdrnum());
		sv.coverage.set(covtlnbIO.getCoverage());
		sv.currcd.set(payrIO.getCntcurr());
		sv.life.set(lifelnbIO.getLife());
		sv.rider.set(covtlnbIO.getRider());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		
		
		if(isEQ(th505rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
		if (isEQ(th505rec.sumInsMax,0)
		&& isEQ(th505rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)
		&& isNE(th505rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(th505rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(sv.sumin,sv.numapp),sv.numavail)));
			}
		}
		if (isEQ(th505rec.mortclss,SPACES)) {
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(th505rec.mortcls01,SPACES)
		&& isEQ(th505rec.mortcls02,SPACES)
		&& isEQ(th505rec.mortcls03,SPACES)
		&& isEQ(th505rec.mortcls04,SPACES)
		&& isEQ(th505rec.mortcls05,SPACES)
		&& isEQ(th505rec.mortcls06,SPACES)) {
			sv.mortcls.set(th505rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(th505rec.liencds,SPACES)) {
			/*sv.liencdOut[varcom.nd.toInt()].set("Y");*/
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (nonfirst.isTrue()
		&& isEQ(covttrmIO.getJlife(),"01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults1600();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(th505rec.eaage,SPACES))) {
			riskCessDate1300();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(th505rec.eaage,SPACES))) {
			premCessDate1350();
		}
		if (isNE(th505rec.eaage,SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void cont1060()
	{
		/* OPTIONS AND EXTRAS*/
		/* If options and extras are  not  allowed (as defined by TH505)   */
		/* non-display and protect the fields.*/
		/* Otherwise,  read the  options  and  extras  details  for  the*/
		/* current coverage/rider.  If any  records  exist, put a '+' in*/
		/* the Options/Extras indicator (to show that there are some).*/
		if (isEQ(th505rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext1700();
		}
	}

	/**
	* <pre>
	* ENQUIRY MODE
	* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to
	* protect  all  input  capable  fields  except  the  indicators
	* controlling  where  to  switch  to  next  (options and extras
	* indicator).
	* </pre>
	*/
protected void prot1070()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.zdivoptOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkRacd1100()
	{
		readRacd1110();
	}

protected void readRacd1110()
	{
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		racdlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		racdlnbIO.setLife(covtlnbIO.getLife());
		racdlnbIO.setCoverage(covtlnbIO.getCoverage());
		racdlnbIO.setRider(covtlnbIO.getRider());
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setCestype("2");
		racdlnbIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		racdlnbIO.setFormat(formatsInner.racdlnbrec);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)
		&& isNE(racdlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(racdlnbIO.getLife(),covtlnbIO.getLife())
		|| isNE(racdlnbIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(racdlnbIO.getRider(),covtlnbIO.getRider())
		|| isNE(racdlnbIO.getPlanSuffix(),ZERO)
		|| isNE(racdlnbIO.getSeqno(),ZERO)
		|| isNE(racdlnbIO.getCestype(),"2")
		|| isEQ(racdlnbIO.getStatuz(),varcom.endp)) {
			if (isEQ(sv.ratypind,"X")) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.ratypind.set(SPACES);
				sv.ratypindOut[varcom.nd.toInt()].set("Y");
				sv.ratypindOut[varcom.pr.toInt()].set("Y");
			}
		}
		else {
			sv.ratypind.set("+");
			sv.ratypindOut[varcom.nd.toInt()].set("N");
			sv.ratypindOut[varcom.pr.toInt()].set("N");
		}
	}

protected void setupBonus1200()
	{
			para1200();
		}

protected void para1200()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemitem(covtlnbIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(itemIO.getGenarea());
		if (isEQ(t6005rec.ind,"1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void riskCessDate1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1300();
				case riskCessTerm1310: 
					riskCessTerm1310();
				case exit1340: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1300()
	{
		if (isEQ(sv.riskCessAge,0)) {
			goTo(GotoLabel.riskCessTerm1310);
		}
		if (isEQ(th505rec.eaage,"A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge,wszzAnbAtCcd));
		}
		if (isEQ(th505rec.eaage,"E")
		|| isEQ(th505rec.eaage,SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.riskCessAge);
		}
		callDatcon21400();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1340);
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1340);
	}

protected void riskCessTerm1310()
	{
		if (isEQ(sv.riskCessTerm,0)) {
			sv.rcesdteErr.set(errorsInner.e186);
			return ;
		}
		if (isEQ(th505rec.eaage,"A")
		|| isEQ(th505rec.eaage,SPACES)) {
			datcon2rec.freqFactor.set(sv.riskCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(th505rec.eaage,"E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.riskCessTerm,wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(),wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21400();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
	}

protected void premCessDate1350()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1350();
				case premCessTerm1360: 
					premCessTerm1360();
				case exit1390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1350()
	{
		if (isEQ(sv.premCessAge,0)) {
			goTo(GotoLabel.premCessTerm1360);
		}
		if (isEQ(th505rec.eaage,"A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge,wszzAnbAtCcd));
		}
		if (isEQ(th505rec.eaage,"E")
		|| isEQ(th505rec.eaage,SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.premCessAge);
		}
		callDatcon21400();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1390);
		}
		sv.premCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1390);
	}

protected void premCessTerm1360()
	{
		if (isEQ(sv.premCessTerm,0)) {
			return ;
		}
		if (isEQ(th505rec.eaage,"A")
		|| isEQ(th505rec.eaage,SPACES)) {
			datcon2rec.freqFactor.set(sv.premCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(th505rec.eaage,"E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.premCessTerm,wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(),wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21400();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.premCessDate.set(datcon2rec.intDate2);
	}

protected void callDatcon21400()
	{
		/*PARA*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadHeading1500()
	{
		try {
			loadScreen1510();
		}
		catch (GOTOException e){
		}
	}

protected void loadScreen1510()
	{
		wsaaHeading.set(SPACES);
		for (wsaaX.set(30); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1530();
		}
		wsaaX.add(1);
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1590);
	}

protected void moveChar1530()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkDefaults1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					searchTable1610();
				case nextColumn1620: 
					nextColumn1620();
				case moveDefaults1650: 
					moveDefaults1650();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void searchTable1610()
	{
		sub1.set(0);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
	}

protected void nextColumn1620()
	{
		sub1.add(1);
		if (isGT(sub1,wsaaMaxOcc)) {
			goTo(GotoLabel.moveDefaults1650);
		}
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.ageIssageTo[sub1.toInt()])
			&& isEQ(th505rec.riskCessageFrom[sub1.toInt()],th505rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(th505rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.ageIssageTo[sub1.toInt()])
			&& isEQ(th505rec.premCessageFrom[sub1.toInt()],th505rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(th505rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.termIssageTo[sub1.toInt()])
			&& isEQ(th505rec.riskCesstermFrom[sub1.toInt()],th505rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(th505rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,th505rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,th505rec.termIssageTo[sub1.toInt()])
			&& isEQ(th505rec.premCesstermFrom[sub1.toInt()],th505rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(th505rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		goTo(GotoLabel.nextColumn1620);
	}

protected void moveDefaults1650()
	{
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkLext1700()
	{
		readLext1710();
	}

protected void readLext1710()
	{
		lextIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lextIO.setChdrnum(covtlnbIO.getChdrnum());
		lextIO.setLife(covtlnbIO.getLife());
		lextIO.setCoverage(covtlnbIO.getCoverage());
		lextIO.setRider(covtlnbIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(),lextIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(),lextIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(),lextIO.getLife())
		|| isNE(covtlnbIO.getCoverage(),lextIO.getCoverage())
		|| isNE(covtlnbIO.getRider(),lextIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

protected void dividendOption1800()
	{
		/*DIV-OPT*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.th505);
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.th505)
		|| isNE(itdmIO.getItemitem(),wsbbTranCurrency)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			itdmIO.setStatuz(varcom.endp);
		}
		else {
			th505rec.th505Rec.set(itdmIO.getGenarea());
		}
		/*EXIT*/
	}

protected void payeeName1200()
{
	/*PAYEE*/
	cltsIO.setClntpfx("CN");
	cltsIO.setClntcoy(wsspcomn.fsuco);
	cltsIO.setFunction(varcom.readr);
	cltsIO.setFormat(formatsInner.cltsrec);
	SmartFileCode.execute(appVars, cltsIO);
	if (isNE(cltsIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(cltsIO.getParams());
		fatalError600();
	}
	plainname();
	sv.payeenme.set(wsspcomn.longconfname);
	/*EXIT*/
}

protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (forcedKill.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		readPovr5300();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(),lifelnbIO.getChdrnum())
		|| isNE(povrIO.getLife(),lifelnbIO.getLife())
		|| isNE(povrIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(),covtlnbIO.getRider())) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			a200CheckCalcTax();
		}
		if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void callScreenIo2010()
	{
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					cont2040();
				case redisplay2080: 
					redisplay2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			if (isEQ(wsaaCredit,0)
			|| isEQ(wsaaCredit,chdrlnbIO.getPolinc())) {
				goTo(GotoLabel.exit2090);
			}
			else {
				sv.numappErr.set(errorsInner.g622);
				goTo(GotoLabel.redisplay2080);
			}
		}
		/*VALIDATE*/
		a100CheckLimit();
		if (isNE(wsspcomn.flag,"I")) {
			editCoverage2200();
		}
	}

protected void cont2040()
	{
		/* If  options/extras already exist, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed. If*/
		/* options  and  extras  are  requested,  DO  NOT  CALCULATE THE*/
		/* PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind,"+")
		&& isNE(sv.optextind,"X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		/* If  reassurance   already exists, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed.*/
		if (isNE(sv.ratypind, " ")
		&& isNE(sv.ratypind,"+")
		&& isNE(sv.ratypind,"X")) {
			sv.ratypindErr.set(errorsInner.g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(errorsInner.g620);
				}
		/* Check the premium breakdown indicator.*/
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind,"+")
		&& isNE(sv.pbind,"X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		/* Bank account required option must either be ' ', '+' or 'X'*/
		if (isNE(sv.bankaccreq, " ")
		&& isNE(sv.bankaccreq,"+")
		&& isNE(sv.bankaccreq,"X")) {
			sv.bankaccreqErr.set(errorsInner.g620);
		}
		/* Tax indicator must either be ' ', '+' or 'X'                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/* If everything else is O-K calculate the Installment Premium.*/
		if (isNE(sv.optextind,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& isNE(wsspcomn.flag,"I")) {
			calcPremium2300();
		}
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.redisplay2080);
		}
		compute(sv.zbinstprem, 2).set(sub(sv.instPrem,sv.zlinstprem));
		/* If 'ROLD' was entered,check this is not the first page.*/
		if (isEQ(scrnparams.statuz,varcom.rold)
		&& isEQ(wsaaFirstSeqnbr,covttrmIO.getSeqnbr())) {
			scrnparams.errorCode.set(errorsInner.e027);
			goTo(GotoLabel.redisplay2080);
		}
		validateOccupationOrOccupationClass();	//ICIL-1494
		/* If 'CALC' was entered then re-display the screen.*/
		if (isNE(scrnparams.statuz,varcom.calc)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void redisplay2080()
	{
		wsspcomn.edterror.set("Y");
	}


protected void validateOccupationOrOccupationClass() {
	isFollowUpRequired=false;
	NBPRP056Permission  = FeaConfg.isFeatureExist("2",NBPRP056, appVars, "IT");
	if(NBPRP056Permission &&  lifelnbIO != null) {
		readTA610();
		String occupation = lifelnbIO.getOccup().toString();
		if( occupation != null && !occupation.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu); 
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(occupation);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu); 
						break;
					}
				}	
			}
			
		}
	}
	
}



private void readTA610() {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tA610);
	itempf.setItemitem(covtlnbIO.getCrtable().toString());
	itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
	itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
	ta610List = itempfDAO.findByItemDates(itempf);	//ICIL-1494
	if (ta610List.size()>0 && ta610List.get(0).getGenarea()!=null) {
		ta610rec.tA610Rec.set(StringUtil.rawToString(ta610List.get(0).getGenarea()));
	}	  
}

protected void getOccupationClass2900(String occupation) {
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
}
protected void editCoverage2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					editFund2200();
					checkSumin2225();
				case checkRcessFields2230: 
					checkRcessFields2230();
					checkPcessFields2235();
					checkAgeTerm2240();
				case ageAnniversary2241: 
					ageAnniversary2241();
				case term2242: 
					term2242();
				case termExact2243: 
					termExact2243();
				case check2244: 
					check2244();
				case checkOccurance2245: 
					checkOccurance2245();
				case checkTermFields2250: 
					checkTermFields2250();
				case checkComplete2255: 
					checkComplete2255();
				case checkMortcls2260: 
					checkMortcls2260();
				case loop2265: 
					loop2265();
				case checkLiencd2270: 
					checkLiencd2270();
				case loop2275: 
					loop2275();
				case checkMore2280: 
					checkMore2280();
					checkCashDividend2285();
				case mop2286: 
					mop2286();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void editFund2200()
	{
		if (plan.isTrue()
		&& isEQ(sv.numapp,0)) {
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(sv.numapp,ZERO)) {
			sv.numappErr.set(errorsInner.l001);
		}
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)
		&& isNE(th505rec.sumInsMax,0)) {
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(th505rec.sumInsMin,sv.numapp),sv.polinc)));
			}
		}
	}

protected void checkSumin2225()
	{
		if (plan.isTrue()) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin,sv.polinc),sv.numapp)));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)) {
			goTo(GotoLabel.checkRcessFields2230);
		}
		if (isLT(wsaaSumin,th505rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin,th505rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

	/**
	* <pre>
	*  2) Check the consistency of the risk age and term fields
	*       and  premium  age and term fields. Either, risk age
	*       and  premium age must be used or risk term and
	*       premium  term  must  be  used.  They  must  not  be
	*       combined. Note that  these  only need validating if
	*       they were not defaulted.
	*     NOTE: Age and Term fields may now be mixed.                 
	* </pre>
	*/
protected void checkRcessFields2230()
	{
		if (isEQ(sv.select,"J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent,SPACES)
		&& jointlif.isTrue()) {
			checkDefaults1600();
		}
		if (isGT(sv.riskCessAge,0)
		&& isGT(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(th505rec.eaage,SPACES)
		&& isEQ(sv.riskCessAge,0)
		&& isEQ(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2235()
	{
		if (isGT(sv.premCessAge,0)
		&& isGT(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)
		&& isEQ(sv.rcessageErr,SPACES)
		&& isEQ(sv.rcesstrmErr,SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(th505rec.eaage,SPACES)
		&& isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2240()
	{
		if ((isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))
		|| (isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2260);
		}
		if ((isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))
		|| (isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2260);
		}
		/*  To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (TH505).                                           */
		if (isNE(th505rec.eaage,SPACES)
		|| isEQ(sv.riskCessDate,varcom.vrcmMaxDate)) {
			riskCessDate1300();
		}
		else {
			if (isEQ(sv.rcesdteErr,SPACES)) {
				if (isNE(sv.riskCessAge,0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.riskCessAge);
					callDatcon21400();
					if (isLT(datcon2rec.intDate2,sv.riskCessDate)) {
						sv.rcessageErr.set(errorsInner.h040);
						sv.rcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21400();
						if (isGTE(datcon2rec.intDate2,sv.riskCessDate)) {
							sv.rcessageErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.riskCessTerm,0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.riskCessTerm);
						callDatcon21400();
						if (isLT(datcon2rec.intDate2,sv.riskCessDate)) {
							sv.rcesstrmErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21400();
							if (isGTE(datcon2rec.intDate2,sv.riskCessDate)) {
								sv.rcesstrmErr.set(errorsInner.h040);
								sv.rcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(th505rec.eaage,SPACES)
		|| isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			premCessDate1350();
		}
		else {
			if (isEQ(sv.pcesdteErr,SPACES)) {
				if (isNE(sv.premCessAge,0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.premCessAge);
					callDatcon21400();
					if (isLT(datcon2rec.intDate2,sv.premCessDate)) {
						sv.pcessageErr.set(errorsInner.h040);
						sv.pcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21400();
						if (isGTE(datcon2rec.intDate2,sv.premCessDate)) {
							sv.pcessageErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.premCessTerm,0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.premCessTerm);
						callDatcon21400();
						if (isLT(datcon2rec.intDate2,sv.premCessDate)) {
							sv.pcesstrmErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21400();
							if (isGTE(datcon2rec.intDate2,sv.premCessDate)) {
								sv.pcesstrmErr.set(errorsInner.h040);
								sv.pcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2260);
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate,sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2260);
		}
		/*  Calculate cessasion age and term.*/
		/*  If age is already entered, do not need to re-calculate it*/
		/*  again.*/
		if (isEQ(th505rec.eaage,"A")) {
			goTo(GotoLabel.ageAnniversary2241);
		}
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		goTo(GotoLabel.term2242);
	}

protected void ageAnniversary2241()
	{
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
			wszzRiskCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
			wszzPremCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
	}

protected void term2242()
	{
		/*  If term is already entered, do not need to re-calculate it*/
		/*  again.*/
		if (isEQ(th505rec.eaage,"E")
		|| isEQ(th505rec.eaage, " ")) {
			goTo(GotoLabel.termExact2243);
		}
		if (isEQ(sv.riskCessTerm,ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm,ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		goTo(GotoLabel.check2244);
	}

protected void termExact2243()
	{
		if (isEQ(sv.riskCessTerm,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			compute(wszzRiskCessTerm, 5).set(sub(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			compute(wszzPremCessTerm, 5).set(sub(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
	}

protected void check2244()
	{
		/*  Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
	}

protected void checkOccurance2245()
	{
		x.add(1);
		if (isGT(x,wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2255);
		}
		if ((isEQ(th505rec.ageIssageFrm[x.toInt()],0)
		&& isEQ(th505rec.ageIssageTo[x.toInt()],0))
		|| isLT(wszzAnbAtCcd,th505rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,th505rec.ageIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkTermFields2250);
		}
		if (isGTE(wszzRiskCessAge,th505rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge,th505rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge,th505rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge,th505rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2250()
	{
		if ((isEQ(th505rec.termIssageFrm[x.toInt()],0)
		&& isEQ(th505rec.termIssageTo[x.toInt()],0))
		|| isLT(wszzAnbAtCcd,th505rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,th505rec.termIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkOccurance2245);
		}
		if (isGTE(wszzRiskCessTerm,th505rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm,th505rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm,th505rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm,th505rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2245);
	}

protected void checkComplete2255()
	{
		if (isNE(sv.rcesstrmErr,SPACES)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr,SPACES)
		&& isEQ(sv.premCessTerm,ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr,SPACES)
		&& isEQ(sv.riskCessAge,ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr,SPACES)
		&& isEQ(sv.premCessAge,ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2260()
	{
		x.set(0);
	}

protected void loop2265()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2270);
		}
		if (isNE(th505rec.mortcls[x.toInt()],sv.mortcls)) {
			goTo(GotoLabel.loop2265);
		}
	}

protected void checkLiencd2270()
	{
		if (isEQ(sv.liencd,SPACES)) {
			goTo(GotoLabel.checkMore2280);
		}
		x.set(0);
	}

protected void loop2275()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2280);
		}
		if (isEQ(th505rec.liencd[x.toInt()],SPACES)
		|| isNE(th505rec.liencd[x.toInt()],sv.liencd)) {
			goTo(GotoLabel.loop2275);
		}
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
			sv.prmbasisErr.set(errorsInner.e186);
	}
}
protected void checkIPPfields()
{
	boolean tH505Flag=false;
	for(int counter=1; counter<th505rec.prmbasis.length;counter++){
		if(isNE(sv.prmbasis,SPACES)){
			if (isEQ(th505rec.prmbasis[counter], sv.prmbasis)){
				tH505Flag=true;
				break;
			}
		}
	}
	if(!tH505Flag && isNE(sv.prmbasis,SPACES)){
		sv.prmbasisErr.set("RFV1");// new error code
	}
	
}

protected void checkMore2280()
	{
		/* Check joint life selection indicator (if applicable).*/
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			if (isNE(sv.select,SPACES)
			&& isNE(sv.select,"J")
			&& isNE(sv.select,"L")) {
				sv.selectErr.set(errorsInner.h039);
			}
		}
		if (isEQ(sv.numapp,ZERO)
		&& isEQ(sv.numavail,sv.polinc)) {
			sv.numappErr.set(errorsInner.l001);
		}
		wsaaWorkCredit.set(wsaaCredit);
		if (nonfirst.isTrue()) {
			wsaaWorkCredit.add(covttrmIO.getNumapp());
		}
		wsaaWorkCredit.subtract(sv.numapp);
		if (isLT(wsaaWorkCredit,ZERO)
		&& isGT(sv.numapp,sv.numavail)) {
			sv.numappErr.set(errorsInner.h437);
			wsspcomn.edterror.set("Y");
		}
		/* Check to see if BONUS APPLICATION METHOD is valid for*/
		/* coverage/rider.*/
		if (isNE(sv.bappmeth,SPACES)
		&& isNE(sv.bappmeth,t6005rec.bappmeth01)
		&& isNE(sv.bappmeth,t6005rec.bappmeth02)
		&& isNE(sv.bappmeth,t6005rec.bappmeth03)
		&& isNE(sv.bappmeth,t6005rec.bappmeth04)
		&& isNE(sv.bappmeth,t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
			wsspcomn.edterror.set("Y");
		}
		if(premiumflag)
		{
			checkIPPmandatory();
			checkIPPfields();
		}
	}

protected void checkCashDividend2285()
	{
		dividendOption1800();
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(sv.zdivopt,SPACES)) {
			if (isNE(th505rec.zdefdivopt,SPACES)) {
				sv.zdivopt.set(th505rec.zdefdivopt);
			}
		}
		else {
			for (wsaaIx.set(1); !(isGT(wsaaIx,7)
			|| isEQ(sv.zdivopt,th505rec.zdivopt[wsaaIx.toInt()])
			|| isNE(sv.zdivoptErr,SPACES)); wsaaIx.add(1)){
				if (isNE(sv.zdivopt,th505rec.zdivopt[wsaaIx.toInt()])
				&& isEQ(wsaaIx,7)) {
					sv.zdivoptErr.set(errorsInner.z042);
				}
			}
		}
		if (isEQ(sv.zdivopt,SPACES)) {
			goTo(GotoLabel.mop2286);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.th500);
		itemIO.setItemitem(sv.zdivopt);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.zdivoptErr.set(errorsInner.z038);
			goTo(GotoLabel.mop2286);
		}
		th500rec.th500Rec.set(itemIO.getGenarea());
		if (isEQ(th500rec.payeereq,"Y")) {
			if (isEQ(sv.payeesel,SPACES)) {
				sv.payeeselErr.set(errorsInner.e133);
			}
			if (isEQ(sv.paymth,SPACES)) {
				sv.paymthErr.set(errorsInner.f020);
			}
			if (isEQ(sv.paycurr,SPACES)) {
				sv.paycurrErr.set(errorsInner.h960);
			}
		}
	}

protected void mop2286()
	{
		/*  If bank account details exist, then the currency code for bank */
		/*  account must be the same as the payment currency.              */
		if (isNE(covtlnbIO.getBankacckey(),SPACES)
		&& isNE(covtlnbIO.getBankkey(),SPACES)) {
			clbaddbIO.setParams(SPACES);
			clbaddbIO.setClntcoy(covtlnbIO.getPaycoy());
			clbaddbIO.setClntnum(covtlnbIO.getPayclt());
			clbaddbIO.setBankkey(covtlnbIO.getBankkey());
			clbaddbIO.setBankacckey(covtlnbIO.getBankacckey());
			clbaddbIO.setFunction(varcom.readr);
			clbaddbIO.setFormat(formatsInner.clbaddbrec);
			SmartFileCode.execute(appVars, clbaddbIO);
			if (isNE(clbaddbIO.getStatuz(),varcom.oK)
			&& isNE(clbaddbIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(clbaddbIO.getParams());
				fatalError600();
			}
			if (isEQ(clbaddbIO.getStatuz(),varcom.mrnf)) {
				sv.bankaccreqErr.set(errorsInner.f826);
				sv.bankaccreq.set("X");
			}
			if (isNE(clbaddbIO.getCurrcode(),sv.paycurr)) {
				sv.paycurrErr.set(errorsInner.z043);
			}
		}
		if (isEQ(sv.paymth,SPACES)) {
			sv.pymdesc.set(SPACES);
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5690);
		itemIO.setItemitem(sv.paymth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.paymthErr.set(errorsInner.f177);
			return ;
		}
		t5690rec.t5690Rec.set(itemIO.getGenarea());
		if (isEQ(t5690rec.bankreq,"Y")) {
			if (isNE(sv.bankaccreq,"+")
			&& isNE(sv.bankaccreq,"X")) {
				sv.bankaccreq.set("X");
			}
		}
		else {
			if (isEQ(sv.bankaccreq,"+")
			|| isEQ(sv.bankaccreq,"X")) {
				/*         MOVE E570           TO SH504-BANKACCREQ-ERR*/
				sv.bankaccreq.set(SPACES);
			}
		}
		descIO.setDataKey(itemIO.getDataKey());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pymdesc.fill("?");
		}
		else {
			sv.pymdesc.set(descIO.getLongdesc());
		}
	}

protected void calcPremium2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2300();
				case calc2310: 
					calc2310();
				case exit2390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2300()
	{
		if (plan.isTrue()
		&& isEQ(sv.numapp,0)) {
			goTo(GotoLabel.exit2390);
		}
		if (isNE(t5687rec.bbmeth,SPACES)) {
			goTo(GotoLabel.exit2390);
		}
		if (isEQ(premReqd,"N")) {
			goTo(GotoLabel.calc2310);
		}
		if (isEQ(sv.instPrem,0)) {
			sv.instprmErr.set(errorsInner.g818);
		}
		goTo(GotoLabel.exit2390);
	}

protected void calc2310()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covtlnbIO.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select,"J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(sv.premCessDate);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.cnttype.set(chdrlnbIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		if (isNE(premiumrec.calcBasPrem,ZERO)) {
			premiumrec.calcPrem.set(premiumrec.calcBasPrem);
		}
		if (plan.isTrue()) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,sv.polinc),sv.numapp)));
		}
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			
			if(isNE(sv.prmbasis,SPACES)){
				premiumrec.prmbasis.set(sv.prmbasis);
			}
			premiumrec.occpcode.set("");
			if(isNE(sv.dialdownoption,SPACES)){
				if(sv.dialdownoption.toString().startsWith("0"))
					premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
				else
					premiumrec.dialdownoption.set(sv.dialdownoption);
			}
			else
				premiumrec.dialdownoption.set("100");
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/	
		//****Ticket #ILIFE-2005 end		
		
		if (isEQ(premiumrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2390);
		}
		if (plan.isTrue()) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,sv.numapp),sv.polinc)));
		}
		if (plan.isTrue()) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin,sv.numapp),sv.polinc)));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		/*BRD-306 END */
		if (isEQ(sv.instPrem,0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2390);
		}
		if (isGTE(sv.instPrem,premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2390);
		}
		/* check the tolerance amount against the table entry read above.*/
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem,sv.instPrem));
		sv.instprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub,11)
		|| isEQ(sv.instprmErr,SPACES)); wsaaSub.add(1)){
			searchForTolerance2340();
		}
		if (isEQ(sv.instprmErr,SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem,sv.zlinstprem));
		}
		goTo(GotoLabel.exit2390);
	}

protected void searchForTolerance2340()
	{
		if (isEQ(payrIO.getBillfreq(),t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtol[wsaaSub.toInt()])),100));
			if (isLTE(wsaaDiff,wsaaTol)
			&& isLTE(wsaaDiff,t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()],0)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtoln[wsaaSub.toInt()])),100));
				if (isLTE(wsaaDiff,wsaaTol)
				&& isLTE(wsaaDiff,t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.instprmErr.set(SPACES);
				}
			}
		}
	}

protected void callDatcon32600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
if(rcvdPFObject!=null){
	rcvdPFObject.setChdrcoy(covtlnbIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtlnbIO.getChdrnum().toString());
	rcvdPFObject.setCoverage(covtlnbIO.getCoverage().toString());
	rcvdPFObject.setCrtable(covtlnbIO.getCrtable().toString());
	rcvdPFObject.setLife(covtlnbIO.getLife().toString());
	rcvdPFObject.setRider(covtlnbIO.getRider().toString());
	if(rcvdPFObject.getPrmbasis()!=null){
		if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			rcvdUpdateFlag=true;
		}
	}
	
		//BRD-NBP-011 starts
	if(rcvdPFObject.getDialdownoption()!=null){
		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			rcvdUpdateFlag=true;
		}
	}
		//BRD-NBP-011 ends
		if(rcvdUpdateFlag){
			rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
		}
	}else{
		rcvdPFObject=new Rcvdpf();
		rcvdPFObject.setChdrcoy(covtlnbIO.getChdrcoy().toString());
		rcvdPFObject.setChdrnum(covtlnbIO.getChdrnum().toString());
		rcvdPFObject.setCoverage(covtlnbIO.getCoverage().toString());
		rcvdPFObject.setCrtable(covtlnbIO.getCrtable().toString());
		rcvdPFObject.setLife(covtlnbIO.getLife().toString());
		rcvdPFObject.setRider(covtlnbIO.getRider().toString());
		if(isNE(sv.prmbasis,SPACES)){
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		}
		//snayeni added for ILIFE-3614 -start
		else{
		rcvdPFObject.setPrmbasis(SPACE);
		}
		//snayeni added for ILIFE-3614 -en
		//BRD-NBP-011 starts
		if(isNE(sv.dialdownoption,SPACES)){
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
		}
	
		//BRD-NBP-011 ends
		rcvdPFObject.setUsrprf(covttrmIO.getUserProfile().toString());
		rcvdPFObject.setJobnm(covttrmIO.getJobName().toString());
		//rcvdpf.setDatime(covtrbnIO.getDatime());
		rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
	}
	
}

protected void update3000()
	{
	if(premiumflag || dialdownFlag){
	     insertAndUpdateRcvdpf();}
		try {
			loadWsspFields3010();
			checkCredits3020();
			getUnderwriting3030();
		}
		catch (GOTOException e){
		}
		if(isFollowUpRequired){		
			fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			fupno = fluppfAvaList.size();	//ICIL-1494
			createFollowUps3400();
		}
	}

protected void createFollowUps3400()
{
	try {
		writeFollowUps3430();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}


protected void writeFollowUps3430()
{
	for (wsaaCount.set(1); !(isGT(wsaaCount, 5)) && isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)) {
		boolean entryFlag = false;
		//ILIFE-9209 STARTS
		if(fluppfAvaList!=null && !fluppfAvaList.isEmpty()) {
			for (Fluppf fluppfdata : fluppfAvaList) {
				if(isEQ(fluppfdata.getFupCde(),ta610rec.fupcdes[wsaaCount.toInt()])) 
					entryFlag=true;
			}
				
		}
		//ILIFE-9209 ENDS
		if(!entryFlag) {		//ILIFE-9209 
		writeFollowUp3500();
		}
	}

fluppfDAO.insertFlupRecord(fluplnbList);
}

protected void writeFollowUp3500()
{
	try {
		fluppf = new Fluppf();
		lookUpStatus3510();
		lookUpDescription3520();
		writeRecord3530();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void lookUpStatus3510() {
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
{ 
	descpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());

}

protected void writeRecord3530()
{
	fupno++;
	
	fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
	fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	setPrecision(fupno, 0);
	fluppf.setFupNo(fupno);
	fluppf.setLife("01");
	fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
	fluppf.setFupDt(wsaaToday.toInt());
	fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString());
	fluppf.setFupTyp('P');
	fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	fluppf.setFupRmk(descpf.getLongdesc());/* IJTI-1523 */
	fluppf.setjLife("00");
	fluppf.setTrdt(varcom.vrcmDate.toInt());
	fluppf.setTrtm(varcom.vrcmTime.toInt());
	fluppf.setUserT(varcom.vrcmUser.toInt());
	fluppf.setEffDate(wsaaToday.toInt());
	fluppf.setCrtDate(wsaaToday.toInt());
	fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
	fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
	fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
	fluplnbList.add(fluppf);
}
protected void loadWsspFields3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")
		|| forcedKill.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.pbind,"X")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.bankaccreq,"X")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.optextind,"X")) {
			if (isNE(sv.premCessTerm,ZERO)) {
				wssplife.bigAmt.set(sv.premCessTerm);
				goTo(GotoLabel.exit3090);
			}
			else {
				datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
				datcon3rec.intDate2.set(sv.premCessDate);
				callDatcon33600();
				wsaaFreqFactor.set(datcon3rec.freqFactor);
				wssplife.fupno.set(wsaaFreqFactor);
				goTo(GotoLabel.exit3090);
			}
		}
		/* If TAXIND is selected, do not write / update or delete record   */
		if (isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void checkCredits3020()
	{
		if (nonfirst.isTrue()) {
			wsaaCredit.add(covttrmIO.getNumapp());
		}
		wsaaCredit.subtract(sv.numapp);
		if (isGT(sv.numapp,0)) {
			if (firsttime.isTrue()) {
				initcovr3500();
				setupcovttrm3550();
				covttrmIO.setFunction(varcom.writr);
				initundc3750();
				undcIO.setFunction(varcom.writr);
				wsaaCtrltime.set("N");
			}
			else {
				setupcovttrm3550();
				covttrmIO.setFunction(varcom.updat);
				undcIO.setFunction(varcom.updat);
			}
		}
		if (isLTE(sv.numapp,ZERO)) {
			covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
			covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
			covttrmIO.setLife(covtlnbIO.getLife());
			covttrmIO.setCoverage(covtlnbIO.getCoverage());
			covttrmIO.setRider(covtlnbIO.getRider());
			covttrmIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, covttrmIO);
			if (isNE(covttrmIO.getStatuz(),varcom.oK)
			&& isNE(covttrmIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(covttrmIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(covttrmIO.getStatuz(),varcom.mrnf)) {
					goTo(GotoLabel.exit3090);
				}
				else {
					covttrmIO.setFunction(varcom.delet);
					if (isEQ(wsaaFirstSeqnbr,covttrmIO.getSeqnbr())) {
						wsaaFirstSeqnbr.set(ZERO);
					}
				}
			}
		}
		updateCovttrm3700();
	}

protected void getUnderwriting3030()
	{
		if (underwritingReqd.isTrue()) {
			calcUndwAge3a00();
		}
		if (underwritingReqd.isTrue()
		&& isGT(sv.sumin,0)) {
			getUndwRule3900();
		}
		if (isGT(sv.sumin,0)
		&& underwritingReqd.isTrue()
		&& isEQ(wsaaT6768Found,"Y")) {
			updateUndc3800();
		}
	}

protected void initcovr3500()
	{
		/*PARA*/
		covttrmIO.setCrtable(covtlnbIO.getCrtable());
		covttrmIO.setPayrseqno(1);
		covttrmIO.setRiskCessAge(0);
		covttrmIO.setSingp(0);
		covttrmIO.setInstprem(0);
		covttrmIO.setZbinstprem(0);
		covttrmIO.setZlinstprem(0);
		covttrmIO.setPremCessAge(0);
		covttrmIO.setRiskCessTerm(0);
		covttrmIO.setPremCessTerm(0);
		covttrmIO.setBenCessAge(0);
		covttrmIO.setBenCessTerm(0);
		covttrmIO.setSumins(0);
		covttrmIO.setRiskCessDate(varcom.vrcmMaxDate);
		covttrmIO.setPremCessDate(varcom.vrcmMaxDate);
		covttrmIO.setBenCessDate(varcom.vrcmMaxDate);
		covttrmIO.setEffdate(varcom.vrcmMaxDate);
		/*EXIT*/
	}

protected void setupcovttrm3550()
	{
		para3550();
	}

protected void para3550()
	{
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate,covttrmIO.getRiskCessDate())) {
			covttrmIO.setRiskCessDate(sv.riskCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate,covttrmIO.getPremCessDate())) {
			covttrmIO.setPremCessDate(sv.premCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge,covttrmIO.getRiskCessAge())) {
			covttrmIO.setRiskCessAge(sv.riskCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge,covttrmIO.getPremCessAge())) {
			covttrmIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm,covttrmIO.getRiskCessTerm())) {
			covttrmIO.setRiskCessTerm(sv.riskCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm,covttrmIO.getPremCessTerm())) {
			covttrmIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin,covttrmIO.getSumins())) {
			covttrmIO.setSumins(sv.sumin);
			wsaaUpdateFlag.set("Y");
		}
		if (isEQ(payrIO.getBillfreq(),"00")) {
			if (isNE(sv.instPrem,covttrmIO.getSingp())) {
				covttrmIO.setSingp(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(sv.instPrem,covttrmIO.getInstprem())) {
				covttrmIO.setInstprem(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.zbinstprem,covttrmIO.getZbinstprem())) {
			covttrmIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem,covttrmIO.getZlinstprem())) {
			covttrmIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covttrmIO.setLoadper(sv.loadper);
			covttrmIO.setRateadj(sv.rateadj);
			covttrmIO.setFltmort(sv.fltmort);
			covttrmIO.setPremadj(sv.premadj);
			covttrmIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.mortcls,covttrmIO.getMortcls())) {
			covttrmIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd,covttrmIO.getLiencd())) {
			covttrmIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		if ((isEQ(sv.select,"J")
		&& (isEQ(covttrmIO.getJlife(),"00")
		|| isEQ(covttrmIO.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select,"L"))
		&& isEQ(covttrmIO.getJlife(),"01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select,"J")) {
				covttrmIO.setJlife("01");
			}
			else {
				covttrmIO.setJlife("00");
			}
		}
		if (isNE(sv.polinc,covttrmIO.getPolinc())) {
			covttrmIO.setPolinc(sv.polinc);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.numapp,covttrmIO.getNumapp())) {
			covttrmIO.setNumapp(sv.numapp);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillfreq(),covttrmIO.getBillfreq())) {
			covttrmIO.setBillfreq(payrIO.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillchnl(),covttrmIO.getBillchnl())) {
			covttrmIO.setBillchnl(payrIO.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(chdrlnbIO.getOccdate(),covttrmIO.getEffdate())) {
			covttrmIO.setEffdate(chdrlnbIO.getOccdate());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd,covttrmIO.getAnbccd(1))) {
			covttrmIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covttrmIO.getSex(1))) {
			covttrmIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd,covttrmIO.getAnbccd(2))) {
			covttrmIO.setAnbccd(2, wsbbAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covttrmIO.getSex(2))) {
			covttrmIO.setSex(2, wsbbSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth,covttrmIO.getBappmeth())) {
			covttrmIO.setBappmeth(sv.bappmeth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zdivopt,covttrmIO.getZdivopt())) {
			covttrmIO.setZdivopt(sv.zdivopt);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.payeesel,covttrmIO.getPayclt())) {
			covttrmIO.setPayclt(sv.payeesel);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.paymth,covttrmIO.getPaymth())) {
			covttrmIO.setPaymth(sv.paymth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.paycurr,covttrmIO.getPaycurr())) {
			covttrmIO.setPaycurr(sv.paycurr);
			wsaaUpdateFlag.set("Y");
		}
		covttrmIO.setCntcurr(chdrlnbIO.getCntcurr());
	}

protected void callDatcon33600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateCovttrm3700()
	{
		try {
			para3700();
			para3710();
		}
		catch (GOTOException e){
		}
	}

protected void para3700()
	{
		if (isEQ(wsaaUpdateFlag,"N")) {
			goTo(GotoLabel.exit3790);
		}
	}

protected void para3710()
	{
		covttrmIO.setBenCessAge(ZERO);
		covttrmIO.setBenCessTerm(ZERO);
		covttrmIO.setBenCessDate(varcom.vrcmMaxDate);
		covttrmIO.setTermid(varcom.vrcmTermid);
		covttrmIO.setUser(varcom.vrcmUser);
		covttrmIO.setTransactionDate(varcom.vrcmDate);
		covttrmIO.setTransactionTime(varcom.vrcmTime);
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setZdivopt(sv.zdivopt);
		covttrmIO.setPaycoy(wsspcomn.fsuco);
		covttrmIO.setPayclt(sv.payeesel);
		covttrmIO.setPaymth(sv.paymth);
		covttrmIO.setPaycurr(sv.paycurr);
		if (isEQ(t5690rec.bankreq,"Y")) {
			covttrmIO.setBankkey(covtlnbIO.getBankkey());
			covttrmIO.setBankacckey(covtlnbIO.getBankacckey());
			covttrmIO.setFacthous(covtlnbIO.getFacthous());
		}
		else {
			covttrmIO.setBankkey(SPACES);
			covttrmIO.setBankacckey(SPACES);
			covttrmIO.setFacthous(SPACES);
		}
		covttrmIO.setFormat(formatsInner.covttrmrec);
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
	}

protected void initundc3750()
	{
		/*PARA*/
		/* Before creating a  new Underwriting Component record            */
		/* initialize the fields.                                          */
		if (isEQ(sv.sumin,0)) {
			return ;
		}
		undcIO.setChdrnum(ZERO);
		undcIO.setChdrcoy(ZERO);
		undcIO.setLife(ZERO);
		undcIO.setCoverage(ZERO);
		undcIO.setRider(ZERO);
		undcIO.setCurrfrom(ZERO);
		undcIO.setCurrto(varcom.vrcmMaxDate);
		undcIO.setUndwrule(SPACES);
		undcIO.setSpecind(SPACES);
		undcIO.setTranno(1);
		undcIO.setValidflag("0");
		/*EXIT*/
	}

protected void updateUndc3800()
	{
		para3810();
	}

protected void para3810()
	{
		undcIO.setChdrcoy(covttrmIO.getChdrcoy());
		undcIO.setChdrnum(covttrmIO.getChdrnum());
		undcIO.setLife(covttrmIO.getLife());
		undcIO.setJlife(covttrmIO.getJlife());
		undcIO.setCoverage(covttrmIO.getCoverage());
		undcIO.setRider(covttrmIO.getRider());
		undcIO.setCurrfrom(covttrmIO.getEffdate());
		undcIO.setCurrto(varcom.vrcmMaxDate);
		undcIO.setUndwrule(wsaaUndwrule);
		undcIO.setSpecind(SPACES);
		undcIO.setTranno(1);
		undcIO.setValidflag("3");
		undcIO.setFormat(formatsInner.undcrec);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(undcIO.getParams());
			syserrrec.statuz.set(undcIO.getStatuz());
			fatalError600();
		}
	}

protected void getUndwRule3900()
	{
			start3910();
		}

	/**
	* <pre>
	* For the given Sum Assured and Age, read the Underwriting        
	* based on Age & SA table T6768.                                  
	* Read T6768 with a key of CRTABLE, CURRENCY and SEX, for the     
	* given coverage.                                                 
	* If the Proposal is a joint-life case, the table will be read    
	* with just CRTABLE and CURRENCY.                                 
	* T6768 holds a matrix of Underwriting rules held in a            
	* two-dimensional table with  Age as the vertical ('y') axis and  
	* SA as the horizontal ('x') axis.                                
	* Locate  the occurrence of Age that is greater than or equal to  
	* the  Age  Next  Birthday.                                       
	* Locate the occurrence of Term that is greater  than  or  equal  
	* to the Term.                                                    
	* Locate the appropriate method.                                  
	* </pre>
	*/
protected void start3910()
	{
		wsaaT6768Crtable.set(covtlnbIO.getCrtable());
		wsaaT6768Curr.set(chdrlnbIO.getCntcurr());
		if (singlif.isTrue()) {
			wsaaT6768Sex.set(wsaaSex);
		}
		else {
			wsaaT6768Sex.set(SPACES);
		}
		/* Read T6768 to determine the underwriting rule based on the age  */
		/* of the life and the sum assured. This is not a straightforward  */
		/* read due to the table having two continuation items.            */
		itdmIO.setDataKey(SPACES);
		wsaaItemtabl.set(tablesInner.t6768);
		wsaaItemitem.set(wsaaT6768Key);
		/* Work out the array's Y index based on AGE                       */
		wsaaYa.set(ZERO);
		wsaaZa.set(ZERO);
		wsaaYaFlag.set("N");
		while ( !(yaFound.isTrue()
		|| t6768NotFound.isTrue())) {
			determineYa3e00();
		}

		/* If the key does not exist on T6768 there will be no             */
		/* Age/Sum Assured underwriting rule.                              */
		if (t6768NotFound.isTrue()) {
			wsaaUndwrule.set(SPACES);
			return ;
		}
		/* Work out the array's X index based on SUMINS (WSAA-XA).         */
		wsaaXa.set(ZERO);
		wsaaXaFlag.set("N");
		while ( !(xaFound.isTrue())) {
			determineXa3f00();
		}

		/* Now we have the correct position(WSAA-XA, WSAA-YA) of the RULE. */
		/* All we have to do now is to work out the corresponding position */
		/* in the copybook by using these co-ordinates.(The copybook is    */
		/* defined as one-dimensional.)                                    */
		wsaaYa.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaYa,5));
		wsaaIndex.add(wsaaXa);
		wsaaUndwrule.set(t6768rec.undwrule[wsaaIndex.toInt()]);
	}

protected void calcUndwAge3a00()
	{
			start3a10();
		}

protected void start3a10()
	{
		/* If the coverage/rider                                           */
		/* is joint-life case, determine the adjusted                      */
		/* combined age, using T5585.                                      */
		/* If coverage/rider is applicable to Life                         */
		if (isEQ(sv.select,"L")
		|| isEQ(sv.select, " ")) {
			wsaaUndwAge.set(wsaaAnbAtCcd);
			return ;
		}
		/* If coverage/rider is applicable to Joint Life                   */
		if (isEQ(sv.select,"J")) {
			wsaaUndwAge.set(wsbbAnbAtCcd);
			return ;
		}
		/* Must be Joint-life so get combined age                          */
		/* Read T5585 to get Joint Life Age parameters                     */
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5585);
		itdmIO.setItemitem(covttrmIO.getCrtable());
		itdmIO.setItmfrm(covttrmIO.getEffdate());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(covtlnbIO.getCrtable(),itdmIO.getItemitem())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5585)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getCrtable());
			syserrrec.statuz.set(errorsInner.f261);
			fatalError600();
		}
		t5585rec.t5585Rec.set(itdmIO.getGenarea());
		wsaaSub.set(0);
		/* Adjust the lives                                                */
		wsaaSub00.set(ZERO);
		wsaaSub01.set(ZERO);
		wsaaAge00Adjusted.set(wsaaAnbAtCcd);
		wsaaAge01Adjusted.set(wsbbAnbAtCcd);
		if (isEQ(wsaaSex,t5585rec.sexageadj)) {
			adjustAge003b00();
		}
		if (isEQ(wsbbSex,t5585rec.sexageadj)) {
			adjustAge013c00();
		}
		compute(wsaaAgeDifference, 0).set(sub(wsaaAge00Adjusted,wsaaAge01Adjusted));
		wsaaSub.set(0);
		ageDifferance3d00();
		wsaaUndwAge.set(wsaaAdjustedAge);
	}

protected void adjustAge003b00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust3b10: 
					adjust3b10();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust3b10()
	{
		wsaaSub00.add(1);
		if (isGT(wsaaSub00,9)) {
			syserrrec.statuz.set(errorsInner.h043);
			return ;
		}
		if (isLTE(wsaaAnbAtCcd,t5585rec.agelimit[wsaaSub00.toInt()])) {
			compute(wsaaAge00Adjusted, 0).set(add(wsaaAnbAtCcd,t5585rec.ageadj[wsaaSub00.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust3b10);
		}
		/*B90-EXIT*/
	}

protected void adjustAge013c00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust3c10: 
					adjust3c10();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust3c10()
	{
		wsaaSub01.add(1);
		if (isGT(wsaaSub01,9)) {
			syserrrec.statuz.set(errorsInner.h043);
			return ;
		}
		if (isLTE(wsbbAnbAtCcd,t5585rec.agelimit[wsaaSub01.toInt()])) {
			compute(wsaaAge01Adjusted, 0).set(add(wsbbAnbAtCcd,t5585rec.ageadj[wsaaSub01.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust3c10);
		}
		/*C90-EXIT*/
	}

protected void ageDifferance3d00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case go3d10: 
					go3d10();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go3d10()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,18)) {
			premiumrec.statuz.set(errorsInner.f262);
			return ;
		}
		if (isGT(wsaaAgeDifference,t5585rec.agedif[wsaaSub.toInt()])) {
			goTo(GotoLabel.go3d10);
		}
		if (isEQ(t5585rec.hghlowage,"H")) {
			if (isGT(wsaaAge00Adjusted,wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaAge01Adjusted));
			}
		}
		if (isEQ(t5585rec.hghlowage,"L")) {
			if (isLTE(wsaaAge00Adjusted,wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaAge01Adjusted));
			}
		}
	}

protected void determineYa3e00()
	{
			init3e10();
		}

protected void init3e10()
	{
		x100ReadItdm();
		if (t6768NotFound.isTrue()) {
			return ;
		}
		for (wsaaZa.set(1); !(isGT(wsaaZa,10)); wsaaZa.add(1)){
			if (isLTE(wsaaUndwAge,t6768rec.undage[wsaaZa.toInt()])) {
				wsaaYa.set(wsaaZa);
				wsaaZa.set(11);
				yaFound.setTrue();
			}
		}
		if (yaFound.isTrue()) {
			return ;
		}
		/* If the age does not fall into the range specified and an age    */
		/* continuation item exists, read T6768 again using the            */
		/* continuation item as the key.                                   */
		if (isGT(wsaaZa,10)) {
			if (isNE(t6768rec.agecont,SPACES)) {
				wsaaItemitem.set(t6768rec.agecont);
			}
			else {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(errorsInner.e390);
				fatalError600();
			}
		}
	}

protected void determineXa3f00()
	{
			init3f10();
		}

protected void init3f10()
	{
		for (wsaaZa.set(1); !(isGT(wsaaZa,5)); wsaaZa.add(1)){
			if (isLTE(sv.sumin,t6768rec.undsa[wsaaZa.toInt()])) {
				wsaaXa.set(wsaaZa);
				wsaaZa.set(6);
				xaFound.setTrue();
			}
		}
		if (xaFound.isTrue()) {
			return ;
		}
		/* If the sum assured does not fall into the range specified and a */
		/* sum assured continuation item exists, read T6768 again using    */
		/* continuation item as the key.                                   */
		if (isGT(wsaaZa,5)) {
			if (isNE(t6768rec.sacont,SPACES)) {
				wsaaItemitem.set(t6768rec.sacont);
				x100ReadItdm();
				t6768rec.t6768Rec.set(itdmIO.getGenarea());
			}
			else {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(errorsInner.e389);
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
			para4000();
		}

protected void para4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		if (forcedKill.isTrue()) {
			wsaaKillFlag.set("N");
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(sv.bankaccreq,"X")) {
			bankaccExe5400();
		}
		else {
			if (isEQ(sv.bankaccreq,"?")) {
				bankaccRet5500();
			}
			else {
				if (isEQ(sv.pbind,"X")) {
					pbindExe5100();
				}
				else {
					if (isEQ(sv.pbind,"?")) {
						pbindRet5200();
					}
					else {
						if (isEQ(sv.optextind,"X")) {
							optionsExe4500();
						}
						else {
							if (isEQ(sv.optextind,"?")) {
								optionsRet4600();
							}
							else {
								if (isEQ(sv.ratypind,"X")) {
									reassuranceExe4200();
								}
								else {
									if (isEQ(sv.ratypind,"?")) {
										reassuranceRet4300();
									}
									else {
										if (isEQ(sv.taxind, "X")) {
											taxExe5600();
										}
										else {
											if (isEQ(sv.taxind, "?")) {
												taxRet5700();
											}
											else {
												if (isEQ(sv.exclind, "X")) {
													exclExe6000();
												}
												else {
													if (isEQ(sv.exclind, "?")) {
														exclRet6100();
													}
													else
													{
														if (isEQ(sv.aepaydet, "X") && isEndMat) {
															antcpEndwPayDet();
															chdrenq();
														}else
														{
															if (isEQ(sv.aepaydet, "?") && isEndMat){
																antcpEndwPayDet1();
																chdrenq();
															}else
															{
																if (isNE(aepaydet, "+") && isEndMat) {
																	antcpEndwPayDet();
																	chdrenq();
																	}
																
											          else {
												            if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], " ")) {
											              wayout4700();
										                 }
										                         else {
											                       wayin4800();
										                         }
											          }
															}
														
														
														}	
												}											
													
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
		}
	}


protected void antcpEndwPayDet(){
	/*    If the reassurance details have been selected,(value - 'X'), */
	/*    then set an asterisk in the program stack action field to    */
	/*    ensure that control returns here, set the parameters for     */
	/*    generalised secondary switching and save the original        */
	/*    programs from the program stack.                             */
	/*    Set up WSSP-CURRFORM for next programs.              <R96REA>*/
	wsspcomn.currfrom.set(chdrlnbIO.getOccdate());
	sv.aepaydet.set("?");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		save4510();
	}
	gensswrec.function.set("H");
	gensww4210();
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		load4530();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	
}
protected void antcpEndwPayDet1()
{
	/*START*/
	/* On return from this  request, the current stack "action" will   */
	/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
	/* handle the return from options and extras:                      */
	/* Note that to have selected this option in the first place then  */
	/* details must exist.......set the flag for re-selection.         */
	sv.aepaydet.set("+");
	aepaydet ="+";
	/*  - restore the saved programs to the program stack              */
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar22 = 0; !(loopVar22 == 8); loopVar22 += 1){
		restore4610();
	}
	/*  - blank  out  the  stack  "action"                             */
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	/*       Set  WSSP-NEXTPROG to  the current screen                 */
	/*       name (thus returning to re-display the screen).           */
	wsspcomn.nextprog.set(scrnparams.scrname);
	/*EXIT*/
}


protected void chdrenq()
{
	chdrenqIO.setChdrcoy(wsspcomn.company);
	chdrenqIO.setChdrnum(chdrlnbIO.getChdrnum());
	chdrenqIO.setFunction(varcom.readr);
	chdrenqIO.setFormat(chdrenqrec);
	SmartFileCode.execute(appVars, chdrenqIO);
	if (isNE(chdrenqIO.getStatuz(),varcom.oK)
	&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(chdrenqIO.getParams());
		syserrrec.statuz.set(chdrenqIO.getStatuz());
		fatalError600();
	}
	chdrenqIO.setCnttype(chdrlnbIO.getCnttype());
	chdrenqIO.setCntcurr(chdrlnbIO.getCntcurr());
	chdrenqIO.setCownnum(chdrlnbIO.getCownnum());
	chdrenqIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, chdrenqIO);
	if (isNE(chdrenqIO.getStatuz(),varcom.oK)
	&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(chdrenqIO.getParams());
		syserrrec.statuz.set(chdrenqIO.getStatuz());
		fatalError600();
	}	


}
protected void exclExe6000(){
	/* - Keep the CHDR/COVT record */
	//PINNACLE-2855
	chdrlnbIO.setFunction("KEEPS");
	chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
	syserrrec.params.set(chdrlnbIO.getParams());
	syserrrec.statuz.set(chdrlnbIO.getStatuz());
	fatalError600();
	}
	covtlnbIO.setFunction("KEEPS");
	covtlnbIO.setFormat(formatsInner.covtlnbrec);
	SmartFileCode.execute(appVars, covtlnbIO);
	if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(covtlnbIO.getParams());
		syserrrec.statuz.set(covtlnbIO.getStatuz());
		fatalError600();
	}	//IJS-537
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsspcomn.crtable.set(covtlnbIO.getCrtable().toString());
		
		if(isNE(wsspcomn.flag,"I")){
			wsspcomn.flag.set("X");
			wsspcomn.cmode.set("PS");}
			else
				wsspcomn.cmode.set("IFE");
			gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	checkExcl();
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}

protected void reassuranceExe4200()
	{
		para4200();
	}

protected void para4200()
	{
		/*    If the reassurance details have been selected,(value - 'X'),*/
		/*    then set an asterisk in the program stack action field to*/
		/*    ensure that control returns here, set the parameters for*/
		/*    generalised secondary switching and save the original*/
		/*    programs from the program stack.*/
		/*    Set up WSSP-CURRFORM for next programs.*/
		wsspcomn.currfrom.set(chdrlnbIO.getOccdate());
		sv.ratypind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call
	*   the generalised secondary switching module to obtain the
	*   next 8 programs and load them into the program stack.
	* </pre>
	*/
protected void gensww4210()
	{
			para4211();
		}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(),varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void reassuranceRet4300()
	{
		/*PARA*/
		sv.ratypind.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*EXIT*/
	}

protected void optionsExe4500()
	{
		para4500();
	}

protected void para4500()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'A' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.optextind.set("+");
		if (isNE(wsspcomn.flag,"I")) {
			calcPremium2300();
		}
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		checkLext1700();
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4700();
				case cont4710: 
					cont4710();
				case cont4715: 
					cont4715();
				case cont4717: 
					cont4717();
				case cont4720: 
					cont4720();
				case exit4790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4700()
	{
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		/*    IF SCRN-STATUZ              = 'KILL'*/
		/*       MOVE SPACES           TO WSSP-SEC-PROG (WSSP-PROGRAM-PTR)*/
		/*       GO TO 4790-EXIT.*/
		if (isEQ(scrnparams.statuz,varcom.rolu)
		|| isEQ(scrnparams.statuz,varcom.rold)) {
			goTo(GotoLabel.cont4710);
		}
		/*    If 'Enter' has been  pressed and "Credit" is non-zero*/
		/*    set the current select  action  field to '*', add 1*/
		/*    to the program pointer and exit.*/
		if (isNE(wsaaCredit,0)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		/*    If 'Enter' has been  pressed and "Credit" is zero add*/
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4790);
	}

protected void cont4710()
	{
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has been changed set the current*/
		/*    select action field to '*',  add 1 to the program*/
		/*    pointer and exit.*/
		if (isNE(sv.numapp,covttrmIO.getNumapp())) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4790);
		}
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has not been changed then loop*/
		/*    round within the program and display the next /*/
		/*    previous screen as requested  (including BEGN).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covttrmIO.getSeqnbr());
		if (isEQ(sv.numapp,0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz,varcom.rolu)) {
				covttrmIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4715);
			}
			else {
				covttrmIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covttrmIO);
				if (isNE(covttrmIO.getStatuz(),varcom.oK)
				&& isNE(covttrmIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(covttrmIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			covttrmIO.setFunction(varcom.nextr);
		}
		else {
			covttrmIO.setFunction(varcom.nextp);
		}
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(),varcom.oK)
		&& isNE(covttrmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(),covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(),covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
	}

protected void cont4715()
	{
		if (isEQ(covttrmIO.getStatuz(),varcom.endp)) {
			covttrmIO.setNonKey(SPACES);
			covttrmIO.setAnbccd(1, 0);
			covttrmIO.setAnbccd(2, 0);
			covttrmIO.setSingp(0);
			covttrmIO.setInstprem(0);
			covttrmIO.setZbinstprem(0);
			covttrmIO.setZlinstprem(0);
			covttrmIO.setNumapp(0);
			covttrmIO.setPremCessAge(0);
			covttrmIO.setPremCessTerm(0);
			covttrmIO.setPolinc(0);
			covttrmIO.setRiskCessAge(0);
			covttrmIO.setRiskCessTerm(0);
			covttrmIO.setSumins(0);
			covttrmIO.setBenCessAge(0);
			covttrmIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
			goTo(GotoLabel.cont4717);
		}
		/* ELSE*/
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covttrmIO.getRiskCessDate());
		sv.premCessDate.set(covttrmIO.getPremCessDate());
		sv.riskCessAge.set(covttrmIO.getRiskCessAge());
		sv.premCessAge.set(covttrmIO.getPremCessAge());
		sv.riskCessTerm.set(covttrmIO.getRiskCessTerm());
		sv.premCessTerm.set(covttrmIO.getPremCessTerm());
		if (isNE(covttrmIO.getSingp(),ZERO)) {
			sv.instPrem.set(covttrmIO.getSingp());
			if (isEQ(covttrmIO.getInstprem(),0)) {
				sv.zbinstprem.set(covttrmIO.getZbinstprem());
				sv.zlinstprem.set(covttrmIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covttrmIO.getZbinstprem());
			sv.zlinstprem.set(covttrmIO.getZlinstprem());
			sv.instPrem.set(covttrmIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covttrmIO.getLoadper());
		sv.rateadj.set(covttrmIO.getRateadj());
		sv.fltmort.set(covttrmIO.fltmort);
		sv.premadj.set(covttrmIO.getPremadj());
		sv.adjustageamt.set(covttrmIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covttrmIO.getSumins());
		sv.mortcls.set(covttrmIO.getMortcls());
		sv.liencd.set(covttrmIO.getLiencd());
		sv.bappmeth.set(covttrmIO.getBappmeth());
	}

protected void cont4717()
	{
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covttrmIO.getFunction(),varcom.nextp)) {
			wsaaNumavail.add(covttrmIO.getNumapp());
			goTo(GotoLabel.cont4720);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail,0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covttrmIO.getSeqnbr(), 0);
			covttrmIO.setSeqnbr(add(1,wsaaSaveSeqnbr));
			covttrmIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4720()
	{
		if (isEQ(covttrmIO.getFunction(),varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr,0)) {
			wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covttrmIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)
		&& isNE(th505rec.sumInsMax,0)) {
			compute(sv.sumin, 1).setRounded((div(mult(th505rec.sumInsMin,sv.numapp),sv.polinc)));
		}
		if (isNE(sv.sumin,covttrmIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			calcPremium2300();
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent,SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr,covttrmIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void wayin4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case rolu4805: 
					rolu4805();
				case cont4810: 
					cont4810();
				case cont4820: 
					cont4820();
				case readCovttrm4830: 
					readCovttrm4830();
				case cont4835: 
					cont4835();
				case cont4837: 
					cont4837();
				case cont4840: 
					cont4840();
				case exit4890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		if (isEQ(scrnparams.statuz,varcom.rolu)
		|| isEQ(scrnparams.statuz,varcom.rold)) {
			goTo(GotoLabel.cont4820);
		}
		if (isEQ(wsaaCredit,0)) {
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4890);
		}
		if (isLTE(sv.numapp,sv.numavail)
		&& isLT(wsaaCredit,ZERO)) {
			goTo(GotoLabel.rolu4805);
		}
		if (isLT(wsaaCredit,0)) {
			goTo(GotoLabel.cont4810);
		}
	}

	/**
	* <pre>
	*    If 'Enter' has been pressed and 'Credit' has a
	*    positive value then force the program to "roll up".
	* </pre>
	*/
protected void rolu4805()
	{
		scrnparams.statuz.set(varcom.rolu);
		goTo(GotoLabel.cont4820);
	}

protected void cont4810()
	{
		/*   If 'Enter' has been pressed and "Credit" has a negative*/
		/*   value, loop round within the program displaying the*/
		/*   details from the first transaction record, with the*/
		/*   'Number Applicable' highlighted, and give a message*/
		/*   indicating that more policies have been defined than*/
		/*   are on the Contract Header.*/
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setSeqnbr(0);
		covttrmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*covttrmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covttrmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");*/

		goTo(GotoLabel.readCovttrm4830);
	}

protected void cont4820()
	{
		/*    If one of the Roll keys has been pressed then loop*/
		/*    round within the program and display the next or*/
		/*    previous screen as requested (including  BEGN,*/
		/*    available count etc., see above).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covttrmIO.getSeqnbr());
		if (isEQ(sv.numapp,0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz,varcom.rolu)) {
				covttrmIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4835);
			}
			else {
				covttrmIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covttrmIO);
				if (isNE(covttrmIO.getStatuz(),varcom.oK)
				&& isNE(covttrmIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(covttrmIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			covttrmIO.setFunction(varcom.nextr);
		}
		else {
			covttrmIO.setFunction(varcom.nextp);
		}
	}

protected void readCovttrm4830()
	{
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(),varcom.oK)
		&& isNE(covttrmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(),covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(),covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
	}

protected void cont4835()
	{
		if (isEQ(covttrmIO.getStatuz(),varcom.endp)) {
			wsaaCtrltime.set("Y");
			covttrmIO.setNonKey(SPACES);
			covttrmIO.setAnbccd(1, 0);
			covttrmIO.setAnbccd(2, 0);
			covttrmIO.setSingp(0);
			covttrmIO.setInstprem(0);
			covttrmIO.setZbinstprem(0);
			covttrmIO.setZlinstprem(0);
			covttrmIO.setNumapp(0);
			covttrmIO.setPremCessAge(0);
			covttrmIO.setPremCessTerm(0);
			covttrmIO.setPolinc(0);
			covttrmIO.setRiskCessAge(0);
			covttrmIO.setRiskCessTerm(0);
			covttrmIO.setSumins(0);
			covttrmIO.setBenCessAge(0);
			covttrmIO.setBenCessTerm(0);
			goTo(GotoLabel.cont4837);
		}
		/* ELSE*/
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covttrmIO.getRiskCessDate());
		sv.premCessDate.set(covttrmIO.getPremCessDate());
		sv.riskCessAge.set(covttrmIO.getRiskCessAge());
		sv.premCessAge.set(covttrmIO.getPremCessAge());
		sv.riskCessTerm.set(covttrmIO.getRiskCessTerm());
		sv.premCessTerm.set(covttrmIO.getPremCessTerm());
		if (isNE(covttrmIO.getSingp(),ZERO)) {
			sv.instPrem.set(covttrmIO.getSingp());
			if (isEQ(covttrmIO.getInstprem(),0)) {
				sv.zbinstprem.set(covttrmIO.getZbinstprem());
				sv.zlinstprem.set(covttrmIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covttrmIO.getZbinstprem());
			sv.zlinstprem.set(covttrmIO.getZlinstprem());
			sv.instPrem.set(covttrmIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covttrmIO.getLoadper());
		sv.rateadj.set(covttrmIO.getRateadj());
		sv.fltmort.set(covttrmIO.fltmort);
		sv.premadj.set(covttrmIO.getPremadj());
		sv.adjustageamt.set(covttrmIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covttrmIO.getSumins());
		sv.mortcls.set(covttrmIO.getMortcls());
		sv.liencd.set(covttrmIO.getLiencd());
		sv.bappmeth.set(covttrmIO.getBappmeth());
	}

protected void cont4837()
	{
		/*    When displaying the first record again,  set the*/
		/*    number available as the number included in the plan*/
		if (isEQ(covttrmIO.getFunction(),varcom.begn)) {
			wsaaNumavail.set(chdrlnbIO.getPolinc());
			wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covttrmIO.getFunction(),varcom.nextp)) {
			wsaaNumavail.add(covttrmIO.getNumapp());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail,0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covttrmIO.getSeqnbr(), 0);
			covttrmIO.setSeqnbr(add(1,wsaaSaveSeqnbr));
			covttrmIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4840()
	{
		if (isEQ(covttrmIO.getFunction(),varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr,0)) {
			wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covttrmIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(th505rec.sumInsMax,th505rec.sumInsMin)
		&& isNE(th505rec.sumInsMax,0)) {
			compute(sv.sumin, 1).setRounded((div(mult(th505rec.sumInsMin,sv.numapp),sv.polinc)));
		}
		if (isNE(sv.sumin,covttrmIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			calcPremium2300();
		}
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent,SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr,covttrmIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void pbindExe5100()
	{
		start5110();
	}

protected void start5110()
	{
		/*  - Keep the POVR/CHDR record for Premium Breakdown enquiry.*/
		/*    Ensure we get the latest one.*/
		readPovr5300();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(),lifelnbIO.getChdrnum())
		|| isNE(povrIO.getLife(),lifelnbIO.getLife())
		|| isNE(povrIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(),covtlnbIO.getRider())) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option*/
		/* on table T1675.*/
		/*  - change the request indicator to '?',*/
		sv.pbind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the*/
		/*    next program.*/
		gensswrec.function.set("C");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet5200()
	{
		start5210();
	}

protected void start5210()
	{
		/* Release the POVR records as no longer required.*/
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/* Note that to have selected this option in the first place then*/
		/* details must exist.......set the flag for re-selection.*/
		sv.pbind.set("+");
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void readPovr5300()
	{
		start5310();
	}

protected void start5310()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covtlnbIO.getChdrcoy());
		povrIO.setChdrnum(lifelnbIO.getChdrnum());
		povrIO.setLife(lifelnbIO.getLife());
		povrIO.setCoverage(covtlnbIO.getCoverage());
		povrIO.setRider(covtlnbIO.getRider());
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)
		&& isNE(povrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void bankaccExe5400()
	{
		bankAccExe5400();
	}

protected void bankAccExe5400()
	{
		sv.bankaccreq.set("?");
		covtlnbIO.setZdivopt(sv.zdivopt);
		covtlnbIO.setPaycoy(wsspcomn.fsuco);
		covtlnbIO.setPayclt(sv.payeesel);
		covtlnbIO.setPaymth(sv.paymth);
		covtlnbIO.setPaycurr(sv.paycurr);
		covtlnbIO.setFunction("KEEPS");
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option*/
		/* on table T1675.*/
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the*/
		/*    next program.*/
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void bankaccRet5500()
	{
		bankAccRet5500();
		programStack5510();
	}

protected void bankAccRet5500()
	{
		covtlnbIO.setFunction("RETRV");
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getBankkey(),SPACES)
		&& isEQ(covtlnbIO.getBankacckey(),SPACES)) {
			sv.bankaccreq.set(SPACES);
		}
		else {
			sv.bankaccreq.set("+");
		}
	}

	/**
	* <pre>
	*  - restore the saved programs to the program stack
	* </pre>
	*/
protected void programStack5510()
	{
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void taxExe5600()
	{
		start5600();
	}

protected void start5600()
	{
		/*  - Keep the CHDR/COVT record                                    */
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		covtlnbIO.setZbinstprem(sv.zbinstprem);
		covtlnbIO.setInstprem(sv.instPrem);
		covtlnbIO.setFunction("KEEPS");
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.taxind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet5700()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.taxind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifcnum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.paycurr);
		chkrlrec.cnttype.set(chdrlnbIO.getCnttype());
		chkrlrec.crtable.set(covtlnbIO.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtlnbIO.getLife());
		chkrlrec.chdrnum.set(chdrlnbIO.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz,varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CheckCalcTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a210Start();
				case a250CallTaxSubr: 
					a250CallTaxSubr();
				case a290Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210Start()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.a290Exit);
		}
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.a250CallTaxSubr);
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		a300ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void a250CallTaxSubr()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covtlnbIO.getLife());
			txcalcrec.coverage.set(covtlnbIO.getCoverage());
			txcalcrec.rider.set(covtlnbIO.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covtlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
			txcalcrec.register.set(chdrlnbIO.getRegister());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCntCurr.set(chdrlnbIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.instPrem);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
			txcalcrec.tranno.set(chdrlnbIO.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(sv.instPrem);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void a300ReadTr52e()
	{
		a310Start();
	}

protected void a310Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void x100ReadItdm()
	{
		x100Init();
	}

	/**
	* <pre>
	************************                                  <V74L01>
	* </pre>
	*/
protected void x100Init()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(wsaaItemtabl);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setParams(syserrrec.params);
			itdmIO.setStatuz(syserrrec.statuz);
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6768)
		|| isNE(itdmIO.getItemitem(),wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaT6768Found.set("N");
		}
		else {
			wsaaT6768Found.set("Y");
			t6768rec.t6768Rec.set(itdmIO.getGenarea());
		}
	}
/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData e027 = new FixedLengthStringData(4).init("E027");
	private FixedLengthStringData e133 = new FixedLengthStringData(4).init("E133");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e389 = new FixedLengthStringData(4).init("E389");
	private FixedLengthStringData e390 = new FixedLengthStringData(4).init("E390");
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f020 = new FixedLengthStringData(4).init("F020");
	private FixedLengthStringData f177 = new FixedLengthStringData(4).init("F177");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f261 = new FixedLengthStringData(4).init("F261");
	private FixedLengthStringData f262 = new FixedLengthStringData(4).init("F262");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData hl52 = new FixedLengthStringData(4).init("HL52");
	private FixedLengthStringData f826 = new FixedLengthStringData(4).init("F826");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g622 = new FixedLengthStringData(4).init("G622");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData h040 = new FixedLengthStringData(4).init("H040");
	private FixedLengthStringData h043 = new FixedLengthStringData(4).init("H043");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h437 = new FixedLengthStringData(4).init("H437");
	private FixedLengthStringData h960 = new FixedLengthStringData(4).init("H960");
	private FixedLengthStringData l001 = new FixedLengthStringData(4).init("L001");
	private FixedLengthStringData z038 = new FixedLengthStringData(4).init("Z038");
	private FixedLengthStringData z042 = new FixedLengthStringData(4).init("Z042");
	private FixedLengthStringData z043 = new FixedLengthStringData(4).init("Z043");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5585 = new FixedLengthStringData(5).init("T5585");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5690 = new FixedLengthStringData(5).init("T5690");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData t6768 = new FixedLengthStringData(5).init("T6768");
	private FixedLengthStringData th500 = new FixedLengthStringData(5).init("TH500");
	private FixedLengthStringData th505 = new FixedLengthStringData(5).init("TH505");
	private FixedLengthStringData tr675 = new FixedLengthStringData(5).init("TR675");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData covttrmrec = new FixedLengthStringData(10).init("COVTTRMREC");
	private FixedLengthStringData clbaddbrec = new FixedLengthStringData(10).init("CLBADDBREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData racdlnbrec = new FixedLengthStringData(10).init("RACDLNBREC");
	private FixedLengthStringData undcrec = new FixedLengthStringData(10).init("UNDCREC");
}
}