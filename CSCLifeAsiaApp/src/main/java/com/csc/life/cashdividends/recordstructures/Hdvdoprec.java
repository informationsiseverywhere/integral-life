package com.csc.life.cashdividends.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:35
 * Description:
 * Copybook name: HDVDOPREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdvdoprec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dividendRec = new FixedLengthStringData(101);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(dividendRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(dividendRec, 5);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(dividendRec, 9);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(dividendRec, 10);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(dividendRec, 18);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(dividendRec, 20);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(dividendRec, 22);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(dividendRec, 24);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(dividendRec, 26);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(dividendRec, 29);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(dividendRec, 30);
  	public ZonedDecimalData batcactyr = new ZonedDecimalData(4, 0).isAPartOf(dividendRec, 32).setUnsigned();
  	public ZonedDecimalData batcactmn = new ZonedDecimalData(2, 0).isAPartOf(dividendRec, 36).setUnsigned();
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(dividendRec, 38);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(dividendRec, 42);
  	public ZonedDecimalData hdivTranno = new ZonedDecimalData(5, 0).isAPartOf(dividendRec, 47).setUnsigned();
  	public ZonedDecimalData newTranno = new ZonedDecimalData(5, 0).isAPartOf(dividendRec, 52).setUnsigned();
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(dividendRec, 57);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(dividendRec, 60);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(dividendRec, 64);
  	public PackedDecimalData effectiveDate = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 67);
  	public PackedDecimalData hdivEffdate = new PackedDecimalData(8, 0).isAPartOf(dividendRec, 72);
  	public FixedLengthStringData divdOption = new FixedLengthStringData(4).isAPartOf(dividendRec, 77);
  	public PackedDecimalData divdAmount = new PackedDecimalData(17, 2).isAPartOf(dividendRec, 81);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(dividendRec, 90);
  	public FixedLengthStringData userName = new FixedLengthStringData(10).isAPartOf(dividendRec, 91);


	public void initialize() {
		COBOLFunctions.initialize(dividendRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		dividendRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}