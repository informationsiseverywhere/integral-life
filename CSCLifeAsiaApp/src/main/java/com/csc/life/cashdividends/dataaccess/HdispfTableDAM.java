package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdispfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:27
 * Class transformed from HDISPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdispfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 159;
	public FixedLengthStringData hdisrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hdispfRecord = hdisrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hdisrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hdisrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hdisrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hdisrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hdisrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hdisrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hdisrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(hdisrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hdisrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(hdisrec);
	public PackedDecimalData firstDivdDate = DD.hdv1stdt.copy().isAPartOf(hdisrec);
	public PackedDecimalData lastDivdDate = DD.hdvldt.copy().isAPartOf(hdisrec);
	public PackedDecimalData lastIntDate = DD.hintldt.copy().isAPartOf(hdisrec);
	public PackedDecimalData nextIntDate = DD.hintndt.copy().isAPartOf(hdisrec);
	public PackedDecimalData lastCapDate = DD.hcapldt.copy().isAPartOf(hdisrec);
	public PackedDecimalData nextCapDate = DD.hcapndt.copy().isAPartOf(hdisrec);
	public PackedDecimalData balAtLastDivd = DD.hdvball.copy().isAPartOf(hdisrec);
	public PackedDecimalData balSinceLastCap = DD.hdvbalc.copy().isAPartOf(hdisrec);
	public PackedDecimalData osInterest = DD.hintos.copy().isAPartOf(hdisrec);
	public PackedDecimalData divdStmtDate = DD.hdvsmtdt.copy().isAPartOf(hdisrec);
	public PackedDecimalData divdStmtNo = DD.hdvsmtno.copy().isAPartOf(hdisrec);
	public PackedDecimalData balAtStmtDate = DD.hdvbalst.copy().isAPartOf(hdisrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(hdisrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(hdisrec);
	public FixedLengthStringData intDueMm = DD.hintduem.copy().isAPartOf(hdisrec);
	public FixedLengthStringData intDueDd = DD.hintdued.copy().isAPartOf(hdisrec);
	public FixedLengthStringData capDueMm = DD.hcapduem.copy().isAPartOf(hdisrec);
	public FixedLengthStringData capDueDd = DD.hcapdued.copy().isAPartOf(hdisrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hdisrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hdisrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hdisrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HdispfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HdispfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HdispfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HdispfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdispfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HdispfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdispfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HDISPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"CNTCURR, " +
							"HDV1STDT, " +
							"HDVLDT, " +
							"HINTLDT, " +
							"HINTNDT, " +
							"HCAPLDT, " +
							"HCAPNDT, " +
							"HDVBALL, " +
							"HDVBALC, " +
							"HINTOS, " +
							"HDVSMTDT, " +
							"HDVSMTNO, " +
							"HDVBALST, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"HINTDUEM, " +
							"HINTDUED, " +
							"HCAPDUEM, " +
							"HCAPDUED, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     tranno,
                                     cntcurr,
                                     firstDivdDate,
                                     lastDivdDate,
                                     lastIntDate,
                                     nextIntDate,
                                     lastCapDate,
                                     nextCapDate,
                                     balAtLastDivd,
                                     balSinceLastCap,
                                     osInterest,
                                     divdStmtDate,
                                     divdStmtNo,
                                     balAtStmtDate,
                                     sacscode,
                                     sacstyp,
                                     intDueMm,
                                     intDueDd,
                                     capDueMm,
                                     capDueDd,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		tranno.clear();
  		cntcurr.clear();
  		firstDivdDate.clear();
  		lastDivdDate.clear();
  		lastIntDate.clear();
  		nextIntDate.clear();
  		lastCapDate.clear();
  		nextCapDate.clear();
  		balAtLastDivd.clear();
  		balSinceLastCap.clear();
  		osInterest.clear();
  		divdStmtDate.clear();
  		divdStmtNo.clear();
  		balAtStmtDate.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		intDueMm.clear();
  		intDueDd.clear();
  		capDueMm.clear();
  		capDueDd.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHdisrec() {
  		return hdisrec;
	}

	public FixedLengthStringData getHdispfRecord() {
  		return hdispfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHdisrec(what);
	}

	public void setHdisrec(Object what) {
  		this.hdisrec.set(what);
	}

	public void setHdispfRecord(Object what) {
  		this.hdispfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hdisrec.getLength());
		result.set(hdisrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}