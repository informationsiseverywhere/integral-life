package com.csc.life.cashdividends.dataaccess.model;

import java.math.BigDecimal;

public class Dryh524DTO {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int hintndt;
	private int hcapndt;
	private int hcapldt;
	private int tranno;
	private String life;
	private String coverage;
	private String rider;
	private int plnsfx;
	private BigDecimal hintos;
	private long hcsdUniqueNumber;
	private String zcshdivmth;
	
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getHintndt() {
		return hintndt;
	}
	public void setHintndt(int hintndt) {
		this.hintndt = hintndt;
	}
	public int getHcapndt() {
		return hcapndt;
	}
	public void setHcapndt(int hcapndt) {
		this.hcapndt = hcapndt;
	}
	public int getHcapldt() {
		return hcapldt;
	}
	public void setHcapldt(int hcapldt) {
		this.hcapldt = hcapldt;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public BigDecimal getHintos() {
		return hintos;
	}
	public void setHintos(BigDecimal hintos) {
		this.hintos = hintos;
	}
	public long getHcsdUniqueNumber() {
		return hcsdUniqueNumber;
	}
	public void setHcsdUniqueNumber(long hcsdUniqueNumber) {
		this.hcsdUniqueNumber = hcsdUniqueNumber;
	}
	public String getZcshdivmth() {
		return zcshdivmth;
	}
	public void setZcshdivmth(String zcshdivmth) {
		this.zcshdivmth = zcshdivmth;
	}
}
