/*
 * File: Hdoppmst.java
 * Date: 29 August 2009 22:53:20
 * Author: Quipoz Limited
 *
 * Class transformed from HDOPPMST.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.List;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.recordstructures.Hdvdoprec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*           Cash Dividend Premium Settlement Option
*           ---------------------------------------
*
* This subroutine is called from BH526 via TH500.
*
* It is responsible for withdrawing the cash dividend allocated
* for the current period to pay for the contract's future premium.
*
* The processing is simply to transfer the dividend suspense to a
* premium suspense in contract currency.
*
*  PROCESSING.
*  -----------
*
* 1. Read irrelevant tables T5645, T5688.
* 2. Perform account posting for the transfer of dividend to become
*    premium suspense.  Posting to dividend suspense and premium
*    suspense.
* 3. Update HDIS to denote the transfer and settlement of future
*    premium has been effected.
*
*****************************************************************
*
* </pre>
*/
public class Hdoppmst extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HDOPPMST";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();


	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private String itemrec = "ITEMREC";
	private String hdisrec = "HDISREC";
	private String hdivrec = "HDIVREC";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5688 = "T5688";
	private Getdescrec getdescrec = new Getdescrec();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private Hdvdoprec hdvdoprec = new Hdvdoprec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Varcom varcom = new Varcom();
	private HdivpfDAO hdivpfDao = getApplicationContext().getBean("hdivpfDAO",HdivpfDAO.class);
	private List<Hdivpf> hdivpfList;
	private Hdivpf hdivpf = new Hdivpf();

	private static final String feaConfigDivNetPrem = "CSMIN006";
	private boolean divpremFlag = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit870,
		exit970
	}

	public Hdoppmst() {
		super();
	}

public void mainline(Object... parmArray)
	{
		hdvdoprec.dividendRec = convertAndSetParam(hdvdoprec.dividendRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		postSettlement300();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
		
	}

protected void initialise210()
	{

		divpremFlag = FeaConfg.isFeatureExist("2", feaConfigDivNetPrem, appVars, "IT");
		syserrrec.subrname.set(wsaaSubr);
		hdvdoprec.statuz.set(varcom.oK);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.batccoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setItemitem(wsaaSubr);
			syserrrec.params.set(itemIO.getParams());
			databaseError800();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		itdmIO.setItemcoy(hdvdoprec.batccoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(hdvdoprec.cnttype);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),hdvdoprec.batccoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),hdvdoprec.cnttype)) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemtabl(t5688);
			itdmIO.setItemitem(hdvdoprec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			databaseError800();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void postSettlement300()
	{
		posting310();
	}

protected void posting310()
	{
		wsaaJrnseq.set(ZERO);
		lifacmvrec.batccoy.set(hdvdoprec.batccoy);
		lifacmvrec.batcbrn.set(hdvdoprec.batcbrn);
		lifacmvrec.batcactyr.set(hdvdoprec.batcactyr);
		lifacmvrec.batcactmn.set(hdvdoprec.batcactmn);
		lifacmvrec.batctrcde.set(hdvdoprec.batctrcde);
		lifacmvrec.batcbatch.set(hdvdoprec.batcbatch);
		lifacmvrec.rldgcoy.set(hdvdoprec.batccoy);
		lifacmvrec.genlcoy.set(hdvdoprec.batccoy);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.rdocnum.set(hdvdoprec.chdrChdrnum);
		lifacmvrec.tranref.set(hdvdoprec.chdrChdrnum);
		lifacmvrec.tranno.set(hdvdoprec.newTranno);
		lifacmvrec.effdate.set(hdvdoprec.effectiveDate);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(hdvdoprec.effectiveDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.substituteCode[1].set(hdvdoprec.cnttype);
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.substituteCode[6].set(hdvdoprec.crtable);
		}
		else {
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hdvdoprec.chdrChdrcoy);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(hdvdoprec.batctrcde);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(hdvdoprec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
		lifacmvrec.origamt.set(hdvdoprec.divdAmount);
		lifacmvrec.origcurr.set(hdvdoprec.cntcurr);
		wsaaRldgacct.set(SPACES);
		wsaaRldgacct.set(hdvdoprec.chdrChdrnum);
		wsaaSub1.set(1);				
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError900();
			
		}
		
		
		if(!divpremFlag)
		{
			wsaaSub2.set(3);			
		}
		else
		{
			wsaaSub2.set(4);			
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub2.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub2.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub2.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub2.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub2.toInt()]);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError900();
		}
		hdisIO.setChdrcoy(hdvdoprec.chdrChdrcoy);
		hdisIO.setChdrnum(hdvdoprec.chdrChdrnum);
		hdisIO.setLife(hdvdoprec.lifeLife);
		hdisIO.setCoverage(hdvdoprec.covrCoverage);
		hdisIO.setRider(hdvdoprec.covrRider);
		hdisIO.setPlanSuffix(hdvdoprec.plnsfx);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		hdisIO.setValidflag("1");
		hdisIO.setTranno(hdvdoprec.newTranno);
		if(!divpremFlag)  //ICIL-1336
		{
			hdisIO.setSacscode(t5645rec.sacscode03);
			hdisIO.setSacstyp(t5645rec.sacstype03);
		}
		else
		{
			hdisIO.setSacscode(t5645rec.sacscode04);
			hdisIO.setSacstyp(t5645rec.sacstype04);
			hdisIO.setBalAtLastDivd(ZERO);
			hdisIO.setBalSinceLastCap(ZERO);
		}
		
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			databaseError800();
		}
		

/*ICIL-1336*/
		
/* In B5349 (Collection-L2POLRNWL), If installment premium is greater than LP S amount(difference will be stored as shortfall),
 then it will check the amount in LP DS. If LP DS > shortfall, 
 LP DS amount will be transferred to LP S and same amount will be updated in hdiv (Hdvamt). 
 However, in POS019 (ICIL-1044), dividend is transferred directly to LP S and Hdvamt will not get updated in B5349.
 So here, Hdvamt is explicitly set to zero so that no interest is calculated on this amount when next time L2DIVALOC is executed. 
*/
				
		if(divpremFlag) 
		{
			hdivpf.setChdrcoy(hdvdoprec.chdrChdrcoy.toString());
			hdivpf.setChdrnum(hdvdoprec.chdrChdrnum.toString());
			hdivpf.setLife(hdvdoprec.lifeLife.toString());
			hdivpf.setCoverage(hdvdoprec.covrCoverage.toString());
			hdivpf.setRider(hdvdoprec.covrRider.toString());
			hdivpf.setPlnsfx(hdvdoprec.plnsfx.toInt());
			hdivpfList = hdivpfDao.searchHdivcshRecord(hdivpf);
			if(hdivpfList != null && !hdivpfList.isEmpty() ){
					hdivpf = hdivpfList.get(0);
					hdivpf.setTranno(hdvdoprec.newTranno.toInt());
					hdivpf.setHdvamt(BigDecimal.ZERO);
					hdivpfDao.updateHdivRecord(hdivpf);
			}
	
		}
	
	}

protected void databaseError800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start810();
				}
				case exit870: {
					exit870();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit870);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start910();
				}
				case exit970: {
					exit970();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit970);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		hdvdoprec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
