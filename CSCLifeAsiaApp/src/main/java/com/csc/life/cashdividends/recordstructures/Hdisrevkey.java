package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:30
 * Description:
 * Copybook name: HDISREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdisrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdisrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdisrevKey = new FixedLengthStringData(64).isAPartOf(hdisrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdisrevChdrcoy = new FixedLengthStringData(1).isAPartOf(hdisrevKey, 0);
  	public FixedLengthStringData hdisrevChdrnum = new FixedLengthStringData(8).isAPartOf(hdisrevKey, 1);
  	public PackedDecimalData hdisrevTranno = new PackedDecimalData(5, 0).isAPartOf(hdisrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(hdisrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdisrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdisrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}