/******************************************************************************
 * File Name 		: HcsdpfDAOImpl.java
 * Author			: smalchi2
 * Creation Date	: 02 January 2017
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for HCSDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HcsdpfDAOImpl extends BaseDAOImpl<Hcsdpf> implements HcsdpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HcsdpfDAOImpl.class);

	public List<Hcsdpf> searchHcsdpfRecord(Hcsdpf hcsdpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, EFFDATE, ");
		sqlSelect.append("ZDIVOPT, ZCSHDIVMTH, PAYCOY, PAYCLT, PAYMTH, ");
		sqlSelect.append("FACTHOUS, BANKKEY, BANKACCKEY, PAYCURR, USRPRF, ");
		sqlSelect.append("JOBNM, DATIME ");
		sqlSelect.append("FROM HCSDPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHcsdpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHcsdpfRs = null;
		List<Hcsdpf> outputList = new ArrayList<Hcsdpf>();

		try {

			psHcsdpfSelect.setString(1, hcsdpf.getChdrcoy());
			psHcsdpfSelect.setString(2, hcsdpf.getChdrnum());
			psHcsdpfSelect.setString(3, hcsdpf.getLife());
			psHcsdpfSelect.setString(4, hcsdpf.getCoverage());
			psHcsdpfSelect.setString(5, hcsdpf.getRider());
			psHcsdpfSelect.setInt(6, hcsdpf.getPlnsfx());

			sqlHcsdpfRs = executeQuery(psHcsdpfSelect);

			while (sqlHcsdpfRs.next()) {

				Hcsdpf hcsdpfnew = new Hcsdpf();

				hcsdpfnew.setUniqueNumber(sqlHcsdpfRs.getInt("UNIQUE_NUMBER"));
				hcsdpfnew.setChdrcoy(sqlHcsdpfRs.getString("CHDRCOY"));
				hcsdpfnew.setChdrnum(sqlHcsdpfRs.getString("CHDRNUM"));
				hcsdpfnew.setLife(sqlHcsdpfRs.getString("LIFE"));
				hcsdpfnew.setJlife(sqlHcsdpfRs.getString("JLIFE"));
				hcsdpfnew.setCoverage(sqlHcsdpfRs.getString("COVERAGE"));
				hcsdpfnew.setRider(sqlHcsdpfRs.getString("RIDER"));
				hcsdpfnew.setPlnsfx(sqlHcsdpfRs.getInt("PLNSFX"));
				hcsdpfnew.setValidflag(sqlHcsdpfRs.getString("VALIDFLAG"));
				hcsdpfnew.setTranno(sqlHcsdpfRs.getInt("TRANNO"));
				hcsdpfnew.setEffdate(sqlHcsdpfRs.getInt("EFFDATE"));
				hcsdpfnew.setZdivopt(sqlHcsdpfRs.getString("ZDIVOPT"));
				hcsdpfnew.setZcshdivmth(sqlHcsdpfRs.getString("ZCSHDIVMTH"));
				hcsdpfnew.setPaycoy(sqlHcsdpfRs.getString("PAYCOY"));
				hcsdpfnew.setPayclt(sqlHcsdpfRs.getString("PAYCLT"));
				hcsdpfnew.setPaymth(sqlHcsdpfRs.getString("PAYMTH"));
				hcsdpfnew.setFacthous(sqlHcsdpfRs.getString("FACTHOUS"));
				hcsdpfnew.setBankkey(sqlHcsdpfRs.getString("BANKKEY"));
				hcsdpfnew.setBankacckey(sqlHcsdpfRs.getString("BANKACCKEY"));
				hcsdpfnew.setPaycurr(sqlHcsdpfRs.getString("PAYCURR"));
				hcsdpfnew.setUsrprf(sqlHcsdpfRs.getString("USRPRF"));
				hcsdpfnew.setJobnm(sqlHcsdpfRs.getString("JOBNM"));
				hcsdpfnew.setDatime(sqlHcsdpfRs.getDate("DATIME"));

				outputList.add(hcsdpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHcsdpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHcsdpfSelect, sqlHcsdpfRs);
		}

		return outputList;
	}
	
    public Map<String, Hcsdpf> searchHcsdRecord(String coy, List<String> chdrnumList) {
        StringBuilder sqlHcsdSelect1 = new StringBuilder(
                "SELECT * FROM HCSDPF WHERE CHDRCOY=? AND  LIFE='01' AND COVERAGE='01' AND RIDER='00' AND PLNSFX=0 AND ");
        sqlHcsdSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlHcsdSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
        PreparedStatement psHcsdSelect = getPrepareStatement(sqlHcsdSelect1.toString());
        ResultSet rs = null;
        Map<String, Hcsdpf> hcsdpfMap = new HashMap<String, Hcsdpf>();
        try {
            psHcsdSelect.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(psHcsdSelect);

            while (rs.next()) {
                Hcsdpf hcsdpf = new Hcsdpf();

                hcsdpf.setChdrcoy(rs.getString("CHDRCOY"));
                hcsdpf.setChdrnum(rs.getString("CHDRNUM"));
                hcsdpf.setLife(rs.getString("LIFE"));
                hcsdpf.setJlife(rs.getString("JLIFE"));
                hcsdpf.setCoverage(rs.getString("COVERAGE"));
                hcsdpf.setRider(rs.getString("RIDER"));
                hcsdpf.setPlnsfx(rs.getInt("PLNSFX"));
                hcsdpf.setValidflag(rs.getString("VALIDFLAG"));
                hcsdpf.setTranno(rs.getInt("TRANNO"));
                hcsdpf.setEffdate(rs.getInt("EFFDATE"));
                hcsdpf.setZdivopt(rs.getString("ZDIVOPT"));
                hcsdpf.setZcshdivmth(rs.getString("ZCSHDIVMTH"));
                hcsdpf.setPaycoy(rs.getString("PAYCOY"));
                hcsdpf.setPayclt(rs.getString("PAYCLT"));
                hcsdpf.setPaymth(rs.getString("PAYMTH"));
                hcsdpf.setFacthous(rs.getString("FACTHOUS"));
                hcsdpf.setBankkey(rs.getString("BANKKEY"));
                hcsdpf.setBankacckey(rs.getString("BANKACCKEY"));
                hcsdpf.setPaycurr(rs.getString("PAYCURR"));

                String chdrnum = hcsdpf.getChdrnum();
                if (hcsdpfMap.containsKey(chdrnum)) {
                    continue;
                } else {
                    hcsdpfMap.put(chdrnum, hcsdpf);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("searchHcsdRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psHcsdSelect, rs);
        }
        return hcsdpfMap;
    }    	

    public Map<String, List<Hcsdpf>> getHcsdMap(String coy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, EFFDATE, ZDIVOPT, ZCSHDIVMTH, PAYCOY, PAYCLT, PAYMTH, FACTHOUS, BANKKEY, BANKACCKEY, PAYCURR, USRPRF, JOBNM, DATIME  ");
        sb.append("FROM VM1DTA.HCSDPF WHERE VALIDFLAG='1' AND CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");            
        LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Hcsdpf>> hcsdMap = new HashMap<String, List<Hcsdpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
            	Hcsdpf hcsdpf = new Hcsdpf();
                hcsdpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                hcsdpf.setChdrcoy(rs.getString("CHDRCOY"));
                hcsdpf.setChdrnum(rs.getString("CHDRNUM"));
                hcsdpf.setLife(rs.getString("LIFE"));                
                hcsdpf.setJlife(rs.getString("JLIFE"));                
                hcsdpf.setCoverage(rs.getString("COVERAGE"));
                hcsdpf.setRider(rs.getString("RIDER"));        
                hcsdpf.setPlnsfx(rs.getInt("PLNSFX"));
                hcsdpf.setValidflag(rs.getString("VALIDFLAG"));
                hcsdpf.setTranno(rs.getInt("TRANNO"));
                hcsdpf.setEffdate(rs.getInt("EFFDATE"));
                hcsdpf.setZdivopt(rs.getString("ZDIVOPT"));
                hcsdpf.setZcshdivmth(rs.getString("ZCSHDIVMTH"));
                hcsdpf.setPaycoy(rs.getString("PAYCOY"));
                hcsdpf.setPayclt(rs.getString("PAYCLT"));
                hcsdpf.setPaymth(rs.getString("PAYMTH"));
                hcsdpf.setFacthous(rs.getString("FACTHOUS"));                
                hcsdpf.setBankkey(rs.getString("BANKKEY")); 
                hcsdpf.setBankacckey(rs.getString("BANKACCKEY")); 
				hcsdpf.setPaycurr(rs.getString("PAYCURR")); 
				hcsdpf.setUsrprf(rs.getString("USRPRF"));
				hcsdpf.setJobnm(rs.getString("JOBNM"));                
				hcsdpf.setDatime(rs.getDate("DATIME"));                 

                if (hcsdMap.containsKey(hcsdpf.getChdrnum())) {
                	hcsdMap.get(hcsdpf.getChdrnum()).add(hcsdpf);
                } else {
                    List<Hcsdpf> hcsdList = new ArrayList<Hcsdpf>();
                    hcsdList.add(hcsdpf);
                    hcsdMap.put(hcsdpf.getChdrnum(), hcsdList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("getHcsdMap()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return hcsdMap;

    }    
    
    public Hcsdpf getHcsdRecordByCoyAndNum(String chdrcoy, String chdrnum){
    	StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, EFFDATE, ZDIVOPT,");
    	sb.append(" ZCSHDIVMTH, PAYCOY, PAYCLT, PAYMTH, FACTHOUS, BANKKEY, BANKACCKEY, PAYCURR, USRPRF, JOBNM FROM HCSDPF WHERE CHDRCOY=? AND CHDRNUM=? ");
    	sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
    	LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Hcsdpf hcsdpf = null;
        try{
         ps.setString(1, chdrcoy);	
         ps.setString(2, chdrnum);
         rs = executeQuery(ps);
         if(rs.next()){
        	 hcsdpf = new Hcsdpf();
        	 hcsdpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
        	 hcsdpf.setChdrcoy(rs.getString("CHDRCOY"));
        	 hcsdpf.setChdrnum(rs.getString("CHDRNUM"));
        	 hcsdpf.setLife(rs.getString("LIFE"));
        	 hcsdpf.setJlife(rs.getString("JLIFE"));
        	 hcsdpf.setCoverage(rs.getString("COVERAGE"));
        	 hcsdpf.setRider(rs.getString("RIDER"));
        	 hcsdpf.setPlnsfx(rs.getInt("PLNSFX"));
        	 hcsdpf.setValidflag(rs.getString("VALIDFLAG"));
        	 hcsdpf.setTranno(rs.getInt("TRANNO"));
        	 hcsdpf.setEffdate(rs.getInt("EFFDATE"));
        	 hcsdpf.setZdivopt(rs.getString("ZDIVOPT"));
        	 hcsdpf.setZcshdivmth(rs.getString("ZCSHDIVMTH"));
        	 hcsdpf.setPaycoy(rs.getString("PAYCOY"));
        	 hcsdpf.setPayclt(rs.getString("PAYCLT"));
        	 hcsdpf.setPaymth(rs.getString("PAYMTH"));
        	 hcsdpf.setFacthous(rs.getString("FACTHOUS"));
        	 hcsdpf.setBankkey(rs.getString("BANKKEY"));
        	 hcsdpf.setBankacckey(rs.getString("BANKACCKEY"));
        	 hcsdpf.setPaycurr(rs.getString("PAYCURR"));
        	 hcsdpf.setUsrprf(rs.getString("USRPRF"));
        	 hcsdpf.setJobnm(rs.getString("JOBNM"));
         }
         
        }catch (SQLException e) {
            LOGGER.error("getHcsdRecordByCoyAndNum()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
    	return hcsdpf;
    } 
    public List<Hcsdpf> searchHcsdRecord(Hcsdpf Hcsd) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, EFFDATE, ");
		sqlSelect.append("ZDIVOPT, ZCSHDIVMTH, PAYCOY, PAYCLT, PAYMTH, ");
		sqlSelect.append("FACTHOUS, BANKKEY, BANKACCKEY, PAYCURR, USRPRF, ");
		sqlSelect.append("JOBNM, DATIME ");
		sqlSelect.append("FROM HCSD WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND  VALIDFLAG='1'");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHcsdSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHcsdRs = null;
		List<Hcsdpf> outputList = new ArrayList<Hcsdpf>();

		try {

			psHcsdSelect.setString(1, Hcsd.getChdrcoy());
			psHcsdSelect.setString(2, Hcsd.getChdrnum());
			psHcsdSelect.setString(3, Hcsd.getLife());
			psHcsdSelect.setString(4, Hcsd.getCoverage());
			psHcsdSelect.setString(5, Hcsd.getRider());
			psHcsdSelect.setInt(6, Hcsd.getPlnsfx());

			sqlHcsdRs = executeQuery(psHcsdSelect);

			while (sqlHcsdRs.next()) {

				Hcsdpf Hcsdnew = new Hcsdpf();

				Hcsdnew.setUniqueNumber(sqlHcsdRs.getInt("UNIQUE_NUMBER"));
				Hcsdnew.setChdrcoy(sqlHcsdRs.getString("CHDRCOY"));
				Hcsdnew.setChdrnum(sqlHcsdRs.getString("CHDRNUM"));
				Hcsdnew.setLife(sqlHcsdRs.getString("LIFE"));
				Hcsdnew.setJlife(sqlHcsdRs.getString("JLIFE"));
				Hcsdnew.setCoverage(sqlHcsdRs.getString("COVERAGE"));
				Hcsdnew.setRider(sqlHcsdRs.getString("RIDER"));
				Hcsdnew.setPlnsfx(sqlHcsdRs.getInt("PLNSFX"));
				Hcsdnew.setValidflag(sqlHcsdRs.getString("VALIDFLAG"));
				Hcsdnew.setTranno(sqlHcsdRs.getInt("TRANNO"));
				Hcsdnew.setEffdate(sqlHcsdRs.getInt("EFFDATE"));
				Hcsdnew.setZdivopt(sqlHcsdRs.getString("ZDIVOPT"));
				Hcsdnew.setZcshdivmth(sqlHcsdRs.getString("ZCSHDIVMTH"));
				Hcsdnew.setPaycoy(sqlHcsdRs.getString("PAYCOY"));
				Hcsdnew.setPayclt(sqlHcsdRs.getString("PAYCLT"));
				Hcsdnew.setPaymth(sqlHcsdRs.getString("PAYMTH"));
				Hcsdnew.setFacthous(sqlHcsdRs.getString("FACTHOUS"));
				Hcsdnew.setBankkey(sqlHcsdRs.getString("BANKKEY"));
				Hcsdnew.setBankacckey(sqlHcsdRs.getString("BANKACCKEY"));
				Hcsdnew.setPaycurr(sqlHcsdRs.getString("PAYCURR"));
				Hcsdnew.setUsrprf(sqlHcsdRs.getString("USRPRF"));
				Hcsdnew.setJobnm(sqlHcsdRs.getString("JOBNM"));
				Hcsdnew.setDatime(sqlHcsdRs.getDate("DATIME"));

				outputList.add(Hcsdnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHcsdRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHcsdSelect, sqlHcsdRs);
		}

		return outputList;
	}
}
