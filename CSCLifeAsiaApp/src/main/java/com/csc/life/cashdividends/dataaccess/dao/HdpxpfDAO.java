/******************************************************************************
 * File Name 		: HdpxpfDAO.java
 * Author			: sbatra9
 * Creation Date	: 22 June 2020
 * Project			: Integral Life
 * Description		: The DAO Interface for HDPXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao;

import java.util.List;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdpxpf;

public interface HdpxpfDAO extends BaseDAO<Hdpxpf> {
	public List<Hdpxpf> searchHdpxpfRecord(String tableId, String memName,int batchExtractSize, int batchID);
}