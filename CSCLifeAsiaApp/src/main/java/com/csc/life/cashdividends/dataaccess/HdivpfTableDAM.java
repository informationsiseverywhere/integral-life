package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdivpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:27
 * Class transformed from HDIVPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdivpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 143;
	public FixedLengthStringData hdivrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hdivpfRecord = hdivrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hdivrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hdivrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hdivrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hdivrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hdivrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hdivrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hdivrec);
	public PackedDecimalData puAddNbr = DD.hpuanbr.copy().isAPartOf(hdivrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hdivrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(hdivrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(hdivrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(hdivrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(hdivrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(hdivrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(hdivrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(hdivrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdAllocDate = DD.hdvaldt.copy().isAPartOf(hdivrec);
	public FixedLengthStringData divdType = DD.hdvtyp.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdAmount = DD.hdvamt.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdRate = DD.hdvrate.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdRtEffdt = DD.hdveffdt.copy().isAPartOf(hdivrec);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(hdivrec);
	public FixedLengthStringData zcshdivmth = DD.zcshdivmth.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdOptprocTranno = DD.hdvopttx.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdCapTranno = DD.hdvcaptx.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdIntCapDate = DD.hincapdt.copy().isAPartOf(hdivrec);
	public PackedDecimalData divdStmtNo = DD.hdvsmtno.copy().isAPartOf(hdivrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hdivrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hdivrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hdivrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HdivpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HdivpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HdivpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HdivpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdivpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HdivpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdivpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HDIVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"HPUANBR, " +
							"TRANNO, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"CNTCURR, " +
							"EFFDATE, " +
							"HDVALDT, " +
							"HDVTYP, " +
							"HDVAMT, " +
							"HDVRATE, " +
							"HDVEFFDT, " +
							"ZDIVOPT, " +
							"ZCSHDIVMTH, " +
							"HDVOPTTX, " +
							"HDVCAPTX, " +
							"HINCAPDT, " +
							"HDVSMTNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     puAddNbr,
                                     tranno,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     cntcurr,
                                     effdate,
                                     divdAllocDate,
                                     divdType,
                                     divdAmount,
                                     divdRate,
                                     divdRtEffdt,
                                     zdivopt,
                                     zcshdivmth,
                                     divdOptprocTranno,
                                     divdCapTranno,
                                     divdIntCapDate,
                                     divdStmtNo,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		puAddNbr.clear();
  		tranno.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		cntcurr.clear();
  		effdate.clear();
  		divdAllocDate.clear();
  		divdType.clear();
  		divdAmount.clear();
  		divdRate.clear();
  		divdRtEffdt.clear();
  		zdivopt.clear();
  		zcshdivmth.clear();
  		divdOptprocTranno.clear();
  		divdCapTranno.clear();
  		divdIntCapDate.clear();
  		divdStmtNo.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHdivrec() {
  		return hdivrec;
	}

	public FixedLengthStringData getHdivpfRecord() {
  		return hdivpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHdivrec(what);
	}

	public void setHdivrec(Object what) {
  		this.hdivrec.set(what);
	}

	public void setHdivpfRecord(Object what) {
  		this.hdivpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hdivrec.getLength());
		result.set(hdivrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}