/******************************************************************************
 * File Name 		: Hdpxpf.java
 * Author			: sbatra9
 * Creation Date	: 23 June 2020
 * Project			: Integral Life
 * Description		: The Model Class for HDVXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.model;

import java.util.Date;

public class Hdvxpf {

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int plnsfx;
	private String crtable;
	private int crrcd;
	private int cbunst;
	private int rcesdte;
	private int sumins;
	private int currfrom;
	private int currto;
	private String cntcurr;
	private int tranno;
	private String pstatcode;
	private String statcode;
	private int ptdate;
	private String bnusin;
	private String usrprf;
	private String jobnm;
	private Date datime;
	// Constructor
		public Hdvxpf ( ) {
			//Empty Constructor
		}
	public Hdvxpf(Hdvxpf hdvxpf){
		this.chdrcoy=hdvxpf.chdrcoy;
    	this.chdrnum=hdvxpf.chdrnum;
    	this.life=hdvxpf.life;
    	this.jlife=hdvxpf.jlife;
    	this.coverage=hdvxpf.coverage;
    	this.rider=hdvxpf.rider;
    	this.plnsfx=hdvxpf.plnsfx;
    	this.crtable=hdvxpf.crtable;
    	this.crrcd=hdvxpf.crrcd;
    	this.cbunst=hdvxpf.cbunst;
    	this.rcesdte=hdvxpf.rcesdte;
    	this.sumins=hdvxpf.sumins;
    	this.currfrom=hdvxpf.currfrom;
    	this.currto=hdvxpf.currto;
    	this.cntcurr=hdvxpf.cntcurr;
    	this.tranno=hdvxpf.tranno;
    	this.pstatcode=hdvxpf.pstatcode;
    	this.statcode=hdvxpf.statcode;
    	this.ptdate=hdvxpf.ptdate;
    	this.bnusin=hdvxpf.bnusin;
    	this.usrprf=hdvxpf.usrprf;
    	this.jobnm=hdvxpf.jobnm;
    	this.datime=hdvxpf.datime;
	}
	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public int getPlnsfx(){
		return this.plnsfx;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());
	}
	public String getCrtable() {
		return crtable;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public int getCbunst() {
		return cbunst;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public int getSumins() {
		return sumins;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public int getTranno() {
		return tranno;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public String getBnusin() {
		return bnusin;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public int getCurrto() {
		return currto;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	
	public void setCntcurr( String cntcurr ){
		 this.cntcurr = cntcurr;
	}

	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public void setCbunst(int cbunst) {
		this.cbunst = cbunst;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	public void setSumins(int sumins) {
		this.sumins = sumins;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public void setBnusin(String bnusin) {
		this.bnusin = bnusin;
	}

	// ToString method
	public String toString(){
		StringBuilder output = new StringBuilder();
		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("CRTABLE:		");
		output.append(getCrtable());
		output.append("\r\n");
		output.append("CRRCD:     ");
		output.append(getCrrcd());
		output.append("\r\n");
		output.append("CBUNST:     ");
		output.append(getCbunst());
		output.append("\r\n");
		output.append("RCESDTE:		");
		output.append(getRcesdte());
		output.append("\r\n");
		output.append("SUMINS:		");
		output.append(getSumins());
		output.append("\r\n");
		output.append("CURRFROM:		");
		output.append(getCurrfrom());
		output.append("\r\n");
		output.append("CURRTO:		");
		output.append(getCurrto());
		output.append("\r\n");
		output.append("CNTCURR:		");
		output.append(getCntcurr());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("PSTATCODE:		");
		output.append(getPstatcode());
		output.append("\r\n");
		output.append("STATCODE:		");
		output.append(getStatcode());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");
		return output.toString();
	}
}
