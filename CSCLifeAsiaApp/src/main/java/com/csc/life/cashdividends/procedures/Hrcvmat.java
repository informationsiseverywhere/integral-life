/*
 * File: Hrcvmat.java
 * Date: 29 August 2009 22:54:46
 * Author: Quipoz Limited
 *
 * Class transformed from HRCVMAT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisgrvTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivgrvTableDAM;
import com.csc.life.cashdividends.dataaccess.HmtdTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuarevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*  CASH DIVIDEND MATURITY GENERIC REVERSAL
*
*
*  This subroutine is called from REVMATY via T5671 as part of   m
*  the maturity reversal.  It reverses the cash dividend
*  related processing done in HCVMATP.
*
*
*  MAIN PROCESSING
*
* - Delete validflag '1' HPUA records and re-instate most recent
*   validflag '2' records
* - Delete HDIV with statement no zeroes or write a new one with
*   dividend amount negated
* - Delete validflag '1' HDIS records and re-instate most recent
*   validflag '2' records
*                                                                 <002>
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hrcvmat extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "HRCVMAT ";
		/* FORMATS */
	private String hmtdrec = "HMTDREC";
	private String hdivgrvrec = "HDIVGRVREC";
	private String hdisgrvrec = "HDISGRVREC";
	private String hdivrec = "HDIVREC";
	private String hdisrec = "HDISREC";
	private String hpuarec = "HPUAREC";
	private String hpuarevrec = "HPUAREVREC";
	private String hl15 = "HL15";
	private Greversrec greversrec = new Greversrec();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend & Interest Generic Reversal*/
	private HdisgrvTableDAM hdisgrvIO = new HdisgrvTableDAM();
		/*Dividend Allocation Trans Details Logica*/
	private HdivTableDAM hdivIO = new HdivTableDAM();
		/*Cash Dividend Alloc Generic Reversal*/
	private HdivgrvTableDAM hdivgrvIO = new HdivgrvTableDAM();
		/*Maturity Additional Details*/
	private HmtdTableDAM hmtdIO = new HmtdTableDAM();
		/*Paid Up Addition Coverage Details Logica*/
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
		/*Dividend Paid Up Addition Reversal*/
	private HpuarevTableDAM hpuarevIO = new HpuarevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		xxxxErrorProg
	}

	public Hrcvmat() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void xxxxFatalError()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					xxxxFatalErrors();
				}
				case xxxxErrorProg: {
					xxxxErrorProg();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.xxxxErrorProg);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		greversrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		wsaaBatckey.set(greversrec.batckey);
		/*EXIT*/
	}

protected void process2000()
	{
		process2001();
	}

protected void process2001()
	{
		hmtdIO.setDataArea(SPACES);
		hmtdIO.setChdrcoy(greversrec.chdrcoy);
		hmtdIO.setChdrnum(greversrec.chdrnum);
		hmtdIO.setLife(greversrec.life);
		hmtdIO.setCoverage(greversrec.coverage);
		hmtdIO.setRider(greversrec.rider);
		hmtdIO.setPlanSuffix(greversrec.planSuffix);
		hmtdIO.setTranno(greversrec.tranno);
		hmtdIO.setFormat(hmtdrec);
		hmtdIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hmtdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hmtdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX","TRANNO");

		SmartFileCode.execute(appVars, hmtdIO);
		if (isNE(hmtdIO.getStatuz(),varcom.oK)
		&& isNE(hmtdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hmtdIO.getParams());
			xxxxFatalError();
		}
		if (isNE(hmtdIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(hmtdIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(hmtdIO.getLife(),greversrec.life)
		|| isNE(hmtdIO.getCoverage(),greversrec.coverage)
		|| isNE(hmtdIO.getRider(),greversrec.rider)
		|| isNE(hmtdIO.getPlanSuffix(),greversrec.planSuffix)
		|| isNE(hmtdIO.getTranno(),greversrec.tranno)) {
			hmtdIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hmtdIO.getStatuz(),varcom.endp))) {
			hmtdIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hmtdIO);
			if (isNE(hmtdIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hmtdIO.getParams());
				xxxxFatalError();
			}
			hmtdIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hmtdIO);
			if (isNE(hmtdIO.getChdrcoy(),greversrec.chdrcoy)
			|| isNE(hmtdIO.getChdrnum(),greversrec.chdrnum)
			|| isNE(hmtdIO.getLife(),greversrec.life)
			|| isNE(hmtdIO.getCoverage(),greversrec.coverage)
			|| isNE(hmtdIO.getRider(),greversrec.rider)
			|| isNE(hmtdIO.getPlanSuffix(),greversrec.planSuffix)
			|| isNE(hmtdIO.getTranno(),greversrec.tranno)) {
				hmtdIO.setStatuz(varcom.endp);
			}
		}

		hpuarevIO.setDataArea(SPACES);
		hpuarevIO.setChdrcoy(greversrec.chdrcoy);
		hpuarevIO.setChdrnum(greversrec.chdrnum);
		hpuarevIO.setLife(greversrec.life);
		hpuarevIO.setCoverage(greversrec.coverage);
		hpuarevIO.setRider(greversrec.rider);
		hpuarevIO.setPlanSuffix(greversrec.planSuffix);
		hpuarevIO.setTranno(greversrec.tranno);
		hpuarevIO.setFormat(hpuarevrec);
		hpuarevIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hpuarevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuarevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX","TRANNO");

		SmartFileCode.execute(appVars, hpuarevIO);
		if (isNE(hpuarevIO.getStatuz(),varcom.oK)
		&& isNE(hpuarevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuarevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(hpuarevIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(hpuarevIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(hpuarevIO.getLife(),greversrec.life)
		|| isNE(hpuarevIO.getCoverage(),greversrec.coverage)
		|| isNE(hpuarevIO.getRider(),greversrec.rider)
		|| isNE(hpuarevIO.getPlanSuffix(),greversrec.planSuffix)
		|| isNE(hpuarevIO.getTranno(),greversrec.tranno)) {
			hpuarevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hpuarevIO.getStatuz(),varcom.endp))) {
			hpuaIO.setDataArea(SPACES);
			hpuaIO.setRrn(hpuarevIO.getRrn());
			hpuaIO.setFormat(hpuarec);
			hpuaIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hpuaIO.getParams());
				xxxxFatalError();
			}
			hpuaIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hpuaIO.getParams());
				xxxxFatalError();
			}
			hpuaIO.setChdrcoy(hpuarevIO.getChdrcoy());
			hpuaIO.setChdrnum(hpuarevIO.getChdrnum());
			hpuaIO.setLife(hpuarevIO.getLife());
			hpuaIO.setCoverage(hpuarevIO.getCoverage());
			hpuaIO.setRider(hpuarevIO.getRider());
			hpuaIO.setPlanSuffix(hpuarevIO.getPlanSuffix());
			hpuaIO.setPuAddNbr(hpuarevIO.getPuAddNbr());
			hpuaIO.setFunction(varcom.begn);

			//performance improvement -- Anjali
			/*hpuaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);*/

			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getChdrcoy(),hpuarevIO.getChdrcoy())
			|| isNE(hpuaIO.getChdrnum(),hpuarevIO.getChdrnum())
			|| isNE(hpuaIO.getLife(),hpuarevIO.getLife())
			|| isNE(hpuaIO.getCoverage(),hpuarevIO.getCoverage())
			|| isNE(hpuaIO.getRider(),hpuarevIO.getRider())
			|| isNE(hpuaIO.getPlanSuffix(),hpuarevIO.getPlanSuffix())
			|| isNE(hpuaIO.getPuAddNbr(),hpuarevIO.getPuAddNbr())) {
				hpuarevIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(hpuaIO.getValidflag(),"2")) {
					syserrrec.params.set(hpuarevIO.getParams());
					syserrrec.statuz.set(hl15);
					xxxxFatalError();
				}
				hpuaIO.setValidflag("1");
				hpuaIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hpuaIO.getParams());
					xxxxFatalError();
				}
				hpuarevIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hpuarevIO);
				if (isNE(hpuarevIO.getStatuz(),varcom.oK)
				&& isNE(hpuarevIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hpuarevIO.getParams());
					xxxxFatalError();
				}
				if (isNE(hpuarevIO.getChdrcoy(),greversrec.chdrcoy)
				|| isNE(hpuarevIO.getChdrnum(),greversrec.chdrnum)
				|| isNE(hpuarevIO.getLife(),greversrec.life)
				|| isNE(hpuarevIO.getCoverage(),greversrec.coverage)
				|| isNE(hpuarevIO.getRider(),greversrec.rider)
				|| isNE(hpuarevIO.getPlanSuffix(),greversrec.planSuffix)
				|| isNE(hpuarevIO.getTranno(),greversrec.tranno)) {
					hpuarevIO.setStatuz(varcom.endp);
				}
			}
		}

		hdivgrvIO.setDataArea(SPACES);
		hdivgrvIO.setChdrcoy(greversrec.chdrcoy);
		hdivgrvIO.setChdrnum(greversrec.chdrnum);
		hdivgrvIO.setLife(greversrec.life);
		hdivgrvIO.setCoverage(greversrec.coverage);
		hdivgrvIO.setRider(greversrec.rider);
		hdivgrvIO.setPlanSuffix(greversrec.planSuffix);
		hdivgrvIO.setTranno(greversrec.tranno);
		hdivgrvIO.setFormat(hdivgrvrec);
		hdivgrvIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdivgrvIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, hdivgrvIO);
		if (isNE(hdivgrvIO.getStatuz(),varcom.oK)
		&& isNE(hdivgrvIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdivgrvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(hdivgrvIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(hdivgrvIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(hdivgrvIO.getLife(),greversrec.life)
		|| isNE(hdivgrvIO.getCoverage(),greversrec.coverage)
		|| isNE(hdivgrvIO.getRider(),greversrec.rider)
		|| isNE(hdivgrvIO.getPlanSuffix(),greversrec.planSuffix)
		|| isNE(hdivgrvIO.getTranno(),greversrec.tranno)) {
			hdivgrvIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivgrvIO.getStatuz(),varcom.endp))) {
			if (isEQ(hdivgrvIO.getDivdStmtNo(),ZERO)) {
				hdivgrvIO.setFunction(varcom.deltd);
				SmartFileCode.execute(appVars, hdivgrvIO);
				if (isNE(hdivgrvIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivgrvIO.getParams());
					xxxxFatalError();
				}
			}
			else {
				hdivIO.setNonKey(hdivgrvIO.getNonKey());
				hdivIO.setDataKey(SPACES);
				hdivIO.setChdrcoy(hdivgrvIO.getChdrcoy());
				hdivIO.setChdrnum(hdivgrvIO.getChdrnum());
				hdivIO.setLife(hdivgrvIO.getLife());
				hdivIO.setCoverage(hdivgrvIO.getCoverage());
				hdivIO.setRider(hdivgrvIO.getRider());
				hdivIO.setPlanSuffix(hdivgrvIO.getPlanSuffix());
				hdivIO.setEffdate(hdivgrvIO.getEffdate());
				hdivIO.setTranno(greversrec.tranno);
				setPrecision(hdivIO.getDivdAmount(), 2);
				hdivIO.setDivdAmount(mult(hdivgrvIO.getDivdAmount(),-1));
				hdivIO.setDivdStmtNo(ZERO);
				hdivIO.setDivdAllocDate(greversrec.effdate);
				hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
				hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
				hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
				hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
				hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
				hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
				hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
				hdivIO.setFormat(hdivrec);
				hdivIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivIO);
				if (isNE(hdivIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivIO.getParams());
					xxxxFatalError();
				}
			}
			hdivgrvIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivgrvIO);
			if (isNE(hdivgrvIO.getStatuz(),varcom.oK)
			&& isNE(hdivgrvIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdivgrvIO.getParams());
				xxxxFatalError();
			}
			if (isNE(hdivgrvIO.getChdrcoy(),greversrec.chdrcoy)
			|| isNE(hdivgrvIO.getChdrnum(),greversrec.chdrnum)
			|| isNE(hdivgrvIO.getLife(),greversrec.life)
			|| isNE(hdivgrvIO.getCoverage(),greversrec.coverage)
			|| isNE(hdivgrvIO.getRider(),greversrec.rider)
			|| isNE(hdivgrvIO.getPlanSuffix(),greversrec.planSuffix)
			|| isNE(hdivgrvIO.getTranno(),greversrec.tranno)) {
				hdivgrvIO.setStatuz(varcom.endp);
			}
		}

		hdisgrvIO.setDataKey(SPACES);
		hdisgrvIO.setChdrcoy(greversrec.chdrcoy);
		hdisgrvIO.setChdrnum(greversrec.chdrnum);
		hdisgrvIO.setLife(greversrec.life);
		hdisgrvIO.setCoverage(greversrec.coverage);
		hdisgrvIO.setRider(greversrec.rider);
		hdisgrvIO.setPlanSuffix(greversrec.planSuffix);
		hdisgrvIO.setTranno(greversrec.tranno);
		hdisgrvIO.setFormat(hdisgrvrec);
		hdisgrvIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdisgrvIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, hdisgrvIO);
		if (isNE(hdisgrvIO.getStatuz(),varcom.oK)
		&& isNE(hdisgrvIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdisgrvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(greversrec.chdrcoy,hdisgrvIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hdisgrvIO.getChdrnum())
		|| isNE(greversrec.life,hdisgrvIO.getLife())
		|| isNE(greversrec.coverage,hdisgrvIO.getCoverage())
		|| isNE(greversrec.rider,hdisgrvIO.getRider())
		|| isNE(greversrec.planSuffix,hdisgrvIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hdisgrvIO.getTranno())) {
			hdisgrvIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdisgrvIO.getStatuz(),varcom.endp))) {
			hdisIO.setParams(SPACES);
			hdisIO.setRrn(hdisgrvIO.getRrn());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				xxxxFatalError();
			}
			hdisIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				xxxxFatalError();
			}
			hdisIO.setChdrcoy(hdisgrvIO.getChdrcoy());
			hdisIO.setChdrnum(hdisgrvIO.getChdrnum());
			hdisIO.setLife(hdisgrvIO.getLife());
			hdisIO.setCoverage(hdisgrvIO.getCoverage());
			hdisIO.setRider(hdisgrvIO.getRider());
			hdisIO.setPlanSuffix(hdisgrvIO.getPlanSuffix());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.begn);

			//performance improvement -- Anjali
			hdisIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)
			&& isNE(hdisIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdisIO.getParams());
				xxxxFatalError();
			}
			if (isEQ(hdisIO.getStatuz(),varcom.endp)
			|| isNE(hdisIO.getChdrcoy(),hdisgrvIO.getChdrcoy())
			|| isNE(hdisIO.getChdrnum(),hdisgrvIO.getChdrnum())
			|| isNE(hdisIO.getLife(),hdisgrvIO.getLife())
			|| isNE(hdisIO.getCoverage(),hdisgrvIO.getCoverage())
			|| isNE(hdisIO.getRider(),hdisgrvIO.getRider())
			|| isNE(hdisIO.getPlanSuffix(),hdisgrvIO.getPlanSuffix())) {
				hdisgrvIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(hdisIO.getValidflag(),"2")) {
					hdisgrvIO.setStatuz(hl15);
					syserrrec.params.set(hdisgrvIO.getParams());
					xxxxFatalError();
				}
				hdisIO.setValidflag("1");
				hdisIO.setFormat(hdisrec);
				hdisIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, hdisIO);
				if (isNE(hdisIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdisIO.getParams());
					xxxxFatalError();
				}
				hdisgrvIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, hdisgrvIO);
				if (isNE(hdisgrvIO.getStatuz(),varcom.oK)
				&& isNE(hdisgrvIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hdisgrvIO.getParams());
					xxxxFatalError();
				}
				if (isNE(hdisgrvIO.getChdrcoy(),greversrec.chdrcoy)
				|| isNE(hdisgrvIO.getChdrnum(),greversrec.chdrnum)
				|| isNE(hdisgrvIO.getLife(),greversrec.life)
				|| isNE(hdisgrvIO.getCoverage(),greversrec.coverage)
				|| isNE(hdisgrvIO.getRider(),greversrec.rider)
				|| isNE(hdisgrvIO.getPlanSuffix(),greversrec.planSuffix)
				|| isNE(hdisgrvIO.getTranno(),greversrec.tranno)) {
					hdisgrvIO.setStatuz(varcom.endp);
				}
			}
		}

	}
}
