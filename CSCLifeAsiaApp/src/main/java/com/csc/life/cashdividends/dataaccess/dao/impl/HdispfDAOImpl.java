/******************************************************************************
 * File Name 		: HdispfDAOImpl.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for HDISPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.cashdividends.dataaccess.dao.HdispfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HdispfDAOImpl extends BaseDAOImpl<Hdispf> implements HdispfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HdispfDAOImpl.class);

	public List<Hdispf> searchHdispfRecord(Hdispf hdispf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, CNTCURR, ");
		sqlSelect.append("HDV1STDT, HDVLDT, HINTLDT, HINTNDT, HCAPLDT, ");
		sqlSelect.append("HCAPNDT, HDVBALL, HDVBALC, HINTOS, HDVSMTDT, ");
		sqlSelect.append("HDVSMTNO, HDVBALST, SACSCODE, SACSTYP, HINTDUEM, ");
		sqlSelect.append("HINTDUED, HCAPDUEM, HCAPDUED, USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM HDISPF WHERE CHDRCOY = ? AND CHDRNUM = ?");//IJTI-1726
		
		//IJTI-1726 starts
		if(hdispf.getPlnsfx() != null) {
			sqlSelect.append(" AND PLNSFX = ?");
		}
		//IJTI-1726 ends
		
		if(hdispf.getCoverage() != null && hdispf.getRider() != null && hdispf.getLife() != null)
		{
			sqlSelect.append(" AND COVERAGE = ? AND RIDER = ? AND LIFE = ?");
		}
		sqlSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHdispfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdispfRs = null;
		List<Hdispf> outputList = new ArrayList<>();//IJTI-1726

		try {

			psHdispfSelect.setString(1, hdispf.getChdrcoy());
			psHdispfSelect.setString(2, hdispf.getChdrnum());
			//IJTI-1726 starts
			int index = 3;
			if(hdispf.getPlnsfx() != null) {
				psHdispfSelect.setInt(index, hdispf.getPlnsfx());
				index++;
			}
			
			if(hdispf.getCoverage() != null && hdispf.getRider() != null && hdispf.getLife() != null)
			{
				psHdispfSelect.setString(index, hdispf.getCoverage());
				index++;
				psHdispfSelect.setString(index, hdispf.getRider());
				index++;
				psHdispfSelect.setString(index, hdispf.getLife());
				index++;
			}
			//IJTI-1726 ends

			sqlHdispfRs = executeQuery(psHdispfSelect);

			while (sqlHdispfRs.next()) {
				Hdispf hdispfnew = prepareHdispf(sqlHdispfRs);//IJTI-1726
				outputList.add(hdispfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHdispfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdispfSelect, sqlHdispfRs);
		}

		return outputList;
	}
	
	//IJTI-1726 starts
	private Hdispf prepareHdispf(ResultSet sqlHdispfRs) throws SQLException {
		Hdispf hdispf = new Hdispf();
		hdispf.setUniqueNumber(sqlHdispfRs.getInt("UNIQUE_NUMBER"));
		hdispf.setChdrcoy(sqlHdispfRs.getString("CHDRCOY"));
		hdispf.setChdrnum(sqlHdispfRs.getString("CHDRNUM"));
		hdispf.setLife(sqlHdispfRs.getString("LIFE"));
		hdispf.setJlife(sqlHdispfRs.getString("JLIFE"));
		hdispf.setCoverage(sqlHdispfRs.getString("COVERAGE"));
		hdispf.setRider(sqlHdispfRs.getString("RIDER"));
		hdispf.setPlnsfx(sqlHdispfRs.getInt("PLNSFX"));
		hdispf.setValidflag(sqlHdispfRs.getString("VALIDFLAG"));				
		hdispf.setTranno(sqlHdispfRs.getInt("TRANNO"));				
		hdispf.setCntcurr(sqlHdispfRs.getString("CNTCURR"));					
		hdispf.setHdv1stdt(sqlHdispfRs.getInt("HDV1STDT"));
		hdispf.setHdvldt(sqlHdispfRs.getInt("HDVLDT"));
		hdispf.setHintldt(sqlHdispfRs.getInt("HINTLDT"));
		hdispf.setHintndt(sqlHdispfRs.getInt("HINTNDT"));
		hdispf.setHcapldt(sqlHdispfRs.getInt("HCAPLDT"));
		hdispf.setHcapndt(sqlHdispfRs.getInt("HCAPNDT"));
		hdispf.setHdvball(sqlHdispfRs.getBigDecimal("HDVBALL"));
		hdispf.setHdvbalc(sqlHdispfRs.getBigDecimal("HDVBALC"));				
		hdispf.setHintos(sqlHdispfRs.getBigDecimal("HINTOS"));				
		hdispf.setHdvsmtdt(sqlHdispfRs.getInt("HDVSMTDT"));				
		hdispf.setHdvsmtno(sqlHdispfRs.getInt("HDVSMTNO"));
		hdispf.setHdvbalst(sqlHdispfRs.getBigDecimal("HDVBALST"));
		hdispf.setSacscode(sqlHdispfRs.getString("SACSCODE"));
		hdispf.setSacstyp(sqlHdispfRs.getString("SACSTYP"));
		hdispf.setHintduem(sqlHdispfRs.getString("HINTDUEM"));
		hdispf.setHintdued(sqlHdispfRs.getString("HINTDUED"));
		hdispf.setHcapduem(sqlHdispfRs.getString("HCAPDUEM"));
		hdispf.setHcapdued(sqlHdispfRs.getString("HCAPDUED"));				
		hdispf.setUsrprf(sqlHdispfRs.getString("USRPRF"));
		hdispf.setJobnm(sqlHdispfRs.getString("JOBNM"));
		hdispf.setDatime(sqlHdispfRs.getDate("DATIME"));
		return hdispf;
	}
	//IJTI-1726 ends

	public void insertIntoHdispf(Hdispf hdispf) throws SQLRuntimeException{

		StringBuilder sqlInsert = new StringBuilder();

		sqlInsert.append("INSERT INTO HDISPF (");
		sqlInsert.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlInsert.append("RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, ");
		sqlInsert.append("BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ");
		sqlInsert.append("CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, ");
		sqlInsert.append("HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, ");
		sqlInsert.append("HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sqlInsert.append("DATIME ");
		sqlInsert.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

	//	PreparedStatement psHdispfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;

		/*try {

			psHdispfInsert.setChdrcoy(index++, hdispf.getString("CHDRCOY"));
			psHdispfInsert.setChdrnum(index++, hdispf.getString("CHDRNUM"));
			psHdispfInsert.setLife(index++, hdispf.getString("LIFE"));
			psHdispfInsert.setJlife(index++, hdispf.getString("JLIFE"));
			psHdispfInsert.setCoverage(index++, hdispf.getString("COVERAGE"));
			psHdispfInsert.setRider(index++, hdispf.getString("RIDER"));
			psHdispfInsert.setPlnsfx(index++, hdispf.getInt("PLNSFX"));
			psHdispfInsert.setHpuanbr(index++, hdispf.getInt("HPUANBR"));
			psHdispfInsert.setTranno(index++, hdispf.getInt("TRANNO"));
			psHdispfInsert.setBatccoy(index++, hdispf.getString("BATCCOY"));
			psHdispfInsert.setBatcbrn(index++, hdispf.getString("BATCBRN"));
			psHdispfInsert.setBatcactyr(index++, hdispf.getInt("BATCACTYR"));
			psHdispfInsert.setBatcactmn(index++, hdispf.getInt("BATCACTMN"));
			psHdispfInsert.setBatctrcde(index++, hdispf.getString("BATCTRCDE"));
			psHdispfInsert.setBatcbatch(index++, hdispf.getString("BATCBATCH"));
			psHdispfInsert.setCntcurr(index++, hdispf.getString("CNTCURR"));
			psHdispfInsert.setEffdate(index++, hdispf.getInt("EFFDATE"));
			psHdispfInsert.setHdvaldt(index++, hdispf.getInt("HDVALDT"));
			psHdispfInsert.setHdvtyp(index++, hdispf.getString("HDVTYP"));
			psHdispfInsert.setHdvamt(index++, hdispf.getBigDecimal("HDVAMT"));
			psHdispfInsert.setHdvrate(index++, hdispf.getBigDecimal("HDVRATE"));
			psHdispfInsert.setHdveffdt(index++, hdispf.getInt("HDVEFFDT"));
			psHdispfInsert.setZdivopt(index++, hdispf.getString("ZDIVOPT"));
			psHdispfInsert.setZcshdivmth(index++, hdispf.getString("ZCSHDIVMTH"));
			psHdispfInsert.setHdvopttx(index++, hdispf.getInt("HDVOPTTX"));
			psHdispfInsert.setHdvcaptx(index++, hdispf.getInt("HDVCAPTX"));
			psHdispfInsert.setHincapdt(index++, hdispf.getInt("HINCAPDT"));
			psHdispfInsert.setHdvsmtno(index++, hdispf.getInt("HDVSMTNO"));
			psHdispfInsert.setUsrprf(index++, hdispf.getString("USRPRF"));
			psHdispfInsert.setJobnm(index++, hdispf.getString("JOBNM"));
			psHdispfInsert.setDatime(index++, hdispf.getDate("DATIME"));

			int affectedCount = executeUpdate(psHdispfInsert);

			LOGGER.debug("insertIntoHdispf {"+ affectedCount +"} rows inserted.");

		} catch (SQLException e) {
			LOGGER.error("insertIntoHdispf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdispfInsert, null);
		}*/
	}
	/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction*/
	public List<Hdispf>	readHdispfData(Hdispf hdisModel)	
	{
		// ---------------------------------
		// Initialize variables
		// ---------------------------------
		Hdispf hdispf = null;
		List<Hdispf> hdispfResult = new LinkedList<Hdispf>();
		PreparedStatement stmn = null;
		ResultSet rs = null;
		StringBuilder sqlStringBuilder = new StringBuilder();
		
		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlStringBuilder.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CNTCURR,HDV1STDT,HDVLDT,HINTLDT,HINTNDT,HCAPLDT,HCAPNDT,HDVBALL,HDVBALC,HINTOS,HDVSMTDT,HDVSMTNO,HDVBALST,SACSCODE,SACSTYP,HINTDUEM,HINTDUED,HCAPDUEM,HCAPDUED,USRPRF,JOBNM,DATIME");
		sqlStringBuilder.append(" FROM HDISDRY  ");
		sqlStringBuilder.append(" WHERE CHDRCOY=? AND CHDRNUM = ? AND PLNSFX=? ");
		stmn = getPrepareStatement(sqlStringBuilder.toString());
		
		try {
			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			stmn.setString(1, hdisModel.getChdrcoy());
			stmn.setString(2, hdisModel.getChdrnum());
			stmn.setInt(3, hdisModel.getPlnsfx());
			
			// ---------------------------------
			// Execute Query
			// ---------------------------------
			rs = executeQuery(stmn);
			
			while (rs.next()) {
				hdispf = new Hdispf();
				hdispf.setUniqueNumber(rs.getLong(1));
				hdispf.setChdrcoy(rs.getString(2));
				hdispf.setJlife(rs.getString(4));
				hdispf.setChdrnum(rs.getString(3));  //ILIFE-3838 dpuhawan
				hdispf.setLife(rs.getString(4)); //ILIFE-3838 dpuhawan
				hdispf.setJlife(rs.getString(5));
				hdispf.setCoverage(rs.getString(6));
				hdispf.setRider(rs.getString(7));
				hdispf.setPlnsfx(rs.getInt(8));
				hdispf.setValidflag(rs.getString(9));
				hdispf.setTranno(rs.getInt(10));
				hdispf.setCntcurr(rs.getString(11));
				hdispf.setHdv1stdt(rs.getInt(12));
				hdispf.setHdvldt(rs.getInt(13));
				hdispf.setHintldt(rs.getInt(14));
				hdispf.setHintndt(rs.getInt(15));
				hdispf.setHcapldt(rs.getInt(16));
				hdispf.setHcapndt(rs.getInt(17));
				hdispf.setHdvball(rs.getBigDecimal(18));
				hdispf.setHdvbalc(rs.getBigDecimal(19));
				hdispf.setHintos(rs.getBigDecimal(20));
				hdispf.setHdvsmtdt(rs.getInt(21));
				hdispf.setHdvsmtno(rs.getInt(22));
				hdispf.setHdvbalst(rs.getBigDecimal(23));
				hdispf.setSacscode(rs.getString(24));
				hdispf.setSacstyp(rs.getString(25));
				hdispf.setHintduem(rs.getString(26));
				hdispf.setHintdued(rs.getString(27));
				hdispf.setHcapduem(rs.getString(28));
				hdispf.setHcapdued(rs.getString(29));
				hdispf.setUsrprf(rs.getString(30));
				hdispf.setJobnm(rs.getString(31));
				hdispf.setDatime(rs.getDate(32));
				hdispfResult.add(hdispf);
			}

		} catch (SQLException e) {
			LOGGER.error("readHdispfData()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(stmn, rs);
		}
		return hdispfResult;
	
			
	}

    public Map<String, Hdispf> searchHdispfRecord(String coy, List<String> chdrnumList) {
        StringBuilder sql = new StringBuilder();
        sql.append(
                "SELECT HCAPDUED,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CNTCURR,HDV1STDT,HDVLDT,HINTLDT,HINTNDT,HCAPLDT,HCAPNDT,HDVBALL,HDVBALC,HINTOS,HDVSMTDT,HDVSMTNO,HDVBALST,SACSCODE,SACSTYP,HINTDUEM,HINTDUED,HCAPDUEM FROM HDISPF WHERE CHDRCOY=? AND ")
                .append(getSqlInStr("CHDRNUM", chdrnumList))
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        Map<String, Hdispf> hdispfMap = new HashMap<String, Hdispf>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
                Hdispf hdispf = new Hdispf();
                hdispf.setHcapdued(rs.getString(1));
                hdispf.setChdrcoy(rs.getString(2));
                hdispf.setChdrnum(rs.getString(3));
                hdispf.setLife(rs.getString(4));
                hdispf.setJlife(rs.getString(5));
                hdispf.setCoverage(rs.getString(6));
                hdispf.setRider(rs.getString(7));
                hdispf.setPlnsfx(rs.getInt(8));
                hdispf.setValidflag(rs.getString(9));
                hdispf.setTranno(rs.getInt(10));
                hdispf.setCntcurr(rs.getString(11));
                hdispf.setHdv1stdt(rs.getInt(12));
                hdispf.setHdvldt(rs.getInt(13));
                hdispf.setHintldt(rs.getInt(14));
                hdispf.setHintndt(rs.getInt(15));
                hdispf.setHcapldt(rs.getInt(16));
                hdispf.setHcapndt(rs.getInt(17));
                hdispf.setHdvbalst(rs.getBigDecimal(18));

                hdispf.setHdvbalc(rs.getBigDecimal(19));                
                hdispf.setHintos(rs.getBigDecimal(20));
                hdispf.setHdvsmtdt(rs.getInt(21));
                hdispf.setHdvsmtno(rs.getInt(22));
                hdispf.setHdvbalst(rs.getBigDecimal(23));
                hdispf.setSacscode(rs.getString(24));

                hdispf.setSacstyp(rs.getString(25));
                hdispf.setHintduem(rs.getString(26));
                hdispf.setHintdued(rs.getString(27));
                hdispf.setHcapduem(rs.getString(28));

                String chdrnum = hdispf.getChdrnum();
                if (!hdispfMap.containsKey(chdrnum)) {
                    hdispfMap.put(chdrnum, hdispf);
                } else {
                    continue;
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchHdispfRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return hdispfMap;
    }	
    
    public Map<String, List<Hdispf>> getHdisMap(String coy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CNTCURR, HDV1STDT, HDVLDT, HINTLDT, HINTNDT, HCAPLDT, HCAPNDT, HDVBALL, HDVBALC, HINTOS, HDVSMTDT, HDVSMTNO, HDVBALST, SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, USRPRF, JOBNM, DATIME  ");
        sb.append("FROM VM1DTA.HDISPF WHERE VALIDFLAG='1' AND CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");            
       // LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Hdispf>> hdisMap = new HashMap<String, List<Hdispf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
            	Hdispf hdispf = new Hdispf();
                hdispf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                hdispf.setChdrcoy(rs.getString("CHDRCOY"));
                hdispf.setChdrnum(rs.getString("CHDRNUM"));
                hdispf.setLife(rs.getString("LIFE"));                
                hdispf.setJlife(rs.getString("JLIFE"));                
                hdispf.setCoverage(rs.getString("COVERAGE"));
                hdispf.setRider(rs.getString("RIDER"));        
                hdispf.setPlnsfx(rs.getInt("PLNSFX"));
                hdispf.setValidflag(rs.getString("VALIDFLAG"));
                hdispf.setTranno(rs.getInt("TRANNO"));
                hdispf.setCntcurr(rs.getString("CNTCURR"));
                hdispf.setHdv1stdt(rs.getInt("HDV1STDT"));
                hdispf.setHdvldt(rs.getInt("HDVLDT"));
                hdispf.setHintldt(rs.getInt("HINTLDT"));
                hdispf.setHintndt(rs.getInt("HINTNDT"));  
                hdispf.setHcapldt(rs.getInt("HCAPLDT"));
                hdispf.setHcapndt(rs.getInt("HCAPNDT"));     
                hdispf.setHdvball(rs.getBigDecimal("HDVBALL"));
                hdispf.setHdvbalc(rs.getBigDecimal("HDVBALC"));      
                hdispf.setHintos(rs.getBigDecimal("HINTOS"));
                hdispf.setHdvsmtdt(rs.getInt("HDVSMTDT"));
                hdispf.setHdvsmtno(rs.getInt("HDVSMTNO"));  
                hdispf.setHdvbalst(rs.getBigDecimal("HDVBALST")); 
                hdispf.setSacscode(rs.getString("SACSCODE")); 
                hdispf.setSacstyp(rs.getString("SACSTYP")); 
				hdispf.setHintduem(rs.getString("HINTDUEM")); 
				hdispf.setHintdued(rs.getString("HINTDUED"));
				hdispf.setHcapduem(rs.getString("HCAPDUEM"));
				hdispf.setHcapdued(rs.getString("HCAPDUED"));
				hdispf.setUsrprf(rs.getString("USRPRF"));
				hdispf.setJobnm(rs.getString("JOBNM"));                
				hdispf.setDatime(rs.getDate("DATIME"));                 

                if (hdisMap.containsKey(hdispf.getChdrnum())) {
                	hdisMap.get(hdispf.getChdrnum()).add(hdispf);
                } else {
                    List<Hdispf> hdisList = new ArrayList<Hdispf>();
                    hdisList.add(hdispf);
                    hdisMap.put(hdispf.getChdrnum(), hdisList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("getHdisMap()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return hdisMap;

    }

	 public boolean updateHdisValidflag(Hdispf hdispf) {
		 StringBuilder sb = new StringBuilder("");
			sb.append("UPDATE HDISPF SET VALIDFLAG=? ");
			sb.append(" WHERE UNIQUE_NUMBER=? ");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				 ps = getPrepareStatement(sb.toString());	
				 ps.setString(1, hdispf.getValidflag());/* IJTI-1523 */
				 ps.setLong(2, hdispf.getUniqueNumber());
				 ps.executeUpdate();				
				
			}catch (SQLException e) {
				LOGGER.error("updateHdisValidflag()",e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}		
			
			return true;
	}

	@Override
	public void insertHdisValidRecord(List<Hdispf> hdisBulkOpList) {
		if (hdisBulkOpList != null && !hdisBulkOpList.isEmpty()) {
	           
            String SQL_HDIS_INSERT = "INSERT INTO HDISPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CNTCURR,"
            		+ " HDV1STDT, HDVLDT, HINTLDT, HINTNDT, HCAPLDT, HCAPNDT, HDVBALL, HDVBALC, HINTOS, HDVSMTDT, HDVSMTNO, HDVBALST, "
            		+ "SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, USRPRF, JOBNM, DATIME) SELECT CHDRCOY, CHDRNUM, LIFE, "
            		+ "JLIFE, COVERAGE, RIDER, PLNSFX, ?, ?, CNTCURR,HDV1STDT, HDVLDT, HINTLDT, HINTNDT, ?, ?, HDVBALL, ?, ?, HDVSMTDT, "
            		+ "HDVSMTNO, HDVBALST,SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, ?, ?, ? FROM "
            		+ "HDISPF WHERE UNIQUE_NUMBER=?";
       
            PreparedStatement psHdisInsert = getPrepareStatement(SQL_HDIS_INSERT);
            try {
                for (Hdispf c : hdisBulkOpList) {
                	psHdisInsert.setString(1, c.getValidflag());/* IJTI-1523 */
                	psHdisInsert.setInt(2, c.getTranno());
                	psHdisInsert.setInt(3, c.getHcapldt());
                	psHdisInsert.setInt(4, c.getHcapndt());
                	psHdisInsert.setBigDecimal(5, c.getHdvbalc());
                	psHdisInsert.setBigDecimal(6, c.getHintos());
                    // JOBNM,USRPRF,DATIME
                	psHdisInsert.setString(7, getJobnm());
                	psHdisInsert.setString(8, getUsrprf());
                	psHdisInsert.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
                	psHdisInsert.setLong(10, c.getUniqueNumber());					
                	psHdisInsert.addBatch();
                }
                psHdisInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertHdisValidRecord()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psHdisInsert, null);
            }
        }
		
	}	
	
	public boolean updateHdisRecords(Hdispf hdispf) {
		
		 StringBuilder sb = new StringBuilder("");
			sb.append("UPDATE HDISPF SET HDVBALL=? AND HDVBALC=?");
			sb.append("WHERE TRANNO=?  ");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				 ps = getPrepareStatement(sb.toString());	
				 ps.setString(1, hdispf.getHdvball().toString());
				 ps.setString(2, hdispf.getHdvbalc().toString());
				 ps.setInt(3, hdispf.getTranno());
				 ps.executeUpdate();				
				
			}catch (SQLException e) {
				LOGGER.error("updateHdisRecords()",e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}		
			
			return true;
	}
	
	
	public void insertHdisRecords(List<Hdispf> hdisBulkOpList) {
		
		if (hdisBulkOpList != null && !hdisBulkOpList.isEmpty()) {
	           
            String SQL_HDIS_INSERT = "INSERT INTO HDISPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CNTCURR,"
            		+ " HDV1STDT, HDVLDT, HINTLDT, HINTNDT, HCAPLDT, HCAPNDT, HDVBALL, HDVBALC, HINTOS, HDVSMTDT, HDVSMTNO, HDVBALST, "
            		+ "SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, USRPRF, JOBNM, DATIME) SELECT CHDRCOY, CHDRNUM, LIFE, "
            		+ "JLIFE, COVERAGE, RIDER, PLNSFX, ?, ?, CNTCURR,HDV1STDT, ?, HINTLDT, HINTNDT, HCAPLDT, HCAPNDT, ?, ?, HINTOS, HDVSMTDT, "
            		+ "HDVSMTNO, HDVBALST,SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, ?, ?, ? FROM "
            		+ "HDISPF WHERE UNIQUE_NUMBER=?";
       
            PreparedStatement psHdisInsert = getPrepareStatement(SQL_HDIS_INSERT);
            try {
                for (Hdispf c : hdisBulkOpList) {
                	psHdisInsert.setString(1, c.getValidflag());
                	psHdisInsert.setInt(2, c.getTranno());
                	psHdisInsert.setInt(3, c.getHdvldt());
                	psHdisInsert.setBigDecimal(4, c.getHdvball());
                	psHdisInsert.setBigDecimal(5, c.getHdvbalc());
                    // JOBNM,USRPRF,DATIME
                	psHdisInsert.setString(6, getJobnm());
                	psHdisInsert.setString(7, getUsrprf());
                	psHdisInsert.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                	psHdisInsert.setLong(9, c.getUniqueNumber());					
                	psHdisInsert.addBatch();
                }
                psHdisInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertHdisRecords()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psHdisInsert, null);
            }
        }
	}
	
public void insertHdisRecord(List<Hdispf> hdisBulkOpList) {
		
		if (hdisBulkOpList != null && !hdisBulkOpList.isEmpty()) {
	           
            String SQL_HDIS_INSERT = "INSERT INTO HDISPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CNTCURR,"
            		+ " HDV1STDT, HDVLDT, HINTLDT, HINTNDT, HCAPLDT, HCAPNDT, HDVBALL, HDVBALC, HINTOS, HDVSMTDT, HDVSMTNO, HDVBALST, "
            		+ "SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, USRPRF, JOBNM, DATIME) SELECT CHDRCOY, CHDRNUM, LIFE, "
            		+ "JLIFE, COVERAGE, RIDER, PLNSFX, ?, ?, CNTCURR,HDV1STDT, HDVLDT, ?, ?, HCAPLDT, HCAPNDT, HDVBALL, HDVBALC, ?, HDVSMTDT, "
            		+ "HDVSMTNO, HDVBALST,SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, ?, ?, ? FROM "
            		+ "HDISPF WHERE UNIQUE_NUMBER=?";
       
            PreparedStatement psHdisInsert = getPrepareStatement(SQL_HDIS_INSERT);
            try {
                for (Hdispf c : hdisBulkOpList) {
                	psHdisInsert.setString(1, c.getValidflag());
                	psHdisInsert.setInt(2, c.getTranno());
                	psHdisInsert.setInt(3, c.getHintldt());
                	psHdisInsert.setInt(4, c.getHintndt());
                	psHdisInsert.setBigDecimal(5, c.getHintos());
                    // JOBNM,USRPRF,DATIME
                	psHdisInsert.setString(6, getUsrprf()); //ILIFE-9444
                	psHdisInsert.setString(7, getJobnm()); //ILIFE-9444
                	psHdisInsert.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                	psHdisInsert.setLong(9, c.getUniqueNumber());					
                	psHdisInsert.addBatch();
                }
                psHdisInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertHdisRecord()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psHdisInsert, null);
            }
        }
	}
	
	public void insertAllRecords(List<Hdispf> hdispfList){
		
		if (hdispfList != null && !hdispfList.isEmpty()) {

			String sqlInsert = "INSERT INTO HDISPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CNTCURR,"
	        		+ " HDV1STDT, HDVLDT, HINTLDT, HINTNDT, HCAPLDT, HCAPNDT, HDVBALL, HDVBALC, HINTOS, HDVSMTDT, HDVSMTNO, HDVBALST, "
	        		+ "SACSCODE, SACSTYP, HINTDUEM, HINTDUED, HCAPDUEM, HCAPDUED, USRPRF, JOBNM, DATIME)"
					+ "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
			PreparedStatement psHdispfInsert = getPrepareStatement(sqlInsert);/* IJTI-1523 */
	
			try {
				for (Hdispf hdispf : hdispfList) {
						psHdispfInsert.setString(1, hdispf.getChdrcoy());
						psHdispfInsert.setString(2, hdispf.getChdrnum());
						psHdispfInsert.setString(3, hdispf.getLife());
						psHdispfInsert.setString(4, hdispf.getJlife());
						psHdispfInsert.setString(5, hdispf.getCoverage());
						psHdispfInsert.setString(6, hdispf.getRider());
						psHdispfInsert.setInt(7, hdispf.getPlnsfx());
						psHdispfInsert.setString(8, hdispf.getValidflag());
						psHdispfInsert.setInt(9, hdispf.getTranno());
						psHdispfInsert.setString(10, hdispf.getCntcurr());
						psHdispfInsert.setInt(11, hdispf.getHdv1stdt());
						psHdispfInsert.setInt(12, hdispf.getHdvldt());
						psHdispfInsert.setInt(13, hdispf.getHintldt());
						psHdispfInsert.setInt(14, hdispf.getHintndt());
						psHdispfInsert.setInt(15, hdispf.getHcapldt());
						psHdispfInsert.setInt(16, hdispf.getHcapndt());
						psHdispfInsert.setBigDecimal(17, hdispf.getHdvball());
						psHdispfInsert.setBigDecimal(18, hdispf.getHdvbalc());
						psHdispfInsert.setBigDecimal(19, hdispf.getHintos());
						psHdispfInsert.setInt(20, hdispf.getHdvsmtdt());
						psHdispfInsert.setInt(21, hdispf.getHdvsmtno());
						psHdispfInsert.setBigDecimal(22, hdispf.getHdvbalst());
						psHdispfInsert.setString(23, hdispf.getSacscode());
						psHdispfInsert.setString(24, hdispf.getSacstyp());
						psHdispfInsert.setString(25, hdispf.getHintduem());
						psHdispfInsert.setString(26, hdispf.getHintdued());
						psHdispfInsert.setString(27, hdispf.getHcapduem());
						psHdispfInsert.setString(28, hdispf.getHcapdued());
						psHdispfInsert.setString(29, getUsrprf());
						psHdispfInsert.setString(30, getJobnm());
						psHdispfInsert.setTimestamp(31, getDatime());
						psHdispfInsert.addBatch();
				}
				psHdispfInsert.executeBatch();
	
			} catch (SQLException e) {
				LOGGER.error("insertAllRecords()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psHdispfInsert, null);
			}
		}
	}
	public List<Hdispf> searchHdisRecord(Hdispf hdis) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, CNTCURR, ");
		sqlSelect.append("HDV1STDT, HDVLDT, HINTLDT, HINTNDT, HCAPLDT, ");
		sqlSelect.append("HCAPNDT, HDVBALL, HDVBALC, HINTOS, HDVSMTDT, ");
		sqlSelect.append("HDVSMTNO, HDVBALST, SACSCODE, SACSTYP, HINTDUEM, ");
		sqlSelect.append("HINTDUED, HCAPDUEM, HCAPDUED, USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM HDISDRY WHERE CHDRCOY = ? AND CHDRNUM = ? AND PLNSFX = ? ");
		if(hdis.getCoverage() != null && hdis.getRider() != null && hdis.getLife() != null)
		{
			sqlSelect.append("AND COVERAGE = ? AND RIDER = ? AND LIFE = ? ");
		}
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psHdisSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlHdisRs = null;
		List<Hdispf> outputList = new ArrayList<Hdispf>();

		try {

			psHdisSelect.setString(1, hdis.getChdrcoy());
			psHdisSelect.setString(2, hdis.getChdrnum());		
			psHdisSelect.setInt(3, hdis.getPlnsfx());
			if(hdis.getCoverage() != null && hdis.getRider() != null && hdis.getLife() != null)
			{
				psHdisSelect.setString(4, hdis.getCoverage());
				psHdisSelect.setString(5, hdis.getRider());
				psHdisSelect.setString(6, hdis.getLife());
			}

			sqlHdisRs = executeQuery(psHdisSelect);

			while (sqlHdisRs.next()) {

				Hdispf hdisnew = new Hdispf();

				hdisnew.setUniqueNumber(sqlHdisRs.getInt("UNIQUE_NUMBER"));
				hdisnew.setChdrcoy(sqlHdisRs.getString("CHDRCOY"));
				hdisnew.setChdrnum(sqlHdisRs.getString("CHDRNUM"));
				hdisnew.setLife(sqlHdisRs.getString("LIFE"));
				hdisnew.setJlife(sqlHdisRs.getString("JLIFE"));
				hdisnew.setCoverage(sqlHdisRs.getString("COVERAGE"));
				hdisnew.setRider(sqlHdisRs.getString("RIDER"));
				hdisnew.setPlnsfx(sqlHdisRs.getInt("PLNSFX"));
				hdisnew.setValidflag(sqlHdisRs.getString("VALIDFLAG"));				
				hdisnew.setTranno(sqlHdisRs.getInt("TRANNO"));				
				hdisnew.setCntcurr(sqlHdisRs.getString("CNTCURR"));					
				hdisnew.setHdv1stdt(sqlHdisRs.getInt("HDV1STDT"));
				hdisnew.setHdvldt(sqlHdisRs.getInt("HDVLDT"));
				hdisnew.setHintldt(sqlHdisRs.getInt("HINTLDT"));
				hdisnew.setHintndt(sqlHdisRs.getInt("HINTNDT"));
				hdisnew.setHcapldt(sqlHdisRs.getInt("HCAPLDT"));
				hdisnew.setHcapndt(sqlHdisRs.getInt("HCAPNDT"));
				hdisnew.setHdvball(sqlHdisRs.getBigDecimal("HDVBALL"));
				hdisnew.setHdvbalc(sqlHdisRs.getBigDecimal("HDVBALC"));				
				hdisnew.setHintos(sqlHdisRs.getBigDecimal("HINTOS"));				
				hdisnew.setHdvsmtdt(sqlHdisRs.getInt("HDVSMTDT"));				
				hdisnew.setHdvsmtno(sqlHdisRs.getInt("HDVSMTNO"));
				hdisnew.setHdvbalst(sqlHdisRs.getBigDecimal("HDVBALST"));
				hdisnew.setSacscode(sqlHdisRs.getString("SACSCODE"));
				hdisnew.setSacstyp(sqlHdisRs.getString("SACSTYP"));
				hdisnew.setHintduem(sqlHdisRs.getString("HINTDUEM"));
				hdisnew.setHintdued(sqlHdisRs.getString("HINTDUED"));
				hdisnew.setHcapduem(sqlHdisRs.getString("HCAPDUEM"));
				hdisnew.setHcapdued(sqlHdisRs.getString("HCAPDUED"));				
				hdisnew.setUsrprf(sqlHdisRs.getString("USRPRF"));
				hdisnew.setJobnm(sqlHdisRs.getString("JOBNM"));
				hdisnew.setDatime(sqlHdisRs.getDate("DATIME"));

				outputList.add(hdisnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHdisRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHdisSelect, sqlHdisRs);
		}

		return outputList;
	}
	
	@Override
	public void updateHdisRecord(List<Hdispf> hdispfupdateList) {

		 StringBuilder sb = new StringBuilder("");
			sb.append("UPDATE HDISPF SET VALIDFLAG= ?, HDVBALL=?, HDVBALC=? ");
			sb.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? ");
			LOGGER.info(sb.toString());
			PreparedStatement ps = getPrepareStatement(sb.toString());
			try{
				ps = getPrepareStatement(sb.toString());
				for(Hdispf hdispf:hdispfupdateList) {
					 ps.setString(1, hdispf.getValidflag());
					 ps.setBigDecimal(2, hdispf.getHdvball());
					 ps.setBigDecimal(3, hdispf.getHdvbalc());
					 ps.setString(4, hdispf.getChdrcoy());
					 ps.setString(5, hdispf.getChdrnum());
					 ps.setInt(6, hdispf.getTranno());
					 ps.addBatch();
				}
				 ps.executeBatch();				
			}
			catch (SQLException e) {
				LOGGER.error("updateHdisRecord()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
	
		
	}

}
