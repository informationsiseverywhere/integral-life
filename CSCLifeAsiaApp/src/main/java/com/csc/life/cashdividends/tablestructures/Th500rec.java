package com.csc.life.cashdividends.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:38
 * Description:
 * Copybook name: TH500REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th500rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th500Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData payeereq = new FixedLengthStringData(1).isAPartOf(th500Rec, 0);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(th500Rec, 1);
  	public FixedLengthStringData trevsub = new FixedLengthStringData(10).isAPartOf(th500Rec, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(479).isAPartOf(th500Rec, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th500Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th500Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}