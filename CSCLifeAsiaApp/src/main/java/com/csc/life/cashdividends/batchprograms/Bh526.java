/*
 * File: Bh526.java
 * Date: 29 August 2009 21:31:18
 * Author: Quipoz Limited
 *
 * Class transformed from BH526.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap; //ILIFE-9080
import java.util.Iterator; //ILIFE-9080
import java.util.List;
import java.util.Map; //ILIFE-9080

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdpxpfDAO; //ILIFE-9080
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.dataaccess.model.Hdpxpf; //ILIFE-9080
import com.csc.life.cashdividends.recordstructures.Hdvdoprec;
import com.csc.life.cashdividends.tablestructures.Th500rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Varcom; //ILIFE-9080
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.quipoz.COBOLFramework.util.StringUtil;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This program is responsible for performing the processing
*   for the dividend option chosen by the policyholder after
*   each dividend allocation.  The processing is driven by the
*   dividend option held on the extract record from HDPXPF
*   which has been produced by the previous splitter program.
*   Each HDPXPF represents one dividend allocation from a
*   participating coverage, including its associated paid up
*   addition coverage.  The actual processing is done in a
*   subroutine held on TH500 and there is a different subroutine
*   for each dividend option.
*
*
*   PROCESSING
*   ==========
*
*   Initialise
*
*  - Check the restart method is 3.
*  - Find the name of the HDPX temporary file. HDPX is a temporary
*    file which has the name of "HDPX" + first two characters of
*    param 4 + the last four digits of the job number.
*  - Open HDPXPF as input.
*  - Pre-load constantly referenced Tables into working storage.
*    These are: TH500.
*  - Set up the general fields in LIFACMV.
*
*  Read.
*
*  - Read a HDPXPF.
*  - Check end of file and end processing
*
*  Edit.
*
*  - Softlock the contract if this HDPX is for a new contract.
*
*  Update.
*
*  - Calculate the new bonus date by adding one year to its
*      CBUNST
*  - Read HCSD(Cash dividend details)
*  - Read and hold the CHDRLIF(Contract header)
*  - New TRANNO = CHDRLIF-TRANNO + 1
*  - Read COVR(Coverage/Rider Details)
*  - Read TH500 with dividend option , for the processing
*    subroutine.
*  - For each HDIVDOP read, accumulate the dividend amount into
*    working field, until change of key except TRANNO.
*  - Set up the linkage for the new copy book HDVDOPREC to call
*    the dividend option processing subr.
*  - Update all HDIV record to denote processing for the chosen
*    option has been completed.
*  - various file updates for transaction history, in CHDRLIF and
*    PTRN.
*
*  Finalise.
*
*  - Close HDPXPF
*  - Remove the override
*  - Set the parameter statuz to O-K
*
*
*   Control totals:
*     01 - No. of HDPX records read
*     02 - No. of HDPX records locked
*     03 - No. of HDPX records processed
*     04 - Total value of Dividend
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Bh526 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH526");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaHdpxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHdpxFn, 0, FILLER).init("HDPX");
	private FixedLengthStringData wsaaHdpxRunid = new FixedLengthStringData(2).isAPartOf(wsaaHdpxFn, 4);
	private ZonedDecimalData wsaaHdpxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHdpxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaSystemDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSysDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaSystemDate, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaTotDivd = new PackedDecimalData(11, 2).init(0);
		/* WSAA-HDIV-STORE */
	private PackedDecimalData wsaaHdivdopTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaHdivdopEffdate = new PackedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaTh500Array = new FixedLengthStringData(25000);	//ILIFE-2628 fixed--Array size increase
	private FixedLengthStringData[] wsaaTh500Rec = FLSArrayPartOfStructure(1000, 25, wsaaTh500Array, 0);//ILIFE-2628 fixed--Array size increased
	private FixedLengthStringData[] wsaaTh500Key = FLSDArrayPartOfArrayStructure(4, wsaaTh500Rec, 0);
	private FixedLengthStringData[] wsaaTh500Divdopt = FLSDArrayPartOfArrayStructure(4, wsaaTh500Key, 0, SPACES);
	private FixedLengthStringData[] wsaaTh500Data = FLSDArrayPartOfArrayStructure(21, wsaaTh500Rec, 4);
	private FixedLengthStringData[] wsaaTh500Payeereq = FLSDArrayPartOfArrayStructure(1, wsaaTh500Data, 0);
	private FixedLengthStringData[] wsaaTh500Subprog = FLSDArrayPartOfArrayStructure(10, wsaaTh500Data, 1);
	private FixedLengthStringData[] wsaaTh500Trevsub = FLSDArrayPartOfArrayStructure(10, wsaaTh500Data, 11);
	//private int wsaaTh500Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private int wsaaTh500Size = 1000;
		/* TABLES */
	private String th500 = "TH500";
		/* ERRORS */
	private String ivrm = "IVRM";
	private String h791 = "H791";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Header Life Fields*/
//	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Component (Coverage/Rider) Record*/
//	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Dividend Option Processing Trans Details*/
//	private HdivdopTableDAM hdivdopIO = new HdivdopTableDAM();
	private Hdvdoprec hdvdoprec = new Hdvdoprec();
		/*Table items, date - maintenance view*/
//	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
//	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Policy transaction history logical file*/
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Th500rec th500rec = new Th500rec();
	
	private List<Itempf> th500List = new ArrayList<>(); //ILIFE-9080
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private SftlockUtil sftlockUtil = new SftlockUtil();
//	SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
//	private Ptrnpf ptrnpf = new Ptrnpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private List<Ptrnpf> ptrnpfList = new ArrayList<>(); //ILIFE-9080
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private HdivpfDAO hdivpfDao = getApplicationContext().getBean("hdivpfDAO",HdivpfDAO.class);
//	private List<Hdivpf> hdivdopList = new ArrayList<>(); //ILIFE-9080
	private Hdivpf hdivpf = new Hdivpf();
	private String username;
	private int hdivdopCount = 0;
	 //ILIFE-9080 start
	private Iterator<Hdpxpf> iter;
	private int batchID;
	private int batchExtractSize;
	private HdpxpfDAO hdpxpfDAO = getApplicationContext().getBean("hdpxpfDAO", HdpxpfDAO.class);
	private Hdpxpf hdpxpf; 
	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<>();
	private Map<String, List<Covrpf>> covrpfMap = new HashMap<>();
//	private Covrpf covrIO = new Covrpf();
	private List<Chdrpf> chdrpfListUpdt = new ArrayList<>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<>();
	private int ct02Value;
	private int ct03Value;
	private int ct01Value;
	private BigDecimal ct04Value = BigDecimal.ZERO;
 	//ILIFE-9080 end
	private List<Hdivpf> hdivupdateList = new ArrayList<Hdivpf>();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1509,
		exit2090,
		exit2590,
		updateChdr3030
	}

	public Bh526() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		username = (String) ThreadLocalStore.get(ThreadLocalStore.USER);
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
	 //ILIFE-9080 start
		
		wsaaHdpxRunid.set(bprdIO.getSystemParam04());
		wsaaHdpxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
         //ILIFE-9080 end
		wsaaSystemDate.set(getCobolDate());
		th500List=itempfDAO.findItemByTable("IT", bsprIO.getCompany().toString(), th500);
		if (th500List.size() > wsaaTh500Size) {
	           syserrrec.statuz.set(h791);
	           syserrrec.params.set("t6687");
	           fatalError600();
	       }
		loadTh5001500(th500List);

	}

protected void loadTh5001500(List<Itempf> itemList)
{
       int i = 1;
       for (Itempf itempf : itemList) {
           th500rec.th500Rec.set(StringUtil.rawToString(itempf.getGenarea()));
           wsaaTh500Divdopt[i].set(itempf.getItemitem());
   		   wsaaTh500Subprog[i].set(th500rec.subprog);
           i++;
       }
   }
 //ILIFE-9080 start
private void readChunkRecord() {
	ct01Value = 0;
	ct02Value = 0;
	ct03Value = 0;
	ct04Value = BigDecimal.ZERO;
	
	List<Hdpxpf> hdpxpfList = hdpxpfDAO.searchHdpxpfRecord(wsaaHdpxFn.toString(), wsaaThreadMember.toString(),
			batchExtractSize, batchID);
	iter= hdpxpfList.iterator();
	if (!hdpxpfList.isEmpty()) {
	List<String> chdrnumList = new ArrayList<>(hdpxpfList.size());
	for (Hdpxpf p : hdpxpfList) {
		chdrnumList.add(p.getChdrnum());
	}
	String coy = bsprIO.getCompany().toString();
	chdrpfMap = chdrpfDAO.searchChdrpf(coy, chdrnumList);
	covrpfMap = covrpfDAO.searchCovrMap(coy, chdrnumList);
	}
}

protected void readFile2000()
	{
	
	if (!iter.hasNext()) {
		batchID++;
		readChunkRecord();
		if (!iter.hasNext()) {
			wsspEdterror.set(Varcom.endp);
			return;
		}
	}
	 this.hdpxpf = iter.next();
	 ct01Value++;
		
	}
 //ILIFE-9080 end

protected void edit2500()
	{
	 //ILIFE-9080 start
		wsspEdterror.set(SPACES);
		if (isEQ(hdpxpf.getChdrnum(),wsaaALockedChdrnum)) {
			
			ct02Value++;
			goTo(GotoLabel.exit2590);
		}
		if (isNE(hdpxpf.getChdrnum(),lastChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz,"LOCK")) {
				wsaaALockedChdrnum.set(hdpxpf.getChdrnum());
				goTo(GotoLabel.exit2590);
			}
			else {
				wsaaALockedChdrnum.set(SPACES);
			}
			lastChdrnum.set(hdpxpf.getChdrnum());
		}
		wsspEdterror.set(varcom.oK);
		 //ILIFE-9080 end
	}



protected void softlockPolicy2600()
	{
		softlockPolicy2610();
	}

protected void softlockPolicy2610()
	{
		SftlockRecBean sftlockRecBean = new SftlockRecBean();
    	sftlockRecBean.setFunction("LOCK");
    	sftlockRecBean.setCompany(hdpxpf.getChdrcoy()); //ILIFE-9080
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(hdpxpf.getChdrnum()); //ILIFE-9080
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (isEQ(sftlockRecBean.getStatuz(),"LOCK")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(hdpxpf.getChdrcoy()); //ILIFE-9080
			stringVariable1.append('|');
			stringVariable1.append(hdpxpf.getChdrnum()); //ILIFE-9080
			conlogrec.params.setLeft(stringVariable1.toString());
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++; //ILIFE-9080

		}
    	else {
    	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
    		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
    			syserrrec.statuz.set(sftlockRecBean.getStatuz());
    			fatalError600();
    		}
    	}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					update3010();
				}
				case updateChdr3030: {
					updateChdr3030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		
 //ILIFE-9080 start		
		if(chdrpfMap!=null &&chdrpfMap.containsKey(hdpxpf.getChdrnum())){
			for(Chdrpf c:chdrpfMap.get(hdpxpf.getChdrnum())){
				if(c.getChdrcoy().toString().equals(hdpxpf.getChdrcoy())){
					chdrpf = c;
					break;
				}
			}
		}
		if(chdrpf == null){
			syserrrec.params.set(hdpxpf.getChdrnum());
			fatalError600();
		}
		compute(wsaaNewTranno, 0).set(add(chdrpf.getTranno(),1));
		
		Covrpf covrIO = null;
		if (covrpfMap != null && covrpfMap.containsKey(hdpxpf.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(hdpxpf.getChdrnum())) {
				if (c.getLife().equals(hdpxpf.getLife())
						&& c.getCoverage().equals(hdpxpf.getCoverage())
						&& c.getRider().equals(hdpxpf.getRider()) 
						&& c.getPlanSuffix() == hdpxpf.getPlanSuffix()) {
					covrIO = c;
					break;
				}
			}
		}
		if (covrIO == null) {
			syserrrec.params.set(hdpxpf.getChdrnum());
			fatalError600();
		}
	 //ILIFE-9080 end	
		for (ix.set(1); !(isGT(ix,wsaaTh500Size)
		|| isEQ(wsaaTh500Divdopt[ix.toInt()],SPACES)
		|| isEQ(wsaaTh500Divdopt[ix.toInt()],hdpxpf.getZdivopt())); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaTh500Size)
		|| isEQ(wsaaTh500Divdopt[ix.toInt()],SPACES)) {
			th500List=itempfDAO.findItem("IT", bsprIO.getCompany().toString(), th500, hdpxpf.getZdivopt()); //ILIFE-9080
		}
		if (isEQ(wsaaTh500Subprog[ix.toInt()],SPACES)) {
			updateChdr3030(); //ILIFE-9080
		}
		wsaaTotDivd.set(0);
	 //ILIFE-9080 start	
		hdivpf.setChdrcoy(hdpxpf.getChdrcoy());
		hdivpf.setChdrnum(hdpxpf.getChdrnum());
		hdivpf.setLife(hdpxpf.getLife());
		hdivpf.setCoverage(hdpxpf.getCoverage());
		hdivpf.setRider(hdpxpf.getRider());
		hdivpf.setPlnsfx(hdpxpf.getPlanSuffix());
		List<Hdivpf> hdivdopList = hdivpfDao.getHdivdopRecord(hdivpf);
		if(!(hdivdopList.isEmpty())) {
			for(Hdivpf hdiv:hdivdopList) {
				hdivpf = hdiv;
				wsaaTotDivd.add(hdiv.getHdvamt().doubleValue());
				wsaaHdivdopEffdate.set(hdiv.getEffdate());
				wsaaHdivdopTranno.set(hdiv.getTranno());
			}
		}
		
		initialize(hdvdoprec.dividendRec);
		hdvdoprec.chdrChdrcoy.set(hdpxpf.getChdrcoy());
		hdvdoprec.chdrChdrnum.set(hdpxpf.getChdrnum());
		hdvdoprec.lifeLife.set(hdpxpf.getLife());
		hdvdoprec.lifeJlife.set(hdpxpf.getJLife());
		hdvdoprec.covrCoverage.set(hdpxpf.getCoverage());
		hdvdoprec.covrRider.set(hdpxpf.getRider());
		hdvdoprec.plnsfx.set(hdpxpf.getPlanSuffix());
		hdvdoprec.cntcurr.set(chdrpf.getCntcurr());
		hdvdoprec.cnttype.set(chdrpf.getCnttype());
		hdvdoprec.batccoy.set(batcdorrec.company);
		hdvdoprec.batcbrn.set(batcdorrec.branch);
		hdvdoprec.batcactyr.set(batcdorrec.actyear);
		hdvdoprec.batcactmn.set(batcdorrec.actmonth);
		hdvdoprec.batctrcde.set(batcdorrec.trcde);
		hdvdoprec.batcbatch.set(batcdorrec.batch);
		hdvdoprec.hdivTranno.set(wsaaHdivdopTranno);
		hdvdoprec.crtable.set(covrpf.getCrtable());
		hdvdoprec.newTranno.set(wsaaNewTranno);
		hdvdoprec.effectiveDate.set(bsscIO.getEffectiveDate());
		hdvdoprec.hdivEffdate.set(wsaaHdivdopEffdate);
		hdvdoprec.divdOption.set(hdpxpf.getZdivopt());
		hdvdoprec.divdAmount.set(wsaaTotDivd);
		hdvdoprec.language.set(bsscIO.getLanguage());
		hdvdoprec.userName.set(bsscIO.getUserName());
		callProgram(wsaaTh500Subprog[ix.toInt()], hdvdoprec.dividendRec);
		if (isNE(hdvdoprec.statuz,varcom.oK)) {
			syserrrec.params.set(hdvdoprec.dividendRec);
			syserrrec.statuz.set(hdvdoprec.statuz);
			fatalError600();
		}
		
		for(Hdivpf hdivpf : hdivdopList)
		{
			if (isEQ(hdivpf.getChdrcoy(), hdpxpf.getChdrcoy())
				&& isEQ(hdivpf.getChdrnum(), hdpxpf.getChdrnum())
				&& isEQ(hdivpf.getLife(), hdpxpf.getLife())
				&& isEQ(hdivpf.getCoverage(), hdpxpf.getCoverage())
				&& isEQ(hdivpf.getRider(), hdpxpf.getRider())
				&& isEQ(hdivpf.getPlnsfx(), hdpxpf.getPlanSuffix()))
				{
					Hdivpf hdivupdt = new Hdivpf(hdivpf);
					hdivupdt.setHdvopttx(wsaaNewTranno.toInt());
					if(hdivupdateList.isEmpty()) {
						hdivupdateList = new ArrayList<Hdivpf>();
					}
					hdivupdateList.add(hdivupdt);
					//hdivpfDao.updateHdivdopRecord(hdivpf);
				}
		}
 //ILIFE-9080 end
	}

protected void updateChdr3030()
	{
 //ILIFE-9080 start
		Chdrpf pf = new Chdrpf(chdrpf);
		pf.setUniqueNumber(chdrpf.getUniqueNumber());
		pf.setValidflag('2');
		pf.setCurrto(bsscIO.getEffectiveDate().toInt());
		if(chdrpfListUpdt==null){
			chdrpfListUpdt = new ArrayList<>();
		}
		this.chdrpfListUpdt.add(pf);

		Chdrpf chdrpfinst = new Chdrpf(chdrpf);
		chdrpfinst.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrpfinst.setValidflag('1');
		chdrpfinst.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		chdrpfinst.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpfinst.setTranno(wsaaNewTranno.toInt());
		if(chdrpfListInst==null){
			chdrpfListInst = new ArrayList<>();
		}
		this.chdrpfListInst.add(chdrpfinst);
		
		Ptrnpf ptrnpf = new Ptrnpf();
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setChdrcoy(hdpxpf.getChdrcoy());
		ptrnpf.setChdrnum(hdpxpf.getChdrnum());
		ptrnpf.setTranno(wsaaNewTranno.toInt());
		ptrnpf.setValidflag("1");
		ptrnpf.setPtrneff(wsaaHdivdopEffdate.toInt());
		ptrnpf.setTermid(SPACES.stringValue());
		ptrnpf.setTrdt(wsaaSysDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTimen.toInt());
		ptrnpf.setUserT(999999);
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		if(ptrnpfList == null){
			ptrnpfList = new ArrayList<>();
		}
		ptrnpfList.add(ptrnpf);
		
		ct03Value++;
		ct04Value = ct04Value.add(wsaaTotDivd.getbigdata());
		 //ILIFE-9080 end
	}

protected void commit3500()
	{
		/*COMMIT*/
	 	//ILIFE-9080 start
		commitControlTotals();
		
		if (chdrpfListInst != null && !chdrpfListInst.isEmpty()) {
			chdrpfDAO.insertChdrValidRecord(chdrpfListInst);
			chdrpfListInst.clear();
		}
		
		if (chdrpfListUpdt != null && !chdrpfListUpdt.isEmpty()) {
			chdrpfDAO.updateChdrRecordByTranno(chdrpfListUpdt);
			chdrpfListUpdt.clear();
		}
		
		if (ptrnpfList != null && !ptrnpfList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(ptrnpfList);
			ptrnpfList.clear();
		}
		//ILIFE-9080 end
		/*EXIT*/
		if(hdivupdateList != null && !hdivupdateList.isEmpty()) {
			hdivpfDao.bulkUpdateHdivRecords(hdivupdateList);
			hdivupdateList.clear();
		}
	}
 //ILIFE-9080 start
private void commitControlTotals(){ 
	
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Value);
	callContot001();
	
	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Value);
	callContot001();
	
	contotrec.totno.set(ct03);
	contotrec.totval.set(ct03Value);
	callContot001();
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Value);
	callContot001();
	
}
 //ILIFE-9080 end

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
 //ILIFE-9080 start
		if (chdrpfMap != null) {
			chdrpfMap.clear();
		}
		
		if (covrpfMap != null) {
			covrpfMap.clear();
		}
		chdrpfListUpdt.clear();
		chdrpfListUpdt = null;
		
		chdrpfListInst.clear();
		chdrpfListInst = null;
		
		ptrnpfList.clear();
		ptrnpfList=null;
		
 //ILIFE-9080 end
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

}
