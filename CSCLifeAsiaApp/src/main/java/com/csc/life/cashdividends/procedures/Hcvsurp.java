/*
 * File: Hcvsurp.java
 * Date: 29 August 2009 22:52:57
 * Author: Quipoz Limited
 *
 * Class transformed from HCVSURP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivcshTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.HsudTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*         CASH DIVIDEND SURRENDER PROCESSING SUBROUTINE.
*
*   This is called from various programs via T6598.
*
*   This subroutine is used as the processing routine for
*   surrender of a traditional cash dividend contract.  It
*   involves writing offset entries to withdraw the cash dividend
*   and interest, writing accounting entries for each type of
*   withdraw within the surrender, repay existing policy loans,
*   place proceeds to contract suspense and change the contract
*   status.
*
*   This program is simply cloned from PRESPRC with addition
*   cash dividend processing to replace the reversionary bonus
*   processing.  The major modifications made in 3000 section.
*
*   Additional files are:
*   HPUA - Paid-up Addition Coverage Details
*   HCSD - Cash Dividend Details
*   HDIV - Dividend Allocation Transaction Details
*   HDIS - Dividend & Interest Summary
*   HSUD - Surrender Additional Details
*
*   Additional Processing:
*
*   2000-INITIALISE
*   - Read HCSD
*
*   3000-PROCESS-SURD-RECORDS
*   - Read HSUD
*   - Depending on SURDCLM-FIELD-TYPE, process accordingly in 4
*     steps
*          S   - basic cash value
*          P   - paid up addition
*          D   - accumulated dividend
*          I   - O/S interest
*   - After all steps processed, accumulate the surrender values
*     for the SURDCLM by contract currency and payment currency
*
*   END-OF-CONTRACT
*   - If SURHCLM-CURRCD <> SURP-CNTCURR
*     post total surrender value into exchange accounts
*     line 18   LP  CY   payment currency value
*     line 19   LP  CY   contract currency value
*
*
*   Basic Cash Value >>
*   - Invalidate the current HPUAs and write new ones with TRANNO
*     set to SURP-TRANNO, prem and risk set to that on T5679
*   - Post ACMV with line 1 on T5645 (3 for component level acct)
*
*   Paid-up Addition Cash Value >>
*   - Post ACMV with line 2 on T5645 (4 for component level acct)
*
*   Dividend >>
*   - Invalid the current HDIS
*   - Write a new HDIV to denote dividend withdrawal with,
*        DIVD-AMOUNT = HDIS-BAL-SINCE-LAST-CAP * -1
*        DIVD-TYPE   = 'C'
*        INT-CAP-DATE = 99999999
*        EFFDATE, ALLOC-DATE = SURP-EFFDATE
*        TRANNO      = SURDCLM-TRANNO
*        DIVD-RATE   = 0
*   - Write a new HDIV to denote interest withdrawal if <> 0,with
*        DIVD-AMOUNT = HDIS-OS-INTEREST * -1
*        DIVD-TYPE   = 'I'
*   - Write a new HDIS to denote the withdrawal with,
*        TRANNO      = SURP-TRANNO
*        BAL-SINCE-LAST-CAP = 0
*        OS-INTEREST = 0
*   - Post ACMV with line 12 on T5645 where SACSCODE and SACSTYP
*     in HDIS if not blank, should be used.
*
*   Pending Dividend Interest >>
*   - Calculate new interest = HSUD-HACTVAL - ex HDIS-OS-INTERST
*   - transfer existing interest out of interest suspense(LIFACMV)
*       LIFA-ORIGAMT = ex HDIS-OS-INTEREST
*     line 13   LC  IS  ..DVDINTPAY   +   6
*     line 15   LE  IS  ..DVDINTPAY   +   6  component level acct
*   - book new interest since last interest calc date (LIFACMV)
*       LIFA-ORIGAMT = new interest
*     line 16   LC  DI  ..DVDINT      +   7
*     line 17   LE  DI  ..DVDINT      +   7  component level acct
*
*
*   FILE READS.
*   ----------
*
*   Surrender Header file for :
*   - Contract level details.
*   - Adjustment amount entered at time of surrender.
*
*   Surrender Detail file for :
*   - details of individual surrender amounts.
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read T5688 to establish whether contract or component
*     level accounting for this contract type.
*
*   - Read T5645 to get the accounting rules for this
*     subroutine.
*
*   - Read T1688 to get description of the transaction
*     which initiated the surrender.
*
*   - Initial read of SURD file.
*
*   - Read SURH file.
*
*   - Set up ACMV fields which are common to all components on
*     this contract.
*
*   PROCESSING LOOP
*   ---------------
*
*   - Read SURD file sequentially for all surrender records
*     associated with this contract.
*
*   - For each Record :
*
*   - Write an ACMV record.
*     - Fields common to all ACMV's
*
*       LIFA-FUNCTION         - 'PSTW'
*       LIFA-BATCCOY          - Contract Company from linkage
*       LIFA-BATCKEY          - Batch Key details from linkage
*       LIFA-RDOCNUM          - Contract Number from linkage
*       LIFA-TRANNO           - Transaction Number from SURD
*       LIFA-JRNSEQ           - Start from 1, increment by 1
*       LIFA-RLDGCOY          - Contract Company from linkage
*       LIFA-ORIGCURR         - Contract Currency from linkage
*       LIFA-TRANREF          - Contract Number from linkage
*       LIFA-TRANDESC         - Tran. Code description from T1688
*       LIFA-CRATE            - 0
*       LIFA-ACCTAMT          - 0
*       LIFA-GENLCOY          - Contract Company from linkage
*       LIFA-GENLCUR          - Contract Currency from linkage
*       LIFA-POSTYEAR         - Spaces
*       LIFA-POSTMONTH        - Spaces
*       LIFA-EFFDATE          - Effective Surr. Date from linkage
*       LIFA-RCAMT            - 0
*       LIFA-FRCDATE          - Max-date
*       LIFA-TRANSACTION-DATE - Effective Date from linkage
*       LIFA-TRANSACTION-TIME - Time of transaction
*       LIFA-USER             - Transaction User from linkage
*       LIFA-TERMID           - Transaction Term-ID from linkage
*       LIFA-SUBSTITUTE-CODE  - 1 - Contract Type
*                               2 - 5 not set
*     - Fields varying :
*
*       LIFA-SACSCODE         - Account Code from T5645
*       LIFA-RLDGACCT         - If Component Level Accounting :
*                                 Full component Key
*                               If Contract Level Accounting :
*                                 Contract Number
*       LIFA-SACSTYP          - Account Type from T5645
*       LIFA-ORIGAMT          - Amount of posting from SURD rec.
*       LIFA-GLCODE           - General Ledger Code from T5645
*       LIFA-GLSIGN           - General Ledger Sign from T5645
*       LIFA-CONTOT           - Control Total from T5645
*       LIFA-SUBSTITUTE-CODE  - 6 - Component Type
*
*   - Accounting Rules from T5645
*
*     Line 1  : Cash Surrender Value for contract level
*     Line 2  : Paid up Cash Surrender Value for contract level
*     Line 3  : Cash Surrender Value for component level
*     Line 4  : Paid up Cash Surrender Value for component level
*     Line 5  : Adjustment per contract from SURH record
*     Line 6  : Payment Suspense
*     Line 7  :
*     Line 8  :  Entries 8 - 11 are the prioritised
*     Line 9  :  list of CODE/TYPE combinations for use
*     Line 10 :  with loan processing. ie. if entry 8 is
*     Line 11 :  for Policy Loan Principal then for any loans
*                which exist, these are the first accounts
*                the program will try to clear, then 9 etc.
*     Line 12 : Dividend Suspense
*     Line 13 : Interest Suspense at contract level
*     Line 14 :
*     Line 15 : Interest Suspense at compenent level
*     Line 16 : Dividend Interest at contract level
*     Line 17 : Dividend Interest at component level
*     Line 18 : Currency Ex-change
*     Line 19 : Currency Ex-change
*
*   - Write ACMV for contract adjustment.
*
*   - Call LOANPYMT using the P2068REC Linkage area for the
*     T5645 entries in the order that they are listed. Each
*     time check to see whether any outstanding surrender
*     remains unallocated.
*
*   - Write balancing ACMV for the total.
*
*   SET EXIT VARIABLES.
*   ------------------
*   set linkage variables as follows :
*
*   To indicate that processing complete :
*
*   - STATUS -        'ENDP'
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Hcvsurp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("HCVSURP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* ERRORS */
	private String e044 = "E044";
	private String e308 = "E308";
	private String f321 = "F321";
	private String h134 = "H134";
	private String h150 = "H150";
	private String surdclmrec = "SURDCLMREC";
	private String surhclmrec = "SURHCLMREC";
	private String hpuarec = "HPUAREC";
	private String hcsdrec = "HCSDREC";
	private String hdisrec = "HDISREC";
	private String hdivrec = "HDIVREC";
	private String hsudrec = "HSUDREC";
	private String covtmjarec = "COVTMJAREC";
	private String covrmjarec = "COVRMJAREC";
	private String payrrec = "PAYRREC";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5679 = "T5679";
	private String t5688 = "T5688";
		/* WSAA-CALC-VARIABLES */
	private PackedDecimalData wsaaNewInt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaProportion = new PackedDecimalData(8, 7);
	private PackedDecimalData wsaaOsInterest = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsaaT5645 = new FixedLengthStringData(84);
	private FixedLengthStringData[] wsaaStoredT5645 = FLSArrayPartOfStructure(4, 21, wsaaT5645, 0);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	private FixedLengthStringData wsbbT5645 = new FixedLengthStringData(630);
	private FixedLengthStringData[] wsbbStoredT5645 = FLSArrayPartOfStructure(30, 21, wsbbT5645, 0);
	private ZonedDecimalData[] wsbbT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsbbStoredT5645, 0);
	private FixedLengthStringData[] wsbbT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsbbStoredT5645, 2);
	private FixedLengthStringData[] wsbbT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsbbStoredT5645, 16);
	private FixedLengthStringData[] wsbbT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsbbStoredT5645, 18);
	private FixedLengthStringData[] wsbbT5645Sign = FLSDArrayPartOfArrayStructure(1, wsbbStoredT5645, 20);

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private PackedDecimalData wsaaAccumValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAccumHvalue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");

	private FixedLengthStringData wsaaEndOfContFlag = new FixedLengthStringData(1).init("N");
	private Validator endOfContract = new Validator(wsaaEndOfContFlag, "Y");
	private Cashedrec cashedrec = new Cashedrec();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Allocation Trans Details Logica*/
	private HdivTableDAM hdivIO = new HdivTableDAM();
		/*Paid Up Addition Coverage Details Logica*/
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
		/*Surrender Additional Details*/
	private HsudTableDAM hsudIO = new HsudTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Surrender Claims Detail Record*/
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
		/*Full Surrender header record*/
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Surpcpy surpcpy = new Surpcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Totloanrec totloanrec = new Totloanrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
	private PackedDecimalData wsaaNextDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCurrOsInt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHdisOsInt = new PackedDecimalData(17, 2);
	private Itempf itempf = null;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ZonedDecimalData wsaaSub3 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub4 = new ZonedDecimalData(2, 0).setUnsigned();
	private Th501rec th501rec = new Th501rec();
	private String hdivcshrec = "HDIVCSHREC";
	private String th501 = "TH501";
	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	private boolean cdivFlag = false;
	private boolean hdisavailFlag = false;
	private boolean susur013Permission = false;
	private static final String SUSUR013 = "SUSUR013";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		endPart1010,
		seExit9090,
		dbExit9190
	}

	public Hcvsurp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1000();
				}
				case endPart1010: {
					endPart1010();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1000()
	{
		initialise2000();
		wsaaProportion.set(1);
		if (isEQ(surpcpy.type,"P")) {
			covtmjaIO.setDataArea(SPACES);
			covtmjaIO.setChdrcoy(surpcpy.chdrcoy);
			covtmjaIO.setChdrnum(surpcpy.chdrnum);
			covtmjaIO.setLife(surpcpy.life);
			covtmjaIO.setCoverage(surpcpy.coverage);
			covtmjaIO.setRider(surpcpy.rider);
			covtmjaIO.setPlanSuffix(surpcpy.planSuffix);
			covtmjaIO.setFormat(covtmjarec);
			covtmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covtmjaIO);
			if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
				goTo(GotoLabel.endPart1010);
			}
			covrmjaIO.setDataArea(SPACES);
			covrmjaIO.setChdrcoy(surpcpy.chdrcoy);
			covrmjaIO.setChdrnum(surpcpy.chdrnum);
			covrmjaIO.setLife(surpcpy.life);
			covrmjaIO.setCoverage(surpcpy.coverage);
			covrmjaIO.setRider(surpcpy.rider);
			covrmjaIO.setPlanSuffix(surpcpy.planSuffix);
			covrmjaIO.setFormat(covrmjarec);
			covrmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				systemError9000();
			}
			compute(wsaaProportion, 8).setRounded(div((sub(covrmjaIO.getSumins(),covtmjaIO.getSumins())),covrmjaIO.getSumins()));
			payrIO.setDataArea(SPACES);
			payrIO.setChdrcoy(surpcpy.chdrcoy);
			payrIO.setChdrnum(surpcpy.chdrnum);
			payrIO.setPayrseqno(1);
			payrIO.setFormat(payrrec);
			payrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				syserrrec.statuz.set(payrIO.getStatuz());
				systemError9000();
			}
		}
	}

protected void endPart1010()
	{
		wsaaInt.set(ZERO);
		while ( !(endOfContract.isTrue())) {
			processSurdRecords3000();
		}

		if (isNE(surhclmIO.getCurrcd(),hsudIO.getHcnstcur())) {
			lifacmvrec1.origcurr.set(hsudIO.getHcnstcur());
			lifacmvrec1.origamt.set(wsaaAccumHvalue);
			lifacmvrec1.sacscode.set(wsbbT5645Sacscode[18]);
			lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[18]);
			lifacmvrec1.glcode.set(wsbbT5645Glmap[18]);
			lifacmvrec1.glsign.set(wsbbT5645Sign[18]);
			lifacmvrec1.contot.set(wsbbT5645Cnttot[18]);
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
			lifacmvrec1.origcurr.set(surhclmIO.getCurrcd());
			lifacmvrec1.origamt.set(wsaaAccumValue);
			lifacmvrec1.sacscode.set(wsbbT5645Sacscode[19]);
			lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[19]);
			lifacmvrec1.glcode.set(wsbbT5645Glmap[19]);
			lifacmvrec1.glsign.set(wsbbT5645Sign[19]);
			lifacmvrec1.contot.set(wsbbT5645Cnttot[19]);
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
		}
		endOfContract4000();
		exitProgram();
	}

protected void initialise2000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaProg);
		varcom.vrcmTime.set(getCobolTime());
		wsaaEndOfContFlag.set("N");
		wsaaAccumValue.set(ZERO);
		wsaaSequenceNo.set(ZERO);
		cdivFlag = FeaConfg.isFeatureExist("2", "BTPRO015", appVars, "IT");
		susur013Permission = FeaConfg.isFeatureExist("2",SUSUR013, appVars, "IT");
		readTables2100();
		initialSurdRead2200();
		readSurhRecord2300();
		initialAcmvFields2400();
		readHcsd2500();
		/*EXIT*/
	}

protected void readTables2100()
	{
		accountingLevel2110();
		accountingRules2120();
		transactionCodeDesc2130();
		validStatii2140();
	}

protected void accountingLevel2110()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(surpcpy.cnttype);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),surpcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surpcpy.cnttype);
			syserrrec.statuz.set(e308);
			systemError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaAcctLevel.set(t5688rec.comlvlacc);
	}

protected void accountingRules2120()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaProg);
			syserrrec.statuz.set(h134);
			systemError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(8); !(isGT(wsaaSub1,11)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(sub(wsaaSub1,7));
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			wsbbT5645Cnttot[wsaaSub1.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsbbT5645Glmap[wsaaSub1.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsbbT5645Sacscode[wsaaSub1.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsbbT5645Sacstype[wsaaSub1.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsbbT5645Sign[wsaaSub1.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq("01");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaProg);
			syserrrec.statuz.set(h134);
			systemError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			wsbbT5645Cnttot[add(wsaaSub1,15).toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsbbT5645Glmap[add(wsaaSub1,15).toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsbbT5645Sacscode[add(wsaaSub1,15).toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsbbT5645Sacstype[add(wsaaSub1,15).toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsbbT5645Sign[add(wsaaSub1,15).toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
	}

protected void transactionCodeDesc2130()
	{
		wsaaBatckey.set(surpcpy.batckey);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(surpcpy.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(surpcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set(e044);
			systemError9000();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}

protected void validStatii2140()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaProg);
			syserrrec.statuz.set(f321);
			systemError9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void initialSurdRead2200()
	{
		start2200();
	}

protected void start2200()
	{
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(surpcpy.chdrcoy);
		surdclmIO.setChdrnum(surpcpy.chdrnum);
		surdclmIO.setTranno(surpcpy.tranno);
		surdclmIO.setPlanSuffix(surpcpy.planSuffix);
		surdclmIO.setLife(surpcpy.life);
		surdclmIO.setCoverage(surpcpy.coverage);
		surdclmIO.setRider(surpcpy.rider);
		surdclmIO.setCrtable(surpcpy.crtable);
		surdclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		surdclmIO.setFormat(surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(surdclmIO.getStatuz());
			dbError9100();
		}
		if (isNE(surdclmIO.getChdrcoy(),surpcpy.chdrcoy)
		|| isNE(surdclmIO.getChdrnum(),surpcpy.chdrnum)
		|| isEQ(surdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surpcpy.chdrnum);
			syserrrec.statuz.set(h150);
			dbError9100();
		}
	}

protected void readSurhRecord2300()
	{
		start2300();
	}

protected void start2300()
	{
		surhclmIO.setParams(SPACES);
		surhclmIO.setChdrcoy(surdclmIO.getChdrcoy());
		surhclmIO.setChdrnum(surdclmIO.getChdrnum());
		surhclmIO.setTranno(surdclmIO.getTranno());
		surhclmIO.setPlanSuffix(surdclmIO.getPlanSuffix());
		surhclmIO.setFunction(varcom.readr);
		surhclmIO.setFormat(surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			syserrrec.statuz.set(surhclmIO.getStatuz());
			dbError9100();
		}
	}

protected void initialAcmvFields2400()
	{
		start2400();
	}

protected void start2400()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batccoy.set(surpcpy.chdrcoy);
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.rdocnum.set(surpcpy.chdrnum);
		lifacmvrec1.rldgcoy.set(surpcpy.chdrcoy);
		lifacmvrec1.tranref.set(surpcpy.chdrnum);
		lifacmvrec1.trandesc.set(wsaaLongdesc);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.genlcoy.set(surpcpy.chdrcoy);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.effdate.set(surpcpy.effdate);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(surpcpy.effdate);
		lifacmvrec1.transactionTime.set(varcom.vrcmTime);
		lifacmvrec1.user.set(surpcpy.user);
		lifacmvrec1.termid.set(surpcpy.termid);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.substituteCode[1].set(surpcpy.cnttype);
		lifacmvrec1.rldgacct.set(SPACES);
		if (componentLevelAccounting.isTrue()) {
			wsaaRldgChdrnum.set(surpcpy.chdrnum);
			wsaaPlan.set(surdclmIO.getPlanSuffix());
			wsaaRldgLife.set(surdclmIO.getLife());
			wsaaRldgCoverage.set(surdclmIO.getCoverage());
			wsaaRldgRider.set(surdclmIO.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		}
		lifacmvrec1.substituteCode[6].set(surpcpy.crtable);
	}

protected void readHcsd2500()
	{
		start2500();
	}

protected void start2500()
	{
		hcsdIO.setParams(SPACES);
		hcsdIO.setChdrcoy(surdclmIO.getChdrcoy());
		hcsdIO.setChdrnum(surdclmIO.getChdrnum());
		hcsdIO.setLife(surdclmIO.getLife());
		hcsdIO.setCoverage(surdclmIO.getCoverage());
		hcsdIO.setRider(surdclmIO.getRider());
		hcsdIO.setPlanSuffix(surdclmIO.getPlanSuffix());
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			dbError9100();
		}
	}

protected void processSurdRecords3000()
	{
		start3000();
		basicCashValue3200();
		paidupCashValue3300();
		dividend3400();
		dividendInterest3500();
		accumulateSurValue3600();
	}

protected void start3000()
	{
		hsudIO.setParams(SPACES);
		hsudIO.setChdrcoy(surdclmIO.getChdrcoy());
		hsudIO.setChdrnum(surdclmIO.getChdrnum());
		hsudIO.setLife(surdclmIO.getLife());
		hsudIO.setCoverage(surdclmIO.getCoverage());
		hsudIO.setRider(surdclmIO.getRider());
		hsudIO.setPlanSuffix(surdclmIO.getPlanSuffix());
		hsudIO.setTranno(surdclmIO.getTranno());
		hsudIO.setFieldType(surdclmIO.getFieldType());
		hsudIO.setFormat(hsudrec);
		hsudIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hsudIO);
		if (isNE(hsudIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hsudIO.getParams());
			syserrrec.statuz.set(hsudIO.getStatuz());
			dbError9100();
		}
		lifacmvrec1.origcurr.set(hsudIO.getHcnstcur());
		lifacmvrec1.origamt.set(hsudIO.getHactval());
		lifacmvrec1.tranno.set(surdclmIO.getTranno());
	}

protected void basicCashValue3200()
	{
		if (isEQ(surdclmIO.getFieldType(),"S")) {
			hpuaIO.setParams(SPACES);
			hpuaIO.setChdrcoy(surdclmIO.getChdrcoy());
			hpuaIO.setChdrnum(surdclmIO.getChdrnum());
			hpuaIO.setLife(surdclmIO.getLife());
			hpuaIO.setCoverage(surdclmIO.getCoverage());
			hpuaIO.setRider(surdclmIO.getRider());
			hpuaIO.setPlanSuffix(surdclmIO.getPlanSuffix());
			hpuaIO.setPuAddNbr(0);
			hpuaIO.setFormat(hpuarec);
			hpuaIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			hpuaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)
			&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hpuaIO.getParams());
				syserrrec.statuz.set(hpuaIO.getStatuz());
				dbError9100();
			}
			if (isNE(hpuaIO.getChdrcoy(),surdclmIO.getChdrcoy())
			|| isNE(hpuaIO.getChdrnum(),surdclmIO.getChdrnum())
			|| isNE(hpuaIO.getLife(),surdclmIO.getLife())
			|| isNE(hpuaIO.getCoverage(),surdclmIO.getCoverage())
			|| isNE(hpuaIO.getRider(),surdclmIO.getRider())
			|| isNE(hpuaIO.getPlanSuffix(),surdclmIO.getPlanSuffix())) {
				hpuaIO.setStatuz(varcom.endp);
			}
			while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
				hpuaIO.setValidflag("2");
				hpuaIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9100();
				}
				hpuaIO.setValidflag("1");
				hpuaIO.setTranno(surpcpy.tranno);
				hpuaIO.setRstatcode(t5679rec.setCovRiskStat);
				hpuaIO.setPstatcode(t5679rec.setCovPremStat);
				hpuaIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9100();
				}
				setPrecision(hpuaIO.getPuAddNbr(), 0);
				hpuaIO.setPuAddNbr(add(hpuaIO.getPuAddNbr(),1));
				hpuaIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, hpuaIO);
				if (isNE(hpuaIO.getStatuz(),varcom.oK)
				&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(hpuaIO.getParams());
					syserrrec.statuz.set(hpuaIO.getStatuz());
					dbError9100();
				}
			}

			if (componentLevelAccounting.isTrue()) {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[3]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[3]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[3]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[3]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[3]);
			}
			else {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[1]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[1]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[1]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[1]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[1]);
			}
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
		}
	}

protected void paidupCashValue3300()
	{
		if (isEQ(surdclmIO.getFieldType(),"P")) {
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[4]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[4]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[4]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[4]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[4]);
			}
			else {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[2]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[2]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[2]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[2]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[2]);
			}
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
		}
	}

protected void dividend3400()
	{
		if (isEQ(surdclmIO.getFieldType(),"D")) {
		
			hdisIO.setParams(SPACES);
			hdisIO.setChdrcoy(surdclmIO.getChdrcoy());
			hdisIO.setChdrnum(surdclmIO.getChdrnum());
			hdisIO.setLife(surdclmIO.getLife());
			hdisIO.setCoverage(surdclmIO.getCoverage());
			hdisIO.setRider(surdclmIO.getRider());
			hdisIO.setPlanSuffix(surdclmIO.getPlanSuffix());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hdisIO);
			if(cdivFlag){
				if(isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
						wsaaTotDividend.set(0);
						compute(wsaaNextDividend, 2).set(sub(hsudIO.getHactval() ,wsaaTotDividend));
						lifacmvrec1.origamt.set(wsaaTotDividend);
						hsudIO.setHactval(wsaaTotDividend);
						lifacmvrec1.origamt.set(wsaaTotDividend);
						callacmv();
						return;
				}
				else {
					if (isNE(hdisIO.getStatuz(),varcom.oK)) {
						syserrrec.params.set(hdisIO.getParams());
						syserrrec.statuz.set(hdisIO.getStatuz());
						dbError9100();
					}
					else{
						calcPreviousDiv();
						compute(wsaaNextDividend, 2).set(sub(hsudIO.getHactval() ,wsaaTotDividend));
						lifacmvrec1.origamt.set(wsaaTotDividend);
						hsudIO.setHactval(wsaaTotDividend);	
					}
				}
			}
			else{
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				dbError9100();
			}
			}
			hdisIO.setValidflag("2");
			hdisIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				dbError9100();
			}
			hdivIO.setParams(SPACES);
			hdivIO.setChdrcoy(surdclmIO.getChdrcoy());
			hdivIO.setChdrnum(surdclmIO.getChdrnum());
			hdivIO.setLife(surdclmIO.getLife());
			hdivIO.setCoverage(surdclmIO.getCoverage());
			hdivIO.setRider(surdclmIO.getRider());
			hdivIO.setPlanSuffix(surdclmIO.getPlanSuffix());
			hdivIO.setTranno(surdclmIO.getTranno());
			hdivIO.setEffdate(surpcpy.effdate);
			hdivIO.setDivdAllocDate(surpcpy.effdate);
			hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
			hdivIO.setCntcurr(hdisIO.getCntcurr());
			setPrecision(hdivIO.getDivdAmount(), 2);
			hdivIO.setDivdAmount(mult(hsudIO.getHactval(),-1));
			hdivIO.setDivdRate(ZERO);
			hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
			hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
			hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			hdivIO.setDivdType("C");
			hdivIO.setZdivopt(hcsdIO.getZdivopt());
			hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
			hdivIO.setDivdOptprocTranno(ZERO);
			hdivIO.setDivdCapTranno(ZERO);
			hdivIO.setDivdStmtNo(ZERO);
			hdivIO.setPuAddNbr(ZERO);
			hdivIO.setFormat(hdivrec);
			hdivIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hdivIO);
			if (isNE(hdivIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdivIO.getParams());
				syserrrec.statuz.set(hdivIO.getStatuz());
				dbError9100();
			}
			hdisIO.setValidflag("1");
			hdisIO.setTranno(surpcpy.tranno);
			if (isNE(surpcpy.type,"P")) {
				hdisIO.setOsInterest(ZERO);
			}
			else {
				if (isGTE(surpcpy.effdate,payrIO.getPtdate())) {
					setPrecision(hdisIO.getOsInterest(), 2);
					hdisIO.setOsInterest(add(sub(hdisIO.getOsInterest(),wsaaInt),wsaaNewInt));
				}
				else {
					setPrecision(hdisIO.getOsInterest(), 2);
					hdisIO.setOsInterest(sub(hdisIO.getOsInterest(),wsaaInt));
				}
			}
			hdisIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				dbError9100();
			}
			lifacmvrec1.sacscode.set(wsbbT5645Sacscode[12]);
			lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[12]);
			lifacmvrec1.glcode.set(wsbbT5645Glmap[12]);
			lifacmvrec1.glsign.set(wsbbT5645Sign[12]);
			lifacmvrec1.contot.set(wsbbT5645Cnttot[12]);
			if (isNE(hdisIO.getSacscode(),SPACES)
			&& isNE(hdisIO.getSacstyp(),SPACES)) {
				lifacmvrec1.sacscode.set(hdisIO.getSacscode());
				lifacmvrec1.sacstyp.set(hdisIO.getSacstyp());
			}
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
			if(cdivFlag){
				callacmv();
			}
		}
	}

protected void callacmv(){
	if(isNE(wsaaNextDividend,ZERO)){
		wsaaSub3.set(21);
		wsaaSub4.set(4);
		lifacmvrec1.origamt.set(wsaaNextDividend);
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		setupLifeacmv();
	}
}

protected void setupLifeacmv(){
	
	for (wsaaSub1.set(1); !(isGTE(wsaaSub1,wsaaSub4)) ; wsaaSub1.add(1)){
		lifacmvrec1.sacscode.set(wsbbT5645Sacscode[wsaaSub3.toInt()]);
		lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[wsaaSub3.toInt()]);
		lifacmvrec1.glcode.set(wsbbT5645Glmap[wsaaSub3.toInt()]);
		lifacmvrec1.glsign.set(wsbbT5645Sign[wsaaSub3.toInt()]);
		lifacmvrec1.contot.set(wsbbT5645Cnttot[wsaaSub3.toInt()]);	
		if (isNE(lifacmvrec1.origamt, 0)) {
			postAcmvRecord5000();
		}
		wsaaSub3.add(1);
	}
}

protected void calcPreviousDiv(){
	
	wsaaTotDividend.set(hdisIO.getBalSinceLastCap());
	hdivcshIO.setDataArea(SPACES);
	hdivcshIO.setChdrcoy(surdclmIO.getChdrcoy());
	hdivcshIO.setChdrnum(surdclmIO.getChdrnum());
	hdivcshIO.setLife(surdclmIO.getLife());
	hdivcshIO.setCoverage(surdclmIO.getCoverage());
	hdivcshIO.setRider(surdclmIO.getRider());
	hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
	compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
	hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
	hdivcshIO.setFormat(hdivcshrec);
	hdivcshIO.setFunction(varcom.begn);

	//performance improvement -- Anjali
	hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

	SmartFileCode.execute(appVars, hdivcshIO);
	if (isNE(hdivcshIO.getStatuz(),varcom.oK)
	&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	if (isNE(hdivcshIO.getChdrcoy(),surdclmIO.getChdrcoy())
	|| isNE(hdivcshIO.getChdrnum(),surdclmIO.getChdrnum())
	|| isNE(hdivcshIO.getLife(),surdclmIO.getLife())
	|| isNE(hdivcshIO.getCoverage(),surdclmIO.getCoverage())
	|| isNE(hdivcshIO.getRider(),surdclmIO.getRider())
	|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
		wsaaTotDividend.add(hdivcshIO.getDivdAmount());
		hdivcshIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		if (isNE(hdivcshIO.getChdrcoy(),surdclmIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(),surdclmIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(),surdclmIO.getLife())
		|| isNE(hdivcshIO.getCoverage(),surdclmIO.getCoverage())
		|| isNE(hdivcshIO.getRider(),surdclmIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
	}
}

protected void dividendInterest3500()
	{
		if (isEQ(surdclmIO.getFieldType(),"I")) {
			if(cdivFlag){
				calcInterest();
				compute(wsaaCurrOsInt, 2).set(sub(hsudIO.getHactval(),wsaaHdisOsInt));
				compute(wsaaCurrOsInt, 2).set(sub(wsaaCurrOsInt,wsaaNewInt));
				hsudIO.setHactval(sub(hsudIO.getHactval(),wsaaCurrOsInt));
				compute(wsaaOsInterest, 7).set(mult(wsaaHdisOsInt,wsaaProportion));
				if(hdisavailFlag){
					if(isNE(wsaaCurrOsInt,ZERO)){
						wsaaSub3.set(24);
						wsaaSub4.set(5);
						lifacmvrec1.origamt.set(wsaaCurrOsInt);
						setupLifeacmv();
						return;
					}
				 return;
				}
			}
			wsaaInt.set(hsudIO.getHactval());
			hdisIO.setParams(SPACES);
			hdisIO.setChdrcoy(surdclmIO.getChdrcoy());
			hdisIO.setChdrnum(surdclmIO.getChdrnum());
			hdisIO.setLife(surdclmIO.getLife());
			hdisIO.setCoverage(surdclmIO.getCoverage());
			hdisIO.setRider(surdclmIO.getRider());
			hdisIO.setPlanSuffix(surdclmIO.getPlanSuffix());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdisIO.getParams());
				syserrrec.statuz.set(hdisIO.getStatuz());
				dbError9100();
			}
			if(cdivFlag && isNE(wsaaOsInterest, ZERO)){
				compute(wsaaCurrOsInt, 2).set(sub(hsudIO.getHactval(),wsaaCurrOsInt));
				compute(wsaaNewInt, 2).set(sub(hsudIO.getHactval(),(sub(wsaaOsInterest,wsaaCurrOsInt))));
			}
			else{
			compute(wsaaOsInterest, 7).set(mult(hdisIO.getOsInterest(),wsaaProportion));
			compute(wsaaNewInt, 2).set(sub(hsudIO.getHactval(),wsaaOsInterest));
			}
			if (isNE(hsudIO.getHactval(),ZERO)) {
				hdivIO.setParams(SPACES);
				hdivIO.setChdrcoy(surdclmIO.getChdrcoy());
				hdivIO.setChdrnum(surdclmIO.getChdrnum());
				hdivIO.setLife(surdclmIO.getLife());
				hdivIO.setCoverage(surdclmIO.getCoverage());
				hdivIO.setRider(surdclmIO.getRider());
				hdivIO.setPlanSuffix(surdclmIO.getPlanSuffix());
				hdivIO.setTranno(surdclmIO.getTranno());
				hdivIO.setEffdate(surpcpy.effdate);
				hdivIO.setDivdAllocDate(surpcpy.effdate);
				hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
				hdivIO.setCntcurr(hdisIO.getCntcurr());
				setPrecision(hdivIO.getDivdAmount(), 2);
				hdivIO.setDivdAmount(mult(hsudIO.getHactval(),-1));
				hdivIO.setDivdRate(ZERO);
				hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
				hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
				hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
				hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
				hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
				hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
				hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
				hdivIO.setDivdType("I");
				hdivIO.setZdivopt(hcsdIO.getZdivopt());
				hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
				hdivIO.setDivdOptprocTranno(ZERO);
				hdivIO.setDivdCapTranno(ZERO);
				hdivIO.setDivdStmtNo(ZERO);
				hdivIO.setPuAddNbr(ZERO);
				hdivIO.setFormat(hdivrec);
				hdivIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivIO);
				if (isNE(hdivIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivIO.getParams());
					syserrrec.statuz.set(hdivIO.getStatuz());
					dbError9100();
				}
			}
			if (isNE(wsaaNewInt,ZERO)) {
				hdivIO.setDivdType("I");
				hdivIO.setDivdAmount(wsaaNewInt);
				hdivIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivIO);
				if (isNE(hdivIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivIO.getParams());
					syserrrec.statuz.set(hdivIO.getStatuz());
					dbError9100();
				}
			}
			lifacmvrec1.origamt.set(hsudIO.getHactval());
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[15]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[15]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[15]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[15]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[15]);
			}
			else {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[13]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[13]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[13]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[13]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[13]);
			}
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
			lifacmvrec1.origamt.set(wsaaNewInt);
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[17]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[17]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[17]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[17]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[17]);
			}
			else {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[16]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[16]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[16]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[16]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[16]);
			}
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
			compute(lifacmvrec1.origamt, 2).set(mult(wsaaNewInt,-1));
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[15]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[15]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[15]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[15]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[15]);
			}
			else {
				lifacmvrec1.sacscode.set(wsbbT5645Sacscode[13]);
				lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[13]);
				lifacmvrec1.glcode.set(wsbbT5645Glmap[13]);
				lifacmvrec1.glsign.set(wsbbT5645Sign[13]);
				lifacmvrec1.contot.set(wsbbT5645Cnttot[13]);
			}
			if (isNE(lifacmvrec1.origamt,0)) {
				postAcmvRecord5000();
			}
			if(cdivFlag){
				if(isNE(wsaaCurrOsInt,ZERO)){
					wsaaSub3.set(24);
					wsaaSub4.set(5);
					lifacmvrec1.origamt.set(wsaaCurrOsInt);
					setupLifeacmv();
				}
			}
		}
	}

protected void calcInterest(){
	
	hdisIO.setParams(SPACES);
	hdisIO.setChdrcoy(surdclmIO.getChdrcoy());
	hdisIO.setChdrnum(surdclmIO.getChdrnum());
	hdisIO.setLife(surdclmIO.getLife());
	hdisIO.setCoverage(surdclmIO.getCoverage());
	hdisIO.setRider(surdclmIO.getRider());
	hdisIO.setPlanSuffix(0);
	hdisIO.setFormat(hdisrec);
	hdisIO.setFunction(varcom.readh);
	SmartFileCode.execute(appVars, hdisIO);
	if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
		hdisavailFlag = true;
		wsaaHdisOsInt.set(0);
		wsaaTotDividend.set(0);
		wsaaNewInt.set(0);
		return;
	}
	else{
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			systemError9000();
		}
	}
	
	wsaaHdisOsInt.set(hdisIO.getOsInterest());
	chdrpf = chdrpfDAO.getchdrRecordData(surdclmIO.getChdrcoy().toString(), surdclmIO.getChdrnum().toString());
	itempf = itempfDAO.readItdmpf("IT", surdclmIO.getChdrcoy().toString(),th501, surpcpy.effdate.toInt(),StringUtils.rightPad(hcsdIO.getZcshdivmth().toString(), 8));
	if(itempf != null){
		th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else{
		syserrrec.params.set("IT".concat(surdclmIO.getChdrcoy().toString()).concat(th501).concat(hcsdIO.getZcshdivmth().toString()));
		systemError9000();
	}
	initialize(hdvdintrec.divdIntRec);
	hdvdintrec.chdrChdrcoy.set(surdclmIO.getChdrcoy());
	hdvdintrec.chdrChdrnum.set(surdclmIO.getChdrnum());
	hdvdintrec.lifeLife.set(surdclmIO.getLife());
	hdvdintrec.covrCoverage.set(surdclmIO.getCoverage());
	hdvdintrec.covrRider.set(surdclmIO.getRider());
	hdvdintrec.plnsfx.set(ZERO);
	hdvdintrec.cntcurr.set(surdclmIO.getCurrcd());
	hdvdintrec.transcd.set(SPACES);
	hdvdintrec.crtable.set(surdclmIO.getCrtable());
	hdvdintrec.effectiveDate.set(surpcpy.effdate);
	hdvdintrec.tranno.set(ZERO);
	hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
	hdvdintrec.intTo.set(chdrpf.getPtdate());
	hdvdintrec.capAmount.set(hdisIO.getBalSinceLastCap());
	hdvdintrec.intDuration.set(ZERO);
	hdvdintrec.intAmount.set(ZERO);
	hdvdintrec.intRate.set(ZERO);
	hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
	if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(hdvdintrec.statuz);
			systemError9000();
		}
		wsaaNewInt.add(hdvdintrec.intAmount);
	}
	hdivcshIO.setDataArea(SPACES);
	hdivcshIO.setChdrcoy(hdisIO.getChdrcoy());
	hdivcshIO.setChdrnum(hdisIO.getChdrnum());
	hdivcshIO.setLife(hdisIO.getLife());
	hdivcshIO.setCoverage(hdisIO.getCoverage());
	hdivcshIO.setRider(hdisIO.getRider());
	hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
	hdivcshIO.setDivdIntCapDate(hdisIO.getNextIntDate());
	hdivcshIO.setFormat(hdivcshrec);
	hdivcshIO.setFunction(varcom.begn);

	//performance improvement -- Anjali
	hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

	SmartFileCode.execute(appVars, hdivcshIO);
	if (isNE(hdivcshIO.getStatuz(),varcom.oK)
	&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
	|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
	|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
	|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
	|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
	|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
		hdivcshIO.setStatuz(varcom.endp);
	}
	while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(surdclmIO.getChdrcoy());
		hdvdintrec.chdrChdrnum.set(surdclmIO.getChdrnum());
		hdvdintrec.lifeLife.set(surdclmIO.getLife());
		hdvdintrec.covrCoverage.set(surdclmIO.getCoverage());
		hdvdintrec.covrRider.set(surdclmIO.getRider());
		hdvdintrec.plnsfx.set(ZERO);
		hdvdintrec.cntcurr.set(surdclmIO.getCurrcd());
		hdvdintrec.transcd.set(SPACES);
		hdvdintrec.crtable.set(surdclmIO.getCrtable());
		hdvdintrec.effectiveDate.set(surpcpy.effdate);
		hdvdintrec.tranno.set(ZERO);
		if (isGT(hdivcshIO.getEffdate(),hdisIO.getLastIntDate())) {
			hdvdintrec.intFrom.set(hdivcshIO.getEffdate());
		}
		else {
			hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
		}
		hdvdintrec.intTo.set(chdrpf.getPtdate());
		hdvdintrec.capAmount.set(hdivcshIO.getDivdAmount());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
			callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
			if (isNE(hdvdintrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(hdvdintrec.statuz);
				dbError9100();
			}
			wsaaNewInt.add(hdvdintrec.intAmount);
		}
		hdivcshIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
		|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
		|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
	}
}


protected void accumulateSurValue3600()
	{
		wsaaAccumValue.add(surdclmIO.getActvalue());
		wsaaAccumHvalue.add(hsudIO.getHactval());
		surdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(surdclmIO.getStatuz());
			dbError9100();
		}
		if (isNE(surdclmIO.getChdrcoy(),surpcpy.chdrcoy)
		|| isNE(surdclmIO.getChdrnum(),surpcpy.chdrnum)
		|| isEQ(surdclmIO.getStatuz(),varcom.endp)) {
			wsaaEndOfContFlag.set("Y");
		}
		/*EXIT*/
	}

protected void endOfContract4000()
	{
		start4000();
	}

protected void start4000()
	{
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		lifacmvrec1.sacscode.set(wsbbT5645Sacscode[5]);
		lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[5]);
		lifacmvrec1.glcode.set(wsbbT5645Glmap[5]);
		lifacmvrec1.glsign.set(wsbbT5645Sign[5]);
		lifacmvrec1.contot.set(wsbbT5645Cnttot[5]);
		lifacmvrec1.origamt.set(surhclmIO.getOtheradjst());
		lifacmvrec1.origcurr.set(surhclmIO.getCurrcd());
		if (isNE(lifacmvrec1.origamt,0)) {
			postAcmvRecord5000();
		}
		wsaaAccumValue.add(lifacmvrec1.origamt);
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		lifacmvrec1.sacscode.set(wsbbT5645Sacscode[20]);
		lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[20]);
		lifacmvrec1.glcode.set(wsbbT5645Glmap[20]);
		lifacmvrec1.glsign.set(wsbbT5645Sign[20]);
		lifacmvrec1.contot.set(wsbbT5645Cnttot[20]);
		lifacmvrec1.origamt.set(surhclmIO.getTaxamt());
		lifacmvrec1.origcurr.set(surhclmIO.getCurrcd());
		if (isNE(lifacmvrec1.origamt,0)) {
			postAcmvRecord5000();
		}
		wsaaAccumValue.subtract(lifacmvrec1.origamt);
		if (isNE(surpcpy.type,"P")) {
			allocateToLoans6000();
		}
		
		// Japan localization- ILB-1016 : Posting LP-UP and LP-S
		if(susur013Permission) {
			lifacmvrec1.origamt.set(surhclmIO.unexpiredprm);
			lifacmvrec1.sacscode.set(wsbbT5645Sacscode[21]);
			lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[21]);
			lifacmvrec1.glcode.set(wsbbT5645Glmap[21]);
			lifacmvrec1.glsign.set(wsbbT5645Sign[21]);
			lifacmvrec1.contot.set(wsbbT5645Cnttot[21]);
			if (isNE(lifacmvrec1.origamt, 0)) {
				postAcmvRecord5000();
			}
			wsaaAccumValue.add(surhclmIO.getUnexpiredprm());
			
			
			lifacmvrec1.origamt.set(surhclmIO.suspenseamt);
			lifacmvrec1.sacscode.set(wsbbT5645Sacscode[22]);
			lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[22]);
			lifacmvrec1.glcode.set(wsbbT5645Glmap[22]);
			lifacmvrec1.glsign.set(wsbbT5645Sign[22]);
			lifacmvrec1.contot.set(wsbbT5645Cnttot[22]);
			if (isNE(lifacmvrec1.origamt, 0)) {
				postAcmvRecord5000();
			}
			wsaaAccumValue.add(surhclmIO.getSuspenseamt());
		}
		lifacmvrec1.sacscode.set(wsbbT5645Sacscode[6]);
		lifacmvrec1.sacstyp.set(wsbbT5645Sacstype[6]);
		lifacmvrec1.glcode.set(wsbbT5645Glmap[6]);
		lifacmvrec1.glsign.set(wsbbT5645Sign[6]);
		lifacmvrec1.contot.set(wsbbT5645Cnttot[6]);
		lifacmvrec1.origamt.set(wsaaAccumValue);
		if (isNE(lifacmvrec1.origamt,0)) {
			postAcmvRecord5000();
		}
		surpcpy.status.set(varcom.endp);
	}

protected void postAcmvRecord5000()
	{
		/*START*/
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			systemError9000();
		}
		/*EXIT*/
	}

protected void allocateToLoans6000()
	{
		start6000();
	}

protected void start6000()
	{
		wsaaGlCompany.set(lifacmvrec1.genlcoy);
		wsaaGlCurrency.set(lifacmvrec1.genlcur);
		wsaaTranTermid.set(surpcpy.termid);
		wsaaTranUser.set(surpcpy.user);
		wsaaTranTime.set(surpcpy.time);
		wsaaTranDate.set(surpcpy.date_var);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(surpcpy.chdrcoy);
		wsaaTranEntity.set(surpcpy.chdrnum);
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(lifacmvrec1.rdocnum);
		cashedrec.doctCompany.set(lifacmvrec1.batccoy);
		cashedrec.trandate.set(lifacmvrec1.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.transeq.set(wsaaSequenceNo);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(lifacmvrec1.origcurr);
		cashedrec.dissrate.set(lifacmvrec1.crate);
		cashedrec.trandesc.set(lifacmvrec1.trandesc);
		cashedrec.chdrcoy.set(surhclmIO.getChdrcoy());
		cashedrec.chdrnum.set(surhclmIO.getChdrnum());
		cashedrec.tranno.set(lifacmvrec1.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.language.set(surpcpy.language);
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(surhclmIO.getChdrcoy());
		totloanrec.chdrnum.set(surhclmIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(surhclmIO.getEffdate());
		totloanrec.tranno.set(surpcpy.tranno);
		totloanrec.batchkey.set(wsaaBatckey);
		totloanrec.tranTerm.set(surpcpy.termid);
		totloanrec.tranDate.set(surpcpy.date_var);
		totloanrec.tranTime.set(surpcpy.time);
		totloanrec.tranUser.set(surpcpy.user);
		totloanrec.language.set(surpcpy.language);
		totloanrec.postFlag.set("Y");
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError9000();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal,totloanrec.interest));
		if (isGT(wsaaHeldCurrLoans,0)) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)
			|| isEQ(wsaaAccumValue,0)); wsaaSub1.add(1)){
				loans6100();
			}
		}
		wsaaSequenceNo.set(cashedrec.transeq);
	}

protected void loans6100()
	{
		start6100();
	}

protected void start6100()
	{
		wsaaGlMap.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.origamt.set(wsaaAccumValue);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			systemError9000();
		}
		wsaaAccumValue.set(cashedrec.docamt);
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		surpcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		surpcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
