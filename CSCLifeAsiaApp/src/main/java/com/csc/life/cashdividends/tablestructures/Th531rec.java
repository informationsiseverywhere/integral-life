package com.csc.life.cashdividends.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:59
 * Description:
 * Copybook name: TH531REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th531rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th531Rec = new FixedLengthStringData(5);
  	public FixedLengthStringData divdParticipant = new FixedLengthStringData(1).isAPartOf(th531Rec, 0);
  	public FixedLengthStringData puCvCode = new FixedLengthStringData(4).isAPartOf(th531Rec, 1);


	public void initialize() {
		COBOLFunctions.initialize(th531Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th531Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}