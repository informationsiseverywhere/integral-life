/******************************************************************************
 * File Name 		: Hdpxpf.java
 * Author			: sbatra9
 * Creation Date	: 23 June 2020
 * Project			: Integral Life
 * Description		: The Model Class for HDVXPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.model;

import java.math.BigDecimal; //ILIFE-9444
import java.util.Date;

public class Hdcxpf {

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int plnsfx;
	private String crtable;
	private int hdvbalc;
	private int hcapndt;
	private int tranno;
	private BigDecimal hintos; //ILIFE-9444
	private int rcesdte;
	private String usrprf;
	private String jobnm;
	private Date datime;
	
	// Constructor
	public Hdcxpf ( ) {
		//Empty Constructor
	}
		
	public Hdcxpf(Hdcxpf hdcxpf){
		this.chdrcoy=hdcxpf.chdrcoy;
    	this.chdrnum=hdcxpf.chdrnum;
    	this.life=hdcxpf.life;
    	this.jlife=hdcxpf.jlife;
    	this.coverage=hdcxpf.coverage;
    	this.rider=hdcxpf.rider;
    	this.plnsfx=hdcxpf.plnsfx;
    	this.crtable=hdcxpf.crtable;
    	this.hdvbalc=hdcxpf.hdvbalc;
    	this.hcapndt=hdcxpf.hcapndt;
    	this.tranno=hdcxpf.tranno;
    	this.hintos=hdcxpf.hintos;
    	this.rcesdte=hdcxpf.rcesdte;
    	this.usrprf=hdcxpf.usrprf;
    	this.jobnm=hdcxpf.jobnm;
    	this.datime=hdcxpf.datime;
	}
	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJLife() {
        return jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public int getPlnsfx(){
		return this.plnsfx;
	}
	public String getCrtable() {
		return crtable;
	}
	public int getHdvbalc() {
		return hdvbalc;
	}
	public int getHcapndt() {
		return hcapndt;
	}
	public int getTranno() {
		return tranno;
	}
	public BigDecimal getHintos() {
		return hintos;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());
	}
	
	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJLife(String jlife) {
	     this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public void setHdvbalc(int hdvbalc) {
		this.hdvbalc = hdvbalc;
	}
	
	public void setHcapndt(int hcapndt) {
		this.hcapndt = hcapndt;
	}
	
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	
	public void setHintos(BigDecimal hintos) {
		this.hintos = hintos;
	}
	
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());
	}

	// ToString method
	public String toString(){
		StringBuilder output = new StringBuilder();
		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJLife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("CRTABLE:		");
		output.append(getCrtable());
		output.append("\r\n");
		output.append("HDVBALC:     ");
		output.append(getHdvbalc());
		output.append("\r\n");
		output.append("HCAPNDT:     ");
		output.append(getHcapndt());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("HINTOS:		");
		output.append(getHintos());
		output.append("\r\n");
		output.append("RCESDTE:		");
		output.append(getRcesdte());
		output.append("\r\n");
		output.append("CURRFROM:		");
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");
		return output.toString();
	}
}
