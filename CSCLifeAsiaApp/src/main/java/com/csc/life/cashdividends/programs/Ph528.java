/*
 * File: Ph528.java
 * Date: 30 August 2009 1:05:32
 * Author: Quipoz Limited
 * 
 * Class transformed from PH528.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.screens.Sh528ScreenVars;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* Cash/Dividend Value Table.
*
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* EXTRA DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-3.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FIELD
* MOVES TO AVOID CONVERSION OF COMP-3 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
*                                                                     *
***********************************************************************
* </pre>
*/
public class Ph528 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH528");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Th528rec th528rec = new Th528rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh528ScreenVars sv = ScreenProgram.getScreenVars( Sh528ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1020, 
		exit3090
	}

	public Ph528() {
		super();
		screenVars = sv;
		new ScreenModel("Sh528", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case generalArea1020: 
					generalArea1020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th528rec.th528Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1020);
		}
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 99); loopVar1 += 1){
			initInsprms1500();
		}
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 11); loopVar2 += 1){
			initInstprs1700();
		}
		/*    MOVE ZERO                   TO TH528-INSTPR                  */
		/*                                   TH528-MFACTHM                 */
		th528rec.mfacthm.set(ZERO);
		th528rec.mfacthy.set(ZERO);
		th528rec.mfactm.set(ZERO);
		th528rec.mfactq.set(ZERO);
		th528rec.mfactw.set(ZERO);
		th528rec.mfact2w.set(ZERO);
		th528rec.mfact4w.set(ZERO);
		th528rec.mfacty.set(ZERO);
		th528rec.premUnit.set(ZERO);
		th528rec.unit.set(ZERO);
	}

protected void generalArea1020()
	{
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 99); loopVar3 += 1){
			moveInsprms1600();
		}
		/*    MOVE TH528-INSTPR           TO SH528-INSTPR.                 */
		wsaaSub1.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 11); loopVar4 += 1){
			moveInstprs1800();
		}
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.mfacthm.set(th528rec.mfacthm);
		sv.mfacthy.set(th528rec.mfacthy);
		sv.mfactm.set(th528rec.mfactm);
		sv.mfactq.set(th528rec.mfactq);
		sv.mfactw.set(th528rec.mfactw);
		sv.mfact2w.set(th528rec.mfact2w);
		sv.mfact4w.set(th528rec.mfact4w);
		sv.mfacty.set(th528rec.mfacty);
		sv.premUnit.set(th528rec.premUnit);
		sv.unit.set(th528rec.unit);
	}

protected void initInsprms1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		th528rec.insprm[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveInsprms1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.insprm[wsaaSub1.toInt()].set(th528rec.insprm[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void initInstprs1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		th528rec.instpr[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveInstprs1800()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isEQ(th528rec.instpr[wsaaSub1.toInt()], NUMERIC)) {
			sv.instpr[wsaaSub1.toInt()].set(th528rec.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		/*    CALL 'SH528IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SH528-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
	}

	/**
	* <pre>
	*    Enter screen validation here.
	* </pre>
	*/
protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			preparation3010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
		checkChanges3100();
		/*  ALWAYS DO UPDATE TO RELEASE HELD RECORD*/
		/*IF WSAA-UPDATE-FLAG         NOT = 'Y'*/
		/*    GO TO 3080-OTHER.*/
		itmdIO.setItemGenarea(th528rec.th528Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 99); loopVar5 += 1){
			updateInsprms3500();
		}
		/*    IF SH528-INSTPR             NOT = TH528-INSTPR               */
		/*       MOVE SH528-INSTPR        TO TH528-INSTPR                  */
		/*       MOVE 'Y' TO WSAA-UPDATE-FLAG.                             */
		wsaaSub1.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 11); loopVar6 += 1){
			updateInstprs3600();
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
		}
		if (isNE(sv.mfacthm, th528rec.mfacthm)) {
			th528rec.mfacthm.set(sv.mfacthm);
		}
		if (isNE(sv.mfacthy, th528rec.mfacthy)) {
			th528rec.mfacthy.set(sv.mfacthy);
		}
		if (isNE(sv.mfactm, th528rec.mfactm)) {
			th528rec.mfactm.set(sv.mfactm);
		}
		if (isNE(sv.mfactq, th528rec.mfactq)) {
			th528rec.mfactq.set(sv.mfactq);
		}
		if (isNE(sv.mfactw, th528rec.mfactw)) {
			th528rec.mfactw.set(sv.mfactw);
		}
		if (isNE(sv.mfact2w, th528rec.mfact2w)) {
			th528rec.mfact2w.set(sv.mfact2w);
		}
		if (isNE(sv.mfact4w, th528rec.mfact4w)) {
			th528rec.mfact4w.set(sv.mfact4w);
		}
		if (isNE(sv.mfacty, th528rec.mfacty)) {
			th528rec.mfacty.set(sv.mfacty);
		}
		if (isNE(sv.premUnit, th528rec.premUnit)) {
			th528rec.premUnit.set(sv.premUnit);
		}
		if (isNE(sv.unit, th528rec.unit)) {
			th528rec.unit.set(sv.unit);
		}
	}

protected void updateInsprms3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.insprm[wsaaSub1.toInt()], th528rec.insprm[wsaaSub1.toInt()])) {
			th528rec.insprm[wsaaSub1.toInt()].set(sv.insprm[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

protected void updateInstprs3600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.instpr[wsaaSub1.toInt()], th528rec.instpr[wsaaSub1.toInt()])) {
			th528rec.instpr[wsaaSub1.toInt()].set(sv.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
