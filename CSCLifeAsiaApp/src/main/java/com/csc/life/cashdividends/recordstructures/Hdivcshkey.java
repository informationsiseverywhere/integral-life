package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:31
 * Description:
 * Copybook name: HDIVCSHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdivcshkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdivcshFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdivcshKey = new FixedLengthStringData(64).isAPartOf(hdivcshFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdivcshChdrcoy = new FixedLengthStringData(1).isAPartOf(hdivcshKey, 0);
  	public FixedLengthStringData hdivcshChdrnum = new FixedLengthStringData(8).isAPartOf(hdivcshKey, 1);
  	public FixedLengthStringData hdivcshLife = new FixedLengthStringData(2).isAPartOf(hdivcshKey, 9);
  	public FixedLengthStringData hdivcshCoverage = new FixedLengthStringData(2).isAPartOf(hdivcshKey, 11);
  	public FixedLengthStringData hdivcshRider = new FixedLengthStringData(2).isAPartOf(hdivcshKey, 13);
  	public PackedDecimalData hdivcshPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hdivcshKey, 15);
  	public PackedDecimalData hdivcshDivdIntCapDate = new PackedDecimalData(8, 0).isAPartOf(hdivcshKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(hdivcshKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdivcshFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdivcshFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}