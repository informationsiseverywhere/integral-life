package com.csc.life.cashdividends.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdisTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:31
 * Class transformed from HDIS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdisTableDAM extends HdispfTableDAM {

	public HdisTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HDIS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "CNTCURR, " +
		            "HDV1STDT, " +
		            "HDVLDT, " +
		            "HINTLDT, " +
		            "HINTNDT, " +
		            "HCAPLDT, " +
		            "HCAPNDT, " +
		            "HDVBALL, " +
		            "HDVBALC, " +
		            "HINTOS, " +
		            "HDVSMTDT, " +
		            "HDVSMTNO, " +
		            "HDVBALST, " +
		            "SACSCODE, " +
		            "SACSTYP, " +
		            "HINTDUEM, " +
		            "HINTDUED, " +
		            "HCAPDUEM, " +
		            "HCAPDUED, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               coverage,
                               rider,
                               planSuffix,
                               validflag,
                               tranno,
                               cntcurr,
                               firstDivdDate,
                               lastDivdDate,
                               lastIntDate,
                               nextIntDate,
                               lastCapDate,
                               nextCapDate,
                               balAtLastDivd,
                               balSinceLastCap,
                               osInterest,
                               divdStmtDate,
                               divdStmtNo,
                               balAtStmtDate,
                               sacscode,
                               sacstyp,
                               intDueMm,
                               intDueDd,
                               capDueMm,
                               capDueDd,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller5.setInternal(coverage.toInternal());
	nonKeyFiller6.setInternal(rider.toInternal());
	nonKeyFiller7.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(159);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ getJlife().toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getCntcurr().toInternal()
					+ getFirstDivdDate().toInternal()
					+ getLastDivdDate().toInternal()
					+ getLastIntDate().toInternal()
					+ getNextIntDate().toInternal()
					+ getLastCapDate().toInternal()
					+ getNextCapDate().toInternal()
					+ getBalAtLastDivd().toInternal()
					+ getBalSinceLastCap().toInternal()
					+ getOsInterest().toInternal()
					+ getDivdStmtDate().toInternal()
					+ getDivdStmtNo().toInternal()
					+ getBalAtStmtDate().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getIntDueMm().toInternal()
					+ getIntDueDd().toInternal()
					+ getCapDueMm().toInternal()
					+ getCapDueDd().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, firstDivdDate);
			what = ExternalData.chop(what, lastDivdDate);
			what = ExternalData.chop(what, lastIntDate);
			what = ExternalData.chop(what, nextIntDate);
			what = ExternalData.chop(what, lastCapDate);
			what = ExternalData.chop(what, nextCapDate);
			what = ExternalData.chop(what, balAtLastDivd);
			what = ExternalData.chop(what, balSinceLastCap);
			what = ExternalData.chop(what, osInterest);
			what = ExternalData.chop(what, divdStmtDate);
			what = ExternalData.chop(what, divdStmtNo);
			what = ExternalData.chop(what, balAtStmtDate);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, intDueMm);
			what = ExternalData.chop(what, intDueDd);
			what = ExternalData.chop(what, capDueMm);
			what = ExternalData.chop(what, capDueDd);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public PackedDecimalData getFirstDivdDate() {
		return firstDivdDate;
	}
	public void setFirstDivdDate(Object what) {
		setFirstDivdDate(what, false);
	}
	public void setFirstDivdDate(Object what, boolean rounded) {
		if (rounded)
			firstDivdDate.setRounded(what);
		else
			firstDivdDate.set(what);
	}	
	public PackedDecimalData getLastDivdDate() {
		return lastDivdDate;
	}
	public void setLastDivdDate(Object what) {
		setLastDivdDate(what, false);
	}
	public void setLastDivdDate(Object what, boolean rounded) {
		if (rounded)
			lastDivdDate.setRounded(what);
		else
			lastDivdDate.set(what);
	}	
	public PackedDecimalData getLastIntDate() {
		return lastIntDate;
	}
	public void setLastIntDate(Object what) {
		setLastIntDate(what, false);
	}
	public void setLastIntDate(Object what, boolean rounded) {
		if (rounded)
			lastIntDate.setRounded(what);
		else
			lastIntDate.set(what);
	}	
	public PackedDecimalData getNextIntDate() {
		return nextIntDate;
	}
	public void setNextIntDate(Object what) {
		setNextIntDate(what, false);
	}
	public void setNextIntDate(Object what, boolean rounded) {
		if (rounded)
			nextIntDate.setRounded(what);
		else
			nextIntDate.set(what);
	}	
	public PackedDecimalData getLastCapDate() {
		return lastCapDate;
	}
	public void setLastCapDate(Object what) {
		setLastCapDate(what, false);
	}
	public void setLastCapDate(Object what, boolean rounded) {
		if (rounded)
			lastCapDate.setRounded(what);
		else
			lastCapDate.set(what);
	}	
	public PackedDecimalData getNextCapDate() {
		return nextCapDate;
	}
	public void setNextCapDate(Object what) {
		setNextCapDate(what, false);
	}
	public void setNextCapDate(Object what, boolean rounded) {
		if (rounded)
			nextCapDate.setRounded(what);
		else
			nextCapDate.set(what);
	}	
	public PackedDecimalData getBalAtLastDivd() {
		return balAtLastDivd;
	}
	public void setBalAtLastDivd(Object what) {
		setBalAtLastDivd(what, false);
	}
	public void setBalAtLastDivd(Object what, boolean rounded) {
		if (rounded)
			balAtLastDivd.setRounded(what);
		else
			balAtLastDivd.set(what);
	}	
	public PackedDecimalData getBalSinceLastCap() {
		return balSinceLastCap;
	}
	public void setBalSinceLastCap(Object what) {
		setBalSinceLastCap(what, false);
	}
	public void setBalSinceLastCap(Object what, boolean rounded) {
		if (rounded)
			balSinceLastCap.setRounded(what);
		else
			balSinceLastCap.set(what);
	}	
	public PackedDecimalData getOsInterest() {
		return osInterest;
	}
	public void setOsInterest(Object what) {
		setOsInterest(what, false);
	}
	public void setOsInterest(Object what, boolean rounded) {
		if (rounded)
			osInterest.setRounded(what);
		else
			osInterest.set(what);
	}	
	public PackedDecimalData getDivdStmtDate() {
		return divdStmtDate;
	}
	public void setDivdStmtDate(Object what) {
		setDivdStmtDate(what, false);
	}
	public void setDivdStmtDate(Object what, boolean rounded) {
		if (rounded)
			divdStmtDate.setRounded(what);
		else
			divdStmtDate.set(what);
	}	
	public PackedDecimalData getDivdStmtNo() {
		return divdStmtNo;
	}
	public void setDivdStmtNo(Object what) {
		setDivdStmtNo(what, false);
	}
	public void setDivdStmtNo(Object what, boolean rounded) {
		if (rounded)
			divdStmtNo.setRounded(what);
		else
			divdStmtNo.set(what);
	}	
	public PackedDecimalData getBalAtStmtDate() {
		return balAtStmtDate;
	}
	public void setBalAtStmtDate(Object what) {
		setBalAtStmtDate(what, false);
	}
	public void setBalAtStmtDate(Object what, boolean rounded) {
		if (rounded)
			balAtStmtDate.setRounded(what);
		else
			balAtStmtDate.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}	
	public FixedLengthStringData getIntDueMm() {
		return intDueMm;
	}
	public void setIntDueMm(Object what) {
		intDueMm.set(what);
	}	
	public FixedLengthStringData getIntDueDd() {
		return intDueDd;
	}
	public void setIntDueDd(Object what) {
		intDueDd.set(what);
	}	
	public FixedLengthStringData getCapDueMm() {
		return capDueMm;
	}
	public void setCapDueMm(Object what) {
		capDueMm.set(what);
	}	
	public FixedLengthStringData getCapDueDd() {
		return capDueDd;
	}
	public void setCapDueDd(Object what) {
		capDueDd.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		jlife.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		validflag.clear();
		tranno.clear();
		cntcurr.clear();
		firstDivdDate.clear();
		lastDivdDate.clear();
		lastIntDate.clear();
		nextIntDate.clear();
		lastCapDate.clear();
		nextCapDate.clear();
		balAtLastDivd.clear();
		balSinceLastCap.clear();
		osInterest.clear();
		divdStmtDate.clear();
		divdStmtNo.clear();
		balAtStmtDate.clear();
		sacscode.clear();
		sacstyp.clear();
		intDueMm.clear();
		intDueDd.clear();
		capDueMm.clear();
		capDueDd.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}