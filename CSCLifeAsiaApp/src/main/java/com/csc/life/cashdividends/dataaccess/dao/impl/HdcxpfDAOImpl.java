/******************************************************************************
 * File Name 		: HdcxpfDAOImpl.java
 * Author			: sbatra9
 * Creation Date	: 09 July 2020
 * Project			: Integral Life
 * Description		: The Model Class for Hdcxpf table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/

package com.csc.life.cashdividends.dataaccess.dao.impl;

import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.csc.life.cashdividends.dataaccess.dao.HdcxpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdcxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HdcxpfDAOImpl extends BaseDAOImpl<Hdcxpf> implements HdcxpfDAO{
    private static final Logger LOGGER = LoggerFactory.getLogger(HdcxpfDAOImpl.class);

    public List<Hdcxpf> searchHdcxpfRecord(String tableId, String memName,
			int batchExtractSize, int batchID) {
    	
    	StringBuilder  sqlStr = new StringBuilder();
    	sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.CHDRCOY, TE.CHDRNUM)-1)/?) ROWNM, TE.* FROM ");
        sqlStr.append(tableId);
        sqlStr.append(" TE");
        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
 		List<Hdcxpf> pfList = new ArrayList<>();
 		ResultSet rs = null;

 		try {
 			 ps.setInt(1, batchExtractSize);
        	 ps.setString(2, memName);
        	 ps.setInt(3, batchID);
        	 rs = ps.executeQuery();
             while (rs.next()) {
                Hdcxpf hdcxpf = new Hdcxpf();
                hdcxpf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
                hdcxpf.setChdrcoy(rs.getString("CHDRCOY"));
                hdcxpf.setChdrnum(rs.getString("CHDRNUM"));
                hdcxpf.setLife(rs.getString("LIFE"));
                hdcxpf.setJLife(rs.getString("JLIFE"));
                hdcxpf.setCoverage(rs.getString("COVERAGE"));
                hdcxpf.setRider(rs.getString("RIDER"));
                hdcxpf.setPlnsfx(rs.getInt("PLNSFX"));
                hdcxpf.setCrtable(rs.getString("CRTABLE"));
                hdcxpf.setHdvbalc(rs.getInt("HDVBALC"));
                hdcxpf.setHcapndt(rs.getInt("HCAPNDT"));
                hdcxpf.setTranno(rs.getInt("TRANNO"));
                hdcxpf.setRcesdte(rs.getInt("RCESDTE"));
                hdcxpf.setHintos(rs.getBigDecimal("HINTOS")); //ILIFE-9444
            	pfList.add(hdcxpf);
             
            }

        } catch (SQLException e) {
            LOGGER.error("searchHdcxpfRecord()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pfList;
    }

}