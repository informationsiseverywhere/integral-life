package com.csc.life.cashdividends.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SH545
 * @version 1.0 generated on 30/08/09 07:03
 * @author Quipoz
 */
public class Sh545ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(592);
	public FixedLengthStringData dataFields = new FixedLengthStringData(272).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,106);
	public ZonedDecimalData mpsi = DD.mpsi.copyToZonedDecimal().isAPartOf(dataFields,153);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,170);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,225);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,235);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,245);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,255);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 272);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData mpsiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 352);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] mpsiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(205);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(59).isAPartOf(subfileArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public FixedLengthStringData divdParticipant = DD.hdvpart.copy().isAPartOf(subfileFields,11);
	public ZonedDecimalData puAddNbr = DD.hpuanbr.copyToZonedDecimal().isAPartOf(subfileFields,12);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(subfileFields,15);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(subfileFields,17);
	public FixedLengthStringData rstatcode = DD.rstatcode.copy().isAPartOf(subfileFields,25);
	public ZonedDecimalData singp = DD.singp.copyToZonedDecimal().isAPartOf(subfileFields,27);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(subfileFields,44);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 59);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData hdvpartErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hpuanbrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData rstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData singpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 95);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] hdvpartOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hpuanbrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] rstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] singpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 203);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);

	public LongData Sh545screensflWritten = new LongData(0);
	public LongData Sh545screenctlWritten = new LongData(0);
	public LongData Sh545screenWritten = new LongData(0);
	public LongData Sh545protectWritten = new LongData(0);
	public GeneralTable sh545screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sh545screensfl;
	}

	public Sh545ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ptdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btdateOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {puAddNbr, crrcd, riskCessDate, sumin, singp, rstatcode, pstatcode, anbAtCcd, divdParticipant};
		screenSflOutFields = new BaseData[][] {hpuanbrOut, crrcdOut, rcesdteOut, suminOut, singpOut, rstatcodeOut, pstatcodeOut, anbccdOut, hdvpartOut};
		screenSflErrFields = new BaseData[] {hpuanbrErr, crrcdErr, rcesdteErr, suminErr, singpErr, rstatcodeErr, pstatcodeErr, anbccdErr, hdvpartErr};
		screenSflDateFields = new BaseData[] {crrcd, riskCessDate};
		screenSflDateErrFields = new BaseData[] {crrcdErr, rcesdteErr};
		screenSflDateDispFields = new BaseData[] {crrcdDisp, riskCessDateDisp};

		screenFields = new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, currcd, life, coverage, rider, crtable, occdate, rstate, pstate, ptdate, btdate, cownnum, ownername, lifcnum, linsname, mpsi, sumins};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, currcdOut, lifeOut, coverageOut, riderOut, crtableOut, occdateOut, rstateOut, pstateOut, ptdateOut, btdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, mpsiOut, suminsOut};
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, currcdErr, lifeErr, coverageErr, riderErr, crtableErr, occdateErr, rstateErr, pstateErr, ptdateErr, btdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, mpsiErr, suminsErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sh545screen.class;
		screenSflRecord = Sh545screensfl.class;
		screenCtlRecord = Sh545screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sh545protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sh545screenctl.lrec.pageSubfile);
	}
}
