package com.csc.life.cashdividends.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:29
 * Description:
 * Copybook name: HCSDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hcsdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hcsdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hcsdKey = new FixedLengthStringData(64).isAPartOf(hcsdFileKey, 0, REDEFINE);
  	public FixedLengthStringData hcsdChdrcoy = new FixedLengthStringData(1).isAPartOf(hcsdKey, 0);
  	public FixedLengthStringData hcsdChdrnum = new FixedLengthStringData(8).isAPartOf(hcsdKey, 1);
  	public FixedLengthStringData hcsdLife = new FixedLengthStringData(2).isAPartOf(hcsdKey, 9);
  	public FixedLengthStringData hcsdCoverage = new FixedLengthStringData(2).isAPartOf(hcsdKey, 11);
  	public FixedLengthStringData hcsdRider = new FixedLengthStringData(2).isAPartOf(hcsdKey, 13);
  	public PackedDecimalData hcsdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hcsdKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(hcsdKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hcsdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hcsdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}