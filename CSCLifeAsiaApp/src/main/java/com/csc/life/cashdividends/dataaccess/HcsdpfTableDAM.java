package com.csc.life.cashdividends.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HcsdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:26
 * Class transformed from HCSDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HcsdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 129;
	public FixedLengthStringData hcsdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hcsdpfRecord = hcsdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hcsdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(hcsdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hcsdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData zcshdivmth = DD.zcshdivmth.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData paycoy = DD.paycoy.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hcsdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hcsdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HcsdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HcsdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HcsdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HcsdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HcsdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HcsdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HcsdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HCSDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"EFFDATE, " +
							"ZDIVOPT, " +
							"ZCSHDIVMTH, " +
							"PAYCOY, " +
							"PAYCLT, " +
							"PAYMTH, " +
							"FACTHOUS, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"PAYCURR, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     tranno,
                                     effdate,
                                     zdivopt,
                                     zcshdivmth,
                                     paycoy,
                                     payclt,
                                     paymth,
                                     facthous,
                                     bankkey,
                                     bankacckey,
                                     paycurr,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		tranno.clear();
  		effdate.clear();
  		zdivopt.clear();
  		zcshdivmth.clear();
  		paycoy.clear();
  		payclt.clear();
  		paymth.clear();
  		facthous.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		paycurr.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHcsdrec() {
  		return hcsdrec;
	}

	public FixedLengthStringData getHcsdpfRecord() {
  		return hcsdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHcsdrec(what);
	}

	public void setHcsdrec(Object what) {
  		this.hcsdrec.set(what);
	}

	public void setHcsdpfRecord(Object what) {
  		this.hcsdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hcsdrec.getLength());
		result.set(hcsdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}