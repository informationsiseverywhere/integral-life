package com.csc.life.cashdividends.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh545screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {10, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 22, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh545ScreenVars sv = (Sh545ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sh545screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sh545screensfl, 
			sv.Sh545screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sh545ScreenVars sv = (Sh545ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sh545screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sh545ScreenVars sv = (Sh545ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sh545screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sh545screensflWritten.gt(0))
		{
			sv.sh545screensfl.setCurrentIndex(0);
			sv.Sh545screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sh545ScreenVars sv = (Sh545ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sh545screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh545ScreenVars screenVars = (Sh545ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.puAddNbr.setFieldName("puAddNbr");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.riskCessDateDisp.setFieldName("riskCessDateDisp");
				screenVars.sumin.setFieldName("sumin");
				screenVars.singp.setFieldName("singp");
				screenVars.rstatcode.setFieldName("rstatcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.anbAtCcd.setFieldName("anbAtCcd");
				screenVars.divdParticipant.setFieldName("divdParticipant");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.puAddNbr.set(dm.getField("puAddNbr"));
			screenVars.crrcdDisp.set(dm.getField("crrcdDisp"));
			screenVars.riskCessDateDisp.set(dm.getField("riskCessDateDisp"));
			screenVars.sumin.set(dm.getField("sumin"));
			screenVars.singp.set(dm.getField("singp"));
			screenVars.rstatcode.set(dm.getField("rstatcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.anbAtCcd.set(dm.getField("anbAtCcd"));
			screenVars.divdParticipant.set(dm.getField("divdParticipant"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh545ScreenVars screenVars = (Sh545ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.puAddNbr.setFieldName("puAddNbr");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.riskCessDateDisp.setFieldName("riskCessDateDisp");
				screenVars.sumin.setFieldName("sumin");
				screenVars.singp.setFieldName("singp");
				screenVars.rstatcode.setFieldName("rstatcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.anbAtCcd.setFieldName("anbAtCcd");
				screenVars.divdParticipant.setFieldName("divdParticipant");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("puAddNbr").set(screenVars.puAddNbr);
			dm.getField("crrcdDisp").set(screenVars.crrcdDisp);
			dm.getField("riskCessDateDisp").set(screenVars.riskCessDateDisp);
			dm.getField("sumin").set(screenVars.sumin);
			dm.getField("singp").set(screenVars.singp);
			dm.getField("rstatcode").set(screenVars.rstatcode);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("anbAtCcd").set(screenVars.anbAtCcd);
			dm.getField("divdParticipant").set(screenVars.divdParticipant);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sh545screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sh545ScreenVars screenVars = (Sh545ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.puAddNbr.clearFormatting();
		screenVars.crrcdDisp.clearFormatting();
		screenVars.riskCessDateDisp.clearFormatting();
		screenVars.sumin.clearFormatting();
		screenVars.singp.clearFormatting();
		screenVars.rstatcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.anbAtCcd.clearFormatting();
		screenVars.divdParticipant.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sh545ScreenVars screenVars = (Sh545ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.puAddNbr.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.singp.setClassString("");
		screenVars.rstatcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.divdParticipant.setClassString("");
	}

/**
 * Clear all the variables in Sh545screensfl
 */
	public static void clear(VarModel pv) {
		Sh545ScreenVars screenVars = (Sh545ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.puAddNbr.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.sumin.clear();
		screenVars.singp.clear();
		screenVars.rstatcode.clear();
		screenVars.pstatcode.clear();
		screenVars.anbAtCcd.clear();
		screenVars.divdParticipant.clear();
	}
}
