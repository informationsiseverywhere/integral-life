/*
 * File: Hcvmatc.java
 * Date: 29 August 2009 22:52:25
 * Author: Quipoz Limited
 *
 * Class transformed from HCVMATC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.cashdividends.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivcshTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.cashdividends.tablestructures.Th527rec;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.life.cashdividends.tablestructures.Th532rec;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*      HCVMATC - Cash Dividend Maturity Calculation
*    ------------------------------------------------
*
*   This subroutine is called from various programs via T6598.
*   It is responsible for returning the cash value, accumulated
*   dividend and O/S interest of a component including its
*   paid-up covers.  The assumption made here is that the
*   maturity transaction cannot be effected before the final date
*   of the term, also the dividend and interest allocation would
*   have been carried out for the final year of the term.
*   If the maturity date is not on the policy anniversary date,
*   the cash value is obtained according to premium modes as
*   follows:
*
*   Monthly:
*   By interpolation across two in-force duration.
*
*         Cash Value = LV + (HV - LV) * D / 365
*
*   Where HV = Cash Value at next anniversary
*         LV = Cash Value at last anniversary
*         D = No. of in-force days from last anniversary
*
*   Yearly:
*   By discount of interest.
*
*         Cash Value = HV / (1 + I  * (1 - D / 365))
*
*   Where HV = Cash Value at next anniversary
*         I = Variable annual interest rate by
*              Cover/Issue Age/Mortality/Sex
*         D = No. of in-force days from last anniversary
*
*   Note that plan processing is NOT part of this scope and
*   hence the subroutine is only catered for plan level process-
*   ing.
*
*   The subroutine is called four times for each component, each
*   time has a different function as follow:
*
*       1/ Cash Value of the basic SI
*       2/ Cash Value of all paid-up SI
*       3/ Accumulated Dividend and capitalised interest
*       4/ O/S interest to be capitalised
*
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read CHDRMJA for billing frequency to determine if pro-rata
*     on cash value is required.
*
*   - Read COVRTRB for coverage details, such as ANB-AT-RCD, SEX,
*     etc.
*
*   - determine last and next anniversary dates
*      last = COVRTRB-UNIT-STATEMENT-DATE
*      next = COVRTRB-UNIT-STATEMENT-DATE + 1 (via DATCON2)
*
*   - Depending on MATC-ENDF, process cash value of SI in 4 steps
*        space - basic cash value
*          2   - paid up addition
*          3   - accumulated dividend
*          4   - O/S interest
*
*   Basic Cash Value >>
*   - work out in force durations base on the last and next
*     anniversary date against COVRTRB-CRRCD, so that the 2
*     durations reference to TH528 and retrieve the cash values,
*     LV and HV.
*   - also work out inforce durations in days, D with last
*     anniversary date against MATC-EFFDATE
*   - calculate for monthly case,
*       Maturity value = LV + ((HV - LV) * D / 365)
*   - for yearly case, read TH532 for the interest discount rate
*       Maturity value = LV / (1 + rate * (1 - D / 365))
*   - set MATC-TYPE to 'S' and description 'Cash Value'
*
*   Paid-up Cash Value >>
*   For each HPUA read,
*     - work out in force durations base on the last and next
*       anniversary date against HPUA-CRRCD, so that the 2
*       durations reference to TH528 and retrieve the cash values,
*       LV and HV.
*     - also work out inforce durations in days, D with last
*       anniversary date against MATC-EFFDATE
*     - calculate by interpolation only,
*          Maturity value = LV + ((HV - LV) * D / 365)
*   End-for
*   - set MATC-TYPE to 'P' and description 'P/U CshVal'
*
*   Accumulate Dividend>>
*   - Read HDIS for the dividend balance since last capitalisa-
*     tion date
*   - Get all withdrawn dividend since last capitalisation date
*     from HDIV via HDIVCSH. Where these values will be -ve in
*     nature, add them to the dividend balance
*   - set MATC-TYPE to 'D' and description 'Dividend' and
*     maturity value to total dividend balance
*
*   O/S Interest      >>
*   - Get O/S interest to be capitalised from HDIS
*   - Read HCSD, with its ZCSHDIVMTH access TH501 for the
*     interest calculation subroutine
*   - Call TH501 interest calculation subr with parameters,
*        INT-FROM = last interest date from HDIS
*        INT-TO   = risk cessation date
*        CAP-AMOUNT = dividend balance since last capitalisa-
*                     tion date
*        TRANNO   = zeroes
*        TRANSCD  = spaces
*   - Add calculated interest to O/S interest
*   - Calculate interest levied on the withdrawn dividend from
*     HDIV via HDIVCSH,
*     For each HDIVCSH read, call TH501 interest calc sbr
*        INT-FROM = greater (HDIS-LAST-INT-DATE, HDIVCSH-EFFDATE)
*        INT-TO   = risk cessation date
*        CAP-AMOUNT = HDIVCSH-DIVD-AMOUNT
*        TRANNO   = zeroes
*        TRANSCD  = spaces
*   - These interests are -ve in nature, add to O/S interest
*   - set MATC-TYPE to 'I' and description 'O/S Int' and
*     maturity value to total O/S interest
*
*
*   NB: If Component is not eligible for maturity (ie. Effective
*       Date less than Risk Cessation Date) then set
*       MATC-STATUS to NETM.
*
*   To indicate the current pass :
*
*   - PROCESS-STATUS  '1'    - FIRST-TIME-THROUGH
*                     '2'    - SECOND-TIME-THROUGH
*                     '3'    - THIRD-TIME-THROUGH
*                     '4'    - FOURTH-TIME-THROUGH
*
*   To indicate the type of the current calculation :
*
*   - TYPE -          'S'    - FIRST-TIME-THROUGH
*                              (Cash Value of basic SI)
*                     'P'    - SECOND-TIME-THROUGH
*                              (Cash Value of all paid up SI)
*                     'D'    - THIRD-TIME-THROUGH
*                              (Accum Dividend and Interest)
*                     'I'    - FOURTH-TIME-THROUGH
*                              (O/S Interest)
*
*   The linkage copybook is MATCCPY and the following fields
*   must be initialed with valid values from the calling program:
*
*        - CHDR-CHDRCOY
*        - CHDR-CHDRNUM
*        - PLAN-SUFFIX
*        - POLSUM
*        - LIFE-LIFE
*        - LIFE-JLIFE
*        - COVR-COVERAGE
*        - COVR-RIDER
*        - CRTABLE
*        - CRRCD
*        - EFFDATE
*        - LANGUAGE
*        - CURRCODE
*        - CHDR-CURR
*        - CHDR-TYPE
*        - MAT-CALC-METH
*        - PSTATCODE
*        - BATCKEY
*        - STATUS = O-K
*        - PLAN-SWITCH
*
*   and the rest of the fields with zeroes for numeric fields
*   and blanks for character fields.
*
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Hcvmatc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-VARIABLES */
	private PackedDecimalData wsaaNextAnnivDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastAnnivDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaDurNext = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaDurLast = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaDayFromLast = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCvNext = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaCvLast = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaHpuaCv = new PackedDecimalData(18, 5);
		/* WSAA-ACCUMULATORS */
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaPrevDividend = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotInt = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaTotHpua = new PackedDecimalData(18, 5);

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaIssuedAge = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private FixedLengthStringData wsaaMortcls = new FixedLengthStringData(1).isAPartOf(wsaaItem, 6);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaItem, 7);
	private ZonedDecimalData wsaaAnbAtCcd = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanSuff = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanSuff, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaProcessStatus = new FixedLengthStringData(1).init("1");
	private Validator firstTimeThrough = new Validator(wsaaProcessStatus, "1");
	private Validator secondTimeThrough = new Validator(wsaaProcessStatus, "2");
	private Validator thirdTimeThrough = new Validator(wsaaProcessStatus, "3");
	private Validator fourthTimeThrough = new Validator(wsaaProcessStatus, "4");
	private Validator fifthTimeThrough = new Validator(wsaaProcessStatus, "5");
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
		/* ERRORS */
	private static final String g344 = "G344";
	private static final String g641 = "G641";
	private static final String h155 = "H155";
	private static final String hl16 = "HL16";
	private static final String itemrec = "ITEMREC";
	private static final String hcsdrec = "HCSDREC";
	private static final String hpuarec = "HPUAREC";
	private static final String hdisrec = "HDISREC";
	private static final String hdivcshrec = "HDIVCSHREC";
	private static final String covrtrbrec = "COVRTRBREC";
		/* TABLES */
	private static final String th501 = "TH501";
	private static final String th528 = "TH528";
	private static final String th532 = "TH532";
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Dividend Cash Withdraw Trans. Details*/
	private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Th501rec th501rec = new Th501rec();
	private Th528rec th528rec = new Th528rec();
	private Th532rec th532rec = new Th532rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
	private Matccpy matccpy = new Matccpy();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	private boolean cdivFlag = false;
	private boolean endowFlag = false;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private static final String th527 = "TH527";
	private static final String DVAC = "DVAC";
	private Iterator<Itempf> itempfListIterator;
	private Th527rec th527rec = new Th527rec();
	private PackedDecimalData wsaaAnniversary = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaath527Date = new PackedDecimalData(8, 0).init(0);
	private Hdivdrec hdivdrec = new Hdivdrec();
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",  HcsdpfDAO.class);
	private List<Hcsdpf> hsdpfList = null;
	private	Hcsdpf hcsdpf = null;
	private Itempf itempf = null;
	private int intdividFlag = 0;
	private String t6639 = "T6639";
	private T6639rec t6639rec = new T6639rec();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Acblpf acblpf = null;
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private List<Zraepf> zraepfList;
	private T5645rec t5645rec = new T5645rec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf();
	private Batckey wsaaBatckey = new Batckey();
	private int chdrTranno;
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("HCVMATC");
	int counter=0;
	private String t5645 = "T5645";
	private static final String t3695 = "T3695";
	private static final String PRPRO001 = "PRPRO001";   
	private static final String BTPRO015 = "BTPRO015";
	private Descpf descpf = null;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		seExit9090,
		dbExit9190
	}

	public Hcvmatc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		matccpy.maturityRec = convertAndSetParam(matccpy.maturityRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
			mainRoutine1000();
			return1900();
		}



protected void mainRoutine1000()
	{
		matccpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaProg);
		wsaaTotInt.set(ZERO);
		cdivFlag = FeaConfg.isFeatureExist(matccpy.chdrChdrcoy.toString(),BTPRO015, appVars, "IT");
		endowFlag = FeaConfg.isFeatureExist(matccpy.chdrChdrcoy.toString(), PRPRO001, appVars, "IT");
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(matccpy.chdrChdrcoy);
		covrtrbIO.setChdrnum(matccpy.chdrChdrnum);
		covrtrbIO.setLife(matccpy.lifeLife);
		covrtrbIO.setCoverage(matccpy.covrCoverage);
		covrtrbIO.setRider(matccpy.covrRider);
		covrtrbIO.setPlanSuffix(matccpy.planSuffix);
		covrtrbIO.setFormat(covrtrbrec);
		covrtrbIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covrtrbIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrtrbIO.getChdrcoy(),matccpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),matccpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),matccpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),matccpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),matccpy.covrRider)
		|| isNE(covrtrbIO.getPlanSuffix(),matccpy.planSuffix)
		|| isEQ(covrtrbIO.getStatuz(),varcom.endp)) {
			wsaaPlan.set(matccpy.planSuffix);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(matccpy.chdrChdrcoy);
			stringVariable1.addExpression(matccpy.chdrChdrnum);
			stringVariable1.addExpression(matccpy.lifeLife);
			stringVariable1.addExpression(matccpy.covrCoverage);
			stringVariable1.addExpression(matccpy.covrRider);
			stringVariable1.addExpression(wsaaPlanSuff);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(g344);
			dbError9100();
		}
		if (isEQ(covrtrbIO.getUnitStatementDate(),0)
		|| isEQ(covrtrbIO.getUnitStatementDate(),99999999)) {
			covrtrbIO.setUnitStatementDate(covrtrbIO.getRiskCessDate());
		}
		wsaaLastAnnivDate.set(covrtrbIO.getUnitStatementDate());
		datcon2rec.intDate1.set(covrtrbIO.getUnitStatementDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNextAnnivDate.set(datcon2rec.intDate2);
		if (isLT(matccpy.effdate,covrtrbIO.getRiskCessDate()) && (isLT(wsaaNextAnnivDate,covrtrbIO.getRiskCessDate())))
		{
			matccpy.status.set("NETM");
			return ;
		}
		
		if (firstTimeThrough.isTrue()
		|| isEQ(matccpy.endf,SPACES)) {
			basicCashValue1000();
			wsaaProcessStatus.set("2");
			matccpy.endf.set(varcom.oK);
		}
		else {
			if (secondTimeThrough.isTrue()) {
				puaCashValue2000();
				wsaaProcessStatus.set("3");
			}
			else {
				if (thirdTimeThrough.isTrue()) {
					if(cdivFlag){
						calcDividend3000();
					}
					accumDividend3000();
					wsaaProcessStatus.set("4");
				}
				else {
					if (fourthTimeThrough.isTrue()) {
						if(cdivFlag){
							calcinterest4000();
						}
						interest4000();
						wsaaProcessStatus.set("5");
					}
					else {
						if (fifthTimeThrough.isTrue()) {
							if(endowFlag){	
								readZrae();
								if(zraepfList.size() > 0)
									calcEndowment();
							}							
							matccpy.status.set(varcom.endp);
						}
					else {
						syserrrec.statuz.set(h155);
						systemError9000();
					}
				}
				}
			}
		}
	}


private void calcinterest4000() {
	if(intdividFlag == 0){
		return;
	}
	matccpy.actualVal.set(ZERO);
	
	if(isEQ(hcsdpf.getZdivopt(),DVAC) && isNE(wsaaPrevDividend,ZERO)){
		itempf = itempfDAO.readItdmpf("IT",matccpy.chdrChdrcoy.toString(),th501,datcon1rec.intDate.toInt(),StringUtils.rightPad(hcsdpf.getZcshdivmth(), 8));
		if(itempf != null){
			th501rec.th501Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else{
			syserrrec.params.set("IT".concat(matccpy.chdrChdrcoy.toString()).concat(th501).concat(hcsdpf.getZcshdivmth()));
			systemError9000();
		}
	}
	else{
		return;
	}
	initialize(hdvdintrec.divdIntRec);
	hdvdintrec.chdrChdrcoy.set(matccpy.chdrChdrcoy);
	hdvdintrec.chdrChdrnum.set(matccpy.chdrChdrnum);
	hdvdintrec.lifeLife.set(matccpy.lifeLife);
	hdvdintrec.covrCoverage.set(matccpy.covrCoverage);
	hdvdintrec.covrRider.set(matccpy.covrRider);
	hdvdintrec.plnsfx.set(ZERO);
	hdvdintrec.cntcurr.set(matccpy.currcode);
	hdvdintrec.transcd.set(SPACES);
	hdvdintrec.crtable.set(matccpy.crtable);
	hdvdintrec.effectiveDate.set(datcon1rec.intDate);
	hdvdintrec.tranno.set(ZERO);
	if(intdividFlag == 1){
	hdvdintrec.intFrom.set(wsaaAnniversary);
	}
	else{
	hdvdintrec.intFrom.set(covrtrbIO.getUnitStatementDate());
	}
	hdvdintrec.intTo.set(datcon1rec.intDate);
	hdvdintrec.capAmount.set(wsaaPrevDividend);
	hdvdintrec.intDuration.set(ZERO);
	hdvdintrec.intAmount.set(ZERO);
	hdvdintrec.intRate.set(ZERO);
	hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
	if (isLT(hdvdintrec.intFrom,hdvdintrec.intTo)) {
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(hdvdintrec.statuz);
			systemError9000();
		}	
		wsaaTotInt.add(hdvdintrec.intAmount);
	}

	matccpy.actualVal.set(wsaaTotInt);
	if (isLT(matccpy.actualVal,ZERO)) {
		matccpy.actualVal.set(ZERO);
	}
	matccpy.description.set("O/S Int");
 }

private void calcDividend3000() {
	
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	wsaaPrevDividend.set(0);
	if(isGTE(covrtrbIO.getUnitStatementDate(),datcon1rec.intDate)){
		return;
	}
	readth527();
	calcDividend();
	if(isNE(hdivdrec.divdAmount,ZERO)){
		matccpy.actualVal.set(hdivdrec.divdAmount);
		wsaaPrevDividend.set(hdivdrec.divdAmount);
		matccpy.description.set("Dividend");
	}
}

private void calcDividend() {
	
	datcon2rec.intDate1.set(covrtrbIO.getUnitStatementDate());
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		dbError9100();
	}
	else {
		wsaaAnniversary.set(datcon2rec.intDate2);	//CntAnniversaryDate
	}
	setuphcsd();
	if(isGT(wsaaAnniversary,datcon1rec.intDate)){
		if(isGTE(datcon1rec.intDate,covrtrbIO.getUnitStatementDate()) && isLTE(datcon1rec.intDate,wsaaAnniversary))
			intdividFlag = 2;
		return;
	}
	itempfListIterator = itempfList.iterator();
	iterateitempfList();
	if(isEQ(th527rec.th527Rec,SPACES)){
		itempfListIterator = itempfList.iterator();
		getLatestFactor();
		if(isNE(th527rec.th527Rec,SPACES)){
			intdividFlag = 1;
			processDividend();
		}
		else{
			return;
		 }
	}
}

protected void setuphcsd(){
	
	hcsdpf = new Hcsdpf();
	hcsdpf.setChdrcoy(matccpy.chdrChdrcoy.toString());
	hcsdpf.setChdrnum(matccpy.chdrChdrnum.toString());	
	hcsdpf.setLife(matccpy.lifeLife.toString());
	hcsdpf.setCoverage(matccpy.covrCoverage.toString());
	hcsdpf.setRider(matccpy.covrRider.toString());
	hcsdpf.setPlnsfx(0);		
	hsdpfList = hcsdpfDAO.searchHcsdpfRecord(hcsdpf);
	for(Hcsdpf hcsdobj : hsdpfList){
	if(isEQ(hcsdobj.getValidflag(),"1")){
		itempf = itempfDAO.readItdmpf("IT",matccpy.chdrChdrcoy.toString(),t6639,matccpy.effdate.toInt(),StringUtils.rightPad(hcsdobj.getZcshdivmth()+covrtrbIO.getPstatcode(), 8));
		if(itempf != null){
			t6639rec.t6639Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else{
			syserrrec.params.set("IT".concat(matccpy.chdrChdrcoy.toString()).concat(t6639).concat(hcsdobj.getZcshdivmth()+covrtrbIO.getPstatcode()));
			systemError9000();
		}
		hcsdpf = hcsdobj;
		break;
	}
  }
}
private void getLatestFactor() {
	
	itempfListIterator = itempfList.iterator();
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
			wsaath527Date.set(itempf.getItmfrm());
			return;
		}else{
			th527rec.th527Rec.set(SPACES);				
		}				
	}
}

private void processDividend() {	
	/* check table has been set up correctly - must have non-zero*/
	/*  risk units otherwise we will be dividing by zero !!*/
	if (isEQ(th527rec.unit,ZERO)) {
		itdmIO.setItemitem(wsaaItem);
		syserrrec.params.set(itdmIO.getParams());
		dbError9100();
	}
	initialize(hdivdrec.dividendRec);
	hdivdrec.crrcd.set(covrtrbIO.getCrrcd());
	hdivdrec.periodTo.set(wsaaAnniversary);
	hdivdrec.periodFrom.set(covrtrbIO.getUnitStatementDate());
	hdivdrec.divdAmount.set(ZERO);
	hdivdrec.sumin.set(covrtrbIO.getSumins());
	hdivdrec.crtable.set(covrtrbIO.getCrtable());
	hdivdrec.issuedAge.set(covrtrbIO.getAnbAtCcd());
	hdivdrec.mortcls.set(covrtrbIO.getMortcls());
	hdivdrec.sex.set(covrtrbIO.getSex());
	hdivdrec.chdrChdrcoy.set(matccpy.chdrChdrcoy);
	hdivdrec.itmfrmTh527.set(wsaath527Date);
	if(isNE(t6639rec.revBonusProg,SPACES)){
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
	}
}

private void iterateitempfList() {
	
	if (itempfListIterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = itempfListIterator.next();
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			if((wsaaAnniversary.toInt() >= Integer.parseInt(itempf.getItmfrm().toString()) )
					&& wsaaAnniversary.toInt() <= Integer.parseInt(itempf.getItmto().toString())){
				th527rec.th527Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				return;
			}else{
				th527rec.th527Rec.set(SPACES);
			}
		}				
	}	
	
}

private void readth527() {
	
	wsaaCrtable.set(matccpy.crtable);
	wsaaIssuedAge.set(covrtrbIO.getAnbAtCcd());
	wsaaMortcls.set(covrtrbIO.getMortcls());		
	wsaaSex.set(covrtrbIO.getSex());

	itempfList = itempfDAO.findBy("IT",matccpy.chdrChdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
	if(itempfList.size() == 0){ 
		
		while(itempfList.size() == 0){
 		
			if (isEQ(subString(wsaaItem, 5, 4),"****")) {
				itdmIO.setItemitem(wsaaItem);
				dbError9100();
				
			}
			else {
				if (isNE(subString(wsaaItem, 8, 1),"*")) {
					wsaaItem.setSub1String(8, 1, "*");
				}
				else {
					if (isNE(subString(wsaaItem, 7, 1),"*")) {
						wsaaItem.setSub1String(7, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 5, 2),"**")) {
							wsaaItem.setSub1String(5, 2, "**");
						}
					}
				}
			}
			itempfList = itempfDAO.findBy("IT",matccpy.chdrChdrcoy.toString(),th527,wsaaItem.toString().trim(),SPACES.toString());
 		}
 				
 	}
}

protected void return1900()
	{
		exitProgram();
	}

protected void basicCashValue1000()
	{
		start1000();
	}

protected void start1000()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(wsaaNextAnnivDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaDurNext.set(datcon3rec.freqFactor);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaLastAnnivDate);
		datcon3rec.intDate2.set(matccpy.effdate);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaDayFromLast.set(datcon3rec.freqFactor);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItmfrm(matccpy.crrcd);
		itdmIO.setItemtabl(th528);
		wsaaCrtable.set(matccpy.crtable);
		wsaaAnbAtCcd.set(covrtrbIO.getAnbAtCcd());
		wsaaIssuedAge.set(wsaaAnbAtCcd);
		wsaaMortcls.set(covrtrbIO.getMortcls());
		wsaaSex.set(covrtrbIO.getSex());
		itdmIO.setFormat(itemrec);
		itdmIO.setStatuz(SPACES);
		while ( !((isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemitem(),wsaaItem)))) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(matccpy.chdrChdrcoy);
			itdmIO.setItmfrm(matccpy.crrcd);
			itdmIO.setItemtabl(th528);
			itdmIO.setItemitem(wsaaItem);
			itdmIO.setFunction(varcom.begn);

			//performance improvement -- Anjali
//			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				dbError9100();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),th528)
			|| isNE(itdmIO.getItemcoy(),matccpy.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaItem)) {
				if (isEQ(subString(wsaaItem, 5, 4),"****")) {
					itdmIO.setItemitem(wsaaItem);
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(g641);
					dbError9100();
				}
				else {
					if (isNE(subString(wsaaItem, 8, 1),"*")) {
						wsaaItem.setSub1String(8, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 7, 1),"*")) {
							wsaaItem.setSub1String(7, 1, "*");
						}
						else {
							if (isNE(subString(wsaaItem, 5, 2),"**")) {
								wsaaItem.setSub1String(5, 2, "**");
							}
						}
					}
				}
			}
			else {
				th528rec.th528Rec.set(itdmIO.getGenarea());
			}
		}

		if (isLTE(wsaaDurNext,99)) {
			wsaaCvNext.set(th528rec.insprm[wsaaDurNext.toInt()]);
		}
		else {
			/*        MOVE TH528-INSTPR                TO WSAA-CV-NEXT         */
			compute(wsaaCvNext, 3).set(th528rec.instpr[sub(wsaaDurNext, 99).toInt()]);
		}
		if (isNE(matccpy.billfreq,"01")) {
			wsaaCvLast.set(ZERO);
			if (isGT(wsaaLastAnnivDate,covrtrbIO.getCrrcd())) {
				datcon3rec.datcon3Rec.set(SPACES);
				datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
				datcon3rec.intDate2.set(wsaaLastAnnivDate);
				datcon3rec.frequency.set("01");
				callProgram(Datcon3.class, datcon3rec.datcon3Rec);
				if (isNE(datcon3rec.statuz,varcom.oK)) {
					syserrrec.statuz.set(datcon3rec.statuz);
					syserrrec.params.set(datcon3rec.datcon3Rec);
					systemError9000();
				}
				else {
					wsaaDurLast.set(datcon3rec.freqFactor);
				}
				if (isLTE(wsaaDurLast,99)) {
					wsaaCvLast.set(th528rec.insprm[wsaaDurLast.toInt()]);
				}
				else {
					/*            MOVE TH528-INSTPR                TO WSAA-CV-LAST     */
					compute(wsaaCvLast, 3).set(th528rec.instpr[sub(wsaaDurLast, 99).toInt()]);
				}
			}
			compute(matccpy.actualVal, 4).setRounded(div(mult((add(wsaaCvLast,div(mult((sub(wsaaCvNext,wsaaCvLast)),wsaaDayFromLast),365))),covrtrbIO.getSumins()),(mult(th528rec.unit,th528rec.premUnit))));
		}
		else {
			itdmIO.setDataArea(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(matccpy.chdrChdrcoy);
			itdmIO.setItmfrm(matccpy.effdate);
			itdmIO.setItemtabl(th532);
			wsaaCrtable.set(matccpy.crtable);
			wsaaAnbAtCcd.set(covrtrbIO.getAnbAtCcd());
			wsaaIssuedAge.set(wsaaAnbAtCcd);
			wsaaMortcls.set(covrtrbIO.getMortcls());
			wsaaSex.set(covrtrbIO.getSex());
			itdmIO.setFormat(itemrec);
			itdmIO.setStatuz(SPACES);
			while ( !((isEQ(itdmIO.getStatuz(),varcom.oK)
			&& isEQ(itdmIO.getItemitem(),wsaaItem)))) {
				itdmIO.setItempfx("IT");
				itdmIO.setItemcoy(matccpy.chdrChdrcoy);
				itdmIO.setItmfrm(matccpy.effdate);
				itdmIO.setItemtabl(th532);
				itdmIO.setItemitem(wsaaItem);
				itdmIO.setFunction(varcom.begn);

				//performance improvement -- Anjali
//				itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//				itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

				SmartFileCode.execute(appVars, itdmIO);
				if (isNE(itdmIO.getStatuz(),varcom.oK)
				&& isNE(itdmIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(itdmIO.getStatuz());
					dbError9100();
				}
				if (isEQ(itdmIO.getStatuz(),varcom.endp)
				|| isNE(itdmIO.getItemtabl(),th532)
				|| isNE(itdmIO.getItemcoy(),matccpy.chdrChdrcoy)
				|| isNE(itdmIO.getItemitem(),wsaaItem)) {
					if (wsaaItem.containsOnly("*")) {
						itdmIO.setItemitem(wsaaItem);
						syserrrec.params.set(itdmIO.getParams());
						syserrrec.statuz.set(g641);
						dbError9100();
					}
					else {
						if (isNE(subString(wsaaItem, 8, 1),"*")) {
							wsaaItem.setSub1String(8, 1, "*");
						}
						else {
							if (isNE(subString(wsaaItem, 7, 1),"*")) {
								wsaaItem.setSub1String(7, 1, "*");
							}
							else {
								if (isNE(subString(wsaaItem, 5, 2),"**")) {
									wsaaItem.setSub1String(5, 2, "**");
								}
								else {
									if (isNE(subString(wsaaItem, 1, 4),"****")) {
										wsaaItem.setSub1String(1, 4, "****");
									}
								}
							}
						}
					}
				}
				else {
					th532rec.th532Rec.set(itdmIO.getGenarea());
				}
			}

			compute(matccpy.actualVal, 4).setRounded(div(mult((div(wsaaCvNext,(add(1,mult((div(th532rec.intanny,100)),(sub(1,div(wsaaDayFromLast,365)))))))),covrtrbIO.getSumins()),(mult(th528rec.unit,th528rec.premUnit))));
		}
		matccpy.type.set("S");
		matccpy.description.set("Cash Value");
	}

protected void puaCashValue2000()
	{
		start2000();
	}

protected void start2000()
	{
		hpuaIO.setDataArea(SPACES);
		hpuaIO.setChdrcoy(matccpy.chdrChdrcoy);
		hpuaIO.setChdrnum(matccpy.chdrChdrnum);
		hpuaIO.setLife(matccpy.lifeLife);
		hpuaIO.setCoverage(matccpy.covrCoverage);
		hpuaIO.setRider(matccpy.covrRider);
		hpuaIO.setPlanSuffix(matccpy.planSuffix);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFormat(hpuarec);
		hpuaIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			syserrrec.statuz.set(hpuaIO.getStatuz());
			dbError9100();
		}
		if (isNE(hpuaIO.getChdrcoy(),matccpy.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(),matccpy.chdrChdrnum)
		|| isNE(hpuaIO.getLife(),matccpy.lifeLife)
		|| isNE(hpuaIO.getCoverage(),matccpy.covrCoverage)
		|| isNE(hpuaIO.getRider(),matccpy.covrRider)
		|| isNE(hpuaIO.getPlanSuffix(),matccpy.planSuffix)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		wsaaTotHpua.set(ZERO);
		while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
			calculatePuaCv2100();
			hpuaIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hpuaIO);
			if (isNE(hpuaIO.getStatuz(),varcom.oK)
			&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hpuaIO.getParams());
				syserrrec.statuz.set(hpuaIO.getStatuz());
				dbError9100();
			}
			if (isNE(hpuaIO.getChdrcoy(),matccpy.chdrChdrcoy)
			|| isNE(hpuaIO.getChdrnum(),matccpy.chdrChdrnum)
			|| isNE(hpuaIO.getLife(),matccpy.lifeLife)
			|| isNE(hpuaIO.getCoverage(),matccpy.covrCoverage)
			|| isNE(hpuaIO.getRider(),matccpy.covrRider)
			|| isNE(hpuaIO.getPlanSuffix(),matccpy.planSuffix)) {
				hpuaIO.setStatuz(varcom.endp);
			}
		}

		compute(matccpy.actualVal, 6).setRounded(mult(wsaaTotHpua,1));
		matccpy.type.set("P");
		matccpy.description.set("P/U CshVal");
	}

protected void calculatePuaCv2100()
	{
		start2100();
	}

protected void start2100()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(hpuaIO.getCrrcd());
		datcon3rec.intDate2.set(wsaaNextAnnivDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		else {
			wsaaDurNext.set(datcon3rec.freqFactor);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItmfrm(hpuaIO.getCrrcd());
		itdmIO.setItemtabl(th528);
		wsaaCrtable.set(hpuaIO.getCrtable());
		wsaaAnbAtCcd.set(hpuaIO.getAnbAtCcd());
		wsaaIssuedAge.set(wsaaAnbAtCcd);
		wsaaMortcls.set(covrtrbIO.getMortcls());
		wsaaSex.set(covrtrbIO.getSex());
		itdmIO.setFormat(itemrec);
		itdmIO.setStatuz(SPACES);
		while ( !((isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemitem(),wsaaItem)))) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(matccpy.chdrChdrcoy);
			itdmIO.setItmfrm(hpuaIO.getCrrcd());
			itdmIO.setItemtabl(th528);
			itdmIO.setItemitem(wsaaItem);
			itdmIO.setFunction(varcom.begn);

			//performance improvement -- Anjali
//			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				dbError9100();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),th528)
			|| isNE(itdmIO.getItemcoy(),matccpy.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaItem)) {
				if (isEQ(subString(wsaaItem, 5, 4),"****")) {
					itdmIO.setItemitem(wsaaItem);
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(g641);
					dbError9100();
				}
				else {
					if (isNE(subString(wsaaItem, 8, 1),"*")) {
						wsaaItem.setSub1String(8, 1, "*");
					}
					else {
						if (isNE(subString(wsaaItem, 7, 1),"*")) {
							wsaaItem.setSub1String(7, 1, "*");
						}
						else {
							if (isNE(subString(wsaaItem, 5, 2),"**")) {
								wsaaItem.setSub1String(5, 2, "**");
							}
						}
					}
				}
			}
			else {
				th528rec.th528Rec.set(itdmIO.getGenarea());
			}
		}

		if (isLTE(wsaaDurNext,99)) {
			wsaaCvNext.set(th528rec.insprm[wsaaDurNext.toInt()]);
		}
		else {
			/*        MOVE TH528-INSTPR                TO WSAA-CV-NEXT         */
			compute(wsaaCvNext, 3).set(th528rec.instpr[sub(wsaaDurNext, 99).toInt()]);
		}
		wsaaCvLast.set(ZERO);
		if (isGT(wsaaLastAnnivDate,hpuaIO.getCrrcd())) {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(hpuaIO.getCrrcd());
			datcon3rec.intDate2.set(wsaaLastAnnivDate);
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				systemError9000();
			}
			else {
				wsaaDurLast.set(datcon3rec.freqFactor);
			}
			if (isLTE(wsaaDurLast,99)) {
				wsaaCvLast.set(th528rec.insprm[wsaaDurLast.toInt()]);
			}
			else {
				/*            MOVE TH528-INSTPR                TO WSAA-CV-LAST     */
				compute(wsaaCvNext, 3).set(th528rec.instpr[sub(wsaaDurLast, 99).toInt()]);
			}
		}
		compute(wsaaHpuaCv, 6).setRounded(div(mult((add(wsaaCvLast,(div(mult((sub(wsaaCvNext,wsaaCvLast)),wsaaDayFromLast),365)))),hpuaIO.getSumin()),(mult(th528rec.unit,th528rec.premUnit))));
		wsaaTotHpua.add(wsaaHpuaCv);
	}

protected void accumDividend3000()
	{
			start3100();
		}

protected void start3100()
	{
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(matccpy.chdrChdrcoy);
		hdisIO.setChdrnum(matccpy.chdrChdrnum);
		hdisIO.setLife(matccpy.lifeLife);
		hdisIO.setCoverage(matccpy.covrCoverage);
		hdisIO.setRider(matccpy.covrRider);
		hdisIO.setPlanSuffix(matccpy.planSuffix);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			dbError9100();
		}
		if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
			if(!cdivFlag){
			matccpy.actualVal.set(0);
			}
			matccpy.type.set("D");
			matccpy.description.set("Dividend");
			return ;
		}
		wsaaTotDividend.set(hdisIO.getBalSinceLastCap());
		if(cdivFlag){
			wsaaPrevDividend.add(hdisIO.getBalSinceLastCap());
		}
		hdivcshIO.setDataArea(SPACES);
		hdivcshIO.setChdrcoy(matccpy.chdrChdrcoy);
		hdivcshIO.setChdrnum(matccpy.chdrChdrnum);
		hdivcshIO.setLife(matccpy.lifeLife);
		hdivcshIO.setCoverage(matccpy.covrCoverage);
		hdivcshIO.setRider(matccpy.covrRider);
		hdivcshIO.setPlanSuffix(matccpy.planSuffix);
		compute(wsaaLastCapDate, 0).set(add(1,hdisIO.getLastCapDate()));
		hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hdivcshIO.getStatuz());
			syserrrec.params.set(hdivcshIO.getParams());
			dbError9100();
		}
		if (isNE(hdivcshIO.getChdrcoy(),matccpy.chdrChdrcoy)
		|| isNE(hdivcshIO.getChdrnum(),matccpy.chdrChdrnum)
		|| isNE(hdivcshIO.getLife(),matccpy.lifeLife)
		|| isNE(hdivcshIO.getCoverage(),matccpy.covrCoverage)
		|| isNE(hdivcshIO.getRider(),matccpy.covrRider)
		|| isNE(hdivcshIO.getPlanSuffix(),matccpy.planSuffix)) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			wsaaTotDividend.add(hdivcshIO.getDivdAmount());
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(hdivcshIO.getStatuz());
				syserrrec.params.set(hdivcshIO.getParams());
				dbError9100();
			}
			if (isNE(hdivcshIO.getChdrcoy(),matccpy.chdrChdrcoy)
			|| isNE(hdivcshIO.getChdrnum(),matccpy.chdrChdrnum)
			|| isNE(hdivcshIO.getLife(),matccpy.lifeLife)
			|| isNE(hdivcshIO.getCoverage(),matccpy.covrCoverage)
			|| isNE(hdivcshIO.getRider(),matccpy.covrRider)
			|| isNE(hdivcshIO.getPlanSuffix(),matccpy.planSuffix)) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}
		if(cdivFlag){
		matccpy.actualVal.add(wsaaTotDividend);
		}
		else{
		matccpy.actualVal.set(wsaaTotDividend);
		}
		matccpy.type.set("D");
		matccpy.description.set("Dividend");
	}

protected void interest4000()
	{
			start4100();
		}

	protected void calcEndowment() {
		
		itempf = itempfDAO.findItemByItdm(matccpy.chdrChdrcoy.toString(), t5645, wsaaProg.toString());
		if (itempf == null) {
			return;
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		/* Read T3695 description to get SACSTYPE description. */
		if (!fifthTimeThrough.isTrue()) {
			descpf = descDAO.getdescData("IT", t3695, t5645rec.sacstype01.toString(),/* IJTI-1523 */
					matccpy.chdrChdrcoy.toString(), matccpy.language.toString());
		} else {
			descpf = descDAO.getdescData("IT", t3695, t5645rec.sacstype02.toString(),/* IJTI-1523 */
					matccpy.chdrChdrcoy.toString(), matccpy.language.toString());
		}
		if (descpf == null) {
			matccpy.description.fill("?");
		} else {
			matccpy.description.set(descpf.getShortdesc());
		}

		acblenqListLPAE = acblpfDAO.getAcblenqRecord(matccpy.chdrChdrcoy.toString(),
				StringUtils.rightPad(matccpy.chdrChdrnum.toString(), 16), t5645rec.sacscode01.toString(),
				t5645rec.sacstype01.toString());

		acblenqListLPAS = acblpfDAO.getAcblenqRecord(matccpy.chdrChdrcoy.toString(),
				StringUtils.rightPad(matccpy.chdrChdrnum.toString(), 16), t5645rec.sacscode02.toString(),
				t5645rec.sacstype02.toString());

		if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
			accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
			accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);
		}
		if (acblenqListLPAS != null && !acblenqListLPAS.isEmpty()) {
			accAmtLPAS = acblenqListLPAS.get(0).getSacscurbal();
			accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
		}
		
		calculateInterest();
		displayendowment();
	}

	protected void readZrae() {
		zraepf = zraepfDAO.getItemByContractNum(matccpy.chdrChdrnum.toString());
		List<String> chdrnumList = new ArrayList<String>();
		chdrnumList.add(matccpy.chdrChdrnum.toString());
		zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, matccpy.chdrChdrcoy.toString());

	}

	protected void calculateInterest() {
		annypyintcalcrec.intcalcRec.set(SPACES);
		annypyintcalcrec.chdrcoy.set(zraepf.getChdrcoy());
		annypyintcalcrec.chdrnum.set(zraepf.getChdrnum());
		annypyintcalcrec.cnttype.set(matccpy.chdrType);
		annypyintcalcrec.interestTo.set(matccpy.effdate);
		annypyintcalcrec.interestFrom.set(zraepf.getAplstintbdte());
		annypyintcalcrec.annPaymt.set(zraepf.getApcaplamt());
		annypyintcalcrec.lastCaplsnDate.set(zraepf.getAplstcapdate());
		annypyintcalcrec.interestAmount.set(ZERO);
		annypyintcalcrec.currency.set(zraepf.getPaycurr());
		annypyintcalcrec.transaction.set("ENDOWMENT");

		callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
		if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(annypyintcalcrec.intcalcRec);
			syserrrec.statuz.set(annypyintcalcrec.statuz);
			systemError9000();
		}

		/* MOVE INTC-INTEREST-AMOUNT TO ZRDP-AMOUNT-IN. */
		/* PERFORM 5000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INTC-INTEREST-AMOUNT. */
		if (isNE(annypyintcalcrec.interestAmount, 0)) {
			zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
			callRounding5100();
			annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
			interestAmount.add(annypyintcalcrec.interestAmount);
		}

	}

	private void callRounding5100() {

		/* CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(2);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(zraepf.getPaycurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError9000();
		}
		/* EXIT */

	}

	private void displayendowment() {
		matccpy.actualVal.set(ZERO);
		wsaaTotInt.set(ZERO);
		wsaaTotInt.add(accAmtLPAE.doubleValue());
		wsaaTotInt.add(accAmtLPAS.doubleValue());
		wsaaTotInt.add(interestAmount);
		matccpy.actualVal.set(wsaaTotInt);
		if (isLT(matccpy.actualVal, ZERO)) {
			matccpy.actualVal.set(ZERO);
		}
		matccpy.type.set("E");

	}
protected void start4100()
	{
		if(cdivFlag)
			wsaaTotInt.set(0);
		hdisIO.setDataArea(SPACES);
		hdisIO.setChdrcoy(matccpy.chdrChdrcoy);
		hdisIO.setChdrnum(matccpy.chdrChdrnum);
		hdisIO.setLife(matccpy.lifeLife);
		hdisIO.setCoverage(matccpy.covrCoverage);
		hdisIO.setRider(matccpy.covrRider);
		hdisIO.setPlanSuffix(matccpy.planSuffix);
		hdisIO.setFormat(hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			syserrrec.statuz.set(hdisIO.getStatuz());
			dbError9100();
		}
		if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
			if(!cdivFlag){
			matccpy.actualVal.set(0);
			}
			matccpy.type.set("I");
			matccpy.description.set("O/S Int");
			return ;
		}
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(matccpy.chdrChdrcoy);
		hcsdIO.setChdrnum(matccpy.chdrChdrnum);
		hcsdIO.setLife(matccpy.lifeLife);
		hcsdIO.setCoverage(matccpy.covrCoverage);
		hcsdIO.setRider(matccpy.covrRider);
		hcsdIO.setPlanSuffix(matccpy.planSuffix);
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hcsdIO.getStatuz());
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItmfrm(hdisIO.getNextIntDate());
		itdmIO.setItemtabl(th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),matccpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),th501)) {
			itdmIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(itdmIO.getParams());
			systemError9000();
		}
		th501rec.th501Rec.set(itdmIO.getGenarea());
		if (isEQ(th501rec.intCalcSbr,SPACES)) {
			itdmIO.setStatuz(hl16);
			syserrrec.params.set(itdmIO.getParams());
			systemError9000();
		}
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(matccpy.chdrChdrcoy);
		hdvdintrec.chdrChdrnum.set(matccpy.chdrChdrnum);
		hdvdintrec.lifeLife.set(matccpy.lifeLife);
		hdvdintrec.covrCoverage.set(matccpy.covrCoverage);
		hdvdintrec.covrRider.set(matccpy.covrRider);
		hdvdintrec.plnsfx.set(matccpy.planSuffix);
		hdvdintrec.cntcurr.set(matccpy.chdrCurr);
		hdvdintrec.transcd.set(SPACES);
		hdvdintrec.crtable.set(matccpy.crtable);
		hdvdintrec.effectiveDate.set(matccpy.effdate);
		hdvdintrec.tranno.set(ZERO);
		hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
		hdvdintrec.intTo.set(covrtrbIO.getRiskCessDate());
		hdvdintrec.capAmount.set(hdisIO.getBalSinceLastCap());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(hdvdintrec.statuz);
			systemError9000();
		}
		wsaaTotInt.add(hdvdintrec.intAmount);
		hdivcshIO.setDataArea(SPACES);
		hdivcshIO.setChdrcoy(hdisIO.getChdrcoy());
		hdivcshIO.setChdrnum(hdisIO.getChdrnum());
		hdivcshIO.setLife(hdisIO.getLife());
		hdivcshIO.setCoverage(hdisIO.getCoverage());
		hdivcshIO.setRider(hdisIO.getRider());
		hdivcshIO.setPlanSuffix(hdisIO.getPlanSuffix());
		hdivcshIO.setDivdIntCapDate(hdisIO.getNextIntDate());
		hdivcshIO.setFormat(hdivcshrec);
		hdivcshIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		hdivcshIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hdivcshIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(),varcom.oK)
		&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hdivcshIO.getStatuz());
			syserrrec.params.set(hdivcshIO.getParams());
			dbError9100();
		}
		if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
		|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
		|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(),varcom.endp))) {
			initialize(hdvdintrec.divdIntRec);
			hdvdintrec.chdrChdrcoy.set(matccpy.chdrChdrcoy);
			hdvdintrec.chdrChdrnum.set(matccpy.chdrChdrnum);
			hdvdintrec.lifeLife.set(matccpy.lifeLife);
			hdvdintrec.covrCoverage.set(matccpy.covrCoverage);
			hdvdintrec.covrRider.set(matccpy.covrRider);
			hdvdintrec.plnsfx.set(matccpy.planSuffix);
			hdvdintrec.cntcurr.set(matccpy.chdrCurr);
			hdvdintrec.transcd.set(SPACES);
			hdvdintrec.crtable.set(matccpy.crtable);
			hdvdintrec.effectiveDate.set(matccpy.effdate);
			hdvdintrec.tranno.set(ZERO);
			if (isGT(hdivcshIO.getEffdate(),hdisIO.getLastIntDate())) {
				hdvdintrec.intFrom.set(hdivcshIO.getEffdate());
			}
			else {
				hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
			}
			hdvdintrec.intTo.set(covrtrbIO.getRiskCessDate());
			hdvdintrec.capAmount.set(hdivcshIO.getDivdAmount());
			hdvdintrec.intDuration.set(ZERO);
			hdvdintrec.intAmount.set(ZERO);
			hdvdintrec.intRate.set(ZERO);
			hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
			callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
			if (isNE(hdvdintrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(hdvdintrec.statuz);
				systemError9000();
			}
			wsaaTotInt.add(hdvdintrec.intAmount);
			hdivcshIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivcshIO);
			if (isNE(hdivcshIO.getStatuz(),varcom.oK)
			&& isNE(hdivcshIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(hdivcshIO.getStatuz());
				syserrrec.params.set(hdivcshIO.getParams());
				dbError9100();
			}
			if (isNE(hdivcshIO.getChdrcoy(),hdisIO.getChdrcoy())
			|| isNE(hdivcshIO.getChdrnum(),hdisIO.getChdrnum())
			|| isNE(hdivcshIO.getLife(),hdisIO.getLife())
			|| isNE(hdivcshIO.getCoverage(),hdisIO.getCoverage())
			|| isNE(hdivcshIO.getRider(),hdisIO.getRider())
			|| isNE(hdivcshIO.getPlanSuffix(),hdisIO.getPlanSuffix())) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}
		if(cdivFlag){
		matccpy.actualVal.add(wsaaTotInt);
		}
		else{
		matccpy.actualVal.set(wsaaTotInt);
		}
		matccpy.type.set("I");
		matccpy.description.set("O/S Int");
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		matccpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		matccpy.status.set(varcom.bomb);
		exitProgram();
	}
}
