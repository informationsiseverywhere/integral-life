package com.csc.life.mailroom.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.mailroom.screens.Sa656ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pa656 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA656");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private static final Logger LOGGER = LoggerFactory.getLogger(Pa656.class);
	private Batckey wsaaBatchkey = new Batckey();
	private Subprogrec subprogrec = new Subprogrec();
	private Wssplife wssplife = new Wssplife();
	private Sa656ScreenVars sv = ScreenProgram.getScreenVars(Sa656ScreenVars.class);

	public Pa656() {
		super();
		screenVars = sv;
		new ScreenModel("Sa656", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			LOGGER.debug("Flow Ended " + e);
		}
	}

	protected void initialise1000() {
		sv.dataArea.set(COBOLFunctions.SPACES);
		wssplife.userArea.set(COBOLFunctions.SPACES);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.mastmenu.set(SPACES);
		sv.action.set("A");
	}

	protected void screenEdit2000() {
		subprogrec.action.set(sv.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
	}

	protected void preScreenEdit() {

	}

	protected void update3000() {
		wsspcomn.sbmaction.set(sv.action);
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
	}

	protected void whereNext4000() {
		wsspcomn.programPtr.set(1);
	}
}
