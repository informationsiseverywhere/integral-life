package com.csc.life.mailroom.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sa656ScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(17);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);

	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(4).isAPartOf(dataArea, 1);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);

	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 5);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);

		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	

	public LongData Sa656screenWritten = new LongData(0);
	public LongData Sa656protectWritten = new LongData(0);
	public LongData Sa656windowWritten = new LongData(0);
	public LongData Sa656hideWritten = new LongData(0);
	public boolean hasSubfile() {
		return false;
	}

	public Sa656ScreenVars() {
		super();
		initialiseScreenVars();
	}
	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"01","02", "-01",null, null, null, null, null, null, null, null, null});

		
		screenFields = new BaseData[] {action};
		screenOutFields = new BaseData[][] {actionOut};
		screenErrFields = new BaseData[] {actionErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa656screen.class;
		protectRecord = Sa656protect.class;
	}

}

