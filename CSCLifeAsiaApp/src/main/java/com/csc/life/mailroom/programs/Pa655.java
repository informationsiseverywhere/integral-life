package com.csc.life.mailroom.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.Timestamp;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.exceptions.WorkflowServiceException;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.mailroom.dataaccess.model.Jbpmwrkflpf;
import com.csc.life.mailroom.screens.Sa655ScreenVars;
import com.csc.life.workflow.LifeWorkflowClient;
import com.csc.life.workflow.config.IntegralLifeWorkflowProperties;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Errmesg;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pa655 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA655");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Alocnorec alocnorec = new Alocnorec();
	private Sa655ScreenVars sv = ScreenProgram.getScreenVars(Sa655ScreenVars.class);
	private BaseDAO base = getApplicationContext().getBean("baseDAO", BaseDAOImpl.class);
	private static final String E186 = "E186";
	private static final Logger LOGGER = LoggerFactory.getLogger(Pa655.class);
	private Jbpmwrkflpf jbpmwrkflpf = new Jbpmwrkflpf();
	private HashMap<String, String> listDoctype = new HashMap<>();
	private LifeWorkflowClient workflowClient = getApplicationContext().getBean("lifefWorkflowClient", LifeWorkflowClient.class);
	private Errmesgrec errmesgrec = new Errmesgrec();


	public Pa655() {
		super();
		screenVars = sv;
		new ScreenModel("Sa655", AppVars.getInstance(), sv);
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 2);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			LOGGER.debug("Flow Ended " , e);
		}
	}

	@Override
	protected void initialise1000() {
		sv.dataArea.set(SPACES);
		sv.docno.set(SPACES);
		sv.docttype.set(SPACES);
		wsspcomn.mastmenu.set(SPACES);
		listDoctype.clear();
	}

	protected void preScreenEdit() {
		/* EXIT */
	}
	
	@Override
	protected void screenEdit2000() {
		//IBPTE-1239 starts
		if(isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
			return;
		}
		//IBPTE-1239 ends
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(sv.docttype, SPACES)) {
			sv.docno.set(SPACES);
			sv.docttypeErr.set(E186);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(wsspcomn.edterror, "Y")) {
			allocateDocumentNumber();
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void allocateDocumentNumber() {
		if (listDoctype.containsKey(sv.docttype.toString().trim())) {
			sv.docno.set(listDoctype.get(sv.docttype.toString().trim()));
		} else {
			alocnorec.function.set("NEXT");
			alocnorec.prefix.set("IW");
			alocnorec.company.set(wsspcomn.company);
			alocnorec.genkey.set(sv.docttype);
			callProgram(Alocno.class, alocnorec.alocnoRec);
			if (isEQ(alocnorec.statuz, Varcom.bomb)) {
				syserrrec.statuz.set(alocnorec.statuz);
				fatalError600();
			}
			sv.docno.set(alocnorec.alocNo);
			if (isNE(sv.docno, SPACES)) {
				listDoctype.put(sv.docttype.toString().trim(), sv.docno.toString().trim());
			} else {
				sv.docnoErr.set(E186);
				wsspcomn.edterror.set("Y");
			}
		}
	}

	@Override
	protected void update3000() {

		boolean isWorkflowFeatureEnabled = false;	
		isWorkflowFeatureEnabled = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP110", appVars,"IT");
	
		if (isEQ(sv.docttype, "PS") && isWorkflowFeatureEnabled ) {
			launchRetailPolicyWorkflow();
		}
	}


	protected void launchRetailPolicyWorkflow() {

		/*
		 * 
		 * Trigger JBPM work flow
		 */
		if (IntegralLifeWorkflowProperties.getWorkflowServiceRuntimeURL() != null
				&& (IntegralLifeWorkflowProperties.isWorkflowEnabled())) {
			try {
				
				workflowClient.startNBWorkflow(sv.docno.toString().trim());
			} catch (WorkflowServiceException e) {
				LOGGER.error(getMessage("RUQ6", "Error occurred during Retail Policy Workflow Launch"), e);
				syserrrec.params.set(e.getMessage());
				//IBPTE-1239
				scrnparams.errorCode.set("RUQ6");
				wsspcomn.edterror.set("Y");
				return;
			}
		}
		
		insertJbpmwrkflpf();
		
		
	}

	protected void insertJbpmwrkflpf() {
		jbpmwrkflpf.setDocttype(sv.docttype.toString());
		jbpmwrkflpf.setJbpmrefnum(sv.docno.toString());
		jbpmwrkflpf.setGcoccno("00");
		jbpmwrkflpf.setChdrcoy(wsspcomn.company.toString());
		jbpmwrkflpf.setDatime(new Timestamp(System.currentTimeMillis()));
		base.insert(jbpmwrkflpf);
	}

	@Override
	protected void whereNext4000() {
		wsspcomn.programPtr.add(1);
	}

	/* IBPTE-1239 START */
	private String getMessage(String errorCode, String errorMessage) {
		errmesgrec.eror.set(errorCode);
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaScreen);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.clear();
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		if (isEQ(errmesgrec.statuz, "BOMB")) {
			scrnparams.statuz.set(errmesgrec.statuz);
			screenErrors200();
		}
		return errorMessage + ". " + errmesgrec.errorline.trim();
	}
	/* IBPTE-1239 END */
}
