package com.csc.life.mailroom.dataaccess.dao;


import com.csc.life.mailroom.dataaccess.model.Jbpmwrkflpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface JbpmwrkflpfDAO extends BaseDAO<Jbpmwrkflpf>{

	public Jbpmwrkflpf getClaimNum();
	public Jbpmwrkflpf updateClaim(String claimNo, String gcoccno, long jbpmrefnum );
	public Jbpmwrkflpf getLastRecord();//IGB-2757
	public Jbpmwrkflpf updateChdrnum(String chdrnum, long uniqNum);//IGB-2757
}
