package com.csc.life.mailroom.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.mailroom.dataaccess.dao.JbpmwrkflpfDAO;
import com.csc.life.mailroom.dataaccess.model.Jbpmwrkflpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class JbpmwrkflpfDAOImpl extends BaseDAOImpl<Jbpmwrkflpf>  implements JbpmwrkflpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(JbpmwrkflpfDAOImpl.class);

	@Override
	public Jbpmwrkflpf getClaimNum() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT *  FROM VM1DTA.JBPMWRKFLPF WHERE CLAMNUM IS NULL OR CLAMNUM = '' ORDER BY DATIME DESC");
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Jbpmwrkflpf jbpmwrkflpf = null;
		try {
			stmt = getConnection().prepareStatement(sql.toString());
			rs = stmt.executeQuery();
			if (rs.next()) {
				jbpmwrkflpf = new Jbpmwrkflpf();
				jbpmwrkflpf.setUnique_number(rs.getLong("UNIQUE_NUMBER")); //IGB-482
				jbpmwrkflpf.setChdrcoy(rs.getString("CHDRCOY"));
				jbpmwrkflpf.setClamnum(rs.getString("CLAMNUM"));
				jbpmwrkflpf.setGcoccno(rs.getString("GCOCCNO"));
				jbpmwrkflpf.setJbpmrefnum(rs.getString("JBPMREFNUM"));
			}
		} catch (SQLException e) {
			LOGGER.error("getClaimNum()", e);
			throw new SQLRuntimeException(e);

		} finally {
			close(stmt, rs);
		}
		return jbpmwrkflpf;
	}
	
	@Override
	public Jbpmwrkflpf updateClaim(String claimNo, String gcoccno, long uniqueNum ) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE VM1DTA.JBPMWRKFLPF SET CLAMNUM=?, GCOCCNO=? WHERE UNIQUE_NUMBER=?" );
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Jbpmwrkflpf jbpmwrkflpf = null;
		try {
			stmt = getConnection().prepareStatement(sql.toString());
			stmt.setString(1, claimNo);
			stmt.setString(2, gcoccno);
			stmt.setLong(3, uniqueNum);
			int result = stmt.executeUpdate();
			if (result == 1) {
				LOGGER.info("Claim Number and Claim Occurance number updated Successfully");
			}else{
				LOGGER.info("Sometthing gone wrong, Claim Number and Claim Occurance number not updated");
			}
		} catch (SQLException e) {
			LOGGER.error("updateClaim()" , e);
			throw new SQLRuntimeException(e);

		} finally {
			close(stmt, rs);
		}
		return jbpmwrkflpf;
	}
	
	@Override
	public Jbpmwrkflpf getLastRecord() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT CLAMNUM, UNIQUE_NUMBER,CHDRCOY, CHDRNUM, DOCTTYPE FROM ");
		sql.append("(SELECT ROW_NUMBER() OVER (ORDER BY DATIME DESC) AS CLAMNUM, UNIQUE_NUMBER , CHDRCOY, CHDRNUM, DOCTTYPE  FROM JBPMWRKFLPF WHERE CHDRNUM IS NULL OR CHDRNUM='') ");
		sql.append("TAB WHERE CLAMNUM < 2"); //IBPLIFE-2429
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Jbpmwrkflpf jbpmwrkflpf = null;
		try {
			stmt = getConnection().prepareStatement(sql.toString());
			rs = stmt.executeQuery();
			if (rs.next()) {
				jbpmwrkflpf = new Jbpmwrkflpf();
				jbpmwrkflpf.setUnique_number(rs.getLong("UNIQUE_NUMBER")); //IGB-482
				jbpmwrkflpf.setChdrcoy(rs.getString("CHDRCOY"));
				jbpmwrkflpf.setChdrnum(rs.getString("CHDRNUM"));
				jbpmwrkflpf.setDocttype(rs.getString("DOCTTYPE"));
				
			}
		} catch (SQLException e) {
			LOGGER.error("getLastRecord()", e);
			throw new SQLRuntimeException(e);

		} finally {
			close(stmt, rs);
		}
		return jbpmwrkflpf;
	}
	
	@Override
	public Jbpmwrkflpf updateChdrnum( String chdrnum, long uniqNum) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE VM1DTA.JBPMWRKFLPF SET CHDRNUM=? WHERE UNIQUE_NUMBER=?"  );
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Jbpmwrkflpf jbpmwrkflpf = null;
		try {
			stmt = getConnection().prepareStatement(sql.toString());
			stmt.setString(1, chdrnum);
			stmt.setLong(2, uniqNum);
			int result = stmt.executeUpdate();
			if (result == 1) {
				LOGGER.info("Chdrnum column updated Successfully");
			}else{
				LOGGER.info("Sometthing gone wrong, Chdrnum column not updated");
			}
		} catch (SQLException e) {
			LOGGER.error("updateChdrnum()" , e);
			throw new SQLRuntimeException(e);

		} finally {
			close(stmt, rs);
		}
		return jbpmwrkflpf;
	}
}
