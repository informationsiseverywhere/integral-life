package com.csc.life.mailroom.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sa655ScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(42);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(10).isAPartOf(dataArea, 0);
	public FixedLengthStringData docttype = DD.doctype1.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData docno = DD.docno.copy().isAPartOf(dataFields,2);

	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 10);
	public FixedLengthStringData docttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData docnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);

	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 18);
	public FixedLengthStringData[] docttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] docnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);

		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	

	public LongData Sa655screenWritten = new LongData(0);
	public LongData Sa655protectWritten = new LongData(0);
	public LongData Sa655windowWritten = new LongData(0);
	public LongData Sa655hideWritten = new LongData(0);
	public boolean hasSubfile() {
		return false;
	}

	public Sa655ScreenVars() {
		super();
		initialiseScreenVars();
	}
	protected void initialiseScreenVars() {
		fieldIndMap.put(docttypeOut,new String[] {"01","02", "-01",null, null, null, null, null, null, null, null, null});
fieldIndMap.put(docnoOut,new String[] {"03","04", "-03",null, null, null, null, null, null, null, null, null});

		
		screenFields = new BaseData[] {docttype, docno};
		screenOutFields = new BaseData[][] {docttypeOut, docnoOut};
		screenErrFields = new BaseData[] {docttypeErr, docnoErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa655screen.class;
		protectRecord = Sa655protect.class;
	}

}

