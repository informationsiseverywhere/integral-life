package com.csc.life.mailroom.dataaccess.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "JBPMWRKFLPF")
public class Jbpmwrkflpf implements Serializable { 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private Long unique_number;

	private String chdrcoy;
	private String clamnum;
	private String gcoccno;
	private String jbpmrefnum;
	private String docttype;
	private String chdrnum; //IGB-2650
	private Timestamp datime;
	/**
	 * @return the unique_Number
	 */

	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}
	/**
	 * @return the unique_number
	 */
	public Long getUnique_number() {
		return unique_number;
	}
	/**
	 * @param unique_number the unique_number to set
	 */
	public void setUnique_number(Long unique_number) {
		this.unique_number = unique_number;
	}
	/**
	 * @param chdrcoy the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	/**
	 * @return the clamnum
	 */
	public String getClamnum() {
		return clamnum;
	}
	/**
	 * @param clamnum the clamnum to set
	 */
	public void setClamnum(String clamnum) {
		this.clamnum = clamnum;
	}
	/**
	 * @return the gcoccno
	 */
	public String getGcoccno() {
		return gcoccno;
	}
	/**
	 * @param gcoccno the gcoccno to set
	 */
	public void setGcoccno(String gcoccno) {
		this.gcoccno = gcoccno;
	}
	/**
	 * @return the jbpmrefnum
	 */
	public String getJbpmrefnum() {
		return jbpmrefnum;
	}
	/**
	 * @param jbpmrefnum the jbpmrefnum to set
	 */
	public void setJbpmrefnum(String jbpmrefnum) {
		this.jbpmrefnum = jbpmrefnum;
	}
	/**
	 * @return the datime
	 */
	public Timestamp getDatime() {
		return datime;
	}
	/**
	 * @param datime the datime to set
	 */
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	/**
	 * @return the docttype
	 */
	public String getDocttype() {
		return docttype;
	}
	/**
	 * @param docttype the docttype to set
	 */
	public void setDocttype(String docttype) {
		this.docttype = docttype;
	}
	//IGB-2650 starts
	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}
	/**
	 * @param chdrnum the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	//IGB-2650 ends
}
