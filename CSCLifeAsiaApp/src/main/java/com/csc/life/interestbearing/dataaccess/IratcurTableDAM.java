package com.csc.life.interestbearing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IratcurTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:56
 * Class transformed from IRATCUR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IratcurTableDAM extends IratpfTableDAM {

	public IratcurTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("IRATCUR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "COMPANY"
		             + ", VRTFND";
		
		QUALIFIEDCOLUMNS = 
		            "COMPANY, " +
		            "VRTFND, " +
		            "FROMDATE, " +
		            "TODATE, " +
		            "INTERATE01, " +
		            "INTERATE02, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "COMPANY ASC, " +
		            "VRTFND ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "COMPANY DESC, " +
		            "VRTFND DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               company,
                               unitVirtualFund,
                               fromdate,
                               todate,
                               interate01,
                               interate02,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(59);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getCompany().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(4);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(company.toInternal());
	nonKeyFiller20.setInternal(unitVirtualFund.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(67);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getFromdate().toInternal()
					+ getTodate().toInternal()
					+ getInterate01().toInternal()
					+ getInterate02().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, fromdate);
			what = ExternalData.chop(what, todate);
			what = ExternalData.chop(what, interate01);
			what = ExternalData.chop(what, interate02);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getFromdate() {
		return fromdate;
	}
	public void setFromdate(Object what) {
		setFromdate(what, false);
	}
	public void setFromdate(Object what, boolean rounded) {
		if (rounded)
			fromdate.setRounded(what);
		else
			fromdate.set(what);
	}	
	public PackedDecimalData getTodate() {
		return todate;
	}
	public void setTodate(Object what) {
		setTodate(what, false);
	}
	public void setTodate(Object what, boolean rounded) {
		if (rounded)
			todate.setRounded(what);
		else
			todate.set(what);
	}	
	public PackedDecimalData getInterate01() {
		return interate01;
	}
	public void setInterate01(Object what) {
		setInterate01(what, false);
	}
	public void setInterate01(Object what, boolean rounded) {
		if (rounded)
			interate01.setRounded(what);
		else
			interate01.set(what);
	}	
	public PackedDecimalData getInterate02() {
		return interate02;
	}
	public void setInterate02(Object what) {
		setInterate02(what, false);
	}
	public void setInterate02(Object what, boolean rounded) {
		if (rounded)
			interate02.setRounded(what);
		else
			interate02.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getInterates() {
		return new FixedLengthStringData(interate01.toInternal()
										+ interate02.toInternal());
	}
	public void setInterates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInterates().getLength()).init(obj);
	
		what = ExternalData.chop(what, interate01);
		what = ExternalData.chop(what, interate02);
	}
	public PackedDecimalData getInterate(BaseData indx) {
		return getInterate(indx.toInt());
	}
	public PackedDecimalData getInterate(int indx) {

		switch (indx) {
			case 1 : return interate01;
			case 2 : return interate02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInterate(BaseData indx, Object what) {
		setInterate(indx, what, false);
	}
	public void setInterate(BaseData indx, Object what, boolean rounded) {
		setInterate(indx.toInt(), what, rounded);
	}
	public void setInterate(int indx, Object what) {
		setInterate(indx, what, false);
	}
	public void setInterate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInterate01(what, rounded);
					 break;
			case 2 : setInterate02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		company.clear();
		unitVirtualFund.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		fromdate.clear();
		todate.clear();
		interate01.clear();
		interate02.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}