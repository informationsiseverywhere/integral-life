/*
 * File: Zrevinc.java
 * Date: 30 August 2009 2:55:55
 * Author: Quipoz Limited
 * 
 * Class transformed from ZREVINC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import org.apache.commons.collections.iterators.LoopingIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;

import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* ZREVINC - Reversal of Interest Bearing Interest Calculation
*
* This subroutine is called by Full Contract Reversal via table
* T6661. The parameters passed to this subroutine are contained
* in the REVERSEREC copybook.
*
* This subroutine will reverse the following records:
*
* 1. Contract header (CHDR).
*
* 2. Accounting movements (ACMV).
*
* 3. Interest bearing fund transaction details (HITR).
*
* 4. Interest bearing fund interest detail records (HITDs) will
*    be reversed at component level by calling the generic
*    subroutine on T5671.
*
***********************************************************************
* </pre>
*/
public class Zrevinc extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrevinc.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZREVINC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* FORMATS */
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String descrec = "DESCREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String arcmrec = "ARCMREC";
	private static final String hitrrevrec = "HITRREVREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private T5671rec t5671rec = new T5671rec();
	private Reverserec reverserec = new Reverserec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		feedback4040, 
		nextHitr4050, 
		exit4090
	}

	public Zrevinc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void main010()
	{
		/*MAINLINE-START*/
		reverserec.statuz.set(varcom.oK);
		initialise1000();
		processChdrs2000();
		processAcmvs3000();
		processHitrs4000();
		genericProcessing5000();
		/*MAINLINE-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1010();
	}

protected void start1010()
	{
		wsaaTransDesc.set(SPACES);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		/* Get today's date.*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			syserr570();
		}
		/* Get transaction description for putting on ACMV later on.*/
		getDescription1100();
	}

protected void getDescription1100()
	{
		start1110();
	}

protected void start1110()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			dbError580();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void processChdrs2000()
	{
		chdr2010();
	}

protected void chdr2010()
	{
		/* Delete the validflag '1' record.*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		|| isNE(chdrlifIO.getValidflag(),"1")) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError580();
		}
		chdrlifIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError580();
		}
		/* Reinstate the validflag '2' record.*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		|| isNE(chdrlifIO.getValidflag(),"2")) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError580();
		}
		/* Update the selected record from the contract header file*/
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError580();
		}
	}

protected void processAcmvs3000()
	{
		start3010();
		processAcmvOptical3050();
	}

protected void start3010()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			dbError580();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvs3100();
		}
		
	}

protected void processAcmvOptical3050()
	{
		/* Find the latest accounting period archived.*/
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			dbError580();
				}
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		/* If the PTRN being processed has an Accounting Period greater*/
		/* than the last period archived then there is no need to attempt*/
		/* to read the Optical Device.*/
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		/* If PTRN Accounting Period <= ARCM Accouting Period, call*/
		/* COLDPOINT to determine if ACMVs on Optical are to be*/
		/* reversed.*/
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			dbError580();
				}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
				}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs3100();
				}
		
		/* If the next policy is found then we must call the COLD API*/
		/* again with a function of CLOSE.*/
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError580();
			}
			}
		}

protected void reverseAcmvs3100()
	{
		start3110();
		readNextAcmv3180();
	}

protected void start3110()
	{
		/* Check If ACMV just read has same trancode as transaction*/
		/* we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			return ;
		}
		/* Post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.effdate.set(datcon1rec.intDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserr570();
		}
	}

protected void readNextAcmv3180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			dbError580();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processHitrs4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					hitr4010();
				case feedback4040: 
					feedback4040();
				case nextHitr4050: 
					nextHitr4050();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void hitr4010()
	{
		hitrrevIO.setParams(SPACES);
		hitrrevIO.setChdrcoy(reverserec.company);
		hitrrevIO.setChdrnum(reverserec.chdrnum);
		hitrrevIO.setTranno(reverserec.tranno);
		hitrrevIO.setFormat(hitrrevrec);
		hitrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)
		&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			dbError580();
		}
		if (isNE(hitrrevIO.getChdrcoy(),reverserec.company)
		|| isNE(hitrrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(hitrrevIO.getTranno(),reverserec.tranno)
		|| isEQ(hitrrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit4090);
		}
	}

protected void feedback4040()
	{
		if (isEQ(hitrrevIO.getFeedbackInd(),SPACES)) {
			deleteHitr4100();
			goTo(GotoLabel.nextHitr4050);
		}
		if (isEQ(hitrrevIO.getFeedbackInd(),"Y")) {
			createHitr4200();
		}
	}

protected void nextHitr4050()
	{
		hitrrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)
		&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			dbError580();
		}
		if (isNE(hitrrevIO.getChdrcoy(),reverserec.company)
		|| isNE(hitrrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(hitrrevIO.getTranno(),reverserec.tranno)
		|| isEQ(hitrrevIO.getStatuz(),varcom.endp)) {
			return ;
		}
		goTo(GotoLabel.feedback4040);
	}

protected void deleteHitr4100()
	{
		/*DELETE*/
		hitrrevIO.setFormat(hitrrevrec);
		hitrrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrrevIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void createHitr4200()
	{
		format4210();
	}

protected void format4210()
	{
		hitrrevIO.setTranno(reverserec.newTranno);
		setPrecision(hitrrevIO.getContractAmount(), 2);
		hitrrevIO.setContractAmount(mult(hitrrevIO.getContractAmount(),-1));
		setPrecision(hitrrevIO.getFundAmount(), 2);
		hitrrevIO.setFundAmount(mult(hitrrevIO.getFundAmount(),-1));
		setPrecision(hitrrevIO.getProcSeqNo(), 0);
		hitrrevIO.setProcSeqNo(mult(hitrrevIO.getProcSeqNo(),-1));
		hitrrevIO.setFormat(hitrrevrec);
		hitrrevIO.setSurrenderPercent(ZERO);
		hitrrevIO.setFeedbackInd(SPACES);
		hitrrevIO.setSwitchIndicator(SPACES);
		hitrrevIO.setTriggerModule(SPACES);
		hitrrevIO.setTriggerKey(SPACES);
		hitrrevIO.setBatctrcde(reverserec.batctrcde);
		/* MOVE SPACE                  TO HITRREV-ZINTAPPIND.           */
		hitrrevIO.setFunction(varcom.writr);
		hitrrevIO.setUstmno(ZERO);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrrevIO.getParams());
			dbError580();
		}
	}

protected void genericProcessing5000()
	{
		/*START*/
		/* We will read through all the coverages & riders attached to*/
		/* the contract that we are processing, and for each one we will*/
		/* do any required generic processing by calling the subroutine(s)*/
		/* specified on table T5671.*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs5100();
		}
		
		/*EXIT*/
	}

protected void processCovrs5100()
	{
			start5110();
		}

protected void start5110()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			dbError580();
		}
		if (isNE(reverserec.company,covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,covrenqIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(),varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* Check whether Life/Coverage/Rider have changed - if so, we*/
		/* need to call any generic subroutines that are on T5671.*/
		if (isNE(covrenqIO.getLife(),wsaaLife)
		|| isNE(covrenqIO.getCoverage(),wsaaCoverage)
		|| isNE(covrenqIO.getRider(),wsaaRider)
		|| isNE(covrenqIO.getPlanSuffix(),wsaaPlanSuffix)) {
			genericSubr5200();
			wsaaLife.set(covrenqIO.getLife());
			wsaaCoverage.set(covrenqIO.getCoverage());
			wsaaRider.set(covrenqIO.getRider());
			wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void genericSubr5200()
	{
			start5210();
		}

protected void start5210()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/* the coverage/rider we are processing.*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the subroutines on*/
		/* T5671.*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(varcom.vrcmMaxDate);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the subroutines specified in the T5671 entry.*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callTrevsubs5300();
		}
	}

protected void callTrevsubs5300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz,varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				syserr570();
			}
		}
		/*EXIT*/
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		reverserec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		reverserec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
