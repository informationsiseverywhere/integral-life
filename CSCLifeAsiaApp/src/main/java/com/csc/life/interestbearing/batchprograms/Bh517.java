/*
 * File: Bh517.java
 * Date: 29 August 2009 21:28:04
 * Author: Quipoz Limited
 * 
 * Class transformed from BH517.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.dataaccess.HitxpfTableDAM;
import com.csc.life.interestbearing.reports.Rh517Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   INTEREST BEARING FUND MOVEMENT REPORT.
*   ======================================
*
*   This program has been cloned from B5101, the Unit-linked
*   Fund Movement Report.
*
*   This program is part of the Multi-Thread Unit Deal batch job.
*   If reports are required, it must be run directly after the
*   Interest Bearing Fund Deal Extract splitter program BH508.
*   The program has two connected functions, namely:
*
*   - to print the total value of the funds extracted
*
*   - to print the outstanding transactions extracted
*     grouped by fund
*
*    The program is run single thread and processes all the
*    records in all the members created by the previous splitter
*    program.
*
*    SQL will be used for the fetching of records using these
*    criteria:
*      -  CHDRCOY = BSPR-COMPANY
*         ORDER BY ZINTBFND, CHDRNUM, LIFE,
*         COVERAGE, RIDER, PLNSFX.
*
*    In order to improve the program's performace block fetching
*    has been employed. This merits some explanation (see also
*    comments in sections).
*
*    The blocks (SQL-FIELDS storage) are large enough to store a
*    thousand sets of records at a time. The program loops round
*    the 2000- section fetching sets of records into the current
*    block until the block is full or until all the records have
*    been fetched in, leaving a partially full block. These
*    records are then referenced by the pointer SQL-I for use in
*    the 3000- section.
*
*    Two kinds of record are extracted for inclusion in the
*    reports: HITRs and HIFDs. These are extracted at the same
*    time from their respective physical files by the use of an
*    SQL UNION statement. The field RECTYPE has been created to
*    allow easy identification of the origin of records and will
*    contain either 'HIFD' or 'HITR'.
*
*    HITR records are extracted from the threads created by the
*    splitter program BH508. Since the parameters used are thus
*    variable (since there will be more than one member from
*    which to extract records) dynamic SQL is used.
*
*    The threads from BH508 are overriden to the temporary file
*    HITXxxnnnn, where xx are the two characters on BPRD system
*    parameter 4 and nnnn are the last four numbers of the job no.
*
*    The 3000- section runs using control break logic. A control
*    break is triggered on a change of fund. This causes a page
*    throw and header lines are printed. When the first record
*    is detected, the FIRST-REC flag, set on at the start, will
*    still be on and the headers will be set up as above.
*
*    Detail lines are printed when records are extracted which
*    have the same fund type as the last record; totals lines
*    are produced when there is a change, and are set up prior
*    to the printing of a new header line.
*
*   Control totals:
*     01  -  Number of outstanding transactions read
*     02  -  No. of outstanding transactions processed
*     03  -  No. of o/s transactions with no fund
*     04  -  Number of non-invested transactions
*     05  -  Number of funds with no transactions
*     06  -  Number of report 1 pages printed
*     07  -  Number of report 2 pages printed
*
*****************************************************************
* </pre>
*/
public class Bh517 extends Mainb {

	private static final Logger LOGGER = LoggerFactory.getLogger(Bh517.class);
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhitxpf1rs = null;
	private java.sql.PreparedStatement sqlhitxpf1ps = null;
	private java.sql.Connection sqlhitxpf1conn = null;
	private String sqlhitxpf1 = "";
	private int hitxpf1LoopIndex = 0;
	private Rh517Report printerFile1 = new Rh517Report();
	private Rh517Report printerFile2 = new Rh517Report();
	private FixedLengthStringData printerRec1 = new FixedLengthStringData(133);
	private FixedLengthStringData printerRec2 = new FixedLengthStringData(133);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH517");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData ap = new FixedLengthStringData(1).init("'");
	private ZonedDecimalData iz = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaZintbfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLastRecType = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30).init("?", true);
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastChdrcoy = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaBeforeTotVal = new ZonedDecimalData(18, 5);
	private ZonedDecimalData wsaaNewTotValue = new ZonedDecimalData(18, 5);
	private PackedDecimalData wsaaPrcntDiff = new PackedDecimalData(6, 3);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaSqlSelect = new FixedLengthStringData(8192).init("##");

	private FixedLengthStringData wsaaRecType = new FixedLengthStringData(4);
	private Validator hifdRec = new Validator(wsaaRecType, "HIFD");
	private Validator hitrRec = new Validator(wsaaRecType, "HITR");

	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private Validator firstRec = new Validator(wsaaFirstRecord, "Y");

	private FixedLengthStringData wsaaReportType = new FixedLengthStringData(1);
	private Validator summaryOnlyReport = new Validator(wsaaReportType, "S");

	private FixedLengthStringData wsaaHitxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHitxFn, 0, FILLER).init("HITX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHitxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHitxFn, 6).setUnsigned();
		/* SQLA-CODES */
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler1 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler1, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler1, 8);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
	private PackedDecimalData isql = new PackedDecimalData(5, 0).init(ZERO);
	private int sqlerrd = 0;
		/* TABLES */
	private static final String T1693 = "T1693";
	private static final String T3629 = "T3629";
	private static final String T5515 = "T5515";
		/* ERRORS */
	private static final String IVRM = "IVRM";
	private static final String ESQL = "ESQL";
	private static final String HL10 = "HL10";
		/* CONTROL-TOTALS */
	private static final int CT01 = 1;
	private static final int CT02 = 2;
	private static final int CT03 = 3;
	private static final int CT04 = 4;
	private static final int CT05 = 5;
	private static final int CT06 = 6;
	private static final int CT07 = 7;
	private static final String CHECK = "CHECK";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rh517dh1Record = new FixedLengthStringData(98);
	private FixedLengthStringData rh517dh1O = new FixedLengthStringData(98).isAPartOf(rh517dh1Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh517dh1O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh517dh1O, 1);
	private FixedLengthStringData zintbfnd = new FixedLengthStringData(4).isAPartOf(rh517dh1O, 31);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rh517dh1O, 35);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(rh517dh1O, 65);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30).isAPartOf(rh517dh1O, 68);


	private FixedLengthStringData rh517dt1Record = new FixedLengthStringData(57);
	private FixedLengthStringData rh517dt1O = new FixedLengthStringData(57).isAPartOf(rh517dt1Record, 0);
	private ZonedDecimalData fundvalb = new ZonedDecimalData(18, 5).isAPartOf(rh517dt1O, 0);
	private ZonedDecimalData fundvalc = new ZonedDecimalData(18, 5).isAPartOf(rh517dt1O, 18);
	private ZonedDecimalData zvaldiff = new ZonedDecimalData(15, 2).isAPartOf(rh517dt1O, 36);
	private ZonedDecimalData prcntdiff = new ZonedDecimalData(6, 3).isAPartOf(rh517dt1O, 51);

	private FixedLengthStringData rh517sh1Record = new FixedLengthStringData(31);
	private FixedLengthStringData rh517sh1O = new FixedLengthStringData(31).isAPartOf(rh517sh1Record, 0);
	private FixedLengthStringData company1 = new FixedLengthStringData(1).isAPartOf(rh517sh1O, 0);
	private FixedLengthStringData companynm1 = new FixedLengthStringData(30).isAPartOf(rh517sh1O, 1);

	private FixedLengthStringData rh517sd1Record = new FixedLengthStringData(60);
	private FixedLengthStringData rh517sd1O = new FixedLengthStringData(60).isAPartOf(rh517sd1Record, 0);
	private FixedLengthStringData zintbfnd1 = new FixedLengthStringData(4).isAPartOf(rh517sd1O, 0);
	private FixedLengthStringData longdesc1 = new FixedLengthStringData(30).isAPartOf(rh517sd1O, 4);
	private ZonedDecimalData ztranval = new ZonedDecimalData(17, 2).isAPartOf(rh517sd1O, 34);
	private FixedLengthStringData fndcurr1 = new FixedLengthStringData(3).isAPartOf(rh517sd1O, 51);
	private ZonedDecimalData prcntdiff1 = new ZonedDecimalData(6, 3).isAPartOf(rh517sd1O, 54);

	private FixedLengthStringData rh517e01Record = new FixedLengthStringData(2);

	private FixedLengthStringData rh517e02Record = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Rh517dd1RecordInner rh517dd1RecordInner = new Rh517dd1RecordInner();
	private SqlHitxpfInner sqlHitxpfInner = new SqlHitxpfInner();

	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		ENDOFCURSOR2080, 
		EXIT2090
	}

	public Bh517() {
		super();
	}

@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}
@Override
protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

@Override
protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

@Override
protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

@Override
protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

@Override
protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

@Override
protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

@Override
protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

@Override
protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

@Override
protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

@Override
protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

@Override
protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

@Override
protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

@Override
protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

@Override
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("mainline()", e);
		}
	}

@Override
protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

@Override
protected void initialise1000()
	{
		indicArea.set("0");
		wsaaFirstRecord.set("Y");
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(IVRM);
			fatalError600();
		}
		printerFile1.openOutput();
		printerFile2.openOutput();
		wsaaReportType.set(bprdIO.getSystemParam01());
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaSqlSelect, "##");
		stringVariable1.addExpression("SELECT ALL CHDRCOY,");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(",");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(",");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(",");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(", -0, ZINTBFND, -0,");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(", -0,");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(",");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(",-0, -0,");
		stringVariable1.addExpression("ZTOTFNDVAL, ");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(" ##");
		stringVariable1.setStringInto(wsaaSqlSelect);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(wsaaSqlSelect, "##");
		stringVariable2.addExpression(", -0, -0, -0,");
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression("HIFD");
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression(" FROM HIFDPF ");
		stringVariable2.addExpression("WHERE CHDRCOY = ");
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression(bsprIO.getCompany());
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression(" ##");
		stringVariable2.setStringInto(wsaaSqlSelect);
		StringUtil stringVariable3 = new StringUtil();
		stringVariable3.addExpression(wsaaSqlSelect, "##");
		stringVariable3.addExpression("ORDER BY  7, 2, 3, 4, 5, 6, 9");
		stringVariable3.setStringInto(wsaaSqlSelect);
		/* This DISPLAY statement allows us to examine active data from*/
		/* the SELECT statement.*/
		getAppVars().addDiagnostic(wsaaSqlSelect);
		/* The PREPARE statement has to be used here since we wish to run*/
		/* the WSAA-SQL-SELECT several times within the program using*/
		/* varying parameters.*/
		sqlhitxpf1 = wsaaSqlSelect.toString();
		sqlerrorflag = false;
		try {
			sqlhitxpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new HitxpfTableDAM());
			sqlhitxpf1ps = getAppVars().prepareStatementEmbeded(sqlhitxpf1conn, sqlhitxpf1);
			sqlhitxpf1rs = getAppVars().executeQuery(sqlhitxpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		isql.set(wsaaRowsInBlock);
		sqlerrd = wsaaRowsInBlock.toInt();
		/*    Get company name.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(T1693);
		itemIO.setItemitem(bsprIO.getCompany());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set(CHECK);
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,Varcom.oK)) {
			wsaaCompanynm.fill("?");
		}
		else {
			wsaaCompanynm.set(getdescrec.longdesc);
		}
	}

protected void openThreadMember1100()
	{
		/*OPEN-THREAD-MEMBER*/
		/* This SQL command takes as many HITXnn members as have been     t*/
		/* created in BH508, and overrides them all to the temporary file **/
		/* HITXxxnnnn (where xx are the two characters on BPRD sysparam 4*/
		/* and nnnn are the last four numbers of the job no.)*/
		iy.set(iz);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HITX");
		stringVariable1.addExpression(iy);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHitxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(THREAD");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(wsaaSqlSelect, "##");
		stringVariable2.addExpression("SELECT ALL CHDRCOY, CHDRNUM, LIFE, COVERAGE, ");
		stringVariable2.addExpression("RIDER, PLNSFX, ZINTBFND, TRANNO, ");
		stringVariable2.addExpression("BATCTRCDE, EFFDATE, CNTCURR, FDBKIND, ");
		stringVariable2.addExpression("CNTAMNT, FUNDAMNT, -0, CNTTYP, PRCSEQ, ");
		stringVariable2.addExpression("PERSUR, FUNDRATE,");
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression("HITR");
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression("FROM " + wsaaHitxFn.toString());
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression("WHERE CHDRCOY = ");
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression(bsprIO.getCompany());
		stringVariable2.addExpression(ap);
		stringVariable2.addExpression(" UNION ALL ##");
		stringVariable2.setStringInto(wsaaSqlSelect);
		/*EXIT*/
	}

@Override
protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case ENDOFCURSOR2080: 
					endOfCursor2080();
				case EXIT2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(Varcom.oK);
		/*    In the 1000 section we have set the SQLERRD(3) to the*/
		/*    maximum. The first thing we do is set the pointer up by*/
		/*    one and then compare it to SQLERRD(3) which contains the*/
		/*    number of rows that were actually fetched.*/
		isql.add(1);
		if (isGTE(sqlerrd, isql.toInt())) {
			/*        Still processing a block*/
			contotrec.totno.set(CT01);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.EXIT2090);
		}
		else {
			if (isEQ(sqlerrd, wsaaRowsInBlock.toInt())) {
				/*        The current block was full, get another one*/
				/*NEXT_SENTENCE*/
			}
			else {
				/*        The last block contained the last record, no point doing*/
				/*        further fetches, all done*/
				goTo(GotoLabel.ENDOFCURSOR2080);
			}
		}
		sqlerrorflag = false;
		try {
			for (hitxpf1LoopIndex = 1; isLT(hitxpf1LoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlhitxpf1rs); hitxpf1LoopIndex++ ){
				getAppVars().getDBObject(sqlhitxpf1rs, 1, sqlHitxpfInner.sqlChdrcoy[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 2, sqlHitxpfInner.sqlChdrnum[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 3, sqlHitxpfInner.sqlLife[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 4, sqlHitxpfInner.sqlCoverage[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 5, sqlHitxpfInner.sqlRider[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 6, sqlHitxpfInner.sqlPlnsfx[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 7, sqlHitxpfInner.sqlZintbfnd[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 8, sqlHitxpfInner.sqlTranno[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 9, sqlHitxpfInner.sqlBatctrcde[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 10, sqlHitxpfInner.sqlEffdate[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 11, sqlHitxpfInner.sqlCntcurr[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 12, sqlHitxpfInner.sqlFdbkind[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 13, sqlHitxpfInner.sqlCntamnt[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 14, sqlHitxpfInner.sqlFundamnt[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 15, sqlHitxpfInner.sqlZtotfndval[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 16, sqlHitxpfInner.sqlCnttyp[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 17, sqlHitxpfInner.sqlPrcseq[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 18, sqlHitxpfInner.sqlPersur[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 19, sqlHitxpfInner.sqlFundrate[hitxpf1LoopIndex]);
				getAppVars().getDBObject(sqlhitxpf1rs, 20, sqlHitxpfInner.sqlRectype[hitxpf1LoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		sqlerrd = hitxpf1LoopIndex - 1;
		if (sqlerrd == 0) {
			goTo(GotoLabel.ENDOFCURSOR2080);
		}
		isql.set(1);
		contotrec.totno.set(CT01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.EXIT2090);
	}

protected void endOfCursor2080()
	{
		getAppVars().freeDBConnectionIgnoreErr(sqlhitxpf1conn, sqlhitxpf1ps, sqlhitxpf1rs);
		wsspEdterror.set(Varcom.endp);
	}

@Override
protected void edit2500()
	{
		/*EDIT*/
		/* The UNION command performed earlier means that we have brought i*/
		/* two different types of record: HIFDs and HITRs. These are easily*/
		/* identifiable by use of the RECTYPE field which will contain a*/
		/* reference to one or the other.*/
		wsaaRecType.set(sqlHitxpfInner.sqlRectype[isql.toInt()]);
		/* It is conceivable, though highly improbable, that the user may w*/
		/* to process transactions not actually connected to any fund. In s*/
		/* cases, we ignore that HITR until such a time as there may be a f*/
		/* to which it can be attached.*/
		if (isEQ(sqlHitxpfInner.sqlZintbfnd[isql.toInt()], SPACES)
		&& hitrRec.isTrue()) {
			contotrec.totno.set(CT04);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		wsspEdterror.set(Varcom.oK);
		/*EXIT*/
	}

@Override
protected void update3000()
	{
		if (firstRec.isTrue()) {
			wsaaFirstRecord.set("N");
			setControlBreak3010();
			setUpFundDetails3020();
			summaryPageHeading3030();
			detailPageHeading3040();
		}
		if (isNE(sqlHitxpfInner.sqlZintbfnd[isql.toInt()], wsaaZintbfnd)) {
			detailFooting3050();
			summaryLine3060();
			zeroiseAccumulations3070();
			setUpFundDetails3020();
			detailPageHeading3040();
		}
		if (hitrRec.isTrue()) {
			accumulate3110();
			if (!summaryOnlyReport.isTrue()) {
				printDetailLine3100();
			}
		}
		if (hifdRec.isTrue()
		&& isEQ(wsaaLastRecType,"HIFD")) {
			/*        Clock up number of funds without trans (HITRs)*/
			contotrec.totno.set(CT05);
			contotrec.totval.set(1);
			callContot001();
		}
		setControlBreak3010();
	}

protected void setControlBreak3010()
	{
		/*SET-CONTROL-BREAK*/
		wsaaLastRecType.set(sqlHitxpfInner.sqlRectype[isql.toInt()]);
		wsaaZintbfnd.set(sqlHitxpfInner.sqlZintbfnd[isql.toInt()]);
		indOn.setTrue(10);
		/*EXIT*/
	}

protected void setUpFundDetails3020()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(sqlHitxpfInner.sqlChdrcoy[isql.toInt()]);
		itemIO.setItemtabl(T5515);
		itemIO.setItemitem(sqlHitxpfInner.sqlZintbfnd[isql.toInt()]);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set(CHECK);
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,Varcom.oK)) {
			longdesc.fill("?");
			longdesc1.fill("?");
		}
		else {
			longdesc.set(getdescrec.longdesc);
			longdesc1.set(getdescrec.longdesc);
		}
		/* Get the fund currency from table T5515.*/
		itemIO.setFunction(Varcom.readr);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(T5515);
		itemIO.setItemitem(sqlHitxpfInner.sqlZintbfnd[isql.toInt()]);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		fndcurr.set(t5515rec.currcode);
		fndcurr1.set(t5515rec.currcode);
		/*    Look up currency description*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(T3629);
		itemIO.setItemitem(t5515rec.currcode);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set(CHECK);
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,Varcom.oK)) {
			curdesc.fill("?");
		}
		else {
			curdesc.set(getdescrec.longdesc);
		}
		wsaaBeforeTotVal.set(sqlHitxpfInner.sqlZtotfndval[isql.toInt()]);
	}

protected void summaryPageHeading3030()
	{
		/*SUMMARY-PAGE-HEADING*/
		company1.set(bsprIO.getCompany());
		companynm1.set(wsaaCompanynm);
		printerFile2.printRh517sh1(rh517sh1Record, indicArea);
		/*    Control Total on number of summary report pages written.*/
		contotrec.totno.set(CT07);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void detailPageHeading3040()
	{
		/*DETAIL-PAGE-HEADING*/
		company.set(bsprIO.getCompany());
		companynm.set(wsaaCompanynm);
		zintbfnd.set(sqlHitxpfInner.sqlZintbfnd[isql.toInt()]);
		/*PRINT-RH517DH1*/
		printerFile1.printRh517dh1(rh517dh1Record, indicArea);
		contotrec.totno.set(CT06);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void detailFooting3050()
	{
		fundvalb.set(wsaaBeforeTotVal);
		wsaaNewTotValue.add(wsaaBeforeTotVal);
		fundvalc.set(wsaaNewTotValue);
		/* Calculate the difference between the total fund value*/
		/* BEFORE the transaction and AFTER.*/
		compute(zvaldiff, 6).setRounded(sub(wsaaNewTotValue,wsaaBeforeTotVal));
		/* Calculate the difference as a percentage change.*/
		if (isNE(wsaaBeforeTotVal,ZERO)
		&& isNE(wsaaNewTotValue,ZERO)) {
			compute(wsaaPrcntDiff, 6).setRounded(mult((div(wsaaBeforeTotVal,wsaaNewTotValue)),100));
		}
		else {
			wsaaPrcntDiff.set(100);
		}
		prcntdiff.set(wsaaPrcntDiff);
		prcntdiff1.set(wsaaPrcntDiff);
		printerFile1.printRh517dt1(rh517dt1Record, indicArea);
		/* Control Total on number of total lines printed.*/
		contotrec.totno.set(CT03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void summaryLine3060()
	{
		/*SUMMARY-LINE*/
		zintbfnd1.set(wsaaZintbfnd);
		ztranval.set(wsaaNewTotValue);
		printerFile2.printRh517sd1(rh517sd1Record, indicArea);
		/*EXIT*/
	}

protected void zeroiseAccumulations3070()
	{
		/*ZEROISE-ACCUMULATIONS*/
		wsaaBeforeTotVal.set(ZERO);
		wsaaNewTotValue.set(ZERO);
		wsaaPrcntDiff.set(ZERO);
		/*EXIT*/
	}

protected void printDetailLine3100()
	{
		datcon1rec.intDate.set(sqlHitxpfInner.sqlEffdate[isql.toInt()]);
		datcon1rec.function.set(Varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,Varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		rh517dd1RecordInner.effdate.set(datcon1rec.extDate);
		if (isNE(sqlHitxpfInner.sqlChdrnum[isql.toInt()], wsaaLastChdrnum)
		|| isNE(sqlHitxpfInner.sqlChdrcoy[isql.toInt()], wsaaLastChdrcoy)) {
			wsaaLastChdrnum.set(sqlHitxpfInner.sqlChdrnum[isql.toInt()]);
			wsaaLastChdrcoy.set(sqlHitxpfInner.sqlChdrcoy[isql.toInt()]);
			indOn.setTrue(10);
		}
		else {
			indOff.setTrue(10);
		}
		rh517dd1RecordInner.chdrnum.set(sqlHitxpfInner.sqlChdrnum[isql.toInt()]);
		rh517dd1RecordInner.life.set(sqlHitxpfInner.sqlLife[isql.toInt()]);
		rh517dd1RecordInner.coverage.set(sqlHitxpfInner.sqlCoverage[isql.toInt()]);
		rh517dd1RecordInner.rider.set(sqlHitxpfInner.sqlRider[isql.toInt()]);
		rh517dd1RecordInner.plnsfx.set(sqlHitxpfInner.sqlPlnsfx[isql.toInt()]);
		rh517dd1RecordInner.batctrcde.set(sqlHitxpfInner.sqlBatctrcde[isql.toInt()]);
		rh517dd1RecordInner.prcseq.set(sqlHitxpfInner.sqlPrcseq[isql.toInt()]);
		printerFile1.printRh517dd1(rh517dd1RecordInner.rh517dd1Record, indicArea);
		contotrec.totno.set(CT02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void accumulate3110()
	{
		rh517dd1RecordInner.ztranval.set(ZERO);
		rh517dd1RecordInner.convrate.set(ZERO);
		if (isNE(t5515rec.currcode, sqlHitxpfInner.sqlCntcurr[isql.toInt()])) {
			rh517dd1RecordInner.cntcurr.set(sqlHitxpfInner.sqlCntcurr[isql.toInt()]);
		}
		else {
			rh517dd1RecordInner.cntcurr.set(SPACES);
		}
		/*    Firstly check if we have a "complete" HITR. This will be*/
		/*    the case for reversals as the original HITR will have been*/
		/*    through unit dealing and will have completed amounts.*/
		if (isNE(sqlHitxpfInner.sqlFundamnt[isql.toInt()], ZERO)
		&& isNE(sqlHitxpfInner.sqlCntamnt[isql.toInt()], ZERO)) {
			rh517dd1RecordInner.ztranval.set(sqlHitxpfInner.sqlFundamnt[isql.toInt()]);
			if (isNE(t5515rec.currcode, sqlHitxpfInner.sqlCntcurr[isql.toInt()])) {
				rh517dd1RecordInner.convrate.setRounded(sqlHitxpfInner.sqlFundrate[isql.toInt()]);
			}
		}
		if (isNE(sqlHitxpfInner.sqlCntamnt[isql.toInt()], ZERO)
		&& isEQ(sqlHitxpfInner.sqlFundamnt[isql.toInt()], ZERO)) {
			if (isEQ(t5515rec.currcode, sqlHitxpfInner.sqlCntcurr[isql.toInt()])) {
				rh517dd1RecordInner.ztranval.set(sqlHitxpfInner.sqlCntamnt[isql.toInt()]);
				rh517dd1RecordInner.convrate.set(ZERO);
			}
			else {
				convertToFundCurr3130();
				rh517dd1RecordInner.convrate.setRounded(conlinkrec.rateUsed);
				rh517dd1RecordInner.ztranval.set(conlinkrec.amountOut);
			}
		}
		if (isEQ(sqlHitxpfInner.sqlCntamnt[isql.toInt()], ZERO)
		&& isNE(sqlHitxpfInner.sqlFundamnt[isql.toInt()], ZERO)) {
			rh517dd1RecordInner.ztranval.set(sqlHitxpfInner.sqlFundamnt[isql.toInt()]);
			if (isEQ(t5515rec.currcode, sqlHitxpfInner.sqlCntcurr[isql.toInt()])) {
				rh517dd1RecordInner.convrate.set(ZERO);
			}
			else {
				convertToCntCurr3140();
				rh517dd1RecordInner.convrate.setRounded(conlinkrec.rateUsed);
			}
		}
		if (isEQ(sqlHitxpfInner.sqlCntamnt[isql.toInt()], ZERO)
		&& isEQ(sqlHitxpfInner.sqlFundamnt[isql.toInt()], ZERO)
		&& isNE(sqlHitxpfInner.sqlPersur[isql.toInt()], ZERO)) {
			calcSurrenderValue3120();
		}
		wsaaNewTotValue.add(rh517dd1RecordInner.ztranval);
	}

protected void calcSurrenderValue3120()
	{
		/* Read HITS for the Current Fund Balance.*/
		/* There is no need to do a currency conversion here since*/
		/* all HITS amounts are in the fund currency.*/
		
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(sqlHitxpfInner.sqlChdrcoy[isql.toInt()]);
		hitsIO.setChdrnum(sqlHitxpfInner.sqlChdrnum[isql.toInt()]);
		hitsIO.setLife(sqlHitxpfInner.sqlLife[isql.toInt()]);
		hitsIO.setCoverage(sqlHitxpfInner.sqlCoverage[isql.toInt()]);
		hitsIO.setRider(sqlHitxpfInner.sqlRider[isql.toInt()]);
		hitsIO.setPlanSuffix(sqlHitxpfInner.sqlPlnsfx[isql.toInt()]);
		hitsIO.setZintbfnd(sqlHitxpfInner.sqlZintbfnd[isql.toInt()]);
		hitsIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),Varcom.oK)
		&& isNE(hitsIO.getStatuz(),Varcom.mrnf)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		if (isEQ(hitsIO.getStatuz(),Varcom.mrnf)) {
			conlogrec.error.set(HL10);
			conlogrec.params.set(hitsIO.getParams());
			callConlog003();
			hitsIO.setZcurprmbal(ZERO);
		}
		compute(rh517dd1RecordInner.ztranval, 3).setRounded(mult(hitsIO.getZcurprmbal(), 
				(div(sqlHitxpfInner.sqlPersur[isql.toInt()], 100))));
		compute(rh517dd1RecordInner.ztranval, 0).set(mult(rh517dd1RecordInner.ztranval, -1));
	}

protected void convertToFundCurr3130()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.currIn.set(sqlHitxpfInner.sqlCntcurr[isql.toInt()]);
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.currOut.set(t5515rec.currcode);
		conlinkrec.amountIn.set(sqlHitxpfInner.sqlCntamnt[isql.toInt()]);
		conlinkrec.amountOut.set(ZERO);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,Varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* MOVE T5515-CURRCODE         TO ZRDP-CURRENCY.                */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(t5515rec.currcode);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

protected void convertToCntCurr3140()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.currIn.set(t5515rec.currcode);
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.currOut.set(sqlHitxpfInner.sqlCntcurr[isql.toInt()]);
		conlinkrec.amountIn.set(sqlHitxpfInner.sqlFundamnt[isql.toInt()]);
		conlinkrec.amountOut.set(ZERO);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,Varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* MOVE SQL-CNTCURR (ISQL)     TO ZRDP-CURRENCY.                */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(sqlHitxpfInner.sqlCntcurr[isql.toInt()]);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

@Override
protected void commit3500()
	{
		/*COMMIT*/
		/** No additional commitment processing in here.*/
		/*EXIT*/
	}

@Override
protected void rollback3600()
	{
		/*ROLLBACK*/
		/** No additional rollback processing in here.*/
		/*EXIT*/
	}

@Override
protected void close4000()
	{
		/*CLOSE-FILES*/
		detailFooting3050();
		summaryLine3060();
		printerFile1.printRh517e01(rh517e01Record, indicArea);
		printerFile2.printRh517e02(rh517e02Record, indicArea);
		printerFile1.close();
		printerFile2.close();
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(Varcom.oK);
		/*EXIT*/
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(ESQL);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure SQL-HITXPF--INNER
 */
private static final class SqlHitxpfInner { 

		/* SQL-HITXPF */
	private FixedLengthStringData[] sqlData = FLSInittedArray (1000, 87);
	private FixedLengthStringData[] sqlChdrcoy = FLSDArrayPartOfArrayStructure(1, sqlData, 0);
	private FixedLengthStringData[] sqlChdrnum = FLSDArrayPartOfArrayStructure(8, sqlData, 1);
	private FixedLengthStringData[] sqlLife = FLSDArrayPartOfArrayStructure(2, sqlData, 9);
	private FixedLengthStringData[] sqlCoverage = FLSDArrayPartOfArrayStructure(2, sqlData, 11);
	private FixedLengthStringData[] sqlRider = FLSDArrayPartOfArrayStructure(2, sqlData, 13);
	private PackedDecimalData[] sqlPlnsfx = PDArrayPartOfArrayStructure(4, 0, sqlData, 15);
	private FixedLengthStringData[] sqlZintbfnd = FLSDArrayPartOfArrayStructure(4, sqlData, 18);
	private PackedDecimalData[] sqlTranno = PDArrayPartOfArrayStructure(5, 0, sqlData, 22);
	private FixedLengthStringData[] sqlBatctrcde = FLSDArrayPartOfArrayStructure(4, sqlData, 25);
	private PackedDecimalData[] sqlEffdate = PDArrayPartOfArrayStructure(8, 0, sqlData, 29);
	private FixedLengthStringData[] sqlCntcurr = FLSDArrayPartOfArrayStructure(3, sqlData, 34);
	private FixedLengthStringData[] sqlFdbkind = FLSDArrayPartOfArrayStructure(1, sqlData, 37);
	private PackedDecimalData[] sqlCntamnt = PDArrayPartOfArrayStructure(17, 2, sqlData, 38);
	private PackedDecimalData[] sqlFundamnt = PDArrayPartOfArrayStructure(17, 2, sqlData, 47);
	private PackedDecimalData[] sqlZtotfndval = PDArrayPartOfArrayStructure(17, 2, sqlData, 56);
	private FixedLengthStringData[] sqlCnttyp = FLSDArrayPartOfArrayStructure(3, sqlData, 65);
	private PackedDecimalData[] sqlPrcseq = PDArrayPartOfArrayStructure(3, 0, sqlData, 68);
	private PackedDecimalData[] sqlPersur = PDArrayPartOfArrayStructure(5, 2, sqlData, 70);
	private PackedDecimalData[] sqlFundrate = PDArrayPartOfArrayStructure(18, 9, sqlData, 73);
	private FixedLengthStringData[] sqlRectype = FLSDArrayPartOfArrayStructure(4, sqlData, 83);
}
/*
 * Class transformed  from Data Structure RH517DD1-RECORD--INNER
 */
private static final class Rh517dd1RecordInner { 

	private FixedLengthStringData rh517dd1Record = new FixedLengthStringData(68);
	private FixedLengthStringData rh517dd1O = new FixedLengthStringData(68).isAPartOf(rh517dd1Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rh517dd1O, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rh517dd1O, 8);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rh517dd1O, 10);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rh517dd1O, 12);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(rh517dd1O, 14);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(rh517dd1O, 18);
	private ZonedDecimalData prcseq = new ZonedDecimalData(3, 0).isAPartOf(rh517dd1O, 22);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(rh517dd1O, 25);
	private ZonedDecimalData ztranval = new ZonedDecimalData(17, 2).isAPartOf(rh517dd1O, 35);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rh517dd1O, 52);
	private ZonedDecimalData convrate = new ZonedDecimalData(13, 4).isAPartOf(rh517dd1O, 55);
}
}
