package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class Hitddrykey extends ExternalData {

		public FixedLengthStringData wskyHitddryFileKey = new FixedLengthStringData(256);
		public FixedLengthStringData wskyHitddryKey = new FixedLengthStringData(256).isAPartOf(wskyHitddryFileKey, 0, REDEFINE);
		public FixedLengthStringData wskyHitddryChdrcoy = new FixedLengthStringData(1).isAPartOf(wskyHitddryKey, 0);
		public FixedLengthStringData wskyHitddryChdrnum = new FixedLengthStringData(8).isAPartOf(wskyHitddryKey, 1);
		public FixedLengthStringData wskyHitddryLife = new FixedLengthStringData(2).isAPartOf(wskyHitddryKey, 9);
		public FixedLengthStringData wskyHitddryCoverage = new FixedLengthStringData(2).isAPartOf(wskyHitddryKey, 11);
		public FixedLengthStringData wskyHitddryRider = new FixedLengthStringData(2).isAPartOf(wskyHitddryKey, 13);
		public PackedDecimalData wskyHitddryPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wskyHitddryKey, 15);
		public FixedLengthStringData wskyHitddryZintbfnd = new FixedLengthStringData(4).isAPartOf(wskyHitddryKey, 18);
		public FixedLengthStringData filler = new FixedLengthStringData(234).isAPartOf(wskyHitddryKey, 22, FILLER);

		public void initialize() {
			COBOLFunctions.initialize(wskyHitddryFileKey);
		}	
		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			wskyHitddryFileKey.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}
	}

