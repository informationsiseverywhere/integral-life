package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HitrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:30
 * Class transformed from HITRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HitrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 253;
	public FixedLengthStringData hitrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hitrpfRecord = hitrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hitrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hitrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hitrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hitrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hitrrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hitrrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(hitrrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(hitrrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(hitrrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(hitrrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(hitrrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(hitrrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(hitrrec);
	public FixedLengthStringData zintbfnd = DD.zintbfnd.copy().isAPartOf(hitrrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(hitrrec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(hitrrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(hitrrec);
	public FixedLengthStringData zrectyp = DD.zrectyp.copy().isAPartOf(hitrrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hitrrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hitrrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(hitrrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(hitrrec);
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(hitrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hitrrec);
	public FixedLengthStringData covdbtind = DD.covdbtind.copy().isAPartOf(hitrrec);
	public FixedLengthStringData zintappind = DD.zintappind.copy().isAPartOf(hitrrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(hitrrec);
	public FixedLengthStringData triggerModule = DD.triger.copy().isAPartOf(hitrrec);
	public FixedLengthStringData triggerKey = DD.trigky.copy().isAPartOf(hitrrec);
	public FixedLengthStringData switchIndicator = DD.swchind.copy().isAPartOf(hitrrec);
	public PackedDecimalData surrenderPercent = DD.persur.copy().isAPartOf(hitrrec);
	public PackedDecimalData svp = DD.svp.copy().isAPartOf(hitrrec);
	public PackedDecimalData zlstintdte = DD.zlstintdte.copy().isAPartOf(hitrrec);
	public PackedDecimalData zintrate = DD.zintrate.copy().isAPartOf(hitrrec);
	public PackedDecimalData zinteffdt = DD.zinteffdt.copy().isAPartOf(hitrrec);
	public FixedLengthStringData zintalloc = DD.zintalloc.copy().isAPartOf(hitrrec);
	public PackedDecimalData inciNum = DD.incinum.copy().isAPartOf(hitrrec);
	public PackedDecimalData inciPerd01 = DD.inciperd.copy().isAPartOf(hitrrec);
	public PackedDecimalData inciPerd02 = DD.inciperd.copy().isAPartOf(hitrrec);
	public PackedDecimalData inciprm01 = DD.inciprm.copy().isAPartOf(hitrrec);
	public PackedDecimalData inciprm02 = DD.inciprm.copy().isAPartOf(hitrrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(hitrrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(hitrrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(hitrrec);
	public FixedLengthStringData genlcde = DD.genlcde.copy().isAPartOf(hitrrec);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(hitrrec);
	public PackedDecimalData scheduleNumber = DD.bschednum.copy().isAPartOf(hitrrec);
	public FixedLengthStringData fundpool = DD.fundpool.copy().isAPartOf(hitrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hitrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hitrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hitrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HitrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HitrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HitrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HitrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HitrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HITRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"PRCSEQ, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"ZINTBFND, " +
							"FUNDAMNT, " +
							"FNDCURR, " +
							"FUNDRATE, " +
							"ZRECTYP, " +
							"EFFDATE, " +
							"TRANNO, " +
							"CNTCURR, " +
							"CNTAMNT, " +
							"CNTTYP, " +
							"CRTABLE, " +
							"COVDBTIND, " +
							"ZINTAPPIND, " +
							"FDBKIND, " +
							"TRIGER, " +
							"TRIGKY, " +
							"SWCHIND, " +
							"PERSUR, " +
							"SVP, " +
							"ZLSTINTDTE, " +
							"ZINTRATE, " +
							"ZINTEFFDT, " +
							"ZINTALLOC, " +
							"INCINUM, " +
							"INCIPERD01, " +
							"INCIPERD02, " +
							"INCIPRM01, " +
							"INCIPRM02, " +
							"USTMNO, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"GENLCDE, " +
							"BSCHEDNAM, " +
							"BSCHEDNUM, " +
							"FUNDPOOL," +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     procSeqNo,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     zintbfnd,
                                     fundAmount,
                                     fundCurrency,
                                     fundRate,
                                     zrectyp,
                                     effdate,
                                     tranno,
                                     cntcurr,
                                     contractAmount,
                                     cnttyp,
                                     crtable,
                                     covdbtind,
                                     zintappind,
                                     feedbackInd,
                                     triggerModule,
                                     triggerKey,
                                     switchIndicator,
                                     surrenderPercent,
                                     svp,
                                     zlstintdte,
                                     zintrate,
                                     zinteffdt,
                                     zintalloc,
                                     inciNum,
                                     inciPerd01,
                                     inciPerd02,
                                     inciprm01,
                                     inciprm02,
                                     ustmno,
                                     sacscode,
                                     sacstyp,
                                     genlcde,
                                     scheduleName,
                                     scheduleNumber,
                                     fundpool,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		procSeqNo.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		zintbfnd.clear();
  		fundAmount.clear();
  		fundCurrency.clear();
  		fundRate.clear();
  		zrectyp.clear();
  		effdate.clear();
  		tranno.clear();
  		cntcurr.clear();
  		contractAmount.clear();
  		cnttyp.clear();
  		crtable.clear();
  		covdbtind.clear();
  		zintappind.clear();
  		feedbackInd.clear();
  		triggerModule.clear();
  		triggerKey.clear();
  		switchIndicator.clear();
  		surrenderPercent.clear();
  		svp.clear();
  		zlstintdte.clear();
  		zintrate.clear();
  		zinteffdt.clear();
  		zintalloc.clear();
  		inciNum.clear();
  		inciPerd01.clear();
  		inciPerd02.clear();
  		inciprm01.clear();
  		inciprm02.clear();
  		ustmno.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		genlcde.clear();
  		scheduleName.clear();
  		scheduleNumber.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		fundpool.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHitrrec() {
  		return hitrrec;
	}

	public FixedLengthStringData getHitrpfRecord() {
  		return hitrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHitrrec(what);
	}

	public void setHitrrec(Object what) {
  		this.hitrrec.set(what);
	}

	public void setHitrpfRecord(Object what) {
  		this.hitrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hitrrec.getLength());
		result.set(hitrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}