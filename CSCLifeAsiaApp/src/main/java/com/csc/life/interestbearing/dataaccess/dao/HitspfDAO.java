package com.csc.life.interestbearing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HitspfDAO extends BaseDAO<Hitspf> {
    public List<Hitspf> searchHitsRecord(Hitspf hitsPf);
    
    public Map<String, List<Hitspf>> searchHitsRecordByChdrnum(List<String> chdrnum);
    
    public Hitspf getHitsRecord(Hitspf hitsPf);
    
    public void bulkInsertHit(List<Hitspf> hitspfList);
    
    public void bulkUpdateHits(List<Hitspf> hitspfList);
    
    public Map<String, List<Hitspf>> searchHitsRecordByChdrcoy(String chdrcoy, List<String> chdrnumList);
}