package com.csc.life.interestbearing.dataaccess.model;

import java.math.BigDecimal;

public class Hitspf {
    private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int planSuffix;
    private String zintbfnd;
    private BigDecimal zcurprmbal;
    private BigDecimal zlstupdt;
    private String userProfile;
    private String jobName;
    private String datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getZintbfnd() {
		return zintbfnd;
	}
	public void setZintbfnd(String zintbfnd) {
		this.zintbfnd = zintbfnd;
	}
	public BigDecimal getZcurprmbal() {
		return zcurprmbal;
	}
	public void setZcurprmbal(BigDecimal zcurprmbal) {
		this.zcurprmbal = zcurprmbal;
	}
	public BigDecimal getZlstupdt() {
		return zlstupdt;
	}
	public void setZlstupdt(BigDecimal zlstupdt) {
		this.zlstupdt = zlstupdt;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}

    
    
}