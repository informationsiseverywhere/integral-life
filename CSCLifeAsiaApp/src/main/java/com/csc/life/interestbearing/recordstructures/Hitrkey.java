package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:39
 * Description:
 * Copybook name: HITRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrKey = new FixedLengthStringData(64).isAPartOf(hitrFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrKey, 0);
  	public FixedLengthStringData hitrChdrnum = new FixedLengthStringData(8).isAPartOf(hitrKey, 1);
  	public FixedLengthStringData hitrLife = new FixedLengthStringData(2).isAPartOf(hitrKey, 9);
  	public FixedLengthStringData hitrCoverage = new FixedLengthStringData(2).isAPartOf(hitrKey, 11);
  	public FixedLengthStringData hitrRider = new FixedLengthStringData(2).isAPartOf(hitrKey, 13);
  	public PackedDecimalData hitrPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitrKey, 15);
  	public FixedLengthStringData hitrZintbfnd = new FixedLengthStringData(4).isAPartOf(hitrKey, 18);
  	public PackedDecimalData hitrTranno = new PackedDecimalData(5, 0).isAPartOf(hitrKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(hitrKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}