/*
 * 
 * File: HitrextTempTblDAOImp.java
 * Date: May 05, 2017
 * Author: CSC
 * 
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.dataaccess.dao.impl;

import com.csc.life.interestbearing.dataaccess.dao.HitrextTempTblDAO;
import com.csc.life.interestbearing.dataaccess.model.HitrextTempTblDTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;

public abstract class HitrextTempTblDAOImpl extends BaseDAOImpl<HitrextTempTblDTO> implements HitrextTempTblDAO {
	
	protected final String HITREXT_COLUMNS = "CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, "
			+ "ZINTBFND, TRANNO, ZRECTYP, BATCTRCDE, EFFDATE, CNTCURR, FDBKIND, "
			+ "CNTAMNT, FUNDAMNT, CNTTYP, PRCSEQ, PERSUR, FUNDRATE";
	
	protected static final String DEBT = "DEBT";
	protected static final String SWCH = "SWCH";
	
}
