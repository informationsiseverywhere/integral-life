package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:40
 * Description:
 * Copybook name: HITRTRGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrtrgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrtrgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrtrgKey = new FixedLengthStringData(64).isAPartOf(hitrtrgFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrtrgChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrtrgKey, 0);
  	public FixedLengthStringData hitrtrgChdrnum = new FixedLengthStringData(8).isAPartOf(hitrtrgKey, 1);
  	public PackedDecimalData hitrtrgTranno = new PackedDecimalData(5, 0).isAPartOf(hitrtrgKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(hitrtrgKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrtrgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrtrgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}