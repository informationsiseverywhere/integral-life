package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:38
 * Description:
 * Copybook name: HITRCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrclmKey = new FixedLengthStringData(64).isAPartOf(hitrclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrclmChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrclmKey, 0);
  	public FixedLengthStringData hitrclmChdrnum = new FixedLengthStringData(8).isAPartOf(hitrclmKey, 1);
  	public FixedLengthStringData hitrclmLife = new FixedLengthStringData(2).isAPartOf(hitrclmKey, 9);
  	public FixedLengthStringData hitrclmCoverage = new FixedLengthStringData(2).isAPartOf(hitrclmKey, 11);
  	public FixedLengthStringData hitrclmRider = new FixedLengthStringData(2).isAPartOf(hitrclmKey, 13);
  	public PackedDecimalData hitrclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitrclmKey, 15);
  	public FixedLengthStringData hitrclmZintbfnd = new FixedLengthStringData(4).isAPartOf(hitrclmKey, 18);
  	public PackedDecimalData hitrclmTranno = new PackedDecimalData(5, 0).isAPartOf(hitrclmKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(hitrclmKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}