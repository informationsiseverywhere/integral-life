package com.csc.life.interestbearing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.interestbearing.dataaccess.dao.HidxpfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hidxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HidxpfDAOImpl extends BaseDAOImpl<Hidxpf> implements HidxpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(HidxpfDAOImpl.class);

	public List<Hidxpf> searchHidxRecord(String tableId, String memName, int batchExtractSize, int batchID) {

		StringBuilder sqlHidxSelect = new StringBuilder();
		sqlHidxSelect.append(" SELECT * FROM (");
		sqlHidxSelect
				.append("  SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,ZINTBFND,ZLSTINTDTE,ZNXTINTDTE,ZLSTFNDVAL,EFFDATE");
		sqlHidxSelect.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY ASC, CHDRNUM ASC)-1)/?) BATCHNUM ");
		sqlHidxSelect.append("  FROM ");
		sqlHidxSelect.append(tableId);
		sqlHidxSelect.append("   WHERE MEMBER_NAME = ? ");
		sqlHidxSelect.append(" ) MAIN WHERE batchnum = ? ");

		PreparedStatement psHidxSelect = getPrepareStatement(sqlHidxSelect.toString());
		ResultSet sqlhidxpf1rs = null;
		List<Hidxpf> hidxpfList = new ArrayList<>();
		try {
			psHidxSelect.setInt(1, batchExtractSize);
			psHidxSelect.setString(2, memName);
			psHidxSelect.setInt(3, batchID);

			sqlhidxpf1rs = executeQuery(psHidxSelect);
			while (sqlhidxpf1rs.next()) {
				Hidxpf hidxpf = new Hidxpf();
				hidxpf.setUniqueNumber(sqlhidxpf1rs.getLong("UNIQUE_NUMBER"));
				hidxpf.setChdrcoy(sqlhidxpf1rs.getString("CHDRCOY"));
				hidxpf.setChdrnum(sqlhidxpf1rs.getString("CHDRNUM"));
				hidxpf.setLife(sqlhidxpf1rs.getString("LIFE"));
				hidxpf.setCoverage(sqlhidxpf1rs.getString("COVERAGE"));
				hidxpf.setRider(sqlhidxpf1rs.getString("RIDER"));
				hidxpf.setPlanSuffix(sqlhidxpf1rs.getInt("PLNSFX"));
				hidxpf.setZintbfnd(sqlhidxpf1rs.getString("ZINTBFND"));
				hidxpf.setZlstintdte(sqlhidxpf1rs.getInt("ZLSTINTDTE"));
				hidxpf.setZnxtintdte(sqlhidxpf1rs.getInt("ZNXTINTDTE"));
				hidxpf.setZlstfndval(sqlhidxpf1rs.getBigDecimal("ZLSTFNDVAL"));
				hidxpf.setEffdate(sqlhidxpf1rs.getInt("EFFDATE"));
				hidxpfList.add(hidxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchHidxRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psHidxSelect, sqlhidxpf1rs);
		}
		return hidxpfList;

	}

}