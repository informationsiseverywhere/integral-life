package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HitspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:31
 * Class transformed from HITSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HitspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 83;
	public FixedLengthStringData hitsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hitspfRecord = hitsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hitsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hitsrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hitsrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hitsrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hitsrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hitsrec);
	public FixedLengthStringData zintbfnd = DD.zintbfnd.copy().isAPartOf(hitsrec);
	public PackedDecimalData zcurprmbal = DD.zcurprmbal.copy().isAPartOf(hitsrec);
	public PackedDecimalData zlstupdt = DD.zlstupdt.copy().isAPartOf(hitsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hitsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hitsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hitsrec);
	public FixedLengthStringData fundpool = DD.fundpool.copy().isAPartOf(hitsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HitspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HitspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HitspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HitspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HitspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HITSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"ZINTBFND, " +
							"ZCURPRMBAL, " +
							"ZLSTUPDT, " +
							"FUNDPOOL, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     zintbfnd,
                                     zcurprmbal,
                                     zlstupdt,
                                     fundpool,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		zintbfnd.clear();
  		zcurprmbal.clear();
  		zlstupdt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		fundpool.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHitsrec() {
  		return hitsrec;
	}

	public FixedLengthStringData getHitspfRecord() {
  		return hitspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHitsrec(what);
	}

	public void setHitsrec(Object what) {
  		this.hitsrec.set(what);
	}

	public void setHitspfRecord(Object what) {
  		this.hitspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hitsrec.getLength());
		result.set(hitsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}