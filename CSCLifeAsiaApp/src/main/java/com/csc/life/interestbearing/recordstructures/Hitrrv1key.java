package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:40
 * Description:
 * Copybook name: HITRRV1KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrrv1key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrrv1FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrrv1Key = new FixedLengthStringData(64).isAPartOf(hitrrv1FileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrrv1Chdrcoy = new FixedLengthStringData(1).isAPartOf(hitrrv1Key, 0);
  	public FixedLengthStringData hitrrv1Chdrnum = new FixedLengthStringData(8).isAPartOf(hitrrv1Key, 1);
  	public FixedLengthStringData hitrrv1Life = new FixedLengthStringData(2).isAPartOf(hitrrv1Key, 9);
  	public FixedLengthStringData hitrrv1Coverage = new FixedLengthStringData(2).isAPartOf(hitrrv1Key, 11);
  	public PackedDecimalData hitrrv1PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitrrv1Key, 13);
  	public PackedDecimalData hitrrv1Tranno = new PackedDecimalData(5, 0).isAPartOf(hitrrv1Key, 16);
  	public PackedDecimalData hitrrv1ProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(hitrrv1Key, 19);
  	public FixedLengthStringData hitrrv1Zintbfnd = new FixedLengthStringData(4).isAPartOf(hitrrv1Key, 21);
  	public FixedLengthStringData hitrrv1Zrectyp = new FixedLengthStringData(1).isAPartOf(hitrrv1Key, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(hitrrv1Key, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrrv1FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrrv1FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}