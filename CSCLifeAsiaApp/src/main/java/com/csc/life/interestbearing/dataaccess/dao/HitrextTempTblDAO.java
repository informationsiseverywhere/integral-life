package com.csc.life.interestbearing.dataaccess.dao;

import com.csc.life.interestbearing.dataaccess.model.HitrextTempTblDTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HitrextTempTblDAO extends BaseDAO<HitrextTempTblDTO> {
	
	public int copyDataToTempTable(String sysParam01, String sourceTableName, 
			String tempTableName, int noOfSubThread, HitrextTempTblDTO hitrextDTO);
	
}
