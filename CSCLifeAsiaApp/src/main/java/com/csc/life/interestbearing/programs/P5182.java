/*
 * File: P5182.java
 * Date: December 3, 2013 2:56:42 AM ICT
 * Author: CSC
 *
 * Class transformed from P5182.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.IjnlTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

//Diary ILPI-119 START by dpuhawan
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import com.csc.diary.procedures.Dryproces;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.utility.Validator;
import com.csc.smart.dataaccess.ItemTableDAM;
//END by dpuhawan


/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Interest Fund Journals End Processing.
*
* The program converts Temporary Interest Fund Journal records
* created by the previous program into Interest Transaction
* records and creates a Transaction record for the journalling.
*
* If breakout processing is neccessary for the contract, BRKOUT
* module need to be called.
*
* Processing Required
* -------------------
*
* Increment the transaction number on the contract header
* record and write a PTRN transaction record.
*
* Loop through the IJNL file to find the lowest plan suffix
* If breakout processing is required call BRKOUT module.
*
* If breakout is not required, again loop through the IJNL
* file for all records for this contract.
*
* For each record found, convert the IJNL to an HITR.
*
****************************************************************** ****
* </pre>
*/
public class P5182 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5182");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaLowestPlnsfx = new PackedDecimalData(4, 0);
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String ptrnrec = "PTRNREC   ";
	private static final String ijnlrec = "IJNLREC";
	private static final String hitralorec = "HITRALOREC";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private IjnlTableDAM ijnlIO = new IjnlTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	
	//Diary ILPI-119 START by dpuhawan
	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	
	private T7508rec t7508rec = new T7508rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private TablesInner tablesInner = new TablesInner();	
	private ItemTableDAM itemIO = new ItemTableDAM();
	//END by dpuhawan	
	
	//ILIFE-8137 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit3190
	}

	public P5182() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		/* Get the effective date for subsequent use.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* Retrieve and release the Contract Header record for*/
		/* subsequent updating.*/
		chdrmjaio5000();
		//chdrmjaIO.setFunction(varcom.rlse);
		chdrpfDAO.deleteCacheObject(chdrpf);
	//	chdrmjaio5000();
		/*EXIT*/
	}

protected void screenEdit2000()
	{
		/*EDIT*/
		/* No processing required.*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		begnh3010();
		//Diary ILPI-119 START by dpuhawan  
		diaryProcessing3200(); 	
		//END by dpuhawan
	}

protected void begnh3010()
	{
		/* Update the Contract Header record before writing a PTRN*/
		updateContractHeader3200();
		writePtrn3300();
		/* Check to see if breakout is neccessary. If it is then*/
		/* we need to call BRKOUT module.*/
		wsaaLowestPlnsfx.set(9999);
		ijnlIO.setParams(SPACES);
		ijnlIO.setStatuz(varcom.oK);
		ijnlIO.setChdrcoy(chdrpf.getChdrcoy());
		ijnlIO.setChdrnum(chdrpf.getChdrnum());
		ijnlIO.setPlanSuffix(0);
		ijnlIO.setSeqno(0);
		ijnlIO.setFunction(varcom.begn);
		while ( !(isEQ(ijnlIO.getStatuz(), varcom.endp))) {
			findPlanSuffix3500();
		}

		if (isLTE(wsaaLowestPlnsfx, chdrpf.getPolsum())) {
			processBreakout6000();
		}
		/* Loop through the IJNL file for this contract.  For each*/
		/* record found, create an HITR record before deleting the IJNL*/
		/* record.*/
		ijnlIO.setParams(SPACES);
		ijnlIO.setStatuz(varcom.oK);
		ijnlIO.setChdrcoy(chdrpf.getChdrcoy());
		ijnlIO.setChdrnum(chdrpf.getChdrnum());
		ijnlIO.setPlanSuffix(0);
		ijnlIO.setSeqno(0);
		ijnlIO.setFunction(varcom.begnh);
		while ( !(isEQ(ijnlIO.getStatuz(), varcom.endp))) {
			createHitrLoop3100();
		}

		releaseSoftlock3400();
	}

protected void createHitrLoop3100()
	{
		try {
			findIjnl3110();
			createHitr3120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void findIjnl3110()
	{
		ijnlio5100();
		if (isEQ(ijnlIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit3190);
		}
	}

protected void createHitr3120()
	{
		hitraloIO.setRecKeyData(SPACES);
		hitraloIO.setRecNonKeyData(SPACES);
		hitraloIO.setChdrcoy(ijnlIO.getChdrcoy());
		hitraloIO.setChdrnum(ijnlIO.getChdrnum());
		hitraloIO.setLife(ijnlIO.getLife());
		hitraloIO.setCoverage(ijnlIO.getCoverage());
		hitraloIO.setRider(ijnlIO.getRider());
		hitraloIO.setPlanSuffix(ijnlIO.getPlanSuffix());
		hitraloIO.setZintbfnd(ijnlIO.getZintbfnd());
		hitraloIO.setZrectyp(ijnlIO.getZrectyp());
		hitraloIO.setTranno(ijnlIO.getTranno());
		hitraloIO.setBatccoy(ijnlIO.getBatccoy());
		hitraloIO.setBatcbrn(ijnlIO.getBatcbrn());
		hitraloIO.setBatcactyr(ijnlIO.getBatcactyr());
		hitraloIO.setBatcactmn(ijnlIO.getBatcactmn());
		hitraloIO.setBatctrcde(ijnlIO.getBatctrcde());
		hitraloIO.setBatcbatch(ijnlIO.getBatcbatch());
		hitraloIO.setEffdate(ijnlIO.getEffdate());
		hitraloIO.setCnttyp(ijnlIO.getContractType());
		hitraloIO.setCrtable(ijnlIO.getCrtable());
		hitraloIO.setCntcurr(ijnlIO.getCntcurr());
		hitraloIO.setFundCurrency(ijnlIO.getFundCurrency());
		hitraloIO.setFundAmount(ijnlIO.getFundAmount());
		hitraloIO.setSacscode(ijnlIO.getSacscode());
		hitraloIO.setSacstyp(ijnlIO.getSacstype());
		hitraloIO.setGenlcde(ijnlIO.getGenlcde());
		hitraloIO.setProcSeqNo(ijnlIO.getProcSeqNo());
		hitraloIO.setSvp(1);
		hitraloIO.setZlstintdte(varcom.vrcmMaxDate);
		hitraloIO.setZinteffdt(varcom.vrcmMaxDate);
		hitraloIO.setFunction(varcom.writr);
		hitraloio5200();
		/* Delete the temporary IJNL record.*/
		//smalchi2 for ILIFE-2249 Starts
		ijnlIO.setFunction(varcom.delet);
		ijnlio5100(); 
		//ENDS
		ijnlIO.setFunction(varcom.nextr);
	}

protected void updateContractHeader3200()
	{
		readh3210();
		batchUpdate3250();
	}

protected void readh3210()
	{
		/* Increment the transaction number on the Contract Header.*/
		/*chdrmjaIO.setFunction(varcom.readh);
		chdrmjaio5000();*/
		//ILIFE-8137
		chdrpf = chdrpfDAO.getchdrRecordservunit(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (chdrpf == null) {
			fatalError600();
		}
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		chdrpfDAO.updatebyTran(chdrpf);
		/*chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaio5000();*/
	}

protected void batchUpdate3250()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		initialize(batcuprec.batcupRec);
		batcuprec.trancnt.set(1);
		batcuprec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.function.set(varcom.writs);
		batcupCall5300();
	}

protected void writePtrn3300()
	{
		writr3310();
	}

protected void writr3310()
	{
		/* Write a transaction record to the PTRN file.*/
		ptrnIO.setRecKeyData(SPACES);
		ptrnIO.setRecNonKeyData(SPACES);
		ptrnIO.setChdrcoy(chdrpf.getChdrcoy());
		ptrnIO.setChdrnum(chdrpf.getChdrnum());
		ptrnIO.setTranno(chdrpf.getTranno());
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		ptrnio5400();
	}

protected void releaseSoftlock3400()
	{
		/*UNLK*/
		initialize(sftlockrec.sftlockRec);
		sftlockrec.company.set(chdrpf.getChdrcoy());
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		sftlockCall5500();
		/*EXIT*/
	}

protected void findPlanSuffix3500()
	{
		/*READ*/
		ijnlio5100();
		if (isEQ(ijnlIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isLT(ijnlIO.getPlanSuffix(), wsaaLowestPlnsfx)
		&& isNE(ijnlIO.getPlanSuffix(), ZERO)) {
			wsaaLowestPlnsfx.set(ijnlIO.getPlanSuffix());
		}
		ijnlIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

//Diary ILPI-119 START by dpuhawan
protected void diaryProcessing3200()
{
	/* This section will determine if the DIARY system is present      */
	/* If so, the appropriate parameters are filled and the            */
	/* diary processor is called.                                      */
	wsaaT7508Cnttype.set(chdrpf.getCnttype());
	wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
	readT75085100();
	/* If item not found try other types of contract.                  */
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		wsaaT7508Cnttype.set("***");
		readT75085100();
	}
	/* If item not found no Diary Update Processing is Required.       */
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		return ;
	}
	t7508rec.t7508Rec.set(itemIO.getGenarea());
	drypDryprcRecInner.drypStatuz.set(varcom.oK);
	drypDryprcRecInner.onlineMode.setTrue();
	drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
	drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
	drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
	drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
	drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
	drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
	drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
	drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum());
	drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
	drypDryprcRecInner.drypEffectiveTime.set(ZERO);
	drypDryprcRecInner.drypFsuCompany.set("9");
	drypDryprcRecInner.drypProcSeqNo.set(100);
	drypDryprcRecInner.drypAplsupto.set(ZERO);
	drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
	/* Fill the parameters.                                            */
	drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
	drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
	drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
	drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
	drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
	drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
	drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
	drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
	drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
	drypDryprcRecInner.drypCpiDate.set(varcom.vrcmDate);
	drypDryprcRecInner.drypBbldate.set(varcom.vrcmDate);
	drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
	drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
	drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
	drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
	callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
	if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
		syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
		syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
		fatalError600();
	}
}
protected void readT75085100()
{
	start5110();
}

protected void start5110()
{
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setItemtabl(tablesInner.t7508);
	itemIO.setItemitem(wsaaT7508Key);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(itemIO.getStatuz());
		fatalError600();
	}
}
//END by dpuhawan

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void chdrmjaio5000()
	{
		/*CALL*/
		//ILIFE-8137
			chdrpf = chdrpfDAO.getCacheObject(chdrpf);
			if(null==chdrpf) {
				chdrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					syserrrec.statuz.set(chdrmjaIO.getStatuz());
					fatalError600();
				}
				else {
					chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
					if(null==chdrpf) {
						fatalError600();
					}
					else {
						chdrpfDAO.setCacheObject(chdrpf);
					}
				}
			}
		/*EXIT*/
	}

protected void ijnlio5100()
	{
		/*CALL*/
		ijnlIO.setFormat(ijnlrec);
		SmartFileCode.execute(appVars, ijnlIO);
		if (isNE(ijnlIO.getStatuz(), varcom.oK)
		&& isNE(ijnlIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ijnlIO.getStatuz());
			syserrrec.params.set(ijnlIO.getParams());
			fatalError600();
		}
		if (isNE(ijnlIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(ijnlIO.getChdrnum(), chdrpf.getChdrnum())) {
			ijnlIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void hitraloio5200()
	{
		/*CALL*/
		hitraloIO.setFormat(hitralorec);
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hitraloIO.getStatuz());
			syserrrec.params.set(hitraloIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void batcupCall5300()
	{
		/*CALL*/
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void ptrnio5400()
	{
		/*CALL*/
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void sftlockCall5500()
	{
		/*CALL*/
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void processBreakout6000()
	{
		start6010();
	}

protected void start6010()
	{
		/* Call the BRKOUT subroutine*/
		brkoutrec.outRec.set(SPACES);
		brkoutrec.brkChdrcoy.set(chdrpf.getChdrcoy());
		brkoutrec.brkChdrnum.set(chdrpf.getChdrnum());
		brkoutrec.brkOldSummary.set(chdrpf.getPolsum());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaLowestPlnsfx, 1));
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
			syserrrec.statuz.set(brkoutrec.brkStatuz);
			syserrrec.params.set(brkoutrec.outRec);
			fatalError600();
		}
	}
//ILPI-119 START by dpuhawan
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}

private static final class TablesInner {
	/* TABLES */
private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
}
//END by dpuhawan
}
