package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:02
 * Description:
 * Copybook name: IRATKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Iratkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData iratFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData iratKey = new FixedLengthStringData(64).isAPartOf(iratFileKey, 0, REDEFINE);
  	public FixedLengthStringData iratCompany = new FixedLengthStringData(1).isAPartOf(iratKey, 0);
  	public FixedLengthStringData iratUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(iratKey, 1);
  	public PackedDecimalData iratFromdate = new PackedDecimalData(8, 0).isAPartOf(iratKey, 5);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(iratKey, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(iratFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		iratFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}