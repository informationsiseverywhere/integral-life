package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:39
 * Description:
 * Copybook name: HITRINTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrintkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrintFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrintKey = new FixedLengthStringData(64).isAPartOf(hitrintFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrintChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrintKey, 0);
  	public FixedLengthStringData hitrintChdrnum = new FixedLengthStringData(8).isAPartOf(hitrintKey, 1);
  	public FixedLengthStringData hitrintLife = new FixedLengthStringData(2).isAPartOf(hitrintKey, 9);
  	public FixedLengthStringData hitrintCoverage = new FixedLengthStringData(2).isAPartOf(hitrintKey, 11);
  	public FixedLengthStringData hitrintRider = new FixedLengthStringData(2).isAPartOf(hitrintKey, 13);
  	public PackedDecimalData hitrintPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitrintKey, 15);
  	public FixedLengthStringData hitrintZintbfnd = new FixedLengthStringData(4).isAPartOf(hitrintKey, 18);
  	public PackedDecimalData hitrintEffdate = new PackedDecimalData(8, 0).isAPartOf(hitrintKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(37).isAPartOf(hitrintKey, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrintFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrintFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}