package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:01
 * Description:
 * Copybook name: IRATCHGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Iratchgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData iratchgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData iratchgKey = new FixedLengthStringData(64).isAPartOf(iratchgFileKey, 0, REDEFINE);
  	public FixedLengthStringData iratchgCompany = new FixedLengthStringData(1).isAPartOf(iratchgKey, 0);
  	public FixedLengthStringData iratchgUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(iratchgKey, 1);
  	public PackedDecimalData iratchgFromdate = new PackedDecimalData(8, 0).isAPartOf(iratchgKey, 5);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(iratchgKey, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(iratchgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		iratchgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}