package com.csc.life.interestbearing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH518.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh518Report extends SMARTReportLayout { 

	private FixedLengthStringData batctrcde = new FixedLengthStringData(4);
	private FixedLengthStringData bschednam = new FixedLengthStringData(10);
	private ZonedDecimalData bschednum = new ZonedDecimalData(8, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData schalph = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totfndamt = new ZonedDecimalData(18, 2);
	private FixedLengthStringData trandesc = new FixedLengthStringData(30);
	private FixedLengthStringData zintbfnd = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh518Report() {
		super();
	}


	/**
	 * Print the XML for Rh518d01
	 */
	public void printRh518d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(10).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		fundamnt.setFieldName("fundamnt");
		fundamnt.setInternal(subString(recordData, 9, 17));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 26, 10));
		batctrcde.setFieldName("batctrcde");
		batctrcde.setInternal(subString(recordData, 36, 4));
		trandesc.setFieldName("trandesc");
		trandesc.setInternal(subString(recordData, 40, 30));
		printLayout("Rh518d01",			// Record name
			new BaseData[]{			// Fields:
					chdrnum,
					fundamnt,
					effdate,
					batctrcde,
					trandesc
		}
		, new Object[] {			// indicators
			new Object[]{"ind10", indicArea.charAt(10)}
		}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh518e01
	 */
	public void printRh518e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rh518e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh518h01
	 */
	public void printRh518h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		bschednam.setFieldName("bschednam");
		bschednam.setInternal(subString(recordData, 1, 10));
		bschednum.setFieldName("bschednum");
		bschednum.setInternal(subString(recordData, 11, 8));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 19, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 20, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		zintbfnd.setFieldName("zintbfnd");
		zintbfnd.setInternal(subString(recordData, 50, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 54, 30));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 84, 3));
		curdesc.setFieldName("curdesc");
		curdesc.setInternal(subString(recordData, 87, 30));
		printLayout("Rh518h01",			// Record name
			new BaseData[]{			// Fields:
				bschednam,
				bschednum,
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr,
				zintbfnd,
				longdesc,
				fndcurr,
				curdesc
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for Rh518h02
	 */
	public void printRh518h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh518h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh518n01
	 */
	public void printRh518n01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(20).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		schalph.setFieldName("schalph");
		schalph.setInternal(subString(recordData, 1, 10));
		printLayout("Rh518n01",			// Record name
			new BaseData[]{			// Fields:
				schalph
			}
			, new Object[] {			// indicators
				new Object[]{"ind20", indicArea.charAt(20)}
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for Rh518t01
	 */
	public void printRh518t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		totfndamt.setFieldName("totfndamt");
		totfndamt.setInternal(subString(recordData, 1, 18));
		printLayout("Rh518t01",			// Record name
			new BaseData[]{			// Fields:
				totfndamt
			}
		);

		currentPrintLine.add(2);
	}


}
