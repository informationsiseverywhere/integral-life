package com.csc.life.interestbearing.dataaccess.model;

public class HitrextTempTblDTO {
	
	public HitrextTempTblDTO() {
	}

	private String chdrcoy;
	
	private String fromChdrnum;
	
	private String toChdrnum;
	
	private String fdbkind;
	
	private String covdbtind; 
	
	private String swchind; 
	
	private int effdate;

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getFromChdrnum() {
		return fromChdrnum;
	}

	public void setFromChdrnum(String fromChdrnum) {
		this.fromChdrnum = fromChdrnum;
	}

	public String getToChdrnum() {
		return toChdrnum;
	}

	public void setToChdrnum(String toChdrnum) {
		this.toChdrnum = toChdrnum;
	}

	public String getFdbkind() {
		return fdbkind;
	}

	public void setFdbkind(String fdbkind) {
		this.fdbkind = fdbkind;
	}

	public String getCovdbtind() {
		return covdbtind;
	}

	public void setCovdbtind(String covdbtind) {
		this.covdbtind = covdbtind;
	}

	public String getSwchind() {
		return swchind;
	}

	public void setSwchind(String swchind) {
		this.swchind = swchind;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	
}
