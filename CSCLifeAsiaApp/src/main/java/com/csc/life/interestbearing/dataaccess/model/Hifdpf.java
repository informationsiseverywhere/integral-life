/******************************************************************************
 * File Name 		: Hifdpf.java
 * Author			: CSC
 * Creation Date	: 22 May 2017
 * Project			: Integral Life
 * Description		: The Model Class for HIFDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			| *											| *				  |
 * ***************************************************************************/
package com.csc.life.interestbearing.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class Hifdpf which acts as Model Class for the table HIFDPF.
 */
public class Hifdpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "HIFDPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String zintbfnd;
	private BigDecimal ztotfndval;
	private String usrprf;
	private String jobnm;
	private java.sql.Timestamp datime;

	// Constructor
	public Hifdpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getZintbfnd(){
		return this.zintbfnd;
	}
	public BigDecimal getZtotfndval(){
		return this.ztotfndval;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public java.sql.Timestamp getDatime(){
		return new Timestamp(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setZintbfnd( String zintbfnd ){
		 this.zintbfnd = zintbfnd;
	}
	public void setZtotfndval( BigDecimal ztotfndval ){
		 this.ztotfndval = ztotfndval;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( java.sql.Timestamp datime ){
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("ZINTBFND:		");
		output.append(getZintbfnd());
		output.append("\r\n");
		output.append("ZTOTFNDVAL:		");
		output.append(getZtotfndval());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
