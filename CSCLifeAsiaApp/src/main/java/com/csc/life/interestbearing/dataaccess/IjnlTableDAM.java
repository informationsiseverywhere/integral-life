package com.csc.life.interestbearing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IjnlTableDAM.java
 * Date: Tue, 3 Dec 2013 04:10:02
 * Class transformed from IJNL.LF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IjnlTableDAM extends IjnlpfTableDAM {

	public IjnlTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("IJNL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX"
		             + ", SEQNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "SEQNO, " +
		            "ZINTBFND, " +
		            "ZRECTYP, " +
		            "PRCSEQ, " +
		            "TRANNO, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "CRTABLE, " +
		            "CNTCURR, " +
		            "FDBKIND, " +
		            "INCINUM, " +
		            "INCIPERD01, " +
		            "INCIPERD02, " +
		            "INCIPRM01, " +
		            "INCIPRM02, " +
		            "CNTAMNT, " +
		            "FNDCURR, " +
		            "FUNDAMNT, " +
		            "FUNDRATE, " +
		            "SACSCODE, " +
		            "SACSTYPE, " +
		            "GENLCDE, " +
		            "CONTYP, " +
		            "TRIGER, " +
		            "TRIGKY, " +
		            "SVP, " +
		            "PERSUR, " +
		            "SWCHIND, " +
		            "EFFDATE, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "ZCURPRMBAL," + //for ILIFE-2249
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
		            "SEQNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
		            "SEQNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               seqno,
                               zintbfnd,
                               zrectyp,
                               procSeqNo,
                               tranno,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               crtable,
                               cntcurr,
                               feedbackInd,
                               inciNum,
                               inciPerd01,
                               inciPerd02,
                               inciprm01,
                               inciprm02,
                               contractAmount,
                               fundCurrency,
                               fundAmount,
                               fundRate,
                               sacscode,
                               sacstype,
                               genlcde,
                               contractType,
                               triggerModule,
                               triggerKey,
                               svp,
                               surrenderPercent,
                               switchIndicator,
                               effdate,
                               jobName,
                               userProfile,
                               datime,
                               zcurprmbal,//ILIFE-2249
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(236);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getSeqno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(planSuffix.toInternal());
	nonKeyFiller70.setInternal(seqno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(272);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getZintbfnd().toInternal()
					+ getZrectyp().toInternal()
					+ getProcSeqNo().toInternal()
					+ getTranno().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getCrtable().toInternal()
					+ getCntcurr().toInternal()
					+ getFeedbackInd().toInternal()
					+ getInciNum().toInternal()
					+ getInciPerd01().toInternal()
					+ getInciPerd02().toInternal()
					+ getInciprm01().toInternal()
					+ getInciprm02().toInternal()
					+ getContractAmount().toInternal()
					+ getFundCurrency().toInternal()
					+ getFundAmount().toInternal()
					+ getFundRate().toInternal()
					+ getSacscode().toInternal()
					+ getSacstype().toInternal()
					+ getGenlcde().toInternal()
					+ getContractType().toInternal()
					+ getTriggerModule().toInternal()
					+ getTriggerKey().toInternal()
					+ getSvp().toInternal()
					+ getSurrenderPercent().toInternal()
					+ getSwitchIndicator().toInternal()
					+ getEffdate().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ getZcurprmbal().toInternal());//ILIFE-2249
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, zintbfnd);
			what = ExternalData.chop(what, zrectyp);
			what = ExternalData.chop(what, procSeqNo);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, feedbackInd);
			what = ExternalData.chop(what, inciNum);
			what = ExternalData.chop(what, inciPerd01);
			what = ExternalData.chop(what, inciPerd02);
			what = ExternalData.chop(what, inciprm01);
			what = ExternalData.chop(what, inciprm02);
			what = ExternalData.chop(what, contractAmount);
			what = ExternalData.chop(what, fundCurrency);
			what = ExternalData.chop(what, fundAmount);
			what = ExternalData.chop(what, fundRate);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstype);
			what = ExternalData.chop(what, genlcde);
			what = ExternalData.chop(what, contractType);
			what = ExternalData.chop(what, triggerModule);
			what = ExternalData.chop(what, triggerKey);
			what = ExternalData.chop(what, svp);
			what = ExternalData.chop(what, surrenderPercent);
			what = ExternalData.chop(what, switchIndicator);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);	
			what = ExternalData.chop(what, zcurprmbal);	//ILIFE-2249
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getZintbfnd() {
		return zintbfnd;
	}
	public void setZintbfnd(Object what) {
		zintbfnd.set(what);
	}	
	public FixedLengthStringData getZrectyp() {
		return zrectyp;
	}
	public void setZrectyp(Object what) {
		zrectyp.set(what);
	}	
	public PackedDecimalData getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(Object what) {
		setProcSeqNo(what, false);
	}
	public void setProcSeqNo(Object what, boolean rounded) {
		if (rounded)
			procSeqNo.setRounded(what);
		else
			procSeqNo.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(Object what) {
		feedbackInd.set(what);
	}	
	public PackedDecimalData getInciNum() {
		return inciNum;
	}
	public void setInciNum(Object what) {
		setInciNum(what, false);
	}
	public void setInciNum(Object what, boolean rounded) {
		if (rounded)
			inciNum.setRounded(what);
		else
			inciNum.set(what);
	}	
	public PackedDecimalData getInciPerd01() {
		return inciPerd01;
	}
	public void setInciPerd01(Object what) {
		setInciPerd01(what, false);
	}
	public void setInciPerd01(Object what, boolean rounded) {
		if (rounded)
			inciPerd01.setRounded(what);
		else
			inciPerd01.set(what);
	}	
	public PackedDecimalData getInciPerd02() {
		return inciPerd02;
	}
	public void setInciPerd02(Object what) {
		setInciPerd02(what, false);
	}
	public void setInciPerd02(Object what, boolean rounded) {
		if (rounded)
			inciPerd02.setRounded(what);
		else
			inciPerd02.set(what);
	}	
	public PackedDecimalData getInciprm01() {
		return inciprm01;
	}
	public void setInciprm01(Object what) {
		setInciprm01(what, false);
	}
	public void setInciprm01(Object what, boolean rounded) {
		if (rounded)
			inciprm01.setRounded(what);
		else
			inciprm01.set(what);
	}	
	public PackedDecimalData getInciprm02() {
		return inciprm02;
	}
	public void setInciprm02(Object what) {
		setInciprm02(what, false);
	}
	public void setInciprm02(Object what, boolean rounded) {
		if (rounded)
			inciprm02.setRounded(what);
		else
			inciprm02.set(what);
	}	
	public PackedDecimalData getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(Object what) {
		setContractAmount(what, false);
	}
	public void setContractAmount(Object what, boolean rounded) {
		if (rounded)
			contractAmount.setRounded(what);
		else
			contractAmount.set(what);
	}	
	public FixedLengthStringData getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(Object what) {
		fundCurrency.set(what);
	}	
	public PackedDecimalData getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(Object what) {
		setFundAmount(what, false);
	}
	public void setFundAmount(Object what, boolean rounded) {
		if (rounded)
			fundAmount.setRounded(what);
		else
			fundAmount.set(what);
	}	
	public PackedDecimalData getFundRate() {
		return fundRate;
	}
	public void setFundRate(Object what) {
		setFundRate(what, false);
	}
	public void setFundRate(Object what, boolean rounded) {
		if (rounded)
			fundRate.setRounded(what);
		else
			fundRate.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstype() {
		return sacstype;
	}
	public void setSacstype(Object what) {
		sacstype.set(what);
	}	
	public FixedLengthStringData getGenlcde() {
		return genlcde;
	}
	public void setGenlcde(Object what) {
		genlcde.set(what);
	}	
	public FixedLengthStringData getContractType() {
		return contractType;
	}
	public void setContractType(Object what) {
		contractType.set(what);
	}	
	public FixedLengthStringData getTriggerModule() {
		return triggerModule;
	}
	public void setTriggerModule(Object what) {
		triggerModule.set(what);
	}	
	public FixedLengthStringData getTriggerKey() {
		return triggerKey;
	}
	public void setTriggerKey(Object what) {
		triggerKey.set(what);
	}	
	public PackedDecimalData getSvp() {
		return svp;
	}
	public void setSvp(Object what) {
		setSvp(what, false);
	}
	public void setSvp(Object what, boolean rounded) {
		if (rounded)
			svp.setRounded(what);
		else
			svp.set(what);
	}	
	public PackedDecimalData getSurrenderPercent() {
		return surrenderPercent;
	}
	public void setSurrenderPercent(Object what) {
		setSurrenderPercent(what, false);
	}
	public void setSurrenderPercent(Object what, boolean rounded) {
		if (rounded)
			surrenderPercent.setRounded(what);
		else
			surrenderPercent.set(what);
	}	
	public FixedLengthStringData getSwitchIndicator() {
		return switchIndicator;
	}
	public void setSwitchIndicator(Object what) {
		switchIndicator.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getInciprms() {
		return new FixedLengthStringData(inciprm01.toInternal()
										+ inciprm02.toInternal());
	}
	public void setInciprms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInciprms().getLength()).init(obj);
	
		what = ExternalData.chop(what, inciprm01);
		what = ExternalData.chop(what, inciprm02);
	}
	public PackedDecimalData getInciprm(BaseData indx) {
		return getInciprm(indx.toInt());
	}
	public PackedDecimalData getInciprm(int indx) {

		switch (indx) {
			case 1 : return inciprm01;
			case 2 : return inciprm02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInciprm(BaseData indx, Object what) {
		setInciprm(indx, what, false);
	}
	public void setInciprm(BaseData indx, Object what, boolean rounded) {
		setInciprm(indx.toInt(), what, rounded);
	}
	public void setInciprm(int indx, Object what) {
		setInciprm(indx, what, false);
	}
	public void setInciprm(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInciprm01(what, rounded);
					 break;
			case 2 : setInciprm02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInciperds() {
		return new FixedLengthStringData(inciPerd01.toInternal()
										+ inciPerd02.toInternal());
	}
	public void setInciperds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInciperds().getLength()).init(obj);
	
		what = ExternalData.chop(what, inciPerd01);
		what = ExternalData.chop(what, inciPerd02);
	}
	public PackedDecimalData getInciperd(BaseData indx) {
		return getInciperd(indx.toInt());
	}
	public PackedDecimalData getInciperd(int indx) {

		switch (indx) {
			case 1 : return inciPerd01;
			case 2 : return inciPerd02;
			default: return null; // Throw error instead?
		}
	
	}
	
	//ILIFE-2249
	
	public PackedDecimalData getZcurprmbal() {
		return zcurprmbal;
	}
	public void setZcurprmbal(Object what) {
		setZcurprmbal(what, false);
	}
	public void setZcurprmbal(Object what, boolean rounded) {
		if (rounded)
			zcurprmbal.setRounded(what);
		else
			zcurprmbal.set(what);
	}	
	
	//ENDS
	public void setInciperd(BaseData indx, Object what) {
		setInciperd(indx, what, false);
	}
	public void setInciperd(BaseData indx, Object what, boolean rounded) {
		setInciperd(indx.toInt(), what, rounded);
	}
	public void setInciperd(int indx, Object what) {
		setInciperd(indx, what, false);
	}
	public void setInciperd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInciPerd01(what, rounded);
					 break;
			case 2 : setInciPerd02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		seqno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		zintbfnd.clear();
		zrectyp.clear();
		procSeqNo.clear();
		tranno.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		crtable.clear();
		cntcurr.clear();
		feedbackInd.clear();
		inciNum.clear();
		inciPerd01.clear();
		inciPerd02.clear();
		inciprm01.clear();
		inciprm02.clear();
		contractAmount.clear();
		fundCurrency.clear();
		fundAmount.clear();
		fundRate.clear();
		sacscode.clear();
		sacstype.clear();
		genlcde.clear();
		contractType.clear();
		triggerModule.clear();
		triggerKey.clear();
		svp.clear();
		surrenderPercent.clear();
		switchIndicator.clear();
		effdate.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
		zcurprmbal.clear();//ILIFE-2249
	}


}