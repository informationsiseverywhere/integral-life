package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HitxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:31
 * Class transformed from HITXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HitxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 75;
	public FixedLengthStringData hitxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hitxpfRecord = hitxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hitxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hitxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hitxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hitxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hitxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hitxrec);
	public FixedLengthStringData zintbfnd = DD.zintbfnd.copy().isAPartOf(hitxrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hitxrec);
	public FixedLengthStringData zrectyp = DD.zrectyp.copy().isAPartOf(hitxrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(hitxrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hitxrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(hitxrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(hitxrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(hitxrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(hitxrec);
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(hitxrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(hitxrec);
	public PackedDecimalData surrenderPercent = DD.persur.copy().isAPartOf(hitxrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(hitxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HitxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HitxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HitxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HitxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HitxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HITXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"ZINTBFND, " +
							"TRANNO, " +
							"ZRECTYP, " +
							"BATCTRCDE, " +
							"EFFDATE, " +
							"CNTCURR, " +
							"FDBKIND, " +
							"CNTAMNT, " +
							"FUNDAMNT, " +
							"CNTTYP, " +
							"PRCSEQ, " +
							"PERSUR, " +
							"FUNDRATE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     zintbfnd,
                                     tranno,
                                     zrectyp,
                                     batctrcde,
                                     effdate,
                                     cntcurr,
                                     feedbackInd,
                                     contractAmount,
                                     fundAmount,
                                     cnttyp,
                                     procSeqNo,
                                     surrenderPercent,
                                     fundRate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		zintbfnd.clear();
  		tranno.clear();
  		zrectyp.clear();
  		batctrcde.clear();
  		effdate.clear();
  		cntcurr.clear();
  		feedbackInd.clear();
  		contractAmount.clear();
  		fundAmount.clear();
  		cnttyp.clear();
  		procSeqNo.clear();
  		surrenderPercent.clear();
  		fundRate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHitxrec() {
  		return hitxrec;
	}

	public FixedLengthStringData getHitxpfRecord() {
  		return hitxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHitxrec(what);
	}

	public void setHitxrec(Object what) {
  		this.hitxrec.set(what);
	}

	public void setHitxpfRecord(Object what) {
  		this.hitxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hitxrec.getLength());
		result.set(hitxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}