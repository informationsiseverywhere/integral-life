/*
 * File: Pr509.java
 * Date: 30 August 2009 1:30:57
 * Author: Quipoz Limited
 *
 * Class transformed from PR509.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.interestbearing.dataaccess.IratTableDAM;
import com.csc.life.interestbearing.dataaccess.IratcurTableDAM;
import com.csc.life.interestbearing.screens.Sr509ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Interest Bearing Generic Interest Rate Change Submenu.
*
* This submenu allows the selection of an IBG fund type in order
* to modify interest rate details for the given fund fund or
* number of funds.
*
* Processing Required
* -------------------
*
* The interest rate funds can be changed once either an eligible
* date or an eligible date and fund has been entered. Validation
* is performed to ensure that the fund is eligible for change
* and secondly that the date selected is within the correct to &
* from dates for the fund.
*
* Processing to identify eligible funds includes reference
* in particular to T5515 and T5568, or the IRATPF which stores
* the interest rate details.
*
* Fields on the IRATPF:-
*
* Fund
* From Date
* To Date
* Interest Rate
* Company
*
* The logical files used to access the IRATPF are as follows:-
*
* IRATPF - All fields on the IRATPF keyed on From Date plus the fund
* IRATCUR - All fields keyed on the fund and select records with
*           a To Date set to 9's
*
*
*****************************************************************
* </pre>
*/
public class Pr509 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR509");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private String wsaaIratentFlag = "N";
		/* ERRORS */
	private String e070 = "E070";
	private String e073 = "E073";
	private String h116 = "H116";
	private String h387 = "H387";
	private String i032 = "I032";
	private String rlab = "RLAB";
	private String rlac = "RLAC";
	private String rlad = "RLAD";
	private String rlae = "RLAE";
	private String e859 = "E859";
		/* TABLES */
	private String t5515 = "T5515";
		/* FORMATS */
	private String iratrec = "IRATREC";
	private String iratcurrec = "IRATCURREC";
	private String itemrec = "ITEMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Interest Rate File*/
	private IratTableDAM iratIO = new IratTableDAM();
		/*Interest Rate File*/
	private IratcurTableDAM iratcurIO = new IratcurTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Subprogrec subprogrec = new Subprogrec();
	private T5515rec t5515rec = new T5515rec();
	private Batckey wsaaBatchkey = new Batckey();
	private Sr509ScreenVars sv = ScreenProgram.getScreenVars( Sr509ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		preExit,
		exit2190,
		exit2290,
		exit2300,
		exit2310,
		exit2331,
		exit2340,
		exit2351,
		exit2990,
		exit3090
	}

	public Pr509() {
		super();
		screenVars = sv;
		new ScreenModel("Sr509", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		sv.fromdate.set(varcom.vrcmMaxDate);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		try {
			validateKey12210();
			continue2230();
		}
		catch (GOTOException e){
		}
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1,"Y")) {
			goTo(GotoLabel.exit2290);
		}
		/*VALIDATE-KEY2*/
		if (isEQ(subprogrec.key2,"Y")
		&& isEQ(sv.fromdate,varcom.vrcmMaxDate)
		&& (isEQ(sv.action,"A")
		|| isEQ(sv.action,"B"))) {
			sv.fromdateErr.set(i032);
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(subprogrec.key2,"Y")
		&& isEQ(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.fromdate,varcom.vrcmMaxDate)
		&& isEQ(sv.action,"C")) {
			sv.fromdateErr.set(rlab);
			sv.vrtfndErr.set(rlab);
			goTo(GotoLabel.exit2290);
		}
	}

protected void continue2230()
	{
		if ((isEQ(sv.action,"A")
		|| isEQ(sv.action,"B"))
		&& isLT(sv.fromdate,wsaaToday)) {
			sv.fromdateErr.set(e859);
			goTo(GotoLabel.exit2290);
		}
		checkFund2300();
	}

protected void checkFund2300()
	{
		try {
			start2300();
		}
		catch (GOTOException e){
		}
	}

protected void start2300()
	{
		if (isNE(sv.unitVirtualFund,SPACES)) {
			readT55152310();
			if (isEQ(wsaaIratentFlag,"Y")) {
				goTo(GotoLabel.exit2300);
			}
		}
		if (isNE(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.action,"A")) {
			readIratcur2320();
			if (isEQ(iratcurIO.getStatuz(),varcom.oK)
			&& isLTE(sv.fromdate,iratcurIO.getFromdate())) {
				sv.fromdateErr.set(rlad);
				goTo(GotoLabel.exit2300);
			}
		}
		if (isEQ(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.action,"A")) {
			readIratcurRecs2330();
			if (isEQ(sv.fromdateErr,rlad)) {
				goTo(GotoLabel.exit2300);
			}
		}
		if (isNE(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.action,"B")) {
			readIrat2340();
			if (isEQ(sv.fromdateErr,rlae)) {
				goTo(GotoLabel.exit2300);
			}
		}
		if (isEQ(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.action,"B")) {
			readIratRecs2350();
			if (isEQ(sv.fromdateErr,rlae)) {
				goTo(GotoLabel.exit2300);
			}
		}
	}

protected void readT55152310()
	{
		try {
			para2310();
		}
		catch (GOTOException e){
		}
	}

protected void para2310()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setItmfrm(sv.fromdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItempfx(),smtpfxcpy.item)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),sv.unitVirtualFund)) {
			sv.vrtfndErr.set(h116);
			goTo(GotoLabel.exit2310);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		if (isNE(t5515rec.zfundtyp,"D")) {
			sv.vrtfndErr.set(rlac);
		}
	}

protected void readIratcur2320()
	{
		/*START*/
		iratcurIO.setDataKey(SPACES);
		iratcurIO.setCompany(wsspcomn.company);
		iratcurIO.setUnitVirtualFund(sv.unitVirtualFund);
		iratcurIO.setFormat(iratcurrec);
		iratcurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, iratcurIO);
		if (isNE(iratcurIO.getStatuz(),varcom.oK)
		&& isNE(iratcurIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(iratcurIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readIratcurRecs2330()
	{
		/*START*/
		iratcurIO.setDataKey(SPACES);
		iratcurIO.setCompany(wsspcomn.company);
		iratcurIO.setFormat(iratcurrec);
		iratcurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		iratcurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		iratcurIO.setFitKeysSearch("COMPANY");
		iratcurIO.setStatuz(varcom.oK);
		while ( !(isEQ(iratcurIO.getStatuz(),varcom.endp))) {
			callIratcur2331();
		}

		/*EXIT*/
	}

protected void callIratcur2331()
	{
		try {
			para2331();
		}
		catch (GOTOException e){
		}
	}

protected void para2331()
	{
		SmartFileCode.execute(appVars, iratcurIO);
		if (isNE(iratcurIO.getStatuz(),varcom.oK)
		&& isNE(iratcurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratcurIO.getParams());
			fatalError600();
		}
		if (isEQ(iratcurIO.getStatuz(),varcom.endp)
		|| isNE(iratcurIO.getCompany(),wsspcomn.company)) {
			iratcurIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2331);
		}
		if (isLTE(sv.fromdate,iratcurIO.getFromdate())) {
			iratcurIO.setStatuz(varcom.endp);
			sv.fromdateErr.set(rlad);
			goTo(GotoLabel.exit2331);
		}
		iratcurIO.setFunction(varcom.nextr);
	}

protected void readIrat2340()
	{
		try {
			para2340();
		}
		catch (GOTOException e){
		}
	}

protected void para2340()
	{
		iratIO.setDataKey(SPACES);
		iratIO.setCompany(wsspcomn.company);
		iratIO.setUnitVirtualFund(sv.unitVirtualFund);
		iratIO.setFromdate(sv.fromdate);
		iratIO.setFormat(iratrec);
		iratIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, iratIO);
		if (isNE(iratIO.getStatuz(),varcom.oK)
		&& isNE(iratIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(iratIO.getParams());
			fatalError600();
		}
		if (isEQ(iratIO.getStatuz(),varcom.mrnf)) {
			sv.fromdateErr.set(h387);
			goTo(GotoLabel.exit2340);
		}
		if (isNE(iratIO.getTodate(),varcom.vrcmMaxDate)) {
			sv.fromdateErr.set(rlae);
			goTo(GotoLabel.exit2340);
		}
	}

protected void readIratRecs2350()
	{
		/*START*/
		iratIO.setDataKey(SPACES);
		iratIO.setCompany(wsspcomn.company);
		iratIO.setFromdate(sv.fromdate);
		iratIO.setFormat(iratrec);
		iratIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		iratIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		iratIO.setFitKeysSearch("COMPANY");
		iratIO.setStatuz(varcom.oK);
		while ( !(isEQ(iratIO.getStatuz(),varcom.endp))) {
			callIrat2351();
		}

		/*EXIT*/
	}

protected void callIrat2351()
	{
		try {
			para2351();
		}
		catch (GOTOException e){
		}
	}

protected void para2351()
	{
		SmartFileCode.execute(appVars, iratIO);
		if (isNE(iratIO.getStatuz(),varcom.oK)
		&& isNE(iratIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratIO.getParams());
			fatalError600();
		}
		if (isEQ(iratIO.getStatuz(),varcom.endp)
		|| isNE(iratIO.getCompany(),wsspcomn.company)) {
			iratIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2351);
		}
		if (isEQ(sv.fromdate,iratIO.getFromdate())
		&& isNE(iratIO.getTodate(),varcom.vrcmMaxDate)) {
			iratIO.setStatuz(varcom.endp);
			sv.fromdateErr.set(rlae);
			goTo(GotoLabel.exit2351);
		}
		iratIO.setFunction(varcom.nextr);
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit2990);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			goTo(GotoLabel.exit2990);
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		try {
			updateWssp3010();
			batching3080();
		}
		catch (GOTOException e){
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(sv.action,"A")){
			wsspcomn.flag.set("C");
		}
		else if (isEQ(sv.action,"B")){
			wsspcomn.flag.set("D");
		}
		else if (isEQ(sv.action,"C")){
			wsspcomn.flag.set("I");
		}
		iratIO.setFromdate(sv.fromdate);
		iratIO.setUnitVirtualFund(sv.unitVirtualFund);
		iratIO.setFunction(varcom.keeps);
		iratIO.setFormat(iratrec);
		SmartFileCode.execute(appVars, iratIO);
		if (isNE(iratIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(iratIO.getParams());
			syserrrec.statuz.set(iratIO.getStatuz());
			fatalError600();
		}
		initialize(sv.unitVirtualFund);
		initialize(sv.fromdate);
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
