package com.csc.life.interestbearing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:47
 * Description:
 * Copybook name: TH510REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th510rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th510Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zintalofrq = new FixedLengthStringData(2).isAPartOf(th510Rec, 0);
  	public FixedLengthStringData zintcalc = new FixedLengthStringData(7).isAPartOf(th510Rec, 2);
  	public ZonedDecimalData zintfixdd = new ZonedDecimalData(2, 0).isAPartOf(th510Rec, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(489).isAPartOf(th510Rec, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th510Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th510Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}