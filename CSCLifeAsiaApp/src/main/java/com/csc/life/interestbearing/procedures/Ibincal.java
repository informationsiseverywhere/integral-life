/*
 * File: Ibincal.java
 * Date: 29 August 2009 22:55:44
 * Author: Quipoz Limited
 *
 * Class transformed from IBINCAL.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.interestbearing.recordstructures.Ibincalrec;
import com.csc.life.interestbearing.tablestructures.Th507rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* Interest Bearing Interest Calculation Routine.
*
* Interest Rate is retrieved from TH507
* Formula:
*  if annual int rate defined, calculate daily int rate
*       = (1 + annual rate / 100) ** (1 / 365)
*
*  Interest Rate = (daily interest rate ** elapse period in days bet.
*                  (last int applied to this int due) - 1)
*  Interest      = int cap amount * Interest Rate
*
***********************************************************************
* </pre>
*/
public class Ibincal extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("IBINCAL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIntRate = new PackedDecimalData(10, 7);
	private PackedDecimalData wsaaDayRate = new PackedDecimalData(10, 7);
		/* ERRORS */
	private String hl09 = "HL09";
		/* FORMATS */
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String th507 = "TH507";
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ibincalrec ibincalrec = new Ibincalrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Th507rec th507rec = new Th507rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit290
	}

	public Ibincal() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ibincalrec.ibincalRec = convertAndSetParam(ibincalrec.ibincalRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		ibincalrec.statuz.set(varcom.oK);
		readTh507100();
		calcInterest200();
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void readTh507100()
	{
		read110();
	}

protected void read110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(ibincalrec.company);
		itdmIO.setItemtabl(th507);
		itdmIO.setItemitem(ibincalrec.fund);
		itdmIO.setItmfrm(ibincalrec.lastIntApp);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(itdmIO.getItemcoy(),ibincalrec.company)
		|| isNE(itdmIO.getItemtabl(),th507)
		|| isNE(itdmIO.getItemitem(),ibincalrec.fund)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(ibincalrec.company);
			itdmIO.setItemtabl(th507);
			itdmIO.setItemitem(ibincalrec.fund);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(hl09);
			syserr570();
		}
		th507rec.th507Rec.set(itdmIO.getGenarea());
		ibincalrec.intEffdate.set(itdmIO.getItmfrm());
	}

protected void calcInterest200()
	{
		try {
			calc210();
		}
		catch (GOTOException e){
		}
	}

protected void calc210()
	{
		if (isEQ(th507rec.zdayintr,ZERO)
		&& isEQ(th507rec.zannintr,ZERO)) {
			ibincalrec.intAmount.set(ZERO);
			goTo(GotoLabel.exit290);
		}
		if (isNE(th507rec.zdayintr,ZERO)) {
			ibincalrec.intRate.set(th507rec.zdayintr);
			wsaaDayRate.set(th507rec.zdayintr);
		}
		else {
			ibincalrec.intRate.set(th507rec.zannintr);
			compute(wsaaDayRate, 8).setRounded(power((add(1,div(th507rec.zannintr,100))),(div(1,365))));
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(ibincalrec.lastIntApp);
		datcon3rec.intDate2.set(ibincalrec.nextIntDue);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			syserr570();
		}
		compute(wsaaIntRate, 8).setRounded(sub((power(wsaaDayRate,datcon3rec.freqFactor)),1));
		compute(ibincalrec.intAmount, 8).setRounded(mult(ibincalrec.capAmount,wsaaIntRate));
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		ibincalrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		ibincalrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}
}
