/*
 * File: Bh512.java
 * Date: 29 August 2009 21:27:33
 * Author: Quipoz Limited
 *
 * Class transformed from BH512.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.interestbearing.dataaccess.dao.HidxpfDAO;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hidxpf;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.interestbearing.recordstructures.Ibincalrec;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitdpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Hitdpf;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.procedures.Vpxibin;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxibinrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.tablestructures.T6626rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  INTEREST BEARING INTEREST CALCULATION
*  =====================================
*
*  Overview
*  ========
*
*  This program will read HIDX records, HIDXs being HITD records
*  on which interest is due, created by the Interest Bearing Fund
*  Interest Calculation Splitter program, BH511.
*
*  Processing
*  ==========
*
*  Initialise.
*
*  - Check the restart method is 3
*  - Find the name of the HIDX temporary file. HIDX is a temporary
*    file which has the name of "HIDX" + first two characters of
*    param 4 + the last four digits of the job number.
*  - Pre-load constantly referenced Tables into working storage.
*    These are: TH510, T5688, T6647.
*
*  Read.
*
*  - Read a HIDX.
*
*  Edit.
*
*  - Softlock the contract if this HIDX is for a new contract.
*    Note that if a softlock exists we could further corrupt a
*    policy if we process it.
*
*  Update.
*
*
*  Finalise.
*
*  - Close the temporary file.
*  - Remove the override
*  - Set the parameter statuz to O-K
*
***********************************************************************
* </pre>
*/
public class Bh512 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH512");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
		/*    HIDX parameters*/
	private FixedLengthStringData wsaaSaveBsprParams = new FixedLengthStringData(1024);

	private FixedLengthStringData wsaaHidxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHidxFn, 0, FILLER).init("HIDX");
	private FixedLengthStringData wsaaHidxRunid = new FixedLengthStringData(2).isAPartOf(wsaaHidxFn, 4);
	private ZonedDecimalData wsaaHidxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHidxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private int ix = 0;
	private int th510Ix = 0;
	private int t6647Ix = 0;
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaIntRound = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaInterestTot = new PackedDecimalData(17, 4);
	private PackedDecimalData wsaaIntOnFundAmt = new PackedDecimalData(17, 4);
	private PackedDecimalData wsaaIntOnFundBal = new PackedDecimalData(17, 4);
	private BigDecimal wsaaFundAmount = BigDecimal.ZERO;
	private int wsaaTranno = 0;
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaNoMoreInt = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaInterestDate = new PackedDecimalData(8, 0);

		/* WSAA-T5688-ARRAY
		    03 WSAA-T5688-REC               OCCURS 100.                  */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 9);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Rec, 3);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 8);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
	//private static final int wsaaT5688Size = 500;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;

		/* WSAA-T6647-ARRAY
		    03 WSAA-T6647-REC              OCCURS 500.                   */
	private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (5000, 15);
	private FixedLengthStringData[] wsaaT6647Key = FLSDArrayPartOfArrayStructure(7, wsaaT6647Rec, 0);
	private FixedLengthStringData[] wsaaT6647Batctrcde = FLSDArrayPartOfArrayStructure(4, wsaaT6647Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT6647Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6647Key, 4, HIVALUE);
	private PackedDecimalData[] wsaaT6647Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6647Rec, 7);
	private FixedLengthStringData[] wsaaT6647Data = FLSDArrayPartOfArrayStructure(3, wsaaT6647Rec, 12);
	private ZonedDecimalData[] wsaaT6647ProcSeqNo = ZDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 0);
	private static final int wsaaT6647Size = 5000;

		/* WSAA-TH510-ARRAY */
	private FixedLengthStringData[] wsaaTh510Rec = FLSInittedArray (100, 20);
	private FixedLengthStringData[] wsaaTh510Key = FLSDArrayPartOfArrayStructure(4, wsaaTh510Rec, 0);
	private FixedLengthStringData[] wsaaTh510Fund = FLSDArrayPartOfArrayStructure(4, wsaaTh510Key, 0, SPACES);
	private PackedDecimalData[] wsaaTh510Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaTh510Rec, 4);
	private FixedLengthStringData[] wsaaTh510Data = FLSDArrayPartOfArrayStructure(11, wsaaTh510Rec, 9);
	private FixedLengthStringData[] wsaaTh510Zintalofrq = FLSDArrayPartOfArrayStructure(2, wsaaTh510Data, 0);
	private FixedLengthStringData[] wsaaTh510Zintcalc = FLSDArrayPartOfArrayStructure(7, wsaaTh510Data, 2);
	private ZonedDecimalData[] wsaaTh510Zintfixdd = ZDArrayPartOfArrayStructure(2, 0, wsaaTh510Data, 9);
	private static final int wsaaTh510Size = 100;
	private String wsaaTriggerFound = "";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String t6647 = "T6647";
	private static final String th510 = "TH510";
	private static final String t6626 = "T6626";
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String h791 = "H791";

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Ibincalrec ibincalrec = new Ibincalrec();
	private T6626rec t6626rec = new T6626rec();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
	private Th510rec th510rec = new Th510rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	
    private int intBatchID = 0;
    private int intBatchExtractSize;
	private Map<String, List<Itempf>> t5688Map = null;
    private Map<String, List<Itempf>> th510Map = null;
    private Map<String, List<Itempf>> t6647Map = null;
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private HidxpfDAO hidxpfDAO = getApplicationContext().getBean("hidxpfDAO", HidxpfDAO.class);
    private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
    private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private HitdpfDAO hitdpfDAO = getApplicationContext().getBean("hitdpfDAO", HitdpfDAO.class);
    private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    
    private Chdrpf chdrpfRec = null;
    private Hidxpf hidxpfRec = null;
    private Hitrpf hitrintRec = null;
    private Iterator<Hidxpf> iteratorList; 
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
    private int ct01Value = 0;
    private int ct02Value = 0;
    private int ct03Value = 0;
    private int ct04Value = 0;
    private Map<String, List<Hitspf>> hitspfMap = null;
    private Map<String, List<Hitrpf>> hitrpfMap = null;
    private Map<String, List<Chdrpf>> chdrpfMap = null;
    private Map<String, List<Covrpf>> covrpfMap = null;
    private Map<String, List<Hitrpf>> hitrintMap = null;
    private Map<String, List<Hitdpf>> hitdpfMap = null;
    
    private List<Chdrpf> updateChdrpfInvalidList = null;
    private List<Chdrpf> insertChdrpfValidList = null;
    private List<Ptrnpf> insertPtrnpfList = null;
    private List<Hitrpf> updateHitrpfList = null;
    private List<Hitrpf> insertHitrpfList = null;
    private List<Hitdpf> updateHitdpfList = null;
    private List<Hitdpf> updateHitdpfInvalidList = null;
    private List<Hitdpf> insertHitdpfList = null;
    
    private boolean noRecordFound;
    private Hitdpf lastHitdpfRecord;
    private List<String> ptrnpfValidator = null;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		skipLast3120,
		skipInterest5150,
		nextHitr5180,
		exit5190,
		updateHitd5380,
		calc5385
	}

	public Bh512() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
 {
		noRecordFound = false;
		wsspEdterror.set(varcom.oK);
		ptrnpfValidator = new ArrayList<String>();
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		tempFileOverride1100();
        
		if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        readChunkRecord();
        if (noRecordFound) {
        	wsspEdterror.set(varcom.endp);
        	return;
        }
        
		/* Read T5645 for the Accounting Rules. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T5679 for contract status validation. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());

		/* Load All Contract Processing Rules. */
		String coy = bsprIO.getCompany().toString();
		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
		ix = 1;
		loadT56881200();

		/* Load All Interest Bearing Fund Definitions. */
		th510Map = itemDAO.loadSmartTable("IT", coy, th510);
		ix = 1;
		loadTh5101300();

		/* Load Non-Traditional Contract Details. */
		t6647Map = itemDAO.loadSmartTable("IT", coy, "T6647");
		ix = 1;
		loadT66471400();

		/* Read T6626 to find transaction codes that requires no further */
		/* interest allocation. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6626);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6626rec.t6626Rec.set(itemIO.getGenarea());
	}

private void readChunkRecord(){
	List<Hidxpf> hidxpfList = hidxpfDAO.searchHidxRecord(wsaaHidxFn.toString(), wsaaThreadMember.toString(), intBatchExtractSize, intBatchID);
	iteratorList = hidxpfList.iterator();
	if(!hidxpfList.isEmpty()){
		List<String> chdrnumSet = new ArrayList<>();
		for(Hidxpf h:hidxpfList){
			chdrnumSet.add(h.getChdrnum());
		}
		hitspfMap = hitspfDAO.searchHitsRecordByChdrnum(chdrnumSet);
		hitrpfMap = hitrpfDAO.searchHitrRecordByChdrnum(chdrnumSet);
		chdrpfMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumSet);
		covrpfMap = covrpfDAO.searchCovrudlByChdrnum(chdrnumSet);
		hitrintMap = hitrpfDAO.searchHitrintRecordByChdrnum(chdrnumSet);
		hitdpfMap = hitdpfDAO.searchHitdRecordByChdrnum(chdrnumSet);
	}
	else{
		noRecordFound = true;
		return;			
	}
}

protected void tempFileOverride1100()
{
		wsaaHidxRunid.set(bprdIO.getSystemParam04());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		tempFileOverride1101();
}

protected void tempFileOverride1101()
	{
		if (isEQ(bprdIO.getSystemParam01(),SPACES)) {
			wsaaHidxJobno.set(bsprIO.getScheduleNumber());
			return ;
		}
		/*    Firstly save this process's BSPR-Params.*/
		wsaaSaveBsprParams.set(bsprIO.getParams());
		bsprIO.setScheduleName(bprdIO.getSystemParam01());
		bsprIO.setScheduleNumber(99999999);
		bsprIO.setCompany(SPACES);
		bsprIO.setProcessName(SPACES);
		bsprIO.setProcessOccNum(ZERO);
		bsprIO.setFormat(formatsInner.bsprrec);
		bsprIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, bsprIO);
		if (isNE(bsprIO.getStatuz(),varcom.oK)
		|| isNE(bsprIO.getScheduleName(),bprdIO.getSystemParam01())
		|| isNE(bsprIO.getProcessStatus(),"90")) {
			bsprIO.setScheduleName(bprdIO.getSystemParam01());
			bsprIO.setScheduleNumber(99999999);
			bsprIO.setCompany(SPACES);
			bsprIO.setProcessName(SPACES);
			bsprIO.setProcessOccNum(ZERO);
			bsprIO.setFormat(formatsInner.bsprrec);
			bsprIO.setFunction(varcom.endr);
			bsprIO.setStatuz(varcom.endp);
			syserrrec.params.set(bsprIO.getParams());
			fatalError600();
		}
		/*    Set up our temporary file job number as that of the last*/
		/*    Fund Movement Report job number.*/
		wsaaHidxJobno.set(bsprIO.getScheduleNumber());
		/*    Restore the BSPR-Params.*/
		bsprIO.setParams(wsaaSaveBsprParams);
	}

protected void loadT56881200()
	{
	    if (t5688Map == null || t5688Map.size() == 0) {
	        syserrrec.params.set("t5688");
	        fatalError600();
	    }
	    if (t5688Map.size() > wsaaT5688Size) {
	        syserrrec.statuz.set(h791);
	        syserrrec.params.set("t5688");
	        fatalError600();
	    }
	    for (List<Itempf> items : t5688Map.values()) {
	        for (Itempf item : items) {
	            if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
	                t5688rec.t5688Rec.set(StringUtil.rawToString(item.getGenarea()));
	        		wsaaT5688Cnttype[ix].set(item.getItemitem());
	        		wsaaT5688Itmfrm[ix].set(item.getItmfrm());
	        		wsaaT5688Comlvlacc[ix].set(t5688rec.comlvlacc);
	        		ix++;
	            }
	
	        }
	    }
	}


protected void loadTh5101300()
 {
		if (th510Map == null || th510Map.size() == 0) {
			syserrrec.params.set(th510);
			fatalError600();
		}
		if (th510Map.size() > wsaaTh510Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(th510);
			fatalError600();
		}
		for (List<Itempf> items : th510Map.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					th510rec.th510Rec.set(StringUtil.rawToString(item
							.getGenarea()));
					wsaaTh510Fund[ix].set(item.getItemitem());
					wsaaTh510Itmfrm[ix].set(item.getItmfrm());
					wsaaTh510Zintalofrq[ix].set(th510rec.zintalofrq);
					wsaaTh510Zintcalc[ix].set(th510rec.zintcalc);
					wsaaTh510Zintfixdd[ix].set(th510rec.zintfixdd);
					ix++;
				}

			}
		}
	}

protected void loadT66471400()
 {
		if (t6647Map == null || t6647Map.size() == 0) {
			syserrrec.params.set("t6647");
			fatalError600();
		}
		if (t6647Map.size() > wsaaT6647Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("t6647");
			fatalError600();
		}
		for (List<Itempf> items : t6647Map.values()) {
			for (Itempf item : items) {
				if (item.getItemitem().startsWith(bprdIO.getAuthCode().toString())
						&& (item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0)) {
					t6647rec.t6647Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT6647Key[ix].set(item.getItemitem());
					wsaaT6647Itmfrm[ix].set(item.getItmfrm());
					wsaaT6647ProcSeqNo[ix].set(t6647rec.procSeqNo);
					ix++;
				}

			}
		}
	}

protected void readFile2000()
 {
		if (iteratorList.hasNext()) {
			hidxpfRec = iteratorList.next();
			ct01Value++;
		} else {
			intBatchID++;
			clearList2100();
			readChunkRecord();
			if (iteratorList.hasNext()) {
				hidxpfRec = iteratorList.next();
			} else {
				wsspEdterror.set(varcom.endp);
			}
		}
	}

private void clearList2100(){
	iteratorList = null; 
	hitspfMap = null;
	hitrpfMap = null;
	chdrpfMap = null;
	covrpfMap = null;
	hitrintMap = null;
	hitdpfMap = null;
}

protected void edit2500()
 {
		wsspEdterror.set(SPACES);
		if (isEQ(hidxpfRec.getChdrnum(), wsaaALockedChdrnum)) {
			ct03Value++;
			return;
		}
		if (isNE(hidxpfRec.getChdrnum(), lastChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz, "LOCK")) {
				wsaaALockedChdrnum.set(hidxpfRec.getChdrnum());
				return;
			} else {
				wsaaALockedChdrnum.set(SPACES);
			}
			Hitspf hitspf = checkHits2700();
			if (hitspf == null) {
				ct04Value++;
				return;
			}
			checkHitr2800();
			readChdr2900();
			if (!validStatus.isTrue()) {
				return;
			}
			
		}
		wsspEdterror.set(varcom.oK);
	}


protected void softlockPolicy2600()
 {
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(hidxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(hidxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hidxpfRec.getChdrcoy());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(hidxpfRec.getChdrnum());
			stringVariable1.setStringInto(conlogrec.params);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++;
			ct02Value++;
		} else {
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
	}


protected Hitspf checkHits2700()
 {
		Hitspf result = null;
		if (hitspfMap!= null && hitspfMap.containsKey(hidxpfRec.getChdrnum())) {
			List<Hitspf> hitspfList = hitspfMap.get(hidxpfRec.getChdrnum());
			for (Hitspf h : hitspfList) {
				if (h.getChdrcoy().equals(hidxpfRec.getChdrcoy())
						&& h.getLife().equals(hidxpfRec.getLife())
						&& h.getCoverage().equals(hidxpfRec.getCoverage())
						&& h.getRider().equals(hidxpfRec.getRider())
						&& (h.getPlanSuffix() == hidxpfRec.getPlanSuffix())
						&& h.getZintbfnd().equals(hidxpfRec.getZintbfnd())) {
					result = h;
					break;

				}
			}
		}
		return result;
	}


protected void checkHitr2800()
 {
		wsaaTriggerFound = "N";
		if (hitrpfMap!= null && hitrpfMap.containsKey(hidxpfRec.getChdrnum())) {
			List<Hitrpf> hitrpfList = hitrpfMap.get(hidxpfRec.getChdrnum());
			for (Hitrpf h : hitrpfList) {
				if (h.getChdrcoy().equals(hidxpfRec.getChdrcoy())
						&& h.getLife().equals(hidxpfRec.getLife())
						&& h.getCoverage().equals(hidxpfRec.getCoverage())
						&& h.getRider().equals(hidxpfRec.getRider())
						&& (h.getPlanSuffix() == hidxpfRec.getPlanSuffix())) {
					if (isNE(h.getTriggerModule(), SPACES)) {
						wsaaTriggerFound = "Y";
						wsaaTranno = h.getTranno();
						wsaaBatctrcde.set(h.getBatctrcde());
						return;
					}
				}
			}
		}
	}

protected void readChdr2900()
 {
		chdrpfRec = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(hidxpfRec.getChdrnum())) {
			for (Chdrpf c : chdrpfMap.get(hidxpfRec.getChdrnum())) {
				if (c.getChdrcoy().toString().equals(hidxpfRec.getChdrcoy())) {
					chdrpfRec = c;
					break;
				}
			}
		}

		if (chdrpfRec == null) {
			syserrrec.params.set(hidxpfRec.getChdrnum());
			fatalError600();
		}
		validateContractStatus2a00(chdrpfRec);
		if (!validStatus.isTrue()) {
			return;
		}
		/* Get the pre-loaded T6647 entry for this contract */
		for (ix = 1; !(isGT(ix, wsaaT6647Size)
				|| isEQ(wsaaT6647Batctrcde[ix], SPACES) || (isEQ(
				wsaaT6647Batctrcde[ix], bprdIO.getAuthCode())
				&& isEQ(wsaaT6647Cnttype[ix], chdrpfRec.getCnttype()) && isLTE(
					wsaaT6647Itmfrm[ix], hidxpfRec.getZnxtintdte()))); ix++) {
			/* CONTINUE_STMT */
		}
		if (isGT(ix, wsaaT6647Size)
				|| isEQ(wsaaT6647Cnttype[ix], SPACES)) {
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t6647);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(bprdIO.getAuthCode());
			stringVariable1.addExpression(chdrpfRec.getCnttype());
			stringVariable1.setStringInto(itdmIO.getItemitem());
			itdmIO.setItmfrm(hidxpfRec.getZnxtintdte());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			fatalError600();
		}
		t6647Ix = ix;
		/* Get the pre-loaded T5688 entry for this contract */
		for (ix = 1; !(isGT(ix, wsaaT5688Size)
				|| isEQ(wsaaT5688Cnttype[ix], SPACES) || (isEQ(
				wsaaT5688Cnttype[ix], chdrpfRec.getCnttype()) && isLTE(
				wsaaT5688Itmfrm[ix], hidxpfRec.getZnxtintdte()))); ix++) {
			/* CONTINUE_STMT */
		}
		if (isGT(ix, wsaaT5688Size)
				|| isEQ(wsaaT5688Cnttype[ix], SPACES)) {
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5688);
			itdmIO.setItemitem(chdrpfRec.getCnttype());
			itdmIO.setItmfrm(hidxpfRec.getZnxtintdte());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			fatalError600();
		}
		wsaaComponLevelAccounted.set(wsaaT5688Comlvlacc[ix]);
	}


protected void validateContractStatus2a00(Chdrpf chdrpf)
	{
		/*A00-PARA*/
		wsaaValidStatus.set("N");
		for (ix = 1; !(isGT(ix, 12)); ix++){
			if (isEQ(t5679rec.cnRiskStat[ix], chdrpf.getStatcode())) {
				for (ix = 1; !(isGT(ix, 12)); ix++){
					if (isEQ(t5679rec.cnPremStat[ix], chdrpf.getPstcde())) {
						ix = 13;
						wsaaValidStatus.set("Y");
					}
				}
			}
		}
		/*A90-EXIT*/
	}
protected void update3000()
 {
		Covrpf covrpf = null;
		if (covrpfMap != null && covrpfMap.containsKey(hidxpfRec.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(hidxpfRec.getChdrnum())) {
				if (hidxpfRec.getChdrcoy().equals(c.getChdrcoy())
						&& hidxpfRec.getLife().equals(c.getLife())
						&& hidxpfRec.getCoverage().equals(c.getCoverage())
						&& hidxpfRec.getRider().equals(c.getRider())
						&& (hidxpfRec.getPlanSuffix() == c.getPlanSuffix())) {
					covrpf = c;
					break;
				}
			}
		}
		if (covrpf == null) {
			syserrrec.params.set(hidxpfRec.getChdrnum());
			fatalError600();
		}
		/* If the first time in, performs only one cycle of interest */
		/* allocation. This is to ensure that after an AFi and then */
		/* re-issued the policy, the interest allocation will not be */
		/* deferred for too long. This is because the Prem HITR record */
		/* will not be picked up by this program immediately after */
		/* re-issued as the feedback ind is still blank. So under this */
		/* circumstances we don't want to set the next interest date */
		/* beyond today. This is to ensure unit dealing can pick this */
		/* HITD up again next time it is run and to allocate interest. */
		if (isEQ(hidxpfRec.getZlstintdte(), varcom.vrcmMaxDate)) {
			calculateInterest3100(covrpf);
			return;
		}
		while (hidxpfRec.getZnxtintdte() <= bsscIO.getEffectiveDate().toInt()) {
			calculateInterest3100(covrpf);
		}
	}


protected void calculateInterest3100(Covrpf c)
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT:
				calc3110(c);
			case skipLast3120:
				skipLast3120(c);
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void calc3110(Covrpf c)
 {
		if (isEQ(wsaaTriggerFound, "N")) {
			updates5000();
		}
		/* The first thing to do here is to check whether there are */
		/* any HITR records that should have been allocated interest */
		/* prior to today. This may happen if a transaction is reversed */
		/* and causing the effective date of the original HITR to be */
		/* in the past. If any such HITR records are found, we must */
		/* calculate the interest due on each of these records up to */
		/* the last interest allocation date, then add the interest */
		/* calculated and the fund amounts on these HITRs into the */
		/* current fund balance. Once this has been done, the fund */
		/* balance will be accurate and up to date, and we will be */
		/* ready to calculate interest for the current allocation */
		/* period. */
		if (isEQ(hidxpfRec.getZlstintdte(), varcom.vrcmMaxDate)) {
			goTo(GotoLabel.skipLast3120);
		}
		for (ix = 1; !(isGT(ix, wsaaTh510Size)
				|| isEQ(wsaaTh510Fund[ix], SPACES) || (isEQ(
				wsaaTh510Fund[ix], hidxpfRec.getZintbfnd()) && isLTE(
				wsaaTh510Itmfrm[ix], hidxpfRec.getZlstintdte()))); ix++) {
			/* CONTINUE_STMT */
		}
		if (isGT(ix, wsaaTh510Size) || isEQ(wsaaTh510Fund[ix], SPACES)) {
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(th510);
			itdmIO.setItemitem(hidxpfRec.getZintbfnd());
			itdmIO.setItmfrm(hidxpfRec.getZlstintdte());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			fatalError600();
		}
		th510Ix = ix;
		wsaaIntOnFundAmt.set(ZERO);
		wsaaFundAmount = BigDecimal.ZERO;

		wsaaInterestDate.set(hidxpfRec.getZlstintdte());

		if (hitrintMap != null && hitrintMap.containsKey(hidxpfRec.getChdrnum())) {
			List<Hitrpf> hitrintList = hitrintMap.get(hidxpfRec.getChdrnum());
			for (Hitrpf h : hitrintList) {
				if (hidxpfRec.getChdrcoy().equals(h.getChdrcoy())
						&& hidxpfRec.getLife().equals(h.getLife())
						&& hidxpfRec.getCoverage().equals(h.getCoverage())
						&& hidxpfRec.getRider().equals(h.getRider())
						&& (hidxpfRec.getPlanSuffix() == h.getPlanSuffix())
						&& hidxpfRec.getZintbfnd().equals(h.getZintbfnd())
						&& h.getEffdate() <= hidxpfRec.getZlstintdte()) {
					hitrintRec = h; 
					readHitr5100(c, h);
				}
			}
		}
		if(hitrintRec == null){
			hitrintRec = new Hitrpf();
			hitrintRec.setChdrcoy(hidxpfRec.getChdrcoy());
			hitrintRec.setChdrnum(hidxpfRec.getChdrnum());
			hitrintRec.setLife(hidxpfRec.getLife());
			hitrintRec.setCoverage(hidxpfRec.getCoverage());
			hitrintRec.setRider(hidxpfRec.getRider());
			hitrintRec.setPlanSuffix(hidxpfRec.getPlanSuffix());
			hitrintRec.setZintbfnd(hidxpfRec.getZintbfnd());
			hitrintRec.setEffdate(hidxpfRec.getZlstintdte());
		}
		wsaaIntRound.setRounded(wsaaIntOnFundAmt);
		if (isLT(wsaaIntRound, 0)) {
			hidxpfRec.setZlstfndval(hidxpfRec.getZlstfndval().add(
					wsaaFundAmount));
		} else {
			hidxpfRec.setZlstfndval(hidxpfRec.getZlstfndval().add(
					add(wsaaIntRound, wsaaFundAmount).getbigdata()));
		}
		wsaaInterestTot.set(wsaaIntOnFundAmt);
		if (isGT(wsaaInterestTot, 0)) {
			writeHitr5200(c);
		}
	}

protected void skipLast3120(Covrpf c)
{
	for (ix=1; !(isGT(ix,wsaaTh510Size)
	|| isEQ(wsaaTh510Fund[ix],SPACES)
	|| (isEQ(wsaaTh510Fund[ix],hidxpfRec.getZintbfnd())
	&& isLTE(wsaaTh510Itmfrm[ix],hidxpfRec.getZnxtintdte()))); ix++)
{
		/*CONTINUE_STMT*/
	}
	if (isGT(ix,wsaaTh510Size)
	|| isEQ(wsaaTh510Fund[ix],SPACES)) {
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.endp);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(th510);
		itdmIO.setItemitem(hidxpfRec.getZintbfnd());
		itdmIO.setItmfrm(hidxpfRec.getZnxtintdte());
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(varcom.endp);
		fatalError600();
	}
	th510Ix = ix;
	/*    Calculate interest on the current fund balance.*/
	ibincalrec.ibincalRec.set(SPACES);
	ibincalrec.company.set(bsprIO.getCompany());
	if (isEQ(hidxpfRec.getZlstintdte(),varcom.vrcmMaxDate)) {
		ibincalrec.lastIntApp.set(hidxpfRec.getEffdate());
	}
	else {
		ibincalrec.lastIntApp.set(hidxpfRec.getZlstintdte());
	}
	ibincalrec.nextIntDue.set(hidxpfRec.getZnxtintdte());
	ibincalrec.capAmount.set(hidxpfRec.getZlstfndval());
	ibincalrec.intAmount.set(ZERO);
	ibincalrec.intRate.set(ZERO);
	ibincalrec.fund.set(hidxpfRec.getZintbfnd());
	ibincalrec.crrcd.set(c.getCrrcd());
	/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
	
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(wsaaTh510Zintcalc[th510Ix].toString()) && er.isExternalized(chdrpfRec.getCnttype(), c.getCrtable())))
	{
		callProgramX(wsaaTh510Zintcalc[th510Ix], ibincalrec.ibincalRec);
	}
	else
	{
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		Vpxibinrec vpxibinrec  = new Vpxibinrec();

		vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
		vpxibinrec.function.set("INIT");
		callProgram(Vpxibin.class, vpmcalcrec.vpmcalcRec, vpxibinrec);
		ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
		//ILIFE-7300
		ibincalrec.cnttype.set(chdrpfRec.getCnttype());
		ibincalrec.crtable.set(c.getCrtable());

		callProgram(wsaaTh510Zintcalc[th510Ix], ibincalrec.ibincalRec);
		
	}
	/* ILIFE-3142 End */
	if (isNE(ibincalrec.statuz,varcom.oK)) {
		syserrrec.params.set(ibincalrec.ibincalRec);
		syserrrec.statuz.set(ibincalrec.statuz);
		fatalError600();
	}
	/* MOVE IBIN-INT-AMOUNT         TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 8000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO IBIN-INT-AMOUNT.             */
	if (isNE(ibincalrec.intAmount, 0)) {
		zrdecplrec.amountIn.set(ibincalrec.intAmount);
		getAppVars().addDiagnostic("OLD = "+zrdecplrec.amountIn);
		callRounding8000();
		ibincalrec.intAmount.set(zrdecplrec.amountOut);
		getAppVars().addDiagnostic("NEW= "+ibincalrec.intAmount);
	}
	wsaaIntOnFundBal.set(ibincalrec.intAmount);
	/*    Now we calculate the interest due on all HITR records which*/
	/*    have been completed by Unit Deal, but which have not yet*/
	/*    been allocated interest for the current period.*/
	wsaaIntOnFundAmt.set(ZERO);
	wsaaFundAmount = BigDecimal.ZERO;
	
	wsaaInterestDate.set(hidxpfRec.getZnxtintdte());

	if (hitrintMap!=null && hitrintMap.containsKey(hidxpfRec.getChdrnum())) {
		List<Hitrpf> hitrintList = hitrintMap.get(hidxpfRec.getChdrnum());
		for (Hitrpf h : hitrintList) {
			if (hidxpfRec.getChdrcoy().equals(h.getChdrcoy())
					&& hidxpfRec.getLife().equals(h.getLife())
					&& hidxpfRec.getCoverage().equals(h.getCoverage())
					&& hidxpfRec.getRider().equals(h.getRider())
					&& (hidxpfRec.getPlanSuffix() == h.getPlanSuffix())
					&& hidxpfRec.getZintbfnd().equals(h.getZintbfnd())
					&& h.getEffdate() <= hidxpfRec.getZnxtintdte()) {
				hitrintRec = h;
				readHitr5100(c, h);
			}
		}
	}

	compute(wsaaInterestTot, 4).set(add(wsaaIntOnFundBal,wsaaIntOnFundAmt));
	if (isGT(wsaaInterestTot,0)) {
		writeHitr5200(c);
	}
	updateHitd5300();
}

protected void commit3500()
 {
		commitControlTotals();
		if (updateChdrpfInvalidList != null && !updateChdrpfInvalidList.isEmpty()) {
			chdrpfDAO.updateInvalidChdrRecord(updateChdrpfInvalidList);
			updateChdrpfInvalidList.clear();
		}
		if (insertChdrpfValidList != null && !insertChdrpfValidList.isEmpty()) {
			chdrpfDAO.insertChdrValidRecord(insertChdrpfValidList);
			insertChdrpfValidList.clear();
		}
		if (insertPtrnpfList != null && !insertPtrnpfList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(insertPtrnpfList);
			insertPtrnpfList.clear();
		}
		if (updateHitrpfList != null && !updateHitrpfList.isEmpty()) {
			hitrpfDAO.updatedHitrRecord(updateHitrpfList);
			updateHitrpfList.clear();
		}
		if (insertHitrpfList != null && !insertHitrpfList.isEmpty()) {
			hitrpfDAO.insertHitrpfRecord(insertHitrpfList);
			insertHitrpfList.clear();
		}
		if (updateHitdpfList != null && !updateHitdpfList.isEmpty()) {
			hitdpfDAO.updatedHitdRecord(updateHitdpfList);
			updateHitdpfList.clear();
		}
		if (insertHitdpfList != null && !insertHitdpfList.isEmpty()) {
			hitdpfDAO.insertHitdRecord(insertHitdpfList);
			insertHitdpfList.clear();
		}
		if (updateHitdpfInvalidList != null && !updateHitdpfInvalidList.isEmpty()) {
			hitdpfDAO.updatedHitdInvalidRecord(updateHitdpfInvalidList);
			updateHitdpfInvalidList.clear();
		}
	}

private void commitControlTotals(){
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Value);
	callProgram(Contot.class, contotrec.contotRec);		
	ct01Value = 0;

	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct02Value = 0;
	
	contotrec.totno.set(ct03);
	contotrec.totval.set(ct03Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct03Value = 0;
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct04Value = 0;
}

protected void rollback3600()
{
	/*ROLLBACK*/
	/*EXIT*/
}

protected void close4000()
{
	/*CLOSE-FILES*/
	lsaaStatuz.set(varcom.oK);
	/*EXIT*/
}

protected void updates5000()
{
	if(updateChdrpfInvalidList == null){
		updateChdrpfInvalidList = new ArrayList<Chdrpf>();
	}
	Chdrpf chdrpfInvalid = new Chdrpf();
	chdrpfInvalid.setValidflag('2');
	chdrpfInvalid.setCurrto(hidxpfRec.getZnxtintdte());
	chdrpfInvalid.setUniqueNumber(chdrpfRec.getUniqueNumber());
	updateChdrpfInvalidList.add(chdrpfInvalid);

	if(insertChdrpfValidList == null){
		insertChdrpfValidList = new ArrayList<>();
	}
	else if(insertChdrpfValidList.get(insertChdrpfValidList.size()-1).getChdrnum() == chdrpfRec.getChdrnum())
		insertChdrpfValidList.remove(insertChdrpfValidList.size()-1);
	chdrpfRec.setValidflag('1');
	chdrpfRec.setCurrfrom(hidxpfRec.getZnxtintdte());
	chdrpfRec.setCurrto(varcom.vrcmMaxDate.toInt());
	if (isNE(hidxpfRec.getChdrnum(), lastChdrnum)) 
		chdrpfRec.setTranno(chdrpfRec.getTranno()+1);
	else
		chdrpfRec.setTranno(chdrpfRec.getTranno()); 
	lastChdrnum.set(hidxpfRec.getChdrnum());
	wsaaTranno = chdrpfRec.getTranno();
	insertChdrpfValidList.add(chdrpfRec);
	
	Ptrnpf ptrnIO = new Ptrnpf();
	ptrnIO.setUserT(999999);
	ptrnIO.setTermid("");
	ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
	ptrnIO.setBatccoy(batcdorrec.company.toString());
	ptrnIO.setBatcbrn(batcdorrec.branch.toString());
	ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnIO.setBatcbatch(batcdorrec.batch.toString());
	ptrnIO.setPtrneff(hidxpfRec.getZnxtintdte());
	ptrnIO.setValidflag("1");
	ptrnIO.setChdrcoy(chdrpfRec.getChdrcoy().toString());
	ptrnIO.setChdrnum(chdrpfRec.getChdrnum());
	ptrnIO.setTranno(chdrpfRec.getTranno());
	ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
	ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
	ptrnIO.setTrdt(Integer.parseInt(getCobolDate()));
	PackedDecimalData trtm = new PackedDecimalData(6, 0);
	trtm.set(getCobolTime());
	ptrnIO.setTrtm(trtm.toInt());
	/* MOVE WSAA-TODAY             TO PTRN-DATESUB.         <LA5184>*/
	ptrnIO.setCrtuser(bsscIO.getUserName().toString());
	
	if(insertPtrnpfList == null){
		insertPtrnpfList = new ArrayList<>();
	}
	//IBPLIFE-1839 
	if(!ptrnpfValidator.contains(chdrpfRec.getChdrcoy().toString().concat(chdrpfRec.getChdrnum()))) {
	    insertPtrnpfList.add(ptrnIO);
	    ptrnpfValidator.add(chdrpfRec.getChdrcoy().toString().concat(chdrpfRec.getChdrnum()));
	}
}


protected void readHitr5100(Covrpf c, Hitrpf h)
{
	if(hitr5110(c,h)){
		skipInterest5150(h);
	}
}

protected boolean hitr5110(Covrpf c,Hitrpf h)
{
	/*    If the interest allocation field is set to 'N', then*/
	/*    interest allocation is not required for that particular*/
	/*    transaction. However, we must still include the amount*/
	/*    of the HITR when updating the HITD record with the latest*/
	/*    fund balance.*/
	/*    Note: there is no interest allocation for the following*/
	/*    transactions:*/
	/*    1. Fund switching (out only)*/
	/*    2. Reversal of fund switch (in and out)*/
	/*    3. Surrenders and claims*/
	/*    4. Reversal of surrenders and claims*/
	/*    5. Debt recovery*/
	if (isEQ(h.getZintalloc(),"N")) {
		ibincalrec.intEffdate.set(varcom.vrcmMaxDate);
		ibincalrec.intRate.set(ZERO);
		return true;
	}
	/* If the effective date on the HITR is the same as the current    */
	/* interest date, then this record is not due for processing yet.  */
	if (isEQ(h.getEffdate(),wsaaInterestDate)) {
		return false;
	}
	ibincalrec.ibincalRec.set(SPACES);
	ibincalrec.company.set(bsprIO.getCompany());
	ibincalrec.lastIntApp.set(h.getEffdate());
	ibincalrec.nextIntDue.set(wsaaInterestDate);
	ibincalrec.capAmount.set(h.getFundAmount());
	ibincalrec.intAmount.set(ZERO);
	ibincalrec.intRate.set(ZERO);
	ibincalrec.fund.set(hidxpfRec.getZintbfnd());
	ibincalrec.crrcd.set(c.getCrrcd());
	
	/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */

	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(wsaaTh510Zintcalc[th510Ix] .toString())))
	{
		callProgram(wsaaTh510Zintcalc[th510Ix], ibincalrec.ibincalRec);
	}
	else
	{
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		Vpxibinrec vpxibinrec  = new Vpxibinrec();

		vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
		vpxibinrec.function.set("INIT");
		callProgram(Vpxibin.class, vpmcalcrec.vpmcalcRec, vpxibinrec);
		ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
		//ILIFE-7300
		ibincalrec.cnttype.set(chdrpfRec.getCnttype());
		ibincalrec.crtable.set(c.getCrtable());

		callProgram(wsaaTh510Zintcalc[th510Ix], ibincalrec.ibincalRec);
		
	}
	/* ILIFE-3142 End */
	if (isNE(ibincalrec.statuz,varcom.oK)) {
		syserrrec.params.set(ibincalrec.ibincalRec);
		syserrrec.statuz.set(ibincalrec.statuz);
		fatalError600();
	}
	/* MOVE IBIN-INT-AMOUNT         TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 8000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO IBIN-INT-AMOUNT.             */
	if (isNE(ibincalrec.intAmount, 0)) {
		zrdecplrec.amountIn.set(ibincalrec.intAmount);
		getAppVars().addDiagnostic("OLD = "+zrdecplrec.amountIn);
		callRounding8000();
		ibincalrec.intAmount.set(zrdecplrec.amountOut);
		getAppVars().addDiagnostic("NEW = "+ibincalrec.intAmount);
	}
	wsaaIntOnFundAmt.add(ibincalrec.intAmount);
	return true;
}

protected void skipInterest5150(Hitrpf h)
{
	wsaaFundAmount = wsaaFundAmount.add(h.getFundAmount());
	Hitrpf hitrIO = new Hitrpf();
	hitrIO.setUniqueNumber(h.getUniqueNumber());
	hitrIO.setZintappind("Y");
	hitrIO.setZlstintdte(wsaaInterestDate.toInt());
	hitrIO.setZinteffdt(ibincalrec.intEffdate.toInt());
	hitrIO.setZintrate(ibincalrec.intRate.getbigdata());
	
	if(updateHitrpfList == null){
		updateHitrpfList = new ArrayList<>();
	}
	updateHitrpfList.add(hitrIO);
}

protected void writeHitr5200(Covrpf c)
 {
		Hitrpf hitrIO = new Hitrpf();
		//hitrIO.setFeedbackInd("");
		hitrIO.setFeedbackInd(SPACE); //ICIL-293
		hitrIO.setTranno(0);
		hitrIO.setUstmno(0);
		hitrIO.setPlanSuffix(0);
		hitrIO.setFundRate(BigDecimal.ZERO);
		hitrIO.setInciNum(0);
		hitrIO.setInciPerd01(0);
		hitrIO.setInciPerd02(0);
		hitrIO.setInciprm01(BigDecimal.ZERO);
		hitrIO.setInciprm02(BigDecimal.ZERO);
		hitrIO.setContractAmount(BigDecimal.ZERO);
		hitrIO.setFundAmount(BigDecimal.ZERO);
		hitrIO.setProcSeqNo(0);
		hitrIO.setSvp(BigDecimal.ZERO);
		hitrIO.setSurrenderPercent(BigDecimal.ZERO);
		hitrIO.setChdrcoy(hidxpfRec.getChdrcoy());
		hitrIO.setBatctrcde(batcdorrec.trcde.toString());
		hitrIO.setBatccoy(batcdorrec.company.toString());
		hitrIO.setBatcbrn(batcdorrec.branch.toString());
		hitrIO.setBatcactyr(batcdorrec.actyear.toInt());
		hitrIO.setBatcactmn(batcdorrec.actmonth.toInt());
		hitrIO.setBatcbatch(batcdorrec.batch.toString());
		hitrIO.setChdrnum(hidxpfRec.getChdrnum());
		hitrIO.setPlanSuffix(hidxpfRec.getPlanSuffix());
		hitrIO.setLife(hidxpfRec.getLife());
		hitrIO.setCoverage(hidxpfRec.getCoverage());
		hitrIO.setRider(hidxpfRec.getRider());
		hitrIO.setCrtable(c.getCrtable());
		hitrIO.setProcSeqNo(wsaaT6647ProcSeqNo[t6647Ix].toInt());
		hitrIO.setFundAmount(BigDecimal.ZERO);
		hitrIO.setZintbfnd(hidxpfRec.getZintbfnd());
		hitrIO.setCnttyp(chdrpfRec.getCnttype());
		if (componLevelAccounted.isTrue()) {
			hitrIO.setSacscode(t5645rec.sacscode02.toString());
			hitrIO.setSacstyp(t5645rec.sacstype02.toString());
			hitrIO.setGenlcde(t5645rec.glmap02.toString());
		} else {
			hitrIO.setSacscode(t5645rec.sacscode01.toString());
			hitrIO.setSacstyp(t5645rec.sacstype01.toString());
			hitrIO.setGenlcde(t5645rec.glmap01.toString());
		}
		hitrIO.setTranno(wsaaTranno);
		hitrIO.setSvp(BigDecimal.ONE);
		hitrIO.setZrectyp("I");
		/* Read Table T5515 to find the Fund currency. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hitrIO.getChdrcoy());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(hitrIO.getZintbfnd());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		hitrIO.setFundCurrency(t5515rec.currcode.toString());
		hitrIO.setCntcurr(chdrpfRec.getCntcurr());
		hitrIO.setEffdate(wsaaInterestDate.toInt());
		hitrIO.setZlstintdte(99999999);
		hitrIO.setZinteffdt(99999999);
		hitrIO.setZintappind("Y");
		hitrIO.setZintalloc("N");
		hitrIO.setZintrate(BigDecimal.ZERO);
		hitrIO.setZlstintdte(wsaaInterestDate.toInt());
		hitrIO.setZinteffdt(ibincalrec.intEffdate.toInt());
		hitrIO.setZintrate(ibincalrec.intRate.getbigdata());
		wsaaIntRound.setRounded(wsaaInterestTot);
		hitrIO.setFundAmount(wsaaIntRound.getbigdata());
		if (isEQ(wsaaIntRound, 0)) {
			return;
		}
		hitrIO.setZlstintdte(wsaaInterestDate.toInt());
		hitrIO.setZinteffdt(ibincalrec.intEffdate.toInt());
		hitrIO.setZintrate(ibincalrec.intRate.getbigdata());

		if (insertHitrpfList == null) {
			insertHitrpfList = new ArrayList<>();
		}
		insertHitrpfList.add(hitrIO);
	}


protected void updateHitd5300()
{
	Hitdpf hd = hitd5310();
	calc5385(hd);
}

protected Hitdpf hitd5310()
 {
		Hitdpf hdResult = null;
		if(lastHitdpfRecord == null){
			if (hitdpfMap != null && hitdpfMap.containsKey(hidxpfRec.getChdrnum())) {
				for (Hitdpf hd : hitdpfMap.get(hidxpfRec.getChdrnum())) {
					if (hidxpfRec.getChdrcoy().equals(hd.getChdrcoy())
							&& hidxpfRec.getLife().equals(hd.getLife())
							&& hidxpfRec.getCoverage().equals(hd.getCoverage())
							&& hidxpfRec.getRider().equals(hd.getRider())
							&& (hidxpfRec.getPlanSuffix() == hd.getPlanSuffix())
							&& hidxpfRec.getZintbfnd().equals(hd.getZintbfnd())) {
						hdResult = hd;
						break;
					}
				}
			}
		}else{
			hdResult = lastHitdpfRecord;
		}
		
		if (hdResult == null) {
			syserrrec.params.set(hidxpfRec.getChdrnum());
			fatalError600();
		}
		
		if (hdResult != null  ) {
			if (updateHitdpfList == null) {
				updateHitdpfList = new ArrayList<Hitdpf>();
			}
			
			updateHitd5380(hdResult);
			updateHitdpfList.add(hdResult);
			return hdResult;
		}
		//IJTI-320 START
		if (hdResult != null){
			updateInvalidHitdpfRecord(hdResult);
		}
		//IJTI-320 END
		
		if (insertHitdpfList == null) {
			insertHitdpfList = new ArrayList<Hitdpf>();
		}
		if(lastHitdpfRecord!=null){
			lastHitdpfRecord.setValidflag("2");
		}
		Hitdpf hdNew = new Hitdpf(hdResult);
		insertHitdpfList.add(hdNew);
		updateHitd5380(hdNew);
		lastHitdpfRecord = hdNew;
		return hdNew;
	}
	private void updateInvalidHitdpfRecord(Hitdpf hdResult){
		if (updateHitdpfInvalidList == null) {
			updateHitdpfInvalidList = new ArrayList<Hitdpf>();
		}
		for(Hitdpf h:updateHitdpfInvalidList){
			if(h.getUniqueNumber() == hdResult.getUniqueNumber()){
				return;
			}
		}
		Hitdpf hdOld = new Hitdpf(hdResult);
		hdOld.setValidflag("2");
		updateHitdpfInvalidList.add(hdOld);
	}
protected void updateHitd5380(Hitdpf hd)
{
	/* If O/S HITR is one of the transaction defined on T6626, no      */
	/* more interest in future.                                        */
	if (isEQ(wsaaTriggerFound,"Y")
	&& isNE(wsaaBatctrcde,SPACES)) {
		wsaaNoMoreInt.set("N");
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			if (isEQ(wsaaBatctrcde,t6626rec.trncd[wsaaSub.toInt()])) {
				wsaaNoMoreInt.set("Y");
				wsaaSub.set(11);
			}
		}
		if (isEQ(wsaaNoMoreInt,"Y")) {
			hd.setZnxtintdte(99999999);
			return;
		}
	}
	getNextFreq5500();
	/*    MOVE DTC2-INT-DATE-2        TO HITD-ZNXTINTDTE.      <LA4351>*/
	hd.setZnxtintdte(datcon4rec.intDate2.toInt());
}

protected void calc5385(Hitdpf hitdIO)
{
	wsaaIntRound.setRounded(wsaaInterestTot);
	if (isLT(wsaaIntRound,0)) {
		hitdIO.setZlstfndval(hidxpfRec.getZlstfndval().add(wsaaFundAmount));
	}
	else {
		hitdIO.setZlstfndval(hidxpfRec.getZlstfndval().add(wsaaIntRound.getbigdata()).add(wsaaFundAmount));
	}
	hitdIO.setZlstintdte(hidxpfRec.getZnxtintdte());
	hitdIO.setTranno(wsaaTranno);
	hitdIO.setEffdate(bsscIO.getEffectiveDate().toInt());
	hitdIO.setValidflag("1");

	hidxpfRec.setZnxtintdte(hitdIO.getZnxtintdte());
	hidxpfRec.setZlstintdte(hitdIO.getZlstintdte());
	hidxpfRec.setZlstfndval(hitdIO.getZlstfndval());
}

protected void getNextFreq5500()
 {

		/* MOVE SPACES TO DTC2-DATCON2-REC. */
		/* MOVE ZNXTINTDTE TO DTC2-INT-DATE-1. */
		/* MOVE 1 TO DTC2-FREQ-FACTOR. */
		/* MOVE WSAA-TH510-ZINTALOFRQ (TH510-IX) TO DTC2-FREQUENCY. */
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC. */
		/* IF DTC2-STATUZ NOT = O-K */
		/* MOVE DTC2-DATCON2-REC TO SYSR-PARAMS */
		/* MOVE DTC2-STATUZ TO SYSR-STATUZ */
		/* PERFORM 600-FATAL-ERROR */
		/* END-IF. */
		/* IF WSAA-TH510-ZINTFIXDD (TH510-IX) NOT = ZEROES */
		/* MOVE WSAA-TH510-ZINTFIXDD (TH510-IX) */
		/* TO DTC2-INT-DATE-2 (5:2) */
		/* END-IF. */
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(hidxpfRec.getZnxtintdte());
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(wsaaTh510Zintalofrq[th510Ix]);
		datcon4rec.intDate2.set(0);
		datcon4rec.billdayNum.set(subString(datcon4rec.intDate1, 7, 2));
		datcon4rec.billmonthNum.set(subString(datcon4rec.intDate1, 5, 2));
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		if (isNE(wsaaTh510Zintfixdd[th510Ix], ZERO)) {
			datcon4rec.intDate2.setSub1String(7, 2,
					wsaaTh510Zintfixdd[th510Ix]);
		}
	}


protected void callRounding8000()
{
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(bsprIO.getCompany());
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(hitrintRec.getFundCurrency());
	zrdecplrec.batctrcde.set(batcdorrec.trcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		fatalError600();
	}
	/*EXIT*/
}
/*
* Class transformed  from Data Structure FORMATS--INNER
*/
private static final class FormatsInner {
private FixedLengthStringData bsprrec = new FixedLengthStringData(10).init("BSPRREC");
private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}

}