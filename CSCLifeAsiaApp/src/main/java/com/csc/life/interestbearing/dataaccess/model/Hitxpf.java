/*
 * File: Hitxpf.java
 * Date: 05 May 2017 
 * Author: CSC
 * 
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.dataaccess.model;

public class Hitxpf {
	
	public void hitxpf() {
	}
	
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String zintbfnd;
	private int tranno;
	private String zrectyp;
	private int effdate;
	private int procSeqNo;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getZintbfnd() {
		return zintbfnd;
	}
	public void setZintbfnd(String zintbfnd) {
		this.zintbfnd = zintbfnd;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getZrectyp() {
		return zrectyp;
	}
	public void setZrectyp(String zrectyp) {
		this.zrectyp = zrectyp;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(int procSeqNo) {
		this.procSeqNo = procSeqNo;
	}
	
	

}
