package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:37
 * Description:
 * Copybook name: HITDEXTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitdextkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitdextFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitdextKey = new FixedLengthStringData(64).isAPartOf(hitdextFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitdextChdrcoy = new FixedLengthStringData(1).isAPartOf(hitdextKey, 0);
  	public FixedLengthStringData hitdextChdrnum = new FixedLengthStringData(8).isAPartOf(hitdextKey, 1);
  	public FixedLengthStringData hitdextZintbfnd = new FixedLengthStringData(4).isAPartOf(hitdextKey, 9);
  	public PackedDecimalData hitdextZnxtintdte = new PackedDecimalData(8, 0).isAPartOf(hitdextKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(hitdextKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitdextFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitdextFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}