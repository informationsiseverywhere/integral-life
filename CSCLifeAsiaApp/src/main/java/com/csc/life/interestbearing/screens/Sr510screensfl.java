package com.csc.life.interestbearing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr510screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 18;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 9, 4, 5, 6, 7, 1, 2, 3}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {5, 21, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr510ScreenVars sv = (Sr510ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr510screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr510screensfl, 
			sv.Sr510screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr510ScreenVars sv = (Sr510ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr510screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr510ScreenVars sv = (Sr510ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr510screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr510screensflWritten.gt(0))
		{
			sv.sr510screensfl.setCurrentIndex(0);
			sv.Sr510screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr510ScreenVars sv = (Sr510ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr510screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr510ScreenVars screenVars = (Sr510ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.interate01.setFieldName("interate01");
				screenVars.interate02.setFieldName("interate02");
				screenVars.fromdate01Disp.setFieldName("fromdate01Disp");
				screenVars.todateDisp.setFieldName("todateDisp");
				screenVars.interate03.setFieldName("interate03");
				screenVars.interate04.setFieldName("interate04");
				screenVars.fromdate02Disp.setFieldName("fromdate02Disp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.shortdesc.set(dm.getField("shortdesc"));
			screenVars.interate01.set(dm.getField("interate01"));
			screenVars.interate02.set(dm.getField("interate02"));
			screenVars.fromdate01Disp.set(dm.getField("fromdate01Disp"));
			screenVars.todateDisp.set(dm.getField("todateDisp"));
			screenVars.interate03.set(dm.getField("interate03"));
			screenVars.interate04.set(dm.getField("interate04"));
			screenVars.fromdate02Disp.set(dm.getField("fromdate02Disp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr510ScreenVars screenVars = (Sr510ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.interate01.setFieldName("interate01");
				screenVars.interate02.setFieldName("interate02");
				screenVars.fromdate01Disp.setFieldName("fromdate01Disp");
				screenVars.todateDisp.setFieldName("todateDisp");
				screenVars.interate03.setFieldName("interate03");
				screenVars.interate04.setFieldName("interate04");
				screenVars.fromdate02Disp.setFieldName("fromdate02Disp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("shortdesc").set(screenVars.shortdesc);
			dm.getField("interate01").set(screenVars.interate01);
			dm.getField("interate02").set(screenVars.interate02);
			dm.getField("fromdate01Disp").set(screenVars.fromdate01Disp);
			dm.getField("todateDisp").set(screenVars.todateDisp);
			dm.getField("interate03").set(screenVars.interate03);
			dm.getField("interate04").set(screenVars.interate04);
			dm.getField("fromdate02Disp").set(screenVars.fromdate02Disp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr510screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr510ScreenVars screenVars = (Sr510ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.shortdesc.clearFormatting();
		screenVars.interate01.clearFormatting();
		screenVars.interate02.clearFormatting();
		screenVars.fromdate01Disp.clearFormatting();
		screenVars.todateDisp.clearFormatting();
		screenVars.interate03.clearFormatting();
		screenVars.interate04.clearFormatting();
		screenVars.fromdate02Disp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr510ScreenVars screenVars = (Sr510ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.interate01.setClassString("");
		screenVars.interate02.setClassString("");
		screenVars.fromdate01Disp.setClassString("");
		screenVars.todateDisp.setClassString("");
		screenVars.interate03.setClassString("");
		screenVars.interate04.setClassString("");
		screenVars.fromdate02Disp.setClassString("");
	}

/**
 * Clear all the variables in Sr510screensfl
 */
	public static void clear(VarModel pv) {
		Sr510ScreenVars screenVars = (Sr510ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.shortdesc.clear();
		screenVars.interate01.clear();
		screenVars.interate02.clear();
		screenVars.fromdate01Disp.clear();
		screenVars.fromdate01.clear();
		screenVars.todateDisp.clear();
		screenVars.todate.clear();
		screenVars.interate03.clear();
		screenVars.interate04.clear();
		screenVars.fromdate02Disp.clear();
		screenVars.fromdate02.clear();
	}
}
