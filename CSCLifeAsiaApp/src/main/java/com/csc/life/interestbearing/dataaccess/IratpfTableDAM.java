package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IratpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:39
 * Class transformed from IRATPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IratpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 67;
	public FixedLengthStringData iratrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData iratpfRecord = iratrec;
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(iratrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(iratrec);
	public PackedDecimalData fromdate = DD.fromdate.copy().isAPartOf(iratrec);
	public PackedDecimalData todate = DD.todate.copy().isAPartOf(iratrec);
	public PackedDecimalData interate01 = DD.interate.copy().isAPartOf(iratrec);
	public PackedDecimalData interate02 = DD.interate.copy().isAPartOf(iratrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(iratrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(iratrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(iratrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public IratpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for IratpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public IratpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for IratpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public IratpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for IratpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public IratpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("IRATPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"COMPANY, " +
							"VRTFND, " +
							"FROMDATE, " +
							"TODATE, " +
							"INTERATE01, " +
							"INTERATE02, " +
							"JOBNM, " +
							"USRPRF, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     company,
                                     unitVirtualFund,
                                     fromdate,
                                     todate,
                                     interate01,
                                     interate02,
                                     jobName,
                                     userProfile,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		company.clear();
  		unitVirtualFund.clear();
  		fromdate.clear();
  		todate.clear();
  		interate01.clear();
  		interate02.clear();
  		jobName.clear();
  		userProfile.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getIratrec() {
  		return iratrec;
	}

	public FixedLengthStringData getIratpfRecord() {
  		return iratpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setIratrec(what);
	}

	public void setIratrec(Object what) {
  		this.iratrec.set(what);
	}

	public void setIratpfRecord(Object what) {
  		this.iratpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(iratrec.getLength());
		result.set(iratrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}