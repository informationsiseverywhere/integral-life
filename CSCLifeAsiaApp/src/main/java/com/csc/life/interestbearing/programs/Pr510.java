/*
 * File: Pr510.java
 * Date: 30 August 2009 1:34:17
 * Author: Quipoz Limited
 *
 * Class transformed from PR510.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.interestbearing.dataaccess.IratTableDAM;
import com.csc.life.interestbearing.dataaccess.IratchgTableDAM;
import com.csc.life.interestbearing.dataaccess.IratcurTableDAM;
import com.csc.life.interestbearing.screens.Sr510ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This program is cloned from P5190.
*
* Interest Bearing Generic Interest Rate Entry Screen.
*
* This program permits entry and deletion of and enquiry upon
* interest rates.  If a fund was entered on the submenu, only
* information pertaining to this fund is displayed or available
* for updating.  If no fund was entered on the submenu, all funds
* eligible for interest rate entry are displayed or available for
* updating.
*
*
* Processing Required
* ---------------------
*
* When entering or deleting rates, if a fund is entered, the
* current record for that fund is displayed.  If no fund is
* entered, the current record for all eligible funds are
* displayed.
* When enquiring on rates, if a fund is entered without a date,
* all rates for that fund are displayed in descending date order.
* If a fund and a date are entered, the record for the fund where
* the date entered falls within the From and To dates is
* displayed.  If only the date is entered, records for all funds
* where the date entered falls within the From and To dates are
* displayed.
*
* When entering a new rate, update the To date of the existing
* current rate before writing the new rate.  When deleting the
* current rate, update the To date of the old record, reinstating
* it as the current record.
*
*
*****************************************************************
* </pre>
*/
public class Pr510 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR510");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaVrtfund = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaInterate = new ZonedDecimalData(4, 2).init(ZERO);
	private ZonedDecimalData wsaaFromdate = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaDescription = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaUnitItem = new FixedLengthStringData(4).init(SPACES);
		/* TABLES */
	private String t5515 = "T5515";
	private String descrec = "DESCREC";
	private String iratrec = "IRATREC";
	private String iratchgrec = "IRATCHGREC";
	private String iratcurrec = "IRATCURREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Interest Rate File*/
	private IratTableDAM iratIO = new IratTableDAM();
		/*Interest Rate File (Fromdate - Desc)*/
	private IratchgTableDAM iratchgIO = new IratchgTableDAM();
		/*Interest Rate File*/
	private IratcurTableDAM iratcurIO = new IratcurTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5515rec t5515rec = new T5515rec();
	private Sr510ScreenVars sv = ScreenProgram.getScreenVars( Sr510ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1155,
		next1165,
		exit1165,
		exit1175,
		exit1210,
		preExit,
		exit3090,
		exit3190,
		exit3200,
		exit3300
	}

	public Pr510() {
		super();
		screenVars = sv;
		new ScreenModel("Sr510", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		iratIO.setRecKeyData(SPACES);
		iratIO.setRecNonKeyData(SPACES);
		iratIO.setFunction(varcom.retrv);
		iratIO.setFormat(iratrec);
		SmartFileCode.execute(appVars, iratIO);
		if (isNE(iratIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(iratIO.getParams());
			fatalError600();
		}
		iratIO.setFunction(varcom.rlse);
		iratIO.setFormat(iratrec);
		SmartFileCode.execute(appVars, iratIO);
		if (isNE(iratIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(iratIO.getParams());
			fatalError600();
		}
		wsaaVrtfund.set(iratIO.getUnitVirtualFund());
		wsaaFromdate.set(iratIO.getFromdate());
		initialize(sv.dataArea);
		sv.initialiseSubfileArea();
		sv.fromdate01.set(varcom.vrcmMaxDate);
		sv.fromdate02.set(varcom.vrcmMaxDate);
		sv.todate.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR510", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		scrnparams.subfileMore.set("N");
		loadSubfile1100();
	}

protected void loadSubfile1100()
	{
		/*START*/
		if (isEQ(wsspcomn.flag,"C")) {
			create1110();
		}
		if (isEQ(wsspcomn.flag,"D")) {
			delete1120();
		}
		if (isEQ(wsspcomn.flag,"I")) {
			iratchg1160();
		}
		/*EXIT*/
	}

protected void create1110()
	{
		/*START*/
		if (isEQ(wsaaVrtfund,SPACES)) {
			begnT55151200();
		}
		else {
			iratcur1140();
		}
		/*EXIT*/
	}

protected void delete1120()
	{
		/*START*/
		if (isEQ(wsaaVrtfund,SPACES)) {
			irat1150();
		}
		else {
			iratDelete1170();
		}
		/*EXIT*/
	}

protected void iratcur1140()
	{
		start1140();
	}

protected void start1140()
	{
		iratcurIO.setRecKeyData(SPACES);
		iratcurIO.setRecNonKeyData(SPACES);
		iratcurIO.setStatuz(varcom.oK);
		iratcurIO.setUnitVirtualFund(wsaaVrtfund);
		iratcurIO.setCompany(wsspcomn.company);
		iratcurIO.setFunction(varcom.readr);
		iratcurIO.setFormat(iratcurrec);
		SmartFileCode.execute(appVars, iratcurIO);
		if (isNE(iratcurIO.getStatuz(),varcom.oK)
		&& isNE(iratcurIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(iratcurIO.getParams());
			syserrrec.statuz.set(iratcurIO.getStatuz());
			fatalError600();
		}
		if (isEQ(iratcurIO.getStatuz(),varcom.mrnf)) {
			iratcurIO.setFromdate(varcom.vrcmMaxDate);
			iratcurIO.setTodate(varcom.vrcmMaxDate);
		}
		wsaaDescription.set(iratcurIO.getUnitVirtualFund());
		getDescripT55151300();
		sv.unitVirtualFund.set(iratcurIO.getUnitVirtualFund());
		sv.fromdate01.set(iratcurIO.getFromdate());
		sv.todate.set(iratcurIO.getTodate());
		sv.interate01.set(iratcurIO.getInterate01());
		sv.interate02.set(iratcurIO.getInterate02());
		sv.shortdesc.set(descIO.getShortdesc());
		sv.fromdate02.set(wsaaFromdate);
		addtoSubfile1400();
	}

protected void irat1150()
	{
		/*START*/
		iratIO.setRecKeyData(SPACES);
		iratIO.setRecNonKeyData(SPACES);
		iratIO.setStatuz(varcom.oK);
		iratIO.setUnitVirtualFund(wsaaVrtfund);
		iratIO.setCompany(wsspcomn.company);
		iratIO.setFromdate(wsaaFromdate);
		iratIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		iratIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		iratIO.setFitKeysSearch("COMPANY");
		iratIO.setFormat(iratrec);
		wsaaCount.set(1);
		while ( !(isEQ(iratIO.getStatuz(),varcom.endp)
		|| isGT(wsaaCount,sv.subfilePage))) {
			loadTableSubfile1155();
		}

		/*EXIT*/
	}

protected void loadTableSubfile1155()
	{
		try {
			start1155();
		}
		catch (GOTOException e){
		}
	}

protected void start1155()
	{
		SmartFileCode.execute(appVars, iratIO);
		if (isNE(iratIO.getStatuz(),varcom.oK)
		&& isNE(iratIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratIO.getParams());
			syserrrec.statuz.set(iratIO.getStatuz());
			fatalError600();
		}
		if (isEQ(iratIO.getStatuz(),varcom.endp)
		|| isNE(iratIO.getCompany(),wsspcomn.company)) {
			goTo(GotoLabel.exit1155);
		}
		wsaaDescription.set(iratIO.getUnitVirtualFund());
		getDescripT55151300();
		sv.fromdate01.set(varcom.vrcmMaxDate);
		sv.fromdate02.set(varcom.vrcmMaxDate);
		sv.todate.set(varcom.vrcmMaxDate);
		if (isNE(wsaaFromdate,varcom.vrcmMaxDate)
		&& isEQ(wsaaFromdate,iratIO.getFromdate())) {
			sv.unitVirtualFund.set(iratIO.getUnitVirtualFund());
			sv.fromdate01.set(iratIO.getFromdate());
			sv.todate.set(iratIO.getTodate());
			sv.interate01.set(iratIO.getInterate01());
			sv.interate02.set(iratIO.getInterate02());
			sv.shortdesc.set(descIO.getShortdesc());
			addtoSubfile1400();
		}
		if (isEQ(wsspcomn.flag,"I")
		&& isGTE(wsaaFromdate,iratIO.getFromdate())
		&& isLTE(wsaaFromdate,iratIO.getTodate())) {
			sv.unitVirtualFund.set(iratIO.getUnitVirtualFund());
			sv.fromdate01.set(iratIO.getFromdate());
			sv.todate.set(iratIO.getTodate());
			sv.interate01.set(iratIO.getInterate01());
			sv.interate02.set(iratIO.getInterate02());
			sv.shortdesc.set(descIO.getShortdesc());
			addtoSubfile1400();
		}
		iratIO.setFunction(varcom.nextr);
	}

protected void iratchg1160()
	{
		start1160();
	}

protected void start1160()
	{
		iratchgIO.setRecKeyData(SPACES);
		iratchgIO.setRecNonKeyData(SPACES);
		iratchgIO.setStatuz(varcom.oK);
		iratchgIO.setCompany(wsspcomn.company);
		iratchgIO.setFromdate(wsaaFromdate);
		iratchgIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		iratchgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		// Ticket #ILIFE-127 - Cannot enquire on the interest rate that has just been entered
		if(isNE(wsaaVrtfund,SPACE)){
			iratchgIO.setFitKeysSearch("VRTFND");
			iratchgIO.setUnitVirtualFund(wsaaVrtfund);
		}
		iratchgIO.setFormat(iratchgrec);
		iratchgIO.setStatuz(varcom.oK);
		wsaaCount.set(1);
		while ( !(isEQ(iratchgIO.getStatuz(),varcom.endp)
		|| isGT(wsaaCount,sv.subfilePage))) {
			loadIratchgSubfile1165();
		}

		if (isGT(wsaaCount,sv.subfilePage)) {
			scrnparams.subfileMore.set("Y");
		}
		scrnparams.subfileRrn.set(1);
	}

protected void loadIratchgSubfile1165()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1165();
				}
				case next1165: {
					next1165();
				}
				case exit1165: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1165()
	{
		SmartFileCode.execute(appVars, iratchgIO);
		if (isEQ(iratchgIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1165);
		}
		if (isNE(iratchgIO.getStatuz(),varcom.oK)
		&& isNE(iratchgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratchgIO.getParams());
			syserrrec.statuz.set(iratchgIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsaaVrtfund,SPACES)) {
			if (isNE(wsaaVrtfund,iratchgIO.getUnitVirtualFund())) {
				iratchgIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit1165);
			}
		}
		if (isNE(wsaaFromdate,varcom.vrcmMaxDate)) {
			if (isGTE(wsaaFromdate,iratchgIO.getFromdate())
			&& isLTE(wsaaFromdate,iratchgIO.getTodate())) {
				/*NEXT_SENTENCE*/
			}
			else {
				goTo(GotoLabel.next1165);
			}
		}
		wsaaDescription.set(iratchgIO.getUnitVirtualFund());
		getDescripT55151300();
		sv.fromdate01.set(varcom.vrcmMaxDate);
		sv.fromdate02.set(varcom.vrcmMaxDate);
		sv.todate.set(varcom.vrcmMaxDate);
		if (isEQ(iratchgIO.getFromdate(),ZERO)) {
			sv.fromdate01.set(varcom.vrcmMaxDate);
			sv.todate.set(varcom.vrcmMaxDate);
		}
		sv.unitVirtualFund.set(iratchgIO.getUnitVirtualFund());
		sv.fromdate01.set(iratchgIO.getFromdate());
		sv.todate.set(iratchgIO.getTodate());
		sv.interate01.set(iratchgIO.getInterate01());
		sv.interate02.set(iratchgIO.getInterate02());
		sv.interate03Out[varcom.pr.toInt()].set("Y");
		sv.interate04Out[varcom.pr.toInt()].set("Y");
		sv.shortdesc.set(descIO.getShortdesc());
		addtoSubfile1400();
	}

protected void next1165()
	{
		iratchgIO.setFunction(varcom.nextr);
	}

protected void iratDelete1170()
	{
		/*START*/
		iratchgIO.setRecKeyData(SPACES);
		iratchgIO.setRecNonKeyData(SPACES);
		iratchgIO.setStatuz(varcom.oK);
		iratchgIO.setCompany(wsspcomn.company);
		iratchgIO.setUnitVirtualFund(wsaaVrtfund);
		iratchgIO.setFromdate(varcom.vrcmMaxDate);
		iratchgIO.setFormat(iratchgrec);
		iratchgIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		iratchgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		iratchgIO.setFitKeysSearch("VRTFND");
		while ( !(isEQ(iratchgIO.getStatuz(),varcom.endp))) {
			deleteNext1175();
		}

		addtoSubfile1400();
		/*EXIT*/
	}

protected void deleteNext1175()
	{
		try {
			start1175();
		}
		catch (GOTOException e){
		}
	}

protected void start1175()
	{
		SmartFileCode.execute(appVars, iratchgIO);
		if (isNE(iratchgIO.getStatuz(),varcom.oK)
		&& isNE(iratchgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratchgIO.getParams());
			syserrrec.statuz.set(iratchgIO.getStatuz());
			fatalError600();
		}
		if (isEQ(iratchgIO.getStatuz(),varcom.endp)
		|| isNE(iratchgIO.getUnitVirtualFund(),wsaaVrtfund)) {
			iratchgIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1175);
		}
		if (isEQ(iratchgIO.getTodate(),varcom.vrcmMaxDate)) {
			sv.unitVirtualFund.set(iratchgIO.getUnitVirtualFund());
			sv.interate01.set(iratchgIO.getInterate01());
			sv.interate02.set(iratchgIO.getInterate02());
			sv.fromdate01.set(iratchgIO.getFromdate());
			sv.todate.set(iratchgIO.getTodate());
			sv.interate03.set(iratchgIO.getInterate01());
			sv.interate04.set(iratchgIO.getInterate02());
			sv.fromdate02.set(iratchgIO.getTodate());
			sv.shortdesc.set(descIO.getShortdesc());
		}
		else {
			sv.interate03.set(iratchgIO.getInterate01());
			sv.interate04.set(iratchgIO.getInterate02());
			sv.fromdate02.set(iratchgIO.getFromdate());
			iratchgIO.setStatuz(varcom.endp);
		}
		iratchgIO.setFunction(varcom.nextr);
	}

protected void begnT55151200()
	{
		start1200();
	}

protected void start1200()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(wsaaFromdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaCount.set(1);
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isGT(wsaaCount,sv.subfilePage))) {
			nextrT55151210();
		}

		if (isGT(wsaaCount,sv.subfilePage)) {
			scrnparams.subfileMore.set("Y");
		}
		scrnparams.subfileRrn.set(1);
	}

protected void nextrT55151210()
	{
		try {
			start1210();
		}
		catch (GOTOException e){
		}
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1210);
		}
		if (isGTE(wsaaFromdate,itdmIO.getItmfrm())) {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
			wsaaVrtfund.set(itdmIO.getItemitem());
			wsaaUnitItem.set(itdmIO.getItemitem());
			if (isEQ(t5515rec.zfundtyp,"D")) {
				wsaaDescription.set(wsaaUnitItem);
				getDescripT55151300();
				sv.fromdate01.set(varcom.vrcmMaxDate);
				sv.fromdate02.set(varcom.vrcmMaxDate);
				sv.todate.set(varcom.vrcmMaxDate);
				sv.unitVirtualFund.set(wsaaUnitItem);
				sv.shortdesc.set(descIO.getShortdesc());
				if (isEQ(wsspcomn.flag,"C")) {
					sv.fromdate02.set(wsaaFromdate);
					iratcur1140();
				}
			}
		}
		itdmIO.setFunction(varcom.nextr);
	}

protected void getDescripT55151300()
	{
		start1300();
	}

protected void start1300()
	{
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(wsaaDescription);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void addtoSubfile1400()
	{
		/*START*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR510", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"D")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2010();
		checkForErrors2050();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaCount.set(1);
			if (isEQ(wsspcomn.flag,"I")) {
				while ( !(isEQ(iratchgIO.getStatuz(),varcom.endp)
				|| isGT(wsaaCount,sv.subfilePage))) {
					loadIratchgSubfile1165();
				}

			}
			if (isEQ(wsspcomn.flag,"D")) {
				while ( !(isEQ(iratIO.getStatuz(),varcom.endp)
				|| isGT(wsaaCount,sv.subfilePage))) {
					loadTableSubfile1155();
				}

			}
			if (isEQ(wsspcomn.flag,"C")) {
				while ( !(isEQ(itdmIO.getStatuz(),varcom.endp)
				|| isGT(wsaaCount,sv.subfilePage))) {
					nextrT55151210();
				}

			}
			if (isGT(wsaaCount,sv.subfilePage)) {
				scrnparams.subfileMore.set("Y");
			}
			else {
				scrnparams.subfileMore.set("N");
			}
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR510", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}

		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR510", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR510", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.function.set(varcom.sstrt);
		scrnparams.statuz.set(SPACES);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			moreSubfile3100();
		}

	}

protected void moreSubfile3100()
	{
		try {
			nextSubfile3110();
		}
		catch (GOTOException e){
		}
	}

protected void nextSubfile3110()
	{
		processScreen("SR510", sv);
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3190);
		}
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(wsspcomn.flag,"C")) {
			createRecord3200();
		}
		else {
			deleteRecord3300();
		}
		scrnparams.function.set(varcom.srdn);
	}

protected void createRecord3200()
	{
		try {
			start3200();
		}
		catch (GOTOException e){
		}
	}

protected void start3200()
	{
		if (isEQ(sv.interate03,ZERO)
		&& isEQ(sv.interate04,ZERO)) {
			goTo(GotoLabel.exit3200);
		}
		if (isNE(sv.fromdate01,varcom.vrcmMaxDate)) {
			iratcurIO.setUnitVirtualFund(sv.unitVirtualFund);
			iratcurIO.setFromdate(sv.fromdate01);
			iratcurIO.setCompany(wsspcomn.company);
			iratcurIO.setFunction(varcom.readh);
			callIratcur3310();
			iratcurIO.setTodate(sv.fromdate02);
			iratcurIO.setFunction(varcom.rewrt);
			callIratcur3310();
		}
		iratIO.setUnitVirtualFund(sv.unitVirtualFund);
		iratIO.setTodate(varcom.vrcmMaxDate);
		iratIO.setInterate01(sv.interate03);
		iratIO.setInterate02(sv.interate04);
		iratIO.setFromdate(sv.fromdate02);
		iratIO.setCompany(wsspcomn.company);
		iratIO.setFunction(varcom.writr);
		iratIO.setFormat(iratrec);
		SmartFileCode.execute(appVars, iratIO);
		if (isNE(iratIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(iratIO.getParams());
			fatalError600();
		}
	}

protected void deleteRecord3300()
	{
		try {
			start3300();
		}
		catch (GOTOException e){
		}
	}

protected void start3300()
	{
		iratcurIO.setUnitVirtualFund(sv.unitVirtualFund);
		iratcurIO.setFromdate(sv.fromdate01);
		iratcurIO.setCompany(wsspcomn.company);
		iratcurIO.setFunction(varcom.readh);
		callIratcur3310();
		iratcurIO.setFunction(varcom.delet);
		callIratcur3310();
		iratchgIO.setUnitVirtualFund(sv.unitVirtualFund);
		iratchgIO.setCompany(wsspcomn.company);
		iratchgIO.setFromdate(varcom.vrcmMaxDate);
		iratchgIO.setFunction(varcom.begnh);
		iratchgIO.setFormat(iratchgrec);
		SmartFileCode.execute(appVars, iratchgIO);
		if (isNE(iratchgIO.getStatuz(),varcom.oK)
		&& isNE(iratchgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratchgIO.getParams());
			fatalError600();
		}
		if (isEQ(iratchgIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3300);
		}
		if (isNE(iratchgIO.getUnitVirtualFund(),sv.unitVirtualFund)) {
			goTo(GotoLabel.exit3300);
		}
		iratchgIO.setTodate(varcom.vrcmMaxDate);
		iratchgIO.setFunction(varcom.rewrt);
		iratchgIO.setFormat(iratchgrec);
		SmartFileCode.execute(appVars, iratchgIO);
		if (isNE(iratchgIO.getStatuz(),varcom.oK)
		&& isNE(iratchgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratchgIO.getParams());
			fatalError600();
		}
	}

protected void callIratcur3310()
	{
		/*START*/
		iratcurIO.setFormat(iratcurrec);
		SmartFileCode.execute(appVars, iratcurIO);
		if (isNE(iratcurIO.getStatuz(),varcom.oK)
		&& isNE(iratcurIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(iratcurIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
