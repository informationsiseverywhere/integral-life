/*
 * File: HitxpfDAOImpl.java
 * Date: 04 May 2009 21:27:33
 * Author: CSC Limited
 *
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.interestbearing.dataaccess.dao.HitxpfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HitxpfDAOImpl extends BaseDAOImpl<Hitxpf> implements HitxpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HitxpfDAOImpl.class);
	private static final int MEMBER_QUERY_INDEX = 2;
	private static final int BATCHID_QUERY_INDEX = 3;

	@Override
	public List<Hitxpf> searchHitxpfRecord(String tableId, String memName, int batchExtractSize, int batchID) {
		
		StringBuilder selectQuery = new StringBuilder("SELECT * FROM (");
		selectQuery.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
		selectQuery.append("PLNSFX, PRCSEQ, ZINTBFND, ZRECTYP, EFFDATE, TRANNO, ");
		selectQuery.append("FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY ASC, CHDRNUM ASC)-1)/?) BATCHNUM FROM ");	
		selectQuery.append(tableId);
		selectQuery.append("   WHERE MEMBER_NAME = ? ");
		selectQuery.append(" ) MAIN WHERE batchnum = ? ");
		
		PreparedStatement psHitxpfSelect = getPrepareStatement(selectQuery.toString());
		ResultSet sqlhitxpfrs = null;
		List<Hitxpf> hitxpfList = new ArrayList<Hitxpf>();
		try {
			psHitxpfSelect.setInt(1, batchExtractSize);
			psHitxpfSelect.setString(MEMBER_QUERY_INDEX, memName);
			psHitxpfSelect.setInt(BATCHID_QUERY_INDEX, batchID);

			sqlhitxpfrs = executeQuery(psHitxpfSelect);
			while (sqlhitxpfrs.next()) {
				Hitxpf hitxpf = new Hitxpf();
				hitxpf.setUniqueNumber(sqlhitxpfrs.getLong("UNIQUE_NUMBER"));
				hitxpf.setChdrcoy(sqlhitxpfrs.getString("CHDRCOY"));
				hitxpf.setChdrnum(sqlhitxpfrs.getString("CHDRNUM"));
				hitxpf.setLife(sqlhitxpfrs.getString("LIFE"));
				hitxpf.setCoverage(sqlhitxpfrs.getString("COVERAGE"));
				hitxpf.setRider(sqlhitxpfrs.getString("RIDER"));
				hitxpf.setPlanSuffix(sqlhitxpfrs.getInt("PLNSFX"));
				hitxpf.setProcSeqNo(sqlhitxpfrs.getInt("PRCSEQ"));
				hitxpf.setZintbfnd(sqlhitxpfrs.getString("ZINTBFND"));
				hitxpf.setZrectyp(sqlhitxpfrs.getString("ZRECTYP"));
				hitxpf.setEffdate(sqlhitxpfrs.getInt("EFFDATE"));
				hitxpf.setTranno(sqlhitxpfrs.getInt("TRANNO"));
				hitxpfList.add(hitxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchHidxRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitxpfSelect, sqlhitxpfrs);
		}
		return hitxpfList;
	}

}
