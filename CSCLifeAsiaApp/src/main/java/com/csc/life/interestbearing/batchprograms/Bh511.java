/*
 * File: Bh511.java
 * Date: 29 August 2009 21:27:20
 * Author: Quipoz Limited
 * 
 * Class transformed from BH511.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.interestbearing.dataaccess.HidxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.procedures.Contot;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is a Transaction Splitter Program to be run before the
* Interest Bearing Interest Allocation process (BH512) in
* Unit Deal.
*
* SQL will be used to access the HITD physical file. This
* program will extract HITD records that meet the following
* criteria:
*
*     chdrcoy     = sign-on company
*     chdrnum     within contract number range entered
*     validflag   = '1'
*     znxtintdte <= batch effective date
*
* Notes on Splitter Programs.
*
* Splitter programs should be simple. They should only deal
* with a single file. If there is insufficient data on the
* primary file to completely identify a transaction then
* further editing should be done in the subsequent process.
* The objective of a splitter is to rapidily isolate potential
* transactions, not process the transaction. The primary concern
* for splitter program design is elasped time speed and this is
* achieved by minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first
* two characters of the 4th system parameter and 9999 is the
* schedule run number. Each member added to the temporary file
* will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* It is important that the process which runs the splitter
* program has the same 'number of subsequent threads' as the
* following process has defined in 'number of threads this
* process.'
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
***********************************************************************
* </pre>
*/
public class Bh511 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhitdCursorrs = null;
	private java.sql.PreparedStatement sqlhitdCursorps = null;
	private java.sql.Connection sqlhitdCursorconn = null;
	private DiskFileDAM hidx01 = new DiskFileDAM("HIDX01");
	private DiskFileDAM hidx02 = new DiskFileDAM("HIDX02");
	private DiskFileDAM hidx03 = new DiskFileDAM("HIDX03");
	private DiskFileDAM hidx04 = new DiskFileDAM("HIDX04");
	private DiskFileDAM hidx05 = new DiskFileDAM("HIDX05");
	private DiskFileDAM hidx06 = new DiskFileDAM("HIDX06");
	private DiskFileDAM hidx07 = new DiskFileDAM("HIDX07");
	private DiskFileDAM hidx08 = new DiskFileDAM("HIDX08");
	private DiskFileDAM hidx09 = new DiskFileDAM("HIDX09");
	private DiskFileDAM hidx10 = new DiskFileDAM("HIDX10");
	private DiskFileDAM hidx11 = new DiskFileDAM("HIDX11");
	private DiskFileDAM hidx12 = new DiskFileDAM("HIDX12");
	private DiskFileDAM hidx13 = new DiskFileDAM("HIDX13");
	private DiskFileDAM hidx14 = new DiskFileDAM("HIDX14");
	private DiskFileDAM hidx15 = new DiskFileDAM("HIDX15");
	private DiskFileDAM hidx16 = new DiskFileDAM("HIDX16");
	private DiskFileDAM hidx17 = new DiskFileDAM("HIDX17");
	private DiskFileDAM hidx18 = new DiskFileDAM("HIDX18");
	private DiskFileDAM hidx19 = new DiskFileDAM("HIDX19");
	private DiskFileDAM hidx20 = new DiskFileDAM("HIDX20");
	private HidxpfTableDAM hidxpfData = new HidxpfTableDAM();
		/*    Change the record length to that of the temporary file.
		    This can be found by doing a DSPFD of the file being
		    duplicated by the CRTTMPF process.*/
	private FixedLengthStringData hidx01Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx02Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx03Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx04Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx05Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx06Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx07Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx08Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx09Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx10Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx11Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx12Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx13Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx14Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx15Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx16Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx17Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx18Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx19Rec = new FixedLengthStringData(46);
	private FixedLengthStringData hidx20Rec = new FixedLengthStringData(46);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH511");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

		/*    HIDX member parameters*/
	private FixedLengthStringData wsaaHidxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHidxFn, 0, FILLER).init("HIDX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHidxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHidxFn, 6).setUnsigned();
		/*    Host variables*/
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaOne = new FixedLengthStringData(1).init("1");

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*    Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*    Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private P6671par p6671par = new P6671par();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();
	private int ct01Value = 0;
	private int ct02Value = 0;

	public Bh511() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	
	}
protected void restart0900()
	{
		/*RESTART*/
		/**    The  restart section is being handled in section 1100.*/
		/**    This is because the code is applicable to every run,*/
		/**    not just restarts.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*    Check that the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Do ovrdbf for each temporary file member. Note that we have*/
		/*    allowed for a maximum of 20.*/
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Open the required number of temporary files, depending*/
		/*    on IZ and determined by the number of threads specified*/
		/*    for the subsequent process.*/
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		ct01Value += bprdIO.getThreadsSubsqntProc().toInt();
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*    Define the cursor.*/


		/*ILB 470 index on 4 fields > L2NEWUNITD batch completion takes longer time. */
		String sqlhitdCursor = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ZINTBFND, ZLSTINTDTE, ZNXTINTDTE, ZLSTFNDVAL, EFFDATE" +
	   " FROM   " + getAppVars().getTableNameOverriden("HITDEXT") + " " +
	   " WHERE CHDRCOY = ?" +
	   " AND CHDRNUM >= ?" +
 	   " AND CHDRNUM <= ?" +
       //  "AND CHDRNUM BETWEEN ? AND "+ " ? "+
	   " AND ZNXTINTDTE <= ?" +
	   " AND VALIDFLAG = ?"+

	   " ORDER BY CHDRCOY, CHDRNUM, ZINTBFND, ZNXTINTDTE";
		sqlerrorflag = false;
		try {

			/*ILB 470 index on 4 fields > L2NEWUNITD batch completion takes longer time. */
			sqlhitdCursorconn = getAppVars().getDBConnectionForTable();
			sqlhitdCursorps = getAppVars().prepareStatementEmbeded(sqlhitdCursorconn, sqlhitdCursor, "HITDEXT");
			getAppVars().setDBString(sqlhitdCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlhitdCursorps, 2, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlhitdCursorps, 3, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqlhitdCursorps, 4, wsaaEffdate);
			getAppVars().setDBString(sqlhitdCursorps, 5, wsaaOne);
			sqlhitdCursorrs = getAppVars().executeQuery(sqlhitdCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as*/
		/*    the array is indexed).*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
	}

protected void openThreadMember1100()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHidxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HIDX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHidxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 50)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz,1)) {
			hidx01.openOutput();
		}
		if (isEQ(iz,2)) {
			hidx02.openOutput();
		}
		if (isEQ(iz,3)) {
			hidx03.openOutput();
		}
		if (isEQ(iz,4)) {
			hidx04.openOutput();
		}
		if (isEQ(iz,5)) {
			hidx05.openOutput();
		}
		if (isEQ(iz,6)) {
			hidx06.openOutput();
		}
		if (isEQ(iz,7)) {
			hidx07.openOutput();
		}
		if (isEQ(iz,8)) {
			hidx08.openOutput();
		}
		if (isEQ(iz,9)) {
			hidx09.openOutput();
		}
		if (isEQ(iz,10)) {
			hidx10.openOutput();
		}
		if (isEQ(iz,11)) {
			hidx11.openOutput();
		}
		if (isEQ(iz,12)) {
			hidx12.openOutput();
		}
		if (isEQ(iz,13)) {
			hidx13.openOutput();
		}
		if (isEQ(iz,14)) {
			hidx14.openOutput();
		}
		if (isEQ(iz,15)) {
			hidx15.openOutput();
		}
		if (isEQ(iz,16)) {
			hidx16.openOutput();
		}
		if (isEQ(iz,17)) {
			hidx17.openOutput();
		}
		if (isEQ(iz,18)) {
			hidx18.openOutput();
		}
		if (isEQ(iz,19)) {
			hidx19.openOutput();
		}
		if (isEQ(iz,20)) {
			hidx20.openOutput();
		}
	}


protected void initialiseArray1500()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaZintbfnd[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaZlstintdte[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaZnxtintdte[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaZeffdate[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaZlstfndval[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{

		/*    Now a block of records is fetched into the array.*/
		/*    Also on the first entry into the program we must set up the*/
		/*    WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (int hitdCursorLoopIndex = 1; isLTE(hitdCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlhitdCursorrs); hitdCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlhitdCursorrs, 1, wsaaFetchArrayInner.wsaaChdrcoy[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 2, wsaaFetchArrayInner.wsaaChdrnum[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 3, wsaaFetchArrayInner.wsaaLife[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 4, wsaaFetchArrayInner.wsaaCoverage[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 5, wsaaFetchArrayInner.wsaaRider[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 6, wsaaFetchArrayInner.wsaaPlnsfx[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 7, wsaaFetchArrayInner.wsaaZintbfnd[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 8, wsaaFetchArrayInner.wsaaZlstintdte[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 9, wsaaFetchArrayInner.wsaaZnxtintdte[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 10, wsaaFetchArrayInner.wsaaZlstfndval[hitdCursorLoopIndex]);
				getAppVars().getDBObject(sqlhitdCursorrs, 11, wsaaFetchArrayInner.wsaaZeffdate[hitdCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*    If either of the above cases occur, then an SQLCODE = +100*/
		/*    is returned.*/
		/*    The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		} else if (isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*    Re-initialise the block for next fetch and point to*/
		/*    the first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*    If the CHDRNUM being processed is not equal to the previous*/
		/*    one we should move to the next output file member.*/
		/*    The condition is here to allow for checking the last CHDRNUM*/
		/*    of the old block with the first of the new and to move to*/
		/*    the next HIDX member to write to if they have changed.*/
		if (isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*    Load from storage all HIDX data for the same contract until*/
		/*    the CHDRNUM on HIDX has changed or until the end of an*/
		/*    incomplete block is reached.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		hidxpfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
		hidxpfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
		hidxpfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
		hidxpfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
		hidxpfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
		hidxpfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
		hidxpfData.zintbfnd.set(wsaaFetchArrayInner.wsaaZintbfnd[wsaaInd.toInt()]);
		hidxpfData.zlstintdte.set(wsaaFetchArrayInner.wsaaZlstintdte[wsaaInd.toInt()]);
		hidxpfData.znxtintdte.set(wsaaFetchArrayInner.wsaaZnxtintdte[wsaaInd.toInt()]);
		hidxpfData.zlstfndval.set(wsaaFetchArrayInner.wsaaZlstfndval[wsaaInd.toInt()]);
		hidxpfData.effdate.set(wsaaFetchArrayInner.wsaaZeffdate[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy,1)) {
			hidx01.write(hidxpfData);
		}
		if (isEQ(iy,2)) {
			hidx02.write(hidxpfData);
		}
		if (isEQ(iy,3)) {
			hidx03.write(hidxpfData);
		}
		if (isEQ(iy,4)) {
			hidx04.write(hidxpfData);
		}
		if (isEQ(iy,5)) {
			hidx05.write(hidxpfData);
		}
		if (isEQ(iy,6)) {
			hidx06.write(hidxpfData);
		}
		if (isEQ(iy,7)) {
			hidx07.write(hidxpfData);
		}
		if (isEQ(iy,8)) {
			hidx08.write(hidxpfData);
		}
		if (isEQ(iy,9)) {
			hidx09.write(hidxpfData);
		}
		if (isEQ(iy,10)) {
			hidx10.write(hidxpfData);
		}
		if (isEQ(iy,11)) {
			hidx11.write(hidxpfData);
		}
		if (isEQ(iy,12)) {
			hidx12.write(hidxpfData);
		}
		if (isEQ(iy,13)) {
			hidx13.write(hidxpfData);
		}
		if (isEQ(iy,14)) {
			hidx14.write(hidxpfData);
		}
		if (isEQ(iy,15)) {
			hidx15.write(hidxpfData);
		}
		if (isEQ(iy,16)) {
			hidx16.write(hidxpfData);
		}
		if (isEQ(iy,17)) {
			hidx17.write(hidxpfData);
		}
		if (isEQ(iy,18)) {
			hidx18.write(hidxpfData);
		}
		if (isEQ(iy,19)) {
			hidx19.write(hidxpfData);
		}
		if (isEQ(iy,20)) {
			hidx20.write(hidxpfData);
		}
		/*    Log the number of extracted records.*/
		ct02Value++;
		/*    Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*    Check for an incomplete block retrieved.*/
		//ILIFE-1337 - bpham7
		if (isLTE(wsaaInd, wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}


protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callProgram(Contot.class, contotrec.contotRec);		
		ct01Value = 0;
	
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct02Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*    Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlhitdCursorconn, sqlhitdCursorps, sqlhitdCursorrs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void close4100()
	{
		if (isEQ(iz,1)) {
			hidx01.close();
		}
		if (isEQ(iz,2)) {
			hidx02.close();
		}
		if (isEQ(iz,3)) {
			hidx03.close();
		}
		if (isEQ(iz,4)) {
			hidx04.close();
		}
		if (isEQ(iz,5)) {
			hidx05.close();
		}
		if (isEQ(iz,6)) {
			hidx06.close();
		}
		if (isEQ(iz,7)) {
			hidx07.close();
		}
		if (isEQ(iz,8)) {
			hidx08.close();
		}
		if (isEQ(iz,9)) {
			hidx09.close();
		}
		if (isEQ(iz,10)) {
			hidx10.close();
		}
		if (isEQ(iz,11)) {
			hidx11.close();
		}
		if (isEQ(iz,12)) {
			hidx12.close();
		}
		if (isEQ(iz,13)) {
			hidx13.close();
		}
		if (isEQ(iz,14)) {
			hidx14.close();
		}
		if (isEQ(iz,15)) {
			hidx15.close();
		}
		if (isEQ(iz,16)) {
			hidx16.close();
		}
		if (isEQ(iz,17)) {
			hidx17.close();
		}
		if (isEQ(iz,18)) {
			hidx18.close();
		}
		if (isEQ(iz,19)) {
			hidx19.close();
		}
		if (isEQ(iz,20)) {
			hidx20.close();
		}
	}


protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaHitdData = FLSInittedArray (1000, 46);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaHitdData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaHitdData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaHitdData, 9);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaHitdData, 11);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaHitdData, 13);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaHitdData, 15);
	private FixedLengthStringData[] wsaaZintbfnd = FLSDArrayPartOfArrayStructure(4, wsaaHitdData, 18);
	private PackedDecimalData[] wsaaZlstintdte = PDArrayPartOfArrayStructure(8, 0, wsaaHitdData, 22);
	private PackedDecimalData[] wsaaZnxtintdte = PDArrayPartOfArrayStructure(8, 0, wsaaHitdData, 27);
	private PackedDecimalData[] wsaaZlstfndval = PDArrayPartOfArrayStructure(17, 2, wsaaHitdData, 32);
	private PackedDecimalData[] wsaaZeffdate = PDArrayPartOfArrayStructure(8, 0, wsaaHitdData, 41);
}
}
