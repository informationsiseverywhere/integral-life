package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:38
 * Description:
 * Copybook name: HITRCFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrcfikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrcfiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrcfiKey = new FixedLengthStringData(64).isAPartOf(hitrcfiFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrcfiChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrcfiKey, 0);
  	public FixedLengthStringData hitrcfiChdrnum = new FixedLengthStringData(8).isAPartOf(hitrcfiKey, 1);
  	public FixedLengthStringData hitrcfiLife = new FixedLengthStringData(2).isAPartOf(hitrcfiKey, 9);
  	public FixedLengthStringData hitrcfiCoverage = new FixedLengthStringData(2).isAPartOf(hitrcfiKey, 11);
  	public FixedLengthStringData hitrcfiRider = new FixedLengthStringData(2).isAPartOf(hitrcfiKey, 13);
  	public PackedDecimalData hitrcfiPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitrcfiKey, 15);
  	public PackedDecimalData hitrcfiTranno = new PackedDecimalData(5, 0).isAPartOf(hitrcfiKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(hitrcfiKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrcfiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrcfiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}