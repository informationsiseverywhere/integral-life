package com.csc.life.interestbearing.dataaccess.dao;

import java.util.List;

import com.csc.life.interestbearing.dataaccess.model.Hidxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HidxpfDAO extends BaseDAO<Hidxpf> {
    public List<Hidxpf> searchHidxRecord(String tableId, String memName, int batchExtractSize, int batchID);
}