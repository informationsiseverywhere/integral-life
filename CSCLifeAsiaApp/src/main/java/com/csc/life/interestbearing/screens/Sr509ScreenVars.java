package com.csc.life.interestbearing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR509
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sr509ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(61);
	public FixedLengthStringData dataFields = new FixedLengthStringData(13).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData fromdate = DD.fromdate.copyToZonedDecimal().isAPartOf(dataFields,1);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 13);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData fromdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 25);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] fromdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData fromdateDisp = new FixedLengthStringData(10);

	public LongData Sr509screenWritten = new LongData(0);
	public LongData Sr509protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr509ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vrtfndOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fromdateOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {unitVirtualFund, fromdate, action};
		screenOutFields = new BaseData[][] {vrtfndOut, fromdateOut, actionOut};
		screenErrFields = new BaseData[] {vrtfndErr, fromdateErr, actionErr};
		screenDateFields = new BaseData[] {fromdate};
		screenDateErrFields = new BaseData[] {fromdateErr};
		screenDateDispFields = new BaseData[] {fromdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sr509screen.class;
		protectRecord = Sr509protect.class;
	}

}
