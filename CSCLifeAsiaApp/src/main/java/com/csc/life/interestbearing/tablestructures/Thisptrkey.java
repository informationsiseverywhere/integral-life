package com.csc.life.interestbearing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:45
 * Description:
 * Copybook name: THISPTRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Thisptrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData thisptrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData thisptrKey = new FixedLengthStringData(64).isAPartOf(thisptrFileKey, 0, REDEFINE);
  	public FixedLengthStringData thisptrChdrcoy = new FixedLengthStringData(1).isAPartOf(thisptrKey, 0);
  	public FixedLengthStringData thisptrChdrnum = new FixedLengthStringData(8).isAPartOf(thisptrKey, 1);
  	public FixedLengthStringData thisptrValidflag = new FixedLengthStringData(1).isAPartOf(thisptrKey, 9);
  	public PackedDecimalData thisptrTranno = new PackedDecimalData(5, 0).isAPartOf(thisptrKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(thisptrKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(thisptrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		thisptrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}