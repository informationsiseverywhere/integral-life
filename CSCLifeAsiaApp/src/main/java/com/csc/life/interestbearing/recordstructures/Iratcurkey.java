package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:02
 * Description:
 * Copybook name: IRATCURKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Iratcurkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData iratcurFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData iratcurKey = new FixedLengthStringData(64).isAPartOf(iratcurFileKey, 0, REDEFINE);
  	public FixedLengthStringData iratcurCompany = new FixedLengthStringData(1).isAPartOf(iratcurKey, 0);
  	public FixedLengthStringData iratcurUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(iratcurKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(59).isAPartOf(iratcurKey, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(iratcurFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		iratcurFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}