package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:37
 * Description:
 * Copybook name: HISPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hispkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hispFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hispKey = new FixedLengthStringData(64).isAPartOf(hispFileKey, 0, REDEFINE);
  	public FixedLengthStringData hispChdrcoy = new FixedLengthStringData(1).isAPartOf(hispKey, 0);
  	public FixedLengthStringData hispChdrnum = new FixedLengthStringData(8).isAPartOf(hispKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(hispKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hispFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hispFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}