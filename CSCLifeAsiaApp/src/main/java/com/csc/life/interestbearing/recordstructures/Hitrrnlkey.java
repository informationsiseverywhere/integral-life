package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:40
 * Description:
 * Copybook name: HITRRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrrnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrrnlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrrnlKey = new FixedLengthStringData(64).isAPartOf(hitrrnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrrnlKey, 0);
  	public FixedLengthStringData hitrrnlChdrnum = new FixedLengthStringData(8).isAPartOf(hitrrnlKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(hitrrnlKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrrnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrrnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}