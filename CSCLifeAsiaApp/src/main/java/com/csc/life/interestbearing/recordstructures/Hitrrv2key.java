package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:40
 * Description:
 * Copybook name: HITRRV2KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrrv2key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrrv2FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrrv2Key = new FixedLengthStringData(64).isAPartOf(hitrrv2FileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrrv2Chdrcoy = new FixedLengthStringData(1).isAPartOf(hitrrv2Key, 0);
  	public FixedLengthStringData hitrrv2Chdrnum = new FixedLengthStringData(8).isAPartOf(hitrrv2Key, 1);
  	public FixedLengthStringData hitrrv2Life = new FixedLengthStringData(2).isAPartOf(hitrrv2Key, 9);
  	public FixedLengthStringData hitrrv2Coverage = new FixedLengthStringData(2).isAPartOf(hitrrv2Key, 11);
  	public FixedLengthStringData hitrrv2Rider = new FixedLengthStringData(2).isAPartOf(hitrrv2Key, 13);
  	public PackedDecimalData hitrrv2PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitrrv2Key, 15);
  	public PackedDecimalData hitrrv2Zlstintdte = new PackedDecimalData(8, 0).isAPartOf(hitrrv2Key, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(hitrrv2Key, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrrv2FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrrv2FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}