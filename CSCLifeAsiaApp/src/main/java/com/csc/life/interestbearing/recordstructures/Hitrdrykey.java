package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class Hitrdrykey extends ExternalData {
	//ILIFE-1953 starts by slakkala
/*
	public FixedLengthStringData wskyHitrdryFileKey = new FixedLengthStringData(256);
	public FixedLengthStringData wskyHitrdryKey = new FixedLengthStringData(256).isAPartOf(wskyHitrdryFileKey, 0, REDEFINE);

	public FixedLengthStringData wskyHitrdryChdrcoy = new FixedLengthStringData(1).isAPartOf(wskyHitrdryKey, 0);


	private FixedLengthStringData wskyHitrdryChdrnum = new FixedLengthStringData(8).isAPartOf(wskyHitrdryKey, 1);
	private PackedDecimalData wskyHitrdryEffdate  = new PackedDecimalData(8, 0).isAPartOf(wskyHitrdryKey, 9);
	private FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(wskyHitrdryKey, 13, FILLER); */
	
	
	public FixedLengthStringData wskyHitrdryFileKey = new FixedLengthStringData(256);
	public FixedLengthStringData wskyHitrdryKey = new FixedLengthStringData(256).isAPartOf(wskyHitrdryFileKey, 0, REDEFINE);




	public FixedLengthStringData wskyHitrdryChdrcoy = new FixedLengthStringData(1).isAPartOf(wskyHitrdryKey, 0);

	public FixedLengthStringData wskyHitrdryChdrnum = new FixedLengthStringData(8).isAPartOf(wskyHitrdryKey, 1);
	public PackedDecimalData wskyHitrdryEffdate  = new PackedDecimalData(8, 0).isAPartOf(wskyHitrdryKey, 9);
	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(wskyHitrdryKey, 13, FILLER);
	//ILIFE-1953 ends
	public void initialize() {
		COBOLFunctions.initialize(wskyHitrdryFileKey);
	}	
		
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			wskyHitrdryFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}

