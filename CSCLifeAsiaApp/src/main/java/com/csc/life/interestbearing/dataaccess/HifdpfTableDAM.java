package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HifdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:29
 * Class transformed from HIFDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HifdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 60;
	public FixedLengthStringData hifdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hifdpfRecord = hifdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hifdrec);
	public FixedLengthStringData zintbfnd = DD.zintbfnd.copy().isAPartOf(hifdrec);
	public PackedDecimalData ztotfndval = DD.ztotfndval.copy().isAPartOf(hifdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hifdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hifdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hifdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HifdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HifdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HifdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HifdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HifdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HifdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HifdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HIFDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"ZINTBFND, " +
							"ZTOTFNDVAL, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     zintbfnd,
                                     ztotfndval,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		zintbfnd.clear();
  		ztotfndval.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHifdrec() {
  		return hifdrec;
	}

	public FixedLengthStringData getHifdpfRecord() {
  		return hifdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHifdrec(what);
	}

	public void setHifdrec(Object what) {
  		this.hifdrec.set(what);
	}

	public void setHifdpfRecord(Object what) {
  		this.hifdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hifdrec.getLength());
		result.set(hifdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}