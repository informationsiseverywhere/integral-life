package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:36
 * Description:
 * Copybook name: HIFDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hifdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hifdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hifdKey = new FixedLengthStringData(64).isAPartOf(hifdFileKey, 0, REDEFINE);
  	public FixedLengthStringData hifdChdrcoy = new FixedLengthStringData(1).isAPartOf(hifdKey, 0);
  	public FixedLengthStringData hifdZintbfnd = new FixedLengthStringData(4).isAPartOf(hifdKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(59).isAPartOf(hifdKey, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hifdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hifdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}