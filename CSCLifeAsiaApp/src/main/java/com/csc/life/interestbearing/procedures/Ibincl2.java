/*
 * File: Ibincl2.java
 * Date: 29 August 2009 22:55:46
 * Author: Quipoz Limited
 *
 * Class transformed from IBINCL2.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.interestbearing.dataaccess.IratchgTableDAM;
import com.csc.life.interestbearing.recordstructures.Ibincalrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This subroutine is cloned from IBINCAL.
*
* Interest Bearing Interest Calculation Routine.
* This new subroutine calculates interest for the Interest
* Bearing component using interest rate from IRAT file.
*
*****************************************************************
* </pre>
*/
public class Ibincl2 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("IBINCL2");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIntRate = new PackedDecimalData(10, 7);
	private PackedDecimalData wsaaDayRate = new PackedDecimalData(10, 7);
		/* FORMATS */
	private String iratchgrec = "IRATCHGREC";
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ibincalrec ibincalrec = new Ibincalrec();
		/*Interest Rate File (Fromdate - Desc)*/
	private IratchgTableDAM iratchgIO = new IratchgTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit190,
		exit290
	}

	public Ibincl2() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ibincalrec.ibincalRec = convertAndSetParam(ibincalrec.ibincalRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		ibincalrec.statuz.set(varcom.oK);
		getIntRate100();
		calcInterest200();
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void getIntRate100()
	{
		try {
			begn110();
		}
		catch (GOTOException e){
		}
	}

protected void begn110()
	{
		iratchgIO.setDataKey(SPACES);
		iratchgIO.setCompany(ibincalrec.company);
		iratchgIO.setUnitVirtualFund(ibincalrec.fund);
		iratchgIO.setFromdate(ibincalrec.crrcd);
		iratchgIO.setFormat(iratchgrec);
		iratchgIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		iratchgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		iratchgIO.setFitKeysSearch("COMPANY", "VRTFND");
		SmartFileCode.execute(appVars, iratchgIO);
		if (isNE(iratchgIO.getStatuz(),varcom.oK)
		&& isNE(iratchgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(iratchgIO.getParams());
			syserrrec.statuz.set(iratchgIO.getStatuz());
			dbError580();
		}
		if (isNE(iratchgIO.getCompany(),ibincalrec.company)
		|| isNE(iratchgIO.getUnitVirtualFund(),ibincalrec.fund)
		|| isEQ(iratchgIO.getStatuz(),varcom.endp)) {
			ibincalrec.intRate.set(ZERO);
			ibincalrec.intEffdate.set(ZERO);
			ibincalrec.intAmount.set(ZERO);
			goTo(GotoLabel.exit190);
		}
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(ibincalrec.crrcd);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		datcon2rec.statuz.set(varcom.oK);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserr570();
		}
		if (isGT(datcon2rec.intDate2,ibincalrec.lastIntApp)) {
			ibincalrec.intRate.set(iratchgIO.getInterate01());
		}
		else {
			ibincalrec.intRate.set(iratchgIO.getInterate02());
		}
		ibincalrec.intEffdate.set(iratchgIO.getFromdate());
	}

protected void calcInterest200()
	{
		try {
			calc210();
		}
		catch (GOTOException e){
		}
	}

protected void calc210()
	{
		if (isEQ(ibincalrec.intRate,ZERO)) {
			ibincalrec.intAmount.set(ZERO);
			goTo(GotoLabel.exit290);
		}
		compute(wsaaDayRate, 8).setRounded(power((add(1,div(ibincalrec.intRate,100))),(div(1,365))));
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(ibincalrec.lastIntApp);
		datcon3rec.intDate2.set(ibincalrec.nextIntDue);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			syserr570();
		}
		compute(wsaaIntRate, 8).setRounded(sub((power(wsaaDayRate,datcon3rec.freqFactor)),1));
		compute(ibincalrec.intAmount, 8).setRounded(mult(ibincalrec.capAmount,wsaaIntRate));
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		ibincalrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		ibincalrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}
}
