package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HisppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:29
 * Class transformed from HISPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HisppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 75;
	public FixedLengthStringData hisprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hisppfRecord = hisprec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hisprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hisprec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hisprec);
	public FixedLengthStringData hintsupind = DD.hintsupind.copy().isAPartOf(hisprec);
	public PackedDecimalData hintsupfrm = DD.hintsupfrm.copy().isAPartOf(hisprec);
	public PackedDecimalData hintsupto = DD.hintsupto.copy().isAPartOf(hisprec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(hisprec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hisprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hisprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hisprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hisprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HisppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HisppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HisppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HisppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HisppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HisppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HisppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HISPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"EFFDATE, " +
							"HINTSUPIND, " +
							"HINTSUPFRM, " +
							"HINTSUPTO, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     effdate,
                                     hintsupind,
                                     hintsupfrm,
                                     hintsupto,
                                     validflag,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		effdate.clear();
  		hintsupind.clear();
  		hintsupfrm.clear();
  		hintsupto.clear();
  		validflag.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHisprec() {
  		return hisprec;
	}

	public FixedLengthStringData getHisppfRecord() {
  		return hisppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHisprec(what);
	}

	public void setHisprec(Object what) {
  		this.hisprec.set(what);
	}

	public void setHisppfRecord(Object what) {
  		this.hisppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hisprec.getLength());
		result.set(hisprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}