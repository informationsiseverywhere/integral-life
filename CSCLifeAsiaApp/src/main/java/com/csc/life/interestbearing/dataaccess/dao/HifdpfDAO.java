/******************************************************************************
 * File Name 		: HifdpfDAO.java
 * Author			: CSC
 * Creation Date	: 22 May 2017
 * Project			: Integral Life
 * Description		: The DAO Interface for HIFDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			| *											| *				  |
 * ***************************************************************************/
package com.csc.life.interestbearing.dataaccess.dao;

import java.util.List;

import com.csc.life.interestbearing.dataaccess.model.Hifdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface HifdpfDAO extends BaseDAO<Hifdpf> {

	public Hifdpf searchHifdpfRecord(Hifdpf hifdpf) throws SQLRuntimeException;

	public void bulkInsertHifd(List<Hifdpf> hifdpfList) throws SQLRuntimeException;
	
	public void bulkUpdateHifdp(List<Hifdpf> hifdpfList) throws SQLRuntimeException;

}
