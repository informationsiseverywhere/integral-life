package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:38
 * Description:
 * Copybook name: HITRALOKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitralokey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitraloFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitraloKey = new FixedLengthStringData(64).isAPartOf(hitraloFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitraloChdrcoy = new FixedLengthStringData(1).isAPartOf(hitraloKey, 0);
  	public FixedLengthStringData hitraloChdrnum = new FixedLengthStringData(8).isAPartOf(hitraloKey, 1);
  	public FixedLengthStringData hitraloLife = new FixedLengthStringData(2).isAPartOf(hitraloKey, 9);
  	public FixedLengthStringData hitraloCoverage = new FixedLengthStringData(2).isAPartOf(hitraloKey, 11);
  	public FixedLengthStringData hitraloRider = new FixedLengthStringData(2).isAPartOf(hitraloKey, 13);
  	public PackedDecimalData hitraloPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitraloKey, 15);
  	public FixedLengthStringData hitraloZintbfnd = new FixedLengthStringData(4).isAPartOf(hitraloKey, 18);
  	public FixedLengthStringData hitraloZrectyp = new FixedLengthStringData(1).isAPartOf(hitraloKey, 22);
  	public PackedDecimalData hitraloProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(hitraloKey, 23);
  	public PackedDecimalData hitraloTranno = new PackedDecimalData(5, 0).isAPartOf(hitraloKey, 25);
  	public PackedDecimalData hitraloEffdate = new PackedDecimalData(8, 0).isAPartOf(hitraloKey, 28);
  	public FixedLengthStringData filler = new FixedLengthStringData(31).isAPartOf(hitraloKey, 33, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitraloFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitraloFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}