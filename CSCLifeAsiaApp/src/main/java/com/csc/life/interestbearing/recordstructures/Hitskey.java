package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:41
 * Description:
 * Copybook name: HITSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitsKey = new FixedLengthStringData(64).isAPartOf(hitsFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitsChdrcoy = new FixedLengthStringData(1).isAPartOf(hitsKey, 0);
  	public FixedLengthStringData hitsChdrnum = new FixedLengthStringData(8).isAPartOf(hitsKey, 1);
  	public FixedLengthStringData hitsLife = new FixedLengthStringData(2).isAPartOf(hitsKey, 9);
  	public FixedLengthStringData hitsCoverage = new FixedLengthStringData(2).isAPartOf(hitsKey, 11);
  	public FixedLengthStringData hitsRider = new FixedLengthStringData(2).isAPartOf(hitsKey, 13);
  	public PackedDecimalData hitsPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitsKey, 15);
  	public FixedLengthStringData hitsZintbfnd = new FixedLengthStringData(4).isAPartOf(hitsKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(hitsKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}