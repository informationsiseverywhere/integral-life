package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IjnlpfTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:45
 * Class transformed from IJNLPF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IjnlpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 272;
	public FixedLengthStringData ijnlrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ijnlpfRecord = ijnlrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ijnlrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ijnlrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData zintbfnd = DD.zintbfnd.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData zrectyp = DD.zrectyp.copy().isAPartOf(ijnlrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(ijnlrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(ijnlrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(ijnlrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(ijnlrec);
	public PackedDecimalData inciNum = DD.incinum.copy().isAPartOf(ijnlrec);
	public PackedDecimalData inciPerd01 = DD.inciperd.copy().isAPartOf(ijnlrec);
	public PackedDecimalData inciPerd02 = DD.inciperd.copy().isAPartOf(ijnlrec);
	public PackedDecimalData inciprm01 = DD.inciprm.copy().isAPartOf(ijnlrec);
	public PackedDecimalData inciprm02 = DD.inciprm.copy().isAPartOf(ijnlrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(ijnlrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(ijnlrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData sacstype = DD.sacstype.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData genlcde = DD.genlcde.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData contractType = DD.contyp.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData triggerModule = DD.triger.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData triggerKey = DD.trigky.copy().isAPartOf(ijnlrec);
	public PackedDecimalData svp = DD.svp.copy().isAPartOf(ijnlrec);
	public PackedDecimalData surrenderPercent = DD.persur.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData switchIndicator = DD.swchind.copy().isAPartOf(ijnlrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ijnlrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ijnlrec);
	public PackedDecimalData zcurprmbal = DD.zcurprmbal.copy().isAPartOf(ijnlrec);//ILIFE-2249

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public IjnlpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for IjnlpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public IjnlpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for IjnlpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public IjnlpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for IjnlpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public IjnlpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("IJNLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"SEQNO, " +
							"ZINTBFND, " +
							"ZRECTYP, " +
							"PRCSEQ, " +
							"TRANNO, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"CRTABLE, " +
							"CNTCURR, " +
							"FDBKIND, " +
							"INCINUM, " +
							"INCIPERD01, " +
							"INCIPERD02, " +
							"INCIPRM01, " +
							"INCIPRM02, " +
							"CNTAMNT, " +
							"FNDCURR, " +
							"FUNDAMNT, " +
							"FUNDRATE, " +
							"SACSCODE, " +
							"SACSTYPE, " +
							"GENLCDE, " +
							"CONTYP, " +
							"TRIGER, " +
							"TRIGKY, " +
							"SVP, " +
							"PERSUR, " +
							"SWCHIND, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"ZCURPRMBAL," + //for ILIFE-2249
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     seqno,
                                     zintbfnd,
                                     zrectyp,
                                     procSeqNo,
                                     tranno,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     crtable,
                                     cntcurr,
                                     feedbackInd,
                                     inciNum,
                                     inciPerd01,
                                     inciPerd02,
                                     inciprm01,
                                     inciprm02,
                                     contractAmount,
                                     fundCurrency,
                                     fundAmount,
                                     fundRate,
                                     sacscode,
                                     sacstype,
                                     genlcde,
                                     contractType,
                                     triggerModule,
                                     triggerKey,
                                     svp,
                                     surrenderPercent,
                                     switchIndicator,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     zcurprmbal, //for ILIFE-2249
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		seqno.clear();
  		zintbfnd.clear();
  		zrectyp.clear();
  		procSeqNo.clear();
  		tranno.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		crtable.clear();
  		cntcurr.clear();
  		feedbackInd.clear();
  		inciNum.clear();
  		inciPerd01.clear();
  		inciPerd02.clear();
  		inciprm01.clear();
  		inciprm02.clear();
  		contractAmount.clear();
  		fundCurrency.clear();
  		fundAmount.clear();
  		fundRate.clear();
  		sacscode.clear();
  		sacstype.clear();
  		genlcde.clear();
  		contractType.clear();
  		triggerModule.clear();
  		triggerKey.clear();
  		svp.clear();
  		surrenderPercent.clear();
  		switchIndicator.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		zcurprmbal.clear(); //ILIFE-2249
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getIjnlrec() {
  		return ijnlrec;
	}

	public FixedLengthStringData getIjnlpfRecord() {
  		return ijnlpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setIjnlrec(what);
	}

	public void setIjnlrec(Object what) {
  		this.ijnlrec.set(what);
	}

	public void setIjnlpfRecord(Object what) {
  		this.ijnlpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ijnlrec.getLength());
		result.set(ijnlrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}