package com.csc.life.interestbearing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HitspfDAOImpl extends BaseDAOImpl<Hitspf> implements HitspfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(HitspfDAOImpl.class);

	public List<Hitspf> searchHitsRecord(Hitspf hitsPf) {
		StringBuilder sqlHitsSelect1 = new StringBuilder(
				"SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ZINTBFND, ZCURPRMBAL, ZLSTUPDT ");
		sqlHitsSelect1.append("FROM HITS WHERE CHDRCOY= '" + hitsPf.getChdrcoy() + "' AND CHDRNUM = '"	+ hitsPf.getChdrnum() );
		sqlHitsSelect1.append("' AND LIFE= '" + hitsPf.getLife() + "' AND COVERAGE = '" + hitsPf.getCoverage()
				+ "' AND RIDER= '" + hitsPf.getRider() + "' ");
		sqlHitsSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitsSelect = getPrepareStatement(sqlHitsSelect1.toString());
		ResultSet sqlhitspf1rs = null;
		List<Hitspf> hitspfList = new ArrayList<>();
		try {
			sqlhitspf1rs = executeQuery(psHitsSelect);
			while (sqlhitspf1rs.next()) {
				Hitspf hitspf = new Hitspf();
				hitspf.setChdrcoy(sqlhitspf1rs.getString("CHDRCOY"));
				hitspf.setChdrnum(sqlhitspf1rs.getString("CHDRNUM"));
				hitspf.setLife(sqlhitspf1rs.getString("LIFE"));
				hitspf.setCoverage(sqlhitspf1rs.getString("COVERAGE"));
				hitspf.setRider(sqlhitspf1rs.getString("RIDER"));
				hitspf.setPlanSuffix(sqlhitspf1rs.getInt("PLNSFX"));
				hitspf.setZintbfnd(sqlhitspf1rs.getString("ZINTBFND"));
				hitspf.setZcurprmbal(sqlhitspf1rs.getBigDecimal("ZCURPRMBAL"));
				hitspf.setZlstupdt(sqlhitspf1rs.getBigDecimal("ZLSTUPDT"));
				hitspfList.add(hitspf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchHitsRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitsSelect, sqlhitspf1rs);
		}
		return hitspfList;

	}
    
    
	public Map<String, List<Hitspf>> searchHitsRecordByChdrnum(List<String> chdrnumList) {
		StringBuilder sqlHitsSelect1 = new StringBuilder(
				"SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,ZINTBFND,ZCURPRMBAL,ZLSTUPDT,UNIQUE_NUMBER ");
		sqlHitsSelect1.append("FROM HITS WHERE ");
		sqlHitsSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlHitsSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitsSelect = getPrepareStatement(sqlHitsSelect1.toString());
		ResultSet sqlhitspf1rs = null;
		Map<String, List<Hitspf>> resultMap = new HashMap<>();
		try {
			sqlhitspf1rs = executeQuery(psHitsSelect);
			while (sqlhitspf1rs.next()) {
				Hitspf hitspf = new Hitspf();
				hitspf.setChdrcoy(sqlhitspf1rs.getString("CHDRCOY"));
				hitspf.setChdrnum(sqlhitspf1rs.getString("CHDRNUM"));
				hitspf.setLife(sqlhitspf1rs.getString("LIFE"));
				hitspf.setCoverage(sqlhitspf1rs.getString("COVERAGE"));
				hitspf.setRider(sqlhitspf1rs.getString("RIDER"));
				hitspf.setPlanSuffix(sqlhitspf1rs.getInt("PLNSFX"));
				hitspf.setZintbfnd(sqlhitspf1rs.getString("ZINTBFND"));
				hitspf.setZcurprmbal(sqlhitspf1rs.getBigDecimal("ZCURPRMBAL"));
				hitspf.setZlstupdt(sqlhitspf1rs.getBigDecimal("ZLSTUPDT"));
				hitspf.setUniqueNumber(sqlhitspf1rs.getLong("UNIQUE_NUMBER"));

				if (resultMap.containsKey(hitspf.getChdrnum())) {
					resultMap.get(hitspf.getChdrnum()).add(hitspf);
				} else {
					List<Hitspf> hitspfList = new ArrayList<>();
					hitspfList.add(hitspf);
					resultMap.put(hitspf.getChdrnum(), hitspfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchHitsRecordByChdrnum()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitsSelect, sqlhitspf1rs);
		}
		return resultMap;

	}

	@Override
	public Hitspf getHitsRecord(Hitspf hitsPf) {
		StringBuilder sqlHitsSelect = new StringBuilder(
				"SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,ZINTBFND,ZCURPRMBAL,ZLSTUPDT,UNIQUE_NUMBER ");
		sqlHitsSelect.append("FROM HITS WHERE ");
		sqlHitsSelect
			.append("CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND ZINTBFND=? ");
		sqlHitsSelect
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hitspf hitspf = null;
		try {
			ps = getConnection().prepareStatement(sqlHitsSelect.toString());
			ps.setString(1, hitsPf.getChdrcoy());
			ps.setString(2, hitsPf.getChdrnum());
			ps.setString(3, hitsPf.getLife());
			ps.setString(4, hitsPf.getCoverage());
			ps.setString(5, hitsPf.getRider());
			ps.setInt(6, hitsPf.getPlanSuffix());
			ps.setString(7, hitsPf.getZintbfnd());
			rs = ps.executeQuery();
			if(rs.next()){
				hitspf = new Hitspf();
				hitspf.setChdrcoy(rs.getString("CHDRCOY"));
				hitspf.setChdrnum(rs.getString("CHDRNUM"));
				hitspf.setLife(rs.getString("LIFE"));
				hitspf.setCoverage(rs.getString("COVERAGE"));
				hitspf.setRider(rs.getString("RIDER"));
				hitspf.setPlanSuffix(rs.getInt("PLNSFX"));
				hitspf.setZintbfnd(rs.getString("ZINTBFND"));
				hitspf.setZcurprmbal(rs.getBigDecimal("ZCURPRMBAL"));
				hitspf.setZlstupdt(rs.getBigDecimal("ZLSTUPDT"));
				hitspf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
			}
		} catch (SQLException e) {
			LOGGER.error("getHitsRecord() ", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps ,null);
		}
		return hitspf;
	}
	
	public void bulkInsertHit(List<Hitspf> hitspfList){
		
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO HITS(CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ZINTBFND, ZCURPRMBAL, USRPRF, JOBNM, DATIME)");
		sql.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		try {
			for (Hitspf hitspf : hitspfList) {
				ps.setString(1, hitspf.getChdrcoy());
				ps.setString(2, hitspf.getChdrnum());
				ps.setString(3, hitspf.getLife());
				ps.setString(4, hitspf.getCoverage());
				ps.setString(5, hitspf.getRider());
				ps.setInt(6, hitspf.getPlanSuffix());
				ps.setString(7, hitspf.getZintbfnd());
				ps.setBigDecimal(8, hitspf.getZcurprmbal());
				ps.setString(9, getUsrprf());
				ps.setString(10, getJobnm());
				ps.setTimestamp(11, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("bulkInsertHit()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}


	@Override
	public void bulkUpdateHits(List<Hitspf> hitspfList) {
		String sql = "UPDATE HITS SET ZCURPRMBAL=?, ZLSTUPDT=? WHERE UNIQUE_NUMBER=?";
		PreparedStatement psHitspfUpdate = getPrepareStatement(sql);
        try {
       	 for(Hitspf hitspf : hitspfList){
                psHitspfUpdate.setBigDecimal(1, hitspf.getZcurprmbal());
                psHitspfUpdate.setBigDecimal(2, hitspf.getZlstupdt());
                psHitspfUpdate.setLong(3, hitspf.getUniqueNumber());
                psHitspfUpdate.addBatch();
       	 }
       	 psHitspfUpdate.executeBatch();
        } catch (SQLException e) {
            LOGGER.error("bulkUpdateHits()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psHitspfUpdate, null);
        }
	}
	
	public Map<String, List<Hitspf>> searchHitsRecordByChdrcoy(String chdrcoy, List<String> chdrnumList) {
		StringBuilder sqlHitsSelect1 = new StringBuilder(
				"SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,ZINTBFND,ZCURPRMBAL,ZLSTUPDT,UNIQUE_NUMBER ");
		sqlHitsSelect1.append("FROM HITS WHERE CHDRCOY = ? AND ");
		sqlHitsSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlHitsSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitsSelect = getPrepareStatement(sqlHitsSelect1.toString());
		ResultSet sqlhitspf1rs = null;
		Map<String, List<Hitspf>> resultMap = new HashMap<>();
		try {
			psHitsSelect.setString(1, chdrcoy);
			sqlhitspf1rs = executeQuery(psHitsSelect);
			while (sqlhitspf1rs.next()) {
				Hitspf hitspf = new Hitspf();
				hitspf.setChdrcoy(sqlhitspf1rs.getString("CHDRCOY"));
				hitspf.setChdrnum(sqlhitspf1rs.getString("CHDRNUM"));
				hitspf.setLife(sqlhitspf1rs.getString("LIFE"));
				hitspf.setCoverage(sqlhitspf1rs.getString("COVERAGE"));
				hitspf.setRider(sqlhitspf1rs.getString("RIDER"));
				hitspf.setPlanSuffix(sqlhitspf1rs.getInt("PLNSFX"));
				hitspf.setZintbfnd(sqlhitspf1rs.getString("ZINTBFND"));
				hitspf.setZcurprmbal(sqlhitspf1rs.getBigDecimal("ZCURPRMBAL"));
				hitspf.setZlstupdt(sqlhitspf1rs.getBigDecimal("ZLSTUPDT"));
				hitspf.setUniqueNumber(sqlhitspf1rs.getLong("UNIQUE_NUMBER"));

				if (resultMap.containsKey(hitspf.getChdrnum())) {
					resultMap.get(hitspf.getChdrnum()).add(hitspf);
				} else {
					List<Hitspf> hitspfList = new ArrayList<>();
					hitspfList.add(hitspf);
					resultMap.put(hitspf.getChdrnum(), hitspfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchHitsRecordByChdrcoy()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitsSelect, sqlhitspf1rs);
		}
		return resultMap;
	}	
}