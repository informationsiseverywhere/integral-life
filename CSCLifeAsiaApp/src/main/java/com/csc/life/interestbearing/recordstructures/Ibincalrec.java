package com.csc.life.interestbearing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:46
 * Description:
 * Copybook name: IBINCALREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ibincalrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData ibincalRec = new FixedLengthStringData(59);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(ibincalRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(ibincalRec, 4);
  	public PackedDecimalData lastIntApp = new PackedDecimalData(8, 0).isAPartOf(ibincalRec, 5);
  	public PackedDecimalData nextIntDue = new PackedDecimalData(8, 0).isAPartOf(ibincalRec, 10);
  	public PackedDecimalData capAmount = new PackedDecimalData(17, 2).isAPartOf(ibincalRec, 15);
  	public PackedDecimalData intAmount = new PackedDecimalData(17, 4).isAPartOf(ibincalRec, 24);
  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(ibincalRec, 33);
  	public PackedDecimalData intRate = new PackedDecimalData(8, 5).isAPartOf(ibincalRec, 37);
  	public PackedDecimalData intEffdate = new PackedDecimalData(8, 0).isAPartOf(ibincalRec, 42);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(ibincalRec, 47);
  	//ILIFE-7300
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(ibincalRec, 52);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(ibincalRec, 55);


	public void initialize() {
		COBOLFunctions.initialize(ibincalRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ibincalRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}