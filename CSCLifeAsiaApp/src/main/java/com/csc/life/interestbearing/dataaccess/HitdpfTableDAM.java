package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HitdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:30
 * Class transformed from HITDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HitdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 113;
	public FixedLengthStringData hitdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hitdpfRecord = hitdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hitdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hitdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hitdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hitdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hitdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hitdrec);
	public FixedLengthStringData zintbfnd = DD.zintbfnd.copy().isAPartOf(hitdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hitdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hitdrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(hitdrec);
	public PackedDecimalData zlstintdte = DD.zlstintdte.copy().isAPartOf(hitdrec);
	public PackedDecimalData znxtintdte = DD.znxtintdte.copy().isAPartOf(hitdrec);
	public PackedDecimalData zlstfndval = DD.zlstfndval.copy().isAPartOf(hitdrec);
	public PackedDecimalData zlstsmtdt = DD.zlstsmtdt.copy().isAPartOf(hitdrec);
	public PackedDecimalData zlstsmtno = DD.zlstsmtno.copy().isAPartOf(hitdrec);
	public PackedDecimalData zlstsmtbal = DD.zlstsmtbal.copy().isAPartOf(hitdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hitdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hitdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hitdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HitdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HitdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HitdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HitdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HitdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HitdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HITDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"ZINTBFND, " +
							"TRANNO, " +
							"EFFDATE, " +
							"VALIDFLAG, " +
							"ZLSTINTDTE, " +
							"ZNXTINTDTE, " +
							"ZLSTFNDVAL, " +
							"ZLSTSMTDT, " +
							"ZLSTSMTNO, " +
							"ZLSTSMTBAL, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     zintbfnd,
                                     tranno,
                                     effdate,
                                     validflag,
                                     zlstintdte,
                                     znxtintdte,
                                     zlstfndval,
                                     zlstsmtdt,
                                     zlstsmtno,
                                     zlstsmtbal,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		zintbfnd.clear();
  		tranno.clear();
  		effdate.clear();
  		validflag.clear();
  		zlstintdte.clear();
  		znxtintdte.clear();
  		zlstfndval.clear();
  		zlstsmtdt.clear();
  		zlstsmtno.clear();
  		zlstsmtbal.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHitdrec() {
  		return hitdrec;
	}

	public FixedLengthStringData getHitdpfRecord() {
  		return hitdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHitdrec(what);
	}

	public void setHitdrec(Object what) {
  		this.hitdrec.set(what);
	}

	public void setHitdpfRecord(Object what) {
  		this.hitdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hitdrec.getLength());
		result.set(hitdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}