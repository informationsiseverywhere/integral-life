package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:39
 * Description:
 * Copybook name: HITRREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrrevKey = new FixedLengthStringData(64).isAPartOf(hitrrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrrevChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrrevKey, 0);
  	public FixedLengthStringData hitrrevChdrnum = new FixedLengthStringData(8).isAPartOf(hitrrevKey, 1);
  	public PackedDecimalData hitrrevTranno = new PackedDecimalData(5, 0).isAPartOf(hitrrevKey, 9);
  	public FixedLengthStringData hitrrevFeedbackInd = new FixedLengthStringData(1).isAPartOf(hitrrevKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(hitrrevKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}