package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:37
 * Description:
 * Copybook name: HITDREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitdrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitdrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitdrevKey = new FixedLengthStringData(64).isAPartOf(hitdrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitdrevChdrcoy = new FixedLengthStringData(1).isAPartOf(hitdrevKey, 0);
  	public FixedLengthStringData hitdrevChdrnum = new FixedLengthStringData(8).isAPartOf(hitdrevKey, 1);
  	public FixedLengthStringData hitdrevLife = new FixedLengthStringData(2).isAPartOf(hitdrevKey, 9);
  	public FixedLengthStringData hitdrevCoverage = new FixedLengthStringData(2).isAPartOf(hitdrevKey, 11);
  	public FixedLengthStringData hitdrevRider = new FixedLengthStringData(2).isAPartOf(hitdrevKey, 13);
  	public PackedDecimalData hitdrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitdrevKey, 15);
  	public PackedDecimalData hitdrevTranno = new PackedDecimalData(5, 0).isAPartOf(hitdrevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(hitdrevKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitdrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitdrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}