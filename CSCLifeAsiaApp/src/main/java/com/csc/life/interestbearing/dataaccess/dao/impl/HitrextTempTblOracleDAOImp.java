package com.csc.life.interestbearing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.interestbearing.dataaccess.model.HitrextTempTblDTO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HitrextTempTblOracleDAOImp extends HitrextTempTblDAOImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(HitrextTempTblOracleDAOImp.class);
	
	@Override
	public int copyDataToTempTable(String sysParam01, String sourceTableName, String tempTableName, 
			int noOfSubThread, HitrextTempTblDTO hitrextDTO){
		long startTime = System.currentTimeMillis();
		//IJTI-308 START
		StringBuilder query = new StringBuilder("INSERT INTO "); 
		query.append(tempTableName);
		query.append(" ( ");
		query.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ");
		query.append("ZINTBFND, TRANNO, ZRECTYP, BATCTRCDE, EFFDATE, CNTCURR, FDBKIND, ");
		query.append("CNTAMNT, FUNDAMNT, CNTTYP, PRCSEQ, PERSUR, FUNDRATE");
		query.append(", MEMBER_NAME) ");
		query.append("SELECT ");
		query.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ");
		query.append("ZINTBFND, TRANNO, ZRECTYP, BATCTRCDE, EFFDATE, CNTCURR, FDBKIND, ");
		query.append("CNTAMNT, FUNDAMNT, CNTTYP, PRCSEQ, PERSUR, FUNDRATE");
		query.append(", CONCAT('THREAD',TO_CHAR((MOD((ROW_NUMBER()OVER(  ORDER BY CHDRCOY, CHDRNUM, PRCSEQ, ZINTBFND, ZRECTYP)-1), ?) + 1), 'FM000')) MEMBER_NAME FROM ");
		query.append(sourceTableName );
		query.append(" WHERE CHDRCOY = ? AND FDBKIND = ? AND CHDRNUM >= ? AND CHDRNUM <= ? AND EFFDATE <= ? ");
		//IJTI-308 END
		if(DEBT.equalsIgnoreCase(sysParam01)){
			query.append(" AND COVDBTIND = ? ");
		}else if(SWCH.equalsIgnoreCase(sysParam01)){
			query.append(" AND SWCHIND = ?");
		}
		//query.append(" ORDER BY CHDRCOY, CHDRNUM, PRCSEQ, ZINTBFND, ZRECTYP");
		
		int copiedRecordCount = 0;		
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(query.toString());
			ps.setInt(1,noOfSubThread);
			ps.setString(2, hitrextDTO.getChdrcoy());
			ps.setString(3, hitrextDTO.getFdbkind());
			ps.setString(4, hitrextDTO.getFromChdrnum());
			ps.setString(5, hitrextDTO.getToChdrnum());
			ps.setInt(6, hitrextDTO.getEffdate());
			if(DEBT.equalsIgnoreCase(sysParam01)){
				ps.setString(7, hitrextDTO.getCovdbtind());
			}else if(SWCH.equalsIgnoreCase(sysParam01)){
				ps.setString(7, hitrextDTO.getSwchind());
			}
			copiedRecordCount = ps.executeUpdate();
			LOGGER.info("{} record(s) copied in {} ms", copiedRecordCount, (System.currentTimeMillis() - startTime));//IJTI-1561
			LOGGER.info("from table {} to table {}", sourceTableName, tempTableName);//IJTI-1561
		} catch (SQLException e) {
			LOGGER.error("copyDataToTable() ", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps ,null);
		}
		return copiedRecordCount;
	}

}
