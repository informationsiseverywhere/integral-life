package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:37
 * Description:
 * Copybook name: HITDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitdKey = new FixedLengthStringData(64).isAPartOf(hitdFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitdChdrcoy = new FixedLengthStringData(1).isAPartOf(hitdKey, 0);
  	public FixedLengthStringData hitdChdrnum = new FixedLengthStringData(8).isAPartOf(hitdKey, 1);
  	public FixedLengthStringData hitdLife = new FixedLengthStringData(2).isAPartOf(hitdKey, 9);
  	public FixedLengthStringData hitdCoverage = new FixedLengthStringData(2).isAPartOf(hitdKey, 11);
  	public FixedLengthStringData hitdRider = new FixedLengthStringData(2).isAPartOf(hitdKey, 13);
  	public PackedDecimalData hitdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(hitdKey, 15);
  	public FixedLengthStringData hitdZintbfnd = new FixedLengthStringData(4).isAPartOf(hitdKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(hitdKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}