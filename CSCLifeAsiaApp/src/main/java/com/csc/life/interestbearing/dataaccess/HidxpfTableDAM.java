package com.csc.life.interestbearing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HidxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:29
 * Class transformed from HIDXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HidxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 46;
	public FixedLengthStringData hidxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hidxpfRecord = hidxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hidxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hidxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hidxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hidxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hidxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(hidxrec);
	public FixedLengthStringData zintbfnd = DD.zintbfnd.copy().isAPartOf(hidxrec);
	public PackedDecimalData zlstintdte = DD.zlstintdte.copy().isAPartOf(hidxrec);
	public PackedDecimalData znxtintdte = DD.znxtintdte.copy().isAPartOf(hidxrec);
	public PackedDecimalData zlstfndval = DD.zlstfndval.copy().isAPartOf(hidxrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hidxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HidxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HidxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HidxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HidxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HidxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HidxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HidxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HIDXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"ZINTBFND, " +
							"ZLSTINTDTE, " +
							"ZNXTINTDTE, " +
							"ZLSTFNDVAL, " +
							"EFFDATE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     zintbfnd,
                                     zlstintdte,
                                     znxtintdte,
                                     zlstfndval,
                                     effdate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		zintbfnd.clear();
  		zlstintdte.clear();
  		znxtintdte.clear();
  		zlstfndval.clear();
  		effdate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHidxrec() {
  		return hidxrec;
	}

	public FixedLengthStringData getHidxpfRecord() {
  		return hidxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHidxrec(what);
	}

	public void setHidxrec(Object what) {
  		this.hidxrec.set(what);
	}

	public void setHidxpfRecord(Object what) {
  		this.hidxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hidxrec.getLength());
		result.set(hidxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}