package com.csc.life.interestbearing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:47
 * Description:
 * Copybook name: TH507REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th507rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th507Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData zannintr = new ZonedDecimalData(8, 5).isAPartOf(th507Rec, 0);
  	public ZonedDecimalData zdayintr = new ZonedDecimalData(8, 5).isAPartOf(th507Rec, 8);
  	public FixedLengthStringData filler = new FixedLengthStringData(484).isAPartOf(th507Rec, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th507Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th507Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}