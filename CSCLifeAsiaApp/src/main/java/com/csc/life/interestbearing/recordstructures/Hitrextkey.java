package com.csc.life.interestbearing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:39
 * Description:
 * Copybook name: HITREXTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hitrextkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hitrextFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hitrextKey = new FixedLengthStringData(64).isAPartOf(hitrextFileKey, 0, REDEFINE);
  	public FixedLengthStringData hitrextChdrcoy = new FixedLengthStringData(1).isAPartOf(hitrextKey, 0);
  	public FixedLengthStringData hitrextChdrnum = new FixedLengthStringData(8).isAPartOf(hitrextKey, 1);
  	public PackedDecimalData hitrextProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(hitrextKey, 9);
  	public FixedLengthStringData hitrextZintbfnd = new FixedLengthStringData(4).isAPartOf(hitrextKey, 11);
  	public FixedLengthStringData hitrextZrectyp = new FixedLengthStringData(1).isAPartOf(hitrextKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(hitrextKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hitrextFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hitrextFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}