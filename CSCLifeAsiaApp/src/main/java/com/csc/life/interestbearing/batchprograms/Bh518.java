/*
 * File: Bh518.java
 * Date: 29 August 2009 21:28:21
 * Author: Quipoz Limited
 * 
 * Class transformed from BH518.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.csc.life.interestbearing.reports.Rh518Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   Interest Bearing Fund Movement Listing.
*
*   This batch program has been cloned from B5106, the Unit
*   Movement Listing report for Unit Linked funds.
*
*   It reports on all the HITR records that have been processed in
*   the most recent (or one specified) run of the dealing program.
*   The precise name and number of the schedule that the program
*   is concerned with are held on the system parameters 01 and 02.
*   '*LAST' is specified for the schedule number when the most
*   recent run is of interest.
*
*   The HITR records are extracted using an SQL block fetch with
*   criteria of:  CHDRCOY = BSPR-COMPANY
*                 FDBKIND = 'Y'
*                 BSCHEDNAM = BPRD-system-parameter01
*                        or = BSPR schedname if param01 = spaces
*                 BSCHEDNUM = BPRD-system-parameter02
*      order by:  CHDRCOY, ZINTBFND, CHDRNUM, BATCTRCDE
*
*   The report is set up in the 3000- section. Headings are written
*   for the first record, on a change of fund or type or when the
*   end of a page has been reached. Detail lines are produced when
*   the fund has the same value as the previous record; total lines
*   on change of fund.
*
*****************************************************************
* </pre>
*/
public class Bh518 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlhitrCursorrs = null;
	private java.sql.PreparedStatement sqlhitrCursorps = null;
	private java.sql.Connection sqlhitrCursorconn = null;
	private Rh518Report printerFile = new Rh518Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH518");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLastZintbfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30).init("?", true);
	private String wsaaY = "Y";
	private FixedLengthStringData wsaaSaveBsprParams = new FixedLengthStringData(1024);

	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private Validator firstRec = new Validator(wsaaFirstRecord, "Y");

	private FixedLengthStringData wsaaFirstHeader = new FixedLengthStringData(1);
	private Validator firstHeader = new Validator(wsaaFirstHeader, "Y");
		/* SQLA-CODES */
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler, 8);
	private PackedDecimalData wsaaTotFundAmount = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private FixedLengthStringData wsaaSchedname = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaSchednumAlpha = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSchednumNum = new ZonedDecimalData(8, 0).isAPartOf(wsaaSchednumAlpha, 0, REDEFINE).setUnsigned();
	private PackedDecimalData wsaaSchednumComp = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
	private PackedDecimalData isql = new PackedDecimalData(5, 0).init(ZERO);
	private int sqlerrd = 0;

	private FixedLengthStringData sqlHitrpf = new FixedLengthStringData(47000);
	private FixedLengthStringData[] sqlFields = FLSArrayPartOfStructure(1000, 47, sqlHitrpf, 0);
	private FixedLengthStringData[] sqlChdrcoy = FLSDArrayPartOfArrayStructure(1, sqlFields, 0);
	private FixedLengthStringData[] sqlChdrnum = FLSDArrayPartOfArrayStructure(8, sqlFields, 1);
	private FixedLengthStringData[] sqlZintbfnd = FLSDArrayPartOfArrayStructure(4, sqlFields, 9);
	private PackedDecimalData[] sqlFundamnt = PDArrayPartOfArrayStructure(17, 2, sqlFields, 13);
	private PackedDecimalData[] sqlEffdate = PDArrayPartOfArrayStructure(8, 0, sqlFields, 22);
	private FixedLengthStringData[] sqlBatctrcde = FLSDArrayPartOfArrayStructure(4, sqlFields, 27);
	private FixedLengthStringData[] sqlFdbkind = FLSDArrayPartOfArrayStructure(1, sqlFields, 31);
	private FixedLengthStringData[] sqlBschednam = FLSDArrayPartOfArrayStructure(10, sqlFields, 32);
	private PackedDecimalData[] sqlBschednum = PDArrayPartOfArrayStructure(8, 0, sqlFields, 42);
	private String bsprrec = "BSPRREC";
		/* ERRORS */
	private String ivrm = "IVRM";
	private String esql = "ESQL";
	private String g159 = "G159";
		/* TABLES */
	private String t1688 = "T1688";
	private String t1693 = "T1693";
	private String t3629 = "T3629";
	private String t5515 = "T5515";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rh518h01Record = new FixedLengthStringData(116);
	private FixedLengthStringData rh518h01O = new FixedLengthStringData(116).isAPartOf(rh518h01Record, 0);
	private FixedLengthStringData bschednam = new FixedLengthStringData(10).isAPartOf(rh518h01O, 0);
	private ZonedDecimalData bschednum = new ZonedDecimalData(8, 0).isAPartOf(rh518h01O, 10);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh518h01O, 18);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh518h01O, 19);
	private FixedLengthStringData zintbfnd = new FixedLengthStringData(4).isAPartOf(rh518h01O, 49);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rh518h01O, 53);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(rh518h01O, 83);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30).isAPartOf(rh518h01O, 86);

	private FixedLengthStringData rh518h02Record = new FixedLengthStringData(2);

	private FixedLengthStringData rh518d01Record = new FixedLengthStringData(69);
	private FixedLengthStringData rh518d01O = new FixedLengthStringData(69).isAPartOf(rh518d01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rh518d01O, 0);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2).isAPartOf(rh518d01O, 8);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(rh518d01O, 25);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(rh518d01O, 35);
	private FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(rh518d01O, 39);

	private FixedLengthStringData rh518n01Record = new FixedLengthStringData(12);
	private FixedLengthStringData rh518n01O = new FixedLengthStringData(10).isAPartOf(rh518n01Record, 0);
	private FixedLengthStringData schalph = new FixedLengthStringData(10).isAPartOf(rh518n01O, 0);

	private FixedLengthStringData rh518t01Record = new FixedLengthStringData(18);
	private FixedLengthStringData rh518t01O = new FixedLengthStringData(18).isAPartOf(rh518t01Record, 0);
	private ZonedDecimalData totfndamt = new ZonedDecimalData(18, 2).isAPartOf(rh518t01O, 0);

	private FixedLengthStringData rh518e01Record = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Logical File: Extra data screen*/
	private Getdescrec getdescrec = new Getdescrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5515rec t5515rec = new T5515rec();
	
	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	
	private int ct01Value = 0;
	private int ct02Value = 0;
    private Map<String, List<Itempf>> t5515Map = null;
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		endOfCursor2080, 
		exit2090, 
		exit3029
	}

	public Bh518() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		indicArea.set("0");
		wsaaFirstRecord.set("Y");
		wsaaFirstHeader.set("Y");
		fundamnt.set(ZERO);
		effdate.set(ZERO);
		bschednum.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(bsprIO.getCompany());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			wsaaCompanynm.fill("?");
		}
		else {
			wsaaCompanynm.set(getdescrec.longdesc);
		}
		printerFile.openOutput();
		if (isEQ(bprdIO.getSystemParam01(),SPACES)) {
			wsaaSchedname.set(bsprIO.getScheduleName());
			bschednam.set(bsprIO.getScheduleName());
		}
		else {
			wsaaSchedname.set(bprdIO.getSystemParam01());
			bschednam.set(bprdIO.getSystemParam01());
		}
		if (isEQ(bprdIO.getSystemParam02(),SPACES)) {
			wsaaSchednumAlpha.set(bsprIO.getScheduleNumber());
			bschednum.set(bsprIO.getScheduleNumber());
		}
		else {
			wsaaSchednumAlpha.set(bprdIO.getSystemParam02());
		}
		if (isEQ(wsaaSchednumAlpha,"*LAST")) {
			lastJobNo1100();
		}
		if (isEQ(wsaaSchednumAlpha,NUMERIC)) {
			wsaaSchednumComp.set(wsaaSchednumNum);
			bschednum.set(wsaaSchednumNum);
		}
		if (isNE(wsaaSchednumAlpha,"*LAST")
		&& isNE(wsaaSchednumAlpha,NUMERIC)) {
			conlogrec.error.set(g159);
			conlogrec.params.set(bprdIO.getParams());
			callConlog003();
			pageHeading3030();
			wsaaFirstHeader.set("N");
			indOn.setTrue(20);
			if (isNE(wsaaSchednumAlpha,SPACES)) {
				schalph.set(wsaaSchednumAlpha);
			}
			else {
				schalph.set("(BLANK)");
				printerFile.printRh518n01(rh518n01Record, indicArea);
				printerFile.printRh518d01(rh518d01Record, indicArea);
				wsspEdterror.set(varcom.endp);
				goTo(GotoLabel.exit1090);
			}
		}
		wsaaCompany.set(bsprIO.getCompany());
		String sqlhitrCursor = " SELECT  CHDRCOY, CHDRNUM, ZINTBFND, FUNDAMNT, EFFDATE, BATCTRCDE, FDBKIND, BSCHEDNAM, BSCHEDNUM" +
	" FROM   " + appVars.getTableNameOverriden("HITRPF") + " " +
	" WHERE CHDRCOY = ?" +
	" AND FDBKIND = ?" +
	" AND BSCHEDNAM = ?" +
	" AND BSCHEDNUM = ?" +
	" AND FUNDAMNT <> 0" +
	" ORDER BY CHDRCOY, ZINTBFND, CHDRNUM, BATCTRCDE";
		sqlerrorflag = false;
		try {
			sqlhitrCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.interestbearing.dataaccess.HitrpfTableDAM());
			sqlhitrCursorps = appVars.prepareStatementEmbeded(sqlhitrCursorconn, sqlhitrCursor, "HITRPF");
			appVars.setDBString(sqlhitrCursorps, 1, wsaaCompany.toString());
			appVars.setDBString(sqlhitrCursorps, 2, wsaaY);
			appVars.setDBString(sqlhitrCursorps, 3, wsaaSchedname.toString());
			appVars.setDBDouble(sqlhitrCursorps, 4, wsaaSchednumComp.toDouble());
			sqlhitrCursorrs = appVars.executeQuery(sqlhitrCursorps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		isql.set(wsaaRowsInBlock);
		sqlerrd = wsaaRowsInBlock.toInt();
		
		String coy = bsprIO.getCompany().toString();
		t5515Map = itemDAO.loadSmartTable("IT", coy, "T5515");
	}


protected void lastJobNo1100()
 {
		wsaaSaveBsprParams.set(bsprIO.getParams());
		bsprIO.setScheduleName(bprdIO.getSystemParam01());
		bsprIO.setScheduleNumber(99999999);
		bsprIO.setCompany(SPACES);
		bsprIO.setProcessName(SPACES);
		bsprIO.setProcessOccNum(ZERO);
		bsprIO.setFormat(bsprrec);
		bsprIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, bsprIO);
		if ((isNE(bsprIO.getStatuz(), varcom.oK) && isNE(bsprIO.getStatuz(), varcom.endp))
				|| isNE(bsprIO.getScheduleName(), bprdIO.getSystemParam01())
				|| (isNE(bsprIO.getProcessStatus(), "90") && isNE(bprdIO.getSystemParam01(), SPACES))) {
			syserrrec.params.set(bsprIO.getParams());
			fatalError600();
		}
		if (isEQ(bsprIO.getStatuz(), varcom.endp)) {
			wsaaSchedname.set("ENDP");
			wsaaSchednumComp.set(ZERO);
		}
		wsaaSchednumComp.set(bsprIO.getScheduleNumber());
		bschednum.set(bsprIO.getScheduleNumber());
		bsprIO.setParams(wsaaSaveBsprParams);
	}


protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case endOfCursor2080: {
					endOfCursor2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		isql.add(1);
		if (isGTE(sqlerrd, isql.toInt())) {
			ct01Value++;
			goTo(GotoLabel.exit2090);
		}
		else {
			if (isEQ(sqlerrd, wsaaRowsInBlock.toInt())) {
				/*NEXT_SENTENCE*/
			}
			else {
				goTo(GotoLabel.endOfCursor2080);
			}
		}
		sqlerrorflag = false;
		int hitrCursorLoopIndex = 1;
		try {
			for (; isLTE(hitrCursorLoopIndex,wsaaRowsInBlock.toInt())
			&& sqlhitrCursorrs.next(); hitrCursorLoopIndex++ ){
				appVars.getDBObject(sqlhitrCursorrs, 1, sqlChdrcoy[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 2, sqlChdrnum[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 3, sqlZintbfnd[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 4, sqlFundamnt[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 5, sqlEffdate[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 6, sqlBatctrcde[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 7, sqlFdbkind[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 8, sqlBschednam[hitrCursorLoopIndex]);
				appVars.getDBObject(sqlhitrCursorrs, 9, sqlBschednum[hitrCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		sqlerrd = hitrCursorLoopIndex - 1;
		if (sqlerrd == 0) {
			goTo(GotoLabel.endOfCursor2080);
		}
		isql.set(1);
		ct01Value++;
		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		appVars.freeDBConnectionIgnoreErr(sqlhitrCursorconn, sqlhitrCursorps, sqlhitrCursorrs);
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
 {
		if (firstRec.isTrue()) {
			wsaaFirstRecord.set("N");
			wsaaFirstHeader.set("N");
			setControlBreak3010();
			setUpFundDetails3020();
			pageHeading3030();
		}
		if (isNE(sqlZintbfnd[isql.toInt()], wsaaLastZintbfnd)) {
			totalsLine3040();
			zeroiseAccumulations3050();
			setUpFundDetails3020();
			pageHeading3030();
		}
		if (isEQ(sqlZintbfnd[isql.toInt()], wsaaLastZintbfnd)) {
			accumulate3060();
			printDetailLine3070();
		}
		setControlBreak3010();
	}


protected void setControlBreak3010()
	{
		/*SET-CONTROL-BREAK*/
		wsaaLastZintbfnd.set(sqlZintbfnd[isql.toInt()]);
		indOn.setTrue(10);
		/*EXIT*/
	}

protected void setUpFundDetails3020()
 {
		if (isEQ(sqlZintbfnd[isql.toInt()], SPACES)) {
			return;
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(sqlChdrcoy[isql.toInt()]);
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(sqlZintbfnd[isql.toInt()]);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			longdesc.fill("?");
		} else {
			longdesc.set(getdescrec.longdesc);
		}

		if (t5515Map != null && t5515Map.containsKey(sqlZintbfnd[isql.toInt()])) {
			for (Itempf item : t5515Map.get(sqlZintbfnd[isql.toInt()])) {
				if (item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
					t5515rec.t5515Rec.set(StringUtil.rawToString(item.getGenarea()));
					break;
				}
			}
		} else {
			syserrrec.params.set("T5515:" + sqlZintbfnd[isql.toInt()]);
			fatalError600();
		}

		fndcurr.set(t5515rec.currcode);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(t5515rec.currcode);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			curdesc.fill("?");
		} else {
			curdesc.set(getdescrec.longdesc);
		}
	}


protected void pageHeading3030()
	{
		/*PAGE-HEADING*/
		company.set(bsprIO.getCompany());
		companynm.set(wsaaCompanynm);
		if (isNE(isql,0)) {
			zintbfnd.set(sqlZintbfnd[isql.toInt()]);
		}
		/*PRINT-RH518H01*/
		printerFile.printRh518h01(rh518h01Record, indicArea);
		printerFile.printRh518h02(rh518h02Record, indicArea);
		ct02Value++;
		/*EXIT*/
	}

protected void totalsLine3040()
	{
		/*TOTALS-LINE*/
		totfndamt.set(wsaaTotFundAmount);
		/*PRINT-RH518T01*/
		printerFile.printRh518t01(rh518t01Record, indicArea);
		/*EXIT*/
	}

protected void zeroiseAccumulations3050()
	{
		/*ZEROISE-ACCUMULATIONS*/
		wsaaTotFundAmount.set(ZERO);
		/*EXIT*/
	}

protected void accumulate3060()
	{
		/*ACCUMULATE*/
		wsaaTotFundAmount.add(sqlFundamnt[isql.toInt()]);
		/*EXIT*/
	}

protected void printDetailLine3070()
 {
		if (isNE(sqlChdrnum[isql.toInt()], wsaaLastChdrnum)) {
			wsaaLastChdrnum.set(sqlChdrnum[isql.toInt()]);
			indOn.setTrue(10);
		} else {
			indOff.setTrue(10);
		}
		chdrnum.set(sqlChdrnum[isql.toInt()]);
		fundamnt.set(sqlFundamnt[isql.toInt()]);
		batctrcde.set(sqlBatctrcde[isql.toInt()]);
		effdate.set(sqlEffdate[isql.toInt()]);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(sqlBatctrcde[isql.toInt()]);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			trandesc.set(SPACES);
		} else {
			trandesc.set(getdescrec.longdesc);
		}
		printerFile.printRh518d01(rh518d01Record, indicArea);

	}


protected void commit3500()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callProgram(Contot.class, contotrec.contotRec);		
		ct01Value = 0;
	
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct02Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		if (firstHeader.isTrue()) {
			isql.set(1);
			indOn.setTrue(10);
			setUpFundDetails3020();
			pageHeading3030();
			schalph.set(wsaaSchednumAlpha);
			printerFile.printRh518n01(rh518n01Record, indicArea);
		}
		totalsLine3040();
		printerFile.printRh518e01(rh518e01Record, indicArea);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
