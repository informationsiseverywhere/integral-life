/*
 * File: Bh509.java
 * Date: 29 August 2009 21:26:57
 * Author: Quipoz Limited
 * 
 * Class transformed from BH509.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.interestbearing.dataaccess.HifdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.dataaccess.HitxpfTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  INTEREST BEARING FUND DEALING
*  =============================
*
*  Overview
*  ========
*
*  This program will read HITX records, HITXs being undealt HITRs
*  created by the Interest Bearing Fund dealing Splitter program,
*  BH508, and "deal" each record found. Each "deal" involves the
*  creation of accounting movements for each fund identified on
*  the HITX.
*
*  BH508 has three functions and each function will return
*  different types of HITRs:                                     is:
*
*  DEAL: This function will extract all undealt HITRs of any type
*        (i.e any HITR with a feedback indicator equal to spaces)
*
*  DEBT: This function will extract only unprocessed HITRs which
*        have been created by the Coverage Debt Settlement(B5104)
*        program. (i.e. feedback indicator equal to spaces and a
*        coverage debt indicator set to "D".)
*
*  SWCH: This function will pick up undealt HITRs which have been
*        created as part of a fund switch (i.e.feedback indicator
*        equal to spaces and switch indicator equal to 'Y'.
*
*  Thus this program could be run three times within the Unit
*  Dealing bacth job. The first time to pick up all outstanding  nd
*  deals, the second time to pick up fund switches and the third nd
*  time to try to settle coverage debt.
*
*  Processing
*  ==========
*
*  Initialise.
*
*  - Check the restart method is 3
*  - Find the name of the HITX temporary file. HITX is a temporary
*    file which has the name of "HITX" + first two characters of
*    param 4 + the last four digits of the job number.
*  - Pre-load constantly referenced Tables into working storage.
*    These are: T5645, T5688.
*  - Set up the LIFACMV fields which will not change.
*
*  Read.
*
*  - Read a HITX.
*
*  Edit.
*
*  - Softlock the contract if this HITX is for a new contract.
*    Note that if a softlock exists we could further corrupt a
*    policy if we process its HITRs.
*  - If the HITX has no fund then we will still process it
*    although there will be no fund movements. Log it.
*
*  Update.
*
*  - Read and hold the HITR using HITRALO. It is conceivable
*    although unlikely that the HITR extracted by BH508 may have
*    been removed. If so log its disappearance and skip all
*    further processing.
*  - Search WSAA-T5688 for the accounting level for this contract
*    type. It will be either coverage or contract level accounted.
*  - If the fund is spaces, account for the non-invested amounts,
*    flag the HITR as processed and skip all further processing.
*  - Read and hold the coverage level summary record (HITS).
*  - If this is a surrender, then we need to calculate the HITR
*     fund amount based on the surrender percentage.
*  - Now we need to write ACMVs to reflect the ledger changes
*    for this almost complete HITR. Before we do this however
*    we need to round the various posting from five decimal
*    places to two. As this can result in rounding errors we
*    sum the rounded amounts up to ensure they balance to zero.
*    If they do not balance to zero we add the balance to a
*    non-zero posting, thus balancing the posted ACMVs. Then
*    we post the ACMVs.
*  - Now update the relevant fund records. Rewrite the now
*    completed HITR as processed, create or update the coverage
*    summary record (HITS) with the fund balance, and finally
*    finally update the fund summary record with the new fund
*    balance. (This last bit is done by storing the balance for
*    each fund in a working storage table keyed by fund code
*    and then flushing the table to HIFD immediately before a
*    commit in the 3500 section.)
*  - And finally ... if the trigger module on the HITR is not
*    blank call the trigger routine (e.g. FUNDSWCH).
*
*  Finalise.
*
*  - Flush any fund totals to HIFD.
*  - Close the temporary file.
*  - Remove the override
*  - Set the parameter statuz to O-K
*
***********************************************************************
* </pre>
*/
public class Bh509 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private HitxpfTableDAM hitxpf = new HitxpfTableDAM();
	private HitxpfTableDAM hitxpfRec = new HitxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH509");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/*    HITX parameters*/
	private FixedLengthStringData wsaaSaveBsprParams = new FixedLengthStringData(1024);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaHitxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHitxFn, 0, FILLER).init("HITX");
	private FixedLengthStringData wsaaHitxRunid = new FixedLengthStringData(2).isAPartOf(wsaaHitxFn, 4);
	private ZonedDecimalData wsaaHitxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHitxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaDatimeInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDatimeDate = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 0);
	private FixedLengthStringData wsaaDatimeTime = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 10);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaDatimeInit, 20);

		/* WSAA-HIFD-ARRAY */
	private FixedLengthStringData[] wsaaHifdRec = FLSInittedArray (50, 13);
	private FixedLengthStringData[] wsaaHifdFund = FLSDArrayPartOfArrayStructure(4, wsaaHifdRec, 0);
	private PackedDecimalData[] wsaaHifdZtotfndval = PDArrayPartOfArrayStructure(17, 2, wsaaHifdRec, 4);
	private static final int wsaaHifdSize = 50;

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2605 fixed--Array size increased
		private static final int wsaaT5645Size = 1000;
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);

		/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 9);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(8, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Key, 3);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 8);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
	//private static final int wsaaT5688Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;
	private static final String bsprrec = "BSPRREC";
	private static final String hitsrec = "HITSREC";
	private static final String hifdrec = "HIFDREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String h791 = "H791";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private HifdTableDAM hifdIO = new HifdTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Udtrigrec udtrigrec = new Udtrigrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		triggerProcessing3070, 
		exit3090
	}

	public Bh509() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaDatimeInit.set(bsscIO.getDatimeInit());
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		tempFileOverride1100();
		hitxpf.openInput();
		/*    Load All Accounting Rules.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setStatuz(varcom.oK);
		iy.set(1);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			loadT56451200();
		}
		
		/*    Load All Contract Processing Rules.*/
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		itdmIO.setStatuz(varcom.oK);
		ix.set(1);
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadT56881300();
		}
		
		/*    Fields which require initialisation only once.*/
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(bprdIO.getAuthCode());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
	}

protected void tempFileOverride1100()
	{
					tempFileOverride1101();
					override1105();
				}

protected void tempFileOverride1101()
	{
		if (isEQ(bprdIO.getSystemParam01(),SPACES)) {
			wsaaHitxJobno.set(bsprIO.getScheduleNumber());
			return ;
		}
		/*    Firstly save this process's BSPR-Params.*/
		wsaaSaveBsprParams.set(bsprIO.getParams());
		bsprIO.setScheduleName(bprdIO.getSystemParam01());
		bsprIO.setScheduleNumber(99999999);
		bsprIO.setCompany(SPACES);
		bsprIO.setProcessName(SPACES);
		bsprIO.setProcessOccNum(ZERO);
		bsprIO.setFormat(bsprrec);
		bsprIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, bsprIO);
		if (isNE(bsprIO.getStatuz(),varcom.oK)
		|| isNE(bsprIO.getScheduleName(),bprdIO.getSystemParam01())
		|| isNE(bsprIO.getProcessStatus(),"90")) {
			bsprIO.setScheduleName(bprdIO.getSystemParam01());
			bsprIO.setScheduleNumber(99999999);
			bsprIO.setCompany(SPACES);
			bsprIO.setProcessName(SPACES);
			bsprIO.setProcessOccNum(ZERO);
			bsprIO.setFormat(bsprrec);
			bsprIO.setFunction(varcom.endr);
			bsprIO.setStatuz(varcom.endp);
			syserrrec.params.set(bsprIO.getParams());
			fatalError600();
		}
		/*    Set up our temporary file job number as that of the last*/
		/*    Fund Movement Report job number.*/
		wsaaHitxJobno.set(bsprIO.getScheduleNumber());
		/*    Restore the BSPR-Params.*/
		bsprIO.setParams(wsaaSaveBsprParams);
	}

protected void override1105()
	{
		/*    Now set up the rest of the temporary file params.*/
		wsaaHitxRunid.set(bprdIO.getSystemParam04());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HITXPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHitxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void loadT56451200()
	{
			start1201();
		}

protected void start1201()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),t5645)
		|| isNE(itemIO.getItemitem(),wsaaProg)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			if (isEQ(itemIO.getFunction(),varcom.begn)) {
				itemIO.setItemitem(wsaaProg);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		if (isGT(iy,wsaaT5645Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5645);
			fatalError600();
		}
		ix.set(1);
		while ( !(isGT(ix,15))) {
			wsaaT5645Cnttot[iy.toInt()].set(t5645rec.cnttot[ix.toInt()]);
			wsaaT5645Glmap[iy.toInt()].set(t5645rec.glmap[ix.toInt()]);
			wsaaT5645Sacscode[iy.toInt()].set(t5645rec.sacscode[ix.toInt()]);
			wsaaT5645Sacstype[iy.toInt()].set(t5645rec.sacstype[ix.toInt()]);
			wsaaT5645Sign[iy.toInt()].set(t5645rec.sign[ix.toInt()]);
			ix.add(1);
			iy.add(1);
		}
		
		itemIO.setFunction(varcom.nextr);
	}

protected void loadT56881300()
	{
			loadT56881301();
		}

protected void loadT56881301()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5688)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isGT(ix,wsaaT5688Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5688);
			fatalError600();
		}
		wsaaT5688Cnttype[ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5688Itmfrm[ix.toInt()].set(itdmIO.getItmfrm());
		wsaaT5688Comlvlacc[ix.toInt()].set(t5688rec.comlvlacc);
		ix.add(1);
		itdmIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		hitxpf.read(hitxpfRec);
		if (hitxpf.isAtEnd()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		contotrec.totno.set(controlTotalsInner.ct01);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		wsspEdterror.set(SPACES);
		if (isEQ(hitxpfRec.chdrnum,wsaaALockedChdrnum)) {
			contotrec.totno.set(controlTotalsInner.ct03);
			contotrec.totval.set(1);
			callContot001();
			return ;
		}
		if (isNE(hitxpfRec.chdrnum,lastChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz,"LOCK")) {
				wsaaALockedChdrnum.set(hitxpfRec.chdrnum);
				return ;
			}
			else {
				wsaaALockedChdrnum.set(SPACES);
			}
			lastChdrnum.set(hitxpfRec.chdrnum);
			wsaaJrnseq.set(ZERO);
		}
		/*    If we don't have a fund, we will do no 'dealing' for this*/
		/*    HITR. However, we do still need to process it.*/
		if (isEQ(hitxpfRec.zintbfnd,SPACES)) {
			contotrec.totno.set(controlTotalsInner.ct04);
			contotrec.totval.set(1);
			callContot001();
		}
		wsspEdterror.set(varcom.oK);
	}

protected void softlockPolicy2600()
	{
		softlockPolicy2610();
	}

protected void softlockPolicy2610()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(hitxpfRec.chdrcoy);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(hitxpfRec.chdrnum);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hitxpfRec.chdrcoy);
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(hitxpfRec.chdrnum);
			stringVariable1.setStringInto(conlogrec.params);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			contotrec.totno.set(controlTotalsInner.ct03);
			contotrec.totval.set(1);
			callContot001();
			contotrec.totno.set(controlTotalsInner.ct02);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			if (isNE(sftlockrec.statuz,varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
				case triggerProcessing3070: 
					triggerProcessing3070();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		readhHitr5100();
		if (isEQ(hitraloIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3090);
		}
		setAcctLvlForPostings5200();
		/*    Process non-invested premium.*/
		if (isEQ(hitraloIO.getZintbfnd(),SPACES)) {
			processHitrWithoutFund5300();
			hitraloIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, hitraloIO);
			if (isNE(hitraloIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hitraloIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.triggerProcessing3070);
		}
		/*    Process invested premium.*/
		readhHits5400();
		/*    Calculate surrender/switch amount.*/
		if (isNE(hitraloIO.getSurrenderPercent(),ZERO)
		&& isEQ(hitsIO.getStatuz(),varcom.oK)) {
			setPrecision(hitraloIO.getFundAmount(), 6);
			hitraloIO.setFundAmount(mult(div(mult(mult(hitsIO.getZcurprmbal(),hitraloIO.getSvp()),hitraloIO.getSurrenderPercent()),100),-1), true);
		}
		/* MOVE HITRALO-FUND-AMOUNT     TO ZRDP-AMOUNT-IN.              */
		/* MOVE HITRALO-FUND-CURRENCY   TO ZRDP-CURRENCY.               */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO HITRALO-FUND-AMOUNT.         */
		if (isNE(hitraloIO.getFundAmount(), 0)) {
			zrdecplrec.amountIn.set(hitraloIO.getFundAmount());
			zrdecplrec.currency.set(hitraloIO.getFundCurrency());
			callRounding8000();
			hitraloIO.setFundAmount(zrdecplrec.amountOut);
		}
		completeHitrDetails5500();
		accountingPostings5600();
		hitraloIO.setScheduleName(bsscIO.getScheduleName());
		hitraloIO.setScheduleNumber(bsscIO.getScheduleNumber());
		hitraloIO.setFeedbackInd("Y");
		hitraloIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitraloIO.getParams());
			fatalError600();
		}
		setPrecision(hitsIO.getZcurprmbal(), 2);
		hitsIO.setZcurprmbal(add(hitsIO.getZcurprmbal(),hitraloIO.getFundAmount()));
		hitsIO.setZlstupdt(bsscIO.getEffectiveDate());
		hitsIO.setFormat(hitsrec);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(controlTotalsInner.ct07);
		contotrec.totval.set(1);
		callContot001();
		/*    We must now update the total fund balance on the HIFD*/
		/*    record for the fund. Instead of performing reads and*/
		/*    rewrites for each HITR we process, we store the values in*/
		/*    working storage. When MAINB is ready to commit the 3500*/
		/*    section will be performed where we will flush the working*/
		/*    storage values to HIFD.*/
		for (ix.set(1); !(isGT(ix,wsaaHifdSize)
		|| isEQ(wsaaHifdFund[ix.toInt()],hitraloIO.getZintbfnd())
		|| isEQ(wsaaHifdFund[ix.toInt()],SPACES)); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaHifdSize)) {
			syserrrec.params.set(hitraloIO.getParams());
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		if (isEQ(wsaaHifdFund[ix.toInt()],SPACES)) {
			wsaaHifdFund[ix.toInt()].set(hitraloIO.getZintbfnd());
			wsaaHifdZtotfndval[ix.toInt()].set(hitraloIO.getFundAmount());
		}
		else {
			wsaaHifdZtotfndval[ix.toInt()].add(hitraloIO.getFundAmount());
		}
	}

protected void triggerProcessing3070()
	{
		if (isNE(hitraloIO.getTriggerModule(),SPACES)) {
			callTrigger5700();
		}
	}

protected void commit3500()
	{
		commit3510();
	}

protected void commit3510()
	{
		/*    In the pre-commit section we simply loop through the        d*/
		/*    accumulated totals of funds and update the respective fund*/
		/*    totals.*/
		for (ix.set(1); !(isGT(ix,wsaaHifdSize)
		|| isEQ(wsaaHifdFund[ix.toInt()],SPACES)); ix.add(1)){
			hifdIO.setChdrcoy(bsprIO.getCompany());
			hifdIO.setZintbfnd(wsaaHifdFund[ix.toInt()]);
			hifdIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, hifdIO);
			if (isEQ(hifdIO.getStatuz(),varcom.oK)) {
				hifdIO.setFunction(varcom.readh);
				SmartFileCode.execute(appVars, hifdIO);
				if (isNE(hifdIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hifdIO.getParams());
					fatalError600();
				}
				setPrecision(hifdIO.getZtotfndval(), 2);
				hifdIO.setZtotfndval(add(hifdIO.getZtotfndval(),wsaaHifdZtotfndval[ix.toInt()]));
				hifdIO.setFunction(varcom.rewrt);
			}
			else {
				if (isEQ(hifdIO.getStatuz(),varcom.mrnf)) {
					hifdIO.setZtotfndval(wsaaHifdZtotfndval[ix.toInt()]);
					hifdIO.setFormat(hifdrec);
					hifdIO.setFunction(varcom.writr);
				}
				else {
					syserrrec.params.set(hifdIO.getParams());
					fatalError600();
				}
			}
			SmartFileCode.execute(appVars, hifdIO);
			if (isNE(hifdIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hifdIO.getParams());
				fatalError600();
			}
			wsaaHifdFund[ix.toInt()].set(SPACES);
			wsaaHifdZtotfndval[ix.toInt()].set(ZERO);
		}
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*    Ensure we flush the HIFD accumulations.*/
		commit3500();
		hitxpf.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void readhHitr5100()
	{
		readhHitr5110();
	}

protected void readhHitr5110()
	{
		hitraloIO.setFunction(varcom.readh);
		hitraloIO.setChdrcoy(hitxpfRec.chdrcoy);
		hitraloIO.setChdrnum(hitxpfRec.chdrnum);
		hitraloIO.setLife(hitxpfRec.life);
		hitraloIO.setCoverage(hitxpfRec.coverage);
		hitraloIO.setRider(hitxpfRec.rider);
		hitraloIO.setPlanSuffix(hitxpfRec.planSuffix);
		hitraloIO.setZintbfnd(hitxpfRec.zintbfnd);
		hitraloIO.setZrectyp(hitxpfRec.zrectyp);
		hitraloIO.setProcSeqNo(hitxpfRec.procSeqNo);
		hitraloIO.setTranno(hitxpfRec.tranno);
		hitraloIO.setEffdate(hitxpfRec.effdate);
		SmartFileCode.execute(appVars, hitraloIO);
		/*    It is possible, although very unlikely, that a reversal may*/
		/*    have been done against the contract which could result in*/
		/*    the HITR disappearing. Log this fact and skip the HITR.*/
		if (isEQ(hitraloIO.getStatuz(),varcom.mrnf)) {
			contotrec.totno.set(controlTotalsInner.ct05);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			if (isNE(hitraloIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hitraloIO.getParams());
				fatalError600();
			}
		}
	}

protected void setAcctLvlForPostings5200()
	{
		setAcctLvlForPostings5210();
	}

protected void setAcctLvlForPostings5210()
	{
		for (ix.set(1); !(isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)
		|| (isEQ(wsaaT5688Cnttype[ix.toInt()],hitraloIO.getCnttyp())
		&& isLTE(wsaaT5688Itmfrm[ix.toInt()],hitraloIO.getEffdate()))); ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(ix,wsaaT5688Size)
		|| isEQ(wsaaT5688Cnttype[ix.toInt()],SPACES)) {
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5688);
			itdmIO.setItemitem(hitraloIO.getCnttyp());
			itdmIO.setItmfrm(hitraloIO.getEffdate());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		wsaaComponLevelAccounted.set(wsaaT5688Comlvlacc[ix.toInt()]);
	}

protected void processHitrWithoutFund5300()
	{
		processHitrWithoutFund5310();
	}

protected void processHitrWithoutFund5310()
	{
		lifacmvrec.origcurr.set(hitraloIO.getCntcurr());
		lifacmvrec.origamt.set(hitraloIO.getContractAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
			contotrec.totno.set(controlTotalsInner.ct09);
			x1000CallLifacmv();
			t5645Ix.set(5);
			wsaaT5645Sacscode[5].set(hitraloIO.getSacscode());
			wsaaT5645Sacstype[5].set(hitraloIO.getSacstyp());
			wsaaT5645Glmap[5].set(hitraloIO.getGenlcde());
			contotrec.totno.set(controlTotalsInner.ct10);
			x1000CallLifacmv();
		}
		else {
			t5645Ix.set(8);
			lifacmvrec.substituteCode[6].set(SPACES);
			contotrec.totno.set(controlTotalsInner.ct09);
			x1000CallLifacmv();
			t5645Ix.set(12);
			wsaaT5645Sacscode[12].set(hitraloIO.getSacscode());
			wsaaT5645Sacstype[12].set(hitraloIO.getSacstyp());
			wsaaT5645Glmap[12].set(hitraloIO.getGenlcde());
			contotrec.totno.set(controlTotalsInner.ct10);
			x1000CallLifacmv();
		}
		hitraloIO.setScheduleName(bsscIO.getScheduleName());
		hitraloIO.setScheduleNumber(bsscIO.getScheduleNumber());
		hitraloIO.setFeedbackInd("Y");
	}

protected void readhHits5400()
	{
			readhHits5410();
		}

protected void readhHits5410()
	{
		hitsIO.setFunction(varcom.readh);
		hitsIO.setChdrcoy(hitraloIO.getChdrcoy());
		hitsIO.setChdrnum(hitraloIO.getChdrnum());
		hitsIO.setLife(hitraloIO.getLife());
		hitsIO.setCoverage(hitraloIO.getCoverage());
		hitsIO.setRider(hitraloIO.getRider());
		hitsIO.setPlanSuffix(hitraloIO.getPlanSuffix());
		hitsIO.setZintbfnd(hitraloIO.getZintbfnd());
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		if (isEQ(hitsIO.getStatuz(),varcom.mrnf)) {
			hitsIO.setFunction(varcom.writr);
			hitsIO.setZcurprmbal(ZERO);
			return ;
		}
		hitsIO.setFunction(varcom.rewrt);
	}

protected void completeHitrDetails5500()
	{
		completeHitrDetails5510();
	}

protected void completeHitrDetails5510()
	{
		/*    First, check if we have an amount in the Contract*/
		/*    Currency, but no amount in the Fund Currency.*/
		/*    If yes, then move the Contract Amount to the Fund Amount,*/
		/*    applying currency conversion if the Fund Currency is not*/
		/*    equal to the Contract Currency.*/
		if (isNE(hitraloIO.getContractAmount(),ZERO)
		&& isEQ(hitraloIO.getFundAmount(),ZERO)) {
			if (isEQ(hitraloIO.getFundCurrency(),hitraloIO.getCntcurr())) {
				hitraloIO.setFundAmount(hitraloIO.getContractAmount());
				hitraloIO.setFundRate(1);
			}
			else {
				conlinkrec.amountIn.set(hitraloIO.getContractAmount());
				conlinkrec.currIn.set(hitraloIO.getCntcurr());
				conlinkrec.currOut.set(hitraloIO.getFundCurrency());
				conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
				if (isLT(hitraloIO.getContractAmount(),ZERO)) {
					conlinkrec.function.set("SURR");
				}
				else {
					conlinkrec.function.set("CVRT");
				}
				x2000CallXcvrt();
				if (isNE(conlinkrec.amountOut, 0)) {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					zrdecplrec.currency.set(hitraloIO.getFundCurrency());
					callRounding8000();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
				}
				hitraloIO.setFundAmount(conlinkrec.amountOut);
				hitraloIO.setFundRate(conlinkrec.rateUsed);
			}
		}
		/*    Next, check if we have an amount in the Fund*/
		/*    Currency, but no amount in the Contract Currency.*/
		/*    If yes, then move the Fund Amount to the Contract Amount,*/
		/*    applying currenct conversion if the Fund Currency is not*/
		/*    equal to the Contract Currency.*/
		if (isNE(hitraloIO.getFundAmount(),ZERO)
		&& isEQ(hitraloIO.getContractAmount(),ZERO)) {
			if (isEQ(hitraloIO.getFundCurrency(),hitraloIO.getCntcurr())) {
				hitraloIO.setContractAmount(hitraloIO.getFundAmount());
				hitraloIO.setFundRate(1);
			}
			else {
				conlinkrec.amountIn.set(hitraloIO.getFundAmount());
				conlinkrec.currIn.set(hitraloIO.getFundCurrency());
				conlinkrec.currOut.set(hitraloIO.getCntcurr());
				conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
				conlinkrec.function.set("CVRT");
				x2000CallXcvrt();
				if (isNE(conlinkrec.amountOut, 0)) {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					zrdecplrec.currency.set(hitraloIO.getCntcurr());
					callRounding8000();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
				}
				hitraloIO.setContractAmount(conlinkrec.amountOut);
				hitraloIO.setFundRate(conlinkrec.rateUsed);
			}
		}
	}

protected void accountingPostings5600()
	{
		accountingPostings5601();
	}

protected void accountingPostings5601()
	{
		if (isNE(hitraloIO.getCntcurr(),hitraloIO.getFundCurrency())) {
			acctForCurrConversion5650();
		}
		/*    Investment premium/interest.*/
		lifacmvrec.origcurr.set(hitraloIO.getCntcurr());
		lifacmvrec.origamt.set(hitraloIO.getContractAmount());
		if (isEQ(hitraloIO.getZrectyp(),"I")) {
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(6);
				lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
				wsaaT5645Sacscode[6].set(hitraloIO.getSacscode());
				wsaaT5645Sacstype[6].set(hitraloIO.getSacstyp());
				wsaaT5645Glmap[6].set(hitraloIO.getGenlcde());
			}
			else {
				t5645Ix.set(13);
				lifacmvrec.substituteCode[6].set(SPACES);
				wsaaT5645Sacscode[13].set(hitraloIO.getSacscode());
				wsaaT5645Sacstype[13].set(hitraloIO.getSacstyp());
				wsaaT5645Glmap[13].set(hitraloIO.getGenlcde());
			}
			contotrec.totno.set(controlTotalsInner.ct16);
		}
		else {
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(5);
				lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
				wsaaT5645Sacscode[5].set(hitraloIO.getSacscode());
				wsaaT5645Sacstype[5].set(hitraloIO.getSacstyp());
				wsaaT5645Glmap[5].set(hitraloIO.getGenlcde());
			}
			else {
				t5645Ix.set(12);
				lifacmvrec.substituteCode[6].set(SPACES);
				wsaaT5645Sacscode[12].set(hitraloIO.getSacscode());
				wsaaT5645Sacstype[12].set(hitraloIO.getSacstyp());
				wsaaT5645Glmap[12].set(hitraloIO.getGenlcde());
			}
			contotrec.totno.set(controlTotalsInner.ct11);
		}
		x1000CallLifacmv();
		/*    Fund investment.*/
		lifacmvrec.origamt.set(hitraloIO.getFundAmount());
		lifacmvrec.origcurr.set(hitraloIO.getFundCurrency());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(7);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
		}
		else {
			t5645Ix.set(14);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		contotrec.totno.set(controlTotalsInner.ct12);
		x1000CallLifacmv();
	}

protected void acctForCurrConversion5650()
	{
		curr5651();
	}

protected void curr5651()
	{
		/*    Post the fund amount in its currency. (LIFACMV will look*/
		/*    T3629 to get the rate to convert the ORIGAMT into the*/
		/*    ACCTAMT.)*/
		lifacmvrec.origcurr.set(hitraloIO.getFundCurrency());
		lifacmvrec.origamt.set(hitraloIO.getFundAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(2);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
		}
		else {
			t5645Ix.set(9);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		contotrec.totno.set(controlTotalsInner.ct13);
		x1000CallLifacmv();
		/*    Post the Contract amount in its currency. (ditto).*/
		lifacmvrec.origcurr.set(hitraloIO.getCntcurr());
		lifacmvrec.origamt.set(hitraloIO.getContractAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(3);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
		}
		else {
			t5645Ix.set(10);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		contotrec.totno.set(controlTotalsInner.ct14);
		x1000CallLifacmv();
	}

protected void callTrigger5700()
	{
		callTrigger5710();
	}

protected void callTrigger5710()
	{
		udtrigrec.function.set(wsaaProg);
		udtrigrec.statuz.set(varcom.oK);
		udtrigrec.mode.set("BATCH");
		udtrigrec.callingProg.set(wsaaProg);
		udtrigrec.fsuco.set(bsprIO.getFsuco());
		udtrigrec.language.set(bsscIO.getLanguage());
		udtrigrec.batchkey.set(batcdorrec.batchkey);
		udtrigrec.effdate.set(bsscIO.getEffectiveDate());
		udtrigrec.chdrcoy.set(hitraloIO.getChdrcoy());
		udtrigrec.chdrnum.set(hitraloIO.getChdrnum());
		udtrigrec.life.set(hitraloIO.getLife());
		udtrigrec.coverage.set(hitraloIO.getCoverage());
		udtrigrec.rider.set(hitraloIO.getRider());
		udtrigrec.planSuffix.set(hitraloIO.getPlanSuffix());
		udtrigrec.unitVirtualFund.set(hitraloIO.getZintbfnd());
		udtrigrec.tranno.set(hitraloIO.getTranno());
		udtrigrec.procSeqNo.set(hitraloIO.getProcSeqNo());
		udtrigrec.triggerKey.set(hitraloIO.getTriggerKey());
		udtrigrec.user.set(wsaaUser);
		callProgram(hitraloIO.getTriggerModule(), udtrigrec.udtrigrecRec);
		if (isNE(udtrigrec.statuz,varcom.oK)) {
			syserrrec.params.set(udtrigrec.udtrigrecRec);
			syserrrec.statuz.set(udtrigrec.statuz);
			fatalError600();
		}
		if (isNE(hitraloIO.getTriggerKey(),udtrigrec.triggerKey)) {
			syserrrec.params.set(udtrigrec.udtrigrecRec);
			syserrrec.statuz.set(udtrigrec.statuz);
			fatalError600();
		}
		/*    Control total for number of Trigger Processing HITRs*/
		contotrec.totno.set(controlTotalsInner.ct15);
		contotrec.totval.set(1);
		callContot001();
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void x1000CallLifacmv()
	{
			x1010Call();
		}

protected void x1010Call()
	{
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(hitraloIO.getPlanSuffix());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hitraloIO.getChdrnum());
			stringVariable1.addExpression(hitraloIO.getLife());
			stringVariable1.addExpression(hitraloIO.getCoverage());
			stringVariable1.addExpression(hitraloIO.getRider());
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
		}
		else {
			lifacmvrec.rldgacct.set(hitraloIO.getChdrnum());
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		lifacmvrec.rdocnum.set(hitraloIO.getChdrnum());
		lifacmvrec.tranno.set(hitraloIO.getTranno());
		lifacmvrec.tranref.set(hitraloIO.getTranno());
		lifacmvrec.effdate.set(hitraloIO.getEffdate());
		lifacmvrec.substituteCode[1].set(hitraloIO.getCnttyp());
		lifacmvrec.substituteCode[2].set(hitraloIO.getZintbfnd());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaJrnseq.add(1);
		/*    Each invocation of this section will have set the control*/
		/*    total which will be used to log the accounting amount.*/
		/*    Simply set the appropriate sign and call contot.*/
		if (isEQ(lifacmvrec.glsign,"-")) {
			compute(contotrec.totval, 2).set(sub(ZERO,lifacmvrec.origamt));
		}
		else {
			contotrec.totval.set(lifacmvrec.origamt);
		}
		callContot001();
		/*    Log number of ACMVs written.*/
		contotrec.totno.set(controlTotalsInner.ct08);
		contotrec.totval.set(1);
		callContot001();
	}

protected void x2000CallXcvrt()
	{
		/*X2010-CALL-XCVRT*/
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(bsprIO.getCompany());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/*X2090-EXIT*/
	}
/*
 * Class transformed  from Data Structure CONTROL-TOTALS--INNER
 */
private static final class ControlTotalsInner { 
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
	private ZonedDecimalData ct13 = new ZonedDecimalData(2, 0).init(13).setUnsigned();
	private ZonedDecimalData ct14 = new ZonedDecimalData(2, 0).init(14).setUnsigned();
	private ZonedDecimalData ct15 = new ZonedDecimalData(2, 0).init(15).setUnsigned();
	private ZonedDecimalData ct16 = new ZonedDecimalData(2, 0).init(15).setUnsigned();
}
}
