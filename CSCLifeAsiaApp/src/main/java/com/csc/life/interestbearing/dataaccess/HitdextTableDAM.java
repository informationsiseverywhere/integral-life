package com.csc.life.interestbearing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HitdextTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:48
 * Class transformed from HITDEXT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HitdextTableDAM extends HitdpfTableDAM {

	public HitdextTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HITDEXT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", ZINTBFND"
		             + ", ZNXTINTDTE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "ZINTBFND, " +
		            "TRANNO, " +
		            "EFFDATE, " +
		            "VALIDFLAG, " +
		            "ZLSTINTDTE, " +
		            "ZNXTINTDTE, " +
		            "ZLSTFNDVAL, " +
		            "ZLSTSMTDT, " +
		            "ZLSTSMTNO, " +
		            "ZLSTSMTBAL, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "ZINTBFND ASC, " +
		            "ZNXTINTDTE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "ZINTBFND DESC, " +
		            "ZNXTINTDTE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               zintbfnd,
                               tranno,
                               effdate,
                               validflag,
                               zlstintdte,
                               znxtintdte,
                               zlstfndval,
                               zlstsmtdt,
                               zlstsmtno,
                               zlstsmtbal,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getZintbfnd().toInternal()
					+ getZnxtintdte().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, zintbfnd);
			what = ExternalData.chop(what, znxtintdte);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller12 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller7.setInternal(zintbfnd.toInternal());
	nonKeyFiller12.setInternal(znxtintdte.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(113);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ nonKeyFiller7.toInternal()
					+ getTranno().toInternal()
					+ getEffdate().toInternal()
					+ getValidflag().toInternal()
					+ getZlstintdte().toInternal()
					+ nonKeyFiller12.toInternal()
					+ getZlstfndval().toInternal()
					+ getZlstsmtdt().toInternal()
					+ getZlstsmtno().toInternal()
					+ getZlstsmtbal().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, zlstintdte);
			what = ExternalData.chop(what, nonKeyFiller12);
			what = ExternalData.chop(what, zlstfndval);
			what = ExternalData.chop(what, zlstsmtdt);
			what = ExternalData.chop(what, zlstsmtno);
			what = ExternalData.chop(what, zlstsmtbal);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getZintbfnd() {
		return zintbfnd;
	}
	public void setZintbfnd(Object what) {
		zintbfnd.set(what);
	}
	public PackedDecimalData getZnxtintdte() {
		return znxtintdte;
	}
	public void setZnxtintdte(Object what) {
		setZnxtintdte(what, false);
	}
	public void setZnxtintdte(Object what, boolean rounded) {
		if (rounded)
			znxtintdte.setRounded(what);
		else
			znxtintdte.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getZlstintdte() {
		return zlstintdte;
	}
	public void setZlstintdte(Object what) {
		setZlstintdte(what, false);
	}
	public void setZlstintdte(Object what, boolean rounded) {
		if (rounded)
			zlstintdte.setRounded(what);
		else
			zlstintdte.set(what);
	}	
	public PackedDecimalData getZlstfndval() {
		return zlstfndval;
	}
	public void setZlstfndval(Object what) {
		setZlstfndval(what, false);
	}
	public void setZlstfndval(Object what, boolean rounded) {
		if (rounded)
			zlstfndval.setRounded(what);
		else
			zlstfndval.set(what);
	}	
	public PackedDecimalData getZlstsmtdt() {
		return zlstsmtdt;
	}
	public void setZlstsmtdt(Object what) {
		setZlstsmtdt(what, false);
	}
	public void setZlstsmtdt(Object what, boolean rounded) {
		if (rounded)
			zlstsmtdt.setRounded(what);
		else
			zlstsmtdt.set(what);
	}	
	public PackedDecimalData getZlstsmtno() {
		return zlstsmtno;
	}
	public void setZlstsmtno(Object what) {
		setZlstsmtno(what, false);
	}
	public void setZlstsmtno(Object what, boolean rounded) {
		if (rounded)
			zlstsmtno.setRounded(what);
		else
			zlstsmtno.set(what);
	}	
	public PackedDecimalData getZlstsmtbal() {
		return zlstsmtbal;
	}
	public void setZlstsmtbal(Object what) {
		setZlstsmtbal(what, false);
	}
	public void setZlstsmtbal(Object what, boolean rounded) {
		if (rounded)
			zlstsmtbal.setRounded(what);
		else
			zlstsmtbal.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		zintbfnd.clear();
		znxtintdte.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		nonKeyFiller7.clear();
		tranno.clear();
		effdate.clear();
		validflag.clear();
		zlstintdte.clear();
		nonKeyFiller12.clear();
		zlstfndval.clear();
		zlstsmtdt.clear();
		zlstsmtno.clear();
		zlstsmtbal.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}