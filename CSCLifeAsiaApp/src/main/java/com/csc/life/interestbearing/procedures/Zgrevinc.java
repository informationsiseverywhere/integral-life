/*
 * File: Zgrevinc.java
 * Date: 30 August 2009 2:55:23
 * Author: Quipoz Limited
 * 
 * Class transformed from ZGREVINC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitdrevTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.Hitrrv2TableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Generic I/B Interest Calculation Reversal Subroutine.
*
* This is a new subroutine which forms part of Interest Bearing
* Calculation Reversals. It is called upon via T5671 for any
* transactions that create interest bearing interest records
* (i.e. Lapse, Full Surrender, Maturity, Expiry, Death Claim,
* Part Surrender, Fund Switching, Cancel From Inception, Paid-up
* and Interest Bearing Interest Calculation).
*
* It will be called using the standard generic reversal linkage
* copybook, GREVERSREC.
*
* It will delete all HITD records created by the forward
* transaction and reinstate the previous validflag '2' records.
* It will search the HITR file for any records for which interest
* was calculated and set the interest applied indicator back to
* space so that they will be picked up again on the next run of
* the interest calculation bacth job.
*
* This subroutine is performed at component level.
*
***********************************************************************
* </pre>
*/
public class Zgrevinc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ZGREVINC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String hitdrec = "HITDREC";
	private String hitdrevrec = "HITDREVREC";
	private String hitrrec = "HITRREC";
	private String hitrrv2rec = "HITRRV2REC";
	private Greversrec greversrec = new Greversrec();
		/*Interestr Bearing Fund Interest Details*/
	private HitdTableDAM hitdIO = new HitdTableDAM();
		/*I/B fund interest details Reversal*/
	private HitdrevTableDAM hitdrevIO = new HitdrevTableDAM();
		/*Interest Bearing Transaction Details*/
	private HitrTableDAM hitrIO = new HitrTableDAM();
		/*I/B Tran details keyed on Interest Date*/
	private Hitrrv2TableDAM hitrrv2IO = new Hitrrv2TableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next2080
	}

	public Zgrevinc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		greversrec.statuz.set(varcom.oK);
		hitdrevIO.setParams(SPACES);
		hitdrevIO.setChdrcoy(greversrec.chdrcoy);
		hitdrevIO.setChdrnum(greversrec.chdrnum);
		hitdrevIO.setLife(greversrec.life);
		hitdrevIO.setCoverage(greversrec.coverage);
		hitdrevIO.setRider(greversrec.rider);
		hitdrevIO.setPlanSuffix(greversrec.planSuffix);
		hitdrevIO.setTranno(greversrec.tranno);
		hitdrevIO.setFormat(hitdrevrec);
		hitdrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitdrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitdrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX","TRANNO");
		SmartFileCode.execute(appVars, hitdrevIO);
		if (isNE(hitdrevIO.getStatuz(),varcom.oK)
		&& isNE(hitdrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitdrevIO.getParams());
			dbError580();
		}
		if (isNE(greversrec.chdrcoy,hitdrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitdrevIO.getChdrnum())
		|| isNE(greversrec.life,hitdrevIO.getLife())
		|| isNE(greversrec.coverage,hitdrevIO.getCoverage())
		|| isNE(greversrec.rider,hitdrevIO.getRider())
		|| isNE(greversrec.planSuffix,hitdrevIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitdrevIO.getTranno())) {
			hitdrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hitdrevIO.getStatuz(),varcom.endp))) {
			reverseHitds2000();
		}
		
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void reverseHitds2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					hitd2010();
				}
				case next2080: {
					next2080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void hitd2010()
	{
		checkProcessedHitr3000();
		hitdIO.setParams(SPACES);
		hitdIO.setRrn(hitdrevIO.getRrn());
		hitdIO.setFormat(hitdrec);
		hitdIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			dbError580();
		}
		hitdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			dbError580();
		}
		hitdIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)
		&& isNE(hitdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitdIO.getParams());
			dbError580();
		}
		if (isNE(hitdIO.getChdrcoy(),hitdrevIO.getChdrcoy())
		|| isNE(hitdIO.getChdrnum(),hitdrevIO.getChdrnum())
		|| isNE(hitdIO.getCoverage(),hitdrevIO.getCoverage())
		|| isNE(hitdIO.getLife(),hitdrevIO.getLife())
		|| isNE(hitdIO.getRider(),hitdrevIO.getRider())
		|| isNE(hitdIO.getPlanSuffix(),hitdrevIO.getPlanSuffix())
		|| isNE(hitdIO.getZintbfnd(),hitdrevIO.getZintbfnd())
		|| isEQ(hitdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.next2080);
		}
		if (isNE(hitdIO.getValidflag(),"2")) {
			syserrrec.params.set(hitdIO.getParams());
			dbError580();
		}
		hitdIO.setValidflag("1");
		hitdIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			dbError580();
		}
	}

protected void next2080()
	{
		hitdrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitdrevIO);
		if (isNE(hitdrevIO.getStatuz(),varcom.oK)
		&& isNE(hitdrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitdrevIO.getParams());
			dbError580();
		}
		if (isNE(greversrec.chdrcoy,hitdrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitdrevIO.getChdrnum())
		|| isNE(greversrec.life,hitdrevIO.getLife())
		|| isNE(greversrec.coverage,hitdrevIO.getCoverage())
		|| isNE(greversrec.rider,hitdrevIO.getRider())
		|| isNE(greversrec.planSuffix,hitdrevIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitdrevIO.getTranno())) {
			hitdrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkProcessedHitr3000()
	{
		hitr3010();
	}

protected void hitr3010()
	{
		hitrrv2IO.setParams(SPACES);
		hitrrv2IO.setChdrcoy(hitdrevIO.getChdrcoy());
		hitrrv2IO.setChdrnum(hitdrevIO.getChdrnum());
		hitrrv2IO.setLife(hitdrevIO.getLife());
		hitrrv2IO.setCoverage(hitdrevIO.getCoverage());
		hitrrv2IO.setRider(hitdrevIO.getRider());
		hitrrv2IO.setPlanSuffix(hitdrevIO.getPlanSuffix());
		hitrrv2IO.setZlstintdte(hitdrevIO.getZlstintdte());
		hitrrv2IO.setFormat(hitrrv2rec);
		hitrrv2IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrv2IO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, hitrrv2IO);
		if (isNE(hitrrv2IO.getStatuz(),varcom.oK)
		&& isNE(hitrrv2IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrv2IO.getParams());
			dbError580();
		}
		if (isNE(hitrrv2IO.getChdrcoy(),hitdrevIO.getChdrcoy())
		|| isNE(hitrrv2IO.getChdrnum(),hitdrevIO.getChdrnum())
		|| isNE(hitrrv2IO.getLife(),hitdrevIO.getLife())
		|| isNE(hitrrv2IO.getCoverage(),hitdrevIO.getCoverage())
		|| isNE(hitrrv2IO.getRider(),hitdrevIO.getRider())
		|| isNE(hitrrv2IO.getPlanSuffix(),hitdrevIO.getPlanSuffix())
		|| isNE(hitrrv2IO.getZlstintdte(),hitdrevIO.getZlstintdte())
		|| isEQ(hitrrv2IO.getStatuz(),varcom.endp)) {
			hitrrv2IO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hitrrv2IO.getStatuz(),varcom.endp))) {
			reverseHitrs3100();
		}
		
	}

protected void reverseHitrs3100()
	{
		hitr3110();
	}

protected void hitr3110()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setRrn(hitrrv2IO.getRrn());
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			dbError580();
		}
		hitrIO.setZintappind(SPACES);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZintrate(ZERO);
		hitrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			dbError580();
		}
		hitrrv2IO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrrv2IO);
		if (isNE(hitrrv2IO.getStatuz(),varcom.oK)
		&& isNE(hitrrv2IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrv2IO.getParams());
			dbError580();
		}
		if (isNE(hitrrv2IO.getChdrcoy(),hitdrevIO.getChdrcoy())
		|| isNE(hitrrv2IO.getChdrnum(),hitdrevIO.getChdrnum())
		|| isNE(hitrrv2IO.getLife(),hitdrevIO.getLife())
		|| isNE(hitrrv2IO.getCoverage(),hitdrevIO.getCoverage())
		|| isNE(hitrrv2IO.getRider(),hitdrevIO.getRider())
		|| isNE(hitrrv2IO.getPlanSuffix(),hitdrevIO.getPlanSuffix())
		|| isNE(hitrrv2IO.getZlstintdte(),hitdrevIO.getZlstintdte())
		|| isEQ(hitrrv2IO.getStatuz(),varcom.endp)) {
			hitrrv2IO.setStatuz(varcom.endp);
		}
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
