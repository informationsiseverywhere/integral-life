package com.csc.life.interestbearing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH517.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh517Report extends SMARTReportLayout { 

	private FixedLengthStringData batctrcde = new FixedLengthStringData(4);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private ZonedDecimalData convrate = new ZonedDecimalData(13, 4);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3);
	private ZonedDecimalData fundvalb = new ZonedDecimalData(18, 5);
	private ZonedDecimalData fundvalc = new ZonedDecimalData(18, 5);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private ZonedDecimalData prcntdiff = new ZonedDecimalData(6, 3);
	private ZonedDecimalData prcseq = new ZonedDecimalData(3, 0);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private RPGTimeData time = new RPGTimeData();
	private FixedLengthStringData zintbfnd = new FixedLengthStringData(4);
	private ZonedDecimalData ztranval = new ZonedDecimalData(17, 2);
	private ZonedDecimalData zvaldiff = new ZonedDecimalData(15, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh517Report() {
		super();
	}


	/**
	 * Print the XML for Rh517dd1
	 */
	public void printRh517dd1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 15, 4));
		batctrcde.setFieldName("batctrcde");
		batctrcde.setInternal(subString(recordData, 19, 4));
		prcseq.setFieldName("prcseq");
		prcseq.setInternal(subString(recordData, 23, 3));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 26, 10));
		ztranval.setFieldName("ztranval");
		ztranval.setInternal(subString(recordData, 36, 17));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 53, 3));
		convrate.setFieldName("convrate");
		convrate.setInternal(subString(recordData, 56, 13));
		printLayout("Rh517dd1",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				plnsfx,
				batctrcde,
				prcseq,
				effdate,
				ztranval,
				cntcurr,
				convrate
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh517dh1
	 */
	public void printRh517dh1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		zintbfnd.setFieldName("zintbfnd");
		zintbfnd.setInternal(subString(recordData, 32, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 36, 30));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 66, 3));
		curdesc.setFieldName("curdesc");
		curdesc.setInternal(subString(recordData, 69, 30));
		printLayout("Rh517dh1",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr,
				zintbfnd,
				longdesc,
				fndcurr,
				curdesc
			}
		);

		currentPrintLine.set(13);
	}

	/**
	 * Print the XML for Rh517dt1
	 */
	public void printRh517dt1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		fundvalb.setFieldName("fundvalb");
		fundvalb.setInternal(subString(recordData, 1, 18));
		fundvalc.setFieldName("fundvalc");
		fundvalc.setInternal(subString(recordData, 19, 18));
		zvaldiff.setFieldName("zvaldiff");
		zvaldiff.setInternal(subString(recordData, 37, 15));
		prcntdiff.setFieldName("prcntdiff");
		prcntdiff.setInternal(subString(recordData, 52, 6));
		printLayout("Rh517dt1",			// Record name
			new BaseData[]{			// Fields:
				fundvalb,
				fundvalc,
				zvaldiff,
				prcntdiff
			}
		);

		currentPrintLine.add(7);
	}

	/**
	 * Print the XML for Rh517e01
	 */
	public void printRh517e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rh517e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh517e02
	 */
	public void printRh517e02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rh517e02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh517sd1
	 */
	public void printRh517sd1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zintbfnd.setFieldName("zintbfnd");
		zintbfnd.setInternal(subString(recordData, 1, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 5, 30));
		ztranval.setFieldName("ztranval");
		ztranval.setInternal(subString(recordData, 35, 17));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 52, 3));
		prcntdiff.setFieldName("prcntdiff");
		prcntdiff.setInternal(subString(recordData, 55, 6));
		printLayout("Rh517sd1",			// Record name
			new BaseData[]{			// Fields:
				zintbfnd,
				longdesc,
				ztranval,
				fndcurr,
				prcntdiff
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh517sh1
	 */
	public void printRh517sh1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rh517sh1",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(13);
	}


}
