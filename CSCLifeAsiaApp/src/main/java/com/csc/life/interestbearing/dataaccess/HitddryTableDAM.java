package com.csc.life.interestbearing.dataaccess;

import com.csc.life.interestbearing.dataaccess.HitdpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * File: HitddryTableDAM.java
 * Date: Thu, 21 May 2015 15:20:25
 * Author: CSCV Integral
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
 
public class HitddryTableDAM extends HitdpfTableDAM {

	public HitddryTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HITDDRY");
	}
	
	public String getTable() {
		return TABLE;
	}
	
	public void setKeyConstants() {
		
		KEYCOLUMNS = ""
					 +  "CHDRCOY"
					 + ", CHDRNUM"
					 + ", LIFE"
					 + ", COVERAGE"
					 + ", RIDER"
					 + ", PLNSFX"
					 + ", ZINTBFND";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "ZINTBFND, " +
		            "TRANNO, " +
		            "EFFDATE, " +
					"VALIDFLAG, " +
					"ZLSTINTDTE, " +
					"ZNXTINTDTE, " +
					"ZLSTFNDVAL, " +
					"ZLSTSMTDT, " +
					"ZLSTSMTNO, " +
					"ZLSTSMTBAL, " +
					"JOBNM, " +
					"USRPRF, " +										
					"DATIME, " +
					"UNIQUE_NUMBER";
					
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +									
		            "LIFE ASC, " +									
		            "COVERAGE ASC, " +									
		            "RIDER ASC, " +									
		            "PLNSFX ASC, " +									
		            "ZINTBFND ASC, " +									
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +									
		            "LIFE DESC, " +									
		            "COVERAGE DESC, " +									
		            "RIDER DESC, " +									
		            "PLNSFX DESC, " +									
		            "ZINTBFND DESC, " +									
					"UNIQUE_NUMBER ASC";
		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
									chdrcoy,
									chdrnum,
									life,
									coverage,
									rider,
									planSuffix,
									zintbfnd,
									tranno,
									effdate,
									validflag,
									zlstintdte,
									znxtintdte,
									zlstfndval,
									zlstsmtdt,
									zlstsmtno,
									zlstsmtbal,
									jobName,
									userProfile,									
									datime,
									unique_number
		                       };
		
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(42);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()					
					+ getCoverage().toInternal()					
					+ getRider().toInternal()					
					+ getPlanSuffix().toInternal()					
					+ getZintbfnd().toInternal()					
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, zintbfnd);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(4);
	
	
	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(planSuffix.toInternal());
	nonKeyFiller70.setInternal(zintbfnd.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(113);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getTranno().toInternal() 
					+ getEffdate().toInternal()
					+ getValidflag().toInternal()
					+ getZlstintdte().toInternal()
					+ getZnxtintdte().toInternal()
					+ getZlstfndval().toInternal()
					+ getZlstsmtdt().toInternal()
					+ getZlstsmtno().toInternal()
					+ getZlstsmtbal().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()										
					+ getDatime().toInternal());
					
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, zlstintdte);
			what = ExternalData.chop(what, zlstfndval);
			what = ExternalData.chop(what, zlstsmtdt);		
			what = ExternalData.chop(what, zlstsmtno);		
			what = ExternalData.chop(what, zlstsmtbal);		
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);							
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getZintbfnd() {
		return zintbfnd;
	}
	public void setZintbfnd(Object what) {
		zintbfnd.set(what);
	}	
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getZlstintdte() {
		return zlstintdte;
	}
	public void setZlstintdte(Object what) {
		setZlstintdte(what, false);
	}
	public void setZlstintdte(Object what, boolean rounded) {
		if (rounded)
			zlstintdte.setRounded(what);
		else
			zlstintdte.set(what);
	}	
	public PackedDecimalData getZnxtintdte() {
		return znxtintdte;
	}
	public void setZnxtintdte(Object what) {
		setZnxtintdte(what, false);
	}
	public void setZnxtintdte(Object what, boolean rounded) {
		if (rounded)
			znxtintdte.setRounded(what);
		else
			znxtintdte.set(what);
	}	
	public PackedDecimalData getZlstfndval() {
		return zlstfndval;
	}
	public void setZlstfndval(Object what) {
		setZlstfndval(what, false);
	}
	public void setZlstfndval(Object what, boolean rounded) {
		if (rounded)
			zlstfndval.setRounded(what);
		else
			zlstfndval.set(what);
	}	
	public PackedDecimalData getZlstsmtdt() {
		return zlstsmtdt;
	}
	public void setZlstsmtdt(Object what) {
		setZlstsmtdt(what, false);
	}
	public void setZlstsmtdt(Object what, boolean rounded) {
		if (rounded)
			zlstsmtdt.setRounded(what);
		else
			zlstsmtdt.set(what);
	}	
	public PackedDecimalData getZlstsmtno() {
		return zlstsmtno;
	}
	public void setZlstsmtno(Object what) {
		setZlstsmtno(what, false);
	}
	public void setZlstsmtno(Object what, boolean rounded) {
		if (rounded)
			zlstsmtno.setRounded(what);
		else
			zlstsmtno.set(what);
	}	
	public PackedDecimalData getZlstsmtbal() {
		return zlstsmtbal;
	}
	public void setZlstsmtbal(Object what) {
		setZlstsmtbal(what, false);
	}
	public void setZlstsmtbal(Object what, boolean rounded) {
		if (rounded)
			zlstsmtbal.setRounded(what);
		else
			zlstsmtbal.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		zintbfnd.clear();		
		keyFiller.clear();
		
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		tranno.clear();
		effdate.clear();
		validflag.clear();
		zlstintdte.clear();
		znxtintdte.clear();
		zlstfndval.clear();
		zlstsmtdt.clear();
		zlstsmtno.clear();
		zlstsmtbal.clear();
		jobName.clear();
		userProfile.clear();		
		datime.clear();		
	}


}