/******************************************************************************
 * File Name 		: HifdpfDAOImpl.java
 * Author			: CSC
 * Creation Date	: 22 May 2017
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for HIFDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			| *											| *				  |
 * ***************************************************************************/
package com.csc.life.interestbearing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.interestbearing.dataaccess.dao.HifdpfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hifdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * The Class Hifdpf which contains the DAO implementation methods for the table HIFDPF.
 */
public class HifdpfDAOImpl extends BaseDAOImpl<Hifdpf> implements HifdpfDAO {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(HifdpfDAOImpl.class);

	/**
	 * Fetches records from HIFDPF for the given Criteria.
	 */
	public Hifdpf searchHifdpfRecord(Hifdpf hifdpf) throws SQLRuntimeException{
		StringBuilder sql = new StringBuilder("SELECT CHDRCOY, ZINTBFND, UNIQUE_NUMBER, ZTOTFNDVAL FROM HIFD WHERE CHDRCOY=? AND ZINTBFND=? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hifdpf hifdpfObj = null;
		try {
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, hifdpf.getChdrcoy());
			ps.setString(2, hifdpf.getZintbfnd());
			rs = ps.executeQuery();
			if(rs.next()){
				hifdpfObj = new Hifdpf();
				hifdpfObj.setChdrcoy(rs.getString("CHDRCOY"));
				hifdpfObj.setZintbfnd(rs.getString("ZINTBFND"));
				hifdpfObj.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				hifdpfObj.setZtotfndval(rs.getBigDecimal("ZTOTFNDVAL"));
			}
		} catch (SQLException e) {
			LOGGER.error("searchHifdpfRecord() ", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps ,null);
		}
		return hifdpfObj;
	}


	public void bulkInsertHifd(List<Hifdpf> hifdpfList) throws SQLRuntimeException{
		StringBuilder sql = new StringBuilder("INSERT INTO HIFD (CHDRCOY, ZINTBFND, ZTOTFNDVAL, USRPRF, JOBNM, DATIME )");
		sql.append(" VALUES ( ?, ?, ?, ?, ?, ?)");
		PreparedStatement psHifdpfInsert = getPrepareStatement(sql.toString());
		try {
			for(Hifdpf hifdpf : hifdpfList){
				psHifdpfInsert.setString(1, hifdpf.getChdrcoy());
				psHifdpfInsert.setString(2, hifdpf.getZintbfnd());
				psHifdpfInsert.setBigDecimal(3, hifdpf.getZtotfndval());
				psHifdpfInsert.setString(4, getUsrprf());
				psHifdpfInsert.setString(5, getJobnm());
				psHifdpfInsert.setTimestamp(6, getDatime());
				psHifdpfInsert.addBatch();
			}
			psHifdpfInsert.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("bulkInsertHifd()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHifdpfInsert, null);
		}
	}

	@Override
	public void bulkUpdateHifdp(List<Hifdpf> hifdpfList) throws SQLRuntimeException {
		StringBuilder sql = new StringBuilder("UPDATE HIFD SET ZTOTFNDVAL=? WHERE UNIQUE_NUMBER=?");
		PreparedStatement psHifdpfInsert = getPrepareStatement(sql.toString());
		try {
			for(Hifdpf hifdpf : hifdpfList){
				psHifdpfInsert.setBigDecimal(1, hifdpf.getZtotfndval());
				psHifdpfInsert.setLong(2, hifdpf.getUniqueNumber());
				psHifdpfInsert.addBatch();
			}
			psHifdpfInsert.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("bulkUpdateHifdp()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHifdpfInsert, null);
		}
	}
}
