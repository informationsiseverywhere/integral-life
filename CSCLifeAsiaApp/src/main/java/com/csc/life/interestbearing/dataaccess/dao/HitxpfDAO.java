package com.csc.life.interestbearing.dataaccess.dao;

import java.util.List;

import com.csc.life.interestbearing.dataaccess.model.Hitxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HitxpfDAO extends BaseDAO<Hitxpf> {
	
	public List<Hitxpf> searchHitxpfRecord(String tableId, String memName, int batchExtractSize, int batchID);

}
