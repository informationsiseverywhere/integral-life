/*
 * File: Bh508.java
 * Date: 29 August 2009 21:26:42
 * Author: Quipoz Limited
 * 
 * Class transformed from BH508.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.interestbearing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.interestbearing.dataaccess.HitrextTableDAM;
import com.csc.life.interestbearing.dataaccess.HitxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is a Transaction Splitter Program to be run before the
* Interest Bearing Fund Dealing program (BH509) in Unit Deal.
*
* It has three functions which will invoke separate SQL Select
* statements. The functions are found in system param 01.
*
* DEAL: This function will extract all undealt HITRs of any type.
*       (i.e any HITR with a feedback indicator equal to spaces.)
*
* DEBT: This function will extract only unprocessed HITRs which
*       have been created by the Coverage Debt Settlement (B5104)
*       program. (i.e. feedback indicator equal to spaces and a
*       coverage debt indicator set to "D".)
*
* SWCH: This function will pick up undealt HITRs which have been
*       created as part of a fund switch.(i.e. feedback indicator
*       equal to spaces and switch indicator equal to 'Y'.
*
* Notes on Splitter Programs.
*
* Splitter programs should be simple. They should only deal
* with a single file. If there is insufficient data on the
* primary file to completely identify a transaction then
* further editing should be done in the subsequent process.
* The objective of a splitter is to rapidily isolate potential
* transactions, not process the transaction. The primary concern
* for splitter program design is elasped time speed and this is
* achieved by minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first
* two characters of the 4th system parameter and 9999 is the
* schedule run number. Each member added to the temporary file
* will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* It is important that the process which runs the splitter
* program has the same 'number of subsequent threads' as the
* following process has defined in 'number of threads this
* process.'
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
***********************************************************************
* </pre>
*/
public class Bh508 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqldealCursorrs = null;
	private java.sql.PreparedStatement sqldealCursorps = null;
	private java.sql.Connection sqldealCursorconn = null;
	private String sqldealCursor = "";
	private java.sql.ResultSet sqldebtCursorrs = null;
	private java.sql.PreparedStatement sqldebtCursorps = null;
	private java.sql.Connection sqldebtCursorconn = null;
	private String sqldebtCursor = "";
	private java.sql.ResultSet sqlswchCursorrs = null;
	private java.sql.PreparedStatement sqlswchCursorps = null;
	private java.sql.Connection sqlswchCursorconn = null;
	private String sqlswchCursor = "";
	private int dealCursorLoopIndex = 0;
	private int debtCursorLoopIndex = 0;
	private int swchCursorLoopIndex = 0;
	private HitxpfTableDAM hitxpf = new HitxpfTableDAM();
	private DiskFileDAM hitx01 = new DiskFileDAM("HITX01");
	private DiskFileDAM hitx02 = new DiskFileDAM("HITX02");
	private DiskFileDAM hitx03 = new DiskFileDAM("HITX03");
	private DiskFileDAM hitx04 = new DiskFileDAM("HITX04");
	private DiskFileDAM hitx05 = new DiskFileDAM("HITX05");
	private DiskFileDAM hitx06 = new DiskFileDAM("HITX06");
	private DiskFileDAM hitx07 = new DiskFileDAM("HITX07");
	private DiskFileDAM hitx08 = new DiskFileDAM("HITX08");
	private DiskFileDAM hitx09 = new DiskFileDAM("HITX09");
	private DiskFileDAM hitx10 = new DiskFileDAM("HITX10");
	private DiskFileDAM hitx11 = new DiskFileDAM("HITX11");
	private DiskFileDAM hitx12 = new DiskFileDAM("HITX12");
	private DiskFileDAM hitx13 = new DiskFileDAM("HITX13");
	private DiskFileDAM hitx14 = new DiskFileDAM("HITX14");
	private DiskFileDAM hitx15 = new DiskFileDAM("HITX15");
	private DiskFileDAM hitx16 = new DiskFileDAM("HITX16");
	private DiskFileDAM hitx17 = new DiskFileDAM("HITX17");
	private DiskFileDAM hitx18 = new DiskFileDAM("HITX18");
	private DiskFileDAM hitx19 = new DiskFileDAM("HITX19");
	private DiskFileDAM hitx20 = new DiskFileDAM("HITX20");
	private HitxpfTableDAM hitxpfData = new HitxpfTableDAM();
	private FixedLengthStringData hitx01Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx02Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx03Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx04Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx05Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx06Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx07Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx08Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx09Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx10Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx11Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx12Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx13Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx14Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx15Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx16Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx17Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx18Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx19Rec = new FixedLengthStringData(75);
	private FixedLengthStringData hitx20Rec = new FixedLengthStringData(75);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH508");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaHitxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHitxFn, 0, FILLER).init("HITX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHitxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHitxFn, 6).setUnsigned();
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSpaces = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaD = new FixedLengthStringData(1).init("D");
	private FixedLengthStringData wsaaY = new FixedLengthStringData(1).init("Y");

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String invf = "INVF";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
		/*Interest Bearing Transactions extract*/
	private HitrextTableDAM hitrextIO = new HitrextTableDAM();
	private P6671par p6671par = new P6671par();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();

	public Bh508() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		if (isEQ(bprdIO.getSystemParam01(),"DEAL")) {
			sqlDeal1200();
		}
		else {
			if (isEQ(bprdIO.getSystemParam01(),"DEBT")) {
				sqlDebt1300();
			}
			else {
				if (isEQ(bprdIO.getSystemParam01(),"SWCH")) {
					sqlSwch1400();
				}
				else {
					syserrrec.syserrType.set("2");
					syserrrec.params.set(bprdIO.getSystemParam01());
					syserrrec.statuz.set(invf);
					fatalError600();
				}
			}
		}
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHitxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HITX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHitxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 50)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			hitx01.openOutput();
		}
		if (isEQ(iz,2)) {
			hitx02.openOutput();
		}
		if (isEQ(iz,3)) {
			hitx03.openOutput();
		}
		if (isEQ(iz,4)) {
			hitx04.openOutput();
		}
		if (isEQ(iz,5)) {
			hitx05.openOutput();
		}
		if (isEQ(iz,6)) {
			hitx06.openOutput();
		}
		if (isEQ(iz,7)) {
			hitx07.openOutput();
		}
		if (isEQ(iz,8)) {
			hitx08.openOutput();
		}
		if (isEQ(iz,9)) {
			hitx09.openOutput();
		}
		if (isEQ(iz,10)) {
			hitx10.openOutput();
		}
		if (isEQ(iz,11)) {
			hitx11.openOutput();
		}
		if (isEQ(iz,12)) {
			hitx12.openOutput();
		}
		if (isEQ(iz,13)) {
			hitx13.openOutput();
		}
		if (isEQ(iz,14)) {
			hitx14.openOutput();
		}
		if (isEQ(iz,15)) {
			hitx15.openOutput();
		}
		if (isEQ(iz,16)) {
			hitx16.openOutput();
		}
		if (isEQ(iz,17)) {
			hitx17.openOutput();
		}
		if (isEQ(iz,18)) {
			hitx18.openOutput();
		}
		if (isEQ(iz,19)) {
			hitx19.openOutput();
		}
		if (isEQ(iz,20)) {
			hitx20.openOutput();
		}
	}

protected void sqlDeal1200()
	{
		/*SQL-DEAL*/
		sqldealCursor = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ZINTBFND, TRANNO, ZRECTYP, BATCTRCDE, EFFDATE, CNTCURR, FDBKIND, COVDBTIND, CNTAMNT, FUNDAMNT, CNTTYP, PRCSEQ, PERSUR, SWCHIND, FUNDRATE" +
" FROM   " + getAppVars().getTableNameOverriden("HITREXT") + " " +
" WHERE CHDRCOY = ?" +
" AND FDBKIND = ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND EFFDATE <= ?" +
" ORDER BY CHDRCOY, CHDRNUM, PRCSEQ, ZINTBFND, ZRECTYP";
		sqlerrorflag = false;
		try {
			sqldealCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.interestbearing.dataaccess.HitrextTableDAM());
			sqldealCursorps = getAppVars().prepareStatementEmbeded(sqldealCursorconn, sqldealCursor, "HITREXT");
			getAppVars().setDBString(sqldealCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqldealCursorps, 2, wsaaSpaces);
			getAppVars().setDBString(sqldealCursorps, 3, wsaaChdrnumFrom);
			getAppVars().setDBString(sqldealCursorps, 4, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqldealCursorps, 5, wsaaEffectiveDate);
			sqldealCursorrs = getAppVars().executeQuery(sqldealCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void sqlDebt1300()
	{
		/*SQL-DEBT*/
		sqldebtCursor = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ZINTBFND, TRANNO, ZRECTYP, BATCTRCDE, EFFDATE, CNTCURR, FDBKIND, COVDBTIND, CNTAMNT, FUNDAMNT, CNTTYP, PRCSEQ, PERSUR, SWCHIND, FUNDRATE" +
" FROM   " + getAppVars().getTableNameOverriden("HITREXT") + " " +
" WHERE CHDRCOY = ?" +
" AND FDBKIND = ?" +
" AND COVDBTIND = ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND EFFDATE <= ?" +
" ORDER BY CHDRCOY, CHDRNUM, PRCSEQ, ZINTBFND, ZRECTYP";
		sqlerrorflag = false;
		try {
			sqldebtCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.interestbearing.dataaccess.HitrextTableDAM());
			sqldebtCursorps = getAppVars().prepareStatementEmbeded(sqldebtCursorconn, sqldebtCursor, "HITREXT");
			getAppVars().setDBString(sqldebtCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqldebtCursorps, 2, wsaaSpaces);
			getAppVars().setDBString(sqldebtCursorps, 3, wsaaD);
			getAppVars().setDBString(sqldebtCursorps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqldebtCursorps, 5, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqldebtCursorps, 6, wsaaEffectiveDate);
			sqldebtCursorrs = getAppVars().executeQuery(sqldebtCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void sqlSwch1400()
	{
		/*SQL-SWCH*/
		sqlswchCursor = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ZINTBFND, TRANNO, ZRECTYP, BATCTRCDE, EFFDATE, CNTCURR, FDBKIND, COVDBTIND, CNTAMNT, FUNDAMNT, CNTTYP, PRCSEQ, PERSUR, SWCHIND, FUNDRATE" +
" FROM   " + getAppVars().getTableNameOverriden("HITREXT") + " " +
" WHERE CHDRCOY = ?" +
" AND FDBKIND = ?" +
" AND SWCHIND = ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND EFFDATE <= ?" +
" ORDER BY CHDRCOY, CHDRNUM, PRCSEQ, ZINTBFND, ZRECTYP";
		sqlerrorflag = false;
		try {
			sqlswchCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.interestbearing.dataaccess.HitrextTableDAM());
			sqlswchCursorps = getAppVars().prepareStatementEmbeded(sqlswchCursorconn, sqlswchCursor, "HITREXT");
			getAppVars().setDBString(sqlswchCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlswchCursorps, 2, wsaaSpaces);
			getAppVars().setDBString(sqlswchCursorps, 3, wsaaY);
			getAppVars().setDBString(sqlswchCursorps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlswchCursorps, 5, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqlswchCursorps, 6, wsaaEffectiveDate);
			sqlswchCursorrs = getAppVars().executeQuery(sqlswchCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaZintbfnd[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaBatctrcde[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaZrectyp[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCnttyp[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCntcurr[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaFdbkind[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaEffdate[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCntamnt[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaFundamnt[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaPrcseq[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaPersur[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaFundrate[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFiles2010();
		}

protected void readFiles2010()
	{
		/*    Now a block of records is fetched into the array.*/
		/*    Also on the first entry into the program we must set up the*/
		/*    WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		if (isEQ(bprdIO.getSystemParam01(),"DEAL")) {
			sqlerrorflag = false;
			try {
				for (dealCursorLoopIndex = 1; isLTE(dealCursorLoopIndex, wsaaRowsInBlock.toInt())
				&& getAppVars().fetchNext(sqldealCursorrs); dealCursorLoopIndex++ ){
					getAppVars().getDBObject(sqldealCursorrs, 1, wsaaFetchArrayInner.wsaaChdrcoy[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 2, wsaaFetchArrayInner.wsaaChdrnum[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 3, wsaaFetchArrayInner.wsaaLife[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 4, wsaaFetchArrayInner.wsaaCoverage[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 5, wsaaFetchArrayInner.wsaaRider[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 6, wsaaFetchArrayInner.wsaaPlnsfx[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 7, wsaaFetchArrayInner.wsaaZintbfnd[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 8, wsaaFetchArrayInner.wsaaTranno[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 9, wsaaFetchArrayInner.wsaaZrectyp[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 10, wsaaFetchArrayInner.wsaaBatctrcde[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 11, wsaaFetchArrayInner.wsaaEffdate[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 12, wsaaFetchArrayInner.wsaaCntcurr[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 13, wsaaFetchArrayInner.wsaaFdbkind[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 14, wsaaFetchArrayInner.wsaaCovdbtind[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 15, wsaaFetchArrayInner.wsaaCntamnt[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 16, wsaaFetchArrayInner.wsaaFundamnt[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 17, wsaaFetchArrayInner.wsaaCnttyp[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 18, wsaaFetchArrayInner.wsaaPrcseq[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 19, wsaaFetchArrayInner.wsaaPersur[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 20, wsaaFetchArrayInner.wsaaSwchind[dealCursorLoopIndex]);
					getAppVars().getDBObject(sqldealCursorrs, 21, wsaaFetchArrayInner.wsaaFundrate[dealCursorLoopIndex]);
				}
			}
			catch (SQLException ex){
				sqlerrorflag = true;
				getAppVars().setSqlErrorCode(ex);
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		if (isEQ(bprdIO.getSystemParam01(),"DEBT")) {
			sqlerrorflag = false;
			try {
				for (debtCursorLoopIndex = 1; isLTE(debtCursorLoopIndex, wsaaRowsInBlock.toInt())
				&& getAppVars().fetchNext(sqldebtCursorrs); debtCursorLoopIndex++ ){
					getAppVars().getDBObject(sqldebtCursorrs, 1, wsaaFetchArrayInner.wsaaChdrcoy[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 2, wsaaFetchArrayInner.wsaaChdrnum[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 3, wsaaFetchArrayInner.wsaaLife[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 4, wsaaFetchArrayInner.wsaaCoverage[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 5, wsaaFetchArrayInner.wsaaRider[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 6, wsaaFetchArrayInner.wsaaPlnsfx[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 7, wsaaFetchArrayInner.wsaaZintbfnd[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 8, wsaaFetchArrayInner.wsaaTranno[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 9, wsaaFetchArrayInner.wsaaZrectyp[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 10, wsaaFetchArrayInner.wsaaBatctrcde[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 11, wsaaFetchArrayInner.wsaaEffdate[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 12, wsaaFetchArrayInner.wsaaCntcurr[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 13, wsaaFetchArrayInner.wsaaFdbkind[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 14, wsaaFetchArrayInner.wsaaCovdbtind[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 15, wsaaFetchArrayInner.wsaaCntamnt[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 16, wsaaFetchArrayInner.wsaaFundamnt[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 17, wsaaFetchArrayInner.wsaaCnttyp[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 18, wsaaFetchArrayInner.wsaaPrcseq[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 19, wsaaFetchArrayInner.wsaaPersur[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 20, wsaaFetchArrayInner.wsaaSwchind[debtCursorLoopIndex]);
					getAppVars().getDBObject(sqldebtCursorrs, 21, wsaaFetchArrayInner.wsaaFundrate[debtCursorLoopIndex]);
				}
			}
			catch (SQLException ex){
				sqlerrorflag = true;
				getAppVars().setSqlErrorCode(ex);
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		if (isEQ(bprdIO.getSystemParam01(),"SWCH")) {
			sqlerrorflag = false;
			try {
				for (swchCursorLoopIndex = 1; isLTE(swchCursorLoopIndex, wsaaRowsInBlock.toInt())
				&& getAppVars().fetchNext(sqlswchCursorrs); swchCursorLoopIndex++ ){
					getAppVars().getDBObject(sqlswchCursorrs, 1, wsaaFetchArrayInner.wsaaChdrcoy[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 2, wsaaFetchArrayInner.wsaaChdrnum[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 3, wsaaFetchArrayInner.wsaaLife[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 4, wsaaFetchArrayInner.wsaaCoverage[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 5, wsaaFetchArrayInner.wsaaRider[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 6, wsaaFetchArrayInner.wsaaPlnsfx[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 7, wsaaFetchArrayInner.wsaaZintbfnd[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 8, wsaaFetchArrayInner.wsaaTranno[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 9, wsaaFetchArrayInner.wsaaZrectyp[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 10, wsaaFetchArrayInner.wsaaBatctrcde[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 11, wsaaFetchArrayInner.wsaaEffdate[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 12, wsaaFetchArrayInner.wsaaCntcurr[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 13, wsaaFetchArrayInner.wsaaFdbkind[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 14, wsaaFetchArrayInner.wsaaCovdbtind[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 15, wsaaFetchArrayInner.wsaaCntamnt[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 16, wsaaFetchArrayInner.wsaaFundamnt[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 17, wsaaFetchArrayInner.wsaaCnttyp[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 18, wsaaFetchArrayInner.wsaaPrcseq[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 19, wsaaFetchArrayInner.wsaaPersur[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 20, wsaaFetchArrayInner.wsaaSwchind[swchCursorLoopIndex]);
					getAppVars().getDBObject(sqlswchCursorrs, 21, wsaaFetchArrayInner.wsaaFundrate[swchCursorLoopIndex]);
				}
			}
			catch (SQLException ex){
				sqlerrorflag = true;
				getAppVars().setSqlErrorCode(ex);
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		/*    We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*    If either of the above cases occur, then an SQLCODE = +100*/
		/*    is returned.*/
		/*    The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}  else if (isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsspEdterror.set(varcom.endp);
		} 
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*    Re-initialise the block for next fetch and point to the*/
		/*    first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*    If the CHDRNUM being processed is not equal to the previous*/
		/*    one we should move to the next output file member.*/
		/*    The condition is here to allow for checking the last CHDRNUM*/
		/*    of the old block with the first of the new and to move to*/
		/*    the next HITX member to write to if they have changed.*/
		if (isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*    Load from storage all HITX data for the same contract until*/
		/*    the CHDRNUM on HITX has changed or until the end of an*/
		/*    incomplete block is reached.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3201();
	}

protected void start3201()
	{
		hitxpfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
		hitxpfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
		hitxpfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
		hitxpfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
		hitxpfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
		hitxpfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
		hitxpfData.zintbfnd.set(wsaaFetchArrayInner.wsaaZintbfnd[wsaaInd.toInt()]);
		hitxpfData.tranno.set(wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()]);
		hitxpfData.zrectyp.set(wsaaFetchArrayInner.wsaaZrectyp[wsaaInd.toInt()]);
		hitxpfData.batctrcde.set(wsaaFetchArrayInner.wsaaBatctrcde[wsaaInd.toInt()]);
		hitxpfData.effdate.set(wsaaFetchArrayInner.wsaaEffdate[wsaaInd.toInt()]);
		hitxpfData.cntcurr.set(wsaaFetchArrayInner.wsaaCntcurr[wsaaInd.toInt()]);
		hitxpfData.feedbackInd.set(wsaaFetchArrayInner.wsaaFdbkind[wsaaInd.toInt()]);
		hitxpfData.contractAmount.set(wsaaFetchArrayInner.wsaaCntamnt[wsaaInd.toInt()]);
		hitxpfData.fundAmount.set(wsaaFetchArrayInner.wsaaFundamnt[wsaaInd.toInt()]);
		hitxpfData.cnttyp.set(wsaaFetchArrayInner.wsaaCnttyp[wsaaInd.toInt()]);
		hitxpfData.procSeqNo.set(wsaaFetchArrayInner.wsaaPrcseq[wsaaInd.toInt()]);
		hitxpfData.surrenderPercent.set(wsaaFetchArrayInner.wsaaPersur[wsaaInd.toInt()]);
		hitxpfData.fundRate.set(wsaaFetchArrayInner.wsaaFundrate[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy,1)) {
			hitx01.write(hitxpfData);
		}
		if (isEQ(iy,2)) {
			hitx02.write(hitxpfData);
		}
		if (isEQ(iy,3)) {
			hitx03.write(hitxpfData);
		}
		if (isEQ(iy,4)) {
			hitx04.write(hitxpfData);
		}
		if (isEQ(iy,5)) {
			hitx05.write(hitxpfData);
		}
		if (isEQ(iy,6)) {
			hitx06.write(hitxpfData);
		}
		if (isEQ(iy,7)) {
			hitx07.write(hitxpfData);
		}
		if (isEQ(iy,8)) {
			hitx08.write(hitxpfData);
		}
		if (isEQ(iy,9)) {
			hitx09.write(hitxpfData);
		}
		if (isEQ(iy,10)) {
			hitx10.write(hitxpfData);
		}
		if (isEQ(iy,11)) {
			hitx11.write(hitxpfData);
		}
		if (isEQ(iy,12)) {
			hitx12.write(hitxpfData);
		}
		if (isEQ(iy,13)) {
			hitx13.write(hitxpfData);
		}
		if (isEQ(iy,14)) {
			hitx14.write(hitxpfData);
		}
		if (isEQ(iy,15)) {
			hitx15.write(hitxpfData);
		}
		if (isEQ(iy,16)) {
			hitx16.write(hitxpfData);
		}
		if (isEQ(iy,17)) {
			hitx17.write(hitxpfData);
		}
		if (isEQ(iy,18)) {
			hitx18.write(hitxpfData);
		}
		if (isEQ(iy,19)) {
			hitx19.write(hitxpfData);
		}
		if (isEQ(iy,20)) {
			hitx20.write(hitxpfData);
		}
		/*    Log the number of extracted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*    Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*    Check for an incomplete block retrieved.*/
		//ILIFE-1337 - bpham7
		if (isLTE(wsaaInd, wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*    Close the open files and remove the overrides.*/
		if (isEQ(bprdIO.getSystemParam01(),"DEAL")) {
			getAppVars().freeDBConnectionIgnoreErr(sqldealCursorconn, sqldealCursorps, sqldealCursorrs);
		}
		if (isEQ(bprdIO.getSystemParam01(),"DEBT")) {
			getAppVars().freeDBConnectionIgnoreErr(sqldebtCursorconn, sqldebtCursorps, sqldebtCursorrs);
		}
		if (isEQ(bprdIO.getSystemParam01(),"SWCH")) {
			getAppVars().freeDBConnectionIgnoreErr(sqlswchCursorconn, sqlswchCursorps, sqlswchCursorrs);
		}
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			hitx01.close();
		}
		if (isEQ(iz,2)) {
			hitx02.close();
		}
		if (isEQ(iz,3)) {
			hitx03.close();
		}
		if (isEQ(iz,4)) {
			hitx04.close();
		}
		if (isEQ(iz,5)) {
			hitx05.close();
		}
		if (isEQ(iz,6)) {
			hitx06.close();
		}
		if (isEQ(iz,7)) {
			hitx07.close();
		}
		if (isEQ(iz,8)) {
			hitx08.close();
		}
		if (isEQ(iz,9)) {
			hitx09.close();
		}
		if (isEQ(iz,10)) {
			hitx10.close();
		}
		if (isEQ(iz,11)) {
			hitx11.close();
		}
		if (isEQ(iz,12)) {
			hitx12.close();
		}
		if (isEQ(iz,13)) {
			hitx13.close();
		}
		if (isEQ(iz,14)) {
			hitx14.close();
		}
		if (isEQ(iz,15)) {
			hitx15.close();
		}
		if (isEQ(iz,16)) {
			hitx16.close();
		}
		if (isEQ(iz,17)) {
			hitx17.close();
		}
		if (isEQ(iz,18)) {
			hitx18.close();
		}
		if (isEQ(iz,19)) {
			hitx19.close();
		}
		if (isEQ(iz,20)) {
			hitx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaHitrData = FLSInittedArray (1000, 77);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaHitrData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaHitrData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaHitrData, 9);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaHitrData, 11);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaHitrData, 13);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaHitrData, 15);
	private FixedLengthStringData[] wsaaZintbfnd = FLSDArrayPartOfArrayStructure(4, wsaaHitrData, 18);
	private PackedDecimalData[] wsaaTranno = PDArrayPartOfArrayStructure(5, 0, wsaaHitrData, 22);
	private FixedLengthStringData[] wsaaZrectyp = FLSDArrayPartOfArrayStructure(1, wsaaHitrData, 25);
	private FixedLengthStringData[] wsaaBatctrcde = FLSDArrayPartOfArrayStructure(4, wsaaHitrData, 26);
	private PackedDecimalData[] wsaaEffdate = PDArrayPartOfArrayStructure(8, 0, wsaaHitrData, 30);
	private FixedLengthStringData[] wsaaCntcurr = FLSDArrayPartOfArrayStructure(3, wsaaHitrData, 35);
	private FixedLengthStringData[] wsaaFdbkind = FLSDArrayPartOfArrayStructure(1, wsaaHitrData, 38);
	private FixedLengthStringData[] wsaaCovdbtind = FLSDArrayPartOfArrayStructure(1, wsaaHitrData, 39);
	private PackedDecimalData[] wsaaCntamnt = PDArrayPartOfArrayStructure(17, 2, wsaaHitrData, 40);
	private PackedDecimalData[] wsaaFundamnt = PDArrayPartOfArrayStructure(17, 2, wsaaHitrData, 49);
	private FixedLengthStringData[] wsaaCnttyp = FLSDArrayPartOfArrayStructure(3, wsaaHitrData, 58);
	private PackedDecimalData[] wsaaPrcseq = PDArrayPartOfArrayStructure(3, 0, wsaaHitrData, 61);
	private PackedDecimalData[] wsaaPersur = PDArrayPartOfArrayStructure(5, 2, wsaaHitrData, 63);
	private FixedLengthStringData[] wsaaSwchind = FLSDArrayPartOfArrayStructure(1, wsaaHitrData, 66);
	private PackedDecimalData[] wsaaFundrate = PDArrayPartOfArrayStructure(18, 9, wsaaHitrData, 67);
}
}
