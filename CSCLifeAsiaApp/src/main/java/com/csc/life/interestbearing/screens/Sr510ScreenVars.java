package com.csc.life.interestbearing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR510
 * @version 1.0 generated on 30/08/09 07:16
 * @author Quipoz
 */
public class Sr510ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(3);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(dataFields, 0, FILLER);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 1);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(errorIndicators, 0, FILLER);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 2);
	public FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(outputIndicators, 0, FILLER);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(200);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(54).isAPartOf(subfileArea, 0);
	public FixedLengthStringData fromdates = new FixedLengthStringData(16).isAPartOf(subfileFields, 0);
	public ZonedDecimalData[] fromdate = ZDArrayPartOfStructure(2, 8, 0, fromdates, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(fromdates, 0, FILLER_REDEFINE);
	public ZonedDecimalData fromdate01 = DD.fromdate.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData fromdate02 = DD.fromdate.copyToZonedDecimal().isAPartOf(filler3,8);
	public FixedLengthStringData interates = new FixedLengthStringData(16).isAPartOf(subfileFields, 16);
	public ZonedDecimalData[] interate = ZDArrayPartOfStructure(4, 4, 2, interates, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(interates, 0, FILLER_REDEFINE);
	public ZonedDecimalData interate01 = DD.interate.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData interate02 = DD.interate.copyToZonedDecimal().isAPartOf(filler4,4);
	public ZonedDecimalData interate03 = DD.interate.copyToZonedDecimal().isAPartOf(filler4,8);
	public ZonedDecimalData interate04 = DD.interate.copyToZonedDecimal().isAPartOf(filler4,12);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(subfileFields,32);
	public ZonedDecimalData todate = DD.todate.copyToZonedDecimal().isAPartOf(subfileFields,42);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 54);
	public FixedLengthStringData fromdatesErr = new FixedLengthStringData(8).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData[] fromdateErr = FLSArrayPartOfStructure(2, 4, fromdatesErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(fromdatesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fromdate01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData fromdate02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData interatesErr = new FixedLengthStringData(16).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData[] interateErr = FLSArrayPartOfStructure(4, 4, interatesErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(16).isAPartOf(interatesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData interate01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData interate02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData interate03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData interate04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData todateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 90);
	public FixedLengthStringData fromdatesOut = new FixedLengthStringData(24).isAPartOf(outputSubfile, 0);
	public FixedLengthStringData[] fromdateOut = FLSArrayPartOfStructure(2, 12, fromdatesOut, 0);
	public FixedLengthStringData[][] fromdateO = FLSDArrayPartOfArrayStructure(12, 1, fromdateOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(24).isAPartOf(fromdatesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fromdate01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] fromdate02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData interatesOut = new FixedLengthStringData(48).isAPartOf(outputSubfile, 24);
	public FixedLengthStringData[] interateOut = FLSArrayPartOfStructure(4, 12, interatesOut, 0);
	public FixedLengthStringData[][] interateO = FLSDArrayPartOfArrayStructure(12, 1, interateOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(48).isAPartOf(interatesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] interate01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] interate02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] interate03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] interate04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] todateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 198);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData fromdate01Disp = new FixedLengthStringData(10);
	public FixedLengthStringData fromdate02Disp = new FixedLengthStringData(10);
	public FixedLengthStringData todateDisp = new FixedLengthStringData(10);

	public LongData Sr510screensflWritten = new LongData(0);
	public LongData Sr510screenctlWritten = new LongData(0);
	public LongData Sr510screenWritten = new LongData(0);
	public LongData Sr510protectWritten = new LongData(0);
	public GeneralTable sr510screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr510screensfl;
	}

	public Sr510ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vrtfndOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(shortdescOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(interate01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(interate02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fromdate01Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(todateOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(interate03Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(interate04Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fromdate02Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {unitVirtualFund, shortdesc, interate01, interate02, fromdate01, todate, interate03, interate04, fromdate02};
		screenSflOutFields = new BaseData[][] {vrtfndOut, shortdescOut, interate01Out, interate02Out, fromdate01Out, todateOut, interate03Out, interate04Out, fromdate02Out};
		screenSflErrFields = new BaseData[] {vrtfndErr, shortdescErr, interate01Err, interate02Err, fromdate01Err, todateErr, interate03Err, interate04Err, fromdate02Err};
		screenSflDateFields = new BaseData[] {fromdate01, todate, fromdate02};
		screenSflDateErrFields = new BaseData[] {fromdate01Err, todateErr, fromdate02Err};
		screenSflDateDispFields = new BaseData[] {fromdate01Disp, todateDisp, fromdate02Disp};

		screenFields = new BaseData[] {};
		screenOutFields = new BaseData[][] {};
		screenErrFields = new BaseData[] {};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr510screen.class;
		screenSflRecord = Sr510screensfl.class;
		screenCtlRecord = Sr510screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr510protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr510screenctl.lrec.pageSubfile);
	}
}
