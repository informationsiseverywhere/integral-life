package com.csc.life.productdefinition.dataaccess.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface VprcpfDAO extends BaseDAO<Vprcpf> {
    public List<Vprcpf> searchVprcRecord(int intdate, Set<String> vfund);
    public Vprcpf getNowPrice(String company, String unitVirtualFund, String unitType, int effdate, String validflag); //ILIFE-3407 vdivisala
    // BRD-78 NAV Upload:Start
    public List<Integer> insert(List<Vprcpf> vprcpfList);
    public Integer getMaxJobNumber();
    // BRD-78 NAV Upload:Start
    public Map<String,List<Vprcpf>> searchVprnudlRecord(int intdate, List<String> vfundSet, String company);
    public BigDecimal getvprcRecord(String company, String unitVirtualFund, int effdate);
    public BigDecimal  getUnitBidPrice(String chdrcoy,String unitVirtualFund, int effdate); //ALS-4706
}