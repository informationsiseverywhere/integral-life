package com.csc.life.productdefinition.dataaccess.model;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Zafapf implements java.io.Serializable
{
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private String chdrpfx;
	private String chdrcoy;
	private String chdrnum;
	private String cnttype;
	private String validflag;
	private String crtable;
	private String zafropt;
	private String zchkopt;
	private int zregdte;
	private int age;
	private int tranno;
	private String zafrfreq;
	private String vrtfund;
	private String ualfnd01;
	private String ualfnd02;
	private String ualfnd03;
	private String ualfnd04;
	private String ualfnd05;
	private String ualfnd06;
	private String ualfnd07;
	private String ualfnd08;
	private String ualfnd09;
	private String ualfnd10;
	private Double ualprc01;
	private Double ualprc02;
	private Double ualprc03;
	private Double ualprc04;
	private Double ualprc05;
	private Double ualprc06;
	private Double ualprc07;
	private Double ualprc08;
	private Double ualprc09;
	private Double ualprc10;
	private Double fundamnt01;
	private Double fundamnt02;
	private Double fundamnt03;
	private Double fundamnt04;
	private Double fundamnt05;
	private Double fundamnt06;
	private Double fundamnt07;
	private Double fundamnt08;
	private Double fundamnt09;
	private Double fundamnt10;
	private Double zwtavga01;
	private Double zwtavga02;
	private Double zwtavga03;
	private Double zwtavga04;
	private Double zwtavga05;
	private Double zwtavga06;
	private Double zwtavga07;
	private Double zwtavga08;
	private Double zwtavga09;	
	private Double zwtavga10;
	private Double zdevita01;
	private Double zdevita02;
	private Double zdevita03;
	private Double zdevita04;
	private Double zdevita05;
	private Double zdevita06;
	private Double zdevita07;
	private Double zdevita08;
	private Double zdevita09;
	private Double zdevita10;
	private String zbuysell01;
	private String zbuysell02;
	private String zbuysell03;
	private String zbuysell04;
	private String zbuysell05;
	private String zbuysell06;
	private String zbuysell07;
	private String zbuysell08;
	private String zbuysell09;
	private String zbuysell10;
	private Double zbsamnt01;
	private Double zbsamnt02;
	private Double zbsamnt03;
	private Double zbsamnt04;
	private Double zbsamnt05;
	private Double zbsamnt06;
	private Double zbsamnt07;
	private Double zbsamnt08;
	private Double zbsamnt09;
	private Double zbsamnt10;
	private String linedetail;
	private String zwdwlrsn;
	private int zlastdte;
	private int znextdte;
	private int zwdlddte;
	private String zenable;
	private int zlstupdte;
	private String usrprf;
	private String jobnm;
	private String zafritem;
	private Timestamp datime;
	
	private Double uniqueNumber;	
	public Double getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(Double uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getZafropt() {
		return zafropt;
	}
	public void setZafropt(String zafropt) {
		this.zafropt = zafropt;
	}
	public String getZchkopt() {
		return zchkopt;
	}
	public void setZchkopt(String zchkopt) {
		this.zchkopt = zchkopt;
	}
	public int getZregdte() {
		return zregdte;
	}
	public void setZregdte(int zregdte) {
		this.zregdte = zregdte;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getZafrfreq() {
		return zafrfreq;
	}
	public void setZafrfreq(String zafrfreq) {
		this.zafrfreq = zafrfreq;
	}
	public String getVrtfund() {
		return vrtfund;
	}
	public void setVrtfund(String vrtfund) {
		this.vrtfund = vrtfund;
	}
	public String getUalfnd01() {
		return ualfnd01;
	}
	public void setUalfnd01(String ualfnd01) {
		this.ualfnd01 = ualfnd01;
	}
	public String getUalfnd02() {
		return ualfnd02;
	}
	public void setUalfnd02(String ualfnd02) {
		this.ualfnd02 = ualfnd02;
	}
	public String getUalfnd03() {
		return ualfnd03;
	}
	public void setUalfnd03(String ualfnd03) {
		this.ualfnd03 = ualfnd03;
	}
	public String getUalfnd04() {
		return ualfnd04;
	}
	public void setUalfnd04(String ualfnd04) {
		this.ualfnd04 = ualfnd04;
	}
	public String getUalfnd05() {
		return ualfnd05;
	}
	public void setUalfnd05(String ualfnd05) {
		this.ualfnd05 = ualfnd05;
	}
	public String getUalfnd06() {
		return ualfnd06;
	}
	public void setUalfnd06(String ualfnd06) {
		this.ualfnd06 = ualfnd06;
	}
	public String getUalfnd07() {
		return ualfnd07;
	}
	public void setUalfnd07(String ualfnd07) {
		this.ualfnd07 = ualfnd07;
	}
	public String getUalfnd08() {
		return ualfnd08;
	}
	public void setUalfnd08(String ualfnd08) {
		this.ualfnd08 = ualfnd08;
	}
	public String getUalfnd09() {
		return ualfnd09;
	}
	public void setUalfnd09(String ualfnd09) {
		this.ualfnd09 = ualfnd09;
	}
	public String getUalfnd10() {
		return ualfnd10;
	}
	public void setUalfnd10(String ualfnd10) {
		this.ualfnd10 = ualfnd10;
	}
	public Double getUalprc01() {
		return ualprc01;
	}
	public void setUalprc01(Double ualprc01) {
		this.ualprc01 = ualprc01;
	}
	public Double getUalprc02() {
		return ualprc02;
	}
	public void setUalprc02(Double ualprc02) {
		this.ualprc02 = ualprc02;
	}
	public Double getUalprc03() {
		return ualprc03;
	}
	public void setUalprc03(Double ualprc03) {
		this.ualprc03 = ualprc03;
	}
	public Double getUalprc04() {
		return ualprc04;
	}
	public void setUalprc04(Double ualprc04) {
		this.ualprc04 = ualprc04;
	}
	public Double getUalprc05() {
		return ualprc05;
	}
	public void setUalprc05(Double ualprc05) {
		this.ualprc05 = ualprc05;
	}
	public Double getUalprc06() {
		return ualprc06;
	}
	public void setUalprc06(Double ualprc06) {
		this.ualprc06 = ualprc06;
	}
	public Double getUalprc07() {
		return ualprc07;
	}
	public void setUalprc07(Double ualprc07) {
		this.ualprc07 = ualprc07;
	}
	public Double getUalprc08() {
		return ualprc08;
	}
	public void setUalprc08(Double ualprc08) {
		this.ualprc08 = ualprc08;
	}
	public Double getUalprc09() {
		return ualprc09;
	}
	public void setUalprc09(Double ualprc09) {
		this.ualprc09 = ualprc09;
	}
	public Double getUalprc10() {
		return ualprc10;
	}
	public void setUalprc10(Double ualprc10) {
		this.ualprc10 = ualprc10;
	}
	public Double getFundamnt01() {
		return fundamnt01;
	}
	public void setFundamnt01(Double fundamnt01) {
		this.fundamnt01 = fundamnt01;
	}
	public Double getFundamnt02() {
		return fundamnt02;
	}
	public void setFundamnt02(Double fundamnt02) {
		this.fundamnt02 = fundamnt02;
	}
	public Double getFundamnt03() {
		return fundamnt03;
	}
	public void setFundamnt03(Double fundamnt03) {
		this.fundamnt03 = fundamnt03;
	}
	public Double getFundamnt04() {
		return fundamnt04;
	}
	public void setFundamnt04(Double fundamnt04) {
		this.fundamnt04 = fundamnt04;
	}
	public Double getFundamnt05() {
		return fundamnt05;
	}
	public void setFundamnt05(Double fundamnt05) {
		this.fundamnt05 = fundamnt05;
	}
	public Double getFundamnt06() {
		return fundamnt06;
	}
	public void setFundamnt06(Double fundamnt06) {
		this.fundamnt06 = fundamnt06;
	}
	public Double getFundamnt07() {
		return fundamnt07;
	}
	public void setFundamnt07(Double fundamnt07) {
		this.fundamnt07 = fundamnt07;
	}
	public Double getFundamnt08() {
		return fundamnt08;
	}
	public void setFundamnt08(Double fundamnt08) {
		this.fundamnt08 = fundamnt08;
	}
	public Double getFundamnt09() {
		return fundamnt09;
	}
	public void setFundamnt09(Double fundamnt09) {
		this.fundamnt09 = fundamnt09;
	}
	public Double getFundamnt10() {
		return fundamnt10;
	}
	public void setFundamnt10(Double fundamnt10) {
		this.fundamnt10 = fundamnt10;
	}
	public Double getZwtavga01() {
		return zwtavga01;
	}
	public void setZwtavga01(Double zwtavga01) {
		this.zwtavga01 = zwtavga01;
	}
	public Double getZwtavga02() {
		return zwtavga02;
	}
	public void setZwtavga02(Double zwtavga02) {
		this.zwtavga02 = zwtavga02;
	}
	public Double getZwtavga03() {
		return zwtavga03;
	}
	public void setZwtavga03(Double zwtavga03) {
		this.zwtavga03 = zwtavga03;
	}
	public Double getZwtavga04() {
		return zwtavga04;
	}
	public void setZwtavga04(Double zwtavga04) {
		this.zwtavga04 = zwtavga04;
	}
	public Double getZwtavga05() {
		return zwtavga05;
	}
	public void setZwtavga05(Double zwtavga05) {
		this.zwtavga05 = zwtavga05;
	}
	public Double getZwtavga06() {
		return zwtavga06;
	}
	public void setZwtavga06(Double zwtavga06) {
		this.zwtavga06 = zwtavga06;
	}
	public Double getZwtavga07() {
		return zwtavga07;
	}
	public void setZwtavga07(Double zwtavga07) {
		this.zwtavga07 = zwtavga07;
	}
	public Double getZwtavga08() {
		return zwtavga08;
	}
	public void setZwtavga08(Double zwtavga08) {
		this.zwtavga08 = zwtavga08;
	}
	public Double getZwtavga09() {
		return zwtavga09;
	}
	public void setZwtavga09(Double zwtavga09) {
		this.zwtavga09 = zwtavga09;
	}
	public Double getZwtavga10() {
		return zwtavga10;
	}
	public void setZwtavga10(Double zwtavga10) {
		this.zwtavga10 = zwtavga10;
	}
	public Double getZdevita01() {
		return zdevita01;
	}
	public void setZdevita01(Double zdevita01) {
		this.zdevita01 = zdevita01;
	}
	public Double getZdevita02() {
		return zdevita02;
	}
	public void setZdevita02(Double zdevita02) {
		this.zdevita02 = zdevita02;
	}
	public Double getZdevita03() {
		return zdevita03;
	}
	public void setZdevita03(Double zdevita03) {
		this.zdevita03 = zdevita03;
	}
	public Double getZdevita04() {
		return zdevita04;
	}
	public void setZdevita04(Double zdevita04) {
		this.zdevita04 = zdevita04;
	}
	public Double getZdevita05() {
		return zdevita05;
	}
	public void setZdevita05(Double zdevita05) {
		this.zdevita05 = zdevita05;
	}
	public Double getZdevita06() {
		return zdevita06;
	}
	public void setZdevita06(Double zdevita06) {
		this.zdevita06 = zdevita06;
	}
	public Double getZdevita07() {
		return zdevita07;
	}
	public void setZdevita07(Double zdevita07) {
		this.zdevita07 = zdevita07;
	}
	public Double getZdevita08() {
		return zdevita08;
	}
	public void setZdevita08(Double zdevita08) {
		this.zdevita08 = zdevita08;
	}
	public Double getZdevita09() {
		return zdevita09;
	}
	public void setZdevita09(Double zdevita09) {
		this.zdevita09 = zdevita09;
	}
	public Double getZdevita10() {
		return zdevita10;
	}
	public void setZdevita10(Double zdevita10) {
		this.zdevita10 = zdevita10;
	}
	public String getZbuysell01() {
		return zbuysell01;
	}
	public void setZbuysell01(String zbuysell01) {
		this.zbuysell01 = zbuysell01;
	}
	public String getZbuysell02() {
		return zbuysell02;
	}
	public void setZbuysell02(String zbuysell02) {
		this.zbuysell02 = zbuysell02;
	}
	public String getZbuysell03() {
		return zbuysell03;
	}
	public void setZbuysell03(String zbuysell03) {
		this.zbuysell03 = zbuysell03;
	}
	public String getZbuysell04() {
		return zbuysell04;
	}
	public void setZbuysell04(String zbuysell04) {
		this.zbuysell04 = zbuysell04;
	}
	public String getZbuysell05() {
		return zbuysell05;
	}
	public void setZbuysell05(String zbuysell05) {
		this.zbuysell05 = zbuysell05;
	}
	public String getZbuysell06() {
		return zbuysell06;
	}
	public void setZbuysell06(String zbuysell06) {
		this.zbuysell06 = zbuysell06;
	}
	public String getZbuysell07() {
		return zbuysell07;
	}
	public void setZbuysell07(String zbuysell07) {
		this.zbuysell07 = zbuysell07;
	}
	public String getZbuysell08() {
		return zbuysell08;
	}
	public void setZbuysell08(String zbuysell08) {
		this.zbuysell08 = zbuysell08;
	}
	public String getZbuysell09() {
		return zbuysell09;
	}
	public void setZbuysell09(String zbuysell09) {
		this.zbuysell09 = zbuysell09;
	}
	public String getZbuysell10() {
		return zbuysell10;
	}
	public void setZbuysell10(String zbuysell10) {
		this.zbuysell10 = zbuysell10;
	}
	public Double getZbsamnt01() {
		return zbsamnt01;
	}
	public void setZbsamnt01(Double zbsamnt01) {
		this.zbsamnt01 = zbsamnt01;
	}
	public Double getZbsamnt02() {
		return zbsamnt02;
	}
	public void setZbsamnt02(Double zbsamnt02) {
		this.zbsamnt02 = zbsamnt02;
	}
	public Double getZbsamnt03() {
		return zbsamnt03;
	}
	public void setZbsamnt03(Double zbsamnt03) {
		this.zbsamnt03 = zbsamnt03;
	}
	public Double getZbsamnt04() {
		return zbsamnt04;
	}
	public void setZbsamnt04(Double zbsamnt04) {
		this.zbsamnt04 = zbsamnt04;
	}
	public Double getZbsamnt05() {
		return zbsamnt05;
	}
	public void setZbsamnt05(Double zbsamnt05) {
		this.zbsamnt05 = zbsamnt05;
	}
	public Double getZbsamnt06() {
		return zbsamnt06;
	}
	public void setZbsamnt06(Double zbsamnt06) {
		this.zbsamnt06 = zbsamnt06;
	}
	public Double getZbsamnt07() {
		return zbsamnt07;
	}
	public void setZbsamnt07(Double zbsamnt07) {
		this.zbsamnt07 = zbsamnt07;
	}
	public Double getZbsamnt08() {
		return zbsamnt08;
	}
	public void setZbsamnt08(Double zbsamnt08) {
		this.zbsamnt08 = zbsamnt08;
	}
	public Double getZbsamnt09() {
		return zbsamnt09;
	}
	public void setZbsamnt09(Double zbsamnt09) {
		this.zbsamnt09 = zbsamnt09;
	}
	public Double getZbsamnt10() {
		return zbsamnt10;
	}
	public void setZbsamnt10(Double zbsamnt10) {
		this.zbsamnt10 = zbsamnt10;
	}
	public String getLinedetail() {
		return linedetail;
	}
	public void setLinedetail(String linedetail) {
		this.linedetail = linedetail;
	}
	public String getZwdwlrsn() {
		return zwdwlrsn;
	}
	public void setZwdwlrsn(String zwdwlrsn) {
		this.zwdwlrsn = zwdwlrsn;
	}
	public int getZlastdte() {
		return zlastdte;
	}
	public void setZlastdte(int zlastdte) {
		this.zlastdte = zlastdte;
	}
	public int getZnextdte() {
		return znextdte;
	}
	public void setZnextdte(int znextdte) {
		this.znextdte = znextdte;
	}
	public int getZwdlddte() {
		return zwdlddte;
	}
	public void setZwdlddte(int zwdlddte) {
		this.zwdlddte = zwdlddte;
	}
	public String getZenable() {
		return zenable;
	}
	public void setZenable(String zenable) {
		this.zenable = zenable;
	}
	public int getZlstupdte() {
		return zlstupdte;
	}
	public void setZlstupdte(int zlstupdte) {
		this.zlstupdte = zlstupdte;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getZafritem() {
		return zafritem;
	}
	public void setZafritem(String zafritem) {
		this.zafritem = zafritem;
	}
	//IJTI-461 START
	public Timestamp getDatime() {
		if (this.datime==null){
			return null;
		} else {
			return (Timestamp) this.datime.clone();
		}
	} //IJTI-461 END
	public void setDatime(Timestamp datime) {
		this.datime = (Timestamp) datime.clone(); //IJTI-460
	}
	
	
}
