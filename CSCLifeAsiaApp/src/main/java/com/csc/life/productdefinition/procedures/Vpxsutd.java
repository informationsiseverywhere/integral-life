package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
//import com.csc.life.productdefinition.procedures.Vpxsurc.GotoLabel;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxsutdrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* VPMS - Surrender value Calc for Traditional Product
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*   This subroutine extracts data from CHDR/COVR/ACBL/Tables... 
*   to the VPMS calculation subroutine.
*
*   Functions:
*   ----------
*   INIT       - returns the various fields needed from files
*
*   Requests/Response:
*   --------
*   refer to LifeExternalisedRulesMapping.xml
*
*   Status:
*   -------
*   ****       - Everything checks out OK
*   E049       - Invalid Function
*   H134       - T5645 Item not found
*   H135	   - T3695 Item not found
*   H053       - T5687 Entry Not Found
*
***********************************************************************
* </pre>
*/

public class Vpxsutd extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private String wsaaSubr = "VPXSUTD";
	// the key of ACBLPF
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	// PlanSuffix
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaPlanSuffixx = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanSuffix, 0, REDEFINE).setUnsigned();
	private ZonedDecimalData filler = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffixx, 0, FILLER).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffixx, 2).setUnsigned();

	/* ERRORS */
	private String e049 = "E049";	
	private String h134 = "H134";
	private String h135 = "H135";
	private String h053 = "H053";
	
	/* Table */
	private String t5645 = "T5645";
	private String t3695 = "T3695";
	private String t5687 = "T5687";
	
	/* Formats */
	private String itemrec = "ITEMREC";
	
	/* COPYBOOK */
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Vpxsutdrec vpxsutdrec = new Vpxsutdrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	
	/* DAO */
	private List<Covrpf> covrpfList = null;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Acblpf acblpf = null;
	private Chdrpf chdrpf = null;
    private Iterator<Covrpf> covrpfIter;
    private Covrpf covrpf = null;
    
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit000,
	}

	public Vpxsutd() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		vpxsutdrec = (Vpxsutdrec) parmArray[1];	
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
		
		try {
			main000();
			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para000();
				}
				case exit000: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para000()
{
	syserrrec.subrname.set(wsaaSubr);
	vpxsutdrec.statuz.set(varcom.oK);
	
	if (isEQ(vpxsutdrec.function,"INIT")){
		initialize050();
		readrchdrpf1000();
		readrcovrpf2000();
		//call subroutine at first time
		if (isEQ(srcalcpy.type,"S")){
			readtables3100();
			return;
		}

		//call subroutine at other time
		if (isEQ(srcalcpy.type,"B")){
			readtables3200();
			readracblpf4000();
		}
	}
	else {
		srcalcpy.status.set(e049);
		vpxsutdrec.statuz.set(e049);
		fatalError6000();
	}	
}


protected void initialize050()
{
	//Initialize the output fields.
	vpxsutdrec.anbccd.set(varcom.maxdate);
	vpxsutdrec.mortcls.set(SPACES);
	vpxsutdrec.sex.set(SPACES);
	vpxsutdrec.instprem.set(ZERO);
	vpxsutdrec.sumins.set(ZERO);
	vpxsutdrec.crrcd.set(varcom.maxdate);
	vpxsutdrec.rcsdte.set(varcom.maxdate);
	vpxsutdrec.pcesdte.set(varcom.maxdate);
	vpxsutdrec.curbal.set(ZERO);
	vpxsutdrec.cnttype.set(SPACES);
}

protected void readrchdrpf1000()
{
	chdrpf = chdrpfDAO.getchdrRecord(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString());
	
	if (chdrpf == null) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(srcalcpy.chdrChdrcoy);
		stringVariable1.addExpression(srcalcpy.chdrChdrnum);
		stringVariable1.addExpression(srcalcpy.lifeLife);
		stringVariable1.addExpression(srcalcpy.covrCoverage);
		stringVariable1.addExpression(srcalcpy.covrRider);
		stringVariable1.setStringInto(syserrrec.params);
		vpxsutdrec.statuz.set(varcom.bomb);
		fatalError6000();
	}
	
	vpxsutdrec.statuz.set(varcom.oK);
	vpxsutdrec.cnttype.set(chdrpf.getCnttype());
	
}

protected void readrcovrpf2000()
{
	covrpfList = covrpfDAO.getcovrtrbRecord(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString(), srcalcpy.lifeLife.toString(), srcalcpy.covrCoverage.toString(), srcalcpy.covrRider.toString());
	if (covrpfList == null || covrpfList.size() == 0) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(srcalcpy.chdrChdrcoy);
		stringVariable1.addExpression(srcalcpy.chdrChdrnum);
		stringVariable1.addExpression(srcalcpy.lifeLife);
		stringVariable1.addExpression(srcalcpy.covrCoverage);
		stringVariable1.addExpression(srcalcpy.covrRider);
		stringVariable1.setStringInto(syserrrec.params);
		vpxsutdrec.statuz.set(varcom.bomb);
		fatalError6000();
	}
	
	covrpfIter = covrpfList.iterator();
	if(covrpfIter.hasNext()){   
	covrpf = covrpfIter.next();
	}       
	vpxsutdrec.statuz.set(varcom.oK);
	vpxsutdrec.anbccd.set(covrpf.getAnbAtCcd());
	vpxsutdrec.mortcls.set(covrpf.getMortcls());
	vpxsutdrec.sex.set(covrpf.getSex());
	vpxsutdrec.instprem.set(covrpf.getInstprem());
	vpxsutdrec.sumins.set(covrpf.getSumins());
	vpxsutdrec.crrcd.set(covrpf.getCrrcd());
	vpxsutdrec.rcsdte.set(covrpf.getRiskCessDate());
	vpxsutdrec.pcesdte.set(covrpf.getPremCessDate());
}

protected void readtables3100(){
	descIO.setDataKey(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(srcalcpy.chdrChdrcoy);
	descIO.setDesctabl(t5687);
	descIO.setDescitem(srcalcpy.crtable);
	descIO.setLanguage(srcalcpy.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		syserrrec.statuz.set(descIO.getStatuz());
		vpxsutdrec.statuz.set(descIO.getStatuz());
		fatalError6000();
	}
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(h053);
		syserrrec.params.set(descIO.getParams());
		vpxsutdrec.statuz.set(h053);
		fatalError6000();
	}
	if (isEQ(descIO.getShortdesc(),SPACES)) {
		srcalcpy.description.fill("?");
	}
	else {
		srcalcpy.description.set(descIO.getShortdesc());
	}
}

protected void readtables3200(){
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(srcalcpy.chdrChdrcoy);
	itemIO.setItemtabl(t5645);
	itemIO.setItemitem(vpxsutdrec.subname);
	itemIO.setFormat(itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)
	&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(itemIO.getStatuz());
		vpxsutdrec.statuz.set(itemIO.getStatuz());
		fatalError6000();
	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(h134);
		vpxsutdrec.statuz.set(h134);
		fatalError6000();
	}
	t5645rec.t5645Rec.set(itemIO.getGenarea());
	descIO.setDataKey(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(srcalcpy.chdrChdrcoy);
	descIO.setDesctabl(t3695);
	descIO.setDescitem(t5645rec.sacstype01);
	descIO.setLanguage(srcalcpy.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		syserrrec.statuz.set(descIO.getStatuz());
		vpxsutdrec.statuz.set(descIO.getStatuz());
		fatalError6000();
	}
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		syserrrec.statuz.set(h135);
		vpxsutdrec.statuz.set(h135);
		fatalError6000();
	}
	if (isEQ(descIO.getShortdesc(),SPACES)) {
		srcalcpy.description.fill("?");
	}
	else {
		srcalcpy.description.set(descIO.getShortdesc());
	}
}

protected void readracblpf4000()
{
	String wsaaRldgacct = "";
	wsaaRldgChdrnum.set(srcalcpy.chdrChdrnum);
	wsaaRldgLife.set(srcalcpy.lifeLife);
	wsaaRldgCoverage.set(srcalcpy.covrCoverage);
	wsaaRldgRider.set(srcalcpy.covrRider);
	wsaaPlanSuffix.set(covrpf.getPlanSuffix());
	wsaaRldgPlnsfx.set(wsaaPlnsfx);


	acblpf = acblDao.getAcblpfRecord(srcalcpy.chdrChdrcoy.toString().trim(), t5645rec.sacscode01.toString().trim(), wsaaRldgacct.trim(),/* IJTI-1523 */
			t5645rec.sacstype01.toString().trim(),srcalcpy.chdrCurr.toString().trim());

	if (acblpf == null){
		vpxsutdrec.curbal.set(ZERO);
	}
	
	if (isEQ(vpxsutdrec.curbal,ZERO)){
		srcalcpy.type.set(SPACES);
		srcalcpy.description.set(SPACES);
		return;
	}
	
	vpxsutdrec.curbal.set(acblpf.getSacscurbal());
}

protected void fatalError6000()
{
	if (isNE(syserrrec.statuz,SPACES)
	|| isNE(syserrrec.syserrStatuz,SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
}

protected void exit000()
{
	exitProgram();
}
}
