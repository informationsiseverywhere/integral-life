/*
 * File: Cr555.java
 * Date: 30 August 2009 2:59:22
 * Author: $Id$
 * 
 * Class transformed from CR555.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.cls;

import com.quipoz.COBOLFramework.printing.PrintManager;
import com.quipoz.COBOLFramework.printing.PrintConfiguration;
import com.quipoz.COBOLFramework.dataqueue.DataQueue;
import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.util.ResidentUtilityModel;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.quipoz.COBOLFramework.query.Opnqryf;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.COBOLFramework.common.exception.*;
import com.quipoz.COBOLFramework.util.*;
import com.quipoz.framework.util.log.QPLogger;
import com.quipoz.framework.exception.TransferProgramException;
import static com.csc.smart400framework.batch.cls.BatchCLFunctions.*;




import com.csc.life.productdefinition.batchprograms.Br555;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;

public class Cr555 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData statuz = new FixedLengthStringData(4);
	private FixedLengthStringData bsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData buparec = new FixedLengthStringData(1024);
	private FixedLengthStringData lib = new FixedLengthStringData(8);
	private FixedLengthStringData file = new FixedLengthStringData(8).init("LIAMPF");
	private PackedDecimalData rcdlen = new PackedDecimalData(4, 0).init(158);

	public Cr555() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		buparec = convertAndSetParam(buparec, parmArray, 4);
		bprdrec = convertAndSetParam(bprdrec, parmArray, 3);
		bsprrec = convertAndSetParam(bsprrec, parmArray, 2);
		bsscrec = convertAndSetParam(bsscrec, parmArray, 1);
		statuz = convertAndSetParam(statuz, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			switch (qState) {
			case QS_START: {
				try {
					lib.set("QGPL");
				}
				catch (ExtMsgException ex){
					if (ex.messageMatches("CPF0000")
					|| ex.messageMatches("LBE0000")) {
						qState = error;
						break;
					}
					else {
						throw ex;
					}
				}
				try {
					FileCode.checkExistence(file);
				}
				catch (ExtMsgException ex){
					if (ex.messageMatches("CPF9801")) {
						FileCode.createPhysicalFile(file, rcdlen, null, null, null, null, null, null, null);
					}
					else {
						throw ex;
					}
				}
				FileCode.clearMember(file);
				callProgram(Br555.class, new Object[] {statuz, bsscrec, bsprrec, bprdrec, buparec});
			}
			case returnVar: {
				return ;
			}
			case error: {
				appVars.sendMessageToQueue("Unexpected errors occurred", "*");
				statuz.set("BOMB");
				qState = returnVar;
				break;
			}
			default:{
				qState = QS_END;
			}
			}
		}
		
	}
}
