package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 22 Aug 2012 15:34:00
 * Description:
 * Copybook name: Vpxlextrec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxlextrec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
  	public FixedLengthStringData vpxlextRec = new FixedLengthStringData(297);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxlextRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxlextRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxlextRec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxlextRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	
  	
  	
  	public FixedLengthStringData dataRec = new FixedLengthStringData(156).isAPartOf(vpxlextRec, 136);
  	
  	/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
	*/
  	public PackedDecimalData count = new PackedDecimalData(3, 0).isAPartOf(dataRec, 0);
  	public FixedLengthStringData oppcs = new FixedLengthStringData(40).isAPartOf(dataRec, 3);
  	public PackedDecimalData oppc01 = new PackedDecimalData(5,2);
  	public PackedDecimalData oppc02 = new PackedDecimalData(5,2);
  	public PackedDecimalData oppc03 = new PackedDecimalData(5,2);
  	public PackedDecimalData oppc04 = new PackedDecimalData(5,2);
  	public PackedDecimalData oppc05 = new PackedDecimalData(5,2);
  	public PackedDecimalData oppc06 = new PackedDecimalData(5,2);
  	public PackedDecimalData oppc07 = new PackedDecimalData(5,2);
  	public PackedDecimalData oppc08 = new PackedDecimalData(5,2);
  	
  	public FixedLengthStringData tabsorsel = new FixedLengthStringData(1).isAPartOf(dataRec, 43); //ILIFE-7528
  	
	public FixedLengthStringData zmortpcts = new FixedLengthStringData(24);
	public PackedDecimalData zmortpct01 = new PackedDecimalData(3,0);
	public PackedDecimalData zmortpct02 = new PackedDecimalData(3,0);
	public PackedDecimalData zmortpct03 = new PackedDecimalData(3,0);
	public PackedDecimalData zmortpct04 = new PackedDecimalData(3,0);
	public PackedDecimalData zmortpct05 = new PackedDecimalData(3,0);
	public PackedDecimalData zmortpct06 = new PackedDecimalData(3,0);
	public PackedDecimalData zmortpct07 = new PackedDecimalData(3,0);
	public PackedDecimalData zmortpct08 = new PackedDecimalData(3,0);
	public FixedLengthStringData opcdas =  new FixedLengthStringData(16);
	public FixedLengthStringData opcda01 = new FixedLengthStringData(2);
	public FixedLengthStringData opcda02 = new FixedLengthStringData(2);
	public FixedLengthStringData opcda03 = new FixedLengthStringData(2);
	public FixedLengthStringData opcda04 = new FixedLengthStringData(2);
	public FixedLengthStringData opcda05 = new FixedLengthStringData(2);
	public FixedLengthStringData opcda06 = new FixedLengthStringData(2);
	public FixedLengthStringData opcda07 = new FixedLengthStringData(2);
	public FixedLengthStringData opcda08 = new FixedLengthStringData(2);
  	public FixedLengthStringData agerates = new FixedLengthStringData(24);
	public PackedDecimalData agerate01 = new PackedDecimalData(3,0);
	public PackedDecimalData agerate02 = new PackedDecimalData(3,0);
	public PackedDecimalData agerate03 = new PackedDecimalData(3,0);
	public PackedDecimalData agerate04 = new PackedDecimalData(3,0);
	public PackedDecimalData agerate05 = new PackedDecimalData(3,0);
	public PackedDecimalData agerate06 = new PackedDecimalData(3,0);
	public PackedDecimalData agerate07 = new PackedDecimalData(3,0);
	public PackedDecimalData agerate08 = new PackedDecimalData(3,0);
  	public FixedLengthStringData insprms = new FixedLengthStringData(48);
	public PackedDecimalData insprm01 = getPackData();
	public PackedDecimalData insprm02 = getPackData();
	public PackedDecimalData insprm03 = getPackData();
	public PackedDecimalData insprm04 = getPackData();
	public PackedDecimalData insprm05 = getPackData();
	public PackedDecimalData insprm06 = getPackData();
	public PackedDecimalData insprm07 = getPackData();
	public PackedDecimalData insprm08 = getPackData();
	
	//IBPLIFE-2435 start
	public FixedLengthStringData premAdjusted = new FixedLengthStringData(48);
	public PackedDecimalData premAdjusted01 = new PackedDecimalData(15,2);
	public PackedDecimalData premAdjusted02 = new PackedDecimalData(15,2);
	public PackedDecimalData premAdjusted03 = new PackedDecimalData(15,2);
	public PackedDecimalData premAdjusted04 = new PackedDecimalData(15,2);
	public PackedDecimalData premAdjusted05 = new PackedDecimalData(15,2);
	public PackedDecimalData premAdjusted06 = new PackedDecimalData(15,2);
	public PackedDecimalData premAdjusted07 = new PackedDecimalData(15,2);
	public PackedDecimalData premAdjusted08 = new PackedDecimalData(15,2);
	//IBPLIFE-2435 end
	
	//Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models
	public PackedDecimalData znadjperc = new PackedDecimalData(5,2);
	/*Ticket #ILIFE-2005 - End*/
	/*ILIFE-2808 Code Promotion for VPMS externalization of LIFE RTA product calculations Start*/
	public FixedLengthStringData zszprmcd = new FixedLengthStringData(1);
	public PackedDecimalData prat = new PackedDecimalData(6,2);
	public FixedLengthStringData coverc = new FixedLengthStringData(2);
	public FixedLengthStringData trmofcontract = new FixedLengthStringData(2);
	public PackedDecimalData hpropdte = new PackedDecimalData(8,0);
	public FixedLengthStringData premind = new FixedLengthStringData(1);
	public PackedDecimalData tempprat = new PackedDecimalData(3,0);
	public PackedDecimalData temptoc = new PackedDecimalData(2,0);

	public PackedDecimalData noOfCoverages = new PackedDecimalData(1,0);
	public FixedLengthStringData aidsCoverInd = new FixedLengthStringData(1);
	public PackedDecimalData totalSumInsured = new PackedDecimalData(17,2);
	public FixedLengthStringData ncdCode = new FixedLengthStringData(2);
	public FixedLengthStringData claimAutoIncrease = new FixedLengthStringData(1);
	public FixedLengthStringData syndicateCode = new FixedLengthStringData(2);
	public FixedLengthStringData riskExpiryAge = new FixedLengthStringData(2);
	public PackedDecimalData pratNew = new PackedDecimalData(6,2);//ILIFE-7521

	/*Ticket #ILIFE-2808 - End*/
	
	public PackedDecimalData getPackData() {
		return new PackedDecimalData(6,0);
	}
	public void initialize() {
		COBOLFunctions.initialize(vpxlextRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxlextRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}