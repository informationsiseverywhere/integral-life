/*
 * File: Pr634.java
 * Date: 30 August 2009 1:50:06
 * Author: Quipoz Limited
 * 
 * Class transformed from PR634.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.productdefinition.dataaccess.ZmpdTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.screens.Sr634ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    Doctor Maintenance
*    ------------------
*    This program is used to maintain and enquire the doctor
*    details upon a medical provider's doctor details.
*
*    The doctor number is window-able to client scroll screen.
*
*    All details will be captured in Medical Provider Doctor
*    Details(ZMPD) file.
*
***********************************************************************
* </pre>
*/
public class Pr634 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR634");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaRecordFound = new FixedLengthStringData(1);
	private Validator recordFound = new Validator(wsaaRecordFound, "Y");

	private FixedLengthStringData wsaaDuplicateFound = new FixedLengthStringData(1);
	private Validator duplicateFound = new Validator(wsaaDuplicateFound, "Y");
	private PackedDecimalData wsaaPrvDteapp = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPrvDtetrm = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaSubfileRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaSubfileRrnD = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaTempZdoctor = new FixedLengthStringData(8);
		/* ERRORS */
	private String e038 = "E038";
	private String e048 = "E048";
	private String e983 = "E983";
	private String f393 = "F393";
	private String rl67 = "RL67";
		/* FORMATS */
	private String clntrec = "CLNTREC";
	private String clrrrec = "CLRRREC";
	private String zmpdrec = "ZMPDREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
		/*Client role and relationships logical fi*/
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Medical Provider Doctor Details*/
	private ZmpdTableDAM zmpdIO = new ZmpdTableDAM();
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Sr634ScreenVars sv = ScreenProgram.getScreenVars( Sr634ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1290, 
		preExit, 
		exit2090, 
		exit3090, 
		b1090Exit
	}

	public Pr634() {
		super();
		screenVars = sv;
		new ScreenModel("Sr634", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		a1000CallSr634io();
		scrnparams.subfileRrn.set(1);
		scrnparams.subfileMore.set("Y");
		zmpvIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		sv.clntnum.set(zmpvIO.getZmednum());
		sv.zmedprv.set(zmpvIO.getZmedprv());
		sv.clntnam.set(subString(zmpvIO.getZmednam(), 1, 30));
		wsaaPrvDteapp.set(zmpvIO.getDteapp());
		wsaaPrvDtetrm.set(zmpvIO.getDtetrm());
		zmpdIO.setParams(SPACES);
		zmpdIO.setStatuz(varcom.oK);
		zmpdIO.setZmedcoy(wsspcomn.company);
		zmpdIO.setZmedprv(zmpvIO.getZmedprv());
		zmpdIO.setSeqno(1);
		zmpdIO.setFormat(zmpdrec);
		zmpdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zmpdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zmpdIO.setFitKeysSearch("ZMEDCOY", "ZMEDPRV");
		zmpdIO.setStatuz(varcom.oK);
		wsaaCount.set(ZERO);
		while ( !(isEQ(zmpdIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaCount,sv.subfilePage))) {
			loadSubfile1200();
		}
		
		compute(wsaaRemainder, 0).set(sub(sv.subfilePage,wsaaCount));
		for (ix.set(1); !(isGT(ix,wsaaRemainder)); ix.add(1)){
			loadRemainder1300();
		}
		if (isEQ(wsspcomn.flag,"I")
		&& isEQ(sv.zdoctor,SPACES)) {
			scrnparams.subfileMore.set("N");
		}
	}

protected void loadSubfile1200()
	{
		try {
			begin1210();
		}
		catch (GOTOException e){
		}
	}

protected void begin1210()
	{
		SmartFileCode.execute(appVars, zmpdIO);
		if (isNE(zmpdIO.getStatuz(),varcom.oK)
		&& isNE(zmpdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zmpdIO.getParams());
			syserrrec.statuz.set(zmpdIO.getStatuz());
			fatalError600();
		}
		if (isNE(zmpdIO.getStatuz(),varcom.oK)
		|| isNE(zmpdIO.getZmedcoy(),wsspcomn.company)
		|| isNE(zmpdIO.getZmedprv(),zmpvIO.getZmedprv())) {
			zmpdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		sv.zdoctor.set(zmpdIO.getZdocnum());
		sv.zmmccde.set(zmpdIO.getZmmccde());
		sv.effdate.set(zmpdIO.getEffdate());
		sv.dtetrm.set(zmpdIO.getDtetrm());
		sv.hflag.set("Y");
		if (isNE(sv.zdoctor,SPACES)) {
			sv.name.set(SPACES);
			b1000CallClnt();
		}
		wsaaCount.add(1);
		if (isEQ(wsaaCount,sv.subfilePage)) {
			scrnparams.subfileMore.set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		a1000CallSr634io();
		zmpdIO.setFunction(varcom.nextr);
	}

protected void loadRemainder1300()
	{
		/*BEGIN*/
		sv.zdoctor.set(SPACES);
		sv.name.set(SPACES);
		sv.zmmccde.set(SPACES);
		sv.hflag.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.dtetrm.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sadd);
		a1000CallSr634io();
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isNE(wsaaPrvDtetrm,varcom.vrcmMaxDate)
		|| isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set("PROT");
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaCount.set(ZERO);
			while ( !(isEQ(zmpdIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaCount,sv.subfilePage))) {
				loadSubfile1200();
			}
			
			compute(wsaaRemainder, 0).set(sub(sv.subfilePage,wsaaCount));
			for (ix.set(1); !(isGT(ix,wsaaRemainder)); ix.add(1)){
				loadRemainder1300();
			}
			scrnparams.subfileMore.set("Y");
			wsspcomn.edterror.set("Y");
			if (isEQ(wsspcomn.flag,"I")
			&& isEQ(sv.zdoctor,SPACES)) {
				scrnparams.subfileMore.set("N");
			}
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		a1000CallSr634io();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		checkDuplicate2700();
	}

protected void validateSubfile2600()
	{
		validation2610();
		updateErrorIndicators2670();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		if (isEQ(sv.zdoctor,SPACES)) {
			sv.name.set(SPACES);
		}
		if (isNE(sv.zdoctor,SPACES)
		&& (isEQ(sv.zmmccde,SPACES)
		|| isEQ(sv.effdate,varcom.vrcmMaxDate))) {
			sv.zdoctorErr.set(e038);
			sv.zmmccdeErr.set(e038);
			sv.effdateErr.set(e038);
		}
		if (isNE(sv.zmmccde,SPACES)
		&& (isEQ(sv.zdoctor,SPACES)
		|| isEQ(sv.effdate,varcom.vrcmMaxDate))) {
			sv.zdoctorErr.set(e038);
			sv.zmmccdeErr.set(e038);
			sv.effdateErr.set(e038);
		}
		if (isNE(sv.effdate,varcom.vrcmMaxDate)
		&& (isEQ(sv.zdoctor,SPACES)
		|| isEQ(sv.zmmccde,SPACES))) {
			sv.zdoctorErr.set(e038);
			sv.zmmccdeErr.set(e038);
			sv.effdateErr.set(e038);
		}
		if (isNE(sv.dtetrm,varcom.vrcmMaxDate)
		&& (isEQ(sv.zdoctor,SPACES)
		|| isEQ(sv.zmmccde,SPACES)
		|| isEQ(sv.effdate,varcom.vrcmMaxDate))) {
			sv.zdoctorErr.set(e038);
			sv.zmmccdeErr.set(e038);
			sv.effdateErr.set(e038);
			sv.dtetrmErr.set(e038);
		}
		if (isNE(sv.zdoctor,SPACES)) {
			sv.name.set(SPACES);
			b1000CallClnt();
		}
		if (isGT(sv.effdate,sv.dtetrm)) {
			sv.dtetrmErr.set(e983);
		}
		if (isLT(sv.effdate,wsaaPrvDteapp)) {
			sv.effdateErr.set(rl67);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR634", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SR634", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkDuplicate2700()
	{
		begin2710();
	}

protected void begin2710()
	{
		wsaaSubfileRrn.set(1);
		scrnparams.subfileRrn.set(wsaaSubfileRrn);
		scrnparams.function.set(varcom.sread);
		a1000CallSr634io();
		wsaaDuplicateFound.set("N");
		while ( !(isNE(scrnparams.statuz,varcom.oK)
		|| duplicateFound.isTrue())) {
			wsaaTempZdoctor.set(sv.zdoctor);
			if (isNE(sv.zdoctor,SPACES)) {
				wsaaSubfileRrnD.set(wsaaSubfileRrn);
				wsaaSubfileRrnD.add(1);
				scrnparams.subfileRrn.set(wsaaSubfileRrnD);
				scrnparams.function.set(varcom.sread);
				a1000CallSr634io();
				while ( !(isNE(scrnparams.statuz,varcom.oK)
				|| duplicateFound.isTrue())) {
					if (isNE(wsaaSubfileRrn,scrnparams.subfileRrn)
					&& isEQ(wsaaTempZdoctor,sv.zdoctor)) {
						wsspcomn.edterror.set("Y");
						wsaaDuplicateFound.set("Y");
						sv.zdoctorErr.set(e048);
						if (isNE(sv.errorSubfile,SPACES)) {
							wsspcomn.edterror.set("Y");
						}
						scrnparams.function.set(varcom.supd);
						a1000CallSr634io();
					}
					else {
						wsaaSubfileRrnD.add(1);
						scrnparams.subfileRrn.set(wsaaSubfileRrnD);
						scrnparams.function.set(varcom.sread);
						a1000CallSr634io();
					}
				}
				
			}
			if (!duplicateFound.isTrue()) {
				wsaaSubfileRrn.add(1);
				scrnparams.subfileRrn.set(wsaaSubfileRrn);
				scrnparams.function.set(varcom.sread);
				a1000CallSr634io();
			}
		}
		
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		a1000CallSr634io();
		wsaaSeqno.set(1);
		while ( !(isNE(scrnparams.statuz,varcom.oK))) {
			if (isNE(sv.zdoctor,SPACES)) {
				zmpdIO.setDataKey(SPACES);
				zmpdIO.setZmedcoy(wsspcomn.company);
				zmpdIO.setZmedprv(sv.zmedprv);
				zmpdIO.setSeqno(wsaaSeqno);
				zmpdIO.setFunction(varcom.readh);
				b2000CallZmpd();
				zmpdIO.setFunction(varcom.updat);
				zmpdIO.setZdocnum(sv.zdoctor);
				zmpdIO.setZmmccde(sv.zmmccde);
				zmpdIO.setEffdate(sv.effdate);
				zmpdIO.setDtetrm(sv.dtetrm);
				b2000CallZmpd();
				wsaaSeqno.add(1);
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("SR634", sv);
			if (isNE(scrnparams.statuz,varcom.oK)
			&& isNE(scrnparams.statuz,varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		zmpdIO.setSeqno(wsaaSeqno);
		zmpdIO.setFunction(varcom.begnh);
		b2000CallZmpd();
		while ( !(isNE(zmpdIO.getStatuz(),varcom.oK)
		|| isNE(zmpdIO.getZmedcoy(),wsspcomn.company)
		|| isNE(zmpdIO.getZmedprv(),sv.zmedprv))) {
			zmpdIO.setFunction(varcom.delet);
			b2000CallZmpd();
			zmpdIO.setFunction(varcom.nextr);
			b2000CallZmpd();
		}
		
		zmpdIO.setFunction(varcom.rlse);
		b2000CallZmpd();
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		scrnparams.function.set("HIDEW");
		processScreen("SR634", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a1000CallSr634io()
	{
		/*A1010-BEGIN*/
		processScreen("SR634", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*A1090-EXIT*/
	}

protected void b1000CallClnt()
	{
		try {
			b1010Begin();
		}
		catch (GOTOException e){
		}
	}

protected void b1010Begin()
	{
		clntIO.setDataKey(SPACES);
		clntIO.setClntpfx(fsupfxcpy.clnt);
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setClntnum(sv.zdoctor);
		clntIO.setFormat(clntrec);
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)
		&& isNE(clntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clntIO.getParams());
			syserrrec.statuz.set(clntIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clntIO.getStatuz(),varcom.mrnf)) {
			sv.zdoctorErr.set(f393);
			goTo(GotoLabel.b1090Exit);
		}
		else {
			b3000CheckRole();
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(clntIO.getGivname(), "  "));
			sv.name.setLeft(stringVariable1.toString());
		}
	}

protected void b2000CallZmpd()
	{
		/*B2010-BEGIN*/
		zmpdIO.setFormat(zmpdrec);
		SmartFileCode.execute(appVars, zmpdIO);
		if (isNE(zmpdIO.getStatuz(),varcom.oK)
		&& isNE(zmpdIO.getStatuz(),varcom.endp)
		&& isNE(zmpdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(zmpdIO.getParams());
			syserrrec.statuz.set(zmpdIO.getStatuz());
			fatalError600();
		}
		/*B2090-EXIT*/
	}

protected void b3000CheckRole()
	{
		b3010Begin();
	}

protected void b3010Begin()
	{
		clrrIO.setDataKey(SPACES);
		clrrIO.setClntpfx(fsupfxcpy.clnt);
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClntnum(sv.zdoctor);
		clrrIO.setForenum(sv.zdoctor);
		clrrIO.setClrrrole(clntrlsrec.doctor);
		clrrIO.setForepfx(clntrlsrec.doctor);
		clrrIO.setForecoy(wsspcomn.company);
		clrrIO.setFormat(clrrrec);
		clrrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)
		&& isNE(clrrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clrrIO.getParams());
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clrrIO.getStatuz(),varcom.mrnf)) {
			sv.zdoctorErr.set(f393);
		}
	}
}
