package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr633screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {41, 31, 18, 2, 4, 17, 5, 15, 13, 1, 14, 11, 3, 21, 41, 70, 50}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {16, 21, 4, 71}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr633ScreenVars sv = (Sr633ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr633screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr633screensfl, 
			sv.Sr633screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr633ScreenVars sv = (Sr633ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr633screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr633ScreenVars sv = (Sr633ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr633screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr633screensflWritten.gt(0))
		{
			sv.sr633screensfl.setCurrentIndex(0);
			sv.Sr633screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr633ScreenVars sv = (Sr633ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr633screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr633ScreenVars screenVars = (Sr633ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hflag.setFieldName("hflag");
				screenVars.value.setFieldName("value");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.zmedfee.setFieldName("zmedfee");
				screenVars.zmedtyp.setFieldName("zmedtyp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hflag.set(dm.getField("hflag"));
			screenVars.value.set(dm.getField("value"));
			screenVars.longdesc.set(dm.getField("longdesc"));
			screenVars.zmedfee.set(dm.getField("zmedfee"));
			screenVars.zmedtyp.set(dm.getField("zmedtyp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr633ScreenVars screenVars = (Sr633ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hflag.setFieldName("hflag");
				screenVars.value.setFieldName("value");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.zmedfee.setFieldName("zmedfee");
				screenVars.zmedtyp.setFieldName("zmedtyp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hflag").set(screenVars.hflag);
			dm.getField("value").set(screenVars.value);
			dm.getField("longdesc").set(screenVars.longdesc);
			dm.getField("zmedfee").set(screenVars.zmedfee);
			dm.getField("zmedtyp").set(screenVars.zmedtyp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr633screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr633ScreenVars screenVars = (Sr633ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hflag.clearFormatting();
		screenVars.value.clearFormatting();
		screenVars.longdesc.clearFormatting();
		screenVars.zmedfee.clearFormatting();
		screenVars.zmedtyp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr633ScreenVars screenVars = (Sr633ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hflag.setClassString("");
		screenVars.value.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.zmedfee.setClassString("");
		screenVars.zmedtyp.setClassString("");
	}

/**
 * Clear all the variables in Sr633screensfl
 */
	public static void clear(VarModel pv) {
		Sr633ScreenVars screenVars = (Sr633ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hflag.clear();
		screenVars.value.clear();
		screenVars.longdesc.clear();
		screenVars.zmedfee.clear();
		screenVars.zmedtyp.clear();
	}
}
