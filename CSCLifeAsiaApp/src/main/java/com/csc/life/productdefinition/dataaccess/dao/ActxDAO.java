package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Br523DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ActxDAO extends BaseDAO<Br523DTO> {
    public List<Br523DTO> searchActxResult(String tableId, String memName, int batchExtractSize, int batchID);
}