package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:45
 * Description:
 * Copybook name: MLIACHDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mliachdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mliachdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mliachdKey = new FixedLengthStringData(64).isAPartOf(mliachdFileKey, 0, REDEFINE);
  	public FixedLengthStringData mliachdMlentity = new FixedLengthStringData(16).isAPartOf(mliachdKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(mliachdKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mliachdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mliachdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}