package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datadictionarydatatype.FLSDDObj;
import com.quipoz.framework.datatype.*;

/**
 * Screen variables for SR50D
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sr50dScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(425); //408
	public FixedLengthStringData dataFields = new FixedLengthStringData(217).isAPartOf(dataArea, 0); 
	public FixedLengthStringData optdsc = DD.optdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData desc = DD.desca.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData jlnames = new FixedLengthStringData(96).isAPartOf(dataFields, 89);
	public FixedLengthStringData[] jlname = FLSArrayPartOfStructure(2, 48, jlnames, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(96).isAPartOf(jlnames, 0, FILLER_REDEFINE);
	public FixedLengthStringData jlname01 = DD.jlname.copy().isAPartOf(filler,0);
	public FixedLengthStringData jlname02 = DD.jlname.copy().isAPartOf(filler,48);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData pnotecat = DD.pnotecat.copy().isAPartOf(dataFields,193);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,206);
	public StringData word = new StringData(); 
	public FixedLengthStringData feature = DD.lddslvl.copy().isAPartOf(dataFields,216);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 217);
	public FixedLengthStringData optdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData descaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlnamesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] jlnameErr = FLSArrayPartOfStructure(2, 4, jlnamesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(jlnamesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData jlname01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData jlname02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData pnotecatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);	
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData wordErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 269);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] descaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData jlnamesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] jlnameOut = FLSArrayPartOfStructure(2, 12, jlnamesOut, 0);
	public FixedLengthStringData[][] jlnameO = FLSDArrayPartOfArrayStructure(12, 1, jlnameOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(jlnamesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] jlname01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] jlname02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] pnotecatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] wordOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(96);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(78).isAPartOf(subfileArea, 0);
	public FixedLengthStringData message = DD.message.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(4).isAPartOf(subfileArea, 78);
	public FixedLengthStringData messageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(12).isAPartOf(subfileArea, 82);
	public FixedLengthStringData[] messageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 94);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr50dscreensflWritten = new LongData(0);
	public LongData Sr50dscreenctlWritten = new LongData(0);
	public LongData Sr50dscreenWritten = new LongData(0);
	public LongData Sr50dprotectWritten = new LongData(0);
	public GeneralTable sr50dscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50dscreensfl;
	}

	public Sr50dScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {message};
		screenSflOutFields = new BaseData[][] {messageOut};
		screenSflErrFields = new BaseData[] {messageErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, rstate, cownnum, lifcnum, pnotecat, jlname01, jlname02, desc, pstate, cnttype, ctypdesc, optdsc,word};
		screenOutFields = new BaseData[][] {chdrnumOut, rstateOut, cownnumOut, lifcnumOut, pnotecatOut, jlname01Out, jlname02Out, descaOut, pstateOut, cnttypeOut, ctypdescOut, optdscOut,wordOut};
		screenErrFields = new BaseData[] {chdrnumErr, rstateErr, cownnumErr, lifcnumErr, pnotecatErr, jlname01Err, jlname02Err, descaErr, pstateErr, cnttypeErr, ctypdescErr, optdscErr,wordErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50dscreen.class;
		screenSflRecord = Sr50dscreensfl.class;
		screenCtlRecord = Sr50dscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50dprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50dscreenctl.lrec.pageSubfile);
	}
}
