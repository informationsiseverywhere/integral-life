/*
 * File: Pd5g6.java
 * Date: 30 August 2009 1:48:03
 * Author: Quipoz Limited
 * 
 * Class transformed from Pd5g6.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.common.constants.SmartTableConstants;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.ChdrUndrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.UndrpfDAO;
import com.csc.fsu.general.dataaccess.model.Undrpf;
import com.csc.life.productdefinition.recordstructures.Rskamtrec;
import com.csc.life.productdefinition.screens.Sd5g6ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Td5g4rec;
import com.csc.life.productdefinition.tablestructures.Td5gnrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.utils.ListUtils;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This program is called from PR586 by receiving its parameters,
* displays them and retrieve thier detailed information from
* UNDRPRP logical file.
*
*****************************************************************
* </pre>
*/
public class Pd5g6 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private BinaryData wsaaSflct = new BinaryData(5, 0);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5G6");
	private FixedLengthStringData wsaatranCode = new FixedLengthStringData(5).init("TAFG");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Wsspsmart wsspsmart = new Wsspsmart();
    private FixedLengthStringData filler = new FixedLengthStringData(767).isAPartOf(wsspsmart.userArea, 0, FILLER_REDEFINE);// Updated by Tidy
	private FixedLengthStringData wsspLrkcls = new FixedLengthStringData(4).isAPartOf(filler, 3);
	private PackedDecimalData wsspTotsi = new PackedDecimalData(17, 2).isAPartOf(filler, 7);
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData wsaaValid = new FixedLengthStringData(1);
	private Sd5g6ScreenVars sv = ScreenProgram.getScreenVars( Sd5g6ScreenVars.class);
	private UndrpfDAO undrpfDao = getApplicationContext().getBean("UndrpfDAO", UndrpfDAO.class);
	private Undrpf undrpf = new Undrpf();
	private final DescDAO descDao = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf descpf;
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Td5gnrec td5gnrec = new Td5gnrec();
	private Td5g4rec td5g4rec = new Td5g4rec();
	private Rskamtrec rskamtrec =new Rskamtrec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);//ILIFE-3955
	private Itempf itempf = null;
	private T5679rec T5679rec = new T5679rec();
	
	private enum GotoLabel implements GOTOInterface {
		preExit, 
		exit2090
	}

	public Pd5g6() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5g6", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		
	}

protected void initialise1010()
	{
	/* start TMLII-429 UW-01-008 */
	wsaaBatckey.set(wsspcomn.batchkey);

	/* end TMLII-429 UW-01-008 */
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("Sd5g6", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.clntnum.set(wsspsmart.flddkey);
		moveClntnam9100();
		sv.lrkcls.set(wsspLrkcls);
		moveLdesc9200();
		sv.tranamt.set(wsspTotsi);
		wsaaSflct.set(ZERO);
		loadSubfile1200();
				
	}

protected void loadSubfile1200()
	{
		try {
			ctrl1200();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl1200()
	{
	
		initialize(sv.subfileFields);
		ChdrUndrpf chdrundrpf = new ChdrUndrpf();
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		List<Itempf> tD5GNItemList = null;
		tD5GNItemList = itemDAO.loadItdmTable(SmartTableConstants.ITEMPFX_IT,wsspcomn.company.toString(),"TD5GN");
		Map<String,String> td5gnFormMap = new HashMap<String,String>();
		for(Itempf itempfform:tD5GNItemList){
	
			td5gnrec.td5gnRec.set(StringUtil.rawToString(itempfform.getGenarea()));
			td5gnFormMap.put(itempfform.getItemitem().trim(),td5gnrec.risksubr.toString());
		}
	
		List<Itempf> itemTd5g4= itemDAO.loadItdmTable(SmartTableConstants.ITEMPFX_IT,wsspcomn.company.toString(),"TD5G4");
		//Set into REC
		String formula="";
		
		HashMap<String,HashMap<String,String>> formulaMap = new HashMap<String, HashMap<String,String>>();
		for (Itempf itempftmp : itemTd5g4) {
			int i = 0;
			td5g4rec.td5g4Rec.set(StringUtil.rawToString(itempftmp.getGenarea()));
			HashMap<String, String> riskMap = new HashMap<String, String>();
			for (FixedLengthStringData riskclass : td5g4rec.riskclass) {
			if (riskclass != null && riskclass.toString().equalsIgnoreCase(wsspLrkcls.toString())) {
					formula = td5g4rec.formula[i].toString();
					riskMap.put(riskclass.toString(), td5gnFormMap.get(formula.trim()));
				}
				i++;
				formulaMap.put(itempftmp.getItemitem(), riskMap);
			}
		}
		
		itempf = new Itempf();
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5679",wsaatranCode.toString().trim());

		if (itempf == null ) {
			fatalError600();
		}
		T5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		//chdrundrpf.setLrkcls01(wsspLrkcls.toString().trim());
		chdrundrpf.setClntnum(wsspsmart.flddkey.toString().trim());
		chdrundrpf.setCompany(wsspcomn.company.toString());
		List<String> statcode = new ArrayList<String>(); 
		for(int stat=0;stat<T5679rec.cnRiskStat.length;stat++){
			if(T5679rec.cnRiskStat[stat]!=null && !T5679rec.cnRiskStat[stat].toString().trim().equalsIgnoreCase(""))
			statcode.add(T5679rec.cnRiskStat[stat].toString());
		}
		List<ChdrUndrpf> chdrundrpfList = undrpfDao.readInsuredRiskAmount(chdrundrpf,statcode);
		for (ChdrUndrpf chdrUndrpf:chdrundrpfList){
			initialize(sv.subfileFields);
			sv.chdrnum.set(chdrUndrpf.getChdrnum());
			sv.statuz.set(chdrUndrpf.getStatcode());
			sv.compdesc.set(descDao.getdescData("IT", "T5687", chdrUndrpf.getCrtable(), wsspcomn.company.toString(), wsspcomn.language.toString()).getLongdesc());
			sv.riskCessTerm.set(chdrUndrpf.getRiskterm());
			sv.premCessTerm.set(chdrUndrpf.getPremterm());
			sv.compcode.set(chdrUndrpf.getCrtable());
			sv.payfreq.set(chdrUndrpf.getBillfreq());
			sv.modeprem.set(chdrUndrpf.getModepremium());
			sv.sumin.set(chdrUndrpf.getSumins());
			rskamtrec.chdrcoy.set(chdrundrpf.getCompany());
			rskamtrec.compcode.set(chdrUndrpf.getCrtable());
			rskamtrec.lrkcls.set(wsspLrkcls.toString().trim());
			rskamtrec.premCessTerm.set(chdrUndrpf.getPremterm());
			rskamtrec.riskCessTerm.set(chdrUndrpf.getRiskterm());
			rskamtrec.statuz.set(chdrUndrpf.getStatcode());
			rskamtrec.sumin.set(chdrUndrpf.getSumins());
			rskamtrec.modeprem.set(chdrUndrpf.getModepremium());
			rskamtrec.riskcommDate.set(new BigDecimal(chdrUndrpf.getOccdate()));
			rskamtrec.billingfreq.set(chdrUndrpf.getBillfreq());
			sv.riskamnt.set(SPACE);
			if(formulaMap.get(chdrUndrpf.getCrtable())!=null && formulaMap.get(chdrUndrpf.getCrtable()).get(wsspLrkcls.toString())!=null){
			callProgram(formulaMap.get(chdrUndrpf.getCrtable()).get(wsspLrkcls.toString()).trim() , rskamtrec.rskamtRec);
			sv.riskamnt.set(rskamtrec.riskamnt);
			addsubfile();
			}
			
		}

	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
	   if (isEQ(wsspcomn.flag, "I")) {
		scrnparams.function.set(varcom.prot);
	   	}
	}

protected void validateScreen2010()
	{
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("Sd5g6", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("Sd5g6", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("Sd5g6", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.subtract(1);
		/*EXIT*/
	}



protected void moveClntnam9100()
	{
		ctrl9100();
	}

protected void ctrl9100()
	{
	
		clntpf = clntpfDAO.findClientByClntnum(wsspsmart.flddkey.toString());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
		sv.clntnam.setLeft(stringVariable1.toString());
	}

protected void moveLdesc9200()
	{
		ctrl9200();
	}

protected void ctrl9200()
	{
	descpf=descDao.getdescData("IT", "T5446", wsspLrkcls.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			fatalError600();
		}
		sv.ldesc.set(descpf.getLongdesc());
	}
protected void addsubfile(){
	scrnparams.function.set(varcom.sadd); 
	processScreen("SD5G6", sv);	
}
protected void exit2090() {
	if (!sv.errorIndicators.equals(SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/* EXIT */
	/** UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION */
} 

protected void screenEdit2000()
{
	try {
		screenIo2010();
	}
	catch (GOTOException e){
	}
}
protected void screenIo2010()
{
	if (isNE(scrnparams.statuz,varcom.rolu)) {
		goTo(GotoLabel.exit2090);
	}
	wsaaSflct.set(ZERO);

	wsspcomn.edterror.set("Y");
	goTo(GotoLabel.exit2090);

}
}
