package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZmpdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:59
 * Class transformed from ZMPDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZmpdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 87;
	public FixedLengthStringData zmpdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zmpdpfRecord = zmpdrec;
	
	public FixedLengthStringData zmedcoy = DD.zmedcoy.copy().isAPartOf(zmpdrec);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(zmpdrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(zmpdrec);
	public FixedLengthStringData zdocnum = DD.zdocnum.copy().isAPartOf(zmpdrec);
	public FixedLengthStringData zmmccde = DD.zmmccde.copy().isAPartOf(zmpdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(zmpdrec);
	public PackedDecimalData dtetrm = DD.dtetrm.copy().isAPartOf(zmpdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zmpdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zmpdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zmpdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZmpdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZmpdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZmpdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZmpdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZmpdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZMPDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ZMEDCOY, " +
							"ZMEDPRV, " +
							"SEQNO, " +
							"ZDOCNUM, " +
							"ZMMCCDE, " +
							"EFFDATE, " +
							"DTETRM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     zmedcoy,
                                     zmedprv,
                                     seqno,
                                     zdocnum,
                                     zmmccde,
                                     effdate,
                                     dtetrm,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		zmedcoy.clear();
  		zmedprv.clear();
  		seqno.clear();
  		zdocnum.clear();
  		zmmccde.clear();
  		effdate.clear();
  		dtetrm.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZmpdrec() {
  		return zmpdrec;
	}

	public FixedLengthStringData getZmpdpfRecord() {
  		return zmpdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZmpdrec(what);
	}

	public void setZmpdrec(Object what) {
  		this.zmpdrec.set(what);
	}

	public void setZmpdpfRecord(Object what) {
  		this.zmpdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zmpdrec.getLength());
		result.set(zmpdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}