package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6598
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6598ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(65).isAPartOf(dataArea, 0);
	public FixedLengthStringData addprocess = DD.addprocess.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData calcprog = DD.calcprog.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData procesprog = DD.procesprog.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 65);
	public FixedLengthStringData addprocessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData calcprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData procesprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 93);
	public FixedLengthStringData[] addprocessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] calcprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] procesprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6598screenWritten = new LongData(0);
	public LongData S6598protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6598ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, calcprog, procesprog, addprocess};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, calcprogOut, procesprogOut, addprocessOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, calcprogErr, procesprogErr, addprocessErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6598screen.class;
		protectRecord = S6598protect.class;
	}

}
