package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.csc.life.productdefinition.dataaccess.dao.MrtpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MrtpfDAOImpl extends BaseDAOImpl implements MrtpfDAO {
private static final Logger LOGGER = LoggerFactory.getLogger(MrtpfDAOImpl.class);
    
	public Integer getMBNSData(String chdrnum, String yrsinf) {
		
		
	int sumins = 0;
	StringBuilder query = new StringBuilder("SELECT SURRVAL FROM MBNSPF WHERE CHDRNUM =? AND YRSINF = ?"); /* ILIFE-3723*/
	
	PreparedStatement stmt = null;
	ResultSet rs = null;
	

	try {
		stmt = getPrepareStatement(query.toString());
		stmt.setString(1, chdrnum);
		stmt.setString(2, yrsinf);
		rs = stmt.executeQuery();

		while (rs.next()) {
			sumins = rs.getInt(1);

		}	
	}	
	catch (SQLException e) {
		LOGGER.error("getMBNSData()", e);//IJTI-1561
		throw new SQLRuntimeException(e);
	} finally {
		close(stmt, rs);			
	}	
		
		return sumins;
	}


}
