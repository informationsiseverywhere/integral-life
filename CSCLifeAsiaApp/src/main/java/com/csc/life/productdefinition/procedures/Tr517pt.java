/*
 * File: Tr517pt.java
 * Date: 30 August 2009 2:41:44
 * Author: Quipoz Limited
 * 
 * Class transformed from TR517PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR517.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr517pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine001, 32, FILLER).init("Waiver of Premium                      SR517");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 26, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine003, 0, FILLER).init("    Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 18);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 28, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(47);
	private FixedLengthStringData filler9 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Waive itself  . . . . . . . . . .");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 36);
	private FixedLengthStringData filler10 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 37, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 42, FILLER).init("(Y/N)");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(47);
	private FixedLengthStringData filler12 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine005, 0, FILLER).init("  Waive all life  . . . . . . . . .");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 36);
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 37, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 42, FILLER).init("(Y/N)");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(47);
	private FixedLengthStringData filler15 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  Waive Policy Fee. . . . . . . . .");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 36);
	private FixedLengthStringData filler16 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 37, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 42, FILLER).init("(Y/N)");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(47);
	private FixedLengthStringData filler18 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  Accelerated Crisis Waiver . . . .");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 36);
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 42, FILLER).init("(Y/N)");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(36);
	private FixedLengthStringData filler21 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  Waive the following covers/riders:");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(78);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 2);
	private FixedLengthStringData filler23 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 10);
	private FixedLengthStringData filler24 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 18);
	private FixedLengthStringData filler25 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 26);
	private FixedLengthStringData filler26 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 34);
	private FixedLengthStringData filler27 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 42);
	private FixedLengthStringData filler28 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 50);
	private FixedLengthStringData filler29 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 58);
	private FixedLengthStringData filler30 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 66);
	private FixedLengthStringData filler31 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 74);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(78);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 2);
	private FixedLengthStringData filler33 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 10);
	private FixedLengthStringData filler34 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 18);
	private FixedLengthStringData filler35 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 26);
	private FixedLengthStringData filler36 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 34);
	private FixedLengthStringData filler37 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 42);
	private FixedLengthStringData filler38 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 50);
	private FixedLengthStringData filler39 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 58);
	private FixedLengthStringData filler40 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 66);
	private FixedLengthStringData filler41 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 74);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(78);
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 2);
	private FixedLengthStringData filler43 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 10);
	private FixedLengthStringData filler44 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 18);
	private FixedLengthStringData filler45 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 26);
	private FixedLengthStringData filler46 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 34);
	private FixedLengthStringData filler47 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 42);
	private FixedLengthStringData filler48 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 50);
	private FixedLengthStringData filler49 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 58);
	private FixedLengthStringData filler50 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 66);
	private FixedLengthStringData filler51 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 74);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(78);
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 2);
	private FixedLengthStringData filler53 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 10);
	private FixedLengthStringData filler54 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 18);
	private FixedLengthStringData filler55 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 26);
	private FixedLengthStringData filler56 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 34);
	private FixedLengthStringData filler57 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 42);
	private FixedLengthStringData filler58 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 50);
	private FixedLengthStringData filler59 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 58);
	private FixedLengthStringData filler60 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 66);
	private FixedLengthStringData filler61 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 74);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(78);
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 2);
	private FixedLengthStringData filler63 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 10);
	private FixedLengthStringData filler64 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 18);
	private FixedLengthStringData filler65 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 26);
	private FixedLengthStringData filler66 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 34);
	private FixedLengthStringData filler67 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 42);
	private FixedLengthStringData filler68 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 50);
	private FixedLengthStringData filler69 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 58);
	private FixedLengthStringData filler70 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 66);
	private FixedLengthStringData filler71 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 74);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(54);
	private FixedLengthStringData filler72 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler73 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine014, 26, FILLER).init("Continuation Item:");
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 46);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr517rec tr517rec = new Tr517rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr517pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr517rec.tr517Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tr517rec.zrwvflg01);
		fieldNo008.set(tr517rec.zrwvflg02);
		fieldNo011.set(tr517rec.ctable01);
		fieldNo012.set(tr517rec.ctable02);
		fieldNo013.set(tr517rec.ctable03);
		fieldNo014.set(tr517rec.ctable04);
		fieldNo015.set(tr517rec.ctable05);
		fieldNo016.set(tr517rec.ctable06);
		fieldNo017.set(tr517rec.ctable07);
		fieldNo018.set(tr517rec.ctable08);
		fieldNo019.set(tr517rec.ctable09);
		fieldNo020.set(tr517rec.ctable10);
		fieldNo021.set(tr517rec.ctable11);
		fieldNo022.set(tr517rec.ctable12);
		fieldNo023.set(tr517rec.ctable13);
		fieldNo024.set(tr517rec.ctable14);
		fieldNo025.set(tr517rec.ctable15);
		fieldNo026.set(tr517rec.ctable16);
		fieldNo027.set(tr517rec.ctable17);
		fieldNo028.set(tr517rec.ctable18);
		fieldNo029.set(tr517rec.ctable19);
		fieldNo030.set(tr517rec.ctable20);
		fieldNo031.set(tr517rec.ctable21);
		fieldNo032.set(tr517rec.ctable22);
		fieldNo033.set(tr517rec.ctable23);
		fieldNo034.set(tr517rec.ctable24);
		fieldNo035.set(tr517rec.ctable25);
		fieldNo036.set(tr517rec.ctable26);
		fieldNo037.set(tr517rec.ctable27);
		fieldNo038.set(tr517rec.ctable28);
		fieldNo039.set(tr517rec.ctable29);
		fieldNo040.set(tr517rec.ctable30);
		fieldNo041.set(tr517rec.ctable31);
		fieldNo042.set(tr517rec.ctable32);
		fieldNo043.set(tr517rec.ctable33);
		fieldNo044.set(tr517rec.ctable34);
		fieldNo045.set(tr517rec.ctable35);
		fieldNo046.set(tr517rec.ctable36);
		fieldNo047.set(tr517rec.ctable37);
		fieldNo048.set(tr517rec.ctable38);
		fieldNo049.set(tr517rec.ctable39);
		fieldNo050.set(tr517rec.ctable40);
		fieldNo051.set(tr517rec.ctable41);
		fieldNo052.set(tr517rec.ctable42);
		fieldNo053.set(tr517rec.ctable43);
		fieldNo054.set(tr517rec.ctable44);
		fieldNo055.set(tr517rec.ctable45);
		fieldNo056.set(tr517rec.ctable46);
		fieldNo057.set(tr517rec.ctable47);
		fieldNo058.set(tr517rec.ctable48);
		fieldNo059.set(tr517rec.ctable49);
		fieldNo060.set(tr517rec.ctable50);
		fieldNo009.set(tr517rec.zrwvflg03);
		fieldNo010.set(tr517rec.zrwvflg04);
		fieldNo061.set(tr517rec.contitem);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
