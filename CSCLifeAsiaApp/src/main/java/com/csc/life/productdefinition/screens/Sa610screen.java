package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sa610screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa610ScreenVars sv = (Sa610ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa610screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sa610ScreenVars screenVars = (Sa610ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.language.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.longdesc.setClassString("");
		
		screenVars.fupcdes01.setClassString("");
		screenVars.fupcdes02.setClassString("");
		screenVars.fupcdes03.setClassString("");
		screenVars.fupcdes04.setClassString("");
		screenVars.fupcdes05.setClassString("");
		
		screenVars.occcode01.setClassString("");
		screenVars.occcode02.setClassString("");
		screenVars.occcode03.setClassString("");
		screenVars.occcode04.setClassString("");
		screenVars.occcode05.setClassString("");
		screenVars.occcode06.setClassString("");
		screenVars.occcode07.setClassString("");
		screenVars.occcode08.setClassString("");
		screenVars.occcode09.setClassString("");
		screenVars.occcode10.setClassString("");
		
		screenVars.occclassdes01.setClassString("");
		screenVars.occclassdes02.setClassString("");
		screenVars.occclassdes03.setClassString("");
		screenVars.occclassdes04.setClassString("");
		screenVars.occclassdes05.setClassString("");
		screenVars.occclassdes06.setClassString("");
		screenVars.occclassdes07.setClassString("");
		screenVars.occclassdes08.setClassString("");
		screenVars.occclassdes09.setClassString("");
		screenVars.occclassdes10.setClassString("");

	}

/**
 * Clear all the variables in Sa610screen
 */
	public static void clear(VarModel pv) {
		Sa610ScreenVars screenVars = (Sa610ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.language.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmtoDisp.clear();

		screenVars.longdesc.clear();
		screenVars.fupcdes01.clear();
		screenVars.fupcdes02.clear();
		screenVars.fupcdes03.clear();
		screenVars.fupcdes04.clear();
		screenVars.fupcdes05.clear();
		
		screenVars.occcode01.clear();
		screenVars.occcode02.clear();
		screenVars.occcode03.clear();
		screenVars.occcode04.clear();
		screenVars.occcode05.clear();
		screenVars.occcode06.clear();
		screenVars.occcode07.clear();
		screenVars.occcode08.clear();
		screenVars.occcode09.clear();
		screenVars.occcode10.clear();
		
		screenVars.occclassdes01.clear();
		screenVars.occclassdes02.clear();
		screenVars.occclassdes03.clear();
		screenVars.occclassdes04.clear();
		screenVars.occclassdes05.clear();
		screenVars.occclassdes06.clear();
		screenVars.occclassdes07.clear();
		screenVars.occclassdes08.clear();
		screenVars.occclassdes09.clear();
		screenVars.occclassdes10.clear();

	}
}
