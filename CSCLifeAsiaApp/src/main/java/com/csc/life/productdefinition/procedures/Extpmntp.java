/*
 * File: Extpmntp.java
 * Date: 29 August 2009 22:48:09
 * Author: Quipoz Limited
 *
 * Class transformed from EXTPMNTP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.productdefinition.recordstructures.Extprmrec;
import com.csc.smart.recordstructures.Varcom;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This Subroutine is for Extra Premium Loading on Parents
*   Child PDB Loading
*
*   It is called from various premium calculation methods such
*   as PRMPM01. This routine is defined in table TH549 and called
*   when the adjustment code in substandard file is equal to the
*   reason code in TH549.
*   The result EXPT-LOADING will be return to the calling
*   premium calculation method.
*
*   % Loading is passed from LEXT mortality percentage.
*   Freq factor is the modal facotr defined in TH606.
*   annual income beneift is passed from sum insured.
*   Term is CPRM-DURATION, the premium term of the coverage.
*
*    = {((0.5 * Freq Factor) / 2) * Annual Income Benefit *
*      (Term / 1000)} * (% Loading / 100)
*
*****************************************************************
* </pre>
*/
public class Extpmntp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Extprmrec extprmrec = new Extprmrec();
	private Varcom varcom = new Varcom();

	public Extpmntp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		extprmrec.parmRec = convertAndSetParam(extprmrec.parmRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main1000()
	{
		/*COMPUTE*/
		extprmrec.statuz.set(varcom.oK);
		compute(extprmrec.loading, 4).set(mult((mult(mult((div((mult(50,extprmrec.freqFactor)),2)),extprmrec.sumass),(div(extprmrec.term,1000)))),(div(extprmrec.percent,100))));
		/*EXIT*/
		exitProgram();
	}
}
