package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sr5aw
 * @version 1.0 generated on 13/02/17 3:14 PM
 * @author vdivisala
 */
public class Sr5awScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(210);
	public FixedLengthStringData dataFields = new FixedLengthStringData(50).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData cngfrmtaxpyr = DD.keya.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData cngfrmbrthctry = DD.keya.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData cngfrmnatlty = DD.keya.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData cngtotaxpyr = DD.keya.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData cngtobrthctry = DD.keya.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData cngtonatlty = DD.keya.copy().isAPartOf(dataFields,49);
		
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 50);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cngfrmtaxpyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cngfrmbrthctryErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cngfrmnatltyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cngtotaxpyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cngtobrthctryErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cngtonatltyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 90);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cngfrmtaxpyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cngfrmbrthctryOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cngfrmnatltyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cngtotaxpyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cngtobrthctryOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cngtonatltyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr5awscreenWritten = new LongData(0);
	public LongData Sr5awprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr5awScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(cngfrmtaxpyrOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cngfrmbrthctryOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cngfrmnatltyOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cngtotaxpyrOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cngtobrthctryOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cngtonatltyOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
				
		screenFields = new BaseData[] {company, tabl, item,longdesc,cngfrmtaxpyr,cngfrmbrthctry,cngfrmnatlty,cngtotaxpyr,cngtobrthctry,cngtonatlty};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut,longdescOut,cngfrmtaxpyrOut,cngfrmbrthctryOut,cngfrmnatltyOut,cngtotaxpyrOut,cngtobrthctryOut,cngtonatltyOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr,longdescErr,cngfrmtaxpyrErr,cngfrmbrthctryErr,cngfrmnatltyErr,cngtotaxpyrErr,cngtobrthctryErr,cngtonatltyErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr5awscreen.class;
		protectRecord = Sr5awprotect.class;
	}

}
