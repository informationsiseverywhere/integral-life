package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Coevpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CoevpfDAO extends BaseDAO<Coevpf> {
    public void insertCoevpfRecord(List<Coevpf> coevpfList);
}