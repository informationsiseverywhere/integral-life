package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sd5g4
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class Sd5g4ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1388);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(684).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData riskclasss = new FixedLengthStringData(40).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] riskclass = FLSArrayPartOfStructure(10, 4, riskclasss, 0);
	
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(riskclasss, 0, FILLER_REDEFINE);
	
	public FixedLengthStringData riskclass01 = DD.lrkcls.copy().isAPartOf(filler,0);
	public FixedLengthStringData riskclass02 = DD.lrkcls.copy().isAPartOf(filler,4);
	public FixedLengthStringData riskclass03 = DD.lrkcls.copy().isAPartOf(filler,8);
	public FixedLengthStringData riskclass04 = DD.lrkcls.copy().isAPartOf(filler,12);
	public FixedLengthStringData riskclass05 = DD.lrkcls.copy().isAPartOf(filler,16);
	public FixedLengthStringData riskclass06 = DD.lrkcls.copy().isAPartOf(filler,20);
	public FixedLengthStringData riskclass07 = DD.lrkcls.copy().isAPartOf(filler,24);
	public FixedLengthStringData riskclass08 = DD.lrkcls.copy().isAPartOf(filler,28);
	public FixedLengthStringData riskclass09 = DD.lrkcls.copy().isAPartOf(filler,32);
	public FixedLengthStringData riskclass10 = DD.lrkcls.copy().isAPartOf(filler,36);

	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,40);
	
	public FixedLengthStringData riskclassdescs = new FixedLengthStringData(450).isAPartOf(dataFields, 41);
	public FixedLengthStringData[] riskclassdesc = FLSArrayPartOfStructure(10, 45, riskclassdescs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(450).isAPartOf(riskclassdescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData riskclassdesc01 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,0);
	public FixedLengthStringData riskclassdesc02 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,45);
	public FixedLengthStringData riskclassdesc03 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,90);
	public FixedLengthStringData riskclassdesc04 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,135);
	public FixedLengthStringData riskclassdesc05 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,180);
	public FixedLengthStringData riskclassdesc06 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,225);
	public FixedLengthStringData riskclassdesc07 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,270);
	public FixedLengthStringData riskclassdesc08 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,315);
	public FixedLengthStringData riskclassdesc09 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,360);
	public FixedLengthStringData riskclassdesc10 = DD.lrkclsdscRisk.copy().isAPartOf(filler1,405);

	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,491);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,499);
	
	public FixedLengthStringData formulas = new FixedLengthStringData(100).isAPartOf(dataFields, 529);
	public FixedLengthStringData[] formula = FLSArrayPartOfStructure(10, 10, formulas, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(100).isAPartOf(formulas, 0, FILLER_REDEFINE);
	public FixedLengthStringData formula01 = DD.rskformula.copy().isAPartOf(filler2,0);
	public FixedLengthStringData formula02 = DD.rskformula.copy().isAPartOf(filler2,10);
	public FixedLengthStringData formula03 = DD.rskformula.copy().isAPartOf(filler2,20);
	public FixedLengthStringData formula04 = DD.rskformula.copy().isAPartOf(filler2,30);
	public FixedLengthStringData formula05 = DD.rskformula.copy().isAPartOf(filler2,40);
	public FixedLengthStringData formula06 = DD.rskformula.copy().isAPartOf(filler2,50);
	public FixedLengthStringData formula07 = DD.rskformula.copy().isAPartOf(filler2,60);
	public FixedLengthStringData formula08 = DD.rskformula.copy().isAPartOf(filler2,70);
	public FixedLengthStringData formula09 = DD.rskformula.copy().isAPartOf(filler2,80);
	public FixedLengthStringData formula10 = DD.rskformula.copy().isAPartOf(filler2,90);

	public FixedLengthStringData factors = new FixedLengthStringData(50).isAPartOf(dataFields, 629);
	public ZonedDecimalData[] factor = ZDArrayPartOfStructure(10, 5,2, factors, 0); 
	public FixedLengthStringData filler3 = new FixedLengthStringData(50).isAPartOf(factors, 0, FILLER_REDEFINE);
	
	public ZonedDecimalData factor11 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData factor02 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,5);
	public ZonedDecimalData factor03 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,10);
	public ZonedDecimalData factor04 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,15);
	public ZonedDecimalData factor05 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,20);
	public ZonedDecimalData factor06 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,25);
	public ZonedDecimalData factor07 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,30);
	public ZonedDecimalData factor08 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,35);
	public ZonedDecimalData factor09 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,40);
	public ZonedDecimalData factor10 = DD.riskfactor.copyToZonedDecimal().isAPartOf(filler3,45);

	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,679);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(176).isAPartOf(dataArea, 684);
	public FixedLengthStringData riskclasssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] riskclassErr = FLSArrayPartOfStructure(10, 4, riskclasssErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(riskclasssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData riskclass01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData riskclass02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData riskclass03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData riskclass04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData riskclass05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData riskclass06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData riskclass07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData riskclass08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData riskclass09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData riskclass10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData riskclassdescsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] riskclassdescErr = FLSArrayPartOfStructure(10, 4, riskclassdescsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(riskclassdescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData riskclassdesc01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData riskclassdesc02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData riskclassdesc03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData riskclassdesc04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData riskclassdesc05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData riskclassdesc06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData riskclassdesc07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData riskclassdesc08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData riskclassdesc09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData riskclassdesc10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData formulasErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] formulaErr = FLSArrayPartOfStructure(10, 4, formulasErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(formulasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData formula01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData formula02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData formula03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData formula04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData formula05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData formula06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData formula07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData formula08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData formula09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData formula10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);

	public FixedLengthStringData factorsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData[] factorErr = FLSArrayPartOfStructure(10, 4, factorsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(40).isAPartOf(factorsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData factor11Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData factor02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData factor03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData factor04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData factor05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData factor06Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData factor07Err = new FixedLengthStringData(4).isAPartOf(filler8, 24);
	public FixedLengthStringData factor08Err = new FixedLengthStringData(4).isAPartOf(filler8, 28);
	public FixedLengthStringData factor09Err = new FixedLengthStringData(4).isAPartOf(filler8, 32);
	public FixedLengthStringData factor10Err = new FixedLengthStringData(4).isAPartOf(filler8, 36);


	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(528).isAPartOf(dataArea, 660);
	public FixedLengthStringData riskclasssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] riskclassOut = FLSArrayPartOfStructure(10, 12, riskclasssOut, 0);
	public FixedLengthStringData[][] riskclassO = FLSDArrayPartOfArrayStructure(12, 1, riskclassOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(riskclasssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] riskclass01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] riskclass02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] riskclass03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] riskclass04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] riskclass05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] riskclass06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] riskclass07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] riskclass08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] riskclass09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] riskclass10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);

	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData riskclassdescsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] riskclassdescOut = FLSArrayPartOfStructure(10, 12, riskclassdescsOut, 0);
	public FixedLengthStringData[][] riskclassdescO = FLSDArrayPartOfArrayStructure(12, 1, riskclassdescOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(riskclassdescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] riskclassdesc01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] riskclassdesc02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] riskclassdesc03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] riskclassdesc04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] riskclassdesc05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] riskclassdesc06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] riskclassdesc07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] riskclassdesc08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] riskclassdesc09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] riskclassdesc10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);

	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData formulasOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] formulaOut = FLSArrayPartOfStructure(10, 12, formulasOut, 0);
	public FixedLengthStringData[][] formulaO = FLSDArrayPartOfArrayStructure(12, 1, formulaOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(120).isAPartOf(formulasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] formula01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] formula02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] formula03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] formula04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] formula05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData[] formula06Out = FLSArrayPartOfStructure(12, 1, filler12, 60);
	public FixedLengthStringData[] formula07Out = FLSArrayPartOfStructure(12, 1, filler12, 72);
	public FixedLengthStringData[] formula08Out = FLSArrayPartOfStructure(12, 1, filler12, 84);
	public FixedLengthStringData[] formula09Out = FLSArrayPartOfStructure(12, 1, filler12, 96);
	public FixedLengthStringData[] formula10Out = FLSArrayPartOfStructure(12, 1, filler12, 108);

	public FixedLengthStringData factorsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 396);
	public FixedLengthStringData[] factorOut = FLSArrayPartOfStructure(10, 12, factorsOut, 0);
	public FixedLengthStringData[][] factorO = FLSDArrayPartOfArrayStructure(12, 1, factorOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(120).isAPartOf(factorsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] factor11Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] factor02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] factor03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] factor04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] factor05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData[] factor06Out = FLSArrayPartOfStructure(12, 1, filler13, 60);
	public FixedLengthStringData[] factor07Out = FLSArrayPartOfStructure(12, 1, filler13, 72);
	public FixedLengthStringData[] factor08Out = FLSArrayPartOfStructure(12, 1, filler13, 84);
	public FixedLengthStringData[] factor09Out = FLSArrayPartOfStructure(12, 1, filler13, 96);
	public FixedLengthStringData[] factor10Out = FLSArrayPartOfStructure(12, 1, filler13, 108);


	
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sd5g4screenWritten = new LongData(0);
	public LongData Sd5g4protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5g4ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, formula01, factor11, riskclassdesc01,riskclass01, formula02, factor02, riskclassdesc02,  riskclass02, formula03, factor03, riskclassdesc03,  riskclass03, formula04, factor04, riskclassdesc04,  riskclass04, formula05, factor05, riskclassdesc05,  riskclass05, formula06, factor06, riskclassdesc06,  riskclass06, formula07, factor07, riskclassdesc07,  riskclass07, formula08, factor08, riskclassdesc08, riskclass08, formula09, factor09, riskclassdesc09,  riskclass09, formula10, factor10, riskclassdesc10,  riskclass10 };
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, formula01Out, factor11Out, riskclassdesc01Out, riskclass01Out, formula02Out, factor02Out, riskclassdesc02Out,  riskclass02Out, formula03Out, factor03Out, riskclassdesc03Out,  riskclass03Out, formula04Out, factor04Out, riskclassdesc04Out,  riskclass04Out, formula05Out, factor05Out, riskclassdesc05Out,  riskclass05Out, formula06Out, factor06Out, riskclassdesc06Out,  riskclass06Out, formula07Out, factor07Out, riskclassdesc07Out,  riskclass07Out, formula08Out, factor08Out, riskclassdesc08Out,  riskclass08Out, formula09Out, factor09Out, riskclassdesc09Out,  riskclass09Out, formula10Out, factor10Out, riskclassdesc10Out,  riskclass10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, formula01Err, factor11Err, riskclassdesc01Err, riskclass01Err, formula02Err, factor02Err, riskclassdesc02Err,  riskclass02Err, formula03Err, factor03Err, riskclassdesc03Err,  riskclass03Err, formula04Err, factor04Err, riskclassdesc04Err,  riskclass04Err, formula05Err, factor05Err, riskclassdesc05Err,  riskclass05Err, formula06Err, factor06Err, riskclassdesc06Err,  riskclass06Err, formula07Err, factor07Err, riskclassdesc07Err,  riskclass07Err, formula08Err, factor08Err, riskclassdesc08Err,  riskclass08Err, formula09Err, factor09Err, riskclassdesc09Err,  riskclass09Err, formula10Err, factor10Err, riskclassdesc10Err,  riskclass10Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5g4screen.class;
		protectRecord = Sd5g4protect.class;
	}

}
