/*
 * File: Pr51p.java
 * Date: 30 August 2009 1:38:07
 * Author: Quipoz Limited
 * 
 * Class transformed from PR51P.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.life.productdefinition.screens.Sa524ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
***********************************************************************
* </pre>
*/
public class Pa524 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("SA524");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Ta524rec ta524rec = new Ta524rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sa524ScreenVars sv = ScreenProgram.getScreenVars(Sa524ScreenVars.class);
	private T5688rec t5688rec = new T5688rec();
	private Itempf itempf;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Itmdkey wsaaItmdkey = new Itmdkey();
	Smtpfxcpy smtpfxcpy = new Smtpfxcpy();

	public Pa524() {
		super();
		screenVars = sv;
		new ScreenModel("Sa524", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		wsaaItmdkey.set(wsspsmart.itmdkey);
		descpf=descDAO.getdescData("IT", wsaaItmdkey.itemItemtabl.toString(), wsaaItmdkey.itemItemitem.toString(), wsaaItmdkey.itemItemcoy.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			fatalError600();
		}
		itempf = itempfDAO.findItemByItdm(wsaaItmdkey.itemItemcoy.toString(), wsaaItmdkey.itemItemtabl.toString(),wsaaItmdkey.itemItemitem.toString());
		if (itempf == null ) {
			fatalError600();
		}
//		readContractDefinition();
		moveToScreen();
		if (itempf.getGenarea()!=null) {
			ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			generalArea1045();
		}
	}

protected void readContractDefinition() 
	{
		Itempf item;
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		item = itempfDAO.findItemByDate(smtpfxcpy.item.toString(), wsspcomn.company.toString(), "T5688" , itempf.getItemitem().trim(), wsaaToday.toString(), "1");  
		if (item != null)
			t5688rec.t5688Rec.set(StringUtil.rawToString(item.getGenarea()));
	}

protected void moveToScreen()
	{
		sv.company.set(itempf.getItemcoy());
		sv.tabl.set(itempf.getItemtabl());
		sv.item.set(itempf.getItemitem());
		sv.longdesc.set(descpf.getLongdesc());
	}

protected void generalArea1045()
	{
		if (isEQ(itempf.getItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itempf.getItmfrm());
		}
		if (isEQ(itempf.getItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itempf.getItmto());
		}
		sv.minmthif.set(ta524rec.minmthif);
		sv.maxphprd.set(ta524rec.maxphprd);
		sv.prmstatus.set(ta524rec.prmstatus);
		sv.znfopt.set("PHL");
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{		
		preparation3010();
		updatePrimaryRecord3050();
		updateRecord3055();
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
	}

protected void updatePrimaryRecord3050()
	{
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itempf.setTranid(varcom.vrcmCompTranid.trim());
	}

protected void updateRecord3055()
	{
		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			return;
		}
		itempf.setGenarea(ta524rec.ta524Rec.toString().getBytes());
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(wsaaItmdkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItmdkey.itemItemitem.toString());
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
	}

protected void checkChanges3100()
	{
		if (isNE(sv.itmfrm, itempf.getItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itempf.getItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.minmthif, ta524rec.minmthif)) {
			ta524rec.minmthif.set(sv.minmthif);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxphprd, ta524rec.maxphprd)) {
			ta524rec.maxphprd.set(sv.maxphprd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.znfopt, ta524rec.znfopt)) {
			ta524rec.znfopt.set(sv.znfopt);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
