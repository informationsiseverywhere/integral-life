package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AnnypfDAO extends BaseDAO<Annypf> {
    public Map<Long, List<Annypf>> searchAnnyRecord(List<Long> covrUQ);
    
    public Map<String, List<Annypf>> searchAnnyRecordByChdrnum(List<String> chdrnumList);
    public Annypf getAnnyRecordByAnnyKey(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plansuffix);
}