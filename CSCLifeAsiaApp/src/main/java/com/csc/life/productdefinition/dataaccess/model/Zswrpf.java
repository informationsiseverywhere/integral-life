package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Zswrpf 
{
	
	private long uniqueNumber;
	private String chdrpfx;
	private String chdrcoy;
	private String chdrnum;
	private String cnntype;
	private String validflag;	
	private String crtable;
	private String zafropt;
	private String zchkopt; 
	private int zregdte;
	private int age;
	private int tranno;
	private String  zafrfreq;  
	private String  vrtfund;
	private String ualfnd01;
	private String ualfnd02;
	private String ualfnd03;
	private String ualfnd04;
	private String ualfnd05;
	private String ualfnd06;
	private String ualfnd07;
	private String ualfnd08;
	private String ualfnd09;
	private String ualfnd10;
	private long  ualprc01;
	private long  ualprc02;
	private long  ualprc03;
	private long  ualprc04;
	private long  ualprc05;
	private long  ualprc06;
	private long  ualprc07;
	private long  ualprc08;
	private long  ualprc09;
	private long  ualprc10;
	private String  zwdwlrsn;
	private int zlastdte;
	private int znextdte;
	private int zwdlddte;
	private String zenable;	
	private int  zlstupdate;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private String zafritem;
	private String zlstupdusr;  
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getCnntype() {
		return cnntype;
	}
	public void setCnntype(String cnntype) {
		this.cnntype = cnntype;
	}
	 
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getZafropt() {
		return zafropt;
	}
	public void setZafropt(String zafropt) {
		this.zafropt = zafropt;
	}
	public String getZchkopt() {
		return zchkopt;
	}
	public void setZchkopt(String zchkopt) {
		this.zchkopt = zchkopt;
	}
	public int getZregdte() {
		return zregdte;
	}
	public void setZregdte(int zregdte) {
		this.zregdte = zregdte;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getZafrfreq() {
		return zafrfreq;
	}
	public void setZafrfreq(String zafrfreq) {
		this.zafrfreq = zafrfreq;
	}
	public String getVrtfund() {
		return vrtfund;
	}
	public void setVrtfund(String vrtfund) {
		this.vrtfund = vrtfund;
	}
	
	public String getUalfnd01() {
		return ualfnd01;
	}
	public void setUalfnd01(String ualfnd01) {
		this.ualfnd01 = ualfnd01;
	}
	
	public String getUalfnd02() {
		return ualfnd02;
	}
	public void setUalfnd02(String ualfnd02) {
		this.ualfnd02 = ualfnd02;
	}
	
	public String getUalfnd03() {
		return ualfnd03;
	}
	public void setUalfnd03(String ualfnd03) {
		this.ualfnd03 = ualfnd03;
	}
	
	
	public String getUalfnd04() {
		return ualfnd04;
	}
	public void setUalfnd04(String ualfnd04) {
		this.ualfnd04 = ualfnd04;
	}
	
	public String getUalfnd05() {
		return ualfnd05;
	}
	public void setUalfnd05(String ualfnd05) {
		this.ualfnd05 = ualfnd05;
	}
	
	public String getUalfnd06() {
		return ualfnd06;
	}
	public void setUalfnd06(String ualfnd06) {
		this.ualfnd06 = ualfnd06;
	}
	
	public String getUalfnd07() {
		return ualfnd07;
	}
	public void setUalfnd07(String ualfnd07) {
		this.ualfnd07 = ualfnd07;
	}
	
	
	public String getUalfnd08() {
		return ualfnd08;
	}
	public void setUalfnd08(String ualfnd08) {
		this.ualfnd08 = ualfnd08;
	}
	
	public String getUalfnd09() {
		return ualfnd09;
	}
	public void setUalfnd09(String ualfnd09) {
		this.ualfnd09 = ualfnd09;
	}
	
	public long getUalprc01() {
		return ualprc01;
	}
	public void setUalprc01(long ualprc01) {
		this.ualprc01 = ualprc01;
	}
	
	
	public long getUalprc02() {
		return ualprc02;
	}
	public void setUalprc02(long ualprc02) {
		this.ualprc02 = ualprc02;
	}
	
	
	public long getUalprc03() {
		return ualprc03;
	}
	public void setUalprc03(long ualprc03) {
		this.ualprc03 = ualprc03;
	}
	
	
	public long getUalprc04() {
		return ualprc04;
	}
	public void setUalprc04(long ualprc04) {
		this.ualprc04 = ualprc04;
	}
	
	public long getUalprc05() {
		return ualprc05;
	}
	public void setUalprc05(long ualprc05) {
		this.ualprc05 = ualprc05;
	}
	
	public long getUalprc06() {
		return ualprc06;
	}
	public void setUalprc06(long ualprc06) {
		this.ualprc06 = ualprc06;
	}
	
	
	public long getUalprc07() {
		return ualprc07;
	}
	public void setUalprc07(long ualprc07) {
		this.ualprc07 = ualprc07;
	}
	
	
	public long getUalprc08() {
		return ualprc08;
	}
	public void setUalprc08(long ualprc08) {
		this.ualprc08 = ualprc08;
	}
	
	public long getUalprc09() {
		return ualprc09;
	}
	public void setUalprc09(long ualprc09) {
		this.ualprc09 = ualprc09;
	}
	
	public long getUalprc10() {
		return ualprc10;
	}
	public void setUalprc10(long ualprc10) {
		this.ualprc10 = ualprc10;
	}
	
	public String getUalfnd10() {
		return ualfnd10;
	}
	public void setUalfnd10(String ualfnd10) {
		this.ualfnd10 = ualfnd10;
	}
	
	
	public String getZwdwlrsn() {
		return zwdwlrsn;
	}
	public void setZwdwlrsn(String zwdwlrsn) {
		this.zwdwlrsn = zwdwlrsn;
	}
	
	public int  getZlastdte() {
		return zlastdte;
	}
	public void  setZlastdte(int zlastdte) {
		this.zlastdte = zlastdte;
	}
	
	public int  getZnextdte() {
		return znextdte;
	}
	public void  setZnextdte(int znextdte) {
		this.znextdte = znextdte;
	}
	
	public int  getZwdlddte() {
		return zwdlddte;
	}
	public void  setZwdlddte(int zwdlddte) {
		this.zwdlddte = zwdlddte;
	}
	
	
	public String getZenable() {
		return zenable;
	}
	public void setZenable(String zenable) {
		this.zenable = zenable;
	}
	
	public int  getZlstupdate() {
		return zlstupdate;
	}
	public void  setZlstupdate(int zlstupdate) {
		this.zlstupdate = zlstupdate;
	}
	
	
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	public String getZafritem() {
		return zafritem;
	}
	public void setZafritem(String zafritem) {
		this.zafritem = zafritem;
	}
	public String getZlstupdusr() {
		return zlstupdusr;
	}
	public void setZlstupdusr(String zlstupdusr) {
		this.zlstupdusr = zlstupdusr;
	}
	public void clearUnitAllocFundAndPerc() {
		
		this.ualfnd01="";
		this.ualfnd02="";
		this.ualfnd03="";
		this.ualfnd04="";
		this.ualfnd05="";
		this.ualfnd06="";
		this.ualfnd07="";
		this.ualfnd08="";
		this.ualfnd09="";
		this.ualfnd10="";
			
		this.ualprc01=0;
		this.ualprc02=0;
		this.ualprc03=0;
		this.ualprc04=0;
		this.ualprc05=0;
		this.ualprc06=0;
		this.ualprc07=0;
		this.ualprc08=0;
		this.ualprc09=0;
		this.ualprc10=0;
		
	}
}