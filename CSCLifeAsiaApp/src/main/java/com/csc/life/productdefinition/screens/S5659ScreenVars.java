package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5659
 * @version 1.0 generated on 30/08/09 06:47
 * @author Quipoz
 */
public class S5659ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(510);
	public FixedLengthStringData dataFields = new FixedLengthStringData(206).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData rndfact = DD.rndfact.copyToZonedDecimal().isAPartOf(dataFields,55);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData volbanfrs = new FixedLengthStringData(60).isAPartOf(dataFields, 66);
	public ZonedDecimalData[] volbanfr = ZDArrayPartOfStructure(4, 15, 0, volbanfrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(volbanfrs, 0, FILLER_REDEFINE);
	public ZonedDecimalData volbanfr01 = DD.volbanfr.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData volbanfr02 = DD.volbanfr.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData volbanfr03 = DD.volbanfr.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData volbanfr04 = DD.volbanfr.copyToZonedDecimal().isAPartOf(filler,45);
	public FixedLengthStringData volbanles = new FixedLengthStringData(20).isAPartOf(dataFields, 126);
	public ZonedDecimalData[] volbanle = ZDArrayPartOfStructure(4, 5, 0, volbanles, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(volbanles, 0, FILLER_REDEFINE);
	public ZonedDecimalData volbanle01 = DD.volbanle.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData volbanle02 = DD.volbanle.copyToZonedDecimal().isAPartOf(filler1,5);
	public ZonedDecimalData volbanle03 = DD.volbanle.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData volbanle04 = DD.volbanle.copyToZonedDecimal().isAPartOf(filler1,15);
	public FixedLengthStringData volbantos = new FixedLengthStringData(60).isAPartOf(dataFields, 146);
	public ZonedDecimalData[] volbanto = ZDArrayPartOfStructure(4, 15, 0, volbantos, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(volbantos, 0, FILLER_REDEFINE);
	public ZonedDecimalData volbanto01 = DD.volbanto.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData volbanto02 = DD.volbanto.copyToZonedDecimal().isAPartOf(filler2,15);
	public ZonedDecimalData volbanto03 = DD.volbanto.copyToZonedDecimal().isAPartOf(filler2,30);
	public ZonedDecimalData volbanto04 = DD.volbanto.copyToZonedDecimal().isAPartOf(filler2,45);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 206);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData rndfactErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData volbanfrsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] volbanfrErr = FLSArrayPartOfStructure(4, 4, volbanfrsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(volbanfrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData volbanfr01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData volbanfr02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData volbanfr03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData volbanfr04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData volbanlesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] volbanleErr = FLSArrayPartOfStructure(4, 4, volbanlesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(volbanlesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData volbanle01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData volbanle02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData volbanle03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData volbanle04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData volbantosErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] volbantoErr = FLSArrayPartOfStructure(4, 4, volbantosErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(16).isAPartOf(volbantosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData volbanto01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData volbanto02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData volbanto03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData volbanto04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 282);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] rndfactOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData volbanfrsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] volbanfrOut = FLSArrayPartOfStructure(4, 12, volbanfrsOut, 0);
	public FixedLengthStringData[][] volbanfrO = FLSDArrayPartOfArrayStructure(12, 1, volbanfrOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(48).isAPartOf(volbanfrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] volbanfr01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] volbanfr02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] volbanfr03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] volbanfr04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData volbanlesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] volbanleOut = FLSArrayPartOfStructure(4, 12, volbanlesOut, 0);
	public FixedLengthStringData[][] volbanleO = FLSDArrayPartOfArrayStructure(12, 1, volbanleOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(volbanlesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] volbanle01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] volbanle02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] volbanle03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] volbanle04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData volbantosOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] volbantoOut = FLSArrayPartOfStructure(4, 12, volbantosOut, 0);
	public FixedLengthStringData[][] volbantoO = FLSDArrayPartOfArrayStructure(12, 1, volbantoOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(48).isAPartOf(volbantosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] volbanto01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] volbanto02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] volbanto03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] volbanto04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5659screenWritten = new LongData(0);
	public LongData S5659protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5659ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(volbanfr01Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanto01Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanle01Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanfr02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanto02Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanle02Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanfr03Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanto03Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanle03Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanfr04Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanto04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(volbanle04Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rndfactOut,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, volbanfr01, volbanto01, volbanle01, volbanfr02, volbanto02, volbanle02, volbanfr03, volbanto03, volbanle03, volbanfr04, volbanto04, volbanle04, rndfact};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, volbanfr01Out, volbanto01Out, volbanle01Out, volbanfr02Out, volbanto02Out, volbanle02Out, volbanfr03Out, volbanto03Out, volbanle03Out, volbanfr04Out, volbanto04Out, volbanle04Out, rndfactOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, volbanfr01Err, volbanto01Err, volbanle01Err, volbanfr02Err, volbanto02Err, volbanle02Err, volbanfr03Err, volbanto03Err, volbanle03Err, volbanfr04Err, volbanto04Err, volbanle04Err, rndfactErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5659screen.class;
		protectRecord = S5659protect.class;
	}

}
