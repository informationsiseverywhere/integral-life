package com.csc.life.productdefinition.recordstructures;

import java.util.List;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class PmexVpxRec extends ExternalData {
	//VpxLextRec
	private static final long serialVersionUID = 1L;
	
  //*******************************
  //Attribute Declarations
  //*******************************
	
  	private String chdrChdrcoy;
  	private String chdrChdrnum;
  	private List<String> noOfCoverages;
	private  List<List<String>> aidsCoverInd;
	private  List<List<String>> totalSumInsured;
	private  List<List<String>> ncdCode;
	private  List<List<String>> claimAutoIncrease;
	private  List<List<String>> syndicateCode;
	private  List<List<String>> riskExpiryAge;
	private List<List<List<String>>> oppc;
  	private List<List<List<String>>> zmortpct;
	private List<List<List<String>>> opcda;
	private List<List<List<String>>> agerate;
	private List<List<List<String>>> insprm;
	private List<List<String>> count;
	private List<List<List<String>>> insprmRsn;
	private List<List<List<String>>> oppcRsn;
	
	public List<List<String>> getCount() {
		return count;
	}

	public void setCount(List<List<String>> count) {
		this.count = count;
	}

	public String getChdrChdrcoy() {
		return chdrChdrcoy;
	}

	public void setChdrChdrcoy(String chdrChdrcoy) {
		this.chdrChdrcoy = chdrChdrcoy;
	}

	public String getChdrChdrnum() {
		return chdrChdrnum;
	}

	public void setChdrChdrnum(String chdrChdrnum) {
		this.chdrChdrnum = chdrChdrnum;
	}

	public List<String> getNoOfCoverages() {
		return noOfCoverages;
	}

	public void setNoOfCoverages(List<String> noOfCoverages) {
		this.noOfCoverages = noOfCoverages;
	}

	public List<List<String>> getAidsCoverInd() {
		return aidsCoverInd;
	}

	public void setAidsCoverInd( List<List<String>> aidsCoverInd) {
		this.aidsCoverInd = aidsCoverInd;
	}

	public  List<List<String>> getTotalSumInsured() {
		return totalSumInsured;
	}

	public void setTotalSumInsured( List<List<String>> totalSumInsured) {
		this.totalSumInsured = totalSumInsured;
	}

	public  List<List<String>> getNcdCode() {
		return ncdCode;
	}

	public void setNcdCode( List<List<String>> ncdCode) {
		this.ncdCode = ncdCode;
	}

	public  List<List<String>> getClaimAutoIncrease() {
		return claimAutoIncrease;
	}

	public void setClaimAutoIncrease( List<List<String>> claimAutoIncrease) {
		this.claimAutoIncrease = claimAutoIncrease;
	}

	public  List<List<String>> getSyndicateCode() {
		return syndicateCode;
	}

	public void setSyndicateCode( List<List<String>> syndicateCode) {
		this.syndicateCode = syndicateCode;
	}

	public  List<List<String>> getRiskExpiryAge() {
		return riskExpiryAge;
	}

	public void setRiskExpiryAge( List<List<String>> riskExpiryAge) {
		this.riskExpiryAge = riskExpiryAge;
	}
	public List<List<List<String>>> getOppc() {
		return oppc;
	}

	public void setOppc(List<List<List<String>>> oppc) {
		this.oppc = oppc;
	}

	public List<List<List<String>>> getZmortpct() {
		return zmortpct;
	}

	public void setZmortpct(List<List<List<String>>> zmortpct) {
		this.zmortpct = zmortpct;
	}
	public List<List<List<String>>> getOpcda() {
		return opcda;
	}

	public void setOpcda(List<List<List<String>>> opcda) {
		this.opcda = opcda;
	}
	public List<List<List<String>>> getAgerate() {
		return agerate;
	}

	public void setAgerate(List<List<List<String>>> agerate) {
		this.agerate = agerate;
	}
	public List<List<List<String>>> getInsprm() {
		return insprm;
	}

	public void setInsprm(List<List<List<String>>> insprm) {
		this.insprm = insprm;
	}

	public List<List<List<String>>> getInsprmRsn() {
		return insprmRsn;
	}

	public void setInsprmRsn(List<List<List<String>>> insprmRsn) {
		this.insprmRsn = insprmRsn;
	}
	
	public List<List<List<String>>> getOppcRsn() {
		return oppcRsn;
	}

	public void setOppcRsn(List<List<List<String>>> oppcRsn) {
		this.oppcRsn = oppcRsn;
	}
	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
	}


}