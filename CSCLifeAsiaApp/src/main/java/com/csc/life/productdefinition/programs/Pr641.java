/*
 * File: Pr641.java
 * Date: 30 August 2009 1:50:32
 * Author: Quipoz Limited
 * 
 * Class transformed from PR641.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.MediinvTableDAM;
import com.csc.life.productdefinition.dataaccess.MediivcTableDAM;
import com.csc.life.productdefinition.screens.Sr641ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* Medical Bill Submenu
* --------------------
*
***********************************************************************
* </pre>
*/
public class Pr641 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR641");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData ix = new ZonedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaFoundValidStatcode = new FixedLengthStringData(1).init("N");
	private Validator foundValidStatcode = new Validator(wsaaFoundValidStatcode, "Y");
		/* ERRORS */
	private static final String d031 = "D031";
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String rl72 = "RL72";
	private static final String f259 = "F259";
	private static final String f321 = "F321";
	private static final String f910 = "F910";
	private static final String h137 = "H137";
	private static final String rgh2 = "RGH2";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String mediinvrec = "MEDIINVREC";
	private static final String mediivcrec = "MEDIIVCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MediinvTableDAM mediinvIO = new MediinvTableDAM();
	private MediivcTableDAM mediivcIO = new MediivcTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sr641ScreenVars sv = ScreenProgram.getScreenVars( Sr641ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2990, 
		exit3090
	}

	public Pr641() {
		super();
		screenVars = sv;
		new ScreenModel("Sr641", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		validateKey12210();
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1, "Y")) {
			if (isNE(sv.invcref, SPACES)) {
				sv.invcrefErr.set(rgh2);
			}
			if (isEQ(sv.chdrsel, SPACES)) {
				sv.chdrselErr.set(d031);
			}
			else {
				valChdr2300();
			}
		}
		if (isEQ(subprogrec.key2, "Y")) {
			if (isEQ(sv.invcref, SPACES)) {
				sv.invcrefErr.set(rl72);
			}
			if (isNE(sv.chdrsel, SPACES)) {
				sv.chdrselErr.set(rgh2);
			}
		}
		if (isEQ(sv.action, "C")
		|| isEQ(sv.action, "D")) {
			if (isNE(sv.chdrsel, SPACES)) {
				sv.chdrselErr.set(rgh2);
			}
			if (isNE(sv.invcref, SPACES)) {
				sv.invcrefErr.set(rgh2);
			}
		}
	}

protected void valChdr2300()
	{
		begin2300();
	}

protected void begin2300()
	{
		chdrenqIO.setDataArea(SPACES);
		chdrenqIO.setStatuz(varcom.oK);
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrsel);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)
		&& isNE(chdrenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			sv.chdrselErr.set(f259);
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.chdrselErr.set(f321);
			return ;
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaFoundValidStatcode.set("N");
		for (ix.set(1); !(isGT(ix, 12)
		|| foundValidStatcode.isTrue()); ix.add(1)){
			if (isEQ(chdrenqIO.getStatcode(), t5679rec.cnRiskStat[ix.toInt()])) {
				wsaaFoundValidStatcode.set("Y");
			}
		}
		if (!foundValidStatcode.isTrue()) {
			sv.chdrselErr.set(h137);
			return ;
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit2990);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateWssp3010();
			batching3080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(sv.action, "A")){
			wsspcomn.flag.set("C");
			sftlockrec.enttyp.set(fsupfxcpy.chdr);
			sftlockrec.entity.set(sv.chdrsel);
			callSftlock3200();
			keepsMediinv3110();
		}
		else if (isEQ(sv.action, "B")){
			wsspcomn.flag.set("C");
			sftlockrec.enttyp.set(fsupfxcpy.chdr);
			sftlockrec.entity.set(sv.invcref);
			callSftlock3200();
			keepsMediivc3120();
		}
		else if (isEQ(sv.action, "C")){
			wsspcomn.flag.set("M");
		}
		else if (isEQ(sv.action, "D")){
			wsspcomn.flag.set("I");
		}
		else if (isEQ(sv.action, "E")){
			wsspcomn.flag.set("I");
			keepsChdrenq3130();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void keepsMediinv3110()
	{
		start3110();
	}

protected void start3110()
	{
		mediinvIO.setDataArea(SPACES);
		mediinvIO.setStatuz(varcom.oK);
		mediinvIO.setChdrcoy(wsspcomn.company);
		mediinvIO.setChdrnum(sv.chdrsel);
		mediinvIO.setInvref(sv.invcref);
		mediinvIO.setFormat(mediinvrec);
		mediinvIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, mediinvIO);
		if (isNE(mediinvIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mediinvIO.getStatuz());
			syserrrec.params.set(mediinvIO.getParams());
			fatalError600();
		}
	}

protected void keepsMediivc3120()
	{
		start3120();
	}

protected void start3120()
	{
		mediivcIO.setDataArea(SPACES);
		mediivcIO.setChdrcoy(wsspcomn.company);
		mediivcIO.setInvref(sv.invcref);
		mediivcIO.setChdrnum(SPACES);
		mediivcIO.setSeqno(ZERO);
		mediivcIO.setFormat(mediivcrec);
		mediivcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mediivcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediivcIO.setFitKeysSearch("CHDRCOY", "INVREF");
		SmartFileCode.execute(appVars, mediivcIO);
		if (isNE(mediivcIO.getStatuz(), varcom.oK)
		&& isNE(mediivcIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mediivcIO.getStatuz());
			syserrrec.params.set(mediivcIO.getParams());
			fatalError600();
		}
		if (isEQ(mediivcIO.getStatuz(), varcom.endp)
		|| isNE(mediivcIO.getChdrcoy(), wsspcomn.company)
		|| isNE(mediivcIO.getInvref(), sv.invcref)) {
			mediivcIO.setNonKey(SPACES);
			mediivcIO.setChdrcoy(wsspcomn.company);
			mediivcIO.setInvref(sv.invcref);
			mediivcIO.setChdrnum(SPACES);
			mediivcIO.setSeqno(ZERO);
			mediivcIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, mediivcIO);
			if (isNE(mediivcIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(mediivcIO.getStatuz());
				syserrrec.params.set(mediivcIO.getParams());
				fatalError600();
			}
		}
		else {
			mediivcIO.setFunction(varcom.reads);
			SmartFileCode.execute(appVars, mediivcIO);
			if (isNE(mediivcIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(mediivcIO.getStatuz());
				syserrrec.params.set(mediivcIO.getParams());
				fatalError600();
			}
		}
	}

protected void keepsChdrenq3130()
	{
		start3130();
	}

protected void start3130()
	{
		chdrenqIO.setDataArea(SPACES);
		chdrenqIO.setStatuz(varcom.oK);
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrsel);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void callSftlock3200()
	{
		/*BEGIN*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(f910);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
