package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH605
 * @version 1.0 generated on 30/08/09 07:05
 * @author Quipoz
 */
public class Sh605ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(305);
	public FixedLengthStringData dataFields = new FixedLengthStringData(65).isAPartOf(dataArea, 0);
	public FixedLengthStringData agccqind = DD.agccqind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bonusInd = DD.bnusin.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData crtind = DD.crtind.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,5);
	public ZonedDecimalData intRate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields,6);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData liacoy = DD.liacoy.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData ratebas = DD.ratebas.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,55);
	public ZonedDecimalData tdayno = DD.tdayno.copyToZonedDecimal().isAPartOf(dataFields,60);
	public FixedLengthStringData tsalesind = DD.tsalesind.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData uwind = DD.uwind.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 65);
	public FixedLengthStringData agccqindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bnusinErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData intratErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData liacoyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ratebasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData tdaynoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tsalesindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData uwindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 125);
	public FixedLengthStringData[] agccqindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bnusinOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] intratOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] liacoyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ratebasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] tdaynoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tsalesindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] uwindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh605screenWritten = new LongData(0);
	public LongData Sh605protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh605ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agccqindOut,new String[] {"59",null, "-59",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"62",null, "-62",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdaynoOut,new String[] {"60",null, "-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratebasOut,new String[] {"61",null, "-61",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, liacoy, intRate, indic, uwind, crtind, tsalesind, agccqind, bonusInd, comind, tdayno, ratebas};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, liacoyOut, intratOut, indicOut, uwindOut, crtindOut, tsalesindOut, agccqindOut, bnusinOut, comindOut, tdaynoOut, ratebasOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, liacoyErr, intratErr, indicErr, uwindErr, crtindErr, tsalesindErr, agccqindErr, bnusinErr, comindErr, tdaynoErr, ratebasErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh605screen.class;
		protectRecord = Sh605protect.class;
	}

}
