package com.csc.life.productdefinition.procedures;

import java.util.HashMap;

import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.util.QPUtilities;

public class Calcubbl extends Vpmcalc {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private Vpxubblrec vpxubblrec = new Vpxubblrec();


	@Override
	public void mainline(Object... parmArray)
	{
		recordMap = new HashMap<String, ExternalData>();
		recordMap.put("VPMFMTREC", vpmfmtrec);
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		recordMap.put("VPXUBBLREC", vpxubblrec);

		try {
			startSubr010();

		}
		catch (COBOLExitProgramException e) {
		}
	}

}
