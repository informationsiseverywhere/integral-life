package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5567
 * @version 1.0 generated on 30/08/09 06:44
 * @author Quipoz
 */
public class S5567ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(666);
	public FixedLengthStringData dataFields = new FixedLengthStringData(250).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreqs = new FixedLengthStringData(20).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(10, 2, billfreqs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01 = DD.billfreq.copy().isAPartOf(filler,0);
	public FixedLengthStringData billfreq02 = DD.billfreq.copy().isAPartOf(filler,2);
	public FixedLengthStringData billfreq03 = DD.billfreq.copy().isAPartOf(filler,4);
	public FixedLengthStringData billfreq04 = DD.billfreq.copy().isAPartOf(filler,6);
	public FixedLengthStringData billfreq05 = DD.billfreq.copy().isAPartOf(filler,8);
	public FixedLengthStringData billfreq06 = DD.billfreq.copy().isAPartOf(filler,10);
	public FixedLengthStringData billfreq07 = DD.billfreq.copy().isAPartOf(filler,12);
	public FixedLengthStringData billfreq08 = DD.billfreq.copy().isAPartOf(filler,14);
	public FixedLengthStringData billfreq09 = DD.billfreq.copy().isAPartOf(filler,16);
	public FixedLengthStringData billfreq10 = DD.billfreq.copy().isAPartOf(filler,18);
	public FixedLengthStringData cntfees = new FixedLengthStringData(170).isAPartOf(dataFields, 20);
	public ZonedDecimalData[] cntfee = ZDArrayPartOfStructure(10, 17, 2, cntfees, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(170).isAPartOf(cntfees, 0, FILLER_REDEFINE);
	public ZonedDecimalData cntfee01 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData cntfee02 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData cntfee03 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData cntfee04 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData cntfee05 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData cntfee06 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData cntfee07 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData cntfee08 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData cntfee09 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,136);
	public ZonedDecimalData cntfee10 = DD.cntfee.copyToZonedDecimal().isAPartOf(filler1,153);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,190);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,191);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,199);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,207);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 250);
	public FixedLengthStringData billfreqsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] billfreqErr = FLSArrayPartOfStructure(10, 4, billfreqsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(billfreqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData billfreq02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData billfreq03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData billfreq04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData billfreq05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData billfreq06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData billfreq07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData billfreq08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData billfreq09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData billfreq10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData cntfeesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] cntfeeErr = FLSArrayPartOfStructure(10, 4, cntfeesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(cntfeesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cntfee01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData cntfee02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData cntfee03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData cntfee04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData cntfee05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData cntfee06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData cntfee07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData cntfee08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData cntfee09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData cntfee10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 354);
	public FixedLengthStringData billfreqsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(10, 12, billfreqsOut, 0);
	public FixedLengthStringData[][] billfreqO = FLSDArrayPartOfArrayStructure(12, 1, billfreqOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(billfreqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] billfreq01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] billfreq02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] billfreq03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] billfreq04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] billfreq05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] billfreq06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] billfreq07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] billfreq08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] billfreq09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] billfreq10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData cntfeesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] cntfeeOut = FLSArrayPartOfStructure(10, 12, cntfeesOut, 0);
	public FixedLengthStringData[][] cntfeeO = FLSDArrayPartOfArrayStructure(12, 1, cntfeeOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(cntfeesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cntfee01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] cntfee02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] cntfee03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] cntfee04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] cntfee05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] cntfee06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] cntfee07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] cntfee08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] cntfee09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] cntfee10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5567screenWritten = new LongData(0);
	public LongData S5567protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5567ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(billfreq01Out,new String[] {"37","38","-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq02Out,new String[] {"39","40","-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq03Out,new String[] {"41","42","-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq04Out,new String[] {"43","44","-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq05Out,new String[] {"45","46","-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq06Out,new String[] {"47","48","-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq07Out,new String[] {"49","50","-49",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq08Out,new String[] {"51","52","-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq09Out,new String[] {"53","54","-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq10Out,new String[] {"55","56","-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee01Out,new String[] {"57","58","-57",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee02Out,new String[] {"59","60","-59",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee03Out,new String[] {"61","62","-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee04Out,new String[] {"63","64","-63",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee05Out,new String[] {"65","66","-65",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee06Out,new String[] {"67","68","-67",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee07Out,new String[] {"69","70","-69",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee08Out,new String[] {"71","72","-71",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee09Out,new String[] {"73","74","-73",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntfee10Out,new String[] {"75","76","-75",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, billfreq01, billfreq02, billfreq03, billfreq04, billfreq05, billfreq06, billfreq07, billfreq08, billfreq09, billfreq10, cntfee01, cntfee02, cntfee03, cntfee04, cntfee05, cntfee06, cntfee07, cntfee08, cntfee09, cntfee10, itmfrm, itmto};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, billfreq01Out, billfreq02Out, billfreq03Out, billfreq04Out, billfreq05Out, billfreq06Out, billfreq07Out, billfreq08Out, billfreq09Out, billfreq10Out, cntfee01Out, cntfee02Out, cntfee03Out, cntfee04Out, cntfee05Out, cntfee06Out, cntfee07Out, cntfee08Out, cntfee09Out, cntfee10Out, itmfrmOut, itmtoOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, billfreq01Err, billfreq02Err, billfreq03Err, billfreq04Err, billfreq05Err, billfreq06Err, billfreq07Err, billfreq08Err, billfreq09Err, billfreq10Err, cntfee01Err, cntfee02Err, cntfee03Err, cntfee04Err, cntfee05Err, cntfee06Err, cntfee07Err, cntfee08Err, cntfee09Err, cntfee10Err, itmfrmErr, itmtoErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5567screen.class;
		protectRecord = S5567protect.class;
	}

}
