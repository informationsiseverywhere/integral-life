/*
 * File: Vpmcprm.java
 * Date: 30 August 2009 1:58:03
 * Author: Quipoz Limited
 * 
 * Class transformed from PRMPM01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.interestbearing.recordstructures.Ibincalrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*    VPMS I/B Interest Allocation Rules Calculation Subroutine.
*
* </pre>
*/
public class Vpmibin extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPMIBIN";
		/* ERRORS */
	private String e107 = "E107";
	private String h053 = "H053";
	private String tr28x = "TR28X";
	
	private String itemrec = "ITEMREC";
	private String chdrrec = "CHDRREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String lifecmcrec = "LIFECMCREC";
	
	private Ibincalrec ibincalrec = new Ibincalrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();	
	
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T5687rec t5687rec = new T5687rec();
	private Varcom varcom = new Varcom();
	/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	
	private static final String SUBROUTINE_PREFIX = "CPRM";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit1090, exit0190, 
	}

	public Vpmibin() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ibincalrec.ibincalRec = convertAndSetParam(ibincalrec.ibincalRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start0110();
				}
				case exit0190: {
					exit0190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start0110()
{
	vpmcalcrec.vpmcalcRec.set(SPACES);
	ibincalrec.statuz.set(varcom.oK);
	vpmcalcrec.statuz.set(varcom.oK);

	setupKey1000();
	if (isNE(ibincalrec.statuz, varcom.oK)){
		goTo(GotoLabel.exit1090);
	}
	
	vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
	callProgram(Calcibin.class, vpmcalcrec.vpmcalcRec);
	ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
	
	if (isNE(vpmcalcrec.statuz, varcom.oK)){
		ibincalrec.statuz.set(vpmcalcrec.statuz);
	}
	
}
protected void setupKey1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1010();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
{
	vpmcalcrec.vpmskey.set(SPACES);
	vpmcalcrec.vpmscoy.set(ibincalrec.company);
	vpmcalcrec.vpmstabl.set(tr28x);
	vpmcalcrec.vpmsitem.set("IBIN");
}

protected void exit0190()
	{
		exitProgram();
		goBack();
		stopRun();
	}

}
