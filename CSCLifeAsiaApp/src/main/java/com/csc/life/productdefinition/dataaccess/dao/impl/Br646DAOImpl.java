package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.Br646DAO;
import com.csc.life.productdefinition.dataaccess.model.Br646DTO;
import com.csc.life.productdefinition.dataaccess.model.Br64xDTO;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br646DAOImpl extends BaseDAOImpl<MedxTemppf>implements Br646DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Br646DAOImpl.class);

	public List<MedxTemppf> loadDataByBatch(int batchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM BR646DATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY EXMCODE ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<MedxTemppf> medxList = new ArrayList<MedxTemppf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				MedxTemppf medxpf = new MedxTemppf();
				Br646DTO br646DTO = new Br646DTO();
				medxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				medxpf.setChdrcoy(rs.getString("CHDRCOY").trim());
				medxpf.setChdrnum(rs.getString("CHDRNUM").trim());
				medxpf.setSeqno(rs.getInt("SEQNO"));
				medxpf.setPaidby(rs.getString("PAIDBY").trim());
				medxpf.setExmcode(rs.getString("EXMCODE").trim());
				medxpf.setZmedtyp(rs.getString("ZMEDTYP").trim());
				medxpf.setEffdate(rs.getInt("EFFDATE"));
				medxpf.setInvref(rs.getString("INVREF").trim());
				medxpf.setZmedfee(rs.getBigDecimal("ZMEDFEE"));
				medxpf.setLife(rs.getString("LIFE").trim());
				medxpf.setJlife(rs.getString("JLIFE").trim());
				medxpf.setMemberName(rs.getString("MEMBER_NAME").trim());
				medxpf.setClntnum(rs.getString("CLNTNUM").trim());		//ILIFE-6447
				br646DTO.setTranno(rs.getInt("TRANNO"));
				br646DTO.setChdrpfx(rs.getString("CHDRPFX").trim());
				br646DTO.setCnttype(rs.getString("CNTTYPE").trim());
				br646DTO.setCntcurr(rs.getString("CNTCURR").trim());
				br646DTO.setChdrUniqueNumber(rs.getLong("CHDRUNIQUE"));
				br646DTO.setTotpymt(new BigDecimal(rs.getString("TOTPYMT")));
				br646DTO.setBankkey(rs.getString("BANKKEY") == null ? "": rs.getString("BANKKEY").trim());
				br646DTO.setBankacckey(rs.getString("BANKKEY") == null ? "": rs.getString("BANKACCKEY").trim());
				br646DTO.setPaymth(rs.getString("PAYMTH").trim());
				br646DTO.setFacthous(rs.getString("FACTHOUS") == null ? "": rs.getString("FACTHOUS").trim());
				br646DTO.setClntpfx(rs.getString("CLNTPFX").trim());
				br646DTO.setClntcoy(rs.getString("CLNTCOY").trim());
				br646DTO.setClttype(rs.getString("CLTTYPE").trim());
				br646DTO.setLanguage(rs.getString("LANGUAGE").trim());
				br646DTO.setSurname(rs.getString("SURNAME").trim());
				br646DTO.setInitials(rs.getString("INITIALS").trim());
				br646DTO.setSalutl(rs.getString("SALUTL").trim());
				br646DTO.setLsurname(rs.getString("LSURNAME").trim());
				br646DTO.setLgivname(rs.getString("LGIVNAME").trim());
				br646DTO.setMediUniqueNo(rs.getLong("MEDIUNIQUENO"));
				medxpf.setBr646DTO(br646DTO);
				medxList.add(medxpf);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return medxList;
	}


	public int populateBr646Temp(int batchExtractSize, String medxtempTable, String chdrfrom, String chdrto, String fsucoy) {
		initializeBr646Temp();
		int rows = 0;
		boolean isParameterized = false;
		boolean isParamFromOnly = false;
		boolean isParamToOnly = false;
		if (!("".equals(chdrfrom.trim())) && !("".equals(chdrto.trim()))) {
			if (Integer.parseInt(chdrto) >= Integer.parseInt(chdrfrom)) isParameterized = true;
		}
		
		if (!("".equals(chdrfrom.trim())) && "".equals(chdrto.trim())) isParamFromOnly = true;
		if (!("".equals(chdrto.trim())) && "".equals(chdrfrom.trim())) isParamToOnly = true;
		StringBuilder sb = new StringBuilder("");
		 
		sb.append("INSERT INTO VM1DTA.BR646DATA "); //ILB-475
		sb.append("(BATCHID, UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SEQNO, EXMCODE, ZMEDTYP, PAIDBY, LIFE, JLIFE, EFFDATE, INVREF, ZMEDFEE, CLNTNUM, DTETRM, DESC_T, MEMBER_NAME, CHDRUNIQUE, TRANNO, CHDRPFX, CNTCURR, CNTTYPE, TOTPYMT, MEDIUNIQUENO, BANKKEY, BANKACCKEY, PAYMTH, FACTHOUS, CLNTPFX, CLNTCOY, CLTTYPE, LANGUAGE, SURNAME, INITIALS, SALUTL, LSURNAME, LGIVNAME)  ");
		sb.append("SELECT * FROM(  ");
		sb.append("SELECT floor((row_number()over(ORDER BY MAIN.CHDRCOY ASC, MAIN.CHDRNUM ASC, MAIN.SEQNO DESC)-1)/?) BATCHID, MAIN.* FROM (  ");
		sb.append("SELECT MEDX.UNIQUE_NUMBER, MEDX.CHDRCOY, MEDX.CHDRNUM, MEDX.SEQNO, MEDX.EXMCODE, MEDX.ZMEDTYP, MEDX.PAIDBY, MEDX.LIFE, MEDX.JLIFE, MEDX.EFFDATE, MEDX.INVREF, MEDX.ZMEDFEE, MEDX.CLNTNUM, MEDX.DTETRM, MEDX.DESC_T, MEDX.MEMBER_NAME,  ");
		sb.append("CHDR.UNIQUE_NUMBER CHDRUNIQUE, CHDR.TRANNO, CHDR.CHDRPFX, CHDR.CNTCURR, CHDR.CNTTYPE, (SELECT SUM(ZMEDFEE) TOTPYMT FROM MEDIPF WHERE ACTIND = 1 AND CHDRCOY = MEDX.CHDRCOY AND PAIDBY = MEDX.PAIDBY AND CHDRNUM IN (SELECT DISTINCT CHDRNUM FROM " + medxtempTable + ")) TOTPYMT,  ");
		sb.append("MEDI.UNIQUE_NUMBER MEDIUNIQUENO, ");
		sb.append("ZMPV.BANKKEY, ZMPV.BANKACCKEY, ZMPV.PAYMTH, CLBA.FACTHOUS,  ");
		sb.append("CLNT.CLNTPFX, CLNT.CLNTCOY, CLNT.CLTTYPE, CLNT.LANGUAGE, CLNT.SURNAME, CLNT.INITIALS, CLNT.SALUTL, CLNT.LSURNAME, CLNT.LGIVNAME  ");
		sb.append("FROM  ");
		sb.append(medxtempTable); 
		sb.append(" MEDX INNER JOIN (SELECT * FROM(  ");
		sb.append("SELECT CHDR.UNIQUE_NUMBER, CHDRPF.* FROM (  ");
		sb.append("SELECT CHDRPFX, CHDRCOY, CHDRNUM, CNTCURR, CNTTYPE, MAX(TRANNO) TRANNO FROM CHDRPF GROUP BY CHDRPFX, CHDRCOY, CHDRNUM, CNTCURR, CNTTYPE )  ");
		sb.append("CHDRPF  INNER JOIN CHDRPF CHDR ON CHDR.CHDRPFX = CHDRPF.CHDRPFX AND CHDR.CHDRNUM = CHDRPF.CHDRNUM AND CHDR.TRANNO = CHDRPF.TRANNO) CHDR) CHDR ON CHDR.CHDRCOY = MEDX.CHDRCOY AND CHDR.CHDRNUM = MEDX.CHDRNUM  ");
		sb.append("INNER JOIN ( ");
		sb.append("SELECT CHDRCOY, CHDRNUM, SEQNO, PAIDBY, UNIQUE_NUMBER FROM MEDIPF WHERE ACTIND = 1 AND PAIDBY = 'D' GROUP BY CHDRCOY, CHDRNUM, SEQNO, PAIDBY, UNIQUE_NUMBER ");
		sb.append(") MEDI ON MEDI.CHDRCOY = MEDX.CHDRCOY AND MEDI.CHDRNUM = MEDX.CHDRNUM AND MEDI.PAIDBY = MEDX.PAIDBY AND MEDI.SEQNO=MEDX.SEQNO "); 
		sb.append("INNER JOIN ZMPVPF ZMPV ON ZMPV.ZMEDCOY = MEDX.CHDRCOY AND ZMPV.ZMEDPRV = MEDX.EXMCODE  ");
		sb.append("INNER JOIN CLNTPF CLNT ON CLNT.CLNTNUM = MEDX.CLNTNUM AND CLNT.CLNTPFX = 'CN' AND CLNT.CLNTCOY = ?  ");
		sb.append("LEFT OUTER JOIN CLBAPF CLBA ON CLBA.BANKKEY = ZMPV.BANKKEY AND CLBA.BANKACCKEY = ZMPV.BANKACCKEY AND CLBA.CLNTNUM = MEDX.CLNTNUM AND CLBA.CLNTCOY = ?  ");
		sb.append("WHERE MEDX.PAIDBY = 'D'  ");
		sb.append("AND MEDX.ZMEDFEE > 0  ");
		if (isParameterized)
			sb.append("AND MEDX.CHDRNUM BETWEEN ? and ? ");	
		if (isParamFromOnly || isParamToOnly)
			sb.append("AND MEDX.CHDRNUM = ? ");	
		sb.append("GROUP BY MEDX.UNIQUE_NUMBER, MEDX.CHDRCOY, MEDX.CHDRNUM, MEDX.SEQNO, MEDX.EXMCODE, MEDX.ZMEDTYP, MEDX.PAIDBY, MEDX.LIFE, MEDX.JLIFE, MEDX.EFFDATE, MEDX.INVREF, MEDX.ZMEDFEE, MEDX.CLNTNUM, MEDX.DTETRM, MEDX.DESC_T, MEDX.MEMBER_NAME,  ");
		sb.append("CHDR.UNIQUE_NUMBER, CHDR.TRANNO, CHDR.CHDRPFX, CHDR.CNTCURR, CHDR.CNTTYPE, ");
		sb.append("MEDI.UNIQUE_NUMBER, ");
		sb.append("ZMPV.BANKKEY, ZMPV.BANKACCKEY, ZMPV.PAYMTH, CLBA.FACTHOUS,  ");
		sb.append("CLNT.CLNTPFX, CLNT.CLNTCOY, CLNT.CLTTYPE, CLNT.LANGUAGE, CLNT.SURNAME, CLNT.INITIALS, CLNT.SALUTL, CLNT.LSURNAME, CLNT.LGIVNAME  ");
		sb.append(") MAIN ");
		sb.append(") MAIN ORDER BY MAIN.CHDRCOY, MAIN.CHDRNUM, MAIN.SEQNO DESC");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, fsucoy);
			ps.setString(3, fsucoy);
			if (isParameterized){
				ps.setString(4, chdrfrom);
				ps.setString(5, chdrto);
			}
			if (isParamFromOnly) ps.setString(4, chdrfrom);
			if (isParamToOnly) ps.setString(4, chdrto);
			
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBr646Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeBr646Temp() {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM BR646DATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			int rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBr646Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}		
}