package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:54
 * Description:
 * Copybook name: T5679REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5679rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5679Rec = new FixedLengthStringData(508);
  	public FixedLengthStringData cnPremStats = new FixedLengthStringData(24).isAPartOf(t5679Rec, 0);
  	public FixedLengthStringData[] cnPremStat = FLSArrayPartOfStructure(12, 2, cnPremStats, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(cnPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cnPremStat01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData cnPremStat02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData cnPremStat03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData cnPremStat04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData cnPremStat05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData cnPremStat06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData cnPremStat07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData cnPremStat08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData cnPremStat09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData cnPremStat10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData cnPremStat11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData cnPremStat12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData cnRiskStats = new FixedLengthStringData(24).isAPartOf(t5679Rec, 24);
  	public FixedLengthStringData[] cnRiskStat = FLSArrayPartOfStructure(12, 2, cnRiskStats, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(cnRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cnRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData cnRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData cnRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData cnRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData cnRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData cnRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData cnRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData cnRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData cnRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData cnRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	public FixedLengthStringData cnRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler1, 20);
  	public FixedLengthStringData cnRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler1, 22);
  	public FixedLengthStringData covPremStats = new FixedLengthStringData(24).isAPartOf(t5679Rec, 48);
  	public FixedLengthStringData[] covPremStat = FLSArrayPartOfStructure(12, 2, covPremStats, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(covPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData covPremStat01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData covPremStat02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData covPremStat03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData covPremStat04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData covPremStat05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData covPremStat06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData covPremStat07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData covPremStat08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData covPremStat09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData covPremStat10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData covPremStat11 = new FixedLengthStringData(2).isAPartOf(filler2, 20);
  	public FixedLengthStringData covPremStat12 = new FixedLengthStringData(2).isAPartOf(filler2, 22);
  	public FixedLengthStringData covRiskStats = new FixedLengthStringData(24).isAPartOf(t5679Rec, 72);
  	public FixedLengthStringData[] covRiskStat = FLSArrayPartOfStructure(12, 2, covRiskStats, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(covRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData covRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData covRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData covRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData covRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData covRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData covRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData covRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData covRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData covRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData covRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	public FixedLengthStringData covRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler3, 20);
  	public FixedLengthStringData covRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler3, 22);
  	public FixedLengthStringData jlifeStats = new FixedLengthStringData(12).isAPartOf(t5679Rec, 96);
  	public FixedLengthStringData[] jlifeStat = FLSArrayPartOfStructure(6, 2, jlifeStats, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(12).isAPartOf(jlifeStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData jlifeStat01 = new FixedLengthStringData(2).isAPartOf(filler4, 0);
  	public FixedLengthStringData jlifeStat02 = new FixedLengthStringData(2).isAPartOf(filler4, 2);
  	public FixedLengthStringData jlifeStat03 = new FixedLengthStringData(2).isAPartOf(filler4, 4);
  	public FixedLengthStringData jlifeStat04 = new FixedLengthStringData(2).isAPartOf(filler4, 6);
  	public FixedLengthStringData jlifeStat05 = new FixedLengthStringData(2).isAPartOf(filler4, 8);
  	public FixedLengthStringData jlifeStat06 = new FixedLengthStringData(2).isAPartOf(filler4, 10);
  	public FixedLengthStringData lifeStats = new FixedLengthStringData(12).isAPartOf(t5679Rec, 108);
  	public FixedLengthStringData[] lifeStat = FLSArrayPartOfStructure(6, 2, lifeStats, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(12).isAPartOf(lifeStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData lifeStat01 = new FixedLengthStringData(2).isAPartOf(filler5, 0);
  	public FixedLengthStringData lifeStat02 = new FixedLengthStringData(2).isAPartOf(filler5, 2);
  	public FixedLengthStringData lifeStat03 = new FixedLengthStringData(2).isAPartOf(filler5, 4);
  	public FixedLengthStringData lifeStat04 = new FixedLengthStringData(2).isAPartOf(filler5, 6);
  	public FixedLengthStringData lifeStat05 = new FixedLengthStringData(2).isAPartOf(filler5, 8);
  	public FixedLengthStringData lifeStat06 = new FixedLengthStringData(2).isAPartOf(filler5, 10);
  	public FixedLengthStringData ridPremStats = new FixedLengthStringData(24).isAPartOf(t5679Rec, 120);
  	public FixedLengthStringData[] ridPremStat = FLSArrayPartOfStructure(12, 2, ridPremStats, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(ridPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ridPremStat01 = new FixedLengthStringData(2).isAPartOf(filler6, 0);
  	public FixedLengthStringData ridPremStat02 = new FixedLengthStringData(2).isAPartOf(filler6, 2);
  	public FixedLengthStringData ridPremStat03 = new FixedLengthStringData(2).isAPartOf(filler6, 4);
  	public FixedLengthStringData ridPremStat04 = new FixedLengthStringData(2).isAPartOf(filler6, 6);
  	public FixedLengthStringData ridPremStat05 = new FixedLengthStringData(2).isAPartOf(filler6, 8);
  	public FixedLengthStringData ridPremStat06 = new FixedLengthStringData(2).isAPartOf(filler6, 10);
  	public FixedLengthStringData ridPremStat07 = new FixedLengthStringData(2).isAPartOf(filler6, 12);
  	public FixedLengthStringData ridPremStat08 = new FixedLengthStringData(2).isAPartOf(filler6, 14);
  	public FixedLengthStringData ridPremStat09 = new FixedLengthStringData(2).isAPartOf(filler6, 16);
  	public FixedLengthStringData ridPremStat10 = new FixedLengthStringData(2).isAPartOf(filler6, 18);
  	public FixedLengthStringData ridPremStat11 = new FixedLengthStringData(2).isAPartOf(filler6, 20);
  	public FixedLengthStringData ridPremStat12 = new FixedLengthStringData(2).isAPartOf(filler6, 22);
  	public FixedLengthStringData ridRiskStats = new FixedLengthStringData(24).isAPartOf(t5679Rec, 144);
  	public FixedLengthStringData[] ridRiskStat = FLSArrayPartOfStructure(12, 2, ridRiskStats, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(24).isAPartOf(ridRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ridRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler7, 0);
  	public FixedLengthStringData ridRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler7, 2);
  	public FixedLengthStringData ridRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler7, 4);
  	public FixedLengthStringData ridRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler7, 6);
  	public FixedLengthStringData ridRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler7, 8);
  	public FixedLengthStringData ridRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler7, 10);
  	public FixedLengthStringData ridRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler7, 12);
  	public FixedLengthStringData ridRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler7, 14);
  	public FixedLengthStringData ridRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler7, 16);
  	public FixedLengthStringData ridRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler7, 18);
  	public FixedLengthStringData ridRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler7, 20);
  	public FixedLengthStringData ridRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler7, 22);
  	public FixedLengthStringData setCnPremStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 168);
  	public FixedLengthStringData setCnRiskStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 170);
  	public FixedLengthStringData setCovPremStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 172);
  	public FixedLengthStringData setCovRiskStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 174);
  	public FixedLengthStringData setJlifeStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 176);
  	public FixedLengthStringData setLifeStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 178);
  	public FixedLengthStringData setRidPremStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 180);
  	public FixedLengthStringData setRidRiskStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 182);
  	public FixedLengthStringData setSngpCnStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 184);
  	public FixedLengthStringData setSngpCovStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 186);
  	public FixedLengthStringData setSngpRidStat = new FixedLengthStringData(2).isAPartOf(t5679Rec, 188);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(t5679Rec, 190);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(310).isAPartOf(t5679Rec, 198, FILLER);
  	
  	



	public void initialize() {
		COBOLFunctions.initialize(t5679Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5679Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}