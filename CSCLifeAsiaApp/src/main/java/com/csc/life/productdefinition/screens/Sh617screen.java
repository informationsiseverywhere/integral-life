package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh617screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh617ScreenVars sv = (Sh617ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh617screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh617ScreenVars screenVars = (Sh617ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.unit.setClassString("");
		screenVars.insprm01.setClassString("");
		screenVars.insprm02.setClassString("");
		screenVars.insprm03.setClassString("");
		screenVars.insprm04.setClassString("");
		screenVars.insprm05.setClassString("");
		screenVars.insprm06.setClassString("");
		screenVars.insprm07.setClassString("");
		screenVars.insprm08.setClassString("");
		screenVars.insprm09.setClassString("");
		screenVars.insprm10.setClassString("");
		screenVars.insprm11.setClassString("");
		screenVars.insprm12.setClassString("");
		screenVars.insprm13.setClassString("");
		screenVars.insprm14.setClassString("");
		screenVars.insprm15.setClassString("");
		screenVars.insprm16.setClassString("");
		screenVars.insprm17.setClassString("");
		screenVars.insprm18.setClassString("");
		screenVars.insprm19.setClassString("");
		screenVars.insprm20.setClassString("");
		screenVars.insprm21.setClassString("");
		screenVars.insprm22.setClassString("");
		screenVars.insprm23.setClassString("");
		screenVars.insprm24.setClassString("");
		screenVars.insprm25.setClassString("");
		screenVars.insprm26.setClassString("");
		screenVars.insprm27.setClassString("");
		screenVars.insprm28.setClassString("");
		screenVars.insprm29.setClassString("");
		screenVars.insprm30.setClassString("");
		screenVars.insprm31.setClassString("");
		screenVars.insprm32.setClassString("");
		screenVars.insprm33.setClassString("");
		screenVars.insprm34.setClassString("");
		screenVars.insprm35.setClassString("");
		screenVars.insprm36.setClassString("");
		screenVars.insprm37.setClassString("");
		screenVars.insprm38.setClassString("");
		screenVars.insprm39.setClassString("");
		screenVars.insprm40.setClassString("");
		screenVars.insprm41.setClassString("");
		screenVars.insprm42.setClassString("");
		screenVars.insprm43.setClassString("");
		screenVars.insprm44.setClassString("");
		screenVars.insprm45.setClassString("");
		screenVars.insprm46.setClassString("");
		screenVars.insprm47.setClassString("");
		screenVars.insprm48.setClassString("");
		screenVars.insprm49.setClassString("");
		screenVars.insprm50.setClassString("");
		screenVars.insprm51.setClassString("");
		screenVars.insprm52.setClassString("");
		screenVars.insprm53.setClassString("");
		screenVars.insprm54.setClassString("");
		screenVars.insprm55.setClassString("");
		screenVars.insprm56.setClassString("");
		screenVars.insprm57.setClassString("");
		screenVars.insprm58.setClassString("");
		screenVars.insprm59.setClassString("");
		screenVars.insprm60.setClassString("");
		screenVars.insprm61.setClassString("");
		screenVars.insprm62.setClassString("");
		screenVars.insprm63.setClassString("");
		screenVars.insprm64.setClassString("");
		screenVars.insprm65.setClassString("");
		screenVars.insprm66.setClassString("");
		screenVars.insprm67.setClassString("");
		screenVars.insprm68.setClassString("");
		screenVars.insprm69.setClassString("");
		screenVars.insprm70.setClassString("");
		screenVars.insprm71.setClassString("");
		screenVars.insprm72.setClassString("");
		screenVars.insprm73.setClassString("");
		screenVars.insprm74.setClassString("");
		screenVars.insprm75.setClassString("");
		screenVars.insprm76.setClassString("");
		screenVars.insprm77.setClassString("");
		screenVars.insprm78.setClassString("");
		screenVars.insprm79.setClassString("");
		screenVars.insprm80.setClassString("");
		screenVars.insprm81.setClassString("");
		screenVars.insprm82.setClassString("");
		screenVars.insprm83.setClassString("");
		screenVars.insprm84.setClassString("");
		screenVars.insprm85.setClassString("");
		screenVars.insprm86.setClassString("");
		screenVars.insprm87.setClassString("");
		screenVars.insprm88.setClassString("");
		screenVars.insprm89.setClassString("");
		screenVars.insprm90.setClassString("");
		screenVars.insprm91.setClassString("");
		screenVars.insprm92.setClassString("");
		screenVars.insprm93.setClassString("");
		screenVars.insprm94.setClassString("");
		screenVars.insprm95.setClassString("");
		screenVars.insprm96.setClassString("");
		screenVars.insprm97.setClassString("");
		screenVars.insprm98.setClassString("");
		screenVars.insprm99.setClassString("");
		screenVars.instpr01.setClassString("");
		screenVars.instpr02.setClassString("");
		screenVars.instpr03.setClassString("");
		screenVars.instpr04.setClassString("");
		screenVars.instpr05.setClassString("");
		screenVars.instpr06.setClassString("");
		screenVars.instpr07.setClassString("");
		screenVars.instpr08.setClassString("");
		screenVars.instpr09.setClassString("");
		screenVars.instpr10.setClassString("");
		screenVars.instpr11.setClassString("");
		screenVars.insprem.setClassString("");
		screenVars.disccntmeth.setClassString("");
		screenVars.premUnit.setClassString("");
	}

/**
 * Clear all the variables in Sh617screen
 */
	public static void clear(VarModel pv) {
		Sh617ScreenVars screenVars = (Sh617ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.unit.clear();
		screenVars.insprm01.clear();
		screenVars.insprm02.clear();
		screenVars.insprm03.clear();
		screenVars.insprm04.clear();
		screenVars.insprm05.clear();
		screenVars.insprm06.clear();
		screenVars.insprm07.clear();
		screenVars.insprm08.clear();
		screenVars.insprm09.clear();
		screenVars.insprm10.clear();
		screenVars.insprm11.clear();
		screenVars.insprm12.clear();
		screenVars.insprm13.clear();
		screenVars.insprm14.clear();
		screenVars.insprm15.clear();
		screenVars.insprm16.clear();
		screenVars.insprm17.clear();
		screenVars.insprm18.clear();
		screenVars.insprm19.clear();
		screenVars.insprm20.clear();
		screenVars.insprm21.clear();
		screenVars.insprm22.clear();
		screenVars.insprm23.clear();
		screenVars.insprm24.clear();
		screenVars.insprm25.clear();
		screenVars.insprm26.clear();
		screenVars.insprm27.clear();
		screenVars.insprm28.clear();
		screenVars.insprm29.clear();
		screenVars.insprm30.clear();
		screenVars.insprm31.clear();
		screenVars.insprm32.clear();
		screenVars.insprm33.clear();
		screenVars.insprm34.clear();
		screenVars.insprm35.clear();
		screenVars.insprm36.clear();
		screenVars.insprm37.clear();
		screenVars.insprm38.clear();
		screenVars.insprm39.clear();
		screenVars.insprm40.clear();
		screenVars.insprm41.clear();
		screenVars.insprm42.clear();
		screenVars.insprm43.clear();
		screenVars.insprm44.clear();
		screenVars.insprm45.clear();
		screenVars.insprm46.clear();
		screenVars.insprm47.clear();
		screenVars.insprm48.clear();
		screenVars.insprm49.clear();
		screenVars.insprm50.clear();
		screenVars.insprm51.clear();
		screenVars.insprm52.clear();
		screenVars.insprm53.clear();
		screenVars.insprm54.clear();
		screenVars.insprm55.clear();
		screenVars.insprm56.clear();
		screenVars.insprm57.clear();
		screenVars.insprm58.clear();
		screenVars.insprm59.clear();
		screenVars.insprm60.clear();
		screenVars.insprm61.clear();
		screenVars.insprm62.clear();
		screenVars.insprm63.clear();
		screenVars.insprm64.clear();
		screenVars.insprm65.clear();
		screenVars.insprm66.clear();
		screenVars.insprm67.clear();
		screenVars.insprm68.clear();
		screenVars.insprm69.clear();
		screenVars.insprm70.clear();
		screenVars.insprm71.clear();
		screenVars.insprm72.clear();
		screenVars.insprm73.clear();
		screenVars.insprm74.clear();
		screenVars.insprm75.clear();
		screenVars.insprm76.clear();
		screenVars.insprm77.clear();
		screenVars.insprm78.clear();
		screenVars.insprm79.clear();
		screenVars.insprm80.clear();
		screenVars.insprm81.clear();
		screenVars.insprm82.clear();
		screenVars.insprm83.clear();
		screenVars.insprm84.clear();
		screenVars.insprm85.clear();
		screenVars.insprm86.clear();
		screenVars.insprm87.clear();
		screenVars.insprm88.clear();
		screenVars.insprm89.clear();
		screenVars.insprm90.clear();
		screenVars.insprm91.clear();
		screenVars.insprm92.clear();
		screenVars.insprm93.clear();
		screenVars.insprm94.clear();
		screenVars.insprm95.clear();
		screenVars.insprm96.clear();
		screenVars.insprm97.clear();
		screenVars.insprm98.clear();
		screenVars.insprm99.clear();
		screenVars.instpr01.clear();
		screenVars.instpr02.clear();
		screenVars.instpr03.clear();
		screenVars.instpr04.clear();
		screenVars.instpr05.clear();
		screenVars.instpr06.clear();
		screenVars.instpr07.clear();
		screenVars.instpr08.clear();
		screenVars.instpr09.clear();
		screenVars.instpr10.clear();
		screenVars.instpr11.clear();
		screenVars.insprem.clear();
		screenVars.disccntmeth.clear();
		screenVars.premUnit.clear();
	}
}
