/*
 * File: Pr643.java
 * Date: 30 August 2009 1:50:58
 * Author: Quipoz Limited
 * 
 * Class transformed from PR643.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.MediTableDAM;
import com.csc.life.productdefinition.dataaccess.MediivcTableDAM;
import com.csc.life.productdefinition.dataaccess.MediseqTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpfTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.screens.Sr643ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr640rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Medical Bill Data Capture by Invoice
* ------------------------------------
*
***********************************************************************
* </pre>
*/
public class Pr643 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR643");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	private String wsaaLargeName = "LGNMS";
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaEntityDesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(2, 0);
	private ZonedDecimalData wsaaMedFee = new ZonedDecimalData(17, 2);
	private ZonedDecimalData ix = new ZonedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaFoundValidStatcode = new FixedLengthStringData(1);
	private Validator foundValidStatcode = new Validator(wsaaFoundValidStatcode, "Y");

	private FixedLengthStringData wsaaBlankRec = new FixedLengthStringData(1);
	private Validator blankRec = new Validator(wsaaBlankRec, "Y");

	private FixedLengthStringData wsaaFoundSubfileRec = new FixedLengthStringData(1);
	private Validator foundSubfileRec = new Validator(wsaaFoundSubfileRec, "Y");

	private FixedLengthStringData wsaaFoundLoadFromMedi = new FixedLengthStringData(1);
	private Validator foundLoadFromMedi = new Validator(wsaaFoundLoadFromMedi, "Y");
		/* ERRORS */
	private String rl70 = "RL70";
	private String rl69 = "RL69";
	private String e186 = "E186";
	private String e207 = "E207";
	private String e305 = "E305";
	private String e655 = "E655";
	private String f189 = "F189";
	private String f259 = "F259";
	private String f321 = "F321";
	private String h137 = "H137";
	private String h143 = "H143";
		/* TABLES */
	private String tr640 = "TR640";
	private String t5679 = "T5679";
		/* FORMATS */
	private String aglfrec = "AGLFREC";
	private String agntrec = "AGNTREC";
	private String clntrec = "CLNTREC";
	private String chdrenqrec = "CHDRENQREC";
	private String itemrec = "ITEMREC";
	private String lifelnbrec = "LIFELNBREC";
	private String medirec = "MEDIREC";
	private String mediivcrec = "MEDIIVCREC";
	private String mediseqrec = "MEDISEQREC";
	private String zmpfrec = "ZMPFREC";
	private String zmpvrec = "ZMPVREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Medical Bill Payment Detail*/
	private MediTableDAM mediIO = new MediTableDAM();
		/*Medical Bill Paymt Dtls Inv No & Chdr*/
	private MediivcTableDAM mediivcIO = new MediivcTableDAM();
		/*Medical Bill Payment by seqno*/
	private MediseqTableDAM mediseqIO = new MediseqTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Tr640rec tr640rec = new Tr640rec();
	private Batckey wsaaBatchkey = new Batckey();
		/*Medical Provider Facilities*/
	private ZmpfTableDAM zmpfIO = new ZmpfTableDAM();
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Sr643ScreenVars sv = ScreenProgram.getScreenVars( Sr643ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		name1110, 
		exit1110, 
		nextMediivc1200, 
		preExit, 
		exit2090, 
		updateErrorIndicators2670, 
		exit2620, 
		exit2645, 
		nextRec3100, 
		exit3100
	}

	public Pr643() {
		super();
		screenVars = sv;
		new ScreenModel("Sr643", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.pgmnam.set(wsaaProg);
		wsaaBatchkey.set(wsspcomn.batchkey);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		mediivcIO.setFormat(mediivcrec);
		mediivcIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, mediivcIO);
		if (isNE(mediivcIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediivcIO.getStatuz());
			syserrrec.params.set(mediivcIO.getParams());
			fatalError600();
		}
		setScreen1100();
		mediivcIO.setDataArea(SPACES);
		mediivcIO.setStatuz(varcom.oK);
		mediivcIO.setChdrcoy(wsspcomn.company);
		mediivcIO.setInvref(sv.invref);
		mediivcIO.setChdrnum(SPACES);
		mediivcIO.setSeqno(ZERO);
		mediivcIO.setFormat(mediivcrec);
		mediivcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mediivcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediivcIO.setFitKeysSearch("CHDRCOY", "INVREF");
		SmartFileCode.execute(appVars, mediivcIO);
		if (isNE(mediivcIO.getStatuz(),varcom.oK)
		&& isNE(mediivcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mediivcIO.getStatuz());
			syserrrec.params.set(mediivcIO.getParams());
			fatalError600();
		}
		if (isNE(mediivcIO.getStatuz(),varcom.oK)
		|| isNE(mediivcIO.getChdrcoy(),wsspcomn.company)
		|| isNE(mediivcIO.getInvref(),sv.invref)) {
			mediivcIO.setStatuz(varcom.endp);
		}
		wsaaCount.set(1);
		while ( !(isEQ(mediivcIO.getStatuz(),varcom.endp))) {
			loadMediivc1200();
		}
		
		if (isGT(wsaaCount,sv.subfilePage)) {
			wsaaCount.set(1);
		}
		while ( !(isGT(wsaaCount,sv.subfilePage))) {
			blankSubfile1250();
		}
		
		scrnparams.subfileMore.set("Y");
		scrnparams.subfileRrn.set(1);
	}

protected void setScreen1100()
	{
		/*BEGIN*/
		sv.invref.set(mediivcIO.getInvref());
		sv.paidby.set(mediivcIO.getPaidby());
		sv.exmcode.set(mediivcIO.getExmcode());
		getEntityDesc1110();
		sv.name.set(wsaaEntityDesc);
		/*EXIT*/
	}

protected void getEntityDesc1110()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin1110();
				}
				case name1110: {
					name1110();
				}
				case exit1110: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1110()
	{
		wsaaClntnum.set(SPACES);
		if (isEQ(sv.paidby,"A")){
			agntClntnum1111();
			if (isNE(agntIO.getStatuz(),varcom.oK)) {
				sv.exmcodeErr.set(e305);
				goTo(GotoLabel.name1110);
			}
			aglfRec1117();
			if (isNE(aglfIO.getDtetrm(),varcom.vrcmMaxDate)) {
				sv.exmcodeErr.set(f189);
			}
		}
		else if (isEQ(sv.paidby,"D")){
			medProvClntnum1113();
			if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
				sv.exmcodeErr.set(rl69);
				goTo(GotoLabel.name1110);
			}
			if (isNE(zmpvIO.getDtetrm(),varcom.vrcmMaxDate)) {
				sv.exmcodeErr.set(rl70);
			}
		}
		else if (isEQ(sv.paidby,"C")){
			clntClntnum1115();
			if (isNE(clntIO.getStatuz(),varcom.oK)) {
				sv.exmcodeErr.set(e655);
				goTo(GotoLabel.name1110);
			}
		}
	}

protected void name1110()
	{
		if (isEQ(wsaaClntnum,SPACES)) {
			wsaaEntityDesc.set(SPACES);
			goTo(GotoLabel.exit1110);
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.statuz.set(varcom.oK);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
		wsaaEntityDesc.set(namadrsrec.name);
	}

protected void agntClntnum1111()
	{
		begin1111();
	}

protected void begin1111()
	{
		agntIO.setDataArea(SPACES);
		agntIO.setStatuz(varcom.oK);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(sv.exmcode);
		agntIO.setFormat(agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isEQ(agntIO.getStatuz(),varcom.oK)) {
			wsaaClntnum.set(agntIO.getClntnum());
		}
	}

protected void medProvClntnum1113()
	{
		begin1113();
	}

protected void begin1113()
	{
		zmpvIO.setDataArea(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.exmcode);
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)
		&& isNE(zmpvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpvIO.getStatuz(),varcom.oK)) {
			wsaaClntnum.set(zmpvIO.getZmednum());
		}
	}

protected void clntClntnum1115()
	{
		begin1115();
	}

protected void begin1115()
	{
		clntIO.setDataArea(SPACES);
		clntIO.setStatuz(varcom.oK);
		clntIO.setClntpfx(fsupfxcpy.clnt);
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setClntnum(sv.exmcode);
		clntIO.setFormat(clntrec);
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)
		&& isNE(clntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		if (isEQ(clntIO.getStatuz(),varcom.oK)) {
			wsaaClntnum.set(sv.exmcode);
		}
	}

protected void aglfRec1117()
	{
		begin1117();
	}

protected void begin1117()
	{
		aglfIO.setDataArea(SPACES);
		aglfIO.setStatuz(varcom.oK);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.exmcode);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void loadMediivc1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin1200();
				}
				case nextMediivc1200: {
					nextMediivc1200();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1200()
	{
		if (isEQ(mediivcIO.getActiveInd(),"4")) {
			goTo(GotoLabel.nextMediivc1200);
		}
		sv.initialiseSubfileArea();
		sv.chdrsel.set(mediivcIO.getChdrnum());
		sv.life.set(mediivcIO.getLife());
		sv.jlife.set(mediivcIO.getJlife());
		sv.zmedtyp.set(mediivcIO.getZmedtyp());
		sv.effdate.set(mediivcIO.getEffdate());
		sv.zmedfee.set(mediivcIO.getZmedfee());
		sv.seqno.set(mediivcIO.getSeqno());
		sv.chdrnum.set(mediivcIO.getChdrnum());
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaCount.add(1);
	}

protected void nextMediivc1200()
	{
		mediivcIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, mediivcIO);
		if (isNE(mediivcIO.getStatuz(),varcom.oK)
		&& isNE(mediivcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mediivcIO.getStatuz());
			syserrrec.params.set(mediivcIO.getParams());
			fatalError600();
		}
		if (isNE(mediivcIO.getStatuz(),varcom.oK)
		|| isNE(mediivcIO.getChdrcoy(),wsspcomn.company)
		|| isNE(mediivcIO.getInvref(),sv.invref)) {
			mediivcIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void blankSubfile1250()
	{
		/*BEGIN*/
		sv.initialiseSubfileArea();
		sv.pgmnam.set(wsaaProg);
		sv.chdrsel.set(SPACES);
		sv.life.set(SPACES);
		sv.jlife.set(SPACES);
		sv.zmedtyp.set(SPACES);
		sv.zmedfee.set(ZERO);
		sv.seqno.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.chdrnum.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaCount.add(1);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaCount.set(1);
			while ( !(isGT(wsaaCount,sv.subfilePage))) {
				blankSubfile1250();
			}
			
			scrnparams.subfileMore.set("Y");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(sv.paidby,SPACES)) {
			sv.paidbyErr.set(e186);
		}
		if (isEQ(sv.exmcode,SPACES)) {
			sv.exmcodeErr.set(e186);
		}
		else {
			getEntityDesc1110();
			sv.name.set(wsaaEntityDesc);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		wsaaFoundLoadFromMedi.set("N");
		wsaaFoundSubfileRec.set("N");
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (!foundSubfileRec.isTrue()
		&& !foundLoadFromMedi.isTrue()) {
			scrnparams.subfileRrn.set(1);
			scrnparams.function.set(varcom.sread);
			screenIo9000();
			sv.chdrselErr.set(e207);
			sv.lifeErr.set(e207);
			sv.jlifeErr.set(e207);
			sv.zmedtypErr.set(e207);
			sv.effdateErr.set(e207);
			sv.zmedfeeErr.set(e207);
			scrnparams.function.set(varcom.supd);
			screenIo9000();
			wsspcomn.edterror.set("Y");
		}
		scrnparams.subfileRrn.set(1);
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		sv.pgmnam.set(wsaaProg);
		if (isEQ(sv.chdrsel,SPACES)
		&& isEQ(sv.zmedtyp,SPACES)
		&& isEQ(sv.life,SPACES)
		&& isEQ(sv.jlife,SPACES)
		&& isEQ(sv.effdate,varcom.vrcmMaxDate)
		&& isEQ(sv.zmedfee,ZERO)) {
			if (isNE(sv.seqno,ZERO)) {
				wsaaFoundLoadFromMedi.set("Y");
			}
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		wsaaFoundSubfileRec.set("Y");
		
		if (isEQ(sv.chdrsel,SPACES)) {
			sv.chdrselErr.set(e186);
		}
		if (isNE(sv.chdrsel,SPACES)) {
			if (isEQ(sv.life,SPACES)) {
				sv.lifeErr.set(e207);
			}
			if (isEQ(sv.jlife,SPACES)) {
				sv.jlifeErr.set(e207);
			}
			validateStatus2620();
		}
		if (isEQ(sv.zmedtyp,SPACES)) {
			sv.zmedtypErr.set(e186);
		}
		if (isNE(sv.life,SPACES)
		&& isNE(sv.jlife,SPACES)) {
			readLifelnb2650();
			if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
				sv.lifeErr.set(h143);
				sv.jlifeErr.set(h143);
			}
		}
		if (isEQ(sv.paidby,SPACES)) {
			sv.paidbyErr.set(e186);
		}
		if (isEQ(sv.exmcode,SPACES)) {
			sv.exmcodeErr.set(e186);
		}
		if (isEQ(sv.paidby,"A")
		|| isEQ(sv.paidby,"C")) {
			if (isEQ(sv.zmedfee,ZERO)) {
				defaultFeeTr6402630();
			}
		}
		else {
			if (isEQ(sv.paidby,"D")
			&& isEQ(sv.zmedfee,ZERO)) {
				defaultFeeZmpf2640();
			}
		}
		if (isEQ(sv.zmedfee,ZERO)) {
			sv.zmedfeeErr.set(e186);
		}
		if (isEQ(sv.effdate,varcom.vrcmMaxDate)) {
			sv.effdateErr.set(e186);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void validateStatus2620()
	{
		try {
			start2620();
		}
		catch (GOTOException e){
		}
	}

protected void start2620()
	{
		chdrenqIO.setDataArea(SPACES);
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrsel);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			sv.chdrselErr.set(f259);
			goTo(GotoLabel.exit2620);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.chdrselErr.set(f321);
			goTo(GotoLabel.exit2620);
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaFoundValidStatcode.set("N");
		for (ix.set(1); !(isGT(ix,12)
		|| foundValidStatcode.isTrue()); ix.add(1)){
			if (isEQ(chdrenqIO.getStatcode(),t5679rec.cnRiskStat[ix.toInt()])) {
				wsaaFoundValidStatcode.set("Y");
			}
		}
		if (!foundValidStatcode.isTrue()) {
			sv.chdrselErr.set(h137);
		}
	}

protected void defaultFeeTr6402630()
	{
		start2630();
	}

protected void start2630()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr640);
		itemIO.setItemitem(sv.zmedtyp);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			tr640rec.tr640Rec.set(itemIO.getGenarea());
			sv.zmedfee.set(tr640rec.zmedfee);
		}
	}

protected void defaultFeeZmpf2640()
	{
		start2640();
	}

protected void start2640()
	{
		wsaaMedFee.set(ZERO);
		zmpfIO.setDataKey(SPACES);
		zmpfIO.setZmedcoy(wsspcomn.company);
		zmpfIO.setZmedprv(sv.exmcode);
		zmpfIO.setSeqno(1);
		zmpfIO.setFormat(zmpfrec);
		zmpfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zmpfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zmpfIO.setFitKeysSearch("ZMEDCOY", "ZMEDPRV");
		zmpfIO.setStatuz(varcom.oK);
		while ( !(isEQ(zmpfIO.getStatuz(),varcom.endp))) {
			loopZmpf2645();
		}
		
		if (isNE(wsaaMedFee,ZERO)) {
			sv.zmedfee.set(wsaaMedFee);
		}
		else {
			defaultFeeTr6402630();
		}
	}

protected void loopZmpf2645()
	{
		try {
			start2645();
		}
		catch (GOTOException e){
		}
	}

protected void start2645()
	{
		SmartFileCode.execute(appVars, zmpfIO);
		if (isNE(zmpfIO.getStatuz(),varcom.oK)
		&& isNE(zmpfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zmpfIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpfIO.getStatuz(),varcom.endp)
		|| isNE(zmpfIO.getZmedcoy(),wsspcomn.company)
		|| isNE(zmpfIO.getZmedprv(),sv.exmcode)) {
			zmpfIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2645);
		}
		if (isEQ(zmpfIO.getZmedtyp(),sv.zmedtyp)) {
			wsaaMedFee.set(zmpfIO.getZmedfee());
		}
		zmpfIO.setFunction(varcom.nextr);
	}

protected void readLifelnb2650()
	{
		start2650();
	}

protected void start2650()
	{
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(sv.chdrsel);
		lifelnbIO.setLife(sv.life);
		lifelnbIO.setJlife(sv.jlife);
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			updateSubfile3100();
		}
		
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(sv.invref);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void updateSubfile3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin3100();
				}
				case nextRec3100: {
					nextRec3100();
				}
				case exit3100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3100()
	{
		processScreen("SR643", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3100);
		}
		if (isEQ(sv.chdrsel,SPACES)
		&& isEQ(sv.life,SPACES)
		&& isEQ(sv.jlife,SPACES)
		&& isEQ(sv.zmedtyp,SPACES)
		&& isEQ(sv.effdate,varcom.vrcmMaxDate)
		&& isEQ(sv.zmedfee,ZERO)) {
			if (isEQ(sv.seqno,ZERO)) {
				goTo(GotoLabel.nextRec3100);
			}
			else {
				wsaaBlankRec.set("Y");
			}
		}
		else {
			wsaaBlankRec.set("N");
		}
		mediIO.setDataArea(SPACES);
		mediIO.setStatuz(varcom.oK);
		mediIO.setChdrcoy(wsspcomn.company);
		mediIO.setChdrnum(sv.chdrnum);
		mediIO.setSeqno(sv.seqno);
		mediIO.setFormat(medirec);
		mediIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)
		&& isNE(mediIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		if (isEQ(mediIO.getStatuz(),varcom.oK)) {
			if (blankRec.isTrue()) {
				deleteMedi3110();
			}
			else {
				if (isEQ(sv.chdrnum,sv.chdrsel)) {
					rewrtMedi3140();
				}
				else {
					deleteMedi3110();
					readMediseq3120();
					createMedi3130();
				}
			}
		}
		else {
			readMediseq3120();
			createMedi3130();
		}
	}

protected void nextRec3100()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void deleteMedi3110()
	{
		start3110();
	}

protected void start3110()
	{
		mediIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		mediIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
	}

protected void readMediseq3120()
	{
		begin3120();
	}

protected void begin3120()
	{
		mediseqIO.setDataArea(SPACES);
		mediseqIO.setStatuz(varcom.oK);
		mediseqIO.setChdrcoy(wsspcomn.company);
		mediseqIO.setChdrnum(sv.chdrsel);
		mediseqIO.setSeqno(99999);
		mediseqIO.setFormat(mediseqrec);
		mediseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mediseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediseqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, mediseqIO);
		if (isNE(mediseqIO.getStatuz(),varcom.oK)
		&& isNE(mediseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mediseqIO.getStatuz());
			syserrrec.params.set(mediseqIO.getParams());
			fatalError600();
		}
		if (isNE(mediseqIO.getStatuz(),varcom.oK)
		|| isNE(mediseqIO.getChdrcoy(),wsspcomn.company)
		|| isNE(mediseqIO.getChdrnum(),sv.chdrsel)) {
			mediseqIO.setStatuz(varcom.endp);
		}
		if (isEQ(mediseqIO.getStatuz(),varcom.oK)) {
			compute(wsaaSeqno, 0).set(add(mediseqIO.getSeqno(),1));
		}
		else {
			wsaaSeqno.set(1);
		}
	}

protected void createMedi3130()
	{
		start3130();
	}

protected void start3130()
	{
		mediIO.setChdrcoy(wsspcomn.company);
		mediIO.setChdrnum(sv.chdrsel);
		mediIO.setSeqno(wsaaSeqno);
		mediIO.setLife(sv.life);
		mediIO.setJlife(sv.jlife);
		mediIO.setZmedtyp(sv.zmedtyp);
		mediIO.setExmcode(sv.exmcode);
		mediIO.setZmedfee(sv.zmedfee);
		mediIO.setPaidby(sv.paidby);
		mediIO.setInvref(sv.invref);
		mediIO.setEffdate(sv.effdate);
		mediIO.setActiveInd("0");
		mediIO.setPaydte(varcom.vrcmMaxDate);
		mediIO.setTranno(ZERO);
		mediIO.setFormat(medirec);
		mediIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
	}

protected void rewrtMedi3140()
	{
		start3140();
	}

protected void start3140()
	{
		mediIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		mediIO.setLife(sv.life);
		mediIO.setJlife(sv.jlife);
		mediIO.setZmedtyp(sv.zmedtyp);
		mediIO.setExmcode(sv.exmcode);
		mediIO.setZmedfee(sv.zmedfee);
		mediIO.setPaidby(sv.paidby);
		mediIO.setInvref(sv.invref);
		mediIO.setEffdate(sv.effdate);
		mediIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen("SR643", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
