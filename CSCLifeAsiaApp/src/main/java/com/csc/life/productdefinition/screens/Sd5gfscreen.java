package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5gfscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5gfScreenVars sv = (Sd5gfScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5gfscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5gfScreenVars screenVars = (Sd5gfScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.ratebas.setClassString("");
		screenVars.gender01.setClassString("");
		screenVars.gender02.setClassString("");
		screenVars.gender03.setClassString("");
		screenVars.gender04.setClassString("");
		screenVars.frmage01.setClassString("");
		screenVars.frmage02.setClassString("");
		screenVars.frmage03.setClassString("");
		screenVars.frmage04.setClassString("");
		screenVars.toage01.setClassString("");
		screenVars.toage02.setClassString("");
		screenVars.toage03.setClassString("");
		screenVars.toage04.setClassString("");
		screenVars.coolingoff1.setClassString("");
		screenVars.coolingoff2.setClassString("");
		screenVars.coolingoff3.setClassString("");
		screenVars.coolingoff4.setClassString("");
	}

/**
 * Clear all the variables 
 */
	public static void clear(VarModel pv) {
		Sd5gfScreenVars screenVars = (Sd5gfScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.ratebas.clear();
		screenVars.gender01.clear();
		screenVars.gender02.clear();
		screenVars.gender03.clear();
		screenVars.gender04.clear();
		screenVars.frmage01.clear();
		screenVars.frmage02.clear();
		screenVars.frmage03.clear();
		screenVars.frmage04.clear();
		screenVars.toage01.clear();
		screenVars.toage02.clear();
		screenVars.toage03.clear();
		screenVars.toage04.clear();
		screenVars.coolingoff1.clear();
		screenVars.coolingoff2.clear();		
		screenVars.coolingoff3.clear();
		screenVars.coolingoff4.clear();
	}
}
