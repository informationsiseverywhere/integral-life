package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5688screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 4, 22, 17, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5688ScreenVars sv = (S5688ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5688screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5688ScreenVars screenVars = (S5688ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.polmin.setClassString("");
		screenVars.polmax.setClassString("");
		screenVars.poldef.setClassString("");
		screenVars.lifemin.setClassString("");
		screenVars.lifemax.setClassString("");
		screenVars.jlifemin.setClassString("");
		screenVars.jlifemax.setClassString("");
		screenVars.feemeth.setClassString("");
		screenVars.defFupMeth.setClassString("");
		screenVars.minDepMth.setClassString("");
		screenVars.taxrelmth.setClassString("");
		screenVars.cfiLim.setClassString("");
		screenVars.nbusallw.setClassString("");
		screenVars.znfopt.setClassString("");
		screenVars.bankcode.setClassString("");
		screenVars.anbrqd.setClassString("");
		screenVars.comlvlacc.setClassString("");
		screenVars.smkrqd.setClassString("");
		screenVars.revacc.setClassString("");
		screenVars.occrqd.setClassString("");
		screenVars.zcfidtall.setClassString("");
		screenVars.zcertall.setClassString("");
		//ILIFE-1137
		screenVars.dflexmeth.setClassString("");
		screenVars.bfreqallw.setClassString("");
	}

/**
 * Clear all the variables in S5688screen
 */
	public static void clear(VarModel pv) {
		S5688ScreenVars screenVars = (S5688ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.polmin.clear();
		screenVars.polmax.clear();
		screenVars.poldef.clear();
		screenVars.lifemin.clear();
		screenVars.lifemax.clear();
		screenVars.jlifemin.clear();
		screenVars.jlifemax.clear();
		screenVars.feemeth.clear();
		screenVars.defFupMeth.clear();
		screenVars.minDepMth.clear();
		screenVars.taxrelmth.clear();
		screenVars.cfiLim.clear();
		screenVars.nbusallw.clear();
		screenVars.znfopt.clear();
		screenVars.bankcode.clear();
		screenVars.anbrqd.clear();
		screenVars.comlvlacc.clear();
		screenVars.smkrqd.clear();
		screenVars.revacc.clear();
		screenVars.occrqd.clear();
		screenVars.zcfidtall.clear();
		screenVars.zcertall.clear();
		//ILIFE-1137
		screenVars.dflexmeth.clear();
		screenVars.bfreqallw.clear();
	}
}
