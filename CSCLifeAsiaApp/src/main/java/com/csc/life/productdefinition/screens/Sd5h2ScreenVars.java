package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5h2ScreenVars extends SmartVarModel{

	public FixedLengthStringData dataArea = new FixedLengthStringData(314);
	public FixedLengthStringData dataFields = new FixedLengthStringData(58).isAPartOf(dataArea, 0);
	public FixedLengthStringData accInd = DD.accind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntsdets = DD.agntsdets.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData crcind = DD.crcind.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData printLocation = DD.dbprtloc.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,5);
	public FixedLengthStringData grpind = DD.grpind.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData mediaRequired = DD.mediareq.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData payind = DD.payind.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData triggerRequired = DD.triggreq.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData rollovrind = DD.rollovrind.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 58);
	public FixedLengthStringData accindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntsdetsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData dbprtlocErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData grpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData mediareqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData payindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData sacscodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData sacstypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData triggreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData rollovrindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 122);
	public FixedLengthStringData[] accindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntsdetsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] dbprtlocOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] grpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] mediareqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] payindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] sacscodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] sacstypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] triggreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] rollovrindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sd5h2screenWritten = new LongData(0);
	public LongData Sd5h2protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5h2ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(sacscodeOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstypOut,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dbprtlocOut,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rollovrindOut,new String[] {null,null, null,"34", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, agntsdets, payind, ddind, grpind, accInd, triggerRequired, sacscode, sacstyp, mediaRequired, printLocation, crcind, rollovrind};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, agntsdetsOut, payindOut, ddindOut, grpindOut, accindOut, triggreqOut, sacscodeOut, sacstypOut, mediareqOut, dbprtlocOut, crcindOut, rollovrindOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, agntsdetsErr, payindErr, ddindErr, grpindErr, accindErr, triggreqErr, sacscodeErr, sacstypErr, mediareqErr, dbprtlocErr, crcindErr, rollovrindErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5h2screen.class;
		protectRecord = Sd5h2protect.class;
	}

}
