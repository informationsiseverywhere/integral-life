package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MedientTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:28
 * Class transformed from MEDIENT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MedientTableDAM extends MedipfTableDAM {

	public MedientTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MEDIENT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", PAIDBY"
		             + ", EXMCODE"
		             + ", CHDRNUM"
		             + ", SEQNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "SEQNO, " +
		            "ZMEDTYP, " +
		            "EXMCODE, " +
		            "ZMEDFEE, " +
		            "PAIDBY, " +
		            "INVREF, " +
		            "EFFDATE, " +
		            "ACTIND, " +
		            "PAYDTE, " +
		            "TRANNO, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "PAIDBY ASC, " +
		            "EXMCODE ASC, " +
		            "CHDRNUM ASC, " +
		            "SEQNO ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "PAIDBY DESC, " +
		            "EXMCODE DESC, " +
		            "CHDRNUM DESC, " +
		            "SEQNO DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               seqno,
                               zmedtyp,
                               exmcode,
                               zmedfee,
                               paidby,
                               invref,
                               effdate,
                               activeInd,
                               paydte,
                               tranno,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(234);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getPaidby().toInternal()
					+ getExmcode().toInternal()
					+ getChdrnum().toInternal()
					+ getSeqno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, paidby);
			what = ExternalData.chop(what, exmcode);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(10);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller50.setInternal(seqno.toInternal());
	nonKeyFiller70.setInternal(exmcode.toInternal());
	nonKeyFiller90.setInternal(paidby.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(118);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ nonKeyFiller50.toInternal()
					+ getZmedtyp().toInternal()
					+ nonKeyFiller70.toInternal()
					+ getZmedfee().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getInvref().toInternal()
					+ getEffdate().toInternal()
					+ getActiveInd().toInternal()
					+ getPaydte().toInternal()
					+ getTranno().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, zmedtyp);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, zmedfee);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, invref);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, activeInd);
			what = ExternalData.chop(what, paydte);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getPaidby() {
		return paidby;
	}
	public void setPaidby(Object what) {
		paidby.set(what);
	}
	public FixedLengthStringData getExmcode() {
		return exmcode;
	}
	public void setExmcode(Object what) {
		exmcode.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getZmedtyp() {
		return zmedtyp;
	}
	public void setZmedtyp(Object what) {
		zmedtyp.set(what);
	}	
	public PackedDecimalData getZmedfee() {
		return zmedfee;
	}
	public void setZmedfee(Object what) {
		setZmedfee(what, false);
	}
	public void setZmedfee(Object what, boolean rounded) {
		if (rounded)
			zmedfee.setRounded(what);
		else
			zmedfee.set(what);
	}	
	public FixedLengthStringData getInvref() {
		return invref;
	}
	public void setInvref(Object what) {
		invref.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(Object what) {
		activeInd.set(what);
	}	
	public PackedDecimalData getPaydte() {
		return paydte;
	}
	public void setPaydte(Object what) {
		setPaydte(what, false);
	}
	public void setPaydte(Object what, boolean rounded) {
		if (rounded)
			paydte.setRounded(what);
		else
			paydte.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		paidby.clear();
		exmcode.clear();
		chdrnum.clear();
		seqno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		life.clear();
		jlife.clear();
		nonKeyFiller50.clear();
		zmedtyp.clear();
		nonKeyFiller70.clear();
		zmedfee.clear();
		nonKeyFiller90.clear();
		invref.clear();
		effdate.clear();
		activeInd.clear();
		paydte.clear();
		tranno.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}