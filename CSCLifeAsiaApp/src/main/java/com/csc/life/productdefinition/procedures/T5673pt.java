/*
 * File: T5673pt.java
 * Date: 30 August 2009 2:25:36
 * Author: Quipoz Limited
 * 
 * Class transformed from T5673PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5673rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5673.
*
*
*****************************************************************
* </pre>
*/
public class T5673pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(75);
	private FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine001, 32, FILLER).init("Contract Structure                    S5673");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 26, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine003, 0, FILLER).init("    Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 18);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 28, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(77);
	private FixedLengthStringData filler9 = new FixedLengthStringData(77).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Coverage                   Allowable riders              Max");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine005, 7, FILLER).init("Life Rqd?      Covers");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(69);
	private FixedLengthStringData filler12 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(51).isAPartOf(wsaaPrtLine006, 18, FILLER).init("1         2         3         4         5         6");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(77);
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 2);
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 9);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 13);
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 16);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 22);
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 26);
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 32);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 36);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 42);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 46);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 52);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 56);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 62);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 66);
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 72);
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 2);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 9);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 13);
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 16);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 22);
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 26);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 32);
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 36);
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 42);
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 46);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 52);
	private FixedLengthStringData filler41 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 56);
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 62);
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 66);
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72);
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 2);
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 9);
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 13);
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 16);
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 22);
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 26);
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 32);
	private FixedLengthStringData filler53 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 36);
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 42);
	private FixedLengthStringData filler55 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 46);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 52);
	private FixedLengthStringData filler57 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 56);
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 62);
	private FixedLengthStringData filler59 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 66);
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 72);
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 2);
	private FixedLengthStringData filler63 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 9);
	private FixedLengthStringData filler64 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 13);
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 16);
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 22);
	private FixedLengthStringData filler67 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 26);
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 32);
	private FixedLengthStringData filler69 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 36);
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 42);
	private FixedLengthStringData filler71 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 46);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 52);
	private FixedLengthStringData filler73 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 56);
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 62);
	private FixedLengthStringData filler75 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 66);
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 72);
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 2);
	private FixedLengthStringData filler79 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 9);
	private FixedLengthStringData filler80 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 13);
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 16);
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 22);
	private FixedLengthStringData filler83 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 26);
	private FixedLengthStringData filler84 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 32);
	private FixedLengthStringData filler85 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 36);
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 42);
	private FixedLengthStringData filler87 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 46);
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 52);
	private FixedLengthStringData filler89 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 56);
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 62);
	private FixedLengthStringData filler91 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 66);
	private FixedLengthStringData filler92 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 72);
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(77);
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 2);
	private FixedLengthStringData filler95 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 9);
	private FixedLengthStringData filler96 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 13);
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 16);
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 22);
	private FixedLengthStringData filler99 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 26);
	private FixedLengthStringData filler100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo093 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 32);
	private FixedLengthStringData filler101 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 36);
	private FixedLengthStringData filler102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 42);
	private FixedLengthStringData filler103 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 46);
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 52);
	private FixedLengthStringData filler105 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 56);
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 62);
	private FixedLengthStringData filler107 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 66);
	private FixedLengthStringData filler108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 72);
	private FixedLengthStringData filler109 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(77);
	private FixedLengthStringData filler110 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo103 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 2);
	private FixedLengthStringData filler111 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 9);
	private FixedLengthStringData filler112 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 13);
	private FixedLengthStringData filler113 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo106 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 16);
	private FixedLengthStringData filler114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 22);
	private FixedLengthStringData filler115 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo108 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 26);
	private FixedLengthStringData filler116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 32);
	private FixedLengthStringData filler117 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo110 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 36);
	private FixedLengthStringData filler118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 42);
	private FixedLengthStringData filler119 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo112 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 46);
	private FixedLengthStringData filler120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo113 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 52);
	private FixedLengthStringData filler121 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo114 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 56);
	private FixedLengthStringData filler122 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 62);
	private FixedLengthStringData filler123 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo116 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 66);
	private FixedLengthStringData filler124 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 72);
	private FixedLengthStringData filler125 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo118 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(77);
	private FixedLengthStringData filler126 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo119 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 2);
	private FixedLengthStringData filler127 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo120 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 9);
	private FixedLengthStringData filler128 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo121 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 13);
	private FixedLengthStringData filler129 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo122 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 16);
	private FixedLengthStringData filler130 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 22);
	private FixedLengthStringData filler131 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo124 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 26);
	private FixedLengthStringData filler132 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 32);
	private FixedLengthStringData filler133 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo126 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 36);
	private FixedLengthStringData filler134 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo127 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 42);
	private FixedLengthStringData filler135 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo128 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 46);
	private FixedLengthStringData filler136 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo129 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 52);
	private FixedLengthStringData filler137 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo130 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 56);
	private FixedLengthStringData filler138 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo131 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 62);
	private FixedLengthStringData filler139 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo132 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 66);
	private FixedLengthStringData filler140 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 70, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo133 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 72);
	private FixedLengthStringData filler141 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo134 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 75).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(70);
	private FixedLengthStringData filler142 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler143 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine015, 41, FILLER).init("Continuation item:");
	private FixedLengthStringData fieldNo135 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 62);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5673rec t5673rec = new T5673rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5673pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5673rec.t5673Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5673rec.ctable01);
		fieldNo009.set(t5673rec.creq01);
		fieldNo010.set(t5673rec.rtable01);
		fieldNo011.set(t5673rec.rreq01);
		fieldNo012.set(t5673rec.rtable02);
		fieldNo013.set(t5673rec.rreq02);
		fieldNo014.set(t5673rec.rtable03);
		fieldNo015.set(t5673rec.rreq03);
		fieldNo016.set(t5673rec.rtable04);
		fieldNo017.set(t5673rec.rreq04);
		fieldNo018.set(t5673rec.rtable05);
		fieldNo019.set(t5673rec.rreq05);
		fieldNo020.set(t5673rec.rtable06);
		fieldNo021.set(t5673rec.rreq06);
		fieldNo022.set(t5673rec.ctmaxcov01);
		fieldNo023.set(t5673rec.ctable02);
		fieldNo025.set(t5673rec.creq02);
		fieldNo026.set(t5673rec.rtable07);
		fieldNo027.set(t5673rec.rreq07);
		fieldNo028.set(t5673rec.rtable08);
		fieldNo029.set(t5673rec.rreq08);
		fieldNo030.set(t5673rec.rtable09);
		fieldNo031.set(t5673rec.rreq09);
		fieldNo032.set(t5673rec.rtable10);
		fieldNo033.set(t5673rec.rreq10);
		fieldNo034.set(t5673rec.rtable11);
		fieldNo035.set(t5673rec.rreq11);
		fieldNo036.set(t5673rec.rtable12);
		fieldNo037.set(t5673rec.rreq12);
		fieldNo038.set(t5673rec.ctmaxcov02);
		fieldNo039.set(t5673rec.ctable03);
		fieldNo041.set(t5673rec.creq03);
		fieldNo042.set(t5673rec.rtable13);
		fieldNo043.set(t5673rec.rreq13);
		fieldNo044.set(t5673rec.rtable14);
		fieldNo045.set(t5673rec.rreq14);
		fieldNo046.set(t5673rec.rtable15);
		fieldNo047.set(t5673rec.rreq15);
		fieldNo048.set(t5673rec.rtable16);
		fieldNo049.set(t5673rec.rreq16);
		fieldNo050.set(t5673rec.rtable17);
		fieldNo051.set(t5673rec.rreq17);
		fieldNo052.set(t5673rec.rtable18);
		fieldNo053.set(t5673rec.rreq18);
		fieldNo054.set(t5673rec.ctmaxcov03);
		fieldNo055.set(t5673rec.ctable04);
		fieldNo057.set(t5673rec.creq04);
		fieldNo058.set(t5673rec.rtable19);
		fieldNo059.set(t5673rec.rreq19);
		fieldNo060.set(t5673rec.rtable20);
		fieldNo061.set(t5673rec.rreq20);
		fieldNo062.set(t5673rec.rtable21);
		fieldNo063.set(t5673rec.rreq21);
		fieldNo064.set(t5673rec.rtable22);
		fieldNo065.set(t5673rec.rreq22);
		fieldNo066.set(t5673rec.rtable23);
		fieldNo067.set(t5673rec.rreq23);
		fieldNo068.set(t5673rec.rtable24);
		fieldNo069.set(t5673rec.rreq24);
		fieldNo070.set(t5673rec.ctmaxcov04);
		fieldNo071.set(t5673rec.ctable05);
		fieldNo073.set(t5673rec.creq05);
		fieldNo074.set(t5673rec.rtable25);
		fieldNo075.set(t5673rec.rreq25);
		fieldNo076.set(t5673rec.rtable26);
		fieldNo077.set(t5673rec.rreq26);
		fieldNo078.set(t5673rec.rtable27);
		fieldNo079.set(t5673rec.rreq27);
		fieldNo080.set(t5673rec.rtable28);
		fieldNo081.set(t5673rec.rreq28);
		fieldNo082.set(t5673rec.rtable29);
		fieldNo083.set(t5673rec.rreq29);
		fieldNo084.set(t5673rec.rtable30);
		fieldNo085.set(t5673rec.rreq30);
		fieldNo086.set(t5673rec.ctmaxcov05);
		fieldNo087.set(t5673rec.ctable06);
		fieldNo089.set(t5673rec.creq06);
		fieldNo090.set(t5673rec.rtable31);
		fieldNo091.set(t5673rec.rreq31);
		fieldNo092.set(t5673rec.rtable32);
		fieldNo093.set(t5673rec.rreq32);
		fieldNo094.set(t5673rec.rtable33);
		fieldNo095.set(t5673rec.rreq33);
		fieldNo096.set(t5673rec.rtable34);
		fieldNo097.set(t5673rec.rreq34);
		fieldNo098.set(t5673rec.rtable35);
		fieldNo099.set(t5673rec.rreq35);
		fieldNo100.set(t5673rec.rtable36);
		fieldNo101.set(t5673rec.rreq36);
		fieldNo102.set(t5673rec.ctmaxcov06);
		fieldNo103.set(t5673rec.ctable07);
		fieldNo105.set(t5673rec.creq07);
		fieldNo106.set(t5673rec.rtable37);
		fieldNo107.set(t5673rec.rreq37);
		fieldNo108.set(t5673rec.rtable38);
		fieldNo109.set(t5673rec.rreq38);
		fieldNo110.set(t5673rec.rtable39);
		fieldNo111.set(t5673rec.rreq39);
		fieldNo112.set(t5673rec.rtable40);
		fieldNo113.set(t5673rec.rreq40);
		fieldNo114.set(t5673rec.rtable41);
		fieldNo115.set(t5673rec.rreq41);
		fieldNo116.set(t5673rec.rtable42);
		fieldNo117.set(t5673rec.rreq42);
		fieldNo118.set(t5673rec.ctmaxcov07);
		fieldNo119.set(t5673rec.ctable08);
		fieldNo121.set(t5673rec.creq08);
		fieldNo122.set(t5673rec.rtable43);
		fieldNo123.set(t5673rec.rreq43);
		fieldNo124.set(t5673rec.rtable44);
		fieldNo125.set(t5673rec.rreq44);
		fieldNo126.set(t5673rec.rtable45);
		fieldNo127.set(t5673rec.rreq45);
		fieldNo128.set(t5673rec.rtable46);
		fieldNo129.set(t5673rec.rreq46);
		fieldNo130.set(t5673rec.rtable47);
		fieldNo131.set(t5673rec.rreq47);
		fieldNo132.set(t5673rec.rtable48);
		fieldNo133.set(t5673rec.rreq48);
		fieldNo134.set(t5673rec.ctmaxcov08);
		fieldNo135.set(t5673rec.gitem);
		fieldNo008.set(t5673rec.zrlifind01);
		fieldNo024.set(t5673rec.zrlifind02);
		fieldNo040.set(t5673rec.zrlifind03);
		fieldNo056.set(t5673rec.zrlifind04);
		fieldNo072.set(t5673rec.zrlifind05);
		fieldNo088.set(t5673rec.zrlifind06);
		fieldNo104.set(t5673rec.zrlifind07);
		fieldNo120.set(t5673rec.zrlifind08);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
