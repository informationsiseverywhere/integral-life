package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LfevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:27
 * Class transformed from LFEV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LfevTableDAM extends LfevpfTableDAM {

	public LfevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LFEV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", EFFDATE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "EFFDATE, " +
		            "CURRFROM, " +
		            "VALIDFLAG, " +
		            "ALNAME01, " +
		            "ALNAME02, " +
		            "ALNAME03, " +
		            "ALNAME04, " +
		            "ALNAME05, " +
		            "JLNAME01, " +
		            "JLNAME02, " +
		            "JLNAME03, " +
		            "JLNAME04, " +
		            "JLNAME05, " +
		            "SMOKEIND01, " +
		            "SMOKEIND02, " +
		            "SMOKEIND03, " +
		            "SMOKEIND04, " +
		            "SMOKEIND05, " +
		            "SMKRQD01, " +
		            "SMKRQD02, " +
		            "SMKRQD03, " +
		            "SMKRQD04, " +
		            "SMKRQD05, " +
		            "ANBCCD01, " +
		            "ANBCCD02, " +
		            "ANBCCD03, " +
		            "ANBCCD04, " +
		            "ANBCCD05, " +
		            "JANBCCD01, " +
		            "JANBCCD02, " +
		            "JANBCCD03, " +
		            "JANBCCD04, " +
		            "JANBCCD05, " +
		            "ANBISD01, " +
		            "ANBISD02, " +
		            "ANBISD03, " +
		            "ANBISD04, " +
		            "ANBISD05, " +
		            "JANBISD01, " +
		            "JANBISD02, " +
		            "JANBISD03, " +
		            "JANBISD04, " +
		            "JANBISD05, " +
		            "SEX01, " +
		            "SEX02, " +
		            "SEX03, " +
		            "SEX04, " +
		            "SEX05, " +
		            "JLSEX01, " +
		            "JLSEX02, " +
		            "JLSEX03, " +
		            "JLSEX04, " +
		            "JLSEX05, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "EFFDATE DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "EFFDATE ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               effdate,
                               currfrom,
                               validflag,
                               alname01,
                               alname02,
                               alname03,
                               alname04,
                               alname05,
                               jlname01,
                               jlname02,
                               jlname03,
                               jlname04,
                               jlname05,
                               smokeind01,
                               smokeind02,
                               smokeind03,
                               smokeind04,
                               smokeind05,
                               smkrqd01,
                               smkrqd02,
                               smkrqd03,
                               smkrqd04,
                               smkrqd05,
                               anbAtCcd01,
                               anbAtCcd02,
                               anbAtCcd03,
                               anbAtCcd04,
                               anbAtCcd05,
                               janbccd01,
                               janbccd02,
                               janbccd03,
                               janbccd04,
                               janbccd05,
                               anbisd01,
                               anbisd02,
                               anbisd03,
                               anbisd04,
                               anbisd05,
                               janbisd01,
                               janbisd02,
                               janbisd03,
                               janbisd04,
                               janbisd05,
                               sex01,
                               sex02,
                               sex03,
                               sex04,
                               sex05,
                               jlsex01,
                               jlsex02,
                               jlsex03,
                               jlsex04,
                               jlsex05,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(48);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getEffdate().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(effdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(603);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getCurrfrom().toInternal()
					+ getValidflag().toInternal()
					+ getAlname01().toInternal()
					+ getAlname02().toInternal()
					+ getAlname03().toInternal()
					+ getAlname04().toInternal()
					+ getAlname05().toInternal()
					+ getJlname01().toInternal()
					+ getJlname02().toInternal()
					+ getJlname03().toInternal()
					+ getJlname04().toInternal()
					+ getJlname05().toInternal()
					+ getSmokeind01().toInternal()
					+ getSmokeind02().toInternal()
					+ getSmokeind03().toInternal()
					+ getSmokeind04().toInternal()
					+ getSmokeind05().toInternal()
					+ getSmkrqd01().toInternal()
					+ getSmkrqd02().toInternal()
					+ getSmkrqd03().toInternal()
					+ getSmkrqd04().toInternal()
					+ getSmkrqd05().toInternal()
					+ getAnbAtCcd01().toInternal()
					+ getAnbAtCcd02().toInternal()
					+ getAnbAtCcd03().toInternal()
					+ getAnbAtCcd04().toInternal()
					+ getAnbAtCcd05().toInternal()
					+ getJanbccd01().toInternal()
					+ getJanbccd02().toInternal()
					+ getJanbccd03().toInternal()
					+ getJanbccd04().toInternal()
					+ getJanbccd05().toInternal()
					+ getAnbisd01().toInternal()
					+ getAnbisd02().toInternal()
					+ getAnbisd03().toInternal()
					+ getAnbisd04().toInternal()
					+ getAnbisd05().toInternal()
					+ getJanbisd01().toInternal()
					+ getJanbisd02().toInternal()
					+ getJanbisd03().toInternal()
					+ getJanbisd04().toInternal()
					+ getJanbisd05().toInternal()
					+ getSex01().toInternal()
					+ getSex02().toInternal()
					+ getSex03().toInternal()
					+ getSex04().toInternal()
					+ getSex05().toInternal()
					+ getJlsex01().toInternal()
					+ getJlsex02().toInternal()
					+ getJlsex03().toInternal()
					+ getJlsex04().toInternal()
					+ getJlsex05().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, alname01);
			what = ExternalData.chop(what, alname02);
			what = ExternalData.chop(what, alname03);
			what = ExternalData.chop(what, alname04);
			what = ExternalData.chop(what, alname05);
			what = ExternalData.chop(what, jlname01);
			what = ExternalData.chop(what, jlname02);
			what = ExternalData.chop(what, jlname03);
			what = ExternalData.chop(what, jlname04);
			what = ExternalData.chop(what, jlname05);
			what = ExternalData.chop(what, smokeind01);
			what = ExternalData.chop(what, smokeind02);
			what = ExternalData.chop(what, smokeind03);
			what = ExternalData.chop(what, smokeind04);
			what = ExternalData.chop(what, smokeind05);
			what = ExternalData.chop(what, smkrqd01);
			what = ExternalData.chop(what, smkrqd02);
			what = ExternalData.chop(what, smkrqd03);
			what = ExternalData.chop(what, smkrqd04);
			what = ExternalData.chop(what, smkrqd05);
			what = ExternalData.chop(what, anbAtCcd01);
			what = ExternalData.chop(what, anbAtCcd02);
			what = ExternalData.chop(what, anbAtCcd03);
			what = ExternalData.chop(what, anbAtCcd04);
			what = ExternalData.chop(what, anbAtCcd05);
			what = ExternalData.chop(what, janbccd01);
			what = ExternalData.chop(what, janbccd02);
			what = ExternalData.chop(what, janbccd03);
			what = ExternalData.chop(what, janbccd04);
			what = ExternalData.chop(what, janbccd05);
			what = ExternalData.chop(what, anbisd01);
			what = ExternalData.chop(what, anbisd02);
			what = ExternalData.chop(what, anbisd03);
			what = ExternalData.chop(what, anbisd04);
			what = ExternalData.chop(what, anbisd05);
			what = ExternalData.chop(what, janbisd01);
			what = ExternalData.chop(what, janbisd02);
			what = ExternalData.chop(what, janbisd03);
			what = ExternalData.chop(what, janbisd04);
			what = ExternalData.chop(what, janbisd05);
			what = ExternalData.chop(what, sex01);
			what = ExternalData.chop(what, sex02);
			what = ExternalData.chop(what, sex03);
			what = ExternalData.chop(what, sex04);
			what = ExternalData.chop(what, sex05);
			what = ExternalData.chop(what, jlsex01);
			what = ExternalData.chop(what, jlsex02);
			what = ExternalData.chop(what, jlsex03);
			what = ExternalData.chop(what, jlsex04);
			what = ExternalData.chop(what, jlsex05);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getAlname01() {
		return alname01;
	}
	public void setAlname01(Object what) {
		alname01.set(what);
	}	
	public FixedLengthStringData getAlname02() {
		return alname02;
	}
	public void setAlname02(Object what) {
		alname02.set(what);
	}	
	public FixedLengthStringData getAlname03() {
		return alname03;
	}
	public void setAlname03(Object what) {
		alname03.set(what);
	}	
	public FixedLengthStringData getAlname04() {
		return alname04;
	}
	public void setAlname04(Object what) {
		alname04.set(what);
	}	
	public FixedLengthStringData getAlname05() {
		return alname05;
	}
	public void setAlname05(Object what) {
		alname05.set(what);
	}	
	public FixedLengthStringData getJlname01() {
		return jlname01;
	}
	public void setJlname01(Object what) {
		jlname01.set(what);
	}	
	public FixedLengthStringData getJlname02() {
		return jlname02;
	}
	public void setJlname02(Object what) {
		jlname02.set(what);
	}	
	public FixedLengthStringData getJlname03() {
		return jlname03;
	}
	public void setJlname03(Object what) {
		jlname03.set(what);
	}	
	public FixedLengthStringData getJlname04() {
		return jlname04;
	}
	public void setJlname04(Object what) {
		jlname04.set(what);
	}	
	public FixedLengthStringData getJlname05() {
		return jlname05;
	}
	public void setJlname05(Object what) {
		jlname05.set(what);
	}	
	public FixedLengthStringData getSmokeind01() {
		return smokeind01;
	}
	public void setSmokeind01(Object what) {
		smokeind01.set(what);
	}	
	public FixedLengthStringData getSmokeind02() {
		return smokeind02;
	}
	public void setSmokeind02(Object what) {
		smokeind02.set(what);
	}	
	public FixedLengthStringData getSmokeind03() {
		return smokeind03;
	}
	public void setSmokeind03(Object what) {
		smokeind03.set(what);
	}	
	public FixedLengthStringData getSmokeind04() {
		return smokeind04;
	}
	public void setSmokeind04(Object what) {
		smokeind04.set(what);
	}	
	public FixedLengthStringData getSmokeind05() {
		return smokeind05;
	}
	public void setSmokeind05(Object what) {
		smokeind05.set(what);
	}	
	public FixedLengthStringData getSmkrqd01() {
		return smkrqd01;
	}
	public void setSmkrqd01(Object what) {
		smkrqd01.set(what);
	}	
	public FixedLengthStringData getSmkrqd02() {
		return smkrqd02;
	}
	public void setSmkrqd02(Object what) {
		smkrqd02.set(what);
	}	
	public FixedLengthStringData getSmkrqd03() {
		return smkrqd03;
	}
	public void setSmkrqd03(Object what) {
		smkrqd03.set(what);
	}	
	public FixedLengthStringData getSmkrqd04() {
		return smkrqd04;
	}
	public void setSmkrqd04(Object what) {
		smkrqd04.set(what);
	}	
	public FixedLengthStringData getSmkrqd05() {
		return smkrqd05;
	}
	public void setSmkrqd05(Object what) {
		smkrqd05.set(what);
	}	
	public PackedDecimalData getAnbAtCcd01() {
		return anbAtCcd01;
	}
	public void setAnbAtCcd01(Object what) {
		setAnbAtCcd01(what, false);
	}
	public void setAnbAtCcd01(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd01.setRounded(what);
		else
			anbAtCcd01.set(what);
	}	
	public PackedDecimalData getAnbAtCcd02() {
		return anbAtCcd02;
	}
	public void setAnbAtCcd02(Object what) {
		setAnbAtCcd02(what, false);
	}
	public void setAnbAtCcd02(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd02.setRounded(what);
		else
			anbAtCcd02.set(what);
	}	
	public PackedDecimalData getAnbAtCcd03() {
		return anbAtCcd03;
	}
	public void setAnbAtCcd03(Object what) {
		setAnbAtCcd03(what, false);
	}
	public void setAnbAtCcd03(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd03.setRounded(what);
		else
			anbAtCcd03.set(what);
	}	
	public PackedDecimalData getAnbAtCcd04() {
		return anbAtCcd04;
	}
	public void setAnbAtCcd04(Object what) {
		setAnbAtCcd04(what, false);
	}
	public void setAnbAtCcd04(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd04.setRounded(what);
		else
			anbAtCcd04.set(what);
	}	
	public PackedDecimalData getAnbAtCcd05() {
		return anbAtCcd05;
	}
	public void setAnbAtCcd05(Object what) {
		setAnbAtCcd05(what, false);
	}
	public void setAnbAtCcd05(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd05.setRounded(what);
		else
			anbAtCcd05.set(what);
	}	
	public PackedDecimalData getJanbccd01() {
		return janbccd01;
	}
	public void setJanbccd01(Object what) {
		setJanbccd01(what, false);
	}
	public void setJanbccd01(Object what, boolean rounded) {
		if (rounded)
			janbccd01.setRounded(what);
		else
			janbccd01.set(what);
	}	
	public PackedDecimalData getJanbccd02() {
		return janbccd02;
	}
	public void setJanbccd02(Object what) {
		setJanbccd02(what, false);
	}
	public void setJanbccd02(Object what, boolean rounded) {
		if (rounded)
			janbccd02.setRounded(what);
		else
			janbccd02.set(what);
	}	
	public PackedDecimalData getJanbccd03() {
		return janbccd03;
	}
	public void setJanbccd03(Object what) {
		setJanbccd03(what, false);
	}
	public void setJanbccd03(Object what, boolean rounded) {
		if (rounded)
			janbccd03.setRounded(what);
		else
			janbccd03.set(what);
	}	
	public PackedDecimalData getJanbccd04() {
		return janbccd04;
	}
	public void setJanbccd04(Object what) {
		setJanbccd04(what, false);
	}
	public void setJanbccd04(Object what, boolean rounded) {
		if (rounded)
			janbccd04.setRounded(what);
		else
			janbccd04.set(what);
	}	
	public PackedDecimalData getJanbccd05() {
		return janbccd05;
	}
	public void setJanbccd05(Object what) {
		setJanbccd05(what, false);
	}
	public void setJanbccd05(Object what, boolean rounded) {
		if (rounded)
			janbccd05.setRounded(what);
		else
			janbccd05.set(what);
	}	
	public PackedDecimalData getAnbisd01() {
		return anbisd01;
	}
	public void setAnbisd01(Object what) {
		setAnbisd01(what, false);
	}
	public void setAnbisd01(Object what, boolean rounded) {
		if (rounded)
			anbisd01.setRounded(what);
		else
			anbisd01.set(what);
	}	
	public PackedDecimalData getAnbisd02() {
		return anbisd02;
	}
	public void setAnbisd02(Object what) {
		setAnbisd02(what, false);
	}
	public void setAnbisd02(Object what, boolean rounded) {
		if (rounded)
			anbisd02.setRounded(what);
		else
			anbisd02.set(what);
	}	
	public PackedDecimalData getAnbisd03() {
		return anbisd03;
	}
	public void setAnbisd03(Object what) {
		setAnbisd03(what, false);
	}
	public void setAnbisd03(Object what, boolean rounded) {
		if (rounded)
			anbisd03.setRounded(what);
		else
			anbisd03.set(what);
	}	
	public PackedDecimalData getAnbisd04() {
		return anbisd04;
	}
	public void setAnbisd04(Object what) {
		setAnbisd04(what, false);
	}
	public void setAnbisd04(Object what, boolean rounded) {
		if (rounded)
			anbisd04.setRounded(what);
		else
			anbisd04.set(what);
	}	
	public PackedDecimalData getAnbisd05() {
		return anbisd05;
	}
	public void setAnbisd05(Object what) {
		setAnbisd05(what, false);
	}
	public void setAnbisd05(Object what, boolean rounded) {
		if (rounded)
			anbisd05.setRounded(what);
		else
			anbisd05.set(what);
	}	
	public PackedDecimalData getJanbisd01() {
		return janbisd01;
	}
	public void setJanbisd01(Object what) {
		setJanbisd01(what, false);
	}
	public void setJanbisd01(Object what, boolean rounded) {
		if (rounded)
			janbisd01.setRounded(what);
		else
			janbisd01.set(what);
	}	
	public PackedDecimalData getJanbisd02() {
		return janbisd02;
	}
	public void setJanbisd02(Object what) {
		setJanbisd02(what, false);
	}
	public void setJanbisd02(Object what, boolean rounded) {
		if (rounded)
			janbisd02.setRounded(what);
		else
			janbisd02.set(what);
	}	
	public PackedDecimalData getJanbisd03() {
		return janbisd03;
	}
	public void setJanbisd03(Object what) {
		setJanbisd03(what, false);
	}
	public void setJanbisd03(Object what, boolean rounded) {
		if (rounded)
			janbisd03.setRounded(what);
		else
			janbisd03.set(what);
	}	
	public PackedDecimalData getJanbisd04() {
		return janbisd04;
	}
	public void setJanbisd04(Object what) {
		setJanbisd04(what, false);
	}
	public void setJanbisd04(Object what, boolean rounded) {
		if (rounded)
			janbisd04.setRounded(what);
		else
			janbisd04.set(what);
	}	
	public PackedDecimalData getJanbisd05() {
		return janbisd05;
	}
	public void setJanbisd05(Object what) {
		setJanbisd05(what, false);
	}
	public void setJanbisd05(Object what, boolean rounded) {
		if (rounded)
			janbisd05.setRounded(what);
		else
			janbisd05.set(what);
	}	
	public FixedLengthStringData getSex01() {
		return sex01;
	}
	public void setSex01(Object what) {
		sex01.set(what);
	}	
	public FixedLengthStringData getSex02() {
		return sex02;
	}
	public void setSex02(Object what) {
		sex02.set(what);
	}	
	public FixedLengthStringData getSex03() {
		return sex03;
	}
	public void setSex03(Object what) {
		sex03.set(what);
	}	
	public FixedLengthStringData getSex04() {
		return sex04;
	}
	public void setSex04(Object what) {
		sex04.set(what);
	}	
	public FixedLengthStringData getSex05() {
		return sex05;
	}
	public void setSex05(Object what) {
		sex05.set(what);
	}	
	public FixedLengthStringData getJlsex01() {
		return jlsex01;
	}
	public void setJlsex01(Object what) {
		jlsex01.set(what);
	}	
	public FixedLengthStringData getJlsex02() {
		return jlsex02;
	}
	public void setJlsex02(Object what) {
		jlsex02.set(what);
	}	
	public FixedLengthStringData getJlsex03() {
		return jlsex03;
	}
	public void setJlsex03(Object what) {
		jlsex03.set(what);
	}	
	public FixedLengthStringData getJlsex04() {
		return jlsex04;
	}
	public void setJlsex04(Object what) {
		jlsex04.set(what);
	}	
	public FixedLengthStringData getJlsex05() {
		return jlsex05;
	}
	public void setJlsex05(Object what) {
		jlsex05.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSmokeinds() {
		return new FixedLengthStringData(smokeind01.toInternal()
										+ smokeind02.toInternal()
										+ smokeind03.toInternal()
										+ smokeind04.toInternal()
										+ smokeind05.toInternal());
	}
	public void setSmokeinds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSmokeinds().getLength()).init(obj);
	
		what = ExternalData.chop(what, smokeind01);
		what = ExternalData.chop(what, smokeind02);
		what = ExternalData.chop(what, smokeind03);
		what = ExternalData.chop(what, smokeind04);
		what = ExternalData.chop(what, smokeind05);
	}
	public FixedLengthStringData getSmokeind(BaseData indx) {
		return getSmokeind(indx.toInt());
	}
	public FixedLengthStringData getSmokeind(int indx) {

		switch (indx) {
			case 1 : return smokeind01;
			case 2 : return smokeind02;
			case 3 : return smokeind03;
			case 4 : return smokeind04;
			case 5 : return smokeind05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSmokeind(BaseData indx, Object what) {
		setSmokeind(indx.toInt(), what);
	}
	public void setSmokeind(int indx, Object what) {

		switch (indx) {
			case 1 : setSmokeind01(what);
					 break;
			case 2 : setSmokeind02(what);
					 break;
			case 3 : setSmokeind03(what);
					 break;
			case 4 : setSmokeind04(what);
					 break;
			case 5 : setSmokeind05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSmkrqds() {
		return new FixedLengthStringData(smkrqd01.toInternal()
										+ smkrqd02.toInternal()
										+ smkrqd03.toInternal()
										+ smkrqd04.toInternal()
										+ smkrqd05.toInternal());
	}
	public void setSmkrqds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSmkrqds().getLength()).init(obj);
	
		what = ExternalData.chop(what, smkrqd01);
		what = ExternalData.chop(what, smkrqd02);
		what = ExternalData.chop(what, smkrqd03);
		what = ExternalData.chop(what, smkrqd04);
		what = ExternalData.chop(what, smkrqd05);
	}
	public FixedLengthStringData getSmkrqd(BaseData indx) {
		return getSmkrqd(indx.toInt());
	}
	public FixedLengthStringData getSmkrqd(int indx) {

		switch (indx) {
			case 1 : return smkrqd01;
			case 2 : return smkrqd02;
			case 3 : return smkrqd03;
			case 4 : return smkrqd04;
			case 5 : return smkrqd05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSmkrqd(BaseData indx, Object what) {
		setSmkrqd(indx.toInt(), what);
	}
	public void setSmkrqd(int indx, Object what) {

		switch (indx) {
			case 1 : setSmkrqd01(what);
					 break;
			case 2 : setSmkrqd02(what);
					 break;
			case 3 : setSmkrqd03(what);
					 break;
			case 4 : setSmkrqd04(what);
					 break;
			case 5 : setSmkrqd05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSexs() {
		return new FixedLengthStringData(sex01.toInternal()
										+ sex02.toInternal()
										+ sex03.toInternal()
										+ sex04.toInternal()
										+ sex05.toInternal());
	}
	public void setSexs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSexs().getLength()).init(obj);
	
		what = ExternalData.chop(what, sex01);
		what = ExternalData.chop(what, sex02);
		what = ExternalData.chop(what, sex03);
		what = ExternalData.chop(what, sex04);
		what = ExternalData.chop(what, sex05);
	}
	public FixedLengthStringData getSex(BaseData indx) {
		return getSex(indx.toInt());
	}
	public FixedLengthStringData getSex(int indx) {

		switch (indx) {
			case 1 : return sex01;
			case 2 : return sex02;
			case 3 : return sex03;
			case 4 : return sex04;
			case 5 : return sex05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSex(BaseData indx, Object what) {
		setSex(indx.toInt(), what);
	}
	public void setSex(int indx, Object what) {

		switch (indx) {
			case 1 : setSex01(what);
					 break;
			case 2 : setSex02(what);
					 break;
			case 3 : setSex03(what);
					 break;
			case 4 : setSex04(what);
					 break;
			case 5 : setSex05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getJlsexs() {
		return new FixedLengthStringData(jlsex01.toInternal()
										+ jlsex02.toInternal()
										+ jlsex03.toInternal()
										+ jlsex04.toInternal()
										+ jlsex05.toInternal());
	}
	public void setJlsexs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getJlsexs().getLength()).init(obj);
	
		what = ExternalData.chop(what, jlsex01);
		what = ExternalData.chop(what, jlsex02);
		what = ExternalData.chop(what, jlsex03);
		what = ExternalData.chop(what, jlsex04);
		what = ExternalData.chop(what, jlsex05);
	}
	public FixedLengthStringData getJlsex(BaseData indx) {
		return getJlsex(indx.toInt());
	}
	public FixedLengthStringData getJlsex(int indx) {

		switch (indx) {
			case 1 : return jlsex01;
			case 2 : return jlsex02;
			case 3 : return jlsex03;
			case 4 : return jlsex04;
			case 5 : return jlsex05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setJlsex(BaseData indx, Object what) {
		setJlsex(indx.toInt(), what);
	}
	public void setJlsex(int indx, Object what) {

		switch (indx) {
			case 1 : setJlsex01(what);
					 break;
			case 2 : setJlsex02(what);
					 break;
			case 3 : setJlsex03(what);
					 break;
			case 4 : setJlsex04(what);
					 break;
			case 5 : setJlsex05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getJlnames() {
		return new FixedLengthStringData(jlname01.toInternal()
										+ jlname02.toInternal()
										+ jlname03.toInternal()
										+ jlname04.toInternal()
										+ jlname05.toInternal());
	}
	public void setJlnames(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getJlnames().getLength()).init(obj);
	
		what = ExternalData.chop(what, jlname01);
		what = ExternalData.chop(what, jlname02);
		what = ExternalData.chop(what, jlname03);
		what = ExternalData.chop(what, jlname04);
		what = ExternalData.chop(what, jlname05);
	}
	public FixedLengthStringData getJlname(BaseData indx) {
		return getJlname(indx.toInt());
	}
	public FixedLengthStringData getJlname(int indx) {

		switch (indx) {
			case 1 : return jlname01;
			case 2 : return jlname02;
			case 3 : return jlname03;
			case 4 : return jlname04;
			case 5 : return jlname05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setJlname(BaseData indx, Object what) {
		setJlname(indx.toInt(), what);
	}
	public void setJlname(int indx, Object what) {

		switch (indx) {
			case 1 : setJlname01(what);
					 break;
			case 2 : setJlname02(what);
					 break;
			case 3 : setJlname03(what);
					 break;
			case 4 : setJlname04(what);
					 break;
			case 5 : setJlname05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getJanbisds() {
		return new FixedLengthStringData(janbisd01.toInternal()
										+ janbisd02.toInternal()
										+ janbisd03.toInternal()
										+ janbisd04.toInternal()
										+ janbisd05.toInternal());
	}
	public void setJanbisds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getJanbisds().getLength()).init(obj);
	
		what = ExternalData.chop(what, janbisd01);
		what = ExternalData.chop(what, janbisd02);
		what = ExternalData.chop(what, janbisd03);
		what = ExternalData.chop(what, janbisd04);
		what = ExternalData.chop(what, janbisd05);
	}
	public PackedDecimalData getJanbisd(BaseData indx) {
		return getJanbisd(indx.toInt());
	}
	public PackedDecimalData getJanbisd(int indx) {

		switch (indx) {
			case 1 : return janbisd01;
			case 2 : return janbisd02;
			case 3 : return janbisd03;
			case 4 : return janbisd04;
			case 5 : return janbisd05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setJanbisd(BaseData indx, Object what) {
		setJanbisd(indx, what, false);
	}
	public void setJanbisd(BaseData indx, Object what, boolean rounded) {
		setJanbisd(indx.toInt(), what, rounded);
	}
	public void setJanbisd(int indx, Object what) {
		setJanbisd(indx, what, false);
	}
	public void setJanbisd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setJanbisd01(what, rounded);
					 break;
			case 2 : setJanbisd02(what, rounded);
					 break;
			case 3 : setJanbisd03(what, rounded);
					 break;
			case 4 : setJanbisd04(what, rounded);
					 break;
			case 5 : setJanbisd05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getJanbccds() {
		return new FixedLengthStringData(janbccd01.toInternal()
										+ janbccd02.toInternal()
										+ janbccd03.toInternal()
										+ janbccd04.toInternal()
										+ janbccd05.toInternal());
	}
	public void setJanbccds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getJanbccds().getLength()).init(obj);
	
		what = ExternalData.chop(what, janbccd01);
		what = ExternalData.chop(what, janbccd02);
		what = ExternalData.chop(what, janbccd03);
		what = ExternalData.chop(what, janbccd04);
		what = ExternalData.chop(what, janbccd05);
	}
	public PackedDecimalData getJanbccd(BaseData indx) {
		return getJanbccd(indx.toInt());
	}
	public PackedDecimalData getJanbccd(int indx) {

		switch (indx) {
			case 1 : return janbccd01;
			case 2 : return janbccd02;
			case 3 : return janbccd03;
			case 4 : return janbccd04;
			case 5 : return janbccd05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setJanbccd(BaseData indx, Object what) {
		setJanbccd(indx, what, false);
	}
	public void setJanbccd(BaseData indx, Object what, boolean rounded) {
		setJanbccd(indx.toInt(), what, rounded);
	}
	public void setJanbccd(int indx, Object what) {
		setJanbccd(indx, what, false);
	}
	public void setJanbccd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setJanbccd01(what, rounded);
					 break;
			case 2 : setJanbccd02(what, rounded);
					 break;
			case 3 : setJanbccd03(what, rounded);
					 break;
			case 4 : setJanbccd04(what, rounded);
					 break;
			case 5 : setJanbccd05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAnbisds() {
		return new FixedLengthStringData(anbisd01.toInternal()
										+ anbisd02.toInternal()
										+ anbisd03.toInternal()
										+ anbisd04.toInternal()
										+ anbisd05.toInternal());
	}
	public void setAnbisds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnbisds().getLength()).init(obj);
	
		what = ExternalData.chop(what, anbisd01);
		what = ExternalData.chop(what, anbisd02);
		what = ExternalData.chop(what, anbisd03);
		what = ExternalData.chop(what, anbisd04);
		what = ExternalData.chop(what, anbisd05);
	}
	public PackedDecimalData getAnbisd(BaseData indx) {
		return getAnbisd(indx.toInt());
	}
	public PackedDecimalData getAnbisd(int indx) {

		switch (indx) {
			case 1 : return anbisd01;
			case 2 : return anbisd02;
			case 3 : return anbisd03;
			case 4 : return anbisd04;
			case 5 : return anbisd05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnbisd(BaseData indx, Object what) {
		setAnbisd(indx, what, false);
	}
	public void setAnbisd(BaseData indx, Object what, boolean rounded) {
		setAnbisd(indx.toInt(), what, rounded);
	}
	public void setAnbisd(int indx, Object what) {
		setAnbisd(indx, what, false);
	}
	public void setAnbisd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnbisd01(what, rounded);
					 break;
			case 2 : setAnbisd02(what, rounded);
					 break;
			case 3 : setAnbisd03(what, rounded);
					 break;
			case 4 : setAnbisd04(what, rounded);
					 break;
			case 5 : setAnbisd05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAnbccds() {
		return new FixedLengthStringData(anbAtCcd01.toInternal()
										+ anbAtCcd02.toInternal()
										+ anbAtCcd03.toInternal()
										+ anbAtCcd04.toInternal()
										+ anbAtCcd05.toInternal());
	}
	public void setAnbccds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnbccds().getLength()).init(obj);
	
		what = ExternalData.chop(what, anbAtCcd01);
		what = ExternalData.chop(what, anbAtCcd02);
		what = ExternalData.chop(what, anbAtCcd03);
		what = ExternalData.chop(what, anbAtCcd04);
		what = ExternalData.chop(what, anbAtCcd05);
	}
	public PackedDecimalData getAnbccd(BaseData indx) {
		return getAnbccd(indx.toInt());
	}
	public PackedDecimalData getAnbccd(int indx) {

		switch (indx) {
			case 1 : return anbAtCcd01;
			case 2 : return anbAtCcd02;
			case 3 : return anbAtCcd03;
			case 4 : return anbAtCcd04;
			case 5 : return anbAtCcd05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnbccd(BaseData indx, Object what) {
		setAnbccd(indx, what, false);
	}
	public void setAnbccd(BaseData indx, Object what, boolean rounded) {
		setAnbccd(indx.toInt(), what, rounded);
	}
	public void setAnbccd(int indx, Object what) {
		setAnbccd(indx, what, false);
	}
	public void setAnbccd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnbAtCcd01(what, rounded);
					 break;
			case 2 : setAnbAtCcd02(what, rounded);
					 break;
			case 3 : setAnbAtCcd03(what, rounded);
					 break;
			case 4 : setAnbAtCcd04(what, rounded);
					 break;
			case 5 : setAnbAtCcd05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAlnames() {
		return new FixedLengthStringData(alname01.toInternal()
										+ alname02.toInternal()
										+ alname03.toInternal()
										+ alname04.toInternal()
										+ alname05.toInternal());
	}
	public void setAlnames(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAlnames().getLength()).init(obj);
	
		what = ExternalData.chop(what, alname01);
		what = ExternalData.chop(what, alname02);
		what = ExternalData.chop(what, alname03);
		what = ExternalData.chop(what, alname04);
		what = ExternalData.chop(what, alname05);
	}
	public FixedLengthStringData getAlname(BaseData indx) {
		return getAlname(indx.toInt());
	}
	public FixedLengthStringData getAlname(int indx) {

		switch (indx) {
			case 1 : return alname01;
			case 2 : return alname02;
			case 3 : return alname03;
			case 4 : return alname04;
			case 5 : return alname05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAlname(BaseData indx, Object what) {
		setAlname(indx.toInt(), what);
	}
	public void setAlname(int indx, Object what) {

		switch (indx) {
			case 1 : setAlname01(what);
					 break;
			case 2 : setAlname02(what);
					 break;
			case 3 : setAlname03(what);
					 break;
			case 4 : setAlname04(what);
					 break;
			case 5 : setAlname05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		effdate.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		currfrom.clear();
		validflag.clear();
		alname01.clear();
		alname02.clear();
		alname03.clear();
		alname04.clear();
		alname05.clear();
		jlname01.clear();
		jlname02.clear();
		jlname03.clear();
		jlname04.clear();
		jlname05.clear();
		smokeind01.clear();
		smokeind02.clear();
		smokeind03.clear();
		smokeind04.clear();
		smokeind05.clear();
		smkrqd01.clear();
		smkrqd02.clear();
		smkrqd03.clear();
		smkrqd04.clear();
		smkrqd05.clear();
		anbAtCcd01.clear();
		anbAtCcd02.clear();
		anbAtCcd03.clear();
		anbAtCcd04.clear();
		anbAtCcd05.clear();
		janbccd01.clear();
		janbccd02.clear();
		janbccd03.clear();
		janbccd04.clear();
		janbccd05.clear();
		anbisd01.clear();
		anbisd02.clear();
		anbisd03.clear();
		anbisd04.clear();
		anbisd05.clear();
		janbisd01.clear();
		janbisd02.clear();
		janbisd03.clear();
		janbisd04.clear();
		janbisd05.clear();
		sex01.clear();
		sex02.clear();
		sex03.clear();
		sex04.clear();
		sex05.clear();
		jlsex01.clear();
		jlsex02.clear();
		jlsex03.clear();
		jlsex04.clear();
		jlsex05.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}