package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Regppf {

    private String chdrcoy;
    private String chdrnum;
    private int planSuffix;
    private String life;
    private String coverage;
    private String rider;
    private int rgpynum;
    private String validflag;
    private int tranno;
    private String sacscode;
    private String sacstype;
    private String glact;
    private String debcred;
    private String destkey;
    private String payclt;
    private String rgpymop;
    private String regpayfreq;
    private String currcd;
    private BigDecimal pymt;
    private BigDecimal prcnt;
    private BigDecimal totamnt;
    private String rgpystat;
    private String payreason;
    private String claimevd;
    private String bankkey;
    private String bankacckey;
    private int crtdate;
    private int aprvdate;
    private int firstPaydate;
    private int nextPaydate;
    private int revdte;
    private int lastPaydate;
    private int finalPaydate;
    private int anvdate;
    private int cancelDate;
    private String rgpytype;
    private String crtable;
    private String termid;
    private int transactionDate;
    private int transactionTime;
    private int user;
    private String paycoy;
    private int certdate;
    private String clamparty;
    private int recvdDate;
    private int incurdt;
    private String userProfile;
    private String jobName;
    private String datime;
    private String wdrgpymop;
    private String wdbankkey;
    private String wdbankacckey;
    private BigDecimal adjamt;
    private BigDecimal netamt;
    private BigDecimal intrate;
    private BigDecimal intamt;
    private int intdays;
   
    private long uniqueNumber;
    
    public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public Regppf(){
		
	}
	public Regppf(Regppf regppf) {
		super();
		this.chdrcoy = regppf.chdrcoy;
		this.chdrnum = regppf.chdrnum;
		this.planSuffix = regppf.planSuffix;
		this.life = regppf.life;
		this.coverage = regppf.coverage;
		this.rider = regppf.rider;
		this.rgpynum = regppf.rgpynum;
		this.validflag = regppf.validflag;
		this.tranno = regppf.tranno;
		this.sacscode = regppf.sacscode;
		this.sacstype = regppf.sacstype;
		this.glact = regppf.glact;
		this.debcred = regppf.debcred;
		this.destkey = regppf.destkey;
		this.payclt = regppf.payclt;
		this.rgpymop = regppf.rgpymop;
		this.regpayfreq = regppf.regpayfreq;
		this.currcd = regppf.currcd;
		this.pymt = regppf.pymt;
		this.prcnt = regppf.prcnt;
		this.totamnt = regppf.totamnt;
		this.rgpystat = regppf.rgpystat;
		this.payreason = regppf.payreason;
		this.claimevd = regppf.claimevd;
		this.bankkey = regppf.bankkey;
		this.bankacckey = regppf.bankacckey;
		this.crtdate = regppf.crtdate;
		this.aprvdate = regppf.aprvdate;
		this.firstPaydate = regppf.firstPaydate;
		this.nextPaydate = regppf.nextPaydate;
		this.revdte = regppf.revdte;
		this.lastPaydate = regppf.lastPaydate;
		this.finalPaydate = regppf.finalPaydate;
		this.anvdate = regppf.anvdate;
		this.cancelDate = regppf.cancelDate;
		this.rgpytype = regppf.rgpytype;
		this.crtable = regppf.crtable;
		this.termid = regppf.termid;
		this.transactionDate = regppf.transactionDate;
		this.transactionTime = regppf.transactionTime;
		this.user = regppf.user;
		this.paycoy = regppf.paycoy;
		this.certdate = regppf.certdate;
		this.clamparty = regppf.clamparty;
		this.recvdDate = regppf.recvdDate;
		this.incurdt = regppf.incurdt;
		this.userProfile = regppf.userProfile;
		this.jobName = regppf.jobName;
		this.datime = regppf.datime;
		this.uniqueNumber = regppf.uniqueNumber;
		this.wdbankacckey = regppf.wdbankacckey;
	    this.wdbankkey = regppf.wdbankkey;
	    this.adjamt = regppf.adjamt;
	    this.netamt = regppf.netamt;
	    this.intrate = regppf.intrate;
	    this.intamt = regppf.intamt;
	    this.intdays = regppf.intdays;
	}

	public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public int getPlanSuffix() {
        return planSuffix;
    }

    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getRider() {
        return rider;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public int getRgpynum() {
        return rgpynum;
    }

    public void setRgpynum(int rgpynum) {
        this.rgpynum = rgpynum;
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public int getTranno() {
        return tranno;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public String getSacscode() {
        return sacscode;
    }

    public void setSacscode(String sacscode) {
        this.sacscode = sacscode;
    }

    public String getSacstype() {
        return sacstype;
    }

    public void setSacstype(String sacstype) {
        this.sacstype = sacstype;
    }

    public String getGlact() {
        return glact;
    }

    public void setGlact(String glact) {
        this.glact = glact;
    }

    public String getDebcred() {
        return debcred;
    }

    public void setDebcred(String debcred) {
        this.debcred = debcred;
    }

    public String getDestkey() {
        return destkey;
    }

    public void setDestkey(String destkey) {
        this.destkey = destkey;
    }

    public String getPayclt() {
        return payclt;
    }

    public void setPayclt(String payclt) {
        this.payclt = payclt;
    }

    public String getRgpymop() {
        return rgpymop;
    }

    public void setRgpymop(String rgpymop) {
        this.rgpymop = rgpymop;
    }

    public String getRegpayfreq() {
        return regpayfreq;
    }

    public void setRegpayfreq(String regpayfreq) {
        this.regpayfreq = regpayfreq;
    }

    public String getCurrcd() {
        return currcd;
    }

    public void setCurrcd(String currcd) {
        this.currcd = currcd;
    }

    public BigDecimal getPymt() {
        return pymt;
    }

    public void setPymt(BigDecimal pymt) {
        this.pymt = pymt;
    }

    public BigDecimal getPrcnt() {
        return prcnt;
    }

    public void setPrcnt(BigDecimal prcnt) {
        this.prcnt = prcnt;
    }

    public BigDecimal getTotamnt() {
        return totamnt;
    }

    public void setTotamnt(BigDecimal totamnt) {
        this.totamnt = totamnt;
    }

    public String getRgpystat() {
        return rgpystat;
    }

    public void setRgpystat(String rgpystat) {
        this.rgpystat = rgpystat;
    }

    public String getPayreason() {
        return payreason;
    }

    public void setPayreason(String payreason) {
        this.payreason = payreason;
    }

    public String getClaimevd() {
        return claimevd;
    }

    public void setClaimevd(String claimevd) {
        this.claimevd = claimevd;
    }

    public String getBankkey() {
        return bankkey;
    }

    public void setBankkey(String bankkey) {
        this.bankkey = bankkey;
    }

    public String getBankacckey() {
        return bankacckey;
    }

    public void setBankacckey(String bankacckey) {
        this.bankacckey = bankacckey;
    }

    public int getCrtdate() {
        return crtdate;
    }

    public void setCrtdate(int crtdate) {
        this.crtdate = crtdate;
    }

    public int getAprvdate() {
        return aprvdate;
    }

    public void setAprvdate(int aprvdate) {
        this.aprvdate = aprvdate;
    }

    public int getFirstPaydate() {
        return firstPaydate;
    }

    public void setFirstPaydate(int firstPaydate) {
        this.firstPaydate = firstPaydate;
    }

    public int getNextPaydate() {
        return nextPaydate;
    }

    public void setNextPaydate(int nextPaydate) {
        this.nextPaydate = nextPaydate;
    }

    public int getRevdte() {
        return revdte;
    }

    public void setRevdte(int revdte) {
        this.revdte = revdte;
    }

    public int getLastPaydate() {
        return lastPaydate;
    }

    public void setLastPaydate(int lastPaydate) {
        this.lastPaydate = lastPaydate;
    }

    public int getFinalPaydate() {
        return finalPaydate;
    }

    public void setFinalPaydate(int finalPaydate) {
        this.finalPaydate = finalPaydate;
    }

    public int getAnvdate() {
        return anvdate;
    }

    public void setAnvdate(int anvdate) {
        this.anvdate = anvdate;
    }

    public int getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(int cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getRgpytype() {
        return rgpytype;
    }

    public void setRgpytype(String rgpytype) {
        this.rgpytype = rgpytype;
    }

    public String getCrtable() {
        return crtable;
    }

    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }

    public String getTermid() {
        return termid;
    }

    public void setTermid(String termid) {
        this.termid = termid;
    }

    public int getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getPaycoy() {
        return paycoy;
    }

    public void setPaycoy(String paycoy) {
        this.paycoy = paycoy;
    }

    public int getCertdate() {
        return certdate;
    }

    public void setCertdate(int certdate) {
        this.certdate = certdate;
    }

    public String getClamparty() {
        return clamparty;
    }

    public void setClamparty(String clamparty) {
        this.clamparty = clamparty;
    }

    public int getRecvdDate() {
        return recvdDate;
    }

    public void setRecvdDate(int recvdDate) {
        this.recvdDate = recvdDate;
    }

    public int getIncurdt() {
        return incurdt;
    }

    public void setIncurdt(int incurdt) {
        this.incurdt = incurdt;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }

	public String getWdrgpymop() {
		return wdrgpymop;
	}

	public void setWdrgpymop(String wdrgpymop) {
		this.wdrgpymop = wdrgpymop;
	}

	public String getWdbankkey() {
		return wdbankkey;
	}

	public void setWdbankkey(String wdbankkey) {
		this.wdbankkey = wdbankkey;
	}

	public String getWdbankacckey() {
		return wdbankacckey;
	}

	public void setWdbankacckey(String wdbankacckey) {
		this.wdbankacckey = wdbankacckey;
	}

	public BigDecimal getAdjamt() {
		return adjamt;
	}

	public void setAdjamt(BigDecimal adjamt) {
		this.adjamt = adjamt;
	}

	public BigDecimal getNetamt() {
		return netamt;
	}

	public void setNetamt(BigDecimal netamt) {
		this.netamt = netamt;
	}
	 public BigDecimal getIntrate() {
			return intrate;
		}

		public void setIntrate(BigDecimal intrate) {
			this.intrate = intrate;
		}

		public BigDecimal getIntamt() {
			return intamt;
		}

		public void setIntamt(BigDecimal intamt) {
			this.intamt = intamt;
		}

		public int getIntdays() {
			return intdays;
		}

		public void setIntdays(int intdays) {
			this.intdays = intdays;
		}
}