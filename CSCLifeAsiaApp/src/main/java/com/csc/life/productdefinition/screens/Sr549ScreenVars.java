package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR549
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr549ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(176);
	public FixedLengthStringData dataFields = new FixedLengthStringData(48).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData mdblacts = new FixedLengthStringData(4).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] mdblact = FLSArrayPartOfStructure(4, 1, mdblacts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(mdblacts, 0, FILLER_REDEFINE);
	public FixedLengthStringData mdblact01 = DD.mdblact.copy().isAPartOf(filler,0);
	public FixedLengthStringData mdblact02 = DD.mdblact.copy().isAPartOf(filler,1);
	public FixedLengthStringData mdblact03 = DD.mdblact.copy().isAPartOf(filler,2);
	public FixedLengthStringData mdblact04 = DD.mdblact.copy().isAPartOf(filler,3);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 48);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData mdblactsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] mdblactErr = FLSArrayPartOfStructure(4, 4, mdblactsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(mdblactsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mdblact01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData mdblact02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData mdblact03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData mdblact04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 80);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData mdblactsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] mdblactOut = FLSArrayPartOfStructure(4, 12, mdblactsOut, 0);
	public FixedLengthStringData[][] mdblactO = FLSDArrayPartOfArrayStructure(12, 1, mdblactOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(mdblactsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mdblact01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] mdblact02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] mdblact03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] mdblact04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr549screenWritten = new LongData(0);
	public LongData Sr549protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr549ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, mdblact01, mdblact02, mdblact03, mdblact04};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, mdblact01Out, mdblact02Out, mdblact03Out, mdblact04Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, mdblact01Err, mdblact02Err, mdblact03Err, mdblact04Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr549screen.class;
		protectRecord = Sr549protect.class;
	}

}
