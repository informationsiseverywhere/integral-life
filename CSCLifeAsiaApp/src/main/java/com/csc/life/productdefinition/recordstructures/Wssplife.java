package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:48
 * Description:
 * Copybook name: WSSPLIFE
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Wssplife extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData userArea = new FixedLengthStringData(782);
  	public FixedLengthStringData covrkey = new FixedLengthStringData(19).isAPartOf(userArea, 0);
  	public FixedLengthStringData lifekey = new FixedLengthStringData(15).isAPartOf(covrkey, 0);
  	public FixedLengthStringData chdrky = new FixedLengthStringData(11).isAPartOf(lifekey, 0);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(lifekey, 11);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(lifekey, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(covrkey, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(covrkey, 17);
  	public FixedLengthStringData mop = new FixedLengthStringData(1).isAPartOf(userArea, 19);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(userArea, 20);
  	public ZonedDecimalData fupno = new ZonedDecimalData(2, 0).isAPartOf(userArea, 22).setUnsigned();
  	public FixedLengthStringData grupkey = new FixedLengthStringData(12).isAPartOf(userArea, 24);
  	public FixedLengthStringData fuptype = new FixedLengthStringData(1).isAPartOf(userArea, 36);
  	public FixedLengthStringData keysoptItem = new FixedLengthStringData(8).isAPartOf(userArea, 37);
  	public ZonedDecimalData occdate = new ZonedDecimalData(8, 0).isAPartOf(userArea, 45).setUnsigned();
  	public FixedLengthStringData crTable = new FixedLengthStringData(5).isAPartOf(userArea, 53);
  	public ZonedDecimalData rcd = new ZonedDecimalData(8, 0).isAPartOf(userArea, 58).setUnsigned();
  	public FixedLengthStringData campaign = new FixedLengthStringData(6).isAPartOf(userArea, 66);
  	public FixedLengthStringData sagntkey = new FixedLengthStringData(11).isAPartOf(userArea, 72);
  	public FixedLengthStringData sagntAgntpfx = new FixedLengthStringData(2).isAPartOf(sagntkey, 0);
  	public FixedLengthStringData sagntAgntcoy = new FixedLengthStringData(1).isAPartOf(sagntkey, 2);
  	public FixedLengthStringData sagntAgntnum = new FixedLengthStringData(8).isAPartOf(sagntkey, 3);
  	public FixedLengthStringData bnyno = new FixedLengthStringData(2).isAPartOf(userArea, 83);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(userArea, 85).setUnsigned();
  	public FixedLengthStringData mopfrqOrigVals = new FixedLengthStringData(66).isAPartOf(userArea, 93);
  	public FixedLengthStringData mfOwner = new FixedLengthStringData(8).isAPartOf(mopfrqOrigVals, 0);
  	public FixedLengthStringData mfPayor = new FixedLengthStringData(8).isAPartOf(mopfrqOrigVals, 8);
  	public FixedLengthStringData mfMemb = new FixedLengthStringData(12).isAPartOf(mopfrqOrigVals, 16);
  	public FixedLengthStringData mfFreq = new FixedLengthStringData(2).isAPartOf(mopfrqOrigVals, 28);
  	public FixedLengthStringData mfMop = new FixedLengthStringData(1).isAPartOf(mopfrqOrigVals, 30);
  	public FixedLengthStringData mfFhse = new FixedLengthStringData(2).isAPartOf(mopfrqOrigVals, 31);
  	public FixedLengthStringData mfBnk = new FixedLengthStringData(10).isAPartOf(mopfrqOrigVals, 33);
  	public FixedLengthStringData mfBnkacc = new FixedLengthStringData(20).isAPartOf(mopfrqOrigVals, 43);
  	public FixedLengthStringData mfCurr = new FixedLengthStringData(3).isAPartOf(mopfrqOrigVals, 63);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(userArea, 159);
  	public FixedLengthStringData ritrcde = new FixedLengthStringData(4).isAPartOf(userArea, 189);
  	public FixedLengthStringData claim = new FixedLengthStringData(8).isAPartOf(userArea, 193);
  	public FixedLengthStringData wvswfee = new FixedLengthStringData(1).isAPartOf(userArea, 201);
  	public FixedLengthStringData amtperc = new FixedLengthStringData(1).isAPartOf(userArea, 202);
  	public ZonedDecimalData udiscoff = new ZonedDecimalData(5, 2).isAPartOf(userArea, 203).setUnsigned();
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(userArea, 208);
  	public FixedLengthStringData srcebus = new FixedLengthStringData(2).isAPartOf(userArea, 211);
  	public ZonedDecimalData clmdate = new ZonedDecimalData(8, 0).isAPartOf(userArea, 213).setUnsigned();
  	public FixedLengthStringData headerFlag = new FixedLengthStringData(1).isAPartOf(userArea, 221);
  	public FixedLengthStringData nowDeferInd = new FixedLengthStringData(1).isAPartOf(userArea, 222);
  	public FixedLengthStringData usmeth = new FixedLengthStringData(4).isAPartOf(userArea, 223);
  	public FixedLengthStringData allowFunds = new FixedLengthStringData(48).isAPartOf(userArea, 227);
  	public FixedLengthStringData updateFlag = new FixedLengthStringData(1).isAPartOf(userArea, 275);
  	public ZonedDecimalData bigAmt = new ZonedDecimalData(17, 2).isAPartOf(userArea, 276);
  	public FixedLengthStringData fndlist = new FixedLengthStringData(4).isAPartOf(userArea, 293);
  	public ZonedDecimalData notdate = new ZonedDecimalData(8, 0).isAPartOf(userArea, 297);
  	public FixedLengthStringData unitType = new FixedLengthStringData(2).isAPartOf(userArea, 305);
  	public FixedLengthStringData planPolicy = new FixedLengthStringData(1).isAPartOf(userArea, 307);
  	public FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(userArea, 308);
  	public FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(userArea, 312);
  	public FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(userArea, 320);
  	public FixedLengthStringData rrevfrq = new FixedLengthStringData(2).isAPartOf(userArea, 324);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(userArea, 326);
  	public PackedDecimalData subfileRrn = new PackedDecimalData(5, 0).isAPartOf(userArea, 330);
  	public PackedDecimalData subfileEnd = new PackedDecimalData(5, 0).isAPartOf(userArea, 335).setUnsigned();
  	//IJTI-400 new variables for workflow starts
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(userArea, 339);
  	public FixedLengthStringData isUWRequired = new FixedLengthStringData(1).isAPartOf(userArea, 347);
  	public FixedLengthStringData isUWConfirmed = new FixedLengthStringData(1).isAPartOf(userArea, 348);
    //IJTI-400 new variables for workflow ends
  	public FixedLengthStringData filler = new FixedLengthStringData(433).isAPartOf(userArea, 349, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(userArea);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		userArea.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}