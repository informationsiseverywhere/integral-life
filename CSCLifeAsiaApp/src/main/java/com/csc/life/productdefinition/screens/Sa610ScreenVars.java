package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sa610
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class Sa610ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(649);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(141).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	
	public FixedLengthStringData occcodes = new FixedLengthStringData(20).isAPartOf(dataFields, 1);
	
	public FixedLengthStringData[] occcode = FLSArrayPartOfStructure(10, 2, occcodes, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(occcodes, 0, FILLER_REDEFINE);
	
	public FixedLengthStringData occcode01 = DD.occclass.copy().isAPartOf(filler4,0);
	public FixedLengthStringData occcode02 = DD.occclass.copy().isAPartOf(filler4,2);
	public FixedLengthStringData occcode03 = DD.occclass.copy().isAPartOf(filler4,4);
	public FixedLengthStringData occcode04 = DD.occclass.copy().isAPartOf(filler4,6);
	public FixedLengthStringData occcode05 = DD.occclass.copy().isAPartOf(filler4,8);
	public FixedLengthStringData occcode06 = DD.occclass.copy().isAPartOf(filler4,10);
	public FixedLengthStringData occcode07 = DD.occclass.copy().isAPartOf(filler4,12);
	public FixedLengthStringData occcode08 = DD.occclass.copy().isAPartOf(filler4,14);
	public FixedLengthStringData occcode09 = DD.occclass.copy().isAPartOf(filler4,16);
	public FixedLengthStringData occcode10 = DD.occclass.copy().isAPartOf(filler4,18);
	
	public FixedLengthStringData occclassdess = new FixedLengthStringData(40).isAPartOf(dataFields, 21);
	
	public FixedLengthStringData[] occclassdes = FLSArrayPartOfStructure(10, 4, occclassdess, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(occclassdess, 0, FILLER_REDEFINE);
	
	public FixedLengthStringData occclassdes01 = DD.occpcode.copy().isAPartOf(filler3,0);
	public FixedLengthStringData occclassdes02 = DD.occpcode.copy().isAPartOf(filler3,4);
	public FixedLengthStringData occclassdes03 = DD.occpcode.copy().isAPartOf(filler3,8);
	public FixedLengthStringData occclassdes04 = DD.occpcode.copy().isAPartOf(filler3,12);
	public FixedLengthStringData occclassdes05 = DD.occpcode.copy().isAPartOf(filler3,16);
	public FixedLengthStringData occclassdes06 = DD.occpcode.copy().isAPartOf(filler3,20);
	public FixedLengthStringData occclassdes07 = DD.occpcode.copy().isAPartOf(filler3,24);
	public FixedLengthStringData occclassdes08 = DD.occpcode.copy().isAPartOf(filler3,28);
	public FixedLengthStringData occclassdes09 = DD.occpcode.copy().isAPartOf(filler3,32);
	public FixedLengthStringData occclassdes10 = DD.occpcode.copy().isAPartOf(filler3,36);
	
	public FixedLengthStringData fupcdess = new FixedLengthStringData(20).isAPartOf(dataFields, 61);

	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(5, 4, fupcdess, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
	
	public FixedLengthStringData fupcdes01 = DD.fupcdes.copy().isAPartOf(filler,0);
	public FixedLengthStringData fupcdes02 = DD.fupcdes.copy().isAPartOf(filler,4);
	public FixedLengthStringData fupcdes03 = DD.fupcdes.copy().isAPartOf(filler,8);
	public FixedLengthStringData fupcdes04 = DD.fupcdes.copy().isAPartOf(filler,12);
	public FixedLengthStringData fupcdes05 = DD.fupcdes.copy().isAPartOf(filler,16);

	
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,81);//8
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,89);//8
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,97);//8

	
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,105);//1
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,106);//30
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,136);//5
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 141);
	
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	
	public FixedLengthStringData occcodesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 4);	
	public FixedLengthStringData[] occcodeErr = FLSArrayPartOfStructure(10, 4, occcodesErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(occcodesErr, 0, FILLER_REDEFINE);
	
	public FixedLengthStringData occcode01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData occcode02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData occcode03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData occcode04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData occcode05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData occcode06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData occcode07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData occcode08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData occcode09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData occcode10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	
	public FixedLengthStringData occclassdessErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] occclassdesErr = FLSArrayPartOfStructure(10, 4, occclassdessErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(occclassdessErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData occclassdes01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData occclassdes02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData occclassdes03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData occclassdes04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData occclassdes05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData occclassdes06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData occclassdes07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData occclassdes08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData occclassdes09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData occclassdes10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	
	public FixedLengthStringData fupcdessErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 80);

	public FixedLengthStringData[] fupcdesErr = FLSArrayPartOfStructure(5, 4, fupcdessErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(20).isAPartOf(fupcdessErr, 0, FILLER_REDEFINE);

	
	public FixedLengthStringData fupcdes01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData fupcdes02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData fupcdes03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData fupcdes04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData fupcdes05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);

	
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);

	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(384).isAPartOf(dataArea, 265);
	
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	
	public FixedLengthStringData occcodesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] occcodeOut = FLSArrayPartOfStructure(10, 12, occcodesOut, 0);
	public FixedLengthStringData[][] occcodeO = FLSDArrayPartOfArrayStructure(12, 1, occcodeOut, 0);

	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(occcodesOut, 0, FILLER_REDEFINE);

	public FixedLengthStringData[] occcode01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] occcode02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] occcode03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] occcode04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] occcode05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] occcode06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] occcode07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] occcode08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] occcode09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] occcode10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);

	public FixedLengthStringData occclassdessOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] occclassdesOut = FLSArrayPartOfStructure(10, 12, occclassdessOut, 0);
	public FixedLengthStringData[][] occclassdesO = FLSDArrayPartOfArrayStructure(12, 1, occclassdesOut, 0);

	public FixedLengthStringData filler2 = new FixedLengthStringData(120).isAPartOf(occclassdessOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] occclassdes01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] occclassdes02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] occclassdes03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] occclassdes04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] occclassdes05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] occclassdes06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] occclassdes07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] occclassdes08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] occclassdes09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] occclassdes10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	
	
	public FixedLengthStringData fupcdessOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 240);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(5, 12, fupcdessOut, 0);
	public FixedLengthStringData[][] fupcdesO = FLSDArrayPartOfArrayStructure(12, 1, fupcdesOut, 0);

	public FixedLengthStringData filler8 = new FixedLengthStringData(60).isAPartOf(fupcdessOut, 0, FILLER_REDEFINE);
	
	public FixedLengthStringData[] fupcdes01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] fupcdes02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] fupcdes03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] fupcdes04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] fupcdes05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);


	
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);

	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sa610screenWritten = new LongData(0);
	public LongData Sa610protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sa610ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() 
	{
		fieldIndMap.put(fupcdes01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(occcode01Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode03Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode04Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode05Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode06Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode07Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode08Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode09Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occcode10Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(occclassdes01Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes02Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes03Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes04Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes05Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes06Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes07Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes08Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes09Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occclassdes10Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});

		screenFields = new BaseData[] {language, company, tabl, item, itmfrm, itmto, longdesc, occclassdes01, occclassdes02, occclassdes03, occclassdes04, occclassdes05, occclassdes06, occclassdes07, occclassdes08, occclassdes09, occclassdes10, occcode01, occcode02, occcode03, occcode04, occcode05,occcode06, occcode07, occcode08, occcode09, occcode10,fupcdes01,fupcdes02,fupcdes03,fupcdes04,fupcdes05};
		screenOutFields = new BaseData[][] {languageOut, companyOut, tablOut, itemOut, itmfrmOut, itmtoOut ,longdescOut, occclassdes01Out, occclassdes02Out, occclassdes03Out, occclassdes04Out, occclassdes05Out, occclassdes06Out, occclassdes07Out, occclassdes08Out, occclassdes09Out, occclassdes10Out,occcode01Out, occcode02Out, occcode03Out, occcode04Out, occcode05Out,occcode06Out, occcode07Out, occcode08Out, occcode09Out, occcode10Out, fupcdes01Out, fupcdes02Out, fupcdes03Out, fupcdes04Out, fupcdes05Out};
		screenErrFields = new BaseData[] {languageErr, companyErr, tablErr, itemErr,itmfrmErr, itmtoErr ,longdescErr, occclassdes01Err, occclassdes02Err, occclassdes03Err, occclassdes04Err, occclassdes05Err, occclassdes06Err, occclassdes07Err, occclassdes08Err, occclassdes09Err, occclassdes10Err,occcode01Err, occcode02Err, occcode03Err, occcode04Err, occcode05Err,occcode06Err, occcode07Err, occcode08Err, occcode09Err, occcode10Err,fupcdes01Err, fupcdes02Err, fupcdes03Err, fupcdes04Err, fupcdes05Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa610screen.class;
		protectRecord = Sa610protect.class;
	}

}
