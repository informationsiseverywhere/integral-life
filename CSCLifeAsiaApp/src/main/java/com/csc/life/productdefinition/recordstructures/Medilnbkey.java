package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:43
 * Description:
 * Copybook name: MEDILNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Medilnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData medilnbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData medilnbKey = new FixedLengthStringData(256).isAPartOf(medilnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData medilnbChdrcoy = new FixedLengthStringData(1).isAPartOf(medilnbKey, 0);
  	public FixedLengthStringData medilnbChdrnum = new FixedLengthStringData(8).isAPartOf(medilnbKey, 1);
  	public FixedLengthStringData medilnbPaidby = new FixedLengthStringData(1).isAPartOf(medilnbKey, 9);
  	public PackedDecimalData medilnbSeqno = new PackedDecimalData(2, 0).isAPartOf(medilnbKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(medilnbKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(medilnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		medilnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}