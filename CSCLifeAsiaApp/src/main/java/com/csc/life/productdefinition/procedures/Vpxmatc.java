package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxmatcrec;
import com.csc.life.terminationclaims.dataaccess.UtrsmatTableDAM;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Vpxmatc extends COBOLConvCodeModel {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	private PackedDecimalData ia = new PackedDecimalData(5, 0);
	private PackedDecimalData ib = new PackedDecimalData(5, 0);
		private FixedLengthStringData wsaaStart = new FixedLengthStringData(1);
	
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPXMATC";
		/* FORMATS */
	private String t5515 = "T5515";
	private String t5687 = "T5687";
	private String utrsmatrec = "UTRSMATREC";
	private String utrsclmrec = "UTRSCLMREC";	
	private String hitsrec = "HITSREC";
		/* ERRORS */
	private String e049 = "E049";
	private String h832 = "H832";
		/* COPYBOOK */
	private Matccpy matccpy = new Matccpy();
	private Vpxmatcrec vpxmatcrec = new Vpxmatcrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Interest Bearing Transaction Summary*/
	private HitsTableDAM hitsIO = new HitsTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	/*Unit Transactions for Claims*/
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
		/*United linked details - Maturity*/
	private UtrsmatTableDAM utrsmatIO = new UtrsmatTableDAM();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5515rec t5515rec = new T5515rec();
	
	private FixedLengthStringData wsaaNumFmt = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaNumArrs = new FixedLengthStringData(20).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private FixedLengthStringData[] wsaaNumArr = FLSArrayPartOfStructure(20, 1, wsaaNumArrs, 0);
	private ZonedDecimalData wsaaNumD0 = new ZonedDecimalData(18, 0).isAPartOf(wsaaNumFmt, 0, REDEFINE).setUnsigned();
	private ZonedDecimalData wsaaNumD1 = new ZonedDecimalData(18, 1).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD2 = new ZonedDecimalData(18, 2).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD3 = new ZonedDecimalData(18, 3).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD4 = new ZonedDecimalData(18, 4).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD5 = new ZonedDecimalData(18, 5).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD6 = new ZonedDecimalData(18, 6).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD7 = new ZonedDecimalData(18, 7).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD8 = new ZonedDecimalData(18, 8).isAPartOf(wsaaNumFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaNumD9 = new ZonedDecimalData(18, 9).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	
  	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4,0).init(0); 
  	private PackedDecimalData wsaaInitialAmount = new PackedDecimalData(18, 5).init(0);
  	private PackedDecimalData wsaaAccumAmount = new PackedDecimalData(18, 5).init(0);
  	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
  	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
  	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
  	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
  	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
  	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
  	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);

  	private FixedLengthStringData ws88Utype =  new FixedLengthStringData(1);
  	private Validator initialUnits = new Validator(ws88Utype, "I");
  	private Validator accumUnits = new Validator(ws88Utype, "A");
  	
  	private FixedLengthStringData ws88FirstTime =  new FixedLengthStringData(1).init("Y");
  	private Validator firstTime = new Validator(ws88FirstTime, "Y");
  	private Validator notFirstTime = new Validator(ws88FirstTime, "Y");
  	
  	private FixedLengthStringData ws88FirstHits =  new FixedLengthStringData(1).init("Y");
  	private Validator firstHits = new Validator(ws88FirstHits, "Y");
  	private Validator notFirstHits = new Validator(ws88FirstHits, "Y");
  	
  	private ZonedDecimalData ws88PlanSwitch =  new ZonedDecimalData(1,0).init(0).setUnsigned();
  	private Validator wholePlan = new Validator(ws88PlanSwitch, "1");
  	private Validator partPlan = new Validator(ws88PlanSwitch, "2");
  	private Validator summaryPart = new Validator(ws88PlanSwitch, "3");
	
  	//private FixedLengthStringData wsccConstants = new FixedLengthStringData(6);
  	private FixedLengthStringData wsccA = new FixedLengthStringData(1).init("A");
  	private FixedLengthStringData wsccD = new FixedLengthStringData(1).init("D");
  	private FixedLengthStringData wsccE = new FixedLengthStringData(1).init("E");
  	private FixedLengthStringData wsccI = new FixedLengthStringData(1).init("I");
  	private FixedLengthStringData wsccN = new FixedLengthStringData(1).init("N");
  	private FixedLengthStringData wsccY = new FixedLengthStringData(1).init("Y"); 	
	
	public Vpxmatc(){
		super();
	}
	
	public void mainline(Object... parmArray){		
		vpxmatcrec = (Vpxmatcrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		matccpy.maturityRec.set(vpmcalcrec.linkageArea);
		try{
			para();			
			vpmcalcrec.linkageArea.set(matccpy.maturityRec);
		}catch (COBOLExitProgramException e) {
		}catch (Exception e) {
 			systemError();
 		}
	}
	
	protected void para(){
		syserrrec.subrname.set(wsaaSubr);
		vpxmatcrec.statuz.set(varcom.oK);
		if (isEQ(vpxmatcrec.function,"INIT")){
			initialize();
			extract();
		}
		else if (isEQ(vpxmatcrec.function,"GETV")) {
			returnValue();
		}
		else {
			syserrrec.statuz.set(e049);
			vpxmatcrec.statuz.set(e049);
			systemError();
		}		
	}
	
	protected void initialize(){
		vpxmatcrec.curuntbal.set(ZERO);				
	}
	
	protected void extract(){
		syserrrec.subrname.set(wsaaSubr);
		if(isEQ(matccpy.status,varcom.endp)){
			firstTime.setTrue();
			return;
		}

		if(firstTime.isTrue()){
			initialization();
		}
		if(isEQ(matccpy.endf, wsccI)){
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			matccpy.estimatedVal.set(ZERO);
			matccpy.actualVal.set(ZERO);
			if(firstHits.isTrue()){
				callBegnHits();
			}
		}

		if(isEQ(matccpy.status, varcom.oK)){
			if(isEQ(matccpy.endf, wsccI))
				interestBearing();
			else{
				if(wholePlan.isTrue())
					processWholePlan();
				else
					processComponent();
				
			}		
		}
	}	
	
	protected void initialization(){
		//INIT
		wsaaChdrcoy.set(matccpy.chdrChdrcoy);
		wsaaChdrnum.set(matccpy.chdrChdrnum);
		wsaaCoverage.set(matccpy.covrCoverage);
		wsaaRider.set(matccpy.covrRider);
		wsaaLife.set(matccpy.lifeLife);
		wsaaPlanSuffix.set(matccpy.planSuffix);
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(SPACES);
		wsaaUnitType.set(SPACES);
		matccpy.description.set(SPACES);
		matccpy.status.set(varcom.oK);
		//notFirstTime.setTrue();
		ws88FirstTime.set("N");
		
		//Initialize plan suffix to zeroes if part of a summary
		ws88PlanSwitch.set(matccpy.planSwitch);
		if (isEQ(matccpy.planSwitch,"1"))
			wholePlan.setTrue();
		else if (isEQ(matccpy.planSwitch,"2"))
			partPlan.setTrue();
		else if (isEQ(matccpy.planSwitch,"3"))
			summaryPart.setTrue();

		if(summaryPart.isTrue()){
			wsaaPlanSuffix.set(ZERO );
			matccpy.planSuffix.set(ZERO);
		}
		
		//PROCESS-CHOICES
		if(wholePlan.isTrue())
			callBegnUtrsmat();
		else
			callBegnUtrsclm();
		
		if(isEQ(matccpy.status,varcom.endp))
			getCrtableDesc();
	}
		
	protected void returnValue(){
		vpxmatcrec.resultValue.set(SPACES);
		if (isEQ(vpxmatcrec.resultField,"curuntbal")){
			wsaaNumD5.set(vpxmatcrec.curuntbal);			
			vpxmatcrec.resultValue.set(vpxmatcrec.curuntbal.getbigdata().toString());
			//formatNumber();
		}else{
			vpxmatcrec.statuz.set(h832);
		}
	}
	
	protected void callBegnUtrsmat(){

		utrsmatIO.setParams(SPACES);
		utrsmatIO.setChdrcoy(wsaaChdrcoy);
		utrsmatIO.setChdrnum(wsaaChdrnum);
		utrsmatIO.setLife(wsaaLife);
		utrsmatIO.setCoverage(wsaaCoverage);
		utrsmatIO.setRider(wsaaRider);
		utrsmatIO.setPlanSuffix(ZERO);
		utrsmatIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsmatIO.setUnitType(wsaaUnitType);
		utrsmatIO.setFormat(utrsmatrec);
		utrsmatIO.setFunction(varcom.begn);
		utrsmatIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsmatIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, utrsmatIO);
		if(!isEQ(utrsmatIO.statuz,varcom.oK)&&!isEQ(utrsmatIO.statuz,varcom.endp)){
			syserrrec.params.set(utrsmatIO.getParams());
			dbError();
		}

		if(!isEQ(utrsmatIO.chdrcoy,wsaaChdrcoy)||!isEQ(utrsmatIO.chdrnum,wsaaChdrnum)||
				!isEQ(utrsmatIO.life,wsaaLife)||!isEQ(utrsmatIO.coverage,wsaaCoverage)||
				!isEQ(utrsmatIO.rider,wsaaRider)||isEQ(utrsmatIO.statuz,varcom.endp)){
			utrsmatIO.setStatuz(varcom.endp);
		}
	}
	
	protected void dbError()
	{
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);		
	}
	
	protected void processWholePlan(){

		if(isEQ(utrsmatIO.statuz,varcom.endp)){
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
			matccpy.endf.set(wsccI);
			return;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(utrsmatIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsmatIO.getUnitType());
		
		while ( !(isEQ(utrsmatIO.getStatuz(),varcom.endp) || isNE(wsaaVirtualFund, utrsmatIO.getUnitVirtualFund())
				|| isNE(wsaaUnitType,utrsmatIO.getUnitType()))) {
			accumAmtWholePlan();					
		}

		if(initialUnits.isTrue()){
			vpxmatcrec.curuntbal.set(wsaaInitialAmount);
			matccpy.type.set(wsccI);
		}
		else{
			vpxmatcrec.curuntbal.set(wsaaAccumAmount);
			matccpy.type.set(wsccA);
		}
		
		matccpy.fund.set(wsaaVirtualFund);
		readT5515();
		if(isEQ(matccpy.endf,wsccI)){
			wsaaVirtualFund.set(SPACE);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
		}
	}
	
	protected void accumAmtWholePlan(){
		if(isEQ(utrsmatIO.unitType,wsccI)){
			wsaaInitialAmount.add(utrsmatIO.currentUnitBal);
			initialUnits.setTrue();
		}
		else {
			wsaaAccumAmount.add(utrsmatIO.currentUnitBal);
			accumUnits.setTrue();
		}
		
		utrsmatIO.setFormat(utrsmatrec);
		utrsmatIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsmatIO);

		if(!isEQ(utrsmatIO.statuz,varcom.oK)&&!isEQ(utrsmatIO.statuz,varcom.endp)){
			syserrrec.params.set(utrsmatIO.getParams());
			dbError();
		}

		if(!isEQ(utrsmatIO.getChdrcoy(), matccpy.chdrChdrcoy)||!isEQ(utrsmatIO.getChdrnum(), matccpy.chdrChdrnum)||
				!isEQ(utrsmatIO.getLife(), matccpy.lifeLife)||!isEQ(utrsmatIO.getCoverage(), matccpy.covrCoverage)||
				!isEQ(utrsmatIO.getRider(), matccpy.covrRider)||isEQ(utrsmatIO.getStatuz(), varcom.endp)){
			matccpy.endf.set(wsccI);
			utrsmatIO.setStatuz(varcom.endp);
		}
	}
	
	protected void callBegnUtrsclm(){
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setChdrcoy(wsaaChdrcoy);
		utrsclmIO.setChdrnum(wsaaChdrnum);
		utrsclmIO.setLife(wsaaLife);
		utrsclmIO.setCoverage(wsaaCoverage);
		utrsclmIO.setRider(wsaaRider);
		utrsclmIO.setPlanSuffix(wsaaPlanSuffix);
		utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsclmIO.setUnitType(wsaaUnitType);
		utrsclmIO.setFormat(utrsclmrec);
		utrsclmIO.setFunction(varcom.begn);
		utrsclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, utrsclmIO);

		if(!isEQ(utrsclmIO.getStatuz(),varcom.oK)&&!isEQ(utrsclmIO.getStatuz(),varcom.endp)){
			syserrrec.params.set(utrsclmIO.getParams());
			dbError();
		}
		if(!isEQ(utrsclmIO.getChdrcoy(), wsaaChdrcoy)||!isEQ(utrsclmIO.getChdrnum(), wsaaChdrnum)||!isEQ(utrsclmIO.getLife(),wsaaLife)||
				!isEQ(utrsclmIO.getCoverage(), wsaaCoverage)||!isEQ(utrsclmIO.getRider(), wsaaRider)||
				!isEQ(utrsclmIO.getPlanSuffix(), wsaaPlanSuffix)||isEQ(utrsclmIO.getStatuz(), varcom.endp)){
			utrsclmIO.setStatuz(varcom.endp);
		}
	}
	
	protected void processComponent(){
		if(isEQ(utrsclmIO.getStatuz(),varcom.endp)){
			matccpy.endf.set(wsccI);
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);		
			return;
		}
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsclmIO.getUnitType());
		
		while ( !(isEQ(utrsclmIO.getStatuz(),varcom.endp) || isNE(wsaaVirtualFund,utrsclmIO.getUnitVirtualFund())
				|| isNE(wsaaUnitType,utrsclmIO.getUnitType()))) {
			accumAmtPartPlan();
		}
		
		if(initialUnits.isTrue()){
			vpxmatcrec.curuntbal.set(wsaaInitialAmount);
			matccpy.estimatedVal.set(wsaaInitialAmount);
			matccpy.type.set(wsccI);
		}else{
			vpxmatcrec.curuntbal.set(wsaaAccumAmount);
			matccpy.estimatedVal.set(wsaaAccumAmount);
			matccpy.type.set(wsccA);
		}
		matccpy.fund.set(wsaaVirtualFund);
		readT5515();
		if(isEQ(matccpy.endf,wsccI)){
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
		}
	}
	
	protected void accumAmtPartPlan(){
		if(isEQ(utrsclmIO.getUnitType(),wsccI)){
			wsaaInitialAmount.add(utrsclmIO.getCurrentUnitBal());
			initialUnits.setTrue();
		}else{
			wsaaAccumAmount.add(utrsclmIO.getCurrentUnitBal());
			accumUnits.setTrue();
		}
		
		utrsclmIO.setFormat(utrsclmrec);
		utrsclmIO.setFunction(varcom.nextr);

		SmartFileCode.execute(appVars, utrsclmIO);

		if(!isEQ(utrsclmIO.getStatuz(), varcom.oK)&&!isEQ(utrsclmIO.getStatuz(), varcom.endp)){
			syserrrec.params.set(utrsclmIO.getParams());
			dbError();
		}

		if(!isEQ(utrsclmIO.getChdrcoy(), matccpy.chdrChdrcoy)||!isEQ(utrsclmIO.getChdrnum(), matccpy.chdrChdrnum)||
				!isEQ(utrsclmIO.getLife() ,matccpy.lifeLife)||!isEQ(utrsclmIO.getCoverage(), matccpy.covrCoverage)||
				!isEQ(utrsclmIO.getRider(), matccpy.covrRider)||!isEQ(utrsclmIO.getPlanSuffix(), matccpy.planSuffix)||
				isEQ(utrsclmIO.getStatuz(), varcom.endp)){
			matccpy.endf.set(wsccI);
			utrsclmIO.setStatuz(varcom.endp);
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
		}
	}
	
	protected void getCrtableDesc(){
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matccpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(matccpy.crtable);
		descIO.setLanguage(wsccE);
		descIO.setFunction(varcom.readr);
		
		SmartFileCode.execute(appVars, descIO);
		if(!isEQ(descIO.getStatuz(), varcom.oK) && !isEQ(descIO.getStatuz(), varcom.mrnf)){
			syserrrec.params.set(descIO.getParams());
			dbError();
		}

		if(isEQ(descIO.getStatuz(), varcom.oK))
			matccpy.description.set(descIO.getLongdesc());
		else
			matccpy.description.set(SPACES);

		matccpy.type.set(wsccA);
	}
	
	protected void readT5515(){
		descIO.setDataArea(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matccpy.chdrChdrcoy);
		descIO.setLanguage(wsccE);
		descIO.setFunction(varcom.readr);
		
		SmartFileCode.execute(appVars, descIO);

		if(!isEQ(descIO.getStatuz(), varcom.oK) && !isEQ(descIO.getStatuz(), varcom.mrnf)){
			syserrrec.params.set(descIO.getParams());
			dbError();
		}
	
		if(isEQ(descIO.getStatuz(), varcom.oK))
			matccpy.description.set(descIO.getLongdesc());
		else
			matccpy.description.set(SPACES);

		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaVirtualFund);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);

		if(!isEQ(itdmIO.getStatuz(), varcom.oK)&&!isEQ(itdmIO.getStatuz(), varcom.endp)){
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError();
		}

		if(!isEQ(wsaaVirtualFund,itdmIO.getItemitem())||!isEQ(matccpy.chdrChdrcoy,itdmIO.getItemcoy())||
				!isEQ(itdmIO.getItemtabl(), t5515)||isEQ(itdmIO.getStatuz(), varcom.endp)){
			matccpy.currcode.set(SPACES);
		}else{
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
			matccpy.currcode.set(t5515rec.currcode);
			matccpy.element.set(wsaaVirtualFund);
		}
	}
	
	protected void systemError(){
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);		
	}
	
	protected void formatNumber(){		
		wsaaStart.set("N");
		ia.set(ZERO);
		ib.set(ZERO);
		do {
			ia.add(1);
			if (isGT(wsaaNumArr[ia.toInt()], 0) 
					&& isLTE(wsaaNumArr[ia.toInt()], 9)){
				wsaaStart.set("Y");
			}
			if (isEQ(wsaaNumArr[ia.toInt()], ".")) {
				if (isEQ(wsaaStart, "N")) {
					ib.add(1);
					vpxmatcrec.resultValue.setcharat(ib.toInt(), '0');
				}
				wsaaStart.set("Y");
			}
			if (isEQ(wsaaStart, "Y")
					&& (isNE(wsaaNumArr[ia.toInt()], SPACES) 
					|| isEQ(wsaaNumArr[ia.toInt()], "-"))){
				ib.add(1);							
				vpxmatcrec.resultValue.setcharat(ib.toInt(), wsaaNumArr[ia.toInt()].toString().charAt(0));
			}
		} while (isLT(ia, 20));
		
		if (isEQ(wsaaStart, "N")){
			vpxmatcrec.resultValue.set("0");
		}
		wsaaNumFmt.set(SPACES);
	}
	
	protected void callBegnHits(){
		//ILIFE-3535 starts
		ws88FirstHits.set("N");
		//ILIFE-3535 ends 
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(wsaaChdrcoy);
		hitsIO.setChdrnum(wsaaChdrnum);
		hitsIO.setLife(wsaaLife);
		hitsIO.setCoverage(wsaaCoverage);
		hitsIO.setRider(wsaaRider);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setZintbfnd(wsaaVirtualFund);
		hitsIO.setFormat(hitsrec);
		hitsIO.setFunction(varcom.begn);
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, hitsIO);

		if(!isEQ(hitsIO.getStatuz(), varcom.oK)&&!isEQ(hitsIO.getStatuz(), varcom.endp)){
			syserrrec.params.set(hitsIO.getParams());			
			dbError();
		}

		if(!isEQ(hitsIO.getChdrcoy(), wsaaChdrcoy)||!isEQ(hitsIO.getChdrnum(), wsaaChdrnum)||
				!isEQ(hitsIO.getLife(), wsaaLife)||!isEQ(hitsIO.getCoverage(), wsaaCoverage)||
				!isEQ(hitsIO.getRider(), wsaaRider)||isEQ(hitsIO.getStatuz(), varcom.endp)){
			hitsIO.setStatuz(varcom.endp);
			matccpy.status.set(varcom.endp);
			firstTime.setTrue();
		}
	}
	
	protected void interestBearing(){
		if(isEQ(hitsIO.getStatuz(),varcom.endp)){
			matccpy.endf.set(wsccY);
			matccpy.status.set(varcom.endp);
			firstTime.setTrue();
			firstHits.setTrue();
			return;
		}
		
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(hitsIO.getZintbfnd());
		
		while ( !(isEQ(hitsIO.getStatuz(),varcom.endp)|| isNE(wsaaVirtualFund,hitsIO.getZintbfnd()))) {
			accumAmount();
		}
		
		matccpy.estimatedVal.set(wsaaAccumAmount);
		vpxmatcrec.curuntbal.set(wsaaAccumAmount);
		matccpy.fund.set(wsaaVirtualFund);
		matccpy.type.set(wsccD);
		readT5515();
	}
	
	protected void accumAmount(){
		wsaaAccumAmount.add(hitsIO.getZcurprmbal());
		hitsIO.setFormat(hitsrec);
		hitsIO.setFunction(varcom.nextr);

		SmartFileCode.execute(appVars, hitsIO);
		if(!isEQ(hitsIO.getStatuz(), varcom.oK)&&!isEQ(hitsIO.getStatuz(), varcom.endp)){
			syserrrec.params.set(hitsIO.getParams());
			dbError();
		}
		if(!isEQ(hitsIO.getChdrcoy(), matccpy.chdrChdrcoy)||!isEQ(hitsIO.getChdrnum(), matccpy.chdrChdrnum)||
				!isEQ(hitsIO.getLife(), matccpy.lifeLife)||!isEQ(hitsIO.getCoverage(), matccpy.covrCoverage)||
				!isEQ(hitsIO.getRider(), matccpy.covrRider)||isEQ(hitsIO.getStatuz(), varcom.endp)){
			matccpy.endf.set(wsccY);
			hitsIO.setStatuz(varcom.endp);
			matccpy.status.set(varcom.endp);
			firstTime.setTrue();
			firstHits.setTrue();
		}
	}
}
