/*
 * File: Br554.java
 * Date: 29 August 2009 22:19:56
 * Author: Quipoz Limited
 * 
 * Class transformed from BR554.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This is a uploading program from LIA to Life/400.
*
*  When Uploading the LIA interface information, if
*  the Action Code is:
*
*  '1' - Create the entry into the LIA file.
*  '2' - Search for the required record by matching first by
*        new IC Number, failing which, the old IC Number will
*        be matched. If this still fails, then out an exception
*        listing and skip any further processing on this record.
*        Otherwise, replace the contents.
*  '3' - Upload the entry with Action Code as '3'
*
*****************************************************************
* </pre>
*/
public class Br554 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private DiskFileDAM liamFile = new DiskFileDAM("LIAMPF");

	private FixedLengthStringData liamRecord = new FixedLengthStringData(144);
	private FixedLengthStringData liamAction = new FixedLengthStringData(1).isAPartOf(liamRecord, 0);
	private FixedLengthStringData liamRskflg = new FixedLengthStringData(1).isAPartOf(liamRecord, 1);
	private FixedLengthStringData liamDtapp = new FixedLengthStringData(10).isAPartOf(liamRecord, 2);
	private FixedLengthStringData liamMlcoycde = new FixedLengthStringData(2).isAPartOf(liamRecord, 12);
	private FixedLengthStringData liamNic = new FixedLengthStringData(14).isAPartOf(liamRecord, 14);
	private FixedLengthStringData liamOic = new FixedLengthStringData(14).isAPartOf(liamRecord, 28);
	private FixedLengthStringData liamOth = new FixedLengthStringData(14).isAPartOf(liamRecord, 42);
	private FixedLengthStringData liamEntity = new FixedLengthStringData(15).isAPartOf(liamRecord, 56);
	private FixedLengthStringData liamDob = new FixedLengthStringData(10).isAPartOf(liamRecord, 71);
	private FixedLengthStringData liamMlhsperd = new FixedLengthStringData(3).isAPartOf(liamRecord, 81);
	private FixedLengthStringData liamMlmedcde01 = new FixedLengthStringData(3).isAPartOf(liamRecord, 84);
	private FixedLengthStringData liamMlmedcde02 = new FixedLengthStringData(3).isAPartOf(liamRecord, 87);
	private FixedLengthStringData liamMlmedcde03 = new FixedLengthStringData(3).isAPartOf(liamRecord, 90);
	private FixedLengthStringData liamName = new FixedLengthStringData(51).isAPartOf(liamRecord, 93);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR554");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaDates = new FixedLengthStringData(10);

	private FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(wsaaDates, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaDay = new FixedLengthStringData(2).isAPartOf(filler, 0);
	private FixedLengthStringData wsaaMonth = new FixedLengthStringData(2).isAPartOf(filler, 3);
	private FixedLengthStringData wsaaYear = new FixedLengthStringData(4).isAPartOf(filler, 6);
	private ZonedDecimalData wsaaDatec = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaDatec, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaYr = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaMth = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4).setPattern("ZZ");
	private ZonedDecimalData wsaaDy = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6).setPattern("ZZ");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String mliarec = "MLIAREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Substd & Hosp view by IC no. and Contrac*/
	private MliaTableDAM mliaIO = new MliaTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		update3010, 
		nextRecord3050, 
		exit3090
	}

	public Br554() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		liamFile.openInput();
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		if (isEQ(wsaaEof,"Y")) {
			wsspEdterror.set(varcom.endp);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case update3010: {
					update3010();
				}
				case nextRecord3050: {
					nextRecord3050();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		liamFile.read(liamRecord);
		if (liamFile.isAtEnd()) {
			wsaaEof.set("Y");
		}
		if (endOfFile.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		mliaIO.setHpropdte(varcom.vrcmMaxDate);
		mliaIO.setDob(varcom.vrcmMaxDate);
		mliaIO.setMlmedcde01(ZERO);
		mliaIO.setMlmedcde02(ZERO);
		mliaIO.setMlmedcde03(ZERO);
		if (isNE(liamNic,SPACES)) {
			mliaIO.setSecurityno(liamNic);
			mliaIO.setMloldic(liamOic);
			mliaIO.setMlothic(liamOth);
		}
		else {
			if (isNE(liamOic,SPACES)) {
				mliaIO.setSecurityno(liamOic);
				mliaIO.setMloldic(liamNic);
				mliaIO.setMlothic(liamOth);
			}
			else {
				mliaIO.setSecurityno(liamOth);
				mliaIO.setMloldic(liamOic);
				mliaIO.setMlothic(liamNic);
			}
		}
		if (isEQ(liamDob,SPACES)) {
			mliaIO.setDob(varcom.vrcmMaxDate);
		}
		else {
			wsaaDates.set(liamDob);
			wsaaYr.set(wsaaYear);
			wsaaMth.set(wsaaMonth);
			wsaaDy.set(wsaaDay);
			mliaIO.setDob(wsaaDatec);
		}
		if (isEQ(liamDtapp,SPACES)) {
			mliaIO.setHpropdte(varcom.vrcmMaxDate);
		}
		else {
			wsaaDates.set(liamDtapp);
			wsaaYr.set(wsaaYear);
			wsaaMth.set(wsaaMonth);
			wsaaDy.set(wsaaDay);
			mliaIO.setHpropdte(wsaaDatec);
		}
		mliaIO.setRskflg(liamRskflg);
		mliaIO.setActn(liamAction);
		mliaIO.setMlcoycde(liamMlcoycde);
		mliaIO.setMlentity(liamEntity);
		mliaIO.setMlhsperd(ZERO);
		if (isEQ(liamMlhsperd,NUMERIC)) {
			mliaIO.setMlhsperd(liamMlhsperd);
		}
		mliaIO.setClntnaml(liamName);
		mliaIO.setMlmedcde01(liamMlmedcde01);
		mliaIO.setMlmedcde02(liamMlmedcde02);
		mliaIO.setMlmedcde03(liamMlmedcde03);
		mliaIO.setIndc("Y");
		mliaIO.setMind("Y");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		mliaIO.setEffdate(wsaaToday);
		mliaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),varcom.oK)
		&& isNE(mliaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(mliaIO.getParams());
			syserrrec.statuz.set(mliaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(mliaIO.getStatuz(),varcom.oK)) {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.nextRecord3050);
		}
		mliaIO.setFunction(varcom.writr);
		mliaIO.setFormat(mliarec);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(mliaIO.getParams());
			syserrrec.statuz.set(mliaIO.getStatuz());
			fatalError600();
		}
	}

protected void nextRecord3050()
	{
		goTo(GotoLabel.update3010);
	}

protected void writeDetail3080()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		liamFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
