package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Vprcpf {

    private String company;
    private String unitVirtualFund;
    private String unitType;
    private int effdate;
    private int jobno;
    private BigDecimal unitBarePrice;
    private BigDecimal unitBidPrice;
    private BigDecimal unitOfferPrice;
    private String validflag;
    private int tranno;
    private int updateDate;
    private String procflag;
    private int transactionTime;
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }
    public String getUnitVirtualFund() {
        return unitVirtualFund;
    }
    public void setUnitVirtualFund(String unitVirtualFund) {
        this.unitVirtualFund = unitVirtualFund;
    }
    public String getUnitType() {
        return unitType;
    }
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }
    public int getEffdate() {
        return effdate;
    }
    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }
    public int getJobno() {
        return jobno;
    }
    public void setJobno(int jobno) {
        this.jobno = jobno;
    }
    public BigDecimal getUnitBarePrice() {
        return unitBarePrice;
    }
    public void setUnitBarePrice(BigDecimal unitBarePrice) {
        this.unitBarePrice = unitBarePrice;
    }
    public BigDecimal getUnitBidPrice() {
        return unitBidPrice;
    }
    public void setUnitBidPrice(BigDecimal unitBidPrice) {
        this.unitBidPrice = unitBidPrice;
    }
    public BigDecimal getUnitOfferPrice() {
        return unitOfferPrice;
    }
    public void setUnitOfferPrice(BigDecimal unitOfferPrice) {
        this.unitOfferPrice = unitOfferPrice;
    }
    public String getValidflag() {
        return validflag;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public int getTranno() {
        return tranno;
    }
    public void setTranno(int tranno) {
        this.tranno = tranno;
    }
    public int getUpdateDate() {
        return updateDate;
    }
    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }
    public String getProcflag() {
        return procflag;
    }
    public void setProcflag(String procflag) {
        this.procflag = procflag;
    }
    public int getTransactionTime() {
        return transactionTime;
    }
    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }
}