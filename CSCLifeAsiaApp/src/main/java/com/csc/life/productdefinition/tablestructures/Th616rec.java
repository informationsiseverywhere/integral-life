package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:38
 * Description:
 * Copybook name: TH616REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th616rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th616Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData mnth = new ZonedDecimalData(2, 0).isAPartOf(th616Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(th616Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th616Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th616Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}