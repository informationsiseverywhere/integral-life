package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:31
 * Description:
 * Copybook name: T5646REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5646rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5646Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(36).isAPartOf(t5646Rec, 0);
  	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(12, 3, 0, ageIssageFrms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData ageIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData ageIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData ageIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData ageIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData ageIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData ageIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData ageIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData ageIssageFrm09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData ageIssageFrm10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public ZonedDecimalData ageIssageFrm11 = new ZonedDecimalData(3, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData ageIssageFrm12 = new ZonedDecimalData(3, 0).isAPartOf(filler, 33);
  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(36).isAPartOf(t5646Rec, 36);
  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(12, 3, 0, ageIssageTos, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public ZonedDecimalData ageIssageTo09 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData ageIssageTo10 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 27);
  	public ZonedDecimalData ageIssageTo11 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData ageIssageTo12 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 33);
  	public ZonedDecimalData agelimit = new ZonedDecimalData(3, 0).isAPartOf(t5646Rec, 72);
  	public FixedLengthStringData factorsas = new FixedLengthStringData(84).isAPartOf(t5646Rec, 75);
  	public ZonedDecimalData[] factorsa = ZDArrayPartOfStructure(12, 7, 4, factorsas, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(84).isAPartOf(factorsas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData factorsa01 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 0);
  	public ZonedDecimalData factorsa02 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 7);
  	public ZonedDecimalData factorsa03 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 14);
  	public ZonedDecimalData factorsa04 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 21);
  	public ZonedDecimalData factorsa05 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 28);
  	public ZonedDecimalData factorsa06 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 35);
  	public ZonedDecimalData factorsa07 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 42);
  	public ZonedDecimalData factorsa08 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 49);
  	public ZonedDecimalData factorsa09 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 56);
  	public ZonedDecimalData factorsa10 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 63);
  	public ZonedDecimalData factorsa11 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 70);
  	public ZonedDecimalData factorsa12 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 77);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(341).isAPartOf(t5646Rec, 159, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5646Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5646Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}