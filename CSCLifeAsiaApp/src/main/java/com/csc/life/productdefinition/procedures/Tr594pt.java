/*
 * File: Tr594pt.java
 * Date: 30 August 2009 2:43:35
 * Author: Quipoz Limited
 * 
 * Class transformed from TR594PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr594rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR594.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr594pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Underwriter Level                       SR594");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(23);
	private FixedLengthStringData filler7 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine003, 20, FILLER).init("U/W");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(66);
	private FixedLengthStringData filler9 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine004, 17, FILLER).init("Plan Type      Standard Limit  Sub Standard Limit");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(65);
	private FixedLengthStringData filler11 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 21);
	private FixedLengthStringData filler12 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine005, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine005, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(65);
	private FixedLengthStringData filler14 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 21);
	private FixedLengthStringData filler15 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine006, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine006, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(65);
	private FixedLengthStringData filler17 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 21);
	private FixedLengthStringData filler18 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine007, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine007, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(65);
	private FixedLengthStringData filler20 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 21);
	private FixedLengthStringData filler21 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine008, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine008, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(65);
	private FixedLengthStringData filler23 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 21);
	private FixedLengthStringData filler24 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine009, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine009, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(65);
	private FixedLengthStringData filler26 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 21);
	private FixedLengthStringData filler27 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine010, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine010, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(65);
	private FixedLengthStringData filler29 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 21);
	private FixedLengthStringData filler30 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine011, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine011, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(65);
	private FixedLengthStringData filler32 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 21);
	private FixedLengthStringData filler33 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine012, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine012, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(65);
	private FixedLengthStringData filler35 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 21);
	private FixedLengthStringData filler36 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine013, 28).setPattern("ZZZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine013, 48).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(28);
	private FixedLengthStringData filler38 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr594rec tr594rec = new Tr594rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr594pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr594rec.tr594Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tr594rec.uwplntyp01);
		fieldNo006.set(tr594rec.undlimit01);
		fieldNo007.set(tr594rec.usundlim01);
		fieldNo008.set(tr594rec.uwplntyp02);
		fieldNo009.set(tr594rec.undlimit02);
		fieldNo010.set(tr594rec.usundlim02);
		fieldNo011.set(tr594rec.uwplntyp03);
		fieldNo012.set(tr594rec.undlimit03);
		fieldNo013.set(tr594rec.usundlim03);
		fieldNo014.set(tr594rec.uwplntyp04);
		fieldNo015.set(tr594rec.undlimit04);
		fieldNo016.set(tr594rec.usundlim04);
		fieldNo017.set(tr594rec.uwplntyp05);
		fieldNo018.set(tr594rec.undlimit05);
		fieldNo019.set(tr594rec.usundlim05);
		fieldNo020.set(tr594rec.uwplntyp06);
		fieldNo021.set(tr594rec.undlimit06);
		fieldNo022.set(tr594rec.usundlim06);
		fieldNo023.set(tr594rec.uwplntyp07);
		fieldNo024.set(tr594rec.undlimit07);
		fieldNo025.set(tr594rec.usundlim07);
		fieldNo026.set(tr594rec.uwplntyp08);
		fieldNo027.set(tr594rec.undlimit08);
		fieldNo028.set(tr594rec.usundlim08);
		fieldNo029.set(tr594rec.uwplntyp09);
		fieldNo030.set(tr594rec.undlimit09);
		fieldNo031.set(tr594rec.usundlim09);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
