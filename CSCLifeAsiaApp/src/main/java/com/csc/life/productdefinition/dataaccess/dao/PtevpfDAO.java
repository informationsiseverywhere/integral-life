package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Ptevpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PtevpfDAO extends BaseDAO<Ptevpf> {
    public void insertPtevpfRecord(List<Ptevpf> ptevpfList);
}