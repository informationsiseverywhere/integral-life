package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sr28yscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr28yScreenVars sv = (Sr28yScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr28yscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr28yScreenVars screenVars = (Sr28yScreenVars)pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.vpmsprop.clear();
		screenVars.maptofield.clear();
		screenVars.copybk.clear();
		screenVars.numoccurs.clear();
		screenVars.attrslbl01.clear();
		screenVars.attrsvalue01.clear();
		screenVars.attrslbl02.clear();
		screenVars.attrsvalue02.clear();
		screenVars.attrslbl03.clear();
		screenVars.attrsvalue03.clear();
		screenVars.attrslbl04.clear();
		screenVars.attrsvalue04.clear();
		screenVars.attrslbl05.clear();
		screenVars.attrsvalue05.clear();
		screenVars.attrslbl06.clear();
		screenVars.attrsvalue06.clear();
		screenVars.attrslbl07.clear();
		screenVars.attrsvalue07.clear();
		screenVars.attrslbl08.clear();
		screenVars.attrsvalue08.clear();
		screenVars.attrslbl09.clear();
		screenVars.attrsvalue09.clear();
		screenVars.attrslbl10.clear();
		screenVars.attrsvalue10.clear();
		screenVars.attrslbl11.clear();
		screenVars.attrsvalue11.clear();
		screenVars.attrslbl12.clear();
		screenVars.attrsvalue12.clear();
		screenVars.attrslbl13.clear();
		screenVars.attrsvalue13.clear();
		screenVars.attrslbl14.clear();
		screenVars.attrsvalue14.clear();
		screenVars.attrslbl15.clear();
		screenVars.attrsvalue15.clear();
		screenVars.attrslbl16.clear();
		screenVars.attrsvalue16.clear();
	}

/**
 * Clear all the variables in S5675screen
 */
	public static void clear(VarModel pv) {
		Sr28yScreenVars screenVars = (Sr28yScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.vpmsprop.clear();
		screenVars.maptofield.clear();
		screenVars.copybk.clear();
		screenVars.numoccurs.clear();
		screenVars.attrslbl01.clear();
		screenVars.attrsvalue01.clear();
		screenVars.attrslbl02.clear();
		screenVars.attrsvalue02.clear();
		screenVars.attrslbl03.clear();
		screenVars.attrsvalue03.clear();
		screenVars.attrslbl04.clear();
		screenVars.attrsvalue04.clear();
		screenVars.attrslbl05.clear();
		screenVars.attrsvalue05.clear();
		screenVars.attrslbl06.clear();
		screenVars.attrsvalue06.clear();
		screenVars.attrslbl07.clear();
		screenVars.attrsvalue07.clear();
		screenVars.attrslbl08.clear();
		screenVars.attrsvalue08.clear();
		screenVars.attrslbl09.clear();
		screenVars.attrsvalue09.clear();
		screenVars.attrslbl10.clear();
		screenVars.attrsvalue10.clear();
		screenVars.attrslbl11.clear();
		screenVars.attrsvalue11.clear();
		screenVars.attrslbl12.clear();
		screenVars.attrsvalue12.clear();
		screenVars.attrslbl13.clear();
		screenVars.attrsvalue13.clear();
		screenVars.attrslbl14.clear();
		screenVars.attrsvalue14.clear();
		screenVars.attrslbl15.clear();
		screenVars.attrsvalue15.clear();
		screenVars.attrslbl16.clear();
		screenVars.attrsvalue16.clear();
	}
}
