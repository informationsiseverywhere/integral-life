/*
 * File: Ph549.java
 * Date: 30 August 2009 1:07:53
 * Author: Quipoz Limited
 * 
 * Class transformed from PH549.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.procedures.Th549pt;
import com.csc.life.productdefinition.screens.Sh549ScreenVars;
import com.csc.life.productdefinition.tablestructures.Th549rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Ph549 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH549");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/* WSAA-MORTALITY-RATE */
	private String n025m = "A";
	private String n025f = "A";
	private String n037m = "B";
	private String n037f = "B";
	private String n050m = "C";
	private String n050f = "C";
	private String n075m = "D";
	private String n075f = "D";
	private String n100m = "E";
	private String n100f = "E";
	private String n125m = "F";
	private String n125f = "F";
	private String n150m = "G";
	private String n150f = "G";
	private String n175m = "H";
	private String n175f = "H";
	private String n200m = "I";
	private String n200f = "I";
	private String n250m = "J";
	private String n250f = "J";

	private FixedLengthStringData wsaaTemp = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverge = new FixedLengthStringData(4).isAPartOf(wsaaTemp, 0);
	private FixedLengthStringData wsaaRate = new FixedLengthStringData(4).isAPartOf(wsaaTemp, 4);

	private FixedLengthStringData wsaaItemSmokM = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaXSM = new FixedLengthStringData(1).isAPartOf(wsaaItemSmokM, 0);
	private FixedLengthStringData wsaaYSM = new FixedLengthStringData(1).isAPartOf(wsaaItemSmokM, 1).init("1");

	private FixedLengthStringData wsaaItemNonM = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaXNM = new FixedLengthStringData(1).isAPartOf(wsaaItemNonM, 0);
	private FixedLengthStringData wsaaYNM = new FixedLengthStringData(1).isAPartOf(wsaaItemNonM, 1).init("2");

	private FixedLengthStringData wsaaItemSmokF = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaXSF = new FixedLengthStringData(1).isAPartOf(wsaaItemSmokF, 0);
	private FixedLengthStringData wsaaYSF = new FixedLengthStringData(1).isAPartOf(wsaaItemSmokF, 1).init("3");

	private FixedLengthStringData wsaaItemNonF = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaXNF = new FixedLengthStringData(1).isAPartOf(wsaaItemNonF, 0);
	private FixedLengthStringData wsaaYNF = new FixedLengthStringData(1).isAPartOf(wsaaItemNonF, 1).init("4");
		/* ERRORS */
	private String e186 = "E186";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Th549rec th549rec = new Th549rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh549ScreenVars sv = ScreenProgram.getScreenVars( Sh549ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Ph549() {
		super();
		screenVars = sv;
		new ScreenModel("Sh549", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
					confirmationFields1050();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th549rec.th549Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		th549rec.expfactor.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.expfactor.set(th549rec.expfactor);
		sv.indcs.set(th549rec.indcs);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.opcdas.set(th549rec.opcdas);
		sv.subrtns.set(th549rec.subrtns);
	}

protected void confirmationFields1050()
	{
		wsaaTemp.set(itmdIO.getItemItemitem());
		if (isEQ(wsspcomn.flag,"C")) {
			if (isEQ(wsaaRate,"025M")){
				wsaaXSM.set(n025m);
				wsaaXNM.set(n025m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"025F")){
				wsaaXSF.set(n025f);
				wsaaXNF.set(n025f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"037M")){
				wsaaXSM.set(n037m);
				wsaaXNM.set(n037m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"037F")){
				wsaaXSF.set(n037f);
				wsaaXNF.set(n037f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"050M")){
				wsaaXSM.set(n050m);
				wsaaXNM.set(n050m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"050F")){
				wsaaXSF.set(n050f);
				wsaaXNF.set(n050f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"075M")){
				wsaaXSM.set(n075m);
				wsaaXNM.set(n075m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"075F")){
				wsaaXSF.set(n075f);
				wsaaXNF.set(n075f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"100M")){
				wsaaXSM.set(n100m);
				wsaaXNM.set(n100m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"100F")){
				wsaaXSF.set(n100f);
				wsaaXNF.set(n100f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"125M")){
				wsaaXSM.set(n125m);
				wsaaXNM.set(n125m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"125F")){
				wsaaXSF.set(n125f);
				wsaaXNF.set(n125f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"150M")){
				wsaaXSM.set(n150m);
				wsaaXNM.set(n150m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"150F")){
				wsaaXSF.set(n150f);
				wsaaXNF.set(n150f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"175M")){
				wsaaXSM.set(n175m);
				wsaaXNM.set(n175m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"175F")){
				wsaaXSF.set(n175f);
				wsaaXNF.set(n175f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"200M")){
				wsaaXSM.set(n200m);
				wsaaXNM.set(n200m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"200F")){
				wsaaXSF.set(n200f);
				wsaaXNF.set(n200f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
			else if (isEQ(wsaaRate,"250M")){
				wsaaXSM.set(n250m);
				wsaaXNM.set(n250m);
				sv.indc01.set(wsaaItemSmokM);
				sv.indc02.set(wsaaItemNonM);
			}
			else if (isEQ(wsaaRate,"250F")){
				wsaaXSF.set(n250f);
				wsaaXNF.set(n250f);
				sv.indc01.set(wsaaItemSmokF);
				sv.indc02.set(wsaaItemNonF);
			}
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.indc01,SPACES)) {
			sv.indc01Err.set(e186);
		}
		if (isEQ(sv.indc02,SPACES)) {
			sv.indc02Err.set(e186);
		}
		if (isNE(sv.opcda01,SPACES)) {
			if (isEQ(sv.subrtn01,SPACES)) {
				sv.subrtn01Err.set(e186);
			}
		}
		if (isNE(sv.opcda02,SPACES)) {
			if (isEQ(sv.subrtn02,SPACES)) {
				sv.subrtn01Err.set(e186);
			}
		}
		if (isNE(sv.opcda03,SPACES)) {
			if (isEQ(sv.subrtn03,SPACES)) {
				sv.subrtn01Err.set(e186);
			}
		}
		if (isNE(sv.opcda04,SPACES)) {
			if (isEQ(sv.subrtn04,SPACES)) {
				sv.subrtn01Err.set(e186);
			}
		}
		if (isNE(sv.opcda05,SPACES)) {
			if (isEQ(sv.subrtn05,SPACES)) {
				sv.subrtn01Err.set(e186);
			}
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(th549rec.th549Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.expfactor,th549rec.expfactor)) {
			th549rec.expfactor.set(sv.expfactor);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.indcs,th549rec.indcs)) {
			th549rec.indcs.set(sv.indcs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.opcdas,th549rec.opcdas)) {
			th549rec.opcdas.set(sv.opcdas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.subrtns,th549rec.subrtns)) {
			th549rec.subrtns.set(sv.subrtns);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Th549pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
