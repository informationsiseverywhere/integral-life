package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Lextpf {
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int seqnbr;
	private String validflag;
	private int tranno;
	private int currfrom;
	private int currto;
	private String opcda;
	private BigDecimal oppc;
	private int insprm;
	private int agerate;
	private String sbstdl;
	private String termid;
	private int transactionDate;
	private int transactionTime;
	private int user;
	private String jlife;
	private int extCommDate;
	private int extCessTerm;
	private int extCessDate;
	private String reasind;
	private BigDecimal znadjperc;
	private int zmortpct;
	private String userProfile;
	private String jobName;
	private String datime;
	private BigDecimal premadj;
	private long uniqueNumber;

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getSeqnbr() {
		return seqnbr;
	}

	public void setSeqnbr(int seqnbr) {
		this.seqnbr = seqnbr;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public int getCurrfrom() {
		return currfrom;
	}

	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}

	public int getCurrto() {
		return currto;
	}

	public void setCurrto(int currto) {
		this.currto = currto;
	}

	public String getOpcda() {
		return opcda;
	}

	public void setOpcda(String opcda) {
		this.opcda = opcda;
	}

	public BigDecimal getOppc() {
		return oppc;
	}

	public void setOppc(BigDecimal oppc) {
		this.oppc = oppc;
	}

	public int getInsprm() {
		return insprm;
	}

	public void setInsprm(int insprm) {
		this.insprm = insprm;
	}

	public int getAgerate() {
		return agerate;
	}

	public void setAgerate(int agerate) {
		this.agerate = agerate;
	}

	public String getSbstdl() {
		return sbstdl;
	}

	public void setSbstdl(String sbstdl) {
		this.sbstdl = sbstdl;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public int getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}

	public int getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public int getExtCommDate() {
		return extCommDate;
	}

	public void setExtCommDate(int extCommDate) {
		this.extCommDate = extCommDate;
	}

	public int getExtCessTerm() {
		return extCessTerm;
	}

	public void setExtCessTerm(int extCessTerm) {
		this.extCessTerm = extCessTerm;
	}

	public int getExtCessDate() {
		return extCessDate;
	}

	public void setExtCessDate(int extCessDate) {
		this.extCessDate = extCessDate;
	}

	public String getReasind() {
		return reasind;
	}

	public void setReasind(String reasind) {
		this.reasind = reasind;
	}

	public BigDecimal getZnadjperc() {
		return znadjperc;
	}

	public void setZnadjperc(BigDecimal znadjperc) {
		this.znadjperc = znadjperc;
	}

	public int getZmortpct() {
		return zmortpct;
	}

	public void setZmortpct(int zmortpct) {
		this.zmortpct = zmortpct;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

	public BigDecimal getPremadj() {
		return premadj;
	}

	public void setPremadj(BigDecimal premadj) {
		this.premadj = premadj;
	}

}