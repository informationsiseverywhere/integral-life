package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Br646DTO {
	private long chdrUniqueNumber;
	private int tranno;
	private String chdrpfx;
	private String cntcurr;
	private String cnttype;
	private BigDecimal totpymt;
	private String bankkey;
	private String bankacckey;
	private String paymth;
	private String facthous;
	private String clntpfx;
	private String clntcoy;
	private String clttype;
	private String language;
	private String surname;
	private String initials;
	private String salutl;
	private String lsurname;
	private String lgivname;
	private long mediUniqueNo;	
	
	public long getChdrUniqueNumber() {
		return chdrUniqueNumber;
	}
	public void setChdrUniqueNumber(long chdrUniqueNumber) {
		this.chdrUniqueNumber = chdrUniqueNumber;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}	
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public BigDecimal getTotpymt() {
		return totpymt;
	}
	public void setTotpymt(BigDecimal totpymt) {
		this.totpymt = totpymt;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getPaymth() {
		return paymth;
	}
	public void setPaymth(String paymth) {
		this.paymth = paymth;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(String clntpfx) {
		this.clntpfx = clntpfx;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClttype() {
		return clttype;
	}
	public void setClttype(String clttype) {
		this.clttype = clttype;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	public String getSalutl() {
		return salutl;
	}
	public void setSalutl(String salutl) {
		this.salutl = salutl;
	}
	public String getLsurname() {
		return lsurname;
	}
	public void setLsurname(String lsurname) {
		this.lsurname = lsurname;
	}
	public String getLgivname() {
		return lgivname;
	}
	public void setLgivname(String lgivname) {
		this.lgivname = lgivname;
	}
	public long getMediUniqueNo() {
		return mediUniqueNo;
	}
	public void setMediUniqueNo(long mediUniqueNo) {
		this.mediUniqueNo = mediUniqueNo;
	} 

}
