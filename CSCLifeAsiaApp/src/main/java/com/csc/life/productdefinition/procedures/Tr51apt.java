/*
 * File: Tr51apt.java
 * Date: 30 August 2009 2:41:51
 * Author: Quipoz Limited
 * 
 * Class transformed from TR51APT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr51arec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR51A.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr51apt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Rider SA cannot > Basic SA Validation           SR51A");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(47);
	private FixedLengthStringData filler7 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 7, FILLER).init("Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 21);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 31, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 37);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(18);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Component :");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler12 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 7);
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 14);
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 21);
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 28);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 35);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 42);
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 49);
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 56);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 63);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 67, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 70);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(74);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 7);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 14);
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 21);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 28);
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 35);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 42);
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 49);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 56);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 63);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 67, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 70);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(40);
	private FixedLengthStringData filler32 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine007, 20, FILLER).init("Comparison        SA");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(70);
	private FixedLengthStringData filler34 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine008, 6, FILLER).init("Age Band      Operator       Up to(%)        Maximum Sum Assured");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(69);
	private FixedLengthStringData filler36 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler37 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 10).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 23);
	private FixedLengthStringData filler39 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 37).setPattern("ZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(69);
	private FixedLengthStringData filler41 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler42 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 10).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 23);
	private FixedLengthStringData filler44 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(69);
	private FixedLengthStringData filler46 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler47 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 10).setPattern("ZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 23);
	private FixedLengthStringData filler49 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 37).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine011, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(69);
	private FixedLengthStringData filler51 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler52 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 10).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 23);
	private FixedLengthStringData filler54 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 37).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(69);
	private FixedLengthStringData filler56 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler57 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 10).setPattern("ZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 23);
	private FixedLengthStringData filler59 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 37).setPattern("ZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine013, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(69);
	private FixedLengthStringData filler61 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler62 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 10).setPattern("ZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 23);
	private FixedLengthStringData filler64 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 37).setPattern("ZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine014, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(69);
	private FixedLengthStringData filler66 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler67 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 10).setPattern("ZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 23);
	private FixedLengthStringData filler69 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 37).setPattern("ZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(69);
	private FixedLengthStringData filler71 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler72 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 10).setPattern("ZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 23);
	private FixedLengthStringData filler74 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 37).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(69);
	private FixedLengthStringData filler76 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler77 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 10).setPattern("ZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine017, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 23);
	private FixedLengthStringData filler79 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 37).setPattern("ZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine017, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine017, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(69);
	private FixedLengthStringData filler81 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler82 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 6, FILLER).init("<=");
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine018, 10).setPattern("ZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine018, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 23);
	private FixedLengthStringData filler84 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine018, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine018, 37).setPattern("ZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine018, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine018, 51).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr51arec tr51arec = new Tr51arec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr51apt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr51arec.tr51aRec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo027.set(tr51arec.age01);
		fieldNo029.set(tr51arec.overdueMina01);
		fieldNo030.set(tr51arec.sumins01);
		fieldNo031.set(tr51arec.age02);
		fieldNo033.set(tr51arec.overdueMina02);
		fieldNo034.set(tr51arec.sumins02);
		fieldNo035.set(tr51arec.age03);
		fieldNo037.set(tr51arec.overdueMina03);
		fieldNo038.set(tr51arec.sumins03);
		fieldNo039.set(tr51arec.age04);
		fieldNo041.set(tr51arec.overdueMina04);
		fieldNo042.set(tr51arec.sumins04);
		fieldNo043.set(tr51arec.age05);
		fieldNo045.set(tr51arec.overdueMina05);
		fieldNo046.set(tr51arec.sumins05);
		fieldNo047.set(tr51arec.age06);
		fieldNo049.set(tr51arec.overdueMina06);
		fieldNo050.set(tr51arec.sumins06);
		fieldNo051.set(tr51arec.age07);
		fieldNo053.set(tr51arec.overdueMina07);
		fieldNo054.set(tr51arec.sumins07);
		fieldNo055.set(tr51arec.age08);
		fieldNo057.set(tr51arec.overdueMina08);
		fieldNo058.set(tr51arec.sumins08);
		fieldNo059.set(tr51arec.age09);
		fieldNo061.set(tr51arec.overdueMina09);
		fieldNo062.set(tr51arec.sumins09);
		fieldNo063.set(tr51arec.age10);
		fieldNo065.set(tr51arec.overdueMina10);
		fieldNo066.set(tr51arec.sumins10);
		fieldNo007.set(tr51arec.crtable01);
		fieldNo008.set(tr51arec.crtable02);
		fieldNo009.set(tr51arec.crtable03);
		fieldNo010.set(tr51arec.crtable04);
		fieldNo011.set(tr51arec.crtable05);
		fieldNo012.set(tr51arec.crtable06);
		fieldNo013.set(tr51arec.crtable07);
		fieldNo014.set(tr51arec.crtable08);
		fieldNo015.set(tr51arec.crtable09);
		fieldNo016.set(tr51arec.crtable10);
		fieldNo017.set(tr51arec.crtable11);
		fieldNo018.set(tr51arec.crtable12);
		fieldNo019.set(tr51arec.crtable13);
		fieldNo020.set(tr51arec.crtable14);
		fieldNo021.set(tr51arec.crtable15);
		fieldNo022.set(tr51arec.crtable16);
		fieldNo023.set(tr51arec.crtable17);
		fieldNo024.set(tr51arec.crtable18);
		fieldNo025.set(tr51arec.crtable19);
		fieldNo026.set(tr51arec.crtable20);
		fieldNo028.set(tr51arec.indc01);
		fieldNo032.set(tr51arec.indc02);
		fieldNo036.set(tr51arec.indc03);
		fieldNo040.set(tr51arec.indc04);
		fieldNo044.set(tr51arec.indc05);
		fieldNo048.set(tr51arec.indc06);
		fieldNo052.set(tr51arec.indc07);
		fieldNo056.set(tr51arec.indc08);
		fieldNo060.set(tr51arec.indc09);
		fieldNo064.set(tr51arec.indc10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
