package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MinspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:50
 * Class transformed from MINSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MinspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 138;
	public FixedLengthStringData minsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData minspfRecord = minsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(minsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(minsrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(minsrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(minsrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(minsrec);
	public PackedDecimalData linstamt01 = DD.linstamt.copy().isAPartOf(minsrec);
	public PackedDecimalData linstamt02 = DD.linstamt.copy().isAPartOf(minsrec);
	public PackedDecimalData linstamt03 = DD.linstamt.copy().isAPartOf(minsrec);
	public PackedDecimalData linstamt04 = DD.linstamt.copy().isAPartOf(minsrec);
	public PackedDecimalData linstamt05 = DD.linstamt.copy().isAPartOf(minsrec);
	public PackedDecimalData linstamt06 = DD.linstamt.copy().isAPartOf(minsrec);
	public PackedDecimalData linstamt07 = DD.linstamt.copy().isAPartOf(minsrec);
	public PackedDecimalData datedue01 = DD.datedue.copy().isAPartOf(minsrec);
	public PackedDecimalData datedue02 = DD.datedue.copy().isAPartOf(minsrec);
	public PackedDecimalData datedue03 = DD.datedue.copy().isAPartOf(minsrec);
	public PackedDecimalData datedue04 = DD.datedue.copy().isAPartOf(minsrec);
	public PackedDecimalData datedue05 = DD.datedue.copy().isAPartOf(minsrec);
	public PackedDecimalData datedue06 = DD.datedue.copy().isAPartOf(minsrec);
	public PackedDecimalData datedue07 = DD.datedue.copy().isAPartOf(minsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(minsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(minsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(minsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MinspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MinspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MinspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MinspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MinspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MinspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MinspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MINSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"LINSTAMT01, " +
							"LINSTAMT02, " +
							"LINSTAMT03, " +
							"LINSTAMT04, " +
							"LINSTAMT05, " +
							"LINSTAMT06, " +
							"LINSTAMT07, " +
							"DATEDUE01, " +
							"DATEDUE02, " +
							"DATEDUE03, " +
							"DATEDUE04, " +
							"DATEDUE05, " +
							"DATEDUE06, " +
							"DATEDUE07, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     linstamt01,
                                     linstamt02,
                                     linstamt03,
                                     linstamt04,
                                     linstamt05,
                                     linstamt06,
                                     linstamt07,
                                     datedue01,
                                     datedue02,
                                     datedue03,
                                     datedue04,
                                     datedue05,
                                     datedue06,
                                     datedue07,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		linstamt01.clear();
  		linstamt02.clear();
  		linstamt03.clear();
  		linstamt04.clear();
  		linstamt05.clear();
  		linstamt06.clear();
  		linstamt07.clear();
  		datedue01.clear();
  		datedue02.clear();
  		datedue03.clear();
  		datedue04.clear();
  		datedue05.clear();
  		datedue06.clear();
  		datedue07.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMinsrec() {
  		return minsrec;
	}

	public FixedLengthStringData getMinspfRecord() {
  		return minspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMinsrec(what);
	}

	public void setMinsrec(Object what) {
  		this.minsrec.set(what);
	}

	public void setMinspfRecord(Object what) {
  		this.minspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(minsrec.getLength());
		result.set(minsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}