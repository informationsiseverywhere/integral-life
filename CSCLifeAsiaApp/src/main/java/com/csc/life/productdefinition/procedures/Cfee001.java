/*
 * File: Cfee001.java
 * Date: 29 August 2009 22:38:09
 * Author: Quipoz Limited
 *
 * Class transformed from CFEE001.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*              CONTRACT MANAGEMENT FEE CALCULATION
*
* If contract management fees are to be calculated, then an
* entry will be found on the table T5688. This entry is then
* used to reference table T5674 where this subroutine, for
* calculating the fee, will be found.
*
* This routine accesses table T5567 using ITDM. The table is
* found using contract type and currency.
*
* The contract management fee is returned in the linkage
* section.
*
* All the frequency entries are matched against the billing
* frequency, passed in the linkage area, and the corresponding
* fee returned to the linkage area.
*
* Three status types are returned.
*     O-K   - The relevant fee has been found.
*     ENDP  - The billing frequency has not been found.
*    other  - The ITDM came up with an error.
*****************************************************************
* </pre>
*/
public class Cfee001 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CFEE001";

	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 3);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
		/* ERRORS */
	private String h966 = "H966";
		/* TABLES */
	private String t5567 = "T5567";
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5567rec t5567rec = new T5567rec();
	private Varcom varcom = new Varcom();

	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		exit3090
	}

	public Cfee001() {
		super();
	}

public void mainline(Object... parmArray)
	{
		mgfeelrec.mgfeelRec = convertAndSetParam(mgfeelrec.mgfeelRec, parmArray, 0);
		try {
			subroutineMain0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void subroutineMain0000()
	{
		/*MAIN*/
		initialise1000();
		process2000();
		termination3000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		mgfeelrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaCnttype.set(mgfeelrec.cnttype);
		wsaaCntcurr.set(mgfeelrec.cntcurr);
		/*EXIT*/
	}

protected void process2000()
	{
		try {
			process2010();
		}
		catch (GOTOException e){
		}
	}

protected void process2010()
	{
		/*itdmIO.setItemcoy(mgfeelrec.company);
		itdmIO.setItemtabl(t5567);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(mgfeelrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			mgfeelrec.statuz.set(itdmIO.getStatuz());
			goTo(GotoLabel.exit2090);
		}
		if ((isNE(itdmIO.getItemcoy(),mgfeelrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5567))
		|| (isNE(itdmIO.getItemitem(),wsaaItemitem))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			mgfeelrec.statuz.set(h966);
			goTo(GotoLabel.exit2090);
		}
		else {
			t5567rec.t5567Rec.set(itdmIO.getGenarea());
		}*/	
		
		
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(mgfeelrec.company.toString());
		itempf.setItemitem(wsaaItemitem.toString());
		itempf.setItemtabl(t5567);
		itempf.setItmfrm(mgfeelrec.effdate.getbigdata());
		itempf.setItmto(mgfeelrec.effdate.getbigdata());
		
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if((isNE(it.getItemitem(),wsaaItemitem)))
				{
					mgfeelrec.statuz.set(h966);
					goTo(GotoLabel.exit2090);
				}
				t5567rec.t5567Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
		} 
		
		for (wsaaSub.set(1); !(isEQ(wsaaSub,10)
		|| isEQ(t5567rec.billfreq[wsaaSub.toInt()],mgfeelrec.billfreq)); wsaaSub.add(1)){
			matchFrequency2100();
		}
	}

protected void matchFrequency2100()
	{
		/*MATCH-FREQUENCY*/
		/*EXIT*/
	}

protected void termination3000()
	{
		try {
			termination3010();
		}
		catch (GOTOException e){
		}
	}

protected void termination3010()
	{
		if (isNE(mgfeelrec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(t5567rec.billfreq[wsaaSub.toInt()],mgfeelrec.billfreq)) {
			mgfeelrec.mgfee.set(t5567rec.cntfee[wsaaSub.toInt()]);
		}
		else {
			mgfeelrec.mgfee.set(ZERO);
			mgfeelrec.statuz.set(varcom.endp);
		}
	}
}
