package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:41
 * Description:
 * Copybook name: ACMVBK2KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvbk2key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvbk2FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvbk2Key = new FixedLengthStringData(64).isAPartOf(acmvbk2FileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvbk2Rldgcoy = new FixedLengthStringData(1).isAPartOf(acmvbk2Key, 0);
  	public FixedLengthStringData acmvbk2Rldgacct = new FixedLengthStringData(16).isAPartOf(acmvbk2Key, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(acmvbk2Key, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvbk2FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvbk2FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}