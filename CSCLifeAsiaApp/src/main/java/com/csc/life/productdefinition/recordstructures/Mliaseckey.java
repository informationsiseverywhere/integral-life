package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:45
 * Description:
 * Copybook name: MLIASECKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mliaseckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mliasecFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mliasecKey = new FixedLengthStringData(64).isAPartOf(mliasecFileKey, 0, REDEFINE);
  	public FixedLengthStringData mliasecSecurityno = new FixedLengthStringData(15).isAPartOf(mliasecKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(mliasecKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mliasecFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mliasecFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}