package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:22
 * Description:
 * Copybook name: CHDRSDCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrsdckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrsdcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrsdcKey = new FixedLengthStringData(64).isAPartOf(chdrsdcFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrsdcChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrsdcKey, 0);
  	public FixedLengthStringData chdrsdcChdrnum = new FixedLengthStringData(8).isAPartOf(chdrsdcKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrsdcKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrsdcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrsdcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}