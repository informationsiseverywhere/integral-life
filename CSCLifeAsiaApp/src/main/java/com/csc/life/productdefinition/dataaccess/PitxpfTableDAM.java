package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;


/**
 * 	
 * File: PitxpfTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:46
 * Class transformed from PITXPF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PitxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 25;
	public FixedLengthStringData pitxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData pitxpfRecord = pitxrec;
	
	public FixedLengthStringData itempfx = DD.itempfx.copy().isAPartOf(pitxrec);
	public FixedLengthStringData itemcoy = DD.itemcoy.copy().isAPartOf(pitxrec);
	public FixedLengthStringData itemtabl = DD.itemtabl.copy().isAPartOf(pitxrec);
	public FixedLengthStringData itemitem = DD.itemitem.copy().isAPartOf(pitxrec);
	public FixedLengthStringData itemToCreate = DD.itemcrt.copy().isAPartOf(pitxrec);
	public FixedLengthStringData change = DD.change.copy().isAPartOf(pitxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PitxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for PitxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PitxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PitxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PitxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PitxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PitxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PITXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ITEMPFX, " +
							"ITEMCOY, " +
							"ITEMTABL, " +
							"ITEMITEM, " +
							"ITEMCRT, " +
							"CHANGE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     itempfx,
                                     itemcoy,
                                     itemtabl,
                                     itemitem,
                                     itemToCreate,
                                     change,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		itempfx.clear();
  		itemcoy.clear();
  		itemtabl.clear();
  		itemitem.clear();
  		itemToCreate.clear();
  		change.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPitxrec() {
  		return pitxrec;
	}

	public FixedLengthStringData getPitxpfRecord() {
  		return pitxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPitxrec(what);
	}

	public void setPitxrec(Object what) {
  		this.pitxrec.set(what);
	}

	public void setPitxpfRecord(Object what) {
  		this.pitxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(pitxrec.getLength());
		result.set(pitxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}