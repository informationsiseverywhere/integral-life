package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br646DAO extends BaseDAO<MedxTemppf> {
	public List<MedxTemppf> loadDataByBatch(int batchID);
	public int populateBr646Temp(int batchExtractSize, String medxtempTable, String chdrfrom, String chdrto, String fsucoy);

}
