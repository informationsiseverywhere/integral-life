/*********************  */
/*Author  :Liwei						*/
/*Purpose :Instead of subroutine rlpdlon*/
/*Date    :2018.10.26					*/
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.recordstructures.CashedPojo;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.contractservicing.procedures.LoanpymtUtils;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Th623rec;
import com.csc.life.regularprocessing.procedures.CrtloanPojo;
import com.csc.life.regularprocessing.procedures.CrtloanUtils;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class RlpdlonUtilsImpl implements RlpdlonUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(RlpdlonUtilsImpl.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private BigDecimal wsaaAmount;
	private int wsaaSequenceNo = 0;
	
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
			
	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlPrefix = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Th623rec th623rec = new Th623rec();
	private T5645rec t5645rec = new T5645rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Syserr syserr = new Syserr();
	private Chdrpf chdrenq;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	boolean wsaaEof = false;
	Iterator<Acblpf> iterator ;

	//ILIFE-7107
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Payrpf payrpf = null;
	private LifrtrnUtils lifrtrnUtils = getApplicationContext().getBean("lifrtrnUtils", LifrtrnUtils.class);
	private LifrtrnPojo lifrtrnPojo = new LifrtrnPojo();
	//ILB-446
	private CashedPojo cashedPojo = new CashedPojo();								
	private LoanpymtUtils loanpymtUtils = getApplicationContext().getBean("loanpymtUtils", LoanpymtUtils.class);
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private FixedLengthStringData wsaaRldgXtra = new FixedLengthStringData(4).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaT5645Sacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sacstyp = new FixedLengthStringData(2);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private List<Acblpf> acblpfList;
	private Acblpf acblpf = null;
	private PackedDecimalData wsaaPremiumDeposit = new PackedDecimalData(17, 2);
	private CrtloanUtils crtloanUtils = getApplicationContext().getBean("crtloanUtils", CrtloanUtils.class);
	private CrtloanPojo crtloanPojo = new CrtloanPojo();
	
	public void callRlpdlon(RlpdlonPojo rlpdlonPojo) {
		initialize1000(rlpdlonPojo);
		if ((null == chdrenq) && isEQ(rlpdlonPojo.getFunction(), varcom.info)){
			return ;
		}
		if (wsaaEof) {
			return ;
		}
		if (isEQ(rlpdlonPojo.getFunction(), varcom.info)){
			checkApa2000(rlpdlonPojo);
		}
		else if (isEQ(rlpdlonPojo.getFunction(), varcom.insr)){
			insrApa3000(rlpdlonPojo);
		}
		else if (isEQ(rlpdlonPojo.getFunction(), varcom.delt)){
			deltApa4000(rlpdlonPojo);
			rlpdlonPojo.setTranseq(wsaaSequenceNo);
		}
		else{
			rlpdlonPojo.setStatuz("FUNC");
		}
	}
	
	protected void deltApa4000(RlpdlonPojo rlpdlonPojo){
		updateLoanRec4010(rlpdlonPojo);
		apaInterest4020(rlpdlonPojo);
		apaPrincipal4030(rlpdlonPojo);
		updateSuspense4040(rlpdlonPojo);
	}
	protected void updateSuspense4040(RlpdlonPojo rlpdlonPojo){
		wsaaSequenceNo = Integer.parseInt(cashedPojo.getTranseq());
		if (isEQ(rlpdlonPojo.getPstw(), "NPSTW")) {
			return ;
		}
		cashedPojo.setChdrpfx(fsupfxcpy.chdr.toString());
		lifrtrnPojo.setBatccoy(chdrenq.getChdrcoy().toString());
		lifrtrnPojo.setRldgcoy(chdrenq.getChdrcoy().toString());
		lifrtrnPojo.setGenlcoy(chdrenq.getChdrcoy().toString());
		lifrtrnPojo.setBatckey(rlpdlonPojo.getBatchkey());
		lifrtrnPojo.setBatctrcde(rlpdlonPojo.getTrcde());
		lifrtrnPojo.setRcamt(0);
		lifrtrnPojo.setCrate(BigDecimal.ZERO);
		lifrtrnPojo.setAcctamt(BigDecimal.ZERO);
		lifrtrnPojo.setUser(0);
		lifrtrnPojo.setFrcdate(varcom.vrcmMaxDate.toInt());
		lifrtrnPojo.setTransactionTime(rlpdlonPojo.getTime());
		lifrtrnPojo.setTransactionDate(rlpdlonPojo.getDate_var());
		lifrtrnPojo.setTrandesc(rlpdlonPojo.getLongdesc());
		lifrtrnPojo.setOrigamt(rlpdlonPojo.getPrmdepst());
		lifrtrnPojo.setRdocnum(rlpdlonPojo.getDoctNumber());
		lifrtrnPojo.setRldgacct(chdrenq.getChdrnum());
		lifrtrnPojo.setTranref(chdrenq.getChdrnum());
		lifrtrnPojo.setJrnseq(wsaaSequenceNo + 1);
		lifrtrnPojo.setTranno(rlpdlonPojo.getTranno());
		lifrtrnPojo.setOrigcurr(rlpdlonPojo.getCurrency());
		lifrtrnPojo.setEffdate(rlpdlonPojo.getEffdate());
		String[] substituteCodes = new String[5];
	    substituteCodes[0] = chdrenq.getCnttype();
	    lifrtrnPojo.setSubstituteCodes(substituteCodes);
		lifrtrnPojo.setSacscode(t5645rec.sacscode07.toString());
		lifrtrnPojo.setSacstyp(t5645rec.sacstype07.toString());
		lifrtrnPojo.setGlsign(t5645rec.sign07.toString());
		lifrtrnPojo.setGlcode(t5645rec.glmap07.toString());
		lifrtrnPojo.setContot(t5645rec.cnttot07.toInt());
		lifrtrnPojo.setFunction("PSTW");
		lifrtrnUtils.calcLitrtrn(lifrtrnPojo);
		if (isNE(lifrtrnPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifrtrnPojo.getStatuz());
			syserrrec.params.set(lifrtrnPojo);
			dbError580(rlpdlonPojo,"2");
		}
	}
	
	protected void apaPrincipal4030 (RlpdlonPojo rlpdlonPojo){
		if (cashedPojo.getDocamt().compareTo(BigDecimal.ZERO) == 0) {
			return ;
		}
		wsaaGenlkey.set(SPACES);
		wsaaGlCompany.set(chdrenq.getChdrcoy());
		wsaaGlCurrency.set(SPACES);
		wsaaGlMap.set(t5645rec.glmap06);
		cashedPojo.setGenlkey(wsaaGenlkey.toString());
		cashedPojo.setSacscode(t5645rec.sacscode06.toString());
		cashedPojo.setSacstyp(t5645rec.sacstype06.toString());
		cashedPojo.setSign(t5645rec.sign06.toString());
		cashedPojo.setOrigccy(rlpdlonPojo.getCurrency());
		/*    MOVE CSHD-DOCAMT            TO CSHD-ORIGAMT.*/
		cashedPojo.setLanguage(rlpdlonPojo.getLanguage());
		cashedPojo.setOrigamt(cashedPojo.getDocamt());
		loanpymtUtils.callLoanpymt(cashedPojo);
		if (isNE(cashedPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cashedPojo.getStatuz());
			syserrrec.params.set(cashedPojo);
			dbError580(rlpdlonPojo,"1");
		}
	}
	
	protected void apaInterest4020(RlpdlonPojo rlpdlonPojo){
		wsaaGenlkey.set(SPACES);
		wsaaGlCompany.set(chdrenq.getChdrcoy());
		wsaaGlCurrency.set(SPACES);
		wsaaGlMap.set(t5645rec.glmap05);
		cashedPojo.setGenlkey(wsaaGenlkey.toString());
		cashedPojo.setSacscode(t5645rec.sacscode05.toString());
		cashedPojo.setSacstyp(t5645rec.sacstype05.toString());
		cashedPojo.setSign(t5645rec.sign05.toString());
		cashedPojo.setOrigccy(rlpdlonPojo.getCurrency());
		/*    MOVE CSHD-DOCAMT            TO CSHD-ORIGAMT.*/
		cashedPojo.setLanguage(rlpdlonPojo.getLanguage());
		cashedPojo.setOrigamt(rlpdlonPojo.getPrmdepst());
		loanpymtUtils.callLoanpymt(cashedPojo);
		if (isNE(cashedPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cashedPojo.getStatuz());
			syserrrec.params.set(cashedPojo);
			dbError580(rlpdlonPojo,"1");
		}
	}
	protected void updateLoanRec4010(RlpdlonPojo rlpdlonPojo){
		if (isEQ(t5645rec.sacscode05, SPACES)
				|| isEQ(t5645rec.sacstype05, SPACES)
				|| isEQ(t5645rec.sacscode06, SPACES)
				|| isEQ(t5645rec.sacstype06, SPACES)
				|| isEQ(t5645rec.sacscode07, SPACES)
				|| isEQ(t5645rec.sacstype07, SPACES)) {
			syserrrec.statuz.set("G450");
			dbError580(rlpdlonPojo,"1");
		}
		cashedPojo.setDocamt(BigDecimal.ZERO);
		cashedPojo.setContot(0);
		cashedPojo.setAcctamt(BigDecimal.ZERO);
		cashedPojo.setDissrate(0);
		cashedPojo.setFunction("RESID");
		cashedPojo.setDoctCompany(fsupfxcpy.chdr.toString());
		cashedPojo.setDoctNumber(chdrenq.getChdrnum());
		cashedPojo.setChdrpfx(fsupfxcpy.chdr.toString());
		cashedPojo.setChdrcoy(chdrenq.getChdrcoy().toString());
		cashedPojo.setChdrnum(chdrenq.getChdrnum());
		cashedPojo.setBatchkey(rlpdlonPojo.getBatchkey());
		cashedPojo.setDoctkey(rlpdlonPojo.getDoctkey());
		cashedPojo.setTrancode(rlpdlonPojo.getAuthCode());
		cashedPojo.setTranid(rlpdlonPojo.getTranid());
		cashedPojo.setTranno(String.valueOf(rlpdlonPojo.getTranno()));
		cashedPojo.setTranseq(String.valueOf(wsaaSequenceNo));
		cashedPojo.setTrandesc(rlpdlonPojo.getLongdesc());
		cashedPojo.setTrandate(String.valueOf(rlpdlonPojo.getEffdate()));
		wsaaTrankey.set(SPACES);
		wsaaTranPrefix.set(fsupfxcpy.chdr);
		wsaaTranCompany.set(chdrenq.getChdrcoy());
		wsaaTranEntity.set(chdrenq.getChdrnum());
		cashedPojo.setTrankey(wsaaTrankey.toString());
	}
	protected void validateRules3200(RlpdlonPojo rlpdlonPojo){
		wsaaAmount.setScale(2);
		wsaaAmount = payrpf.getSinstamt06().multiply(new BigDecimal(th623rec.noofinst.toString()));
		if (isEQ(th623rec.noofinst, ZERO)
				&& isNE(th623rec.prmdepst, ZERO)
				&& isLT(rlpdlonPojo.getPrmdepst(), th623rec.prmdepst)) {
			rlpdlonPojo.setProcessed("N");
		}else {
			if (isNE(th623rec.noofinst, ZERO)
					&& isEQ(th623rec.prmdepst, ZERO)
					&& isLT(rlpdlonPojo.getPrmdepst(), wsaaAmount)) {
				rlpdlonPojo.setProcessed("N");
			}else {
				if (isNE(th623rec.noofinst, ZERO)
						&& isNE(th623rec.prmdepst, ZERO)
						&& (isLT(rlpdlonPojo.getPrmdepst(), wsaaAmount)
								|| isLT(rlpdlonPojo.getPrmdepst(), th623rec.prmdepst))) {
					rlpdlonPojo.setProcessed("N");
				}
			}
		}
	}
	
	protected void initialize1000(RlpdlonPojo rlpdlonPojo){
		wsaaAmount = BigDecimal.ZERO;
		rlpdlonPojo.setProcessed("Y");
		wsaaSequenceNo = rlpdlonPojo.getTranseq();
		rlpdlonPojo.setStatuz("****");
		varcom.vrcmTranid.set(rlpdlonPojo.getTranid());
		wsaaEof = false;
		chdrenq = chdrpfDAO.getChdrenqRecord(rlpdlonPojo.getChdrcoy(),rlpdlonPojo.getChdrnum());
		if (null == chdrenq) {
			if (isEQ(rlpdlonPojo.getFunction(), varcom.info)) {
				return;
			}
		}
		itempfList = itempfDAO.getItdmByFrmdate(rlpdlonPojo.getChdrcoy(),"TH623",chdrenq.getCnttype(),rlpdlonPojo.getEffdate()); 
		if (itempfList.size() <1 || null == itempfList) {
			wsaaEof = true;
			return ;
		}else {
			th623rec.th623Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		payrpf = payrpfDAO.getpayrRecord(chdrenq.getChdrcoy().toString(),chdrenq.getChdrnum(),1);/* IJTI-1523 */
		if(payrpf == null){
			dbError580(rlpdlonPojo,"2");
		}
		itempfList = itempfDAO.getAllItemitem("IT", rlpdlonPojo.getChdrcoy(), "T5645", "RLPDLON");
		if(null != itempfList && itempfList.size() > 0){
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

	protected void dbError580(RlpdlonPojo rlpdlonPojo,String errtype){
		syserrrec.subrname.set("RLPDLON");
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(errtype);
		syserr.mainline(new Object[] { syserrrec.syserrRec });
		rlpdlonPojo.setStatuz(varcom.bomb.toString());
		LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
		throw new RuntimeException(new COBOLExitProgramException());
	}
	private ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}
	
	protected void checkApa2000(RlpdlonPojo rlpdlonPojo)
	{
		readAcbl2010(rlpdlonPojo);
		readPrincipalApa2020(rlpdlonPojo);
		readInterestAccruel2030(rlpdlonPojo);
	}
	
	protected void readAcbl2010(RlpdlonPojo rlpdlonPojo)
	{
		wsaaRldgacct.set(SPACES);
		rlpdlonPojo.setCurrency("");
		rlpdlonPojo.setPrmdepst(BigDecimal.ZERO);
		if (isEQ(t5645rec.sacscode02, SPACES)
				|| isEQ(t5645rec.sacstype02, SPACES)
				|| isEQ(t5645rec.sacscode05, SPACES)
				|| isEQ(t5645rec.sacstype05, SPACES)) {
			syserrrec.statuz.set("G450");
			dbError580(rlpdlonPojo,"1");
		}
	}

	protected void readPrincipalApa2020(RlpdlonPojo rlpdlonPojo)
	{

		wsaaRldgChdrnum.set(rlpdlonPojo.getChdrnum());
		wsaaRldgLoanno.set("00");
		wsaaT5645Sacscode.set(t5645rec.sacscode02);
		wsaaT5645Sacstyp.set(t5645rec.sacstype02);
		acblpfList = acblpfDAO.getAcblenqRecord(rlpdlonPojo.getChdrcoy(),wsaaRldgacct.toString(),t5645rec.sacscode02.toString(),t5645rec.sacstype02.toString());


		while (!(wsaaEof == true)) {
			processAcbls2100(rlpdlonPojo);
		}

	}

	protected void readInterestAccruel2030(RlpdlonPojo rlpdlonPojo)
	{

		acblpfList.clear();
		wsaaEof = false;
		wsaaRldgChdrnum.set(rlpdlonPojo.getChdrnum());
		wsaaRldgLoanno.set("00");
		wsaaT5645Sacscode.set(t5645rec.sacscode05);
		wsaaT5645Sacstyp.set(t5645rec.sacstype05);
		acblpfList = acblpfDAO.getAcblenqRecord(rlpdlonPojo.getChdrcoy(),wsaaRldgacct.toString(),t5645rec.sacscode05.toString(),t5645rec.sacstype05.toString());

		while (!(wsaaEof == true)) {
			processAcbls2100(rlpdlonPojo);
		}

	}

	protected void processAcbls2100(RlpdlonPojo rlpdlonPojo)
	{


		iterator = acblpfList.iterator();

		if(iterator.hasNext())
		{
			acblpf = iterator.next();
			wsaaRldgacct.set(acblpf.getRldgacct());	
		}
		else{
			wsaaEof = true;
			return;	
		}

		/* Check that the ACBL balance is not Zero*/
		if (isEQ(acblpf.getSacscurbal(), ZERO)) {
			if(iterator.hasNext())
				acblpf = iterator.next();
			else
			{wsaaEof = true;
			return ;
			}
		}
		/* Negate the Account balances if it is negative (for the*/
		/* case of Cash accounts).*/
		if (isLT(acblpf.getSacscurbal(), 0)) {
			setPrecision(acblpf.getSacscurbal(), 2);
			acblpf.setSacscurbal(mult(acblpf.getSacscurbal(), (-1)).getbigdata());
		}
		rlpdlonPojo.setCurrency(acblpf.getOrigcurr());
		compute(wsaaPremiumDeposit, 2).set(add(acblpf.getSacscurbal(),rlpdlonPojo.getPrmdepst()));
		rlpdlonPojo.setPrmdepst(wsaaPremiumDeposit.getbigdata());
		if(iterator.hasNext())
			acblpf = iterator.next();
		else
			wsaaEof = true;
	}
	
	protected void insrApa3000(RlpdlonPojo rlpdlonPojo)
	{
		validate3010(rlpdlonPojo);
	}

	protected void validate3010(RlpdlonPojo rlpdlonPojo)
	{
		if (isLT(rlpdlonPojo.getPrmdepst(), ZERO)) {
			rlpdlonPojo.setStatuz("IVDE");
			return ;
		}
		if (isEQ(rlpdlonPojo.getPrmdepst(), ZERO)) {
			return ;
		}
		rlpdlonPojo.setProcessed("Y");
		if (isNE(rlpdlonPojo.getValidate(), "N")) {
			/*    PERFORM 3100-READ-TH623                                   */
			validateRules3200(rlpdlonPojo);
			if (isEQ(rlpdlonPojo.getProcessed(), "N")) {
				return ;
			}
		}
		if (isEQ(t5645rec.sacscode01, SPACES)
				|| isEQ(t5645rec.sacstype01, SPACES)
				|| isEQ(t5645rec.sacscode02, SPACES)
				|| isEQ(t5645rec.sacstype02, SPACES)) {
			syserrrec.statuz.set("G450");
			dbError580(rlpdlonPojo,"1");
		}
		/* Set up linkage to CRTLOAN, which will write LOAN and ACMV record*/
		crtloanPojo.setFunction(rlpdlonPojo.getPstw());
		crtloanPojo.setChdrcoy(rlpdlonPojo.getChdrcoy());
		crtloanPojo.setChdrnum(rlpdlonPojo.getChdrnum());
		crtloanPojo.setCnttype(chdrenq.getCnttype());
		crtloanPojo.setOccdate(chdrenq.getOccdate());
		crtloanPojo.setLoantype("D");
		crtloanPojo.setCntcurr(rlpdlonPojo.getCurrency());
		crtloanPojo.setBillcurr(payrpf.getBillcurr());
		crtloanPojo.setTranno(rlpdlonPojo.getTranno());
		crtloanPojo.setLongdesc(rlpdlonPojo.getLongdesc());
		crtloanPojo.setEffdate(rlpdlonPojo.getEffdate());
		crtloanPojo.setAuthCode(rlpdlonPojo.getAuthCode());
		crtloanPojo.setLanguage(rlpdlonPojo.getLanguage());
		crtloanPojo.setBatchkey(rlpdlonPojo.getBatchkey());
		crtloanPojo.setOutstamt(rlpdlonPojo.getPrmdepst().intValue());
		crtloanPojo.setCbillamt(rlpdlonPojo.getPrmdepst().intValue());
		crtloanPojo.setSacscode01(t5645rec.sacscode01.toString());
		crtloanPojo.setSacstyp01(t5645rec.sacstype01.toString());
		crtloanPojo.setGlcode01(t5645rec.glmap01.toString());
		crtloanPojo.setGlsign01(t5645rec.sign01.toString());
		crtloanPojo.setSacscode02(t5645rec.sacscode02.toString());
		crtloanPojo.setSacstyp02(t5645rec.sacstype02.toString());
		crtloanPojo.setGlcode02(t5645rec.glmap02.toString());
		crtloanPojo.setGlsign02(t5645rec.sign02.toString());
		crtloanPojo.setSacscode03(t5645rec.sacscode03.toString());
		crtloanPojo.setSacstyp03(t5645rec.sacstype03.toString());
		crtloanPojo.setGlcode03(t5645rec.glmap03.toString());
		crtloanPojo.setGlsign03(t5645rec.sign03.toString());
		crtloanPojo.setSacscode04(t5645rec.sacscode04.toString());
		crtloanPojo.setSacstyp04(t5645rec.sacstype04.toString());
		crtloanPojo.setGlcode04(t5645rec.glmap04.toString());
		crtloanPojo.setGlsign04(t5645rec.sign04.toString());
		crtloanUtils.calCrtloan(crtloanPojo);
		if (isNE(crtloanPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(crtloanPojo.getStatuz());
			syserrrec.params.set(crtloanPojo);
			dbError580(rlpdlonPojo,"1");
		}
		rlpdlonPojo.setLoanno(crtloanPojo.getLoanno());
	}
}
