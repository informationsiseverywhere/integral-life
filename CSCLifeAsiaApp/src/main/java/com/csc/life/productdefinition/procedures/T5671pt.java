/*
 * File: T5671pt.java
 * Date: 30 August 2009 2:25:33
 * Author: Quipoz Limited
 * 
 * Class transformed from T5671PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5671.
*
*
*****************************************************************
* </pre>
*/
public class T5671pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5671rec t5671rec = new T5671rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5671pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5671rec.t5671Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(t5671rec.pgm01);
		generalCopyLinesInner.fieldNo006.set(t5671rec.pgm02);
		generalCopyLinesInner.fieldNo007.set(t5671rec.pgm03);
		generalCopyLinesInner.fieldNo008.set(t5671rec.pgm04);
		generalCopyLinesInner.fieldNo009.set(t5671rec.subprog01);
		generalCopyLinesInner.fieldNo010.set(t5671rec.subprog02);
		generalCopyLinesInner.fieldNo011.set(t5671rec.subprog03);
		generalCopyLinesInner.fieldNo012.set(t5671rec.subprog04);
		generalCopyLinesInner.fieldNo013.set(t5671rec.edtitm01);
		generalCopyLinesInner.fieldNo014.set(t5671rec.edtitm02);
		generalCopyLinesInner.fieldNo015.set(t5671rec.edtitm03);
		generalCopyLinesInner.fieldNo016.set(t5671rec.edtitm04);
		generalCopyLinesInner.fieldNo017.set(t5671rec.trevsub01);
		generalCopyLinesInner.fieldNo018.set(t5671rec.trevsub02);
		generalCopyLinesInner.fieldNo019.set(t5671rec.trevsub03);
		generalCopyLinesInner.fieldNo020.set(t5671rec.trevsub04);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Generic Component Processing Control           S5671");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(73);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 11, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 25, FILLER).init("   Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler6 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 43);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(67);
	private FixedLengthStringData filler7 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine003, 30, FILLER).init("1           2           3           4");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(69);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 5, FILLER).init("Program:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 28);
	private FixedLengthStringData filler11 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 40);
	private FixedLengthStringData filler12 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 52);
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 64);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler14 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine005, 5, FILLER).init("Subroutine:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 28);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 40);
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 52);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 64);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(74);
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine007, 5, FILLER).init("Reversal Subroutine:");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 28);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 40);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 52);
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 64);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(69);
	private FixedLengthStringData filler24 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler25 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine006, 5, FILLER).init("Validation item key:");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 28);
	private FixedLengthStringData filler26 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 40);
	private FixedLengthStringData filler27 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 52);
	private FixedLengthStringData filler28 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 64);
}
}
