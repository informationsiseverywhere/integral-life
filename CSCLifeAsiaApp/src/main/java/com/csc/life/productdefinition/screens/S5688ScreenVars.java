package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5688
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class S5688ScreenVars extends SmartVarModel { 

	
	//ILIFE-1137 starts
//	public FixedLengthStringData dataArea = new FixedLengthStringData(554);
//	public FixedLengthStringData dataFields = new FixedLengthStringData(106).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataArea = new FixedLengthStringData(597);
	public FixedLengthStringData dataFields = new FixedLengthStringData(117).isAPartOf(dataArea, 0);
	//ILIFE-1137 ends
	public FixedLengthStringData anbrqd = DD.anbrqd.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankcode = DD.bankcode.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData cfiLim = DD.cfilim.copyToZonedDecimal().isAPartOf(dataFields,3);
	public FixedLengthStringData comlvlacc = DD.comlvlacc.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData defFupMeth = DD.deffup.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData feemeth = DD.feemeth.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,16);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,24);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,32);
	public ZonedDecimalData jlifemax = DD.jlifemax.copyToZonedDecimal().isAPartOf(dataFields,40);
	public ZonedDecimalData jlifemin = DD.jlifemin.copyToZonedDecimal().isAPartOf(dataFields,42);
	public ZonedDecimalData lifemax = DD.lifemax.copyToZonedDecimal().isAPartOf(dataFields,44);
	public ZonedDecimalData lifemin = DD.lifemin.copyToZonedDecimal().isAPartOf(dataFields,46);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData minDepMth = DD.mindepmth.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData nbusallw = DD.nbusallw.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData occrqd = DD.occrqd.copy().isAPartOf(dataFields,83);
	public ZonedDecimalData poldef = DD.poldef.copyToZonedDecimal().isAPartOf(dataFields,84);
	public ZonedDecimalData polmax = DD.polmax.copyToZonedDecimal().isAPartOf(dataFields,86);
	public ZonedDecimalData polmin = DD.polmin.copyToZonedDecimal().isAPartOf(dataFields,88);
	public FixedLengthStringData revacc = DD.revacc.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData smkrqd = DD.smkrqd.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData taxrelmth = DD.taxrelmth.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData zcertall = DD.zcertall.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData zcfidtall = DD.zcfidtall.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData znfopt = DD.znfopt.copy().isAPartOf(dataFields,103);
	//ILIFE-1137 starts
	public FixedLengthStringData dflexmeth = DD.calcprog.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData bfreqallw = DD.bfreqallw.copy().isAPartOf(dataFields,116);
//	public FixedLengthStringData errorIndicators = new FixedLengthStringData(112).isAPartOf(dataArea, 106);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 117);
	//ILIFE-1137 ends
	public FixedLengthStringData anbrqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cfilimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData comlvlaccErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData deffupErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData feemethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jlifemaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jlifeminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifemaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifeminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mindepmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData nbusallwErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData occrqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData poldefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData polmaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData polminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData revaccErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData smkrqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData taxrelmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData zcertallErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData zcfidtallErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData znfoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	//ILIFE-1137 starts
	public FixedLengthStringData dflexmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData bfreqallwErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
//	public FixedLengthStringData outputIndicators = new FixedLengthStringData(336).isAPartOf(dataArea, 218);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 237);
	//ILIFE-1137 ends
	public FixedLengthStringData[] anbrqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cfilimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] comlvlaccOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] deffupOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] feemethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jlifemaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jlifeminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifemaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifeminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mindepmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] nbusallwOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] occrqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] poldefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] polmaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] polminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] revaccOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] smkrqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] taxrelmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] zcertallOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] zcfidtallOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] znfoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	//ILIFE-1137
	public FixedLengthStringData[]  dflexmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[]  bfreqallwOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5688screenWritten = new LongData(0);
	public LongData S5688protectWritten = new LongData(0);
	public FixedLengthStringData csbil003flag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return false;
	}


	public S5688ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(polminOut,new String[] {"76",null, "-76",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(polmaxOut,new String[] {"77",null, "-77",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poldefOut,new String[] {"78",null, "-78",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeminOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifemaxOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeminOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifemaxOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feemethOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(deffupOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mindepmthOut,new String[] {"84",null, "-84",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cfilimOut,new String[] {"85",null, "-85",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nbusallwOut,new String[] {"80",null, "-80",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(znfoptOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankcodeOut,new String[] {"75",null, "-75",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(anbrqdOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comlvlaccOut,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(smkrqdOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(revaccOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occrqdOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		//ILIFE-1137 starts
		fieldIndMap.put(dflexmethOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, polmin, polmax, poldef, lifemin, lifemax, jlifemin, jlifemax, feemeth, defFupMeth, minDepMth, taxrelmth, cfiLim, nbusallw, znfopt, bankcode, anbrqd, comlvlacc, smkrqd, revacc, occrqd, zcfidtall, zcertall, dflexmeth,bfreqallw};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, polminOut, polmaxOut, poldefOut, lifeminOut, lifemaxOut, jlifeminOut, jlifemaxOut, feemethOut, deffupOut, mindepmthOut, taxrelmthOut, cfilimOut, nbusallwOut, znfoptOut, bankcodeOut, anbrqdOut, comlvlaccOut, smkrqdOut, revaccOut, occrqdOut, zcfidtallOut, zcertallOut, dflexmethOut,bfreqallwOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, polminErr, polmaxErr, poldefErr, lifeminErr, lifemaxErr, jlifeminErr, jlifemaxErr, feemethErr, deffupErr, mindepmthErr, taxrelmthErr, cfilimErr, nbusallwErr, znfoptErr, bankcodeErr, anbrqdErr, comlvlaccErr, smkrqdErr, revaccErr, occrqdErr, zcfidtallErr, zcertallErr, dflexmethErr, bfreqallwErr};
		//ILIFE-1137 ends
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5688screen.class;
		protectRecord = S5688protect.class;
	}

}
