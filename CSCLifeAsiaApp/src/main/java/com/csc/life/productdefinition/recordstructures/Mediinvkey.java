package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:42
 * Description:
 * Copybook name: MEDIINVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mediinvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mediinvFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mediinvKey = new FixedLengthStringData(256).isAPartOf(mediinvFileKey, 0, REDEFINE);
  	public FixedLengthStringData mediinvChdrcoy = new FixedLengthStringData(1).isAPartOf(mediinvKey, 0);
  	public FixedLengthStringData mediinvChdrnum = new FixedLengthStringData(8).isAPartOf(mediinvKey, 1);
  	public FixedLengthStringData mediinvInvref = new FixedLengthStringData(15).isAPartOf(mediinvKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(232).isAPartOf(mediinvKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mediinvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mediinvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}