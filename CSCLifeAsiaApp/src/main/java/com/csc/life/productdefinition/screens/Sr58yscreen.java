package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 08/17/16 
 * @author CSC 
 */
public class Sr58yscreen extends ScreenRecord { 
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}
	
/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */	
	
	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sr58yScreenVars sv = (Sr58yScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sr58yscreenWritten, null, av, null, ind2, ind3);
		}
	public static void read(Indicator ind2, Indicator ind3) {}
	public static void clearClassString(VarModel pv) {
		Sr58yScreenVars screenVars = (Sr58yScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		
		screenVars.unitVirtualFund01.setClassString("");
		screenVars.unitVirtualFund02.setClassString("");
		screenVars.unitVirtualFund03.setClassString("");
		screenVars.unitVirtualFund04.setClassString("");
		screenVars.unitVirtualFund05.setClassString("");
		screenVars.unitVirtualFund06.setClassString("");
		screenVars.unitVirtualFund07.setClassString("");
		screenVars.unitVirtualFund08.setClassString("");
		screenVars.unitVirtualFund09.setClassString("");
		screenVars.unitVirtualFund10.setClassString("");
		screenVars.unitVirtualFund11.setClassString("");
		screenVars.unitVirtualFund12.setClassString("");
		screenVars.unitVirtualFund13.setClassString("");
		screenVars.unitVirtualFund14.setClassString("");
		screenVars.unitVirtualFund15.setClassString("");
		screenVars.unitVirtualFund16.setClassString("");
		screenVars.unitVirtualFund17.setClassString("");
		screenVars.unitVirtualFund18.setClassString("");
		screenVars.unitVirtualFund19.setClassString("");
		screenVars.unitVirtualFund20.setClassString("");
		screenVars.unitVirtualFund21.setClassString("");
		screenVars.unitVirtualFund22.setClassString("");
		screenVars.unitVirtualFund23.setClassString("");
		screenVars.unitVirtualFund24.setClassString("");
		screenVars.unitVirtualFund25.setClassString("");
		screenVars.unitVirtualFund26.setClassString("");
		screenVars.unitVirtualFund27.setClassString("");
		screenVars.unitVirtualFund28.setClassString("");
		screenVars.unitVirtualFund29.setClassString("");
		screenVars.unitVirtualFund30.setClassString("");
		screenVars.unitVirtualFund31.setClassString("");
		screenVars.unitVirtualFund32.setClassString("");
		screenVars.unitVirtualFund33.setClassString("");
		screenVars.unitVirtualFund34.setClassString("");
		screenVars.unitVirtualFund35.setClassString("");
		screenVars.unitVirtualFund36.setClassString("");
		screenVars.unitVirtualFund37.setClassString("");
		screenVars.unitVirtualFund38.setClassString("");
		screenVars.unitVirtualFund39.setClassString("");
		screenVars.unitVirtualFund40.setClassString("");
		screenVars.unitVirtualFund41.setClassString("");
		screenVars.unitVirtualFund42.setClassString("");
		screenVars.unitVirtualFund43.setClassString("");
		screenVars.unitVirtualFund44.setClassString("");
		screenVars.unitVirtualFund45.setClassString("");

		screenVars.unitPremPercent01.setClassString("");
		screenVars.unitPremPercent02.setClassString("");
		screenVars.unitPremPercent03.setClassString("");
		screenVars.unitPremPercent04.setClassString("");
		screenVars.unitPremPercent05.setClassString("");
		screenVars.unitPremPercent06.setClassString("");
		screenVars.unitPremPercent07.setClassString("");
		screenVars.unitPremPercent08.setClassString("");
		screenVars.unitPremPercent09.setClassString("");
		screenVars.unitPremPercent10.setClassString("");
		screenVars.unitPremPercent11.setClassString("");
		screenVars.unitPremPercent12.setClassString("");
		screenVars.unitPremPercent13.setClassString("");
		screenVars.unitPremPercent14.setClassString("");
		screenVars.unitPremPercent15.setClassString("");
		screenVars.unitPremPercent16.setClassString("");
		screenVars.unitPremPercent17.setClassString("");
		screenVars.unitPremPercent18.setClassString("");
		screenVars.unitPremPercent19.setClassString("");
		screenVars.unitPremPercent20.setClassString("");
		screenVars.unitPremPercent21.setClassString("");
		screenVars.unitPremPercent22.setClassString("");
		screenVars.unitPremPercent23.setClassString("");
		screenVars.unitPremPercent24.setClassString("");
		screenVars.unitPremPercent25.setClassString("");
		screenVars.unitPremPercent26.setClassString("");
		screenVars.unitPremPercent27.setClassString("");
		screenVars.unitPremPercent28.setClassString("");
		screenVars.unitPremPercent29.setClassString("");
		screenVars.unitPremPercent30.setClassString("");
		screenVars.unitPremPercent31.setClassString("");
		screenVars.unitPremPercent32.setClassString("");
		screenVars.unitPremPercent33.setClassString("");
		screenVars.unitPremPercent34.setClassString("");
		screenVars.unitPremPercent35.setClassString("");
		screenVars.unitPremPercent36.setClassString("");
		screenVars.unitPremPercent37.setClassString("");
		screenVars.unitPremPercent38.setClassString("");
		screenVars.unitPremPercent39.setClassString("");
		screenVars.unitPremPercent40.setClassString("");
		screenVars.unitPremPercent41.setClassString("");
		screenVars.unitPremPercent42.setClassString("");
		screenVars.unitPremPercent43.setClassString("");
		screenVars.unitPremPercent44.setClassString("");
		screenVars.unitPremPercent45.setClassString("");

		screenVars.ageIssageFrm01.setClassString("");
		screenVars.ageIssageFrm02.setClassString("");
		screenVars.ageIssageFrm03.setClassString("");
		screenVars.ageIssageFrm04.setClassString("");
		screenVars.ageIssageFrm05.setClassString("");
		screenVars.ageIssageFrm06.setClassString("");
		screenVars.ageIssageFrm07.setClassString("");
		screenVars.ageIssageFrm08.setClassString("");
		screenVars.ageIssageFrm09.setClassString("");
		
		screenVars.ageIssageTo01.setClassString("");
		screenVars.ageIssageTo02.setClassString("");
		screenVars.ageIssageTo03.setClassString("");
		screenVars.ageIssageTo04.setClassString("");
		screenVars.ageIssageTo05.setClassString("");
		screenVars.ageIssageTo06.setClassString("");
		screenVars.ageIssageTo07.setClassString("");
		screenVars.ageIssageTo08.setClassString("");
		screenVars.ageIssageTo09.setClassString("");
	}

	public static void clear(VarModel pv) {
		Sr58yScreenVars screenVars = (Sr58yScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		/*screenVars.itmfrm.clear();*/
		screenVars.itmtoDisp.clear();
		/*screenVars.itmto.clear();*/
		
		screenVars.ageIssageFrm01.clear();
		screenVars.ageIssageFrm02.clear();
		screenVars.ageIssageFrm03.clear();
		screenVars.ageIssageFrm04.clear();
		screenVars.ageIssageFrm05.clear();
		screenVars.ageIssageFrm06.clear();
		screenVars.ageIssageFrm07.clear();
		screenVars.ageIssageFrm08.clear();
		screenVars.ageIssageFrm09.clear();
		
		screenVars.ageIssageTo01.clear();
		screenVars.ageIssageTo02.clear();
		screenVars.ageIssageTo03.clear();
		screenVars.ageIssageTo04.clear();
		screenVars.ageIssageTo05.clear();
		screenVars.ageIssageTo06.clear();
		screenVars.ageIssageTo07.clear();
		screenVars.ageIssageTo08.clear();
		screenVars.ageIssageTo09.clear();
		
		screenVars.unitVirtualFund01.clear();
		screenVars.unitVirtualFund02.clear();
		screenVars.unitVirtualFund03.clear();
		screenVars.unitVirtualFund04.clear();
		screenVars.unitVirtualFund05.clear();
		screenVars.unitVirtualFund06.clear();
		screenVars.unitVirtualFund07.clear();
		screenVars.unitVirtualFund08.clear();
		screenVars.unitVirtualFund09.clear();
		screenVars.unitVirtualFund10.clear();
		screenVars.unitVirtualFund11.clear();
		screenVars.unitVirtualFund12.clear();
		screenVars.unitVirtualFund13.clear();
		screenVars.unitVirtualFund14.clear();
		screenVars.unitVirtualFund15.clear();
		screenVars.unitVirtualFund16.clear();
		screenVars.unitVirtualFund17.clear();
		screenVars.unitVirtualFund18.clear();
		screenVars.unitVirtualFund19.clear();
		screenVars.unitVirtualFund20.clear();
		screenVars.unitVirtualFund21.clear();
		screenVars.unitVirtualFund22.clear();
		screenVars.unitVirtualFund23.clear();
		screenVars.unitVirtualFund24.clear();
		screenVars.unitVirtualFund25.clear();
		screenVars.unitVirtualFund26.clear();
		screenVars.unitVirtualFund27.clear();
		screenVars.unitVirtualFund28.clear();
		screenVars.unitVirtualFund29.clear();
		screenVars.unitVirtualFund30.clear();
		screenVars.unitVirtualFund31.clear();
		screenVars.unitVirtualFund32.clear();
		screenVars.unitVirtualFund33.clear();
		screenVars.unitVirtualFund34.clear();
		screenVars.unitVirtualFund35.clear();
		screenVars.unitVirtualFund36.clear();
		screenVars.unitVirtualFund37.clear();
		screenVars.unitVirtualFund38.clear();
		screenVars.unitVirtualFund39.clear();
		screenVars.unitVirtualFund40.clear();
		screenVars.unitVirtualFund41.clear();
		screenVars.unitVirtualFund42.clear();
		screenVars.unitVirtualFund43.clear();
		screenVars.unitVirtualFund44.clear();
		screenVars.unitVirtualFund45.clear();

		screenVars.unitPremPercent01.clear();
		screenVars.unitPremPercent02.clear();
		screenVars.unitPremPercent03.clear();
		screenVars.unitPremPercent04.clear();
		screenVars.unitPremPercent05.clear();
		screenVars.unitPremPercent06.clear();
		screenVars.unitPremPercent07.clear();
		screenVars.unitPremPercent08.clear();
		screenVars.unitPremPercent09.clear();
		screenVars.unitPremPercent10.clear();
		screenVars.unitPremPercent11.clear();
		screenVars.unitPremPercent12.clear();
		screenVars.unitPremPercent13.clear();
		screenVars.unitPremPercent14.clear();
		screenVars.unitPremPercent15.clear();
		screenVars.unitPremPercent16.clear();
		screenVars.unitPremPercent17.clear();
		screenVars.unitPremPercent18.clear();
		screenVars.unitPremPercent19.clear();
		screenVars.unitPremPercent20.clear();
		screenVars.unitPremPercent21.clear();
		screenVars.unitPremPercent22.clear();
		screenVars.unitPremPercent23.clear();
		screenVars.unitPremPercent24.clear();
		screenVars.unitPremPercent25.clear();
		screenVars.unitPremPercent26.clear();
		screenVars.unitPremPercent27.clear();
		screenVars.unitPremPercent28.clear();
		screenVars.unitPremPercent29.clear();
		screenVars.unitPremPercent30.clear();
		screenVars.unitPremPercent31.clear();
		screenVars.unitPremPercent32.clear();
		screenVars.unitPremPercent33.clear();
		screenVars.unitPremPercent34.clear();
		screenVars.unitPremPercent35.clear();
		screenVars.unitPremPercent36.clear();
		screenVars.unitPremPercent37.clear();
		screenVars.unitPremPercent38.clear();
		screenVars.unitPremPercent39.clear();
		screenVars.unitPremPercent40.clear();
		screenVars.unitPremPercent41.clear();
		screenVars.unitPremPercent42.clear();
		screenVars.unitPremPercent43.clear();
		screenVars.unitPremPercent44.clear();
		screenVars.unitPremPercent45.clear();
	}
}