package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sr59xscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 4, 22, 17, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr59xScreenVars sv = (Sr59xScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr59xscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr59xScreenVars screenVars = (Sr59xScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.savsrisk.setClassString("");
		screenVars.otherscheallowed.setClassString("");
		screenVars.defaultScheme.setClassString("");
		screenVars.dfcontrn.setClassString(""); //*ILIFE-3693 -nnaveenkumar 
		//BRD-NBP-014 ends
		screenVars.recoveryType.setClassString("");
		screenVars.cocontpay.setClassString("");
		screenVars.liscpay.setClassString("");
		screenVars.polfee.setClassString("");
	}

/**
 * Clear all the variables in Sr59xscreen
 */
	public static void clear(VarModel pv) {
		Sr59xScreenVars screenVars = (Sr59xScreenVars)pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.savsrisk.clear();;
		screenVars.otherscheallowed.clear();
		screenVars.defaultScheme.clear();
		screenVars.dfcontrn.clear(); //*ILIFE-3693 -nnaveenkumar 
		//BRD-NBP-014 ends
		screenVars.recoveryType.clear();
		screenVars.cocontpay.clear();
		screenVars.liscpay.clear();
		screenVars.polfee.clear();
	}
}
