
/*
 * File: Pr674.java
 * Date: 30 August 2009 1:52:17
 * Author: Quipoz Limited
 *
 * Class transformed from PR674.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import static com.quipoz.COBOLFramework.util.CLFunctions.queryUserJobInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.diary.procedures.Dryproces;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Bchgallrec;
import com.csc.life.contractservicing.recordstructures.Covrmjakey;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.dataaccess.AnrlcntTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
//ILIFE-5131
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.screens.Sr674ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.life.productdefinition.tablestructures.Th611rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.IncrhstTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.recordstructures.Pr676cpy;
import com.csc.life.terminationclaims.tablestructures.Tr686rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;



/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Billing Change.
* ===============
*
* This screen is for billing change quotation/processing. First,
* program will compute all outstanding APL loan, APL loan
* interest, Policy loan, Policy loan interest, o/s premium &
* premium suspense amount. In sub-file portion, program will
* load from COVR (Coverage/Rider details file), calculate
* installment premium for yearly, half-yearly, quarterly &
* monthly, giving gross premium, fee & service tax, discount &
* total premium.
* Screen also with Option switching facility to allow user to
* enter Payor, Direct Debit, Group & Anniversary details.
* (pop-up screens). Printing facility to print quotation letter.
*
*
***********************************************************************
* </pre>
*/
public class Pr674 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR674");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData ix = new PackedDecimalData(5, 0);
	protected PackedDecimalData iy = new PackedDecimalData(5, 0);
	private PackedDecimalData iz = new PackedDecimalData(5, 0);
	protected PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaLastMop = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLastBillfreq = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaOrigBillfreq = new FixedLengthStringData(2);
	private BinaryData wsaaHbnfZunit = new BinaryData(3, 0);

	private FixedLengthStringData wsaaOrigBillfreqx = new FixedLengthStringData(2).isAPartOf(wsaaOrigBillfreq, 0, REDEFINE);
	private ZonedDecimalData wsaaOrigBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaOrigBillfreqx, 0).setUnsigned();
	protected FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaBillfreqx = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, REDEFINE);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqx, 0).setUnsigned();
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
	private ZonedDecimalData wsaaPtdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaPtdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPtdateYy = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaLastAnndate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaLastAnndate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaLastAnndateYy = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaLastAnndateMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
	private ZonedDecimalData wsaaLastAnndateDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();
	private FixedLengthStringData wsaaAiindInd = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaCtables = new FixedLengthStringData(2000);
	private FixedLengthStringData[] wsaaCtable = FLSArrayPartOfStructure(500, 4, wsaaCtables, 0);
	private static final int wsaaZldcMax = 100;

	private FixedLengthStringData wsaaCompPrems = new FixedLengthStringData(7200);
	private FixedLengthStringData[] wsaaCompPrem = FLSArrayPartOfStructure(100, 72, wsaaCompPrems, 0);
	private PackedDecimalData[] wsaaCompBprem01 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 0);
	private PackedDecimalData[] wsaaCompLprem01 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 9);
	private PackedDecimalData[] wsaaCompBprem02 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 18);
	private PackedDecimalData[] wsaaCompLprem02 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 27);
	private PackedDecimalData[] wsaaCompBprem03 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 36);
	private PackedDecimalData[] wsaaCompLprem03 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 45);
	private PackedDecimalData[] wsaaCompBprem04 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 54);
	private PackedDecimalData[] wsaaCompLprem04 = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 63);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaTableToRead = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaItemToRead = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPremiumMethod = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");

	private FixedLengthStringData wsaaRegularUnitLink = new FixedLengthStringData(1);
	private Validator regularUnitLink = new Validator(wsaaRegularUnitLink, "Y");

	private FixedLengthStringData wsaaJointLife = new FixedLengthStringData(1);
	private Validator jointLife = new Validator(wsaaJointLife, "Y");

	private FixedLengthStringData wsaaRerateComp = new FixedLengthStringData(1);
	private Validator rerateComp = new Validator(wsaaRerateComp, "Y");

	private FixedLengthStringData wsaaFreq01Allow = new FixedLengthStringData(1);
	protected Validator freq01Allow = new Validator(wsaaFreq01Allow, "Y");
	private Validator freq01NotAllow = new Validator(wsaaFreq01Allow, "N");

	private FixedLengthStringData wsaaFreq02Allow = new FixedLengthStringData(1);
	protected Validator freq02Allow = new Validator(wsaaFreq02Allow, "Y");
	private Validator freq02NotAllow = new Validator(wsaaFreq02Allow, "N");

	private FixedLengthStringData wsaaFreq04Allow = new FixedLengthStringData(1);
	protected Validator freq04Allow = new Validator(wsaaFreq04Allow, "Y");
	private Validator freq04NotAllow = new Validator(wsaaFreq04Allow, "N");

	private FixedLengthStringData wsaaFreq12Allow = new FixedLengthStringData(1);
	protected Validator freq12Allow = new Validator(wsaaFreq12Allow, "Y");
	private Validator freq12NotAllow = new Validator(wsaaFreq12Allow, "N");
	
	private FixedLengthStringData wsaaFreq26Allow = new FixedLengthStringData(1);
	protected Validator freq26Allow = new Validator(wsaaFreq26Allow, "Y");
	private Validator freq26NotAllow = new Validator(wsaaFreq26Allow, "N");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator statusValid = new Validator(wsaaValidStatus, "Y");
	private Validator statusNotValid = new Validator(wsaaValidStatus, "N");

	protected FixedLengthStringData wsaaSbmaction = new FixedLengthStringData(1);
	private Validator billingChange = new Validator(wsaaSbmaction, "A");
	protected Validator quotation = new Validator(wsaaSbmaction, "B");

	private FixedLengthStringData wsaaWaiveIt = new FixedLengthStringData(1);
	private Validator waiveIt = new Validator(wsaaWaiveIt, "Y");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaIncrFound = new FixedLengthStringData(1);
	protected Validator incrFound = new Validator(wsaaIncrFound, "Y");
	private String wsaaValidBillfreqMop = "";
	private PackedDecimalData wsaaLastRerateDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaZrwvflg01 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg02 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg03 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg04 = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAnnualPremium = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAnnualSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWopCompRrn = new PackedDecimalData(3, 0);
	protected PackedDecimalData wsaaContFee01 = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaContFee02 = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaContFee03 = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaContFee04 = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaContFee05 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWaiverSumins01 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWaiverSumins02 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWaiverSumins03 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWaiverSumins04 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWaiverSumins05 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaJlDob = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaJlAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaJlSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaMlDob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaMlSex = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaNewGrupcoy = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaNewGrupnum = new FixedLengthStringData(12);
	protected FixedLengthStringData wsaaNewPayrnum = new FixedLengthStringData(8);
	protected ZonedDecimalData wsaaNewIncomeSeqno = new ZonedDecimalData(2, 0).setUnsigned();
	protected FixedLengthStringData wsaaNewMandref = new FixedLengthStringData(5);
	protected FixedLengthStringData wsaaNewMembsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaNewBillnet = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTaxPrem01 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxPrem02 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxPrem03 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxPrem04 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxPrem05 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxCntf01 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxCntf02 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxCntf03 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxCntf04 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxCntf05 = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 0);
	protected ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOkeys, 1).setUnsigned();
	private FixedLengthStringData wsaaRqstBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 6);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private BinaryData wsjobVarlen = new BinaryData(9, 0).init(101);
	private FixedLengthStringData wsjobFormat = new FixedLengthStringData(8).init("JOBI0600");
	private FixedLengthStringData wsjobQualjobname = new FixedLengthStringData(26).init("*");
	private FixedLengthStringData wsjobIntjobi = new FixedLengthStringData(26).init(SPACES);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);

	protected FixedLengthStringData wsaaChnlChk = new FixedLengthStringData(1);
	private Validator creditCard = new Validator(wsaaChnlChk, "R");
	private Validator directDebit = new Validator(wsaaChnlChk, "D");
	private ZonedDecimalData wsaaSeqnoN = new ZonedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaContract = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaSeqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private static final String wsaaPayer = "PY";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	//ILIFE-3378 System Error: Billing Changes
	private FixedLengthStringData wsaaPremMeth= new FixedLengthStringData(4);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private AnrlcntTableDAM anrlcntIO = new AnrlcntTableDAM();
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	private IncrhstTableDAM incrhstIO = new IncrhstTableDAM();
	private IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected Batckey wsaaBatchkey = new Batckey();
	protected Covrmjakey wsaaCovrmjakey = new Covrmjakey();
	private Covrmjakey wsaaWopCovrmjakey = new Covrmjakey();
	private Covrmjakey wsaaMainCovrmjakey = new Covrmjakey();
	protected Clntkey wsaaClntkey = new Clntkey();
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Th611rec th611rec = new Th611rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5675rec t5675rec = new T5675rec();
	protected T5688rec t5688rec = new T5688rec();
	protected Tr386rec tr386rec = new Tr386rec();
	protected T5674rec t5674rec = new T5674rec();
	protected T3620rec t3620rec = new T3620rec();
	private T5671rec t5671rec = new T5671rec();
	private T5689rec t5689rec = new T5689rec();
	private T7508rec t7508rec = new T7508rec();
	private Tr686rec tr686rec = new Tr686rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	protected Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Bchgallrec bchgallrec = new Bchgallrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Pr676cpy pr676cpy = new Pr676cpy();
	protected Gensswrec gensswrec = new Gensswrec();
	private Totloanrec totloanrec = new Totloanrec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Sr674ScreenVars sv =getLScreenVars();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	protected ErrorsInner errorsInner = new ErrorsInner();
	protected FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsjobDataInner wsjobDataInner = new WsjobDataInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	protected boolean isbillday=false;
	//ILIFE-5131
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	/*ICIL-298 starts*/
	protected FixedLengthStringData wsaaFreqErr= new FixedLengthStringData(1);
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	/*ICIL-298 ends*/
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	boolean csbil003Permission = false;
	private FixedLengthStringData prevfreq = new FixedLengthStringData(2);
	private int i = 1;
	/*ILIFE-7845 - Starts*/
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";
	private boolean riskPremflag = false;
	/*ILIFE-7845 - Ends*/
	/*ILIFE-8027 - Starts*/
	private static final String  BILLDAY_FEATURE_ID="NBPRP011";
	private boolean billDayflag = false;
	/*ILIFE-8027 - Ends*/
	private boolean stampDutyflag = false;	
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private boolean dialdownFlag = false;
	private PackedDecimalData wsaaZstpduty01 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZstpduty02 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZstpduty03 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZstpduty04 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZstpduty05 = new PackedDecimalData(17, 2);
	
	private List<BigDecimal> zstpduty01List;
	private List<BigDecimal> zstpduty02List;
	private List<BigDecimal> zstpduty03List;
	private List<BigDecimal> zstpduty04List;
	private List<BigDecimal> zstpduty05List;
	private Iterator<BigDecimal> zstpduty01;
	private Iterator<BigDecimal> zstpduty02;
	private Iterator<BigDecimal> zstpduty03;
	private Iterator<BigDecimal> zstpduty04;
	private Iterator<BigDecimal> zstpduty05;
	private List<String> zclstate01List;
	private List<String> zclstate02List;
	private List<String> zclstate03List;
	private List<String> zclstate04List;
	private List<String> zclstate05List;
	private Iterator<String> zclstate01;
	private Iterator<String> zclstate02;
	private Iterator<String> zclstate03;
	private Iterator<String> zclstate04;
	private Iterator<String> zclstate05;
	/*ILIFE-8248 start*/
  	private boolean lnkgFlag = false;
	/*ILIFE-8248 end*/
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;//ILIFE-8502
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;	//ILIFE-8502
	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);	
	private BextpfDAO bextpfDAO = getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	protected Mrtapf mrtaIO;
	protected Hpadpf hpadIO;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private boolean billChgFlag = false;
	//ILJ-49 End
	
	private String wsaaMaxdday;
	private String wsaaNewNextdate;
	private String payrBillday;
	private String changedBillday;
	private boolean prorateFlag;
	private boolean rerateChgFlag;
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ZonedDecimalData wsaaRerateDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	private static final String BTPRO033 = "BTPRO033";
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private static final String t5675 = "T5675";
	private boolean BTPRO033Permission  = false;
	private boolean proratPremCalcFlag;
	private boolean isProcesFreqChange;
	private static final String IT = "IT";
	private boolean isFirstCovr = true;
	List<Rertpf> updatedList = new ArrayList<Rertpf>();
	List<Rertpf> insertedList = new ArrayList<Rertpf>();
	Rertpf insertedRertpf;
	private Freqcpy freqcpy = new Freqcpy();
	//PINNACLE-2676
	private BigDecimal covStampDuty;
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1a90,
		exit2090,
		updateDatabase3040,
		exit3090,
		tr384Found3130,
		exit3190,
		readIncr3220,
		exit3290,
		readCovr3320,
		exit3390,
		readSfl3602,
		exit3609,
		readAgcm3632,
		nextAgcm3638,
		exit3639,
		exit5190,
		noMoreTr5175280,
		exit5290,
		exit5390,
		exit6090,
		readSfl7120,
		nextSfl7180,
		exit7190,
		exit7290,
		supdForWopCoverage7370,
		readSfl8020,
		totalDue8080,
		exit8290,
		exit8390
	}
	
	

	public Pr674() {
		super();
		screenVars = sv;
		new ScreenModel("Sr674", AppVars.getInstance(), sv);
	}
	
	protected Sr674ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sr674ScreenVars.class);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		csbil003Permission = FeaConfg.isFeatureExist("2", "CSBIL003", appVars, "IT");  //ICIL-298
		stampDutyflag = FeaConfg.isFeatureExist("2", "NBPROP01", appVars, "IT");
		/*ILIFE-8248 start*/
		lnkgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(), "NBPRP055", appVars, "IT");
		billChgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(), "CSBIL005", appVars, "IT");//IBPLIFE-4822
		/*ILIFE-8248 end*/	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("4000");
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaSbmaction.set(wsspcomn.sbmaction);
		wsaaFirstTime.set("Y");
		initialize(sv.dataArea);
		sv.initialiseSubfileArea();
		BTPRO033Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), BTPRO033, appVars, "IT");
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
		//ILJ-49 End
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		sv.amtfld01Out[varcom.nd.toInt()].set("Y");
		sv.amtfld02Out[varcom.nd.toInt()].set("Y");
		sv.amtfld03Out[varcom.nd.toInt()].set("Y");
		sv.amtfld04Out[varcom.nd.toInt()].set("Y");
		sv.amtfld05Out[varcom.nd.toInt()].set("Y");
		sv.amtfld01Out[varcom.pr.toInt()].set("Y");
		sv.amtfld02Out[varcom.pr.toInt()].set("Y");
		sv.amtfld03Out[varcom.pr.toInt()].set("Y");
		sv.amtfld04Out[varcom.pr.toInt()].set("Y");
		sv.amtfld05Out[varcom.pr.toInt()].set("Y");
		wsaaTaxPrem01.set(ZERO);
		wsaaTaxPrem02.set(ZERO);
		wsaaTaxPrem03.set(ZERO);
		wsaaTaxPrem04.set(ZERO);
		wsaaTaxPrem05.set(ZERO);
		chdrmjaIO.setRecKeyData(SPACES);
		chdrmjaIO.setRecNonKeyData(SPACES);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		payrIO.setRecKeyData(SPACES);
		payrIO.setRecNonKeyData(SPACES);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		setExtraInfo();//PINNACLE-2875
		sv.nextinsdte.set(payrIO.getBillcd());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.company.set(wsspcomn.fsuco);
		/*    Set screen fields*/
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttyp.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.register.set(chdrmjaIO.getRegister());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.ptdate.set(payrIO.getPtdate());
		sv.btdate.set(payrIO.getBtdate());
		sv.billfreq.set(payrIO.getBillfreq());
		sv.billday.set(payrIO.getBillday());		
		wsaaOrigBillfreq.set(payrIO.getBillfreq());
		sv.mop.set(payrIO.getBillchnl());
		wsaaLastMop.set(payrIO.getBillchnl());
		wsaaTableToRead.set(tablesInner.t3623);
		wsaaItemToRead.set(chdrmjaIO.getStatcode());
		readDesc1100();
		sv.chdrstatus.set(descIO.getShortdesc());
		wsaaTableToRead.set(tablesInner.t3588);
		wsaaItemToRead.set(payrIO.getPstatcode());
		readDesc1100();
		sv.premstatus.set(descIO.getShortdesc());
		wsaaTableToRead.set(tablesInner.t5688);
		wsaaItemToRead.set(sv.cnttyp);
		readDesc1100();
		sv.ctypedes.set(descIO.getLongdesc());
		wsaaTableToRead.set(tablesInner.t3590);
		wsaaItemToRead.set(sv.billfreq);
		readDesc1100();
		sv.freqdesc.set(descIO.getLongdesc());
		wsaaTableToRead.set(tablesInner.t3620);
		wsaaItemToRead.set(sv.mop);
		readDesc1100();
		sv.mopdesc.set(descIO.getLongdesc());
		cltsIO.setClntpfx(chdrmjaIO.getCownpfx());
		cltsIO.setClntcoy(chdrmjaIO.getCowncoy());
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-8537 starts*/
		clntDao = DAOFactory.getClntpfDAO();
		clnt=clntDao.getClientByClntnum(cltsIO.getClntnum().toString());
		/*ILIFE-8537 ends*/
		sv.cownnum.set(chdrmjaIO.getCownnum());
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		readT56791200();
		readT57291300();
		traceEffectiveDate1400();
		getOsLoan1500();
		getOsprem1700();
		getSuspense1800();
		readAnniRules1900();
		readTh6111a00();
		readT56891b00();
		checkBoxAction1c00();
		if(!stampDutyflag) {
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			sv.zstpduty02Out[varcom.nd.toInt()].set("Y");
			sv.zstpduty03Out[varcom.nd.toInt()].set("Y");
			sv.zstpduty04Out[varcom.nd.toInt()].set("Y");
			sv.zstpduty05Out[varcom.nd.toInt()].set("Y");
		}
		loadSubfile5000();
		init1010CustomerSpecific();
		calcContFee6000();
		calcWopPremium7000();
		calcDuePremium8000();
		checkMaxminPrem8100();
		scrnparams.subfileRrn.set(1);
		isbillday = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP011", appVars, "IT");
		if(!isbillday){
			sv.billdayOut[varcom.nd.toInt()].set("Y");
			}
		/*ICIL-298 starts*/
		if (!csbil003Permission) {
			sv.csbil003flag.set("N");
		}
		else {
			sv.csbil003flag.set("Y");
		}
		
		if(csbil003Permission)
		{
			getT5688Data();
		}
		/*ICIL-298 ends*/
		if(billChgFlag) {
			sv.billdayOut[varcom.pr.toInt()].set("N");
		}else {
			sv.tmpChgOut[varcom.nd.toInt()].set("Y");
			sv.nextinsdteOut[varcom.nd.toInt()].set("Y");
		}
	}
private void setExtraInfo() {
	wsaaNewMandref.set(payrIO.getMandref());
	wsaaNewGrupnum.set(payrIO.getGrupnum());
	wsaaNewGrupcoy.set(payrIO.getGrupcoy());
	wsaaNewMembsel.set(chdrmjaIO.getMembsel());
	wsaaNewPayrnum.set(chdrmjaIO.getPayrnum());
	wsaaNewIncomeSeqno.set(payrIO.getIncomeSeqNo());
}	
protected void init1010CustomerSpecific(){
	
}	

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void readDesc1100()
	{
		start1110();
	}

protected void start1110()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaTableToRead);
		descIO.setDescitem(wsaaItemToRead);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
	}

protected void readT56791200()
	{
		n1210();
	}

protected void n1210()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readT57291300()
	{
		start1310();
	}

protected void start1310()
	{
		wsaaFlexiblePremium.set("N");
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5729);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(), tablesInner.t5729)
		&& isEQ(itdmIO.getItemitem(), chdrmjaIO.getCnttype())) {
			wsaaFlexiblePremium.set("Y");
		}
	}

protected void traceEffectiveDate1400()
	{
		findLastAnnivesary1410();
		yearlyEffdate1420();
		halfYearlyEffdate1430();
		quarterlyEffdate1440();
		monthlyEffdate1450();
		fortnightlyEffdate1460();
	}

protected void findLastAnnivesary1410()
	{
		wsaaOccdate.set(chdrmjaIO.getOccdate());
		wsaaPtdate.set(sv.ptdate);
		wsaaLastAnndateYy.set(wsaaPtdateYy);
		wsaaLastAnndateMm.set(wsaaOccdateMm);
		wsaaLastAnndateDd.set(wsaaOccdateDd);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.statuz.set(SPACES);
		while ( !(isEQ(datcon1rec.statuz, varcom.oK))) {
			datcon1rec.intDate.set(wsaaLastAnndate);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				wsaaLastAnndateDd.subtract(1);
			}
		}

		if (isGT(wsaaLastAnndate, sv.ptdate)) {
			datcon4rec.intDate1.set(wsaaLastAnndate);
			datcon4rec.billday.set(payrIO.getDuedd());
			datcon4rec.billmonth.set(payrIO.getDuemm());
			datcon4rec.freqFactor.set(-1);
			datcon4rec.frequency.set("01");
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastAnndate.set(datcon4rec.intDate2);
		}
	}

protected void yearlyEffdate1420()
	{
		datcon4rec.intDate1.set(wsaaLastAnndate);
		datcon4rec.billday.set(payrIO.getDuedd());
		datcon4rec.billmonth.set(payrIO.getDuemm());
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set("01");
		while ( !(isGTE(datcon4rec.intDate1, sv.ptdate))) {
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		sv.effdate01.set(datcon4rec.intDate1);
	}

protected void halfYearlyEffdate1430()
	{
		datcon4rec.intDate1.set(wsaaLastAnndate);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set("02");
		while ( !(isGTE(datcon4rec.intDate1, sv.ptdate))) {
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		sv.effdate02.set(datcon4rec.intDate1);
	}

protected void quarterlyEffdate1440()
	{
		datcon4rec.intDate1.set(wsaaLastAnndate);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set("04");
		while ( !(isGTE(datcon4rec.intDate1, sv.ptdate))) {
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		sv.effdate03.set(datcon4rec.intDate1);
	}

protected void monthlyEffdate1450()
	{
		datcon4rec.intDate1.set(wsaaLastAnndate);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set("12");
		while ( !(isGTE(datcon4rec.intDate1, sv.ptdate))) {
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		sv.effdate04.set(datcon4rec.intDate1);
	}
protected void fortnightlyEffdate1460()
{
	datcon4rec.intDate1.set(wsaaLastAnndate);
	datcon4rec.freqFactor.set(1);
	datcon4rec.frequency.set("26");
	while ( !(isGTE(datcon4rec.intDate1, sv.ptdate))) {
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		datcon4rec.intDate1.set(datcon4rec.intDate2);
	}

	sv.effdate05.set(datcon4rec.intDate1);
}
protected void getOsLoan1500()
	{
		startApl1510();
		policyLoan1530();
	}

protected void startApl1510()
	{
		initialize(totloanrec.totloanRec);
		totloanrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrmjaIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(wsaaToday);
		totloanrec.function.set("APL");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(totloanrec.statuz);
			syserrrec.params.set(totloanrec.totloanRec);
			fatalError600();
		}
		sv.zloanamt01.set(totloanrec.principal);
		sv.zloanamt04.set(totloanrec.interest);
	}

protected void policyLoan1530()
	{
		initialize(totloanrec.totloanRec);
		totloanrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrmjaIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(wsaaToday);
		totloanrec.function.set("PLON");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(totloanrec.statuz);
			syserrrec.params.set(totloanrec.totloanRec);
			fatalError600();
		}
		sv.zloanamt02.set(totloanrec.principal);
		sv.zloanamt05.set(totloanrec.interest);
	}

protected void getOsprem1700()
	{
		/*START*/
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(payrIO.getBtdate());
		datcon3rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(sv.zloanamt03, 5).set(mult((chdrmjaIO.getSinstamt06()), datcon3rec.freqFactor));
		/*EXIT*/
	}

protected void getSuspense1800()
	{
		start1810();
	}

protected void start1810()
	{
		acblIO.setRecKeyData(SPACES);
		acblIO.setRldgcoy(chdrmjaIO.getChdrcoy());
		acblIO.setRldgacct(chdrmjaIO.getChdrnum());
		acblIO.setOrigcurr(payrIO.getBillcurr());
		acblIO.setSacscode("LP");
		acblIO.setSacstyp("S");
		acblIO.setFormat(formatsInner.acblrec);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError600();
		}
		if (isNE(acblIO.getStatuz(), varcom.oK)) {
			sv.zloanamt06.set(ZERO);
		}
		else {
			compute(sv.zloanamt06, 2).set(mult(acblIO.getSacscurbal(), -1));
		}
	}

protected void readAnniRules1900()
	{
		start1910();
		readAnrlcnt1920();
	}

protected void start1910()
	{
		wsaaAiindInd.set(SPACES);
		anrlcntIO.setRecKeyData(SPACES);
		anrlcntIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		anrlcntIO.setChdrnum(chdrmjaIO.getChdrnum());
		anrlcntIO.setFormat(formatsInner.anrlcntrec);
		anrlcntIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		anrlcntIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void readAnrlcnt1920()
	{
		SmartFileCode.execute(appVars, anrlcntIO);
		if (isNE(anrlcntIO.getStatuz(), varcom.oK)
		&& isNE(anrlcntIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(anrlcntIO.getParams());
			syserrrec.statuz.set(anrlcntIO.getStatuz());
			fatalError600();
		}
		if (isNE(anrlcntIO.getStatuz(), varcom.oK)
		|| isNE(anrlcntIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(anrlcntIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			return ;
		}
		if (isEQ(anrlcntIO.getValidflag(), "1")) {
			wsaaAiindInd.set("Y");
			return ;
		}
		anrlcntIO.setFunction(varcom.nextr);
		readAnrlcnt1920();
		return ;
	}

protected void readTh6111a00()
	{
		try {
			start1a10();
			readCatchAll1a20();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1a10()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th611);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.getItemitem().setSub1String(4, 3, payrIO.getCntcurr());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		&& isEQ(itdmIO.getItemtabl(), tablesInner.th611)
		&& isEQ(subString(itdmIO.getItemitem(), 1, 3), chdrmjaIO.getCnttype())
		&& isEQ(subString(itdmIO.getItemitem(), 4, 3), payrIO.getCntcurr())) {
			th611rec.th611Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.exit1a90);
		}
	}

protected void readCatchAll1a20()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th611);
		itdmIO.setItemitem("***");
		itdmIO.getItemitem().setSub1String(4, 3, payrIO.getCntcurr());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		&& isEQ(itdmIO.getItemtabl(), tablesInner.th611)
		&& isEQ(subString(itdmIO.getItemitem(), 1, 3), "***")
		&& isEQ(subString(itdmIO.getItemitem(), 4, 3), payrIO.getCntcurr())) {
			th611rec.th611Rec.set(itdmIO.getGenarea());
			return ;
		}
		syserrrec.statuz.set(errorsInner.rl12);
		fatalError600();
	}

protected void readT56891b00()
	{
		start1b10();
	}

protected void start1b10()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5689);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5689)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())) {
			syserrrec.statuz.set(errorsInner.e368);
			fatalError600();
		}
		t5689rec.t5689Rec.set(itdmIO.getGenarea());
	
		wsaaFreq01Allow.set("N");
		wsaaFreq02Allow.set("N");
		wsaaFreq04Allow.set("N");
		wsaaFreq12Allow.set("N");
		wsaaFreq26Allow.set("N");
		for (ix.set(1); !(isGT(ix, 30)); ix.add(1)){
			if (isEQ(t5689rec.billfreq[ix.toInt()], "01")) {
				wsaaFreq01Allow.set("Y");
			}
			if (isEQ(t5689rec.billfreq[ix.toInt()], "02")) {
				wsaaFreq02Allow.set("Y");
			}
			if (isEQ(t5689rec.billfreq[ix.toInt()], "04")) {
				wsaaFreq04Allow.set("Y");
			}
			if (isEQ(t5689rec.billfreq[ix.toInt()], "12")) {
				wsaaFreq12Allow.set("Y");
			}
			if (isEQ(t5689rec.billfreq[ix.toInt()], "26")) {
				wsaaFreq26Allow.set("Y");
			}
		}
	}

protected void checkBoxAction1c00()
	{
		start1c10();
	}

protected void start1c10()
	{
		/* IF CHDRMJA-PAYRNUM       NOT = SPACES                        */
		/*    MOVE '+'                 TO SR674-PAYIND                  */
		/* END-IF.                                                      */
		c100ReadClrf();
		if (isNE(clrfIO.getClntnum(), chdrmjaIO.getCownnum())) {
			sv.payind.set("+");
		}
		if (isNE(payrIO.getMandref(), SPACES)) {
			if (isEQ(payrIO.getBillchnl(), sv.mop)) {
				if(isEQ(wsaaSbmaction,"A"))
				{
					if (isEQ(sv.mop, "R")) {
						sv.crcind.set("+");
					}
					if (isEQ(sv.mop, "D")) {
						sv.ddind.set("+");
					}
				}
			}
			else {
				b200KeepsPayr();
			}
		}
		else {
			sv.ddind.set(SPACES);
			sv.crcind.set(SPACES);
			/*****    MOVE '+'                 TO SR674-DDIND                   */
		}
		if (isNE(payrIO.getGrupnum(), SPACES)) {
			sv.grpind.set("+");
		}
		if (isEQ(wsaaAiindInd, "Y")) {
			sv.aiind.set("+");
		}
		else {
			sv.aiindOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/*ICIL-298 starts*/
	if(csbil003Permission)
	{
		if(isEQ(wsaaFreqErr,"Y")){
			scrnparams.errorline.set(tr386rec.progdesc01);
		}
	}
		/*ICIL-298 ends*/
		/* IF QUOTATION                                                 */
		/*    MOVE 'N'                 TO SR674-PRTSHD-OUT(PR)          */
		/*    MOVE 'Y'                 TO SR674-DDIND-OUT(PR)           */
		/*    MOVE 'Y'                 TO SR674-AIIND-OUT(PR)           */
		/*    GO TO PRE-EXIT                                            */
		/* END-IF.                                                      */
		if (isNE(sv.amtfld01, ZERO)) {
			sv.amtfld01Out[varcom.nd.toInt()].set("N");
			sv.amtfld01Out[varcom.pr.toInt()].set("N");
		}
		if (isNE(sv.amtfld02, ZERO)) {
			sv.amtfld02Out[varcom.nd.toInt()].set("N");
			sv.amtfld02Out[varcom.pr.toInt()].set("N");
		}
		if (isNE(sv.amtfld03, ZERO)) {
			sv.amtfld03Out[varcom.nd.toInt()].set("N");
			sv.amtfld03Out[varcom.pr.toInt()].set("N");
		}
		if (isNE(sv.amtfld04, ZERO)) {
			sv.amtfld04Out[varcom.nd.toInt()].set("N");
			sv.amtfld04Out[varcom.pr.toInt()].set("N");
		}
		if (isNE(sv.amtfld05, ZERO)) {
			sv.amtfld05Out[varcom.nd.toInt()].set("N");
			sv.amtfld05Out[varcom.pr.toInt()].set("N");
		}
		if (quotation.isTrue()) {
			sv.prtshdOut[varcom.pr.toInt()].set("N");
			sv.ddindOut[varcom.pr.toInt()].set("Y");
			sv.crcindOut[varcom.pr.toInt()].set("Y");
			sv.aiindOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		sv.prtshdOut[varcom.pr.toInt()].set("Y");
		sv.ddindOut[varcom.pr.toInt()].set("N");
		sv.crcindOut[varcom.pr.toInt()].set("N");
		return ;
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			validationForTmpChg();
			checkForErrors2050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
	}

protected void validationForTmpChg() {
	if(!billChgFlag)	return ;
	checkStatus();
	if (statusNotValid.isTrue()) {
		sv.chdrstatusErr.set(errorsInner.h137);
		wsspcomn.edterror.set("Y");
		return ;
	}

	if(isNE(payrIO.getBillday(), sv.billday)) {
		if(isEQ(sv.tmpChg, SPACES)) {
			sv.tmpChgErr.set(errorsInner.rupl);
			wsspcomn.edterror.set("Y");
			return ;
		}
	
		if(wsaaMaxdday.trim().equals("")) {
			setNewNextdate();
		}else if(validateMaxBillingDay()>Integer.parseInt(wsaaMaxdday)) {
	    	sv.billdayErr.set(errorsInner.rupm);
			wsspcomn.edterror.set("Y");
			return ;
	    }
	} else {
		wsaaNewNextdate = payrIO.getBillcd().toString();
	}
	List list = bextpfDAO.searchBextrevRecordForDel(wsspcomn.company.toString(), sv.chdrnum.toString());
	if(list !=null && !list.isEmpty()) {
		sv.billdayErr.set(errorsInner.rupn);
		wsspcomn.edterror.set("Y");
		return ;
	}
	
	
}


protected void checkStatus() {
	wsaaValidStatus.set("N");
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
			if (isEQ(chdrmjaIO.getStatcode(), t5679rec.cnRiskStat[ix.toInt()])) {
				wsaaValidStatus.set("Y");
				ix.set(13);
			}
		}
		if (statusNotValid.isTrue()) {
			return ;
		}
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
			if (isEQ(chdrmjaIO.getPstatcode(), t5679rec.cnPremStat[ix.toInt()])) {
				wsaaValidStatus.set("Y");
				ix.set(13);
			}
		}
		
}

protected void setNewNextdate() {
	if(Integer.parseInt(changedBillday) > Integer.parseInt(payrBillday)) {
		wsaaNewNextdate = payrIO.getBillcd().toString().substring(0, 6).concat(changedBillday);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(wsaaNewNextdate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			sv.billdayErr.set(errorsInner.rupm);
			wsspcomn.edterror.set("Y");
		}
	}else {
       wsaaNewNextdate = payrIO.getBillcd().toString().substring(0, 6).concat(changedBillday);
		
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(wsaaNewNextdate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.billdayErr.set(errorsInner.rupm);
			wsspcomn.edterror.set("Y");
		}
		wsaaNewNextdate = datcon2rec.intDate2.toString();
	}
}

protected int validateMaxBillingDay() {
	   
    if(Integer.parseInt(changedBillday) > Integer.parseInt(payrBillday)) {
        wsaaNewNextdate = payrIO.getBillcd().toString().substring(0, 6).concat(changedBillday);
        datcon1rec.datcon1Rec.set(SPACES);
        datcon1rec.intDate.set(wsaaNewNextdate);
        datcon1rec.function.set(varcom.conv);
        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
        if (isNE(datcon1rec.statuz, varcom.oK)) {
            return Integer.parseInt(wsaaMaxdday) + 1;
        }
    }else {
        wsaaNewNextdate = payrIO.getBillcd().toString().substring(0, 6).concat(changedBillday);
       
        datcon2rec.datcon2Rec.set(SPACES);
        datcon2rec.intDate2.set(ZERO);
        datcon2rec.intDate1.set(wsaaNewNextdate);
        datcon2rec.frequency.set("12");
        datcon2rec.freqFactor.set(1);
        callProgram(Datcon2.class, datcon2rec.datcon2Rec);
        if (isNE(datcon2rec.statuz, varcom.oK)) {
            return Integer.parseInt(wsaaMaxdday) + 1;
        }
        wsaaNewNextdate = datcon2rec.intDate2.toString();       
       
    }
    datcon3rec.datcon3Rec.set(SPACES);
    datcon3rec.intDate1.set(payrIO.getBillcd());
    datcon3rec.intDate2.set(wsaaNewNextdate);
    datcon3rec.frequency.set("DY");
    callProgram(Datcon3.class, datcon3rec.datcon3Rec);
    if (isNE(datcon3rec.statuz, varcom.oK)) {
        syserrrec.statuz.set(datcon3rec.statuz);
        fatalError600();
    }
    return datcon3rec.freqFactor.toInt();
}

protected void validateScreen2010()
	{
		if (quotation.isTrue()) {
			if (isNE(sv.prtshd, "Y")
			&& isNE(sv.prtshd, "N")) {
				sv.prtshdErr.set(errorsInner.f136);
				wsspcomn.edterror.set("Y");
			}
		}
			
		if (isNE(sv.billfreq, "01")
		&& isNE(sv.billfreq, "02")
		&& isNE(sv.billfreq, "04")
		&& isNE(sv.billfreq, "12") && isNE(sv.billfreq, "26")) {
			sv.freqdesc.set(SPACES);
			sv.billfreqErr.set(errorsInner.h048);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.billfreqErr, SPACES)) {
			wsaaTableToRead.set(tablesInner.t3590);
			wsaaItemToRead.set(sv.billfreq);
			readDesc1100();
			sv.freqdesc.set(descIO.getLongdesc());
		}
		if (isEQ(sv.mop, SPACES)) {
			sv.mopdesc.set(SPACES);
			sv.mopErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.mopErr, SPACES)) {
			wsaaTableToRead.set(tablesInner.t3620);
			wsaaItemToRead.set(sv.mop);
			readDesc1100();
			sv.mopdesc.set(descIO.getLongdesc());
		}
		if(billChgFlag)	{
			/* Bill freq. and MOP must in T5689.*/
			wsaaValidBillfreqMop = "N";
			if(isEQ(sv.billday, SPACES)) {
				if(isGT(chdrmjaIO.getOccdate(), ZERO) && chdrmjaIO.getOccdate().toString().length()>=8) {
					changedBillday = chdrmjaIO.getOccdate().toString().substring(6,8);
				}
			} else {
				changedBillday = sv.billday.toString();
			}
			if(isEQ(payrIO.getBillday(), SPACES)) {
				if(isGT(payrIO.getBillcd(), ZERO) && payrIO.getBillcd().toString().length()>=8) {
					payrBillday = payrIO.getBillcd().toString().substring(6,8);
				}
			} else {
				payrBillday = payrIO.getBillday().toString();
			}
			
			for (ix.set(1); !(isGT(ix, 30)); ix.add(1)){ 
				if (isEQ(sv.billfreq, t5689rec.billfreq[ix.toInt()])
				&& isEQ(sv.mop, t5689rec.mop[ix.toInt()])) {
					wsaaValidBillfreqMop = "Y";
					wsaaMaxdday = t5689rec.maxdd[ix.toInt()].toString().trim(); 
					if(! (isEQ(t5689rec.allowBillday[ix.toInt()],"Y"))) {//IBPLIFE-6532
						if(isNE(sv.billday, payrIO.getBillday())) {
								sv.billdayErr.set(errorsInner.rupu);
								wsspcomn.edterror.set("Y");
							}
						}	
							
					 
					ix.set(31);
				}
			}
			if (isNE(sv.billdayErr, SPACES)) {
				goTo(GotoLabel.exit2090);
			}
		}
		if (isEQ(wsaaValidBillfreqMop, "N")) {
			sv.billfreqErr.set(errorsInner.h060);
			sv.mopErr.set(errorsInner.h060);
			wsspcomn.edterror.set("Y");
		}
		/* Need not proceed following validation if Billing Freq or*/
		/* MOP is not valid.*/
		if (isNE(sv.billfreqErr, SPACES)
		|| isNE(sv.mopErr, SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if(stampDutyflag) {
			wsaaZstpduty01.set(ZERO);
			wsaaZstpduty02.set(ZERO);
			wsaaZstpduty03.set(ZERO);
			wsaaZstpduty04.set(ZERO);
			wsaaZstpduty05.set(ZERO);
		}
		/* Recalc and reload subfile screen when MOP change.*/
		if (isNE(sv.mop, wsaaLastMop)) {
			loadSubfile5000();
			vldt2010CustomerSpecific();
			calcContFee6000();
			calcWopPremium7000();
			calcDuePremium8000();
			scrnparams.subfileRrn.set(1);
			wsaaLastMop.set(sv.mop);
		}
		checkMaxminPrem8100();
		if (isEQ(sv.billfreq, "01")){
			if (isNE(sv.totfld01Err, SPACES)) {
				sv.billfreqErr.set(errorsInner.g514);
				wsspcomn.edterror.set("Y");
			}
		}
		else if (isEQ(sv.billfreq, "02")){
			if (isNE(sv.totfld02Err, SPACES)) {
				sv.billfreqErr.set(errorsInner.g514);
				wsspcomn.edterror.set("Y");
			}
		}
		else if (isEQ(sv.billfreq, "04")){
			if (isNE(sv.totfld03Err, SPACES)) {
				sv.billfreqErr.set(errorsInner.g514);
				wsspcomn.edterror.set("Y");
			}
		}
		else if (isEQ(sv.billfreq, "12")){
			if (isNE(sv.totfld04Err, SPACES)) {
				sv.billfreqErr.set(errorsInner.g514);
				wsspcomn.edterror.set("Y");
			}
		}
		else if (isEQ(sv.billfreq, "26")){
			if (isNE(sv.totfld05Err, SPACES)) {
				sv.billfreqErr.set(errorsInner.g514);
				wsspcomn.edterror.set("Y");
			}
		}
		sv.totfld01Err.set(SPACES);
		sv.totfld02Err.set(SPACES);
		sv.totfld03Err.set(SPACES);
		sv.totfld04Err.set(SPACES);
		sv.totfld05Err.set(SPACES);
		if (isEQ(wsaaScrnStatuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		prorateFlag = false;
		/* Following validation for Billing Change.*/
		if (isEQ(sv.billfreq, "01")){
			if (isNE(sv.ptdate, sv.effdate01)) {
				prorateFlag = true;
			}
		}
		else if (isEQ(sv.billfreq, "02")){
			if (isNE(sv.ptdate, sv.effdate02)) {
				prorateFlag = true;
			}
		}
		else if (isEQ(sv.billfreq, "04")){
			if (isNE(sv.ptdate, sv.effdate03)) {
				prorateFlag = true;
			}
		}
		else if (isEQ(sv.billfreq, "12")){
			if (isNE(sv.ptdate, sv.effdate04)) {
				prorateFlag = true;
			}			
		}
		else if (isEQ(sv.billfreq, "26")){
			if (isNE(sv.ptdate, sv.effdate05)) {
				prorateFlag = true;
			}
		}
		if(!BTPRO033Permission && prorateFlag) {
			sv.billfreqErr.set(errorsInner.rl13);
			prorateFlag = false;
			wsspcomn.edterror.set("Y");
		}
		
		/* Quotation validation stop here.*/
		if (quotation.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t3620);
		itemIO.setItemitem(sv.mop);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		/*   Check that Bank Details are required.*/
		wsaaChnlChk.set(sv.mop);
		/* IF T3620-DDIND           NOT = SPACES                        */
		/*    IF SR674-DDIND            = SPACES                        */
		/*        MOVE 'X'             TO SR674-DDIND                   */
		/*    END-IF                                                    */
		/* ELSE                                                         */
		/*    IF  SR674-DDIND           = 'X'                           */
		/*    AND PAYR-MANDREF          = SPACES                        */
		/*        MOVE H062            TO SR674-DDIND-ERR               */
		/*        MOVE 'Y'             TO WSSP-EDTERROR                 */
		/*    END-IF                                                    */
		/*    IF  SR674-DDIND           = '+'                           */
		/*    AND PAYR-MANDREF      NOT = SPACES                        */
		/*        MOVE 'X'             TO SR674-DDIND                   */
		/*    END-IF                                                    */
		/* END-IF.                                                      */
		if (directDebit.isTrue()) {
			if (isNE(t3620rec.ddind, SPACES)) {
				if (isEQ(sv.ddind, SPACES)) {
					sv.ddind.set("X");
				}
			}
			else {
				if (isEQ(sv.ddind, SPACES)) {
					sv.ddind.set("+");
				}
			}
		}
		else {
			if (isEQ(sv.ddind, "X")) {
				sv.ddindErr.set(errorsInner.h062);
				wsspcomn.edterror.set("Y");
			}
			else {
				sv.ddind.set(SPACES);
			}
		}
		/*   Check that Credit Card Details are required.                  */
		if (creditCard.isTrue()) {
			if (isNE(t3620rec.crcind, SPACES)) {
				if (isEQ(sv.crcind, SPACES)) {
					sv.crcind.set("X");
				}
			}
			else {
				if (isEQ(sv.crcind, SPACES)) {
					sv.crcind.set("+");
				}
			}
		}
		else {
			if (isEQ(sv.crcind, "X")) {
				//MIBT-283 STARTS
				sv.crcindErr.set(errorsInner.h062);
//				sv.crcindErr.set(errorsInner.h064);
				wsspcomn.edterror.set("Y");
			}
			else {
				sv.crcind.set(SPACES);
			}
		}
		/*   Check that Group Details are required.*/
		if (isNE(t3620rec.grpind, SPACES)) {
			if (isEQ(sv.grpind, SPACES)) {
				sv.grpind.set("X");
			}
		}
		else {
			if (isEQ(sv.grpind, "X")
			&& isEQ(payrIO.getGrupnum(), SPACES)) {
				sv.grpindErr.set(errorsInner.h063);
				wsspcomn.edterror.set("Y");
			}
			if (isEQ(sv.grpind, "+")
			&& isNE(payrIO.getGrupnum(), SPACES)) {
				sv.grpind.set("X");
			}
		}
		/* Validate check box action.*/
		if (isNE(sv.payind, "X")
		&& isNE(sv.payind, "+")
		&& isNE(sv.payind, " ")) {
			sv.payindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.grpind, "X")
		&& isNE(sv.grpind, "+")
		&& isNE(sv.grpind, " ")) {
			sv.grpindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ddind, "X")
		&& isNE(sv.ddind, "+")
		&& isNE(sv.ddind, " ")) {
			sv.ddindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.crcind, "X")
		&& isNE(sv.crcind, "+")
		&& isNE(sv.crcind, " ")) {
			sv.crcindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.aiind, "X")
		&& isNE(sv.aiind, "+")
		&& isNE(sv.aiind, " ")) {
			sv.aiindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
		}
	}

protected void vldt2010CustomerSpecific(){
	
}


/*ICIL-298 starts*/
protected void getT5688Data(){
	Itempf itempf = new Itempf();
	ItemDAO itempfDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	itempf.setItempfx(smtpfxcpy.item.toString());
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("TR386");
	wsaaTr386Lang.set(wsspcomn.language);
	wsaaTr386Pgm.set(wsaaProg);
	itempf.setItemitem(wsaaTr386Key.toString());
	Itempf itemobj = itempfDao.getItempfRecord(itempf);
	if(itemobj != null){
		tr386rec.tr386Rec.set(StringUtil.rawToString(itemobj.getGenarea()));
	}
	List<Itempf> itempfList = itempfDao.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.t5688.toString(),sv.cnttyp.toString(),covrmjaIO.getCrrcd().toInt());   

	if (itempfList == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(tablesInner.t5688.toString()).concat(sv.cnttyp.toString()));
		fatalError600();
	}
	else{
		
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
				if(isNE(t5688rec.bfreqallw,SPACES)){
					scrnparams.errorline.set(tr386rec.progdesc01);
					wsaaFreqErr.set("Y");
					sv.billfreqOut[varcom.nd.toInt()].set("Y");
					if(isEQ(sv.billfreq,"01"))	
							sv.actionflag.set("Y");
					else if(isEQ(sv.billfreq,"02"))
							sv.actionflag.set("H");
					else if(isEQ(sv.billfreq,"04"))
							sv.actionflag.set("Q");
					else if(isEQ(sv.billfreq,"12"))
							sv.actionflag.set("M");
					else if(isEQ(sv.billfreq,"26"))
							sv.actionflag.set("F");	
				}

	}
}
/*ICIL-298 ends*/
protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
//			MIBT- 283 STARTS
			if(isEQ(sv.ddind,"X"))
			{
				sv.ddind.set(SPACES);
			}
			if(isEQ(sv.crcind,"X"))
			{
			sv.crcind.set(SPACES);
			}

			if (isEQ(sv.grpind,"X"))
			{
			sv.grpind.set(SPACES);
			}
//			MIBT- 283 ENDS
		}
		/*VALIDATE-SUBFILE*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateDatabase3010();
					checkIncr3030();
				case updateDatabase3040:
					updateDatabase3040();
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		if (quotation.isTrue()) {
			if (isEQ(sv.prtshd, "Y")) {
				wsaaPrevTranno.set(chdrmjaIO.getTranno());
				printLetter3100();
			}
			if(BTPRO033Permission) {
				chdrmjaIO.setFunction(varcom.keeps);
				setChdrNewInputs();
				payrIO.setFunction(varcom.keeps);
				setPayrNewInputs();
				SmartFileCode.execute(appVars, payrIO);
				if (isNE(payrIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(payrIO.getParams());
					fatalError600();
				}
			}
			goTo(GotoLabel.exit3090);
		}
		if(prorateFlag) {
			chdrmjaIO.setFunction(varcom.keeps);
			setChdrNewInputs();
			payrIO.setFunction(varcom.keeps);
			setPayrNewInputs() ;
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-BOX-SELECTED*/
		if (isEQ(sv.payind, "X")
		|| isEQ(sv.grpind, "X")
		|| isEQ(sv.ddind, "X")
		|| isEQ(sv.crcind, "X")
		|| isEQ(sv.aiind, "X")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void checkIncr3030()
	{
		if (!firstTime.isTrue()) {
			goTo(GotoLabel.updateDatabase3040);
		}
		wsaaFirstTime.set("N");
		wsaaIncrFound.set("N");
		incrmjaIO.setRecKeyData(SPACES);
		incrmjaIO.setChdrcoy(wsspcomn.company);
		incrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incrmjaIO.setFormat(formatsInner.incrmjarec);
		incrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isEQ(incrmjaIO.getStatuz(), varcom.oK)
		&& isEQ(incrmjaIO.getChdrcoy(), wsspcomn.company)
		&& isEQ(incrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			wsaaIncrFound.set("Y");
			sv.chdrnumErr.set(errorsInner.j008);
			wsspcomn.edterror.set("Y");
			scrnparams.function.set(varcom.prot);
			sv.billfreqOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void updateDatabase3040()
	{
		if (incrFound.isTrue()) {
			disableIncr3200();
			clearCovrIndexationInd3300();
		}
		/* Save the after change info.*/
		wsaaBillfreq.set(sv.billfreq);
		boolean isUpdate = checkExtraUpdate();//PINNACLE-2875
		if (isEQ(t3620rec.ddind, SPACES)
		&& isEQ(t3620rec.crcind, SPACES)) {
			wsaaNewMandref.set(SPACES);
		}
		if (isEQ(t3620rec.grpind, SPACES)) {
			wsaaNewGrupnum.set(SPACES);
			wsaaNewGrupcoy.set(SPACES);
			wsaaNewMembsel.set(SPACES);
		}
		//PINNACLE-2875 start
		if((!isUpdate)
				&& isEQ(sv.billfreq, payrIO.getBillfreq())
				&& isEQ(sv.mop, payrIO.getBillchnl())
				&& isEQ(sv.billday, payrIO.getBillday()) 
				&& isNE(sv.tmpChg, "Y") 
				) {
			releaseSoftlock3900();
			goTo(GotoLabel.exit3090); 
		}
		//PINNACLE-2875 end
		processChdr3400();
		processPayr3500();
		processComponent3600();
		updtDBCustomerSpecific3600();
		createPtrn3700();
		printLetter3100();
		statistic3800();
		/* Maintain Diary entries if required.*/
		dryProcessing4800();
		releaseSoftlock3900();
	}
//PINNACLE-2875 start
private boolean checkExtraUpdate() {
	boolean isUpdate = false;
	if(isNE(wsaaNewMandref,payrIO.getMandref())) {
		wsaaNewMandref.set(payrIO.getMandref());
		isUpdate = true;
	}
	if(isNE(wsaaNewGrupnum,payrIO.getGrupnum())) {
		wsaaNewGrupnum.set(payrIO.getGrupnum());
		isUpdate = true;
	}
	if(isNE(wsaaNewGrupcoy,payrIO.getGrupcoy())) {
		wsaaNewGrupcoy.set(payrIO.getGrupcoy());
		isUpdate = true;
	}
	if(isNE(wsaaNewMembsel,chdrmjaIO.getMembsel())) {
		wsaaNewMembsel.set(chdrmjaIO.getMembsel());
		isUpdate = true;
	}
	if(isNE(wsaaNewPayrnum,chdrmjaIO.getPayrnum())) {
		wsaaNewPayrnum.set(chdrmjaIO.getPayrnum());
		isUpdate = true;
	}
	if(isNE(wsaaNewIncomeSeqno,payrIO.getIncomeSeqNo())) {
		wsaaNewIncomeSeqno.set(payrIO.getIncomeSeqNo());
		isUpdate = true;
	}
	return isUpdate;
}
//PINNACLE-2875 end
protected void updtDBCustomerSpecific3600(){
	
}

protected void printLetter3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readTr384FullKey3110();
					readTr384CatchAll3120();
				case tr384Found3130:
					tr384Found3130();
					letterRequest3160();
				case exit3190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readTr384FullKey3110()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.getItemitem().setSub1String(4, 4, wsaaBatchkey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.tr384Found3130);
		}
	}

protected void readTr384CatchAll3120()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem("***");
		itemIO.getItemitem().setSub1String(4, 4, wsaaBatchkey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.tr384Found3130);
		}
		else {
			goTo(GotoLabel.exit3190);
		}
	}

protected void tr384Found3130()
	{
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType, SPACES)) {
			goTo(GotoLabel.exit3190);
		}
	}

protected void letterRequest3160()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		wsaaOkey1.set(wsspcomn.language);
		wsaaRqstBillfreq.set(sv.billfreq);
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.despnum.set(chdrmjaIO.getDespnum());
		letrqstrec.trcde.set(wsaaBatchkey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

protected void disableIncr3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begnIncr3210();
				case readIncr3220:
					readIncr3220();
					nextIncr3280();
				case exit3290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnIncr3210()
	{
		incrmjaIO.setRecKeyData(SPACES);
		incrmjaIO.setChdrcoy(wsspcomn.company);
		incrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incrmjaIO.setFormat(formatsInner.incrmjarec);
		incrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readIncr3220()
	{
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		&& isNE(incrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incrmjaIO.getParams());
			syserrrec.statuz.set(incrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		|| isNE(incrmjaIO.getChdrcoy(), wsspcomn.company)
		|| isNE(incrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			goTo(GotoLabel.exit3290);
		}
		incrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrmjaIO.getParams());
			syserrrec.statuz.set(incrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void nextIncr3280()
	{
		incrmjaIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readIncr3220);
	}

protected void clearCovrIndexationInd3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begnCovr3310();
				case readCovr3320:
					readCovr3320();
					nextCovr3380();
				case exit3390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnCovr3310()
	{
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readCovr3320()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		|| isNE(covrmjaIO.getChdrcoy(), wsspcomn.company)
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			goTo(GotoLabel.exit3390);
		}
		covrmjaIO.setIndexationInd(SPACES);
		covrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void nextCovr3380()
	{
		covrmjaIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readCovr3320);
	}

protected void processChdr3400()
	{
		rlseChdr3410();
		readChdr3420();
		validflag23430();
		validflag13440();
	}

protected void rlseChdr3410()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void readChdr3420()
	{
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag23430()
	{
		wsaaPrevTranno.set(chdrmjaIO.getTranno());
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(sv.ptdate);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13440()
	{
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setCurrfrom(sv.ptdate);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);		
		chdrmjaIO.setFunction(varcom.writr);
		setChdrNewInputs();
		
	}
protected void setChdrNewInputs() {

		//PINNACLE-2531 START
		if(isNE(wsaaNewGrupnum,SPACES)) {
			chdrmjaIO.setGrupkey(wsaaNewGrupnum);
		}
		if(isNE(wsaaNewMembsel,SPACES)) {
			chdrmjaIO.setMembsel(wsaaNewMembsel);
		}
		if(isNE(wsaaNewPayrnum,SPACES)) {
			chdrmjaIO.setPayrnum(wsaaNewPayrnum);
		}
		if(isNE(wsaaNewMandref,SPACES)) {
			chdrmjaIO.setMandref(wsaaNewMandref);
		}
		//PINNACLE-2531 END
		chdrmjaIO.setBillchnl(sv.mop);
		chdrmjaIO.setBillfreq(sv.billfreq);
		chdrmjaIO.setValidflag("1");
		 if(stampDutyflag) {
				if (isEQ(sv.billfreq, "01")){
					chdrmjaIO.setSinstamt01(add(sv.zgrsamt01,sv.zstpduty01));
					chdrmjaIO.setSinstamt02(wsaaContFee01);
				}
				else if (isEQ(sv.billfreq, "02")){
					chdrmjaIO.setSinstamt01(add(sv.zgrsamt02,sv.zstpduty02));
					chdrmjaIO.setSinstamt02(wsaaContFee02);
				}
				else if (isEQ(sv.billfreq, "04")){
					chdrmjaIO.setSinstamt01(add(sv.zgrsamt03,sv.zstpduty03));
					chdrmjaIO.setSinstamt02(wsaaContFee03);
				}
				else if (isEQ(sv.billfreq, "12")){
					chdrmjaIO.setSinstamt01(add(sv.zgrsamt04,sv.zstpduty04));
					chdrmjaIO.setSinstamt02(wsaaContFee04);
				}
				else if (isEQ(sv.billfreq, "26")){
					chdrmjaIO.setSinstamt01(add(sv.zgrsamt05,sv.zstpduty05));
					chdrmjaIO.setSinstamt02(wsaaContFee05);
				}
			
			} else {
				if (isEQ(sv.billfreq, "01")){
					chdrmjaIO.setSinstamt01(sv.zgrsamt01);
					chdrmjaIO.setSinstamt02(wsaaContFee01);
				}
				else if (isEQ(sv.billfreq, "02")){
					chdrmjaIO.setSinstamt01(sv.zgrsamt02);
					chdrmjaIO.setSinstamt02(wsaaContFee02);
				}
				else if (isEQ(sv.billfreq, "04")){
					chdrmjaIO.setSinstamt01(sv.zgrsamt03);
					chdrmjaIO.setSinstamt02(wsaaContFee03);
				}
				else if (isEQ(sv.billfreq, "12")){
					chdrmjaIO.setSinstamt01(sv.zgrsamt04);
					chdrmjaIO.setSinstamt02(wsaaContFee04);
				}
				else if (isEQ(sv.billfreq, "26")){
					chdrmjaIO.setSinstamt01(sv.zgrsamt05);
					chdrmjaIO.setSinstamt02(wsaaContFee05);
				}
			}
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(), chdrmjaIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
}
protected void processPayr3500()
	{
		rlsePayr3510();
		readPayr3520();
		validflag23530();
		prcssPayr3500CustomerSpecific();
		validflag13540();
	}

protected void prcssPayr3500CustomerSpecific(){
	
}

protected void rlsePayr3510()
	{
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void readPayr3520()
	{
		payrIO.setFunction(varcom.readh);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag23530()
	{
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void validflag13540()
	{
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setTransactionDate(wsaaToday);
		payrIO.setUser(varcom.vrcmUser);
		payrIO.setEffdate(sv.ptdate);
		
		setPayrNewInputs() ;
		billDayflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), BILLDAY_FEATURE_ID, appVars, "IT");
		if(billDayflag){
		payrIO.setBillcd(changeBillDay(sv.billday.toString()));
		}
		/* ILIFE-8027 - Ends*/
		if(billChgFlag) {
			payrIO.setBillcd(wsaaNewNextdate);
		}
		if(isbillday) {
			payrIO.setBillday(sv.billday);
			if(wsaaNewNextdate != null && isNE(wsaaNewNextdate, SPACES))
				payrIO.setBillmonth(String.valueOf(wsaaNewNextdate).substring(4, 6));
		}
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void setPayrNewInputs() {
	//PINNACLE-2531 START
	if(isNE(wsaaNewGrupnum,SPACES)) {
		payrIO.setGrupnum(wsaaNewGrupnum);
	}
	if(isNE(wsaaNewGrupcoy,SPACES)) {
		payrIO.setGrupcoy(wsaaNewGrupcoy);
	}
	if(isNE(wsaaNewMembsel,SPACES)) {
		payrIO.setMembsel(wsaaNewMembsel);
	}
	if(isNE(wsaaNewMandref,SPACES)) {
		payrIO.setMandref(wsaaNewMandref);
	}
	if(isNE(wsaaNewIncomeSeqno,SPACES)) {
		payrIO.setIncomeSeqNo(wsaaNewIncomeSeqno);
	}
	//PINNACLE-2531 END
	payrIO.setBillchnl(sv.mop);
	payrIO.setBillfreq(sv.billfreq);
	payrIO.setValidflag("1");
	/* ILIFE-8027 - Starts*/
	if(billChgFlag) {
		if(isNE(sv.tmpChg,"Y")) {				
			payrIO.setOrgbillcd(0);
		} else {
			if(isEQ(payrIO.getBillday(), SPACES)) {
				payrIO.setOrgbillcd(1);	//PINNACLE-2605
			} else {
				payrIO.setOrgbillcd(payrIO.getBillcd()==null ? 0 : payrIO.getBillcd());
			}
		}
	}
	payrIO.setBillday(sv.billday);
		
  if(stampDutyflag) {
		if (isEQ(sv.billfreq, "01")){
			payrIO.setSinstamt01(add(sv.zgrsamt01,sv.zstpduty01));
			payrIO.setSinstamt02(wsaaContFee01);
		}
		else if (isEQ(sv.billfreq, "02")){
			payrIO.setSinstamt01(add(sv.zgrsamt02,sv.zstpduty02));
			payrIO.setSinstamt02(wsaaContFee02);
		}
		else if (isEQ(sv.billfreq, "04")){
			payrIO.setSinstamt01(add(sv.zgrsamt03,sv.zstpduty03));
			payrIO.setSinstamt02(wsaaContFee03);
		}
		else if (isEQ(sv.billfreq, "12")){
			payrIO.setSinstamt01(add(sv.zgrsamt04,sv.zstpduty04));
			payrIO.setSinstamt02(wsaaContFee04);
		}
		else if (isEQ(sv.billfreq, "26")){
			payrIO.setSinstamt01(add(sv.zgrsamt05,sv.zstpduty05));
			payrIO.setSinstamt02(wsaaContFee05);
		}
	
	}
	
	else {
		if (isEQ(sv.billfreq, "01")){
		payrIO.setSinstamt01(sv.zgrsamt01);
		payrIO.setSinstamt02(wsaaContFee01);
		}
		else if (isEQ(sv.billfreq, "02")){
			payrIO.setSinstamt01(sv.zgrsamt02);
			payrIO.setSinstamt02(wsaaContFee02);
		}
		else if (isEQ(sv.billfreq, "04")){
			payrIO.setSinstamt01(sv.zgrsamt03);
			payrIO.setSinstamt02(wsaaContFee03);
		}
		else if (isEQ(sv.billfreq, "12")){
			payrIO.setSinstamt01(sv.zgrsamt04);
			payrIO.setSinstamt02(wsaaContFee04);
		}
		else if (isEQ(sv.billfreq, "26")){
			payrIO.setSinstamt01(sv.zgrsamt05);
			payrIO.setSinstamt02(wsaaContFee05);
		}
		
	}
  
	
	setPrecision(payrIO.getSinstamt06(), 2);
	payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05()));
}

protected void processComponent3600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		if(stampDutyflag) {
			zstpduty01 = zstpduty01List.iterator();
			zstpduty02 = zstpduty02List.iterator();
			zstpduty03 = zstpduty03List.iterator();
			zstpduty04 = zstpduty04List.iterator();
			zstpduty05 = zstpduty05List.iterator();
			zclstate01 = zclstate01List.iterator();
			zclstate02 = zclstate02List.iterator();
			zclstate03 = zclstate03List.iterator();
			zclstate04 = zclstate04List.iterator();
			zclstate05 = zclstate05List.iterator();
		}
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					startSfl3601();
				case readSfl3602:
					readSfl3602();
					nextSfl3608();
				case exit3609:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void startSfl3601()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl3602()
	{
		processScreen("SR674", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(scrnparams.statuz, varcom.oK)) {
			if (!updatedList.isEmpty()) {
				rertpfDAO.updateRertList(updatedList);
			}
			if (!insertedList.isEmpty()) {
				rertpfDAO.insertRertList(insertedList);
			}
			goTo(GotoLabel.exit3609);
		}
		/*PROCESS-SFL*/
		processCovr3610();
		processIncr3620();
		processRertpf();
		processAgcm3630();
		genericSubroutine3640();
	}

protected void nextSfl3608()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl3602);
	}

protected void processCovr3610()
	{
		readCovrmja3611();
		validflag23612();
		prcssCovr3610CustomerSpecific();
		validflag13613();
	}

protected void prcssCovr3610CustomerSpecific(){
	
}

protected void readCovrmja3611()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag23612()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(sv.ptdate);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void validflag13613()
	{
	riskPremflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), RISKPREM_FEATURE_ID, appVars, "IT"); //ILIFE-7845
	    covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(sv.ptdate);
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(add(covrmjaIO.getTranno(), 1));
		covrmjaIO.setUser(varcom.vrcmUser);
		covrmjaIO.setTransactionDate(wsaaToday);
		if (isEQ(sv.billfreq, "01")){
			covrmjaIO.setInstprem(sv.prmtrb01);
			covrmjaIO.setZbinstprem(sv.basprm01);
			covrmjaIO.setZlinstprem(sv.zloprm01);
			if(stampDutyflag) {
				if(zstpduty01.hasNext()) {
				covrmjaIO.setZstpduty01(zstpduty01.next());
				}
				if(zclstate01.hasNext()) {
				covrmjaIO.setZclstate(zclstate01.next());
				}
				covrmjaIO.setInstprem(add(covrmjaIO.getInstprem(), covrmjaIO.getZstpduty01()));
			}
			//ILIFE-7845
			if(riskPremflag){
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(chdrmjaIO.getCnttype());
				premiumrec.crtable.set(covrmjaIO.getCrtable());
				premiumrec.calcTotPrem.set(add(sv.prmtrb01, covrmjaIO.getZstpduty01()));
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
				{
					callProgram("RISKPREMIUM", premiumrec.premiumRec);
					covrmjaIO.setRiskprem(premiumrec.riskPrem);
				}
			}
			//ILIFE-7845
			if (isEQ(sv.premind, "Y")) {
				covrmjaIO.setSumins(sv.sumins01);
			}
		}
		else if (isEQ(sv.billfreq, "02")){
			covrmjaIO.setInstprem(sv.prmtrb02);
			covrmjaIO.setZbinstprem(sv.basprm02);
			covrmjaIO.setZlinstprem(sv.zloprm02);
			if(stampDutyflag) {
				if(zstpduty02.hasNext()) {
				covrmjaIO.setZstpduty01(zstpduty02.next());
				}
				if(zclstate02.hasNext()) {
				covrmjaIO.setZclstate(zclstate02.next());
				}
				covrmjaIO.setInstprem(add(covrmjaIO.getInstprem(), covrmjaIO.getZstpduty01()));
			}
			//ILIFE-7845
			if(riskPremflag){
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(chdrmjaIO.getCnttype());
				premiumrec.crtable.set(covrmjaIO.getCrtable());
				premiumrec.calcTotPrem.set(add(sv.prmtrb02, covrmjaIO.getZstpduty01()));
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
				{
					callProgram("RISKPREMIUM", premiumrec.premiumRec);
					covrmjaIO.setRiskprem(premiumrec.riskPrem);
				}
			}
			//ILIFE-7845
			if (isEQ(sv.premind, "Y")) {
				covrmjaIO.setSumins(sv.sumins02);
			}
		}
		else if (isEQ(sv.billfreq, "04")){
			covrmjaIO.setInstprem(sv.prmtrb03);
			covrmjaIO.setZbinstprem(sv.basprm03);
			covrmjaIO.setZlinstprem(sv.zloprm03);
			if(stampDutyflag) {
				if(zstpduty03.hasNext()) {
				covrmjaIO.setZstpduty01(zstpduty03.next());
				}
				if(zclstate03.hasNext()) {
				covrmjaIO.setZclstate(zclstate03.next());
				}
				covrmjaIO.setInstprem(add(covrmjaIO.getInstprem(), covrmjaIO.getZstpduty01()));
			}
			//ILIFE-7845
			if(riskPremflag){
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(chdrmjaIO.getCnttype());
				premiumrec.crtable.set(covrmjaIO.getCrtable());
				premiumrec.calcTotPrem.set(add(sv.prmtrb03, covrmjaIO.getZstpduty01()));
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
				{
					callProgram("RISKPREMIUM", premiumrec.premiumRec);
					covrmjaIO.setRiskprem(premiumrec.riskPrem);
				}
			}
			//ILIFE-7845
			if (isEQ(sv.premind, "Y")) {
				covrmjaIO.setSumins(sv.sumins03);
			}
		}
		else if (isEQ(sv.billfreq, "12")){
			covrmjaIO.setInstprem(sv.prmtrb04);
			covrmjaIO.setZbinstprem(sv.basprm04);
			covrmjaIO.setZlinstprem(sv.zloprm04);
			if(stampDutyflag) {
				if(zstpduty04.hasNext()) {
				covrmjaIO.setZstpduty01(zstpduty04.next());
				}
				if(zclstate04.hasNext()) {
				covrmjaIO.setZclstate(zclstate04.next());
				}
				covrmjaIO.setInstprem(add(covrmjaIO.getInstprem(), covrmjaIO.getZstpduty01()));
			}
			//ILIFE-7845
			if(riskPremflag){
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(chdrmjaIO.getCnttype());
				premiumrec.crtable.set(covrmjaIO.getCrtable());
				premiumrec.calcTotPrem.set(add(sv.prmtrb04, covrmjaIO.getZstpduty01()));
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
				{
					callProgram("RISKPREMIUM", premiumrec.premiumRec);
					covrmjaIO.setRiskprem(premiumrec.riskPrem);
				}
			}
			//ILIFE-7845
			if (isEQ(sv.premind, "Y")) {
				covrmjaIO.setSumins(sv.sumins04);
			}
		}
		else if (isEQ(sv.billfreq, "26")){
			covrmjaIO.setInstprem(sv.prmtrb05);
			covrmjaIO.setZbinstprem(sv.basprm05);
			covrmjaIO.setZlinstprem(sv.zloprm05);
			if(stampDutyflag) {
				if(zstpduty05.hasNext()) {
				covrmjaIO.setZstpduty01(zstpduty05.next());
				}
				if(zclstate05.hasNext()) {
				covrmjaIO.setZclstate(zclstate05.next());
				}
				covrmjaIO.setInstprem(add(covrmjaIO.getInstprem(), covrmjaIO.getZstpduty01()));
			}
			//ILIFE-7845
			if(riskPremflag){
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(chdrmjaIO.getCnttype());
				premiumrec.crtable.set(covrmjaIO.getCrtable());
				premiumrec.calcTotPrem.set(add(sv.prmtrb05, covrmjaIO.getZstpduty01()));
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
				{
					callProgram("RISKPREMIUM", premiumrec.premiumRec);
					covrmjaIO.setRiskprem(premiumrec.riskPrem);
				}
			}
			//ILIFE-7845
			if (isEQ(sv.premind, "Y")) {
				covrmjaIO.setSumins(sv.sumins05);
			}
		}
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void processIncr3620()
	{
		readIncr3621();
	}

protected void readIncr3621()
	{
		incrhstIO.setRecKeyData(SPACES);
		incrhstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		incrhstIO.setChdrnum(covrmjaIO.getChdrnum());
		incrhstIO.setLife(covrmjaIO.getLife());
		incrhstIO.setCoverage(covrmjaIO.getCoverage());
		incrhstIO.setRider(covrmjaIO.getRider());
		incrhstIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incrhstIO.setFormat(formatsInner.incrhstrec);
		incrhstIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, incrhstIO);
		if (isNE(incrhstIO.getStatuz(), varcom.oK)
		&& isNE(incrhstIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(incrhstIO.getParams());
			syserrrec.statuz.set(incrhstIO.getStatuz());
			fatalError600();
		}
		if (isNE(incrhstIO.getStatuz(), varcom.oK)) {
			return ;
		}
		if (isEQ(sv.billfreq, "01")){
			incrhstIO.setOrigInst(sv.prmtrb01);
			incrhstIO.setLastInst(sv.prmtrb01);
			incrhstIO.setNewinst(sv.prmtrb01);
			incrhstIO.setZboriginst(sv.basprm01);
			incrhstIO.setZblastinst(sv.basprm01);
			incrhstIO.setZbnewinst(sv.basprm01);
			incrhstIO.setZloriginst(sv.zloprm01);
			incrhstIO.setZllastinst(sv.zloprm01);
			incrhstIO.setZlnewinst(sv.zloprm01);
			if (isEQ(sv.premind, "Y")) {
				incrhstIO.setOrigSum(sv.sumins01);
				incrhstIO.setLastSum(sv.sumins01);
				incrhstIO.setNewsum(sv.sumins01);
			}
		}
		else if (isEQ(sv.billfreq, "02")){
			incrhstIO.setOrigInst(sv.prmtrb02);
			incrhstIO.setLastInst(sv.prmtrb02);
			incrhstIO.setNewinst(sv.prmtrb02);
			incrhstIO.setZboriginst(sv.basprm02);
			incrhstIO.setZblastinst(sv.basprm02);
			incrhstIO.setZbnewinst(sv.basprm02);
			incrhstIO.setZloriginst(sv.zloprm02);
			incrhstIO.setZllastinst(sv.zloprm02);
			incrhstIO.setZlnewinst(sv.zloprm02);
			if (isEQ(sv.premind, "Y")) {
				incrhstIO.setOrigSum(sv.sumins02);
				incrhstIO.setLastSum(sv.sumins02);
				incrhstIO.setNewsum(sv.sumins02);
			}
		}
		else if (isEQ(sv.billfreq, "04")){
			incrhstIO.setOrigInst(sv.prmtrb03);
			incrhstIO.setLastInst(sv.prmtrb03);
			incrhstIO.setNewinst(sv.prmtrb03);
			incrhstIO.setZboriginst(sv.basprm03);
			incrhstIO.setZblastinst(sv.basprm03);
			incrhstIO.setZbnewinst(sv.basprm03);
			incrhstIO.setZloriginst(sv.zloprm03);
			incrhstIO.setZllastinst(sv.zloprm03);
			incrhstIO.setZlnewinst(sv.zloprm03);
			if (isEQ(sv.premind, "Y")) {
				incrhstIO.setOrigSum(sv.sumins03);
				incrhstIO.setLastSum(sv.sumins03);
				incrhstIO.setNewsum(sv.sumins03);
			}
		}
		else if (isEQ(sv.billfreq, "12")){
			incrhstIO.setOrigInst(sv.prmtrb04);
			incrhstIO.setLastInst(sv.prmtrb04);
			incrhstIO.setNewinst(sv.prmtrb04);
			incrhstIO.setZboriginst(sv.basprm04);
			incrhstIO.setZblastinst(sv.basprm04);
			incrhstIO.setZbnewinst(sv.basprm04);
			incrhstIO.setZloriginst(sv.zloprm04);
			incrhstIO.setZllastinst(sv.zloprm04);
			incrhstIO.setZlnewinst(sv.zloprm04);
			if (isEQ(sv.premind, "Y")) {
				incrhstIO.setOrigSum(sv.sumins04);
				incrhstIO.setLastSum(sv.sumins04);
				incrhstIO.setNewsum(sv.sumins04);
			}
		}
		else if (isEQ(sv.billfreq, "26")){
			incrhstIO.setOrigInst(sv.prmtrb05);
			incrhstIO.setLastInst(sv.prmtrb05);
			incrhstIO.setNewinst(sv.prmtrb05);
			incrhstIO.setZboriginst(sv.basprm05);
			incrhstIO.setZblastinst(sv.basprm05);
			incrhstIO.setZbnewinst(sv.basprm05);
			incrhstIO.setZloriginst(sv.zloprm05);
			incrhstIO.setZllastinst(sv.zloprm05);
			incrhstIO.setZlnewinst(sv.zloprm05);
			if (isEQ(sv.premind, "Y")) {
				incrhstIO.setOrigSum(sv.sumins05);
				incrhstIO.setLastSum(sv.sumins05);
				incrhstIO.setNewsum(sv.sumins05);
			}
		}
		incrhstIO.setTranno(chdrmjaIO.getTranno());
		incrhstIO.setStatcode(covrmjaIO.getStatcode());
		incrhstIO.setPstatcode(covrmjaIO.getPstatcode());
		/*incrhstIO.setTransactionDate(wsaaToday);
		incrhstIO.setTransactionTime(getCobolTime());
		*/
		incrhstIO.setTransactionTime(varcom.vrcmTime.toInt());
		incrhstIO.setTransactionDate(varcom.vrcmDate.toInt());
		incrhstIO.setTermid(varcom.vrcmTermid);
		incrhstIO.setUser(varcom.vrcmUser);
		incrhstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrhstIO);
		if (isNE(incrhstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrhstIO.getParams());
			syserrrec.statuz.set(incrhstIO.getStatuz());
			fatalError600();
		}
	}

protected void processRertpf() {
	List<Rertpf> list = rertpfDAO.getRertpfList(covrmjaIO.getChdrcoy().toString(), covrmjaIO.getChdrnum().toString(), covrmjaIO.getLife().toString(),
			covrmjaIO.getJlife().toString(), covrmjaIO.getCoverage().toString(), covrmjaIO.getRider().toString(), covrmjaIO.getPlanSuffix().toInt());
	if(list == null || list.isEmpty())
		return ;
	isProcesFreqChange = true;
	isFirstCovr = true;
	
	List<Covrpf> covrpflist = covrpfDAO.getCovrmjaByComAndNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	if (!covrpflist.isEmpty()) {

		isFirstCovr = true;
			for (int i = 0; i < covrpflist.size(); i++) {
			Covrpf c = covrpflist.get(i);
				for (Rertpf rertpf : list) {
					if (rertpf.getLife().equals(c.getLife()) && rertpf.getCoverage().equals(c.getCoverage()) && rertpf.getRider().equals(c.getRider())) {
						rerateChgFlag = true;
						wsaaRerateDate.set(rertpf.getEffdate());
						rertpf.setValidflag("2");
						updatedList.add(rertpf);
						insertedRertpf = new Rertpf(rertpf);
						insertedRertpf.setValidflag("1");
						insertedRertpf.setTranno(chdrmjaIO.getTranno().toInt());
						if (isEQ(sv.billfreq, "01")){			
							insertedRertpf.setLastInst(sv.prmtrb01.getbigdata());
							insertedRertpf.setZblastinst(sv.basprm01.getbigdata());
							insertedRertpf.setZllastinst(sv.zloprm01.getbigdata());	
						}else if (isEQ(sv.billfreq, "02")){
							insertedRertpf.setLastInst(sv.prmtrb02.getbigdata());
							insertedRertpf.setZblastinst(sv.basprm02.getbigdata());
							insertedRertpf.setZllastinst(sv.zloprm02.getbigdata());	
						}else if (isEQ(sv.billfreq, "04")){
							insertedRertpf.setLastInst(sv.prmtrb03.getbigdata());
							insertedRertpf.setZblastinst(sv.basprm03.getbigdata());
							insertedRertpf.setZllastinst(sv.zloprm03.getbigdata());	
						}else if (isEQ(sv.billfreq, "12")){
							insertedRertpf.setLastInst(sv.prmtrb04.getbigdata());
							insertedRertpf.setZblastinst(sv.basprm04.getbigdata());
							insertedRertpf.setZllastinst(sv.zloprm04.getbigdata());
						}else if (isEQ(sv.billfreq, "26")){
							insertedRertpf.setLastInst(sv.prmtrb05.getbigdata());
							insertedRertpf.setZblastinst(sv.basprm05.getbigdata());
							insertedRertpf.setZllastinst(sv.zloprm05.getbigdata());
						}
						callPmexSubroutine(c);
						insertedRertpf.setNewinst(premiumrec.calcPrem.getbigdata());
						insertedRertpf.setZbnewinst(premiumrec.calcBasPrem.getbigdata());
						insertedRertpf.setZlnewinst(premiumrec.calcLoaPrem.getbigdata());
						rerateChgFlag = false;
						insertedList.add(insertedRertpf);
					}
				}
				if (i + 1 < covrpflist.size()) {
					isFirstCovr = false;
				} else {
					isFirstCovr = true;
				}
			}
	}

}

protected void callPmexSubroutine(Covrpf covrpf) {
	premiumrec.premiumRec.set(SPACES);
	initialize(premiumrec.premiumRec);
	premiumrec.updateRequired.set("N");
	premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
	premiumrec.proratPremCalcFlag.set("N");
	if (isFirstCovr) {
		premiumrec.firstCovr.set("Y");
	} else {
		premiumrec.firstCovr.set("N");
		premiumrec.lifeLife.set(covrpf.getLife());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.covrRider.set(covrpf.getRider());
		callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		if (isNE(premiumrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		return;
	}
	premiumrec.riskPrem.set(ZERO);
	premiumrec.language.set(wsspcomn.language);
	premiumrec.billfreq.set(sv.billfreq);
	readTables(covrpf);
	if (AppVars.getInstance().getAppConfig().isVpmsEnable()) {
		premiumrec.premMethod.set(t5687rec.premmeth);
	}
	if (premiumrec.premMethod.toString().trim().equals("PMEX")) {
		premiumrec.setPmexCall.set("Y");
		premiumrec.cownnum.set(chdrmjaIO.getCownnum());
		premiumrec.occdate.set(chdrmjaIO.getOccdate());
	}
	premiumrec.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
	premiumrec.chdrChdrnum.set(chdrmjaIO.getChdrnum());
	premiumrec.cnttype.set(chdrmjaIO.getCnttype());
	premiumrec.effectdt.set(covrpf.getCrrcd());
	premiumrec.effectdt.set(wsaaRerateDate);  
	premiumrec.ratingdate.set(wsaaRerateDate);
	premiumrec.reRateDate.set(wsaaRerateDate);
	premiumrec.mop.set(payrIO.getBillchnl());
	premiumrec.commissionPrem.set(ZERO);
	premiumrec.zstpduty01.set(ZERO);
	premiumrec.zstpduty02.set(ZERO);
	premiumrec.crtable.set(covrpf.getCrtable());
	premiumrec.lifeLife.set(covrpf.getLife());
	if (("PMEX".equals(t5675rec.premsubr.toString().trim())) && (lnkgFlag)) {
		if (null == covrpf.getLnkgsubrefno() || covrpf.getLnkgsubrefno().trim().isEmpty()) {
			premiumrec.lnkgSubRefNo.set(SPACE);
		} else {
			premiumrec.lnkgSubRefNo.set(covrpf.getLnkgsubrefno().trim());
		}
		if (null == covrpf.getLnkgno() || covrpf.getLnkgno().trim().isEmpty()) {
			premiumrec.linkcov.set(SPACE);
		} else {
			LinkageInfoService linkgService = new LinkageInfoService();
			FixedLengthStringData linkgCov = new FixedLengthStringData(
					linkgService.getLinkageInfo(covrpf.getLnkgno().trim()));
			premiumrec.linkcov.set(linkgCov);
		}
	}
	premiumrec.lifeJlife.set(covrpf.getJlife());
	premiumrec.covrCoverage.set(covrpf.getCoverage());
	premiumrec.covrRider.set(covrpf.getRider());
	premiumrec.termdate.set(covrpf.getPremCessDate());
	Lifepf lifepf = lifepfDAO.getLifeEnqRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
	premiumrec.lsex.set(lifepf.getCltsex());
	Lifepf jlife = lifepfDAO.getLifelnbRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");
	if (isNE(t5687rec.rtrnwfreq, ZERO)) {
		calculateAnb3260(lifepf);
		premiumrec.lage.set(wsaaAnb);
		if (jlife != null) {
			calculateAnb3260(jlife);
			premiumrec.jlage.set(wsaaAnb);
			premiumrec.jlsex.set(jlife.getCltsex());
		} else {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	} else {
		premiumrec.lage.set(covrpf.getAnbAtCcd());
		if (jlife != null) {
			premiumrec.jlsex.set(jlife.getCltsex());
			premiumrec.jlage.set(jlife.getAnbAtCcd());
		} else {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	}
	Clntpf clt = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifepf.getLifcnum());
	if (stampDutyflag && clt.getClntStateCd() != null) {
		premiumrec.rstate01.set(clt.getClntStateCd().substring(3).trim());
	}
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(premiumrec.termdate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.duration.set(datcon3rec.freqFactor);
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(covrpf.getRiskCessDate());
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.riskCessTerm.set(datcon3rec.freqFactor);
	premiumrec.currcode.set(covrpf.getPremCurrency());
	if (isEQ(covrpf.getPlanSuffix(), ZERO) && isNE(chdrmjaIO.getPolinc(), 1)) {
		compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(), chdrmjaIO.getPolsum()));
	} else {
		premiumrec.sumin.set(covrpf.getSumins());
	}
	compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin, chdrmjaIO.getPolinc()));
	premiumrec.mortcls.set(covrpf.getMortcls());
	premiumrec.calcPrem.set(covrpf.getInstprem());
	premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
	premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
	getAnny3280(covrpf);
	premiumrec.commTaxInd.set("Y");
	premiumrec.prevSumIns.set(ZERO);
	premiumrec.inputPrevPrem.set(ZERO);
	com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao = DAOFactory.getClntpfDAO();
	com.csc.smart400framework.dataaccess.model.Clntpf clnt = clntDao.getClientByClntnum(lifepf.getLifcnum());
	if (null != clnt && null != clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()) {
		premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
	} else {
		premiumrec.stateAtIncep.set(clt.getClntStateCd());
	}
	premiumrec.validind.set("2");
	getRcvdpf(covrpf);
	callProgram(t5675rec.premsubr, premiumrec.premiumRec);
	if (isNE(premiumrec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(premiumrec.statuz);
		fatalError600();
	}
}

protected void readTables(Covrpf c) {
	Itempf item = new Itempf();
	item.setItempfx(IT);
	item.setItemcoy(chdrmjaIO.getChdrcoy().toString());
	item.setItemtabl("T5687");
	item.setItemitem(c.getCrtable());
	item.setItmfrm(chdrmjaIO.getOccdate().getbigdata());
	item.setItmto(chdrmjaIO.getOccdate().getbigdata());
	List<Itempf> t5687ItemList = itemDao.findByItemDates(item);
	if (t5687ItemList.isEmpty()) {
		t5687rec.t5687Rec.set(SPACES);
		syserrrec.params.set(IT.concat(chdrmjaIO.getChdrcoy().toString()).concat(t5675)
				.concat(t5687rec.premmeth.toString()));
		fatalError600();
	} else {
		t5687rec.t5687Rec.set(StringUtil.rawToString(t5687ItemList.get(0).getGenarea()));
	}
	List<Itempf> t5675ItempfList = itemDao.getAllItemitem(IT, chdrmjaIO.getChdrcoy().toString(), t5675,
			t5687rec.premmeth.toString());
	if (t5675ItempfList.isEmpty()) {
		t5675rec.t5675Rec.set(SPACES);
		syserrrec.params.set(IT.concat(chdrmjaIO.getChdrcoy().toString()).concat(t5675)
				.concat(t5687rec.premmeth.toString()));
		fatalError600();
	} else {
		t5675rec.t5675Rec.set(StringUtil.rawToString(t5675ItempfList.get(0).getGenarea()));
	}
}

protected void calculateAnb3260(Lifepf lifepf) {
	wsaaAnb.set(ZERO);
	AgecalcPojo agecalcPojo = new AgecalcPojo();
	agecalcPojo.setFunction("CALCP");
	agecalcPojo.setLanguage(wsspcomn.language.toString());
	agecalcPojo.setCnttype(chdrmjaIO.getCnttype().toString());
	agecalcPojo.setIntDate1(Integer.toString(lifepf.getCltdob()));
	agecalcPojo.setIntDate2(premiumrec.effectdt.toString());
	agecalcPojo.setCompany(wsspcomn.fsuco.toString());
	agecalcUtils.calcAge(agecalcPojo);
	if (isNE(agecalcPojo.getStatuz(), Varcom.oK)) {
		syserrrec.params.set(agecalcPojo.toString());
		syserrrec.statuz.set(agecalcPojo.getStatuz());
		fatalError600();
	}
	wsaaAnb.set(agecalcPojo.getAgerating());
}

protected void getAnny3280(Covrpf c) {
	Annypf annypf = annypfDAO.getAnnyRecordByAnnyKey(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
			c.getRider(), c.getPlanSuffix());
	if (annypf != null) {
		premiumrec.freqann.set(annypf.getFreqann());
		premiumrec.advance.set(annypf.getAdvance());
		premiumrec.arrears.set(annypf.getArrears());
		premiumrec.guarperd.set(annypf.getGuarperd());
		premiumrec.intanny.set(annypf.getIntanny());
		premiumrec.capcont.set(annypf.getCapcont());
		premiumrec.withprop.set(annypf.getWithprop());
		premiumrec.withoprop.set(annypf.getWithoprop());
		premiumrec.ppind.set(annypf.getPpind());
		premiumrec.nomlife.set(annypf.getNomlife());
		premiumrec.dthpercn.set(annypf.getDthpercn());
		premiumrec.dthperco.set(annypf.getDthperco());
	} else {
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
	}
}

protected void getRcvdpf(Covrpf covrpf) {
	boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars, IT);
	boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", appVars, IT);
	boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars, IT);
	if (incomeProtectionflag || premiumflag || dialdownFlag) {
		Rcvdpf rcvdPFObject = new Rcvdpf();
		rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
		rcvdPFObject.setChdrnum(covrpf.getChdrnum());
		rcvdPFObject.setLife(covrpf.getLife());
		rcvdPFObject.setCoverage(covrpf.getCoverage());
		rcvdPFObject.setRider(covrpf.getRider());
		rcvdPFObject.setCrtable(covrpf.getCrtable());
		rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
		premiumrec.bentrm.set(rcvdPFObject.getBentrm());
		if (rcvdPFObject.getPrmbasis() != null && isEQ("S", rcvdPFObject.getPrmbasis())) {
			premiumrec.prmbasis.set("Y");
		} else {
			premiumrec.prmbasis.set("");
		}
		premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
		premiumrec.occpcode.set("");
		premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
		premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
	}
}



protected void processAgcm3630()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begnAgcm3631();
				case readAgcm3632:
					readAgcm3632();
					validflag23633();
					validflag13634();
				case nextAgcm3638:
					nextAgcm3638();
				case exit3639:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnAgcm3631()
	{
		agcmbchIO.setRecKeyData(SPACES);
		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
		agcmbchIO.setLife(covrmjaIO.getLife());
		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
		agcmbchIO.setRider(covrmjaIO.getRider());
		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
	}

protected void readAgcm3632()
	{
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			fatalError600();
		}
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		|| isNE(agcmbchIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(agcmbchIO.getChdrnum(), covrmjaIO.getChdrnum())
		|| isNE(agcmbchIO.getLife(), covrmjaIO.getLife())
		|| isNE(agcmbchIO.getCoverage(), covrmjaIO.getCoverage())
		|| isNE(agcmbchIO.getRider(), covrmjaIO.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(), covrmjaIO.getPlanSuffix())) {
			goTo(GotoLabel.exit3639);
		}
		if (isEQ(agcmbchIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.nextAgcm3638);
		}
		if (isEQ(agcmbchIO.getTranno(), chdrmjaIO.getTranno())) {
			goTo(GotoLabel.nextAgcm3638);
		}
	}

protected void validflag23633()
	{
		agcmbchIO.setCurrto(sv.ptdate);
		agcmbchIO.setValidflag("2");
		agcmbchIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13634()
	{
		wsaaBillfreq.set(sv.billfreq);
		 if(stampDutyflag) {
			 if (isEQ(sv.billfreq, "01")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(add(sv.prmtrb01,sv.zstpduty01), wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "02")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(add(sv.prmtrb02,sv.zstpduty02), wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "04")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(add(sv.prmtrb03,sv.zstpduty03), wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "12")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(add(sv.prmtrb04,sv.zstpduty04), wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "26")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(add(sv.prmtrb05,sv.zstpduty05), wsaaBillfreq9));
				}
			
			} else {
					
				if (isEQ(sv.billfreq, "01")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(sv.prmtrb01, wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "02")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(sv.prmtrb02, wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "04")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(sv.prmtrb03, wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "12")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(sv.prmtrb04, wsaaBillfreq9));
				}
				else if (isEQ(sv.billfreq, "26")){
					setPrecision(agcmbchIO.getAnnprem(), 2);
					agcmbchIO.setAnnprem(mult(sv.prmtrb05, wsaaBillfreq9));
				}
			}
		agcmbchIO.setValidflag("1");
		agcmbchIO.setTranno(chdrmjaIO.getTranno());
		agcmbchIO.setCurrfrom(sv.ptdate);
		agcmbchIO.setCurrto(varcom.vrcmMaxDate);
		agcmbchIO.setUser(varcom.vrcmUser);
		/*agcmbchIO.setTransactionDate(wsaaToday);
		agcmbchIO.setTransactionTime(getCobolTime());*/
		agcmbchIO.setTransactionTime(varcom.vrcmTime.toInt());
		agcmbchIO.setTransactionDate(varcom.vrcmDate.toInt());
		agcmbchIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			fatalError600();
		}
	}

protected void nextAgcm3638()
	{
		agcmbchIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readAgcm3632);
	}

protected void genericSubroutine3640()
	{
		readT56713641();
	}

protected void readT56713641()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.getItemitem().setSub1String(5, 4, covrmjaIO.getCrtable());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		initialize(bchgallrec.bchgallRec);
		bchgallrec.function.set("BCHG ");
		bchgallrec.company.set(covrmjaIO.getChdrcoy());
		bchgallrec.chdrnum.set(covrmjaIO.getChdrnum());
		bchgallrec.life.set(covrmjaIO.getLife());
		bchgallrec.coverage.set(covrmjaIO.getCoverage());
		bchgallrec.rider.set(covrmjaIO.getRider());
		bchgallrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		bchgallrec.oldBillfreq.set(wsaaOrigBillfreq);
		bchgallrec.newBillfreq.set(sv.billfreq);
		bchgallrec.effdate.set(sv.ptdate);
		bchgallrec.tranno.set(covrmjaIO.getTranno());
		bchgallrec.crtable.set(covrmjaIO.getCrtable());
		bchgallrec.cnttype.set(chdrmjaIO.getCnttype());
		bchgallrec.occdate.set(chdrmjaIO.getOccdate());
		bchgallrec.trancode.set(wsaaBatchkey.batcBatctrcde);
		bchgallrec.cntcurr.set(chdrmjaIO.getCntcurr());
		for (ix.set(1); !(isGT(ix, 4)); ix.add(1)){
			if (isNE(t5671rec.subprog[ix.toInt()], SPACES)) {
				callProgram(t5671rec.subprog[ix.toInt()], bchgallrec.bchgallRec);
				if (isNE(bchgallrec.statuz, varcom.oK)) {
					syserrrec.params.set(bchgallrec.bchgallRec);
					syserrrec.statuz.set(bchgallrec.statuz);
					fatalError600();
				}
			}
		}
	}

protected void createPtrn3700()
	{
		start3710();
	}

protected void start3710()
	{
		ptrnIO.setRecKeyData(SPACES);
		ptrnIO.setRecNonKeyData(SPACES);
		ptrnIO.setDataKey(wsaaBatchkey);
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setRecode(chdrmjaIO.getRecode());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(sv.ptdate);
		ptrnIO.setDatesub(wsaaToday);
		//ptrnIO.setTransactionDate(wsaaToday);
		//ptrnIO.setTransactionTime(getCobolTime());
		ptrnIO.setTransactionTime(varcom.vrcmTime.toInt());
		ptrnIO.setTransactionDate(varcom.vrcmDate.toInt());
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setTermid(varcom.vrcmTermid);
		getUser800();
		ptrnIO.setCrtuser(wsjobDataInner.wsjobUsername);
		ptrnIO.setValidflag("1");
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void statistic3800()
	{
		start3810();
	}

protected void start3810()
	{
		lifsttrrec.batccoy.set(wsaaBatchkey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatchkey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatchkey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatchkey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatchkey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void releaseSoftlock3900()
	{
		start3910();
	}

protected void start3910()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
		releaseChdrlnb4020();
	}

private void nextProgCustomerSpecific() {
	if(BTPRO033Permission) {
		if(prorateFlag) {
			wsspcomn.programPtr.add(1);
		} else {
			wsspcomn.programPtr.add(2);
		}
	} else {
		wsspcomn.programPtr.add(1);
	}
}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			switchingReturn4500();
		}
		if (quotation.isTrue()) {
			nextProgCustomerSpecific();
			return ;
		}
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatchkey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			ix.set(wsspcomn.programPtr);
			iy.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.payind, "X")) {
			gensswrec.function.set("A");
			sv.payind.set(SPACES);
			callGenssw4300();
			if (isEQ(sv.crcind, "+")) {
				sv.crcind.set("X");
			}
			if (isEQ(sv.ddind, "+")) {
				sv.ddind.set("X");
			}
			return ;
		}
		if (isEQ(sv.grpind, "X")) {
			wsspcomn.bchaction.set(" ");  // IBPLIFE-7239 BY LOHITH
			gensswrec.function.set("B");
			sv.grpind.set(SPACES);
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.ddind, "X")) {
			gensswrec.function.set("C");
			sv.ddind.set(SPACES);
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.crcind, "X")) {
			gensswrec.function.set("E");
			wsaaClntkey.clntClntpfx.set("CN");
			wsaaClntkey.clntClntcoy.set(chdrmjaIO.getPayrcoy());
			wsaaClntkey.clntClntnum.set(chdrmjaIO.getPayrnum());
			wsspcomn.clntkey.set(wsaaClntkey);
			sv.crcind.set(SPACES);
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.aiind, "X")) {
			wsspcomn.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrmjaIO.getChdrnum());
			wsspcomn.chdrCnttype.set(chdrmjaIO.getCnttype());
			wsspcomn.chdrValidflag.set(chdrmjaIO.getValidflag());
			wsspcomn.currfrom.set(chdrmjaIO.getCurrfrom());
			gensswrec.function.set("D");
			sv.aiind.set(SPACES);
			callGenssw4300();
			return ;
		}
		/*   No more selected (or none)*/
		/*      - restore stack from wsaa to wssp*/
		ix.set(wsspcomn.programPtr);
		iy.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		readAnniRules1900();
		checkBoxAction1c00();
		/*   Set the next program name to the current screen*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		nextProgCustomerSpecific();
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[iy.toInt()].set(wsspcomn.secProg[ix.toInt()]);
		ix.add(1);
		iy.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[ix.toInt()].set(wsaaSecProg[iy.toInt()]);
		ix.add(1);
		iy.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		/*CALL-SUBROUTINE*/
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)) {
			syserrrec.params.set(gensswrec.gensswRec);
			fatalError600();
		}
		/*    load from gensw to wssp*/
		compute(ix, 0).set(add(1, wsspcomn.programPtr));
		iy.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[ix.toInt()].set(gensswrec.progOut[iy.toInt()]);
		ix.add(1);
		iy.add(1);
		/*EXIT*/
	}

protected void switchingReturn4500()
	{
		retrvChdrmja4510();
		retrvPayr4520();
	}

protected void retrvChdrmja4510()
	{
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void retrvPayr4520()
	{
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void releaseChdrlnb4020()
	{
		chdrlnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void dryProcessing4800()
	{
		start4810();
	}

protected void start4810()
	{
		/* This section will determine if the DIARY system is active*/
		/* If so, the appropriate parameters are filled and the*/
		/* diary processor is called.*/
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatchkey.batcBatctrcde);
		readT75084900();
		/* If item not found try other types of contract.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75084900();
		}
		/* If item not found no Batch Diary Processing is Required.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(wsaaToday);
		drypDryprcRecInner.drypCompany.set(wsaaBatchkey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatchkey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatchkey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(wsaaToday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.*/
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(covrmjaIO.getCpiDate());
		drypDryprcRecInner.drypBbldate.set(covrmjaIO.getBenBillDate());
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrmjaIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75084900()
	{
		start4910();
	}

protected void start4910()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void loadSubfile5000()
	{
		if(stampDutyflag) {
			zstpduty01List = new ArrayList<>();
			zstpduty02List = new ArrayList<>();
			zstpduty03List = new ArrayList<>();
			zstpduty04List = new ArrayList<>();
			zstpduty05List = new ArrayList<>();
			zclstate01List = new ArrayList<>();
			zclstate02List = new ArrayList<>();
			zclstate03List = new ArrayList<>();
			zclstate04List = new ArrayList<>();
			zclstate05List = new ArrayList<>();
		}
		clrSfl5010();
		readCovrmja5020();
		if(stampDutyflag) {
			sv.zstpduty01.set(wsaaZstpduty01);
			sv.zstpduty02.set(wsaaZstpduty02);
			sv.zstpduty03.set(wsaaZstpduty03);
			sv.zstpduty04.set(wsaaZstpduty04);
			sv.zstpduty05.set(wsaaZstpduty05);
		}
	}

protected void clrSfl5010()
	{
		scrnparams.function.set(varcom.sclr);
		processScreen("SR674", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readCovrmja5020()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setFunction(varcom.nextr);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())) {
			return ;
		}
		statusCheck5100();
		if (statusNotValid.isTrue()) {
			readCovrmja5020();
			return ;
		}
		sv.initialiseSubfileArea();
		initialize(sv.subfileFields);
		sv.sumins01.set(ZERO);
		sv.sumins02.set(ZERO);
		sv.sumins03.set(ZERO);
		sv.sumins04.set(ZERO);
		sv.sumins05.set(ZERO);
		sv.basprm01.set(ZERO);
		sv.basprm02.set(ZERO);
		sv.basprm03.set(ZERO);
		sv.basprm04.set(ZERO);
		sv.basprm05.set(ZERO);
		sv.zloprm01.set(ZERO);
		sv.zloprm02.set(ZERO);
		sv.zloprm03.set(ZERO);
		sv.zloprm04.set(ZERO);
		sv.zloprm05.set(ZERO);
		sv.prem01.set(ZERO);
		sv.prem02.set(ZERO);
		sv.prem03.set(ZERO);
		sv.prem04.set(ZERO);
		sv.prem05.set(ZERO);
		sv.prmtrb01.set(ZERO);
		sv.prmtrb02.set(ZERO);
		sv.prmtrb03.set(ZERO);
		sv.prmtrb04.set(ZERO);
		sv.prmtrb05.set(ZERO);
		sv.datakey.set(covrmjaIO.getDataKey());
		sv.crtable.set(covrmjaIO.getCrtable());
		wsaaTableToRead.set(tablesInner.t5687);
		wsaaItemToRead.set(covrmjaIO.getCrtable());
		readDesc1100();
		sv.entity.set(descIO.getLongdesc());
		sv.pstatdate.set(covrmjaIO.getPremCessDate());
		if (!flexiblePremium.isTrue()) {
			readTr5175200();
			if (isNE(sv.premind, "Y")) {
				sv.sumins01.set(covrmjaIO.getSumins());
				sv.sumins02.set(covrmjaIO.getSumins());
				sv.sumins03.set(covrmjaIO.getSumins());
				sv.sumins04.set(covrmjaIO.getSumins());
				sv.sumins05.set(covrmjaIO.getSumins());
				calcPremium5300();
			}
		}
		else {
			calcFlexiblePremium5400();
		}
		calcCompTax8200();
		scrnparams.function.set(varcom.sadd);
		processScreen("SR674", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		readCovrmja5020();
		return ;
	}

protected void statusCheck5100()
	{
		try {
			covrRiskStatus5110();
			covrPremStatus5120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void covrRiskStatus5110()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		if (statusNotValid.isTrue()) {
			goTo(GotoLabel.exit5190);
		}
	}

protected void covrPremStatus5120()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		if (statusNotValid.isTrue()) {
			return ;
		}
	}

protected void readTr5175200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begnItdm5210();
					readContitem5220();
				case noMoreTr5175280:
					noMoreTr5175280();
				case exit5290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnItdm5210()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			goTo(GotoLabel.exit5290);
		}
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		sv.premind.set("Y");
		sv.activeInd.set("Y");
		sv.zrwvflg01.set(tr517rec.zrwvflg01);
		sv.zrwvflg02.set(tr517rec.zrwvflg02);
		sv.zrwvflg03.set(tr517rec.zrwvflg03);
		sv.zrwvflg04.set(tr517rec.zrwvflg04);
	
		iy.set(ZERO);
		wsaaCtables.set(SPACES);
		for (ix.set(1); !(isGT(ix, 50)
		|| isGTE(iy, 500)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()], SPACES)) {
				iy.add(1);
				wsaaCtable[iy.toInt()].set(tr517rec.ctable[ix.toInt()]);
			}
		}
		if (isEQ(tr517rec.contitem, SPACES)) {
			goTo(GotoLabel.noMoreTr5175280);
		}
	}

protected void readContitem5220()
	{
		itdmIO.setItemitem(tr517rec.contitem);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), tr517rec.contitem)) {
			goTo(GotoLabel.noMoreTr5175280);
		}
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		for (ix.set(1); !(isGT(ix, 50)
		|| isGTE(iy, 500)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()], SPACES)) {
				iy.add(1);
				wsaaCtable[iy.toInt()].set(tr517rec.ctable[ix.toInt()]);
			}
		}
		if (isEQ(tr517rec.contitem, SPACES)) {
			goTo(GotoLabel.noMoreTr5175280);
		}
		else {
			readContitem5220();
			return ;
		}
	}

protected void noMoreTr5175280()
	{
		sv.workAreaData.set(wsaaCtables);
	}

protected void calcPremium5300()
	{
		try {
			lifeAndJointLife5310();
			readT56875320();
			readT56755330();
			initPremiumrec5350();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void lifeAndJointLife5310()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		wsaaMlSex.set(lifemjaIO.getCltsex());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void readT56875320()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/*    IF T5687-BBMETH          NOT = SPACES                        */
		/*       MOVE 'Y'                 TO WSAA-REGULAR-UNIT-LINK        */
		/*    ELSE                                                         */
		/*       MOVE 'N'                 TO WSAA-REGULAR-UNIT-LINK        */
		/*    END-IF.                                                      */
		/* Use T5646 to determine REGULAR-UNIT-LINK:                       */
		wsaaRegularUnitLink.set("N");
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5646);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItempfx(), smtpfxcpy.item)
		&& isEQ(itdmIO.getItemcoy(), wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(), tablesInner.t5646)
		&& isEQ(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			wsaaRegularUnitLink.set("Y");
		}
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			goTo(GotoLabel.exit5390);
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void readT56755330()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			//ILIFE-3378 System Error: Billing Changes
			//premiumrec.premMethod.set(itemIO.getItemitem());
			wsaaPremMeth.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

//ILIFE-5131 start
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covrmjaIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covrmjaIO.getChdrnum().toString());
	rcvdPFObject.setLife(covrmjaIO.getLife().toString());
	rcvdPFObject.setCoverage(covrmjaIO.getCoverage().toString());
	rcvdPFObject.setRider(covrmjaIO.getRider().toString());
	rcvdPFObject.setCrtable(covrmjaIO.getCrtable().toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		premiumrec.dialdownoption.set(100);
}
//ILIFE-5131 end

protected void initPremiumrec5350()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.mop.set(sv.mop);
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(covrmjaIO.getSumins());
		premiumrec.calcPrem.set(covrmjaIO.getInstprem());
		if (rerateComp.isTrue()) {
			traceLastRerateDate5500();
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastRerateDate);
			calcAge5600();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5600();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.jlage.set(wsaaJlAnbAtCcd);
			}
		}
		premiumrec.lsex.set(wsaaMlSex);
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		annyIO.setRecKeyData(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.language.set(wsspcomn.language);
		wsaaCrtable.set(covrmjaIO.getCrtable());
		/*  Read TR686 for waiver code*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr686);
		itemIO.setItemitem(wsaaCrtable);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr686rec.tr686Rec.set(itemIO.getGenarea());
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			tr686rec.waivercode.set(SPACES);
		}
		initialize(pr676cpy.parmRecord);
		hbnfIO.setRecKeyData(SPACES);
		hbnfIO.setChdrcoy(covrmjaIO.getChdrcoy());
		hbnfIO.setChdrnum(covrmjaIO.getChdrnum());
		hbnfIO.setLife(covrmjaIO.getLife());
		hbnfIO.setCoverage(covrmjaIO.getCoverage());
		hbnfIO.setRider(covrmjaIO.getRider());
		hbnfIO.setFormat(formatsInner.hbnfrec);
		hbnfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isEQ(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(tr686rec.waivercode, SPACES)) {
			premiumrec.benpln.set(hbnfIO.getBenpln());
			pr676cpy.mortcls.set(hbnfIO.getMortcls());
			pr676cpy.livesno.set(hbnfIO.getLivesno());
			pr676cpy.waiverprem.set(hbnfIO.getWaiverprem());
			pr676cpy.waiverCrtable.set(tr686rec.waivercode);
			pr676cpy.benpln.set(hbnfIO.getBenpln());
			wsaaHbnfZunit.set(hbnfIO.getZunit());
			premiumrec.liveno.set(hbnfIO.getLivesno());
		}
		else {
			premiumrec.benpln.set(SPACES);
			wsaaHbnfZunit.set(1);
		}
		compute(wsaaAnnualPremium, 2).set(mult(covrmjaIO.getInstprem(), wsaaOrigBillfreq9));
		compute(wsaaAnnualSumins, 2).set(mult(covrmjaIO.getSumins(), wsaaOrigBillfreq9));
		/*  Calc Yearly Premium.*/
		/*  Call Premium Calc Subr.*/
		//ILIFE-5131 start
		callReadRCVDPF();
		if(rcvdPFObject != null){
			premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
			premiumrec.bentrm.set(rcvdPFObject.getBentrm());
			if(rcvdPFObject.getPrmbasis()!=null && isEQ(rcvdPFObject.getPrmbasis(),"S"))
				premiumrec.prmbasis.set("Y");
			else
				premiumrec.prmbasis.set("");
			premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
			dialdownFlag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
			incomeProtectionflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
			premiumflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
			if(incomeProtectionflag || premiumflag || dialdownFlag){			
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					if(rcvdPFObject.getDialdownoption().startsWith("0")) {
						premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption().substring(1));
					}
					else {
						premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
					}
				}
			}
			premiumrec.tpdtype.set(covrmjaIO.getTpdtype().charAt(5));
		}
		//ILIFE-5131 end	
		prevfreq.set(premiumrec.billfreq);
		premiumrec.updateRequired.set("N");	// ILIFE-7584
		premiumrec.billfreq.set("01");
		wsaaBillfreq.set(premiumrec.billfreq);
		if (regularUnitLink.isTrue()) {
			compute(premiumrec.calcPrem, 2).set(div(wsaaAnnualPremium, wsaaBillfreq9));
			compute(premiumrec.sumin, 2).set(div(wsaaAnnualSumins, wsaaBillfreq9));
		}
		if(stampDutyflag) {
			premiumrec.zstpduty01.set(ZERO);
			premiumrec.rstate01.set(SPACE);
		}
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq01Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			if (isEQ(premiumrec.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))//ILIFE-7584
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec);
				}
				else
				{				
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);	
					/*ILIFE-8537 - Start*/
					if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
						hpadRead100();
						m900CheckRedtrm();
						vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
						compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
						vpxlextrec.prat.set(vpxlextrec.tempprat);						
						compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
						vpxlextrec.coverc.set(vpxlextrec.temptoc);						
						compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
						vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
						vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
					}					
					//START OF ILIFE-7584
					premiumrec.cownnum.set(SPACES);
					premiumrec.occdate.set(ZERO);
					if("PMEX".equals(t5675rec.premsubr.toString().trim())){
						premiumrec.setPmexCall.set("Y");
						premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
						//ILIFE-8537-starts
						premiumrec.commTaxInd.set("Y");
						premiumrec.prevSumIns.set(ZERO);					
						if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
							premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
						}else{
							premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
						}						
						premiumrec.inputPrevPrem.set(ZERO);
						//ILIFE-8537-ends
						premiumrec.validflag.set("Y");
						premiumrec.cownnum.set(chdrmjaIO.getCownnum());
						premiumrec.occdate.set(chdrmjaIO.getOccdate());
					}
					
					//END OF ILIFE-7584
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
				}
			}
			else {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
				}
				else
				{				
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					//ILIFE-2465 Sr676_Hospital & Surgical Benefit
					callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
				}
				
			}
			/*Ticket #IVE-792 - End*/
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				// Commented for MIBT-122
				// fatalError600();
			}
			/*     MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM               */
			/*     MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM           */
			/*     MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM           */
			compute(sv.prmtrb01, 2).set(mult(premiumrec.calcPrem, wsaaHbnfZunit));
			sv.basprm01.set(premiumrec.calcBasPrem);
			sv.zloprm01.set(premiumrec.calcLoaPrem);
			if(stampDutyflag) {
				wsaaZstpduty01.add(premiumrec.zstpduty01);
				zstpduty01List.add(premiumrec.zstpduty01.getbigdata());
				zclstate01List.add(premiumrec.rstate01.toString());
			}
			if (regularUnitLink.isTrue()) {
				sv.basprm01.set(premiumrec.calcPrem);
				sv.sumins01.set(premiumrec.sumin);
				sv.premind.set("Y");
			}
		}
		else {
			sv.prmtrb01.set(ZERO);
			sv.basprm01.set(ZERO);
			sv.zloprm01.set(ZERO);
			if(stampDutyflag) {
				zstpduty01List.add(BigDecimal.ZERO);
				zclstate01List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Half Yearly Premium.*/
		/*  Call Premium Calc Subr.*/
		prevfreq.set(premiumrec.billfreq);
		premiumrec.billfreq.set("02");
		wsaaBillfreq.set(premiumrec.billfreq);
		if (regularUnitLink.isTrue()) {
			compute(premiumrec.calcPrem, 2).set(div(wsaaAnnualPremium, wsaaBillfreq9));
			compute(premiumrec.sumin, 2).set(div(wsaaAnnualSumins, wsaaBillfreq9));
		}
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq02Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			if (isEQ(premiumrec.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec);
				}
				else
				{				
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					/*ILIFE-8537 - Start*/
					if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
						hpadRead100();
						m900CheckRedtrm();
						vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
						compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
						vpxlextrec.prat.set(vpxlextrec.tempprat);						
						compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
						vpxlextrec.coverc.set(vpxlextrec.temptoc);						
						compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
						vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
						vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
					}
					/*ILIFE-8537 - End*/
					// ILIFE-7584
					if("PMEX".equals(t5675rec.premsubr.toString().trim())){
						premiumrec.setPmexCall.set("Y");
						premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
						//ILIFE-8537-starts
						premiumrec.commTaxInd.set("Y");
						premiumrec.prevSumIns.set(ZERO);					
						if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
							premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
						}else{
							premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
						}						
						premiumrec.inputPrevPrem.set(ZERO);
						//ILIFE-8537-ends
						premiumrec.validflag.set("Y");
					}
					// ILIFE-7584
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
				}
			}
			else {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
				}
				else
				{				

					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					//ILIFE-2465 Sr676_Hospital & Surgical Benefit
					callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
				}
			}
			/*Ticket #IVE-792 - End*/
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				// Commented for MIBT-122
				// fatalError600();
			}
			/*     MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM               */
			/*     MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM           */
			/*     MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM           */
			compute(sv.prmtrb02, 2).set(mult(premiumrec.calcPrem, wsaaHbnfZunit));
			sv.basprm02.set(premiumrec.calcBasPrem);
			sv.zloprm02.set(premiumrec.calcLoaPrem);
			if(stampDutyflag) {
				wsaaZstpduty02.add(premiumrec.zstpduty01);
				zstpduty02List.add(premiumrec.zstpduty01.getbigdata());
				zclstate02List.add(premiumrec.rstate01.toString());
			}
			if (regularUnitLink.isTrue()) {
				sv.basprm02.set(premiumrec.calcPrem);
				sv.sumins02.set(premiumrec.sumin);
				sv.premind.set("Y");
			}
		}
		else {
			sv.prmtrb02.set(ZERO);
			sv.basprm02.set(ZERO);
			sv.zloprm02.set(ZERO);		
			if(stampDutyflag) {
				zstpduty02List.add(BigDecimal.ZERO);
				zclstate02List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Quarterly Premium.*/
		/*  Call Premium Calc Subr.*/
		prevfreq.set(premiumrec.billfreq);
		premiumrec.billfreq.set("04");
		wsaaBillfreq.set(premiumrec.billfreq);
		if (regularUnitLink.isTrue()) {
			compute(premiumrec.calcPrem, 2).set(div(wsaaAnnualPremium, wsaaBillfreq9));
			compute(premiumrec.sumin, 2).set(div(wsaaAnnualSumins, wsaaBillfreq9));
		}
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq04Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			if (isEQ(premiumrec.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec);
				}
				else
				{
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					/*ILIFE-8537 - Start*/
					if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
						hpadRead100();
						m900CheckRedtrm();
						vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
						compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
						vpxlextrec.prat.set(vpxlextrec.tempprat);						
						compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
						vpxlextrec.coverc.set(vpxlextrec.temptoc);						
						compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
						vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
						vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
					}
					/*ILIFE-8537 - End*/
					// ILIFE-7584
					if("PMEX".equals(t5675rec.premsubr.toString().trim())){
						premiumrec.setPmexCall.set("Y");
						premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
						//ILIFE-8537-starts
						premiumrec.commTaxInd.set("Y");
						premiumrec.prevSumIns.set(ZERO);					
						if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
							premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
						}else{
							premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
						}						
						premiumrec.inputPrevPrem.set(ZERO);
						//ILIFE-8537-ends
						premiumrec.validflag.set("Y");
					}
					// ILIFE-7584
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
				}
			}
			else {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
				}
				else
				{
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					//ILIFE-2465 Sr676_Hospital & Surgical Benefit
					callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
				}

			}
			/*Ticket #IVE-792 - End*/
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				// Commented for MIBT-122
				// fatalError600();
			}
			/*     MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM               */
			/*     MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM           */
			/*     MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM           */
			compute(sv.prmtrb03, 2).set(mult(premiumrec.calcPrem, wsaaHbnfZunit));
			sv.basprm03.set(premiumrec.calcBasPrem);
			sv.zloprm03.set(premiumrec.calcLoaPrem);			
			if(stampDutyflag) {
				wsaaZstpduty03.add(premiumrec.zstpduty01);
				zstpduty03List.add(premiumrec.zstpduty01.getbigdata());
				zclstate03List.add(premiumrec.rstate01.toString());
			}
			if (regularUnitLink.isTrue()) {
				sv.basprm03.set(premiumrec.calcPrem);
				sv.sumins03.set(premiumrec.sumin);
				sv.premind.set("Y");
			}
		}
		else {
			sv.prmtrb03.set(ZERO);
			sv.basprm03.set(ZERO);
			sv.zloprm03.set(ZERO);
			if(stampDutyflag) {
			zstpduty03List.add(BigDecimal.ZERO);
				zclstate03List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Monthly Premium.*/
		/*  Call Premium Calc Subr.*/
		prevfreq.set(premiumrec.billfreq);
		premiumrec.billfreq.set("12");
		wsaaBillfreq.set(premiumrec.billfreq);
		if (regularUnitLink.isTrue()) {
			compute(premiumrec.calcPrem, 2).set(div(wsaaAnnualPremium, wsaaBillfreq9));
			compute(premiumrec.sumin, 2).set(div(wsaaAnnualSumins, wsaaBillfreq9));
		}
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq12Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			if (isEQ(premiumrec.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec);
				}
				else
				{				
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					/*ILIFE-8537 - Start*/
					if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
						hpadRead100();
						m900CheckRedtrm();
						vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
						compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
						vpxlextrec.prat.set(vpxlextrec.tempprat);						
						compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
						vpxlextrec.coverc.set(vpxlextrec.temptoc);						
						compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
						vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
						vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
					}
					/*ILIFE-8537 - End*/
					// ILIFE-7584
					if("PMEX".equals(t5675rec.premsubr.toString().trim())){
						premiumrec.setPmexCall.set("Y");
						premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
						//ILIFE-8537-starts
						premiumrec.commTaxInd.set("Y");
						premiumrec.prevSumIns.set(ZERO);					
						if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
							premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
						}else{
							premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
						}						
						premiumrec.inputPrevPrem.set(ZERO);
						//ILIFE-8537-ends
						premiumrec.validflag.set("Y");
					}
					// ILIFE-7584
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
				}
			}
			else {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
				}
				else
				{				
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					//ILIFE-2465 Sr676_Hospital & Surgical Benefit
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
				}
			}
			/*Ticket #IVE-792 - End*/
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				// Commented for MIBT-122
				// fatalError600();
			}
			/*     MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM               */
			/*     MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM           */
			/*     MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM           */
			compute(sv.prmtrb04, 2).set(mult(premiumrec.calcPrem, wsaaHbnfZunit));
			sv.basprm04.set(premiumrec.calcBasPrem);
			sv.zloprm04.set(premiumrec.calcLoaPrem);		
			if(stampDutyflag) {
				wsaaZstpduty04.add(premiumrec.zstpduty01);
				zstpduty04List.add(premiumrec.zstpduty01.getbigdata());
				zclstate04List.add(premiumrec.rstate01.toString());
			}
			if (regularUnitLink.isTrue()) {
				sv.basprm04.set(premiumrec.calcPrem);
				sv.sumins04.set(premiumrec.sumin);
				sv.premind.set("Y");
			}
		}
		else {
			sv.prmtrb04.set(ZERO);
			sv.basprm04.set(ZERO);
			sv.zloprm04.set(ZERO);
			if(stampDutyflag) {
				zstpduty04List.add(BigDecimal.ZERO);
				zclstate04List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Fortnightly Premium.*/
		/*  Call Premium Calc Subr.*/
		prevfreq.set(premiumrec.billfreq);
		premiumrec.billfreq.set("26");
		wsaaBillfreq.set(premiumrec.billfreq);
		if (regularUnitLink.isTrue()) {
			compute(premiumrec.calcPrem, 2).set(div(wsaaAnnualPremium, wsaaBillfreq9));
			compute(premiumrec.sumin, 2).set(div(wsaaAnnualSumins, wsaaBillfreq9));
		}
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq26Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			if (isEQ(premiumrec.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec);
				}
				else
				{				
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					/*ILIFE-8537 - Start*/
					if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
						hpadRead100();
						m900CheckRedtrm();
						vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
						compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
						vpxlextrec.prat.set(vpxlextrec.tempprat);						
						compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
						vpxlextrec.coverc.set(vpxlextrec.temptoc);						
						compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
						vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
						vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
					}
					/*ILIFE-8537 - End*/
					// ILIFE-7584
					if("PMEX".equals(t5675rec.premsubr.toString().trim())){
						premiumrec.setPmexCall.set("Y");
						premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
						//ILIFE-8537-starts
						premiumrec.commTaxInd.set("Y");
						premiumrec.prevSumIns.set(ZERO);					
						if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
							premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
						}else{
							premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
						}						
						premiumrec.inputPrevPrem.set(ZERO);
						//ILIFE-8537-ends
						premiumrec.validflag.set("Y");
					}
					// ILIFE-7584
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
				}
			}
			else {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
				{
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
				}
				else
				{				
					Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
					Vpxlextrec vpxlextrec = new Vpxlextrec();
					vpxlextrec.function.set("INIT");
					callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
					
					Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
					vpxchdrrec.function.set("INIT");
					callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
					premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
					Vpxacblrec vpxacblrec=new Vpxacblrec();
					callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
					//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
					//ILIFE-3378 System Error: Billing Changes
					premiumrec.premMethod.set(wsaaPremMeth);
					//ILIFE-2465 Sr676_Hospital & Surgical Benefit
					callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
				}
				
			}
			/*Ticket #IVE-792 - End*/
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				// Commented for MIBT-122
				// fatalError600();
			}
			/*     MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM               */
			/*     MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM           */
			/*     MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN               */
			/*     PERFORM A000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM           */
			compute(sv.prmtrb05, 2).set(mult(premiumrec.calcPrem, wsaaHbnfZunit));
			sv.basprm05.set(premiumrec.calcBasPrem);
			sv.zloprm05.set(premiumrec.calcLoaPrem);		
			if(stampDutyflag) {
				wsaaZstpduty05.add(premiumrec.zstpduty01);
				zstpduty05List.add(premiumrec.zstpduty01.getbigdata());
				zclstate05List.add(premiumrec.rstate01.toString());
			}
			if (regularUnitLink.isTrue()) {
				sv.basprm05.set(premiumrec.calcPrem);
				sv.sumins05.set(premiumrec.sumin);
				sv.premind.set("Y");
			}
		}
		else {
			sv.prmtrb05.set(ZERO);
			sv.basprm05.set(ZERO);
			sv.zloprm05.set(ZERO);
			if(stampDutyflag) {
				zstpduty05List.add(BigDecimal.ZERO);
				zclstate05List.add(premiumrec.rstate01.toString());
			}
		}
	}

protected void calcFlexiblePremium5400()
	{
		/*START*/
		/* Calc Yearly premium.*/
		compute(sv.prmtrb01, 2).set(mult(covrmjaIO.getInstprem(), wsaaOrigBillfreq9));
		/* Calc Half Yearly premium.*/
		compute(sv.prmtrb02, 2).set(div(mult(covrmjaIO.getInstprem(), wsaaOrigBillfreq9), 2));
		/* Calc Quarterly premium.*/
		compute(sv.prmtrb03, 2).set(div(mult(covrmjaIO.getInstprem(), wsaaOrigBillfreq9), 4));
		/* Calc Monthly premium.*/
		compute(sv.prmtrb04, 2).set(div(mult(covrmjaIO.getInstprem(), wsaaOrigBillfreq9), 12));
		/* Calc Fortnightly premium.*/
		compute(sv.prmtrb05, 2).set(div(mult(covrmjaIO.getInstprem(), wsaaOrigBillfreq9), 26));
		/*EXIT*/
	}

protected void traceLastRerateDate5500()
	{
	    if(billChgFlag) {
	    	start5510ForBilltmpChg();
	    }else {
		    start5510();
	    }
	}

protected void start5510()
	{
		wsaaLastRerateDate.set(covrmjaIO.getRerateDate());
		datcon4rec.billday.set(payrIO.getDuedd());
		datcon4rec.billmonth.set(payrIO.getDuemm());
		compute(datcon4rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
		datcon4rec.frequency.set("01");
		while ( !(isLTE(wsaaLastRerateDate, sv.ptdate))) {
			datcon4rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastRerateDate.set(datcon4rec.intDate2);
		}

		if (isLT(wsaaLastRerateDate, covrmjaIO.getCrrcd())) {
			wsaaLastRerateDate.set(covrmjaIO.getCrrcd());
		}
	}
/*ILIFE-7343 starts*/

protected void start5510ForBilltmpChg()
{
	wsaaLastRerateDate.set(covrmjaIO.getRerateDate());
	
	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate2.set(ZERO);
	datcon2rec.frequency.set(freqcpy.yrly);
	compute(datcon2rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
		
	while ( !(isLTE(wsaaLastRerateDate, sv.ptdate))) {
		datcon2rec.intDate1.set(wsaaLastRerateDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaLastRerateDate.set(datcon2rec.intDate2);
	}

	if (isLT(wsaaLastRerateDate, covrmjaIO.getCrrcd())) {
		wsaaLastRerateDate.set(covrmjaIO.getCrrcd());
	}
}

protected void setPrevFrequency(String currFreq)
{
	chdrpf = new Chdrpf();
	chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(), covrmjaIO.getChdrnum().toString());
	if(chdrpf!=null && isNE(currFreq,chdrpf.getBillfreq())){
		if(i==1)
			premiumrec.prevBillFreq.set(chdrpf.getBillfreq());
		else
			premiumrec.prevBillFreq.set(prevfreq);
	}	
	else
		premiumrec.prevBillFreq.set(prevfreq);		
	i++;
}
/*ILIFE-7343 ends*/
protected void calcAge5600()
	{
		/*START*/
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			syserrrec.params.set(agecalcrec.agecalcRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcContFee6000()
	{
		try {
			init6010();
			readT56886020();
			readT56746030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void init6010()
	{
		wsaaContFee01.set(ZERO);
		wsaaContFee02.set(ZERO);
		wsaaContFee03.set(ZERO);
		wsaaContFee04.set(ZERO);
		wsaaContFee05.set(ZERO);
	}

protected void readT56886020()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isEQ(t5688rec.feemeth, SPACES)) {
			goTo(GotoLabel.exit6090);
		}
	}

protected void readT56746030()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		initialize(mgfeelrec.mgfeelRec);
		mgfeelrec.cnttype.set(chdrmjaIO.getCnttype());
		mgfeelrec.effdate.set(chdrmjaIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrmjaIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		/* Calc fee yearly.*/
		mgfeelrec.billfreq.set("01");
		if (freq01Allow.isTrue()) {
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, varcom.oK)
			&& isNE(mgfeelrec.statuz, varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				syserrrec.statuz.set(mgfeelrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(mgfeelrec.mgfee);
			a000CallRounding();
			mgfeelrec.mgfee.set(zrdecplrec.amountOut);
			wsaaContFee01.set(mgfeelrec.mgfee);
		}
		else {
			wsaaContFee01.set(ZERO);
		}
		/* Calc fee half yearly.*/
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.billfreq.set("02");
		if (freq02Allow.isTrue()) {
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, varcom.oK)
			&& isNE(mgfeelrec.statuz, varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				syserrrec.statuz.set(mgfeelrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(mgfeelrec.mgfee);
			a000CallRounding();
			mgfeelrec.mgfee.set(zrdecplrec.amountOut);
			wsaaContFee02.set(mgfeelrec.mgfee);
		}
		else {
			wsaaContFee02.set(ZERO);
		}
		/* Calc fee quarterly.*/
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.billfreq.set("04");
		if (freq04Allow.isTrue()) {
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, varcom.oK)
			&& isNE(mgfeelrec.statuz, varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				syserrrec.statuz.set(mgfeelrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(mgfeelrec.mgfee);
			a000CallRounding();
			mgfeelrec.mgfee.set(zrdecplrec.amountOut);
			wsaaContFee03.set(mgfeelrec.mgfee);
		}
		else {
			wsaaContFee03.set(ZERO);
		}
		/* Calc fee monthly.*/
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.billfreq.set("12");
		if (freq12Allow.isTrue()) {
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, varcom.oK)
			&& isNE(mgfeelrec.statuz, varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				syserrrec.statuz.set(mgfeelrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(mgfeelrec.mgfee);
			a000CallRounding();
			mgfeelrec.mgfee.set(zrdecplrec.amountOut);
			wsaaContFee04.set(mgfeelrec.mgfee);
		}
		else {
			wsaaContFee04.set(ZERO);
		}
		/* Calc fee fortnightly.*/
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.billfreq.set("26");
		if (freq26Allow.isTrue()) {
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, varcom.oK)
			&& isNE(mgfeelrec.statuz, varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				syserrrec.statuz.set(mgfeelrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(mgfeelrec.mgfee);
			a000CallRounding();
			mgfeelrec.mgfee.set(zrdecplrec.amountOut);
			wsaaContFee05.set(mgfeelrec.mgfee);
		}
		else {
			wsaaContFee05.set(ZERO);
		}
		/* Calc tax on contract fee                                        */
		calcContTax8300();
	}

protected void calcWopPremium7000()
	{
		findWopComp7010();
	}

protected void findWopComp7010()
	{
		wsaaWopCompRrn.set(ZERO);
		scrnparams.statuz.set(varcom.oK);
		for (iz.set(1); !(isNE(scrnparams.statuz, varcom.oK)); iz.add(1)){
			scrnparams.subfileRrn.set(iz);
			scrnparams.function.set(varcom.sread);
			processScreen("SR674", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.mrnf)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			if (isEQ(scrnparams.statuz, varcom.oK)) {
				if (isEQ(sv.activeInd, "Y")
				&& isEQ(sv.sumins01, ZERO)) {
					
					wsaaWopCompRrn.set(iz);
					sumWopSumins7100();
					if (isEQ(wsaaZrwvflg03, "Y")) {
						wsaaWaiverSumins01.add(wsaaContFee01);
						wsaaWaiverSumins02.add(wsaaContFee02);
						wsaaWaiverSumins03.add(wsaaContFee03);
						wsaaWaiverSumins04.add(wsaaContFee04);
						wsaaWaiverSumins05.add(wsaaContFee05);
					}
					wopPremium7300();
					scrnparams.statuz.set(varcom.oK);
				}
			}
		}
	}

protected void sumWopSumins7100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start7110();
				case readSfl7120:
					readSfl7120();
					waiveCheck7130();
				case nextSfl7180:
					nextSfl7180();
				case exit7190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7110()
	{
		wsaaWaiverSumins01.set(ZERO);
		wsaaWaiverSumins02.set(ZERO);
		wsaaWaiverSumins03.set(ZERO);
		wsaaWaiverSumins04.set(ZERO);
		wsaaWaiverSumins05.set(ZERO);
		wsaaWopCovrmjakey.set(sv.datakey);
		wsaaCtables.set(sv.workAreaData);
		wsaaZrwvflg02.set(sv.zrwvflg02);
		wsaaZrwvflg03.set(sv.zrwvflg03);
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl7120()
	{
		processScreen("SR674", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(scrnparams.statuz, varcom.oK)) {
			goTo(GotoLabel.exit7190);
		}
		if (isEQ(scrnparams.subfileRrn, wsaaWopCompRrn)) {
			goTo(GotoLabel.nextSfl7180);
		}
	}

protected void waiveCheck7130()
	{
		wsaaCovrmjakey.set(sv.datakey);
		if (isNE(wsaaZrwvflg02, "Y")) {
			if (isNE(wsaaCovrmjakey.covrmjaLife, wsaaWopCovrmjakey.covrmjaLife)) {
				goTo(GotoLabel.nextSfl7180);
			}
		}
		wsaaWaiveIt.set("N");
		for (ix.set(1); !(isGT(ix, 500)
		|| waiveIt.isTrue()
		|| isEQ(wsaaCtable[ix.toInt()], SPACES)); ix.add(1)){
			if (isEQ(sv.crtable, wsaaCtable[ix.toInt()])) {
				wsaaWaiveIt.set("Y");
			}
		}
		/* Not waive, skip.*/
		if (!waiveIt.isTrue()) {
			goTo(GotoLabel.nextSfl7180);
		}
		/* None Accelerated Crisis Waiver, just add the premium as WOP*/
		/* sum assured.*/
		if (isNE(tr517rec.zrwvflg04, "Y")) {
			wsaaWaiverSumins01.add(sv.prmtrb01);
			wsaaWaiverSumins02.add(sv.prmtrb02);
			wsaaWaiverSumins03.add(sv.prmtrb03);
			wsaaWaiverSumins04.add(sv.prmtrb04);
			wsaaWaiverSumins05.add(sv.prmtrb05);
			goTo(GotoLabel.nextSfl7180);
		}
		/* Accelerated Crisis Waiver, .....*/
		if (isNE(wsaaCovrmjakey.covrmjaLife, wsaaWopCovrmjakey.covrmjaLife)) {
			goTo(GotoLabel.nextSfl7180);
		}
		if (isNE(wsaaCovrmjakey.covrmjaCoverage, wsaaWopCovrmjakey.covrmjaCoverage)) {
			goTo(GotoLabel.exit7190);
		}
		if (isEQ(wsaaCovrmjakey.covrmjaRider, "00")) {
			wsaaMainCovrmjakey.set(sv.datakey);
			wsaaWaiverSumins01.set(sv.sumins01);
			wsaaWaiverSumins02.set(sv.sumins02);
			wsaaWaiverSumins03.set(sv.sumins03);
			wsaaWaiverSumins04.set(sv.sumins04);
			wsaaWaiverSumins05.set(sv.sumins05);
		}
		else {
			wsaaWaiverSumins01.subtract(sv.sumins01);
			wsaaWaiverSumins02.subtract(sv.sumins02);
			wsaaWaiverSumins03.subtract(sv.sumins03);
			wsaaWaiverSumins04.subtract(sv.sumins04);
			wsaaWaiverSumins05.subtract(sv.sumins05);
			calcBenefitAmount7200();
		}
	}

protected void nextSfl7180()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl7120);
	}

protected void calcBenefitAmount7200()
	{
		try {
			readMainCoverage7210();
			lifeAndJointLife7220();
			readT56877230();
			readT56757240();
			initPremiumrec7250();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readMainCoverage7210()
	{
		covrmjaIO.setDataKey(wsaaMainCovrmjakey);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void lifeAndJointLife7220()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		wsaaMlSex.set(lifemjaIO.getCltsex());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void readT56877230()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			goTo(GotoLabel.exit7290);
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void readT56757240()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			
			//ILIFE-3378 System Error: Billing Changes
			//premiumrec.premMethod.set(itemIO.getItemitem());
			wsaaPremMeth.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void initPremiumrec7250()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.mop.set(sv.mop);
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		if (rerateComp.isTrue()) {
			traceLastRerateDate5500();
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastRerateDate);
			calcAge5600();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5600();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.jlage.set(wsaaJlAnbAtCcd);
			}
		}
		premiumrec.lsex.set(wsaaMlSex);
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		annyIO.setRecKeyData(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.language.set(wsspcomn.language);
		/*  Calc Yearly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins01);
		premiumrec.updateRequired.set("N");// ILIFE-7584
		premiumrec.billfreq.set("01");
		if(stampDutyflag) {
			premiumrec.zstpduty01.set(ZERO);
			premiumrec.rstate01.set(SPACE);
		}
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq01Allow.isTrue()) {
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				premiumrec.cownnum.set(SPACES);
				premiumrec.occdate.set(ZERO);
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
					premiumrec.cownnum.set(chdrmjaIO.getCownnum());
					premiumrec.occdate.set(chdrmjaIO.getOccdate());
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			wsaaWaiverSumins01.set(premiumrec.calcPrem);
			if(stampDutyflag) {
				wsaaZstpduty01.add(premiumrec.zstpduty01);
				zstpduty01List.add(premiumrec.zstpduty01.getbigdata());
				zclstate01List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			wsaaWaiverSumins01.set(ZERO);
		}
		/*  Calc age at half yearly effdate.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins02);
		premiumrec.billfreq.set("02");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq02Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			wsaaWaiverSumins02.set(premiumrec.calcPrem);
			if(stampDutyflag) {
				wsaaZstpduty02.add(premiumrec.zstpduty01);
				zstpduty02List.add(premiumrec.zstpduty01.getbigdata());
				zclstate02List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			wsaaWaiverSumins02.set(ZERO);
		}
		/*  Calc Quarterly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins03);
		premiumrec.billfreq.set("04");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq04Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			wsaaWaiverSumins03.set(premiumrec.calcPrem);
			if(stampDutyflag) {
				wsaaZstpduty03.add(premiumrec.zstpduty01);
				zstpduty03List.add(premiumrec.zstpduty01.getbigdata());
				zclstate03List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			wsaaWaiverSumins03.set(ZERO);
		}
		/*  Calc Monthly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins04);
		premiumrec.billfreq.set("12");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq12Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			wsaaWaiverSumins04.set(premiumrec.calcPrem);
			if(stampDutyflag) {
				wsaaZstpduty04.add(premiumrec.zstpduty01);
				zstpduty04List.add(premiumrec.zstpduty01.getbigdata());
				zclstate04List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			wsaaWaiverSumins04.set(ZERO);
		}
		/*  Calc Fortnightly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins04);
		premiumrec.billfreq.set("26");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq26Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			wsaaWaiverSumins05.set(premiumrec.calcPrem);
			if(stampDutyflag) {
				wsaaZstpduty05.add(premiumrec.zstpduty01);
				zstpduty05List.add(premiumrec.zstpduty01.getbigdata());
				zclstate05List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			wsaaWaiverSumins05.set(ZERO);
		}
	}

protected void wopPremium7300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readWopCoverage7310();
					sreadForWopCoverage7320();
					lifeAndJointLife7330();
					readT56877340();
					readT56757350();
					initPremiumrec7360();
				case supdForWopCoverage7370:
					supdForWopCoverage7370();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readWopCoverage7310()
	{
		covrmjaIO.setDataKey(wsaaWopCovrmjakey);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void sreadForWopCoverage7320()
	{
		scrnparams.subfileRrn.set(wsaaWopCompRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("SR674", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void lifeAndJointLife7330()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		wsaaMlSex.set(lifemjaIO.getCltsex());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void readT56877340()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			/* Need to update BB WOP rider's SI                                */
			if (freq01Allow.isTrue()) {
				sv.sumins01.set(wsaaWaiverSumins01);
			}
			if (freq02Allow.isTrue()) {
				sv.sumins02.set(wsaaWaiverSumins02);
			}
			if (freq04Allow.isTrue()) {
				sv.sumins03.set(wsaaWaiverSumins03);
			}
			if (freq12Allow.isTrue()) {
				sv.sumins04.set(wsaaWaiverSumins04);
			}
			if (freq26Allow.isTrue()) {
				sv.sumins05.set(wsaaWaiverSumins05);
			}
			goTo(GotoLabel.supdForWopCoverage7370);
			/**       GO TO 7390-EXIT                                           */
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void readT56757350()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			//ILIFE-3378 System Error: Billing Changes
			//premiumrec.premMethod.set(itemIO.getItemitem());
			wsaaPremMeth.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void initPremiumrec7360()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.mop.set(sv.mop);
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.calcPrem.set(covrmjaIO.getInstprem());
		if (rerateComp.isTrue()) {
			traceLastRerateDate5500();
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastRerateDate);
			calcAge5600();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5600();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.jlage.set(wsaaJlAnbAtCcd);
			}
		}
		premiumrec.lsex.set(wsaaMlSex);
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		annyIO.setRecKeyData(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.language.set(wsspcomn.language);
		/*  Calc Yearly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins01);
		premiumrec.updateRequired.set("N");// ILIFE-7584
		premiumrec.billfreq.set("01");
		if(stampDutyflag) {
			premiumrec.zstpduty01.set(ZERO);
			premiumrec.rstate01.set(SPACE);
		}
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq01Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.calcPrem.set(SPACES);
				premiumrec.premMethod.set(wsaaPremMeth);
				premiumrec.cownnum.set(SPACES);
				premiumrec.occdate.set(ZERO);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
					premiumrec.cownnum.set(chdrmjaIO.getCownnum());
					premiumrec.occdate.set(chdrmjaIO.getOccdate());
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			sv.sumins01.set(wsaaWaiverSumins01);
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			/*    MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM            */
			/*    MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM            */
			sv.prmtrb01.set(premiumrec.calcPrem);
			sv.basprm01.set(premiumrec.calcBasPrem);
			sv.zloprm01.set(premiumrec.calcLoaPrem);		
			if(stampDutyflag) {
				wsaaZstpduty01.add(premiumrec.zstpduty01);
				zstpduty01List.add(premiumrec.zstpduty01.getbigdata());
				zclstate01List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			sv.sumins01.set(ZERO);
			sv.prmtrb01.set(ZERO);
			sv.basprm01.set(ZERO);
			sv.zloprm01.set(ZERO);
			if(stampDutyflag) {
				zstpduty01List.add(BigDecimal.ZERO);
				zclstate01List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Half Yearly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins02);
		premiumrec.billfreq.set("02");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq02Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.calcPrem.set(SPACES);
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			sv.sumins02.set(wsaaWaiverSumins02);
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			/*    MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM            */
			/*    MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM            */
			sv.prmtrb02.set(premiumrec.calcPrem);
			sv.basprm02.set(premiumrec.calcBasPrem);
			sv.zloprm02.set(premiumrec.calcLoaPrem);			
			if(stampDutyflag) {
				wsaaZstpduty02.add(premiumrec.zstpduty01);
				zstpduty02List.add(premiumrec.zstpduty01.getbigdata());
				zclstate02List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			sv.sumins02.set(ZERO);
			sv.prmtrb02.set(ZERO);
			sv.basprm02.set(ZERO);
			sv.zloprm02.set(ZERO);
			if(stampDutyflag) {
				zstpduty02List.add(BigDecimal.ZERO);
				zclstate02List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Quarterly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins03);
		premiumrec.billfreq.set("04");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq04Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX"))) // ILIFE-7584
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.calcPrem.set(SPACES);
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			sv.sumins03.set(wsaaWaiverSumins03);
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			/*    MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM            */
			/*    MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM            */
			sv.prmtrb03.set(premiumrec.calcPrem);
			sv.basprm03.set(premiumrec.calcBasPrem);
			sv.zloprm03.set(premiumrec.calcLoaPrem);
			if(stampDutyflag) {
				wsaaZstpduty03.add(premiumrec.zstpduty01);
				zstpduty03List.add(premiumrec.zstpduty01.getbigdata());
				zclstate03List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			sv.sumins03.set(ZERO);
			sv.prmtrb03.set(ZERO);
			sv.basprm03.set(ZERO);
			sv.zloprm03.set(ZERO);
			if(stampDutyflag) {
				zstpduty03List.add(BigDecimal.ZERO);
				zclstate03List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Monthly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins04);
		premiumrec.billfreq.set("12");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq12Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.calcPrem.set(SPACES);
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			sv.sumins04.set(wsaaWaiverSumins04);
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			/*    MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM            */
			/*    MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM            */
			sv.prmtrb04.set(premiumrec.calcPrem);
			sv.basprm04.set(premiumrec.calcBasPrem);
			sv.zloprm04.set(premiumrec.calcLoaPrem);			
			if(stampDutyflag) {
				wsaaZstpduty04.add(premiumrec.zstpduty01);
				zstpduty04List.add(premiumrec.zstpduty01.getbigdata());
				zclstate04List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			sv.sumins04.set(ZERO);
			sv.prmtrb04.set(ZERO);
			sv.basprm04.set(ZERO);
			sv.zloprm04.set(ZERO);
			if(stampDutyflag) {
				zstpduty04List.add(BigDecimal.ZERO);
				zclstate04List.add(premiumrec.rstate01.toString());
			}
		}
		/*  Calc Fortnightly Premium.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins05);
		premiumrec.billfreq.set("26");
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (freq26Allow.isTrue()) {
			setPrevFrequency(premiumrec.billfreq.toString());
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//ILIFE-3378 System Error: Billing Changes
				premiumrec.calcPrem.set(SPACES);
				premiumrec.premMethod.set(wsaaPremMeth);
				/*ILIFE-8537 - Start*/
				if("LCP1".equals(covrmjaIO.getCrtable().toString().trim())){ //ILIFE-8537						
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);						
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);						
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());						
				}
				/*ILIFE-8537 - End*/
				// ILIFE-7584
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatchkey.batcBatctrcde);
					//ILIFE-8537-starts
					premiumrec.commTaxInd.set("Y");
					premiumrec.prevSumIns.set(ZERO);					
					if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
						premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
					}else{
						premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
					}						
					premiumrec.inputPrevPrem.set(ZERO);
					//ILIFE-8537-ends
					premiumrec.validflag.set("Y");
				}
				// ILIFE-7584
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End */
			if (isNE(premiumrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			sv.sumins05.set(wsaaWaiverSumins05);
			/*    MOVE CPRM-CALC-PREM      TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-PREM                */
			/*    MOVE CPRM-CALC-BAS-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-BAS-PREM            */
			/*    MOVE CPRM-CALC-LOA-PREM  TO ZRDP-AMOUNT-IN                */
			/*    PERFORM A000-CALL-ROUNDING                                */
			/*    MOVE ZRDP-AMOUNT-OUT     TO CPRM-CALC-LOA-PREM            */
			sv.prmtrb05.set(premiumrec.calcPrem);
			sv.basprm05.set(premiumrec.calcBasPrem);
			sv.zloprm05.set(premiumrec.calcLoaPrem);
			if(stampDutyflag) {
				wsaaZstpduty05.add(premiumrec.zstpduty01);
				zstpduty05List.add(premiumrec.zstpduty01.getbigdata());
				zclstate05List.add(premiumrec.rstate01.toString());
			}
		}
		else {
			sv.sumins05.set(ZERO);
			sv.prmtrb05.set(ZERO);
			sv.basprm05.set(ZERO);
			sv.zloprm05.set(ZERO);
			if(stampDutyflag) {
				zstpduty05List.add(BigDecimal.ZERO);
				zclstate05List.add(premiumrec.rstate01.toString());
			}
		}
		sv.activeInd.set("N");
		calcCompTax8200();
	}

protected void supdForWopCoverage7370()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("SR674", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcDuePremium8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					init8010();
				case readSfl8020:
					readSfl8020();
					calc8030();
					nextSfl8070();
				case totalDue8080:
					totalDue8080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init8010()
	{
		sv.zgrsamt01.set(ZERO);
		sv.zgrsamt02.set(ZERO);
		sv.zgrsamt03.set(ZERO);
		sv.zgrsamt04.set(ZERO);
		sv.zgrsamt05.set(ZERO);
		sv.totfld01.set(ZERO);
		sv.totfld02.set(ZERO);
		sv.totfld03.set(ZERO);
		sv.totfld04.set(ZERO);
		sv.totfld05.set(ZERO);
		sv.zchgamt01.set(ZERO);
		sv.zchgamt02.set(ZERO);
		sv.zchgamt03.set(ZERO);
		sv.zchgamt04.set(ZERO);
		sv.zchgamt05.set(ZERO);
		sv.amtfld01.set(ZERO);
		sv.amtfld02.set(ZERO);
		sv.amtfld03.set(ZERO);
		sv.amtfld04.set(ZERO);
		sv.amtfld05.set(ZERO);
		iy.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl8020()
	{
		processScreen("SR674", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(scrnparams.statuz, varcom.oK)) {
			goTo(GotoLabel.totalDue8080);
		}
	}

protected void calc8030()
	{
		sv.zgrsamt01.add(sv.prmtrb01);
		sv.zgrsamt02.add(sv.prmtrb02);
		sv.zgrsamt03.add(sv.prmtrb03);
		sv.zgrsamt04.add(sv.prmtrb04);
		sv.zgrsamt05.add(sv.prmtrb05);
		iy.add(1);
		wsaaCovrmjakey.set(sv.datakey);
	}

protected void nextSfl8070()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl8020);
	}

protected void totalDue8080()
	{
		sv.zchgamt01.set(wsaaContFee01);
		sv.zchgamt02.set(wsaaContFee02);
		sv.zchgamt03.set(wsaaContFee03);
		sv.zchgamt04.set(wsaaContFee04);
		sv.zchgamt05.set(wsaaContFee05);
		if(!stampDutyflag) {
			compute(sv.totfld01, 2).set(add(sv.zgrsamt01, sv.zchgamt01));
			compute(sv.totfld02, 2).set(add(sv.zgrsamt02, sv.zchgamt02));
			compute(sv.totfld03, 2).set(add(sv.zgrsamt03, sv.zchgamt03));
			compute(sv.totfld04, 2).set(add(sv.zgrsamt04, sv.zchgamt04));
			//ILIFE-4298
			compute(sv.totfld05, 2).set(add(sv.zgrsamt05, sv.zchgamt05));
		}
		else {
			compute(sv.totfld01, 2).set(add(sv.zgrsamt01, sv.zchgamt01, sv.zstpduty01));
			compute(sv.totfld02, 2).set(add(sv.zgrsamt02, sv.zchgamt02, sv.zstpduty02));
			compute(sv.totfld03, 2).set(add(sv.zgrsamt03, sv.zchgamt03, sv.zstpduty03));
			compute(sv.totfld04, 2).set(add(sv.zgrsamt04, sv.zchgamt04, sv.zstpduty04));
			//ILIFE-4298
			compute(sv.totfld05, 2).set(add(sv.zgrsamt05, sv.zchgamt05, sv.zstpduty05));
		}
		compute(sv.amtfld01, 2).set(add(add(sv.totfld01, wsaaTaxPrem01), wsaaTaxCntf01));
		compute(sv.amtfld02, 2).set(add(add(sv.totfld02, wsaaTaxPrem02), wsaaTaxCntf02));
		compute(sv.amtfld03, 2).set(add(add(sv.totfld03, wsaaTaxPrem03), wsaaTaxCntf03));
		compute(sv.amtfld05, 2).set(add(add(sv.totfld05, wsaaTaxPrem05), wsaaTaxCntf05));
		//MIBT-282
//		compute(sv.amtfld04, 2).set(add(add(sv.totfld04, wsaaTaxPrem04), wsaaTaxCntf04));
				if(!sv.totfld04.equals(ZERO))
				{
					compute(sv.amtfld04, 2).set(add(add(sv.totfld04, wsaaTaxPrem04), wsaaTaxCntf04));
				}else
				{
					wsaaTaxPrem04.set(ZERO);
					compute(sv.amtfld04, 2).set(add(add(sv.totfld04, wsaaTaxPrem04), wsaaTaxCntf04));

				}
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.amtfld01Out[varcom.nd.toInt()].set("N");
			sv.amtfld02Out[varcom.nd.toInt()].set("N");
			sv.amtfld03Out[varcom.nd.toInt()].set("N");
			sv.amtfld04Out[varcom.nd.toInt()].set("N");
			sv.amtfld05Out[varcom.nd.toInt()].set("N");
			sv.amtfld01Out[varcom.pr.toInt()].set("N");
			sv.amtfld02Out[varcom.pr.toInt()].set("N");
			sv.amtfld03Out[varcom.pr.toInt()].set("N");
			sv.amtfld04Out[varcom.pr.toInt()].set("N");
			sv.amtfld05Out[varcom.pr.toInt()].set("N");
		}
	}

protected void checkMaxminPrem8100()
	{
		start8110();
	}

protected void start8110()
	{
		for (ix.set(1); !(isGT(ix, 8)); ix.add(1)){
			if (isEQ(th611rec.frequency[ix.toInt()], "01")
			&& freq01Allow.isTrue()) {
				if (isLT(sv.totfld01, th611rec.cmin[ix.toInt()])
				|| isGT(sv.totfld01, th611rec.cmax[ix.toInt()])) {
					sv.totfld01Err.set(errorsInner.rl11);
				}
			}
			if (isEQ(th611rec.frequency[ix.toInt()], "02")
			&& freq02Allow.isTrue()) {
				if (isLT(sv.totfld02, th611rec.cmin[ix.toInt()])
				|| isGT(sv.totfld02, th611rec.cmax[ix.toInt()])) {
					sv.totfld02Err.set(errorsInner.rl11);
				}
			}
			if (isEQ(th611rec.frequency[ix.toInt()], "04")
			&& freq04Allow.isTrue()) {
				if (isLT(sv.totfld03, th611rec.cmin[ix.toInt()])
				|| isGT(sv.totfld03, th611rec.cmax[ix.toInt()])) {
					sv.totfld03Err.set(errorsInner.rl11);
				}
			}
			if (isEQ(th611rec.frequency[ix.toInt()], "12")
			&& freq12Allow.isTrue()) {
				if (isLT(sv.totfld04, th611rec.cmin[ix.toInt()])
				|| isGT(sv.totfld04, th611rec.cmax[ix.toInt()])) {
					sv.totfld04Err.set(errorsInner.rl11);
				}
			}
			if (isEQ(th611rec.frequency[ix.toInt()], "26")
			&& freq26Allow.isTrue()) {
				if (isLT(sv.totfld05, th611rec.cmin[ix.toInt()])
				|| isGT(sv.totfld05, th611rec.cmax[ix.toInt()])) {
					sv.totfld05Err.set(errorsInner.rl11);
				}
			}
		}
	}

protected void calcCompTax8200()
	{
		try {
			start8210();
			readTr52e8220();
			callTaxSubr018230();
			callTaxSubr028240();
			callTaxSubr038250();
			callTaxSubr048260();
			callTaxSubr058270();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start8210()
	{
		/* Read TR52D.                                                     */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit8290);
		}
	}

protected void readTr52e8220()
	{
		/* Read TR52E.                                                     */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		readTr52e8400();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e8400();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e8400();
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			goTo(GotoLabel.exit8290);
		}
	}

protected void callTaxSubr018230()
	{
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(covrmjaIO.getLife());
		txcalcrec.coverage.set(covrmjaIO.getCoverage());
		txcalcrec.rider.set(covrmjaIO.getRider());
		txcalcrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		txcalcrec.crtable.set(covrmjaIO.getCrtable());
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		wsaaCntCurr.set(chdrmjaIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.transType.set("PREM");
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(sv.basprm01);
		}
		else {
			txcalcrec.amountIn.set(sv.prmtrb01);
		}
		if (isEQ(txcalcrec.amountIn, 0)) {
			goTo(GotoLabel.exit8290);
		}
		txcalcrec.effdate.set(chdrmjaIO.getBtdate());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxPrem01.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxPrem01.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void callTaxSubr028240()
	{
		/* Perform tax subroutine 2nd time                                 */
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(sv.basprm02);
		}
		else {
			txcalcrec.amountIn.set(sv.prmtrb02);
		}
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxPrem02.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxPrem02.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void callTaxSubr038250()
	{
		/* Perform tax subroutine 3rd time                                 */
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(sv.basprm03);
		}
		else {
			txcalcrec.amountIn.set(sv.prmtrb03);
		}
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxPrem03.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxPrem03.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void callTaxSubr048260()
	{
		/* Perform tax subroutine 4th time                                 */
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(sv.basprm04);
		}
		else {
			txcalcrec.amountIn.set(sv.prmtrb04);
		}
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxPrem04.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxPrem04.add(txcalcrec.taxAmt[2]);
			}
		}
	}
protected void callTaxSubr058270()
{
	/* Perform tax subroutine 4th time                                 */
	if (isEQ(tr52erec.zbastyp, "Y")) {
		txcalcrec.amountIn.set(sv.basprm05);
	}
	else {
		txcalcrec.amountIn.set(sv.prmtrb05);
	}
	txcalcrec.tranno.set(ZERO);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTaxPrem05.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTaxPrem05.add(txcalcrec.taxAmt[2]);
		}
	}
}

protected void calcContTax8300()
	{
		try {
			start8310();
			callTaxSubr018320();
			callTaxSubr028330();
			callTaxSubr038340();
			callTaxSubr048350();
			callTaxSubr058360();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start8310()
	{
		wsaaTaxCntf01.set(ZERO);
		wsaaTaxCntf02.set(ZERO);
		wsaaTaxCntf03.set(ZERO);
		wsaaTaxCntf04.set(ZERO);
		wsaaTaxCntf05.set(ZERO);
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit8390);
		}
		/* Read TR52E.                                                     */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e8400();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e8400();
		}
		if (isNE(tr52erec.taxind02, "Y")) {
			goTo(GotoLabel.exit8390);
		}
	}

protected void callTaxSubr018320()
	{
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		wsaaCntCurr.set(chdrmjaIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.transType.set("CNTF");
		txcalcrec.amountIn.set(wsaaContFee01);
		txcalcrec.effdate.set(chdrmjaIO.getBtdate());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxCntf01.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxCntf01.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void callTaxSubr028330()
	{
		/* Perform tax subroutine 2nd time                                 */
		txcalcrec.amountIn.set(wsaaContFee02);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxCntf02.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxCntf02.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void callTaxSubr038340()
	{
		/* Perform tax subroutine 3RD time                                 */
		txcalcrec.amountIn.set(wsaaContFee03);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxCntf03.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxCntf03.add(txcalcrec.taxAmt[2]);
			}
		}
	}



protected void callTaxSubr048350()
	{
		/* Perform tax subroutine 4TH time                                 */
		txcalcrec.amountIn.set(wsaaContFee04);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxCntf04.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxCntf04.add(txcalcrec.taxAmt[2]);
			}
		}
	}
protected void callTaxSubr058360()
{
	/* Perform tax subroutine 3RD time                                 */
	txcalcrec.amountIn.set(wsaaContFee05);
	txcalcrec.tranno.set(ZERO);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTaxCntf05.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTaxCntf05.add(txcalcrec.taxAmt[2]);
		}
	}
}

protected void readTr52e8400()
	{
		start8410();
	}

protected void start8410()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrmjaIO.getBtdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy()))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void getUser800()
	{
		/*START*/
		initialize(wsjobDataInner.wsjobData);
		queryUserJobInfo(wsjobDataInner.wsjobData, wsjobVarlen, wsjobFormat, wsjobQualjobname, wsjobIntjobi);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		zrdecplrec.currency.set(payrIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}

protected void b100KeepsChdrmja()
	{
		b110Keeps();
	}

protected void b110Keeps()
	{
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrnum);
		chdrmjaIO.setFunction(varcom.readr);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrnum);
		chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void b200KeepsPayr()
	{
		b210Keeps();
	}

protected void b210Keeps()
	{
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.readr);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setMandref(SPACES);
		payrIO.setFunction(varcom.keeps);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void c100ReadClrf()
	{
		c110Begin();
	}

protected void c110Begin()
	{
		wsaaContract.set(chdrmjaIO.getChdrnum());
		wsaaSeqnoN.set(payrIO.getPayrseqno());
		wsaaSeqno.set(wsaaSeqnoN);
		clrfIO.setParams(SPACES);
		clrfIO.setFunction(varcom.readr);
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setForepfx(chdrmjaIO.getChdrpfx());
		clrfIO.setForecoy(chdrmjaIO.getChdrcoy());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole(wsaaPayer);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
	}
//ILIFE-8015
private String changeBillDay(String billDay) {
	String billCd=chdrmjaIO.getBillcd().toString();
	return billCd.substring(0,6)+billDay;
}

// ILIFE-8248 - Start
protected void checkLinkage() {

		if (null == covrmjaIO.getLnkgsubrefno() || covrmjaIO.getLnkgsubrefno().trim().isEmpty()) {
			premiumrec.lnkgSubRefNo.set(SPACE);
		} else {
			premiumrec.lnkgSubRefNo.set(covrmjaIO.getLnkgsubrefno().toString().trim());
		}

		if (null == covrmjaIO.getLnkgno() || covrmjaIO.getLnkgno().trim().isEmpty()) {
			premiumrec.linkcov.set(SPACE);
		} else {
				LinkageInfoService linkgService = new LinkageInfoService();
				FixedLengthStringData linkgCov = new FixedLengthStringData(
						linkgService.getLinkageInfo(covrmjaIO.getLnkgno().toString().trim()));
				premiumrec.linkcov.set(linkgCov);
		}

}

/*ILIFE-8537 : Starts*/
protected void hpadRead100()
{
	/*HPAD*/
	/* READ OFF HPAD FILE TO RETRIEVE THE PROPOSE DATE (HPAD-PROPDTE)*/
	/* USED TO ACCESS PREMIUM RATE FOR TABLE TH617*/
	
	hpadIO=hpadpfDAO.getHpadData(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
	/*EXIT*/
}

protected void m900CheckRedtrm()
{
	/*M900-START*/
	mrtaIO=mrtapfDAO.getMrtaRecord(covrmjaIO.getChdrcoy().toString(), covrmjaIO.getChdrnum().toString());
		/*M900-EXIT*/
}
/*ILIFE-8537 : Ends*/

// ILIFE-8248 - ends
/*
 * Class transformed  from Data Structure WSJOB-DATA--INNER
 */
private static final class WsjobDataInner {

	private FixedLengthStringData wsjobData = new FixedLengthStringData(101);
	private FixedLengthStringData wsjobUsername = new FixedLengthStringData(10).isAPartOf(wsjobData, 91);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
protected static final class ErrorsInner {
	private FixedLengthStringData rl13 = new FixedLengthStringData(4).init("RL13");
	private FixedLengthStringData rl12 = new FixedLengthStringData(4).init("RL12");
	private FixedLengthStringData rl11 = new FixedLengthStringData(4).init("RL11");
	private FixedLengthStringData h048 = new FixedLengthStringData(4).init("H048");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e368 = new FixedLengthStringData(4).init("E368");
	private FixedLengthStringData f136 = new FixedLengthStringData(4).init("F136");
	private FixedLengthStringData g514 = new FixedLengthStringData(4).init("G514");
	public FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData h060 = new FixedLengthStringData(4).init("H060");
	public FixedLengthStringData h062 = new FixedLengthStringData(4).init("H062");
	private FixedLengthStringData h063 = new FixedLengthStringData(4).init("H063");
	private FixedLengthStringData j008 = new FixedLengthStringData(4).init("J008");
	//	MIBT- 283 STARTS
	private FixedLengthStringData h064 = new FixedLengthStringData(4).init("H064");
//	MIBT- 283 ENDS
	private FixedLengthStringData h137= new FixedLengthStringData(4).init("H137");
	private FixedLengthStringData rfy0 = new FixedLengthStringData(4).init("RFY0");
	private FixedLengthStringData rupl = new FixedLengthStringData(4).init("RUPL");
	private FixedLengthStringData rupm = new FixedLengthStringData(4).init("RUPM");
	private FixedLengthStringData rupn = new FixedLengthStringData(4).init("RUPN");
	private FixedLengthStringData rupu = new FixedLengthStringData(4).init("RUPU");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3620 = new FixedLengthStringData(5).init("T3620");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData th611 = new FixedLengthStringData(5).init("TH611");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5689 = new FixedLengthStringData(5).init("T5689");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData tr686 = new FixedLengthStringData(5).init("TR686");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5646 = new FixedLengthStringData(5).init("T5646");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
protected static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData annyrec = new FixedLengthStringData(10).init("ANNYREC");
	private FixedLengthStringData anrlcntrec = new FixedLengthStringData(10).init("ANRLCNTREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
	private FixedLengthStringData incrhstrec = new FixedLengthStringData(10).init("INCRHSTREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
	private FixedLengthStringData lifemjarec = new FixedLengthStringData(10).init("LIFEMJAREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	public FixedLengthStringData getItemrec() {
		return itemrec;
	}
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}