/*
 * File: P5687.java
 * Date: 30 August 2009 0:34:07
 * Author: Quipoz Limited
 * 
 * Class transformed from P5687.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.procedures.T5687pt;
import com.csc.life.productdefinition.screens.S5687ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5687 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5687");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5687rec t5687rec = new T5687rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5687ScreenVars sv = ScreenProgram.getScreenVars( S5687ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5687() {
		super();
		screenVars = sv;
		new ScreenModel("S5687", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		boolean linkedTPD= FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP085", appVars, "IT");	
		if(!linkedTPD)
		{
			sv.lnkgindOut[Varcom.nd.toInt()].set("Y");
			sv.lnkgind.set("N");
		}
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5687rec.t5687Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5687rec.premGuarPeriod.set(ZERO);
		t5687rec.rtrnwfreq.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.anniversaryMethod.set(t5687rec.anniversaryMethod);
		sv.basicCommMeth.set(t5687rec.basicCommMeth);
		sv.bascpy.set(t5687rec.bascpy);
		sv.basscmth.set(t5687rec.basscmth);
		sv.basscpy.set(t5687rec.basscpy);
		sv.bastcmth.set(t5687rec.bastcmth);
		sv.bastcpy.set(t5687rec.bastcpy);
		sv.bbmeth.set(t5687rec.bbmeth);
		sv.dcmeth.set(t5687rec.dcmeth);
		sv.defFupMeth.set(t5687rec.defFupMeth);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.jlPremMeth.set(t5687rec.jlPremMeth);
		sv.jlifePresent.set(t5687rec.jlifePresent);
		sv.loanmeth.set(t5687rec.loanmeth);
		sv.maturityCalcMeth.set(t5687rec.maturityCalcMeth);
		sv.nonForfeitMethod.set(t5687rec.nonForfeitMethod);
		sv.partsurr.set(t5687rec.partsurr);
		sv.premGuarPeriod.set(t5687rec.premGuarPeriod);
		sv.premmeth.set(t5687rec.premmeth);
		sv.pumeth.set(t5687rec.pumeth);
		sv.reptcds.set(t5687rec.reptcds);
		sv.riind.set(t5687rec.riind);
		sv.rnwcpy.set(t5687rec.rnwcpy);
		sv.rtrnwfreq.set(t5687rec.rtrnwfreq);
		sv.schedCode.set(t5687rec.schedCode);
		sv.sdtyPayMeth.set(t5687rec.sdtyPayMeth);
		sv.singlePremInd.set(t5687rec.singlePremInd);
		sv.srvcpy.set(t5687rec.srvcpy);
		sv.statFund.set(t5687rec.statFund);
		sv.stampDutyMeth.set(t5687rec.stampDutyMeth);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubSect.set(t5687rec.statSubSect);
		sv.svMethod.set(t5687rec.svMethod);
		sv.xfreqAltBasis.set(t5687rec.xfreqAltBasis);
		sv.zrorcmrg.set(t5687rec.zrorcmrg);
		sv.zrorcmsp.set(t5687rec.zrorcmsp);
		sv.zrorcmtu.set(t5687rec.zrorcmtu);
		sv.zrorpmrg.set(t5687rec.zrorpmrg);
		sv.zrorpmsp.set(t5687rec.zrorpmsp);
		sv.zrorpmtu.set(t5687rec.zrorpmtu);
		sv.zrrcombas.set(t5687rec.zrrcombas);
		sv.zsbsmeth.set(t5687rec.zsbsmeth);
		sv.zsredtrm.set(t5687rec.zsredtrm);
		sv.zszprmcd.set(t5687rec.zszprmcd);
		sv.lnkgind.set(t5687rec.lnkgind);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5687rec.t5687Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.anniversaryMethod,t5687rec.anniversaryMethod)) {
			t5687rec.anniversaryMethod.set(sv.anniversaryMethod);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.basicCommMeth,t5687rec.basicCommMeth)) {
			t5687rec.basicCommMeth.set(sv.basicCommMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bascpy,t5687rec.bascpy)) {
			t5687rec.bascpy.set(sv.bascpy);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.basscmth,t5687rec.basscmth)) {
			t5687rec.basscmth.set(sv.basscmth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.basscpy,t5687rec.basscpy)) {
			t5687rec.basscpy.set(sv.basscpy);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bastcmth,t5687rec.bastcmth)) {
			t5687rec.bastcmth.set(sv.bastcmth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bastcpy,t5687rec.bastcpy)) {
			t5687rec.bastcpy.set(sv.bastcpy);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bbmeth,t5687rec.bbmeth)) {
			t5687rec.bbmeth.set(sv.bbmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.dcmeth,t5687rec.dcmeth)) {
			t5687rec.dcmeth.set(sv.dcmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.defFupMeth,t5687rec.defFupMeth)) {
			t5687rec.defFupMeth.set(sv.defFupMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.jlPremMeth,t5687rec.jlPremMeth)) {
			t5687rec.jlPremMeth.set(sv.jlPremMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.jlifePresent,t5687rec.jlifePresent)) {
			t5687rec.jlifePresent.set(sv.jlifePresent);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.loanmeth,t5687rec.loanmeth)) {
			t5687rec.loanmeth.set(sv.loanmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maturityCalcMeth,t5687rec.maturityCalcMeth)) {
			t5687rec.maturityCalcMeth.set(sv.maturityCalcMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.nonForfeitMethod,t5687rec.nonForfeitMethod)) {
			t5687rec.nonForfeitMethod.set(sv.nonForfeitMethod);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.partsurr,t5687rec.partsurr)) {
			t5687rec.partsurr.set(sv.partsurr);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premGuarPeriod,t5687rec.premGuarPeriod)) {
			t5687rec.premGuarPeriod.set(sv.premGuarPeriod);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premmeth,t5687rec.premmeth)) {
			t5687rec.premmeth.set(sv.premmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pumeth,t5687rec.pumeth)) {
			t5687rec.pumeth.set(sv.pumeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.reptcds,t5687rec.reptcds)) {
			t5687rec.reptcds.set(sv.reptcds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riind,t5687rec.riind)) {
			t5687rec.riind.set(sv.riind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rnwcpy,t5687rec.rnwcpy)) {
			t5687rec.rnwcpy.set(sv.rnwcpy);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rtrnwfreq,t5687rec.rtrnwfreq)) {
			t5687rec.rtrnwfreq.set(sv.rtrnwfreq);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.schedCode,t5687rec.schedCode)) {
			t5687rec.schedCode.set(sv.schedCode);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sdtyPayMeth,t5687rec.sdtyPayMeth)) {
			t5687rec.sdtyPayMeth.set(sv.sdtyPayMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.singlePremInd,t5687rec.singlePremInd)) {
			t5687rec.singlePremInd.set(sv.singlePremInd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.srvcpy,t5687rec.srvcpy)) {
			t5687rec.srvcpy.set(sv.srvcpy);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.statFund,t5687rec.statFund)) {
			t5687rec.statFund.set(sv.statFund);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.stampDutyMeth,t5687rec.stampDutyMeth)) {
			t5687rec.stampDutyMeth.set(sv.stampDutyMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.statSect,t5687rec.statSect)) {
			t5687rec.statSect.set(sv.statSect);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.statSubSect,t5687rec.statSubSect)) {
			t5687rec.statSubSect.set(sv.statSubSect);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.svMethod,t5687rec.svMethod)) {
			t5687rec.svMethod.set(sv.svMethod);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.xfreqAltBasis,t5687rec.xfreqAltBasis)) {
			t5687rec.xfreqAltBasis.set(sv.xfreqAltBasis);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrorcmrg,t5687rec.zrorcmrg)) {
			t5687rec.zrorcmrg.set(sv.zrorcmrg);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrorcmsp,t5687rec.zrorcmsp)) {
			t5687rec.zrorcmsp.set(sv.zrorcmsp);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrorcmtu,t5687rec.zrorcmtu)) {
			t5687rec.zrorcmtu.set(sv.zrorcmtu);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrorpmrg,t5687rec.zrorpmrg)) {
			t5687rec.zrorpmrg.set(sv.zrorpmrg);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrorpmsp,t5687rec.zrorpmsp)) {
			t5687rec.zrorpmsp.set(sv.zrorpmsp);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrorpmtu,t5687rec.zrorpmtu)) {
			t5687rec.zrorpmtu.set(sv.zrorpmtu);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrrcombas,t5687rec.zrrcombas)) {
			t5687rec.zrrcombas.set(sv.zrrcombas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zsbsmeth,t5687rec.zsbsmeth)) {
			t5687rec.zsbsmeth.set(sv.zsbsmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zsredtrm,t5687rec.zsredtrm)) {
			t5687rec.zsredtrm.set(sv.zsredtrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zszprmcd,t5687rec.zszprmcd)) {
			t5687rec.zszprmcd.set(sv.zszprmcd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lnkgind,t5687rec.lnkgind)) {
			t5687rec.lnkgind.set(sv.lnkgind);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5687pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
