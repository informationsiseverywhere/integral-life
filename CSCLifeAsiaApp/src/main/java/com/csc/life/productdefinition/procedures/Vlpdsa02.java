/*
 * File: Vlpdsa02.java
 * Date: 30 August 2009 2:53:58
 * Author: Quipoz Limited
 * 
 * Class transformed from VLPDSA02.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51brec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*  Subroutine VLPDSA02.
*  ===================
*
*    This subroutine will be processed to validate SA agaist
*    table TR51B
*    by check following,
*       SA of component1 can't __ (SA of Component2
*                                  and TR51B-SUMINS)
*
*     - Read table TR51B, using VLSB-RID1-CRTABLE + VLSB-RID2-CRTABLE as
*     keys
*     - Get Life Date of birth
*     - Calculate age by using call 'AGECALC'
*     - Validate the limit according to Table TR51B
*
***********************************************************************
* </pre>
*/
public class Vlpdsa02 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VLPDSA02";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private int wsaaMaxRow = 10;
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaVlsbSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaAge = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaErrDet = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaInputAllSpace = new FixedLengthStringData(1);
	private int wsaaMaxError = 20;
	private int wsaaMaxInput = 50;
		/* WSAA-RID1-FIELDS */
	private FixedLengthStringData wsaaRid1Crtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRid1Sa = new ZonedDecimalData(17, 2).init(ZERO).setUnsigned();
		/* WSAA-RID2-FIELDS */
	private FixedLengthStringData wsaaRid2Crtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRid2Sa = new ZonedDecimalData(17, 2).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTr51bSeq = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTr51bItmkey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr51bCompnt1 = new FixedLengthStringData(4).isAPartOf(wsaaTr51bItmkey, 0).init(SPACES);
	private FixedLengthStringData wsaaTr51bCompnt2 = new FixedLengthStringData(4).isAPartOf(wsaaTr51bItmkey, 4).init(SPACES);

	private FixedLengthStringData wsaaTr51bArray = new FixedLengthStringData(300);
	private FixedLengthStringData[] wsaaTr51bRec = FLSArrayPartOfStructure(12, 25, wsaaTr51bArray, 0);
	private ZonedDecimalData[] wsaaTr51bAge = ZDArrayPartOfArrayStructure(3, 0, wsaaTr51bRec, 0);
	private ZonedDecimalData[] wsaaTr51bPercent = ZDArrayPartOfArrayStructure(3, 0, wsaaTr51bRec, 3);
	private FixedLengthStringData[] wsaaTr51bIndc = FLSDArrayPartOfArrayStructure(2, wsaaTr51bRec, 6);
	private ZonedDecimalData[] wsaaTr51bSumins = ZDArrayPartOfArrayStructure(17, 2, wsaaTr51bRec, 8);
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String lifelnbrec = "LIFELNBREC";
		/* TABLES */
	private String tr51b = "TR51B";
	private String t1693 = "T1693";
		/* ERRORS */
	private String rlbk = "RLBK";
	private String rlbn = "RLBN";
	private String rlbo = "RLBO";
	private String e984 = "E984";
	private String rgal = "RGAL";
	private Agecalcrec agecalcrec = new Agecalcrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private Tr51brec tr51brec = new Tr51brec();
	private Varcom varcom = new Varcom();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit190, 
		exit490, 
		exit590, 
		exit690
	}

	public Vlpdsa02() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		try {
			begin010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		if ((isEQ(vlpdsubrec.rid1Life,SPACES)
		&& isEQ(vlpdsubrec.rid1Jlife,SPACES)
		&& isEQ(vlpdsubrec.rid1Coverage,SPACES)
		&& isEQ(vlpdsubrec.rid1Rider,SPACES)
		&& isEQ(vlpdsubrec.rid1Crtable,SPACES))
		|| (isEQ(vlpdsubrec.rid2Life,SPACES)
		&& isEQ(vlpdsubrec.rid2Jlife,SPACES)
		&& isEQ(vlpdsubrec.rid2Coverage,SPACES)
		&& isEQ(vlpdsubrec.rid2Rider,SPACES)
		&& isEQ(vlpdsubrec.rid2Crtable,SPACES))) {
			goTo(GotoLabel.exit090);
		}
		readTables100();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit090);
		}
		ageCalculate200();
		validation300();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTables100()
	{
		try {
			begin110();
		}
		catch (GOTOException e){
		}
	}

protected void begin110()
	{
		wsaaErrSeq.set(1);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51b);
		wsaaTr51bCompnt1.set(vlpdsubrec.rid1Crtable);
		wsaaTr51bCompnt2.set(vlpdsubrec.rid2Crtable);
		itemIO.setItemitem(wsaaTr51bItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
		tr51brec.tr51bRec.set(itemIO.getGenarea());
		for (wsaaTr51bSeq.set(1); !(isGT(wsaaTr51bSeq,wsaaMaxRow)); wsaaTr51bSeq.add(1)){
			wsaaTr51bPercent[wsaaTr51bSeq.toInt()].set(tr51brec.overdueMina[wsaaTr51bSeq.toInt()]);
			wsaaTr51bIndc[wsaaTr51bSeq.toInt()].set(tr51brec.indc[wsaaTr51bSeq.toInt()]);
			wsaaTr51bSumins[wsaaTr51bSeq.toInt()].set(tr51brec.sumins[wsaaTr51bSeq.toInt()]);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(vlpdsubrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
	}

protected void ageCalculate200()
	{
		begin210();
	}

protected void begin210()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(vlpdsubrec.chdrcoy);
		lifelnbIO.setChdrnum(vlpdsubrec.chdrnum);
		lifelnbIO.setLife(vlpdsubrec.rid1Life);
		lifelnbIO.setJlife(vlpdsubrec.rid1Jlife);
		if (isEQ(vlpdsubrec.rid1Jlife,SPACES)) {
			lifelnbIO.setJlife("00");
		}
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		initialize(agecalcrec.agecalcRec);
		agecalcrec.statuz.set(varcom.oK);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(vlpdsubrec.language);
		agecalcrec.cnttype.set(vlpdsubrec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(vlpdsubrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
	}

protected void validation300()
	{
		begin310();
	}

protected void begin310()
	{
		wsaaRid1Crtable.set(SPACES);
		wsaaRid2Crtable.set(SPACES);
		wsaaInputAllSpace.set(SPACES);
		for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq,wsaaMaxInput)
		|| isEQ(wsaaInputAllSpace,"Y")
		|| (isNE(wsaaRid1Crtable,SPACES)
		&& isNE(wsaaRid2Crtable,SPACES))); wsaaVlsbSeq.add(1)){
			chkComponent400();
		}
		if (isEQ(wsaaRid1Crtable,SPACES)
		|| isEQ(wsaaRid2Crtable,SPACES)) {
			syserrrec.statuz.set(rgal);
			fatalError600();
		}
		wsaaErrCode.set(SPACES);
		wsaaErrDet.set(SPACES);
		for (wsaaTr51bSeq.set(1); !(isGT(wsaaTr51bSeq,wsaaMaxRow)); wsaaTr51bSeq.add(1)){
			if (isLTE(agecalcrec.agerating,tr51brec.age[wsaaTr51bSeq.toInt()])) {
				wsaaAge.set(wsaaTr51bSeq);
				wsaaTr51bSeq.set(wsaaMaxRow);
			}
		}
		compute(wsaaRid2Sa, 3).setRounded(mult(wsaaRid2Sa,(div(wsaaTr51bPercent[wsaaAge.toInt()],100))));
		if (isEQ(wsaaTr51bIndc[wsaaAge.toInt()],"EQ")){
			if (isNE(wsaaRid1Sa,wsaaRid2Sa)
			|| isGT(wsaaRid1Sa,wsaaTr51bSumins[wsaaAge.toInt()])) {
				wsaaErrCode.set(rlbo);
			}
		}
		else if (isEQ(wsaaTr51bIndc[wsaaAge.toInt()],"LE")){
			if (isGT(wsaaRid1Sa,wsaaRid2Sa)
			|| isGT(wsaaRid1Sa,wsaaTr51bSumins[wsaaAge.toInt()])) {
				wsaaErrCode.set(rlbk);
			}
		}
		else if (isEQ(wsaaTr51bIndc[wsaaAge.toInt()],"LT")){
			if (isGTE(wsaaRid1Sa,wsaaRid2Sa)
			|| isGT(wsaaRid1Sa,wsaaTr51bSumins[wsaaAge.toInt()])) {
				wsaaErrCode.set(rlbn);
			}
		}
		else{
			wsaaErrCode.set(e984);
		}
		if (isNE(wsaaErrCode,SPACES)) {
			wsaaErrDet.set(wsaaRid1Crtable);
			moveError500();
			/* IBPLIFE -11863 */
			//wsaaErrDet.set(wsaaRid2Crtable);
			//moveError500();
		}
	}

protected void chkComponent400()
	{
		try {
			begin410();
		}
		catch (GOTOException e){
		}
	}

protected void begin410()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()],SPACES)) {
			wsaaInputAllSpace.set("Y");
			goTo(GotoLabel.exit490);
		}
		if (isEQ(wsaaRid1Crtable,SPACES)) {
			if (isEQ(vlpdsubrec.rid1Life,vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Jlife,vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Coverage,vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Rider,vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Crtable,vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()])) {
				wsaaRid1Crtable.set(vlpdsubrec.rid1Crtable);
				wsaaRid1Sa.set(vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]);
			}
		}
		if (isEQ(wsaaRid2Crtable,SPACES)) {
			if (isEQ(vlpdsubrec.rid2Life,vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Jlife,vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Coverage,vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Rider,vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Crtable,vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()])) {
				wsaaRid2Crtable.set(vlpdsubrec.rid2Crtable);
				wsaaRid2Sa.set(vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]);
			}
		}
	}

protected void moveError500()
	{
		try {
			begin510();
		}
		catch (GOTOException e){
		}
	}

protected void begin510()
	{
		if (isGT(wsaaErrSeq,wsaaMaxError)) {
			goTo(GotoLabel.exit590);
		}
		vlpdsubrec.errCode[wsaaErrSeq.toInt()].set(wsaaErrCode);
		vlpdsubrec.errDet[wsaaErrSeq.toInt()].set(wsaaErrDet);
		wsaaErrSeq.add(1);
	}

protected void fatalError600()
	{
		try {
			fatalErrors610();
		}
		catch (GOTOException e){
		}
		finally{
			exit690();
		}
	}

protected void fatalErrors610()
	{
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit690()
	{
		exitProgram();
	}
}
