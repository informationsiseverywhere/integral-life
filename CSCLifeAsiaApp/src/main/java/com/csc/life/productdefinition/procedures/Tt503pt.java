/*
 * File: Tt503pt.java
 * Date: 30 August 2009 2:46:25
 * Author: Quipoz Limited
 * 
 * Class transformed from TT503PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tt503rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TT503.
*
*
*****************************************************************
* </pre>
*/
public class Tt503pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(55).isAPartOf(wsaaPrtLine001, 21, FILLER).init("Hospitalization Weekly Indemnity Premium Amount   ST503");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(66);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("   Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 32, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 36);
	private FixedLengthStringData filler10 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 46, FILLER).init("   Risk Unit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine003, 60).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(32);
	private FixedLengthStringData filler11 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine004, 14, FILLER).init("Benefit    Premium");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(32);
	private FixedLengthStringData filler13 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine005, 15, FILLER).init("Amount     Amount");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(32);
	private FixedLengthStringData filler15 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(32);
	private FixedLengthStringData filler17 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(32);
	private FixedLengthStringData filler19 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(32);
	private FixedLengthStringData filler21 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(32);
	private FixedLengthStringData filler23 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(32);
	private FixedLengthStringData filler25 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(32);
	private FixedLengthStringData filler27 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(32);
	private FixedLengthStringData filler29 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(32);
	private FixedLengthStringData filler31 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(32);
	private FixedLengthStringData filler33 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(32);
	private FixedLengthStringData filler35 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine016, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine016, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(32);
	private FixedLengthStringData filler37 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine017, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine017, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(32);
	private FixedLengthStringData filler39 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine018, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine018, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine018, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(32);
	private FixedLengthStringData filler41 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine019, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine019, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine019, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine019, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine020 = new FixedLengthStringData(32);
	private FixedLengthStringData filler43 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine020, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine020, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine020, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine020, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine021 = new FixedLengthStringData(32);
	private FixedLengthStringData filler45 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine021, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine021, 15).setPattern("ZZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine021, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine021, 26).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine022 = new FixedLengthStringData(28);
	private FixedLengthStringData filler47 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine022, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tt503rec tt503rec = new Tt503rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tt503pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tt503rec.tt503Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tt503rec.riskunit);
		fieldNo008.set(tt503rec.bvolume01);
		fieldNo009.set(tt503rec.insprm01);
		fieldNo010.set(tt503rec.bvolume02);
		fieldNo011.set(tt503rec.insprm02);
		fieldNo012.set(tt503rec.bvolume03);
		fieldNo013.set(tt503rec.insprm03);
		fieldNo014.set(tt503rec.bvolume04);
		fieldNo015.set(tt503rec.insprm04);
		fieldNo016.set(tt503rec.bvolume05);
		fieldNo017.set(tt503rec.insprm05);
		fieldNo018.set(tt503rec.bvolume06);
		fieldNo019.set(tt503rec.insprm06);
		fieldNo020.set(tt503rec.bvolume07);
		fieldNo021.set(tt503rec.insprm07);
		fieldNo022.set(tt503rec.bvolume08);
		fieldNo023.set(tt503rec.insprm08);
		fieldNo024.set(tt503rec.bvolume09);
		fieldNo025.set(tt503rec.insprm09);
		fieldNo026.set(tt503rec.bvolume10);
		fieldNo027.set(tt503rec.insprm10);
		fieldNo028.set(tt503rec.bvolume11);
		fieldNo029.set(tt503rec.insprm11);
		fieldNo030.set(tt503rec.bvolume12);
		fieldNo031.set(tt503rec.insprm12);
		fieldNo032.set(tt503rec.bvolume13);
		fieldNo033.set(tt503rec.insprm13);
		fieldNo034.set(tt503rec.bvolume14);
		fieldNo035.set(tt503rec.insprm14);
		fieldNo036.set(tt503rec.bvolume15);
		fieldNo037.set(tt503rec.insprm15);
		fieldNo038.set(tt503rec.bvolume16);
		fieldNo039.set(tt503rec.insprm16);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine019);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine020);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine021);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine022);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
