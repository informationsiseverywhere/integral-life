package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 30 July 2012 03:09:00
 * Description:
 * Copybook name: Polcalcrec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Polcalcrec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
	public FixedLengthStringData polcalcRec = new FixedLengthStringData(1000);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(polcalcRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(polcalcRec, 4);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(polcalcRec, 8);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(polcalcRec, 9);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(polcalcRec, 17);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(polcalcRec, 20);
  	public PackedDecimalData occdate = new PackedDecimalData(8, 0).isAPartOf(polcalcRec, 23);
  	public PackedDecimalData btdate = new PackedDecimalData(8, 0).isAPartOf(polcalcRec, 28);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(polcalcRec, 33);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(polcalcRec, 38);
  	public FixedLengthStringData batckey = new FixedLengthStringData(22).isAPartOf(polcalcRec, 39);
  	public FixedLengthStringData calcMethod = new FixedLengthStringData(4).isAPartOf(polcalcRec, 61);
  	public PackedDecimalData jrnseq = new PackedDecimalData(3, 0).isAPartOf(polcalcRec, 64);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(polcalcRec, 66);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(polcalcRec, 69);
  	public PackedDecimalData billcd = new PackedDecimalData(8, 0).isAPartOf(polcalcRec, 71);

  	public PackedDecimalData singpfee = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 76);
  	public PackedDecimalData singpfeeTax01 = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 84); 
  	public PackedDecimalData singpfeeTax02 = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 92); 
  	public FixedLengthStringData singpfeeTaxAbsflg01 = new FixedLengthStringData(2).isAPartOf(polcalcRec, 100);
  	public FixedLengthStringData singpfeeTaxAbsflg02 = new FixedLengthStringData(2).isAPartOf(polcalcRec, 102);
  	
  	public FixedLengthStringData polCntfees = new FixedLengthStringData(130).isAPartOf(polcalcRec, 104);
  	public FixedLengthStringData[] polCntfee = FLSArrayPartOfStructure(5, 26, polCntfees, 0);
 	public PackedDecimalData[] cntfee = PDArrayPartOfArrayStructure(15, 2, polCntfee, 0); 
  	public PackedDecimalData[] cntfeeTax01 = PDArrayPartOfArrayStructure(15, 2, polCntfee, 8); 
  	public PackedDecimalData[] cntfeeTax02 = PDArrayPartOfArrayStructure(15, 2, polCntfee, 16); 
  	public FixedLengthStringData[] cntfeeTaxAbsflg01 = FLSDArrayPartOfArrayStructure(1 , polCntfee, 24);
  	public FixedLengthStringData[] cntfeeTaxAbsflg02 = FLSDArrayPartOfArrayStructure(1, polCntfee, 25);
 
  	public PackedDecimalData taxrelAmt = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 234);

  	public FixedLengthStringData discAmts = new FixedLengthStringData(200).isAPartOf(polcalcRec, 242);
  	public PackedDecimalData[] discAmt = PDArrayPartOfStructure(5, 15, 2, discAmts, 0);
  	public PackedDecimalData[] disc01 = PDArrayPartOfArrayStructure(15, 2, discAmt, 0);
  	public PackedDecimalData[] disc02 = PDArrayPartOfArrayStructure(15, 2, discAmt, 8);
  	public PackedDecimalData[] disc03 = PDArrayPartOfArrayStructure(15, 2, discAmt, 16);
  	public PackedDecimalData[] disc04 = PDArrayPartOfArrayStructure(15, 2, discAmt, 24);
  	public PackedDecimalData[] disc05 = PDArrayPartOfArrayStructure(15, 2, discAmt, 32);
  	
  	public PackedDecimalData regPremTot = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 442);
  	public PackedDecimalData basPremTot = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 450);
  	public PackedDecimalData compTaxTot = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 458);
  	public PackedDecimalData tolValue = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 466);
  	public PackedDecimalData premTot = new PackedDecimalData(15, 2).isAPartOf(polcalcRec, 474);
  	
  	public FixedLengthStringData filler = new FixedLengthStringData(518).isAPartOf(polcalcRec, 482, FILLER);
  	
	public void initialize() {
		COBOLFunctions.initialize(polcalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			polcalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}