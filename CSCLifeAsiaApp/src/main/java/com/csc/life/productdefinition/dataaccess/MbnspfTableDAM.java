package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MbnspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:49
 * Class transformed from MBNSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MbnspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 81;
	public FixedLengthStringData mbnsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData mbnspfRecord = mbnsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(mbnsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(mbnsrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(mbnsrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(mbnsrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(mbnsrec);
	public PackedDecimalData yrsinf = DD.yrsinf.copy().isAPartOf(mbnsrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(mbnsrec);
	public PackedDecimalData surrval = DD.surrval.copy().isAPartOf(mbnsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(mbnsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(mbnsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(mbnsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MbnspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MbnspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MbnspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MbnspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MbnspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MbnspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MbnspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MBNSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"YRSINF, " +
							"SUMINS, " +
							"SURRVAL, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     yrsinf,
                                     sumins,
                                     surrval,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		yrsinf.clear();
  		sumins.clear();
  		surrval.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMbnsrec() {
  		return mbnsrec;
	}

	public FixedLengthStringData getMbnspfRecord() {
  		return mbnspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMbnsrec(what);
	}

	public void setMbnsrec(Object what) {
  		this.mbnsrec.set(what);
	}

	public void setMbnspfRecord(Object what) {
  		this.mbnspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(mbnsrec.getLength());
		result.set(mbnsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}