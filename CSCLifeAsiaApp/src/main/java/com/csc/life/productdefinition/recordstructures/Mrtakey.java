package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:47
 * Description:
 * Copybook name: MRTAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mrtakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mrtaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mrtaKey = new FixedLengthStringData(64).isAPartOf(mrtaFileKey, 0, REDEFINE);
  	public FixedLengthStringData mrtaChdrcoy = new FixedLengthStringData(1).isAPartOf(mrtaKey, 0);
  	public FixedLengthStringData mrtaChdrnum = new FixedLengthStringData(8).isAPartOf(mrtaKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(mrtaKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mrtaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mrtaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}