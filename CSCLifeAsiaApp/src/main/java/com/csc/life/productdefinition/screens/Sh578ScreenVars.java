package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH578
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh578ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(244);
	public FixedLengthStringData dataFields = new FixedLengthStringData(68).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,19);
	public ZonedDecimalData efdate = DD.efdate.copyToZonedDecimal().isAPartOf(dataFields,27);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,35);
	public FixedLengthStringData flnam = DD.flnam.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData itemtabl = DD.itemtabl.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 68);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData efdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData flnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itemtablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 112);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] efdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] flnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itemtablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData efdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sh578screenWritten = new LongData(0);
	public LongData Sh578protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh578ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itemtablOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(flnamOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(efdateOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, jobq, acctyear, scheduleName, scheduleNumber, bbranch, bcompany, itemtabl, flnam, efdate};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, jobqOut, acctyearOut, bschednamOut, bschednumOut, bbranchOut, bcompanyOut, itemtablOut, flnamOut, efdateOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, jobqErr, acctyearErr, bschednamErr, bschednumErr, bbranchErr, bcompanyErr, itemtablErr, flnamErr, efdateErr};
		screenDateFields = new BaseData[] {effdate, efdate};
		screenDateErrFields = new BaseData[] {effdateErr, efdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp, efdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh578screen.class;
		protectRecord = Sh578protect.class;
	}

}
