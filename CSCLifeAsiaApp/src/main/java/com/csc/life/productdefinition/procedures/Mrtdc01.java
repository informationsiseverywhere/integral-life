
/*
 * File: Mtrdc01.java
 *Death Caculation program for MRTA
 *Copied from Rldtrrd.java 
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */

package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Mrtdc01rec;
import com.csc.life.newbusiness.recordstructures.Rlrdtrmrec;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.MrtpfDAO;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  A Subroutine to calculate the benefit schedule for Decreasing
*  Term Rider for Annual Rest.
*
*  Logic:
*  . Read TH615 with rest indicator to determine if automatic
*    calculation of benefit schedule is required.
*
*  . If interest rate PRAT is captured in MRTA, use this formula
*      define  VJ = 1 / (1 + (PRAT / 100))
*      loop index for the duration of risk term,
*      derive P = Risk Term - index + 1
*      derive Int Rate = ((1- VJ exp P) / (1 - VJ exp Risk Term))
*         Reduced SA = SA * Int Rate
*
*  . If interest rate is NOT captured in MRTA, the Reduced SA is
*      gradually scaling down for the duration of risk term.
*      loop index for the duration of risk term,
*         Reduced SA = ((Risk Term - index + 1) / Risk Term) * SA
*
*  . Each calculated Reduced SA is written to MBNS.
*
*****************************************************************
* </pre>
*/
public class Mrtdc01 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRiskTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaIntins = new PackedDecimalData(6, 3).setUnsigned();
	private PackedDecimalData wsaaSumins1 = new PackedDecimalData(17, 7).setUnsigned();
	private PackedDecimalData wsaaRdtsum = new PackedDecimalData(15, 0).setUnsigned();
	private PackedDecimalData wsaaVj = new PackedDecimalData(10, 7);
	private PackedDecimalData wsaaP = new PackedDecimalData(15, 7);
	
	

	
	private String wsaaSubr = "MRTDC01";
	protected Syserrrec syserrrec = new Syserrrec();
	private static final Logger logger = LoggerFactory.getLogger(Mrtdc01.class);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
    private PackedDecimalData wsaaDeathBenAccAmt = new PackedDecimalData(17,2);
    private PackedDecimalData wsaaDeathBenNoAccAmt = new PackedDecimalData(17,2);
    protected Mrtdc01rec mrtdc01rec= getMrtdc01rec();
    protected Datcon1rec datcon1rec= new Datcon1rec ();
    protected Datcon3rec datcon3rec= new Datcon3rec ();
   //private PackedDecimalData deathBenefit = new PackedDecimalData(8,0);
    private PackedDecimalData wsaaYear = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaMonthDiff = new PackedDecimalData(2, 0).init(0);
	private PackedDecimalData wsaaDate1 = new PackedDecimalData(5, 0).init(0);  
	private PackedDecimalData wsaaDate2 = new PackedDecimalData(3, 0).init(0); 
	private PackedDecimalData wsaaDay = new PackedDecimalData(5, 0).init(0);  
protected PackedDecimalData wsaaToday = new PackedDecimalData(8,0);
	
	
	protected int deathBenefit =0;
	
	
	
		/* TABLES */
	private String th615 = "TH615";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String mrtarec = "MRTAREC";
	private String mbnsrec = "MBNSREC";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Benefit Schedule Details*/
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
		/*Reducing Term Assurance details*/
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private Rlrdtrmrec rlrdtrmrec = new Rlrdtrmrec();
	private Th615rec th615rec = new Th615rec();
	protected Varcom varcom = new Varcom();
	
	protected FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	
	protected MrtpfDAO mrtpfDAO = getApplicationContext().getBean("mrtpfDAO", MrtpfDAO.class); /*ILIFE-3661*/
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	
	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readContract100,
		calculatedEmbBenf100,
		exit090
	}

	public Mrtdc01() {
		super();
	}
	
	public Mrtdc01rec getMrtdc01rec() {
		return new Mrtdc01rec();
	}

public void mainline(Object... parmArray)
	{
	mrtdc01rec.mrtdc01Rec = convertAndSetParam(mrtdc01rec.mrtdc01Rec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while(true){
		try {
			switch (nextMethod){
			case DEFAULT:{
			para010();
			}
			
			case readContract100:{
				readContract100();
			}
			case  calculatedEmbBenf100:{
				 calculatedEmbBenf100();
			}
			case exit090:
			}
				
			break;
			
			
			}
			catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
			}	}
	}
	
	



protected void para010()
	{
		if(!(isEQ(mrtdc01rec.actualVal,ZERO))){
			mrtdc01rec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit090);
		}
	}
protected void exit090()
	{
		exitProgram();
	}



protected void readContract100()
{
	
	chdrpf.setChdrcoy((mrtdc01rec.chdrcoy.charat(0)));
	chdrpf.setChdrnum(mrtdc01rec.chdrnum.toString());
	chdrpf= chdrpfDAO.getchdrRecord(mrtdc01rec.chdrcoy.toString() , mrtdc01rec.chdrnum.toString() );
	if(chdrpf==null )
	{
		return;	
	}
	/*chdrlnbIO.setDataArea(SPACES);
	chdrlnbIO.setChdrcoy(mrtdc01rec.chdrcoy);
	chdrlnbIO.setChdrnum(mrtdc01rec.chdrnum);
	chdrlnbIO.setFunction(varcom.reads);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
		 
		fatalerror600();
	}*/
}
protected void fatalerror600()
{
	if (isEQ(syserrrec.statuz,varcom.bomb)) {
		return ;
	}	
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	if (isNE(syserrrec.syserrType,"2")) {
		syserrrec.syserrType.set("1");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
}


protected void calculatedEmbBenf100()
{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);	
		
		datcon3rec.set(SPACES);
		datcon3rec.intDate1.set(mrtdc01rec.occDate);
		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec); 
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalerror600();
		}
		wsaaMonthDiff.set(datcon3rec.freqFactor);
		wsaaMonthDiff.add(1);   /*ILIFE-3723*/
		deathBenefit = mrtpfDAO.getMBNSData(mrtdc01rec.chdrnum.toString(), wsaaMonthDiff.toString());  /*ILIFE-3661*/
		mrtdc01rec.sumins.set(deathBenefit);
		mrtdc01rec.actualVal.set((deathBenefit));
		mrtdc01rec.statuz.set(varcom.oK);
		wsaaFirstTime.set("Y");
}
}
