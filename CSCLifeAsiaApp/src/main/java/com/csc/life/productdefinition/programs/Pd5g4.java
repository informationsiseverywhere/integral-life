package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.productdefinition.screens.Sd5g4ScreenVars;
import com.csc.life.productdefinition.tablestructures.Td5g4rec;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Pd5g4 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5G4");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private Td5g4rec Td5g4rec = new Td5g4rec();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itmdkey wsaaItmdkey = new Itmdkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5g4ScreenVars sv = ScreenProgram.getScreenVars(Sd5g4ScreenVars.class);

	 private final DescDAO descDao = getApplicationContext().getBean("descDAO", DescDAO.class);
	    private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);	
	private Descpf descpf;
	 private Itempf itempf = new Itempf();
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		   exit3900, exit5090
	}

	public Pd5g4() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5g4", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void initialise1000() {
		initialise1010();	
		readRecord1020();
		moveToScreen1030();
		moveToScreen1030();
		}
	

	protected void initialise1010() {
		/* INITIALISE-SCREEN */
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		wsaaItmdkey.set(wsspsmart.itmdkey);
		descpf=descDao.getdescData("IT", wsaaItemkey.itemItemtabl.toString(), wsaaItemkey.itemItemitem.toString(), wsaaItemkey.itemItemcoy.toString(), wsspcomn.language.toString());
		if (descpf==null) {
		
			fatalError600();
		}
		sv.longdesc.set(descpf.getLongdesc());
		
		itempf = itempfDAO.findItemByItdm(wsaaItemkey.itemItemcoy.toString(), wsaaItemkey.itemItemtabl.toString(),
				wsaaItemkey.itemItemitem.toString());

		if (itempf == null ) {
			fatalError600();
		}
		Td5g4rec.td5g4Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		cont1190();
	
	}

	protected void cont1190() {
		sv.riskclasss.set(Td5g4rec.riskclasss);
		//ICIL-1209
//		sv.riskclassdescs.set(Td5g4rec.riskclassdescs);
		screenIo2010();
		sv.formulas.set(Td5g4rec.formulas);
		sv.factors.set(Td5g4rec.factors);
		}


	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);

		}
		
	}

	protected void screenEdit2000() {
				screenIo2010();
				if (isNE(sv.errorIndicators,SPACES)) {
					wsspcomn.edterror.set("Y");
				}
				/*REDISPLAY-ON-CALC*/
				if (isEQ(scrnparams.statuz,"CALC")) {
					wsspcomn.edterror.set("Y");
				} 
		
	}

	protected void screenIo2010() {
		Map<String,Descpf> rishkClassDescs = descDao.getItems("IT", wsaaItemkey.itemItemcoy.toString(),"T5446",wsspcomn.language.toString());
		sv.riskclassdesc01.set("");
		sv.riskclassdesc02.set("");
		sv.riskclassdesc03.set("");
		sv.riskclassdesc04.set("");
		sv.riskclassdesc05.set("");
		sv.riskclassdesc06.set("");
		sv.riskclassdesc07.set("");
		sv.riskclassdesc08.set("");
		sv.riskclassdesc09.set("");
		sv.riskclassdesc10.set("");
		if (rishkClassDescs != null && rishkClassDescs.size() > 0) {
			if (riskClassIsnull(sv.riskclass01,rishkClassDescs)) {
				sv.riskclassdesc01.set(rishkClassDescs.get(
						sv.riskclass01).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass02,rishkClassDescs)) {
				sv.riskclassdesc02.set(rishkClassDescs.get(
						sv.riskclass02).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass03,rishkClassDescs)) {
				sv.riskclassdesc03.set(rishkClassDescs.get(
						sv.riskclass03).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass04,rishkClassDescs)) {
				sv.riskclassdesc04.set(rishkClassDescs.get(
						sv.riskclass04).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass05,rishkClassDescs)) {
				sv.riskclassdesc05.set(rishkClassDescs.get(
						sv.riskclass05).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass06,rishkClassDescs)) {
				sv.riskclassdesc06.set(rishkClassDescs.get(
						sv.riskclass06).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass07,rishkClassDescs)) {
				sv.riskclassdesc07.set(rishkClassDescs.get(
						sv.riskclass07).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass08,rishkClassDescs)) {
				sv.riskclassdesc08.set(rishkClassDescs.get(
						sv.riskclass08).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass09,rishkClassDescs)) {
				sv.riskclassdesc09.set(rishkClassDescs.get(
						sv.riskclass09).getLongdesc());
			}
			if (riskClassIsnull(sv.riskclass10,rishkClassDescs)) {
				sv.riskclassdesc10.set(rishkClassDescs.get(
						sv.riskclass10).getLongdesc());
			}

		}
		
		
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		} else {
			wsspcomn.edterror.set(varcom.oK);
		}
		/* VALIDATE */

	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		return;
		/* EXIT */
		/** UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION */
	}

	protected void update3000() {
		try {
			loadWsspFields3100();
		} catch (GOTOException e) {
		}
	}

	protected void loadWsspFields3100() {
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3900);
		}
		updateRec5000();
		/* COMMIT */
	}

	protected void updateRec5000() {
		try {
			
			compareFields5020();
		
		} catch (GOTOException e) {
		}
	}


	protected void compareFields5020() {
		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.exit5090);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itempf.setGenarea(Td5g4rec.td5g4Rec.toString().getBytes());
		itempf.setItempfx(wsaaItemkey.itemItempfx.toString());
		itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
		

	}

	protected void checkChanges3100() {
		check3100();
	}

	protected void check3100() {

		if (isNE(sv.factors, Td5g4rec.factors)) {
			Td5g4rec.factors.set(sv.factors);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.formulas, Td5g4rec.formulas)) {
			Td5g4rec.formulas.set(sv.formulas);
			wsaaUpdateFlag = "Y";
		}
		
		if (isNE(sv.riskclasss, Td5g4rec.riskclasss)) {
			Td5g4rec.riskclasss.set(sv.riskclasss);
			wsaaUpdateFlag = "Y";
		}
		
		if (isNE(sv.riskclassdescs, Td5g4rec.riskclassdescs)) {
			Td5g4rec.riskclassdescs.set(sv.riskclassdescs);
			wsaaUpdateFlag = "Y";
		}
			
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}
	
	private boolean riskClassIsnull(FixedLengthStringData  riskclasstemp,Map<String,Descpf> rishkClassDescstemp){
		
		boolean isRiskDescAvail = false;
		if(riskclasstemp != null
				&& !riskclasstemp.toString().trim().equalsIgnoreCase("")){
			isRiskDescAvail=rishkClassDescstemp.get(riskclasstemp.toString().trim())!=null;
		}
		return isRiskDescAvail;
	}
	
	protected void readRecord1020() {

		descpf = descDao.getdescData(wsaaDesckey.descDescpfx.toString(), wsaaDesckey.descDesctabl.toString(),
			wsaaDesckey.descDescitem.toString().trim(), wsaaDesckey.descDesccoy.toString(),
			wsaaDesckey.descLanguage.toString());

		if (descpf == null) {
			fatalError600();
		}
	}
		 protected void moveToScreen1030() {
				sv.company.set(wsaaItemkey.itemItemcoy);
				sv.tabl.set(wsaaItemkey.itemItemtabl);
				sv.item.set(wsaaItemkey.itemItemitem);
				sv.longdesc.set(descpf.getLongdesc());
				Td5g4rec.td5g4Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
}
