/*
 * File: Br646.java
 * Date: 29 August 2009 22:31:26
 * Author: Quipoz Limited
 *
 * Class transformed from BR646.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.accounting.procedures.Glsubst;
import com.csc.fsu.accounting.recordstructures.Glsubstrec;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.clients.tablestructures.Tr393rec;
import com.csc.fsu.financials.dataaccess.dao.CheqpfDAO;
import com.csc.fsu.financials.dataaccess.dao.PreqpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Cheqpf;
import com.csc.fsu.financials.dataaccess.dao.model.Preqpf;
import com.csc.fsu.financials.tablestructures.T3672rec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Chkbkcd;
import com.csc.fsu.general.procedures.Chkcurr;
import com.csc.fsu.general.procedures.Upper;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Chkbkcdrec;
import com.csc.fsu.general.recordstructures.Chkcurrrec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Reqproccpy;
import com.csc.fsu.general.recordstructures.Upperrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.productdefinition.dataaccess.dao.Br646DAO;
import com.csc.life.productdefinition.dataaccess.dao.MedipfDAO;
import com.csc.life.productdefinition.dataaccess.model.Br646DTO;
import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Spcout;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Spcoutrec;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  Medical Bill Payment - Medical Provider
*  ---------------------------------------
*   Extract Medical Bill records not paid where Action = '1'
*   and Paid by Medical Provider-'D'.
*
*   Accounting posting for medical bill payment.
*      Dr       LP   ME
*      Cr       LP   MP
*
*   Payment method (Cheque/Direct Credit) is pre-defined in the
*   Medical Provider File.
*
*   For payment by cheque, an auto cheque will be generated for
*   Medical Provider payment. For each Medical Provider, one
*   payment cheque will be generated.
*
*   For payment by Direct Credit, a requisition will be
*   generated for Medical Provider payment. For each Medical
*   Provider, one direct credit will be generated.
*
*   Accounting posting for medical bill auto cheque creation.
*      Dr       LP   MP
*      Cr       BK  PY  (Bank a/c)
*   Update contract transaction history.
*
*   CONTROL TOTALS:
*     01  -  No of bills read.
*     02  -  No of payments made.
*     03  -  No of auto cheque.
*     04  -  No of direct credit.
*
***********************************************************************
* </pre>
*/
public class Br646 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR646");
	private FixedLengthStringData wsaaInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaInit, 20);
	private FixedLengthStringData wsaaHdrGenlkey = new FixedLengthStringData(20);
	private ZonedDecimalData wsaaHdrContot = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaHdrSign = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaExmcode = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaOrigcurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaPaidby = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTotalPayment = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaAllocateNo = new FixedLengthStringData(1).init("Y");
	private Validator allocateNo = new Validator(wsaaAllocateNo, "Y");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Batckey wsaaBatckey = new Batckey();
	private Rdockey wsaaRdockey = new Rdockey();
	private Alocnorec alocnorec = new Alocnorec();
	private Chkbkcdrec chkbkcdrec = new Chkbkcdrec();
	private Chkcurrrec chkcurrrec = new Chkcurrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Glsubstrec glsubstrec = new Glsubstrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Spcoutrec spcoutrec = new Spcoutrec();
	private Upperrec upperrec = new Upperrec();
	private T3629rec t3629rec = new T3629rec();
	private T3688rec t3688rec = new T3688rec();
	private T3672rec t3672rec = new T3672rec();
	private T5645rec t5645rec = new T5645rec();
	private T5690rec t5690rec = new T5690rec();
	private Tr393rec tr393rec = new Tr393rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Reqproccpy reqproccpy = new Reqproccpy();
	
	/*Batch Upgrade Variables*/
	private static final String h134 = "H134";
	private P6671par p6671par = new P6671par();
	private int intBatchID;
	private int intBatchExtractSize;	
	private int intBatchStep;
	private String strEffDate;	
	private String tranDesc;
	private String shortDesc;
	private String chdrTranno;
	private int ctrCT01; 
	private int ctrCT02;
	private int ctrCT03;
	private int ctrCT04;
	private BigDecimal totPayment;
	private BigDecimal subTotal;
	private boolean noRecordFound;
	
	/*PF Used*/
	private Chdrpf chdrpf;
	private Ptrnpf ptrnpf;
	private Preqpf preqpf;
	private Cheqpf cheqpf;
	private Medipf medipf = new Medipf();
	private MedxTemppf medxTemppf = new MedxTemppf();
	private Br646DTO br646DTO = new Br646DTO();
	
	private List<MedxTemppf> medxList;
	private List<Chdrpf> chdrBulkUpdtList;
	private List<Ptrnpf> ptrnBulkInsList;
	private List<Preqpf> preqBulkInsList;
	private List<Preqpf> preqHeaderBulkInsList;
	private List<Cheqpf> cheqBulkInsList;
	private List<Medipf> mediBulkUpdList;
	private Map<String, List<Itempf>> t5645ListMap;
	private Map<String, List<Itempf>> tr393ListMap;
	private Map<String, List<Itempf>> t3629ListMap;
	private Map<String, List<Itempf>> t3688ListMap; 
	private Map<String, List<Itempf>> t5690ListMap;
	private Map<String, List<Itempf>> t3672ListMap;
	private List<Descpf> t1688List;
	private List<Descpf> t3583List;
	private Iterator<MedxTemppf> iteratorList;
	
	/*DAO Used*/
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Br646DAO br646DAO =  getApplicationContext().getBean("br646DAO", Br646DAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private MedipfDAO medipfDAO = getApplicationContext().getBean("medipfDAO", MedipfDAO.class);	
	private PreqpfDAO preqpfDAO =  getApplicationContext().getBean("preqpfDAO", PreqpfDAO.class);
	private CheqpfDAO cheqpfDAO =  getApplicationContext().getBean("cheqpfDAO", CheqpfDAO.class);
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br646.class);		

	public Br646() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()	{
	p6671par.parmRecord.set(bupaIO.getParmarea());
	strEffDate = bsscIO.getEffectiveDate().toString();	
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound = false;
	medxList = new ArrayList<MedxTemppf>();
	
	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
	
	StringBuilder medxTempTableName = new StringBuilder("");
	medxTempTableName.append("MEDX");
	medxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
	medxTempTableName.append(String.format("%0"+ (4 - bsscIO.getScheduleNumber().toString().trim().length())+"d%s",0 , bsscIO.getScheduleNumber().toString().trim()));
	
	int extractRows = br646DAO.populateBr646Temp(intBatchExtractSize, medxTempTableName.toString(), p6671par.chdrnum.toString(), p6671par.chdrnum1.toString(),bsprIO.getFsuco().toString());
	if (extractRows > 0) {
		performDataChunk();	
		
		if (!medxList.isEmpty()){
			initialise1010();
			readSmartTables(bsprIO.getCompany().toString().trim());
			readSmartTablesFSU(bsprIO.getFsuco().toString().trim());
			readT5645();
			readTr393();
			readT1688Desc();
		}			
	}
	else{
		LOGGER.info("No Medipay records retrieved for processing.");		
		noRecordFound = true;
		return;
	}	
}

private void readSmartTables(String company){
	t5645ListMap = itemDAO.loadSmartTable("IT", company, "T5645");
	t3629ListMap = itemDAO.loadSmartTable("IT", company, "T3629");
	t3688ListMap = itemDAO.loadSmartTable("IT", company, "T3688");
	t5690ListMap = itemDAO.loadSmartTable("IT", company, "T5690");
	t3672ListMap = itemDAO.loadSmartTable("IT", company, "T3672");
	t1688List = descDAO.getItems(smtpfxcpy.item.toString(), bsprIO.getCompany().toString(), "T1688");
}

private void readSmartTablesFSU(String company){
	tr393ListMap = itemDAO.loadSmartTable("IT", company, "TR393");
}

protected void readT1688Desc(){
	boolean itemFound = false;
	for (Descpf descItem : t1688List) {
		if (descItem.getDescitem().trim().equals(bprdIO.getAuthCode().toString().trim())){
			tranDesc = descItem.getLongdesc();
			itemFound=true;
			break;
		}
	}
	if (!itemFound) {
		fatalError600();	
	}	
}

protected void readT3583Desc(){
	boolean itemFound = false;
	for (Descpf descItem : t3583List) {
		if (descItem.getDescitem().trim().equals(br646DTO.getSalutl().trim())){/* IJTI-1523 */
			tranDesc = descItem.getLongdesc();
			shortDesc = descItem.getShortdesc();
			itemFound=true;
			break;
		}
	}
	if (!itemFound) {
		fatalError600();	
	}	
}

protected void readT5645(){
	String keyItemitem = wsaaProg.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5645ListMap.containsKey(keyItemitem)){	
		itempfList = t5645ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5645rec.t5645Rec);
		syserrrec.statuz.set(h134);
		fatalError600();		
	}	
}

protected void readTr393(){
	String keyItemitem = bsprIO.getFsuco().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr393ListMap.containsKey(keyItemitem)){	
		itempfList = tr393ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					tr393rec.tr393Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				tr393rec.tr393Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(tr393rec.tr393Rec);
		fatalError600();		
	}	
}

protected void readT3629(){
	String keyItemitem = br646DTO.getCntcurr().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3629ListMap.containsKey(keyItemitem)){	
		itempfList = t3629ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t3629rec.t3629Rec);
		fatalError600();		
	}	
}

protected void readT3688(){
	String keyItemitem = t3629rec.bankcode.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3688ListMap.containsKey(keyItemitem)){	
		itempfList = t3688ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3688rec.t3688Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t3688rec.t3688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t3688rec.t3688Rec);
		fatalError600();		
	}	
}

protected void readT5690(){
	String keyItemitem = br646DTO.getPaymth().trim();/* IJTI-1523 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5690ListMap.containsKey(keyItemitem)){	
		itempfList = t5690ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5690rec.t5690Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5690rec.t5690Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5690rec.t5690Rec);
		fatalError600();		
	}	
}

protected void readT3672(){
	String keyItemitem = t5690rec.paymentMethod.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3672ListMap.containsKey(keyItemitem)){	
		itempfList = t3672ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3672rec.t3672Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t3672rec.t3672Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t3672rec.t3672Rec);
		fatalError600();		
	}	
}

protected void performDataChunk(){
	intBatchID = intBatchStep;		
	if (medxList.size() > 0) medxList.clear();
	medxList = br646DAO.loadDataByBatch(intBatchID);
	if (medxList.isEmpty()){
		if (isNE(subTotal,ZERO)) {
			writePreqHeader();
			writeCheqRecord();
		}
		wsspEdterror.set(varcom.endp);
	}else iteratorList = medxList.iterator();		
}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaBatckey.batcKey.set(batcdorrec.batchkey);
		wsaaClntnum.set(SPACES);
		wsaaExmcode.set(SPACES);
		wsaaPaidby.set(SPACES);
		wsaaAllocateNo.set("Y");
		wsaaInit.set(bsscIO.getDatimeInit());
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmUser.set(wsaaUser);
		wsaaSequenceNo.set(ZERO);
		wsaaTotalPayment.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		
		chdrBulkUpdtList = new ArrayList<Chdrpf>();	
		ptrnBulkInsList = new ArrayList<Ptrnpf>();
		mediBulkUpdList = new ArrayList<Medipf>(); 
		preqHeaderBulkInsList = new ArrayList<Preqpf>();
		preqBulkInsList = new ArrayList<Preqpf>();
		cheqBulkInsList = new ArrayList<Cheqpf>();
		
		t5645ListMap =  new HashMap<String, List<Itempf>>();
		tr393ListMap = new HashMap<String, List<Itempf>>();
		t3629ListMap = new HashMap<String, List<Itempf>>();
		t3688ListMap = new HashMap<String, List<Itempf>>();
		t5690ListMap = new HashMap<String, List<Itempf>>();
		t3672ListMap = new HashMap<String, List<Itempf>>();
		t1688List = new ArrayList<Descpf>();	
		t3583List = new ArrayList<Descpf>();
		
		totPayment = BigDecimal.ZERO;
		subTotal = BigDecimal.ZERO;
		ctrCT01=0;
		ctrCT02=0;
		ctrCT03=0;
		ctrCT04=0;
	}


protected void readFile2000(){
	if (!medxList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!medxList.isEmpty()){
				medxTemppf = new MedxTemppf();
				medxTemppf = iteratorList.next();
				br646DTO = medxTemppf.getBr646DTO();	
			}
		}else {		
			medxTemppf = new MedxTemppf();
			medxTemppf = iteratorList.next();		
			br646DTO = medxTemppf.getBr646DTO();	
		}	
	}else wsspEdterror.set(varcom.endp);
}

protected void edit2500(){
	ctrCT01++;
}

protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT01 = 0;
	
	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT02 = 0;	
	
	//ct03
	contotrec.totno.set(ct03);
	contotrec.totval.set(ctrCT03);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT03 = 0;
	
	//ct04
	contotrec.totno.set(ct04);
	contotrec.totval.set(ctrCT04);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT04 = 0;	
}

protected void update3000(){
	update3010();
}

protected void update3010()
	{
		wsaaTranno.set(br646DTO.getTranno() + 1);
		totPayment = br646DTO.getTotpymt();
		if (isLTE(totPayment,ZERO)) {
			return ;
		}
		if (isEQ(wsaaExmcode,SPACES)
		&& isEQ(wsaaPaidby,SPACES)) {
			wsaaExmcode.set(medxTemppf.getExmcode());
			wsaaClntnum.set(medxTemppf.getClntnum());
			wsaaPaidby.set(medxTemppf.getPaidby());
		}
		if (isNE(medxTemppf.getExmcode(),wsaaExmcode)
		&& isNE(subTotal,ZERO)) {
			writePreqHeader();
			writeCheqRecord();
			wsaaOverflow.set("Y");
			wsaaAllocateNo.set("Y");
			wsaaSequenceNo.set(ZERO);
			subTotal = BigDecimal.ZERO;
			wsaaExmcode.set(medxTemppf.getExmcode());
			wsaaClntnum.set(medxTemppf.getClntnum());
			wsaaPaidby.set(medxTemppf.getPaidby());
		}
		updateChdr();
		if (allocateNo.isTrue()) {
			allocateNextNo3800();
			wsaaAllocateNo.set("N");
		}
		writePtrnRecord();
		updateAcmv3620();
		updateMediRecord();
		writePreqDissection();
		ctrCT02++;
	}


protected void writePreqDissection(){

	preqpf = new Preqpf();
	wsaaSequenceNo.add(1);
	preqpf.setJrnseq(wsaaSequenceNo.toInt());
	preqpf.setRdocpfx(fsupfxcpy.reqn.toString());
	preqpf.setRdoccoy(bsprIO.getCompany().toString());
	preqpf.setRdocnum(wsaaRdockey.rdocRdocnum.toString());
	preqpf.setBranch(bprdIO.getDefaultBranch().toString());
	preqpf.setRldgcoy(bsprIO.getCompany().toString());
	preqpf.setGenlcoy(bsprIO.getCompany().toString());
	preqpf.setRldgacct(medxTemppf.getChdrnum());
	preqpf.setSacscode(t5645rec.sacscode[3].toString());
	preqpf.setSacstyp(t5645rec.sacstype[3].toString());
	preqpf.setGlcode(t5645rec.glmap[3].toString());
	preqpf.setGlsign(t5645rec.sign[3].toString());
	preqpf.setCnttot(t5645rec.cnttot[3].toInt());
	preqpf.setRdoccoy(bsprIO.getCompany().toString());
	preqpf.setGenlcur(wsaaOrigcurr.toString());
	preqpf.setPostmonth(batcdorrec.actmonth.toString());
	preqpf.setPostyear(batcdorrec.actyear.toString());
	preqpf.setEffdate(bsscIO.getEffectiveDate().toInt());
	preqpf.setTrandesc(tranDesc);
	preqpf.setAgntpfx(SPACES.toString());
	preqpf.setAgntcoy(SPACES.toString());
	preqpf.setAgntnum(SPACES.toString());
	preqpf.setTranref(SPACES.toString());
	preqpf.setOrigamt(medxTemppf.getZmedfee());
	preqpf.setAcctamt(medxTemppf.getZmedfee());
	subTotal = subTotal.add(medxTemppf.getZmedfee());
	preqpf.setOrigccy(wsaaOrigcurr.toString());
	preqpf.setTaxcat(SPACES.toString());
	preqpf.setFrcdate(varcom.vrcmMaxDate.toInt());
	preqpf.setRcamt(BigDecimal.ZERO);
	preqpf.setTrdt(Integer.parseInt(getCobolDate()));
	preqpf.setTrtm(varcom.vrcmTime.toInt());
	preqpf.setUser_t(varcom.vrcmUser.toInt());
	preqpf.setTermid(varcom.vrcmTermid.toString());
	b1000CallChkcurr();
	preqpf.setCrate(BigDecimal.valueOf(chkcurrrec.rate.toLong()));
	glsubstrec.glsubstRec.set(SPACES);
	glsubstrec.function.set("SBALL");
	glsubstrec.company.set(bsprIO.getCompany());
	glsubstrec.branch.set(bsprIO.getDefaultBranch());
	glsubstrec.glacct.set(preqpf.getGlcode());
	glsubstrec.bankcode.set(t3629rec.bankcode);
	glsubstrec.currency.set(wsaaOrigcurr);
	callProgram(Glsubst.class, glsubstrec.glsubstRec);
	if (isNE(glsubstrec.statuz,varcom.oK)) {
		syserrrec.params.set(glsubstrec.glsubstRec);
		syserrrec.statuz.set(glsubstrec.statuz);
		fatalError600();
	}
	preqpf.setGlcode(glsubstrec.glacct.toString());
	preqBulkInsList.add(preqpf);
}

protected void commitPreqBulkInsert(){	 
	 boolean isInsertPreqPF = preqpfDAO.insertPreqPF(preqBulkInsList);	
	if (!isInsertPreqPF) {
		LOGGER.error("Insert PreqPF record failed.");
		fatalError600();
	}else preqBulkInsList.clear();
}

protected void writePreqHeader(){

	preqpf = new Preqpf();
	preqpf.setJrnseq(0);
	preqpf.setRdocpfx(fsupfxcpy.reqn.toString());
	preqpf.setRdoccoy(bsprIO.getCompany().toString());
	preqpf.setRdocnum(wsaaRdockey.rdocRdocnum.toString());
	preqpf.setRldgcoy(bsprIO.getCompany().toString());
	preqpf.setRldgacct(t3629rec.bankcode.toString());
	preqpf.setGenlcoy(bsprIO.getCompany().toString());
	preqpf.setGenlcur(wsaaOrigcurr.toString());
	b1000CallChkcurr();
	preqpf.setCrate(BigDecimal.valueOf(chkcurrrec.rate.toLong()));
	glsubstrec.glsubstRec.set(SPACES);
	glsubstrec.function.set("SBALL");
	glsubstrec.company.set(bsprIO.getCompany());
	glsubstrec.branch.set(bsprIO.getDefaultBranch());
	glsubstrec.glacct.set(wsaaHdrGenlkey);
	glsubstrec.bankcode.set(t3629rec.bankcode);
	glsubstrec.currency.set(wsaaOrigcurr);
	callProgram(Glsubst.class, glsubstrec.glsubstRec);
	if (isNE(glsubstrec.statuz,varcom.oK)) {
		syserrrec.params.set(glsubstrec.glsubstRec);
		syserrrec.statuz.set(glsubstrec.statuz);
		fatalError600();
	}
	preqpf.setGlcode(glsubstrec.glacct.toString());
	preqpf.setSacscode(t3672rec.sacscode.toString());
	preqpf.setSacstyp(t3672rec.sacstyp.toString());
	preqpf.setGlsign(wsaaHdrSign.toString());
	preqpf.setCnttot(wsaaHdrContot.toInt());
	preqpf.setBranch(bsprIO.getDefaultBranch().toString());
	preqpf.setPostmonth(batcdorrec.actmonth.toString());
	preqpf.setPostyear(batcdorrec.actyear.toString());
	preqpf.setEffdate(bsscIO.getEffectiveDate().toInt());
	preqpf.setTrandesc(tranDesc);
	preqpf.setTranref(SPACES.toString());
	preqpf.setOrigamt(subTotal);
	preqpf.setAcctamt(subTotal);
	preqpf.setOrigccy(wsaaOrigcurr.toString());
	preqpf.setTaxcat(SPACES.toString());
	preqpf.setFrcdate(varcom.vrcmMaxDate.toInt());
	preqpf.setRcamt(BigDecimal.ZERO);
	preqpf.setExtra(SPACES.toString());
	preqpf.setTrdt(Integer.parseInt(getCobolDate()));
	preqpf.setTrtm(varcom.vrcmTime.toInt());
	preqpf.setUser_t(varcom.vrcmUser.toInt());
	preqpf.setTermid(varcom.vrcmTermid.toString());
	preqHeaderBulkInsList.add(preqpf);
}

protected void commitPreqHeaderBulkInsert(){	 
	 boolean isInsertPreqPF = preqpfDAO.insertPreqPF(preqHeaderBulkInsList);	
	if (!isInsertPreqPF) {
		LOGGER.error("Insert PreqPF record failed.");
		fatalError600();
	}else preqHeaderBulkInsList.clear();
}

protected void writeCheqRecord(){

	cheqpf = new Cheqpf();
	cheqpf.setReqnno(wsaaRdockey.rdocRdocnum.toString());
	cheqpf.setReqncoy(bsprIO.getCompany().toString());
	cheqpf.setBranch(bsprIO.getDefaultBranch().toString());
	readT5690();
	cheqpf.setReqntype(t5690rec.paymentMethod.toString());
	if (isEQ(t5690rec.paymentMethod,"1")) {
		ctrCT03++;
	}
	if (isEQ(t5690rec.paymentMethod,"4")) {
		ctrCT04++;
	}
	cheqpf.setCheqno(SPACES.toString());
	cheqpf.setPayamt(subTotal);
	cheqpf.setPaycurr(wsaaOrigcurr.toString());
	cheqpf.setBankkey(br646DTO.getBankkey());
	cheqpf.setBankacckey(br646DTO.getBankacckey());
	cheqpf.setFacthous(br646DTO.getFacthous());
	cheqpf.setReqnbcde(t3629rec.bankcode.toString());
	cheqpf.setClntcoy(bsprIO.getFsuco().toString());
	cheqpf.setClntnum01(wsaaClntnum.toString());
	cheqpf.setClntnum02(SPACES.toString());
	cheqpf.setClttype01(br646DTO.getClttype());
	cheqpf.setClttype02(SPACES.toString());
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.language.set(br646DTO.getLanguage());
	namadrsrec.clntNumber.set(medxTemppf.getClntnum());
	namadrsrec.clntPrefix.set(br646DTO.getClntpfx());
	namadrsrec.clntCompany.set(br646DTO.getClntcoy());
	namadrsrec.function.set("LGNMN");
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.params.set(namadrsrec.statuz);
		fatalError600();
	}
	if (isEQ(br646DTO.getClttype(),"C")) {
		cheqpf.setSurnam01(namadrsrec.name.toString());
		cheqpf.setSalninit02(SPACES.toString());
	}
	else {
		cheqpf.setSurnam01(br646DTO.getSurname());
		if (isEQ(tr393rec.salutind,"S")) {
			readT3583Desc();
			StringBuilder sb = new StringBuilder();
			sb.append(shortDesc);
			sb.append(" ");
			sb.append(br646DTO.getInitials());
			sb.append(" ");
			cheqpf.setSalninit01(sb.toString());
		}
		else {		
			StringBuilder sb = new StringBuilder();
			sb.append(br646DTO.getSalutl());
			sb.append(" ");
			sb.append(br646DTO.getInitials());
			sb.append(" ");
			cheqpf.setSalninit01(sb.toString());
		}
	}
	cheqpf.setSurnam02(SPACES.toString());
	cheqpf.setSalninit02(SPACES.toString());
	/*    Whether personal or corporate client, use longname*/
	StringUtil stringVariable3 = new StringUtil();
	stringVariable3.addExpression(br646DTO.getLsurname());
	stringVariable3.addExpression(" ");
	stringVariable3.addExpression(br646DTO.getLgivname());
	stringVariable3.setStringInto(spcoutrec.field);
	callProgram(Spcout.class, spcoutrec.spcoutRec);
	upperrec.name.set(spcoutrec.field);
	upperrec.statuz.set(SPACES);
	callProgram(Upper.class, upperrec.upperRec);
	if (isNE(upperrec.statuz,varcom.oK)) {
		upperrec.name.set(spcoutrec.field);
	}
	cheqpf.setCapname(upperrec.name.toString());
	cheqpf.setCheqdupn(SPACES.toString());
	setCheqProcind3420();
	cheqpf.setPaydate(bsscIO.getEffectiveDate().toInt());
	cheqpf.setPresflag("N");
	cheqpf.setPresdate(varcom.vrcmMaxDate.toInt());
	cheqpf.setArchdate(varcom.vrcmMaxDate.toInt());
	cheqpf.setStmtpage(0);
	cheqpf.setTrdt(Integer.parseInt(getCobolDate()));
	cheqpf.setTrtm(varcom.vrcmTime.toInt());
	cheqpf.setUserT(varcom.vrcmUser.toInt());
	cheqpf.setTermid(varcom.vrcmTermid.toString());
	cheqpf.setPresdate(varcom.vrcmMaxDate.toInt());
	cheqpf.setZprnno(0);
	cheqBulkInsList.add(cheqpf);
}

protected void commitCheqBulkInsert(){	 
	 boolean isInsertCheqPF = cheqpfDAO.insertCheqPF(cheqBulkInsList);	
	if (!isInsertCheqPF) {
		LOGGER.error("Insert PreqPF record failed.");
		fatalError600();
	}else cheqBulkInsList.clear();
}

protected void setCheqProcind3420()
	{
		start3420();
	}

protected void start3420()
	{
		cheqpf.setRequser(0);
		cheqpf.setReqdate(0);
		cheqpf.setReqtime(0);
		cheqpf.setAuthuser(0);
		cheqpf.setAuthdate(0);
		cheqpf.setAuthtime(0);
		cheqpf.setAppruser(0);
		cheqpf.setApprdte(0);
		cheqpf.setApprtime(0);
		cheqpf.setRequser(varcom.vrcmUser.toInt());
		cheqpf.setReqdate(wsaaToday.toInt());
		cheqpf.setReqtime(Integer.parseInt(getCobolTime()));
		if (isEQ(t3672rec.preAuthInd,"Y")) {
			cheqpf.setProcind(reqproccpy.auth.toString());
			cheqpf.setAuthuser(cheqpf.getRequser());
			cheqpf.setAppruser(cheqpf.getRequser());
			cheqpf.setAuthdate(cheqpf.getReqdate());
			cheqpf.setApprdte(cheqpf.getReqdate());
			cheqpf.setAuthtime(cheqpf.getReqtime());
			cheqpf.setApprtime(cheqpf.getReqtime());
		}
		else {
			cheqpf.setProcind(reqproccpy.reqn.toString());
		}
	}

protected void updateChdr(){
	chdrpf = new Chdrpf();
	chdrpf.setUniqueNumber(br646DTO.getChdrUniqueNumber());
	chdrpf.setTranno(wsaaTranno.toInt() + chdrBulkUpdtList.size());
	chdrTranno = Long.toString(wsaaTranno.toInt() + chdrBulkUpdtList.size());
	chdrBulkUpdtList.add(chdrpf);		
}

protected void commitChdrBulkUpdate()
{	 
	boolean isUpdateChdr = chdrpfDAO.updateChdrTrannoByUniqueNo(chdrBulkUpdtList);
	if (!isUpdateChdr) {
		LOGGER.error("Update CHDR record failed.");
		fatalError600();
	}else chdrBulkUpdtList.clear();
}


protected void writePtrnRecord(){

	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrpfx(br646DTO.getChdrpfx());
	ptrnpf.setChdrcoy(medxTemppf.getChdrcoy());
	ptrnpf.setChdrnum(medxTemppf.getChdrnum());
	ptrnpf.setTranno(wsaaTranno.toInt() + ptrnBulkInsList.size());
	ptrnpf.setTrdt(Integer.parseInt(getCobolDate()));
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setTermid(varcom.vrcmTermid.toString());
	ptrnpf.setUserT(varcom.vrcmUser.toInt());
	ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
	ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());	
	ptrnpf.setValidflag("1");	
	ptrnpf.setBatccoy(batcdorrec.company.toString());
	ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
	ptrnpf.setBatcbrn(batcdorrec.branch.toString());
	ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
	ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnpf.setBatcbatch(batcdorrec.batch.toString());
	ptrnBulkInsList.add(ptrnpf);
}

protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		LOGGER.error("Insert PtrnPF record failed.");
		fatalError600();
	}else ptrnBulkInsList.clear();
}


protected void updateAcmv3620()
	{
		start3620();
	}

protected void start3620()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(medxTemppf.getChdrnum());
		lifacmvrec.rldgacct.set(medxTemppf.getChdrnum());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		b2000CallLifacmv();
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(medxTemppf.getChdrnum());
		lifacmvrec.rldgacct.set(medxTemppf.getChdrnum());
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		b2000CallLifacmv();
	}

protected void updateMediRecord(){	
	medipf = new Medipf();
	//readh portion of the old code
	medipf.setChdrcoy(medxTemppf.getChdrcoy());
	medipf.setChdrnum(medxTemppf.getChdrnum());
	medipf.setLife(medxTemppf.getLife());
	medipf.setJlife(medxTemppf.getJlife());
	medipf.setSeqno(medxTemppf.getSeqno());
	medipf.setZmedtyp(medxTemppf.getZmedtyp());
	medipf.setExmcode(medxTemppf.getExmcode());
	medipf.setZmedfee(medxTemppf.getZmedfee());
	medipf.setPaidby(medxTemppf.getPaidby());
	medipf.setInvref(medxTemppf.getInvref());
	medipf.setEffdate(medxTemppf.getEffdate());
	//end readh	
	medipf.setActind("4");
	medipf.setPaydte(bsscIO.getEffectiveDate().toInt());
	medipf.setTranno(wsaaTranno.toInt() + mediBulkUpdList.size());
	medipf.setUniqueNumber(br646DTO.getMediUniqueNo());
	mediBulkUpdList.add(medipf);
}

protected void commitMediUpdate(){
	boolean isUpdateMedipf;
	isUpdateMedipf = medipfDAO.updateMedipf(mediBulkUpdList);
	if (!isUpdateMedipf) {
		LOGGER.error("Update Medipf record failed.");
		fatalError600();
	}else mediBulkUpdList.clear();	
}

protected void allocateNextNo3800()
	{
		start3800();
	}

protected void start3800()
	{
		readT3629();
		readT3688();
		alocnorec.function.set("NEXT ");
		alocnorec.prefix.set(fsupfxcpy.reqn);
		alocnorec.genkey.set(t3688rec.reqnpfx);
		alocnorec.company.set(bsprIO.getCompany());
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz,varcom.oK)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		wsaaRdockey.rdocRdocnum.set(alocnorec.alocNo);
		/*  Read T5690 to get payment method*/
		readT5690();
		readT3672();
		chkbkcdrec.function.set("REQNBANK");
		chkbkcdrec.bankcode.set(t3629rec.bankcode);
		chkbkcdrec.user.set(varcom.vrcmUser);
		chkbkcdrec.company.set(bsprIO.getCompany());
		callProgram(Chkbkcd.class, chkbkcdrec.chkbkcdRec);
		if (isNE(chkbkcdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(chkbkcdrec.statuz);
			fatalError600();
		}
		wsaaOrigcurr.set(chkbkcdrec.currcode);
		wsaaHdrContot.set(chkbkcdrec.cdcontot);
		wsaaHdrGenlkey.set(chkbkcdrec.genlAccount);
		wsaaHdrSign.set(chkbkcdrec.sign);
	}

protected void commit3500(){
	if (!noRecordFound){
		commitControlTotals();
		commitChdrBulkUpdate();
		commitPtrnBulkInsert();
		commitMediUpdate();
		commitPreqBulkInsert();
		commitPreqHeaderBulkInsert();
		commitCheqBulkInsert();
	}
}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000(){
	if (!medxList.isEmpty()){
		chdrBulkUpdtList.clear();
		chdrBulkUpdtList = null;
		
		ptrnBulkInsList.clear();
		ptrnBulkInsList = null;
		
		cheqBulkInsList.clear();
		cheqBulkInsList = null;	
		
		preqBulkInsList.clear();
		preqBulkInsList = null;	
		
		preqHeaderBulkInsList.clear();
		preqHeaderBulkInsList = null;		
		
		mediBulkUpdList.clear();
		mediBulkUpdList=null;
		
		t5645ListMap.clear();
		t5645ListMap = null;
		
		tr393ListMap.clear();
		tr393ListMap = null;
		
		t3629ListMap.clear();
		t3629ListMap = null;	
		
		t3688ListMap.clear();
		t3688ListMap = null;	
		
		t5690ListMap.clear();
		t5690ListMap = null;	
		
		t3672ListMap.clear();
		t3672ListMap = null;		
		
		t1688List.clear();
		t1688List=null;
		
		lsaaStatuz.set(varcom.oK);
	}
}

protected void b1000CallChkcurr()
	{
		b1000Start();
	}

protected void b1000Start()
	{
		initialize(chkcurrrec.chkcurrRec);
		chkcurrrec.rate.set(ZERO);
		chkcurrrec.company.set(bsprIO.getCompany());
		chkcurrrec.currcode.set(wsaaOrigcurr);
		chkcurrrec.date_var.set(bsscIO.getEffectiveDate());
		chkcurrrec.function.set("RATE");
		callProgram(Chkcurr.class, chkcurrrec.chkcurrRec);
		if (isNE(chkcurrrec.statuz,varcom.oK)) {
			syserrrec.params.set(chkcurrrec.chkcurrRec);
			syserrrec.statuz.set(chkcurrrec.statuz);
			fatalError600();
		}
	}

protected void b2000CallLifacmv()
	{
		b2000Begin();
	}

protected void b2000Begin()
	{
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.tranno.set(chdrTranno);
		lifacmvrec.rldgcoy.set(medxTemppf.getChdrcoy());
		lifacmvrec.genlcoy.set(medxTemppf.getChdrcoy());
		lifacmvrec.origcurr.set(br646DTO.getCntcurr());
		lifacmvrec.origamt.set(medxTemppf.getZmedfee());
		lifacmvrec.tranref.set(medxTemppf.getChdrnum());
		lifacmvrec.trandesc.set(tranDesc);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(getCobolDate());
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.substituteCode[1].set(br646DTO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
	}
}
