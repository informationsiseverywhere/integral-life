/*
 * File:  Pd5f6.java
 * Author: asirvya
 * 
 * Class transformed from  Pd5f6.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.Sd5f6ScreenVars;
import com.csc.life.productdefinition.tablestructures.Td5f6rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.csc.util.SmartTableUtility;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
 *
 *
 *****************************************************************
 * </pre>
 */
public class  Pd5f6 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5F6");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private String e031 = "E031";
	private String e910 = "E910";
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Td5f6rec td5f6rec = new Td5f6rec();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itmdkey wsaaItmdkey = new Itmdkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5f6ScreenVars sv = ScreenProgram.getScreenVars(Sd5f6ScreenVars.class);

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, cont1190, preExit, exit2090, exit3900, exit5090
	}

	public  Pd5f6() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5f6", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1001();
				}
				case cont1190: {
					cont1190();
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1001() {
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setDataArea(SPACES);
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			itemIO.setStatuz(e031);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		td5f6rec.td5f6Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(), SPACES)) {
			goTo(GotoLabel.cont1190);
		}
	}

	protected void cont1190() {
		sv.fhbcl.set(td5f6rec.fhbcl);
		sv.fhscc.set(td5f6rec.fhscc);
		sv.fhfp.set(td5f6rec.fhfp);
		sv.dhbcl.set(td5f6rec.dhbcl);
		sv.dhscc.set(td5f6rec.dhscc);
		
		sv.fundPoolPri01.set(td5f6rec.fundPoolPri01);
		sv.fundPoolPri02.set(td5f6rec.fundPoolPri02);
		sv.fundPoolPri03.set(td5f6rec.fundPoolPri03);
		sv.fundPoolPri04.set(td5f6rec.fundPoolPri04);
		sv.fundPoolPri05.set(td5f6rec.fundPoolPri05);
		sv.fundPoolPri06.set(td5f6rec.fundPoolPri06);
		sv.fundPoolPri07.set(td5f6rec.fundPoolPri07);
		sv.fundPoolPri08.set(td5f6rec.fundPoolPri08);
		sv.fundPoolPri09.set(td5f6rec.fundPoolPri09);
		sv.fundPoolPri10.set(td5f6rec.fundPoolPri10);
		sv.fundPoolPri11.set(td5f6rec.fundPoolPri11);
		sv.fundPoolPri12.set(td5f6rec.fundPoolPri12);
	}

	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

	protected void screenEdit2000() {
		try {
			screenIo2001();
		} catch (GOTOException e) {
		}
	}

	protected void screenIo2001() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		} else {
			wsspcomn.edterror.set(varcom.oK);
		}
		validate();
		/* VALIDATE */
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
	}

	protected void update3000() {
		try {
			loadWsspFields3100();
		} catch (GOTOException e) {
		}
	}

	protected void loadWsspFields3100() {
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3900);
		}
		updateRec5000();
		/* COMMIT */
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}

	protected void updateRec5000() {
		try {
			compareFields5020();
		} catch (GOTOException e) {
		}
	}

	protected void para5000() {
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		callItemio5100();
	}

	protected void compareFields5020() {
		wsaaUpdateFlag = "N";
		checkChanges5300();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.exit5090);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);

		Itempf itempf = SmartTableUtility.getItempfByItemTableDAM(itemIO);
		itempf.setGenarea(td5f6rec.td5f6Rec.toString().getBytes());
		itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
				.getGENAREAJString(td5f6rec.td5f6Rec.toString().getBytes(), td5f6rec));
		itemDAO.updateSmartTableItem(itempf);
	}
	

	protected void callItemio5100() {
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(itemIO.getParams());
			fatalError600();
		}
		/* EXIT */
	}

	protected void checkChanges5300() {
		para5300();
	}

	protected void para5300() {
		if (isNE(sv.fhbcl, td5f6rec.fhbcl)) {
			td5f6rec.fhbcl.set(sv.fhbcl.toString().toUpperCase());
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fhscc, td5f6rec.fhscc)) {
			td5f6rec.fhscc.set(sv.fhscc.toString().toUpperCase());
			wsaaUpdateFlag = "Y";
		}
		
		if (isNE(sv.fhfp, td5f6rec.fhfp)) {
			td5f6rec.fhfp.set(sv.fhfp);
			wsaaUpdateFlag = "Y";
		}

		if (isNE(sv.dhbcl, td5f6rec.dhbcl)) {
			td5f6rec.dhbcl.set(sv.dhbcl.toString().toUpperCase());
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.dhscc, td5f6rec.dhscc)) {
			td5f6rec.dhscc.set(sv.dhscc.toString().toUpperCase());
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri01, td5f6rec.fundPoolPri01)) {
			td5f6rec.fundPoolPri01.set(sv.fundPoolPri01);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri02, td5f6rec.fundPoolPri02)) {
			td5f6rec.fundPoolPri02.set(sv.fundPoolPri02);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri03, td5f6rec.fundPoolPri03)) {
			td5f6rec.fundPoolPri03.set(sv.fundPoolPri03);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri04, td5f6rec.fundPoolPri04)) {
			td5f6rec.fundPoolPri04.set(sv.fundPoolPri04);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri05, td5f6rec.fundPoolPri05)) {
			td5f6rec.fundPoolPri05.set(sv.fundPoolPri05);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri06, td5f6rec.fundPoolPri06)) {
			td5f6rec.fundPoolPri06.set(sv.fundPoolPri06);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri07, td5f6rec.fundPoolPri07)) {
			td5f6rec.fundPoolPri07.set(sv.fundPoolPri07);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri08, td5f6rec.fundPoolPri08)) {
			td5f6rec.fundPoolPri08.set(sv.fundPoolPri08);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri08, td5f6rec.fundPoolPri08)) {
			td5f6rec.fundPoolPri08.set(sv.fundPoolPri08);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri09, td5f6rec.fundPoolPri09)) {
			td5f6rec.fundPoolPri09.set(sv.fundPoolPri09);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri10, td5f6rec.fundPoolPri10)) {
			td5f6rec.fundPoolPri10.set(sv.fundPoolPri10);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri11, td5f6rec.fundPoolPri11)) {
			td5f6rec.fundPoolPri11.set(sv.fundPoolPri11);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fundPoolPri12, td5f6rec.fundPoolPri12)) {
			td5f6rec.fundPoolPri12.set(sv.fundPoolPri12);
			wsaaUpdateFlag = "Y";
		}
	}
	protected void exit2090() {
		
	}
	protected void validate(){
		if ((isNE(sv.fhbcl, "C") && isNE(sv.fhbcl, "c")) && (isNE(sv.fhbcl, "B") && isNE(sv.fhbcl, "b"))) {
			sv.fhbclErr.set(e910);
			wsspcomn.edterror.set("Y");
		}
		if ((isNE(sv.dhbcl, "C") && isNE(sv.dhbcl, "c")) && (isNE(sv.dhbcl, "B") && isNE(sv.dhbcl, "b"))) {
			sv.dhbclErr.set(e910);
			wsspcomn.edterror.set("Y");
		}
	}
}
