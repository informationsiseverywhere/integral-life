package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.Br559DAO;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br559DAOImpl extends BaseDAOImpl<MedxTemppf>implements Br559DAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br559DAOImpl.class);

	public List<MedxTemppf> loadDataByBatch(int batchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM BR559DATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY PAIDBY, EXMCODE, CHDRNUM ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<MedxTemppf> medxList = new ArrayList<MedxTemppf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				MedxTemppf medxpf = new MedxTemppf();
				medxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				medxpf.setChdrcoy(rs.getString("CHDRCOY").trim());
				medxpf.setChdrnum(rs.getString("CHDRNUM").trim());
				medxpf.setSeqno(rs.getInt("SEQNO"));
				medxpf.setPaidby(rs.getString("PAIDBY").trim());
				medxpf.setExmcode(rs.getString("EXMCODE").trim());
				medxpf.setZmedtyp(rs.getString("ZMEDTYP").trim());
				medxpf.setEffdate(rs.getInt("EFFDATE"));
				medxpf.setInvref(rs.getString("INVREF") == null ? "":  rs.getString("INVREF").trim()); //ILIFE-8129
				medxpf.setZmedfee(rs.getBigDecimal("ZMEDFEE"));
				medxpf.setLife(rs.getString("LIFE").trim());
				medxpf.setJlife(rs.getString("JLIFE").trim());
				medxpf.setMemberName(rs.getString("MEMBER_NAME").trim());
				medxpf.setLifcnum(rs.getString("LIFCNUM").trim());
				medxpf.setClntnum(rs.getString("CLNTNUM").trim());		//ILIFE-6447
				medxList.add(medxpf);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return medxList;
	}	
	
	public int populateBr559Temp(int batchExtractSize, String medxtempTable, String chdrfrom, String chdrto) {
		initializeBr559Temp();
		int rows = 0;
		boolean isParameterized = false;
		boolean isParamFromOnly = false;
		boolean isParamToOnly = false;
		if (!("".equals(chdrfrom.trim())) && !("".equals(chdrto.trim()))) {
			if (Integer.parseInt(chdrto) >= Integer.parseInt(chdrfrom)) isParameterized = true;
		}
		
		if (!("".equals(chdrfrom.trim())) && "".equals(chdrto.trim())) isParamFromOnly = true;
		if (!("".equals(chdrto.trim())) && "".equals(chdrfrom.trim())) isParamToOnly = true;
		StringBuilder sb = new StringBuilder("");
		 
		sb.append("INSERT INTO VM1DTA.BR559DATA "); //ILB-475
		sb.append("(BATCHID, UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SEQNO, EXMCODE, ZMEDTYP, PAIDBY, LIFE, JLIFE, EFFDATE, INVREF, ZMEDFEE, CLNTNUM, DTETRM, DESC_T, MEMBER_NAME, LIFCNUM)  ");
		sb.append("SELECT * FROM(  ");
		sb.append("SELECT floor((row_number()over(ORDER BY MAIN.CHDRCOY, MAIN.CHDRNUM, MAIN.SEQNO DESC)-1)/?) BATCHID, MAIN.* FROM (  ");
		sb.append("SELECT MEDX.UNIQUE_NUMBER, MEDX.CHDRCOY, MEDX.CHDRNUM, MEDX.SEQNO, MEDX.EXMCODE, MEDX.ZMEDTYP, MEDX.PAIDBY, MEDX.LIFE, MEDX.JLIFE, MEDX.EFFDATE, MEDX.INVREF, MEDX.ZMEDFEE, MEDX.CLNTNUM, MEDX.DTETRM, MEDX.DESC_T, MEDX.MEMBER_NAME, ");
		sb.append("LIFE.LIFCNUM FROM  ");
		sb.append(medxtempTable); 
		sb.append(" MEDX INNER JOIN LIFEPF LIFE ON LIFE.CHDRCOY = MEDX.CHDRCOY AND LIFE.CHDRNUM = MEDX.CHDRNUM AND LIFE.LIFE = MEDX.LIFE AND LIFE.JLIFE = MEDX.JLIFE ");
		sb.append("GROUP BY MEDX.UNIQUE_NUMBER, MEDX.CHDRCOY, MEDX.CHDRNUM, MEDX.SEQNO, MEDX.EXMCODE, MEDX.ZMEDTYP, MEDX.PAIDBY, MEDX.LIFE, MEDX.JLIFE, MEDX.EFFDATE, MEDX.INVREF, MEDX.ZMEDFEE, MEDX.CLNTNUM, MEDX.DTETRM, MEDX.DESC_T, MEDX.MEMBER_NAME, LIFE.LIFCNUM");
		sb.append(") MAIN ");
		if (isParameterized)
			sb.append("WHERE MAIN.CHDRNUM BETWEEN ? and ? ");	
		if (isParamFromOnly || isParamToOnly)
			sb.append("WHERE MAIN.CHDRNUM = ? ");	
		sb.append(") MAIN ORDER BY MAIN.CHDRCOY ASC, MAIN.CHDRNUM ASC, MAIN.SEQNO  ");

		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			if (isParameterized){
				ps.setString(2, chdrfrom);
				ps.setString(3, chdrto);
			}
			if (isParamFromOnly) ps.setString(2, chdrfrom);
			if (isParamToOnly) ps.setString(2, chdrto);
			
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBr559Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeBr559Temp() {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM BR559DATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			int rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBr559Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}		


}