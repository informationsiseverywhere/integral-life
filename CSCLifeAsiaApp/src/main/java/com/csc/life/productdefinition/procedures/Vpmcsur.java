/*
 * File: Vpmcsur.java
 * Date: 3 Oct 2012 1:58:03
 * Author: Quipoz Limited
 * 
 * Class transformed from Vpmcsur.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*    VPMS-Bonus surrender calculation.
*
* </pre>
*/
public class Vpmcsur extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private String wsaaSubr = "VPMCSUR";
	private String wsccP = "P";
		/* ERRORS */
	private String h053 = "H053";
	
	private String tr28x = "TR28X";
	private String t5687 = "T5687";
	
	private String itdmrec = "ITDMREC";
	private String chdrrec = "CHDRREC";
	
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();	
	
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T5687rec t5687rec = new T5687rec();
	private Varcom varcom = new Varcom();
	
	/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	
	private static final String SUBROUTINE_PREFIX = "CSUR";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit1090, exit0190, 
	}

	public Vpmcsur() {
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start0110();
				}
				case exit0190: {
					exit0190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start0110()
{
	vpmcalcrec.vpmcalcRec.set(SPACES);
	srcalcpy.status.set(varcom.oK);
	vpmcalcrec.statuz.set(varcom.oK);

	setupKey1000();
	if (isNE(srcalcpy.status, varcom.oK)){
		goTo(GotoLabel.exit1090);
	}
	
	vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
	callProgram(Calccsur.class, vpmcalcrec.vpmcalcRec);
	srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
	
	if (isNE(vpmcalcrec.statuz, varcom.oK)){
		srcalcpy.status.set(vpmcalcrec.statuz);
	}
	
}

protected void setupKey1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1010();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
{
	//Read T5687 to get the surrender method.
	itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
	itdmIO.setItemtabl(t5687);
	itdmIO.setItemitem(srcalcpy.crtable);
	itdmIO.setItmfrm(srcalcpy.effdate);
	itdmIO.setFormat(itdmrec);
	itdmIO.setFunction(varcom.begn);
//	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//	itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.statuz, varcom.oK)
	|| isNE(srcalcpy.chdrChdrcoy, itdmIO.getItemcoy())
	|| isNE(t5687, itdmIO.getItemtabl())
	|| isNE(srcalcpy.crtable, itdmIO.getItemitem())) {
		srcalcpy.status.set(h053);
		goTo(GotoLabel.exit1090);
	}
	t5687rec.t5687Rec.set(itdmIO.getGenarea());
	
	vpmcalcrec.vpmskey.set(SPACES);
	vpmcalcrec.vpmscoy.set(srcalcpy.chdrChdrcoy);
	vpmcalcrec.vpmstabl.set(tr28x);
	vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX);
	readChdr1100();
	vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX + chdrIO.getCnttype().getData().trim());/* IJTI-1523 */
	
	if (isEQ(srcalcpy.type, wsccP)){
		vpmcalcrec.extkey.set(t5687rec.partsurr);
	} else {
		vpmcalcrec.extkey.set(t5687rec.svMethod);
	}
}

protected void readChdr1100()
{
	initialize(chdrIO.getParams());
	chdrIO.setStatuz(varcom.oK);
	chdrIO.setChdrpfx(fsupfxcpy.chdr);
	chdrIO.setChdrcoy(srcalcpy.chdrChdrcoy);
	chdrIO.setChdrnum(srcalcpy.chdrChdrnum);
	chdrIO.setValidflag("1");
	chdrIO.setCurrfrom(varcom.maxdate);
	chdrIO.setFormat(chdrrec);
	chdrIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, chdrIO);
	if (isNE(chdrIO.statuz, varcom.oK)
			|| isNE(srcalcpy.chdrChdrnum,chdrIO.getChdrnum())) {
		chdrIO.cnttype.set(SPACES);
	} 
}

protected void exit0190()
	{
		exitProgram();
		goBack();
		stopRun();
	}

}
