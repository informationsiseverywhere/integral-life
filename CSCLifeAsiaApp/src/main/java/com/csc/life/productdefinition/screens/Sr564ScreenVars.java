package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR564
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr564ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(300);
	public FixedLengthStringData dataFields = new FixedLengthStringData(140).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData crcode = DD.crcode.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData coverDesc = DD.cvdes.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,101);
	//BRD-139-STARTS
	public FixedLengthStringData redfreq = DD.redfreq.copy().isAPartOf(dataFields,103);
	public ZonedDecimalData unitval = DD.unitval.copyToZonedDecimal().isAPartOf(dataArea,123);
	//BRD-139-ENDS
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 140);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData crcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cvdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	//BRD-139-STARTS
	public FixedLengthStringData redfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData unitvalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	//BRD-139-ENDS
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 180);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] crcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cvdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	//BRD-139-STARTS
	public FixedLengthStringData[] redfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] unitvalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	//BRD-139-ENDS

	public FixedLengthStringData subfileArea = new FixedLengthStringData(87);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(37).isAPartOf(subfileArea, 0);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData surrval = DD.surrval.copyToZonedDecimal().isAPartOf(subfileFields,17);
	public ZonedDecimalData yrsinf = DD.yrsinf.copyToZonedDecimal().isAPartOf(subfileFields,34);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(12).isAPartOf(subfileArea, 37);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData surrvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData yrsinfErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 49);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] surrvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] yrsinfOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 85);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData mrt1Flag = new FixedLengthStringData(1);//BRD-139
	public FixedLengthStringData policyType = new FixedLengthStringData(1);//ILIFE-7521
	public LongData Sr564screensflWritten = new LongData(0);
	public LongData Sr564screenctlWritten = new LongData(0);
	public LongData Sr564screenWritten = new LongData(0);
	public LongData Sr564protectWritten = new LongData(0);
	public GeneralTable sr564screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr564screensfl;
	}

	public Sr564ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(yrsinfOut,new String[] {"01","05","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(surrvalOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {yrsinf, sumins, surrval};
		screenSflOutFields = new BaseData[][] {yrsinfOut, suminsOut, surrvalOut};
		screenSflErrFields = new BaseData[] {yrsinfErr, suminsErr, surrvalErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, lifenum, crcode, life, coverage, rider, lifename, coverDesc,redfreq,unitval};
		screenOutFields = new BaseData[][] {chdrnumOut, lifenumOut, crcodeOut, lifeOut, coverageOut, riderOut, lifenameOut, cvdesOut,redfreqOut,unitvalOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifenumErr, crcodeErr, lifeErr, coverageErr, riderErr, lifenameErr, cvdesErr,redfreqErr,unitvalErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr564screen.class;
		screenSflRecord = Sr564screensfl.class;
		screenCtlRecord = Sr564screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr564protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr564screenctl.lrec.pageSubfile);
	}
}
