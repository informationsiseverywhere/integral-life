/*
 * File: Pr558.java
 * Date: 30 August 2009 1:40:55
 * Author: Quipoz Limited
 * 
 * Class transformed from PR558.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Covrmjakey;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.enquiries.dataaccess.AcblenqTableDAM;
import com.csc.life.enquiries.dataaccess.AsgnenqTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.screens.Sr558ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th611rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This window is to use to quote for the new instalment premium when
* the billing frequency has to be changed.
*
*  Initialise
*  ----------
*  Prepare Contract Header Section showing the current status,
*  life assured, owner and billing details (Bill frequency &
*  current instalement premium) as well.
*  Retreive the above necessary details using RETRV command and
*  the CHDRENQ logical view and then further refernece with
*  the other logical views like:
*      LIFEENQ
*      PAYR
*
*  Then determine any outstanding premium from CHDRENQ and
*  related loan pincipal and interest from ACBLENQ logical
*  view.
*
*
*  Validation
*  ----------
*  If EXIT is pressed, then skip the validation.
*  If REFRESH  is pressed then re-display  the screen by
*     setting WSSP-EDTERROR to 'Y'.
*  Validate for new billing frequency entry and which is applicable
*  for the elapse period of origianl commencement date and pay-to-date.
*  For each COVR,
*  Calculate the old & new premium via accumulating all INSTPREM
*  and New instalment premium from valid COVRs.
*  New instalement premium = OLD inst. prem * (Old bill freq/
*                            New bill freq) *
*                           (New Load Factor/ Old Load Factor)
*  where Load factor can be retrieved from T5541.
*  Contract Fee will be finally add up to New instalement premium
*  total.
*  If WOP component(s) can be found, the syetem will include
*  required WOP premium into new instalement premium total.
*
*  This final instalement premium will subject to  the minimum
*  and maximum modal premium limits for a contract type in a
*  specific contract currency.
*
*
*  Updating
*  --------
*  Nothing is to be physically updated.
*
*  Next Program
*  ------------
*  Add 1 to the program pointer and exit from the program.
*
*****************************************************************
*                                                                     *
* </pre>
*/
public class Pr558 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR558");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaAplloan = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAplint = new ZonedDecimalData(13, 2);
	private ZonedDecimalData wsaaLoanvalue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaLoanint = new ZonedDecimalData(13, 2);
	private ZonedDecimalData wsaaFactor = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler = new FixedLengthStringData(11).isAPartOf(wsaaFactor, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaReminder = new ZonedDecimalData(5, 5).isAPartOf(filler, 6);
	private FixedLengthStringData wsaaNewFreqFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOldFreqFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNewLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldinst = new ZonedDecimalData(9, 2).init(0);
	private ZonedDecimalData wsaaNewinst = new ZonedDecimalData(9, 2).init(0);
	private ZonedDecimalData wsaaNewinstamt = new ZonedDecimalData(9, 2).init(0);
	private String wsaaValidComp = "N";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub9 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaStatCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaMinPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaMaxPrem = new ZonedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaFlag1 = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaWaiveIt = new FixedLengthStringData(1).init(SPACES);
	private String wsaaWaiveCont = "";

	private FixedLengthStringData wsaaZrwvflgs = new FixedLengthStringData(3);
	private FixedLengthStringData[] wsaaZrwvflg = FLSArrayPartOfStructure(3, 1, wsaaZrwvflgs, 0);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaBenCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private String wsaaWopCompFound = "";
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaWopSub = new ZonedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData wsaaWopComponents = new FixedLengthStringData(2885);
	private FixedLengthStringData[] wsaaCovrmjakey = FLSArrayPartOfStructure(5, 64, wsaaWopComponents, 0);
	private FixedLengthStringData[] wsaaGenarea = FLSArrayPartOfStructure(5, 500, wsaaWopComponents, 320);
	private ZonedDecimalData[] wsaaWaivePremium = ZDArrayPartOfStructure(5, 13, 2, wsaaWopComponents, 2820);
		/* WSAA-CALC-FREQS */
	private ZonedDecimalData wsaaCalcOldfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCalcNewfreq = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaWsspChdrky = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaWsspChdrpfx = new FixedLengthStringData(2).isAPartOf(wsaaWsspChdrky, 0);
	private FixedLengthStringData wsaaWsspChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaWsspChdrky, 2);
	private FixedLengthStringData wsaaWsspChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaWsspChdrky, 3);

	private FixedLengthStringData wsaaTh611Item = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTh611Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTh611Item, 0);
	private FixedLengthStringData wsaaTh611Currcode = new FixedLengthStringData(3).isAPartOf(wsaaTh611Item, 3);
		/* WSAA-TRANS-AREA */
	private int wsaaEffectiveDate = 0;
	private String rl13 = "RL13";
	private String rl12 = "RL12";
	private String rl11 = "RL11";
	private String h134 = "H134";
	private String h048 = "H048";
	private String e186 = "E186";
	private String f321 = "F321";
	private String t075 = "T075";
	private String t076 = "T076";
	private String e300 = "E300";
		/* TABLES */
	private String t3620 = "T3620";
	private String t5645 = "T5645";
	private String t3590 = "T3590";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String t5679 = "T5679";
	private String t5687 = "T5687";
	private String t5541 = "T5541";
	private String tr384 = "TR384";
	private String th611 = "TH611";
	private String tr517 = "TR517";
	private String t5675 = "T5675";
	private String t5674 = "T5674";
		/* FORMATS */
	private String covrmjarec = "COVRMJAREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String descrec = "DESCREC";
	private String payrrec = "PAYRREC";
	private String itdmrec = "ITEMREC";
		/*Sub Account Balances - Contract Enquiry.*/
	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
		/*Assignee Enquiry Logical*/
	private AsgnenqTableDAM asgnenqIO = new AsgnenqTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Contract Additional Details*/
	private HpadTableDAM hpadIO = new HpadTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Letrqstrec letrqstrec = new Letrqstrec();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Premiumrec premiumrec = new Premiumrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5541rec t5541rec = new T5541rec();
	private T5645rec t5645rec = new T5645rec();
	private T5674rec t5674rec = new T5674rec();
	private T5675rec t5675rec = new T5675rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private Th611rec th611rec = new Th611rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Tr517rec tr517rec = new Tr517rec();
	private Batckey wsaaBatckey = new Batckey();
	private Covrmjakey wsaaCovrmjakeyx = new Covrmjakey();
	private Wssplife wssplife = new Wssplife();
	private Sr558ScreenVars sv = ScreenProgram.getScreenVars( Sr558ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		jointOwnerName1030, 
		contractType1040, 
		exit1090, 
		preExit, 
		premiumCalculation2030, 
		displayPremium2060, 
		checkForErrors2080, 
		exit2090, 
		nextr2100, 
		exit2100, 
		exit2290, 
		exit4090, 
		exit5009, 
		x120CallCovrmja, 
		x190Exit, 
		x13cExit, 
		x220Check, 
		x290Exit, 
		x390Exit
	}

	public Pr558() {
		super();
		screenVars = sv;
		new ScreenModel("Sr558", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					ownerName1020();
				}
				case jointOwnerName1030: {
					jointOwnerName1030();
				}
				case contractType1040: {
					contractType1040();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		initialize(sv.dataArea);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.mrnf)) {
			sv.chdrnumErr.set(h134);
		}
		else {
			sv.chdrnum.set(chdrenqIO.getChdrnum());
			sv.cnttype.set(chdrenqIO.getCnttype());
			sv.cntcurr.set(chdrenqIO.getCntcurr());
			sv.register.set(chdrenqIO.getRegister());
			sv.occdate.set(chdrenqIO.getOccdate());
			sv.nextinsdte.set(chdrenqIO.getBillcd());
			sv.cownnum.set(chdrenqIO.getCownnum());
			if (isNE(chdrenqIO.getJownnum(),SPACES)) {
				sv.jowner.set(chdrenqIO.getJownnum());
			}
			payrIO.setDataArea(SPACES);
		}
		payrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		payrIO.setChdrnum(chdrenqIO.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.ptdate.set(payrIO.getPtdate());
		sv.btdate.set(payrIO.getBtdate());
		sv.mop.set(payrIO.getBillchnl());
		sv.billfreq.set(payrIO.getBillfreq());
		sv.instpramt.set(payrIO.getSinstamt06());
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
		}
		else {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

protected void ownerName1020()
	{
		if (isEQ(sv.cownnum,SPACES)) {
			goTo(GotoLabel.jointOwnerName1030);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
	}

protected void jointOwnerName1030()
	{
		if (isEQ(sv.jowner,SPACES)) {
			goTo(GotoLabel.contractType1040);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.jowner);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.jownername.set(wsspcomn.longconfname);
	}

protected void contractType1040()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.set(SPACES);
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3620);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(sv.mop);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pymdesc.set(descIO.getShortdesc());
		}
		asgnenqIO.setDataKey(SPACES);
		asgnenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		asgnenqIO.setChdrnum(chdrenqIO.getChdrnum());
		asgnenqIO.setSeqno(ZERO);
		asgnenqIO.setAsgnnum(SPACES);
		asgnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		asgnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		asgnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, asgnenqIO);
		if (isNE(asgnenqIO.getStatuz(),varcom.oK)
		&& isNE(asgnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(asgnenqIO.getParams());
			fatalError600();
		}
		if (isNE(asgnenqIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(asgnenqIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isEQ(asgnenqIO.getStatuz(),varcom.endp)) {
			sv.indic.set(SPACES);
		}
		else {
			sv.indic.set("X");
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3590);
		descIO.setDescitem(payrIO.getBillfreq());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.shortds01.set(descIO.getShortdesc());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.chdrnumErr.set(f321);
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.loanvalueErr.set(h134);
			goTo(GotoLabel.exit1090);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaAplloan.set(ZERO);
		acblenqIO.setRldgcoy(wsspcomn.company);
		acblenqIO.setSacscode(t5645rec.sacscode03);
		acblenqIO.setSacstyp(t5645rec.sacstype03);
		acblenqIO.getRldgacct().setSub1String(1, 8, sv.chdrnum);
		acblenqIO.getRldgacct().setSub1String(9, 2, ZERO);
		acblenqIO.setOrigcurr(SPACES);
		acblenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acblenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acblenqIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "SACSTYP");
		acblenqIO.setStatuz(SPACES);
		while ( !(isEQ(acblenqIO.getStatuz(),varcom.endp))) {
			loadAplDetails1600();
		}
		
		wsaaAplint.set(ZERO);
		acblenqIO.setRldgcoy(wsspcomn.company);
		acblenqIO.setSacscode(t5645rec.sacscode04);
		acblenqIO.setSacstyp(t5645rec.sacstype04);
		acblenqIO.getRldgacct().setSub1String(1, 8, sv.chdrnum);
		acblenqIO.getRldgacct().setSub1String(9, 2, ZERO);
		acblenqIO.setOrigcurr(SPACES);
		acblenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acblenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acblenqIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "SACSTYP");
		acblenqIO.setStatuz(SPACES);
		while ( !(isEQ(acblenqIO.getStatuz(),varcom.endp))) {
			loadAplIntDetails1700();
		}
		
		wsaaLoanvalue.set(ZERO);
		acblenqIO.setRldgcoy(wsspcomn.company);
		acblenqIO.setSacscode(t5645rec.sacscode01);
		acblenqIO.setSacstyp(t5645rec.sacstype01);
		acblenqIO.getRldgacct().setSub1String(1, 8, sv.chdrnum);
		acblenqIO.getRldgacct().setSub1String(9, 2, ZERO);
		acblenqIO.setOrigcurr(SPACES);
		acblenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acblenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acblenqIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "SACSTYP");
		acblenqIO.setStatuz(SPACES);
		while ( !(isEQ(acblenqIO.getStatuz(),varcom.endp))) {
			loadDetails1800();
		}
		
		wsaaLoanint.set(ZERO);
		acblenqIO.setRldgcoy(wsspcomn.company);
		acblenqIO.setSacscode(t5645rec.sacscode02);
		acblenqIO.setSacstyp(t5645rec.sacstype02);
		acblenqIO.getRldgacct().setSub1String(1, 8, sv.chdrnum);
		acblenqIO.getRldgacct().setSub1String(9, 2, ZERO);
		acblenqIO.setOrigcurr(SPACES);
		acblenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acblenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acblenqIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "SACSTYP");
		acblenqIO.setStatuz(SPACES);
		while ( !(isEQ(acblenqIO.getStatuz(),varcom.endp))) {
			loadIntDetails1900();
		}
		
		x100AnyWopComponent();
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrenqIO.getBtdate());
		datcon3rec.intDate2.set(chdrenqIO.getPtdate());
		datcon3rec.frequency.set(chdrenqIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit1090);
		}
		wsaaFactor.set(datcon3rec.freqFactor);
		compute(sv.outstamt, 5).set(mult(chdrenqIO.getSinstamt06(),wsaaFactor));
	}

protected void loadAplDetails1600()
	{
		/*APL*/
		SmartFileCode.execute(appVars, acblenqIO);
		if ((isNE(acblenqIO.getStatuz(),varcom.oK))
		&& (isNE(acblenqIO.getStatuz(),varcom.mrnf))
		&& (isNE(acblenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acblenqIO.getParams());
			syserrrec.statuz.set(acblenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(acblenqIO.getSacscode(),t5645rec.sacscode03)
		|| isNE(acblenqIO.getRldgcoy(),wsspcomn.company)
		|| isNE(acblenqIO.getSacstyp(),t5645rec.sacstype03)
		|| isNE(subString(acblenqIO.getRldgacct(), 1, 8),chdrenqIO.getChdrnum())
		|| isNE(acblenqIO.getOrigcurr(),chdrenqIO.getCntcurr())
		|| isEQ(acblenqIO.getStatuz(),varcom.endp)) {
			acblenqIO.setStatuz(varcom.endp);
		}
		else {
			wsaaAplloan.add(acblenqIO.getSacscurbal());
			acblenqIO.setFunction(varcom.nextr);
		}
		sv.aplamt.set(wsaaAplloan);
		/*EXIT*/
	}

protected void loadAplIntDetails1700()
	{
		/*APL-INT*/
		SmartFileCode.execute(appVars, acblenqIO);
		if ((isNE(acblenqIO.getStatuz(),varcom.oK))
		&& (isNE(acblenqIO.getStatuz(),varcom.mrnf))
		&& (isNE(acblenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acblenqIO.getParams());
			syserrrec.statuz.set(acblenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(acblenqIO.getSacscode(),t5645rec.sacscode04)
		|| isNE(acblenqIO.getRldgcoy(),wsspcomn.company)
		|| isNE(acblenqIO.getSacstyp(),t5645rec.sacstype04)
		|| isNE(subString(acblenqIO.getRldgacct(), 1, 8),chdrenqIO.getChdrnum())
		|| isNE(acblenqIO.getOrigcurr(),chdrenqIO.getCntcurr())
		|| isEQ(acblenqIO.getStatuz(),varcom.endp)) {
			acblenqIO.setStatuz(varcom.endp);
		}
		else {
			wsaaAplint.add(acblenqIO.getSacscurbal());
			acblenqIO.setFunction(varcom.nextr);
		}
		sv.aplint.set(wsaaAplint);
		/*EXIT*/
	}

protected void loadDetails1800()
	{
		/*LOAN*/
		SmartFileCode.execute(appVars, acblenqIO);
		if ((isNE(acblenqIO.getStatuz(),varcom.oK))
		&& (isNE(acblenqIO.getStatuz(),varcom.mrnf))
		&& (isNE(acblenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acblenqIO.getParams());
			syserrrec.statuz.set(acblenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(acblenqIO.getSacscode(),t5645rec.sacscode01)
		|| isNE(acblenqIO.getRldgcoy(),wsspcomn.company)
		|| isNE(acblenqIO.getSacstyp(),t5645rec.sacstype01)
		|| isNE(subString(acblenqIO.getRldgacct(), 1, 8),chdrenqIO.getChdrnum())
		|| isNE(acblenqIO.getOrigcurr(),chdrenqIO.getCntcurr())
		|| isEQ(acblenqIO.getStatuz(),varcom.endp)) {
			acblenqIO.setStatuz(varcom.endp);
		}
		else {
			wsaaLoanvalue.add(acblenqIO.getSacscurbal());
			acblenqIO.setFunction(varcom.nextr);
		}
		sv.loanvalue.set(wsaaLoanvalue);
		/*EXIT*/
	}

protected void loadIntDetails1900()
	{
		/*LOAN-INT*/
		SmartFileCode.execute(appVars, acblenqIO);
		if ((isNE(acblenqIO.getStatuz(),varcom.oK))
		&& (isNE(acblenqIO.getStatuz(),varcom.mrnf))
		&& (isNE(acblenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acblenqIO.getParams());
			syserrrec.statuz.set(acblenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(acblenqIO.getSacscode(),t5645rec.sacscode02)
		|| isNE(acblenqIO.getRldgcoy(),wsspcomn.company)
		|| isNE(acblenqIO.getSacstyp(),t5645rec.sacstype02)
		|| isNE(subString(acblenqIO.getRldgacct(), 1, 8),chdrenqIO.getChdrnum())
		|| isNE(acblenqIO.getOrigcurr(),chdrenqIO.getCntcurr())
		|| isEQ(acblenqIO.getStatuz(),varcom.endp)) {
			acblenqIO.setStatuz(varcom.endp);
		}
		else {
			wsaaLoanint.add(acblenqIO.getSacscurbal());
			acblenqIO.setFunction(varcom.nextr);
		}
		sv.plint.set(wsaaLoanint);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validateBillfreq2020();
				}
				case premiumCalculation2030: {
					premiumCalculation2030();
				}
				case displayPremium2060: {
					displayPremium2060();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateBillfreq2020()
	{
		for (wsaaWopSub.set(1); !(isGT(wsaaWopSub,5)); wsaaWopSub.add(1)){
			wsaaWaivePremium[wsaaWopSub.toInt()].set(ZERO);
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.frequency,SPACES)) {
			sv.frequencyErr.set(e186);
			sv.shortds02.set(SPACES);
			wsspcomn.edterror.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.frequency,"12")) {
			goTo(GotoLabel.premiumCalculation2030);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrenqIO.getPtdate());
		datcon3rec.intDate2.set(chdrenqIO.getOccdate());
		datcon3rec.frequency.set(sv.frequency);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaFactor.set(datcon3rec.freqFactor);
		if (isNE(wsaaReminder,0)) {
			sv.frequencyErr.set(rl13);
			wsspcomn.edterror.set(SPACES);
			wsaaFlag.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void premiumCalculation2030()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3590);
		descIO.setDescitem(sv.frequency);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.frequencyErr.set(h048);
			wsspcomn.edterror.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		else {
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.shortds02.set(descIO.getShortdesc());
			}
		}
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(sv.chdrnum);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		initialize(wsaaOldinst);
		initialize(wsaaNewinst);
		wsaaNewinstamt.set(ZERO);
		while ( !(isEQ(covrmjaIO.getStatuz(),"ENDP")
		|| isNE(sv.errorIndicators,SPACES))) {
			getOldnewInstprem2100();
		}
		
		wsaaNewinst.add(wsaaNewinstamt);
		contractFee2200();
		if (isEQ(wsaaWopCompFound,"Y")) {
			for (wsaaWopSub.set(1); !(isGT(wsaaWopSub,5)
			|| isEQ(wsaaCovrmjakey[wsaaWopSub.toInt()],SPACES)); wsaaWopSub.add(1)){
				x300RecalcWop();
			}
		}
		if (isEQ(wsaaFlag,"Y")) {
			goTo(GotoLabel.displayPremium2060);
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void displayPremium2060()
	{
		sv.nextinsamt.set(wsaaNewinst);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(th611);
		wsaaTh611Cnttype.set(chdrenqIO.getCnttype());
		wsaaTh611Currcode.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsaaTh611Item);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaTh611Item,itdmIO.getItemitem())
		|| isNE(chdrenqIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),th611)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
			itdmIO.setItemtabl(th611);
			wsaaTh611Cnttype.set("***");
			wsaaTh611Currcode.set(payrIO.getCntcurr());
			itdmIO.setItemitem(wsaaTh611Item);
			itdmIO.setItmfrm(chdrenqIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),"****")
			&& isNE(itdmIO.getStatuz(),"ENDP")) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isNE(wsaaTh611Item,itdmIO.getItemitem())
			|| isNE(chdrenqIO.getChdrcoy(),itdmIO.getItemcoy())
			|| isNE(itdmIO.getItemtabl(),th611)
			|| isEQ(itdmIO.getStatuz(),"ENDP")) {
				itdmIO.setItemitem(wsaaTh611Item);
				sv.nextinsamtErr.set(rl12);
				wsspcomn.edterror.set(SPACES);
				goTo(GotoLabel.exit2090);
			}
		}
		th611rec.th611Rec.set(itdmIO.getGenarea());
		for (wsaaSub9.set(1); !(isGT(wsaaSub9,8)); wsaaSub9.add(1)){
			if (isEQ(sv.frequency,th611rec.frequency[wsaaSub9.toInt()])) {
				wsaaMinPrem.set(th611rec.cmin[wsaaSub9.toInt()]);
				wsaaMaxPrem.set(th611rec.cmax[wsaaSub9.toInt()]);
			}
		}
		if (isLT(wsaaNewinst,wsaaMinPrem)
		|| isGT(wsaaNewinst,wsaaMaxPrem)) {
			sv.nextinsamtErr.set(rl11);
			wsspcomn.edterror.set(SPACES);
		}
		sv.nextinsamt.set(wsaaNewinst);
		if (isEQ(wsaaFlag,"Y")) {
			sv.errorIndicators.set(SPACES);
			wsaaFlag.set(SPACES);
		}
		if (isEQ(wsaaFlag1,"Y")) {
			sv.errorIndicators.set(SPACES);
			wsaaFlag1.set(SPACES);
		}
		goTo(GotoLabel.exit2090);
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void getOldnewInstprem2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin2100();
				}
				case nextr2100: {
					nextr2100();
				}
				case exit2100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin2100()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2100);
		}
		if (isNE(covrmjaIO.getChdrnum(),sv.chdrnum)
		|| isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2100);
		}
		wsaaValidComp = "N";
		if (isLTE(covrmjaIO.getInstprem(),0)) {
			goTo(GotoLabel.nextr2100);
		}
		if (isEQ(wsaaWopCompFound,"Y")) {
			for (wsaaWopSub.set(1); !(isGT(wsaaWopSub,5)
			|| isEQ(wsaaCovrmjakey[wsaaWopSub.toInt()],SPACES)); wsaaWopSub.add(1)){
				if (isEQ(wsaaCovrmjakey[wsaaWopSub.toInt()],covrmjaIO.getDataKey())) {
					wsaaWopSub.set(6);
					goTo(GotoLabel.nextr2100);
				}
			}
		}
		wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(),"00")) {
			while ( !((isGT(wsaaStatCount,12))
			|| (isEQ(t5679rec.covPremStat[wsaaStatCount.toInt()],covrmjaIO.getPstatcode())))) {
				wsaaStatCount.add(1);
			}
			
			if (isLTE(wsaaStatCount,12)) {
				wsaaStatCount.set(1);
				while ( !((isGT(wsaaStatCount,12))
				|| (isEQ(t5679rec.covRiskStat[wsaaStatCount.toInt()],covrmjaIO.getStatcode())))) {
					wsaaStatCount.add(1);
				}
				
			}
		}
		else {
			while ( !((isGT(wsaaStatCount,12))
			|| (isEQ(t5679rec.ridPremStat[wsaaStatCount.toInt()],covrmjaIO.getPstatcode())))) {
				wsaaStatCount.add(1);
			}
			
			if (isLTE(wsaaStatCount,12)) {
				wsaaStatCount.set(1);
				while ( !((isGT(wsaaStatCount,12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaStatCount.toInt()],covrmjaIO.getStatcode())))) {
					wsaaStatCount.add(1);
				}
				
			}
		}
		if (isGT(wsaaStatCount,12)) {
			goTo(GotoLabel.nextr2100);
		}
		if (isLTE(wsaaStatCount,12)) {
			wsaaValidComp = "Y";
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.chdrnumErr.set(e300);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		m200ReadT5541();
		wsaaNewFreqFound.set(SPACES);
		wsaaCalcNewfreq.set(sv.frequency);
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaNewFreqFound,"Y")); wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaSub.toInt()],wsaaCalcNewfreq)) {
				wsaaNewLfact.set(t5541rec.lfact[wsaaSub.toInt()]);
				wsaaNewFreqFound.set("Y");
			}
		}
		wsaaOldFreqFound.set(SPACES);
		wsaaCalcOldfreq.set(sv.billfreq);
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaOldFreqFound,"Y")); wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaSub.toInt()],wsaaCalcOldfreq)) {
				wsaaOldLfact.set(t5541rec.lfact[wsaaSub.toInt()]);
				wsaaOldFreqFound.set("Y");
				wsaaOldinst.add(covrmjaIO.getInstprem());
			}
		}
		if (isGT(wsaaSub,12)) {
			sv.chdrnumErr.set(t076);
			goTo(GotoLabel.exit2100);
		}
		if (isEQ(wsaaOldFreqFound,"Y")
		&& isEQ(wsaaNewFreqFound,"Y")) {
			compute(wsaaNewinstamt, 5).setRounded(mult((div((mult(wsaaOldinst,wsaaCalcOldfreq)),wsaaCalcNewfreq)),(div(wsaaNewLfact,wsaaOldLfact))));
			if (isEQ(wsaaWopCompFound,"Y")) {
				for (wsaaWopSub.set(1); !(isGT(wsaaWopSub,5)
				|| isEQ(wsaaCovrmjakey[wsaaWopSub.toInt()],SPACES)); wsaaWopSub.add(1)){
					x200AccumForWop();
				}
			}
		}
		if (isGT(wsaaSub,12)) {
			sv.chdrnumErr.set(t076);
			goTo(GotoLabel.exit2100);
		}
	}

protected void nextr2100()
	{
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void contractFee2200()
	{
		try {
			readT56882210();
			readSubroutineTable2210();
		}
		catch (GOTOException e){
		}
	}

protected void readT56882210()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrenqIO.getCnttype());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2290);
		}
		if (isNE(itdmIO.getItemcoy(),chdrenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrenqIO.getCnttype())) {
			goTo(GotoLabel.exit2290);
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isEQ(t5688rec.feemeth,SPACES)) {
			goTo(GotoLabel.exit2290);
		}
	}

protected void readSubroutineTable2210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setItemtabl(t5674);
			itemIO.setItemitem(t5688rec.feemeth);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		if (isEQ(t5674rec.commsubr,SPACES)) {
			goTo(GotoLabel.exit2290);
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrenqIO.getCnttype());
		mgfeelrec.billfreq.set(sv.frequency);
		mgfeelrec.effdate.set(chdrenqIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrenqIO.getCntcurr());
		mgfeelrec.company.set(chdrenqIO.getChdrcoy());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		wsaaNewinst.add(mgfeelrec.mgfee);
	}

protected void m200ReadT5541()
	{
		m200Start();
	}

protected void m200Start()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(t5541);
		itdmIO.setItemitem(t5687rec.xfreqAltBasis);
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(t5687rec.xfreqAltBasis,itdmIO.getItemitem())
		|| isNE(chdrenqIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5541)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			sv.nextinsamtErr.set(t075);
		}
		else {
			t5541rec.t5541Rec.set(itdmIO.getGenarea());
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		wsaaWsspChdrnum.set(sv.chdrnum);
		wsaaWsspChdrcoy.set(chdrenqIO.getChdrcoy());
		wsaaWsspChdrpfx.set(SPACES);
		wssplife.chdrky.set(wsaaWsspChdrky);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void writeLetcRecord5000()
	{
		try {
			readT6634Record5000();
			writeRecord5001();
		}
		catch (GOTOException e){
		}
	}

protected void readT6634Record5000()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(tr384);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(chdrenqIO.getCnttype(), SPACES));
		stringVariable1.append(delimitedExp(wsaaBatckey.batcBatctrcde, SPACES));
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit5009);
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType,SPACES)) {
			goTo(GotoLabel.exit5009);
		}
	}

protected void writeRecord5001()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrenqIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaEffectiveDate);
		letrqstrec.clntcoy.set(chdrenqIO.getCowncoy());
		letrqstrec.clntnum.set(chdrenqIO.getCownnum());
		letrqstrec.otherKeys.set(chdrenqIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

protected void x100AnyWopComponent()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					x110();
				}
				case x120CallCovrmja: {
					x120CallCovrmja();
					x130ReadTr517();
					x180NextCovrmja();
				}
				case x190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void x110()
	{
		for (wsaaWopSub.set(1); !(isGT(wsaaWopSub,5)); wsaaWopSub.add(1)){
			wsaaCovrmjakey[wsaaWopSub.toInt()].set(SPACES);
			wsaaGenarea[wsaaWopSub.toInt()].set(SPACES);
		}
		wsaaWopCompFound = "N";
		wsaaWopSub.set(ZERO);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(sv.chdrnum);
		covrmjaIO.setLife(SPACES);
		covrmjaIO.setCoverage(SPACES);
		covrmjaIO.setRider(SPACES);
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrmjaIO.setFormat(covrmjarec);
	}

protected void x120CallCovrmja()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		|| isNE(covrmjaIO.getChdrnum(),sv.chdrnum)
		|| isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)) {
			goTo(GotoLabel.x190Exit);
		}
	}

protected void x130ReadTr517()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemcoy(),wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(),tr517)
		&& isEQ(itdmIO.getItemitem(),covrmjaIO.getCrtable())) {
			wsaaWopCompFound = "Y";
			wsaaWopSub.add(1);
			wsaaCovrmjakey[wsaaWopSub.toInt()].set(covrmjaIO.getDataKey());
			wsaaGenarea[wsaaWopSub.toInt()].set(itdmIO.getGenarea());
		}
	}

protected void x180NextCovrmja()
	{
		covrmjaIO.setFunction(varcom.nextr);
		goTo(GotoLabel.x120CallCovrmja);
	}

protected void x13cReadTr517()
	{
		try {
			x13c();
		}
		catch (GOTOException e){
		}
	}

protected void x13c()
	{
		wsaaWaiveCont = "N";
		wsaaZrwvflgs.set(tr517rec.zrwvflgs);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(tr517rec.contitem);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),tr517)
		|| isNE(itdmIO.getItemitem(),tr517rec.contitem)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.x13cExit);
		}
		wsaaWaiveCont = "Y";
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		tr517rec.zrwvflgs.set(wsaaZrwvflgs);
	}

protected void x200AccumForWop()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					x210SumWaivePremium();
				}
				case x220Check: {
					x220Check();
				}
				case x290Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void x210SumWaivePremium()
	{
		tr517rec.tr517Rec.set(wsaaGenarea[wsaaWopSub.toInt()]);
		wsaaCovrmjakeyx.set(wsaaCovrmjakey[wsaaWopSub.toInt()]);
		if (isNE(covrmjaIO.getLife(),wsaaCovrmjakeyx.covrmjaLife)
		&& isEQ(tr517rec.zrwvflg02,"N")) {
			goTo(GotoLabel.x290Exit);
		}
		wsaaWaiveIt.set("N");
	}

protected void x220Check()
	{
		for (x.set(1); !(isGT(x,50)
		|| isEQ(wsaaWaiveIt,"Y")); x.add(1)){
			if (isEQ(covrmjaIO.getCrtable(),tr517rec.ctable[x.toInt()])) {
				wsaaWaiveIt.set("Y");
			}
		}
		if (isNE(wsaaWaiveIt,"Y")
		&& isNE(tr517rec.contitem,SPACES)) {
			x13cReadTr517();
			if (isEQ(wsaaWaiveCont,"Y")) {
				goTo(GotoLabel.x220Check);
			}
		}
		if (isEQ(wsaaWaiveIt,"N")) {
			goTo(GotoLabel.x290Exit);
		}
		wsaaWaivePremium[wsaaWopSub.toInt()].add(wsaaNewinstamt);
	}

protected void x300RecalcWop()
	{
		try {
			x310WaiveFees();
			x310ReadWop();
			x320ReadT5687();
			x330ReadT5675();
			x340ReadLifeLife();
			x350ReadLifeJlife();
			x360SetCalcPremRec();
		}
		catch (GOTOException e){
		}
	}

protected void x310WaiveFees()
	{
		tr517rec.tr517Rec.set(wsaaGenarea[wsaaWopSub.toInt()]);
		if (isEQ(tr517rec.zrwvflg03,"Y")) {
			chdrlifIO.setChdrcoy(chdrenqIO.getChdrcoy());
			chdrlifIO.setChdrnum(chdrenqIO.getChdrnum());
			chdrlifIO.setFunction(varcom.readr);
			chdrlifIO.setFormat(chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(chdrlifIO.getStatuz());
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			wsaaWaivePremium[wsaaWopSub.toInt()].add(chdrlifIO.getSinstamt02());
		}
	}

protected void x310ReadWop()
	{
		covrmjaIO.setDataKey(wsaaCovrmjakey[wsaaWopSub.toInt()]);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		wsaaValidComp = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaValidComp,"Y")); wsaaIndex.add(1)){
			if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrmjaIO.getPstatcode())) {
				wsaaValidComp = "Y";
			}
		}
		if (isNE(wsaaValidComp,"Y")) {
			goTo(GotoLabel.x390Exit);
		}
		wsaaValidComp = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaValidComp,"Y")); wsaaIndex.add(1)){
			if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrmjaIO.getStatcode())) {
				wsaaValidComp = "Y";
			}
		}
		if (isNE(wsaaValidComp,"Y")) {
			goTo(GotoLabel.x390Exit);
		}
	}

protected void x320ReadT5687()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isEQ(itemIO.getStatuz(),varcom.endp) 
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())) {
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set(covrmjaIO.getCrtable());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void x330ReadT5675()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5675);
		itemIO.setItemitem(t5687rec.premmeth);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void x340ReadLifeLife()
	{
		lifelnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifelnbIO.setChdrnum(covrmjaIO.getChdrnum());
		lifelnbIO.setLife(covrmjaIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isEQ(lifelnbIO.getStatuz(),varcom.oK)) {
			wsaaAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
			wsaaSex.set(lifelnbIO.getCltsex());
		}
		else {
			wsaaAnbAtCcd.set(ZERO);
			wsaaSex.set(SPACES);
		}
	}

protected void x350ReadLifeJlife()
	{
		lifelnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifelnbIO.setChdrnum(covrmjaIO.getChdrnum());
		lifelnbIO.setLife(covrmjaIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isEQ(lifelnbIO.getStatuz(),varcom.oK)) {
			wsbbAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
			wsbbSex.set(lifelnbIO.getCltsex());
		}
		else {
			wsbbAnbAtCcd.set(ZERO);
			wsbbSex.set(SPACES);
		}
	}

protected void x360SetCalcPremRec()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.lifeJlife.set(covrmjaIO.getJlife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.effectdt.set(chdrenqIO.getOccdate());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.currcode.set(chdrenqIO.getCntcurr());
		premiumrec.cnttype.set(chdrenqIO.getCnttype());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(covrmjaIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(covrmjaIO.getBenCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaBenCessTerm.set(datcon3rec.freqFactor);
		premiumrec.benCessTerm.set(wsaaBenCessTerm);
		premiumrec.sumin.set(wsaaWaivePremium[wsaaWopSub.toInt()]);
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.billfreq.set(chdrenqIO.getBillfreq());
		premiumrec.mop.set(chdrenqIO.getBillchnl());
		premiumrec.ratingdate.set(chdrenqIO.getOccdate());
		premiumrec.reRateDate.set(chdrenqIO.getPtdate());
		premiumrec.calcPrem.set(covrmjaIO.getInstprem());
		premiumrec.language.set(wsspcomn.language);
		callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		wsaaNewinst.add(premiumrec.calcPrem);
	}
}
