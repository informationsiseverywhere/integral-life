package com.csc.life.productdefinition.tablestructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: vdivisala
 * @version
 * Creation Date: 15/02/2017 11:17:10
 * Description:
 * Copybook name: TR5AWREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr5awrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr5awRec = new FixedLengthStringData(6);
  	public FixedLengthStringData cngfrmtaxpyr = DD.keya.copy().isAPartOf(tr5awRec,0);
  	public FixedLengthStringData cngfrmbrthctry = DD.keya.copy().isAPartOf(tr5awRec,1);
  	public FixedLengthStringData cngfrmnatlty = DD.keya.copy().isAPartOf(tr5awRec,2);
  	public FixedLengthStringData cngtotaxpyr = DD.keya.copy().isAPartOf(tr5awRec,3);
  	public FixedLengthStringData cngtobrthctry = DD.keya.copy().isAPartOf(tr5awRec,4);
  	public FixedLengthStringData cngtonatlty = DD.keya.copy().isAPartOf(tr5awRec,5);
	
	public void initialize() {
		COBOLFunctions.initialize(tr5awRec);
	}	
	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr5awRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}