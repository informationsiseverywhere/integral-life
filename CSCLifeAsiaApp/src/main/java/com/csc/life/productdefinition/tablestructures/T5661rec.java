package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:38
 * Description:
 * Copybook name: T5661REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5661rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5661Rec = new FixedLengthStringData(getRecSize());
  	public FixedLengthStringData fuposss = new FixedLengthStringData(10).isAPartOf(t5661Rec, 0);
  	public FixedLengthStringData[] fuposs = FLSArrayPartOfStructure(10, 1, fuposss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(fuposss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData fuposs01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData fuposs02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData fuposs03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData fuposs04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData fuposs05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData fuposs06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData fuposs07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData fuposs08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData fuposs09 = new FixedLengthStringData(1).isAPartOf(filler, 8);
  	public FixedLengthStringData fuposs10 = new FixedLengthStringData(1).isAPartOf(filler, 9);
  	public FixedLengthStringData fupstat = new FixedLengthStringData(1).isAPartOf(t5661Rec, 10);
  	public FixedLengthStringData messages = new FixedLengthStringData(390).isAPartOf(t5661Rec, 11);
  	public FixedLengthStringData[] message = FLSArrayPartOfStructure(5, 78, messages, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(390).isAPartOf(messages, 0, FILLER_REDEFINE);
  	public FixedLengthStringData message01 = new FixedLengthStringData(78).isAPartOf(filler1, 0);
  	public FixedLengthStringData message02 = new FixedLengthStringData(78).isAPartOf(filler1, 78);
  	public FixedLengthStringData message03 = new FixedLengthStringData(78).isAPartOf(filler1, 156);
  	public FixedLengthStringData message04 = new FixedLengthStringData(78).isAPartOf(filler1, 234);
  	public FixedLengthStringData message05 = new FixedLengthStringData(78).isAPartOf(filler1, 312);
  	public FixedLengthStringData zdalind = new FixedLengthStringData(1).isAPartOf(t5661Rec, 401);
  	public FixedLengthStringData zdocind = new FixedLengthStringData(1).isAPartOf(t5661Rec, 402);
  	public ZonedDecimalData zelpdays = new ZonedDecimalData(3, 0).isAPartOf(t5661Rec, 403);
  	public ZonedDecimalData zleadays = new ZonedDecimalData(3, 0).isAPartOf(t5661Rec, 406);
  	public FixedLengthStringData zletstat = new FixedLengthStringData(1).isAPartOf(t5661Rec, 409);
  	public FixedLengthStringData zletstatr = new FixedLengthStringData(1).isAPartOf(t5661Rec, 410);
  	public FixedLengthStringData zlettype = new FixedLengthStringData(8).isAPartOf(t5661Rec, 411);
  	public FixedLengthStringData zlettyper = new FixedLengthStringData(8).isAPartOf(t5661Rec, 419);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(73).isAPartOf(t5661Rec, 427, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5661Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5661Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

public int getRecSize() {
		
		return 500 ;
	}
}