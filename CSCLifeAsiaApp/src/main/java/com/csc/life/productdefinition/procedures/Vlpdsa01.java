/*
 * File: Vlpdsa01.java
 * Date: 30 August 2009 2:53:53
 * Author: Quipoz Limited
 * 
 * Class transformed from VLPDSA01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List; //ILB-1062

import com.csc.fsu.general.dataaccess.dao.ItempfDAO; //ILB-1062
import com.csc.fsu.general.dataaccess.model.Itempf; //ILB-1062
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51arec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil; //ILB-1062
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Subroutine VLPDSA01.
* ====================
*
*   This program will to validate limits where the total of the riders'
*   sum assured cannot exceed X times of the basic sum assured and
*   subjected to a maximum limit as specified in TR51A
*
*   - Read table TR51A, using VLSB-CNTTYPE + VLSB-RID1-CRTABLE  as keys
*   - Get Life Date of birth
*   - Calculate age by using call 'AGECALC'
*   - Validate the limit according to Table TR51A
*
***********************************************************************
* </pre>
*/
public class Vlpdsa01 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VLPDSA01";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaErdSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaVlsbSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr51aSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaInputAllSpace = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaInputCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaTr51aFound = new FixedLengthStringData(1);
	private int wsaaMaxRow = 10;
	private int wsaaMaxCrtable = 20;
	private int wsaaMaxError = 20;
	private int wsaaMaxInput = 50;
	private ZonedDecimalData wsaaInputZsa = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaBasicSa = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaLimitSa = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData wsaaErrDetails = new FixedLengthStringData(80);
	private FixedLengthStringData[] wsaaErrRec = FLSArrayPartOfStructure(20, 4, wsaaErrDetails, 0);
	private FixedLengthStringData[] wsaaErrDetail = FLSDArrayPartOfArrayStructure(4, wsaaErrRec, 0);

	private FixedLengthStringData wsaaTr51aItmkey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr51aCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr51aItmkey, 0).init(SPACES);
	private FixedLengthStringData wsaaTr51aComponent = new FixedLengthStringData(4).isAPartOf(wsaaTr51aItmkey, 3).init(SPACES);

	private FixedLengthStringData wsaaTr51aArray = new FixedLengthStringData(80);
	private FixedLengthStringData[] wsaaTr51aRec = FLSArrayPartOfStructure(20, 4, wsaaTr51aArray, 0);
	private FixedLengthStringData[] wsaaTr51aKey = FLSDArrayPartOfArrayStructure(4, wsaaTr51aRec, 0);
	private FixedLengthStringData[] wsaaTr51aCrtable = FLSDArrayPartOfArrayStructure(4, wsaaTr51aKey, 0, HIVALUES);
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String lifelnbrec = "LIFELNBREC";
		/* TABLES */
	private String tr51a = "TR51A";
	private String t1693 = "T1693";
		/* ERRORS */
	private String rlaz = "RLAZ";
	private String rlbg = "RLBG";
	private String rlbh = "RLBH";
	private String e984 = "E984";
	private IntegerData wsaaTr51aIx = new IntegerData();
	private Agecalcrec agecalcrec = new Agecalcrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private Tr51arec tr51arec = new Tr51arec();
	private Varcom varcom = new Varcom();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();

	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class); //ILB-1062
	private Itempf itempf = null; //ILB-1062
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		nextTr51a380, 
		exit390, 
		exit490, 
		exit590, 
		exit690
	}

	public Vlpdsa01() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		try {
			begin010();
			contrctypComponent020();
			xxxComponent040();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		wsaaTr51aFound.set(SPACES);
		wsaaErrSeq.set(1);
		if (isEQ(vlpdsubrec.rid1Life,SPACES)
		&& isEQ(vlpdsubrec.rid1Jlife,SPACES)
		&& isEQ(vlpdsubrec.rid1Coverage,SPACES)
		&& isEQ(vlpdsubrec.rid1Rider,SPACES)
		&& isEQ(vlpdsubrec.rid1Crtable,SPACES)) {
			goTo(GotoLabel.exit090);
		}
		readTable100();
		readLife200();
		initialize(agecalcrec.agecalcRec);
		agecalcrec.statuz.set(varcom.oK);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(vlpdsubrec.language);
		agecalcrec.cnttype.set(vlpdsubrec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(vlpdsubrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
	}

protected void contrctypComponent020()
	{
		 //ILB-1062 start
		itempf = new Itempf();	
		wsaaTr51aCnttype.set(vlpdsubrec.cnttype);
		wsaaTr51aComponent.set(vlpdsubrec.rid1Crtable);
		itempf.setItemitem(wsaaTr51aItmkey.toString());
		List<Itempf> itempfList = itempfDAO.findItemByTable("IT", vlpdsubrec.chdrcoy.toString(), tr51a, wsaaTr51aItmkey.toString(), "1");
		if(!itempfList.isEmpty()) {
			for (Itempf item : itempfList) {
			
				wsaaTr51aFound.set("Y");
				tr51arec.tr51aRec.set(StringUtil.rawToString(item.getGenarea()));
				for (wsaaTr51aSeq.set(1); !(isGT(wsaaTr51aSeq,wsaaMaxCrtable)); wsaaTr51aSeq.add(1)){
					wsaaTr51aCrtable[wsaaTr51aSeq.toInt()].set(tr51arec.crtable[wsaaTr51aSeq.toInt()]);
				}
				initialize(wsaaErrDetails);
				wsaaInputAllSpace.set(SPACES);
				wsaaErdSeq.set(0);
				wsaaInputZsa.set(0);
				for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq,wsaaMaxInput)
				|| isEQ(wsaaInputAllSpace,"Y")); wsaaVlsbSeq.add(1)){
					sumassured400();
				}
				if (isEQ(wsaaInputZsa,0)) {
					continue;
				}
				wsaaErrCode.set(SPACES);
				for (wsaaTr51aSeq.set(1); !(isGT(wsaaTr51aSeq,wsaaMaxRow)); wsaaTr51aSeq.add(1)){
					if (isLTE(agecalcrec.agerating,tr51arec.age[wsaaTr51aSeq.toInt()])) {
						chkSaLimit500();
						wsaaTr51aSeq.set(wsaaMaxRow);
					}
				}
			}
		}
 		//ILB-1062 end
		if (isEQ(wsaaTr51aFound,"Y")) {
			goTo(GotoLabel.exit090);
		}
	}

protected void xxxComponent040()
	{
		//ILB-1062 start
		itempf = new Itempf();	
		wsaaTr51aCnttype.set("***");
		wsaaTr51aComponent.set(vlpdsubrec.rid1Crtable);
		itempf.setItemitem(wsaaTr51aItmkey.toString());
		List<Itempf> itempfList = itempfDAO.findItemByTable("IT", vlpdsubrec.chdrcoy.toString(), tr51a, wsaaTr51aItmkey.toString(), "1");
		if(!itempfList.isEmpty()) {
			for (Itempf item : itempfList) {
				wsaaTr51aFound.set("Y");
				tr51arec.tr51aRec.set(StringUtil.rawToString(item.getGenarea()));
				for (wsaaTr51aSeq.set(1); !(isGT(wsaaTr51aSeq,wsaaMaxCrtable)); wsaaTr51aSeq.add(1)){
					wsaaTr51aCrtable[wsaaTr51aSeq.toInt()].set(tr51arec.crtable[wsaaTr51aSeq.toInt()]);
				}
				initialize(wsaaErrDetails);
				wsaaInputAllSpace.set(SPACES);
				wsaaErdSeq.set(0);
				wsaaInputZsa.set(0);
				for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq,wsaaMaxInput)
				|| isEQ(wsaaInputAllSpace,"Y")); wsaaVlsbSeq.add(1)){
					sumassured400();
				}
				if (isEQ(wsaaInputZsa,0)) {
					continue;
				}
				wsaaErrCode.set(SPACES);
				for (wsaaTr51aSeq.set(1); !(isGT(wsaaTr51aSeq,wsaaMaxRow)); wsaaTr51aSeq.add(1)){
					if (isLTE(agecalcrec.agerating,tr51arec.age[wsaaTr51aSeq.toInt()])) {
						chkSaLimit500();
						wsaaTr51aSeq.set(wsaaMaxRow);
					}
				}
			}
   		}
 		//ILB-1062 end
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTable100()
	{
		begin110();
	}

protected void begin110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(vlpdsubrec.chdrcoy);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
	}

protected void readLife200()
	{
		begin210();
	}

protected void begin210()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(vlpdsubrec.chdrcoy);
		lifelnbIO.setChdrnum(vlpdsubrec.chdrnum);
		lifelnbIO.setLife(vlpdsubrec.rid1Life);
		lifelnbIO.setJlife(vlpdsubrec.rid1Jlife);
		if (isEQ(vlpdsubrec.rid1Jlife,SPACES)) {
			lifelnbIO.setJlife("00");
		}
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

protected void sumassured400()
	{
		try {
			begin410();
		}
		catch (GOTOException e){
		}
	}

protected void begin410()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()],SPACES)) {
			wsaaInputAllSpace.set("Y");
			goTo(GotoLabel.exit490);
		}
		if (isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()],vlpdsubrec.rid1Crtable)) {
			wsaaBasicSa.set(vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]);
		}
		wsaaInputCrtable.set(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()]);
		wsaaTr51aIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaTr51aIx,wsaaTr51aRec.length); wsaaTr51aIx.add(1)){
				if (isEQ(wsaaTr51aKey[wsaaTr51aIx.toInt()],wsaaInputCrtable)) {
					break searchlabel1;
				}
			}
			goTo(GotoLabel.exit490);
		}
		wsaaErdSeq.add(1);
		wsaaErrDetail[wsaaErdSeq.toInt()].set(wsaaInputCrtable);
		compute(wsaaInputZsa, 2).set(add(wsaaInputZsa,vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]));
	}

protected void chkSaLimit500()
	{
		try {
			begin510();
		}
		catch (GOTOException e){
		}
	}

protected void begin510()
	{
		compute(wsaaLimitSa, 2).set((div(mult(wsaaBasicSa,tr51arec.overdueMina[wsaaTr51aSeq.toInt()]),100)));
		if (isEQ(tr51arec.indc[wsaaTr51aSeq.toInt()],"EQ")){
			if (isNE(wsaaInputZsa,wsaaLimitSa)
			|| isGT(wsaaInputZsa,tr51arec.sumins[wsaaTr51aSeq.toInt()])) {
				wsaaErrCode.set(rlbg);
			}
		}
		else if (isEQ(tr51arec.indc[wsaaTr51aSeq.toInt()],"LE")){
			if (isGT(wsaaInputZsa,wsaaLimitSa)
			|| isGT(wsaaInputZsa,tr51arec.sumins[wsaaTr51aSeq.toInt()])) {
				wsaaErrCode.set(rlaz);
			}
		}
		else if (isEQ(tr51arec.indc[wsaaTr51aSeq.toInt()],"LT")){
			if (isGTE(wsaaInputZsa,wsaaLimitSa)
			|| isGT(wsaaInputZsa,tr51arec.sumins[wsaaTr51aSeq.toInt()])) {
				wsaaErrCode.set(rlbh);
			}
		}
		else{
			wsaaErrCode.set(e984);
		}
		if (isEQ(wsaaErrCode,SPACES)) {
			goTo(GotoLabel.exit590);
		}
		for (wsaaErdSeq.set(1); !(isGT(wsaaErdSeq,wsaaMaxCrtable)
		|| isEQ(wsaaErrDetail[wsaaErdSeq.toInt()],SPACES)); wsaaErdSeq.add(1)){
			vlpdsubrec.errCode[wsaaErrSeq.toInt()].set(wsaaErrCode);
			vlpdsubrec.errDet[wsaaErrSeq.toInt()].set(wsaaErrDetail[wsaaErdSeq.toInt()]);
			wsaaErrSeq.add(1);
		}
	}

protected void fatalError600()
	{
		try {
			fatalErrors610();
		}
		catch (GOTOException e){
		}
		finally{
			exit690();
		}
	}

protected void fatalErrors610()
	{
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit690()
	{
		exitProgram();
	}
}
