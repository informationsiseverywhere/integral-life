package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5h7Screen extends ScreenRecord {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] { 21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { 1, 10, 2, 79 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sd5h7ScreenVars sv = (Sd5h7ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5h7screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
	}

	public static void clearClassString(VarModel pv) {
		Sd5h7ScreenVars screenVars = (Sd5h7ScreenVars) pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.itmto.setClassString("");
		screenVars.intrate.setClassString("");
		screenVars.intcalfreq.setClassString("");
		screenVars.intcapfreq.setClassString("");
	}

	/**
	 * Clear all the variables in S6633screen
	 */
	public static void clear(VarModel pv) {
		Sd5h7ScreenVars screenVars = (Sd5h7ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.intrate.clear();
		screenVars.intcalfreq.clear();
		screenVars.intcapfreq.clear();
	}
}
