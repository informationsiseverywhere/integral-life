package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:51
 * @author Quipoz
 */
public class St579screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St579ScreenVars sv = (St579ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St579screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St579ScreenVars screenVars = (St579ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.crtable01.setClassString("");
		screenVars.crtable02.setClassString("");
		screenVars.crtable03.setClassString("");
		screenVars.crtable04.setClassString("");
		screenVars.crtable05.setClassString("");
		screenVars.crtable06.setClassString("");
		screenVars.crtable07.setClassString("");
		screenVars.crtable08.setClassString("");
		screenVars.crtable09.setClassString("");
		screenVars.crtable10.setClassString("");
		screenVars.crtable11.setClassString("");
		screenVars.crtable12.setClassString("");
		screenVars.crtable13.setClassString("");
		screenVars.crtable14.setClassString("");
		screenVars.crtable15.setClassString("");
		screenVars.amta.setClassString("");
	}

/**
 * Clear all the variables in St579screen
 */
	public static void clear(VarModel pv) {
		St579ScreenVars screenVars = (St579ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.crtable01.clear();
		screenVars.crtable02.clear();
		screenVars.crtable03.clear();
		screenVars.crtable04.clear();
		screenVars.crtable05.clear();
		screenVars.crtable06.clear();
		screenVars.crtable07.clear();
		screenVars.crtable08.clear();
		screenVars.crtable09.clear();
		screenVars.crtable10.clear();
		screenVars.crtable11.clear();
		screenVars.crtable12.clear();
		screenVars.crtable13.clear();
		screenVars.crtable14.clear();
		screenVars.crtable15.clear();
		screenVars.amta.clear();
	}
}
