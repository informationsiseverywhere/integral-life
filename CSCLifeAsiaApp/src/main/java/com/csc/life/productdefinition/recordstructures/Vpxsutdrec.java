package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Fri, 26 Oct 2018 10:28:00
 * Description: VPMS-SA/Bonus(Traditional Product) surrender calculation.                                               
 * Copybook name: Vpxsutdrec
 *
 * Copyright (2012) CSC Asia, all rights reserved
 *
 */
public class Vpxsutdrec extends ExternalData {

	// Total Length
	public FixedLengthStringData vpxsutdrec = new FixedLengthStringData(231);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxsutdrec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxsutdrec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxsutdrec, 9);
  	// Length of result
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxsutdrec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	// Length of data area
  	public FixedLengthStringData dataRec = new FixedLengthStringData(95).isAPartOf(vpxsutdrec, 136);
  	// from covrpf
  	public PackedDecimalData anbccd = new PackedDecimalData(3, 0).isAPartOf(dataRec, 0);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(dataRec, 3);
  	public FixedLengthStringData sex = new FixedLengthStringData(1).isAPartOf(dataRec, 4);
  	public PackedDecimalData instprem = new PackedDecimalData(17, 2).isAPartOf(dataRec, 5);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(dataRec, 22);
  	public ZonedDecimalData crrcd = new ZonedDecimalData(8, 0).isAPartOf(dataRec, 39);
  	public ZonedDecimalData rcsdte = new ZonedDecimalData(8, 0).isAPartOf(dataRec, 47);
  	public ZonedDecimalData pcesdte = new ZonedDecimalData(8, 0).isAPartOf(dataRec, 55);
  	// from acblpf
  	public PackedDecimalData curbal = new PackedDecimalData(17, 2).isAPartOf(dataRec, 63);
  	// from chdrpf
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(dataRec, 80);
  	// from subroutine
  	public FixedLengthStringData svMethod = new FixedLengthStringData(4).isAPartOf(dataRec, 83);
  	public FixedLengthStringData subname = new FixedLengthStringData(8).isAPartOf(dataRec, 87);
  	

	public void initialize() {
		COBOLFunctions.initialize(vpxsutdrec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxsutdrec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}