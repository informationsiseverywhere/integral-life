package com.csc.life.productdefinition.recordstructures;

import java.util.List;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class PmexAcblRec extends ExternalData {
	//VpxLextRec
	private static final long serialVersionUID = 1L;
	
  //*******************************
  //Attribute Declarations
  //*******************************
	
	//ACBL FIELDS
	private List<List<String>> totSingPrem;
	private List<List<String>> totRegPrem;
	
	public List<List<String>> getTotSingPrem() {
		return totSingPrem;
	}

	public void setTotSingPrem(List<List<String>> totSingPrem) {
		this.totSingPrem = totSingPrem;
	}

	public List<List<String>> getTotRegPrem() {
		return totRegPrem;
	}

	public void setTotRegPrem(List<List<String>> totRegPrem) {
		this.totRegPrem = totRegPrem;
	}
		
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
	}


}