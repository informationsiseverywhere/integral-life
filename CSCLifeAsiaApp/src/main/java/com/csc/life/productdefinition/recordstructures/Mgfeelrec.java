package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import java.math.BigDecimal;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:44
 * Description:
 * Copybook name: MGFEELREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mgfeelrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData mgfeelRec = new FixedLengthStringData(90);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(mgfeelRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(mgfeelRec, 5);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(mgfeelRec, 9);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(mgfeelRec, 12);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(mgfeelRec, 14).setUnsigned();
  	public ZonedDecimalData mgfee = new ZonedDecimalData(17, 2).isAPartOf(mgfeelRec, 22);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(mgfeelRec, 39);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(mgfeelRec, 42);
	public ZonedDecimalData bilfrmdt = new ZonedDecimalData(8, 0).isAPartOf(mgfeelRec, 43).setUnsigned();
  	public ZonedDecimalData biltodt = new ZonedDecimalData(8, 0).isAPartOf(mgfeelRec, 51).setUnsigned();
  	public ZonedDecimalData policyRCD = new ZonedDecimalData(8, 0).isAPartOf(mgfeelRec, 59).setUnsigned();

  	public FixedLengthStringData filler = new FixedLengthStringData(23).isAPartOf(mgfeelRec, 67, FILLER);
  	
  	
  
  	

	public void initialize() {
		COBOLFunctions.initialize(mgfeelRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mgfeelRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}