package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.Br50hTempDAO;
import com.csc.life.productdefinition.dataaccess.model.Br50hDTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.database.QPBaseDataSource;
import com.quipoz.framework.util.IntegralDBProperties;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br50hTempDAOImpl extends BaseDAOImpl<Br50hDTO> implements Br50hTempDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(Br50hTempDAOImpl.class);
	
	public int getNTURecords(String company, int batchExtractSize) {
		int rows = 0;
		deleteNTURecords ();
		List<Br50hDTO> br50hList = new ArrayList<Br50hDTO>();
		/*  START OF ILIFE-6222 CAST FUNCTION IS USED IN ORACLE ALONG WITH PIPE FOR CONCAT. QUERY MODIFICATION HAS BEEN DONE FOR ORACLE.  */
		String dbType = IntegralDBProperties.getType().trim();//IJTI-449
		StringBuilder sql = new StringBuilder();
		if(QPBaseDataSource.DATABASE_ORACLE.equals(dbType)){
			sql.append(" INSERT INTO VM1DTA.BR50HDTODATA "); //ILB-475
			sql.append(" (CHDRUNIQUENUMBER, CHDRCOY, CHDRNUM, CNTCURR, STATCODE, PSTCDE, CNTTYPE, COVTFLAG, HPADUNIQUENUMBER, HPRRCVDT, HPROPDTE,HUWDCDTE,BILLFREQ,BTDATE,INCSEQNO,PAYRPAYRSEQNO,CLNTCOY,CLNTNUM,BATCHID, DECLDATE) ")	;
			sql.append(" SELECT  CHDRUNIQUENUMBER,CHDRCOY,CHDRNUM,CNTCURR,STATCODE,PSTCDE,CNTTYPE, COVTFLAG, HPADUNIQUENUMBER,HPRRCVDT,HPROPDTE,HUWDCDTE,BILLFREQ, ");
			sql.append(" BTDATE,INCSEQNO,PAYRPAYRSEQNO, CLNTCOY,CLNTNUM , floor((row_number() over (ORDER BY CHDRUNIQUENUMBER ASC)-1)/?) AS BATCHID, decldate " );
			sql.append(" FROM (SELECT DISTINCT CHDR.UNIQUE_NUMBER AS CHDRUNIQUENUMBER,CHDR.CHDRCOY,CHDR.CHDRNUM,CHDR.CNTCURR,STATCODE,CHDR.PSTCDE,CNTTYPE, ");
			sql.append(" CASE WHEN COVT.UNIQUE_NUMBER IS NOT NULL THEN 'Y' ELSE 'N' END COVTFLAG, ");
			sql.append(" HPAD.UNIQUE_NUMBER AS HPADUNIQUENUMBER,HPRRCVDT,HPROPDTE,HUWDCDTE,PAYR.BILLFREQ,PAYR.BTDATE,PAYR.INCSEQNO,PAYR.PAYRSEQNO AS PAYRPAYRSEQNO,");
			sql.append(" CLRF.CLNTCOY,CLRF.CLNTNUM, HPAD.DECLDATE as DECLDATE ");
			sql.append(" FROM CHDRPF CHDR LEFT JOIN COVTPF COVT");
			sql.append(" ON CHDR.CHDRCOY=COVT.CHDRCOY AND CHDR.CHDRNUM=COVT.CHDRNUM ");
			sql.append(" LEFT JOIN HPADPF HPAD ON CHDR.CHDRCOY=HPAD.CHDRCOY AND CHDR.CHDRNUM=HPAD.CHDRNUM");
			sql.append(" LEFT JOIN PAYRPF PAYR ON CHDR.CHDRCOY=PAYR.CHDRCOY AND CHDR.CHDRNUM=PAYR.CHDRNUM");
			sql.append(" LEFT JOIN CLRRPF CLRF ON PAYR.CHDRCOY=CLRF.FORECOY AND PAYR.CHDRNUM||CAST(PAYR.PAYRSEQNO AS VARCHAR2(30))=CLRF.FORENUM");
			sql.append(" WHERE CHDR.VALIDFLAG='3' AND CHDR.CHDRCOY=? AND CHDR.SERVUNIT='LP' AND (CLRF.CLRRROLE='PY' OR CLRF.CLRRROLE IS NULL) AND (CLRF.FOREPFX='CH' OR CLRF.FOREPFX IS NULL) ");
			sql.append(" AND (PAYR.VALIDFLAG='1' OR PAYR.VALIDFLAG IS NULL)) A  ");
		}
		else {
			sql.append(" INSERT INTO VM1DTA.BR50HDTODATA ");
			sql.append(" (CHDRUNIQUENUMBER, CHDRCOY, CHDRNUM, CNTCURR, STATCODE, PSTCDE, CNTTYPE, COVTFLAG, HPADUNIQUENUMBER, HPRRCVDT, HPROPDTE,HUWDCDTE,BILLFREQ,BTDATE,INCSEQNO,PAYRPAYRSEQNO,CLNTCOY,CLNTNUM,BATCHID, DECLDATE) ")	;
			sql.append(" SELECT  CHDRUNIQUENUMBER,CHDRCOY,CHDRNUM,CNTCURR,STATCODE,PSTCDE,CNTTYPE, COVTFLAG, HPADUNIQUENUMBER,HPRRCVDT,HPROPDTE,HUWDCDTE,BILLFREQ, ");
			sql.append(" BTDATE,INCSEQNO,PAYRPAYRSEQNO, CLNTCOY,CLNTNUM , floor((row_number() over (ORDER BY CHDRUNIQUENUMBER ASC)-1)/?) AS BATCHID, decldate " );
			sql.append(" FROM (SELECT DISTINCT CHDR.UNIQUE_NUMBER AS CHDRUNIQUENUMBER,CHDR.CHDRCOY,CHDR.CHDRNUM,CHDR.CNTCURR,STATCODE,CHDR.PSTCDE,CNTTYPE, ");
			sql.append(" CASE WHEN COVT.UNIQUE_NUMBER IS NOT NULL THEN 'Y' ELSE 'N' END COVTFLAG, ");
			sql.append(" HPAD.UNIQUE_NUMBER AS HPADUNIQUENUMBER,HPRRCVDT,HPROPDTE,HUWDCDTE,PAYR.BILLFREQ,PAYR.BTDATE,PAYR.INCSEQNO,PAYR.PAYRSEQNO AS PAYRPAYRSEQNO,");
			sql.append(" CLRF.CLNTCOY,CLRF.CLNTNUM, HPAD.DECLDATE as DECLDATE ");
			sql.append(" FROM CHDRPF CHDR LEFT JOIN COVTPF COVT");
			sql.append(" ON CHDR.CHDRCOY=COVT.CHDRCOY AND CHDR.CHDRNUM=COVT.CHDRNUM ");
			sql.append(" LEFT JOIN HPADPF HPAD ON CHDR.CHDRCOY=HPAD.CHDRCOY AND CHDR.CHDRNUM=HPAD.CHDRNUM");
			sql.append(" LEFT JOIN PAYRPF PAYR ON CHDR.CHDRCOY=PAYR.CHDRCOY AND CHDR.CHDRNUM=PAYR.CHDRNUM");
			sql.append(" LEFT JOIN CLRRPF CLRF ON PAYR.CHDRCOY=CLRF.FORECOY AND PAYR.CHDRNUM+CONVERT(NVARCHAR,PAYR.PAYRSEQNO)=CLRF.FORENUM");
			sql.append(" WHERE CHDR.VALIDFLAG='3' AND CHDR.CHDRCOY=? AND CHDR.SERVUNIT='LP' AND (CLRF.CLRRROLE='PY' OR CLRF.CLRRROLE IS NULL) AND (CLRF.FOREPFX='CH' OR CLRF.FOREPFX IS NULL) ");
			sql.append(" AND (PAYR.VALIDFLAG='1' OR PAYR.VALIDFLAG IS NULL)) A  ");
		}
		/*  END OF ILIFE-6222  */
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sql.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, company);
			rows = ps.executeUpdate();	
			 
		}
		catch (SQLException e) {
      
            LOGGER.error("getNTURecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return rows;
	}
	
	public int checkstautsfailedByBatch(List<String> list) {//IBPLIFE-1463
		boolean statusCheck = list != null && !list.isEmpty();
		int result = 0;
		if(!statusCheck) {
			return result;
		}
			
		StringBuilder sb = new StringBuilder("SELECT COUNT(CHDRNUM) AS CNT FROM VM1DTA.BR50HDTODATA WHERE STATCODE NOT IN ( ");
		for(String statusCode : list) {
			sb.append("'").append(statusCode).append("',");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(")");
		
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;	
		try {
			ps = getPrepareStatement(sb.toString());	
			rs = ps.executeQuery();
			if(rs.next()) {
				result = rs.getInt("CNT");
			}
		}
		catch (SQLException e) {

            LOGGER.error("getNTURecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return result;
	}
	
	public List<Br50hDTO> loadDataByBatch(int batchID, List<String> list) {
		
			
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM VM1DTA.BR50HDTODATA "); //ILB-475
		sb.append("WHERE BATCHID >= ? AND BATCHID < ? ");
		boolean statusCheck = list != null && !list.isEmpty();
		if(statusCheck) {
			sb.append(" AND STATCODE in (");
			for(String statusCode : list) {
				sb.append("'").append(statusCode).append("',");
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append(")");
		}
		List<Br50hDTO> br50hList = new ArrayList<Br50hDTO>();
		//sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));	
			ps.setString(2, Integer.toString(batchID+10));	
			rs = ps.executeQuery();
			while(rs.next()) {
				Br50hDTO br50h = new Br50hDTO();
				br50h.setChdrUniqueNumber(rs.getLong("CHDRUNIQUENUMBER"));
				br50h.setChdrcoy(rs.getString("CHDRCOY"));
				br50h.setChdrnum(rs.getString("CHDRNUM"));
				br50h.setCntcurr(rs.getString("CNTCURR"));
				br50h.setStatcode(rs.getString("STATCODE"));
				br50h.setPstcde(rs.getString("PSTCDE"));
				br50h.setCnttype(rs.getString("CNTTYPE"));
				br50h.setCovtflag(rs.getString("COVTFLAG"));
				br50h.setHpadUniqueNumber(rs.getLong("HPADUNIQUENUMBER"));
				br50h.setHprrcvdt(rs.getInt("HPRRCVDT"));
				br50h.setHpropdte(rs.getInt("HPROPDTE"));
				br50h.setHuwdcdte(rs.getInt("HUWDCDTE"));
				br50h.setBillfreq(rs.getString("BILLFREQ"));
				br50h.setBtdate(rs.getInt("BTDATE"));
				br50h.setIncseqno(rs.getInt("INCSEQNO"));
				br50h.setPayrPayrseqno(rs.getInt("PAYRPAYRSEQNO"));
				br50h.setClntcoy(rs.getString("CLNTCOY"));
				br50h.setClntnum(rs.getString("CLNTNUM"));
				br50h.setDecldate(rs.getInt("DECLDATE"));
				br50hList.add(br50h);
			}
		}
		catch (SQLException e) {

            LOGGER.error("getNTURecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return br50hList;
		
	}
	
public List<Br50hDTO> loadDataByBatch(int batchID) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM VM1DTA.BR50HDTODATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		List<Br50hDTO> br50hList = new ArrayList<Br50hDTO>();
		//sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();
			while(rs.next()) {
				Br50hDTO br50h = new Br50hDTO();
				br50h.setChdrUniqueNumber(rs.getLong("CHDRUNIQUENUMBER"));
				br50h.setChdrcoy(rs.getString("CHDRCOY"));
				br50h.setChdrnum(rs.getString("CHDRNUM"));
				br50h.setCntcurr(rs.getString("CNTCURR"));
				br50h.setStatcode(rs.getString("STATCODE"));
				br50h.setPstcde(rs.getString("PSTCDE"));
				br50h.setCnttype(rs.getString("CNTTYPE"));
				br50h.setCovtflag(rs.getString("COVTFLAG"));
				br50h.setHpadUniqueNumber(rs.getLong("HPADUNIQUENUMBER"));
				br50h.setHprrcvdt(rs.getInt("HPRRCVDT"));
				br50h.setHpropdte(rs.getInt("HPROPDTE"));
				br50h.setHuwdcdte(rs.getInt("HUWDCDTE"));
				br50h.setBillfreq(rs.getString("BILLFREQ"));
				br50h.setBtdate(rs.getInt("BTDATE"));
				br50h.setIncseqno(rs.getInt("INCSEQNO"));
				br50h.setPayrPayrseqno(rs.getInt("PAYRPAYRSEQNO"));
				br50h.setClntcoy(rs.getString("CLNTCOY"));
				br50h.setClntnum(rs.getString("CLNTNUM"));
				br50h.setDecldate(rs.getInt("DECLDATE"));
				br50hList.add(br50h);
			}
		}
		catch (SQLException e) {

            LOGGER.error("getNTURecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return br50hList;
		
	}
	
	public List<Br50hDTO> getUwlmtchlClrrChdrRecords(String chdrnum, String clntpfx, String clntcoy, String clrrole, String clntnum) {
		List<Br50hDTO> br50hList = new ArrayList<Br50hDTO>();
//ILB-1510 start, modified because substring doesn't work in oracle
		String dbType = IntegralDBProperties.getType().trim();
		Br50hDTO br50hDto; 
		StringBuilder sql = new StringBuilder();
		if(QPBaseDataSource.DATABASE_ORACLE.equals(dbType)) {
		sql.append("SELECT CLNTPFX, CLNTCOY, CLNTNUM, CLRRROLE, FORECOY, FORENUM, CHDRNUM, STATCODE, PSTCDE,CHDR.CNTCURR,CHDR.CNTTYPE,CHDR.CHDRCOY FROM CLRRPF CLRR ");
		sql.append("LEFT JOIN CHDRPF CHDR ON CLRR.FORECOY=CHDR.CHDRCOY AND SUBSTR(CLRR.FORENUM,1,8)=CHDR.CHDRNUM ");
		sql.append("WHERE (SERVUNIT = 'LP' OR SERVUNIT IS NULL) AND CLNTPFX=? AND CLNTCOY=? AND CLRRROLE=? AND VALIDFLAG<>'2' ");
		sql.append("AND (CHDRNUM=? OR CHDRNUM IS NULL) AND CLNTNUM=?");
		sql.append(" ORDER BY FORECOY DESC,CLNTNUM DESC,CLRRROLE DESC,CLRR.UNIQUE_NUMBER DESC");
		}
		else {
			sql.append("SELECT CLNTPFX, CLNTCOY, CLNTNUM, CLRRROLE, FORECOY, FORENUM, CHDRNUM, STATCODE, PSTCDE,CHDR.CNTCURR,CHDR.CNTTYPE,CHDR.CHDRCOY FROM CLRRPF CLRR ");
			sql.append("LEFT JOIN CHDRPF CHDR ON CLRR.FORECOY=CHDR.CHDRCOY AND SUBSTRING(CLRR.FORENUM,1,8)=CHDR.CHDRNUM ");
			sql.append("WHERE (SERVUNIT = 'LP' OR SERVUNIT IS NULL) AND CLNTPFX=? AND CLNTCOY=? AND CLRRROLE=? AND VALIDFLAG<>'2' ");
			sql.append("AND (CHDRNUM=? OR CHDRNUM IS NULL) AND CLNTNUM=?");
			sql.append(" ORDER BY FORECOY DESC,CLNTNUM DESC,CLRRROLE DESC,CLRR.UNIQUE_NUMBER DESC");
				
		}
//ILB-1510 end
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, clntpfx);
			ps.setString(2, clntcoy);
			ps.setString(3, clrrole);
			ps.setString(4, chdrnum);
			ps.setString(5, clntnum);
			rs = ps.executeQuery();
			while(rs.next()) {
				br50hDto = new Br50hDTO();
				br50hDto.setClntpfx(rs.getString("CLNTPFX"));
				br50hDto.setClntcoy(rs.getString("CLNTCOY"));
				br50hDto.setClntnum(rs.getString("CLNTNUM"));
				br50hDto.setClrrrole(rs.getString("CLRRROLE"));
				br50hDto.setForecoy(rs.getString("FORECOY"));
				br50hDto.setForenum(rs.getString("FORENUM"));
				br50hDto.setChdrnum(rs.getString("CHDRNUM"));
				br50hDto.setStatcode(rs.getString("STATCODE"));
				br50hDto.setPstcde(rs.getString("PSTCDE"));
				br50hDto.setCntcurr(rs.getString("CNTCURR"));
				br50hDto.setCnttype(rs.getString("CNTTYPE"));
				br50hDto.setChdrcoy(rs.getString("CHDRCOY"));
				br50hList.add(br50hDto);
			}
		}
		catch (SQLException e) {

            LOGGER.error("getNTURecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return br50hList;
	}
	public List<Br50hDTO> getUwlmtchlCovtCovrRecords(String chdrcoy, String chdrnum, String life) {
		List<Br50hDTO> covrCovtList = new ArrayList();
		Br50hDTO br50hDto;
		StringBuilder sql = new StringBuilder("SELECT CHDRCOY,CHDRNUM,CRTABLE,EFFDATE,SUMINS,ZLINSTPREM,0 AS CRRCD,'' AS STATCODE,'' AS PSTATCODE,'COVTPF' AS RECORD FROM COVTPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? ");//ILB-1114
		sql.append("UNION ");
		sql.append(" SELECT CHDRCOY,CHDRNUM,CRTABLE,0 AS EFFDATE,SUMINS,ZLINSTPREM,CRRCD,STATCODE,PSTATCODE,'COVRPF' AS RECORD FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? ");//ILB-1114
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, chdrcoy);//ILB-1114
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, chdrcoy);
			ps.setString(5, chdrnum);
			ps.setString(6, life);
			rs = ps.executeQuery();
			while(rs.next()) {
					br50hDto = new Br50hDTO();
					br50hDto.setChdrcoy(rs.getString("CHDRCOY"));
					br50hDto.setChdrnum(rs.getString("CHDRNUM"));
					br50hDto.setCrtable(rs.getString("CRTABLE"));
					br50hDto.setEffdate(rs.getInt("EFFDATE"));
					br50hDto.setSumins(rs.getBigDecimal("SUMINS"));
					br50hDto.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
					br50hDto.setCrrcd(rs.getInt("CRRCD"));
					br50hDto.setStatcode(rs.getString("STATCODE"));
					br50hDto.setPstatcode(rs.getString("PSTATCODE"));
					br50hDto.setRecordtype(rs.getString("RECORD"));
					covrCovtList.add(br50hDto);
			}
		}
		catch (SQLException e) {
    
            LOGGER.error("getNTURecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return covrCovtList;
	}
	
	public int deleteNTURecords () {
		
		 
		int rows = 0;
	 	StringBuilder sb = new StringBuilder();
		 
		sb.append(" DELETE FROM VM1DTA.BR50HDTODATA ");  //ILB-475
		 
		 
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;			
		try{
			ps = getPrepareStatement(sb.toString());
			rows = ps.executeUpdate();	

		}catch (SQLException e) {
		
			LOGGER.error("Delete BR50HDTODATA()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
}