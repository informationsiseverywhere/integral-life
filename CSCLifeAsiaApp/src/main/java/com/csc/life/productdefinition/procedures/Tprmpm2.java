/*
 * File: Tprmpm2.java
 * Date: 30 August 2009 2:39:11
 * Author: Quipoz Limited
 * 
 * Class transformed from TPRMPM2.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.Tt503rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  PREMIUM CALCULATION METHOD 08 -
*  HOSPITALIZATION WEEKLY INDEMNITY PREMIUM CALCULATION SUBROUTINE.
*
*
* PROCESSING.
* ----------
*
* Initialise all working storage fields and set keys to read
* tables. Include a table (occurs 8) to hold the Options/Extras
* (LEXT) record details.
* Please note that substandard rate only apply to percentage
* loading, rate adjustment and age adjustment is not apply here.
*
* Build a key.  This key (see below) will read table TT503.
* This table contains the parameters to be used in the
* calculation of premium for Coverage/Rider components (0507).
*
* The key is a concatenation of the following fields:-
*
* Coverage/Rider table code
* Bill Frequency
* Sex
*
* Access the required table by reading the dated table directly (ITDM).
* The contents of the table are then stored.
*
* Validate sum assured amount whether it is in TT503 or not.
*
* CALCULATE-PREMIUM.
*
* To get premium from TT503 by the corresponding benefit amount.
*
* APPLY PERCENTAGE LOADING
*
* - from the LEXT working-storage (W/S) table apply the
* percentage loadings. For each loading entry on the table
* compute the PREMIUM AFTER PERCENTAGE LOADING  as follows:
*
*  PREMIUM AFTER LOADING = TOTAL PREMIUM * loading percentage / 100.
*
*****************************************************************
* </pre>
*/
public class Tprmpm2 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "TPRMPM2";
	private String tl05 = "TL05";
	private String f624 = "F624";
		/* TABLES */
	private String tt503 = "TT503";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String lextrec = "LEXTREC";

	private FixedLengthStringData wsaaLextOppcRecs = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaLextOppcs = FLSArrayPartOfStructure(8, 3, wsaaLextOppcRecs, 0);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);

	private FixedLengthStringData wsaaLextZmortpctRecs = new FixedLengthStringData(16);
	private FixedLengthStringData[] wsaaLextZmortpcts = FLSArrayPartOfStructure(8, 2, wsaaLextZmortpctRecs, 0);
	private PackedDecimalData[] wsaaLextZmortpct = PDArrayPartOfArrayStructure(3, 0, wsaaLextZmortpcts, 0, UNSIGNED_TRUE);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(13, 2);
	private String wsaaBasicPremium = "";
	private String wsaaTableStop = "";
	private FixedLengthStringData wsaaTt503Key = new FixedLengthStringData(7);
	private PackedDecimalData wsaaPrem = new PackedDecimalData(7, 0);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
	private Premiumrec premiumrec = new Premiumrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Tt503rec tt503rec = new Tt503rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		tt503120, 
		readLext131, 
		loopForAdjustedAge132, 
		exit190, 
		exit290, 
		calculateLoadings410, 
		exit490, 
		exit9020
	}

	public Tprmpm2() {
		super();
	}

public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		premiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		initialize100();
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			validateSumins200();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			calculatePremium300();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			percentageLoadings400();
		}
		compute(premiumrec.calcLoaPrem, 2).set(sub(premiumrec.calcPrem,premiumrec.calcBasPrem));
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para100();
				}
				case tt503120: {
					tt503120();
					setupLextKey130();
				}
				case readLext131: {
					readLext131();
				}
				case loopForAdjustedAge132: {
				}
				case exit190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaPrem.set(ZERO);
		premiumrec.calcPrem.set(ZERO);
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			clearLextRecs110();
		}
		goTo(GotoLabel.tt503120);
	}

protected void clearLextRecs110()
	{
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
	}

protected void tt503120()
	{
		wsaaTt503Key.set(SPACES);
		itdmIO.setItemrecKeyData(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(premiumrec.crtable.toString());
		stringVariable1.append(premiumrec.billfreq.toString());
		stringVariable1.append(premiumrec.lsex.toString());
		wsaaTt503Key.setLeft(stringVariable1.toString());
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(tt503);
		itdmIO.setItemitem(wsaaTt503Key);
		if (isEQ(premiumrec.ratingdate,ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaTt503Key,itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),tt503)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			premiumrec.statuz.set(tl05);
			goTo(GotoLabel.exit190);
		}
		else {
			tt503rec.tt503Rec.set(itdmIO.getGenarea());
		}
		wsaaTableStop = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,16)
		|| isEQ(wsaaTableStop,"Y")); wsaaSub.add(1)){
			if (isEQ(tt503rec.bvolume[wsaaSub.toInt()],ZERO)) {
				premiumrec.statuz.set(tl05);
				wsaaTableStop = "Y";
			}
			else {
				if (isEQ(tt503rec.bvolume[wsaaSub.toInt()],premiumrec.sumin)) {
					wsaaPrem.set(tt503rec.insprm[wsaaSub.toInt()]);
					wsaaTableStop = "Y";
				}
			}
		}
		if (isEQ(wsaaTableStop,"N")) {
			premiumrec.statuz.set(tl05);
			goTo(GotoLabel.exit190);
		}
	}

protected void setupLextKey130()
	{
		lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lextIO.setChdrnum(premiumrec.chdrChdrnum);
		lextIO.setLife(premiumrec.lifeLife);
		lextIO.setCoverage(premiumrec.covrCoverage);
		lextIO.setRider(premiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		wsaaSub.set(0);
	}

protected void readLext131()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError9000();
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		if (isEQ(lextIO.getChdrcoy(),premiumrec.chdrChdrcoy)
		&& isEQ(lextIO.getChdrnum(),premiumrec.chdrChdrnum)
		&& isEQ(lextIO.getLife(),premiumrec.lifeLife)
		&& isEQ(lextIO.getCoverage(),premiumrec.covrCoverage)
		&& isEQ(lextIO.getRider(),premiumrec.covrRider)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		if (isEQ(premiumrec.reasind,"2")
		&& isEQ(lextIO.getReasind(),"1")) {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		if (isNE(premiumrec.reasind,"2")
		&& isEQ(lextIO.getReasind(),"2")) {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		lextIO.setFunction(varcom.nextr);
		if (isLTE(lextIO.getExtCessDate(),premiumrec.reRateDate)) {
			goTo(GotoLabel.readLext131);
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub,8)) {
			wsaaLextOppc[wsaaSub.toInt()].set(lextIO.getOppc());
		}
		wsaaAgerateTot.add(lextIO.getAgerate());
		goTo(GotoLabel.readLext131);
	}

protected void validateSumins200()
	{
		try {
			validateBasic210();
		}
		catch (GOTOException e){
		}
	}

protected void validateBasic210()
	{
		if (isLTE(wsaaPrem,0)) {
			premiumrec.statuz.set(f624);
			goTo(GotoLabel.exit290);
		}
	}

protected void calculatePremium300()
	{
		/*CALCULATE-PREMIUM*/
		premiumrec.calcPrem.set(wsaaPrem);
		premiumrec.calcBasPrem.set(premiumrec.calcPrem);
		/*EXIT*/
	}

protected void percentageLoadings400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para400();
				}
				case calculateLoadings410: {
					calculateLoadings410();
				}
				case exit490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para400()
	{
		wsaaSub.set(0);
	}

protected void calculateLoadings410()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,8)) {
			goTo(GotoLabel.exit490);
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()],0)) {
			compute(premiumrec.calcPrem, 2).set((div((mult(premiumrec.calcPrem,wsaaLextOppc[wsaaSub.toInt()])),100)));
		}
		goTo(GotoLabel.calculateLoadings410);
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
