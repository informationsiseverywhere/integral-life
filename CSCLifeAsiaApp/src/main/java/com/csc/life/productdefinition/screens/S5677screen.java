package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5677screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5677ScreenVars sv = (S5677ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5677screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5677ScreenVars screenVars = (S5677ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.language.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.fupcdes01.setClassString("");
		screenVars.fupcdes02.setClassString("");
		screenVars.fupcdes03.setClassString("");
		screenVars.fupcdes04.setClassString("");
		screenVars.fupcdes05.setClassString("");
		screenVars.fupcdes06.setClassString("");
		screenVars.fupcdes07.setClassString("");
		screenVars.fupcdes08.setClassString("");
		screenVars.fupcdes09.setClassString("");
		screenVars.fupcdes10.setClassString("");
		screenVars.fupcdes11.setClassString("");
		screenVars.fupcdes12.setClassString("");
		screenVars.fupcdes13.setClassString("");
		screenVars.fupcdes14.setClassString("");
		screenVars.fupcdes15.setClassString("");
		screenVars.fupcdes16.setClassString("");
	}

/**
 * Clear all the variables in S5677screen
 */
	public static void clear(VarModel pv) {
		S5677ScreenVars screenVars = (S5677ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.language.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.fupcdes01.clear();
		screenVars.fupcdes02.clear();
		screenVars.fupcdes03.clear();
		screenVars.fupcdes04.clear();
		screenVars.fupcdes05.clear();
		screenVars.fupcdes06.clear();
		screenVars.fupcdes07.clear();
		screenVars.fupcdes08.clear();
		screenVars.fupcdes09.clear();
		screenVars.fupcdes10.clear();
		screenVars.fupcdes11.clear();
		screenVars.fupcdes12.clear();
		screenVars.fupcdes13.clear();
		screenVars.fupcdes14.clear();
		screenVars.fupcdes15.clear();
		screenVars.fupcdes16.clear();
	}
}
