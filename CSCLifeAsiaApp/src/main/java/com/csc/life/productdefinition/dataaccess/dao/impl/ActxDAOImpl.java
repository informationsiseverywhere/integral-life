package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.ActxDAO;
import com.csc.life.productdefinition.dataaccess.model.Br523DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ActxDAOImpl extends BaseDAOImpl<Br523DTO> implements ActxDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActxDAOImpl.class);

      public List<Br523DTO> searchActxResult(String tableId, String memName, int batchExtractSize, int batchID){
                  
        StringBuilder sqlActxSelect = new StringBuilder();
        sqlActxSelect.append(" SELECT * FROM (");
        sqlActxSelect.append("  SELECT A.CHDRPFX ACHDRPFX, A.CHDRCOY ACHDRCOY, A.CHDRNUM ACHDRNUM, A.CNTTYPE ACNTTYPE, A.CURRFROM ACURRFROM, A.VALIDFLAG AVALIDFLAG, A.STATCODE ASTATCODE, A.PSTATCODE APSTATCODE, A.OCCDATE AOCCDATE, ");
        sqlActxSelect.append("         C.CHDRPFX, C.CHDRCOY, C.CHDRNUM, C.CURRFROM, C.VALIDFLAG, C.BILLFREQ, C.SINSTAMT06, C.SINSTAMT01, C.CNTTYPE, C.COWNCOY, C.COWNNUM, C.PAYRNUM, C.AGNTCOY, C.AGNTNUM, C.BTDATE, C.PTDATE, C.CNTCURR, ");
        sqlActxSelect.append("         H.HOISSDTE, H.HISSDTE, CLO.UNIQUE_NUMBER CLOUNIQUE_NUMBER, CLO.CLTTYPE CLOCLTTYPE, CLO.GIVNAME CLOGIVNAME, CLO.SURNAME CLOSURNAME, CLO.LSURNAME CLOLSURNAME, CLO.LGIVNAME CLOLGIVNAME, ");
        sqlActxSelect.append("         CLP.UNIQUE_NUMBER CLPUNIQUE_NUMBER, CLP.CLTTYPE CLPCLTTYPE, CLP.GIVNAME CLPGIVNAME, CLP.SURNAME CLPSURNAME, CLP.LSURNAME CLPLSURNAME, CLP.LGIVNAME CLPLGIVNAME, CLR.UNIQUE_NUMBER CLRUNIQUE_NUMBER, CLR.CLTTYPE CLRCLTTYPE, CLR.GIVNAME CLRGIVNAME, CLR.SURNAME CLRSURNAME, CLR.LSURNAME CLRLSURNAME, CLR.LGIVNAME CLRLGIVNAME, AGT.AGNTNUM AGTAGNTNUM, AGT.AGTYPE AGTAGTYPE, C.POLSUM POLSUM ");
        sqlActxSelect.append("         ,PYR.BTDATE PYRBTDATE,PYR.PTDATE PYRPTDATE");
        sqlActxSelect.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY A.UNIQUE_NUMBER ASC, C.CHDRPFX ASC, C.CHDRCOY ASC, C.CHDRNUM ASC, C.VALIDFLAG ASC, C.CURRFROM DESC, C.UNIQUE_NUMBER DESC, H.CHDRCOY ASC, H.CHDRNUM ASC, H.UNIQUE_NUMBER DESC, CLO.CLNTPFX ASC, CLO.CLNTCOY ASC, CLO.CLNTNUM ASC, CLO.UNIQUE_NUMBER DESC, CLP.CLNTPFX ASC, CLP.CLNTCOY ASC, CLP.CLNTNUM ASC, CLP.UNIQUE_NUMBER DESC, CLRR.FOREPFX ASC, CLRR.FORECOY ASC, CLRR.FORENUM ASC, CLRR.CLRRROLE ASC, CLRR.UNIQUE_NUMBER ASC, CLR.CLNTPFX ASC, CLR.CLNTCOY ASC, CLR.CLNTNUM ASC, CLR.UNIQUE_NUMBER DESC, AGT.AGNTPFX ASC, AGT.AGNTCOY ASC, AGT.AGNTNUM ASC, AGT.UNIQUE_NUMBER DESC, PYR.CHDRCOY ASC, PYR.CHDRNUM ASC, PYR.PAYRSEQNO ASC, PYR.UNIQUE_NUMBER DESC )-1)/?) BATCHNUM ");
        sqlActxSelect.append("  FROM ");
        sqlActxSelect.append(tableId);
        sqlActxSelect.append(" A ");
        sqlActxSelect.append("   INNER JOIN CHDRPF C ON A.CHDRPFX = C.CHDRPFX AND A.CHDRCOY = C.CHDRCOY AND A.CHDRNUM = C.CHDRNUM AND A.CURRFROM = C.CURRFROM AND A.VALIDFLAG = C.VALIDFLAG  ");
        sqlActxSelect.append("   LEFT JOIN HPADPF H ON A.CHDRNUM = H.CHDRNUM AND A.CHDRCOY = H.CHDRCOY  ");
        sqlActxSelect.append("   LEFT JOIN CLNTPF CLO ON CLO.CLNTPFX = 'CN' AND CLO.CLNTCOY = C.COWNCOY AND CLO.CLNTNUM = C.COWNNUM ");
        sqlActxSelect.append("   LEFT JOIN CLNTPF CLP ON CLP.CLNTPFX = 'CN' AND CLP.CLNTCOY = C.COWNCOY AND CLP.CLNTNUM = C.PAYRNUM ");
        sqlActxSelect.append("   LEFT JOIN CLRRPF CLRR ON CLRR.FORENUM = concat(A.CHDRNUM, '1') AND CLRR.FOREPFX = 'CH' AND CLRR.FORECOY = C.CHDRCOY AND CLRR.CLRRROLE = 'PY' ");
        sqlActxSelect.append("   LEFT JOIN CLNTPF CLR ON CLR.CLNTPFX = 'CN' AND CLR.CLNTCOY = C.COWNCOY AND CLR.CLNTNUM = CLRR.CLNTNUM ");
        sqlActxSelect.append("   LEFT JOIN AGNTPF AGT ON AGT.AGNTPFX = 'AG' AND AGT.AGNTCOY = C.AGNTCOY AND AGT.AGNTNUM = C.AGNTNUM  ");
        sqlActxSelect.append("   LEFT JOIN PAYRPF PYR ON PYR.VALIDFLAG = '1' AND PYR.CHDRCOY = A.CHDRCOY AND PYR.CHDRNUM = A.CHDRNUM AND PYR.EFFDATE = A.CURRFROM ");
        sqlActxSelect.append("   WHERE A.MEMBER_NAME = ? ");
        sqlActxSelect.append(" ) MAIN WHERE batchnum = ? ");

        PreparedStatement psActxSelect = getPrepareStatement(sqlActxSelect.toString());
        ResultSet sqlactxpf1rs = null;
        List<Br523DTO> actxSearchResultDtoList = new LinkedList<Br523DTO>();
        try {
            psActxSelect.setInt(1, batchExtractSize);
            psActxSelect.setString(2, memName);
            psActxSelect.setInt(3, batchID);
            
            sqlactxpf1rs = executeQuery(psActxSelect);

            while (sqlactxpf1rs.next()) {
                Br523DTO actxSearchResultDto = new Br523DTO();
                actxSearchResultDto.setActxChdrpfx(sqlactxpf1rs.getString(1));
                actxSearchResultDto.setActxChdrcoy(sqlactxpf1rs.getString(2));
                actxSearchResultDto.setActxChdrnum(sqlactxpf1rs.getString(3));
                actxSearchResultDto.setActxCnttype(sqlactxpf1rs.getString(4));
                actxSearchResultDto.setActxCurrfrom(sqlactxpf1rs.getInt(5));
                actxSearchResultDto.setActxValidflag(sqlactxpf1rs.getString(6));
                actxSearchResultDto.setActxStatcode(sqlactxpf1rs.getString(7));
                actxSearchResultDto.setActxPstatcode(sqlactxpf1rs.getString(8));
                actxSearchResultDto.setActxOccdate(sqlactxpf1rs.getInt(9));
                actxSearchResultDto.setChdrChdrpfx(sqlactxpf1rs.getString(10));
                actxSearchResultDto.setChdrChdrcoy(sqlactxpf1rs.getString(11));
                actxSearchResultDto.setChdrChdrnum(sqlactxpf1rs.getString(12));
                actxSearchResultDto.setChdrCurrfrom(sqlactxpf1rs.getInt(13));
                actxSearchResultDto.setChdrValidflag(sqlactxpf1rs.getString(14));
                actxSearchResultDto.setChdrBillfreq(sqlactxpf1rs.getString(15));
                actxSearchResultDto.setChdrSinstamt06(sqlactxpf1rs.getBigDecimal(16));
                actxSearchResultDto.setChdrSinstamt01(sqlactxpf1rs.getBigDecimal(17));
                actxSearchResultDto.setChdrCnttype(sqlactxpf1rs.getString(18));
                actxSearchResultDto.setChdrCowncoy(sqlactxpf1rs.getString(19));
                actxSearchResultDto.setChdrCownnum(sqlactxpf1rs.getString(20));
                actxSearchResultDto.setChdrPayrnum(sqlactxpf1rs.getString(21));
                actxSearchResultDto.setChdrAgntcoy(sqlactxpf1rs.getString(22));
                actxSearchResultDto.setChdrAgntnum(sqlactxpf1rs.getString(23));
                actxSearchResultDto.setChdrBtdate(sqlactxpf1rs.getInt(24));
                actxSearchResultDto.setChdrPtdate(sqlactxpf1rs.getInt(25));
                actxSearchResultDto.setChdrCntcurr(sqlactxpf1rs.getString(26));
                actxSearchResultDto.setHpadHoissdte(sqlactxpf1rs.getInt(27));
                actxSearchResultDto.setHpadHissdte(sqlactxpf1rs.getInt(28));
                actxSearchResultDto.setClntowUniqueNumber(sqlactxpf1rs.getString(29));
                actxSearchResultDto.setClntowClttype(sqlactxpf1rs.getString(30));
                actxSearchResultDto.setClntowGivname(sqlactxpf1rs.getString(31));
                actxSearchResultDto.setClntowSurname(sqlactxpf1rs.getString(32));
                actxSearchResultDto.setClntowLsurname(sqlactxpf1rs.getString(33));
                actxSearchResultDto.setClntowLgivname(sqlactxpf1rs.getString(34));
                actxSearchResultDto.setClntpayUniqueNumber(sqlactxpf1rs.getString(35));
                actxSearchResultDto.setClntpayClttype(sqlactxpf1rs.getString(36));
                actxSearchResultDto.setClntpayGivname(sqlactxpf1rs.getString(37));
                actxSearchResultDto.setClntpaySurname(sqlactxpf1rs.getString(38));
                actxSearchResultDto.setClntpayLsurname(sqlactxpf1rs.getString(39));
                actxSearchResultDto.setClntpayLgivname(sqlactxpf1rs.getString(40));
                actxSearchResultDto.setClntclrUniqueNumber(sqlactxpf1rs.getString(41));
                actxSearchResultDto.setClntclrClttype(sqlactxpf1rs.getString(42));
                actxSearchResultDto.setClntclrGivname(sqlactxpf1rs.getString(43));
                actxSearchResultDto.setClntclrSurname(sqlactxpf1rs.getString(44));
                actxSearchResultDto.setClntclrLsurname(sqlactxpf1rs.getString(45));
                actxSearchResultDto.setClntclrLgivname(sqlactxpf1rs.getString(46));
                actxSearchResultDto.setAgtAgntnum(sqlactxpf1rs.getString(47));
                actxSearchResultDto.setAgtAgtype(sqlactxpf1rs.getString(48));
                actxSearchResultDto.setChdrPolsum(sqlactxpf1rs.getInt(49));
                actxSearchResultDto.setPyrBtdate(sqlactxpf1rs.getInt(50));
                actxSearchResultDto.setPyrPtdate(sqlactxpf1rs.getInt(51));
                actxSearchResultDtoList.add(actxSearchResultDto);
            }

        } catch (SQLException e) {
            LOGGER.error("searchActxResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psActxSelect, sqlactxpf1rs);
        }
        return actxSearchResultDtoList;
    }

}