/*
 * File: Br555.java
 * Date: 29 August 2009 22:20:08
 * Author: Quipoz Limited
 * 
 * Class transformed from BR555.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.life.productdefinition.dataaccess.MliaafiTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This is a uploading program from Life/400 to LIA.
*
*  It will Create a CL Program to create LIAPF before Download.
*  - Year End Upload.
*
*  This is a special upload at the end of the Year to synchronise
*  Sub-Standard Risk Information avaiable to all insurance Companies.
*
*  This is achieved using the same upload function because
*  the flat file layout is the same. Duplicates are ignored.
*
*****************************************************************
* </pre>
*/
public class Br555 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private DiskFileDAM liaFile = new DiskFileDAM("LIAMPF");
	private FixedLengthStringData liaRecord = new FixedLengthStringData(148);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR555");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaMlhsperd = new ZonedDecimalData(3, 0).setPattern("ZZZ");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaExist = "";
	private FixedLengthStringData wsaaLen = new FixedLengthStringData(1);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData liaDetail = new FixedLengthStringData(144);
	private FixedLengthStringData liaAction = new FixedLengthStringData(1).isAPartOf(liaDetail, 0);
	private FixedLengthStringData liaRskflg = new FixedLengthStringData(1).isAPartOf(liaDetail, 1);
	private FixedLengthStringData liaDtapp = new FixedLengthStringData(10).isAPartOf(liaDetail, 2);
	private FixedLengthStringData liaMlcoycde = new FixedLengthStringData(2).isAPartOf(liaDetail, 12);
	private FixedLengthStringData liaNic = new FixedLengthStringData(14).isAPartOf(liaDetail, 14);
	private FixedLengthStringData liaOic = new FixedLengthStringData(14).isAPartOf(liaDetail, 28);
	private FixedLengthStringData liaOth = new FixedLengthStringData(14).isAPartOf(liaDetail, 42);
	private FixedLengthStringData liaEntity = new FixedLengthStringData(15).isAPartOf(liaDetail, 56);
	private FixedLengthStringData liaDob = new FixedLengthStringData(10).isAPartOf(liaDetail, 71);
	private FixedLengthStringData liaMlhsperd = new FixedLengthStringData(3).isAPartOf(liaDetail, 81);
	private FixedLengthStringData liaMlmedcde01 = new FixedLengthStringData(3).isAPartOf(liaDetail, 84);
	private FixedLengthStringData liaMlmedcde02 = new FixedLengthStringData(3).isAPartOf(liaDetail, 87);
	private FixedLengthStringData liaMlmedcde03 = new FixedLengthStringData(3).isAPartOf(liaDetail, 90);
	private FixedLengthStringData liaName = new FixedLengthStringData(51).isAPartOf(liaDetail, 93);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Substd & Hosp view by IC no. and Contrac*/
	private MliaTableDAM mliaIO = new MliaTableDAM();
		/*Substd & Hosp by IC/Contract No./Action*/
	private MliaafiTableDAM mliaafiIO = new MliaafiTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2590
	}

	public Br555() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		liaFile.openOutput();
		initialize(liaDetail);
		wsspEdterror.set(varcom.oK);
		mliaIO.setSecurityno(SPACES);
		mliaIO.setMlentity(SPACES);
		mliaIO.setFunction(varcom.begn);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),varcom.oK)
		&& isNE(mliaIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		if (isEQ(mliaIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			mliaIO.setFunction(varcom.nextr);
		}
		if (isEQ(wsaaEof,"Y")) {
			wsspEdterror.set(varcom.endp);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		try {
			edit2510();
		}
		catch (GOTOException e){
		}
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		if (isEQ(mliaIO.getMind(),"Y")) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		liaAction.set(mliaIO.getActn());
		liaRskflg.set(mliaIO.getRskflg());
		if (isEQ(mliaIO.getHpropdte(),varcom.vrcmMaxDate)) {
			liaDtapp.set(SPACES);
		}
		else {
			datcon1rec.function.set(varcom.conv);
			datcon1rec.intDate.set(mliaIO.getHpropdte());
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			liaDtapp.set(datcon1rec.extDate);
		}
		liaMlcoycde.set(mliaIO.getMlcoycde());
		liaNic.set(mliaIO.getSecurityno());
		liaOic.set(mliaIO.getMloldic());
		liaOth.set(mliaIO.getMlothic());
		wsaaExist = "N";
		wsaaLen.set(SPACES);
		for (wsaaIndex.set(15); !(isEQ(wsaaIndex,0)
		|| isEQ(wsaaExist,"Y")); wsaaIndex.add(-1)){
			wsaaLen.set(subString(mliaIO.getSecurityno(), wsaaIndex, 1));
			if (isNE(wsaaLen,SPACES)) {
				wsaaExist = "Y";
			}
		}
		if (isGT(wsaaIndex,10)) {
			liaNic.set(mliaIO.getSecurityno());
		}
		else {
			liaOic.set(mliaIO.getSecurityno());
			liaNic.set(SPACES);
		}
		liaEntity.set(mliaIO.getMlentity());
		if (isEQ(mliaIO.getDob(),varcom.vrcmMaxDate)) {
			liaDob.set(SPACES);
		}
		else {
			datcon1rec.function.set(varcom.conv);
			datcon1rec.intDate.set(mliaIO.getDob());
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			liaDob.set(datcon1rec.extDate);
		}
		wsaaMlhsperd.set(mliaIO.getMlhsperd());
		liaMlhsperd.set(wsaaMlhsperd);
		liaMlmedcde01.set(mliaIO.getMlmedcde01());
		liaMlmedcde02.set(mliaIO.getMlmedcde02());
		liaMlmedcde03.set(mliaIO.getMlmedcde03());
		liaName.set(mliaIO.getClntnaml());
		liaFile.write(liaDetail);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		mliaafiIO.setSecurityno(mliaIO.getSecurityno());
		mliaafiIO.setMlentity(mliaIO.getMlentity());
		mliaafiIO.setActn(mliaIO.getActn());
		mliaafiIO.setEffdate(mliaIO.getEffdate());
		mliaafiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mliaafiIO);
		if (isNE(mliaafiIO.getStatuz(),varcom.oK)
		&& isNE(mliaafiIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(mliaafiIO.getStatuz());
			syserrrec.params.set(mliaafiIO.getParams());
			fatalError600();
		}
		mliaafiIO.setMind("Y");
		mliaafiIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, mliaafiIO);
		if (isNE(mliaafiIO.getStatuz(),varcom.oK)
		&& isNE(mliaafiIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(mliaafiIO.getStatuz());
			syserrrec.params.set(mliaafiIO.getParams());
			fatalError600();
		}
		initialize(liaDetail);
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		liaFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
