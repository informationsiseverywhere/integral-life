/*
 * File: Br523.java
 * Date: 29 August 2009 22:13:51
 * Author: Quipoz Limited
 * 
 * Class transformed from BR523.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.procedures.Totloan;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.interestbearing.recordstructures.Ibincalrec;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.productdefinition.dataaccess.dao.ActxDAO;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ChevpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CoevpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitdpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LfevpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.PtevpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Br523DTO;
import com.csc.life.productdefinition.dataaccess.model.Chevpf;
import com.csc.life.productdefinition.dataaccess.model.Coevpf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Hitdpf;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lfevpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Ptevpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxibin;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Pr534par;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxibinrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;

import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*   This program will read ACTX as primary file and will populate
*   the CHEV, LFEV and COEV files.
*   Some of the information at contract level will be taken directly
*   from the primary file as most of the field in this files are
*   taken from CHDRPF. Other information sich as the surrender
*   value will require looping through the COVRs.
*
*   Extraction files will be populated according to the
*   parameters specified before submitting the Shedule.
*
*   If a dates range is  specified, all the CHDR records with
*   CURRFROM falling in the range will be extracted.
*   Potentially we could be extracting more CHDR for the same
*   contract number  (validflag 2 as well).
*
*   Every time a CHDR is extracted and written to CHEV, all the
*   relevant COVR records will be extracted and written to COEV.
*   As we may have more CHDR for the same policy number, we could
*   end up in situation where we have lots of duplicate records.
*
*   The general rule, also applicable to other secondary
*   files, is that only the records with a CURRFROM (or EFFDATE)
*   less or equal to the CURRFROM on the relevant CHDR record
*   will be extracted together.
*
*   For example a date range 01/01/1999 - 01/01/2000 is
*   specified.
*
*
*   CHDR holds:
*
*   Policy     Valid
*   Number     Flag     Currfrom  Currto
*
*   00001001    2       19980101 19990101
*   00001001    2       19990101 19990501
*   00001001    2       19990501 19990801
*   00001001    2       19990501 20000101
*   00001001    1       20000101 99999999
*
*   COVR Holds:
*
*   Policy     Valid
*   Number     Flag     Currfrom  Currto
*
*   00001001    2       19980101  19990101
*   00001001    2       19990101  19990501
*   00001001    2       19990501  19990801
*   00001001    2       19990801  20000101
*   00001001    1       20000101  99999999
*
*
*   CHEV and COEV will be populated as follow
*
*   CHEV:                         COEV:
*
*
*   00001001 2 19990101 19990501  00001001 2 19980101 19990501
*                                 00001001 2 19990101 19990801
*   ----------------------------------------------------------
*   00001001 2 19990501 19990801  00001001 2 19980101 19990101
*                                 00001001 2 19990101 19990501
*                                 00001001 2 19990501 19990801
*   ---------------------------------------------------------
*   00001001 2 19990501 20000101  00001001 2 19980101 19990101
*                                 00001001 2 19990101 19990501
*                                 00001001 2 19990501 19990801
*                                 00001001 2 19990801 20000101
*   ----------------------------------------------------------
*
*
*
*
*****************************************************************
* </pre>
*/
public class Br523 extends Mainb {

    public static final String ROUTINE = QPUtilities.getThisClass();
    public int numberOfParameters = 0;
    private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR523");
    private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
    private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
    private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
        /* ERRORS */
    private static final String f294 = "F294";
    private static final String h791 = "H791";
    private static final String g094 = "G094";

    private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
    private Validator endOfFile = new Validator(wsaaEof, "Y");

        /* WSAA-TH510-ARRAY */
    // changed by yy for ILIFE-2483, from 100 to 99 as the first index is 0,not 1.
	//private static final int wsaaTh510Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaTh510Size = 1000;

    private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
    private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
    private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

    private FixedLengthStringData wsaaActxFn = new FixedLengthStringData(10);
    private FixedLengthStringData filler3 = new FixedLengthStringData(4).isAPartOf(wsaaActxFn, 0, FILLER).init("ACTX");
    private FixedLengthStringData wsaaActxRunid = new FixedLengthStringData(2).isAPartOf(wsaaActxFn, 4);
    private ZonedDecimalData wsaaActxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaActxFn, 6).setUnsigned();
    
    private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
    private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
    private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

    private Datcon1rec datcon1rec = new Datcon1rec();
    private Ibincalrec ibincalrec = new Ibincalrec();
    private T5679rec t5679rec = new T5679rec();
    private Th510rec th510rec = new Th510rec();
    private Totloanrec totloanrec = new Totloanrec();
    private Srcalcpy srcalcpy = new Srcalcpy();
    private Agecalcrec agecalcrec = new Agecalcrec();
    private Pr534par pr534par = new Pr534par();
    private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
    private Wsspcomn wsspcomn = new Wsspcomn();
    private T5687rec t5687rec = new T5687rec();
    private T6598rec t6598rec = new T6598rec();
    private T5515rec t5515rec = new T5515rec();
    private Zrdecplrec zrdecplrec = new Zrdecplrec();
    private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
    private ExternalisedRules er = new ExternalisedRules();
    
    private List<Itempf> t5679List = null;
    
    private Map<String, List<Itempf>> th510Map = null;
    private Map<String, List<Itempf>> t5687Map = null;
    private Map<String, List<Itempf>> t5515Map = null;
    private Map<String, List<Itempf>> t6598Map = null;
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
    private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private VprcpfDAO vprcpfDAO = getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class);
    private HitdpfDAO hitdpfDAO = getApplicationContext().getBean("hitdpfDAO", HitdpfDAO.class);
    private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
    private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
    private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
    private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
    private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
    private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private CoevpfDAO coevpfDAO = getApplicationContext().getBean("coevpfDAO", CoevpfDAO.class);
    private ChevpfDAO chevpfDAO = getApplicationContext().getBean("chevpfDAO", ChevpfDAO.class);
    private PtevpfDAO ptevpfDAO = getApplicationContext().getBean("ptevpfDAO", PtevpfDAO.class);
    private LfevpfDAO lfevpfDAO = getApplicationContext().getBean("lfevpfDAO", LfevpfDAO.class);
    private ActxDAO actxDAO = getApplicationContext().getBean("actxDAO", ActxDAO.class);
    private List<String> wsaaTh510Fund = new ArrayList<String>();
    private List<BigDecimal> wsaaTh510Itmfrm = new ArrayList<BigDecimal>();
    private List<String> wsaaTh510Zintcalc = new ArrayList<String>();
    private Br523DTO br523DTO = null;
    private int intBatchID = 0;
    private int intBatchExtractSize;
    private Iterator<Br523DTO> iteratorList; 
    private List<Covrpf> covrRecord;
    private List<Ptrnpf> ptrnRecord;
    private List<Vprcpf> vprcRecord;
    private Map<Long, List<Hitdpf>> hitdRecord;
    private Map<Long, List<Hitrpf>> hitrRecord;
    private Map<Long, List<Annypf>> annyRecord;
    private Map<Long, List<Regppf>> regpRecord;
    private Map<Long, List<Lifepf>> lifeRecord;
    private Map<Long, List<Lextpf>> lextRecord;
    private Map<Long, List<Utrspf>> utrspfRecord;
    private Map<String, List<Lifepf>> lifepfActxRecord;
    private Map<String, Clntpf> clntRecord;
    private boolean premStatFlag;
    private boolean riskStatFlag;
    private String wsaaVirtualFund = null;
    private BigDecimal wsaaInitialAmount = BigDecimal.ZERO;
    private BigDecimal wsaaAccumAmount = BigDecimal.ZERO;
    private BigDecimal wsaaInitialBidPrice = BigDecimal.ZERO;
    private BigDecimal wsaaAccumBidPrice = BigDecimal.ZERO;
    private BigDecimal wsaaFundAmount = BigDecimal.ZERO;
    private BigDecimal wsaaIntRound = BigDecimal.ZERO;
    private BigDecimal wsaaInterestTot = BigDecimal.ZERO;
    private BigDecimal wsaaIntOnFundAmt = BigDecimal.ZERO;
    private BigDecimal wsaaIntOnFundBal = BigDecimal.ZERO;
    private BigDecimal wsaaSurrValTot = BigDecimal.ZERO;;
    private String wsaaAnname;
    private int wsaaAnnage;
    private String wsaaSanname;
    private int wsaaSannage;
    private int th510Ix;
    private String wsaaLife;
    private List<Coevpf> coevpfInsertRecord;
    private Coevpf coevpf;
    private List<Chevpf> chevpfInsertRecord;
    private Chevpf chevpf;
    private List<Ptevpf> ptevpfInsertRecord;
    private List<Lfevpf> lfevpfInsertRecord;
    ChdrTableDAM chdrIO = new ChdrTableDAM();
    int ct03 = 3;
    int ct01=1;
    private Itempf itempf = new Itempf();  //ILIFE-9041
	private static final String t5679 = "T5679";  //ILIFE-9041
    private static final Logger LOGGER = LoggerFactory.getLogger(Br523.class);
    private int ct01Value = 0;//ILIFE-9041
    private int ct03Value = 0;//ILIFE-9041
/**
 * Contains all possible labels used by goTo action.
 */
    private enum GotoLabel implements GOTOInterface {
        DEFAULT, 
        exit2090, 
        nextr5180, 
        exit5190, 
        exit5290, 
        readAnnuities5320, 
        nextr5480, 
        exit5490, 
        readAccum5551, 
        skipLast5620, 
        nextHitr5640, 
        exit5640, 
        callIo5810, 
        exit5890, 
        nextr8180, 
        exit8190, 
        nextr9000, 
        exit9000
    }

    public Br523() {
        super();
    }

protected FixedLengthStringData getWsaaProg() {
    return wsaaProg;
    }

protected PackedDecimalData getWsaaCommitCnt() {
    return wsaaCommitCnt;
    }

protected PackedDecimalData getWsaaCycleCnt() {
    return wsaaCycleCnt;
    }

protected FixedLengthStringData getLsaaStatuz() {
    return lsaaStatuz;
    }

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
    this.lsaaStatuz = lsaaStatuz;
    }

protected FixedLengthStringData getLsaaBsscrec() {
    return lsaaBsscrec;
    }

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
    this.lsaaBsscrec = lsaaBsscrec;
    }

protected FixedLengthStringData getLsaaBsprrec() {
    return lsaaBsprrec;
    }

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
    this.lsaaBsprrec = lsaaBsprrec;
    }

protected FixedLengthStringData getLsaaBprdrec() {
    return lsaaBprdrec;
    }

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
    this.lsaaBprdrec = lsaaBprdrec;
    }

protected FixedLengthStringData getLsaaBuparec() {
    return lsaaBuparec;
    }

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
    this.lsaaBuparec = lsaaBuparec;
    }

protected FixedLengthStringData getWsspEdterror() {
    return wsspcomn.edterror;
    }


    /**
    * The mainline method is the default entry point to the class
    */
public void mainline(Object... parmArray)
    {
        lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
        lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
        lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
        lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
        lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
        try {
            super.mainline();
        }
        catch (COBOLExitProgramException e) {
        // Expected exception for control flow purposes
        }
    }

protected void restart0900()
    {
        /*RESTART*/
        /** Place any additional restart processing in here.*/
        /*EXIT*/
    }

protected void initialise1000()
    {
        initialise1010();
        readTableT56791020();
        readTableTh5101040();
    }

protected void initialise1010()
    {
        pr534par.parmRecord.set(bupaIO.getParmarea());
        wsaaActxRunid.set(bprdIO.getSystemParam04());
        wsaaActxJobno.set(bsscIO.getScheduleNumber());
        wsaaThreadNumber.set(bsprIO.getProcessOccNum());
        wsspcomn.edterror.set(varcom.oK);
        /* Get today's date*/
        datcon1rec.function.set(varcom.tday);
        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
        
        String coy = bsprIO.getCompany().toString();
        String pfx = smtpfxcpy.item.toString();
      
        th510Map = itemDAO.loadSmartTable(pfx, coy, "TH510");
        t5687Map = itemDAO.loadSmartTable(pfx, coy, "T5687");
        t5515Map = itemDAO.loadSmartTable(pfx, coy, "T5515");
        t6598Map = itemDAO.loadSmartTable(pfx, coy, "T6598");
        
        if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        readChunkRecord();
    }

private void readChunkRecord(){
	
	try
	{
    // wsaaActxFn = ACTXAC0007
    List<Br523DTO> br523DTOList = actxDAO.searchActxResult(wsaaActxFn.toString(), wsaaThreadMember.toString(), intBatchExtractSize, intBatchID);
    iteratorList = br523DTOList.iterator();
    if(br523DTOList.size()>0){
        Map<String,List<String>> chdrInfor = new HashMap<String,List<String>>();
        Set<String> chdrownCoy = new HashSet<String>();
        for(Br523DTO br523DTO:br523DTOList){
            if(chdrInfor.containsKey(br523DTO.getActxChdrcoy())){
                chdrInfor.get(br523DTO.getActxChdrcoy()).add(br523DTO.getActxChdrnum());
            }else{
                List<String> chdrNums = new ArrayList<String>();
                chdrNums.add(br523DTO.getActxChdrnum());
                chdrInfor.put(br523DTO.getActxChdrcoy(), chdrNums);
            }
            chdrownCoy.add(br523DTO.getChdrCowncoy());
        }
        covrRecord = new LinkedList<Covrpf>();
        ptrnRecord = new LinkedList<Ptrnpf>();
        lifepfActxRecord = new HashMap<String, List<Lifepf>>();
        for(String coy:chdrInfor.keySet())
        {
        	List<String> chdrList = chdrInfor.get(coy);
        	if (chdrList!=null && chdrList.size()>0)
        	{
            covrRecord.addAll(covrpfDAO.searchCovrRecord(coy, chdrInfor.get(coy)));
            ptrnRecord.addAll(ptrnpfDAO.searchPtrnRecord(coy, chdrInfor.get(coy)));
            lifepfActxRecord.putAll(lifepfDAO.searchLifeRecordByActx(coy, chdrInfor.get(coy)));
        	}
        }
        List<Long> covrUQList = new ArrayList<Long>();
        for(Covrpf c:covrRecord){
            covrUQList.add(c.getUniqueNumber());
        }
        
        
        if (covrUQList.size()>0)
        {
        lextRecord = lextpfDAO.searchLextRecord(covrUQList);
        utrspfRecord = utrspfDAO.searchUtrsRecord(covrUQList);
        annyRecord = annypfDAO.searchAnnyRecord(covrUQList);
        }
        
        if(utrspfRecord!=null && !utrspfRecord.isEmpty()){
            Set<String> virtualFundList = new HashSet<String>();
            for(List<Utrspf> ul:utrspfRecord.values()){
                for(Utrspf u:ul){
                    virtualFundList.add(u.getUnitVirtualFund());
                }
            }
            vprcRecord = vprcpfDAO.searchVprcRecord(datcon1rec.intDate.toInt(), virtualFundList);
        }
        
        if (covrUQList.size()>0)
        {
         hitdRecord = hitdpfDAO.searchHitdRecord(covrUQList);
        }
        
        if(hitdRecord!=null && !hitdRecord.isEmpty()){
            List<Long> hitdUQList = new ArrayList<Long>();
            for(List<Hitdpf> hl:hitdRecord.values()){
                for(Hitdpf hd:hl){
                    hitdUQList.add(hd.getUniqueNumber());
                }
            }
            hitrRecord = hitrpfDAO.searchHitrRecord(hitdUQList);
        }

        List<String> clntnumList = new LinkedList<String>();
        if(annyRecord!=null && !annyRecord.isEmpty()){
            List<Long> annyUQList = new ArrayList<Long>();
            for(List<Annypf> anyl:annyRecord.values()){
                for(Annypf an:anyl){
                    annyUQList.add(an.getUniqueNumber());
                }
            }
            regpRecord = regppfDAO.searchRegpRecord(annyUQList);
        
            for (List<Annypf> al : annyRecord.values()) {
                for(Annypf a:al){
                    if(isNE(a.getNomlife(), LOVALUE)) {
                        clntnumList.add(a.getNomlife());
                    }
                }
            }
        }
        if (covrUQList.size()>0)
        {
        lifeRecord = lifepfDAO.searchLifeRecord(covrUQList);
        }
        
        if(lifeRecord!=null && !lifeRecord.isEmpty()){
            for (List<Lifepf> lfl : lifeRecord.values()) {
                for(Lifepf l:lfl){
                    clntnumList.add(l.getLifcnum());
                }
            }
        }
        if(lifepfActxRecord!=null && !lifepfActxRecord.isEmpty()){
            for (List<Lifepf> lfl : lifepfActxRecord.values()) {
                for(Lifepf l:lfl){
                    clntnumList.add(l.getLifcnum());
                }
            }
        }
        if(!clntnumList.isEmpty()){
            clntRecord = new HashMap<String, Clntpf>();
            for(String ownCoy:chdrownCoy){
                clntRecord.putAll(clntpfDAO.searchClntRecord(fsupfxcpy.clnt.toString(), ownCoy, clntnumList));
            }
        }
    }
    
		
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in readChunkRecord", ex);
	}
}

protected void readTableT56791020()
{
	//ILIFE-9041 start
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl(t5679);
	itempf.setItemitem(bprdIO.getAuthCode().toString().trim());
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if (itempf==null) {
		syserrrec.statuz.set("MRNF");
		syserrrec.params.set(t5679.concat(bprdIO.getAuthCode().toString()));
		fatalError600();
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea())); 
 	//ILIFE-9041 end
}

protected void readTableTh5101040()
    {
        if (th510Map == null || th510Map.size() == 0) {
            syserrrec.params.set("TH510");
            fatalError600();
        }
        if (th510Map.size() > wsaaTh510Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("TH510");
            fatalError600();
        }
        for (List<Itempf> items : th510Map.values()) {
            for (Itempf item : items) {
                if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
                    th510rec.th510Rec.set(StringUtil.rawToString(item.getGenarea()));
                    wsaaTh510Fund.add(item.getItemitem());
                    wsaaTh510Itmfrm.add(item.getItmfrm());
                    wsaaTh510Zintcalc.add(th510rec.zintcalc.toString());
                    //break;
                }

            }

        }
    }

protected void readFile2000()
 {
        if (iteratorList.hasNext()) {
            br523DTO = iteratorList.next();
            chdrIO.setChdrpfx(br523DTO.getChdrChdrpfx());
            chdrIO.setChdrcoy(br523DTO.getChdrChdrcoy());
            chdrIO.setChdrnum(br523DTO.getChdrChdrnum());
            chdrIO.setCurrfrom(br523DTO.getChdrCurrfrom());
            chdrIO.setValidflag(br523DTO.getChdrValidflag());
            chdrIO.setBillfreq(br523DTO.getChdrBillfreq());
            chdrIO.setSinstamt06(br523DTO.getChdrSinstamt06());
            chdrIO.setSinstamt01(br523DTO.getChdrSinstamt01());
            chdrIO.setCnttype(br523DTO.getChdrCnttype());
            chdrIO.setCowncoy(br523DTO.getChdrCowncoy());
            chdrIO.setCownnum(br523DTO.getChdrCownnum());
            chdrIO.setPayrnum(br523DTO.getChdrPayrnum());
            chdrIO.setAgntcoy(br523DTO.getChdrAgntcoy());
            chdrIO.setAgntnum(br523DTO.getChdrAgntnum());
            chdrIO.setBtdate(br523DTO.getChdrBtdate());
            chdrIO.setPtdate(br523DTO.getChdrPtdate());
            chdrIO.setCntcurr(br523DTO.getChdrCntcurr());
            //chdrIO.setp(br523DTO.getChdrPolsum());
        } else {
            intBatchID++;
            clearList2100();
            readChunkRecord();
            if (iteratorList.hasNext()) {
                br523DTO = iteratorList.next();
            } else {
                wsaaEof.set("Y");
                wsspcomn.edterror.set(varcom.endp);
            }
        }
        ct01Value++;
    }
protected void clearList2100()
{
	if (covrRecord != null) {
	    covrRecord.clear();
	    covrRecord = null;
	}
	
	if (ptrnRecord != null) {
	    ptrnRecord.clear();
	    ptrnRecord = null;
	}
	
	if (vprcRecord != null) {
	    vprcRecord.clear();
	    vprcRecord = null;
	}
	
	if (hitdRecord != null) {
	    hitdRecord.clear();
	    hitdRecord = null;
	}
	
	if (hitrRecord != null) {
	    hitrRecord.clear();
	    hitrRecord = null;
	}
	
	if (annyRecord != null) {
	    annyRecord.clear();
	    annyRecord = null;
	}
	
	if (regpRecord != null) {
	    regpRecord.clear();
	    regpRecord = null;
	}
	
	if (lifeRecord != null) {
	    lifeRecord.clear();
	    lifeRecord = null;
	}
	
	if (lextRecord != null) {
	    lextRecord.clear();
	    lextRecord = null;
	}
	
	if (utrspfRecord != null) {
	    utrspfRecord.clear();
	    utrspfRecord = null;
	}
	
	if (lifepfActxRecord != null) {
	    lifepfActxRecord.clear();
	    lifepfActxRecord = null;
	}
	
	if (clntRecord != null) {
	    clntRecord.clear();
	    clntRecord = null;
	}
}
protected void edit2500()
 {
        wsspcomn.edterror.set(varcom.oK);
        boolean wsaaValidRisk = false;
        boolean wsaaValidPrem = false;
        for (int i = 1; i <= 12; i++) {
            if (isEQ(t5679rec.cnRiskStat[i], br523DTO.getActxStatcode())) {
                wsaaValidRisk = true;
                break;
            }
        }
        for (int i = 1; i <= 12; i++) {
            if (isEQ(t5679rec.cnPremStat[i], br523DTO.getActxPstatcode())) {
                wsaaValidPrem = true;
                break;
            }
        }
        if ((!wsaaValidRisk) || (!wsaaValidPrem)) {
            wsspcomn.edterror.set(SPACES);
        }
    }


protected void update3000()
 {
	try
	{
            update3010();
            ptrn3080();
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in update3000", ex);
	}
 }

protected void update3010()
    {
        updateChev3100();
        readCovr5000();
        
        chevpf.setSurrchg(BigDecimal.ZERO);
        if (isNE(br523DTO.getActxValidflag(),"2")) {
            chevpf.setSurrval(wsaaSurrValTot.subtract(chevpf.getSurrchg()));
            if (chevpf.getSurrval().compareTo(chevpf.getLoanvalue())>0) {
                chevpf.setCshmt(chevpf.getSurrval().subtract(chevpf.getLoanvalue()));
            }
            else {
                chevpf.setCshmt(BigDecimal.ZERO);
            }
        }
        if(chevpfInsertRecord == null){
            chevpfInsertRecord = new ArrayList<Chevpf>();
        }
        chevpfInsertRecord.add(chevpf);
        
        updateLfev8000();
        ct03Value++;
    }

protected void ptrn3080()
 {
        // ptrnenqIO.setParams(SPACES);
        // ptrnenqIO.setChdrcoy(chevpf.getChdrcoy());
        // ptrnenqIO.setChdrnum(chevpf.getChdrnum());
        // ptrnenqIO.setTranno(wsaaVariablesInner.wsaaHighNum);
        // ptrnenqIO.setBatctrcde(SPACES);
        // ptrnenqIO.setFormat(formatsInner.ptrnrec);
        // ptrnenqIO.setFunction(varcom.begn);
        // performance improvement -- Anjali
	if(ptrnRecord != null) {//ILIFE-8332
        for (Ptrnpf p : ptrnRecord) {
            if (br523DTO.getActxChdrcoy().equals(p.getChdrcoy()) && br523DTO.getActxChdrnum().equals(p.getChdrnum())) {
                if (p.getPtrneff() > br523DTO.getActxCurrfrom()) {
                    continue;
                }
                Ptevpf ptevpf = new Ptevpf();
                ptevpf.setChdrcoy(p.getChdrcoy());
                ptevpf.setChdrnum(p.getChdrnum());
                ptevpf.setEffdate(datcon1rec.intDate.toInt());
                ptevpf.setPtrneff(p.getPtrneff());
                ptevpf.setValidflag(p.getValidflag());
                ptevpf.setTranno(p.getTranno());
                ptevpf.setBatcactyr(p.getBatcactyr());
                ptevpf.setBatcactmn(p.getBatcactmn());
                ptevpf.setBatctrcde(p.getBatctrcde());
                if (ptevpfInsertRecord == null) {
                    ptevpfInsertRecord = new ArrayList<Ptevpf>();
                }
                ptevpfInsertRecord.add(ptevpf);
            }
        }
 }
    }

protected void updateChev3100()
 {
        chevpf = new Chevpf();
        chevpf.setTotlprem(BigDecimal.ZERO);
        chevpf.setInstprem(BigDecimal.ZERO);
        chevpf.setCshmt(BigDecimal.ZERO);
        chevpf.setLoanvalue(BigDecimal.ZERO);
        chevpf.setSurrchg(BigDecimal.ZERO);
        chevpf.setSurrval(BigDecimal.ZERO);
        chevpf.setCurrfrom(0);
        chevpf.setChdrcoy(br523DTO.getActxChdrcoy());
        chevpf.setChdrnum(br523DTO.getActxChdrnum());
        chevpf.setRcdate(br523DTO.getActxOccdate());

        // changed by yy for ILIFE-2483
        if (br523DTO.getHpadHoissdte() == 0 && br523DTO.getHpadHissdte() == 0) {
            chevpf.setHoissdte(varcom.vrcmMaxDate.toInt());
            chevpf.setHissdte(varcom.vrcmMaxDate.toInt());
        } else {
            chevpf.setHoissdte(br523DTO.getHpadHoissdte());
            chevpf.setHissdte(br523DTO.getHpadHissdte());
        }
        chevpf.setStatcode(br523DTO.getActxStatcode());
        chevpf.setPstatcode(br523DTO.getActxPstatcode());
        chevpf.setCurrfrom(br523DTO.getActxCurrfrom());
        chevpf.setValidflag(br523DTO.getActxValidflag());
        chevpf.setBillfreq(br523DTO.getChdrBillfreq());
        chevpf.setTotlprem(br523DTO.getChdrSinstamt06());
        chevpf.setInstprem(br523DTO.getChdrSinstamt01());
        chevpf.setCnttype(br523DTO.getChdrCnttype());
        chevpf.setEffdate(datcon1rec.intDate.toInt());

        // changed by yy for ILIFE-2483
        wsspcomn.longconfname.set(SPACES);
        if (br523DTO.getClntowUniqueNumber() != null && !br523DTO.getClntowUniqueNumber().isEmpty()) {
            if ("C".equals(br523DTO.getClntowClttype())) {
                StringUtil stringVariable1 = new StringUtil();
                stringVariable1.addExpression(br523DTO.getClntowLsurname(), "  ");
                stringVariable1.addExpression(" ");
                stringVariable1.addExpression(br523DTO.getClntowLgivname(), "  ");
                stringVariable1.setStringInto(wsspcomn.longconfname);
            } else {
                if (br523DTO.getClntowGivname() != null && !br523DTO.getClntowGivname().isEmpty()) {
                    StringUtil stringVariable1 = new StringUtil();
                    stringVariable1.addExpression(br523DTO.getClntowSurname(), "  ");
                    stringVariable1.addExpression(", ");
                    stringVariable1.addExpression(br523DTO.getClntowGivname(), "  ");
                    stringVariable1.setStringInto(wsspcomn.longconfname);
                } else {
                    wsspcomn.longconfname.set(br523DTO.getClntowSurname());
                }
            }
        }

        chevpf.setOwnnam(wsspcomn.longconfname.toString());
        if (isNE(br523DTO.getChdrPayrnum(), SPACES)) {
            // changed by yy for ILIFE-2483
            wsspcomn.longconfname.set(SPACES);
            if (br523DTO.getClntpayUniqueNumber() != null && !br523DTO.getClntpayUniqueNumber().isEmpty()) {
                if ("C".equals(br523DTO.getClntpayClttype())) {
                    StringUtil stringVariable1 = new StringUtil();
                    stringVariable1.addExpression(br523DTO.getClntpayLsurname(), "  ");
                    stringVariable1.addExpression(" ");
                    stringVariable1.addExpression(br523DTO.getClntpayLgivname(), "  ");
                    stringVariable1.setStringInto(wsspcomn.longconfname);
                } else {
                    if (br523DTO.getClntpayGivname() != null && !br523DTO.getClntpayGivname().isEmpty()) {
                        StringUtil stringVariable1 = new StringUtil();
                        stringVariable1.addExpression(br523DTO.getClntpaySurname(), "  ");
                        stringVariable1.addExpression(", ");
                        stringVariable1.addExpression(br523DTO.getClntpayGivname(), "  ");
                        stringVariable1.setStringInto(wsspcomn.longconfname);
                    } else {
                        wsspcomn.longconfname.set(br523DTO.getClntpaySurname());
                    }
                }
            }

            chevpf.setPayorname(wsspcomn.longconfname.toString());
        } else {
            wsspcomn.longconfname.set(SPACES);
            if (br523DTO.getClntclrUniqueNumber() != null && !br523DTO.getClntclrUniqueNumber().isEmpty()) {
                if ("C".equals(br523DTO.getClntclrClttype())) {
                    StringUtil stringVariable1 = new StringUtil();
                    stringVariable1.addExpression(br523DTO.getClntclrLsurname(), "  ");
                    stringVariable1.addExpression(" ");
                    stringVariable1.addExpression(br523DTO.getClntclrLgivname(), "  ");
                    stringVariable1.setStringInto(wsspcomn.longconfname);
                } else {
                    if (br523DTO.getClntclrGivname() != null && !br523DTO.getClntclrGivname().isEmpty()) {
                        StringUtil stringVariable1 = new StringUtil();
                        stringVariable1.addExpression(br523DTO.getClntclrSurname(), "  ");
                        stringVariable1.addExpression(", ");
                        stringVariable1.addExpression(br523DTO.getClntclrGivname(), "  ");
                        stringVariable1.setStringInto(wsspcomn.longconfname);
                    } else {
                        wsspcomn.longconfname.set(br523DTO.getClntclrSurname());
                    }
                }
            }
            chevpf.setPayorname(wsspcomn.longconfname.toString());
        }

        chevpf.setAgentno(br523DTO.getAgtAgntnum());
        chevpf.setAgtype(br523DTO.getAgtAgtype());
        chevpf.setPtdate(0);
        chevpf.setBtdate(0);
        chevpf.setBtdate(br523DTO.getChdrBtdate());
        chevpf.setPtdate(br523DTO.getChdrPtdate());
        if (!"2".equals(br523DTO.getActxValidflag())) {
            if (br523DTO.getPyrBtdate() != 0 || br523DTO.getPyrPtdate() != 0) {
                chevpf.setBtdate(br523DTO.getPyrBtdate());
                chevpf.setPtdate(br523DTO.getPyrPtdate());
            }
            callTotloan3300();
        }
    }


protected void callTotloan3300()
    {
        initialize(totloanrec.totloanRec);
        totloanrec.chdrcoy.set(br523DTO.getActxChdrcoy());
        totloanrec.chdrnum.set(br523DTO.getActxChdrnum());
        totloanrec.principal.set(ZERO);
        totloanrec.interest.set(ZERO);
        totloanrec.loanCount.set(ZERO);
        totloanrec.effectiveDate.set(chevpf.getEffdate());
        totloanrec.language.set(bsscIO.getLanguage());
        callProgram(Totloan.class, totloanrec.totloanRec);
        if (isNE(totloanrec.statuz,varcom.oK)) {
            syserrrec.params.set(totloanrec.totloanRec);
            syserrrec.statuz.set(totloanrec.statuz);
            fatalError600();
        }
        chevpf.setLoanvalue(add(totloanrec.principal,totloanrec.interest).getbigdata());
    }


protected void commit3500()
 {
		//ILIFE-9041 start
	    contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callContot001();
		
		contotrec.totval.set(ct03Value);
    	contotrec.totno.set(ct03);
    	callContot001();
 		//ILIFE-9041 end
		
        coevpfDAO.insertCoevpfRecord(coevpfInsertRecord);
        coevpf = null;
        chevpfDAO.insertChevpfRecord(chevpfInsertRecord);
        chevpf = null;
        ptevpfDAO.insertPtevpfRecord(ptevpfInsertRecord);
        lfevpfDAO.insertLfevpfRecord(lfevpfInsertRecord);
        
        if (coevpfInsertRecord != null) {
            coevpfInsertRecord.clear();
            coevpfInsertRecord = null;
        }
        
        if (chevpfInsertRecord != null) {
            chevpfInsertRecord.clear();
            chevpfInsertRecord = null;
        }
        
        if (ptevpfInsertRecord != null) {
            ptevpfInsertRecord.clear();
            ptevpfInsertRecord = null;
        }
        
        if (lfevpfInsertRecord != null) {
            lfevpfInsertRecord.clear();
            lfevpfInsertRecord = null;
        }
        if (covrRecord != null) {
            covrRecord.clear();
            covrRecord = null;
        }
        
        if (ptrnRecord != null) {
            ptrnRecord.clear();
            ptrnRecord = null;
        }
        
        if (vprcRecord != null) {
            vprcRecord.clear();
            vprcRecord = null;
        }
        
        if (hitdRecord != null) {
            hitdRecord.clear();
            hitdRecord = null;
        }
        
        if (hitrRecord != null) {
            hitrRecord.clear();
            hitrRecord = null;
        }
        
        if (annyRecord != null) {
            annyRecord.clear();
            annyRecord = null;
        }
        
        if (regpRecord != null) {
            regpRecord.clear();
            regpRecord = null;
        }
        
        if (lifeRecord != null) {
            lifeRecord.clear();
            lifeRecord = null;
        }
        
        if (lextRecord != null) {
            lextRecord.clear();
            lextRecord = null;
        }
        
        if (utrspfRecord != null) {
            utrspfRecord.clear();
            utrspfRecord = null;
        }
        
        if (lifepfActxRecord != null) {
            lifepfActxRecord.clear();
            lifepfActxRecord = null;
        }
        
        if (clntRecord != null) {
            clntRecord.clear();
            clntRecord = null;
        }
    }

protected void rollback3600()
    {
        /*ROLLBACK*/
        /*EXIT*/
    }

protected void close4000()
    {
        /*CLOSE-FILES*/
        lsaaStatuz.set(varcom.oK);
        /*EXIT*/
        if (t5679List != null) {
            t5679List.clear();
        }
        t5679List = null;
        
        if (th510Map != null) {
            th510Map.clear();
        }
        th510Map = null;
        if (t5687Map != null) {
            t5687Map.clear();
        }
        t5687Map = null;
        if (t5515Map != null) {
            t5515Map.clear();
        }
        t5515Map = null;
        if (t6598Map != null) {
            t6598Map.clear();
        }
        t6598Map = null;
        if (wsaaTh510Fund != null) {
            wsaaTh510Fund.clear();
        }
        wsaaTh510Fund = null;
        if (wsaaTh510Itmfrm != null) {
            wsaaTh510Itmfrm.clear();
        }
        wsaaTh510Itmfrm = null;
        if (wsaaTh510Zintcalc != null) {
            wsaaTh510Zintcalc.clear();
        }
        wsaaTh510Zintcalc = null;
        if (covrRecord != null) {
            covrRecord.clear();
        }
        covrRecord = null;
        if (ptrnRecord != null) {
            ptrnRecord.clear();
        }
        ptrnRecord = null;
        if (vprcRecord != null) {
            vprcRecord.clear();
        }
        vprcRecord = null;
        if (hitdRecord != null) {
            hitdRecord.clear();
        }
        hitdRecord = null;
        if (hitrRecord != null) {
            hitrRecord.clear();
        }
        hitrRecord = null;
        if (annyRecord != null) {
            annyRecord.clear();
        }
        annyRecord = null;
        if (regpRecord != null) {
            regpRecord.clear();
        }
        regpRecord = null;
        if (lifeRecord != null) {
            lifeRecord.clear();
        }
        lifeRecord = null;
        if (lextRecord != null) {
            lextRecord.clear();
        }
        lextRecord = null;
        if (utrspfRecord != null) {
            utrspfRecord.clear();
        }
        utrspfRecord = null;
        if (lifepfActxRecord != null) {
            lifepfActxRecord.clear();
        }
        lifepfActxRecord = null;
        if (clntRecord != null) {
            clntRecord.clear();
        }
        clntRecord = null;
        if (coevpfInsertRecord != null) {
            coevpfInsertRecord.clear();
        }
        coevpfInsertRecord = null;
        if (chevpfInsertRecord != null) {
            chevpfInsertRecord.clear();
        }
        chevpfInsertRecord = null;
        if (ptevpfInsertRecord != null) {
            ptevpfInsertRecord.clear();
        }
        ptevpfInsertRecord = null;
        if (lfevpfInsertRecord != null) {
            lfevpfInsertRecord.clear();
        }
        lfevpfInsertRecord = null;
    }

protected void readCovr5000()
    {
		if(covrRecord != null) {//ILIFE-8332
        for(Covrpf c:covrRecord){
            if(br523DTO.getActxChdrcoy().equals(c.getChdrcoy())&&br523DTO.getActxChdrnum().equals(c.getChdrnum())){
                if(c.getCurrfrom()>br523DTO.getActxCurrfrom()){
                    continue;
                }
                wsaaLife = c.getLife();
                premStatFlag = false;
                riskStatFlag = false;
                checkCoverageStatuz5200(c);
                if (!(premStatFlag && riskStatFlag)){
                    continue;
                }
                updateCoev5300(c);
                /*    Skip the surrender calculation if we are not dealing*/
                /*    with the most recent Contract Header (Valid Flag 1)*/
                if (!"1".equals(br523DTO.getActxValidflag())) {
                    continue;
                }
                
                /*    Check we are reading a Valid Flag 1 COVR before*/
                /*    proceeding to calculate the Suurender Value*/
                if (!"1".equals(c.getValidflag())) {
                    continue;
                }
                readT56875120(c);
                readT65985130();
                callSurSubroutine5140(c);
            }
        }
    }
        /*EXIT*/
    }



protected void readT56875120(Covrpf c)
    {
        // changed by yy for ILIFE-2483
        if (t5687Map.containsKey(c.getCrtable())) {
            List<Itempf> items = t5687Map.get(c.getCrtable());
            for (Itempf item : items) {
                if (item.getItmfrm().compareTo(new BigDecimal(c.getCrrcd())) <= 0) {
                    t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
                    return;
                }
            }
        }
        syserrrec.params.set("t5687" + c.getCrtable());
        syserrrec.statuz.set(f294);
        fatalError600();
    }

protected void readT65985130()
 {
        // changed by yy for ILIFE-2483
        if (t6598Map.containsKey(t5687rec.svMethod)) {
            List<Itempf> items = t6598Map.get(t5687rec.svMethod);
            t6598rec.t6598Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
            return;
        }
        t6598rec.t6598Rec.set(SPACES);
    }

protected void callSurSubroutine5140(Covrpf c)
    {
        srcalcpy.surrenderRec.set(SPACES);
        srcalcpy.tsvtot.set(ZERO);
        srcalcpy.tsv1tot.set(ZERO);
        srcalcpy.chdrChdrcoy.set(c.getChdrcoy());
        srcalcpy.chdrChdrnum.set(c.getChdrnum());
        srcalcpy.planSuffix.set(c.getPlanSuffix());
        srcalcpy.polsum.set(br523DTO.getChdrPolsum());
        srcalcpy.lifeLife.set(c.getLife());
        srcalcpy.lifeJlife.set(c.getJlife());
        srcalcpy.covrCoverage.set(c.getCoverage());
        srcalcpy.covrRider.set(c.getRider());
        srcalcpy.crtable.set(c.getCrtable());
        srcalcpy.crrcd.set(c.getCrrcd());
        srcalcpy.ptdate.set(br523DTO.getChdrPtdate());
        srcalcpy.effdate.set(bsscIO.getEffectiveDate());
        srcalcpy.convUnits.set(c.getConvertInitialUnits());
        srcalcpy.language.set(bsscIO.getLanguage());
        srcalcpy.currcode.set(c.getPremCurrency());
        srcalcpy.chdrCurr.set(br523DTO.getChdrCntcurr());
        srcalcpy.pstatcode.set(c.getPstatcode());
        if (isGT(c.getInstprem(),ZERO)) {
            srcalcpy.singp.set(c.getInstprem());
        }
        else {
            srcalcpy.singp.set(c.getSingp());
        }
        srcalcpy.billfreq.set(br523DTO.getChdrBillfreq());
        srcalcpy.status.set(SPACES);
        while ( !(isEQ(srcalcpy.status,varcom.endp))) {
            callSurrMethod6000();
        }
        
    }

protected void checkCoverageStatuz5200(Covrpf c)
 {
        for (int i = 1; i <= 12; i++) {
            int rider = Integer.parseInt(c.getRider());
            if (rider == 0) {
                if (isNE(t5679rec.covPremStat[i], SPACES)) {
                    if ((isEQ(t5679rec.covPremStat[i], c.getPstatcode()))) {
                        premStatFlag = true;
                    }
                }
                if (isNE(t5679rec.covRiskStat[i], SPACES)) {
                    if ((isEQ(t5679rec.covRiskStat[i], c.getStatcode()))) {
                        riskStatFlag = true;
                    }
                }
            } else if (rider > 0) {
                if (isNE(t5679rec.ridPremStat[i], SPACES)) {
                    if ((isEQ(t5679rec.ridPremStat[i], c.getPstatcode()))) {
                        premStatFlag = true;
                    }
                }
                if (isNE(t5679rec.ridRiskStat[i], SPACES)) {
                    if ((isEQ(t5679rec.ridRiskStat[i], c.getStatcode()))) {
                        riskStatFlag = true;
                    }
                }
            }
            if (premStatFlag && riskStatFlag) {
                return;
            }
        }
    }


protected void updateCoev5300(Covrpf c)
 {
	try
	{
        setCoevValue5320(c);
        begnLext5320(c);
        if ("1".equals(br523DTO.getActxValidflag()) && "1".equals(c.getValidflag())) {
            begnUtrssur5320(c);
            begnHitd5320(c);
        }
        readAnnuities5320(c);
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in updateCoev5300", ex);
	}
  }


protected void setCoevValue5320(Covrpf covrIO)
    {
    
        coevpf = new Coevpf();
        coevpf.setEffdate(datcon1rec.intDate.toInt());
        coevpf.setChdrcoy(covrIO.getChdrcoy());
        coevpf.setChdrnum(covrIO.getChdrnum());
        coevpf.setCoverage(covrIO.getCoverage());
        coevpf.setRider(covrIO.getRider());
        coevpf.setLife(covrIO.getLife());
        coevpf.setCrtable(covrIO.getCrtable());
        coevpf.setRiskCessTerm(covrIO.getRiskCessTerm());
        coevpf.setPremCessTerm(covrIO.getPremCessTerm());
        coevpf.setSumins(covrIO.getSumins());
        coevpf.setCrrcd(covrIO.getCrrcd());
        coevpf.setZlinstprem(covrIO.getZlinstprem());
        coevpf.setCurrfrom(covrIO.getCurrfrom());
        coevpf.setValidflag(covrIO.getValidflag());
        if (covrIO.getInstprem().compareTo(BigDecimal.ZERO)!=0) {
            coevpf.setInstprem(covrIO.getInstprem());
        }
        BigDecimal billfreqC = new BigDecimal(br523DTO.getChdrBillfreq());
        coevpf.setAextprm(covrIO.getZlinstprem().multiply(billfreqC));
        coevpf.setAprem(covrIO.getInstprem().multiply(billfreqC));
    }

protected void begnLext5320(Covrpf covrIO)
 {
        if(!lextRecord.isEmpty()){
            List<Lextpf> lextpfList = lextRecord.get(covrIO.getUniqueNumber());
            int i = 1;
            if(lextpfList != null && lextpfList.size() > 0) {
	            for (Lextpf l : lextpfList) {
	                if (l.getCurrfrom() > br523DTO.getActxCurrfrom()) {
	                    continue;
	                }
	                coevpf.setOpcda(i, l.getOpcda());
	                coevpf.setOppc(i, l.getOppc());
	                coevpf.setAgerate(i, l.getAgerate());
	                coevpf.setInsprm(i, l.getInsprm());
	                coevpf.setExtCessTerm(i, l.getExtCessTerm());
	                i++;
	            }
            }
        }
    }

protected void begnUtrssur5320(Covrpf covrIO)
 {

        int wsaaSub = 1;
        if(!utrspfRecord.isEmpty()){
            
                List<Utrspf> utrspfList = utrspfRecord.get(covrIO.getUniqueNumber());
                if(utrspfList != null && utrspfList.size() > 0) {
	                for (int i = 0; i < utrspfList.size() && wsaaSub <= 5; i++) {
	                    Utrspf u = utrspfList.get(i);	                    
	                    if (wsaaVirtualFund == null || wsaaVirtualFund.isEmpty()) {
	                        wsaaSub = 1;
	                        coevpf.setVrtfnd(wsaaSub, u.getUnitVirtualFund());
	                        wsaaVirtualFund = u.getUnitVirtualFund();
	                    }
	                    if (!u.getUnitVirtualFund().equals(wsaaVirtualFund)) {
	                        fundValue5550();
	                        coevpf.setFundamnt(wsaaSub, wsaaInitialAmount.add(wsaaAccumAmount));
	                        wsaaSub++;
	                        coevpf.setVrtfnd(wsaaSub, u.getUnitVirtualFund());
	                        wsaaVirtualFund = u.getUnitVirtualFund();
	                    }
	                    if ("I".equals(u.getUnitType())) {
	                        wsaaInitialAmount = wsaaInitialAmount.add(u.getCurrentDunitBal());
	                    } else {
	                        wsaaAccumAmount = wsaaAccumAmount.add(u.getCurrentDunitBal());
	                    }
	                }
                } else {
                	if(wsaaSub >= 1){
                        fundValue5550();
                        coevpf.setFundamnt(wsaaSub, wsaaInitialAmount.add(wsaaAccumAmount));
                    }
                    wsaaSub = 6;
                }
            
        }
    }

protected void begnHitd5320(Covrpf c)
 {
        if(!hitdRecord.isEmpty()){
            List<Hitdpf> hitdpfList = hitdRecord.get(c.getUniqueNumber());
            int i;
	        if(hitdpfList != null && hitdpfList.size() > 0) {  	                        	
                for (i = 0;i < hitdpfList.size() && i < 5; ) {
                	Hitdpf h= hitdpfList.get(i);
                    if ("1".equals(h.getValidflag())) {
                        coevpf.setVrtfnd(i, h.getZintbfnd());
                        coevpf.setFundamnt(i, h.getZlstfndval());
                    } else {
                        i++;
                        continue;
                    }
                    calculateInterest5620(h,c,i+1);
                    i++;
                }	            
	        }
        }
    }

protected void readAnnuities5320(Covrpf c)
 {
        if(!annyRecord.isEmpty()){
        	List<Annypf> annypfList = annyRecord.get(c.getUniqueNumber());
        	if(annypfList != null && annypfList.size() > 0) {   
	            for (Annypf a : annypfList) {
	                if (isNE(a.getNomlife(),LOVALUE)) {
	                    nomlife5820(a, c);
	                } else {
	                    nomlifeSpaces5820(c);
	                }
	    
	                int wsaaSub = 1;
	                if(!regpRecord.isEmpty()){
	                    while (wsaaSub <= 5) {
	                    	List<Regppf> regpList = regpRecord.get(a.getUniqueNumber());
	                     	if(regpList != null && regpList.size() > 0) {   
		                        for (Regppf rg : regpList) {
		                            
		                            coevpf.setAnname(wsaaSub, wsaaAnname);
		                            coevpf.setAnnage(wsaaSub, wsaaAnnage);
		                            coevpf.setSanname(wsaaSub, wsaaSanname);
		                            coevpf.setSannage(wsaaSub, wsaaSannage);
		                            coevpf.setRgpymop(wsaaSub, rg.getRgpymop());
		                            coevpf.setFpaydate(wsaaSub, rg.getFirstPaydate());
		                            coevpf.setNpaydate(wsaaSub, rg.getNextPaydate());
		                            coevpf.setEpaydate(wsaaSub, rg.getLastPaydate());
		                            coevpf.setPymt(wsaaSub, rg.getPymt());
		                            wsaaSub++;
		                        }
	                    	}
	                     	else {
		                     	 if (wsaaSub == 1) {
		                             coevpf.setAnname(wsaaSub, wsaaAnname);
		                             coevpf.setAnnage(wsaaSub, wsaaAnnage);
		                             coevpf.setSanname(wsaaSub, wsaaSanname);
		                             coevpf.setSannage(wsaaSub, wsaaSannage);
		                         }
		                         wsaaSub = 6;
	                     	}
	                    }
	                }
	            }
        	}
        }

        /* WRITE-COEV-RECORD */
        /* We can now write the COEV record. */
        if (coevpfInsertRecord == null) {
            coevpfInsertRecord = new LinkedList<Coevpf>();
        }
        coevpfInsertRecord.add(coevpf);
        /* EXIT */
    }


protected void fundValue5550()
 {
        /* Read Table T5515 to find the Fund currency. */
        // changed by yy for ILIFE-2483
	
		if (wsaaVirtualFund==null || wsaaVirtualFund.length()==0)
		{
			return;
		}
		
        if (t5515Map.containsKey(wsaaVirtualFund)) {
            List<Itempf> items = t5515Map.get(wsaaVirtualFund);
            t5515rec.t5515Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
        } else {
            syserrrec.params.set("t5515" + wsaaVirtualFund);
            syserrrec.statuz.set(varcom.mrnf);
            fatalError600();
        }

        /* Read the vprc file for each fund type */
        if (wsaaInitialAmount.compareTo(BigDecimal.ZERO) == 0) {
            readAccum5551();
            return;
        }
        for (Vprcpf v : vprcRecord) {
            if (v.getUnitVirtualFund().equals(wsaaVirtualFund) && "I".equals(v.getUnitType())) {
                wsaaInitialBidPrice = v.getUnitBidPrice();
                wsaaInitialAmount = wsaaInitialAmount.multiply(wsaaInitialBidPrice);
                if (wsaaInitialAmount.compareTo(BigDecimal.ZERO) != 0) {
                    zrdecplrec.amountIn.set(wsaaInitialAmount);
                    zrdecplrec.currency.set(t5515rec.currcode);
                    a000CallRounding();
                    wsaaInitialAmount = zrdecplrec.amountOut.getbigdata();
                    break;
                }
            }
        }

    }

protected void readAccum5551()
 {
        if (wsaaAccumAmount.compareTo(BigDecimal.ZERO) == 0) {
            return;
        }

        for (Vprcpf v : vprcRecord) {
            if (v.getUnitVirtualFund().equals(wsaaVirtualFund) && "A".equals(v.getUnitType())) {
                wsaaAccumBidPrice = v.getUnitBidPrice();
                wsaaAccumAmount = wsaaAccumAmount.multiply(wsaaAccumBidPrice);
                if (wsaaAccumAmount.compareTo(BigDecimal.ZERO) != 0) {
                    zrdecplrec.amountIn.set(wsaaAccumAmount);
                    zrdecplrec.currency.set(t5515rec.currcode);
                    a000CallRounding();
                    wsaaAccumAmount = zrdecplrec.amountOut.getbigdata();
                }
                break;
            }
        }

    }


protected void calculateInterest5620(Hitdpf h,Covrpf c,int i)
    {
    
        /*    The first thing to do here is to check whether there are*/
        /*    any HITR records that should have been allocated interest*/
        /*    prior to today. This may happen if a transaction is reversed*/
        /*    and causing the effective date of the original HITR to be*/
        /*    in the past. If any such HITR records are found, we must*/
        /*    calculate the interest due on each of these records up to*/
        /*    the last interest allocation date, then add the interest*/
        /*    calculated and the fund amounts on these HITRs into the*/
        /*    current fund balance. Once this has been done, the fund*/
        /*    balance will be accurate and up to date, and we will be*/
        /*    ready to calculate interest for the current allocation*/
        /*    period.*/
        /*    Read Table T5515 to find the Fund currency.                  */
    
        // changed by yy for ILIFE-2483
        if (t5515Map.containsKey(h.getZintbfnd())) {
            List<Itempf> items = t5515Map.get(h.getZintbfnd());
            t5515rec.t5515Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
        } else {
            syserrrec.params.set("t5515" + h.getZintbfnd());
            syserrrec.statuz.set(varcom.mrnf);
            fatalError600();
        }

        if (h.getZlstintdte() == varcom.vrcmMaxDate.toInt()) {
            skipLast5620(h, c, i);
        }
        start5620(h, c, i);
    }

protected void start5620(Hitdpf h,Covrpf c,int ix)
    {

        boolean foundFlag = false;
        int i = 0;
        for (; i <= wsaaTh510Size; i++) {
            if (h.getZintbfnd().equals(wsaaTh510Fund.get(i))
                    && wsaaTh510Itmfrm.get(i).compareTo(new BigDecimal(h.getZlstintdte())) <= 0) {
                foundFlag = true;
                break;
            }
        }
        if (!foundFlag) {
            syserrrec.params.set("th510" + h.getZintbfnd() + h.getZlstintdte());
            fatalError600();
        }
        
        th510Ix= i;
        int wsaaInterestDate = h.getZlstintdte();
        if(!hitrRecord.isEmpty()){
        	List<Hitrpf> hitrpfList = hitrRecord.get(h.getUniqueNumber());
        	 if(hitrpfList != null && hitrpfList.size() > 0)  {
	            for(Hitrpf hr:hitrpfList){
	                
	                /*    If the interest allocation field is set to 'N', then*/
	                /*    interest allocation is not required for that particular*/
	                /*    transaction. However, we must still include the amount*/
	                /*    of the HITR when updating the HITD record with the latest*/
	                /*    fund balance.*/
	                /*    Note: there is no interest allocation for the following*/
	                /*    transactions:*/
	                /*    1. Fund switching (out only)*/
	                /*    2. Reversal of fund switch (in and out)*/
	                /*    3. Surrenders and claims*/
	                /*    4. Reversal of surrenders and claims*/
	                /*    5. Debt recovery*/
	                // skip the record which used for skipLast5620
	                if(hr.getEffdate() != h.getZlstintdte()){
	                    continue;
	                }
	                readHitr5640(hr, wsaaInterestDate, h, c);
	            }
        	}
        }

        wsaaIntRound = wsaaIntOnFundAmt;
        if (wsaaIntRound.compareTo(BigDecimal.ZERO)<0) {
            coevpf.setFundamnt(ix, coevpf.getFundamnt(ix).add(wsaaFundAmount));
        }
        else {
            coevpf.setFundamnt(ix, coevpf.getFundamnt(ix).add(wsaaIntRound).add(wsaaFundAmount));
            wsaaInterestTot = wsaaIntOnFundAmt;
        }
    }

protected void skipLast5620(Hitdpf h,Covrpf c,int ix)
    {
        // changed by yy for ILIFE-2483
        boolean foundFlag = false;
        int i = 0;
        for (; i <= wsaaTh510Size; i++) {
            if (h.getZintbfnd().equals(wsaaTh510Fund.get(i))
                    && wsaaTh510Itmfrm.get(i).compareTo(new BigDecimal(h.getZnxtintdte())) <= 0) {
                foundFlag = true;
                break;
            }
        }
        if (!foundFlag) {
            syserrrec.params.set("th510" + h.getZintbfnd() + h.getZnxtintdte());
            fatalError600();
        }
    
        th510Ix = i;
        /*    Calculate interest on the current fund balance.*/
        ibincalrec.ibincalRec.set(SPACES);
        ibincalrec.company.set(bsprIO.getCompany());
        if (h.getZlstintdte() == varcom.vrcmMaxDate.toInt()) {
            ibincalrec.lastIntApp.set(h.getEffdate());
        }
        else {
            ibincalrec.lastIntApp.set(h.getZlstintdte());
        }
        ibincalrec.nextIntDue.set(datcon1rec.intDate);
        ibincalrec.capAmount.set(h.getZlstfndval());
        ibincalrec.intAmount.set(ZERO);
        ibincalrec.intRate.set(ZERO);
        ibincalrec.fund.set(h.getZintbfnd());
        ibincalrec.crrcd.set(c.getCrrcd());
      /* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
       // callProgram(wsaaTh510Zintcalc.get(th510Ix), ibincalrec.ibincalRec, chdrIO);
      
  		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(wsaaTh510Zintcalc.get(th510Ix)) && /* IJTI-1523 */
  				er.isExternalized(chdrIO.getCnttype().toString(), c.getCrtable())))
  		{
  			callProgramX(wsaaTh510Zintcalc.get(th510Ix), ibincalrec.ibincalRec);
  		}
  		else
  		{
  			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
  		  	Vpxibinrec vpxibinrec  = new Vpxibinrec();
  		  	
  			vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
  			vpxibinrec.function.set("INIT");
  			callProgram(Vpxibin.class, vpmcalcrec.vpmcalcRec, vpxibinrec);
  			ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
  			//ILIFE-7300
  			ibincalrec.cnttype.set(chdrIO.getCnttype());
  			ibincalrec.crtable.set(c.getCrtable());

  			callProgram(wsaaTh510Zintcalc.get(th510Ix),ibincalrec.ibincalRec);
  			
  			
  		}
  		/* ILIFE-3142 End*/
        
        if (isNE(ibincalrec.statuz,varcom.oK)) {
            syserrrec.params.set(ibincalrec.ibincalRec);
            syserrrec.statuz.set(ibincalrec.statuz);
            fatalError600();
        }
        /* MOVE IBIN-INT-AMOUNT         TO ZRDP-AMOUNT-IN.              */
        /* MOVE T5515-CURRCODE          TO ZRDP-CURRENCY.               */
        /* PERFORM A000-CALL-ROUNDING.                                  */
        /* MOVE ZRDP-AMOUNT-OUT         TO IBIN-INT-AMOUNT.             */
        if (isNE(ibincalrec.intAmount, 0)) {
            zrdecplrec.amountIn.set(ibincalrec.intAmount);
            zrdecplrec.currency.set(t5515rec.currcode);
            a000CallRounding();
            ibincalrec.intAmount.set(zrdecplrec.amountOut);
        }
        wsaaIntOnFundBal = ibincalrec.intAmount.getbigdata();
        /*    Now we calculate the interest due on all HITR records which*/
        /*    have been completed by Unit Deal, but which have not yet*/
        /*    been allocated interest for the current period.*/
        int wsaaInterestDate = h.getZnxtintdte();
        if(!hitrRecord.isEmpty()){
        	List<Hitrpf> hitrpfList1 = hitrRecord.get(h.getUniqueNumber());
        	if(hitrpfList1 != null && hitrpfList1.size() > 0)  {
	            for (Hitrpf hr : hitrpfList1) {
	             // skip the record which used for start5620
	                if (hr.getEffdate() != h.getZnxtintdte()) {
	                    continue;
	                }
	                readHitr5640(hr, wsaaInterestDate, h, c);
	            }
        	}
        }
        wsaaInterestTot = wsaaIntOnFundBal.add(wsaaIntOnFundAmt);
        wsaaIntRound = wsaaInterestTot;
        if (wsaaIntRound.compareTo(BigDecimal.ZERO)<0) {
            coevpf.setFundamnt(ix, coevpf.getFundamnt(ix).add(wsaaFundAmount));
        }
        else {
            coevpf.setFundamnt(ix, coevpf.getFundamnt(ix).add(wsaaIntRound).add(wsaaFundAmount));
        }
    }

protected void readHitr5640(Hitrpf hr, int wsaaInterestDate, Hitdpf h, Covrpf c){
    if ("N".equals(hr.getZintalloc())) {
        ibincalrec.intEffdate.set(varcom.vrcmMaxDate);
        ibincalrec.intRate.set(ZERO);
        return;
    }
    /* If the effective date on the HITR is the same as the current*/
    /* interest date, then this record is not due for processing yet.*/
    if (wsaaInterestDate == hr.getEffdate()) {
        return;
    }
    ibincalrec.ibincalRec.set(SPACES);
    ibincalrec.company.set(bsprIO.getCompany());
    ibincalrec.lastIntApp.set(hr.getEffdate());
    ibincalrec.nextIntDue.set(wsaaInterestDate);
    ibincalrec.capAmount.set(hr.getFundAmount());
    ibincalrec.intAmount.set(ZERO);
    ibincalrec.intRate.set(ZERO);
    ibincalrec.fund.set(h.getZintbfnd());
    ibincalrec.crrcd.set(c.getCrrcd());
  /* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
    // callProgram(wsaaTh510Zintcalc.get(th510Ix), ibincalrec.ibincalRec, chdrIO);
   
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(wsaaTh510Zintcalc.get(th510Ix))))/* IJTI-1523 */
	{
		callProgram(wsaaTh510Zintcalc.get(th510Ix), ibincalrec.ibincalRec);
	}
	else
	{
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	  	Vpxibinrec vpxibinrec  = new Vpxibinrec();
	  	
		vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
		vpxibinrec.function.set("INIT");
		callProgram(Vpxibin.class, vpmcalcrec.vpmcalcRec, vpxibinrec);
		ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
		//ILIFE-7300
		ibincalrec.cnttype.set(chdrIO.getCnttype());
		ibincalrec.crtable.set(c.getCrtable());

		callProgram(wsaaTh510Zintcalc.get(th510Ix),ibincalrec.ibincalRec);
		
		
	}
	/* ILIFE-3142 End*/
    if (isNE(ibincalrec.statuz,varcom.oK)) {
        syserrrec.params.set(ibincalrec.ibincalRec);
        syserrrec.statuz.set(ibincalrec.statuz);
        fatalError600();
    }
    /* MOVE IBIN-INT-AMOUNT         TO ZRDP-AMOUNT-IN.              */
    /* MOVE T5515-CURRCODE          TO ZRDP-CURRENCY.               */
    /* PERFORM A000-CALL-ROUNDING.                                  */
    /* MOVE ZRDP-AMOUNT-OUT         TO IBIN-INT-AMOUNT.             */
    if (isNE(ibincalrec.intAmount, 0)) {
        zrdecplrec.amountIn.set(ibincalrec.intAmount);
        zrdecplrec.currency.set(t5515rec.currcode);
        a000CallRounding();
        ibincalrec.intAmount.set(zrdecplrec.amountOut);
    }
    wsaaIntOnFundAmt = wsaaIntOnFundAmt.add(ibincalrec.intAmount.getbigdata());
}

protected void nomlife5820(Annypf a, Covrpf c)
 {
        Clntpf clntpf = readClts7000(br523DTO.getChdrCowncoy(), a.getNomlife());
        wsaaAnname = wsspcomn.longconfname.toString();
        initialize(agecalcrec.agecalcRec);
        agecalcrec.function.set("CALCB");
        agecalcrec.language.set(bsscIO.getLanguage());
        agecalcrec.company.set(bsprIO.getFsuco());
        agecalcrec.cnttype.set(br523DTO.getChdrCnttype());
        agecalcrec.intDate1.set(clntpf.getCltdob());
        agecalcrec.intDate2.set(c.getRerateDate());
        callProgram(Agecalc.class, agecalcrec.agecalcRec);
        if (isNE(agecalcrec.statuz, varcom.oK)) {
            syserrrec.params.set(agecalcrec.agecalcRec);
            syserrrec.statuz.set(agecalcrec.statuz);
            fatalError600();
        }
        wsaaAnnage = agecalcrec.agerating.toInt();
        if(!lifeRecord.isEmpty()){
        	List<Lifepf> lifepfList = lifeRecord.get(c.getUniqueNumber());
        	if (lifepfList != null && lifepfList.size() > 0) {
	            for (Lifepf l : lifepfList) {
	                if (!l.getLifcnum().equals(a.getNomlife())) {
	                    Clntpf clntpfLife = readClts7000(br523DTO.getChdrCowncoy(), l.getLifcnum());
	                    wsaaSanname = wsspcomn.longconfname.toString();
	                    initialize(agecalcrec.agecalcRec);
	                    agecalcrec.function.set("CALCB");
	                    agecalcrec.company.set(c.getChdrcoy());
	                    agecalcrec.cnttype.set(br523DTO.getChdrCnttype());
	                    agecalcrec.intDate1.set(clntpfLife.getCltdob());
	                    agecalcrec.intDate2.set(c.getRerateDate());
	                    agecalcrec.statuz.set(varcom.oK);
	                    callProgram(Agecalc.class, agecalcrec.agecalcRec);
	                    if (isNE(agecalcrec.statuz, varcom.oK)) {
	                        syserrrec.params.set(agecalcrec.agecalcRec);
	                        syserrrec.statuz.set(agecalcrec.statuz);
	                        fatalError600();
	                    }
	                    wsaaSannage = agecalcrec.agerating.toInt();
	                }
	            }
        }
        }

    }

protected void nomlifeSpaces5820(Covrpf c)
 {
        if(!lifeRecord.isEmpty()){
        	List<Lifepf> lifeSpacepfList = lifeRecord.get(c.getUniqueNumber());
        	if (lifeSpacepfList != null && lifeSpacepfList.size() > 0) {
	            for (Lifepf l : lifeSpacepfList) {
	                Clntpf clntpf = readClts7000(br523DTO.getChdrCowncoy(), l.getLifcnum());
	                wsaaAnname = wsspcomn.longconfname.toString();
	                initialize(agecalcrec.agecalcRec);
	                agecalcrec.function.set("CALCB");
	                agecalcrec.company.set(c.getChdrcoy());
	                agecalcrec.cnttype.set(br523DTO.getChdrCnttype());
	                agecalcrec.intDate1.set(clntpf.getCltdob());
	                agecalcrec.intDate2.set(c.getRerateDate());
	                agecalcrec.statuz.set(varcom.oK);
	                callProgram(Agecalc.class, agecalcrec.agecalcRec);
	                if (isNE(agecalcrec.statuz, varcom.oK)) {
	                    syserrrec.params.set(agecalcrec.agecalcRec);
	                    syserrrec.statuz.set(agecalcrec.statuz);
	                    fatalError600();
	                }
	                wsaaAnnage = agecalcrec.agerating.toInt();
	                return;// same as before
	            }
        	}
        }
    }



protected void callSurrMethod6000()
 {

        srcalcpy.estimatedVal.set(ZERO);
        srcalcpy.actualVal.set(ZERO);
        if (isEQ(t6598rec.calcprog, SPACES)) {
            srcalcpy.status.set(varcom.endp);
            return;
        }

        /* IVE-797 RUL Product - Full Surrender Calculation started */
        // callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
        // VPMS code start
        if (!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) {
            callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
        } else {
            Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
            Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
            Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

            vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
            vpxsurcrec.function.set("INIT");
            callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec, vpxsurcrec); // IO
                                                                           // read
            srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
            vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
            vpmfmtrec.initialize();
            vpmfmtrec.amount02.set(wsaaEstimateTot);

            callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrIO);//VPMS call

            if (isEQ(srcalcpy.type, "L")) {
                vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
                callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
                srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
                srcalcpy.status.set(varcom.endp);
            } else if (isEQ(srcalcpy.type, "C")) {
                srcalcpy.status.set(varcom.endp);
            } else if (isEQ(srcalcpy.type, "A") && vpxsurcrec.statuz.equals(varcom.endp)) {
                srcalcpy.status.set(varcom.endp);
            } else {
                srcalcpy.status.set(varcom.oK);
            }
        }
        /* IVE-797 RUL Product - Full Surrender Calculation end */
        if (isNE(srcalcpy.status, varcom.oK) && isNE(srcalcpy.status, varcom.endp)) {
            syserrrec.params.set(srcalcpy.surrenderRec);
            syserrrec.statuz.set(srcalcpy.status);
            fatalError600();
        }
        /* MOVE SURC-ACTUAL-VAL TO ZRDP-AMOUNT-IN. */
        /* MOVE SURC-CHDR-CURR TO ZRDP-CURRENCY. */
        /* PERFORM A000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO SURC-ACTUAL-VAL. */
        if (isNE(srcalcpy.actualVal, 0)) {
            if (isNE(srcalcpy.currcode, SPACES)) {
                zrdecplrec.currency.set(srcalcpy.currcode);
            } else {
                zrdecplrec.currency.set(srcalcpy.chdrCurr);
            }
            zrdecplrec.amountIn.set(srcalcpy.actualVal);
            a000CallRounding();
            srcalcpy.actualVal.set(zrdecplrec.amountOut);
        }
        wsaaSurrValTot = wsaaSurrValTot.add(srcalcpy.actualVal.getbigdata());

    }


protected Clntpf readClts7000(String coy, String clntnum)
 {
        // for duplicate company
        if (clntRecord != null && !clntRecord.isEmpty()) {
            String clntKey = coy + clntpfDAO.getSplitSign() + clntnum;
            if (clntRecord.containsKey(clntKey)) {
                Clntpf c = clntRecord.get(clntKey);
                wsspcomn.longconfname.set(SPACES);
                if ("C".equals(c.getClttype())) {
                    wsspcomn.longconfname.set(SPACES);
                    StringUtil stringVariable1 = new StringUtil();
                    stringVariable1.addExpression(c.getLsurname(), "  ");
                    stringVariable1.addExpression(" ");
                    stringVariable1.addExpression(c.getLgivname(), "  ");
                    stringVariable1.setStringInto(wsspcomn.longconfname);
                    return c;
                }
                if (c.getGivname() != null && !c.getGivname().isEmpty()) {
                    StringUtil stringVariable1 = new StringUtil();
                    stringVariable1.addExpression(c.getSurname(), "  ");
                    stringVariable1.addExpression(", ");
                    stringVariable1.addExpression(c.getGivname(), "  ");
                    stringVariable1.setStringInto(wsspcomn.longconfname);
                } else {
                    wsspcomn.longconfname.set(c.getSurname());
                }
                return c;
            }
        }
        return new Clntpf();// a blank record
    }

protected void updateLfev8000()
 {
        wsaaLife = LOVALUE.toString();
        if (lifepfActxRecord != null && !lifepfActxRecord.isEmpty()) {
            List<Lifepf> lifepfList = lifepfActxRecord.get(br523DTO.getActxChdrcoy() + lifepfDAO.getSplitSign()
                    + br523DTO.getActxChdrnum());
            Lfevpf lfevpf = new Lfevpf();
            lfevpf.setChdrcoy(br523DTO.getActxChdrcoy());
            lfevpf.setChdrnum(br523DTO.getActxChdrnum());
            lfevpf.setEffdate(bsscIO.getEffectiveDate().toInt());
            int i = 0;
            boolean foundFlag = false;
            if(lifepfList != null && lifepfList.size() > 0) {
	            for (Lifepf l : lifepfList) {
	                if (l.getCurrfrom() > br523DTO.getActxCurrfrom()) {
	                    continue;
	                }
	                if (!l.getLife().equals(wsaaLife)) {
	                    wsaaLife = l.getLife();
	                    i++;
	                }
	                Clntpf cl = readClts7000(br523DTO.getChdrCowncoy(), l.getLifcnum());
	                lfevpf.setLife(l.getLife());
	                lfevpf.setCurrfrom(l.getCurrfrom());
	                initialize(agecalcrec.agecalcRec);
	                agecalcrec.function.set("CALCB");
	                agecalcrec.language.set(bsscIO.getLanguage());
	                agecalcrec.company.set(bsprIO.getFsuco());
	                agecalcrec.cnttype.set(br523DTO.getChdrCnttype());
	                agecalcrec.intDate1.set(cl.getCltdob());
	                agecalcrec.intDate2.set(bsscIO.getEffectiveDate());
	                callProgram(Agecalc.class, agecalcrec.agecalcRec);
	                if (isNE(agecalcrec.statuz, varcom.oK)) {
	                    syserrrec.params.set(agecalcrec.agecalcRec);
	                    syserrrec.statuz.set(agecalcrec.statuz);
	                    fatalError600();
	                }
	                if ("00".equals(l.getJlife())) {
	                    lfevpf.setAlname(i, wsspcomn.longconfname.toString());
	                    lfevpf.setAnbccd(i, l.getAnbAtCcd());
	                    lfevpf.setSmokeind(i, l.getSmoking());
	                    lfevpf.setAnbisd(i, agecalcrec.agerating.toInt());
	                    lfevpf.setSex(i, l.getCltsex());
	                    lfevpf.setJlsex(i, "");
	                    lfevpf.setJanbccd(i, 0);
	                    lfevpf.setJanbisd(i, 0);
	                } else {
	                    lfevpf.setJlname(i, wsspcomn.longconfname.toString());
	                    lfevpf.setJanbccd(i, l.getAnbAtCcd());
	                    lfevpf.setSmkrqd(i, l.getSmoking());
	                    lfevpf.setJanbisd(i, agecalcrec.agerating.toInt());
	                    lfevpf.setJlsex(i, l.getCltsex());
	                    lfevpf.setSex(i, "");
	                    lfevpf.setAnbccd(i, 0);
	                    lfevpf.setAnbisd(i, 0);
	                }
	                foundFlag = true;
	            }
        }
            if (foundFlag) {
                if (lfevpfInsertRecord == null) {
                    lfevpfInsertRecord = new ArrayList<Lfevpf>();
                }
                lfevpfInsertRecord.add(lfevpf);
            }
        }
 
    }



protected void a000CallRounding()
    {
        /*A100-CALL*/
        zrdecplrec.function.set(SPACES);
        zrdecplrec.company.set(bsprIO.getCompany());
        zrdecplrec.statuz.set(varcom.oK);
        zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
        callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
        if (isNE(zrdecplrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(zrdecplrec.statuz);
            syserrrec.params.set(zrdecplrec.zrdecplRec);
            fatalError600();
        }
        /*A900-EXIT*/
    }
}
