package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZmpdTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:14
 * Class transformed from ZMPD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZmpdTableDAM extends ZmpdpfTableDAM {

	public ZmpdTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZMPD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "ZMEDCOY"
		             + ", ZMEDPRV"
		             + ", SEQNO";
		
		QUALIFIEDCOLUMNS = 
		            "ZMEDCOY, " +
		            "ZMEDPRV, " +
		            "SEQNO, " +
		            "ZDOCNUM, " +
		            "ZMMCCDE, " +
		            "EFFDATE, " +
		            "DTETRM, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "ZMEDCOY ASC, " +
		            "ZMEDPRV ASC, " +
		            "SEQNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "ZMEDCOY DESC, " +
		            "ZMEDPRV DESC, " +
		            "SEQNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               zmedcoy,
                               zmedprv,
                               seqno,
                               zdocnum,
                               zmmccde,
                               effdate,
                               dtetrm,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(243);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getZmedcoy().toInternal()
					+ getZmedprv().toInternal()
					+ getSeqno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, zmedcoy);
			what = ExternalData.chop(what, zmedprv);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(10);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(zmedcoy.toInternal());
	nonKeyFiller2.setInternal(zmedprv.toInternal());
	nonKeyFiller3.setInternal(seqno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(87);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ getZdocnum().toInternal()
					+ getZmmccde().toInternal()
					+ getEffdate().toInternal()
					+ getDtetrm().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, zdocnum);
			what = ExternalData.chop(what, zmmccde);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, dtetrm);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getZmedcoy() {
		return zmedcoy;
	}
	public void setZmedcoy(Object what) {
		zmedcoy.set(what);
	}
	public FixedLengthStringData getZmedprv() {
		return zmedprv;
	}
	public void setZmedprv(Object what) {
		zmedprv.set(what);
	}
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getZdocnum() {
		return zdocnum;
	}
	public void setZdocnum(Object what) {
		zdocnum.set(what);
	}	
	public FixedLengthStringData getZmmccde() {
		return zmmccde;
	}
	public void setZmmccde(Object what) {
		zmmccde.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getDtetrm() {
		return dtetrm;
	}
	public void setDtetrm(Object what) {
		setDtetrm(what, false);
	}
	public void setDtetrm(Object what, boolean rounded) {
		if (rounded)
			dtetrm.setRounded(what);
		else
			dtetrm.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		zmedcoy.clear();
		zmedprv.clear();
		seqno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		zdocnum.clear();
		zmmccde.clear();
		effdate.clear();
		dtetrm.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}