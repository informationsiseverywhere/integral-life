/*
 * File: Tprmpm7.java
 * Date: 30 August 2009 2:39:48
 * Author: Quipoz Limited
 * 
 * Class transformed from TPRMPM7.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  PREMIUM CALCULATION METHOD 07 -
*  ACCIDENTAL HOSPITALIZATION PREMIUM CALCULATION SUBROUTINE
*
*
* PROCESSING.
* ----------
*
* Initialise all working storage fields and set keys to read
* tables. Include a table (occurs 8) to hold the Options/Extras
* (LEXT) record details.
*
* Build a key.  This key (see below) will read table TT502.
* This table contains the parameters to be used in the
* calculation of the Basic and Additional Premium for
* Coverage/Rider components (0505/0506).
*
* The key is a concatenation of the following fields:-
*
* Coverage/Rider table code
* 0 and 1
*
* Access the required table by reading the dated table directly (ITDM).
* The contents of the table are then stored. This table is read
* 2 times use:
*
*  1) XXXX0 -> Basic Level
*
* To get the Basic sum assured from Risk Unit and Basic Premium amount
* by the combination of Mortality Class and Payment Mode (1,2,3,.. and
* Y, HY, Q, M).
*
*  2) XXXX1 -> Additional Level
*
* To get the increment of sum assured amount and Additional Premium rate
* by the combination of Mortality Class and Payment Mode (1,2,3,.. and
* Y, HY, Q, M).
*
* Validate sum assured amount whether it is greater than basic sum
* assured and in the incremental of additional sum assrued amount or not.
*
* CALCULATE-PREMIUM.
*
* Additional premium = Additional premium rate *
*                      Number of additional sum assured.
* Total premium = Basic premium + Additional premium.
*
*****************************************************************
* </pre>
*/
public class Tprmpm7 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "TPRMPM7";
	private String wsaaBasicPremium = "";
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Premiumrec premiumrec = new Premiumrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	public Tprmpm7() {
		super();
	}

public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		premiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		/*EXIT*/
		exitProgram();
	}

protected void initialize100()
	{
		/*PARA*/
	}
}
