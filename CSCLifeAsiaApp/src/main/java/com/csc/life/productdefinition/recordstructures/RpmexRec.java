package com.csc.life.productdefinition.recordstructures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class RpmexRec extends ExternalData {
	// RpmexRec
	private static final long serialVersionUID = 1L;

	// *******************************
	// Attribute Declarations
	// *******************************
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
	private List<List<String>> covrCoverage;
	private List<List<String>> covrRiderId;
	private String billfreq;
	private List<List<String>> crtable;
	private List<List<String>> mortcls;
	private String currcode;
	private BigDecimal effectdt;
	private List<List<String>> sumin;
	private String ratingdate;
	private String lage;
	private List<String> lsex;
	private List<List<String>> duration;
	private String tpdtype;
	private String cnttype;
	private String language;
	private String premMethod;
	private String rstaflag;
	private List<List<String>> prmbasis;
	private List<List<String>> linkcov;
	private List<String> rstate01;
	private List<List<String>> dob;
	private List<List<String>> covrcd;
	private String chdrnum;
	private List<List<String>> reinsSumins;
	private List<List<String>> reinsPrem;
	private List<List<String>> reinsCommprem;
	private String noOfCoverages;
	private List<List<String>> count;
	private List<List<List<String>>> agerate;
	private List<List<List<String>>> insprm;
	private List<List<List<String>>> opcda;
	private List<List<List<String>>> zmortpct;
	private List<List<List<String>>> oppc;
	private List<List<List<String>>> reasind;
	private List<List<List<String>>> premadj;
	private List<List<List<String>>> ecestrm;
	private List<List<List<String>>> lifeId;
	private List<List<List<String>>> znadjperc;
	private int bilfrmdt;
	private int biltodt;
	private List<List<String>> reinsPremBaseRate;
		private List<List<String>> reinsAnnPrem;

	public List<List<String>> getReinsAnnPrem() {
		return reinsAnnPrem;
	}

	public void setReinsAnnPrem(List<List<String>> reinsAnnPrem) {
		this.reinsAnnPrem = reinsAnnPrem;
	}

	public List<List<String>> getReinsPremBaseRate() {
		return reinsPremBaseRate;
	}

	public void setReinsPremBaseRate(List<List<String>> reinsPremBaseRate) {
		this.reinsPremBaseRate = reinsPremBaseRate;
	}

	public int getBilfrmdt() {
		return bilfrmdt;
	}

	public void setBilfrmdt(int bilfrmdt) {
		this.bilfrmdt = bilfrmdt;
	}

	public int getBiltodt() {
		return biltodt;
	}

	public void setBiltodt(int biltodt) {
		this.biltodt = biltodt;
	}

	public List<List<String>> getCovrCoverage() {
		return covrCoverage;
	}

	public void setCovrCoverage(List<List<String>> covrCoverage) {
		this.covrCoverage = covrCoverage;
	}

	public List<List<String>> getCovrRiderId() {
		return covrRiderId;
	}

	public void setCovrRiderId(List<List<String>> covrRiderId) {
		this.covrRiderId = covrRiderId;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public List<List<String>> getCrtable() {
		return crtable;
	}

	public void setCrtable(List<List<String>> crtable) {
		this.crtable = crtable;
	}

	public List<List<String>> getMortcls() {
		return mortcls;
	}

	public void setMortcls(List<List<String>> mortcls) {
		this.mortcls = mortcls;
	}

	public String getCurrcode() {
		return currcode;
	}

	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}

	public BigDecimal getEffectdt() {
		return effectdt;
	}

	public void setEffectdt(BigDecimal effectdt) {
		this.effectdt = effectdt;
	}

	public List<List<String>> getSumin() {
		return sumin;
	}

	public void setSumin(List<List<String>> sumin) {
		this.sumin = sumin;
	}

	public String getRatingdate() {
		return ratingdate;
	}

	public void setRatingdate(String ratingdate) {
		this.ratingdate = ratingdate;
	}

	public String getLage() {
		return lage;
	}

	public void setLage(String lage) {
		this.lage = lage;
	}

	public List<String> getLsex() {
		return lsex;
	}

	public void setLsex(List<String> lsex) {
		this.lsex = lsex;
	}

	public List<List<String>> getDuration() {
		return duration;
	}

	public void setDuration(List<List<String>> duration) {
		this.duration = duration;
	}

	public String getTpdtype() {
		return tpdtype;
	}

	public void setTpdtype(String tpdtype) {
		this.tpdtype = tpdtype;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPremMethod() {
		return premMethod;
	}

	public void setPremMethod(String premMethod) {
		this.premMethod = premMethod;
	}

	public String getRstaflag() {
		return rstaflag;
	}

	public void setRstaflag(String rstaflag) {
		this.rstaflag = rstaflag;
	}

	public List<List<String>> getPrmbasis() {
		return prmbasis;
	}

	public void setPrmbasis(List<List<String>> prmbasis) {
		this.prmbasis = prmbasis;
	}

	public List<List<String>> getLinkcov() {
		return linkcov;
	}

	public void setLinkcov(List<List<String>> linkcov) {
		this.linkcov = linkcov;
	}

	public List<String> getRstate01() {
		return rstate01;
	}

	public void setRstate01(List<String> rstate01) {
		this.rstate01 = rstate01;
	}

	public List<List<String>> getDob() {
		return dob;
	}

	public void setDob(List<List<String>> dob) {
		this.dob = dob;
	}

	public List<List<String>> getCovrcd() {
		return covrcd;
	}

	public void setCovrcd(List<List<String>> covrcd) {
		this.covrcd = covrcd;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public List<List<String>> getReinsSumins() {
		return reinsSumins;
	}

	public void setReinsSumins(List<List<String>> reinsSumins) {
		this.reinsSumins = reinsSumins;
	}

	public List<List<String>> getReinsPrem() {
		return reinsPrem;
	}

	public void setReinsPrem(List<List<String>> reinsPrem) {
		this.reinsPrem = reinsPrem;
	}

	public List<List<String>> getReinsCommprem() {
		return reinsCommprem;
	}

	public void setReinsCommprem(List<List<String>> reinsCommprem) {
		this.reinsCommprem = reinsCommprem;
	}

	public String getNoOfCoverages() {
		return noOfCoverages;
	}

	public void setNoOfCoverages(String noOfCoverages) {
		this.noOfCoverages = noOfCoverages;
	}

	public List<List<String>> getCount() {
		return count;
	}

	public void setCount(List<List<String>> count) {
		this.count = count;
	}

	public List<List<List<String>>> getAgerate() {
		return agerate;
	}

	public void setAgerate(List<List<List<String>>> agerate) {
		this.agerate = agerate;
	}

	public List<List<List<String>>> getInsprm() {
		return insprm;
	}

	public void setInsprm(List<List<List<String>>> insprm) {
		this.insprm = insprm;
	}

	public List<List<List<String>>> getOpcda() {
		return opcda;
	}

	public void setOpcda(List<List<List<String>>> opcda) {
		this.opcda = opcda;
	}

	public List<List<List<String>>> getZmortpct() {
		return zmortpct;
	}

	public void setZmortpct(List<List<List<String>>> zmortpct) {
		this.zmortpct = zmortpct;
	}

	public List<List<List<String>>> getOppc() {
		return oppc;
	}

	public void setOppc(List<List<List<String>>> oppc) {
		this.oppc = oppc;
	}

	public List<List<List<String>>> getReasind() {
		return reasind;
	}

	public void setReasind(List<List<List<String>>> reasind) {
		this.reasind = reasind;
	}

	public List<List<List<String>>> getPremadj() {
		return premadj;
	}

	public void setPremadj(List<List<List<String>>> premadj) {
		this.premadj = premadj;
	}

	public List<List<List<String>>> getEcestrm() {
		return ecestrm;
	}

	public void setEcestrm(List<List<List<String>>> ecestrm) {
		this.ecestrm = ecestrm;
	}

	public List<List<List<String>>> getLifeId() {
		return lifeId;
	}

	public void setLifeId(List<List<List<String>>> lifeId) {
		this.lifeId = lifeId;
	}

	public List<List<List<String>>> getZnadjperc() {
		return znadjperc;
	}

	public void setZnadjperc(List<List<List<String>>> znadjperc) {
		this.znadjperc = znadjperc;
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

	public void initialise() {
		reinsSumins = new ArrayList<>();
		reinsPrem = new ArrayList<>();
		reinsCommprem = new ArrayList<>();
		reinsPremBaseRate = new ArrayList<>();
		reinsAnnPrem = new ArrayList<>();
	}

	@Override
	public void initialize() {

	}
}