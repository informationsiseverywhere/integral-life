package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for ST579
 * @version 1.0 generated on 30/08/09 07:28
 * @author Quipoz
 */
public class St579ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(489);
	public FixedLengthStringData dataFields = new FixedLengthStringData(137).isAPartOf(dataArea, 0);
	public ZonedDecimalData amta = DD.amta.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData crtables = new FixedLengthStringData(60).isAPartOf(dataFields, 18);
	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(15, 4, crtables, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(crtables, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01 = DD.crtable.copy().isAPartOf(filler,0);
	public FixedLengthStringData crtable02 = DD.crtable.copy().isAPartOf(filler,4);
	public FixedLengthStringData crtable03 = DD.crtable.copy().isAPartOf(filler,8);
	public FixedLengthStringData crtable04 = DD.crtable.copy().isAPartOf(filler,12);
	public FixedLengthStringData crtable05 = DD.crtable.copy().isAPartOf(filler,16);
	public FixedLengthStringData crtable06 = DD.crtable.copy().isAPartOf(filler,20);
	public FixedLengthStringData crtable07 = DD.crtable.copy().isAPartOf(filler,24);
	public FixedLengthStringData crtable08 = DD.crtable.copy().isAPartOf(filler,28);
	public FixedLengthStringData crtable09 = DD.crtable.copy().isAPartOf(filler,32);
	public FixedLengthStringData crtable10 = DD.crtable.copy().isAPartOf(filler,36);
	public FixedLengthStringData crtable11 = DD.crtable.copy().isAPartOf(filler,40);
	public FixedLengthStringData crtable12 = DD.crtable.copy().isAPartOf(filler,44);
	public FixedLengthStringData crtable13 = DD.crtable.copy().isAPartOf(filler,48);
	public FixedLengthStringData crtable14 = DD.crtable.copy().isAPartOf(filler,52);
	public FixedLengthStringData crtable15 = DD.crtable.copy().isAPartOf(filler,56);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,78);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,86);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,94);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(88).isAPartOf(dataArea, 137);
	public FixedLengthStringData amtaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData crtablesErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] crtableErr = FLSArrayPartOfStructure(15, 4, crtablesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(crtablesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData crtable02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData crtable03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData crtable04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData crtable05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData crtable06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData crtable07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData crtable08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData crtable09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData crtable10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData crtable11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData crtable12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData crtable13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData crtable14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData crtable15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 225);
	public FixedLengthStringData[] amtaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData crtablesOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(15, 12, crtablesOut, 0);
	public FixedLengthStringData[][] crtableO = FLSDArrayPartOfArrayStructure(12, 1, crtableOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(180).isAPartOf(crtablesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crtable01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] crtable02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] crtable03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] crtable04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] crtable05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] crtable06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] crtable07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] crtable08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] crtable09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] crtable10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] crtable11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] crtable12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] crtable13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] crtable14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] crtable15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData St579screenWritten = new LongData(0);
	public LongData St579protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public St579ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(crtable01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable13Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable14Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable15Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(amtaOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, crtable01, crtable02, crtable03, crtable04, crtable05, crtable06, crtable07, crtable08, crtable09, crtable10, crtable11, crtable12, crtable13, crtable14, crtable15, amta};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, crtable01Out, crtable02Out, crtable03Out, crtable04Out, crtable05Out, crtable06Out, crtable07Out, crtable08Out, crtable09Out, crtable10Out, crtable11Out, crtable12Out, crtable13Out, crtable14Out, crtable15Out, amtaOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, crtable01Err, crtable02Err, crtable03Err, crtable04Err, crtable05Err, crtable06Err, crtable07Err, crtable08Err, crtable09Err, crtable10Err, crtable11Err, crtable12Err, crtable13Err, crtable14Err, crtable15Err, amtaErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = St579screen.class;
		protectRecord = St579protect.class;
	}

}
