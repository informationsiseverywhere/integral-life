package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR636
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr636ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(290);
	public FixedLengthStringData dataFields = new FixedLengthStringData(98).isAPartOf(dataArea, 0);
	public FixedLengthStringData searchs = new FixedLengthStringData(60).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] search = FLSArrayPartOfStructure(2, 30, searchs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(searchs, 0, FILLER_REDEFINE);
	public FixedLengthStringData search01 = DD.search.copy().isAPartOf(filler,0);
	public FixedLengthStringData search02 = DD.search.copy().isAPartOf(filler,30);
	public FixedLengthStringData sels = new FixedLengthStringData(2).isAPartOf(dataFields, 60);
	public FixedLengthStringData[] sel = FLSArrayPartOfStructure(2, 1, sels, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(sels, 0, FILLER_REDEFINE);
	public FixedLengthStringData sel01 = DD.sel.copy().isAPartOf(filler1,0);
	public FixedLengthStringData sel02 = DD.sel.copy().isAPartOf(filler1,1);
	public FixedLengthStringData sexs = new FixedLengthStringData(2).isAPartOf(dataFields, 62);
	public FixedLengthStringData[] sex = FLSArrayPartOfStructure(2, 1, sexs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(sexs, 0, FILLER_REDEFINE);
	public FixedLengthStringData sex01 = DD.sex.copy().isAPartOf(filler2,0);
	public FixedLengthStringData sex02 = DD.sex.copy().isAPartOf(filler2,1);
	public FixedLengthStringData zaracdes = new FixedLengthStringData(16).isAPartOf(dataFields, 64);
	public FixedLengthStringData[] zaracde = FLSArrayPartOfStructure(2, 8, zaracdes, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(zaracdes, 0, FILLER_REDEFINE);
	public FixedLengthStringData zaracde01 = DD.zaracde.copy().isAPartOf(filler3,0);
	public FixedLengthStringData zaracde02 = DD.zaracde.copy().isAPartOf(filler3,8);
	public FixedLengthStringData zmedstss = new FixedLengthStringData(2).isAPartOf(dataFields, 80);
	public FixedLengthStringData[] zmedsts = FLSArrayPartOfStructure(2, 1, zmedstss, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(zmedstss, 0, FILLER_REDEFINE);
	public FixedLengthStringData zmedsts01 = DD.zmedsts.copy().isAPartOf(filler4,0);
	public FixedLengthStringData zmedsts02 = DD.zmedsts.copy().isAPartOf(filler4,1);
	public FixedLengthStringData zmedtyps = new FixedLengthStringData(16).isAPartOf(dataFields, 82);
	public FixedLengthStringData[] zmedtyp = FLSArrayPartOfStructure(2, 8, zmedtyps, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(16).isAPartOf(zmedtyps, 0, FILLER_REDEFINE);
	public FixedLengthStringData zmedtyp01 = DD.zmedtyp.copy().isAPartOf(filler5,0);
	public FixedLengthStringData zmedtyp02 = DD.zmedtyp.copy().isAPartOf(filler5,8);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 98);
	public FixedLengthStringData searchsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] searchErr = FLSArrayPartOfStructure(2, 4, searchsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(8).isAPartOf(searchsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData search01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData search02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData selsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] selErr = FLSArrayPartOfStructure(2, 4, selsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(selsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sel01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData sel02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData sexsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] sexErr = FLSArrayPartOfStructure(2, 4, sexsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(8).isAPartOf(sexsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sex01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData sex02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData zaracdesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] zaracdeErr = FLSArrayPartOfStructure(2, 4, zaracdesErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(8).isAPartOf(zaracdesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zaracde01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData zaracde02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData zmedstssErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] zmedstsErr = FLSArrayPartOfStructure(2, 4, zmedstssErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(zmedstssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zmedsts01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData zmedsts02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData zmedtypsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] zmedtypErr = FLSArrayPartOfStructure(2, 4, zmedtypsErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(8).isAPartOf(zmedtypsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zmedtyp01Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData zmedtyp02Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 146);
	public FixedLengthStringData searchsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] searchOut = FLSArrayPartOfStructure(2, 12, searchsOut, 0);
	public FixedLengthStringData[][] searchO = FLSDArrayPartOfArrayStructure(12, 1, searchOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(searchsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] search01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] search02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData selsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] selOut = FLSArrayPartOfStructure(2, 12, selsOut, 0);
	public FixedLengthStringData[][] selO = FLSDArrayPartOfArrayStructure(12, 1, selOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(24).isAPartOf(selsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sel01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] sel02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData sexsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(2, 12, sexsOut, 0);
	public FixedLengthStringData[][] sexO = FLSDArrayPartOfArrayStructure(12, 1, sexOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(24).isAPartOf(sexsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sex01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] sex02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData zaracdesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] zaracdeOut = FLSArrayPartOfStructure(2, 12, zaracdesOut, 0);
	public FixedLengthStringData[][] zaracdeO = FLSDArrayPartOfArrayStructure(12, 1, zaracdeOut, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(24).isAPartOf(zaracdesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zaracde01Out = FLSArrayPartOfStructure(12, 1, filler15, 0);
	public FixedLengthStringData[] zaracde02Out = FLSArrayPartOfStructure(12, 1, filler15, 12);
	public FixedLengthStringData zmedstssOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] zmedstsOut = FLSArrayPartOfStructure(2, 12, zmedstssOut, 0);
	public FixedLengthStringData[][] zmedstsO = FLSDArrayPartOfArrayStructure(12, 1, zmedstsOut, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(24).isAPartOf(zmedstssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zmedsts01Out = FLSArrayPartOfStructure(12, 1, filler16, 0);
	public FixedLengthStringData[] zmedsts02Out = FLSArrayPartOfStructure(12, 1, filler16, 12);
	public FixedLengthStringData zmedtypsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] zmedtypOut = FLSArrayPartOfStructure(2, 12, zmedtypsOut, 0);
	public FixedLengthStringData[][] zmedtypO = FLSDArrayPartOfArrayStructure(12, 1, zmedtypOut, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(24).isAPartOf(zmedtypsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zmedtyp01Out = FLSArrayPartOfStructure(12, 1, filler17, 0);
	public FixedLengthStringData[] zmedtyp02Out = FLSArrayPartOfStructure(12, 1, filler17, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(97+DD.cltaddr.length);//pmujavadiya
	public FixedLengthStringData subfileFields = new FixedLengthStringData(31+DD.cltaddr.length).isAPartOf(subfileArea, 0);//pmujavadiya//ILIFE-3222
	public FixedLengthStringData addrss = DD.cltaddr.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData givname = DD.givname.copy().isAPartOf(subfileFields,DD.cltaddr.length);//ILIFE-3222
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,20+DD.cltaddr.length);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(subfileFields,21+DD.cltaddr.length);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 31+DD.cltaddr.length);//ILIFE-3222
	public FixedLengthStringData addrssErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData givnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData zmedprvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 47+DD.cltaddr.length);//ILIFE-3222
	public FixedLengthStringData[] addrssOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] givnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] zmedprvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 125);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr636screensflWritten = new LongData(0);
	public LongData Sr636screenctlWritten = new LongData(0);
	public LongData Sr636screenWritten = new LongData(0);
	public LongData Sr636windowWritten = new LongData(0);
	public LongData Sr636hideWritten = new LongData(0);
	public LongData Sr636protectWritten = new LongData(0);
	public GeneralTable sr636screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr636screensfl;
	}

	public Sr636ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"10","11","-10","11",null, null, null, null, null, null, null, null});
		fieldIndMap.put(sel01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(search01Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zaracde01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sex01Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedsts01Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedtyp01Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, zmedprv, givname, addrss};
		screenSflOutFields = new BaseData[][] {selectOut, zmedprvOut, givnameOut, addrssOut};
		screenSflErrFields = new BaseData[] {selectErr, zmedprvErr, givnameErr, addrssErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {sel02, search02, zaracde02, zmedtyp02, sex02, zmedsts02, sel01, search01, zaracde01, sex01, zmedsts01, zmedtyp01};
		screenOutFields = new BaseData[][] {sel02Out, search02Out, zaracde02Out, zmedtyp02Out, sex02Out, zmedsts02Out, sel01Out, search01Out, zaracde01Out, sex01Out, zmedsts01Out, zmedtyp01Out};
		screenErrFields = new BaseData[] {sel02Err, search02Err, zaracde02Err, zmedtyp02Err, sex02Err, zmedsts02Err, sel01Err, search01Err, zaracde01Err, sex01Err, zmedsts01Err, zmedtyp01Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr636screen.class;
		screenSflRecord = Sr636screensfl.class;
		screenCtlRecord = Sr636screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr636protect.class;
		hideRecord = Sr636hide.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr636screenctl.lrec.pageSubfile);
	}
}
