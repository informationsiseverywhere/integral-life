package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:14
 * Description:
 * Copybook name: TT578REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt578rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt578Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData sd = new ZonedDecimalData(7, 2).isAPartOf(tt578Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(493).isAPartOf(tt578Rec, 7, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tt578Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt578Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}