package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:52
 * Description:
 * Copybook name: T5674REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5674rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5674Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData commsubr = new FixedLengthStringData(7).isAPartOf(t5674Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(493).isAPartOf(t5674Rec, 7, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5674Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5674Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}