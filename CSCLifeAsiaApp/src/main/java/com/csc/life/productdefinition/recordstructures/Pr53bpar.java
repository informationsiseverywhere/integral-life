package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:07
 * Description:
 * Copybook name: PR53BPAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pr53bpar extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(453);
  	public FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData cnttyp = new FixedLengthStringData(3).isAPartOf(parmRecord, 30);
  	public FixedLengthStringData contractType = new FixedLengthStringData(3).isAPartOf(parmRecord, 33);
  	public FixedLengthStringData crcodes = new FixedLengthStringData(40).isAPartOf(parmRecord, 36);
  	public FixedLengthStringData[] crcode = FLSArrayPartOfStructure(10, 4, crcodes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(crcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crcode01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData crcode02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData crcode03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData crcode04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData crcode05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData crcode06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData crcode07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData crcode08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData crcode09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData crcode10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData crtableds = new FixedLengthStringData(300).isAPartOf(parmRecord, 76);
  	public FixedLengthStringData[] crtabled = FLSArrayPartOfStructure(10, 30, crtableds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(300).isAPartOf(crtableds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtabled01 = new FixedLengthStringData(30).isAPartOf(filler1, 0);
  	public FixedLengthStringData crtabled02 = new FixedLengthStringData(30).isAPartOf(filler1, 30);
  	public FixedLengthStringData crtabled03 = new FixedLengthStringData(30).isAPartOf(filler1, 60);
  	public FixedLengthStringData crtabled04 = new FixedLengthStringData(30).isAPartOf(filler1, 90);
  	public FixedLengthStringData crtabled05 = new FixedLengthStringData(30).isAPartOf(filler1, 120);
  	public FixedLengthStringData crtabled06 = new FixedLengthStringData(30).isAPartOf(filler1, 150);
  	public FixedLengthStringData crtabled07 = new FixedLengthStringData(30).isAPartOf(filler1, 180);
  	public FixedLengthStringData crtabled08 = new FixedLengthStringData(30).isAPartOf(filler1, 210);
  	public FixedLengthStringData crtabled09 = new FixedLengthStringData(30).isAPartOf(filler1, 240);
  	public FixedLengthStringData crtabled10 = new FixedLengthStringData(30).isAPartOf(filler1, 270);
  	public FixedLengthStringData crtables = new FixedLengthStringData(40).isAPartOf(parmRecord, 376);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(10, 4, crtables, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
  	public FixedLengthStringData ctypdesc = new FixedLengthStringData(30).isAPartOf(parmRecord, 416);
  	public FixedLengthStringData extrval = new FixedLengthStringData(1).isAPartOf(parmRecord, 446);
  	public FixedLengthStringData worku = new FixedLengthStringData(6).isAPartOf(parmRecord, 447);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}