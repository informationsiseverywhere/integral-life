package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZmpfpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:59
 * Class transformed from ZMPFPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZmpfpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 76;
	public FixedLengthStringData zmpfrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zmpfpfRecord = zmpfrec;
	
	public FixedLengthStringData zmedcoy = DD.zmedcoy.copy().isAPartOf(zmpfrec);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(zmpfrec);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(zmpfrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(zmpfrec);
	public PackedDecimalData zmedfee = DD.zmedfee.copy().isAPartOf(zmpfrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zmpfrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zmpfrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zmpfrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZmpfpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZmpfpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZmpfpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZmpfpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpfpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZmpfpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpfpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZMPFPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ZMEDCOY, " +
							"ZMEDPRV, " +
							"ZMEDTYP, " +
							"SEQNO, " +
							"ZMEDFEE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     zmedcoy,
                                     zmedprv,
                                     zmedtyp,
                                     seqno,
                                     zmedfee,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		zmedcoy.clear();
  		zmedprv.clear();
  		zmedtyp.clear();
  		seqno.clear();
  		zmedfee.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZmpfrec() {
  		return zmpfrec;
	}

	public FixedLengthStringData getZmpfpfRecord() {
  		return zmpfpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZmpfrec(what);
	}

	public void setZmpfrec(Object what) {
  		this.zmpfrec.set(what);
	}

	public void setZmpfpfRecord(Object what) {
  		this.zmpfpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zmpfrec.getLength());
		result.set(zmpfrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}