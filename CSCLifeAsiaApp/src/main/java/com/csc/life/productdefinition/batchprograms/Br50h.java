/*
 * File: Br50h.java
 * Date: 29 August 2009 22:11:49
 * Author: Quipoz Limited
 *
 * Class transformed from BR50H.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap; //ILIFE-8901
import java.util.Iterator;
import java.util.LinkedHashSet; //ILIFE-8901
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set; //ILIFE-8901

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.dao.RtrnpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.CrtundwrtUtil;
import com.csc.life.enquiries.procedures.CrtundwrtUtilImpl;
import com.csc.life.enquiries.procedures.Uwlmtchk;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.enquiries.recordstructures.Uwlmtrec;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.ResnpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Resnpf;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.NterpfTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.Br50hTempDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MliapfDAO; //ILIFE-8901
import com.csc.life.productdefinition.dataaccess.model.Br50hDTO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mliapf; //ILIFE-8901
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.BatcupPojo;
import com.csc.smart.procedures.BatcupUtils;
import com.csc.smart.procedures.SftlockUtil; //ILIFE-8901
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.SftlockRecBean; //ILIFE-8901
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Rtrnpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   The batch will auto triggeres the change in contract
*   risk status from "PS" (Proposal) or "UW" (Underwrite)
*   "NT" (Not taken) upon full premium not received and
*   pending follow up (non-financial) not received within
*   holding period. This is automatically sent to the NTU
*   process after the expiry date.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*    Read
*     - read first primary file record.
*
*    Perform    Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock the record if it is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  No of Records Extracted
*     02  -  No of NTU Processong
*     03  -  No of records with errors
*     04  -  No of without Coverages
*     05  -  No of Not Valid Contract
*     06  -  No of Contracts has not exceed time limit
*     07  -  No of Contracts with no O/S following and
*            there is enough $ to cover the premium
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Br50h extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private SortFileDAM sortFile = new SortFileDAM("BR50HSRT");
	private NterpfTableDAM nterpf = new NterpfTableDAM();
	private NterpfTableDAM nterpfRec = new NterpfTableDAM();

	private FixedLengthStringData sortFileRecord = new FixedLengthStringData(119);
	private PackedDecimalData sortHprrcvdt = new PackedDecimalData(8, 0).isAPartOf(sortFileRecord, 0);
	private PackedDecimalData sortTdayno = new PackedDecimalData(3, 0).isAPartOf(sortFileRecord, 5);
	private FixedLengthStringData sortChdrcoy = new FixedLengthStringData(1).isAPartOf(sortFileRecord, 7);
	private FixedLengthStringData sortChdrnum = new FixedLengthStringData(8).isAPartOf(sortFileRecord, 8);
	private FixedLengthStringData sortErorprog = new FixedLengthStringData(10).isAPartOf(sortFileRecord, 16);
	private FixedLengthStringData sortFlddesc = new FixedLengthStringData(30).isAPartOf(sortFileRecord, 26);
	private FixedLengthStringData sortEror = new FixedLengthStringData(4).isAPartOf(sortFileRecord, 56);
	private FixedLengthStringData sortDesc = new FixedLengthStringData(50).isAPartOf(sortFileRecord, 60);
	private PackedDecimalData sortInstprem = new PackedDecimalData(17, 2).isAPartOf(sortFileRecord, 110);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR50H");
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaToFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaToFilename = new FixedLengthStringData(4).isAPartOf(wsaaToFile, 0);
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaToFile, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaToFile, 6).setUnsigned();
	//private PackedDecimalData wsaaTotamnt = new PackedDecimalData(13, 2);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5679Ix = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFlupOustDate = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaFeesAmt = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaNofday = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCovt = new FixedLengthStringData(1);
	private Validator wsaaCovtFound = new Validator(wsaaValidCovt, "Y");
	private Validator wsaaCovtNotFound = new Validator(wsaaValidCovt, "N");

	private FixedLengthStringData wsaaValidFlup = new FixedLengthStringData(1);
	private Validator wsaaCompleteFlup = new Validator(wsaaValidFlup, "Y");
	private Validator wsaaOutstandFlup = new Validator(wsaaValidFlup, "N");

	private FixedLengthStringData wsaaFinancialFlag = new FixedLengthStringData(1);
	private Validator wsaaFinancial = new Validator(wsaaFinancialFlag, "Y");
	private Validator wsaaNotFinancial = new Validator(wsaaFinancialFlag, "N");

	private FixedLengthStringData wsaaErFields = new FixedLengthStringData(103);
	private FixedLengthStringData wsaaErChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaErFields, 0);
	private FixedLengthStringData wsaaErChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaErFields, 1);
	private FixedLengthStringData wsaaErErorprog = new FixedLengthStringData(10).isAPartOf(wsaaErFields, 9);
	private FixedLengthStringData wsaaErFlddesc = new FixedLengthStringData(30).isAPartOf(wsaaErFields, 19);
	private FixedLengthStringData wsaaErEror = new FixedLengthStringData(4).isAPartOf(wsaaErFields, 49);
	private FixedLengthStringData wsaaErDesc = new FixedLengthStringData(50).isAPartOf(wsaaErFields, 53);
		/* WSAA-CONSTANT */
	private static final String wsaaNtuf = "NTUF";
	private static final String wsaaNtup = "NTUP";
	private static final String wsaaMesg1 = "No O/S Reminder, No Underwrite";
	private static final String wsaaMesg2 = "Medical Fees Exists           ";
	private static final String wsaaMesg3 = "Exceed authorized limit       ";
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTotPrem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaCntfee = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaJlifeNum = new FixedLengthStringData(2).isAPartOf(wsaaJlife, 0, REDEFINE);
	private ZonedDecimalData wsaaJlifeN = new ZonedDecimalData(2, 0).isAPartOf(wsaaJlifeNum, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);
		/*paras for calculation*/
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPayrSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmntDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotTolerance2 = new PackedDecimalData(17, 2);
	private String wsaaSuspInd = "";
	private ZonedDecimalData wsaaAmountLimit = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAmountLimit2 = new ZonedDecimalData(17, 2);

	private PackedDecimalData wsaaFactor = new PackedDecimalData(11, 5);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsbbSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaToleranceApp = new ZonedDecimalData(4, 2);
	private ZonedDecimalData wsaaToleranceApp2 = new ZonedDecimalData(4, 2);
	private String wsaaSingPrmInd = "N";
	private PackedDecimalData wsaaSingpFee = new PackedDecimalData(17, 2);

		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private int ctCount1=0;
	private int ctCount2=0;
	private int ctCount3=0;
	private int ctCount4=0;
	private int ctCount5=0;
	private int ctCount6=0;
	private int ctCount7=0;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private BscdTableDAM bscdIO = new BscdTableDAM();

	private Br50hTempDAO br50hTempDao = getApplicationContext().getBean("br50hTempDAO",Br50hTempDAO.class);
	private Br50hDTO br50hDto = new Br50hDTO();
	private List<Br50hDTO> br50hList = new ArrayList<Br50hDTO>();
	private Iterator<Br50hDTO> br50hItr;
	 
	private CovtpfDAO covtDao = getApplicationContext().getBean("covtpfDAO",CovtpfDAO.class);
	private Acblpf acblpf = new Acblpf();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private FluppfDAO flupDao = getApplicationContext().getBean("fluppfDAO",FluppfDAO.class);
	private RtrnpfDAO rtrnsacDao = getApplicationContext().getBean("rtrnpfDAO",RtrnpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
//	private MliaafiTableDAM mliaafiIO = new MliaafiTableDAM();
//	private MliachdTableDAM mliachdIO = new MliachdTableDAM();

	private Batckey wsaaBatckey = new Batckey();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private T5679rec t5679rec = new T5679rec();
	private Th506rec th506rec = new Th506rec();
	private T5645rec t5645rec = new T5645rec();
	private T5661rec t5661rec = new T5661rec();
	private T3695rec t3695rec = new T3695rec();
	private T5667rec t5667rec = new T5667rec();
	private T5674rec t5674rec = new T5674rec();
	private T5567rec t5567rec = new T5567rec();
	private T5688rec t5688rec = new T5688rec();
	private T6687rec t6687rec = new T6687rec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Prasrec prasrec = new Prasrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Uwlmtrec uwlmtrec = new Uwlmtrec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaBillingInformationInner wsaaBillingInformationInner = new WsaaBillingInformationInner();
	private Map<String, List<Itempf>> t5661Map;
	private Map<String, List<Itempf>> th506Map;
	private Map<String, List<Itempf>> th623Map;
	private Map<String, List<Itempf>> t5667Map;
	private Map<String, List<Itempf>> t5688Map;
	private Map<String, List<Itempf>> t6687Map;
	private Map<String, List<Itempf>> t5674Map;
	private Map<String, List<Itempf>> t5567Map;
	private Map<String, List<Itempf>> tr52dMap;
 //ILIFE-8901 start
	private Map<String, List<Itempf>> tr52eListMap;
	private Map<String, List<Lifepf>> lifelnbMap  = new HashMap<String,  List<Lifepf>>();
	private Map<String, List<Fluppf>> fluppfMap = new HashMap<String,  List<Fluppf>>();
	private Map<String, List<Mliapf>> mliaMap = new HashMap<String,  List<Mliapf>>();
	private Lifepf lifepf; 
	
 //ILIFE-8901 end
	
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private String wsaaT5645sacsCode02Rlpdlon;
	private String wsaaT5645sacsType02Rlpdlon;
	private String wsaaT5645sacsCode05Rlpdlon;
	private String wsaaT5645sacsType05Rlpdlon;
	private FixedLengthStringData wsaaTh623Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh623Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTh623Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(5).isAPartOf(wsaaTh623Key, 3, FILLER).init(SPACES);
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private FixedLengthStringData wsaaRldgXtra = new FixedLengthStringData(4).isAPartOf(wsaaRldgacct, 10);
	private BigDecimal wsaaSacscurbal;
	private boolean wsaaT3695flag = false;
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 3);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	
	private int intBatchID=0;
	private int intBatchExtractSize;	
	private int intBatchStep=0;
	private String strCompany;
	private Rtrnpf rtrnsac = null;
	private List<Ptrnpf> ptrnBulkInsList = new ArrayList<Ptrnpf>();

	private FixedLengthStringData br50hDtoChdrnum = new FixedLengthStringData(16);
	private Txcalcrec txcalcrec = new Txcalcrec();
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private Tr52drec tr52drec = new Tr52drec();
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private TablesInner tablesInner = new TablesInner();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5687rec t5687rec = new T5687rec();
	private Tr52erec tr52erec = new Tr52erec();
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCcy = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private PackedDecimalData wsaaTotalTaxedPremium = new PackedDecimalData(17, 2);
	private DescTableDAM descIO = new DescTableDAM();
	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	//ILIFE-8901 start
	private Itempf itempf = null;
	private static final String t5679 = "T5679";
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private MliapfDAO mliapfDAO = getApplicationContext().getBean("mliapfDAO", MliapfDAO.class);
	private SftlockRecBean sftlockRecBean = null;
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private CrtundwrtUtil crtundwrtUtil = new CrtundwrtUtilImpl();
	private BatcupUtils batcupUtils = getApplicationContext().getBean("batcupUtils", BatcupUtils.class);
	private BatcupPojo batcupPojo = null;
	private List<Mliapf> mliaBulkupdateList = null;
	private List<Long> mliaDelList = new ArrayList<Long>();
	
	//ILIFE-8901 end

 	//ILB-947 start
	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<String,  List<Chdrpf>>();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Map<String, List<Covtpf>> covtpfMap = new HashMap<String,  List<Covtpf>>();
	private Covtpf covtIO = new Covtpf();
	protected Chdrpf chdrlnbIO = new Chdrpf();
	protected List<Chdrpf> chdrBulkUpdtList = new ArrayList<Chdrpf>();
	protected static final Logger LOGGER = LoggerFactory.getLogger(Br50h.class);
	private ResnpfDAO resnpfDAO = getApplicationContext().getBean("resnpfDAO", ResnpfDAO.class);
	//ILB-947 end
	
	private boolean NBPRP125Permission = false;
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		exit2590,
		exit2629,
		readseqCovtlnb2671,
		callFlupio2802,
		exit2809,
		nextLife3826,
		exit3829,
		gotItem5000,
		m200Exit
	}

	public Br50h() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspcomn.edterror;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

	protected void initialise1000() {
		otherInit1040();
		strCompany = bsprIO.getCompany().toString();
		intBatchID = 0;
		intBatchStep = 0;
		
		NBPRP125Permission = FeaConfg.isFeatureExist(strCompany, "NBPRP125", appVars,"IT");
		
		br50hList = new ArrayList<Br50hDTO>();		
		//br50hList = br50hTempDao.getNTURecords();
		//br50hItr = br50hList.listIterator(); 
		
		if (bprdIO.systemParam01.isNumeric())
			if (bprdIO.systemParam01.toInt() > 0)
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			else
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
		
		int extractRows = br50hTempDao.getNTURecords(strCompany, intBatchExtractSize);
		
		if (extractRows > 0)
		{
			performDataChunk();	
			if (!br50hList.isEmpty())
			{		
				 initialise1010();
			}			
		}
}

protected void performDataChunk(){
	intBatchID = intBatchStep;		
	if (!br50hList.isEmpty())
	{
		br50hList.clear();
	}
	 
	br50hList = br50hTempDao.loadDataByBatch(intBatchID);
	if (br50hList.isEmpty())	
		wsspcomn.edterror.set(varcom.endp);
	else {
		br50hItr = br50hList.iterator();	
 //ILIFE-8901 start
            Set<String> chdrnumSet = new LinkedHashSet<>();
            for (Br50hDTO c : br50hList) {
                chdrnumSet.add(c.getChdrnum());
            }
            List<String> chdrList = new ArrayList<>(chdrnumSet);
            lifelnbMap = lifepfDAO.searchLifelnbRecord(strCompany,chdrList);
            fluppfMap = flupDao.searchFlupRecord(strCompany,chdrList);
            mliaMap = mliapfDAO.selectMliaRecord(chdrList);
            chdrpfMap = chdrpfDAO.searchChdr(strCompany, chdrList); //ILB-947
            covtpfMap = covtDao.searchCovt(strCompany, chdrList); //ILB-947
            
		}
 //ILIFE-8901 end
}
protected void initialise1010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*----Override NTERPF*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaToFilename.set("NTER");
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(NTERPF) ");
		stringVariable1.addExpression("TOFILE(*LIBL/");
		stringVariable1.addExpression(wsaaToFile);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(*FIRST)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		nterpf.openOutput();
		bscdIO.setScheduleName(bsprIO.getScheduleName());
		bscdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			syserrrec.statuz.set(bscdIO.getStatuz());
			fatalError600();
		}
		initSmartTables();
		readT56791100();
		readT56451120();
		readT36951130();
		//ILIFE-8901 start
}
protected void initSmartTables() {
	try{
		t5661Map = itemDao.loadSmartTable("IT", strCompany, "T5661");
		th506Map = itemDao.loadSmartTable("IT", strCompany, "TH506");
		th623Map = itemDao.loadSmartTable("IT", strCompany, "TH623");
		t5667Map = itemDao.loadSmartTable("IT", strCompany, "T5667");
		t5688Map = itemDao.loadSmartTable("IT", strCompany, "T5688");
		t6687Map = itemDao.loadSmartTable("IT", strCompany, "T6687");
		t5674Map = itemDao.loadSmartTable("IT", strCompany, "T5674");
		t5567Map = itemDao.loadSmartTable("IT", strCompany, "T5567");
		tr52eListMap = itemDao.loadSmartTable("IT", strCompany, "TR52E"); //ILIFE-8901
		tr52dMap = itemDao.loadSmartTable("IT", strCompany, "TR52D");

	}catch(Exception ex){
		
	}
}
protected void otherInit1040()
	{
	wsaaErFields.set(SPACES);
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
protected void readT56791100(){
	//ILIFE-8901 start
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(strCompany);
	itempf.setItemtabl(t5679);
	itempf.setItemitem(bprdIO.getAuthCode().toString().trim());
	itempf = itempfDAO.getItemRecordByItemkey(itempf);
	if (itempf==null) {
		syserrrec.statuz.set("MRNF");
		syserrrec.params.set(t5679.concat(bprdIO.getAuthCode().toString()));
		fatalError600();
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	//ILIFE-8901 end
}

protected void readT36951130() {
	//san
	//ILIFE-8901 start
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(strCompany);
	itempf.setItemtabl(t3695);
	itempf.setItemitem(t5645rec.sacstype01.toString());
	itempf = itempfDAO.getItemRecordByItemkey(itempf);
	
	if(itempf!=null) {
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaT3695flag = true;
	}
	else {
		wsaaT3695flag=false;
	}
	//ILIFE-8901 end
	}
protected void readT56451120(){
	//ILIFE-8901 start
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl(t5645);
	itempf.setItemitem("RLPDLON");
	itempf = itempfDAO.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		syserrrec.params.set("IT".concat(bsprIO.getCompany().toString()).concat(t5645));
		fatalError600();
	}
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	wsaaT5645sacsCode02Rlpdlon = t5645rec.sacscode02.toString();
	wsaaT5645sacsType02Rlpdlon = t5645rec.sacstype02.toString();
	wsaaT5645sacsCode05Rlpdlon = t5645rec.sacscode05.toString();
	wsaaT5645sacsType05Rlpdlon = t5645rec.sacstype05.toString();
    itempf.setItemitem(wsaaProg.toString());
    itempf = itempfDAO.getItemRecordByItemkey(itempf);
    if(itempf==null) {
    	fatalError600();
    }
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	//ILIFE-8901 end
	
}

protected void readFile2000(){
	if (!br50hList.isEmpty()){	
		if (!br50hItr.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!br50hList.isEmpty()){			
				br50hDto=new Br50hDTO();
				br50hDto = br50hItr.next();
				 		
			}
		}else {		
			br50hDto = new Br50hDTO();
			br50hDto = br50hItr.next();
			 
		}	
	}else{
		wsspcomn.edterror.set(varcom.endp);
	}
}


protected void chekCovt2100()
	{
		 //ILB-947 start
		if(br50hDto.getCovtflag().equals("Y") || br50hDto.getCovtflag().trim().equals("Y")) {
			wsaaCovtFound.setTrue();
		}
		else {
			wsaaCovtNotFound.setTrue();
		}
 		//ILB-947 end
	}

protected void checkAuthority2200(){
	/*START*/
	uwlmtrec.function.set("CHCK");
	uwlmtrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
	uwlmtrec.chdrnum.set(chdrlnbIO.getChdrnum());
	uwlmtrec.cnttyp.set(chdrlnbIO.getCnttype());
	uwlmtrec.cownpfx.set(chdrlnbIO.getCownpfx());
	uwlmtrec.lifcnum.set(lifepf.getLifcnum()); //ILIFE-8901
	uwlmtrec.language.set(bsscIO.getLanguage());
	uwlmtrec.fsuco.set(bsprIO.getFsuco());	
	uwlmtrec.userid.set(bsscIO.getUserName());
	callProgram(Uwlmtchk.class, uwlmtrec.rec);
	/*EXIT*/
}

protected void edit2500()
	{
		 try {
			edit2510();
			readAcbl2550();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void edit2510()
	{
	/*  Check record is required for processing.*/
	/*  Softlock the record if it is to be updated.*/
	
	wsspcomn.edterror.set(varcom.oK);
	ctCount1++;
	checkStatuz2600();
	if (!validContract.isTrue()) {
		ctCount5++;
		wsspcomn.edterror.set(SPACES);
		goTo(GotoLabel.exit2590);
	}
	
	 
	wsaaCovtFound.setTrue();
	chekCovt2100();
	if (wsaaCovtNotFound.isTrue()) {
		ctCount4++;
		wsspcomn.edterror.set(SPACES);
		goTo(GotoLabel.exit2590);
	}
	/*  Get the Total Premium Required and the Premium Suspense*/
	readHoldChdr2630();
	calculate2620();
	if (isLTE(wsaaTotalTaxedPremium, wsaaTotalSuspense)) {
		wsspcomn.edterror.set(SPACES);
		goTo(GotoLabel.exit2590);
	}
	checkAuthority2200();
	if (isNE(uwlmtrec.statuz,varcom.oK)) {
		wsaaErErorprog.set(wsaaProg);
		wsaaErFlddesc.set(SPACES);
		wsaaErEror.set(SPACES);
		wsaaErDesc.set(wsaaMesg3);
		writeEr5000();
		wsspcomn.edterror.set(SPACES);
		goTo(GotoLabel.exit2590);
	}
	readTh5062700();
	wsaaFlupOustDate.set(ZERO);
	wsaaNofday.set(ZERO);
	readFlup2800();
	/*  Bypass the case, if there was not O/S following and*/
	/*  there is enough $ to cover the premium.*/
	if ((isEQ(wsaaFlupOustDate, ZERO)
	|| isEQ(wsaaFlupOustDate, 99999999)) && isLTE(wsaaTotalTaxedPremium, wsaaTotalSuspense)) {
		ctCount7++;
		wsspcomn.edterror.set(SPACES);
		goTo(GotoLabel.exit2590);
	}
	// HPROPDTE code: BR6W
	if (NBPRP125Permission) {
		if (isEQ(wsaaFlupOustDate, ZERO)
			|| isEQ(wsaaFlupOustDate, 99999999)) {
			wsaaFlupOustDate.set(br50hDto.getDecldate());
		}
	}
	if (isEQ(wsaaFlupOustDate, ZERO)
	|| isEQ(wsaaFlupOustDate, 99999999)) {
		wsaaFlupOustDate.set(br50hDto.getHuwdcdte());
	}
	calcNofday2910();
	/*if (isEQ(wsaaFlupOustDate, ZERO)
	|| isEQ(wsaaFlupOustDate, 99999999)) {
					wsaaErErorprog.set(wsaaProg);
					wsaaErFlddesc.set(SPACES);
					wsaaErEror.set(SPACES);
					wsaaErDesc.set(wsaaMesg1);
					writeEr5000();
					goTo(GotoLabel.exit2590);
				}*/
	
	
	if (isEQ(wsaaFlupOustDate, ZERO)
			|| isEQ(wsaaFlupOustDate, 99999999)) {
		wsaaFlupOustDate.set(br50hDto.getHprrcvdt());
	}

	datcon3rec.intDate1.set(wsaaFlupOustDate);
	datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
	datcon3rec.frequency.set("DY");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	if (isGTE(datcon3rec.freqFactor, th506rec.expdays)) {
		/*NEXT_SENTENCE*/
	}
	else {
		ctCount6++;
		wsspcomn.edterror.set(SPACES);
		goTo(GotoLabel.exit2590);
	}
}

protected void readAcbl2550(){
		wsaaFeesAmt.set(ZERO);
		acblpf = null;
		br50hDtoChdrnum.set(br50hDto.getChdrnum());
		acblpf = acblDao.getAcblpfRecord(br50hDto.getChdrcoy(), t5645rec.sacscode02.toString(), br50hDtoChdrnum.toString(), t5645rec.sacstype02.toString().trim(), br50hDto.getCntcurr()); 
		/*	ILIFE-6963	readAcbl checks if any medical bills exist, so sacscode01 & sacstype01 are for LP/S while sacscode02 & sacstype02 are for LP/ME	*/
		if(acblpf!=null) { 
			wsaaFeesAmt.set(acblpf.getSacscurbal());
		}
		
		if (isNE(wsaaFeesAmt,ZERO)) {
			wsaaErErorprog.set(wsaaProg);
			wsaaErFlddesc.set(SPACES);
			wsaaErEror.set(SPACES);
			wsaaErDesc.set(wsaaMesg2);
			writeEr5000();
		}
}

protected void checkStatuz2600()
	{
		/*START*/
		/* Validate the new contract status against T5679.*/
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			//if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()],chdrzprIO.getStatcode())) {
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()],br50hDto.getStatcode())) {
				wsaaValidContract.set("Y");
			}
		}
		/*EXIT*/
	}

protected void calculate2620(){
	try {
		start2620();
		getSign2622();
		calcContractFee2623();
		calculatePremium2625();
		calculateApa2624();
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
	}
}

protected void start2620()
	{
		wsaaTotalPremium.set(0);
		wsaaTotalSuspense.set(0);
		wsaaTotalTaxedPremium.set(ZERO);
		wsaaSingPrmInd = "N";
		wsaaSingpFee.set(ZERO);
		/*    MOVE ZERO                   TO WSAA-TOTLPRM.*/
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub,9))) {
			wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(SPACES);
			wsbbSub.add(1);
		}

		wsaaTotalSuspense.set(ZERO);
		wsaaTotalPremium.set(ZERO);
		wsaaCntfee.set(ZERO);
		
		t5688rec.polmin.set(ZERO);
		
		loadPayerDetails2a00();
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
 //ILIFE-8901 start		
		boolean foundFlag = false;
		if (lifelnbMap != null && lifelnbMap.containsKey(br50hDto.getChdrnum())) {
			 List<Lifepf> lifepfList = lifelnbMap.get(br50hDto.getChdrnum());
			 for (Iterator<Lifepf> iterator = lifepfList.iterator(); iterator.hasNext(); ){
				 Lifepf l = iterator.next();
				if (strCompany.equals(l.getChdrcoy())) {
					 lifepf = l;
					 foundFlag = true;
	                 break;
				}
			}

		}
		 if (!foundFlag) {
             syserrrec.params.set(chdrlnbIO.getChdrnum());
             fatalError600();
         }
		 
		
 //ILIFE-8901 end
		readContractTable2680();
		/*  Read financial accounting rules*/
		wsaaBatckey.set(batcdorrec.batchkey);
		
	}

protected void getSign2622()
	{
		if(!wsaaT3695flag) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2629);
		}
	}

	/**
	* <pre>
	*  Calculate amounts
	* </pre>
	*/
protected void calcContractFee2623()
	{
		/*    Calculate the contract fee by using the subroutine found in*/
		/*    table T5674.*/
		if (isNE(t5688rec.feemeth,SPACES)) {
			calcFee2650();
		}
		else {
			wsaaCntfee.set(ZERO);
		}
		if (isNE(wsspcomn.edterror,SPACES)
		&& isNE(wsspcomn.edterror,varcom.oK)) {
			goTo(GotoLabel.exit2629);
		}
	}

protected void calculatePremium2625()
	{
		
		calcPremium2670();
		/* Check if a Suspense payment has been made in the Contract*/
		/* Currency, Billing Currency or any currency.*/
		wsaaCntSuspense.set(ZERO);
		wsaaSuspInd = "N";
		checkSuspense2aa00();
	}

	/**
	* <pre>
	* Look up Advance Premium Deposit available.
	* </pre>
	*/
protected void calculateApa2624()
	{
 		checkApa2ab00();
		if (wsaaSacscurbal.floatValue()>0) {
			wsaaSuspInd = "Y";
		}
		wsaaT5667Trancd.set(bprdIO.getSystemParam02());
		if (isEQ(wsaaSuspInd,"Y")) {
			//wsaaT5667Curr.set(acblenqIO.getOrigcurr());
			wsaaT5667Curr.set(acblpf.getOrigcurr());
		}
		else {
			wsaaT5667Curr.set(SPACES);
		}
		
		if(t5667Map.containsKey(wsaaT5667Key.toString())) {
			List<Itempf> t5667List = t5667Map.get(wsaaT5667Key.toString());
			Itempf t5667Item = t5667List.get(0);
			t5667rec.t5667Rec.set(StringUtil.rawToString(t5667Item.getGenarea()));
		}
		else if(t5667Map.containsKey(wsaaT5667Key.toString().trim())) {
			List<Itempf> t5667List = t5667Map.get(wsaaT5667Key.toString().trim());
			Itempf t5667Item = t5667List.get(0);
			t5667rec.t5667Rec.set(StringUtil.rawToString(t5667Item.getGenarea()));
		}
		else {
			wsaaToleranceApp.set(ZERO);
			wsaaAmountLimit.set(ZERO);
			wsaaToleranceApp2.set(ZERO);
			wsaaAmountLimit2.set(ZERO);
		}
		/* Read the RTRN file to see if a cash receipt has been*/
		/* created for this contract.*/
		rtrnsac = rtrnsacDao.getRtrnsacRecords(strCompany, chdrlnbIO.getChdrnum(), isEQ(wsaaSuspInd,"Y")?acblpf.getOrigcurr():SPACES.toString(), /* IJTI-1523 */
				t5645rec.sacscode01.toString(), t5645rec.sacstype01.toString());
		if(rtrnsac == null){
			rtrnsac = new Rtrnpf();//IJTI-320
			rtrnsac.setEffdate(Integer.parseInt(varcom.vrcmMaxDate.toString()));
		}
		
		if(t5688Map.containsKey(chdrlnbIO.getCnttype())) {
			List<Itempf> t5688List = t5688Map.get(chdrlnbIO.getCnttype());
            for(Itempf t5688Item: t5688List) {
                if(Integer.parseInt(t5688Item.getItmfrm().toString())<=chdrlnbIO.getOccdate() 
                && Integer.parseInt(t5688Item.getItmto().toString())>=chdrlnbIO.getOccdate()) {
                	t5688rec.t5688Rec.set(StringUtil.rawToString(t5688Item.getGenarea()));
                	break;
                }
            }
		}
		else if(t5688Map.containsKey(chdrlnbIO.getCnttype().trim())) {
			List<Itempf> t5688List = t5688Map.get(chdrlnbIO.getCnttype().trim());
            for(Itempf t5688Item: t5688List) {
            	if(Integer.parseInt(t5688Item.getItmfrm().toString())<=chdrlnbIO.getOccdate() 
                && Integer.parseInt(t5688Item.getItmto().toString())>=chdrlnbIO.getOccdate()) {
                	t5688rec.t5688Rec.set(StringUtil.rawToString(t5688Item.getGenarea()));
                	break;
                }
            }
		}
		
		if(t6687Map.containsKey(t5688rec.taxrelmth.toString())) {
			List<Itempf> t6687List = t6687Map.get(t5688rec.taxrelmth.toString());
            Itempf t6687Item = t6687List.get(0);
            t6687rec.t6687Rec.set(StringUtil.rawToString(t6687Item.getGenarea()));
		}
		else if(t6687Map.containsKey(t5688rec.taxrelmth.toString().trim())) {
			List<Itempf> t6687List = t6687Map.get(t5688rec.taxrelmth.toString().trim());
            Itempf t6687Item = t6687List.get(0);
            t6687rec.t6687Rec.set(StringUtil.rawToString(t6687Item.getGenarea()));
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Adjust the regular premiums (if not zero) by the number*/
		/* of frequencies required prior to issue.*/
		/* If the contract has a tax relief method deduct the tax*/
		/* relief amount from the amount due.*/
		/* Also check if there is enough money in suspense to issue*/
		/* this contract.*/
		wsaaTotTolerance.set(0);
		wsaaTotTolerance2.set(0);
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub,9)
		|| isEQ(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()], SPACES))) {
			adjustPremium2b00();
		}

		compute(wsaaTotalPremium, 2).set(add(wsaaTotalPremium,wsaaCntfee));
		/* To incorporate Advance premium for available premium.*/
		if ((setPrecision(wsaaTotalPremium, 2)
		&& isGTE((add(wsaaTotalSuspense,wsaaSacscurbal)),wsaaTotalPremium))) {
			wsaaTolerance.set(ZERO);
		}
		else {
			compute(wsaaDiff, 2).set(sub(sub(wsaaTotalPremium,wsaaTotalSuspense),wsaaSacscurbal));
			if (isLT(wsaaDiff,wsaaTotTolerance)) {
				wsaaTolerance.set(wsaaDiff);
			}
			else {
				if (isEQ(wsaaTotTolerance2,0)) {
					wsaaTolerance.set(wsaaTotTolerance);
				}
				else {
					if (isLT(wsaaDiff,wsaaTotTolerance2)) {
						wsaaTolerance.set(wsaaDiff);
					}
					else {
						wsaaTolerance.set(wsaaTotTolerance2);
					}
				}
			}
		}
		compute(wsaaDiff, 2).set(sub(sub(wsaaTotalPremium,wsaaTotalSuspense),wsaaSacscurbal));
		compute(wsaaTotalSuspense, 2).set(add(add(wsaaTotalSuspense,wsaaSacscurbal),wsaaTolerance));
		
}
protected void readHoldChdr2630()
	{
		/*START*/
		/*  Calling CHDRLNBIO with 'KEEPS' so that CHRDPF gets*/
		/*  updated and will be retrived by P6378 called via BO*/
 //ILB-947 start	
		chdrlnbIO = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(br50hDto.getChdrnum())) {
			for (Chdrpf c : chdrpfMap.get(br50hDto.getChdrnum())) {
					chdrlnbIO = c;
					break;
			}
		}
		if (chdrlnbIO == null) {
			syserrrec.params.set(br50hDto.getChdrnum());
			fatalError600();
		}
		
		chdrpfDAO.setCacheObject(chdrlnbIO);
 //ILB-947 end		
		/*EXIT*/
	}

protected void calcFee2650()
	{
			readSubroutineTable2651();
	}
protected void readSubroutineTable2651(){
		if(t5674Map.containsKey(t5688rec.feemeth.toString())) {
			List<Itempf> t5674List = t5674Map.get(t5688rec.feemeth.toString());
	        Itempf t5674Item = t5674List.get(0);
	        t5674rec.t5674Rec.set(StringUtil.rawToString(t5674Item.getGenarea()));
		}
		else if(t5674Map.containsKey(t5688rec.feemeth.toString().trim())) {
			List<Itempf> t5674List = t5674Map.get(t5688rec.feemeth.toString().trim());
	        Itempf t5674Item = t5674List.get(0);
	        t5674rec.t5674Rec.set(StringUtil.rawToString(t5674Item.getGenarea()));
		}
		else {
			wsspcomn.edterror.set("Y");
			return ;
		}
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set(wsaaBillingInformationInner.wsaaBillfreq[1]);
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(bsprIO.getCompany());
		/* Check subroutine NOT = SPACES before attempting call.*/
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		if(t5674rec.commsubr.toString().equalsIgnoreCase("CFEE01")) {
			callt5674subr();
		}
		else {
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		}
		if ((isNE(mgfeelrec.statuz,varcom.oK))
		&& (isNE(mgfeelrec.statuz,varcom.endp))) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getBillcd(),chdrlnbIO.getOccdate())) {
			wsaaCntfee.set(ZERO);
		}
		else {
			wsaaCntfee.set(mgfeelrec.mgfee);
		}
}
	protected void callt5674subr() {
		wsaaCnttype.set(mgfeelrec.cnttype);
		wsaaCntcurr.set(mgfeelrec.cntcurr);
		mgfeelrec.statuz.set(varcom.oK);
		if(t5567Map.containsKey(wsaaItemitem.toString())) {
			List<Itempf> t5567List = t5567Map.get(wsaaItemitem.toString());
            for(Itempf t5567Item: t5567List) {
                if(Integer.parseInt(t5567Item.getItmfrm().toString())<=chdrlnbIO.getOccdate() 
                && Integer.parseInt(t5567Item.getItmto().toString())>=chdrlnbIO.getOccdate()) {
                	t5567rec.t5567Rec.set(StringUtil.rawToString(t5567Item.getGenarea()));
                	break;
                }
            }
		}
		else if(t5567Map.containsKey(wsaaItemitem.toString().trim())) {
			List<Itempf> t5567List = t5567Map.get(wsaaItemitem.toString().trim());
            for(Itempf t5567Item: t5567List) {
                if(Integer.parseInt(t5567Item.getItmfrm().toString())<=chdrlnbIO.getOccdate() 
                && Integer.parseInt(t5567Item.getItmto().toString())>=chdrlnbIO.getOccdate()) {
                	t5567rec.t5567Rec.set(StringUtil.rawToString(t5567Item.getGenarea()));
                	break;
                }
            }
		}
		else {
			mgfeelrec.statuz.set("H966");
		}
		if (isNE(mgfeelrec.statuz,varcom.oK)) {
			 for (wsaaSub1.set(1); !(isEQ(wsaaSub1,10)
    		|| isEQ(t5567rec.billfreq[wsaaSub1.toInt()],mgfeelrec.billfreq)); wsaaSub1.add(1)){
            	/*MATCH-FREQUENCY*/
    		}
			if (isEQ(t5567rec.billfreq[wsaaSub.toInt()],mgfeelrec.billfreq)) {
				mgfeelrec.mgfee.set(t5567rec.cntfee[wsaaSub.toInt()]);
			}
			else {
				mgfeelrec.mgfee.set(ZERO);
				mgfeelrec.statuz.set(varcom.endp);
			}
		}
		
	}
protected void calcPremium2670(){
 //ILB-947 start
	if (covtpfMap != null && covtpfMap.containsKey(br50hDto.getChdrnum())) {
		for (Covtpf covt : covtpfMap.get(br50hDto.getChdrnum())) {
			wsbbSub.set(covt.getPayrseqno());
			wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].add(Float.parseFloat(covt.getInstprem().toString()));
			wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].add(Float.parseFloat(covt.getSingp().toString()));
			if (covt.getSingp().floatValue()!=0) {
				wsaaSingPrmInd = "Y";
			}
			readTr52erec2000(covt);
			calcTax(covt);
			covtIO = covt;
			break;
		}
	}
	if (covtIO == null) {
		syserrrec.params.set(br50hDto.getChdrnum());
		fatalError600();
	}
	if (covtIO == null) {
		syserrrec.params.set(br50hDto.getChdrnum());
		fatalError600();
	}
 //ILB-947 end
}


protected void readTr52erec2000(Covtpf covt)
{
	if(isEQ(chdrlnbIO.getReg(),"SGP"))
	{
		 if(tr52dMap.containsKey(chdrlnbIO.getReg())) {
			  tr52drec.tr52dRec.set(StringUtil.rawToString(tr52dMap.get(chdrlnbIO.getReg()).get(0).getGenarea()));
		  }else if(tr52dMap.containsKey("***")){
			  tr52drec.tr52dRec.set(StringUtil.rawToString(tr52dMap.get("***").get(0).getGenarea()));
		  }else {
			  syserrrec.params.set(strCompany.concat("TR52D***"));
			  fatalError600();
		  }
	/*
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itemIO.setItemtabl(tablesInner.tr52d);
	itemIO.setItemitem(chdrlnbIO.getReg());
	itemIO.setFormat(formatsInner.itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem("***");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}
	tr52drec.tr52dRec.set(itemIO.getGenarea());
	*/
	}
	
	/* Read table TR52E                                                */
 //ILIFE-8901 start	
	StringBuilder sbTr52eKey = new StringBuilder();
	sbTr52eKey.append(tr52drec.txcode);
	sbTr52eKey.append(chdrlnbIO.getCnttype());
	sbTr52eKey.append(covt.getCrtable());
	boolean itemFound = readTr52e(sbTr52eKey.toString(), Integer.valueOf(chdrlnbIO.getOccdate().toString()));
	
	if (!itemFound){
		sbTr52eKey = new StringBuilder();
		sbTr52eKey.append(tr52drec.txcode);
		sbTr52eKey.append(chdrlnbIO.getCnttype());
		sbTr52eKey.append("****");	
		itemFound = readTr52e(sbTr52eKey.toString(), Integer.valueOf(chdrlnbIO.getOccdate().toString()));
		if (!itemFound){
			sbTr52eKey = new StringBuilder();
			sbTr52eKey.append(tr52drec.txcode);
			sbTr52eKey.append("***");
			sbTr52eKey.append("****");	
			itemFound = readTr52e(sbTr52eKey.toString(), Integer.valueOf(chdrlnbIO.getOccdate().toString()));
			if (!itemFound){
				syserrrec.params.set(sbTr52eKey);
				fatalError600();
			}
		}
	}
 //ILIFE-8901 end
	/*    Obtain the long description from DESC using DESCIO.*/
	descIO.setDataArea(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(chdrlnbIO.getChdrcoy());
	descIO.setDesctabl("T5687");
	descIO.setDescitem(covt.getCrtable());
	descIO.setLanguage("E");
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(), varcom.oK)
	&& isNE(descIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	/*    MOVE DESC-LONGDESC          TO WSAA-HEDLINE.                 */
	if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
		wsaaHedline.fill("?");
	}
	else {
		wsaaHedline.set(descIO.getLongdesc());
	}
}
 //ILIFE-8901 start
protected boolean readTr52e(String itemKey, Integer effDate){
	String keyItemitem = itemKey.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr52eListMap.containsKey(keyItemitem)){	
		itempfList = tr52eListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((effDate >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& effDate <= Integer.parseInt(itempf.getItmto().toString())){
					tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
					break;
				}
			}else{
				tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));	
				itemFound = true;
				break;
			}				
		}		
	}
	return itemFound;	
}
 //ILIFE-8901 end

protected void calcTax(Covtpf covt){
	
	if (isEQ(tr52erec.taxind01, "Y")) {
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covt.getLife());
		txcalcrec.coverage.set(covt.getCoverage());
		txcalcrec.rider.set(covt.getRider());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.jrnseq.set(ZERO);
		txcalcrec.crtable.set(covt.getCrtable());
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getReg());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(covt.getCntcurr());
		wsaaCcy.set(covt.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			txcalcrec.amountIn.set(covt.getInstprem());
		}
		else {
			if (isEQ(tr52erec.zbastyp.trim(), "Y")) {
				compute(txcalcrec.amountIn, 2).set(add(covt.getZbinstprem(), ZERO));
			}
			else {
				compute(txcalcrec.amountIn, 2).set(add(covt.getZbinstprem(), ZERO));
			}
		}
	txcalcrec.transType.set("PREM");
	txcalcrec.effdate.set(chdrlnbIO.getOccdate());
	txcalcrec.tranno.set(chdrlnbIO.getTranno());
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.txcode.set(tr52drec.txcode);
	if(isGT(txcalcrec.amountIn,0)){
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK) && isEQ(txcalcrec.statuz.toString().indexOf("F109"),-1) ) {
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	}
	if(isEQ(txcalcrec.statuz.toString().indexOf("F109"),-1) ){
		wsaaTaxamt.set(ZERO);
	}
	 if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
		wsaaTaxamt.add(txcalcrec.taxAmt[1]);
	}
	if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
		wsaaTaxamt.add(txcalcrec.taxAmt[2]);
	}
	wsaaTotalPremium.set(covt.getInstprem().toString());
	if (covt.getCoverage().equals("01") && covt.getRider().equals("00"))
	{
		wsaaTotalTaxedPremium.add(wsaaTaxamt); 
	    wsaaTotalTaxedPremium.add(wsaaTotalPremium);
	}
}
}
protected void readContractTable2680()
{
		readContractDefinition2681();
}

protected void readContractDefinition2681(){
	if(t5688Map.containsKey(chdrlnbIO.getCnttype())) {
		List<Itempf> t5688List = t5688Map.get(chdrlnbIO.getCnttype());
        for(Itempf t5688Item: t5688List) {
            if(Integer.parseInt(t5688Item.getItmfrm().toString())<=chdrlnbIO.getOccdate() 
            && Integer.parseInt(t5688Item.getItmto().toString())>=chdrlnbIO.getOccdate()) {
            	t5688rec.t5688Rec.set(StringUtil.rawToString(t5688Item.getGenarea()));
            	break;
            }
        }
	}else if(t5688Map.containsKey(chdrlnbIO.getCnttype().trim())) {
		List<Itempf> t5688List = t5688Map.get(chdrlnbIO.getCnttype().trim());
        for(Itempf t5688Item: t5688List) {
        	if(Integer.parseInt(t5688Item.getItmfrm().toString())<=chdrlnbIO.getOccdate() 
            && Integer.parseInt(t5688Item.getItmto().toString())>=chdrlnbIO.getOccdate()) {
            	t5688rec.t5688Rec.set(StringUtil.rawToString(t5688Item.getGenarea()));
            	break;
            }
        }
	}else {
		wsspcomn.edterror.set("Y");//ILB-1057
		goTo(GotoLabel.exit2629);
		//fatalError600();
	}
}

protected void readTh5062700()
	{
	
		if(th506Map.containsKey(br50hDto.getCnttype())) {
			List<Itempf> th506ItemList = th506Map.get(br50hDto.getCnttype());
			Itempf th506Item = th506ItemList.get(0);
			th506rec.th506Rec.set(StringUtil.rawToString(th506Item.getGenarea()));
		}
		else if(th506Map.containsKey(br50hDto.getCnttype().trim())) {
			List<Itempf> th506ItemList = th506Map.get(br50hDto.getCnttype().trim());
			Itempf th506Item = th506ItemList.get(0);
			th506rec.th506Rec.set(StringUtil.rawToString(th506Item.getGenarea()));
		}
		else if(th506Map.containsKey("***")) {
			List<Itempf> th506ItemList = th506Map.get("***");
			Itempf th506Item = th506ItemList.get(0);
			th506rec.th506Rec.set(StringUtil.rawToString(th506Item.getGenarea()));
		}
}

protected void readFlup2800(){
	callFlupio2802();
}

protected void callFlupio2802(){
 //ILIFE-8901 start
		 	
			if(fluppfMap !=null && fluppfMap.containsKey(br50hDto.getChdrnum())) {
				List<Fluppf> fluppfList = fluppfMap.get(br50hDto.getChdrnum());
			/*    MOVE FLUP-FUPCODE           TO ITEM-ITEMITEM.                */
			wsaaT5661Lang.set(bsscIO.getLanguage());
			for (Iterator<Fluppf> iterator = fluppfList.iterator(); iterator.hasNext(); ){
				Fluppf flup = iterator.next();
			
				wsaaT5661Fupcode.set(flup.getFupCde());
				if(t5661Map.containsKey(wsaaT5661Key.toString())) {
					List<Itempf> t5661ItemList = t5661Map.get(wsaaT5661Key.toString());
 //ILIFE-8901 end
					Itempf t5661Item = t5661ItemList.get(0);
		            t5661rec.t5661Rec.set(StringUtil.rawToString(t5661Item.getGenarea()));
				}
				else if(t5661Map.containsKey(wsaaT5661Key.trim())) {/* IJTI-1523 */
					List<Itempf> t5661ItemList = t5661Map.get(wsaaT5661Key.trim());/* IJTI-1523 */
					Itempf t5661Item = t5661ItemList.get(0);
		            t5661rec.t5661Rec.set(StringUtil.rawToString(t5661Item.getGenarea()));
				}
				/* To get the latest Follow Up outstanding creation date*/
				wsaaOutstandFlup.setTrue();
				for (ix.set(1); !(isGT(ix,10)
				|| wsaaCompleteFlup.isTrue()); ix.add(1)){
					if (isEQ(flup.getFupSts(),t5661rec.fuposs[ix.toInt()])) {
						wsaaCompleteFlup.setTrue();
					}
				}
				/* To take the latest O/S date*/
				if (wsaaOutstandFlup.isTrue() && isGT(flup.getEffDate(),wsaaFlupOustDate)) {
					wsaaFlupOustDate.set(flup.getEffDate());
				}
			}
		}
}

protected void calcNofday2910()
	{
			start2910();
		}

protected void start2910()
	{
		wsaaNofday.set(ZERO);
		
		//if (isEQ(hpadIO.getHprrcvdt(),ZERO)) {
		if (isEQ(br50hDto.getHprrcvdt(),ZERO)) {
			return ;
		}
		//datcon3rec.intDate1.set(hpadIO.getHprrcvdt());
	
		if (NBPRP125Permission && !isEQ(br50hDto.getDecldate(), 99999999)
			&& br50hDto.getDecldate() != 0) {
			datcon3rec.intDate1.set(br50hDto.getDecldate());
		} else {
			datcon3rec.intDate1.set(br50hDto.getHprrcvdt());
		}
		
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaNofday.set(datcon3rec.freqFactor);
	}

protected void loadPayerDetails2a00()
{
	wsbbSub.set(br50hDto.getPayrPayrseqno());
	wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(br50hDto.getIncseqno());
	wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(br50hDto.getBillfreq());
	wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(br50hDto.getBtdate());
	wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(br50hDto.getClntnum());
	wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(br50hDto.getClntcoy());

}

protected void checkSuspense2aa00(){
		acblpf = null;
		acblpf = acblDao.getAcblpfRecord(strCompany, t5645rec.sacscode01.toString(), chdrlnbIO.getChdrnum(), t5645rec.sacstype01.toString().trim(), null);
		if (acblpf == null && !StringUtils.isBlank(t5645rec.sacscode03.toString())) {
			acblpf = acblDao.getAcblpfRecord(strCompany, t5645rec.sacscode03.toString(), chdrlnbIO.getChdrnum(), t5645rec.sacstype03.toString().trim(), null);
		}
		if(acblpf!=null && acblpf.getSacscurbal().floatValue()!=0) {
			wsaaSuspInd = "Y";
			if (isEQ(t3695rec.sign,"-")) {
				compute(wsaaCntSuspense, 2).set(mult(acblpf.getSacscurbal(),-1));
			}
			else {
				wsaaCntSuspense.set(acblpf.getSacscurbal());
			}
			wsaaTotalSuspense.set(wsaaCntSuspense);
		}
}

protected void checkApa2ab00(){
	wsaaTh623Cnttype.set(br50hDto.getCnttype());
	wsaaSacscurbal = null;
	wsaaSacscurbal = BigDecimal.ZERO;
	if(th623Map.containsKey(wsaaTh623Key.toString())) {
		readInterestAccruel2030();
	}
}
	protected void readInterestAccruel2030()
	{
		wsaaRldgacct.set(SPACES);
		if (isEQ(wsaaT5645sacsCode02Rlpdlon, SPACES)
		|| isEQ(wsaaT5645sacsType02Rlpdlon, SPACES)
		|| isEQ(wsaaT5645sacsCode05Rlpdlon, SPACES)
		|| isEQ(wsaaT5645sacsType05Rlpdlon, SPACES)) {
			syserrrec.statuz.set("G450");
			fatalError600();
		}
		//ILIFE-8901 start
		Acblpf acblpf = acblDao.getAcblpfRecord(chdrlnbIO.getChdrcoy().toString(), wsaaT5645sacsCode02Rlpdlon, chdrlnbIO.getChdrnum(),wsaaT5645sacsType02Rlpdlon,null);
		if(acblpf!=null) {
		if (acblpf.getSacscurbal().floatValue()==0) {
			return ;
		}
		/* Negate the Account balances if it is negative (for the*/
		/* case of Cash accounts).*/
		if (acblpf.getSacscurbal().floatValue()<0) {
			setPrecision(acblpf.getSacscurbal(), 2);
			acblpf.setSacscurbal(acblpf.getSacscurbal().multiply(BigDecimal.valueOf(-1)));
		}
		wsaaSacscurbal = wsaaSacscurbal.add(acblpf.getSacscurbal());
		}
		//ILIFE-8901 end
	}
protected void adjustPremium2b00()
	{
		adjustPremium2b10();
		checkSuspense2b50();
	}

protected void adjustPremium2b10()
	{
		/*    Get the frequency Factor from DATCON3.*/
		if (isNE(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], ZERO)) {
		
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()]);
			datcon3rec.frequency.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				fatalError600();
			}
			/*    If the risk commencment is 28, 29, 30, of january,*/
			/*    30 march, 30 may, 30 august or the 30 of october*/
			/*    the initial instalment required is incorrectly*/
			/*    calculated as 1 month + 1 day * premium instead of*/
			/*    1 month(frequency 12)*/
			/* Calculate the instalment premium.*/

			wsaaFactor.set(datcon3rec.freqFactor);
			compute(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], 5).set(mult(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], wsaaFactor));
		}
		/* Add in the single premium.*/
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()]);
		/* If the tax relief method is not spaces calculate the tax*/
		/* relief amount and deduct it from the premium.*/
		if (isNE(t5688rec.taxrelmth,SPACES)) {
			prasrec.clntnum.set(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()]);
			prasrec.clntcoy.set(wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()]);
			prasrec.incomeSeqNo.set(wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()]);
			prasrec.cnttype.set(chdrlnbIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			/* Use the due date unless a receipt exists with a date later*/
			/* then the due date.*/
			if (isEQ(rtrnsac.getEffdate(),varcom.vrcmMaxDate)) {
				prasrec.effdate.set(chdrlnbIO.getOccdate());
			}
			else {
				if (isGT(chdrlnbIO.getOccdate(),rtrnsac.getEffdate())) {
					prasrec.effdate.set(chdrlnbIO.getOccdate());
				}
				else {
					prasrec.effdate.set(rtrnsac.getEffdate());
				}
			}
			prasrec.company.set(chdrlnbIO.getChdrcoy());
			prasrec.grossprem.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].subtract(prasrec.taxrelamt);
		}
		wsaaTotalPremium.add(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
		/*    ADD WSAA-INSTPRM(WSBB-SUB)  TO WSAA-TOT-PREM.*/
		/* Add the contract fee to the instalment premium for*/
		/* payer No. 1.*/
		/* If single premium is needed check if any fee is needed*/
		if (isEQ(wsaaSingPrmInd,"Y")
		&& isNE(t5688rec.feemeth,SPACES)) {
			singPremFee2c00();
		}
		if (isEQ(wsbbSub,1)) {
			if (isEQ(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], ZERO)) {
				wsaaCntfee.set(ZERO);
			}
			else {
				compute(wsaaCntfee, 5).set(mult(wsaaCntfee,wsaaFactor));
			}
			wsaaCntfee.add(wsaaSingpFee);
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(wsaaCntfee);
		}
	}

protected void checkSuspense2b50()
	{
		/*  Check if there is enough money in suspense to issue*/
		/*  this contract.*/
		/* Get the payer suspense.*/
		wsaaPayrSuspense.set(ZERO);
		/* Look up tolerance applicable*/
		/* Must convert premium into Suspense currency, if Suspense*/
		/* different from Contract currency, so that tolerance levels*/
		/* are calculated correctly against the Suspense amount.*/
		wsaaToleranceApp.set(ZERO);
		wsaaAmountLimit.set(ZERO);
		wsaaToleranceApp2.set(ZERO);
		wsaaAmountLimit2.set(ZERO);
		if (isEQ(wsaaSuspInd,"N")) {
			wsaaAmntDue.set(ZERO);
		}
		else {
			if (isEQ(chdrlnbIO.getCntcurr(),acblpf.getOrigcurr())) {
				wsaaAmntDue.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
			}
			else {
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountOut.set(ZERO);
				conlinkrec.rateUsed.set(ZERO);
				//conlinkrec.cashdate.set(hpadIO.getHpropdte());
				conlinkrec.cashdate.set(br50hDto.getHpropdte());
				conlinkrec.currIn.set(chdrlnbIO.getCntcurr());
				//conlinkrec.currOut.set(acblenqIO.getOrigcurr());
				conlinkrec.currOut.set(acblpf.getOrigcurr());
				conlinkrec.amountIn.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
				conlinkrec.company.set(chdrlnbIO.getChdrcoy());
				conlinkrec.function.set("SURR");
				callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
				if (isNE(conlinkrec.statuz,varcom.oK)) {
					syserrrec.params.set(conlinkrec.clnk002Rec);
					syserrrec.statuz.set(conlinkrec.statuz);
					fatalError600();
				}
				wsaaAmntDue.set(conlinkrec.amountOut);
			}
		}
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub,11))) {
			if (isEQ(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], t5667rec.freq[wsaaSub.toInt()])) {
				wsaaToleranceApp.set(t5667rec.prmtol[wsaaSub.toInt()]);
				wsaaToleranceApp2.set(t5667rec.prmtoln[wsaaSub.toInt()]);
				wsaaAmountLimit.set(t5667rec.maxAmount[wsaaSub.toInt()]);
				wsaaAmountLimit2.set(t5667rec.maxamt[wsaaSub.toInt()]);
				wsaaSub.set(11);
			}
			wsaaSub.add(1);
		}

		/* Calculate the tolerance applicable.*/
		compute(wsaaCalcTolerance, 2).set(div((mult(wsaaToleranceApp,wsaaAmntDue)),100));
		/*    Check % amount is less than Limit on T5667, if so use this*/
		/*    else use Limit.*/
		/*    Check for 1st premium shortfall tolerance limit.*/
		if (isLT(wsaaCalcTolerance,wsaaAmountLimit)) {
			wsaaTolerance.set(wsaaCalcTolerance);
		}
		else {
			wsaaTolerance.set(wsaaAmountLimit);
		}
		wsaaTotTolerance.add(wsaaTolerance);
		/*    Check for 2nd premium shortfall tolerance limit.*/
		compute(wsaaCalcTolerance2, 2).set(div((mult(wsaaToleranceApp2,wsaaAmntDue)),100));
		if (isLT(wsaaCalcTolerance2,wsaaAmountLimit2)) {
			wsaaTolerance2.set(wsaaCalcTolerance2);
		}
		else {
			wsaaTolerance2.set(wsaaAmountLimit2);
		}
		wsaaTotTolerance2.add(wsaaTolerance2);
		/* If there is not enough money in the payer suspense account*/
		/* plus tolerance to cover the premium due check to see if*/
		/* there is enough money in contract suspense to cover the*/
		/* remainder (or all if payr suspense was zero) of the*/
		/* premium due.*/
		/* If the remainder of the premium amount due is greater then*/
		/* the contract suspense plus the tolerance display an error*/
		/* message.*/
		/* If there is enough money in  contract suspense reduce the*/
		/* contract suspense amount by the remainder of the premium due.*/
		if ((setPrecision(wsaaAmntDue, 2)
		&& isGT(wsaaAmntDue,add(wsaaPayrSuspense,wsaaTolerance)))) {
			wsaaAmntDue.subtract(wsaaPayrSuspense);
			if ((setPrecision(wsaaAmntDue, 2)
			&& isGT(wsaaAmntDue,add(wsaaCntSuspense,wsaaTolerance)))) {
				/* Nothing to do. */
			}
			else {
				wsaaCntSuspense.subtract(wsaaAmntDue);
			}
		}
		wsbbSub.add(1);
	}

protected void singPremFee2c00()
	{
			singPremFeePara2c00();
		}

protected void singPremFeePara2c00()
	{
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set("00");
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(bsprIO.getCompany());
		if(t5674rec.commsubr.toString().equalsIgnoreCase("CFEE01")) {
			callt5674subr();
		}
		else {
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		}
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		wsaaSingpFee.set(mgfeelrec.mgfee);
	}


	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void update3000()
	{
	 
			update3010();
		 
	}
protected void update3010()
	{
		/* Update database records.*/
		/*  Check the Total Premium Required and the Premium Suspense*/
		if (isLT(wsaaTotalTaxedPremium,wsaaTotalSuspense)) {
			wsaaFinancial.setTrue();
		}
		else {
			wsaaNotFinancial.setTrue();
		}
		wsaaBatckey.set(batcdorrec.batchkey);
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmUser.set(subString(bsscIO.getDatimeInit(), 21, 6));
		/*    Soft lock contract.*/
 		//ILIFE-8901 start
		sftlockRecBean = new SftlockRecBean();
		sftlockRecBean.setFunction("LOCK");
		sftlockRecBean.setCompany(bsprIO.getCompany().toString());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(chdrlnbIO.getChdrnum());
		sftlockRecBean.setTransaction(wsaaBatckey.batcBatctrcde.toString());
		sftlockRecBean.setUser(varcom.vrcmUser.toString());
		sftlockUtil.process(sftlockRecBean, appVars);
		//F910
		if ("LOCK".equals(sftlockRecBean.getStatuz())) {
			wsspcomn.edterror.set(SPACE);
			return ;
		}
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
			&& !StringUtils.equalsIgnoreCase(Varcom.mrnf.toString(), sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
		}
		 //ILIFE-8901 end
	
		callNtu3300();
		if (isNE(wsspcomn.edterror,varcom.oK)) {
			return ;
		}
		ctCount2++;
	}

protected void callNtu3300()
	{
			start3300();
		}

protected void start3300()
	{
		chdrlnbIO.setStatdate(wsaaToday.toInt());
		
		//ILIFE-8901 end
		chdrlnbIO.setStatcode(t5679rec.setCnRiskStat.toString()); //ILB-947
		chdrlnbIO.setPstcde(t5679rec.setCnPremStat.toString()); //ILB-947
		/*  contract status transaction no to contract tran no*/
		chdrlnbIO.setStattran(chdrlnbIO.getTranno());
		chdrlnbIO.setPsttrn(chdrlnbIO.getTranno()); //ILB-947
		chdrlnbIO.setTranno(chdrlnbIO.getTranno()+1);
 //ILB-947 start
		chdrBulkUpdtList.add(chdrlnbIO);
		
		chdrpfDAO.setCacheObject(chdrlnbIO);
 //ILB-947 end		
		
		m100ReadMliachd();
		/* Write a PTRN record.*/
		Ptrnpf ptrnpf = new Ptrnpf();
		boolean mexist=false;
		if (!ptrnBulkInsList.isEmpty()) {
			for (Ptrnpf item : ptrnBulkInsList) 
			{
		        if (item.getChdrnum().equals(chdrlnbIO.getChdrnum()))
				{
		        	mexist= true;
		        	break;
		        }
		   
			 }
		}
		if (!mexist)
		{
			ptrnpf.setBatcpfx("BA");
			ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
			ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
			ptrnpf.setBatcactyr(Integer.parseInt(wsaaBatckey.batcBatcactyr.toString().trim()));
			ptrnpf.setBatcactmn(Integer.parseInt(wsaaBatckey.batcBatcactmn.toString().trim()));
			ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
			ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
			ptrnpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
			ptrnpf.setChdrnum(chdrlnbIO.getChdrnum());
			//ptrnpf.setTranno(Integer.parseInt(chdrlnbIO.getTranno().toString().trim()));
			ptrnpf.setTranno(chdrlnbIO.getTranno()); 
			ptrnpf.setTrdt(Integer.parseInt(varcom.vrcmDate.toString().trim()));
			ptrnpf.setTrtm(Integer.parseInt(varcom.vrcmTime.toString().trim()));
			ptrnpf.setTermid(varcom.vrcmTermid.toString());
			ptrnpf.setUserT(Integer.parseInt(varcom.vrcmUser.toString().trim()));
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			/* MOVE DTC1-INT-DATE          TO PTRN-PTRNEFF.                 */
			/* MOVE DTC1-INT-DATE          TO PTRN-DATESUB.                 */
			ptrnpf.setPtrneff(Integer.parseInt(bsscIO.getEffectiveDate().toString().trim()));
			ptrnpf.setDatesub(Integer.parseInt(bsscIO.getEffectiveDate().toString().trim()));
			ptrnBulkInsList.add(ptrnpf);
		}
			
		/* Create a RESN record if reason code or narrative was entered*/
 //ILB-947 start
		Resnpf resnIO = new Resnpf();
		resnIO.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		resnIO.setChdrnum(chdrlnbIO.getChdrnum());
		resnIO.setTranno(chdrlnbIO.getTranno());
		if (wsaaFinancial.isTrue()) {
			resnIO.setReasoncd(wsaaNtuf);
		}
		else {
			resnIO.setReasoncd(wsaaNtup);
		}
		resnIO.setTrancde(wsaaBatckey.batcBatctrcde.toString());
		resnIO.setTrdt(varcom.vrcmDate.toInt());
		resnIO.setTrtm(varcom.vrcmTime.toInt());
		resnIO.setUserT(varcom.vrcmUser.toInt());
		resnpfDAO.updtOrInstResnpf(resnIO);//ILB-1057
 //ILB-947 end
		
		createUndrpf3800();
		 //ILIFE-8901 start
		sftlockRecBean = new SftlockRecBean();
		sftlockRecBean.setFunction("UNLK");
		sftlockRecBean.setCompany(bsprIO.getCompany().toString());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(chdrlnbIO.getChdrnum());
		sftlockRecBean.setTransaction(wsaaBatckey.batcBatctrcde.toString());
		sftlockRecBean.setUser(varcom.vrcmUser.toString());
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz()) && !StringUtils.equalsIgnoreCase(Varcom.mrnf.toString(), sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
		}
		 //ILIFE-8901 end
	}
 //ILB-947 start
protected void commitChdrBulkUpdate(){	 
	 boolean isUpdateChdrLif = chdrpfDAO.updateChdrAndTranno(chdrBulkUpdtList);	
	if (!isUpdateChdrLif) {
		LOGGER.error("Update CHDRPF record failed.");
		fatalError600();
	}else chdrBulkUpdtList.clear();
}
 //ILB-947 end

protected void commit3500()
	{
//	long startTime = System.currentTimeMillis();
	
	/* Update the batch header using BATCUP.*/
	batcupPojo  = new BatcupPojo();
	batcupPojo.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
	batcupPojo.setBatccoy(wsaaBatckey.batcBatccoy.toString());
	batcupPojo.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
	batcupPojo.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
	batcupPojo.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
	batcupPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	batcupPojo.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
	batcupPojo.setTrancnt(1);
	batcupPojo.setEtreqcnt(0);
	batcupPojo.setSub(0);
	batcupPojo.setAscnt(0);
	batcupPojo.setBcnt(0);
	batcupPojo.setBval(0);
	batcupPojo.setStatuz(varcom.oK.toString());
	batcupPojo.setFunction(varcom.writs.toString());
	batcupUtils.callBatcup(batcupPojo);
	if (isNE(batcupPojo.getStatuz(), varcom.oK)) {
		syserrrec.params.set(batcupPojo.getParameters());
		syserrrec.statuz.set(batcupPojo.getStatuz());
		fatalError600();
	}
	/*
	batcuprec.batcupRec.set(SPACES);
	batcuprec.batchkey.set(wsaaBatckey.batcKey);
	batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
	batcuprec.trancnt.set(1);
	batcuprec.etreqcnt.set(ZERO);
	batcuprec.sub.set(ZERO);
	batcuprec.bcnt.set(ZERO);
	batcuprec.bval.set(ZERO);
	batcuprec.ascnt.set(ZERO);
	batcuprec.statuz.set(ZERO);
	batcuprec.function.set(varcom.writs);
	callProgram(Batcup.class, batcuprec.batcupRec);
	if (isNE(batcuprec.statuz,varcom.oK)) {
		syserrrec.params.set(batcuprec.batcupRec);
		syserrrec.statuz.set(batcuprec.statuz);
		fatalError600();
	}
	*/
	
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
	 //ILB-947 start
		commitControlTotals();
		
		/*EXIT*/
		if (ptrnBulkInsList != null && !ptrnBulkInsList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);
			ptrnBulkInsList.clear();
		}
		
		if (mliaBulkupdateList != null && !mliaBulkupdateList.isEmpty()) {
			mliapfDAO.bulkupdateMliaafi(mliaBulkupdateList);	
			mliaBulkupdateList.clear();
		}
		
		if(mliaDelList != null && !mliaDelList.isEmpty())
		{
			mliapfDAO.bulkdeleteMlicdByUnique(mliaDelList);			
			mliaDelList.clear();
			
		} 
		if (chdrBulkUpdtList != null && !chdrBulkUpdtList.isEmpty()) {
			commitChdrBulkUpdate();
			chdrBulkUpdtList.clear();
		}
//		LOGGER.info("commit3500 method run time: " + (System.currentTimeMillis() - startTime));
	}

protected void commitControlTotals() {
	
		contotrec.totno.set(ct01);
		contotrec.totval.set(ctCount1);
		callContot001();
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctCount2);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(ctCount3);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(ctCount4);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(ctCount5);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(ctCount6);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(ctCount7);
		callContot001();
	
}
 //ILB-947 end
protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void createUndrpf3800(){
	if (covtpfMap.containsKey(chdrlnbIO.getChdrnum())) {
		List<Covtpf> covtcsnList = covtpfMap.get(chdrlnbIO.getChdrnum());
		if(covtcsnList != null && !covtcsnList.isEmpty()){
			for(Covtpf covtpf : covtcsnList){
				createUndrCovt3820(covtpf);
			}
		}
	}
//	List<Covtpf> covtcsnList = covtDao.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum());
//	if(covtcsnList != null && !covtcsnList.isEmpty()){
//		for(Covtpf covtpf : covtcsnList){
//			createUndrCovt3820(covtpf);
//		}
//	}
}

	protected void createUndrCovt3820(Covtpf covtpf) {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		//while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					create3825();
				case nextLife3826:
					nextLife3826(covtpf);
				case exit3829:
					break;
				}
				
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		//}  /*ILIFE-5328 
	}

	protected void create3825()
	{
		initialize(crtundwrec.parmRec);
		wsaaJlifeN.set(ZERO);
	}

protected void nextLife3826(Covtpf covtpf)
	{
		Lifepf lifelnb = this.m300ReadLife(covtpf.getLife());
		if (lifelnb == null) {
			return ;
		}
		crtundwrec.clntnum.set(lifelnb.getLifcnum());
		crtundwrec.coy.set(covtpf.getChdrcoy());
		crtundwrec.chdrnum.set(covtpf.getChdrnum());
		crtundwrec.life.set(covtpf.getLife());
		crtundwrec.crtable.set(covtpf.getCrtable());
		crtundwrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		crtundwrec.sumins.set(covtpf.getSumins());
		crtundwrec.cnttyp.set(chdrlnbIO.getCnttype());
		crtundwrec.currcode.set(chdrlnbIO.getCntcurr());
		crtundwrec.function.set("ADD");
		crtundwrec = crtundwrtUtil.process(crtundwrec);
//		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
		wsaaJlifeN.add(1);
		goTo(GotoLabel.nextLife3826);
	}

protected void m100ReadMliachd()
	{
		/*M100-START*/
 //ILIFE-8901 start
		if (mliaMap != null && mliaMap.containsKey(br50hDto.getChdrnum())) {
			 List<Mliapf> mliapfList = mliaMap.get(br50hDto.getChdrnum());
			 for (Iterator<Mliapf> iterator = mliapfList.iterator(); iterator.hasNext(); ){
				 Mliapf mliachd = iterator.next();
						if (isEQ(mliachd.getMind(),"Y")) {
							Mliapf mliaafi = new Mliapf();
							mliaafi.setUniqueNumber(mliachd.getUniqueNumber());
							mliaafi.setMind("N");
							datcon1rec.function.set(varcom.tday);
							Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
							wsaaToday.set(datcon1rec.intDate);
							mliaafi.setEffdate(wsaaToday.toInt());
							mliaafi.setActn("3");
						   if(mliaBulkupdateList == null){
							   mliaBulkupdateList = new LinkedList<Mliapf>();
					        }
						   mliaBulkupdateList.add(mliaafi);
						}
						else {
							if(mliaDelList == null) {
								mliaDelList = new ArrayList<Long>();
							}
							mliaDelList.add(mliachd.getUniqueNumber());
						}
			}
 //ILIFE-8901 end
		}
		/*M100-EXIT*/
	}


protected Lifepf m300ReadLife(String life){
	if (lifelnbMap.containsKey(chdrlnbIO.getChdrnum())) {
		List<Lifepf> lifepfList = lifelnbMap.get(chdrlnbIO.getChdrnum());
		for (Lifepf item : lifepfList) {
			if (StringUtils.equals(StringUtils.trimToEmpty(strCompany), StringUtils.trimToEmpty(item.getChdrcoy()))
				&& StringUtils.equals(StringUtils.trimToEmpty(life), StringUtils.trimToEmpty(item.getLife()))
				&& StringUtils.equals(StringUtils.trimToEmpty(wsaaJlife.toString()),
						StringUtils.trimToEmpty(item.getJlife()))){
				return item;
			}
		}
	}
	
//	Lifepf lifepf = lifepfDAO.getLifeRecord(strCompany,chdrlnbIO.getChdrnum(),life,wsaaJlife.toString());
	return null;
}

protected void close4000(){
	//ILIFE-4940 by liwei
	if(t5661Map != null) {
		t5661Map.clear();
		t5661Map = null;
	}
	if(th506Map != null) {
		th506Map.clear();
		th506Map = null;
	}
	if(th623Map != null) {
		th623Map.clear();
		th623Map = null;
	}
	if(t5667Map != null) {
		t5667Map.clear();
		t5667Map = null;
	}
	if(t5688Map != null) {
		t5688Map.clear();
		t5688Map = null;
	}
	
	if(t6687Map != null) {
		t6687Map.clear();
		t6687Map = null;
	}
	
	if(t5674Map != null) {
		t5674Map.clear();
		t5674Map = null;
	}
	
	if(t5567Map != null) {
		t5567Map.clear();
		t5567Map = null;
	}
	if(tr52dMap != null) {
		tr52dMap.clear();
		tr52dMap = null;
	}
	if (lifelnbMap != null) {
		lifelnbMap.clear();
	}
	if (fluppfMap != null) {
		fluppfMap.clear();
	}
	if(mliaMap !=null) {
		mliaMap.clear();
	}
	
	
	/*CLOSE-FILES*/
	/*  Close any open files.*/
	nterpf.close();
	FileSort fs1 = new FileSort(sortFile);
	fs1.addSortKey(sortChdrcoy, true);
	fs1.addSortKey(sortHprrcvdt, true);
	fs1.addSortKey(sortTdayno, true);
	fs1.addSortKey(sortChdrnum, true);
	fs1.setUsing(nterpf);
	fs1.setGiving(nterpf);
	fs1.sort();
	wsaaQcmdexc.set("DLTOVR FILE(NTERPF)");
	com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
	lsaaStatuz.set(varcom.oK);
	/*EXIT*/
}

protected void writeEr5000()
	{
		start5000();
	}

protected void start5000()
	{
		//nterpfRec.hprrcvdt.set(hpadIO.getHprrcvdt());
		nterpfRec.hprrcvdt.set(br50hDto.getHprrcvdt());
		nterpfRec.tdayno.set(wsaaNofday);
		/*nterpfRec.chdrcoy.set(chdrzprIO.getChdrcoy());
		nterpfRec.chdrnum.set(chdrzprIO.getChdrnum());*/
		nterpfRec.chdrcoy.set(br50hDto.getChdrcoy());
		nterpfRec.chdrnum.set(br50hDto.getChdrnum());
		nterpfRec.erorprog.set(wsaaErErorprog);
		nterpfRec.flddesc.set(wsaaErFlddesc);
		nterpfRec.eror.set(wsaaErEror);
		nterpfRec.desc.set(wsaaErDesc);
		nterpfRec.instprem.set(wsaaTotPrem);
		nterpf.write(nterpfRec);
		wsaaErFields.set(SPACES);
		wsspcomn.edterror.set(SPACES);
		/* no of record with errors*/
		/*contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();*/
		ctCount3++;
	}


/*
 * Class transformed  from Data Structure WSAA-BILLING-INFORMATION--INNER
 */
private static final class WsaaBillingInformationInner {

		/* WSAA-BILLING-INFORMATION */
	private FixedLengthStringData[] wsaaBillingDetails = FLSInittedArray (9, 89);
	private ZonedDecimalData[] wsaaIncomeSeqNo = ZDArrayPartOfArrayStructure(2, 0, wsaaBillingDetails, 0, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaBillfreq = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 2);
	private FixedLengthStringData[] wsaaBillchnl = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 4);
	private PackedDecimalData[] wsaaBillcd = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 6);
	private PackedDecimalData[] wsaaBtdate = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 11);
	private FixedLengthStringData[] wsaaBillcurr = FLSDArrayPartOfArrayStructure(3, wsaaBillingDetails, 16);
	private FixedLengthStringData[] wsaaClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaBillingDetails, 19);
	private FixedLengthStringData[] wsaaClntnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 20);
	private FixedLengthStringData[] wsaaMandref = FLSDArrayPartOfArrayStructure(5, wsaaBillingDetails, 28);
	private FixedLengthStringData[] wsaaGrupkey = FLSDArrayPartOfArrayStructure(12, wsaaBillingDetails, 33);
	private PackedDecimalData[] wsaaRegprem = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 45);
	private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 54);
	private PackedDecimalData[] wsaaInstprm = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 63);
	private PackedDecimalData[] wsaaTaxrelamt = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 72);
	private FixedLengthStringData[] wsaaInrevnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 81);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}


private static final class TablesInner {
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
}

}
