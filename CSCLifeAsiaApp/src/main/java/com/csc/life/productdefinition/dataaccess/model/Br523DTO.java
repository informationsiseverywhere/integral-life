package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;


public class Br523DTO {
    // ActxpfTableDAM
    private String actxChdrpfx;
    private String actxChdrcoy;
    private String actxChdrnum;
    private String actxCnttype;
    private int actxCurrfrom;
    private String actxValidflag;
    private String actxStatcode;
    private String actxPstatcode;
    private int actxOccdate;
    // ChdrpfTableDAM
    private String chdrChdrpfx;
    private String chdrChdrcoy;
    private String chdrChdrnum;
    private int chdrCurrfrom;
    private String chdrValidflag;
    private String chdrBillfreq;
    private BigDecimal chdrSinstamt06;
    private BigDecimal chdrSinstamt01;
    private String chdrCnttype;
    private String chdrCowncoy;
    private String chdrCownnum;
    private String chdrPayrnum;
    private String chdrAgntcoy;
    private String chdrAgntnum;
    private int chdrBtdate;
    private int chdrPtdate;
    private String chdrCntcurr;
    private int chdrPolsum;
    // Hpadpf
    private int hpadHoissdte;
    private int hpadHissdte;
    // CLNT own
    private String clntowUniqueNumber;
    private String clntowClttype;
    private String clntowGivname;
    private String clntowSurname;
    private String clntowLsurname;
    private String clntowLgivname;
    // CLNT payr
    private String clntpayUniqueNumber;
    private String clntpayClttype;
    private String clntpayGivname;
    private String clntpaySurname;
    private String clntpayLsurname;
    private String clntpayLgivname;
    // CLNT clrr
    private String clntclrUniqueNumber;
    private String clntclrClttype;
    private String clntclrGivname;
    private String clntclrSurname;
    private String clntclrLsurname;
    private String clntclrLgivname;
    // agent
    private String agtAgntnum;
    private String agtAgtype;
    // agent
    private int pyrPtdate;
    private int pyrBtdate;
    
    public int getPyrPtdate() {
        return pyrPtdate;
    }
    public void setPyrPtdate(int pyrPtdate) {
        this.pyrPtdate = pyrPtdate;
    }
    public int getPyrBtdate() {
        return pyrBtdate;
    }
    public void setPyrBtdate(int pyrBtdate) {
        this.pyrBtdate = pyrBtdate;
    }
    public int getChdrPolsum() {
        return chdrPolsum;
    }
    public void setChdrPolsum(int chdrPolsum) {
        this.chdrPolsum = chdrPolsum;
    }
    public String getAgtAgntnum() {
        return agtAgntnum;
    }
    public void setAgtAgntnum(String agtAgntnum) {
        this.agtAgntnum = agtAgntnum;
    }
    public String getAgtAgtype() {
        return agtAgtype;
    }
    public void setAgtAgtype(String agtAgtype) {
        this.agtAgtype = agtAgtype;
    }
    public String getClntclrUniqueNumber() {
        return clntclrUniqueNumber;
    }
    public void setClntclrUniqueNumber(String clntclrUniqueNumber) {
        this.clntclrUniqueNumber = clntclrUniqueNumber;
    }
    public String getClntclrClttype() {
        return clntclrClttype;
    }
    public void setClntclrClttype(String clntclrClttype) {
        this.clntclrClttype = clntclrClttype;
    }
    public String getClntclrGivname() {
        return clntclrGivname;
    }
    public void setClntclrGivname(String clntclrGivname) {
        this.clntclrGivname = clntclrGivname;
    }
    public String getClntclrSurname() {
        return clntclrSurname;
    }
    public void setClntclrSurname(String clntclrSurname) {
        this.clntclrSurname = clntclrSurname;
    }
    public String getClntclrLsurname() {
        return clntclrLsurname;
    }
    public void setClntclrLsurname(String clntclrLsurname) {
        this.clntclrLsurname = clntclrLsurname;
    }
    public String getClntclrLgivname() {
        return clntclrLgivname;
    }
    public void setClntclrLgivname(String clntclrLgivname) {
        this.clntclrLgivname = clntclrLgivname;
    }
    public String getClntpayUniqueNumber() {
        return clntpayUniqueNumber;
    }
    public void setClntpayUniqueNumber(String clntpayUniqueNumber) {
        this.clntpayUniqueNumber = clntpayUniqueNumber;
    }
    public String getClntpayClttype() {
        return clntpayClttype;
    }
    public void setClntpayClttype(String clntpayClttype) {
        this.clntpayClttype = clntpayClttype;
    }
    public String getClntpayGivname() {
        return clntpayGivname;
    }
    public void setClntpayGivname(String clntpayGivname) {
        this.clntpayGivname = clntpayGivname;
    }
    public String getClntpaySurname() {
        return clntpaySurname;
    }
    public void setClntpaySurname(String clntpaySurname) {
        this.clntpaySurname = clntpaySurname;
    }
    public String getClntpayLsurname() {
        return clntpayLsurname;
    }
    public void setClntpayLsurname(String clntpayLsurname) {
        this.clntpayLsurname = clntpayLsurname;
    }
    public String getClntpayLgivname() {
        return clntpayLgivname;
    }
    public void setClntpayLgivname(String clntpayLgivname) {
        this.clntpayLgivname = clntpayLgivname;
    }
    public String getClntowUniqueNumber() {
        return clntowUniqueNumber;
    }
    public void setClntowUniqueNumber(String clntowUniqueNumber) {
        this.clntowUniqueNumber = clntowUniqueNumber;
    }
    public String getClntowClttype() {
        return clntowClttype;
    }
    public void setClntowClttype(String clntowClttype) {
        this.clntowClttype = clntowClttype;
    }
    public String getClntowGivname() {
        return clntowGivname;
    }
    public void setClntowGivname(String clntowGivname) {
        this.clntowGivname = clntowGivname;
    }
    public String getClntowSurname() {
        return clntowSurname;
    }
    public void setClntowSurname(String clntowSurname) {
        this.clntowSurname = clntowSurname;
    }
    public String getClntowLsurname() {
        return clntowLsurname;
    }
    public void setClntowLsurname(String clntowLsurname) {
        this.clntowLsurname = clntowLsurname;
    }
    public String getClntowLgivname() {
        return clntowLgivname;
    }
    public void setClntowLgivname(String clntowLgivname) {
        this.clntowLgivname = clntowLgivname;
    }
    public int getHpadHoissdte() {
        return hpadHoissdte;
    }
    public void setHpadHoissdte(int hpadHoissdte) {
        this.hpadHoissdte = hpadHoissdte;
    }
    public int getHpadHissdte() {
        return hpadHissdte;
    }
    public void setHpadHissdte(int hpadHissdte) {
        this.hpadHissdte = hpadHissdte;
    }
    public String getActxChdrpfx() {
        return actxChdrpfx;
    }
    public void setActxChdrpfx(String actxChdrpfx) {
        this.actxChdrpfx = actxChdrpfx;
    }
    public String getActxChdrcoy() {
        return actxChdrcoy;
    }
    public void setActxChdrcoy(String actxChdrcoy) {
        this.actxChdrcoy = actxChdrcoy;
    }
    public String getActxChdrnum() {
        return actxChdrnum;
    }
    public void setActxChdrnum(String actxChdrnum) {
        this.actxChdrnum = actxChdrnum;
    }
    public String getActxCnttype() {
        return actxCnttype;
    }
    public void setActxCnttype(String actxCnttype) {
        this.actxCnttype = actxCnttype;
    }
    public int getActxOccdate() {
        return actxOccdate;
    }
    public void setActxOccdate(int actxOccdate) {
        this.actxOccdate = actxOccdate;
    }
    public String getActxStatcode() {
        return actxStatcode;
    }
    public void setActxStatcode(String actxStatcode) {
        this.actxStatcode = actxStatcode;
    }
    public String getActxPstatcode() {
        return actxPstatcode;
    }
    public void setActxPstatcode(String actxPstatcode) {
        this.actxPstatcode = actxPstatcode;
    }
    public int getActxCurrfrom() {
        return actxCurrfrom;
    }
    public void setActxCurrfrom(int actxCurrfrom) {
        this.actxCurrfrom = actxCurrfrom;
    }
    public String getActxValidflag() {
        return actxValidflag;
    }
    public void setActxValidflag(String actxValidflag) {
        this.actxValidflag = actxValidflag;
    }
    public String getChdrChdrpfx() {
        return chdrChdrpfx;
    }
    public void setChdrChdrpfx(String chdrChdrpfx) {
        this.chdrChdrpfx = chdrChdrpfx;
    }
    public String getChdrChdrcoy() {
        return chdrChdrcoy;
    }
    public void setChdrChdrcoy(String chdrChdrcoy) {
        this.chdrChdrcoy = chdrChdrcoy;
    }
    public String getChdrChdrnum() {
        return chdrChdrnum;
    }
    public void setChdrChdrnum(String chdrChdrnum) {
        this.chdrChdrnum = chdrChdrnum;
    }
    public int getChdrCurrfrom() {
        return chdrCurrfrom;
    }
    public void setChdrCurrfrom(int chdrCurrfrom) {
        this.chdrCurrfrom = chdrCurrfrom;
    }
    public String getChdrValidflag() {
        return chdrValidflag;
    }
    public void setChdrValidflag(String chdrValidflag) {
        this.chdrValidflag = chdrValidflag;
    }
    public String getChdrBillfreq() {
        return chdrBillfreq;
    }
    public void setChdrBillfreq(String chdrBillfreq) {
        this.chdrBillfreq = chdrBillfreq;
    }
    public BigDecimal getChdrSinstamt06() {
        return chdrSinstamt06;
    }
    public void setChdrSinstamt06(BigDecimal chdrSinstamt06) {
        this.chdrSinstamt06 = chdrSinstamt06;
    }
    public BigDecimal getChdrSinstamt01() {
        return chdrSinstamt01;
    }
    public void setChdrSinstamt01(BigDecimal chdrSinstamt01) {
        this.chdrSinstamt01 = chdrSinstamt01;
    }
    public String getChdrCnttype() {
        return chdrCnttype;
    }
    public void setChdrCnttype(String chdrCnttype) {
        this.chdrCnttype = chdrCnttype;
    }
    public String getChdrCowncoy() {
        return chdrCowncoy;
    }
    public void setChdrCowncoy(String chdrCowncoy) {
        this.chdrCowncoy = chdrCowncoy;
    }
    public String getChdrCownnum() {
        return chdrCownnum;
    }
    public void setChdrCownnum(String chdrCownnum) {
        this.chdrCownnum = chdrCownnum;
    }
    public String getChdrPayrnum() {
        return chdrPayrnum;
    }
    public void setChdrPayrnum(String chdrPayrnum) {
        this.chdrPayrnum = chdrPayrnum;
    }
    public String getChdrAgntcoy() {
        return chdrAgntcoy;
    }
    public void setChdrAgntcoy(String chdrAgntcoy) {
        this.chdrAgntcoy = chdrAgntcoy;
    }
    public String getChdrAgntnum() {
        return chdrAgntnum;
    }
    public void setChdrAgntnum(String chdrAgntnum) {
        this.chdrAgntnum = chdrAgntnum;
    }
    public int getChdrBtdate() {
        return chdrBtdate;
    }
    public void setChdrBtdate(int chdrBtdate) {
        this.chdrBtdate = chdrBtdate;
    }
    public int getChdrPtdate() {
        return chdrPtdate;
    }
    public void setChdrPtdate(int chdrPtdate) {
        this.chdrPtdate = chdrPtdate;
    }
    public String getChdrCntcurr() {
        return chdrCntcurr;
    }
    public void setChdrCntcurr(String chdrCntcurr) {
        this.chdrCntcurr = chdrCntcurr;
    }
}
