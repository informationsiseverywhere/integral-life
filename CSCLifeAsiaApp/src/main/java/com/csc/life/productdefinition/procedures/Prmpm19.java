/*
 * File: Prmpm19.java
 * Date: December 3, 2013 3:41:04 AM ICT
 * Author: CSC
 * 
 * Class transformed from PRMPM19.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Tr52rrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5533rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*          IRDA PREMIUM CALCULATION
*
* Note : Clone from ZRPRM03.
*
* This new program is similar to PRMPM03 except for the following
* changes :
*
*   This subroutine will be an item entry on T5675 and will be use  to
*   calculate the sum insured from a given premium, term and payme t
*   frequency.
*
*   A sum insured factor will be obtained from TR52R, using the pr mium
*   term. The factor is obtained from the appropriate term range.  he
*   total premium is multiplied by this factor and the sum insured thus
*   calculated.
*
*   The principal tables referenced by this subroutine are:
*
*        T5533, TR52R
*
*   Key inputs for this subroutine are:
*
*   Sum Insured / Instalment premium or single premium,
*   billing frequency,
*   sex,
*   age,
*   mortality class of joint life,
*   effective date,
*   outstanding term,
*   rating date
*
*   Output from this subroutine are:
*
*   Sum insured
*
*****************************************************************
* </pre>
*/
public class Prmpm19 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PRMPM19";

	private FixedLengthStringData wsaaT5533Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5533Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 0);
	private FixedLengthStringData wsaaT5533Currcode = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 4);

	private FixedLengthStringData wsaaTr52rKey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr52rCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52rKey, 0);
	private FixedLengthStringData wsaaTr52rBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaTr52rKey, 4);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private ZonedDecimalData wsaaRndfact = new ZonedDecimalData(6, 0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(7, 4);
	private PackedDecimalData wsaaQualPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotRegPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotSingPrem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaFreq = new FixedLengthStringData(2);

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaFreq, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(2, 0).isAPartOf(filler, 0).setUnsigned();
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 12).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler3, 13).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler5, 14).setUnsigned();

	private FixedLengthStringData filler7 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler7, 15);
		/* FORMATS */
	private static final String acblrec = "ACBLREC";
	private static final String itemrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";
		/* ERRORS */
	private static final String e031 = "E031";
	private static final String e308 = "E308";
	private static final String e563 = "E563";
	private static final String g070 = "G070";
	private static final String g071 = "G071";
	private static final String g072 = "G072";
		/* TABLES */
	private static final String t5533 = "T5533";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String tr52r = "TR52R";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5533rec t5533rec = new T5533rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52rrec tr52rrec = new Tr52rrec();
	private Premiumrec premiumrec = new Premiumrec();

	public Prmpm19() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*INIT*/
		premiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		main100();
		/*EXIT*/
		exitProgram();
	}

protected void main100()
	{
		para100();
	}

protected void para100()
	{
		initialize200();
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			readTablT5533250();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			readTablTr52r400();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			premiumMethods500();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			determineFactor600();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			a100ReadLext();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			suminRounded700();
		}
	}

protected void initialize200()
	{
		/*PARA*/
		wsaaFactor.set(ZERO);
		wsaaTotPremium.set(ZERO);
		wsaaRndfact.set(ZERO);
		wsaaSub.set(ZERO);
		/*EXIT*/
	}

protected void readTablT5533250()
	{
		para250();
	}

protected void para250()
	{
		/* Check premium against the limits set in T5533 according to*/
		/* the range limits for billing frequency.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5533);
		wsaaT5533Crtable.set(premiumrec.crtable);
		wsaaT5533Currcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5533Item);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(wsaaT5533Item, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5533)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			premiumrec.statuz.set(g071);
			return ;
		}
		else {
			t5533rec.t5533Rec.set(itdmIO.getGenarea());
		}
		/* Process to find MAX/MIN limits for frequency required.*/
		if (isNE(premiumrec.function, "TOPU")) {
			wsaaRndfact.set(t5533rec.rndfact);
			for (wsaaSub.set(1); !(isGT(wsaaSub, 8)
			|| isEQ(premiumrec.billfreq, t5533rec.frequency[wsaaSub.toInt()])); wsaaSub.add(1)){
				/*    If a range is not found or premium is out with the range*/
				/*    then error.*/
				/*    If this program is called by a INCREASE module, skip this*/
				/*    test because the CPRM-CALC-PREM holds the increased part of*/
				/*    the premium and not the actual premium.*/
				findBillfreq270();
			}
			if (isGT(wsaaSub, 8)) {
				premiumrec.statuz.set(g072);
			}
			else {
				if (((isLT(premiumrec.calcPrem, t5533rec.cmin[wsaaSub.toInt()]))
				|| (isGT(premiumrec.calcPrem, t5533rec.cmax[wsaaSub.toInt()])))
				&& isNE(premiumrec.function, "INCR")) {
					premiumrec.statuz.set(g070);
				}
			}
		}
		if (isEQ(premiumrec.function, "TOPU")
		&& ((isLT(premiumrec.calcPrem, t5533rec.casualContribMin)
		|| isGT(premiumrec.calcPrem, t5533rec.casualContribMax)))) {
			premiumrec.statuz.set(g070);
		}
	}

protected void findBillfreq270()
	{
		/*READ*/
		/*** Dummy paragraph to find billing frequency.*/
		/*EXIT*/
	}

protected void readTablTr52r400()
	{
		readTr52r410();
	}

protected void readTr52r410()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(tr52r);
		wsaaTr52rCrtable.set(premiumrec.crtable);
		if (isEQ(premiumrec.function, "TOPU")) {
			wsaaTr52rBillfreq.set("00");
		}
		else {
			wsaaTr52rBillfreq.set(premiumrec.billfreq);
		}
		itdmIO.setItemitem(wsaaTr52rKey);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(wsaaTr52rKey, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tr52r)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e031);
			fatalError9000();
		}
		else {
			tr52rrec.tr52rRec.set(itdmIO.getGenarea());
		}
	}

protected void premiumMethods500()
	{
		para510();
	}

protected void para510()
	{
		/* Derive the premium term*/
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		datcon3rec.function.set(SPACES);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		/* Get the term multiplier*/
		compute(wsaaTerm, 0).set(sub(tr52rrec.agelimit, premiumrec.lage));
		if (isLT(datcon3rec.freqFactor, wsaaTerm)) {
			wsaaTerm.set(datcon3rec.freqFactor);
		}
		if (isEQ(premiumrec.function, "TOPU")
		&& isNE(premiumrec.billfreq, "00")) {
			a200ReadT5688();
			a300ReadT5645();
			/* Get the regular premium collected to date from ACBL*/
			acblIO.setParams(SPACES);
			//if (isEQ(t5688rec.comlvlacc, "N")) {//COMMENTED MIBT-266
			if (isEQ(t5688rec.comlvlacc, " ")) {	
				wsaaRldgacct.set(premiumrec.chdrChdrnum);
			}
			else {
				wsaaRldgChdrnum.set(premiumrec.chdrChdrnum);
				wsaaRldgLife.set(premiumrec.lifeLife);
				wsaaRldgCoverage.set(premiumrec.covrCoverage);
				wsaaRldgRider.set(premiumrec.covrRider);
				wsaaRldgPlnsfx.set(premiumrec.plnsfx);
			}
			//if (isEQ(t5688rec.comlvlacc, "N")) {//COMMENTED MIBT-266
			if (isEQ(t5688rec.comlvlacc, " ")) {
				wsaaSub.set(1);
			}
			else {
				wsaaSub.set(2);
			}
			a400CalcPrem();
			wsaaTotRegPrem.set(acblIO.getSacscurbal());
			/* Get the single premium collected to date from ACBL*/
			acblIO.setParams(SPACES);
			//if (isEQ(t5688rec.comlvlacc, "N")) {//COMMENTED MIBT-266
			if (isEQ(t5688rec.comlvlacc, " ")) {
				wsaaSub.set(3);
			}
			else {
				wsaaSub.set(4);
			}
			a400CalcPrem();
			wsaaTotSingPrem.set(acblIO.getSacscurbal());
			compute(wsaaQualPrem, 4).set(mult(wsaaTotRegPrem, tr52rrec.dsifact));
			compute(wsaaTotPremium, 2).set(sub(add(wsaaTotSingPrem, premiumrec.calcPrem), wsaaQualPrem));
			/*  Aggregated top ups withinn the 25% (stored in the TR52R)*/
			/*  of the total regular premium received will not be eligible*/
			/*  for any guaranteed amount*/
			/*  This only applies for top up done via the single premium top*/
			/*  up submenu.Lump Sum at issue is excluded from this validation*/
			if (isLT(wsaaTotPremium, ZERO)) {
				wsaaTotPremium.set(ZERO);
			}
			return ;
		}
		if (isEQ(premiumrec.billfreq, "00")) {
			wsaaTotPremium.set(premiumrec.calcPrem);
		}
		else {
			wsaaFreq.set(premiumrec.billfreq);
			compute(wsaaTotPremium, 2).set(mult(premiumrec.calcPrem, wsaaFreqFactor));
		}
	}

protected void determineFactor600()
	{
		para610();
	}

protected void para610()
	{
		/* Find the sum assured factor which will determine the sum*/
		/* assured according to the Premium.*/
		wsaaSub.set(0);
		findFactor650();
		/*  WSAA-FACTOR = MAX (TR52R-FACTSAMN, MIN (TR52R-FACTSAMX,*/
		/*  TR52R-FACTORSA * WSAA-TERM)*/
		if (isGT(wsaaSub, 12)) {
			premiumrec.statuz.set(e563);
		}
		else {
			compute(wsaaFactor, 4).set(mult(tr52rrec.factorsa[wsaaSub.toInt()], wsaaTerm));
			if (isEQ(tr52rrec.factsamx[wsaaSub.toInt()], ZERO)) {
				tr52rrec.factsamx[wsaaSub.toInt()].set(999.9999);
			}
			if (isGT(wsaaFactor, tr52rrec.factsamx[wsaaSub.toInt()])) {
				wsaaFactor.set(tr52rrec.factsamx[wsaaSub.toInt()]);
			}
			if (isLT(wsaaFactor, tr52rrec.factsamn[wsaaSub.toInt()])) {
				wsaaFactor.set(tr52rrec.factsamn[wsaaSub.toInt()]);
			}
		}
	}

protected void findFactor650()
	{
		para660();
	}

protected void para660()
	{
		/* Routine to search through table TR52R to find factor accord-*/
		/* to the age input.*/
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 12)) {
			if (isLTE(wsaaTerm, tr52rrec.toterm[wsaaSub.toInt()])) {
				/*NEXT_SENTENCE*/
			}
			else {
				para660();
				return ;
			}
		}
		/*EXIT*/
	}

protected void suminRounded700()
	{
		para710();
	}

protected void para710()
	{
		/* Multiply the tot premium over the term by the factor found.*/
		compute(wsaaTotPremium, 4).set(mult(wsaaTotPremium, wsaaFactor));
		/* Round up the Sum insured amount to its nearest whole unit.*/
		wsaaRoundNum.set(wsaaTotPremium);
		if (isEQ(wsaaRndfact, 1)
		|| isEQ(wsaaRndfact, 0)) {
			if (isLT(wsaaRoundDec, .5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
				if (isEQ(wsaaRndfact, 10)) {
					if (isLT(wsaaRound1, 5)) {
						wsaaRound1.set(0);
					}
					else {
						wsaaRoundNum.add(10);
						wsaaRound1.set(0);
					}
				}
			}
		}
		if (isEQ(wsaaRndfact, 100)) {
			if (isLT(wsaaRound10, 50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(wsaaRndfact, 1000)) {
			if (isLT(wsaaRound100, 500)) {
				wsaaRound100.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound100.set(0);
			}
		}
		premiumrec.sumin.set(wsaaRoundNum);
	}

protected void a100ReadLext()
	{
		a110ReadLext();
	}

protected void a110ReadLext()
	{
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lextIO.setChdrnum(premiumrec.chdrChdrnum);
		lextIO.setLife(premiumrec.lifeLife);
		lextIO.setCoverage(premiumrec.covrCoverage);
		lextIO.setRider(premiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(lextIO.getStatuz(), varcom.oK)
		&& isEQ(premiumrec.chdrChdrcoy, lextIO.getChdrcoy())
		&& isEQ(premiumrec.chdrChdrnum, lextIO.getChdrnum())
		&& isEQ(premiumrec.lifeLife, lextIO.getLife())
		&& isEQ(premiumrec.covrCoverage, lextIO.getCoverage())
		&& isEQ(premiumrec.covrRider, lextIO.getRider())
		&& isNE(lextIO.getZnadjperc(), ZERO)) {
			compute(wsaaFactor, 5).setRounded(div(lextIO.getZnadjperc(), 100));
		}
	}

protected void a200ReadT5688()
	{
		a200ReadT5688Para();
	}

protected void a200ReadT5688Para()
	{
		/* Read T5688*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(premiumrec.cnttype);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(premiumrec.cnttype, itdmIO.getItemitem())
		|| isNE(itdmIO.getItemcoy(), premiumrec.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			premiumrec.statuz.set(e308);
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void a300ReadT5645()
	{
		a300ReadT5645Para();
	}

protected void a300ReadT5645Para()
	{
		/*    Read table T5645.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(premiumrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void a400CalcPrem()
	{
		a410Start();
	}

protected void a410Start()
	{
		/* Read ACBL file with given parameters*/
		acblIO.setRldgcoy(premiumrec.chdrChdrcoy);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setSacscode(t5645rec.sacscode[wsaaSub.toInt()]);
		acblIO.setSacstyp(t5645rec.sacstype[wsaaSub.toInt()]);
		acblIO.setOrigcurr(premiumrec.currcode);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			acblIO.setSacscurbal(ZERO);
		}
		if (isEQ(t5645rec.sign[wsaaSub.toInt()], "-")) {
			setPrecision(acblIO.getSacscurbal(), 2);
			acblIO.setSacscurbal(sub(0, acblIO.getSacscurbal()));
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
