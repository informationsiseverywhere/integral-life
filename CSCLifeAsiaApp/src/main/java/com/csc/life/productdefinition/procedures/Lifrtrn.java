/*
 * File: Lifrtrn.java
 * Date: 29 August 2009 22:58:12
 * Author: Quipoz Limited
 *
 * Class transformed from LIFRTRN.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.accounting.dataaccess.DglxTableDAM;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.accounting.tablestructures.Tr362rec;
import com.csc.fsu.clients.dataaccess.RtrnTableDAM;
import com.csc.fsu.general.procedures.Postyymm;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Postyymmr;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Getstamp;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*         Life system version of Subsidary Ledger Posting
*
* This  subroutine  updates  the  batch totals then writes an
* RTRN record. Depending on the calling function sub-accounts
* balance can also be posted to.
*
*  Functions:
*
*     PSTW  - Write the RTRN and update the SACS balance.
*     NPSTW - Write the RTRN record only.
*
*  Both functions update the running batch header totals kept
*  by calling BATCUP with a KEEPS function.
*
*  On completion of the transaction, BATCUP must be called with
*  a WRITS function to apply the accumulated totals to the
*  database.
*
*  Statuses:
*
*     **** - successful completion of routine
*     BOMB - database error
*     E049 - Invalid linkage into subroutine
*
*  Linkage Area:
*
*  LIFR-BATCKEY   The key of the opened batch.
*  LIFR-RDOCNUM   Document number for this record. Varies
*                 with the calling subsystem.
*  LIFA-TRANNO    This is the sequence number of the transaction
*                 being done to the document
*  LIFA-JRNSEQ    This is the sequence number user to uniquely
*                 identify a specific manual journal posting.
*  LIFR-RLDGCOY   Ledger kay for this record. Varies with
*  LIFR_RLDGACCT  the calling subsystem.
*  LIFR-GENLCOY   General ledger key. Set up from T5645 prior
*  LIFR_GLCODE    to calling this routine.
*  LIFR-GLSIGN
*  LIFR-CONTOT
*  LIFR-SACSCODE
*  LIFR-SACSTYP
*  LIFR-TRANREF   Internal reference number (e.g. secondary
*                 key). Varies with the calling subsystem.
*  LIFR-TRANDESC  Meaningful description of tranaction.
*  LIFR-ORIGCURR  Original (foreign) currency.
*  LIFR-ACCTCCY   Accounting (local) currency.
*  LIFR-CRATE     Exchange rate at time of transaction between
*                 original and accounting currency.
*  LIFR-EFFDATE   The effective date of the transaction.
*  LIFR-ORIGAMT   The transaction amount in original currency.
*  LIFR-ACCTAMT   The transaction amount in accounting currency.
*  LIFR-SUBSTITUTE-CODES
*                 Used to substitute in GL code if passed
*                     1 - @@@ - Contract type
*                     2 - ***
*                     3 - %%%
*                     4 - !!!
*                     5 - +++
*  "LIFR-TRANID"  Date/time stamp
*
/
***********************************************************************
*                                                                     *
* </pre>
*/
public class Lifrtrn extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("LIFRTRN");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* ERRORS */
	private static final String e049 = "E049";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String tr362 = "TR362";
		/* FORMATS */
	private static final String rtrnrec = "RTRNREC";
	private static final String itemrec = "ITEMREC";
	private static final String dglxrec = "DGLXREC";
	private ZonedDecimalData wsaaPost = new ZonedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO);

	private FixedLengthStringData wsaaMachineTimex = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaMachineTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaMachineTimex, 0).setUnsigned();
		/* WSAA-TRANSACTION-DATE */
	private ZonedDecimalData wsaaTransDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaTranTestDate = new FixedLengthStringData(8).isAPartOf(wsaaTransDate, 0, REDEFINE);
	private ZonedDecimalData wsaaTranDateCc = new ZonedDecimalData(2, 0).isAPartOf(wsaaTranTestDate, 0).setUnsigned();
	private ZonedDecimalData wsaaTranDateYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaTranTestDate, 2).setUnsigned();
	private ZonedDecimalData wsaaTranseq = new ZonedDecimalData(4, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(4).isAPartOf(wsaaTranseq, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaTranseqa = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	private FixedLengthStringData wsaaUsrprf = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaJobnm = new FixedLengthStringData(10);
	private PackedDecimalData wsaaX = new PackedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaLastOrigCcy = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();	
	private DglxTableDAM dglxIO = new DglxTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RtrnTableDAM rtrnIO = new RtrnTableDAM();
	private Postyymmr postyymmr = new Postyymmr();
	private Batcuprec batcuprec = new Batcuprec();
	private T3629rec t3629rec = new T3629rec();
	private Tr362rec tr362rec = new Tr362rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Syserrrec syserrrec = new Syserrrec();        /* MLIL-COA, change access modifier from private to protected */
	protected Lifrtrnrec lifrtrnrec = new Lifrtrnrec();  /* MLIL-COA, change access modifier from private to protected */
	protected AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); 
	protected Acblpf acblpf = null;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		reRead1120,
		exit1190
	}

	public Lifrtrn() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lifrtrnrec.lifrtrnRec = convertAndSetParam(lifrtrnrec.lifrtrnRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
			para011();
			exit090();
		}

protected void para011()
	{
		lifrtrnrec.statuz.set("****");
		syserrrec.subrname.set(wsaaProg);
		if (isNE(lifrtrnrec.function,"PSTW ")
		&& isNE(lifrtrnrec.function,"NPSTW")) {
			lifrtrnrec.statuz.set(e049);
			return ;
		}
		checkSchedule24x7Setup4000();
		writeRtrn1000();
		if (isEQ(lifrtrnrec.function,"PSTW ")
		&& isNE(lifrtrnrec.sacscode,SPACES)) {
			updateAcbl2000();
		}
		callBatcup3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void writeRtrn1000()
	{
		glSubstitution1010();
	}

protected void glSubstitution1010()
	{
	    glSubstitution1011();   //COA
		
		/*  If accounting currency details not entered,*/
		/*   and the original currency has changed since the last time*/
		/*     - look up the nominal exchange rate.*/
		if (isEQ(lifrtrnrec.genlcur,SPACES)
		&& isNE(lifrtrnrec.origcurr,wsaaLastOrigCcy)) {
			getRate1100();
		}
		/*    Get the machine date and time to stamp RTRNs*/
		wsaaMachineTimex.set(getCobolTime());
		/*  Write movement record.*/
		rtrnIO.setBatcpfx("BA");
		rtrnIO.setBatccoy(lifrtrnrec.batccoy);
		rtrnIO.setBatcbrn(lifrtrnrec.batcbrn);
		rtrnIO.setBatcactyr(lifrtrnrec.batcactyr);
		rtrnIO.setBatcactmn(lifrtrnrec.batcactmn);
		rtrnIO.setBatctrcde(lifrtrnrec.batctrcde);
		rtrnIO.setBatcbatch(lifrtrnrec.batcbatch);
		rtrnIO.setRdocnum(lifrtrnrec.rdocnum);
		rtrnIO.setJrnseq(lifrtrnrec.jrnseq);
		wsaaTranseq.set(lifrtrnrec.jrnseq);
		rtrnIO.setTranseq(wsaaTranseqa);
		rtrnIO.setRdocpfx("CA");
		rtrnIO.setRdoccoy(lifrtrnrec.batccoy);
		rtrnIO.setRdocnum(lifrtrnrec.rdocnum);
		if (isEQ(lifrtrnrec.sacscode,"LP")) {
			rtrnIO.setRdocpfx("CH");
		}
		if (isEQ(lifrtrnrec.sacscode,"SC")) {
			rtrnIO.setRdocpfx("SC");
		}
		rtrnIO.setRldgcoy(lifrtrnrec.rldgcoy);
		rtrnIO.setSacscode(lifrtrnrec.sacscode);
		rtrnIO.setRldgacct(lifrtrnrec.rldgacct);
		rtrnIO.setOrigccy(lifrtrnrec.origcurr);
		rtrnIO.setSacstyp(lifrtrnrec.sacstyp);
		rtrnIO.setOrigamt(lifrtrnrec.origamt);
		rtrnIO.setTrandesc(lifrtrnrec.trandesc);
		rtrnIO.setCrate(lifrtrnrec.crate);
		rtrnIO.setAcctamt(lifrtrnrec.acctamt);
		rtrnIO.setGenlpfx("GE");
		rtrnIO.setGenlcoy(lifrtrnrec.genlcoy);
		rtrnIO.setGenlcur(lifrtrnrec.genlcur);
		rtrnIO.setGlcode(lifrtrnrec.glcode);
		rtrnIO.setGlsign(lifrtrnrec.glsign);
		postyymmr.function.set("GET");
		postyymmr.batccoy.set(lifrtrnrec.batccoy);
		postyymmr.batcbrn.set(lifrtrnrec.batcbrn);
		postyymmr.batcactyr.set(lifrtrnrec.batcactyr);
		postyymmr.batcactmn.set(lifrtrnrec.batcactmn);
		postyymmr.batctrcde.set(lifrtrnrec.batctrcde);
		postyymmr.batcbatch.set(lifrtrnrec.batcbatch);
		postyymmr.effdate.set(lifrtrnrec.effdate);
		callProgram(Postyymm.class, postyymmr.postyymmRec);
		if (isNE(postyymmr.statuz,varcom.oK)) {
			syserrrec.params.set(postyymmr.postyymmRec);
			syserrrec.statuz.set(postyymmr.statuz);
			syserr570();
		}
		rtrnIO.setPostyear(postyymmr.postyear);
		rtrnIO.setPostmonth(postyymmr.postmonth);
		rtrnIO.setEffdate(lifrtrnrec.effdate);
		rtrnIO.setTranno(lifrtrnrec.tranno);
		if (isEQ(wsaaToday,ZERO)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaTransDate.set(datcon1rec.intDate);
			wsaaToday.set(datcon1rec.intDate);
		}
		else {
			wsaaTransDate.set(wsaaToday);
		}
		if (isEQ(wsaaTranDateCc,ZERO)) {
			if (isGT(wsaaTranDateYy,90)) {
				wsaaTranDateCc.set(19);
			}
			else {
				wsaaTranDateCc.set(20);
			}
		}
		rtrnIO.setTrandate(wsaaTransDate);
		rtrnIO.setTrantime(wsaaMachineTime);
		if (isEQ(lifrtrnrec.genlcur,SPACES)) {
			rtrnIO.setCrate(wsaaNominalRate);
			rtrnIO.setGenlcur(wsaaLedgerCcy);
			/*  Do not forget to round the ACCTAMT*/
			setPrecision(rtrnIO.getAcctamt(), 10);
			rtrnIO.setAcctamt(mult(lifrtrnrec.origamt,wsaaNominalRate), true);
			zrdecplrec.amountIn.set(rtrnIO.getAcctamt());
			zrdecplrec.currency.set(rtrnIO.getGenlcur());
			a000CallRounding();
			rtrnIO.setAcctamt(zrdecplrec.amountOut);
		}
		zrdecplrec.amountIn.set(rtrnIO.getOrigamt());
		zrdecplrec.currency.set(lifrtrnrec.origcurr);
		a000CallRounding();
		rtrnIO.setOrigamt(zrdecplrec.amountOut);
		rtrnIO.setFormat(rtrnrec);
		rtrnIO.setFunction("WRITR");
		if (isEQ(rtrnIO.getPostmonth(),SPACES)) {
			wsaaPost.set(rtrnIO.getBatcactmn());
			rtrnIO.setPostmonth(wsaaPost);
		}
		if (isEQ(rtrnIO.getPostyear(),SPACES)) {
			wsaaPost.set(rtrnIO.getBatcactyr());
			rtrnIO.setPostyear(wsaaPost);
		}
		SmartFileCode.execute(appVars, rtrnIO);
		if (isNE(rtrnIO.getStatuz(),"****")) {
			syserrrec.params.set(rtrnIO.getParams());
			dbError580();
		}
		if (isEQ(tr362rec.appflag, "Y")) {
			diaryUpdate5000();
		}
	}

protected void getRate1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readCurrencyRecord1110();
				case reRead1120:
					reRead1120();
					findRate1150();
				case exit1190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCurrencyRecord1110()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lifrtrnrec.batccoy);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(lifrtrnrec.origcurr);
	}

protected void reRead1120()
	{
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLastOrigCcy.set(lifrtrnrec.origcurr);
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaNominalRate,ZERO)
		|| isGT(wsaaX,7))) {
			findRate1150();
		}

		if (isEQ(wsaaNominalRate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.reRead1120);
			}
			else {
				syserrrec.statuz.set("MRNF");
				syserr570();
			}
		}
		else {
			goTo(GotoLabel.exit1190);
		}
	}

protected void findRate1150()
	{
		if (isGTE(lifrtrnrec.effdate,t3629rec.frmdate[wsaaX.toInt()])
		&& isLTE(lifrtrnrec.effdate,t3629rec.todate[wsaaX.toInt()])) {
			/*    MOVE T3629-EXRATE (WSAA-X) TO WSAA-NOMINAL-RATE*/
			wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
		}
		else {
			wsaaX.add(1);
		}
	}

protected void updateAcbl2000()
{
		boolean insertFlag = false;
		acblpf = acblDao.getAcblpfRecord(lifrtrnrec.rldgcoy.toString().trim(), lifrtrnrec.sacscode.toString().trim(),
					lifrtrnrec.rldgacct.toString(),	lifrtrnrec.sacstyp.toString(),lifrtrnrec.origcurr.toString().trim());  	
		if (acblpf == null) { 
			acblpf =  new Acblpf(); 
			acblpf.setRldgcoy(lifrtrnrec.rldgcoy.toString());
			/* MOVE SCPT-CHDRNUM           TO ACBL-RLDGACCT.                */
			acblpf.setRldgacct(lifrtrnrec.rldgacct.toString());
			acblpf.setOrigcurr(lifrtrnrec.origcurr.toString());
			acblpf.setSacscode(lifrtrnrec.sacscode.toString());
			acblpf.setSacstyp(lifrtrnrec.sacstyp.toString());
			acblpf.setSacscurbal(BigDecimal.ZERO);
			insertFlag = true;
		}
		
		setPrecision(acblpf.getSacscurbal(), 2);
		if (isEQ(lifrtrnrec.glsign, "-")) {			
			acblpf.setSacscurbal(new BigDecimal((sub(acblpf.getSacscurbal(),lifrtrnrec.origamt).toString())));
		}
		else {			
			acblpf.setSacscurbal(new BigDecimal((add(acblpf.getSacscurbal(),lifrtrnrec.origamt).toString())));
		}
			
		
		zrdecplrec.amountIn.set(acblpf.getSacscurbal());
		zrdecplrec.currency.set(acblpf.getOrigcurr());
		a000CallRounding();
		acblpf.setSacscurbal(zrdecplrec.amountOut.getbigdata());
		
		List<Acblpf> acblpfList = new ArrayList<Acblpf>();
		acblpfList.add(acblpf);
		if(insertFlag){
			acblDao.insertAcblpfList(acblpfList);
		}else{
			acblDao.updateAcblRecord(acblpfList);
		}			
		
	}

protected void callBatcup3000()
	{
		batcup3000();
	}

protected void batcup3000()
	{
		batcuprec.batcpfx.set("BA");
		batcuprec.batccoy.set(lifrtrnrec.batccoy);
		batcuprec.batcbrn.set(lifrtrnrec.batcbrn);
		batcuprec.batcactyr.set(lifrtrnrec.batcactyr);
		batcuprec.batcactmn.set(lifrtrnrec.batcactmn);
		batcuprec.batctrcde.set(lifrtrnrec.batctrcde);
		batcuprec.batcbatch.set(lifrtrnrec.batcbatch);
		batcuprec.trancnt.set(0);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(lifrtrnrec.contot);
		batcuprec.ascnt.set(0);
		batcuprec.bcnt.set(1);
		/* MOVE LIFR-ACCTAMT           TO BCUP-BVAL.                    */
		batcuprec.bval.set(rtrnIO.getAcctamt());
		batcuprec.function.set("KEEPS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,"****")) {
			lifrtrnrec.statuz.set(batcuprec.statuz);
			syserrrec.statuz.set(batcuprec.statuz);
			syserr570();
		}
	}

protected void checkSchedule24x7Setup4000()
	{
		begin4010();
	}

protected void begin4010()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lifrtrnrec.batccoy);
		itemIO.setItemseq(SPACES);
		itemIO.setItemtabl(tr362);
		itemIO.setItemitem("***");
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			tr362rec.tr362Rec.set(itemIO.getGenarea());
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				tr362rec.tr362Rec.set(SPACES);
			}
			else {
				lifrtrnrec.statuz.set(itemIO.getStatuz());
				syserrrec.statuz.set(itemIO.getStatuz());
				syserr570();
			}
		}
	}

protected void diaryUpdate5000()
	{
		update5100();
	}

protected void update5100()
	{
		if (isEQ(rtrnIO.getOrigamt(), ZERO)
		&& isEQ(rtrnIO.getAcctamt(), ZERO)) {
			return ;
		}
		if (isEQ(rtrnIO.getGlcode(), SPACES)) {
			return ;
		}
		dglxIO.setParams(SPACES);
		dglxIO.setBatccoy(rtrnIO.getBatccoy());
		dglxIO.setBatcbrn(rtrnIO.getBatcbrn());
		dglxIO.setBatcactyr(rtrnIO.getBatcactyr());
		dglxIO.setBatcactmn(rtrnIO.getBatcactmn());
		dglxIO.setBatctrcde(rtrnIO.getBatctrcde());
		dglxIO.setBatcbatch(rtrnIO.getBatcbatch());
		dglxIO.setGenlcoy(rtrnIO.getGenlcoy());
		dglxIO.setGenlcur(rtrnIO.getGenlcur());
		dglxIO.setGenlcde(rtrnIO.getGlcode());
		dglxIO.setEffdate(rtrnIO.getEffdate());
		dglxIO.setOrigcurr(rtrnIO.getOrigccy());
		dglxIO.setPostyear(rtrnIO.getPostyear());
		dglxIO.setPostmonth(rtrnIO.getPostmonth());
		dglxIO.setRldgacct(rtrnIO.getRldgacct());
		if (isEQ(rtrnIO.getGlsign(), "-")) {
			setPrecision(dglxIO.getOrigamt(), 2);
			dglxIO.setOrigamt(sub(0, rtrnIO.getOrigamt()));
			setPrecision(dglxIO.getAcctamt(), 2);
			dglxIO.setAcctamt(sub(0, rtrnIO.getAcctamt()));
		}
		else {
			dglxIO.setOrigamt(rtrnIO.getOrigamt());
			dglxIO.setAcctamt(rtrnIO.getAcctamt());
		}
		callProgram(Getstamp.class, wsaaUsrprf, wsaaJobnm, dglxIO.getCrtdatime());
		dglxIO.setFunction(varcom.writr);
		dglxIO.setFormat(dglxrec);
		SmartFileCode.execute(appVars, dglxIO);
		if (isNE(dglxIO.getStatuz(), varcom.oK)) {
			lifrtrnrec.statuz.set(dglxIO.getStatuz());
			syserrrec.statuz.set(dglxIO.getStatuz());
			syserr570();
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(lifrtrnrec.batccoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(lifrtrnrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr570();
		}
		/*A900-EXIT*/
	}

//COA

protected void glSubstitution1011() {
	
	lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "?", lifrtrnrec.batccoy));
	lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "##", lifrtrnrec.batcbrn));
	lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "&&&", lifrtrnrec.origcurr));
	if (isNE(lifrtrnrec.substituteCode[1],SPACES)) {
		lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "@@@", lifrtrnrec.substituteCode[1]));
	}
	if (isNE(lifrtrnrec.substituteCode[2],SPACES)) {
		lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "***", lifrtrnrec.substituteCode[2]));
	}
	if (isNE(lifrtrnrec.substituteCode[3],SPACES)) {
		lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "%%%", lifrtrnrec.substituteCode[3]));
	}
	if (isNE(lifrtrnrec.substituteCode[4],SPACES)) {
		lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "!!!", lifrtrnrec.substituteCode[4]));
	}
	if (isNE(lifrtrnrec.substituteCode[5],SPACES)) {
		lifrtrnrec.glcode.set(inspectReplaceAll(lifrtrnrec.glcode, "+++", lifrtrnrec.substituteCode[5]));
	}
	
}

}
