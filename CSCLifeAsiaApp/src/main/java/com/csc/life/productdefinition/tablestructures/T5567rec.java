package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:00
 * Description:
 * Copybook name: T5567REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5567rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5567Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData billfreqs = new FixedLengthStringData(20).isAPartOf(t5567Rec, 0);
  	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(10, 2, billfreqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData billfreq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData billfreq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData billfreq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData billfreq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData billfreq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData billfreq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData billfreq07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData billfreq08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData billfreq09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData billfreq10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData cntfees = new FixedLengthStringData(170).isAPartOf(t5567Rec, 20);
  	public ZonedDecimalData[] cntfee = ZDArrayPartOfStructure(10, 17, 2, cntfees, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(170).isAPartOf(cntfees, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cntfee01 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData cntfee02 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 17);
  	public ZonedDecimalData cntfee03 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 34);
  	public ZonedDecimalData cntfee04 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 51);
  	public ZonedDecimalData cntfee05 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 68);
  	public ZonedDecimalData cntfee06 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData cntfee07 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 102);
  	public ZonedDecimalData cntfee08 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 119);
  	public ZonedDecimalData cntfee09 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 136);
  	public ZonedDecimalData cntfee10 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 153);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(310).isAPartOf(t5567Rec, 190, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5567Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5567Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}