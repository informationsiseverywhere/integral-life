package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br64xDAO extends BaseDAO<MedxTemppf> {
	public List<MedxTemppf> loadDataByBatch(int batchID);
	public int populateBr64xTemp(int batchExtractSize, String medxtempTable, String chdrfrom, String chdrto, String paidBy);
}
