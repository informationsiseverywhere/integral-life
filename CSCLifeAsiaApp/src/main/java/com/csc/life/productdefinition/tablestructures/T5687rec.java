package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:58
 * Description:
 * Copybook name: T5687REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5687rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5687Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData anniversaryMethod = new FixedLengthStringData(4).isAPartOf(t5687Rec, 0);
  	public FixedLengthStringData basicCommMeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 4);
  	public FixedLengthStringData bascpy = new FixedLengthStringData(4).isAPartOf(t5687Rec, 8);
  	public FixedLengthStringData basscmth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 12);
  	public FixedLengthStringData basscpy = new FixedLengthStringData(4).isAPartOf(t5687Rec, 16);
  	public FixedLengthStringData bastcmth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 20);
  	public FixedLengthStringData bastcpy = new FixedLengthStringData(4).isAPartOf(t5687Rec, 24);
  	public FixedLengthStringData bbmeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 28);
  	public FixedLengthStringData dcmeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 32);
  	public FixedLengthStringData defFupMeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 36);
  	public FixedLengthStringData jlPremMeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 40);
  	public FixedLengthStringData jlifePresent = new FixedLengthStringData(1).isAPartOf(t5687Rec, 44);
  	public FixedLengthStringData loanmeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 45);
  	public FixedLengthStringData maturityCalcMeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 49);
  	public FixedLengthStringData nonForfeitMethod = new FixedLengthStringData(4).isAPartOf(t5687Rec, 53);
  	public FixedLengthStringData partsurr = new FixedLengthStringData(4).isAPartOf(t5687Rec, 57);
  	public ZonedDecimalData premGuarPeriod = new ZonedDecimalData(3, 0).isAPartOf(t5687Rec, 61);
  	public FixedLengthStringData premmeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 64);
  	public FixedLengthStringData pumeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 68);
  	public FixedLengthStringData reptcds = new FixedLengthStringData(18).isAPartOf(t5687Rec, 72);
  	public FixedLengthStringData[] reptcd = FLSArrayPartOfStructure(6, 3, reptcds, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(reptcds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData reptcd01 = new FixedLengthStringData(3).isAPartOf(filler, 0);
  	public FixedLengthStringData reptcd02 = new FixedLengthStringData(3).isAPartOf(filler, 3);
  	public FixedLengthStringData reptcd03 = new FixedLengthStringData(3).isAPartOf(filler, 6);
  	public FixedLengthStringData reptcd04 = new FixedLengthStringData(3).isAPartOf(filler, 9);
  	public FixedLengthStringData reptcd05 = new FixedLengthStringData(3).isAPartOf(filler, 12);
  	public FixedLengthStringData reptcd06 = new FixedLengthStringData(3).isAPartOf(filler, 15);
  	public FixedLengthStringData riind = new FixedLengthStringData(1).isAPartOf(t5687Rec, 90);
  	public FixedLengthStringData rnwcpy = new FixedLengthStringData(4).isAPartOf(t5687Rec, 91);
  	public ZonedDecimalData rtrnwfreq = new ZonedDecimalData(2, 0).isAPartOf(t5687Rec, 95);
  	public FixedLengthStringData schedCode = new FixedLengthStringData(12).isAPartOf(t5687Rec, 97);
  	public FixedLengthStringData sdtyPayMeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 109);
  	public FixedLengthStringData singlePremInd = new FixedLengthStringData(1).isAPartOf(t5687Rec, 113);
  	public FixedLengthStringData srvcpy = new FixedLengthStringData(4).isAPartOf(t5687Rec, 114);
  	public FixedLengthStringData statFund = new FixedLengthStringData(1).isAPartOf(t5687Rec, 118);
  	public FixedLengthStringData stampDutyMeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 119);
  	public FixedLengthStringData statSect = new FixedLengthStringData(2).isAPartOf(t5687Rec, 123);
  	public FixedLengthStringData statSubSect = new FixedLengthStringData(4).isAPartOf(t5687Rec, 125);
  	public FixedLengthStringData svMethod = new FixedLengthStringData(4).isAPartOf(t5687Rec, 129);
  	public FixedLengthStringData xfreqAltBasis = new FixedLengthStringData(4).isAPartOf(t5687Rec, 133);
  	public FixedLengthStringData zrorcmrg = new FixedLengthStringData(4).isAPartOf(t5687Rec, 137);
  	public FixedLengthStringData zrorcmsp = new FixedLengthStringData(4).isAPartOf(t5687Rec, 141);
  	public FixedLengthStringData zrorcmtu = new FixedLengthStringData(4).isAPartOf(t5687Rec, 145);
  	public FixedLengthStringData zrorpmrg = new FixedLengthStringData(4).isAPartOf(t5687Rec, 149);
  	public FixedLengthStringData zrorpmsp = new FixedLengthStringData(4).isAPartOf(t5687Rec, 153);
  	public FixedLengthStringData zrorpmtu = new FixedLengthStringData(4).isAPartOf(t5687Rec, 157);
  	public FixedLengthStringData zrrcombas = new FixedLengthStringData(1).isAPartOf(t5687Rec, 161);
  	public FixedLengthStringData zsbsmeth = new FixedLengthStringData(4).isAPartOf(t5687Rec, 162);
  	public FixedLengthStringData zsredtrm = new FixedLengthStringData(1).isAPartOf(t5687Rec, 166);
  	public FixedLengthStringData zszprmcd = new FixedLengthStringData(1).isAPartOf(t5687Rec, 167);
  	public FixedLengthStringData lnkgind = new FixedLengthStringData(1).isAPartOf(t5687Rec, 168);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(331).isAPartOf(t5687Rec, 169, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5687Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5687Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}