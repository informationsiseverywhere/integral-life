/*
 * File: Ph5a6.java
 * Date: 30 August 2009 1:40:33
 * Author: Quipoz Limited
 *
 * Class transformed from Ph5a6.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.ArrayList;
import java.util.List;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.screens.Sh5a6ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!
*REMARKS.
*
* This program is the capture Tentative approval Update.
*
* Introduce Tentative Approval as a Separate Transaction.
*

*****************************************************************
* </pre>
*/
public class Ph5a6 extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH5A6");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaWorkArea = new FixedLengthStringData(90);
	protected FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8).isAPartOf(wsaaWorkArea, 0);
	protected FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2).isAPartOf(wsaaWorkArea, 8);
	protected FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1).isAPartOf(wsaaWorkArea, 10);
	protected FixedLengthStringData wsaaName = new FixedLengthStringData(50).isAPartOf(wsaaWorkArea, 11);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).isAPartOf(wsaaWorkArea, 61).setUnsigned();
	protected FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaWorkArea, 79);
	protected FixedLengthStringData wsaaItem = new FixedLengthStringData(8).isAPartOf(wsaaWorkArea, 82);

	private static final String E032 = "E032";
	private static final String HL03 = "HL03";
	private static final String HL01 = "HL01";
	private static final String RUL2 = "RUL2";
	private static final String H039 = "H039";
	protected FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1 = new Datcon1rec();
	protected Namadrsrec namadrsrec = new Namadrsrec();
	private Sh5a6ScreenVars sv = ScreenProgram.getScreenVars( Sh5a6ScreenVars.class);

	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private Lifepf lifepf = new Lifepf();
	protected Descpf descpf;
	protected DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);

	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private String t5679 = "T5679";
	private T5679rec t5679rec = new T5679rec();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private Chdrpf chdrpf = null;
	private Ptrnpf ptrnpf = new Ptrnpf();
	private List<Ptrnpf> ptrnpfList = null;
	private Sftlockrec sftlockrec = new Sftlockrec();
	private FixedLengthStringData wsaa1Pflag = new FixedLengthStringData(1).init(SPACES);
	private Gensswrec gensswrec = new Gensswrec();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	protected FormatsInner formatsInner = new FormatsInner();
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	
	public Ph5a6() {
		super();
		screenVars = sv;
		new ScreenModel("Sh5a6", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			throw e;
		}
	}

	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			throw e;
		}
	}

	@Override
	protected void initialise1000()
	{
		initialise1010();
		retrvChdrlnb1020();
		getHpadrec1030();
		getCtypedes1040();
		getLinsname1050();
		getJlinsname1055();
		getCownname1060();
		getJownname1065();
		displayFld1070();
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		wsaa1Pflag.set("X");
	}

	protected void initialise1010()
	{	
		initialize(sv.dataArea);
		initialize(wsaaWorkArea);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[Varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		wsaaBatckey.set(wsspcomn.batchkey);
		datcon1.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1.datcon1Rec);
		sv.huwdcdte.set(datcon1.intDate);
		wsaaToday.set(datcon1.intDate);
	}


	protected void retrvChdrlnb1020()
	{
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		wsaaCnttype.set(chdrlnbIO.getCnttype());
		
	}

	protected void getHpadrec1030()
	{
		hpadIO.setDataKey(SPACES);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFunction("READR");
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),Varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
	}

	protected void getCtypedes1040()
	{
	
		descpf=descDAO.getdescData("IT", "T5688", chdrlnbIO.getCnttype().toString(),
				wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
	}

	protected void getLinsname1050()
	{
		lifepf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		lifepf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		lifepf.setLife("01");
		lifepf.setJlife(ZERO.stringValue(2));
		lifepf = lifepfDAO.getLifepf(lifepf);
		if (null==lifepf) {
			fatalError600();
		}
		wsaaClntNumber.set(lifepf.getLifcnum());
		wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
		wsaaClntCompany.set(chdrlnbIO.getCowncoy());
		getName1100();
		sv.lifcnum.set(lifepf.getLifcnum());
		sv.linsname.set(wsaaName);
	}

	protected void getJlinsname1055()
	{
		lifepf = new Lifepf();
		lifepf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		lifepf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		lifepf.setLife("01");
		lifepf.setJlife("01");
		lifepf = lifepfDAO.getLifepf(lifepf);
		if (null == lifepf) {
			fatalError600();
		}
		else {
			wsaaClntNumber.set(sv.lifcnum);
			wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
			wsaaClntCompany.set(chdrlnbIO.getCowncoy());
			getName1100();
			sv.jlifcnum.set(sv.lifcnum);
			sv.jlinsname.set(wsaaName);
		}
	}

	protected void getCownname1060()
	{
		wsaaClntNumber.set(chdrlnbIO.getCownnum());
		wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
		wsaaClntCompany.set(chdrlnbIO.getCowncoy());
		getName1100();
		sv.cownnum.set(chdrlnbIO.getCownnum());
		sv.ownername.set(wsaaName);
	}

	protected void getJownname1065()
	{
		if (isNE(chdrlnbIO.getJownnum(),SPACES)) {
			wsaaClntNumber.set(chdrlnbIO.getJownnum());
			wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
			wsaaClntCompany.set(chdrlnbIO.getCowncoy());
			getName1100();
			sv.jownnum.set(chdrlnbIO.getJownnum());
			sv.jownname.set(wsaaName);
		}
		else {
			sv.jownname.set(SPACES);
		}
	}

	protected void displayFld1070()
	{
		sv.occdate.set(chdrlnbIO.getOccdate());
		sv.hpropdte.set(hpadIO.getHpropdte());
		sv.hprrcvdt.set(hpadIO.getHprrcvdt());
		if (isEQ(wsspcomn.flag,"L")) {	//IBPLIFE-2472
			sv.huwdcdte.set(hpadIO.getHuwdcdte());
			sv.huwdcdteOut[Varcom.pr.toInt()].set("Y");
		}
		if(isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")){
			if (isNE(chdrlnbIO.getAvlisu(),"Y")) {				
				sv.chdrnumErr.set(RUL2);
			}
			if (isNE(sv.errorIndicators,SPACES)) {
				wsspcomn.edterror.set("Y");
			}
		}
	}


	protected void getName1100()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,Varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		wsaaName.set(namadrsrec.name);
	}

	protected void preScreenEdit()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(Varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}
		return ;
	}

	@Override
	protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

	protected void screenIo2010()
	{
		wsspcomn.edterror.set(Varcom.oK);
	}

	protected void validate2020()
	{
		if (isNE(sv.huwdcdte,varcom.vrcmMaxDate)) {
			if (isEQ(sv.huwdcdte,0)) {
				sv.huwdcdteErr.set(E032);
			}
			else {
				if (isLT(sv.huwdcdte,sv.hprrcvdt)) {
					sv.huwdcdteErr.set(HL03);
				}
				if (isGT(sv.huwdcdte,wsaaToday)) {
					sv.huwdcdteErr.set(HL01);
				}
			}
		}	
		checkValidations();
	}

	protected void checkValidations() {
		if((isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")||isEQ(wsaa1Pflag, "+"))) {	
			if (isNE(chdrlnbIO.getAvlisu(),"Y")) {				
				sv.chdrnumErr.set(RUL2);
			}
			if (isNE(sv.errorIndicators,SPACES)) {
				wsspcomn.edterror.set("Y");
			}
			if (isEQ(scrnparams.statuz, "CALC")) {
				wsspcomn.edterror.set("Y");
			}
		}
	}
	
	protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	@Override
	protected void update3000()
	{
		if ((isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")||isEQ(wsaa1Pflag, "+"))) {
			return ;	
		}
		updateChdrpf3010();
		updateHpad3020();
		createPtrn3030();
		rlseSoftlock3070();
	}

	protected void readT56793100()
	{	
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (null != itempf) {
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

	protected void updateChdrpf3010()
	{
		readT56793100();
		
		chdrpf = chdrpfDAO.updateChdrpfStatcodePstcde(wsspcomn.company.toString(), sv.chdrnum.toString(), 
					t5679rec.setCnRiskStat.toString(), t5679rec.setCnPremStat.toString());
		
		if (chdrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat(sv.chdrnum.toString()));
			fatalError600();
		}
	}

	protected void updateHpad3020()
	{
	    hpadpfDAO.updateTntapdate(wsspcomn.company.toString(), sv.chdrnum.toString(), sv.huwdcdte.toInt());
	}
	
	protected void createPtrn3030()
	{

		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());		
		ptrnpf.setTranno(chdrpf.getTranno());
		ptrnpf.setPtrneff(sv.huwdcdte.toInt());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setValidflag("1");
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setChdrpfx("  ");
		ptrnpf.setPrtflg(" ");
		ptrnpf.setCrtuser(" ");
		ptrnpf.setRecode(" ");
		
		ptrnpfList = new ArrayList<>();
		ptrnpfList.add(ptrnpf);
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
	}
	
	protected void rlseSoftlock3070()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrpf.getChdrcoy());
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set(chdrpf.getChdrpfx());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,Varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}
	
	@Override
	protected void whereNext4000()
	{
		String tbgd = "TBGD";
		if(tbgd.equals(wsaaBatckey.batcBatctrcde.toString())){		
			nextProgram4010();		
		}
		else {   /* IBPLIFE-2274 - Ends */
			wsspcomn.programPtr.add(1);
		}
	}
	
	protected void nextProgram4010() {
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(0);
			for (int loopVar1 = 0; loopVar1 != 8; loopVar1 += 1) {
				saveProgram4100();
			}
		}
		if (isEQ(wsaa1Pflag, "X")) {
			
			chdrlnbIO.setFunction("KEEPS");
			chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			wsaa1Pflag.set("?");
			gensswrec.function.set("A");
			callGenssw4300();
			return ;
		}
		if (isEQ(wsaa1Pflag, "?")) {
			onePCashRet4900();
			endChoices4050();
		}			
	}

	protected void onePCashRet4900()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/*  - calculate  the  premium  as  described  above  in  the       */
		/*       'Validation' section, and  check  that it is within       */
		/*       the tolerance limit,                                      */
		wsaa1Pflag.set("+");
		
		/*  - restore the saved programs to the program stack              */
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar10 = 0; loopVar10 != 8; loopVar10 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

	protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaY.add(1);
		wsaaX.add(1);
		/*EXIT*/
	}

	protected void endChoices4050() {
		/*    No more selected (or none)*/
		/*       - restore stack form wsaa to wssp*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(0);
			for (int loopVar2 = 0; loopVar2 != 8; loopVar2 += 1){
				restoreProgram4200();
			}
		}
		/*    If current stack action is * then re-display screen*/
		/*       (in this case, some other option(s) were requested)*/
		/*    Otherwise continue as normal.*/


		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		} else {
			wsspcomn.programPtr.add(1);
		}
	}

	protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsaaX.add(1);
		wsaaY.add(1);
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		/*EXIT*/
	}


	protected void callGenssw4300(){	
		callSubroutine4310();
	}

	protected void callSubroutine4310(){	
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,Varcom.oK)
		&& isNE(gensswrec.statuz,Varcom.mrnf)) {
			syserrrec.params.set(gensswrec.gensswRec);
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz,Varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(H039);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1,wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	protected void loadProgram4400(){	
		/*RESTORE1*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT1*/
	}

	protected void saveProgram4100(){	
		/*SAVE*/
		wsaaX.add(1);
		wsaaY.add(1);
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		/*EXIT*/
	}
	
	private static final class FormatsInner {
		/* FORMATS */
		private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	}

}