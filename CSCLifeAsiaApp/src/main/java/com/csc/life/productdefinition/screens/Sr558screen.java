package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr558screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr558ScreenVars sv = (Sr558ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr558screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr558ScreenVars screenVars = (Sr558ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.jowner.setClassString("");
		screenVars.jownername.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.nextinsdteDisp.setClassString("");
		screenVars.aplint.setClassString("");
		screenVars.plint.setClassString("");
		screenVars.aplamt.setClassString("");
		screenVars.loanvalue.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.instpramt.setClassString("");
		screenVars.shortds01.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.pymdesc.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.outstamt.setClassString("");
		screenVars.frequency.setClassString("");
		screenVars.shortds02.setClassString("");
		screenVars.nextinsamt.setClassString("");
	}

/**
 * Clear all the variables in Sr558screen
 */
	public static void clear(VarModel pv) {
		Sr558ScreenVars screenVars = (Sr558ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.jowner.clear();
		screenVars.jownername.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.nextinsdteDisp.clear();
		screenVars.nextinsdte.clear();
		screenVars.aplint.clear();
		screenVars.plint.clear();
		screenVars.aplamt.clear();
		screenVars.loanvalue.clear();
		screenVars.billfreq.clear();
		screenVars.instpramt.clear();
		screenVars.shortds01.clear();
		screenVars.mop.clear();
		screenVars.pymdesc.clear();
		screenVars.indic.clear();
		screenVars.lifenum.clear();
		screenVars.outstamt.clear();
		screenVars.frequency.clear();
		screenVars.shortds02.clear();
		screenVars.nextinsamt.clear();
	}
}
