package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 22 Aug 2012 15:34:00
 * Description:
 * Copybook name: Vpxaliferec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxaliferec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
	public FixedLengthStringData vpxalifeRec = new FixedLengthStringData(160);
  	public FixedLengthStringData lifes = new FixedLengthStringData(20).isAPartOf(vpxalifeRec, 0);
  	public FixedLengthStringData[] life = FLSArrayPartOfStructure(10, 2, lifes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(lifes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData life01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData life02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData life03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData life04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData life05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData life06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData life07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData life08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData life09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData life10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	
  	public FixedLengthStringData jlifes = new FixedLengthStringData(20).isAPartOf(vpxalifeRec, 20);
  	public FixedLengthStringData[] jlife = FLSArrayPartOfStructure(10, 2, jlifes, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(jlifes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData jlife01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData jlife02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData jlife03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData jlife04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData jlife05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData jlife06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData jlife07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData jlife08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData jlife09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData jlife10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	
  	public FixedLengthStringData smokings = new FixedLengthStringData(20).isAPartOf(vpxalifeRec, 40);
  	public FixedLengthStringData[] smoking = FLSArrayPartOfStructure(10, 2, smokings, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(smokings, 0, FILLER_REDEFINE);
  	public FixedLengthStringData smoking01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData smoking02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData smoking03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData smoking04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData smoking05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData smoking06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData smoking07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData smoking08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData smoking09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData smoking10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	
  	public FixedLengthStringData cltsexs = new FixedLengthStringData(10).isAPartOf(vpxalifeRec, 60);
  	public FixedLengthStringData[] cltsex = FLSArrayPartOfStructure(10, 1, cltsexs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(cltsexs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cltsex01 = new FixedLengthStringData(1).isAPartOf(filler4, 0);
  	public FixedLengthStringData cltsex02 = new FixedLengthStringData(1).isAPartOf(filler4, 1);
  	public FixedLengthStringData cltsex03 = new FixedLengthStringData(1).isAPartOf(filler4, 2);
  	public FixedLengthStringData cltsex04 = new FixedLengthStringData(1).isAPartOf(filler4, 3);
  	public FixedLengthStringData cltsex05 = new FixedLengthStringData(1).isAPartOf(filler4, 4);
  	public FixedLengthStringData cltsex06 = new FixedLengthStringData(1).isAPartOf(filler4, 5);
  	public FixedLengthStringData cltsex07 = new FixedLengthStringData(1).isAPartOf(filler4, 6);
  	public FixedLengthStringData cltsex08 = new FixedLengthStringData(1).isAPartOf(filler4, 7);
  	public FixedLengthStringData cltsex09 = new FixedLengthStringData(1).isAPartOf(filler4, 8);
  	public FixedLengthStringData cltsex10 = new FixedLengthStringData(1).isAPartOf(filler4, 9);
  	
  	public FixedLengthStringData cltdobs = new FixedLengthStringData(50).isAPartOf(vpxalifeRec, 70);
  	public PackedDecimalData[] cltdob = PDArrayPartOfStructure(10, 8, 0, cltdobs, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(50).isAPartOf(cltdobs, 0, FILLER_REDEFINE);
  	public PackedDecimalData cltdob01 = new PackedDecimalData(8, 0).isAPartOf(filler5, 0);
  	public PackedDecimalData cltdob02 = new PackedDecimalData(8, 0).isAPartOf(filler5, 5);
  	public PackedDecimalData cltdob03 = new PackedDecimalData(8, 0).isAPartOf(filler5, 10);
  	public PackedDecimalData cltdob04 = new PackedDecimalData(8, 0).isAPartOf(filler5, 15);
  	public PackedDecimalData cltdob05 = new PackedDecimalData(8, 0).isAPartOf(filler5, 20);
  	public PackedDecimalData cltdob06 = new PackedDecimalData(8, 0).isAPartOf(filler5, 25);
  	public PackedDecimalData cltdob07 = new PackedDecimalData(8, 0).isAPartOf(filler5, 30);
  	public PackedDecimalData cltdob08 = new PackedDecimalData(8, 0).isAPartOf(filler5, 35);
  	public PackedDecimalData cltdob09 = new PackedDecimalData(8, 0).isAPartOf(filler5, 40);
  	public PackedDecimalData cltdob10 = new PackedDecimalData(8, 0).isAPartOf(filler5, 45);
  	
  	public FixedLengthStringData occupations = new FixedLengthStringData(40).isAPartOf(vpxalifeRec, 120);
  	public FixedLengthStringData[] occupation = FLSArrayPartOfStructure(10, 4, occupations, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(occupations, 0, FILLER_REDEFINE);
  	public FixedLengthStringData occupation01 = new FixedLengthStringData(4).isAPartOf(filler6, 0);
  	public FixedLengthStringData occupation02 = new FixedLengthStringData(4).isAPartOf(filler6, 4);
  	public FixedLengthStringData occupation03 = new FixedLengthStringData(4).isAPartOf(filler6, 8);
  	public FixedLengthStringData occupation04 = new FixedLengthStringData(4).isAPartOf(filler6, 12);
  	public FixedLengthStringData occupation05 = new FixedLengthStringData(4).isAPartOf(filler6, 16);
  	public FixedLengthStringData occupation06 = new FixedLengthStringData(4).isAPartOf(filler6, 20);
  	public FixedLengthStringData occupation07 = new FixedLengthStringData(4).isAPartOf(filler6, 24);
  	public FixedLengthStringData occupation08 = new FixedLengthStringData(4).isAPartOf(filler6, 28);
  	public FixedLengthStringData occupation09 = new FixedLengthStringData(4).isAPartOf(filler6, 32);
  	public FixedLengthStringData occupation10 = new FixedLengthStringData(4).isAPartOf(filler6, 36);
  	
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxalifeRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxalifeRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}