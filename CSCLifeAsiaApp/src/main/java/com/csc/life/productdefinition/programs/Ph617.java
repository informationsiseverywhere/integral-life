/*
 * File: Ph617.java
 * Date: 30 August 2009 1:11:47
 * Author: Quipoz Limited
 * 
 * Class transformed from PH617.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.procedures.Th617pt;
import com.csc.life.productdefinition.screens.Sh617ScreenVars;
import com.csc.life.productdefinition.tablestructures.Th617rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Ph617 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH617");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Th617rec th617rec = new Th617rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh617ScreenVars sv = ScreenProgram.getScreenVars( Sh617ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public Ph617() {
		super();
		screenVars = sv;
		new ScreenModel("Sh617", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		initialize(sv.dataArea);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th617rec.th617Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		th617rec.insprm01.set(ZERO);
		th617rec.insprm02.set(ZERO);
		th617rec.insprm03.set(ZERO);
		th617rec.insprm04.set(ZERO);
		th617rec.insprm05.set(ZERO);
		th617rec.insprm06.set(ZERO);
		th617rec.insprm07.set(ZERO);
		th617rec.insprm08.set(ZERO);
		th617rec.insprm09.set(ZERO);
		th617rec.insprm10.set(ZERO);
		th617rec.insprm11.set(ZERO);
		th617rec.insprm12.set(ZERO);
		th617rec.insprm13.set(ZERO);
		th617rec.insprm14.set(ZERO);
		th617rec.insprm15.set(ZERO);
		th617rec.insprm16.set(ZERO);
		th617rec.insprm17.set(ZERO);
		th617rec.insprm18.set(ZERO);
		th617rec.insprm19.set(ZERO);
		th617rec.insprm20.set(ZERO);
		th617rec.insprm21.set(ZERO);
		th617rec.insprm22.set(ZERO);
		th617rec.insprm23.set(ZERO);
		th617rec.insprm24.set(ZERO);
		th617rec.insprm25.set(ZERO);
		th617rec.insprm26.set(ZERO);
		th617rec.insprm27.set(ZERO);
		th617rec.insprm28.set(ZERO);
		th617rec.insprm29.set(ZERO);
		th617rec.insprm30.set(ZERO);
		th617rec.insprm31.set(ZERO);
		th617rec.insprm32.set(ZERO);
		th617rec.insprm33.set(ZERO);
		th617rec.insprm34.set(ZERO);
		th617rec.insprm35.set(ZERO);
		th617rec.insprm36.set(ZERO);
		th617rec.insprm37.set(ZERO);
		th617rec.insprm38.set(ZERO);
		th617rec.insprm39.set(ZERO);
		th617rec.insprm40.set(ZERO);
		th617rec.insprm41.set(ZERO);
		th617rec.insprm42.set(ZERO);
		th617rec.insprm43.set(ZERO);
		th617rec.insprm44.set(ZERO);
		th617rec.insprm45.set(ZERO);
		th617rec.insprm46.set(ZERO);
		th617rec.insprm47.set(ZERO);
		th617rec.insprm48.set(ZERO);
		th617rec.insprm49.set(ZERO);
		th617rec.insprm50.set(ZERO);
		th617rec.insprm51.set(ZERO);
		th617rec.insprm52.set(ZERO);
		th617rec.insprm53.set(ZERO);
		th617rec.insprm54.set(ZERO);
		th617rec.insprm55.set(ZERO);
		th617rec.insprm56.set(ZERO);
		th617rec.insprm57.set(ZERO);
		th617rec.insprm58.set(ZERO);
		th617rec.insprm59.set(ZERO);
		th617rec.insprm60.set(ZERO);
		th617rec.insprm61.set(ZERO);
		th617rec.insprm62.set(ZERO);
		th617rec.insprm63.set(ZERO);
		th617rec.insprm64.set(ZERO);
		th617rec.insprm65.set(ZERO);
		th617rec.insprm66.set(ZERO);
		th617rec.insprm67.set(ZERO);
		th617rec.insprm68.set(ZERO);
		th617rec.insprm69.set(ZERO);
		th617rec.insprm70.set(ZERO);
		th617rec.insprm71.set(ZERO);
		th617rec.insprm72.set(ZERO);
		th617rec.insprm73.set(ZERO);
		th617rec.insprm74.set(ZERO);
		th617rec.insprm75.set(ZERO);
		th617rec.insprm76.set(ZERO);
		th617rec.insprm77.set(ZERO);
		th617rec.insprm78.set(ZERO);
		th617rec.insprm79.set(ZERO);
		th617rec.insprm80.set(ZERO);
		th617rec.insprm81.set(ZERO);
		th617rec.insprm82.set(ZERO);
		th617rec.insprm83.set(ZERO);
		th617rec.insprm84.set(ZERO);
		th617rec.insprm85.set(ZERO);
		th617rec.insprm86.set(ZERO);
		th617rec.insprm87.set(ZERO);
		th617rec.insprm88.set(ZERO);
		th617rec.insprm89.set(ZERO);
		th617rec.insprm90.set(ZERO);
		th617rec.insprm91.set(ZERO);
		th617rec.insprm92.set(ZERO);
		th617rec.insprm93.set(ZERO);
		th617rec.insprm94.set(ZERO);
		th617rec.insprm95.set(ZERO);
		th617rec.insprm96.set(ZERO);
		th617rec.insprm97.set(ZERO);
		th617rec.insprm98.set(ZERO);
		th617rec.insprm99.set(ZERO);
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 11); loopVar1 += 1){
			initInstprs1700();
		}
		/*    MOVE ZERO                                                    */
		/*      TO TH617-INSTPR                   .                        */
		th617rec.insprem.set(ZERO);
		th617rec.premUnit.set(ZERO);
		th617rec.unit.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.disccntmeth.set(th617rec.disccntmeth);
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 99); loopVar2 += 1){
			moveInsprms1600();
		}
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 11); loopVar3 += 1){
			moveInstprs1800();
		}
		/*    MOVE TH617-INSTPR                                            */
		/*      TO SH617-INSTPR                  .                         */
		sv.insprem.set(th617rec.insprem);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.premUnit.set(th617rec.premUnit);
		sv.unit.set(th617rec.unit);
	}

protected void moveInsprms1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.insprm[wsaaSub1.toInt()].set(th617rec.insprm[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void initInstprs1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		th617rec.instpr[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
		}

protected void moveInstprs1800()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isEQ(th617rec.instpr[wsaaSub1.toInt()], NUMERIC)) {
			sv.instpr[wsaaSub1.toInt()].set(th617rec.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
					screenIo2010();
					exit2090();
				}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(th617rec.th617Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.disccntmeth,th617rec.disccntmeth)) {
			th617rec.disccntmeth.set(sv.disccntmeth);
			wsaaUpdateFlag = "Y";
		}
		wsaaSub1.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 99); loopVar4 += 1){
			updateInsprms3500();
		}
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 11); loopVar5 += 1){
			updateInstprs3600();
		}
		if (isNE(sv.insprem,th617rec.insprem)) {
			th617rec.insprem.set(sv.insprem);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premUnit,th617rec.premUnit)) {
			th617rec.premUnit.set(sv.premUnit);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.unit,th617rec.unit)) {
			th617rec.unit.set(sv.unit);
			wsaaUpdateFlag = "Y";
		}
	}

protected void updateInsprms3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.insprm[wsaaSub1.toInt()],th617rec.insprm[wsaaSub1.toInt()])) {
			th617rec.insprm[wsaaSub1.toInt()].set(sv.insprm[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateInstprs3600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.instpr[wsaaSub1.toInt()], th617rec.instpr[wsaaSub1.toInt()])) {
			th617rec.instpr[wsaaSub1.toInt()].set(sv.instpr[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Th617pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
