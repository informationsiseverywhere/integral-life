package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr674screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr674screensfl";
		lrec.subfileClass = Sr674screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 4;
		lrec.pageSubfile = 3;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr674ScreenVars sv = (Sr674ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr674screenctlWritten, sv.Sr674screensflWritten, av, sv.sr674screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr674ScreenVars screenVars = (Sr674ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttyp.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.freqdesc.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.mopdesc.setClassString("");
		screenVars.prtshd.setClassString("");
		screenVars.zloanamt01.setClassString("");
		screenVars.zloanamt02.setClassString("");
		screenVars.zloanamt03.setClassString("");
		screenVars.zloanamt04.setClassString("");
		screenVars.zloanamt05.setClassString("");
		screenVars.zloanamt06.setClassString("");
		screenVars.effdate01Disp.setClassString("");
		screenVars.effdate02Disp.setClassString("");
		screenVars.effdate03Disp.setClassString("");
		screenVars.effdate04Disp.setClassString("");
		screenVars.effdate05Disp.setClassString("");
		screenVars.nextinsdteDisp.setClassString("");
		screenVars.tmpChg.setClassString("");
	}

/**
 * Clear all the variables in Sr674screenctl
 */
	public static void clear(VarModel pv) {
		Sr674ScreenVars screenVars = (Sr674ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttyp.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.billfreq.clear();
		screenVars.freqdesc.clear();
		screenVars.mop.clear();
		screenVars.mopdesc.clear();
		screenVars.prtshd.clear();
		screenVars.zloanamt01.clear();
		screenVars.zloanamt02.clear();
		screenVars.zloanamt03.clear();
		screenVars.zloanamt04.clear();
		screenVars.zloanamt05.clear();
		screenVars.zloanamt06.clear();
		screenVars.effdate01Disp.clear();
		screenVars.effdate01.clear();
		screenVars.effdate02Disp.clear();
		screenVars.effdate02.clear();
		screenVars.effdate03Disp.clear();
		screenVars.effdate03.clear();
		screenVars.effdate04Disp.clear();
		screenVars.effdate04.clear();
		screenVars.effdate05Disp.clear();
		screenVars.effdate05.clear();
		screenVars.nextinsdte.clear();
		screenVars.nextinsdteDisp.clear();
		screenVars.tmpChg.clear();
	}
}
