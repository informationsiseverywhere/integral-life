package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5675
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class Sr28xScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1429);
	public FixedLengthStringData dataFields = new FixedLengthStringData(499).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
  	public FixedLengthStringData dataextracts = new FixedLengthStringData(90).isAPartOf(dataFields, 44);
  	public FixedLengthStringData[] dataextract = FLSArrayPartOfStructure(9, 10, dataextracts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(90).isAPartOf(dataextracts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData dataextract01 = DD.dataextract.copy().isAPartOf(filler, 0);
  	public FixedLengthStringData dataextract02 = DD.dataextract.copy().isAPartOf(filler, 10);
  	public FixedLengthStringData dataextract03 = DD.dataextract.copy().isAPartOf(filler, 20);
  	public FixedLengthStringData dataextract04 = DD.dataextract.copy().isAPartOf(filler, 30);
  	public FixedLengthStringData dataextract05 = DD.dataextract.copy().isAPartOf(filler, 40);
  	public FixedLengthStringData dataextract06 = DD.dataextract.copy().isAPartOf(filler, 50);
  	public FixedLengthStringData dataextract07 = DD.dataextract.copy().isAPartOf(filler, 60);
  	public FixedLengthStringData dataextract08 = DD.dataextract.copy().isAPartOf(filler, 70);
  	public FixedLengthStringData dataextract09 = DD.dataextract.copy().isAPartOf(filler, 80);
  	
  	public FixedLengthStringData funcs = new FixedLengthStringData(45).isAPartOf(dataFields, 134);
  	public FixedLengthStringData[] func = FLSArrayPartOfStructure(9, 5, funcs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(45).isAPartOf(funcs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData func01 = DD.function.copy().isAPartOf(filler1, 0); 
  	public FixedLengthStringData func02 = DD.function.copy().isAPartOf(filler1, 5); 
  	public FixedLengthStringData func03 = DD.function.copy().isAPartOf(filler1, 10);
  	public FixedLengthStringData func04 = DD.function.copy().isAPartOf(filler1, 15);
  	public FixedLengthStringData func05 = DD.function.copy().isAPartOf(filler1, 20);
  	public FixedLengthStringData func06 = DD.function.copy().isAPartOf(filler1, 25);
  	public FixedLengthStringData func07 = DD.function.copy().isAPartOf(filler1, 30);
  	public FixedLengthStringData func08 = DD.function.copy().isAPartOf(filler1, 35);
  	public FixedLengthStringData func09 = DD.function.copy().isAPartOf(filler1, 40);
  	
  	public FixedLengthStringData vpmmodel = DD.vpmmodel.copy().isAPartOf(dataFields, 179);
  	public FixedLengthStringData linkstatuz = DD.linkstatuz.copy().isAPartOf(dataFields, 199);
  	public FixedLengthStringData linkcopybk = DD.linkcopybk.copy().isAPartOf(dataFields, 219);

  	public FixedLengthStringData dataupdates = new FixedLengthStringData(50).isAPartOf(dataFields, 229);
  	public FixedLengthStringData[] dataupdate = FLSArrayPartOfStructure(5, 10, dataupdates, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(dataupdates, 0, FILLER_REDEFINE);
  	public FixedLengthStringData dataupdate01 = DD.dataupdate.copy().isAPartOf(filler2, 0);
  	public FixedLengthStringData dataupdate02 = DD.dataupdate.copy().isAPartOf(filler2, 10);
  	public FixedLengthStringData dataupdate03 = DD.dataupdate.copy().isAPartOf(filler2, 20);
  	public FixedLengthStringData dataupdate04 = DD.dataupdate.copy().isAPartOf(filler2, 30);  	
  	public FixedLengthStringData dataupdate05 = DD.dataupdate.copy().isAPartOf(filler2, 40);  	
  	
  	
  	public FixedLengthStringData extfldids = new FixedLengthStringData(200).isAPartOf(dataFields, 279);
  	public FixedLengthStringData[] extfldid = FLSArrayPartOfStructure(25, 8, extfldids, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(200).isAPartOf(extfldids, 0, FILLER_REDEFINE);
	public FixedLengthStringData extfldid01 = DD.extfldid.copy().isAPartOf(filler3, 0);  
	public FixedLengthStringData extfldid02 = DD.extfldid.copy().isAPartOf(filler3, 8);  
	public FixedLengthStringData extfldid03 = DD.extfldid.copy().isAPartOf(filler3, 16); 
	public FixedLengthStringData extfldid04 = DD.extfldid.copy().isAPartOf(filler3, 24); 
	public FixedLengthStringData extfldid05 = DD.extfldid.copy().isAPartOf(filler3, 32); 
	public FixedLengthStringData extfldid06 = DD.extfldid.copy().isAPartOf(filler3, 40); 
	public FixedLengthStringData extfldid07 = DD.extfldid.copy().isAPartOf(filler3, 48); 
	public FixedLengthStringData extfldid08 = DD.extfldid.copy().isAPartOf(filler3, 56); 
	public FixedLengthStringData extfldid09 = DD.extfldid.copy().isAPartOf(filler3, 64); 
	public FixedLengthStringData extfldid10 = DD.extfldid.copy().isAPartOf(filler3, 72); 
	public FixedLengthStringData extfldid11 = DD.extfldid.copy().isAPartOf(filler3, 80); 
	public FixedLengthStringData extfldid12 = DD.extfldid.copy().isAPartOf(filler3, 88); 
	public FixedLengthStringData extfldid13 = DD.extfldid.copy().isAPartOf(filler3, 96); 
	public FixedLengthStringData extfldid14 = DD.extfldid.copy().isAPartOf(filler3, 104);
	public FixedLengthStringData extfldid15 = DD.extfldid.copy().isAPartOf(filler3, 112);
	public FixedLengthStringData extfldid16 = DD.extfldid.copy().isAPartOf(filler3, 120);
	public FixedLengthStringData extfldid17 = DD.extfldid.copy().isAPartOf(filler3, 128);
	public FixedLengthStringData extfldid18 = DD.extfldid.copy().isAPartOf(filler3, 136);
	public FixedLengthStringData extfldid19 = DD.extfldid.copy().isAPartOf(filler3, 144);
	public FixedLengthStringData extfldid20 = DD.extfldid.copy().isAPartOf(filler3, 152);
	public FixedLengthStringData extfldid21 = DD.extfldid.copy().isAPartOf(filler3, 160);
	public FixedLengthStringData extfldid22 = DD.extfldid.copy().isAPartOf(filler3, 168);
	public FixedLengthStringData extfldid23 = DD.extfldid.copy().isAPartOf(filler3, 176);
	public FixedLengthStringData extfldid24 = DD.extfldid.copy().isAPartOf(filler3, 184);
	public FixedLengthStringData extfldid25 = DD.extfldid.copy().isAPartOf(filler3, 192); 
	
	public FixedLengthStringData dataLog = DD.datalog.copy().isAPartOf(dataFields, 479);
	public FixedLengthStringData vpmsubr = DD.vpmsubr.copy().isAPartOf(dataFields, 489);
	
	//error indicators
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(238).isAPartOf(dataArea, 499);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
  	public FixedLengthStringData dataextractsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 16);
  	public FixedLengthStringData[] dataextractErr = FLSArrayPartOfStructure(9, 4, dataextractsErr, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(dataextractsErr, 0, FILLER_REDEFINE);
  	public FixedLengthStringData dataextract01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
  	public FixedLengthStringData dataextract02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
  	public FixedLengthStringData dataextract03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
  	public FixedLengthStringData dataextract04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
  	public FixedLengthStringData dataextract05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
  	public FixedLengthStringData dataextract06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
  	public FixedLengthStringData dataextract07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
  	public FixedLengthStringData dataextract08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
  	public FixedLengthStringData dataextract09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
  	
  	public FixedLengthStringData funcsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 52);
  	public FixedLengthStringData[] funcErr = FLSArrayPartOfStructure(9, 4, funcsErr, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(funcsErr, 0, FILLER_REDEFINE);
  	public FixedLengthStringData func01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0); 
  	public FixedLengthStringData func02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4); 
  	public FixedLengthStringData func03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8); 
  	public FixedLengthStringData func04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
  	public FixedLengthStringData func05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
  	public FixedLengthStringData func06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
  	public FixedLengthStringData func07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
  	public FixedLengthStringData func08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
  	public FixedLengthStringData func09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
  	
  	public FixedLengthStringData dataupdatesErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 88);
  	public FixedLengthStringData[] dataupdateErr = FLSArrayPartOfStructure(5, 4, dataupdatesErr, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(20).isAPartOf(dataupdatesErr, 0, FILLER_REDEFINE);
  	public FixedLengthStringData dataupdate01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
  	public FixedLengthStringData dataupdate02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
  	public FixedLengthStringData dataupdate03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
  	public FixedLengthStringData dataupdate04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);  	
  	public FixedLengthStringData dataupdate05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);  	
  	
  	public FixedLengthStringData vpmmodelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
  	public FixedLengthStringData linkstatuzErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
  	public FixedLengthStringData linkcopybkErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 126);
  	
  	public FixedLengthStringData extfldidsErr = new FixedLengthStringData(100).isAPartOf(errorIndicators, 130);
  	public FixedLengthStringData[] extfldidErr = FLSArrayPartOfStructure(25, 4, extfldidsErr, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(100).isAPartOf(extfldidsErr, 0, FILLER_REDEFINE);
  	public FixedLengthStringData extfldid01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);   
  	public FixedLengthStringData extfldid02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4); 
  	public FixedLengthStringData extfldid03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8); 
  	public FixedLengthStringData extfldid04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);  	 
  	public FixedLengthStringData extfldid05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);  	 
  	public FixedLengthStringData extfldid06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);  	 
  	public FixedLengthStringData extfldid07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);  	 
  	public FixedLengthStringData extfldid08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);  	 
  	public FixedLengthStringData extfldid09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);  	 
  	public FixedLengthStringData extfldid10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);  	 
  	public FixedLengthStringData extfldid11Err = new FixedLengthStringData(4).isAPartOf(filler7, 40);  	
  	public FixedLengthStringData extfldid12Err = new FixedLengthStringData(4).isAPartOf(filler7, 44);  	
  	public FixedLengthStringData extfldid13Err = new FixedLengthStringData(4).isAPartOf(filler7, 48);  	
  	public FixedLengthStringData extfldid14Err = new FixedLengthStringData(4).isAPartOf(filler7, 52);
  	public FixedLengthStringData extfldid15Err = new FixedLengthStringData(4).isAPartOf(filler7, 56);
  	public FixedLengthStringData extfldid16Err = new FixedLengthStringData(4).isAPartOf(filler7, 60);
  	public FixedLengthStringData extfldid17Err = new FixedLengthStringData(4).isAPartOf(filler7, 64);
  	public FixedLengthStringData extfldid18Err = new FixedLengthStringData(4).isAPartOf(filler7, 68);
  	public FixedLengthStringData extfldid19Err = new FixedLengthStringData(4).isAPartOf(filler7, 72);
  	public FixedLengthStringData extfldid20Err = new FixedLengthStringData(4).isAPartOf(filler7, 76);
  	public FixedLengthStringData extfldid21Err = new FixedLengthStringData(4).isAPartOf(filler7, 80);
  	public FixedLengthStringData extfldid22Err = new FixedLengthStringData(4).isAPartOf(filler7, 84);
  	public FixedLengthStringData extfldid23Err = new FixedLengthStringData(4).isAPartOf(filler7, 88);
  	public FixedLengthStringData extfldid24Err = new FixedLengthStringData(4).isAPartOf(filler7, 92);
  	public FixedLengthStringData extfldid25Err = new FixedLengthStringData(4).isAPartOf(filler7, 96);
  	
  	public FixedLengthStringData dataLogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 230);
  	public FixedLengthStringData vpmsubrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 234);
	
	//output indicators
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(692).isAPartOf(dataArea, 737);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData dataextractsOut = new FixedLengthStringData(112).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] dataextractOut = FLSArrayPartOfStructure(9, 12, dataextractsOut, 0);
	public FixedLengthStringData[][] dataextractO = FLSDArrayPartOfArrayStructure(12, 1, dataextractOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(112).isAPartOf(dataextractsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] dataextract01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] dataextract02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] dataextract03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] dataextract04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] dataextract05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] dataextract06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] dataextract07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] dataextract08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] dataextract09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData funcsOut = new FixedLengthStringData(112).isAPartOf(outputIndicators, 160);
	public FixedLengthStringData[] funcOut = FLSArrayPartOfStructure(9, 12, funcsOut, 0);
	public FixedLengthStringData[][] funcO = FLSDArrayPartOfArrayStructure(12, 1, funcOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(112).isAPartOf(funcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] func01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);  
	public FixedLengthStringData[] func02Out = FLSArrayPartOfStructure(12, 1, filler9, 12); 
	public FixedLengthStringData[] func03Out = FLSArrayPartOfStructure(12, 1, filler9, 24); 
	public FixedLengthStringData[] func04Out = FLSArrayPartOfStructure(12, 1, filler9, 36); 
	public FixedLengthStringData[] func05Out = FLSArrayPartOfStructure(12, 1, filler9, 48); 
	public FixedLengthStringData[] func06Out = FLSArrayPartOfStructure(12, 1, filler9, 60); 
	public FixedLengthStringData[] func07Out = FLSArrayPartOfStructure(12, 1, filler9, 72); 
	public FixedLengthStringData[] func08Out = FLSArrayPartOfStructure(12, 1, filler9, 84); 
	public FixedLengthStringData[] func09Out = FLSArrayPartOfStructure(12, 1, filler9, 96); 

	public FixedLengthStringData[] vpmmodelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 272);
	public FixedLengthStringData[] linkstatuzOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 284);
	public FixedLengthStringData[] linkcopybkOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 296);
	
	public FixedLengthStringData dataupdatesOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 308);
	public FixedLengthStringData[] dataupdateOut = FLSArrayPartOfStructure(5, 12, dataupdatesOut, 0);
	public FixedLengthStringData[][] dataupdateO = FLSDArrayPartOfArrayStructure(12, 1, dataupdateOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(dataupdatesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] dataupdate01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] dataupdate02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] dataupdate03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] dataupdate04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] dataupdate05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	
	public FixedLengthStringData extfldidsOut = new FixedLengthStringData(300).isAPartOf(outputIndicators, 368);
	public FixedLengthStringData[] extfldidOut = FLSArrayPartOfStructure(25, 12, extfldidsOut, 0);
	public FixedLengthStringData[][] extfldidO = FLSDArrayPartOfArrayStructure(12, 1, extfldidOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(300).isAPartOf(extfldidsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] extfldid01Out = FLSArrayPartOfStructure(12, 1, filler11, 0 );
	public FixedLengthStringData[] extfldid02Out = FLSArrayPartOfStructure(12, 1, filler11, 12 );   
	public FixedLengthStringData[] extfldid03Out = FLSArrayPartOfStructure(12, 1, filler11, 24 );   
	public FixedLengthStringData[] extfldid04Out = FLSArrayPartOfStructure(12, 1, filler11, 36 );   
	public FixedLengthStringData[] extfldid05Out = FLSArrayPartOfStructure(12, 1, filler11, 48 );   
	public FixedLengthStringData[] extfldid06Out = FLSArrayPartOfStructure(12, 1, filler11, 60 );   
	public FixedLengthStringData[] extfldid07Out = FLSArrayPartOfStructure(12, 1, filler11, 72 );   
	public FixedLengthStringData[] extfldid08Out = FLSArrayPartOfStructure(12, 1, filler11, 84 );   
	public FixedLengthStringData[] extfldid09Out = FLSArrayPartOfStructure(12, 1, filler11, 96 );   
	public FixedLengthStringData[] extfldid10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);   
	public FixedLengthStringData[] extfldid11Out = FLSArrayPartOfStructure(12, 1, filler11, 120);   
	public FixedLengthStringData[] extfldid12Out = FLSArrayPartOfStructure(12, 1, filler11, 132);   
	public FixedLengthStringData[] extfldid13Out = FLSArrayPartOfStructure(12, 1, filler11, 144);   
	public FixedLengthStringData[] extfldid14Out = FLSArrayPartOfStructure(12, 1, filler11, 156);   
	public FixedLengthStringData[] extfldid15Out = FLSArrayPartOfStructure(12, 1, filler11, 168);   
	public FixedLengthStringData[] extfldid16Out = FLSArrayPartOfStructure(12, 1, filler11, 180);   
	public FixedLengthStringData[] extfldid17Out = FLSArrayPartOfStructure(12, 1, filler11, 192);   
	public FixedLengthStringData[] extfldid18Out = FLSArrayPartOfStructure(12, 1, filler11, 204);   
	public FixedLengthStringData[] extfldid19Out = FLSArrayPartOfStructure(12, 1, filler11, 216);   
	public FixedLengthStringData[] extfldid20Out = FLSArrayPartOfStructure(12, 1, filler11, 228);   
	public FixedLengthStringData[] extfldid21Out = FLSArrayPartOfStructure(12, 1, filler11, 240);   
	public FixedLengthStringData[] extfldid22Out = FLSArrayPartOfStructure(12, 1, filler11, 252);   
	public FixedLengthStringData[] extfldid23Out = FLSArrayPartOfStructure(12, 1, filler11, 264);   
	public FixedLengthStringData[] extfldid24Out = FLSArrayPartOfStructure(12, 1, filler11, 276);   
	public FixedLengthStringData[] extfldid25Out = FLSArrayPartOfStructure(12, 1, filler11, 288);   
	
	public FixedLengthStringData[] dataLogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 668);
	public FixedLengthStringData[] vpmsubrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 680);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr28xscreenWritten = new LongData(0);
	public LongData Sr28xprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr28xScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, dataextract01,  dataextract02,  dataextract03,  dataextract04,  dataextract05,  dataextract06,  dataextract07,  dataextract08,  dataextract09,   func01,  func02,  func03,  func04,  func05,  func06,  func07,  func08,  func09,    dataupdate01,  dataupdate02,  dataupdate03,  dataupdate04,  dataupdate05,  vpmmodel,  linkstatuz,  linkcopybk,  extfldid01,  extfldid02,  extfldid03,  extfldid04,  extfldid05,  extfldid06,  extfldid07,  extfldid08,  extfldid09,  extfldid10,  extfldid11,  extfldid12,  extfldid13,  extfldid14,  extfldid15,  extfldid16,  extfldid17,  extfldid18,  extfldid19,  extfldid20,  extfldid21,  extfldid22,  extfldid23,  extfldid24,  extfldid25, dataLog, vpmsubr};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, dataextract01Out,  dataextract02Out,  dataextract03Out,  dataextract04Out,  dataextract05Out,  dataextract06Out,  dataextract07Out,  dataextract08Out,  dataextract09Out,   func01Out,  func02Out,  func03Out,  func04Out,  func05Out,  func06Out,  func07Out,  func08Out,  func09Out,  dataupdate01Out,  dataupdate02Out,  dataupdate03Out,  dataupdate04Out,  dataupdate05Out,  vpmmodelOut,  linkstatuzOut,  linkcopybkOut,  extfldid01Out,  extfldid02Out,  extfldid03Out,  extfldid04Out,  extfldid05Out,  extfldid06Out,  extfldid07Out,  extfldid08Out,  extfldid09Out,  extfldid10Out,  extfldid11Out,  extfldid12Out,  extfldid13Out,  extfldid14Out,  extfldid15Out,  extfldid16Out,  extfldid17Out,  extfldid18Out,  extfldid19Out,  extfldid20Out,  extfldid21Out,  extfldid22Out,  extfldid23Out,  extfldid24Out,  extfldid25Out, dataLogOut, vpmsubrOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, dataextract01Err,  dataextract02Err,  dataextract03Err,  dataextract04Err,  dataextract05Err,  dataextract06Err,  dataextract07Err,  dataextract08Err,  dataextract09Err,    func01Err,  func02Err,  func03Err,  func04Err,  func05Err,  func06Err,  func07Err,  func08Err,  func09Err,  dataupdate01Err,  dataupdate02Err,  dataupdate03Err,  dataupdate04Err,  dataupdate05Err,  vpmmodelErr,  linkstatuzErr,  linkcopybkErr,  extfldid01Err,  extfldid02Err,  extfldid03Err,  extfldid04Err,  extfldid05Err,  extfldid06Err,  extfldid07Err,  extfldid08Err,  extfldid09Err,  extfldid10Err,  extfldid11Err,  extfldid12Err,  extfldid13Err,  extfldid14Err,  extfldid15Err,  extfldid16Err,  extfldid17Err,  extfldid18Err,  extfldid19Err,  extfldid20Err,  extfldid21Err,  extfldid22Err,  extfldid23Err,  extfldid24Err,  extfldid25Err, dataLogErr, vpmsubrErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr28xscreen.class;
		protectRecord = Sr28xprotect.class;
	}

}