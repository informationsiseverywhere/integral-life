package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:42
 * Description:
 * Copybook name: MEDIACTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mediactkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mediactFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mediactKey = new FixedLengthStringData(256).isAPartOf(mediactFileKey, 0, REDEFINE);
  	public FixedLengthStringData mediactChdrcoy = new FixedLengthStringData(1).isAPartOf(mediactKey, 0);
  	public FixedLengthStringData mediactActiveInd = new FixedLengthStringData(1).isAPartOf(mediactKey, 1);
  	public FixedLengthStringData mediactInvref = new FixedLengthStringData(15).isAPartOf(mediactKey, 2);
  	public FixedLengthStringData mediactChdrnum = new FixedLengthStringData(8).isAPartOf(mediactKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(231).isAPartOf(mediactKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mediactFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mediactFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}