package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR51F
 * @version 1.0 generated on 30/08/09 07:17
 * @author Quipoz
 */
public class Sr51fScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1076);
	public FixedLengthStringData dataFields = new FixedLengthStringData(340).isAPartOf(dataArea, 0);
	public FixedLengthStringData ages = new FixedLengthStringData(30).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] age = ZDArrayPartOfStructure(10, 3, 0, ages, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ages, 0, FILLER_REDEFINE);
	public ZonedDecimalData age01 = DD.age.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData age02 = DD.age.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData age03 = DD.age.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData age04 = DD.age.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData age05 = DD.age.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData age06 = DD.age.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData age07 = DD.age.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData age08 = DD.age.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData age09 = DD.age.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData age10 = DD.age.copyToZonedDecimal().isAPartOf(filler,27);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData crtables = new FixedLengthStringData(80).isAPartOf(dataFields, 31);
	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(20, 4, crtables, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(crtables, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01 = DD.crtable.copy().isAPartOf(filler1,0);
	public FixedLengthStringData crtable02 = DD.crtable.copy().isAPartOf(filler1,4);
	public FixedLengthStringData crtable03 = DD.crtable.copy().isAPartOf(filler1,8);
	public FixedLengthStringData crtable04 = DD.crtable.copy().isAPartOf(filler1,12);
	public FixedLengthStringData crtable05 = DD.crtable.copy().isAPartOf(filler1,16);
	public FixedLengthStringData crtable06 = DD.crtable.copy().isAPartOf(filler1,20);
	public FixedLengthStringData crtable07 = DD.crtable.copy().isAPartOf(filler1,24);
	public FixedLengthStringData crtable08 = DD.crtable.copy().isAPartOf(filler1,28);
	public FixedLengthStringData crtable09 = DD.crtable.copy().isAPartOf(filler1,32);
	public FixedLengthStringData crtable10 = DD.crtable.copy().isAPartOf(filler1,36);
	public FixedLengthStringData crtable11 = DD.crtable.copy().isAPartOf(filler1,40);
	public FixedLengthStringData crtable12 = DD.crtable.copy().isAPartOf(filler1,44);
	public FixedLengthStringData crtable13 = DD.crtable.copy().isAPartOf(filler1,48);
	public FixedLengthStringData crtable14 = DD.crtable.copy().isAPartOf(filler1,52);
	public FixedLengthStringData crtable15 = DD.crtable.copy().isAPartOf(filler1,56);
	public FixedLengthStringData crtable16 = DD.crtable.copy().isAPartOf(filler1,60);
	public FixedLengthStringData crtable17 = DD.crtable.copy().isAPartOf(filler1,64);
	public FixedLengthStringData crtable18 = DD.crtable.copy().isAPartOf(filler1,68);
	public FixedLengthStringData crtable19 = DD.crtable.copy().isAPartOf(filler1,72);
	public FixedLengthStringData crtable20 = DD.crtable.copy().isAPartOf(filler1,76);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,111);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,119);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,127);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData suminss = new FixedLengthStringData(170).isAPartOf(dataFields, 165);
	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(10, 17, 2, suminss, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(170).isAPartOf(suminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumins01 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData sumins02 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData sumins03 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData sumins04 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData sumins05 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,68);
	public ZonedDecimalData sumins06 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,85);
	public ZonedDecimalData sumins07 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,102);
	public ZonedDecimalData sumins08 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,119);
	public ZonedDecimalData sumins09 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,136);
	public ZonedDecimalData sumins10 = DD.sumins.copyToZonedDecimal().isAPartOf(filler2,153);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,335);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(184).isAPartOf(dataArea, 340);
	public FixedLengthStringData agesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] ageErr = FLSArrayPartOfStructure(10, 4, agesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(agesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData age01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData age02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData age03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData age04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData age05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData age06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData age07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData age08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData age09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData age10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData crtablesErr = new FixedLengthStringData(80).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] crtableErr = FLSArrayPartOfStructure(20, 4, crtablesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(80).isAPartOf(crtablesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData crtable02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData crtable03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData crtable04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData crtable05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData crtable06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData crtable07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData crtable08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData crtable09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData crtable10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData crtable11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData crtable12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData crtable13Err = new FixedLengthStringData(4).isAPartOf(filler4, 48);
	public FixedLengthStringData crtable14Err = new FixedLengthStringData(4).isAPartOf(filler4, 52);
	public FixedLengthStringData crtable15Err = new FixedLengthStringData(4).isAPartOf(filler4, 56);
	public FixedLengthStringData crtable16Err = new FixedLengthStringData(4).isAPartOf(filler4, 60);
	public FixedLengthStringData crtable17Err = new FixedLengthStringData(4).isAPartOf(filler4, 64);
	public FixedLengthStringData crtable18Err = new FixedLengthStringData(4).isAPartOf(filler4, 68);
	public FixedLengthStringData crtable19Err = new FixedLengthStringData(4).isAPartOf(filler4, 72);
	public FixedLengthStringData crtable20Err = new FixedLengthStringData(4).isAPartOf(filler4, 76);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData suminssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData[] suminsErr = FLSArrayPartOfStructure(10, 4, suminssErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(suminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumins01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData sumins02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData sumins03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData sumins04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData sumins05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData sumins06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData sumins07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData sumins08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData sumins09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData sumins10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(552).isAPartOf(dataArea, 524);
	public FixedLengthStringData agesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] ageOut = FLSArrayPartOfStructure(10, 12, agesOut, 0);
	public FixedLengthStringData[][] ageO = FLSDArrayPartOfArrayStructure(12, 1, ageOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(agesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] age01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] age02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] age03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] age04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] age05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] age06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] age07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] age08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] age09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] age10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData crtablesOut = new FixedLengthStringData(240).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(20, 12, crtablesOut, 0);
	public FixedLengthStringData[][] crtableO = FLSDArrayPartOfArrayStructure(12, 1, crtableOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(240).isAPartOf(crtablesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crtable01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] crtable02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] crtable03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] crtable04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] crtable05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] crtable06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] crtable07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] crtable08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] crtable09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] crtable10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] crtable11Out = FLSArrayPartOfStructure(12, 1, filler7, 120);
	public FixedLengthStringData[] crtable12Out = FLSArrayPartOfStructure(12, 1, filler7, 132);
	public FixedLengthStringData[] crtable13Out = FLSArrayPartOfStructure(12, 1, filler7, 144);
	public FixedLengthStringData[] crtable14Out = FLSArrayPartOfStructure(12, 1, filler7, 156);
	public FixedLengthStringData[] crtable15Out = FLSArrayPartOfStructure(12, 1, filler7, 168);
	public FixedLengthStringData[] crtable16Out = FLSArrayPartOfStructure(12, 1, filler7, 180);
	public FixedLengthStringData[] crtable17Out = FLSArrayPartOfStructure(12, 1, filler7, 192);
	public FixedLengthStringData[] crtable18Out = FLSArrayPartOfStructure(12, 1, filler7, 204);
	public FixedLengthStringData[] crtable19Out = FLSArrayPartOfStructure(12, 1, filler7, 216);
	public FixedLengthStringData[] crtable20Out = FLSArrayPartOfStructure(12, 1, filler7, 228);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData suminssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 420);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(10, 12, suminssOut, 0);
	public FixedLengthStringData[][] suminsO = FLSDArrayPartOfArrayStructure(12, 1, suminsOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(suminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumins01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] sumins02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] sumins03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] sumins04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] sumins05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] sumins06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] sumins07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] sumins08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] sumins09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] sumins10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr51fscreenWritten = new LongData(0);
	public LongData Sr51fprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr51fScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(age01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins01Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins02Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins03Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins04Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins05Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins06Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins07Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins08Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins09Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins10Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable01Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable02Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable03Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable04Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable05Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable06Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable07Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable08Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable09Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable10Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable11Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable12Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable13Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable14Out,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable15Out,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable16Out,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable17Out,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable18Out,new String[] {"48",null, "-48",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable19Out,new String[] {"49",null, "-49",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable20Out,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, age01, sumins01, age02, sumins02, age03, sumins03, age04, sumins04, age05, sumins05, age06, sumins06, age07, sumins07, age08, sumins08, age09, sumins09, age10, sumins10, crtable01, crtable02, crtable03, crtable04, crtable05, crtable06, crtable07, crtable08, crtable09, crtable10, crtable11, crtable12, crtable13, crtable14, crtable15, crtable16, crtable17, crtable18, crtable19, crtable20};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, age01Out, sumins01Out, age02Out, sumins02Out, age03Out, sumins03Out, age04Out, sumins04Out, age05Out, sumins05Out, age06Out, sumins06Out, age07Out, sumins07Out, age08Out, sumins08Out, age09Out, sumins09Out, age10Out, sumins10Out, crtable01Out, crtable02Out, crtable03Out, crtable04Out, crtable05Out, crtable06Out, crtable07Out, crtable08Out, crtable09Out, crtable10Out, crtable11Out, crtable12Out, crtable13Out, crtable14Out, crtable15Out, crtable16Out, crtable17Out, crtable18Out, crtable19Out, crtable20Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, age01Err, sumins01Err, age02Err, sumins02Err, age03Err, sumins03Err, age04Err, sumins04Err, age05Err, sumins05Err, age06Err, sumins06Err, age07Err, sumins07Err, age08Err, sumins08Err, age09Err, sumins09Err, age10Err, sumins10Err, crtable01Err, crtable02Err, crtable03Err, crtable04Err, crtable05Err, crtable06Err, crtable07Err, crtable08Err, crtable09Err, crtable10Err, crtable11Err, crtable12Err, crtable13Err, crtable14Err, crtable15Err, crtable16Err, crtable17Err, crtable18Err, crtable19Err, crtable20Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr51fscreen.class;
		protectRecord = Sr51fprotect.class;
	}

}
