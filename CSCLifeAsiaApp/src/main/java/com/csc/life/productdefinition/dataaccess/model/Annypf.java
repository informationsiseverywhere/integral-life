package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Annypf {
    private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int planSuffix;
    private int guarperd;
    private String freqann;
    private String arrears;
    private String advance;
    private BigDecimal dthpercn;
    private BigDecimal dthperco;
    private BigDecimal intanny;
    private String withprop;
    private String withoprop;
    private String ppind;
    private BigDecimal capcont;
    private String validflag;
    private int tranno;
    private String userProfile;
    private String jobName;
    private String datime;
    private String nomlife;
    
    public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getNomlife() {
        return nomlife;
    }
    public void setNomlife(String nomlife) {
        this.nomlife = nomlife;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public int getPlanSuffix() {
        return planSuffix;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public int getGuarperd() {
        return guarperd;
    }
    public void setGuarperd(int guarperd) {
        this.guarperd = guarperd;
    }
    public String getFreqann() {
        return freqann;
    }
    public void setFreqann(String freqann) {
        this.freqann = freqann;
    }
    public String getArrears() {
        return arrears;
    }
    public void setArrears(String arrears) {
        this.arrears = arrears;
    }
    public String getAdvance() {
        return advance;
    }
    public void setAdvance(String advance) {
        this.advance = advance;
    }
    public BigDecimal getDthpercn() {
        return dthpercn;
    }
    public void setDthpercn(BigDecimal dthpercn) {
        this.dthpercn = dthpercn;
    }
    public BigDecimal getDthperco() {
        return dthperco;
    }
    public void setDthperco(BigDecimal dthperco) {
        this.dthperco = dthperco;
    }
    public BigDecimal getIntanny() {
        return intanny;
    }
    public void setIntanny(BigDecimal intanny) {
        this.intanny = intanny;
    }
    public String getWithprop() {
        return withprop;
    }
    public void setWithprop(String withprop) {
        this.withprop = withprop;
    }
    public String getWithoprop() {
        return withoprop;
    }
    public void setWithoprop(String withoprop) {
        this.withoprop = withoprop;
    }
    public String getPpind() {
        return ppind;
    }
    public void setPpind(String ppind) {
        this.ppind = ppind;
    }
    public BigDecimal getCapcont() {
        return capcont;
    }
    public void setCapcont(BigDecimal capcont) {
        this.capcont = capcont;
    }
    public int getTranno() {
        return tranno;
    }
    public void setTranno(int tranno) {
        this.tranno = tranno;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
}