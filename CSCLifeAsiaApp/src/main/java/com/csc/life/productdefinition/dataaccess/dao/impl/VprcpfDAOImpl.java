package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.BatchUpdateException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.database.QPBaseDataSource;
import com.quipoz.framework.util.IntegralDBProperties;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class VprcpfDAOImpl extends BaseDAOImpl<Vprcpf> implements VprcpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(VprcpfDAOImpl.class);
    String dbType = IntegralDBProperties.getType().trim(); //IBPLIFE-978

    public List<Vprcpf> searchVprcRecord(int intdate, Set<String> vfundSet) {
        StringBuilder sqlVprcSelect1 = new StringBuilder("SELECT UBIDPR,ULTYPE,VRTFND FROM VPRCPF WHERE ");
        sqlVprcSelect1.append(" EFFDATE<=? and (ULTYPE='I' or ULTYPE='A') AND ");

        sqlVprcSelect1.append(getSqlInStr("VRTFND", new ArrayList<String>(vfundSet)));

        sqlVprcSelect1.append(" ORDER BY VRTFND ASC, ULTYPE ASC, EFFDATE DESC, JOBNO ASC, UNIQUE_NUMBER DESC ");
        PreparedStatement psVprcSelect = getPrepareStatement(sqlVprcSelect1.toString());
        ResultSet sqlvprcpf1rs = null;
        List<Vprcpf> vprcpfSearchResult = new LinkedList<Vprcpf>();
        try {
            psVprcSelect.setInt(1, intdate);
            sqlvprcpf1rs = executeQuery(psVprcSelect);

            while (sqlvprcpf1rs.next()) {
                Vprcpf vprcpf = new Vprcpf();
                vprcpf.setUnitBidPrice(sqlvprcpf1rs.getBigDecimal(1));
                vprcpf.setUnitType(sqlvprcpf1rs.getString(2));
                vprcpf.setUnitVirtualFund(sqlvprcpf1rs.getString(3));
                vprcpfSearchResult.add(vprcpf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchVprcRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psVprcSelect, sqlvprcpf1rs);
        }
        return vprcpfSearchResult;
    }
    // Start ILIFE-3407 vdivisala
 	@Override
 	public Vprcpf getNowPrice(String company, String unitVirtualFund, String unitType, int effdate,	String validflag) {
 		StringBuilder query = new StringBuilder("SELECT EFFDATE, UBREPR, UBIDPR, UOFFPR FROM VPRNUDL ");
 		query.append("WHERE COMPANY=? AND VRTFND=? AND ULTYPE=? AND VALIDFLAG=? AND EFFDATE<=? ");
 		query.append("ORDER BY COMPANY ASC, VRTFND ASC, ULTYPE ASC, EFFDATE DESC, UNIQUE_NUMBER DESC");
 		PreparedStatement stmt = null;
 		ResultSet rs = null;
 		Vprcpf vprcpf = null;
 		try {
 			stmt = getPrepareStatement(query.toString());
 			stmt.setString(1, company);
 			stmt.setString(2, unitVirtualFund);
 			stmt.setString(3, unitType);
 			stmt.setString(4, validflag);
 			stmt.setInt(5, effdate);
 			rs = stmt.executeQuery();
 			if (rs.next()) {
 				vprcpf = new Vprcpf();
 				vprcpf.setEffdate(rs.getInt(1));
 				vprcpf.setUnitBarePrice(rs.getBigDecimal(2));
 				vprcpf.setUnitBidPrice(rs.getBigDecimal(3));
 				vprcpf.setUnitOfferPrice(rs.getBigDecimal(4));
 			}
 			
 		} catch (SQLException e) {
 			LOGGER.error("getNowPrice()", e);//IJTI-1485
 			throw new SQLRuntimeException(e);
 		} finally {
 			close(stmt, rs);
 		}
 		return vprcpf;
 	} // End ILIFE-3407
    
 // BRD-78 NAV Upload:Start
 	@Override
	public List<Integer> insert(List<Vprcpf> vprcpfList) {
		List<Integer> insertStatus = new ArrayList<>();
		StringBuilder query = new StringBuilder("INSERT INTO VPRCPF ");
		query.append("(COMPANY, VRTFND, ULTYPE, EFFDATE, JOBNO, UBIDPR, UOFFPR, VALIDFLAG, TRANNO, UPDDTE, PROCFLAG, TRTM)");
		query.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		final int batchSize = 100;
		int batchCount = 0;
		try (PreparedStatement stmt = getPrepareStatement(query.toString())){
			for(Vprcpf vprcpf : vprcpfList){
				stmt.setString(1, vprcpf.getCompany());
				stmt.setString(2, vprcpf.getUnitVirtualFund());
				stmt.setString(3, vprcpf.getUnitType());
				stmt.setInt(4, vprcpf.getEffdate());
				stmt.setInt(5, vprcpf.getJobno());
				stmt.setBigDecimal(6, vprcpf.getUnitBidPrice());
				stmt.setBigDecimal(7, vprcpf.getUnitOfferPrice());
				stmt.setString(8, vprcpf.getValidflag());
				stmt.setInt(9, vprcpf.getTranno());
				stmt.setInt(10, vprcpf.getUpdateDate());
				stmt.setString(11, vprcpf.getProcflag());
				stmt.setInt(12, vprcpf.getTransactionTime());
				stmt.addBatch();
				if(++batchCount % batchSize == 0){
					insertStatus.addAll(executeBatchInsert(stmt, batchSize));
				}
			}
			insertStatus.addAll(executeBatchInsert(stmt, (batchCount % batchSize)));
		}catch (SQLException e) {
			// To update status of remaining records during exception
			if(insertStatus.size() < vprcpfList.size()){
				for(int count = insertStatus.size() ; count < vprcpfList.size(); count++){
					insertStatus.add(Statement.EXECUTE_FAILED);
				}
			}
			LOGGER.error("insert()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
		}
		return insertStatus;
	}
    
	private List<Integer> executeBatchInsert(PreparedStatement stmt, int insertSize) throws SQLException{
		List<Integer> insertResult = new ArrayList<>();
		try {
			int[] insertCountArray = stmt.executeBatch();
			for(int insertCount : insertCountArray){
				insertResult.add(insertCount);
			}
		} catch (BatchUpdateException e) {
			LOGGER.error("executeBatchInsert()", e);//IJTI-1485
			
			int[] insertCountArray = e.getUpdateCounts();
			for(int insertCount : insertCountArray){
				insertResult.add(insertCount);
			}
			//Below code may be needed for drivers which  may not continue to process the remaining commands in the batch
			//upon one failure
			if(insertResult.size() < insertSize){
				for(int count = insertResult.size() ; count < insertSize; count++){
					insertResult.add(Statement.EXECUTE_FAILED);
				}
			}
		}
		return insertResult;
	}
	
	@Override
	public Integer getMaxJobNumber() {
		String query = "SELECT MAX(JOBNO) FROM VPRCPF";
		Integer count = 0;
		try (
				Statement stmt = getConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);/* IJTI-1523 */
				){
			if(rs.next())
			{
				count = rs.getInt(1);
			}								
		} catch (SQLException e) {
			LOGGER.error("controlTotalsGext()",e);//IJTI-1485
			throw new SQLRuntimeException(e);
		}
		return count;
	}
	// BRD-78 NAV Upload:End
	
	public Map<String, List<Vprcpf>> searchVprnudlRecord(int intdate, List<String> vfundSet, String company) {
		StringBuilder sqlVprcSelect1 = new StringBuilder("SELECT UBIDPR,ULTYPE,VRTFND FROM VPRNUDL WHERE ");
		sqlVprcSelect1.append(" EFFDATE<=? AND COMPANY = ? AND ");

		sqlVprcSelect1.append(getSqlInStr("VRTFND", new ArrayList<String>(vfundSet)));

		sqlVprcSelect1.append(" ORDER BY COMPANY ASC, VRTFND ASC, ULTYPE ASC, EFFDATE DESC, UNIQUE_NUMBER DESC ");
		PreparedStatement psVprcSelect = getPrepareStatement(sqlVprcSelect1.toString());
		ResultSet sqlvprcpf1rs = null;
		Map<String, List<Vprcpf>> vprcpfSearchResult = new HashMap<String, List<Vprcpf>>();
		try {
			psVprcSelect.setInt(1, intdate);
			psVprcSelect.setString(2, company);
			sqlvprcpf1rs = executeQuery(psVprcSelect);

			while (sqlvprcpf1rs.next()) {
				Vprcpf vprcpf = new Vprcpf();
				vprcpf.setUnitBidPrice(sqlvprcpf1rs.getBigDecimal("UBIDPR"));
				vprcpf.setUnitType(sqlvprcpf1rs.getString("ULTYPE"));
				vprcpf.setUnitVirtualFund(sqlvprcpf1rs.getString("VRTFND"));

				if (vprcpfSearchResult.containsKey(vprcpf.getUnitVirtualFund())) {
					vprcpfSearchResult.get(vprcpf.getUnitVirtualFund()).add(vprcpf);
				} else {
					List<Vprcpf> vList = new ArrayList<Vprcpf>();
					vList.add(vprcpf);
					vprcpfSearchResult.put(vprcpf.getUnitVirtualFund(), vList);
				}

			}
		} catch (SQLException e) {
			LOGGER.error("searchVprnudlRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psVprcSelect, sqlvprcpf1rs);
		}
		return vprcpfSearchResult;
	}	
	
	BigDecimal cur_unit = BigDecimal.ZERO;
 	public BigDecimal  getvprcRecord(String chdrcoy, String unitVirtualFund, int effdate) {
 		
 		StringBuilder query = new StringBuilder("select UOFFPR from (SELECT row_number() over(ORDER BY EFFDATE DESC, JOBNO DESC) as rank, VPRCPF.* FROM VPRCPF WHERE COMPANY=? AND VRTFND=? AND ULTYPE=? AND VALIDFLAG=? AND EFFDATE<=?  ) s1 where s1.rank <= 1");
 	
 		PreparedStatement stmt = null;
 		ResultSet rs = null;
 		Vprcpf vprcpf = null;
 		 List<Vprcpf> vprcpfSearchResult = new LinkedList<Vprcpf>();
 		try {
 			stmt = getPrepareStatement(query.toString());
 			stmt.setString(1, chdrcoy);
 			stmt.setString(2, unitVirtualFund);
 			stmt.setString(3, "A");
 			stmt.setString(4, "1");
 			stmt.setInt(5, effdate);
 			rs = stmt.executeQuery();
 			if (rs.next()) {
 				cur_unit = rs.getBigDecimal("UOFFPR");	
 			}
 			
 		} catch (SQLException e) {
 			LOGGER.error("getvprcRecord()", e);//IJTI-1485
 			throw new SQLRuntimeException(e);
 		} finally {
 			close(stmt, rs);
 		}
 		return cur_unit;
 	} // End ILIFE-3407
 	//ALS-4706
 	@Override
 	public BigDecimal  getUnitBidPrice(String chdrcoy,String unitVirtualFund, int effdate){
   		BigDecimal unitBidPrice = BigDecimal.ZERO;
			StringBuilder query = new StringBuilder();  //IBPLIFE-978 Different attribute names used for MSSQL and Oracle.
   				if(QPBaseDataSource.DATABASE_ORACLE.equals(dbType)){  //IBPLIFE-978
   				query.append("SELECT UBIDPR from (SELECT row_number() over(ORDER BY COMPANY ASC, VRTFND ASC, ULTYPE ASC, EFFDATE DESC, UNIQUE_NUMBER DESC) "); //IBLIFE-2349, issue coming because of IBLIFE-978
   				query.append("as rank, VPRCPF.* FROM VPRCPF WHERE COMPANY=? AND VRTFND=? AND EFFDATE<=? ) s1 where s1.rank <= 1");  
   			}  //IBPLIFE-978
   			else {  
   				query.append("SELECT TOP 1 UBIDPR FROM VPRCPF WHERE ");
   				query.append("COMPANY=? AND VRTFND=? AND EFFDATE<=? ");
   				query.append("ORDER BY COMPANY ASC, VRTFND ASC, ULTYPE ASC, EFFDATE DESC, UNIQUE_NUMBER DESC");
   			}
   				PreparedStatement stmt = null;
   				ResultSet rs = null;
 		try {
 			stmt = getPrepareStatement(query.toString());
 			stmt.setString(1, chdrcoy);
 			stmt.setString(2, unitVirtualFund);
 			stmt.setInt(3, effdate);
 			rs = stmt.executeQuery();
 			if (rs.next()) {
 				unitBidPrice = rs.getBigDecimal("UBIDPR");	
 			}
 		} catch (SQLException e) {
 			LOGGER.error("getvprcRecord()", e);//IJTI-1485
 			throw new SQLRuntimeException(e);
 		} finally {
 			close(stmt, rs);
 		}
 		return unitBidPrice;
 	}
}