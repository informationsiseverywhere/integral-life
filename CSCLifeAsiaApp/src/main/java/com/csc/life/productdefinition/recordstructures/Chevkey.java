package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:30
 * Description:
 * Copybook name: CHEVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chevKey = new FixedLengthStringData(64).isAPartOf(chevFileKey, 0, REDEFINE);
  	public FixedLengthStringData chevChdrcoy = new FixedLengthStringData(1).isAPartOf(chevKey, 0);
  	public FixedLengthStringData chevChdrnum = new FixedLengthStringData(8).isAPartOf(chevKey, 1);
  	public PackedDecimalData chevEffdate = new PackedDecimalData(8, 0).isAPartOf(chevKey, 9);
  	public PackedDecimalData chevCurrfrom = new PackedDecimalData(8, 0).isAPartOf(chevKey, 14);
  	public FixedLengthStringData chevValidflag = new FixedLengthStringData(1).isAPartOf(chevKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(chevKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}