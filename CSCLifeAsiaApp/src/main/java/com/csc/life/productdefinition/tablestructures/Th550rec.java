package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:13
 * Description:
 * Copybook name: TH550REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th550rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th550Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zkeys = new FixedLengthStringData(194).isAPartOf(th550Rec, 0);
  	public FixedLengthStringData[] zkey = FLSArrayPartOfStructure(97, 2, zkeys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(194).isAPartOf(zkeys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zkey01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData zkey02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData zkey03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData zkey04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData zkey05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData zkey06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData zkey07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData zkey08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData zkey09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData zkey10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData zkey11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData zkey12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData zkey13 = new FixedLengthStringData(2).isAPartOf(filler, 24);
  	public FixedLengthStringData zkey14 = new FixedLengthStringData(2).isAPartOf(filler, 26);
  	public FixedLengthStringData zkey15 = new FixedLengthStringData(2).isAPartOf(filler, 28);
  	public FixedLengthStringData zkey16 = new FixedLengthStringData(2).isAPartOf(filler, 30);
  	public FixedLengthStringData zkey17 = new FixedLengthStringData(2).isAPartOf(filler, 32);
  	public FixedLengthStringData zkey18 = new FixedLengthStringData(2).isAPartOf(filler, 34);
  	public FixedLengthStringData zkey19 = new FixedLengthStringData(2).isAPartOf(filler, 36);
  	public FixedLengthStringData zkey20 = new FixedLengthStringData(2).isAPartOf(filler, 38);
  	public FixedLengthStringData zkey21 = new FixedLengthStringData(2).isAPartOf(filler, 40);
  	public FixedLengthStringData zkey22 = new FixedLengthStringData(2).isAPartOf(filler, 42);
  	public FixedLengthStringData zkey23 = new FixedLengthStringData(2).isAPartOf(filler, 44);
  	public FixedLengthStringData zkey24 = new FixedLengthStringData(2).isAPartOf(filler, 46);
  	public FixedLengthStringData zkey25 = new FixedLengthStringData(2).isAPartOf(filler, 48);
  	public FixedLengthStringData zkey26 = new FixedLengthStringData(2).isAPartOf(filler, 50);
  	public FixedLengthStringData zkey27 = new FixedLengthStringData(2).isAPartOf(filler, 52);
  	public FixedLengthStringData zkey28 = new FixedLengthStringData(2).isAPartOf(filler, 54);
  	public FixedLengthStringData zkey29 = new FixedLengthStringData(2).isAPartOf(filler, 56);
  	public FixedLengthStringData zkey30 = new FixedLengthStringData(2).isAPartOf(filler, 58);
  	public FixedLengthStringData zkey31 = new FixedLengthStringData(2).isAPartOf(filler, 60);
  	public FixedLengthStringData zkey32 = new FixedLengthStringData(2).isAPartOf(filler, 62);
  	public FixedLengthStringData zkey33 = new FixedLengthStringData(2).isAPartOf(filler, 64);
  	public FixedLengthStringData zkey34 = new FixedLengthStringData(2).isAPartOf(filler, 66);
  	public FixedLengthStringData zkey35 = new FixedLengthStringData(2).isAPartOf(filler, 68);
  	public FixedLengthStringData zkey36 = new FixedLengthStringData(2).isAPartOf(filler, 70);
  	public FixedLengthStringData zkey37 = new FixedLengthStringData(2).isAPartOf(filler, 72);
  	public FixedLengthStringData zkey38 = new FixedLengthStringData(2).isAPartOf(filler, 74);
  	public FixedLengthStringData zkey39 = new FixedLengthStringData(2).isAPartOf(filler, 76);
  	public FixedLengthStringData zkey40 = new FixedLengthStringData(2).isAPartOf(filler, 78);
  	public FixedLengthStringData zkey41 = new FixedLengthStringData(2).isAPartOf(filler, 80);
  	public FixedLengthStringData zkey42 = new FixedLengthStringData(2).isAPartOf(filler, 82);
  	public FixedLengthStringData zkey43 = new FixedLengthStringData(2).isAPartOf(filler, 84);
  	public FixedLengthStringData zkey44 = new FixedLengthStringData(2).isAPartOf(filler, 86);
  	public FixedLengthStringData zkey45 = new FixedLengthStringData(2).isAPartOf(filler, 88);
  	public FixedLengthStringData zkey46 = new FixedLengthStringData(2).isAPartOf(filler, 90);
  	public FixedLengthStringData zkey47 = new FixedLengthStringData(2).isAPartOf(filler, 92);
  	public FixedLengthStringData zkey48 = new FixedLengthStringData(2).isAPartOf(filler, 94);
  	public FixedLengthStringData zkey49 = new FixedLengthStringData(2).isAPartOf(filler, 96);
  	public FixedLengthStringData zkey50 = new FixedLengthStringData(2).isAPartOf(filler, 98);
  	public FixedLengthStringData zkey51 = new FixedLengthStringData(2).isAPartOf(filler, 100);
  	public FixedLengthStringData zkey52 = new FixedLengthStringData(2).isAPartOf(filler, 102);
  	public FixedLengthStringData zkey53 = new FixedLengthStringData(2).isAPartOf(filler, 104);
  	public FixedLengthStringData zkey54 = new FixedLengthStringData(2).isAPartOf(filler, 106);
  	public FixedLengthStringData zkey55 = new FixedLengthStringData(2).isAPartOf(filler, 108);
  	public FixedLengthStringData zkey56 = new FixedLengthStringData(2).isAPartOf(filler, 110);
  	public FixedLengthStringData zkey57 = new FixedLengthStringData(2).isAPartOf(filler, 112);
  	public FixedLengthStringData zkey58 = new FixedLengthStringData(2).isAPartOf(filler, 114);
  	public FixedLengthStringData zkey59 = new FixedLengthStringData(2).isAPartOf(filler, 116);
  	public FixedLengthStringData zkey60 = new FixedLengthStringData(2).isAPartOf(filler, 118);
  	public FixedLengthStringData zkey61 = new FixedLengthStringData(2).isAPartOf(filler, 120);
  	public FixedLengthStringData zkey62 = new FixedLengthStringData(2).isAPartOf(filler, 122);
  	public FixedLengthStringData zkey63 = new FixedLengthStringData(2).isAPartOf(filler, 124);
  	public FixedLengthStringData zkey64 = new FixedLengthStringData(2).isAPartOf(filler, 126);
  	public FixedLengthStringData zkey65 = new FixedLengthStringData(2).isAPartOf(filler, 128);
  	public FixedLengthStringData zkey66 = new FixedLengthStringData(2).isAPartOf(filler, 130);
  	public FixedLengthStringData zkey67 = new FixedLengthStringData(2).isAPartOf(filler, 132);
  	public FixedLengthStringData zkey68 = new FixedLengthStringData(2).isAPartOf(filler, 134);
  	public FixedLengthStringData zkey69 = new FixedLengthStringData(2).isAPartOf(filler, 136);
  	public FixedLengthStringData zkey70 = new FixedLengthStringData(2).isAPartOf(filler, 138);
  	public FixedLengthStringData zkey71 = new FixedLengthStringData(2).isAPartOf(filler, 140);
  	public FixedLengthStringData zkey72 = new FixedLengthStringData(2).isAPartOf(filler, 142);
  	public FixedLengthStringData zkey73 = new FixedLengthStringData(2).isAPartOf(filler, 144);
  	public FixedLengthStringData zkey74 = new FixedLengthStringData(2).isAPartOf(filler, 146);
  	public FixedLengthStringData zkey75 = new FixedLengthStringData(2).isAPartOf(filler, 148);
  	public FixedLengthStringData zkey76 = new FixedLengthStringData(2).isAPartOf(filler, 150);
  	public FixedLengthStringData zkey77 = new FixedLengthStringData(2).isAPartOf(filler, 152);
  	public FixedLengthStringData zkey78 = new FixedLengthStringData(2).isAPartOf(filler, 154);
  	public FixedLengthStringData zkey79 = new FixedLengthStringData(2).isAPartOf(filler, 156);
  	public FixedLengthStringData zkey80 = new FixedLengthStringData(2).isAPartOf(filler, 158);
  	public FixedLengthStringData zkey81 = new FixedLengthStringData(2).isAPartOf(filler, 160);
  	public FixedLengthStringData zkey82 = new FixedLengthStringData(2).isAPartOf(filler, 162);
  	public FixedLengthStringData zkey83 = new FixedLengthStringData(2).isAPartOf(filler, 164);
  	public FixedLengthStringData zkey84 = new FixedLengthStringData(2).isAPartOf(filler, 166);
  	public FixedLengthStringData zkey85 = new FixedLengthStringData(2).isAPartOf(filler, 168);
  	public FixedLengthStringData zkey86 = new FixedLengthStringData(2).isAPartOf(filler, 170);
  	public FixedLengthStringData zkey87 = new FixedLengthStringData(2).isAPartOf(filler, 172);
  	public FixedLengthStringData zkey88 = new FixedLengthStringData(2).isAPartOf(filler, 174);
  	public FixedLengthStringData zkey89 = new FixedLengthStringData(2).isAPartOf(filler, 176);
  	public FixedLengthStringData zkey90 = new FixedLengthStringData(2).isAPartOf(filler, 178);
  	public FixedLengthStringData zkey91 = new FixedLengthStringData(2).isAPartOf(filler, 180);
  	public FixedLengthStringData zkey92 = new FixedLengthStringData(2).isAPartOf(filler, 182);
  	public FixedLengthStringData zkey93 = new FixedLengthStringData(2).isAPartOf(filler, 184);
  	public FixedLengthStringData zkey94 = new FixedLengthStringData(2).isAPartOf(filler, 186);
  	public FixedLengthStringData zkey95 = new FixedLengthStringData(2).isAPartOf(filler, 188);
  	public FixedLengthStringData zkey96 = new FixedLengthStringData(2).isAPartOf(filler, 190);
  	public FixedLengthStringData zkey97 = new FixedLengthStringData(2).isAPartOf(filler, 192);
  	public FixedLengthStringData zrterms = new FixedLengthStringData(194).isAPartOf(th550Rec, 194);
  	public ZonedDecimalData[] zrterm = ZDArrayPartOfStructure(97, 2, 0, zrterms, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(194).isAPartOf(zrterms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrterm01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData zrterm02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData zrterm03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData zrterm04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData zrterm05 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData zrterm06 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData zrterm07 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData zrterm08 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
  	public ZonedDecimalData zrterm09 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData zrterm10 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData zrterm11 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 20);
  	public ZonedDecimalData zrterm12 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 22);
  	public ZonedDecimalData zrterm13 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData zrterm14 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 26);
  	public ZonedDecimalData zrterm15 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 28);
  	public ZonedDecimalData zrterm16 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData zrterm17 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 32);
  	public ZonedDecimalData zrterm18 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 34);
  	public ZonedDecimalData zrterm19 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 36);
  	public ZonedDecimalData zrterm20 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 38);
  	public ZonedDecimalData zrterm21 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 40);
  	public ZonedDecimalData zrterm22 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 42);
  	public ZonedDecimalData zrterm23 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 44);
  	public ZonedDecimalData zrterm24 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 46);
  	public ZonedDecimalData zrterm25 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 48);
  	public ZonedDecimalData zrterm26 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 50);
  	public ZonedDecimalData zrterm27 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 52);
  	public ZonedDecimalData zrterm28 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 54);
  	public ZonedDecimalData zrterm29 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 56);
  	public ZonedDecimalData zrterm30 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 58);
  	public ZonedDecimalData zrterm31 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 60);
  	public ZonedDecimalData zrterm32 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 62);
  	public ZonedDecimalData zrterm33 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 64);
  	public ZonedDecimalData zrterm34 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 66);
  	public ZonedDecimalData zrterm35 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 68);
  	public ZonedDecimalData zrterm36 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 70);
  	public ZonedDecimalData zrterm37 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 72);
  	public ZonedDecimalData zrterm38 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 74);
  	public ZonedDecimalData zrterm39 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 76);
  	public ZonedDecimalData zrterm40 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 78);
  	public ZonedDecimalData zrterm41 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 80);
  	public ZonedDecimalData zrterm42 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 82);
  	public ZonedDecimalData zrterm43 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 84);
  	public ZonedDecimalData zrterm44 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 86);
  	public ZonedDecimalData zrterm45 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 88);
  	public ZonedDecimalData zrterm46 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 90);
  	public ZonedDecimalData zrterm47 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 92);
  	public ZonedDecimalData zrterm48 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 94);
  	public ZonedDecimalData zrterm49 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 96);
  	public ZonedDecimalData zrterm50 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 98);
  	public ZonedDecimalData zrterm51 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 100);
  	public ZonedDecimalData zrterm52 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 102);
  	public ZonedDecimalData zrterm53 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 104);
  	public ZonedDecimalData zrterm54 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 106);
  	public ZonedDecimalData zrterm55 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 108);
  	public ZonedDecimalData zrterm56 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 110);
  	public ZonedDecimalData zrterm57 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 112);
  	public ZonedDecimalData zrterm58 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 114);
  	public ZonedDecimalData zrterm59 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 116);
  	public ZonedDecimalData zrterm60 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 118);
  	public ZonedDecimalData zrterm61 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 120);
  	public ZonedDecimalData zrterm62 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 122);
  	public ZonedDecimalData zrterm63 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 124);
  	public ZonedDecimalData zrterm64 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 126);
  	public ZonedDecimalData zrterm65 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 128);
  	public ZonedDecimalData zrterm66 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 130);
  	public ZonedDecimalData zrterm67 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 132);
  	public ZonedDecimalData zrterm68 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 134);
  	public ZonedDecimalData zrterm69 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 136);
  	public ZonedDecimalData zrterm70 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 138);
  	public ZonedDecimalData zrterm71 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 140);
  	public ZonedDecimalData zrterm72 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 142);
  	public ZonedDecimalData zrterm73 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 144);
  	public ZonedDecimalData zrterm74 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 146);
  	public ZonedDecimalData zrterm75 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 148);
  	public ZonedDecimalData zrterm76 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 150);
  	public ZonedDecimalData zrterm77 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 152);
  	public ZonedDecimalData zrterm78 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 154);
  	public ZonedDecimalData zrterm79 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 156);
  	public ZonedDecimalData zrterm80 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 158);
  	public ZonedDecimalData zrterm81 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 160);
  	public ZonedDecimalData zrterm82 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 162);
  	public ZonedDecimalData zrterm83 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 164);
  	public ZonedDecimalData zrterm84 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 166);
  	public ZonedDecimalData zrterm85 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 168);
  	public ZonedDecimalData zrterm86 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 170);
  	public ZonedDecimalData zrterm87 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 172);
  	public ZonedDecimalData zrterm88 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 174);
  	public ZonedDecimalData zrterm89 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 176);
  	public ZonedDecimalData zrterm90 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 178);
  	public ZonedDecimalData zrterm91 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 180);
  	public ZonedDecimalData zrterm92 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 182);
  	public ZonedDecimalData zrterm93 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 184);
  	public ZonedDecimalData zrterm94 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 186);
  	public ZonedDecimalData zrterm95 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 188);
  	public ZonedDecimalData zrterm96 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 190);
  	public ZonedDecimalData zrterm97 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 192);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(112).isAPartOf(th550Rec, 388, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th550Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th550Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}