package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class MedxTemppf {
	
	private long uniqueNumber;
	private String chdrcoy; 
	private String chdrnum;
	private int seqno;
	private String exmcode;
	private String zmedtyp;
	private String paidby; 
	private String life;
	private String jlife;
	private int effdate;
	private String invref; 
	private BigDecimal zmedfee;
	private String clntnum;
	private int dtetrm;
	private String descT;
	private String memberName;
	private String lifcnum;
	private Br64xDTO br64xDTO;
	private Br646DTO br646DTO;
	
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getExmcode() {
		return exmcode;
	}
	public void setExmcode(String exmcode) {
		this.exmcode = exmcode;
	}
	public String getZmedtyp() {
		return zmedtyp;
	}
	public void setZmedtyp(String zmedtyp) {
		this.zmedtyp = zmedtyp;
	}
	public String getPaidby() {
		return paidby;
	}
	public void setPaidby(String paidby) {
		this.paidby = paidby;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getInvref() {
		return invref;
	}
	public void setInvref(String invref) {
		this.invref = invref;
	}
	public BigDecimal getZmedfee() {
		return zmedfee;
	}
	public void setZmedfee(BigDecimal zmedfee) {
		this.zmedfee = zmedfee;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public int getDtetrm() {
		return dtetrm;
	}
	public void setDtetrm(int dtetrm) {
		this.dtetrm = dtetrm;
	}
	public String getDescT() {
		return descT;
	}
	public void setDescT(String descT) {
		this.descT = descT;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}
	public Br64xDTO getBr64xDTO() {
		return br64xDTO;
	}
	public void setBr64xDTO(Br64xDTO br64xdto) {
		br64xDTO = br64xdto;
	}
	public Br646DTO getBr646DTO() {
		return br646DTO;
	}
	public void setBr646DTO(Br646DTO br646dto) {
		br646DTO = br646dto;
	}
	
}
