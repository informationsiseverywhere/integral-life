/*
 * File: Vpxchdr.java
 * Date: 20 August 2012 22:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Polcalcrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpxcovrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This subroutine will extract the necessary details from COVT or 
* COVR for policy level calculations.
*  
* This data extract subroutine will cater for the requirements of 
* the following Policy Level Calculation Programs: P6378, P5074 and
*  P5074AT. This subroutine will be performed via the TR28X set-up 
*  for item PL01.  	 
* 
* New Copybooks POLCALCREC and VPXCOVREC will be the linkages of 
* this subroutine.
* 
* VPXCOVREC Copybook
*   1.	LIFE					X(02)
*   2.	JLIFE					X(02)
*   3.	COVERAGE				X(02)
*   4.	RIDER					X(02)
*   5.	CRTABLE 					X(04) 
*   6.	Single Premium (from COVTLNB-SINGP) 	S9(15)V9(02)
*   7.	Regular Premium (from COVTLNB-INSTPREM) 	S9(15)V9(02)
*   8.	Base Premium (from COVTLNB-ZBINSTPREM) 	S9(15)V9(02)
* 
* Processing:
* 
* 	Read COVTLNB until end of file is reached
* 	For each COVTLNB record found
* 	   MOVE corresponding COVTLNB fields to POLCALCREC counterparts 
* 	End-For
* 	
* 	## During New Business, A Policy will only have either COVT 
*   records or COVR records but not both 
* 	COVT records are for unissued policies while COVR are for 
*   issued ones ##
* 	If at least 1 COVTLNB record is found
* 	   EXIT SUBROUTINE
* 	End-if
*
* 	## No COVT record found, proceed with retrieving data from COVR
*    instead ## 
* 	Read COVRLNB until end of file is reached
* 	For each COVRLNB record found
* 	   MOVE corresponding COVRLNB fields to POLCALCREC counterparts 
* 	End-For
* </pre>
* ******************************************
**/ 
public class Vpxcov extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPXLIFEALL";
		/* FORMATS */
	private String chdrlnbrec = "CHDRLNBREC";
	private String clexrec = "CLEXREC";
		/* ERRORS */
	private String e882 = "E882";
	private String f950 = "F950";
		/*Agent header*/
	private Syserrrec syserrrec = new Syserrrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Vpxcovrec vpxcovrec= new Vpxcovrec();
	private Polcalcrec polcalcrec = new Polcalcrec();
	/*Life and joint life details - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Varcom varcom = new Varcom();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private String wsaaIssuedPolicyFlg = "N";
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9020, 
		exit400, 
		exit440, 
		nextCovr430, 
		nextCovt300
	}

	public Vpxcov() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vpxcovrec.vpxcovRec = convertAndSetParam(vpxcovrec.vpxcovRec, parmArray, 1);
		polcalcrec.polcalcRec = convertAndSetParam(polcalcrec.polcalcRec, parmArray, 0);
		try {
			startSubr100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr100()
	{
		readCovtlnb200();
		checkCovtExist300();
		if (isEQ(wsaaIssuedPolicyFlg, "Y")){
			readCovrlnb400();
		}
		exit090();
	}

protected void readCovtlnb200()
{
	covtlnbIO.setDataKey(SPACES);
	covtlnbIO.setChdrcoy(polcalcrec.chdrcoy);
	covtlnbIO.setChdrnum(polcalcrec.chdrnum);
	covtlnbIO.setLife("00");
//	covtlnbIO.setJlife(SPACES);
	covtlnbIO.setFunction(varcom.begn);
	wsaaSub.set(0);
	while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		loopCovtlnb120();
	}
}

protected void loopCovtlnb120()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startCovt200();
			}
			case nextCovt300: {
				nextCovt300();
			}
			case exit400: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void startCovt200()
{
	
	SmartFileCode.execute(appVars, covtlnbIO);
	if (isNE(covtlnbIO.getStatuz(),varcom.oK)
	&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(covtlnbIO.getParams());
		fatalError6000();
	}
	if (isNE(covtlnbIO.getChdrcoy(),polcalcrec.chdrcoy)
	|| isNE(covtlnbIO.getChdrnum(),polcalcrec.chdrnum)) {
		covtlnbIO.setStatuz(varcom.endp);
		goTo(GotoLabel.exit400);
	} 
	wsaaSub.add(1);
	vpxcovrec.life[wsaaSub.toInt()].set(covtlnbIO.getLife());
	vpxcovrec.jlife[wsaaSub.toInt()].set(covtlnbIO.getJlife());
	vpxcovrec.coverage[wsaaSub.toInt()].set(covtlnbIO.getCoverage());
	vpxcovrec.rider[wsaaSub.toInt()].set(covtlnbIO.getRider());
	vpxcovrec.crtable[wsaaSub.toInt()].set(covtlnbIO.getCrtable());
	vpxcovrec.singPrem[wsaaSub.toInt()].set(covtlnbIO.getSingp());
	vpxcovrec.regPrem[wsaaSub.toInt()].set(covtlnbIO.getInstprem());
	vpxcovrec.basPrem[wsaaSub.toInt()].set(covtlnbIO.getZbinstprem());
	goTo(GotoLabel.nextCovt300);
}

protected void nextCovt300()
{
	covtlnbIO.setFunction(varcom.nextr);
}


protected void checkCovtExist300()
{
	if (isEQ(wsaaSub.toInt(), ZERO)){
		wsaaIssuedPolicyFlg = "Y";
	}
}

protected void readCovrlnb400()
{
	covrlnbIO.setDataKey(SPACES);
	covrlnbIO.setChdrcoy(polcalcrec.chdrcoy);
	covrlnbIO.setChdrnum(polcalcrec.chdrnum);
	covrlnbIO.setLife("00");
//	covrlnbIO.setJlife(SPACES);
	covrlnbIO.setFunction(varcom.begn);
	wsaaSub1.set(0);
	while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		loopCovrlnb410();
	}
}

protected void loopCovrlnb410()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startCovr420();
			}
			case nextCovr430: {
				nextCovr430();
			}
			case exit440: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void startCovr420()
{
	
	SmartFileCode.execute(appVars, covrlnbIO);
	if (isNE(covrlnbIO.getStatuz(),varcom.oK)
	&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(covrlnbIO.getParams());
		fatalError6000();
	}
	if (isNE(covrlnbIO.getChdrcoy(),polcalcrec.chdrcoy)
	|| isNE(covrlnbIO.getChdrnum(),polcalcrec.chdrnum)) {
		covrlnbIO.setStatuz(varcom.endp);
		goTo(GotoLabel.exit400);
	} 
	wsaaSub1.add(1);
	vpxcovrec.life[wsaaSub1.toInt()].set(covrlnbIO.getLife());
	vpxcovrec.jlife[wsaaSub1.toInt()].set(covrlnbIO.getJlife());
	vpxcovrec.coverage[wsaaSub1.toInt()].set(covrlnbIO.getCoverage());
	vpxcovrec.rider[wsaaSub1.toInt()].set(covrlnbIO.getRider());
	vpxcovrec.crtable[wsaaSub1.toInt()].set(covrlnbIO.getCrtable());
	vpxcovrec.singPrem[wsaaSub1.toInt()].set(covrlnbIO.getSingp());
	vpxcovrec.regPrem[wsaaSub1.toInt()].set(covrlnbIO.getInstprem());
	vpxcovrec.basPrem[wsaaSub1.toInt()].set(covrlnbIO.getZbinstprem());
	goTo(GotoLabel.nextCovr430);
}

protected void nextCovr430()
{
	covrlnbIO.setFunction(varcom.nextr);
}

protected void fatalError6000()
{
	/*ERROR*/
	syserrrec.subrname.set(wsaaSubr);
	if (isNE(syserrrec.statuz,SPACES)
	|| isNE(syserrrec.syserrStatuz,SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT*/
	goBack();
}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
protected void exit090()
	{
		exitProgram();
	}
}
