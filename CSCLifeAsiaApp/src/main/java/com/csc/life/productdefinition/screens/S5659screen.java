package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5659screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5659ScreenVars sv = (S5659ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5659screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5659ScreenVars screenVars = (S5659ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.volbanfr01.setClassString("");
		screenVars.volbanto01.setClassString("");
		screenVars.volbanle01.setClassString("");
		screenVars.volbanfr02.setClassString("");
		screenVars.volbanto02.setClassString("");
		screenVars.volbanle02.setClassString("");
		screenVars.volbanfr03.setClassString("");
		screenVars.volbanto03.setClassString("");
		screenVars.volbanle03.setClassString("");
		screenVars.volbanfr04.setClassString("");
		screenVars.volbanto04.setClassString("");
		screenVars.volbanle04.setClassString("");
		screenVars.rndfact.setClassString("");
	}

/**
 * Clear all the variables in S5659screen
 */
	public static void clear(VarModel pv) {
		S5659ScreenVars screenVars = (S5659ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.volbanfr01.clear();
		screenVars.volbanto01.clear();
		screenVars.volbanle01.clear();
		screenVars.volbanfr02.clear();
		screenVars.volbanto02.clear();
		screenVars.volbanle02.clear();
		screenVars.volbanfr03.clear();
		screenVars.volbanto03.clear();
		screenVars.volbanle03.clear();
		screenVars.volbanfr04.clear();
		screenVars.volbanto04.clear();
		screenVars.volbanle04.clear();
		screenVars.rndfact.clear();
	}
}
