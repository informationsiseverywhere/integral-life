package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Mbnspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MbnspfDAO extends BaseDAO<Mbnspf> {
	public Mbnspf getmbnsRecord(String chdrcoy, String chdrnum, String life, String coverage,String rider, int yrsinf );
	public void insertMbnspfRecord(Mbnspf mbnspf);
	public boolean updateMbnspfRecord(Mbnspf mbnspf);
}
