package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Lifeclnt;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LifepfDAO extends BaseDAO<Lifepf> {
    public Map<Long, List<Lifepf>> searchLifeRecord(List<Long> covrUQ);
    public Map<String, List<Lifepf>> searchLifeRecordByActx(String coy, List<String> chdrnumList);
    public String getSplitSign();
	public List<Lifepf> getLifeList(String chdrcoy, String chdrnum);
	public void updateLifeRecord(List<Lifepf> updateLifepflist, int busDate);
	public boolean insertLifepfList(List<Lifepf> insertLifepflist);
	public String getOccRecord(String chdrcoy, String chdrnum,String life);//brd-009
	public List<Lifepf> selectLifepfRecord(Lifepf lifepf);
    //Ilfe-3310 by liwei
    public List<Lifepf>  getLifeData(String chdrcoy,String chdrnum,String life,String jlife);
    /*ilife-3395 starts */
    public List<Lifepf> selectLifeDetails(Lifepf lifepfData);
    /*ilife-3395 starts */
    // Introduced for ILIFE-3439
    public List<Lifepf> searchLifeRecordByChdrNum(String chdrCoy, String chdrnum);
    public List<Lifepf> searchLifeRecordByLifcNum(String chdrCoy, String lifcNum);
    public List<Lifepf> searchLifeRecordByLife(String chdrCoy, String chdrnum, String life, String jLife);
    public Map<String ,Lifepf> getLifeEnqData(Lifepf lifepfModel);
    public Lifepf getLifeRecord(String chdrcoy, String chdrnum, String life, String jlife) ;
    public List<Lifepf> getLifeRecords(String chdrcoy, String chdrnum, String validflag) ;
    
    public List<Lifeclnt> searchLifeClntRecord(String chdrcoy,String chdrnum,String jlife, String cltCoy, String life);
    
    public Map<String, List<Lifepf>> searchLifelnbRecord(String chdrcoy,List<String> chdrnumList); //ILIFE-8901
    public List<Lifepf> getLifelnb(String chdrcoy, String chdrnum, String life, String[] jlife);
    public List<Lifeclnt> searchLifeClntRecord(String chdrcoy,String chdrnum,String jlife,String life,String cowncoy,String cownpfx);
    
    public void updateLifeRecord(List<Lifepf> updateLifepflist);
	public List<Lifepf> getLfcllnbRecords(String chdrcoy, String chdrnum);
	public Lifepf getLifeEnqRecord(String chdrcoy, String chdrnum, String life, String jlife);
    public List<Lifepf> getLfRecords(String chdrcoy, String chdrnum, String jlife);
    public List<Lifepf> getLifematRecords(String chdrcoy, String chdrnum,String life);
	public void updateOccup(Lifepf pf);    
    public Lifepf getLifeRecordByLifcNum(String chdrcoy, String chdrnum, String lifecNum);	//ILIFE-6911
	public Lifepf getLifelnbRecord(String chdrcoy, String chdrnum, String life, String jlife);
	public List<Lifepf> getOccRecordJLife(String chdrcoy, String chdrnum,String life,String Jlife);
	public List<Lifepf> getLifeDetails(Lifepf lifepfData);//ILIFE-8108
	public void updateLifeandJointRecord(String chdrcoy,String chdrnum,String relationwithlife,String relationwithlifejoint);
	public Lifepf getLifepf(Lifepf lifepf);// ILB-555

	public List<Lifepf> searchLifeRecordByLifcNum(String chdrCoy, String lifcNum,String validflag, String jlife);
	int deleteLifeRecord(String chdrcoy, String chdrnum, String life, String jlife);
	public Lifepf getLifeRecordByKey(Lifepf life);

}