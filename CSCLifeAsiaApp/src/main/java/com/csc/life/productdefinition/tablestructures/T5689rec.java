package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:00
 * Description:
 * Copybook name: T5689REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5689rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5689Rec = new FixedLengthStringData(560);
  	public FixedLengthStringData billfreqs = new FixedLengthStringData(60).isAPartOf(t5689Rec, 0);
  	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(30, 2, billfreqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData billfreq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData billfreq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData billfreq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData billfreq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData billfreq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData billfreq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData billfreq07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData billfreq08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData billfreq09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData billfreq10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData billfreq11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData billfreq12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData billfreq13 = new FixedLengthStringData(2).isAPartOf(filler, 24);
  	public FixedLengthStringData billfreq14 = new FixedLengthStringData(2).isAPartOf(filler, 26);
  	public FixedLengthStringData billfreq15 = new FixedLengthStringData(2).isAPartOf(filler, 28);
  	public FixedLengthStringData billfreq16 = new FixedLengthStringData(2).isAPartOf(filler, 30);
  	public FixedLengthStringData billfreq17 = new FixedLengthStringData(2).isAPartOf(filler, 32);
  	public FixedLengthStringData billfreq18 = new FixedLengthStringData(2).isAPartOf(filler, 34);
  	public FixedLengthStringData billfreq19 = new FixedLengthStringData(2).isAPartOf(filler, 36);
  	public FixedLengthStringData billfreq20 = new FixedLengthStringData(2).isAPartOf(filler, 38);
  	public FixedLengthStringData billfreq21 = new FixedLengthStringData(2).isAPartOf(filler, 40);
  	public FixedLengthStringData billfreq22 = new FixedLengthStringData(2).isAPartOf(filler, 42);
  	public FixedLengthStringData billfreq23 = new FixedLengthStringData(2).isAPartOf(filler, 44);
  	public FixedLengthStringData billfreq24 = new FixedLengthStringData(2).isAPartOf(filler, 46);
  	public FixedLengthStringData billfreq25 = new FixedLengthStringData(2).isAPartOf(filler, 48);
  	public FixedLengthStringData billfreq26 = new FixedLengthStringData(2).isAPartOf(filler, 50);
  	public FixedLengthStringData billfreq27 = new FixedLengthStringData(2).isAPartOf(filler, 52);
  	public FixedLengthStringData billfreq28 = new FixedLengthStringData(2).isAPartOf(filler, 54);
  	public FixedLengthStringData billfreq29 = new FixedLengthStringData(2).isAPartOf(filler, 56);
  	public FixedLengthStringData billfreq30 = new FixedLengthStringData(2).isAPartOf(filler, 58);
  	public FixedLengthStringData mops = new FixedLengthStringData(30).isAPartOf(t5689Rec, 60);
  	public FixedLengthStringData[] mop = FLSArrayPartOfStructure(30, 1, mops, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(mops, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mop01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData mop02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData mop03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData mop04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData mop05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData mop06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData mop07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
  	public FixedLengthStringData mop08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
  	public FixedLengthStringData mop09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
  	public FixedLengthStringData mop10 = new FixedLengthStringData(1).isAPartOf(filler1, 9);
  	public FixedLengthStringData mop11 = new FixedLengthStringData(1).isAPartOf(filler1, 10);
  	public FixedLengthStringData mop12 = new FixedLengthStringData(1).isAPartOf(filler1, 11);
  	public FixedLengthStringData mop13 = new FixedLengthStringData(1).isAPartOf(filler1, 12);
  	public FixedLengthStringData mop14 = new FixedLengthStringData(1).isAPartOf(filler1, 13);
  	public FixedLengthStringData mop15 = new FixedLengthStringData(1).isAPartOf(filler1, 14);
  	public FixedLengthStringData mop16 = new FixedLengthStringData(1).isAPartOf(filler1, 15);
  	public FixedLengthStringData mop17 = new FixedLengthStringData(1).isAPartOf(filler1, 16);
  	public FixedLengthStringData mop18 = new FixedLengthStringData(1).isAPartOf(filler1, 17);
  	public FixedLengthStringData mop19 = new FixedLengthStringData(1).isAPartOf(filler1, 18);
  	public FixedLengthStringData mop20 = new FixedLengthStringData(1).isAPartOf(filler1, 19);
  	public FixedLengthStringData mop21 = new FixedLengthStringData(1).isAPartOf(filler1, 20);
  	public FixedLengthStringData mop22 = new FixedLengthStringData(1).isAPartOf(filler1, 21);
  	public FixedLengthStringData mop23 = new FixedLengthStringData(1).isAPartOf(filler1, 22);
  	public FixedLengthStringData mop24 = new FixedLengthStringData(1).isAPartOf(filler1, 23);
  	public FixedLengthStringData mop25 = new FixedLengthStringData(1).isAPartOf(filler1, 24);
  	public FixedLengthStringData mop26 = new FixedLengthStringData(1).isAPartOf(filler1, 25);
  	public FixedLengthStringData mop27 = new FixedLengthStringData(1).isAPartOf(filler1, 26);
  	public FixedLengthStringData mop28 = new FixedLengthStringData(1).isAPartOf(filler1, 27);
  	public FixedLengthStringData mop29 = new FixedLengthStringData(1).isAPartOf(filler1, 28);
  	public FixedLengthStringData mop30 = new FixedLengthStringData(1).isAPartOf(filler1, 29);
  	public FixedLengthStringData rendds = new FixedLengthStringData(60).isAPartOf(t5689Rec, 90);
  	public ZonedDecimalData[] rendd = ZDArrayPartOfStructure(30, 2, 0, rendds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(rendds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData rendd01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData rendd02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
  	public ZonedDecimalData rendd03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData rendd04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData rendd05 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData rendd06 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 10);
  	public ZonedDecimalData rendd07 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData rendd08 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 14);
  	public ZonedDecimalData rendd09 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 16);
  	public ZonedDecimalData rendd10 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 18);
  	public ZonedDecimalData rendd11 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 20);
  	public ZonedDecimalData rendd12 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 22);
  	public ZonedDecimalData rendd13 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 24);
  	public ZonedDecimalData rendd14 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 26);
  	public ZonedDecimalData rendd15 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 28);
  	public ZonedDecimalData rendd16 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 30);
  	public ZonedDecimalData rendd17 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 32);
  	public ZonedDecimalData rendd18 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 34);
  	public ZonedDecimalData rendd19 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 36);
  	public ZonedDecimalData rendd20 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 38);
  	public ZonedDecimalData rendd21 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 40);
  	public ZonedDecimalData rendd22 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 42);
  	public ZonedDecimalData rendd23 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 44);
  	public ZonedDecimalData rendd24 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 46);
  	public ZonedDecimalData rendd25 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 48);
  	public ZonedDecimalData rendd26 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 50);
  	public ZonedDecimalData rendd27 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 52);
  	public ZonedDecimalData rendd28 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 54);
  	public ZonedDecimalData rendd29 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 56);
  	public ZonedDecimalData rendd30 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 58);
  	
 // IBPLIFE-4825
 	public FixedLengthStringData maxdds = new FixedLengthStringData(60).isAPartOf(t5689Rec, 150);
 	public ZonedDecimalData[] maxdd = ZDArrayPartOfStructure(30, 2, 0, maxdds, 0);
 	public FixedLengthStringData filler9 = new FixedLengthStringData(60).isAPartOf(maxdds, 0, FILLER_REDEFINE);
 	public ZonedDecimalData maxdd01 = new ZonedDecimalData(2, 0).isAPartOf(filler9,0);
 	public ZonedDecimalData maxdd02 = new ZonedDecimalData(2, 0).isAPartOf(filler9,2);
 	public ZonedDecimalData maxdd03 = new ZonedDecimalData(2, 0).isAPartOf(filler9,4);
 	public ZonedDecimalData maxdd04 = new ZonedDecimalData(2, 0).isAPartOf(filler9,6);
 	public ZonedDecimalData maxdd05 = new ZonedDecimalData(2, 0).isAPartOf(filler9,8);
 	public ZonedDecimalData maxdd06 = new ZonedDecimalData(2, 0).isAPartOf(filler9,10);
 	public ZonedDecimalData maxdd07 = new ZonedDecimalData(2, 0).isAPartOf(filler9,12);
 	public ZonedDecimalData maxdd08 = new ZonedDecimalData(2, 0).isAPartOf(filler9,14);
 	public ZonedDecimalData maxdd09 = new ZonedDecimalData(2, 0).isAPartOf(filler9,16);
 	public ZonedDecimalData maxdd10 = new ZonedDecimalData(2, 0).isAPartOf(filler9,18);
 	public ZonedDecimalData maxdd11 = new ZonedDecimalData(2, 0).isAPartOf(filler9,20);
 	public ZonedDecimalData maxdd12 = new ZonedDecimalData(2, 0).isAPartOf(filler9,22);
 	public ZonedDecimalData maxdd13 = new ZonedDecimalData(2, 0).isAPartOf(filler9,24);
 	public ZonedDecimalData maxdd14 = new ZonedDecimalData(2, 0).isAPartOf(filler9,26);
 	public ZonedDecimalData maxdd15 = new ZonedDecimalData(2, 0).isAPartOf(filler9,28);
 	public ZonedDecimalData maxdd16 = new ZonedDecimalData(2, 0).isAPartOf(filler9,30);
 	public ZonedDecimalData maxdd17 = new ZonedDecimalData(2, 0).isAPartOf(filler9,32);
 	public ZonedDecimalData maxdd18 = new ZonedDecimalData(2, 0).isAPartOf(filler9,34);
 	public ZonedDecimalData maxdd19 = new ZonedDecimalData(2, 0).isAPartOf(filler9,36);
 	public ZonedDecimalData maxdd20 = new ZonedDecimalData(2, 0).isAPartOf(filler9,38);
 	public ZonedDecimalData maxdd21 = new ZonedDecimalData(2, 0).isAPartOf(filler9,40);
 	public ZonedDecimalData maxdd22 = new ZonedDecimalData(2, 0).isAPartOf(filler9,42);
 	public ZonedDecimalData maxdd23 = new ZonedDecimalData(2, 0).isAPartOf(filler9,44);
 	public ZonedDecimalData maxdd24 = new ZonedDecimalData(2, 0).isAPartOf(filler9,46);
 	public ZonedDecimalData maxdd25 = new ZonedDecimalData(2, 0).isAPartOf(filler9,48);
 	public ZonedDecimalData maxdd26 = new ZonedDecimalData(2, 0).isAPartOf(filler9,50);
 	public ZonedDecimalData maxdd27 = new ZonedDecimalData(2, 0).isAPartOf(filler9,52);
 	public ZonedDecimalData maxdd28 = new ZonedDecimalData(2, 0).isAPartOf(filler9,54);
 	public ZonedDecimalData maxdd29 = new ZonedDecimalData(2, 0).isAPartOf(filler9,56);
 	public ZonedDecimalData maxdd30 = new ZonedDecimalData(2, 0).isAPartOf(filler9,58);
 	// IBPLIFE-5785
 	public FixedLengthStringData allowBilldays = new FixedLengthStringData(30).isAPartOf(t5689Rec, 210);
  	public FixedLengthStringData[] allowBillday = FLSArrayPartOfStructure(30, 1, allowBilldays, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(allowBilldays, 0, FILLER_REDEFINE);
  	public FixedLengthStringData allowBillday01 = new FixedLengthStringData(1).isAPartOf(filler3, 0);
  	public FixedLengthStringData allowBillday02 = new FixedLengthStringData(1).isAPartOf(filler3, 1);
  	public FixedLengthStringData allowBillday03 = new FixedLengthStringData(1).isAPartOf(filler3, 2);
  	public FixedLengthStringData allowBillday04 = new FixedLengthStringData(1).isAPartOf(filler3, 3);
  	public FixedLengthStringData allowBillday05 = new FixedLengthStringData(1).isAPartOf(filler3, 4);
  	public FixedLengthStringData allowBillday06 = new FixedLengthStringData(1).isAPartOf(filler3, 5);
  	public FixedLengthStringData allowBillday07 = new FixedLengthStringData(1).isAPartOf(filler3, 6);
  	public FixedLengthStringData allowBillday08 = new FixedLengthStringData(1).isAPartOf(filler3, 7);
  	public FixedLengthStringData allowBillday09 = new FixedLengthStringData(1).isAPartOf(filler3, 8);
  	public FixedLengthStringData allowBillday10 = new FixedLengthStringData(1).isAPartOf(filler3, 9);
  	public FixedLengthStringData allowBillday11 = new FixedLengthStringData(1).isAPartOf(filler3, 10);
  	public FixedLengthStringData allowBillday12 = new FixedLengthStringData(1).isAPartOf(filler3, 11);
  	public FixedLengthStringData allowBillday13 = new FixedLengthStringData(1).isAPartOf(filler3, 12);
  	public FixedLengthStringData allowBillday14 = new FixedLengthStringData(1).isAPartOf(filler3, 13);
  	public FixedLengthStringData allowBillday15 = new FixedLengthStringData(1).isAPartOf(filler3, 14);
  	public FixedLengthStringData allowBillday16 = new FixedLengthStringData(1).isAPartOf(filler3, 15);
  	public FixedLengthStringData allowBillday17 = new FixedLengthStringData(1).isAPartOf(filler3, 16);
  	public FixedLengthStringData allowBillday18 = new FixedLengthStringData(1).isAPartOf(filler3, 17);
  	public FixedLengthStringData allowBillday19 = new FixedLengthStringData(1).isAPartOf(filler3, 18);
  	public FixedLengthStringData allowBillday20 = new FixedLengthStringData(1).isAPartOf(filler3, 19);
  	public FixedLengthStringData allowBillday21 = new FixedLengthStringData(1).isAPartOf(filler3, 20);
  	public FixedLengthStringData allowBillday22 = new FixedLengthStringData(1).isAPartOf(filler3, 21);
  	public FixedLengthStringData allowBillday23 = new FixedLengthStringData(1).isAPartOf(filler3, 22);
  	public FixedLengthStringData allowBillday24 = new FixedLengthStringData(1).isAPartOf(filler3, 23);
  	public FixedLengthStringData allowBillday25 = new FixedLengthStringData(1).isAPartOf(filler3, 24);
  	public FixedLengthStringData allowBillday26 = new FixedLengthStringData(1).isAPartOf(filler3, 25);
  	public FixedLengthStringData allowBillday27 = new FixedLengthStringData(1).isAPartOf(filler3, 26);
  	public FixedLengthStringData allowBillday28 = new FixedLengthStringData(1).isAPartOf(filler3, 27);
  	public FixedLengthStringData allowBillday29 = new FixedLengthStringData(1).isAPartOf(filler3, 28);
  	public FixedLengthStringData allowBillday30 = new FixedLengthStringData(1).isAPartOf(filler3, 29);
 	
 	
  	public FixedLengthStringData filler4 = new FixedLengthStringData(320).isAPartOf(t5689Rec, 240, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5689Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5689Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}