package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:11
 * Description:
 * Copybook name: TH549REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th549rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th549Rec = new FixedLengthStringData(501);
  	public ZonedDecimalData expfactor = new ZonedDecimalData(6, 5).isAPartOf(th549Rec, 0);
  	public FixedLengthStringData indcs = new FixedLengthStringData(4).isAPartOf(th549Rec, 6);
  	public FixedLengthStringData[] indc = FLSArrayPartOfStructure(2, 2, indcs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(indcs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indc01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData indc02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData opcdas = new FixedLengthStringData(10).isAPartOf(th549Rec, 10);
  	public FixedLengthStringData[] opcda = FLSArrayPartOfStructure(5, 2, opcdas, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(10).isAPartOf(opcdas, 0, FILLER_REDEFINE);
  	public FixedLengthStringData opcda01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData opcda02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData opcda03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData opcda04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData opcda05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData subrtns = new FixedLengthStringData(40).isAPartOf(th549Rec, 20);
  	public FixedLengthStringData[] subrtn = FLSArrayPartOfStructure(5, 8, subrtns, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(subrtns, 0, FILLER_REDEFINE);
  	public FixedLengthStringData subrtn01 = new FixedLengthStringData(8).isAPartOf(filler2, 0);
  	public FixedLengthStringData subrtn02 = new FixedLengthStringData(8).isAPartOf(filler2, 8);
  	public FixedLengthStringData subrtn03 = new FixedLengthStringData(8).isAPartOf(filler2, 16);
  	public FixedLengthStringData subrtn04 = new FixedLengthStringData(8).isAPartOf(filler2, 24);
  	public FixedLengthStringData subrtn05 = new FixedLengthStringData(8).isAPartOf(filler2, 32);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(441).isAPartOf(th549Rec, 60, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th549Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th549Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}