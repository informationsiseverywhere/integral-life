/*
 * File: Br53c.java
 * Date: December 3, 2013 2:10:37 AM ICT
 * Author: CSC
 * 
 * Class transformed from BR53C.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTallyAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.cls.Fcsvstd1cl;
import com.csc.fsu.general.cls.Fcsvstdcl;
import com.csc.life.productdefinition.dataaccess.PitxpfTableDAM;
import com.csc.life.productdefinition.recordstructures.Pr53bpar;
import com.csc.smart.dataaccess.DescsmtTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItdmpfxTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.WrkelevTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.tablestructures.T1716rec;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.batch.cls.Getcurlev;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    This program will clone existing product and/or components
*    as specified in the parameter screen.
*
*    Program Flow:
*    1.) Read ITDM until end of file
*    2.) Check if item table is found in TR53D(Product Cloning
*        Table Exclusion), if found skip this record.
*    3.) Check if item matches any of the Old Component or Old Pro uct
*        specified in the parameter screen, if no match skip this  ecord
*    4.) Clone the Old Product/Component into the New Product/Comp nent
*        - Create the new ITEM record
*        - Create the corresponding DESC record
*        - For Tables T6647 and T5671 we will not used the
*          inputted description but instead use the description of
*          the old product/component.
*        - Write cloned record in PITXPF
*    5.) At end of file check if to be extracted to CSV file, if ' '
*        call FCSVSTD1CL to generate the CSV file. The CSV file
*        generated will have a filename of;
*        SCHEDULE NAME||SCHEDULE NUMBER(e.g. L2PRDCLONE0004)
*
****************************************************************** ****
* </pre>
*/
public class Br53c extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private PitxpfTableDAM pitxpf = new PitxpfTableDAM();
	private FixedLengthStringData pitxpfRec = new FixedLengthStringData(25);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR53C");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(500);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(500);
	private final String wsaaLevel = "{LEVEL}/";
	private FixedLengthStringData wsaaCurrent = new FixedLengthStringData(8);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaDataArea = new FixedLengthStringData(700);
	private FixedLengthStringData wsaaDescLongdesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaOldLongdesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaDescShortdesc = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaAddrep = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaForrep = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaMember = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaLoc = new FixedLengthStringData(78);
	private FixedLengthStringData wsaaIfspath = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaTemplate = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaLib = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private String wsaaExclude = "";
	private FixedLengthStringData wsaaTable = new FixedLengthStringData(5);
	private ZonedDecimalData wsaaChangeCtr = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaDataKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaItemcoy = new FixedLengthStringData(1).isAPartOf(wsaaDataKey, 0);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(5).isAPartOf(wsaaDataKey, 1);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8).isAPartOf(wsaaDataKey, 6);
	private PackedDecimalData wsaaItmfrm = new PackedDecimalData(8, 0).isAPartOf(wsaaDataKey, 14);

	private FixedLengthStringData wsaaDefaultDescs = new FixedLengthStringData(5);
	private Validator wsaaDefaultDesc = new Validator(wsaaDefaultDescs, "T6647","T5671");

	private FixedLengthStringData wsaaPrevDataKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaPrevItemcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrevDataKey, 0);
	private FixedLengthStringData wsaaPrevItemtabl = new FixedLengthStringData(5).isAPartOf(wsaaPrevDataKey, 1);
	private FixedLengthStringData wsaaPrevItemitem = new FixedLengthStringData(8).isAPartOf(wsaaPrevDataKey, 6);
	private PackedDecimalData wsaaPrevItmfrm = new PackedDecimalData(8, 0).isAPartOf(wsaaPrevDataKey, 14);

	private FixedLengthStringData wsaaGetcurlevParm = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGetcurlevLib = new FixedLengthStringData(10).isAPartOf(wsaaGetcurlevParm, 0);
	private FixedLengthStringData wsaaGetcurlevCurrent = new FixedLengthStringData(6).isAPartOf(wsaaGetcurlevParm, 10);
	private FixedLengthStringData wsaaGetcurlevStatuz = new FixedLengthStringData(4).isAPartOf(wsaaGetcurlevParm, 16);

	private FixedLengthStringData wsaaPitxpf = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaPitxpf, 0, FILLER).init("PITX");
	private FixedLengthStringData wsaaPitxpfRunid = new FixedLengthStringData(2).isAPartOf(wsaaPitxpf, 4);
	private ZonedDecimalData wsaaPitxpfJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaPitxpf, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler3 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* TABLES */
	private static final String t1716 = "T1716";
	private static final String t1680 = "T1680";
	private static final String tr53d = "TR53D";
	private static final String descsmtrec = "DESCSMTREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmpfxrec = "ITDMPFXREC";
	private static final String wrkelevrec = "WRKELEVREC";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescsmtTableDAM descsmtIO = new DescsmtTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItdmpfxTableDAM itdmpfxIO = new ItdmpfxTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private WrkelevTableDAM wrkelevIO = new WrkelevTableDAM();
	private PitxpfTableDAM pitxpfData = new PitxpfTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T1716rec t1716rec = new T1716rec();
	private Pr53bpar pr53bpar = new Pr53bpar();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setup3011, 
		processLanguage3025, 
		nextLanguage3080, 
		exit3090
	}

	public Br53c() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*    We must clear the file member if we are restarting in*/
		/*    case the remporary file member already contains details.*/
		wsaaPitxpfRunid.set(bprdIO.getSystemParam04());
		wsaaPitxpfJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaPitxpf, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		pr53bpar.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		/* Get the Current Level*/
		wsaaGetcurlevParm.set(SPACES);
		callProgram(Getcurlev.class, wsaaGetcurlevLib, wsaaGetcurlevCurrent, wsaaGetcurlevStatuz);
		if (isNE(wsaaGetcurlevStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaGetcurlevStatuz);
			syserrrec.params.set(wsaaGetcurlevParm);
			fatalError600();
		}
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaGetcurlevCurrent, SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.setStringInto(wsaaCurrent);
		/*    the process CRTTMPF will have been run first the PITXPF file*/
		/*    and thread member will already exist. We simply need to do*/
		wsaaPitxpfRunid.set(bprdIO.getSystemParam04());
		wsaaPitxpfJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		wsaaQcmdexc.set(SPACES);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(PITXPF) TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaPitxpf);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*  Open the over-ridden PITXPF file.*/
		pitxpf.openOutput();
		/* Move fields to primary file key.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(SPACES);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), bsprIO.getCompany())) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		itdmIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(SPACES);
		wsaaDescLongdesc.set(SPACES);
		if (isNE(itdmIO.getItemtabl(), wsaaTable)) {
			wsaaTable.set(itdmIO.getItemtabl());
			checkExclusion2600();
		}
		if (isEQ(wsaaExclude, "Y")) {
			return ;
		}
		/* Component Code Match Check*/
		for (ix.set(1); !(isGT(ix, 10)
		|| isEQ(pr53bpar.crtable[ix.toInt()], SPACES)); ix.add(1)){
			for (iy.set(1); !(isGT(iy, 5)); iy.add(1)){
				if (isEQ(subString(itdmIO.getItemitem(), iy, 4), pr53bpar.crtable[ix.toInt()])) {
					wsspEdterror.set(varcom.oK);
					wsaaDescLongdesc.set(pr53bpar.crtabled[ix.toInt()]);
					return ;
				}
			}
		}
		/* Contract Type Match Check*/
		for (ix.set(1); !(isGT(ix, 6)); ix.add(1)){
			if (isEQ(subString(itdmIO.getItemitem(), ix, 3), pr53bpar.contractType)
			&& isNE(pr53bpar.contractType, SPACES)
			&& isNE(pr53bpar.cnttyp, SPACES)) {
				wsspEdterror.set(varcom.oK);
				wsaaDescLongdesc.set(pr53bpar.ctypdesc);
				return ;
			}
		}
	}

protected void checkExclusion2600()
	{
		begin2610();
	}

protected void begin2610()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tr53d);
		itemIO.setItemitem(wsaaTable);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			wsaaExclude = "Y";
		}
		else {
			wsaaExclude = "N";
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
				case setup3011: 
					setup3011();
					getLanguages3020();
				case processLanguage3025: 
					processLanguage3025();
					getDescription3030();
				case nextLanguage3080: 
					nextLanguage3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		wsaaChangeCtr.set(ZERO);
		wsaaDataArea.set(SPACES);
		wsaaDataArea.set(itdmIO.getDataArea());
		FixedLengthStringData groupTEMP = itdmIO.getDataArea();
		wsaaPrevDataKey.set(subString(groupTEMP, 1, 64));
		itdmIO.setDataArea(groupTEMP);
		/* Clone component information in GENAREA.*/
		for (ix.set(1); !(isGT(ix, 10)
		|| isEQ(pr53bpar.crcode[ix.toInt()], SPACES)); ix.add(1)){
			wsaaDataArea.set(inspectReplaceAll(wsaaDataArea, pr53bpar.crtable[ix.toInt()], pr53bpar.crcode[ix.toInt()]));
			/* Check if changes is made to the GENAREA*/
			if (isEQ(wsaaChangeCtr, ZERO)) {
				wsaaChangeCtr.add(inspectTallyAll(itdmIO.getGenarea(), pr53bpar.crtable[ix.toInt()]));
			}
		}
		/* Clone product information in GENAREA.*/
		if (isNE(pr53bpar.contractType, SPACES)
		&& isNE(pr53bpar.cnttyp, SPACES)) {
			wsaaDataArea.set(inspectReplaceAll(wsaaDataArea, pr53bpar.contractType, pr53bpar.cnttyp));
			/* Check if changes is made to the GENAREA*/
			if (isEQ(wsaaChangeCtr, ZERO)) {
				wsaaChangeCtr.add(inspectTallyAll(itdmIO.getGenarea(), pr53bpar.contractType));
			}
		}
		/* See if the record already exists.*/
		wsaaDataKey.set(subString(wsaaDataArea, 1, 64));
		itdmpfxIO.setItempfx(smtpfxcpy.item);
		itdmpfxIO.setItemcoy(wsaaItemcoy);
		itdmpfxIO.setItemtabl(wsaaItemtabl);
		itdmpfxIO.setItemitem(wsaaItemitem);
		itdmpfxIO.setItmfrm(wsaaItmfrm);
		itdmpfxIO.setFunction(varcom.begn);
		itdmpfxIO.setFormat(itdmpfxrec);
		SmartFileCode.execute(appVars, itdmpfxIO);
		if (isNE(itdmpfxIO.getStatuz(), varcom.oK)
		&& isNE(itdmpfxIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmpfxIO.getStatuz());
			syserrrec.params.set(itdmpfxIO.getParams());
			fatalError600();
		}
		/* End of file i.e. record does not exist.*/
		if (isNE(bsprIO.getCompany(), itdmpfxIO.getItemcoy())
		|| isNE(wsaaItemtabl, itdmpfxIO.getItemtabl())
		|| isNE(wsaaItemitem, itdmpfxIO.getItemitem())
		|| isNE(wsaaItmfrm, itdmpfxIO.getItmfrm())
		|| isEQ(itdmpfxIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.setup3011);
		}
		else {
			goTo(GotoLabel.exit3090);
		}
	}

protected void setup3011()
	{
		/* Write a new ITDM record.*/
		itdmpfxIO.setDataArea(wsaaDataArea);
		itdmpfxIO.setFunction(varcom.writr);
		itdmpfxIO.setFormat(itdmpfxrec);
		SmartFileCode.execute(appVars, itdmpfxIO);
		if (isNE(itdmpfxIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itdmpfxIO.getStatuz());
			syserrrec.params.set(itdmpfxIO.getParams());
			fatalError600();
		}
		if (isNE(pr53bpar.worku, SPACES)) {
			registerInWrke3100();
		}
		if (isEQ(pr53bpar.extrval, "Y")) {
			writePitxpf3200();
		}
	}

protected void getLanguages3020()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1680);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		itemIO.setFormat(itemrec);
	}

protected void processLanguage3025()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getItempfx(), smtpfxcpy.item)
		|| isNE(itemIO.getItemcoy(), "0")
		|| isNE(itemIO.getItemtabl(), t1680)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.exit3090);
		}
		wsaaLanguage.set(itemIO.getItemitem());
	}

protected void getDescription3030()
	{
		/* Check if description for new record already exist.*/
		descsmtIO.setDataArea(SPACES);
		descsmtIO.setDescpfx(smtpfxcpy.item);
		descsmtIO.setDesccoy(wsaaItemcoy);
		descsmtIO.setDesctabl(wsaaItemtabl);
		descsmtIO.setDescitem(wsaaItemitem);
		descsmtIO.setItemseq(SPACES);
		descsmtIO.setLanguage(wsaaLanguage);
		descsmtIO.setFormat(descsmtrec);
		descsmtIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descsmtIO);
		if (isNE(descsmtIO.getStatuz(), varcom.oK)
		&& isNE(descsmtIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descsmtIO.getStatuz());
			syserrrec.params.set(descsmtIO.getParams());
			fatalError600();
		}
		if (isEQ(descsmtIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.nextLanguage3080);
		}
		/* Check if old description exist.*/
		descsmtIO.setDataArea(SPACES);
		descsmtIO.setDescpfx(smtpfxcpy.item);
		descsmtIO.setDesccoy(wsaaPrevItemcoy);
		descsmtIO.setDesctabl(wsaaPrevItemtabl);
		descsmtIO.setDescitem(wsaaPrevItemitem);
		descsmtIO.setItemseq(SPACES);
		descsmtIO.setLanguage(wsaaLanguage);
		descsmtIO.setFormat(descsmtrec);
		descsmtIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descsmtIO);
		if (isNE(descsmtIO.getStatuz(), varcom.oK)
		&& isNE(descsmtIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descsmtIO.getStatuz());
			syserrrec.params.set(descsmtIO.getParams());
			fatalError600();
		}
		if (isNE(descsmtIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.nextLanguage3080);
		}
		wsaaDefaultDescs.set(wsaaPrevItemtabl);
		wsaaOldLongdesc.set(descsmtIO.getLongdesc());
		wsaaDescShortdesc.set(descsmtIO.getShortdesc());
		for (ix.set(1); !(isGT(ix, 10)
		|| isEQ(pr53bpar.crcode[ix.toInt()], SPACES)); ix.add(1)){
			wsaaDescShortdesc.set(inspectReplaceAll(wsaaDescShortdesc, pr53bpar.crtable[ix.toInt()], pr53bpar.crcode[ix.toInt()]));
			if (wsaaDefaultDesc.isTrue()) {
				wsaaOldLongdesc.set(inspectReplaceAll(wsaaOldLongdesc, pr53bpar.crtable[ix.toInt()], pr53bpar.crcode[ix.toInt()]));
			}
		}
		if (isNE(pr53bpar.contractType, SPACES)
		&& isNE(pr53bpar.cnttyp, SPACES)) {
			wsaaDescShortdesc.set(inspectReplaceAll(wsaaDescShortdesc, pr53bpar.contractType, pr53bpar.cnttyp));
			if (wsaaDefaultDesc.isTrue()) {
				wsaaOldLongdesc.set(inspectReplaceAll(wsaaOldLongdesc, pr53bpar.contractType, pr53bpar.cnttyp));
			}
		}
		/* Create the DESCSMT record for the new record.*/
		descsmtIO.setDataArea(SPACES);
		descsmtIO.setDescpfx(smtpfxcpy.item);
		descsmtIO.setDesccoy(wsaaItemcoy);
		descsmtIO.setDesctabl(wsaaItemtabl);
		descsmtIO.setDescitem(wsaaItemitem);
		descsmtIO.setItemseq(SPACES);
		descsmtIO.setLanguage(wsaaLanguage);
		descsmtIO.setShortdesc(wsaaDescShortdesc);
		if (wsaaDefaultDesc.isTrue()) {
			descsmtIO.setLongdesc(wsaaOldLongdesc);
		}
		else {
			descsmtIO.setLongdesc(wsaaDescLongdesc);
		}
		descsmtIO.setFormat(descsmtrec);
		descsmtIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, descsmtIO);
		if (isNE(descsmtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(descsmtIO.getStatuz());
			syserrrec.params.set(descsmtIO.getParams());
			fatalError600();
		}
	}

protected void nextLanguage3080()
	{
		itemIO.setFunction(varcom.nextr);
		goTo(GotoLabel.processLanguage3025);
	}

protected void registerInWrke3100()
	{
		begin3110();
	}

protected void begin3110()
	{
		wrkelevIO.setDataArea(SPACES);
		wrkelevIO.setWorku(pr53bpar.worku);
		wrkelevIO.setObjlevl(wsaaGetcurlevCurrent);
		wrkelevIO.setContainer(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaItemcoy, SPACES);
		stringVariable1.addExpression(wsaaItemtabl, SPACES);
		stringVariable1.addExpression(wsaaItemitem, SPACES);
		stringVariable1.setStringInto(wrkelevIO.getElemname());
		wrkelevIO.setObjectType("ITM");
		wrkelevIO.setFormat(wrkelevrec);
		wrkelevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, wrkelevIO);
		if (isNE(wrkelevIO.getStatuz(), varcom.oK)
		&& isNE(wrkelevIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(wrkelevIO.getStatuz());
			syserrrec.params.set(wrkelevIO.getParams());
			fatalError600();
		}
		if (isNE(wrkelevIO.getStatuz(), varcom.oK)) {
			wrkelevIO.setFunction(varcom.writr);
		}
		else {
			wrkelevIO.setFunction(varcom.rewrt);
		}
		wrkelevIO.setUserid(bsscIO.getUserName());
		wrkelevIO.setPrflag(" ");
		SmartFileCode.execute(appVars, wrkelevIO);
		if (isNE(wrkelevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(wrkelevIO.getStatuz());
			syserrrec.params.set(wrkelevIO.getParams());
			fatalError600();
		}
	}

protected void writePitxpf3200()
	{
		/*WRITE*/
		pitxpfData.itempfx.set(smtpfxcpy.item);
		pitxpfData.itemcoy.set(wsaaItemcoy);
		pitxpfData.itemtabl.set(wsaaItemtabl);
		pitxpfData.itemitem.set(wsaaPrevItemitem);
		pitxpfData.itemToCreate.set(wsaaItemitem);
		if (isNE(wsaaChangeCtr, ZERO)) {
			pitxpfData.change.set("Y");
		}
		else {
			pitxpfData.change.set("N");
		}
		pitxpf.write(pitxpfData);
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		pitxpf.close();
		if (isEQ(pr53bpar.extrval, "Y")) {
			generateCsv5000();
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void generateCsv5000()
	{
		begin5010();
	}

protected void begin5010()
	{
		/* Read Table to get the report path name*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1716);
		itemIO.setItemitem("LPDCLONE");
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1716rec.t1716Rec.set(itemIO.getGenarea());
		wsaaLoc.set(t1716rec.ifspath04);
		wsaaLoc.set(inspectReplaceAll(wsaaLoc, wsaaLevel, wsaaCurrent));
		/*Call FCSVSTDCL  to validate path name.*/
		callProgram(Fcsvstdcl.class, wsaaStatuz, wsaaLoc);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaLoc);
			fatalError600();
		}
		wsaaAddrep.set("A");
		wsaaAddrep.set("A");
		wsaaForrep.set("Y");
		wsaaIfspath.set(SPACES);
		wsaaTemplate.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaLoc, SPACES);
		stringVariable1.addExpression(bsscIO.getScheduleName(), SPACES);
		stringVariable1.addExpression(wsaaPitxpfJobno);
		stringVariable1.setStringInto(wsaaIfspath);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(wsaaLoc, SPACES);
		stringVariable2.addExpression(bsscIO.getScheduleName(), SPACES);
		stringVariable2.addExpression(".CSV");
		stringVariable2.setStringInto(wsaaTemplate);
		wsaaFile.set(wsaaPitxpf);
		wsaaLib.set(bprdIO.getRunLibrary());
		wsaaStatuz.set(SPACES);
		wsaaMember.set(SPACES);
		/*Call FCSVSTD1CL to generate CSV report.*/
		callProgram(Fcsvstd1cl.class, wsaaStatuz, wsaaLib, wsaaFile, wsaaAddrep, wsaaForrep, wsaaIfspath, wsaaTemplate, wsaaMember);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("FCSVSTD1CL");
			fatalError600();
		}
	}
}
