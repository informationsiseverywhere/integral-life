package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:29
 * Description:
 * Copybook name: COVTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtKey = new FixedLengthStringData(64).isAPartOf(covtFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtChdrcoy = new FixedLengthStringData(1).isAPartOf(covtKey, 0);
  	public FixedLengthStringData covtChdrnum = new FixedLengthStringData(8).isAPartOf(covtKey, 1);
  	public FixedLengthStringData covtLife = new FixedLengthStringData(2).isAPartOf(covtKey, 9);
  	public FixedLengthStringData covtCoverage = new FixedLengthStringData(2).isAPartOf(covtKey, 11);
  	public FixedLengthStringData covtRider = new FixedLengthStringData(2).isAPartOf(covtKey, 13);
  	public PackedDecimalData covtPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covtKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covtKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}