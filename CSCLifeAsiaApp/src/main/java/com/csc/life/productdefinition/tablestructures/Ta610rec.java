package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:53
 * Description:
 * Copybook name: TA610REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ta610rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tA610Rec = new FixedLengthStringData(536);  	

	public FixedLengthStringData occcodes = new FixedLengthStringData(20).isAPartOf(tA610Rec, 0);
  	public FixedLengthStringData[] occcode = FLSArrayPartOfStructure(10, 2, occcodes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(occcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData occcode01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData occcode02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData occcode03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData occcode04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData occcode05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData occcode06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData occcode07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData occcode08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData occcode09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData occcode10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	
  	public FixedLengthStringData occclassdess = new FixedLengthStringData(40).isAPartOf(tA610Rec, 20);	
	public FixedLengthStringData[] occclassdes = FLSArrayPartOfStructure(10, 4, occclassdess, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(occclassdess, 0, FILLER_REDEFINE);
  	
  	public FixedLengthStringData occclassdes01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData occclassdes02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData occclassdes03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData occclassdes04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData occclassdes05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData occclassdes06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData occclassdes07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData occclassdes08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData occclassdes09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData occclassdes10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
 

 	public FixedLengthStringData fupcdess = new FixedLengthStringData(20).isAPartOf(tA610Rec, 60);
  	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(5, 4, fupcdess, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
  	public FixedLengthStringData fupcdes01 = new FixedLengthStringData(4).isAPartOf(filler3, 0);
  	public FixedLengthStringData fupcdes02 = new FixedLengthStringData(4).isAPartOf(filler3, 4);
  	public FixedLengthStringData fupcdes03 = new FixedLengthStringData(4).isAPartOf(filler3, 8);
  	public FixedLengthStringData fupcdes04 = new FixedLengthStringData(4).isAPartOf(filler3, 12);
  	public FixedLengthStringData fupcdes05 = new FixedLengthStringData(4).isAPartOf(filler3, 16);  	
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(tA610Rec, 80);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(435).isAPartOf(tA610Rec, 81, FILLER);
	public void initialize() {
		COBOLFunctions.initialize(tA610Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tA610Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}