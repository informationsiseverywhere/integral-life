package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.Br64xDAO;
import com.csc.life.productdefinition.dataaccess.model.Br64xDTO;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br64xDAOImpl extends BaseDAOImpl<MedxTemppf>implements Br64xDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br64xDAOImpl.class);

	public List<MedxTemppf> loadDataByBatch(int batchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM BR64xDATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, SEQNO DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<MedxTemppf> medxList = new ArrayList<MedxTemppf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				MedxTemppf medxpf = new MedxTemppf();
				Br64xDTO br64xDTO = new Br64xDTO();
				medxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				medxpf.setChdrcoy(rs.getString("CHDRCOY").trim());
				medxpf.setChdrnum(rs.getString("CHDRNUM").trim());
				medxpf.setSeqno(rs.getInt("SEQNO"));
				medxpf.setPaidby(rs.getString("PAIDBY").trim());
				medxpf.setExmcode(rs.getString("EXMCODE").trim());
				medxpf.setZmedtyp(rs.getString("ZMEDTYP").trim());
				medxpf.setEffdate(rs.getInt("EFFDATE"));
				medxpf.setInvref(rs.getString("INVREF") == null ? "":  rs.getString("INVREF").trim()); //ILIFE-8129
				medxpf.setZmedfee(rs.getBigDecimal("ZMEDFEE"));
				medxpf.setLife(rs.getString("LIFE").trim());
				medxpf.setJlife(rs.getString("JLIFE").trim());
				medxpf.setMemberName(rs.getString("MEMBER_NAME").trim());
				br64xDTO.setChdrtranno(rs.getInt("CHDRTRANNO"));
				br64xDTO.setChdrUniqueNo(rs.getLong("CHDRUNIQUENO"));
				br64xDTO.setChdrpfx(rs.getString("CHDRPFX").trim());
				br64xDTO.setCnttype(rs.getString("CNTTYPE").trim());
				br64xDTO.setCntcurr(rs.getString("CNTCURR").trim());
				br64xDTO.setOccdate(rs.getInt("OCCDATE"));
				br64xDTO.setPtdate(rs.getInt("PTDATE"));
				br64xDTO.setMediUniqueNo(rs.getLong("MEDIUNIQUE"));
				medxpf.setBr64xDTO(br64xDTO);
				medxList.add(medxpf);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return medxList;
	}	
	
	public int populateBr64xTemp(int batchExtractSize, String medxtempTable, String chdrfrom, String chdrto, String paidBy) {
		initializeBr64xTemp();
		int rows = 0;
		boolean isParameterized = false;
		boolean isParamFromOnly = false;
		boolean isParamToOnly = false;
		if (!("".equals(chdrfrom.trim())) && !("".equals(chdrto.trim()))) {
			if (Integer.parseInt(chdrto) >= Integer.parseInt(chdrfrom)) isParameterized = true;
		}
		
		if (!("".equals(chdrfrom.trim())) && "".equals(chdrto.trim())) isParamFromOnly = true;
		if (!("".equals(chdrto.trim())) && "".equals(chdrfrom.trim())) isParamToOnly = true;
		StringBuilder sb = new StringBuilder("");
		 
		sb.append("INSERT INTO VM1DTA.BR64xDATA "); //ILB-475
		sb.append("(BATCHID, UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SEQNO, EXMCODE, ZMEDTYP, PAIDBY, LIFE, JLIFE, EFFDATE, INVREF, ZMEDFEE, CLNTNUM, DTETRM, DESC_T, MEMBER_NAME, CHDRTRANNO, CHDRUNIQUENO, CHDRPFX, CNTTYPE, OCCDATE, CNTCURR, PTDATE, MEDIUNIQUE)  ");
		sb.append("SELECT * FROM(  ");
		sb.append("SELECT floor((row_number()over(ORDER BY MAIN.CHDRCOY ASC, MAIN.CHDRNUM ASC, MAIN.SEQNO DESC)-1)/?) BATCHID, MAIN.* FROM ( ");
		sb.append("SELECT MEDX.UNIQUE_NUMBER, MEDX.CHDRCOY, MEDX.CHDRNUM, MEDX.SEQNO, MEDX.EXMCODE, MEDX.ZMEDTYP, MEDX.PAIDBY, MEDX.LIFE, MEDX.JLIFE, MEDX.EFFDATE, MEDX.INVREF, MEDX.ZMEDFEE, MEDX.CLNTNUM, MEDX.DTETRM, MEDX.DESC_T, MEDX.MEMBER_NAME, ");  
		sb.append("CHDR.TRANNO CHDRTRANNO, CHDR.UNIQUE_NUMBER CHDRUNIQUENO, CHDR.CHDRPFX, CHDR.CNTTYPE, CHDR.OCCDATE, CHDR.CNTCURR, CHDR.PTDATE, ");
		sb.append("MEDI.UNIQUE_NUMBER MEDIUNIQUE ");
		sb.append("FROM  ");
		sb.append(medxtempTable); 
		sb.append(" MEDX INNER JOIN (SELECT * FROM( ");
		sb.append("SELECT CHDR.UNIQUE_NUMBER, CHDRPF.* FROM ( ");
		sb.append("SELECT CHDRPFX, CHDRCOY, CHDRNUM, CNTCURR, CNTTYPE, OCCDATE, PTDATE, MAX(TRANNO) TRANNO FROM CHDRPF GROUP BY CHDRPFX, CHDRCOY, CHDRNUM, CNTCURR, CNTTYPE, OCCDATE, PTDATE ) ");
		sb.append("CHDRPF  INNER JOIN CHDRPF CHDR ON CHDR.CHDRPFX = CHDRPF.CHDRPFX AND CHDR.CHDRNUM = CHDRPF.CHDRNUM AND CHDR.TRANNO = CHDRPF.TRANNO) CHDR) CHDR ON CHDR.CHDRCOY = MEDX.CHDRCOY AND CHDR.CHDRNUM = MEDX.CHDRNUM ");
		sb.append("INNER JOIN ( ");
		sb.append("SELECT CHDRCOY, CHDRNUM, SEQNO, PAIDBY, UNIQUE_NUMBER FROM MEDIPF WHERE ACTIND = 1 AND PAIDBY = ? GROUP BY CHDRCOY, CHDRNUM, SEQNO, PAIDBY, UNIQUE_NUMBER ");
		sb.append(") MEDI ON MEDI.CHDRCOY = MEDX.CHDRCOY AND MEDI.CHDRNUM = MEDX.CHDRNUM AND MEDI.PAIDBY = MEDX.PAIDBY AND MEDI.SEQNO=MEDX.SEQNO  ");
		if ("D".equals(paidBy.toUpperCase())){
			sb.append("WHERE MEDX.ZMEDFEE < 0 ");
		}		
		sb.append("GROUP BY MEDX.UNIQUE_NUMBER, MEDX.CHDRCOY, MEDX.CHDRNUM, MEDX.SEQNO, MEDX.EXMCODE, MEDX.ZMEDTYP, MEDX.PAIDBY, MEDX.LIFE, MEDX.JLIFE, MEDX.EFFDATE, MEDX.INVREF, MEDX.ZMEDFEE, MEDX.CLNTNUM, MEDX.DTETRM, MEDX.DESC_T, MEDX.MEMBER_NAME, "); 
		sb.append("CHDR.UNIQUE_NUMBER, CHDR.CHDRPFX, CHDR.CNTTYPE, CHDR.OCCDATE, CHDR.PTDATE, CHDR.CNTCURR, CHDR.TRANNO, ");
		sb.append("MEDI.UNIQUE_NUMBER  ");
		sb.append(") MAIN ");
		if (isParameterized)
			sb.append("WHERE MAIN.CHDRNUM BETWEEN ? and ? ");	
		if (isParamFromOnly || isParamToOnly)
			sb.append("WHERE MAIN.CHDRNUM = ? ");	
		sb.append(") MAIN ORDER BY CHDRCOY, CHDRNUM, SEQNO DESC");

		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, paidBy);
			if (isParameterized){
				ps.setString(3, chdrfrom);
				ps.setString(4, chdrto);
			}
			if (isParamFromOnly) ps.setString(3, chdrfrom);
			if (isParamToOnly) ps.setString(3, chdrto);
			
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBr64xTemp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeBr64xTemp() {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM BR64xDATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			int rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBr64xTemp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}		


}