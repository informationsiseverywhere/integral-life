package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5585
 * @version 1.0 generated on 30/08/09 06:44
 * @author Quipoz
 */
public class S5585ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1189);
	public FixedLengthStringData dataFields = new FixedLengthStringData(197).isAPartOf(dataArea, 0);
	public FixedLengthStringData addages = new FixedLengthStringData(54).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] addage = ZDArrayPartOfStructure(18, 3, 0, addages, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(addages, 0, FILLER_REDEFINE);
	public ZonedDecimalData addage01 = DD.addage.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData addage02 = DD.addage.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData addage03 = DD.addage.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData addage04 = DD.addage.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData addage05 = DD.addage.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData addage06 = DD.addage.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData addage07 = DD.addage.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData addage08 = DD.addage.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData addage09 = DD.addage.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData addage10 = DD.addage.copyToZonedDecimal().isAPartOf(filler,27);
	public ZonedDecimalData addage11 = DD.addage.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData addage12 = DD.addage.copyToZonedDecimal().isAPartOf(filler,33);
	public ZonedDecimalData addage13 = DD.addage.copyToZonedDecimal().isAPartOf(filler,36);
	public ZonedDecimalData addage14 = DD.addage.copyToZonedDecimal().isAPartOf(filler,39);
	public ZonedDecimalData addage15 = DD.addage.copyToZonedDecimal().isAPartOf(filler,42);
	public ZonedDecimalData addage16 = DD.addage.copyToZonedDecimal().isAPartOf(filler,45);
	public ZonedDecimalData addage17 = DD.addage.copyToZonedDecimal().isAPartOf(filler,48);
	public ZonedDecimalData addage18 = DD.addage.copyToZonedDecimal().isAPartOf(filler,51);
	public FixedLengthStringData ageadjs = new FixedLengthStringData(18).isAPartOf(dataFields, 54);
	public ZonedDecimalData[] ageadj = ZDArrayPartOfStructure(9, 2, 0, ageadjs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(18).isAPartOf(ageadjs, 0, FILLER_REDEFINE);
	public ZonedDecimalData ageadj01 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData ageadj02 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,2);
	public ZonedDecimalData ageadj03 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,4);
	public ZonedDecimalData ageadj04 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData ageadj05 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,8);
	public ZonedDecimalData ageadj06 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData ageadj07 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData ageadj08 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,14);
	public ZonedDecimalData ageadj09 = DD.ageadj.copyToZonedDecimal().isAPartOf(filler1,16);
	public FixedLengthStringData agedifs = new FixedLengthStringData(36).isAPartOf(dataFields, 72);
	public ZonedDecimalData[] agedif = ZDArrayPartOfStructure(18, 2, 0, agedifs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(agedifs, 0, FILLER_REDEFINE);
	public ZonedDecimalData agedif01 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData agedif02 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,2);
	public ZonedDecimalData agedif03 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,4);
	public ZonedDecimalData agedif04 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,6);
	public ZonedDecimalData agedif05 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,8);
	public ZonedDecimalData agedif06 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,10);
	public ZonedDecimalData agedif07 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,12);
	public ZonedDecimalData agedif08 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,14);
	public ZonedDecimalData agedif09 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,16);
	public ZonedDecimalData agedif10 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,18);
	public ZonedDecimalData agedif11 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,20);
	public ZonedDecimalData agedif12 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,22);
	public ZonedDecimalData agedif13 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,24);
	public ZonedDecimalData agedif14 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,26);
	public ZonedDecimalData agedif15 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,28);
	public ZonedDecimalData agedif16 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,30);
	public ZonedDecimalData agedif17 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,32);
	public ZonedDecimalData agedif18 = DD.agedif.copyToZonedDecimal().isAPartOf(filler2,34);
	public FixedLengthStringData agelimits = new FixedLengthStringData(27).isAPartOf(dataFields, 108);
	public ZonedDecimalData[] agelimit = ZDArrayPartOfStructure(9, 3, 0, agelimits, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(27).isAPartOf(agelimits, 0, FILLER_REDEFINE);
	public ZonedDecimalData agelimit01 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData agelimit02 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,3);
	public ZonedDecimalData agelimit03 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,6);
	public ZonedDecimalData agelimit04 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,9);
	public ZonedDecimalData agelimit05 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,12);
	public ZonedDecimalData agelimit06 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,15);
	public ZonedDecimalData agelimit07 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,18);
	public ZonedDecimalData agelimit08 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,21);
	public ZonedDecimalData agelimit09 = DD.agelimit.copyToZonedDecimal().isAPartOf(filler3,24);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData hghlowage = DD.hghlowage.copy().isAPartOf(dataFields,136);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,137);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,145);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,153);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,161);
	public FixedLengthStringData sexageadj = DD.sexageadj.copy().isAPartOf(dataFields,191);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,192);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(248).isAPartOf(dataArea, 197);
	public FixedLengthStringData addagesErr = new FixedLengthStringData(72).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] addageErr = FLSArrayPartOfStructure(18, 4, addagesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(72).isAPartOf(addagesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData addage01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData addage02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData addage03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData addage04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData addage05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData addage06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData addage07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData addage08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData addage09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData addage10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData addage11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData addage12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData addage13Err = new FixedLengthStringData(4).isAPartOf(filler4, 48);
	public FixedLengthStringData addage14Err = new FixedLengthStringData(4).isAPartOf(filler4, 52);
	public FixedLengthStringData addage15Err = new FixedLengthStringData(4).isAPartOf(filler4, 56);
	public FixedLengthStringData addage16Err = new FixedLengthStringData(4).isAPartOf(filler4, 60);
	public FixedLengthStringData addage17Err = new FixedLengthStringData(4).isAPartOf(filler4, 64);
	public FixedLengthStringData addage18Err = new FixedLengthStringData(4).isAPartOf(filler4, 68);
	public FixedLengthStringData ageadjsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] ageadjErr = FLSArrayPartOfStructure(9, 4, ageadjsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(ageadjsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ageadj01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData ageadj02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData ageadj03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData ageadj04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData ageadj05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData ageadj06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData ageadj07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData ageadj08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData ageadj09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData agedifsErr = new FixedLengthStringData(72).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] agedifErr = FLSArrayPartOfStructure(18, 4, agedifsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(72).isAPartOf(agedifsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData agedif01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData agedif02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData agedif03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData agedif04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData agedif05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData agedif06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData agedif07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData agedif08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData agedif09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData agedif10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData agedif11Err = new FixedLengthStringData(4).isAPartOf(filler6, 40);
	public FixedLengthStringData agedif12Err = new FixedLengthStringData(4).isAPartOf(filler6, 44);
	public FixedLengthStringData agedif13Err = new FixedLengthStringData(4).isAPartOf(filler6, 48);
	public FixedLengthStringData agedif14Err = new FixedLengthStringData(4).isAPartOf(filler6, 52);
	public FixedLengthStringData agedif15Err = new FixedLengthStringData(4).isAPartOf(filler6, 56);
	public FixedLengthStringData agedif16Err = new FixedLengthStringData(4).isAPartOf(filler6, 60);
	public FixedLengthStringData agedif17Err = new FixedLengthStringData(4).isAPartOf(filler6, 64);
	public FixedLengthStringData agedif18Err = new FixedLengthStringData(4).isAPartOf(filler6, 68);
	public FixedLengthStringData agelimitsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData[] agelimitErr = FLSArrayPartOfStructure(9, 4, agelimitsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(36).isAPartOf(agelimitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData agelimit01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData agelimit02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData agelimit03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData agelimit04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData agelimit05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData agelimit06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData agelimit07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData agelimit08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData agelimit09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData hghlowageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData sexageadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(744).isAPartOf(dataArea, 445);
	public FixedLengthStringData addagesOut = new FixedLengthStringData(216).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] addageOut = FLSArrayPartOfStructure(18, 12, addagesOut, 0);
	public FixedLengthStringData[][] addageO = FLSDArrayPartOfArrayStructure(12, 1, addageOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(216).isAPartOf(addagesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] addage01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] addage02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] addage03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] addage04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] addage05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] addage06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] addage07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] addage08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] addage09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] addage10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] addage11Out = FLSArrayPartOfStructure(12, 1, filler8, 120);
	public FixedLengthStringData[] addage12Out = FLSArrayPartOfStructure(12, 1, filler8, 132);
	public FixedLengthStringData[] addage13Out = FLSArrayPartOfStructure(12, 1, filler8, 144);
	public FixedLengthStringData[] addage14Out = FLSArrayPartOfStructure(12, 1, filler8, 156);
	public FixedLengthStringData[] addage15Out = FLSArrayPartOfStructure(12, 1, filler8, 168);
	public FixedLengthStringData[] addage16Out = FLSArrayPartOfStructure(12, 1, filler8, 180);
	public FixedLengthStringData[] addage17Out = FLSArrayPartOfStructure(12, 1, filler8, 192);
	public FixedLengthStringData[] addage18Out = FLSArrayPartOfStructure(12, 1, filler8, 204);
	public FixedLengthStringData ageadjsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] ageadjOut = FLSArrayPartOfStructure(9, 12, ageadjsOut, 0);
	public FixedLengthStringData[][] ageadjO = FLSDArrayPartOfArrayStructure(12, 1, ageadjOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(108).isAPartOf(ageadjsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ageadj01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] ageadj02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] ageadj03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] ageadj04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] ageadj05Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData[] ageadj06Out = FLSArrayPartOfStructure(12, 1, filler9, 60);
	public FixedLengthStringData[] ageadj07Out = FLSArrayPartOfStructure(12, 1, filler9, 72);
	public FixedLengthStringData[] ageadj08Out = FLSArrayPartOfStructure(12, 1, filler9, 84);
	public FixedLengthStringData[] ageadj09Out = FLSArrayPartOfStructure(12, 1, filler9, 96);
	public FixedLengthStringData agedifsOut = new FixedLengthStringData(216).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] agedifOut = FLSArrayPartOfStructure(18, 12, agedifsOut, 0);
	public FixedLengthStringData[][] agedifO = FLSDArrayPartOfArrayStructure(12, 1, agedifOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(216).isAPartOf(agedifsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] agedif01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] agedif02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] agedif03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] agedif04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] agedif05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] agedif06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] agedif07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] agedif08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] agedif09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] agedif10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData[] agedif11Out = FLSArrayPartOfStructure(12, 1, filler10, 120);
	public FixedLengthStringData[] agedif12Out = FLSArrayPartOfStructure(12, 1, filler10, 132);
	public FixedLengthStringData[] agedif13Out = FLSArrayPartOfStructure(12, 1, filler10, 144);
	public FixedLengthStringData[] agedif14Out = FLSArrayPartOfStructure(12, 1, filler10, 156);
	public FixedLengthStringData[] agedif15Out = FLSArrayPartOfStructure(12, 1, filler10, 168);
	public FixedLengthStringData[] agedif16Out = FLSArrayPartOfStructure(12, 1, filler10, 180);
	public FixedLengthStringData[] agedif17Out = FLSArrayPartOfStructure(12, 1, filler10, 192);
	public FixedLengthStringData[] agedif18Out = FLSArrayPartOfStructure(12, 1, filler10, 204);
	public FixedLengthStringData agelimitsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 540);
	public FixedLengthStringData[] agelimitOut = FLSArrayPartOfStructure(9, 12, agelimitsOut, 0);
	public FixedLengthStringData[][] agelimitO = FLSDArrayPartOfArrayStructure(12, 1, agelimitOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(108).isAPartOf(agelimitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] agelimit01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] agelimit02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] agelimit03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] agelimit04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] agelimit05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] agelimit06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] agelimit07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] agelimit08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] agelimit09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] hghlowageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public FixedLengthStringData[] sexageadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5585screenWritten = new LongData(0);
	public LongData S5585protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5585ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, sexageadj, hghlowage, agedif01, addage01, agedif02, addage02, agelimit01, ageadj01, agedif03, addage03, agedif04, addage04, agelimit02, ageadj02, agedif05, addage05, agedif06, addage06, agelimit03, ageadj03, agedif07, addage07, agedif08, addage08, agelimit04, ageadj04, agedif09, addage09, agedif10, addage10, agelimit05, ageadj05, agedif11, addage11, agedif12, addage12, agelimit06, ageadj06, agedif13, addage13, agedif14, addage14, agelimit07, ageadj07, agedif15, addage15, agedif16, addage16, agelimit08, ageadj08, agedif17, addage17, agedif18, addage18, agelimit09, ageadj09};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, sexageadjOut, hghlowageOut, agedif01Out, addage01Out, agedif02Out, addage02Out, agelimit01Out, ageadj01Out, agedif03Out, addage03Out, agedif04Out, addage04Out, agelimit02Out, ageadj02Out, agedif05Out, addage05Out, agedif06Out, addage06Out, agelimit03Out, ageadj03Out, agedif07Out, addage07Out, agedif08Out, addage08Out, agelimit04Out, ageadj04Out, agedif09Out, addage09Out, agedif10Out, addage10Out, agelimit05Out, ageadj05Out, agedif11Out, addage11Out, agedif12Out, addage12Out, agelimit06Out, ageadj06Out, agedif13Out, addage13Out, agedif14Out, addage14Out, agelimit07Out, ageadj07Out, agedif15Out, addage15Out, agedif16Out, addage16Out, agelimit08Out, ageadj08Out, agedif17Out, addage17Out, agedif18Out, addage18Out, agelimit09Out, ageadj09Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, sexageadjErr, hghlowageErr, agedif01Err, addage01Err, agedif02Err, addage02Err, agelimit01Err, ageadj01Err, agedif03Err, addage03Err, agedif04Err, addage04Err, agelimit02Err, ageadj02Err, agedif05Err, addage05Err, agedif06Err, addage06Err, agelimit03Err, ageadj03Err, agedif07Err, addage07Err, agedif08Err, addage08Err, agelimit04Err, ageadj04Err, agedif09Err, addage09Err, agedif10Err, addage10Err, agelimit05Err, ageadj05Err, agedif11Err, addage11Err, agedif12Err, addage12Err, agelimit06Err, ageadj06Err, agedif13Err, addage13Err, agedif14Err, addage14Err, agelimit07Err, ageadj07Err, agedif15Err, addage15Err, agedif16Err, addage16Err, agelimit08Err, ageadj08Err, agedif17Err, addage17Err, agedif18Err, addage18Err, agelimit09Err, ageadj09Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5585screen.class;
		protectRecord = S5585protect.class;
	}

}
