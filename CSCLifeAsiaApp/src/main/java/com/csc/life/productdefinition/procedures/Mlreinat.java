/*
 * File: Mlreinat.java
 * Date: 29 August 2009 22:59:45
 * Author: Quipoz Limited
 *
 * Class transformed from MLREINAT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrvsTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv1TableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv2TableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmrevTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*  Reinstatement Processing
* -------------------------
*
*  The following transactions are reversed :
*
*  - CHDR
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract.
*       DELET this record.
*       NEXTR on  the  Contract  Header record (CHDRLIF) for the
*            same contract. This should read a record with
*            valid flag of '2'. If not, then this is an error.
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1'.
*
*  - COVRs
*
*       Only reverse COVR records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  COVR  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  COVR  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       REWRT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' . This will reinstate the
*            coverage prior to the LAPSE/PUP.
*
*  - UTRNs
*
*       Call the T5671 generic subroutine to reverse Unit
*       transactions for the TRANNO being reversed.
*       The subroutine GREVUTRN should be entered in the
*       relevant table entry.
*
*  - ACMVs
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*
*  - AGCMs
*
*       Read the first AGCM record (AGCMRVS).
*       Delete this record (via AGCMDBC).
*       Read the next AGCM looking for valid flag 2 records.
*       Update these AGCM records with valid flag 1.
*
*  - PAYR
*
*       READH on the Payor record (PAYRLIF) for the relevant
*            contract.
*       DELET this record.
*       NEXTR on the Payor record (PAYRLIF) for the same
*            contract. This should read a record with valid
*            flag of '2'. If not, then this is an error.
*       REWRT this Payor record (PAYRLIF) after altering the
*            valid flag from '2' to '1'.
*
* AGENT/GOVERNMENT STATISTICS RECORDS REVERSAL.
*
* All of these records are reversed when REVGENAT, the AT module,
*  calls the statistical subroutine LIFSTTR.
*
* NO processing is therefore required in this subroutine.
*
*
*****************************************************************
*
* </pre>
*/
public class Mlreinat extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("MLREINAT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

		/* ATRT-BATCH-KEY */
	private FixedLengthStringData wsaaAtmdBatchKey = new FixedLengthStringData(18);
	private FixedLengthStringData atrtPrefix = new FixedLengthStringData(2).isAPartOf(wsaaAtmdBatchKey, 0);
	private FixedLengthStringData atrtCompany = new FixedLengthStringData(1).isAPartOf(wsaaAtmdBatchKey, 2);
	private FixedLengthStringData atrtBranch = new FixedLengthStringData(2).isAPartOf(wsaaAtmdBatchKey, 3);
	private FixedLengthStringData atrtAcctPerd = new FixedLengthStringData(4).isAPartOf(wsaaAtmdBatchKey, 5);
	private PackedDecimalData atrtAcctYear = new PackedDecimalData(2, 0).isAPartOf(atrtAcctPerd, 0);
	private PackedDecimalData atrtAcctMonth = new PackedDecimalData(2, 0).isAPartOf(atrtAcctPerd, 2);
	private FixedLengthStringData atrtTransCode = new FixedLengthStringData(4).isAPartOf(wsaaAtmdBatchKey, 9);
	private FixedLengthStringData atrtBatch = new FixedLengthStringData(5).isAPartOf(wsaaAtmdBatchKey, 13);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(53);
	private PackedDecimalData wsaaTotamnt = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 9);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 14);
	private PackedDecimalData wsaaTranDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 15);
	private PackedDecimalData wsaaTranTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 19);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 23);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 27);
	private ZonedDecimalData wsaaPtrneff = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 31).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 39).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 44);
	private ZonedDecimalData wsaaPtrnBatcactyr = new ZonedDecimalData(2, 0).isAPartOf(wsaaTransArea, 48).setUnsigned();
	private ZonedDecimalData wsaaPtrnBatcactmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaTransArea, 50).setUnsigned();
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 52);
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSusBal = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaAdvBal = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaDifBal = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaPostAmt = new ZonedDecimalData(13, 2).init(ZERO);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(SPACES);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");

	private FixedLengthStringData wsaaFprmUpdate = new FixedLengthStringData(1).init(SPACES);
	private Validator fprmUpdated = new Validator(wsaaFprmUpdate, "Y");

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 2).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private String t5671 = "T5671";
	private String t1688 = "T1688";
	private String t5729 = "T5729";
	private String t5645 = "T5645";
	private String t5679 = "T5679";
	private String t3695 = "T3695";
		/* FORMATS */
	private String chdrlifrec = "CHDRLIFREC";
	private String ptrnrec = "PTRNREC";
	private String covrrec = "COVRREC";
	private String agcmrvsrec = "AGCMRVSREC";
	private String payrlifrec = "PAYRLIFREC";
	private String itemrec = "ITEMREC";
	private String incrselrec = "INCRSELREC";
	private String fpcorv1rec = "FPCORV1REC";
	private String fpcorv2rec = "FPCORV2REC";
	private String fprmrevrec = "FPRMREVREC";
	private String loanenqrec = "LOANENQREC";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*ACCOUNT MOVEMENTS LOGICAL VIEW FOR REVER*/
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
		/*Agent Commission Dets- Billing Change*/
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
		/*Logical File for Reversals*/
	private AgcmrvsTableDAM agcmrvsIO = new AgcmrvsTableDAM();
	private Atmodrec atmodrec = new Atmodrec();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*FPCO for reversals  - Version 9604*/
	private Fpcorv1TableDAM fpcorv1IO = new Fpcorv1TableDAM();
		/*FPCO logical for Reversals*/
	private Fpcorv2TableDAM fpcorv2IO = new Fpcorv2TableDAM();
		/*Reversal of FLex Premiums*/
	private FprmrevTableDAM fprmrevIO = new FprmrevTableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Automatic Increase Refusal Select File*/
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec2 = new Lifrtrnrec();
		/*LOAN Enquiries Logical file*/
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
		/*Payr logical file*/
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		z190Exit,
		processComponent3050,
		exit3090,
		exit3390,
		exit3690,
		readNextAcmv4180,
		exit4190,
		findRecordToReverse5120,
		exit5190,
		exit7090,
		loopLoanenq8020,
		nextLoanenq8080,
		exit8090,
		xxx1ErrorProg
	}

	public Mlreinat() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		rlseSoftlock2800();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		wsaaToday.set(datcon1rec.intDate);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(atmodrec.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		wsaaRevTrandesc.set(descIO.getLongdesc());
	}

protected void process2000()
	{
		process2001();
	}

protected void process2001()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("MLREINAT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(atmodrec.company);
		descIO.setDesctabl(t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("MLREINAT");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLanguage(atmodrec.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				syserrrec.statuz.set(descIO.getStatuz());
				xxxxFatalError();
			}
		}
		processChdrs2100();
		addNewPtrn2500();
		processCovrs3000();
		processAcmvs4000();
		processAgcms5000();
		processPayrs6000();
		processLoan8000();
		if (flexiblePremium.isTrue()) {
			updateFprm7000();
			if (fprmUpdated.isTrue()) {
				processFpco7500();
			}
		}
		z100Accounting();
	}

protected void processChdrs2100()
	{
		start2101();
	}

protected void start2101()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(),1));
		if (isNE(chdrlifIO.getValidflag(),1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setFunction(varcom.delet);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrcoy(),atmodrec.company)
		|| isNE(chdrlifIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(chdrlifIO.getValidflag(),2)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setValidflag("1");
		chdrlifIO.setTranno(wsaaNewTranno);
		chdrlifIO.setFunction(varcom.writd);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5729);
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemitem(chdrlifIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			wsaaFlexiblePremium.set("Y");
		}
		if (isEQ(wsaaFlag,"R")) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(atmodrec.company);
			itemIO.setItemtabl(t5679);
			itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				xxxxFatalError();
			}
			t5679rec.t5679Rec.set(itemIO.getGenarea());
			chdrlifIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				syserrrec.statuz.set(chdrlifIO.getStatuz());
				xxxxFatalError();
			}
			chdrlifIO.setCurrto(wsaaEffdate);
			chdrlifIO.setValidflag("2");
			chdrlifIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(chdrlifIO.getStatuz());
				syserrrec.params.set(chdrlifIO.getParams());
				xxxxFatalError();
			}
			chdrlifIO.setStatcode(t5679rec.setCnRiskStat);
			chdrlifIO.setCurrfrom(wsaaToday);
			chdrlifIO.setCurrto(varcom.vrcmMaxDate);
			chdrlifIO.setValidflag("1");
			chdrlifIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(chdrlifIO.getStatuz());
				syserrrec.params.set(chdrlifIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void addNewPtrn2500()
	{
		start2510();
	}

protected void start2510()
	{
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTranDate);
		ptrnIO.setTransactionTime(wsaaTranTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setValidflag("1");
		ptrnIO.setDatesub(wsaaToday);
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void z100Accounting()
	{
		try {
			z110IsItZeroes();
			z120SuspenseBalance();
			z130AdvanceBalance();
			z140PostSusOrAdv();
			z150PostInterest();
		}
		catch (GOTOException e){
		}
	}

protected void z110IsItZeroes()
	{
		if (isEQ(wsaaTotamnt,0)) {
			goTo(GotoLabel.z190Exit);
		}
	}

protected void z120SuspenseBalance()
	{
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		acblIO.setRldgcoy(chdrlifIO.getChdrcoy());
		acblIO.setRldgacct(chdrlifIO.getChdrnum());
		acblIO.setOrigcurr(chdrlifIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(acblIO.getStatuz());
			syserrrec.params.set(acblIO.getParams());
			xxxxFatalError();
		}
		if (isNE(acblIO.getStatuz(),varcom.oK)) {
			wsaaSusBal.set(0);
		}
		else {
			if (isEQ(t3695rec.sign,"-")) {
				compute(wsaaSusBal, 2).set(mult(acblIO.getSacscurbal(),-1));
			}
			else {
				wsaaSusBal.set(acblIO.getSacscurbal());
			}
		}
	}

protected void z130AdvanceBalance()
	{
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype03);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		acblIO.setRldgcoy(chdrlifIO.getChdrcoy());
		acblIO.setRldgacct(chdrlifIO.getChdrnum());
		acblIO.setOrigcurr(chdrlifIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode03);
		acblIO.setSacstyp(t5645rec.sacstype03);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(acblIO.getStatuz());
			syserrrec.params.set(acblIO.getParams());
			xxxxFatalError();
		}
		if (isNE(acblIO.getStatuz(),varcom.oK)) {
		}
		else {
			if (isEQ(t3695rec.sign,"-")) {
			}
			else {
			}
		}
	}

protected void z140PostSusOrAdv()
	{
		if (isGTE(wsaaSusBal,wsaaTotamnt)) {
			wsaaPostAmt.set(wsaaTotamnt);
			z300PostSus();
		}
		else {
			if (isLTE(wsaaSusBal,ZERO)) {
				wsaaPostAmt.set(wsaaTotamnt);
				z400PostAdv();
			}
			else {
				wsaaPostAmt.set(wsaaSusBal);
				z300PostSus();
				compute(wsaaDifBal, 2).set(sub(wsaaTotamnt,wsaaSusBal));
				wsaaPostAmt.set(wsaaDifBal);
				z400PostAdv();
			}
		}
	}

protected void z150PostInterest()
	{
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(atmodrec.batchKey);
		lifacmvrec1.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec1.rldgacct.set(chdrlifIO.getChdrnum());
		lifacmvrec1.tranno.set(chdrlifIO.getTranno());
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec1.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec1.tranref.set(chdrlifIO.getChdrnum());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.frcdate.set(0);
		lifacmvrec1.transactionDate.set(wsaaTranDate);
		lifacmvrec1.transactionTime.set(wsaaTranTime);
		lifacmvrec1.user.set(wsaaUser);
		lifacmvrec1.termid.set(wsaaTermid);
		lifacmvrec1.origamt.set(wsaaTotamnt);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.effdate.set(wsaaEffdate);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.contot.set(t5645rec.cnttot02);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		z200LifacmvCheck();
	}

protected void z200LifacmvCheck()
	{
		/*Z210-CHECK*/
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			xxxxFatalError();
		}
		/*Z259-EXIT*/
	}

protected void z300PostSus()
	{
		z310();
	}

protected void z310()
	{
		lifrtrnrec2.function.set("PSTW");
		lifrtrnrec2.batckey.set(atmodrec.batchKey);
		lifrtrnrec2.rdocnum.set(chdrlifIO.getChdrnum());
		lifrtrnrec2.rldgacct.set(chdrlifIO.getChdrnum());
		lifrtrnrec2.tranno.set(chdrlifIO.getTranno());
		lifrtrnrec2.jrnseq.set(ZERO);
		lifrtrnrec2.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifrtrnrec2.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec1.tranref.set(chdrlifIO.getChdrnum());
		lifrtrnrec2.trandesc.set(descIO.getLongdesc());
		lifrtrnrec2.crate.set(0);
		lifrtrnrec2.acctamt.set(0);
		lifrtrnrec2.rcamt.set(0);
		lifrtrnrec2.user.set(wsaaUser);
		lifrtrnrec2.termid.set(wsaaTermid);
		lifrtrnrec2.origamt.set(wsaaPostAmt);
		lifrtrnrec2.genlcur.set(SPACES);
		lifrtrnrec2.genlcoy.set(chdrlifIO.getChdrcoy());
		lifrtrnrec2.postyear.set(SPACES);
		lifrtrnrec2.postmonth.set(SPACES);
		lifrtrnrec2.effdate.set(wsaaEffdate);
		lifrtrnrec2.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec2.substituteCode[1].set(chdrlifIO.getCnttype());
		lifrtrnrec2.sacscode.set(t5645rec.sacscode01);
		lifrtrnrec2.sacstyp.set(t5645rec.sacstype01);
		lifrtrnrec2.glcode.set(t5645rec.glmap01);
		lifrtrnrec2.glsign.set(t5645rec.sign01);
		lifrtrnrec2.contot.set(t5645rec.cnttot01);
		callProgram(Lifrtrn.class, lifrtrnrec2.lifrtrnRec);
		if (isNE(lifrtrnrec2.statuz,varcom.oK)) {
			syserrrec.params.set(lifrtrnrec2.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec2.statuz);
			xxxxFatalError();
		}
	}

protected void z400PostAdv()
	{
		z400();
	}

protected void z400()
	{
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(atmodrec.batchKey);
		lifacmvrec1.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec1.rldgacct.set(chdrlifIO.getChdrnum());
		lifacmvrec1.tranno.set(chdrlifIO.getTranno());
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec1.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec1.tranref.set(chdrlifIO.getChdrnum());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.frcdate.set(0);
		lifacmvrec1.transactionDate.set(wsaaTranDate);
		lifacmvrec1.transactionTime.set(wsaaTranTime);
		lifacmvrec1.user.set(wsaaUser);
		lifacmvrec1.termid.set(wsaaTermid);
		lifacmvrec1.origamt.set(wsaaPostAmt);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.effdate.set(wsaaEffdate);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec1.sacscode.set(t5645rec.sacscode03);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec1.glcode.set(t5645rec.glmap03);
		lifacmvrec1.glsign.set(t5645rec.sign03);
		lifacmvrec1.contot.set(t5645rec.cnttot03);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		z200LifacmvCheck();
	}

protected void rlseSoftlock2800()
	{
		rlseSoftlock2810();
	}

protected void rlseSoftlock2810()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set(chdrlifIO.getChdrpfx());
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void processCovrs3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					covers3001();
				}
				case processComponent3050: {
					processComponent3050();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covers3001()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(atmodrec.company);
		covrIO.setChdrnum(wsaaPrimaryChdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),atmodrec.company)
		|| isNE(covrIO.getChdrnum(),wsaaPrimaryChdrnum)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void processComponent3050()
	{
		wsaaCovrKey.set(covrIO.getDataKey());
		if (isEQ(covrIO.getTranno(),wsaaTranNum)) {
			deleteAndUpdatComp3200();
			processGenericSubr3300();
		}
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getDataKey(),wsaaCovrKey))) {
			getNextCovr3500();
		}

		if (isNE(covrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.processComponent3050);
		}
	}

protected void deleteAndUpdatComp3200()
	{
		deleteRecs3201();
	}

protected void deleteRecs3201()
	{
		if (isNE(covrIO.getValidflag(),1)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		checkIncr3600();
		covrIO.setFunction(varcom.delet);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getValidflag(),2)
		|| isNE(covrIO.getChdrcoy(),atmodrec.company)
		|| isNE(covrIO.getChdrnum(),wsaaPrimaryChdrnum)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processGenericSubr3300()
	{
		try {
			readGenericTable3301();
		}
		catch (GOTOException e){
		}
	}

protected void readGenericTable3301()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Trancode.set(wsaaTranCode);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3390);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(wsaaTranNum);
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.life.set(covrIO.getLife());
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.batckey.set(wsaaBatckey);
		greversrec.termid.set(wsaaTermid);
		greversrec.user.set(wsaaUser);
		greversrec.newTranno.set(wsaaNewTranno);
		greversrec.transDate.set(wsaaTranDate);
		greversrec.transTime.set(wsaaTranTime);
		greversrec.language.set(atmodrec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callSubprog3400();
		}
	}

protected void callSubprog3400()
	{
		/*GO*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
		}
		if (isNE(greversrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(greversrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void getNextCovr3500()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(),atmodrec.company)
		|| isNE(covrIO.getChdrnum(),wsaaPrimaryChdrnum)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkIncr3600()
	{
		try {
			readh3610();
		}
		catch (GOTOException e){
		}
	}

protected void readh3610()
	{
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrIO.getChdrcoy());
		incrselIO.setChdrnum(covrIO.getChdrnum());
		incrselIO.setLife(covrIO.getLife());
		incrselIO.setCoverage(covrIO.getCoverage());
		incrselIO.setRider(covrIO.getRider());
		incrselIO.setPlanSuffix(covrIO.getPlanSuffix());
		incrselIO.setTranno(wsaaTranNum);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)
		&& isNE(incrselIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrselIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3690);
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
	}

protected void processAcmvs4000()
	{
		readSubAcctTable4001();
	}

protected void readSubAcctTable4001()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(atmodrec.company);
		acmvrevIO.setRdocnum(wsaaPrimaryChdrnum);
		acmvrevIO.setTranno(wsaaTranNum);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(atmodrec.company,acmvrevIO.getRldgcoy())
		|| isNE(wsaaPrimaryChdrnum,acmvrevIO.getRdocnum())
		|| isNE(wsaaTranNum,acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
	}

protected void reverseAcmvs4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start4100();
				}
				case readNextAcmv4180: {
					readNextAcmv4180();
				}
				case exit4190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4100()
	{
		if (isNE(acmvrevIO.getBatctrcde(),wsaaTranCode)) {
			goTo(GotoLabel.readNextAcmv4180);
		}
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(wsaaBatckey);
		lifacmvrec1.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec1.tranno.set(wsaaNewTranno);
		lifacmvrec1.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec1.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec1.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec1.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec1.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec1.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec1.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec1.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec1.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec1.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
		compute(lifacmvrec1.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec1.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec1.crate.set(acmvrevIO.getCrate());
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(acmvrevIO.getTranref());
		lifacmvrec1.trandesc.set(wsaaRevTrandesc);
		lifacmvrec1.effdate.set(wsaaToday);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.termid.set(wsaaTermid);
		lifacmvrec1.user.set(wsaaUser);
		lifacmvrec1.transactionTime.set(wsaaTranTime);
		lifacmvrec1.transactionDate.set(wsaaTranDate);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		z200LifacmvCheck();
	}

protected void readNextAcmv4180()
	{
		acmvrevIO.setFunction(varcom.nextr);
		if (isEQ(wsaaIoCall,"CP")) {
			goTo(GotoLabel.exit4190);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(atmodrec.company,acmvrevIO.getRldgcoy())
		|| isNE(wsaaPrimaryChdrnum,acmvrevIO.getRdocnum())
		|| isNE(wsaaTranNum,acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
	}

protected void processAgcms5000()
	{
		agcms5001();
	}

protected void agcms5001()
	{
		agcmrvsIO.setParams(SPACES);
		agcmrvsIO.setChdrcoy(atmodrec.company);
		agcmrvsIO.setChdrnum(wsaaPrimaryChdrnum);
		agcmrvsIO.setTranno(wsaaTranNum);
		agcmrvsIO.setFormat(agcmrvsrec);
		agcmrvsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrvsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmrvsIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(agcmrvsIO.getChdrcoy(),atmodrec.company)
		|| isNE(agcmrvsIO.getTranno(),wsaaTranNum)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmrvsIO.getStatuz(),varcom.endp))) {
			reverseAgcm5100();
		}

	}

protected void reverseAgcm5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					deleteRecs5101();
				}
				case findRecordToReverse5120: {
					findRecordToReverse5120();
				}
				case exit5190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void deleteRecs5101()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmdbcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmdbcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void findRecordToReverse5120()
	{
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getChdrcoy(),agcmrvsIO.getChdrcoy())
		|| isNE(agcmdbcIO.getChdrnum(),agcmrvsIO.getChdrnum())
		|| isNE(agcmdbcIO.getAgntnum(),agcmrvsIO.getAgntnum())
		|| isNE(agcmdbcIO.getLife(),agcmrvsIO.getLife())
		|| isNE(agcmdbcIO.getCoverage(),agcmrvsIO.getCoverage())
		|| isNE(agcmdbcIO.getRider(),agcmrvsIO.getRider())
		|| isNE(agcmdbcIO.getPlanSuffix(),agcmrvsIO.getPlanSuffix())
		|| isNE(agcmdbcIO.getSeqno(),agcmrvsIO.getSeqno())) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getValidflag(),"2")) {
			agcmdbcIO.setFunction(varcom.nextr);
			goTo(GotoLabel.findRecordToReverse5120);
		}
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmrvsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(agcmrvsIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit5190);
		}
		if (isNE(agcmrvsIO.getChdrcoy(),atmodrec.company)
		|| isNE(agcmrvsIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(agcmrvsIO.getTranno(),wsaaTranNum)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
	}

protected void processPayrs6000()
	{
		start6001();
	}

protected void start6001()
	{
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(atmodrec.company);
		payrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getValidflag(),1)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setFunction(varcom.delet);
		payrlifIO.setFormat(payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(atmodrec.company);
		payrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getChdrcoy(),atmodrec.company)
		|| isNE(payrlifIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(payrlifIO.getValidflag(),2)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		payrlifIO.setValidflag("1");
		payrlifIO.setTranno(wsaaNewTranno);
		payrlifIO.setFunction(varcom.writd);
		payrlifIO.setFormat(payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void updateFprm7000()
	{
		try {
			para7010();
		}
		catch (GOTOException e){
		}
	}

protected void para7010()
	{
		fprmrevIO.setDataArea(SPACES);
		fprmrevIO.setChdrcoy(atmodrec.company);
		fprmrevIO.setChdrnum(wsaaPrimaryChdrnum);
		fprmrevIO.setPayrseqno(payrlifIO.getPayrseqno());
		fprmrevIO.setFormat(fprmrevrec);
		fprmrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(fprmrevIO.getValidflag(),"1")) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(fprmrevIO.getTranno(),wsaaTranNum)) {
			fprmrevIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, fprmrevIO);
			if (isNE(fprmrevIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(fprmrevIO.getStatuz());
				syserrrec.params.set(fprmrevIO.getParams());
				xxxxFatalError();
			}
			goTo(GotoLabel.exit7090);
		}
		wsaaFprmUpdate.set("Y");
		fprmrevIO.setFormat(fprmrevrec);
		fprmrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		fprmrevIO.setFunction(varcom.readh);
		fprmrevIO.setFormat(fprmrevrec);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(fprmrevIO.getValidflag(),"2")) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		fprmrevIO.setValidflag("1");
		fprmrevIO.setCurrto(varcom.vrcmMaxDate);
		fprmrevIO.setFunction(varcom.rewrt);
		fprmrevIO.setFormat(fprmrevrec);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
	}

protected void processFpco7500()
	{
		/*PARA*/
		fpcorv1IO.setChdrcoy(atmodrec.company);
		fpcorv1IO.setChdrnum(wsaaPrimaryChdrnum);
		fpcorv1IO.setEffdate(wsaaPtrneff);
		fpcorv1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorv1IO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		fpcorv1IO.setFormat(fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isEQ(fpcorv1IO.getStatuz(),varcom.endp)
		|| isNE(fpcorv1IO.getStatuz(),varcom.oK)
		|| isNE(fpcorv1IO.getChdrcoy(),atmodrec.company)
		|| isNE(fpcorv1IO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(fpcorv1IO.getEffdate(),wsaaPtrneff)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		while ( !(isEQ(fpcorv1IO.getStatuz(),varcom.endp))) {
			reverseFpco7600();
		}

		/*EXIT*/
	}

protected void reverseFpco7600()
	{
		para7600();
		readNext7680();
	}

protected void para7600()
	{
		fpcorv2IO.setChdrcoy(fpcorv1IO.getChdrcoy());
		fpcorv2IO.setChdrnum(fpcorv1IO.getChdrnum());
		fpcorv2IO.setLife(fpcorv1IO.getLife());
		fpcorv2IO.setCoverage(fpcorv1IO.getCoverage());
		fpcorv2IO.setRider(fpcorv1IO.getRider());
		fpcorv2IO.setPlanSuffix(fpcorv1IO.getPlanSuffix());
		fpcorv2IO.setTargfrom(fpcorv1IO.getTargfrom());
		fpcorv1IO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		fpcorv2IO.setFormat(fpcorv2rec);
		fpcorv2IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
		fpcorv2IO.setValidflag("1");
		fpcorv2IO.setCurrto(varcom.vrcmMaxDate);
		fpcorv2IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
	}

protected void readNext7680()
	{
		fpcorv1IO.setFunction(varcom.nextr);
		fpcorv1IO.setFormat(fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(),varcom.oK)
		&& isNE(fpcorv1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		if (isNE(fpcorv1IO.getChdrcoy(),atmodrec.company)
		|| isNE(fpcorv1IO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(fpcorv1IO.getEffdate(),wsaaPtrneff)) {
			fpcorv1IO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processLoan8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start8010();
				}
				case loopLoanenq8020: {
					loopLoanenq8020();
				}
				case nextLoanenq8080: {
					nextLoanenq8080();
				}
				case exit8090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start8010()
	{
		loanenqIO.setChdrcoy(atmodrec.company);
		loanenqIO.setChdrnum(wsaaPrimaryChdrnum);
		loanenqIO.setLoanNumber(ZERO);
		loanenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loanenqIO.setFormat(loanenqrec);
	}

protected void loopLoanenq8020()
	{
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(),varcom.oK)
		&& isNE(loanenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(loanenqIO.getStatuz());
			syserrrec.params.set(loanenqIO.getParams());
			xxxxFatalError();
		}
		if (isNE(loanenqIO.getStatuz(),varcom.oK)
		|| isNE(loanenqIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(loanenqIO.getChdrcoy(),atmodrec.company)) {
			goTo(GotoLabel.exit8090);
		}
		if ((setPrecision(loanenqIO.getLastTranno(), 0)
		&& isNE(loanenqIO.getLastTranno(),(sub(wsaaNewTranno,1))))) {
			goTo(GotoLabel.nextLoanenq8080);
		}
		loanenqIO.setValidflag("1");
		loanenqIO.setLastTranno(ZERO);
		loanenqIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(loanenqIO.getStatuz());
			syserrrec.params.set(loanenqIO.getParams());
			xxxxFatalError();
		}
	}

protected void nextLoanenq8080()
	{
		loanenqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loopLoanenq8020);
	}

protected void xxxxFatalError()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					xxxxFatalErrors();
				}
				case xxx1ErrorProg: {
					xxx1ErrorProg();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.xxx1ErrorProg);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxx1ErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
}
