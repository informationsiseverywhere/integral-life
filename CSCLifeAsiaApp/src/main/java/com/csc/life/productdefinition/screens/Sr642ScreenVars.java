package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR642
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr642ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(650);
	public FixedLengthStringData dataFields = new FixedLengthStringData(346).isAPartOf(dataArea, 0);
	public FixedLengthStringData brchname = DD.brchlname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData jlifedesc = DD.jlifedesc.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData jlifeno = DD.jlifeno.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData jownnum = DD.jownnum.copy().isAPartOf(dataFields,140);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,148);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData lname = DD.lname.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData ownerdesc = DD.ownerdesc.copy().isAPartOf(dataFields,236);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,276);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,286);
	public FixedLengthStringData servagnam = DD.servagnam.copy().isAPartOf(dataFields,289);
	public FixedLengthStringData servagnt = DD.servagnt.copy().isAPartOf(dataFields,336);
	public FixedLengthStringData servbr = DD.servbr.copy().isAPartOf(dataFields,344);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 346);
	public FixedLengthStringData brchlnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifenoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ownerdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData servagnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData servagntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData servbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 422);
	public FixedLengthStringData[] brchlnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifenoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ownerdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] servagnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] servagntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] servbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(278);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(100).isAPartOf(subfileArea, 0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData exmcode = DD.exmcode.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData invref = DD.invref.copy().isAPartOf(subfileFields,18);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,33);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,35);
	public FixedLengthStringData name = DD.name.copy().isAPartOf(subfileFields,37);
	public FixedLengthStringData paidby = DD.paidby.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData pgmnam = DD.pgmnam.copy().isAPartOf(subfileFields,68);
	public ZonedDecimalData seqno = DD.seqno.copyToZonedDecimal().isAPartOf(subfileFields,73);
	public ZonedDecimalData zmedfee = DD.zmedfee.copyToZonedDecimal().isAPartOf(subfileFields,75);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(subfileFields,92);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 100);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData exmcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData invrefErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData nameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData paidbyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData pgmnamErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData seqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData zmedfeeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData zmedtypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 144);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] exmcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] invrefOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] nameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] paidbyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] pgmnamOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] seqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] zmedfeeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] zmedtypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 276);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sr642screensflWritten = new LongData(0);
	public LongData Sr642screenctlWritten = new LongData(0);
	public LongData Sr642screenWritten = new LongData(0);
	public LongData Sr642protectWritten = new LongData(0);
	public GeneralTable sr642screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr642screensfl;
	}

	public Sr642ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zmedtypOut,new String[] {"01","10","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {"02","10","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeOut,new String[] {"03","10","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paidbyOut,new String[] {"04","10","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(exmcodeOut,new String[] {"05","10","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"07","10","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedfeeOut,new String[] {"08","10","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(invrefOut,new String[] {"06","10","-06",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {pgmnam, seqno, zmedtyp, life, jlife, paidby, exmcode, name, effdate, zmedfee, invref};
		screenSflOutFields = new BaseData[][] {pgmnamOut, seqnoOut, zmedtypOut, lifeOut, jlifeOut, paidbyOut, exmcodeOut, nameOut, effdateOut, zmedfeeOut, invrefOut};
		screenSflErrFields = new BaseData[] {pgmnamErr, seqnoErr, zmedtypErr, lifeErr, jlifeErr, paidbyErr, exmcodeErr, nameErr, effdateErr, zmedfeeErr, invrefErr};
		screenSflDateFields = new BaseData[] {effdate};
		screenSflDateErrFields = new BaseData[] {effdateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifedesc, jlifeno, jlifedesc, cownnum, ownerdesc, jownnum, lname, servagnt, servagnam, servbr, brchname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifedescOut, jlifenoOut, jlifedescOut, cownnumOut, ownerdescOut, jownnumOut, lnameOut, servagntOut, servagnamOut, servbrOut, brchlnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifedescErr, jlifenoErr, jlifedescErr, cownnumErr, ownerdescErr, jownnumErr, lnameErr, servagntErr, servagnamErr, servbrErr, brchlnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr642screen.class;
		screenSflRecord = Sr642screensfl.class;
		screenCtlRecord = Sr642screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr642protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr642screenctl.lrec.pageSubfile);
	}
}
