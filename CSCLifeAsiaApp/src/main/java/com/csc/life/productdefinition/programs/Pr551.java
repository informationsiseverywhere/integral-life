/*
 * File: Pr551.java
 * Date: 30 August 2009 1:40:07
 * Author: Quipoz Limited
 * 
 * Class transformed from PR551.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.life.productdefinition.dataaccess.MliaenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.screens.Sr551ScreenVars;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                 LIA RECORDS MAINTENANCE
*                 =======================
*
*  This screen is used to enter the Sub-standard Details of a
*  Life Assured, including the type of underwriting decision.
*
*****************************************************************
* </pre>
*/
public class Pr551 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR551");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaWholeValue = new FixedLengthStringData(1000);

	private FixedLengthStringData filler = new FixedLengthStringData(994).isAPartOf(wsaaWholeValue, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaCompany = new ZonedDecimalData(2, 0).isAPartOf(filler, 0).setUnsigned();
	private static final String f744 = "F744";
	private static final String h926 = "H926";
		/* TABLES */
	private static final String th600 = "TH600";
	private static final String th607 = "TH607";
	private static final String th605 = "TH605";
		/* FORMATS */
	private static final String mliarec = "MLIAREC";
	private static final String descrec = "DESCREC";
	private static final String mliaenqrec = "MLIAENQREC";
	private static final String cltsrec = "CLTSREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MliaTableDAM mliaIO = new MliaTableDAM();
	private MliaenqTableDAM mliaenqIO = new MliaenqTableDAM();
	private Th605rec th605rec = new Th605rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	private Sr551ScreenVars sv = ScreenProgram.getScreenVars( Sr551ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public Pr551() {
		super();
		screenVars = sv;
		new ScreenModel("Sr551", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		/*    Dummy field initilisation for prototype version.*/
		sv.dob.set(varcom.maxdate);
		sv.effdate.set(varcom.maxdate);
		sv.hpropdte.set(varcom.maxdate);
		sv.mlhsperd.set(ZERO);
		sv.reptname.set(wssplife.trandesc);
		initialize(mliaIO.getMlcoycde());
		initialize(mliaIO.getMloldic());
		initialize(mliaIO.getMlothic());
		initialize(mliaIO.getClntnaml());
		initialize(mliaIO.getRskflg());
		initialize(mliaIO.getMlhsperd());
		initialize(mliaIO.getMlmedcde01());
		initialize(mliaIO.getMlmedcde02());
		initialize(mliaIO.getMlmedcde03());
		initialize(mliaIO.getActn());
		initialize(mliaIO.getIndc());
		initialize(mliaIO.getMind());
		mliaIO.setDob(varcom.vrcmMaxDate);
		mliaIO.setHpropdte(varcom.vrcmMaxDate);
		mliaIO.setEffdate(varcom.vrcmMaxDate);
		mliaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		mliaenqIO.setSecurityno(mliaIO.getSecurityno());
		mliaenqIO.setMlentity(mliaIO.getMlentity());
		mliaenqIO.setFormat(mliaenqrec);
		mliaenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mliaenqIO);
		if (isNE(mliaenqIO.getStatuz(), varcom.oK)
		&& isNE(mliaenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaenqIO.getStatuz());
			syserrrec.params.set(mliaenqIO.getParams());
			fatalError600();
		}
		sv.securityno.set(mliaIO.getSecurityno());
		sv.mlentity.set(mliaIO.getMlentity());
		if (isEQ(mliaenqIO.getStatuz(), varcom.oK)) {
			sv.mlothic.set(mliaenqIO.getMlothic());
			sv.mloldic.set(mliaenqIO.getMloldic());
			sv.dob.set(mliaenqIO.getDob());
			sv.clntname.set(mliaenqIO.getClntnaml());
			sv.rskflg.set(mliaenqIO.getRskflg());
			sv.hpropdte.set(mliaenqIO.getHpropdte());
			sv.mlmedcde01.set(mliaenqIO.getMlmedcde01());
			sv.mlmedcde02.set(mliaenqIO.getMlmedcde02());
			sv.mlmedcde03.set(mliaenqIO.getMlmedcde03());
			sv.mlhsperd.set(mliaenqIO.getMlhsperd());
			sv.actn.set(mliaenqIO.getActn());
			sv.indc.set(mliaenqIO.getIndc());
			sv.mind.set(mliaenqIO.getMind());
			sv.effdate.set(mliaenqIO.getEffdate());
			sv.mlcoycde.set(mliaenqIO.getMlcoycde());
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(th600);
			descIO.setDescitem(mliaenqIO.getMlcoycde());
			descIO.setItemseq(SPACES);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.desi.set(descIO.getLongdesc());
			}
			else {
				sv.desi.set(SPACES);
			}
			if (isNE(sv.mlmedcde01, SPACES)) {
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl(th607);
				descIO.setDescitem(sv.mlmedcde01);
				descIO.setItemseq(SPACES);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				descIO.setFormat(descrec);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(), varcom.oK)
				&& isNE(descIO.getStatuz(), varcom.mrnf)) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.descrip01.set(descIO.getLongdesc());
				}
				else {
					sv.descrip01.fill("?");
				}
			}
			if (isNE(sv.mlmedcde02, SPACES)) {
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl(th607);
				descIO.setDescitem(sv.mlmedcde02);
				descIO.setItemseq(SPACES);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				descIO.setFormat(descrec);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(), varcom.oK)
				&& isNE(descIO.getStatuz(), varcom.mrnf)) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.descrip02.set(descIO.getLongdesc());
				}
				else {
					sv.descrip02.fill("?");
				}
			}
			if (isNE(sv.mlmedcde03, SPACES)) {
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl(th607);
				descIO.setDescitem(sv.mlmedcde03);
				descIO.setItemseq(SPACES);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				descIO.setFormat(descrec);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(), varcom.oK)
				&& isNE(descIO.getStatuz(), varcom.mrnf)) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.descrip03.set(descIO.getLongdesc());
				}
				else {
					sv.descrip03.fill("?");
				}
			}
		}
		else {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
			sv.effdate.set(wsaaToday);
			sv.indc.set("N");
			sv.mind.set("N");
			/*    IF WSSP-FLAG                 = 'A'                        */
			if (isEQ(wsspcomn.flag, "C")) {
				sv.actn.set("1");
			}
			getLiacoy1100();
			sv.mlcoycde.set(wsaaCompany);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(th600);
			descIO.setDescitem(wsaaCompany);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.desi.set(descIO.getLongdesc());
			}
			else {
				sv.desi.set(SPACES);
			}
			hpadIO.setChdrcoy(wsspcomn.company);
			hpadIO.setChdrnum(sv.mlentity);
			hpadIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, hpadIO);
			if (isNE(hpadIO.getStatuz(), varcom.oK)
			&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(hpadIO.getStatuz());
				syserrrec.params.set(hpadIO.getParams());
				fatalError600();
			}
			if (isEQ(hpadIO.getStatuz(), varcom.oK)) {
				sv.hpropdte.set(hpadIO.getHpropdte());
			}
			chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.mlentity);
			chdrenqIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), varcom.oK)
			&& isNE(chdrenqIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			if (isEQ(chdrenqIO.getStatuz(), varcom.oK)) {
				namadrsrec.namadrsRec.set(SPACES);
				namadrsrec.clntPrefix.set("CN");
				namadrsrec.clntCompany.set(wsspcomn.fsuco);
				namadrsrec.clntNumber.set(chdrenqIO.getCownnum());
				/*       MOVE 'E'                    TO NMAD-LANGUAGE           */
				namadrsrec.language.set(wsspcomn.language);
				namadrsrec.function.set("LGNMS");
				callProgram(Namadrs.class, namadrsrec.namadrsRec);
				if (isNE(namadrsrec.statuz, varcom.oK)) {
					syserrrec.statuz.set(namadrsrec.statuz);
					fatalError600();
				}
				sv.clntname.set(namadrsrec.name);
				cltsIO.setClntpfx("CN");
				//ILIFE-1066 start kpalani6
				cltsIO.setClntcoy(wsspcomn.fsuco);
				//ILIFE-1066 end kpalani6
				cltsIO.setClntnum(chdrenqIO.getCownnum());
				cltsIO.setFormat(cltsrec);
				cltsIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, cltsIO);
				if (isNE(cltsIO.getStatuz(), varcom.oK)
				&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
					syserrrec.statuz.set(cltsIO.getStatuz());
					syserrrec.params.set(cltsIO.getParams());
					fatalError600();
				}
				if (isEQ(cltsIO.getStatuz(), varcom.oK)) {
					sv.dob.set(cltsIO.getCltdob());
				}
			}
		}
	}

protected void getLiacoy1100()
	{
		read1110();
	}

protected void read1110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(wsspcomn.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		wsaaCompany.set(th605rec.liacoy);
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(wsspcomn.flag, "D")) {
			scrnparams.function.set("PROT");
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SR551IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SR551-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set(SPACES);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.checkForErrors2080);
		}
		/*    Validate fields*/
		if (isEQ(sv.rskflg, SPACES)) {
			sv.rskflgErr.set(h926);
			wsspcomn.edterror.set(SPACES);
		}
		if (isNE(sv.rskflg, "1")
		&& isNE(sv.rskflg, "2")
		&& isNE(sv.rskflg, "3")
		&& isNE(sv.rskflg, "4")) {
			sv.rskflgErr.set(f744);
			wsspcomn.edterror.set(SPACES);
		}
		if (isNE(sv.mlmedcde01, SPACES)) {
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(th607);
			descIO.setDescitem(sv.mlmedcde01);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.descrip01.set(descIO.getLongdesc());
			}
			else {
				sv.descrip01.fill("?");
			}
		}
		if (isNE(sv.mlmedcde02, SPACES)) {
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(th607);
			descIO.setDescitem(sv.mlmedcde02);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.descrip02.set(descIO.getLongdesc());
			}
			else {
				sv.descrip02.fill("?");
			}
		}
		if (isNE(sv.mlmedcde03, SPACES)) {
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(th607);
			descIO.setDescitem(sv.mlmedcde03);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.descrip03.set(descIO.getLongdesc());
			}
			else {
				sv.descrip03.fill("?");
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		wssplife.usmeth.set(scrnparams.statuz);
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		mliaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		mliaIO.setSecurityno(sv.securityno);
		mliaIO.setMlentity(sv.mlentity);
		mliaIO.setFormat(mliarec);
		mliaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.flag, "D")) {
			mliaIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, mliaIO);
			if (isNE(mliaIO.getStatuz(), varcom.oK)
			&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(mliaIO.getStatuz());
				syserrrec.params.set(mliaIO.getParams());
				fatalError600();
			}
			return ;
		}
		mliaIO.setMlcoycde(sv.mlcoycde);
		mliaIO.setMloldic(sv.mloldic);
		mliaIO.setMlothic(sv.mlothic);
		mliaIO.setClntnaml(sv.clntname);
		mliaIO.setDob(sv.dob);
		mliaIO.setRskflg(sv.rskflg);
		mliaIO.setHpropdte(sv.hpropdte);
		mliaIO.setMlhsperd(sv.mlhsperd);
		mliaIO.setMlmedcde01(sv.mlmedcde01);
		mliaIO.setMlmedcde02(sv.mlmedcde02);
		mliaIO.setMlmedcde03(sv.mlmedcde03);
		mliaIO.setActn(sv.actn);
		mliaIO.setIndc(sv.indc);
		mliaIO.setMind(sv.mind);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		mliaIO.setEffdate(wsaaToday);
		if (isEQ(wsspcomn.flag, "M")) {
			mliaIO.setMind("N");
			mliaIO.setIndc("Y");
			mliaIO.setActn("2");
		}
		if (isEQ(mliaIO.getStatuz(), varcom.oK)) {
			mliaIO.setFunction(varcom.rewrt);
		}
		else {
			mliaIO.setFunction(varcom.updat);
		}
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
