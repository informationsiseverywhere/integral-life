package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

import com.quipoz.framework.datatype.PackedDecimalData;

public class Coevpf {

    private String chdrcoy;
    private String chdrnum;
    private int effdate;
    private String life;
    private String coverage;
    private String rider;
    private String crtable;
    private BigDecimal sumins = BigDecimal.ZERO;
    private int riskCessTerm;
    private int premCessTerm;
    private int crrcd;
    private BigDecimal zlinstprem = BigDecimal.ZERO;
    private BigDecimal instprem = BigDecimal.ZERO;
    private BigDecimal aextprm = BigDecimal.ZERO;
    private BigDecimal aprem = BigDecimal.ZERO;
    private String opcda01;
    private String opcda02;
    private String opcda03;
    private String opcda04;
    private String opcda05;
    private BigDecimal oppc01 = BigDecimal.ZERO;
    private BigDecimal oppc02 = BigDecimal.ZERO;
    private BigDecimal oppc03 = BigDecimal.ZERO;
    private BigDecimal oppc04 = BigDecimal.ZERO;
    private BigDecimal oppc05 = BigDecimal.ZERO;
    private int agerate01;
    private int agerate02;
    private int agerate03;
    private int agerate04;
    private int agerate05;
    private int insprm01;
    private int insprm02;
    private int insprm03;
    private int insprm04;
    private int insprm05;
    private int extCessTerm01;
    private int extCessTerm02;
    private int extCessTerm03;
    private int extCessTerm04;
    private int extCessTerm05;
    private String unitVirtualFund01;
    private String unitVirtualFund02;
    private String unitVirtualFund03;
    private String unitVirtualFund04;
    private String unitVirtualFund05;
    private BigDecimal fundAmount01 = BigDecimal.ZERO;
    private BigDecimal fundAmount02 = BigDecimal.ZERO;
    private BigDecimal fundAmount03 = BigDecimal.ZERO;
    private BigDecimal fundAmount04 = BigDecimal.ZERO;
    private BigDecimal fundAmount05 = BigDecimal.ZERO;
    private String rgpymop01;
    private String rgpymop02;
    private String rgpymop03;
    private String rgpymop04;
    private String rgpymop05;
    private BigDecimal pymt01 = BigDecimal.ZERO;
    private BigDecimal pymt02 = BigDecimal.ZERO;
    private BigDecimal pymt03 = BigDecimal.ZERO;
    private BigDecimal pymt04 = BigDecimal.ZERO;
    private BigDecimal pymt05 = BigDecimal.ZERO;
    private String anname01;
    private String anname02;
    private String anname03;
    private String anname04;
    private String anname05;
    private int annage01;
    private int annage02;
    private int annage03;
    private int annage04;
    private int annage05;
    private String sanname01;
    private String sanname02;
    private String sanname03;
    private String sanname04;
    private String sanname05;
    private int sannage01;
    private int sannage02;
    private int sannage03;
    private int sannage04;
    private int sannage05;
    private int firstPaydate01;
    private int firstPaydate02;
    private int firstPaydate03;
    private int firstPaydate04;
    private int firstPaydate05;
    private int nextPaydate01;
    private int nextPaydate02;
    private int nextPaydate03;
    private int nextPaydate04;
    private int nextPaydate05;
    private int finalPaydate01;
    private int finalPaydate02;
    private int finalPaydate03;
    private int finalPaydate04;
    private int finalPaydate05;
    private int currfrom;
    private String validflag;
    private String userProfile;
    private String jobName;
    private String datime;

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public int getEffdate() {
        return effdate;
    }

    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getRider() {
        return rider;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public String getCrtable() {
        return crtable;
    }

    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }

    public BigDecimal getSumins() {
        return sumins;
    }

    public void setSumins(BigDecimal sumins) {
        this.sumins = sumins;
    }

    public int getRiskCessTerm() {
        return riskCessTerm;
    }

    public void setRiskCessTerm(int riskCessTerm) {
        this.riskCessTerm = riskCessTerm;
    }

    public int getPremCessTerm() {
        return premCessTerm;
    }

    public void setPremCessTerm(int premCessTerm) {
        this.premCessTerm = premCessTerm;
    }

    public int getCrrcd() {
        return crrcd;
    }

    public void setCrrcd(int crrcd) {
        this.crrcd = crrcd;
    }

    public BigDecimal getZlinstprem() {
        return zlinstprem;
    }

    public void setZlinstprem(BigDecimal zlinstprem) {
        this.zlinstprem = zlinstprem;
    }

    public BigDecimal getInstprem() {
        return instprem;
    }

    public void setInstprem(BigDecimal instprem) {
        this.instprem = instprem;
    }

    public BigDecimal getAextprm() {
        return aextprm;
    }

    public void setAextprm(BigDecimal aextprm) {
        this.aextprm = aextprm;
    }

    public BigDecimal getAprem() {
        return aprem;
    }

    public void setAprem(BigDecimal aprem) {
        this.aprem = aprem;
    }

    public String getOpcda01() {
        return opcda01;
    }

    public void setOpcda(int index, String opcda) {
        switch (index) {
        case 1:
            setOpcda01(opcda);
            break;
        case 2:
            setOpcda02(opcda);
            break;
        case 3:
            setOpcda03(opcda);
            break;
        case 4:
            setOpcda04(opcda);
            break;
        case 5:
            setOpcda05(opcda);
            break;
        default:
            return;
        }
    }

    public void setOppc(int index, BigDecimal oppc) {
        switch (index) {
        case 1:
            setOppc01(oppc);
            break;
        case 2:
            setOppc02(oppc);
            break;
        case 3:
            setOppc03(oppc);
            break;
        case 4:
            setOppc04(oppc);
            break;
        case 5:
            setOppc05(oppc);
            break;
        default:
            return;
        }
    }

    public void setAgerate(int index, int agerate) {
        switch (index) {
        case 1:
            setAgerate01(agerate);
            break;
        case 2:
            setAgerate02(agerate);
            break;
        case 3:
            setAgerate03(agerate);
            break;
        case 4:
            setAgerate04(agerate);
            break;
        case 5:
            setAgerate05(agerate);
            break;
        default:
            return;
        }
    }

    public void setInsprm(int index, int insprm) {
        switch (index) {
        case 1:
            setInsprm01(insprm);
            break;
        case 2:
            setInsprm02(insprm);
            break;
        case 3:
            setInsprm03(insprm);
            break;
        case 4:
            setInsprm04(insprm);
            break;
        case 5:
            setInsprm05(insprm);
            break;
        default:
            return;
        }
    }

    public void setExtCessTerm(int index, int extCessTerm) {
        switch (index) {
        case 1:
            setExtCessTerm01(extCessTerm);
            break;
        case 2:
            setExtCessTerm02(extCessTerm);
            break;
        case 3:
            setExtCessTerm03(extCessTerm);
            break;
        case 4:
            setExtCessTerm04(extCessTerm);
            break;
        case 5:
            setExtCessTerm05(extCessTerm);
            break;
        default:
            return;
        }
    }

    public void setOpcda01(String opcda01) {
        this.opcda01 = opcda01;
    }

    public String getOpcda02() {
        return opcda02;
    }

    public void setOpcda02(String opcda02) {
        this.opcda02 = opcda02;
    }

    public String getOpcda03() {
        return opcda03;
    }

    public void setOpcda03(String opcda03) {
        this.opcda03 = opcda03;
    }

    public String getOpcda04() {
        return opcda04;
    }

    public void setOpcda04(String opcda04) {
        this.opcda04 = opcda04;
    }

    public String getOpcda05() {
        return opcda05;
    }

    public void setOpcda05(String opcda05) {
        this.opcda05 = opcda05;
    }

    public BigDecimal getOppc01() {
        return oppc01;
    }

    public void setOppc01(BigDecimal oppc01) {
        this.oppc01 = oppc01;
    }

    public BigDecimal getOppc02() {
        return oppc02;
    }

    public void setOppc02(BigDecimal oppc02) {
        this.oppc02 = oppc02;
    }

    public BigDecimal getOppc03() {
        return oppc03;
    }

    public void setOppc03(BigDecimal oppc03) {
        this.oppc03 = oppc03;
    }

    public BigDecimal getOppc04() {
        return oppc04;
    }

    public void setOppc04(BigDecimal oppc04) {
        this.oppc04 = oppc04;
    }

    public BigDecimal getOppc05() {
        return oppc05;
    }

    public void setOppc05(BigDecimal oppc05) {
        this.oppc05 = oppc05;
    }

    public int getAgerate01() {
        return agerate01;
    }

    public void setAgerate01(int agerate01) {
        this.agerate01 = agerate01;
    }

    public int getAgerate02() {
        return agerate02;
    }

    public void setAgerate02(int agerate02) {
        this.agerate02 = agerate02;
    }

    public int getAgerate03() {
        return agerate03;
    }

    public void setAgerate03(int agerate03) {
        this.agerate03 = agerate03;
    }

    public int getAgerate04() {
        return agerate04;
    }

    public void setAgerate04(int agerate04) {
        this.agerate04 = agerate04;
    }

    public int getAgerate05() {
        return agerate05;
    }

    public void setAgerate05(int agerate05) {
        this.agerate05 = agerate05;
    }

    public int getInsprm01() {
        return insprm01;
    }

    public void setInsprm01(int insprm01) {
        this.insprm01 = insprm01;
    }

    public int getInsprm02() {
        return insprm02;
    }

    public void setInsprm02(int insprm02) {
        this.insprm02 = insprm02;
    }

    public int getInsprm03() {
        return insprm03;
    }

    public void setInsprm03(int insprm03) {
        this.insprm03 = insprm03;
    }

    public int getInsprm04() {
        return insprm04;
    }

    public void setInsprm04(int insprm04) {
        this.insprm04 = insprm04;
    }

    public int getInsprm05() {
        return insprm05;
    }

    public void setInsprm05(int insprm05) {
        this.insprm05 = insprm05;
    }

    public int getExtCessTerm01() {
        return extCessTerm01;
    }

    public void setExtCessTerm01(int extCessTerm01) {
        this.extCessTerm01 = extCessTerm01;
    }

    public int getExtCessTerm02() {
        return extCessTerm02;
    }

    public void setExtCessTerm02(int extCessTerm02) {
        this.extCessTerm02 = extCessTerm02;
    }

    public int getExtCessTerm03() {
        return extCessTerm03;
    }

    public void setExtCessTerm03(int extCessTerm03) {
        this.extCessTerm03 = extCessTerm03;
    }

    public int getExtCessTerm04() {
        return extCessTerm04;
    }

    public void setExtCessTerm04(int extCessTerm04) {
        this.extCessTerm04 = extCessTerm04;
    }

    public int getExtCessTerm05() {
        return extCessTerm05;
    }

    public void setExtCessTerm05(int extCessTerm05) {
        this.extCessTerm05 = extCessTerm05;
    }

    public String getUnitVirtualFund01() {
        return unitVirtualFund01;
    }

    public void setUnitVirtualFund01(String unitVirtualFund01) {
        this.unitVirtualFund01 = unitVirtualFund01;
    }

    public String getUnitVirtualFund02() {
        return unitVirtualFund02;
    }

    public void setUnitVirtualFund02(String unitVirtualFund02) {
        this.unitVirtualFund02 = unitVirtualFund02;
    }

    public String getUnitVirtualFund03() {
        return unitVirtualFund03;
    }

    public void setUnitVirtualFund03(String unitVirtualFund03) {
        this.unitVirtualFund03 = unitVirtualFund03;
    }

    public String getUnitVirtualFund04() {
        return unitVirtualFund04;
    }

    public void setUnitVirtualFund04(String unitVirtualFund04) {
        this.unitVirtualFund04 = unitVirtualFund04;
    }

    public String getUnitVirtualFund05() {
        return unitVirtualFund05;
    }

    public void setUnitVirtualFund05(String unitVirtualFund05) {
        this.unitVirtualFund05 = unitVirtualFund05;
    }

    public BigDecimal getFundAmount01() {
        return fundAmount01;
    }

    public void setFundAmount01(BigDecimal fundAmount01) {
        this.fundAmount01 = fundAmount01;
    }

    public BigDecimal getFundAmount02() {
        return fundAmount02;
    }

    public void setFundAmount02(BigDecimal fundAmount02) {
        this.fundAmount02 = fundAmount02;
    }

    public BigDecimal getFundAmount03() {
        return fundAmount03;
    }

    public void setFundAmount03(BigDecimal fundAmount03) {
        this.fundAmount03 = fundAmount03;
    }

    public BigDecimal getFundAmount04() {
        return fundAmount04;
    }

    public void setFundAmount04(BigDecimal fundAmount04) {
        this.fundAmount04 = fundAmount04;
    }

    public BigDecimal getFundAmount05() {
        return fundAmount05;
    }

    public void setFundAmount05(BigDecimal fundAmount05) {
        this.fundAmount05 = fundAmount05;
    }

    public String getRgpymop01() {
        return rgpymop01;
    }

    public void setRgpymop01(String rgpymop01) {
        this.rgpymop01 = rgpymop01;
    }

    public String getRgpymop02() {
        return rgpymop02;
    }

    public void setRgpymop02(String rgpymop02) {
        this.rgpymop02 = rgpymop02;
    }

    public String getRgpymop03() {
        return rgpymop03;
    }

    public void setRgpymop03(String rgpymop03) {
        this.rgpymop03 = rgpymop03;
    }

    public String getRgpymop04() {
        return rgpymop04;
    }

    public void setRgpymop04(String rgpymop04) {
        this.rgpymop04 = rgpymop04;
    }

    public String getRgpymop05() {
        return rgpymop05;
    }

    public void setRgpymop05(String rgpymop05) {
        this.rgpymop05 = rgpymop05;
    }

    public BigDecimal getPymt01() {
        return pymt01;
    }

    public void setPymt01(BigDecimal pymt01) {
        this.pymt01 = pymt01;
    }

    public BigDecimal getPymt02() {
        return pymt02;
    }

    public void setPymt02(BigDecimal pymt02) {
        this.pymt02 = pymt02;
    }

    public BigDecimal getPymt03() {
        return pymt03;
    }

    public void setPymt03(BigDecimal pymt03) {
        this.pymt03 = pymt03;
    }

    public BigDecimal getPymt04() {
        return pymt04;
    }

    public void setPymt04(BigDecimal pymt04) {
        this.pymt04 = pymt04;
    }

    public BigDecimal getPymt05() {
        return pymt05;
    }

    public void setPymt05(BigDecimal pymt05) {
        this.pymt05 = pymt05;
    }

    public String getAnname01() {
        return anname01;
    }

    public void setAnname01(String anname01) {
        this.anname01 = anname01;
    }

    public String getAnname02() {
        return anname02;
    }

    public void setAnname02(String anname02) {
        this.anname02 = anname02;
    }

    public String getAnname03() {
        return anname03;
    }

    public void setAnname03(String anname03) {
        this.anname03 = anname03;
    }

    public String getAnname04() {
        return anname04;
    }

    public void setAnname04(String anname04) {
        this.anname04 = anname04;
    }

    public String getAnname05() {
        return anname05;
    }

    public void setAnname05(String anname05) {
        this.anname05 = anname05;
    }

    public int getAnnage01() {
        return annage01;
    }

    public void setAnnage01(int annage01) {
        this.annage01 = annage01;
    }

    public int getAnnage02() {
        return annage02;
    }

    public void setAnnage02(int annage02) {
        this.annage02 = annage02;
    }

    public int getAnnage03() {
        return annage03;
    }

    public void setAnnage03(int annage03) {
        this.annage03 = annage03;
    }

    public int getAnnage04() {
        return annage04;
    }

    public void setAnnage04(int annage04) {
        this.annage04 = annage04;
    }

    public int getAnnage05() {
        return annage05;
    }

    public void setAnnage05(int annage05) {
        this.annage05 = annage05;
    }

    public String getSanname01() {
        return sanname01;
    }

    public void setSanname01(String sanname01) {
        this.sanname01 = sanname01;
    }

    public String getSanname02() {
        return sanname02;
    }

    public void setSanname02(String sanname02) {
        this.sanname02 = sanname02;
    }

    public String getSanname03() {
        return sanname03;
    }

    public void setSanname03(String sanname03) {
        this.sanname03 = sanname03;
    }

    public String getSanname04() {
        return sanname04;
    }

    public void setSanname04(String sanname04) {
        this.sanname04 = sanname04;
    }

    public String getSanname05() {
        return sanname05;
    }

    public void setSanname05(String sanname05) {
        this.sanname05 = sanname05;
    }

    public int getSannage01() {
        return sannage01;
    }

    public void setSannage01(int sannage01) {
        this.sannage01 = sannage01;
    }

    public int getSannage02() {
        return sannage02;
    }

    public void setSannage02(int sannage02) {
        this.sannage02 = sannage02;
    }

    public int getSannage03() {
        return sannage03;
    }

    public void setSannage03(int sannage03) {
        this.sannage03 = sannage03;
    }

    public int getSannage04() {
        return sannage04;
    }

    public void setSannage04(int sannage04) {
        this.sannage04 = sannage04;
    }

    public int getSannage05() {
        return sannage05;
    }

    public void setSannage05(int sannage05) {
        this.sannage05 = sannage05;
    }

    public int getFirstPaydate01() {
        return firstPaydate01;
    }

    public void setFirstPaydate01(int firstPaydate01) {
        this.firstPaydate01 = firstPaydate01;
    }

    public int getFirstPaydate02() {
        return firstPaydate02;
    }

    public void setFirstPaydate02(int firstPaydate02) {
        this.firstPaydate02 = firstPaydate02;
    }

    public int getFirstPaydate03() {
        return firstPaydate03;
    }

    public void setFirstPaydate03(int firstPaydate03) {
        this.firstPaydate03 = firstPaydate03;
    }

    public int getFirstPaydate04() {
        return firstPaydate04;
    }

    public void setFirstPaydate04(int firstPaydate04) {
        this.firstPaydate04 = firstPaydate04;
    }

    public int getFirstPaydate05() {
        return firstPaydate05;
    }

    public void setFirstPaydate05(int firstPaydate05) {
        this.firstPaydate05 = firstPaydate05;
    }

    public int getNextPaydate01() {
        return nextPaydate01;
    }

    public void setNextPaydate01(int nextPaydate01) {
        this.nextPaydate01 = nextPaydate01;
    }

    public int getNextPaydate02() {
        return nextPaydate02;
    }

    public void setNextPaydate02(int nextPaydate02) {
        this.nextPaydate02 = nextPaydate02;
    }

    public int getNextPaydate03() {
        return nextPaydate03;
    }

    public void setNextPaydate03(int nextPaydate03) {
        this.nextPaydate03 = nextPaydate03;
    }

    public int getNextPaydate04() {
        return nextPaydate04;
    }

    public void setNextPaydate04(int nextPaydate04) {
        this.nextPaydate04 = nextPaydate04;
    }

    public int getNextPaydate05() {
        return nextPaydate05;
    }

    public void setNextPaydate05(int nextPaydate05) {
        this.nextPaydate05 = nextPaydate05;
    }

    public int getFinalPaydate01() {
        return finalPaydate01;
    }

    public void setFinalPaydate01(int finalPaydate01) {
        this.finalPaydate01 = finalPaydate01;
    }

    public int getFinalPaydate02() {
        return finalPaydate02;
    }

    public void setFinalPaydate02(int finalPaydate02) {
        this.finalPaydate02 = finalPaydate02;
    }

    public int getFinalPaydate03() {
        return finalPaydate03;
    }

    public void setFinalPaydate03(int finalPaydate03) {
        this.finalPaydate03 = finalPaydate03;
    }

    public int getFinalPaydate04() {
        return finalPaydate04;
    }

    public void setFinalPaydate04(int finalPaydate04) {
        this.finalPaydate04 = finalPaydate04;
    }

    public int getFinalPaydate05() {
        return finalPaydate05;
    }

    public void setFinalPaydate05(int finalPaydate05) {
        this.finalPaydate05 = finalPaydate05;
    }

    public int getCurrfrom() {
        return currfrom;
    }

    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }

    public BigDecimal getFundAmount(int indx) {
        switch (indx) {
        case 1:
            return fundAmount01;
        case 2:
            return fundAmount02;
        case 3:
            return fundAmount03;
        case 4:
            return fundAmount04;
        case 5:
            return fundAmount05;
        default:
            return null;
        }
    }

    public void setFundamnt(int indx, BigDecimal what) {
        switch (indx) {
        case 1:
            setFundAmount01(what);
            break;
        case 2:
            setFundAmount02(what);
            break;
        case 3:
            setFundAmount03(what);
            break;
        case 4:
            setFundAmount04(what);
            break;
        case 5:
            setFundAmount05(what);
            break;
        default:
            return;
        }
    }

    public BigDecimal getFundamnt(int indx) {

        switch (indx) {
        case 1:
            return fundAmount01;
        case 2:
            return fundAmount02;
        case 3:
            return fundAmount03;
        case 4:
            return fundAmount04;
        case 5:
            return fundAmount05;
        default:
            return null;
        }

    }

    public void setAnname(int indx, String what) {

        switch (indx) {
        case 1:
            setAnname01(what);
            break;
        case 2:
            setAnname02(what);
            break;
        case 3:
            setAnname03(what);
            break;
        case 4:
            setAnname04(what);
            break;
        case 5:
            setAnname05(what);
            break;
        default:
            return;
        }

    }

    public void setVrtfnd(int indx, String what) {

        switch (indx) {
        case 1:
            setUnitVirtualFund01(what);
            break;
        case 2:
            setUnitVirtualFund02(what);
            break;
        case 3:
            setUnitVirtualFund03(what);
            break;
        case 4:
            setUnitVirtualFund04(what);
            break;
        case 5:
            setUnitVirtualFund05(what);
            break;
        default:
            return;
        }
    }

    public void setAnnage(int indx, int what) {

        switch (indx) {
        case 1:
            setAnnage01(what);
            break;
        case 2:
            setAnnage02(what);
            break;
        case 3:
            setAnnage03(what);
            break;
        case 4:
            setAnnage04(what);
            break;
        case 5:
            setAnnage05(what);
            break;
        default:
            return;
        }
    }

    public void setSanname(int indx, String what) {

        switch (indx) {
        case 1:
            setSanname01(what);
            break;
        case 2:
            setSanname02(what);
            break;
        case 3:
            setSanname03(what);
            break;
        case 4:
            setSanname04(what);
            break;
        case 5:
            setSanname05(what);
            break;
        default:
            return;
        }
    }

    public void setSannage(int indx, int what) {

        switch (indx) {
        case 1:
            setSannage01(what);
            break;
        case 2:
            setSannage02(what);
            break;
        case 3:
            setSannage03(what);
            break;
        case 4:
            setSannage04(what);
            break;
        case 5:
            setSannage05(what);
            break;
        default:
            return;
        }
    }

    public void setRgpymop(int indx, String what) {

        switch (indx) {
        case 1:
            setRgpymop01(what);
            break;
        case 2:
            setRgpymop02(what);
            break;
        case 3:
            setRgpymop03(what);
            break;
        case 4:
            setRgpymop04(what);
            break;
        case 5:
            setRgpymop05(what);
            break;
        default:
            return;
        }
    }

    public void setFpaydate(int indx, int what) {

        switch (indx) {
        case 1:
            setFirstPaydate01(what);
            break;
        case 2:
            setFirstPaydate02(what);
            break;
        case 3:
            setFirstPaydate03(what);
            break;
        case 4:
            setFirstPaydate04(what);
            break;
        case 5:
            setFirstPaydate05(what);
            break;
        default:
            return;
        }
    }

    public void setNpaydate(int indx, int what) {

        switch (indx) {
        case 1:
            setNextPaydate01(what);
            break;
        case 2:
            setNextPaydate02(what);
            break;
        case 3:
            setNextPaydate03(what);
            break;
        case 4:
            setNextPaydate04(what);
            break;
        case 5:
            setNextPaydate05(what);
            break;
        default:
            return;
        }
    }

    public void setEpaydate(int indx, int what) {

        switch (indx) {
        case 1:
            setFinalPaydate01(what);
            break;
        case 2:
            setFinalPaydate02(what);
            break;
        case 3:
            setFinalPaydate03(what);
            break;
        case 4:
            setFinalPaydate04(what);
            break;
        case 5:
            setFinalPaydate05(what);
            break;
        default:
            return;
        }
    }

    public void setPymt(int indx, BigDecimal what) {

        switch (indx) {
        case 1:
            setPymt01(what);
            break;
        case 2:
            setPymt02(what);
            break;
        case 3:
            setPymt03(what);
            break;
        case 4:
            setPymt04(what);
            break;
        case 5:
            setPymt05(what);
            break;
        default:
            return;
        }

    }
}