/*
 * File: Br559.java
 * Date: 29 August 2009 22:20:37
 * Author: Quipoz Limited
 * 
 * Class transformed from BR559.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.productdefinition.dataaccess.dao.Br559DAO;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.life.productdefinition.reports.Rr559Report;
import com.csc.life.productdefinition.tablestructures.T5620rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*  Medical Bill Payment Statement
*  ------------------------------
*
*    This program reads Medical Bill Payment (MEDIPF) records
*   and prints out the Medical Bill Payment Statement.
*
*    The Medical Bill Payment Statement is sorted by
*   Paid By Indicator ('A', 'C' or 'D'), by Agent/Client/Medical
*   Provider, break on Agent/Client/Medical Provider, having page
*   total by Agent/Client/Medical Provider.
*
***********************************************************************
* </pre>
*/
public class Br559 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private Rr559Report printerFile = new Rr559Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(300);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR559");
	private FixedLengthStringData wsaaExmcode = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(7, 0).setUnsigned();
	private final String wsaaLargeName = "LGNMS";
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final int ct01 = 1;
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData rr559D01 = new FixedLengthStringData(105);
	private FixedLengthStringData rr559d01O = new FixedLengthStringData(105).isAPartOf(rr559D01, 0);
	private ZonedDecimalData rd01Nofmbr = new ZonedDecimalData(7, 0).isAPartOf(rr559d01O, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rr559d01O, 7);
	private FixedLengthStringData rd01Zmedtyp = new FixedLengthStringData(8).isAPartOf(rr559d01O, 15);
	private FixedLengthStringData rd01Alname = new FixedLengthStringData(40).isAPartOf(rr559d01O, 23);
	private FixedLengthStringData rd01Effdate = new FixedLengthStringData(10).isAPartOf(rr559d01O, 63);
	private FixedLengthStringData rd01Invref = new FixedLengthStringData(15).isAPartOf(rr559d01O, 73);
	private ZonedDecimalData rd01Zmedfee = new ZonedDecimalData(17, 2).isAPartOf(rr559d01O, 88);
	private FixedLengthStringData rr559T01 = new FixedLengthStringData(32);
	private FixedLengthStringData rr559t01O = new FixedLengthStringData(32).isAPartOf(rr559T01, 0);
	private FixedLengthStringData rt01Descrn = new FixedLengthStringData(15).isAPartOf(rr559t01O, 0);
	private ZonedDecimalData rt01Zmedfee = new ZonedDecimalData(17, 2).isAPartOf(rr559t01O, 15);
	private FixedLengthStringData rr559E01 = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Clntkey wsaaClntkey = new Clntkey();
	private P6671par p6671par = new P6671par();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private T5620rec t5620rec = new T5620rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Rr559H01Inner rr559H01Inner = new Rr559H01Inner();
	
	/*Batch Upgrade Variables*/
	private int intBatchID;
	private int intBatchExtractSize;	
	private int intBatchStep;
	private String strEffDate;	
	private String tranDesc;
	private int ctrCT01; 
	private BigDecimal grandTotal;
	private BigDecimal subTotal;
	private boolean noRecordFound;
	
	/*PF Used*/
	private MedxTemppf medxTemppf = new MedxTemppf();
	
	private List<MedxTemppf> medxList;
	private Map<String, List<Itempf>> t5620ListMap;
	private List<Descpf> t1693List;
	private List<Descpf> t3629List;
	private Iterator<MedxTemppf> iteratorList;
	
	/*DAO Used*/
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Br559DAO br559DAO =  getApplicationContext().getBean("br559DAO", Br559DAO.class);
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br645.class);	

	public Br559() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000(){
	p6671par.parmRecord.set(bupaIO.getParmarea());
	strEffDate = bsscIO.getEffectiveDate().toString();	
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound=false;
	medxList = new ArrayList<MedxTemppf>();
	
	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
	
	StringBuilder medxTempTableName = new StringBuilder("");
	medxTempTableName.append("MEDX");
	medxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
	medxTempTableName.append(String.format("%0"+ (4 - bsscIO.getScheduleNumber().toString().trim().length())+"d%s",0 , bsscIO.getScheduleNumber().toString().trim()));
	
	int extractRows = br559DAO.populateBr559Temp(intBatchExtractSize, medxTempTableName.toString(), p6671par.chdrnum.toString(), p6671par.chdrnum1.toString());
	if (extractRows > 0) {
		performDataChunk();	
		
		if (!medxList.isEmpty()){			
			initialise1010();
			readSmartTables(bsprIO.getCompany().toString().trim());
			prepareHeader1300();
		}			
	}
	else{
		LOGGER.info("No Medipay records retrieved for processing.");	
		noRecordFound=true;
		return;
	}		
}
private void readSmartTables(String company){
	t5620ListMap = itemDAO.loadSmartTable("IT", company, "T5620");
	t1693List = descDAO.getItems(smtpfxcpy.item.toString(), "0", "T1693");
	t3629List = descDAO.getItems(smtpfxcpy.item.toString(), bsprIO.getCompany().toString(), "T3629");
}

protected void readT1693Desc(){
	boolean itemFound = false;
	for (Descpf descItem : t1693List) {
		if (descItem.getDescitem().trim().equals(bsprIO.getCompany().toString().trim())){
			tranDesc = descItem.getLongdesc();
			itemFound=true;
			break;
		}
	}
	if (!itemFound) {
		fatalError600();	
	}	
}

protected void readT3629Desc(){
	boolean itemFound = false;
	for (Descpf descItem : t3629List) {
		if (descItem.getDescitem().trim().equals(t5620rec.cntcurr.toString().trim())){
			tranDesc = descItem.getLongdesc();
			itemFound=true;
			break;
		}
	}
	if (!itemFound) {
		fatalError600();	
	}	
}

protected void readT5620(){
	String keyItemitem = bprdIO.getDefaultBranch().toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5620ListMap.containsKey(keyItemitem)){	
		itempfList = t5620ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5620rec.t5620Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5620rec.t5620Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5620rec.t5620Rec);
		fatalError600();		
	}	
}

protected void performDataChunk(){
	intBatchID = intBatchStep;		
	if (medxList.size() > 0) medxList.clear();
	medxList = br559DAO.loadDataByBatch(intBatchID);
	if (medxList.isEmpty()){
		if (isNE(grandTotal,ZERO)) {
			printSubTotal3300();
			printGrdTotal3400();
			printEof3500();
		}
		wsspEdterror.set(varcom.endp);
	}else iteratorList = medxList.iterator();		

}
protected void initialise1010(){
	wsspEdterror.set(varcom.oK);
	wsaaCount.set(ZERO);
	wsaaExmcode.set(SPACES);
	p6671par.parmRecord.set(bupaIO.getParmarea());
	t5620ListMap = new HashMap<String, List<Itempf>>();
	t1693List = new ArrayList<Descpf>();	
	t3629List = new ArrayList<Descpf>();
	
	printerFile.openOutput();
	
	grandTotal = BigDecimal.ZERO;
	subTotal = BigDecimal.ZERO;
}

protected void prepareHeader1300()
	{
		start1300();
	}

protected void start1300(){
	readT1693Desc();
	rr559H01Inner.rh01Company.set(bsprIO.getCompany());
	rr559H01Inner.rh01Companynm.set(tranDesc);
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	rr559H01Inner.rh01Sdate.set(datcon1rec.extDate);
	readT5620();
	readT3629Desc();
	rr559H01Inner.rh01Currcode.set(t5620rec.cntcurr);
	rr559H01Inner.rh01Currencynm.set(tranDesc);
}

protected void readFile2000(){
	if (!medxList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!medxList.isEmpty()){
				medxTemppf = new MedxTemppf();
				medxTemppf = iteratorList.next();		
			}
		}else {		
			medxTemppf = new MedxTemppf();
			medxTemppf = iteratorList.next();
		}		
	}else wsspEdterror.set(varcom.endp);
}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT01 = 0;	
}

protected void update3000()	{
	if (isEQ(wsaaExmcode,SPACES)) {
		wsaaExmcode.set(medxTemppf.getExmcode());
		prepareHeader3100();
	}
	if (isNE(medxTemppf.getExmcode(),wsaaExmcode)) {
		printSubTotal3300();
		wsaaOverflow.set("Y");
		wsaaExmcode.set(medxTemppf.getExmcode());
		wsaaCount.set(0);
		subTotal = BigDecimal.ZERO;
		prepareHeader3100();
	}
	printDetails3200();
}

protected void prepareHeader3100(){
	rr559H01Inner.rh01Exmcode.set(wsaaExmcode);
	wsaaClntkey.clntClntnum.set(medxTemppf.getClntnum());
	a1000CallNamadrs();
	rr559H01Inner.rh01Alname.set(namadrsrec.name);
	rr559H01Inner.rh01Cltaddr01.set(namadrsrec.addr1);
	rr559H01Inner.rh01Cltaddr02.set(namadrsrec.addr2);
	rr559H01Inner.rh01Cltaddr03.set(namadrsrec.addr3);
	rr559H01Inner.rh01Cltaddr04.set(namadrsrec.addr4);
	rr559H01Inner.rh01Cltaddr05.set(namadrsrec.addr5);
	rr559H01Inner.rh01Cltpcode.set(namadrsrec.pcode);
}

protected void printDetails3200(){
	start3200();
	writeDetail3200();
}

protected void start3200(){
	wsaaCount.add(1);
	rd01Nofmbr.set(wsaaCount);
	rd01Chdrnum.set(medxTemppf.getChdrnum());
	rd01Zmedtyp.set(medxTemppf.getZmedtyp());
	if ("".equals(medxTemppf.getLifcnum().trim())){
		wsaaClntkey.clntClntnum.set(medxTemppf.getLifcnum());
		a1000CallNamadrs();
		rd01Alname.set(namadrsrec.name);
	}else {
		rd01Alname.set(SPACES);
	}
	rd01Invref.set(medxTemppf.getInvref());
	rd01Zmedfee.set(medxTemppf.getZmedfee());
	datcon1rec.function.set(varcom.conv);
	datcon1rec.intDate.set(medxTemppf.getEffdate());
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	rd01Effdate.set(datcon1rec.extDate);
	subTotal = subTotal.add(medxTemppf.getZmedfee());
	grandTotal = grandTotal.add(medxTemppf.getZmedfee());
}

protected void writeDetail3200(){
	if (newPageReq.isTrue()) {
		ctrCT01++;
		printerFile.printRr559h01(rr559H01Inner.rr559H01, indicArea);
		wsaaOverflow.set("N");
	}
	printerFile.printRr559d01(rr559D01, indicArea);
}

protected void printSubTotal3300(){
	start3310();
}

protected void start3310(){
	rt01Descrn.set("Sub Total");
	rt01Zmedfee.set(subTotal);
	if (newPageReq.isTrue()) {
		ctrCT01++;
		printerFile.printRr559h01(rr559H01Inner.rr559H01, indicArea);
		wsaaOverflow.set("N");
	}
	printerFile.printRr559t01(rr559T01, indicArea);
}

protected void printGrdTotal3400(){
	start3410();
}

protected void start3410(){
	rt01Descrn.set("Grand Total");
	rt01Zmedfee.set(grandTotal);
	if (newPageReq.isTrue()) {
		ctrCT01++;
		printerFile.printRr559h01(rr559H01Inner.rr559H01, indicArea);
		wsaaOverflow.set("N");
	}
	/*  Write detail, checking for page overflow*/
	printerFile.printRr559t01(rr559T01, indicArea);
}

protected void printEof3500(){

	/* If first page/overflow - write standard headings*/
	if (newPageReq.isTrue()) {
		ctrCT01++;
		printerFile.printRr559h01(rr559H01Inner.rr559H01, indicArea);
		wsaaOverflow.set("N");
	}
	printerFile.printRr559e01(rr559E01, indicArea);

}

protected void commit3500(){
	if (!noRecordFound){
		commitControlTotals();
	}
}

protected void rollback3600(){
	/*ROLLBACK*/
	/** Place any additional rollback processing in here.*/
	/*EXIT*/
}

protected void close4000(){
	printerFile.close();
	if (!medxList.isEmpty()){
		lsaaStatuz.set(varcom.oK);
	}
}

protected void a1000CallNamadrs()	{
	a1000Start();
}

protected void a1000Start()	{
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntNumber.set(wsaaClntkey.clntClntnum);
	namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
	namadrsrec.clntCompany.set(bsprIO.getFsuco());
	namadrsrec.language.set(bsscIO.getLanguage());
	namadrsrec.function.set(wsaaLargeName);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.params.set(namadrsrec.namadrsRec);
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError600();
	}
}
/*
 * Class transformed  from Data Structure RR559-H01--INNER
 */
private static final class Rr559H01Inner { 

	private FixedLengthStringData rr559H01 = new FixedLengthStringData(141+DD.cltaddr.length*5);/*pmujavadiya ILIFE-3222*/
	private FixedLengthStringData rr559h01O = new FixedLengthStringData(141+DD.cltaddr.length*5).isAPartOf(rr559H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr559h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr559h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr559h01O, 11);
	private FixedLengthStringData rh01Currcode = new FixedLengthStringData(3).isAPartOf(rr559h01O, 41);
	private FixedLengthStringData rh01Currencynm = new FixedLengthStringData(30).isAPartOf(rr559h01O, 44);
	private FixedLengthStringData rh01Exmcode = new FixedLengthStringData(10).isAPartOf(rr559h01O, 74);
	private FixedLengthStringData rh01Alname = new FixedLengthStringData(47).isAPartOf(rr559h01O, 84);
	private FixedLengthStringData rh01Cltaddr01 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr559h01O, 131);/*pmujavadiya ILIFE-3222*/
	private FixedLengthStringData rh01Cltaddr02 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr559h01O, 131+DD.cltaddr.length);
	private FixedLengthStringData rh01Cltaddr03 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr559h01O, 131+DD.cltaddr.length*2);
	private FixedLengthStringData rh01Cltaddr04 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr559h01O, 131+DD.cltaddr.length*3);
	private FixedLengthStringData rh01Cltpcode = new FixedLengthStringData(10).isAPartOf(rr559h01O, 131+DD.cltaddr.length*4);
	private FixedLengthStringData rh01Cltaddr05 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr559h01O, 141+DD.cltaddr.length*4);/*pmujavadiya ILIFE-3222*/
}
}
