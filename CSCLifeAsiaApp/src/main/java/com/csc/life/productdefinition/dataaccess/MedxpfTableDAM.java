package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MedxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:50
 * Class transformed from MEDXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MedxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 126;
	public FixedLengthStringData medxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData medxpfRecord = medxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(medxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(medxrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(medxrec);
	public FixedLengthStringData exmcode = DD.exmcode.copy().isAPartOf(medxrec);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(medxrec);
	public FixedLengthStringData paidby = DD.paidby.copy().isAPartOf(medxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(medxrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(medxrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(medxrec);
	public FixedLengthStringData invref = DD.invref.copy().isAPartOf(medxrec);
	public PackedDecimalData zmedfee = DD.zmedfee.copy().isAPartOf(medxrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(medxrec);
	public PackedDecimalData dtetrm = DD.dtetrm.copy().isAPartOf(medxrec);
	public FixedLengthStringData desc = DD.desc.copy().isAPartOf(medxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MedxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for MedxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MedxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MedxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MedxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MedxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MedxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MEDXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"SEQNO, " +
							"EXMCODE, " +
							"ZMEDTYP, " +
							"PAIDBY, " +
							"LIFE, " +
							"JLIFE, " +
							"EFFDATE, " +
							"INVREF, " +
							"ZMEDFEE, " +
							"CLNTNUM, " +
							"DTETRM, " +
							"DESC_T, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     seqno,
                                     exmcode,
                                     zmedtyp,
                                     paidby,
                                     life,
                                     jlife,
                                     effdate,
                                     invref,
                                     zmedfee,
                                     clntnum,
                                     dtetrm,
                                     desc,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		seqno.clear();
  		exmcode.clear();
  		zmedtyp.clear();
  		paidby.clear();
  		life.clear();
  		jlife.clear();
  		effdate.clear();
  		invref.clear();
  		zmedfee.clear();
  		clntnum.clear();
  		dtetrm.clear();
  		desc.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMedxrec() {
  		return medxrec;
	}

	public FixedLengthStringData getMedxpfRecord() {
  		return medxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMedxrec(what);
	}

	public void setMedxrec(Object what) {
  		this.medxrec.set(what);
	}

	public void setMedxpfRecord(Object what) {
  		this.medxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(medxrec.getLength());
		result.set(medxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}