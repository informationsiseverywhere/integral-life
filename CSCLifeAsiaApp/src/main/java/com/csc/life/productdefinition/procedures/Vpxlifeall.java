/*
 * File: Vpxchdr.java
 * Date: 20 August 2012 22:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Polcalcrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpxaliferec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* VPXLIFEALL - All LIFE Data Extract Subroutine
* This subroutine will extract the necessary details of ALL the LIFE
* records of a particular policy. 
* Assumption is that there will be a maximum of 10 Life/Joint Life 
* per policy.
*  
* This data extract subroutine will cater for the requirements of 
* the following Policy Level Calculation Programs: P6378, P5074 and 
* P5074AT. This subroutine will be performed via the TR28X set-up for 
* item PL01.  	 
* 
* New Copybooks POLCALCREC and VPXALIFEREC will be the linkages of 
* this subroutine.
* 
* VPXALIFEREC Copybook
*   1.	LIFE			X(02)	Occurs 10
*   2.	JLIFE			X(02)	Occurs 10
*   3.	SMOKING			X(02)	Occurs 10
*   4.	CLTSEX			X(01)	Occurs 10
*   5.	CLTDOB			S9(08) 	Occurs 10
*   6.	OCCUPATION		X(04)	Occurs 10
* 
* 
* Processing
* 
* 	Read LIFELNB until end of file is reached
* 	For each LIFELNB record found
* 	   MOVE corresponding LIFELNB fields to VPXLIFEAREC counterparts 
* 	End-For
* 
*   Status :
*     **** - O-K
*     E882 - Invalid function
*     H211 - Invalid prefix
*     Unknown - Status returned as a result of database
*     error.
*
*****************************************************************
* </pre>
*/
public class Vpxlifeall extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPXLIFEALL";
		/* FORMATS */
	private String chdrlnbrec = "CHDRLNBREC";
	private String clexrec = "CLEXREC";
		/* ERRORS */
	private String e882 = "E882";
	private String f950 = "F950";
		/*Agent header*/
	private Syserrrec syserrrec = new Syserrrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Vpxaliferec vpxaliferec= new Vpxaliferec();
	private Polcalcrec polcalcrec = new Polcalcrec();
	/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Varcom varcom = new Varcom();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9020, 
		exit400, 
		nextLife300
	}

	public Vpxlifeall() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vpxaliferec.vpxalifeRec = convertAndSetParam(vpxaliferec.vpxalifeRec, parmArray, 1);
		polcalcrec.polcalcRec = convertAndSetParam(polcalcrec.polcalcRec, parmArray, 0);
		try {
			startSubr100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr100()
	{
		readLifelnb110();
		exit090();
	}

protected void readLifelnb110()
{
	lifelnbIO.setDataKey(SPACES);
	lifelnbIO.setChdrcoy(polcalcrec.chdrcoy);
	lifelnbIO.setChdrnum(polcalcrec.chdrnum);
	lifelnbIO.setLife(SPACES);
	lifelnbIO.setJlife(SPACES);
	lifelnbIO.setFunction(varcom.begn);
	wsaaSub.set(0);
	while ( !(isEQ(lifelnbIO.getStatuz(),varcom.endp))) {
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		loopLifelnb120();
	}
}

protected void loopLifelnb120()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startLife200();
			}
			case nextLife300: {
				nextLife300();
			}
			case exit400: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void startLife200()
{
	
	SmartFileCode.execute(appVars, lifelnbIO);
	if (isNE(lifelnbIO.getStatuz(),varcom.oK)
	&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(lifelnbIO.getParams());
		fatalError6000();
	}
	if (isNE(lifelnbIO.getChdrcoy(),polcalcrec.chdrcoy)
	|| isNE(lifelnbIO.getChdrnum(),polcalcrec.chdrnum)) {
		lifelnbIO.setStatuz(varcom.endp);
		goTo(GotoLabel.exit400);
	} 
	wsaaSub.add(1);
	vpxaliferec.life[wsaaSub.toInt()].set(lifelnbIO.getLife());
	vpxaliferec.jlife[wsaaSub.toInt()].set(lifelnbIO.getJlife());
	vpxaliferec.smoking[wsaaSub.toInt()].set(lifelnbIO.getSmoking());
	vpxaliferec.cltsex[wsaaSub.toInt()].set(lifelnbIO.getCltsex());
	vpxaliferec.cltdob[wsaaSub.toInt()].set(lifelnbIO.getCltdob());
	vpxaliferec.occupation[wsaaSub.toInt()].set(lifelnbIO.getOccup());
	goTo(GotoLabel.nextLife300);
}

protected void nextLife300()
{
	lifelnbIO.setFunction(varcom.nextr);
}

protected void fatalError6000()
{
	/*ERROR*/
	syserrrec.subrname.set(wsaaSubr);
	if (isNE(syserrrec.statuz,SPACES)
	|| isNE(syserrrec.syserrStatuz,SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT*/
	goBack();
}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
protected void exit090()
	{
		exitProgram();
	}
}
