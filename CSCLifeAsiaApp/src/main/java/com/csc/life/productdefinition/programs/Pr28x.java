/*
 * File: Pr28x.java
 * Date: 30 August 2009 1:08:55
 * Author: Quipoz Limited
 * 
 * Class transformed from PH578.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.Sr28xScreenVars;
import com.csc.life.productdefinition.tablestructures.Tr28xrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            PH578 MAINLINE.
*
*   Parameter prompt program
*
*****************************************************************
* </pre>
*/
public class Pr28x extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR28X");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();	
	private Tr28xrec tr28xrec = new Tr28xrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr28xScreenVars sv = ScreenProgram.getScreenVars( Sr28xScreenVars.class);
	private String e186 = "E186";
	private String e315 = "E315";
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045,
		preExit,
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr28x() {
		super();
		screenVars = sv;
		new ScreenModel("Sr28x", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				initialise1010();
				readRecord1031();
				moveToScreen1040();
			}
			case generalArea1045: {
				generalArea1045();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void initialise1010()
{
	/*INITIALISE-SCREEN*/
	sv.dataArea.set(SPACES);
	/*READ-PRIMARY-RECORD*/
	/*READ-RECORD*/
	itemIO.setDataKey(wsspsmart.itemkey);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	/*READ-SECONDARY-RECORDS*/
}

protected void readRecord1031()
{
	descIO.setDescpfx(itemIO.getItempfx());
	descIO.setDesccoy(itemIO.getItemcoy());
	descIO.setDesctabl(itemIO.getItemtabl());
	descIO.setDescitem(itemIO.getItemitem());
	descIO.setItemseq(SPACES);
	descIO.setLanguage(wsspcomn.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
}

protected void moveToScreen1040()
{
	sv.company.set(itemIO.getItemcoy());
	sv.tabl.set(itemIO.getItemtabl());
	sv.item.set(itemIO.getItemitem());
	sv.longdesc.set(descIO.getLongdesc());
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		sv.longdesc.set(SPACES);
	}
	tr28xrec.tr28xRec.set(itemIO.getGenarea());
	if (isNE(itemIO.getGenarea(),SPACES)) {
		goTo(GotoLabel.generalArea1045);
	}
}

protected void generalArea1045()
{
	sv.dataextracts.set(tr28xrec.dataextracts);
	sv.funcs.set(tr28xrec.funcs);
	sv.dataupdates.set(tr28xrec.dataupdates);
	sv.vpmmodel.set(tr28xrec.vpmmodel);
	sv.linkstatuz.set(tr28xrec.linkstatuz);
	sv.linkcopybk.set(tr28xrec.linkcopybk);
	sv.vpmsubr.set(tr28xrec.vpmsubr);
	sv.extfldids.set(tr28xrec.extfldids);
	sv.dataLog.set(tr28xrec.dataLog);
	/*CONFIRMATION-FIELDS*/
	/*OTHER*/
	/*EXIT*/
}

protected void preScreenEdit()
{
	try {
		preStart();
	}
	catch (GOTOException e){
	}
}

protected void preStart()
{
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
	goTo(GotoLabel.preExit);
}

protected void screenEdit2000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				screenIo2010();
				checkDataExtract9010();
				checkVpmsModel9020();
				checkLinkage9030();
				checkVpmsubr9035();
				checkExtFields9040();
				checkDataLog9050();
			}
			case exit2090: {
				exit2090();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void screenIo2010()
{
	wsspcomn.edterror.set(varcom.oK);
	/*VALIDATE*/
	if (isEQ(wsspcomn.flag,"I")) {
		goTo(GotoLabel.exit2090);
	}
	/*OTHER*/
}

protected void checkDataExtract9010()
{
	if (isEQ(sv.dataextract01,SPACES) && isNE(sv.func01,SPACES) 
			|| isNE(sv.dataextract01,SPACES) && isEQ(sv.func01,SPACES) ) {
		sv.dataextract01Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract02,SPACES) && isNE(sv.func02,SPACES) 
			|| isNE(sv.dataextract02,SPACES) && isEQ(sv.func02,SPACES) ) {
		sv.dataextract02Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract03,SPACES) && isNE(sv.func03,SPACES) 
			|| isNE(sv.dataextract03,SPACES) && isEQ(sv.func03,SPACES) ) {
		sv.dataextract03Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract04,SPACES) && isNE(sv.func04,SPACES) 
			|| isNE(sv.dataextract04,SPACES) && isEQ(sv.func04,SPACES) ) {
		sv.dataextract04Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract05,SPACES) && isNE(sv.func05,SPACES) 
			|| isNE(sv.dataextract05,SPACES) && isEQ(sv.func05,SPACES) ) {
		sv.dataextract05Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract06,SPACES) && isNE(sv.func06,SPACES) 
			|| isNE(sv.dataextract06,SPACES) && isEQ(sv.func06,SPACES) ) {
		sv.dataextract06Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract07,SPACES) && isNE(sv.func07,SPACES) 
			|| isNE(sv.dataextract07,SPACES) && isEQ(sv.func07,SPACES) ) {
		sv.dataextract07Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract08,SPACES) && isNE(sv.func08,SPACES) 
			|| isNE(sv.dataextract08,SPACES) && isEQ(sv.func08,SPACES) ) {
		sv.dataextract08Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.dataextract09,SPACES) && isNE(sv.func09,SPACES) 
			|| isNE(sv.dataextract09,SPACES) && isEQ(sv.func09,SPACES) ) {
		sv.dataextract09Err.set(e186);
		goTo(GotoLabel.exit2090);
	}

}
protected void checkVpmsModel9020()
{
	if (isEQ(sv.vpmmodel,SPACES)) {
		sv.vpmmodelErr.set(e186);
		goTo(GotoLabel.exit2090);
	}
}
protected void checkLinkage9030()
{
	if (isEQ(sv.linkstatuz,SPACES) && isNE(sv.linkcopybk,SPACES) 
			|| isNE(sv.linkstatuz,SPACES) && isEQ(sv.linkcopybk,SPACES) ) {
		sv.linkstatuzErr.set(e186);
		goTo(GotoLabel.exit2090);
	}
}
protected void checkVpmsubr9035()
{
	if (isEQ(sv.vpmsubr,SPACES) && isNE(sv.vpmsubr,SPACES) 
			|| isNE(sv.vpmsubr,SPACES) && isEQ(sv.vpmsubr,SPACES) ) {
		sv.vpmsubrErr.set(e186);
		goTo(GotoLabel.exit2090);
	}
}
protected void checkExtFields9040()
{
	for (int i = 1; i < sv.extfldid.length; i++){
		FixedLengthStringData exfldChild = sv.extfldid[i];
		if (isNE(exfldChild,SPACES)) {
			return;
		}
	}
	sv.extfldidsErr.set(e186);
	goTo(GotoLabel.exit2090);
}

protected void checkDataLog9050()
{
	if (isNE(sv.dataLog,"Y") && isNE(sv.dataLog,"N") ) {
		sv.dataLogErr.set(e315);
		goTo(GotoLabel.exit2090);
	}
}

protected void exit2090()
{
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}

protected void update3000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				preparation3010();
				updatePrimaryRecord3050();
				updateRecord3055();
			}
			case other3080: {
			}
			case exit3090: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void preparation3010()
{
	if (isEQ(wsspcomn.flag,"I")) {
		goTo(GotoLabel.exit3090);
	}
}

protected void updatePrimaryRecord3050()
{
	itemIO.setFunction(varcom.readh);
	itemIO.setDataKey(wsspsmart.itemkey);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	varcom.vrcmTranid.set(wsspcomn.tranid);
	varcom.vrcmCompTermid.set(varcom.vrcmTermid);
	varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
	itemIO.setTranid(varcom.vrcmCompTranid);
}

protected void updateRecord3055()
{
	wsaaUpdateFlag = "N";
	checkChanges3100();
	if (isNE(wsaaUpdateFlag,"Y")) {
		goTo(GotoLabel.other3080);
	}
	itemIO.setGenarea(tr28xrec.tr28xRec);
	itemIO.setFunction(varcom.rewrt);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
}

protected void checkChanges3100()
{
	/*CHECK*/
	if (isNE(sv.dataextracts,tr28xrec.dataextracts)) {
		tr28xrec.dataextracts.set(sv.dataextracts);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.funcs,tr28xrec.funcs)) {
		tr28xrec.funcs.set(sv.funcs);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.dataupdates,tr28xrec.dataupdates)) {
		tr28xrec.dataupdates.set(sv.dataupdates);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.dataLog,tr28xrec.dataLog)) {
		tr28xrec.dataLog.set(sv.dataLog);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.vpmmodel,tr28xrec.vpmmodel)) {
		tr28xrec.vpmmodel.set(sv.vpmmodel);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.linkstatuz,tr28xrec.linkstatuz)) {
		tr28xrec.linkstatuz.set(sv.linkstatuz);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.linkcopybk,tr28xrec.linkcopybk)) {
		tr28xrec.linkcopybk.set(sv.linkcopybk);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.vpmsubr,tr28xrec.vpmsubr)) {
		tr28xrec.vpmsubr.set(sv.vpmsubr);
		wsaaUpdateFlag = "Y";
	}
	if (isNE(sv.extfldids,tr28xrec.extfldids)) {
		tr28xrec.extfldids.set(sv.extfldids);
		wsaaUpdateFlag = "Y";
	}
	/*EXIT*/
}

protected void whereNext4000()
{
	/*NEXT-PROGRAM*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}
}