package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: NterpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:57
 * Class transformed from NTERPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class NterpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 119;
	public FixedLengthStringData nterrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData nterpfRecord = nterrec;
	
	public PackedDecimalData hprrcvdt = DD.hprrcvdt.copy().isAPartOf(nterrec);
	public PackedDecimalData tdayno = DD.tdayno.copy().isAPartOf(nterrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(nterrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(nterrec);
	public FixedLengthStringData erorprog = DD.erorprog.copy().isAPartOf(nterrec);
	public FixedLengthStringData flddesc = DD.flddesc.copy().isAPartOf(nterrec);
	public FixedLengthStringData eror = DD.eror.copy().isAPartOf(nterrec);
	public FixedLengthStringData desc = DD.desc.copy().isAPartOf(nterrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(nterrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public NterpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for NterpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public NterpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for NterpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public NterpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for NterpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public NterpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("NTERPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"HPRRCVDT, " +
							"TDAYNO, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"ERORPROG, " +
							"FLDDESC, " +
							"EROR, " +
							"DESC_T, " +
							"INSTPREM, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     hprrcvdt,
                                     tdayno,
                                     chdrcoy,
                                     chdrnum,
                                     erorprog,
                                     flddesc,
                                     eror,
                                     desc,
                                     instprem,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		hprrcvdt.clear();
  		tdayno.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		erorprog.clear();
  		flddesc.clear();
  		eror.clear();
  		desc.clear();
  		instprem.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getNterrec() {
  		return nterrec;
	}

	public FixedLengthStringData getNterpfRecord() {
  		return nterpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setNterrec(what);
	}

	public void setNterrec(Object what) {
  		this.nterrec.set(what);
	}

	public void setNterpfRecord(Object what) {
  		this.nterpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(nterrec.getLength());
		result.set(nterrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}