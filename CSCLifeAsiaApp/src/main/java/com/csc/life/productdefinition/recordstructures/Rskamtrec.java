package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 03 Jul 2018 03:09:00
 * Description:
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rskamtrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rskamtRec = new FixedLengthStringData(98);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rskamtRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rskamtRec, 5);
  	public FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(rskamtRec, 9);
  	public ZonedDecimalData factor = new ZonedDecimalData(5,2).isAPartOf(rskamtRec, 13);
  	public ZonedDecimalData riskCessTerm = new ZonedDecimalData(3).isAPartOf(rskamtRec, 18);
  	public ZonedDecimalData premCessTerm = new ZonedDecimalData(3).isAPartOf(rskamtRec, 21);
  	public ZonedDecimalData riskamnt = new ZonedDecimalData(17,2).isAPartOf(rskamtRec, 24);
  	public ZonedDecimalData modeprem = new ZonedDecimalData(17,2).isAPartOf(rskamtRec, 41);
    public ZonedDecimalData sumin = new ZonedDecimalData(17,2).isAPartOf(rskamtRec, 58);
	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rskamtRec, 75);
	public FixedLengthStringData compcode =new FixedLengthStringData(4).isAPartOf(rskamtRec, 76);
	public ZonedDecimalData riskcommDate = new ZonedDecimalData(8, 0).isAPartOf(rskamtRec, 80).setUnsigned();
	public FixedLengthStringData billingfreq = new FixedLengthStringData(10).isAPartOf(rskamtRec,88);
	
  
	public void initialize() {
		COBOLFunctions.initialize(rskamtRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			rskamtRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}
