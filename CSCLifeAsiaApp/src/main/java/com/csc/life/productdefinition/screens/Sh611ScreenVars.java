package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH611
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sh611ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(828);
	public FixedLengthStringData dataFields = new FixedLengthStringData(348).isAPartOf(dataArea, 0);
	public FixedLengthStringData cmaxs = new FixedLengthStringData(136).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] cmax = ZDArrayPartOfStructure(8, 17, 2, cmaxs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(136).isAPartOf(cmaxs, 0, FILLER_REDEFINE);
	public ZonedDecimalData cmax01 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData cmax02 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,17);
	public ZonedDecimalData cmax03 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,34);
	public ZonedDecimalData cmax04 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,51);
	public ZonedDecimalData cmax05 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,68);
	public ZonedDecimalData cmax06 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,85);
	public ZonedDecimalData cmax07 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,102);
	public ZonedDecimalData cmax08 = DD.cmax.copyToZonedDecimal().isAPartOf(filler,119);
	public FixedLengthStringData cmins = new FixedLengthStringData(136).isAPartOf(dataFields, 136);
	public ZonedDecimalData[] cmin = ZDArrayPartOfStructure(8, 17, 2, cmins, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(136).isAPartOf(cmins, 0, FILLER_REDEFINE);
	public ZonedDecimalData cmin01 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData cmin02 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData cmin03 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData cmin04 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData cmin05 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData cmin06 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData cmin07 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData cmin08 = DD.cmin.copyToZonedDecimal().isAPartOf(filler1,119);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,272);
	public FixedLengthStringData frequencys = new FixedLengthStringData(16).isAPartOf(dataFields, 273);
	public FixedLengthStringData[] frequency = FLSArrayPartOfStructure(8, 2, frequencys, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(frequencys, 0, FILLER_REDEFINE);
	public FixedLengthStringData frequency01 = DD.frqncy.copy().isAPartOf(filler2,0);
	public FixedLengthStringData frequency02 = DD.frqncy.copy().isAPartOf(filler2,2);
	public FixedLengthStringData frequency03 = DD.frqncy.copy().isAPartOf(filler2,4);
	public FixedLengthStringData frequency04 = DD.frqncy.copy().isAPartOf(filler2,6);
	public FixedLengthStringData frequency05 = DD.frqncy.copy().isAPartOf(filler2,8);
	public FixedLengthStringData frequency06 = DD.frqncy.copy().isAPartOf(filler2,10);
	public FixedLengthStringData frequency07 = DD.frqncy.copy().isAPartOf(filler2,12);
	public FixedLengthStringData frequency08 = DD.frqncy.copy().isAPartOf(filler2,14);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,289);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,297);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,305);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,313);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,343);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 348);
	public FixedLengthStringData cmaxsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] cmaxErr = FLSArrayPartOfStructure(8, 4, cmaxsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(32).isAPartOf(cmaxsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cmax01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData cmax02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData cmax03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData cmax04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData cmax05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData cmax06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData cmax07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData cmax08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData cminsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] cminErr = FLSArrayPartOfStructure(8, 4, cminsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(32).isAPartOf(cminsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cmin01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData cmin02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData cmin03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData cmin04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData cmin05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData cmin06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData cmin07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData cmin08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData frqncysErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] frqncyErr = FLSArrayPartOfStructure(8, 4, frqncysErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(32).isAPartOf(frqncysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData frqncy01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData frqncy02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData frqncy03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData frqncy04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData frqncy05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData frqncy06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData frqncy07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData frqncy08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 468);
	public FixedLengthStringData cmaxsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] cmaxOut = FLSArrayPartOfStructure(8, 12, cmaxsOut, 0);
	public FixedLengthStringData[][] cmaxO = FLSDArrayPartOfArrayStructure(12, 1, cmaxOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(96).isAPartOf(cmaxsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cmax01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] cmax02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] cmax03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] cmax04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] cmax05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] cmax06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] cmax07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] cmax08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData cminsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] cminOut = FLSArrayPartOfStructure(8, 12, cminsOut, 0);
	public FixedLengthStringData[][] cminO = FLSDArrayPartOfArrayStructure(12, 1, cminOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(96).isAPartOf(cminsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cmin01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] cmin02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] cmin03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] cmin04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] cmin05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] cmin06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] cmin07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] cmin08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData frqncysOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] frqncyOut = FLSArrayPartOfStructure(8, 12, frqncysOut, 0);
	public FixedLengthStringData[][] frqncyO = FLSDArrayPartOfArrayStructure(12, 1, frqncyOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(96).isAPartOf(frqncysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] frqncy01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] frqncy02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] frqncy03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] frqncy04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] frqncy05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] frqncy06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] frqncy07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] frqncy08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sh611screenWritten = new LongData(0);
	public LongData Sh611protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh611ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itmfrmOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itmtoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin01Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax01Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax02Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy03Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin03Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax03Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy04Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin04Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin05Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax05Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy06Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin06Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax06Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy07Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin07Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax07Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqncy08Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmin08Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmax08Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, frequency01, cmin01, cmax01, frequency02, cmin02, cmax02, frequency03, cmin03, cmax03, frequency04, cmin04, cmax04, frequency05, cmin05, cmax05, frequency06, cmin06, cmax06, frequency07, cmin07, cmax07, frequency08, cmin08, cmax08};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, frqncy01Out, cmin01Out, cmax01Out, frqncy02Out, cmin02Out, cmax02Out, frqncy03Out, cmin03Out, cmax03Out, frqncy04Out, cmin04Out, cmax04Out, frqncy05Out, cmin05Out, cmax05Out, frqncy06Out, cmin06Out, cmax06Out, frqncy07Out, cmin07Out, cmax07Out, frqncy08Out, cmin08Out, cmax08Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, frqncy01Err, cmin01Err, cmax01Err, frqncy02Err, cmin02Err, cmax02Err, frqncy03Err, cmin03Err, cmax03Err, frqncy04Err, cmin04Err, cmax04Err, frqncy05Err, cmin05Err, cmax05Err, frqncy06Err, cmin06Err, cmax06Err, frqncy07Err, cmin07Err, cmax07Err, frqncy08Err, cmin08Err, cmax08Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh611screen.class;
		protectRecord = Sh611protect.class;
	}

}
