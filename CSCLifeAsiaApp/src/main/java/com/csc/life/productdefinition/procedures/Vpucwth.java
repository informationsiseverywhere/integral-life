package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.unitlinkedprocessing.recordstructures.Rwcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Vpucwth extends COBOLConvCodeModel {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	
	private String wsaaSubr = "VPUCWTH";
	
	private ZonedDecimalData wsaaVariables = new ZonedDecimalData(23);

	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(7).isAPartOf(wsaaVariables, 0);

	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1).isAPartOf(wsaaVariables, 7);

	private ZonedDecimalData wsaaTotestval = new ZonedDecimalData(15).isAPartOf(wsaaVariables, 8);


	private FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq,"Y");

	
	private String wscc12 = "12";
	private String wsccCmdf = "CMDF";
	private String wsccN = "N";
	private String wsccNext ="NEXT";
	private String wsccY = "Y";
	private String t5540 = "T5540";
	private String t5542 = "T5542";
	private String h141 = "H141";
	
	private String itemrec = "ITEMREC";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T5540rec t5540rec = new T5540rec();
	private T5542rec t5542rec = new T5542rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Rwcalcpy rwcalcpy = new Rwcalcpy();
	private Vpmfmtrec vpmfmtrec = new Vpmfmtrec();	
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	
	public Vpucwth() {
		super();
	}	
	
	public void mainline(Object... parmArray){
		vpmfmtrec = (Vpmfmtrec) parmArray[1];			
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		rwcalcpy.withdrawRec.set(vpmcalcrec.linkageArea);
		
		main0100();
				
		vpmcalcrec.linkageArea.set(rwcalcpy.withdrawRec);
	}
	
	private void main0100() {
		if(isEQ(rwcalcpy.function,varcom.begn)){
			wsaaTotestval.set(ZERO);
			rwcalcpy.actualVal.set(ZERO);
			rwcalcpy.function.set(wsccNext);
		}

		if(!isEQ(rwcalcpy.status,varcom.endp)){
			wsaaTotestval.add(rwcalcpy.estimatedVal);
			return;
		}

		initialize(itdmIO.getDataArea());		
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		itdmIO.setItemitem(rwcalcpy.crtable);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setParams(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)||!isEQ(itdmIO.getItemtabl(), t5540)||
				!isEQ(itdmIO.getItemitem(), rwcalcpy.crtable)||!isEQ(itdmIO.getStatuz(), varcom.oK)){
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h141);

		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		initialize(itdmIO.getDataArea());
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5542);
		wsaaItemitem.set(t5540rec.wdmeth.toString() + rwcalcpy.currcode.toString());
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (!isEQ(itdmIO.getParams(), varcom.oK)	&& !isEQ(itdmIO.getParams(),varcom.endp)){ 
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h141);

		}

		if(!isEQ(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)||!isEQ(itdmIO.getItemtabl(), t5542)||
				!isEQ(itdmIO.getItemitem(), wsaaItemitem)||isEQ(itdmIO.getStatuz(), varcom.endp)){
			rwcalcpy.psNotAllwd.set(SPACES);
			rwcalcpy.neUnits.set(SPACES);
			rwcalcpy.tmUnits.set(SPACES);
			rwcalcpy.actualVal.set(ZERO);
			return;
		}else{
			t5542rec.t5542Rec.set(itdmIO.getGenarea());
		}


		wsaaSub.set(ZERO);
		wsaaGotFreq.set(wsccY);
		
		for (wsaaSub.set(1); !(!gotFreq.isTrue()
		|| isEQ(rwcalcpy.billfreq,t5542rec.billfreq[wsaaSub.toInt()])); wsaaSub.add(1)){
			if (isGT(wsaaSub,6)) {
				wsaaGotFreq.set(wsccN);
				syserrrec.statuz.set(h141);
			}
		}
		
		if(!gotFreq.isTrue()){
			wsaaGotFreq.set(wsccY);
			rwcalcpy.psNotAllwd.set(SPACES);
			rwcalcpy.neUnits.set(SPACES);
			rwcalcpy.tmUnits.set(SPACES);
			rwcalcpy.actualVal.set(ZERO);
			return;
		}
		
		rwcalcpy.actualVal.set(div((mult(wsaaTotestval,t5542rec.feepc[wsaaSub.toInt()])),100));
		
		if (isLT(rwcalcpy.actualVal,t5542rec.feemin[wsaaSub.toInt()])) {
			rwcalcpy.actualVal.set(t5542rec.feemin[wsaaSub.toInt()]);
		}
		
		if(!isEQ(t5542rec.wdlFreq[wsaaSub.toInt()],ZERO)){
			if(isLT(vpmfmtrec.rate01, t5542rec.wdlFreq[wsaaSub.toInt()])){
				rwcalcpy.psNotAllwd.set(wsccY);
			}
		}
		
		if(!isEQ(t5542rec.wdlAmount[wsaaSub.toInt()],ZERO)){
			if(isGT(t5542rec.wdlAmount[wsaaSub.toInt()], wsaaTotestval)){
				rwcalcpy.neUnits.set(wsccY);
			}

		}
		
		if(!isEQ(t5542rec.wdrem[wsaaSub.toInt()],ZERO)){
			if(isLT(t5542rec.wdrem[wsaaSub.toInt()], wsaaTotestval)){
				rwcalcpy.tmUnits.set(wsccY);
			}
		}
	}
	
}
