package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR594
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr594ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(864);
	public FixedLengthStringData dataFields = new FixedLengthStringData(368).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData undlimits = new FixedLengthStringData(153).isAPartOf(dataFields, 44);
	public ZonedDecimalData[] undlimit = ZDArrayPartOfStructure(9, 17, 0, undlimits, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(153).isAPartOf(undlimits, 0, FILLER_REDEFINE);
	public ZonedDecimalData undlimit01 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData undlimit02 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,17);
	public ZonedDecimalData undlimit03 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,34);
	public ZonedDecimalData undlimit04 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,51);
	public ZonedDecimalData undlimit05 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,68);
	public ZonedDecimalData undlimit06 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,85);
	public ZonedDecimalData undlimit07 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,102);
	public ZonedDecimalData undlimit08 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,119);
	public ZonedDecimalData undlimit09 = DD.undlimit.copyToZonedDecimal().isAPartOf(filler,136);
	public FixedLengthStringData usundlims = new FixedLengthStringData(153).isAPartOf(dataFields, 197);
	public ZonedDecimalData[] usundlim = ZDArrayPartOfStructure(9, 17, 0, usundlims, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(153).isAPartOf(usundlims, 0, FILLER_REDEFINE);
	public ZonedDecimalData usundlim01 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData usundlim02 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData usundlim03 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData usundlim04 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData usundlim05 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData usundlim06 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData usundlim07 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData usundlim08 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData usundlim09 = DD.usundlim.copyToZonedDecimal().isAPartOf(filler1,136);
	public FixedLengthStringData uwplntyps = new FixedLengthStringData(18).isAPartOf(dataFields, 350);
	public FixedLengthStringData[] uwplntyp = FLSArrayPartOfStructure(9, 2, uwplntyps, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(18).isAPartOf(uwplntyps, 0, FILLER_REDEFINE);
	public FixedLengthStringData uwplntyp01 = DD.uwplntyp.copy().isAPartOf(filler2,0);
	public FixedLengthStringData uwplntyp02 = DD.uwplntyp.copy().isAPartOf(filler2,2);
	public FixedLengthStringData uwplntyp03 = DD.uwplntyp.copy().isAPartOf(filler2,4);
	public FixedLengthStringData uwplntyp04 = DD.uwplntyp.copy().isAPartOf(filler2,6);
	public FixedLengthStringData uwplntyp05 = DD.uwplntyp.copy().isAPartOf(filler2,8);
	public FixedLengthStringData uwplntyp06 = DD.uwplntyp.copy().isAPartOf(filler2,10);
	public FixedLengthStringData uwplntyp07 = DD.uwplntyp.copy().isAPartOf(filler2,12);
	public FixedLengthStringData uwplntyp08 = DD.uwplntyp.copy().isAPartOf(filler2,14);
	public FixedLengthStringData uwplntyp09 = DD.uwplntyp.copy().isAPartOf(filler2,16);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 368);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData undlimitsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] undlimitErr = FLSArrayPartOfStructure(9, 4, undlimitsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(36).isAPartOf(undlimitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData undlimit01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData undlimit02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData undlimit03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData undlimit04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData undlimit05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData undlimit06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData undlimit07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData undlimit08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData undlimit09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData usundlimsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] usundlimErr = FLSArrayPartOfStructure(9, 4, usundlimsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(usundlimsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData usundlim01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData usundlim02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData usundlim03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData usundlim04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData usundlim05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData usundlim06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData usundlim07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData usundlim08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData usundlim09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData uwplntypsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData[] uwplntypErr = FLSArrayPartOfStructure(9, 4, uwplntypsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(uwplntypsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData uwplntyp01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData uwplntyp02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData uwplntyp03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData uwplntyp04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData uwplntyp05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData uwplntyp06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData uwplntyp07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData uwplntyp08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData uwplntyp09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(372).isAPartOf(dataArea, 492);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData undlimitsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] undlimitOut = FLSArrayPartOfStructure(9, 12, undlimitsOut, 0);
	public FixedLengthStringData[][] undlimitO = FLSDArrayPartOfArrayStructure(12, 1, undlimitOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(108).isAPartOf(undlimitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] undlimit01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] undlimit02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] undlimit03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] undlimit04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] undlimit05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] undlimit06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] undlimit07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] undlimit08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] undlimit09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData usundlimsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] usundlimOut = FLSArrayPartOfStructure(9, 12, usundlimsOut, 0);
	public FixedLengthStringData[][] usundlimO = FLSDArrayPartOfArrayStructure(12, 1, usundlimOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(108).isAPartOf(usundlimsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] usundlim01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] usundlim02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] usundlim03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] usundlim04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] usundlim05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] usundlim06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] usundlim07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] usundlim08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] usundlim09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData uwplntypsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 264);
	public FixedLengthStringData[] uwplntypOut = FLSArrayPartOfStructure(9, 12, uwplntypsOut, 0);
	public FixedLengthStringData[][] uwplntypO = FLSDArrayPartOfArrayStructure(12, 1, uwplntypOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(108).isAPartOf(uwplntypsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] uwplntyp01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] uwplntyp02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] uwplntyp03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] uwplntyp04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] uwplntyp05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] uwplntyp06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] uwplntyp07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] uwplntyp08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] uwplntyp09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr594screenWritten = new LongData(0);
	public LongData Sr594protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr594ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(uwplntyp01Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit01Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim01Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp02Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit02Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim02Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp03Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit03Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim03Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit04Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim04Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp05Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim05Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp06Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit06Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim06Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp07Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit07Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim07Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp08Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit08Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim08Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwplntyp09Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undlimit09Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(usundlim09Out,new String[] {"16",null, "-15",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, uwplntyp01, undlimit01, usundlim01, uwplntyp02, undlimit02, usundlim02, uwplntyp03, undlimit03, usundlim03, uwplntyp04, undlimit04, usundlim04, uwplntyp05, undlimit05, usundlim05, uwplntyp06, undlimit06, usundlim06, uwplntyp07, undlimit07, usundlim07, uwplntyp08, undlimit08, usundlim08, uwplntyp09, undlimit09, usundlim09};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, uwplntyp01Out, undlimit01Out, usundlim01Out, uwplntyp02Out, undlimit02Out, usundlim02Out, uwplntyp03Out, undlimit03Out, usundlim03Out, uwplntyp04Out, undlimit04Out, usundlim04Out, uwplntyp05Out, undlimit05Out, usundlim05Out, uwplntyp06Out, undlimit06Out, usundlim06Out, uwplntyp07Out, undlimit07Out, usundlim07Out, uwplntyp08Out, undlimit08Out, usundlim08Out, uwplntyp09Out, undlimit09Out, usundlim09Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, uwplntyp01Err, undlimit01Err, usundlim01Err, uwplntyp02Err, undlimit02Err, usundlim02Err, uwplntyp03Err, undlimit03Err, usundlim03Err, uwplntyp04Err, undlimit04Err, usundlim04Err, uwplntyp05Err, undlimit05Err, usundlim05Err, uwplntyp06Err, undlimit06Err, usundlim06Err, uwplntyp07Err, undlimit07Err, usundlim07Err, uwplntyp08Err, undlimit08Err, usundlim08Err, uwplntyp09Err, undlimit09Err, usundlim09Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr594screen.class;
		protectRecord = Sr594protect.class;
	}

}
