/*
 * File: Pr653s.java
 * Date: 30 August 2009 1:51:54
 * Author: Quipoz Limited
 *
 * Class transformed from PR653S.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*    This is a Variable Dialogue subroutine. It is called
*    by OPTSWCH.
*
***********************************************************************
* </pre>
*/
public class Pr653s extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("PR653S");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String chdrrec = "CHDRREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaPrograms = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaProgram = FLSArrayPartOfStructure(4, 5, wsaaPrograms, 0);
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Contract header file*/
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		errorProg620
	}

	public Pr653s() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 2);
		wsaaPrograms = convertAndSetParam(wsaaPrograms, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		begin0010();
		exit0090();
	}

protected void begin0010()
	{
		wsaaChkpStatuz.set(varcom.oK);
		wsaaPrograms.set(SPACES);
		chdrIO.setFunction(varcom.retrv);
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrIO.getStatuz());
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getValidflag(),"1")){
			wsaaProgram[1].set("P6353");
		}
		else if (isEQ(chdrIO.getValidflag(),"3")){
			wsaaProgram[1].set("P5003");
		}
	}

protected void exit0090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError610();
				}
				case errorProg620: {
					errorProg620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg620);
		}
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg620()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
