package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sd5g6
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sd5g6ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(169);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(89).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData clntnam = DD.clntnam.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData ldesc = DD.ldesc.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(dataFields,68);
	public ZonedDecimalData tranamt = DD.tranamt.copyToZonedDecimal().isAPartOf(dataFields,72);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 89);
	public FixedLengthStringData clntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lrkclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tranamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 109);
	public FixedLengthStringData[] clntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lrkclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tranamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(283);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(101).isAPartOf(subfileArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData statuz = DD.statuz.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData compdesc = DD.compdesc.copy().isAPartOf(subfileFields,12);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(subfileFields,42);
	public ZonedDecimalData riskCessTerm = DD.rcestrm.copyToZonedDecimal().isAPartOf(subfileFields,57);
	public FixedLengthStringData premCessTerm = DD.enqPTerm.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData compcode = DD.crtable.copy().isAPartOf(subfileFields,63);
	public FixedLengthStringData payfreq = DD.payfreq.copy().isAPartOf(subfileFields,67);
	public ZonedDecimalData riskamnt = DD.tranamt.copyToZonedDecimal().isAPartOf(subfileFields,69);
	public ZonedDecimalData modeprem = DD.suminmin.copyToZonedDecimal().isAPartOf(subfileFields,86);
	
	
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 101);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnttypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData statuzErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData compdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData riskCessTermErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData premCessTermErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData compcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData payfreqErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData riskamntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData modepremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 149);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnttypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] statuzOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] compdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] riskCessTermOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[]  premCessTermOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[]  compcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[]  payfreqOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[]  riskamntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[]  modepremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 281);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sd5g6screensflWritten = new LongData(0);
	public LongData Sd5g6screenctlWritten = new LongData(0);
	public LongData Sd5g6screenWritten = new LongData(0);
	public LongData Sd5g6protectWritten = new LongData(0);
	public GeneralTable Sd5g6screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return Sd5g6screensfl;
	}

	public Sd5g6ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] { statuz, chdrnum,  compdesc, riskCessTerm, premCessTerm,sumin,compcode,payfreq, riskamnt,modeprem};
		screenSflOutFields = new BaseData[][] { statuzOut, chdrnumOut, compdescOut, riskCessTermOut,premCessTermOut, suminsOut ,compcodeOut,payfreqOut, riskamntOut,modepremOut};
		screenSflErrFields = new BaseData[] {statuzErr, chdrnumErr, compdescErr, riskCessTermErr,premCessTermErr, suminsErr ,compcodeErr,payfreqErr, riskamntErr,modepremErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntnum, clntnam, lrkcls, ldesc, tranamt};
		screenOutFields = new BaseData[][] {clntnumOut, clntnamOut, lrkclsOut, ldescOut, tranamtOut};
		screenErrFields = new BaseData[] {clntnumErr, clntnamErr, lrkclsErr, ldescErr, tranamtErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5g6screen.class;
		screenSflRecord = Sd5g6screensfl.class;
		screenCtlRecord = Sd5g6screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5g6protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5g6screenctl.lrec.pageSubfile);
	}
}
