package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:44
 * Description:
 * Copybook name: T5667REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5667rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5667Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData freqs = new FixedLengthStringData(22).isAPartOf(t5667Rec, 0);
  	public FixedLengthStringData[] freq = FLSArrayPartOfStructure(11, 2, freqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(22).isAPartOf(freqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData freq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData freq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData freq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData freq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData freq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData freq07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData freq08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData freq09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData freq10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData freq11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData maxAmounts = new FixedLengthStringData(187).isAPartOf(t5667Rec, 22);
  	public ZonedDecimalData[] maxAmount = ZDArrayPartOfStructure(11, 17, 2, maxAmounts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(187).isAPartOf(maxAmounts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxAmount01 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData maxAmount02 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 17);
  	public ZonedDecimalData maxAmount03 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 34);
  	public ZonedDecimalData maxAmount04 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 51);
  	public ZonedDecimalData maxAmount05 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 68);
  	public ZonedDecimalData maxAmount06 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData maxAmount07 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 102);
  	public ZonedDecimalData maxAmount08 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 119);
  	public ZonedDecimalData maxAmount09 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 136);
  	public ZonedDecimalData maxAmount10 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 153);
  	public ZonedDecimalData maxAmount11 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 170);
  	public FixedLengthStringData prmtols = new FixedLengthStringData(44).isAPartOf(t5667Rec, 209);
  	public ZonedDecimalData[] prmtol = ZDArrayPartOfStructure(11, 4, 2, prmtols, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(prmtols, 0, FILLER_REDEFINE);
  	public ZonedDecimalData prmtol01 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData prmtol02 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 4);
  	public ZonedDecimalData prmtol03 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 8);
  	public ZonedDecimalData prmtol04 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 12);
  	public ZonedDecimalData prmtol05 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 16);
  	public ZonedDecimalData prmtol06 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 20);
  	public ZonedDecimalData prmtol07 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 24);
  	public ZonedDecimalData prmtol08 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 28);
  	public ZonedDecimalData prmtol09 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 32);
  	public ZonedDecimalData prmtol10 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 36);
  	public ZonedDecimalData prmtol11 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 40);
  	public FixedLengthStringData maxamts = new FixedLengthStringData(187).isAPartOf(t5667Rec, 253);
  	public ZonedDecimalData[] maxamt = ZDArrayPartOfStructure(11, 17, 2, maxamts, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(187).isAPartOf(maxamts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxamt01 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData maxamt02 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 17);
  	public ZonedDecimalData maxamt03 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 34);
  	public ZonedDecimalData maxamt04 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 51);
  	public ZonedDecimalData maxamt05 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 68);
  	public ZonedDecimalData maxamt06 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 85);
  	public ZonedDecimalData maxamt07 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 102);
  	public ZonedDecimalData maxamt08 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 119);
  	public ZonedDecimalData maxamt09 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 136);
  	public ZonedDecimalData maxamt10 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 153);
  	public ZonedDecimalData maxamt11 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 170);
  	public FixedLengthStringData prmtolns = new FixedLengthStringData(44).isAPartOf(t5667Rec, 440);
  	public ZonedDecimalData[] prmtoln = ZDArrayPartOfStructure(11, 4, 2, prmtolns, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(44).isAPartOf(prmtolns, 0, FILLER_REDEFINE);
  	public ZonedDecimalData prmtoln01 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 0);
  	public ZonedDecimalData prmtoln02 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 4);
  	public ZonedDecimalData prmtoln03 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 8);
  	public ZonedDecimalData prmtoln04 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 12);
  	public ZonedDecimalData prmtoln05 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 16);
  	public ZonedDecimalData prmtoln06 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 20);
  	public ZonedDecimalData prmtoln07 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 24);
  	public ZonedDecimalData prmtoln08 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 28);
  	public ZonedDecimalData prmtoln09 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 32);
  	public ZonedDecimalData prmtoln10 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 36);
  	public ZonedDecimalData prmtoln11 = new ZonedDecimalData(4, 2).isAPartOf(filler4, 40);
  	public FixedLengthStringData sfind = new FixedLengthStringData(1).isAPartOf(t5667Rec, 484);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(15).isAPartOf(t5667Rec, 485, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5667Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5667Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}