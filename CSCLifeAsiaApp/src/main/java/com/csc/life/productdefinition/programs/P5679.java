/*
 * File: P5679.java
 * Date: 30 August 2009 0:34:01
 * Author: Quipoz Limited
 * 
 * Class transformed from P5679.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.S5679ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.P1690up;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.P1690rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.csc.util.SmartTableUtility;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
 *
 *
 *****************************************************************
 * </pre>
 */
public class P5679 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5679");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	/* ERRORS */
	private String e031 = "E031";
	/* FORMATS */
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
	/* Logical File: Extra data screen */
	private DescTableDAM descIO = new DescTableDAM();
	/* Logical File: SMART table reference data */
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5679ScreenVars sv = ScreenProgram.getScreenVars(S5679ScreenVars.class);

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, cont1190, preExit, exit2090, exit3900, exit5090,generalArea1045
	}
	
	

	public P5679() {
		super();
		screenVars = sv;
		new ScreenModel("S5679", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1001();
					endPara1230();
				}
				case cont1190: {
					cont1190();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		
	}
	
	
	protected void generalArea1045()
	{
		sv.contitem.set(t5679rec.contitem);
		
	}

	protected void endPara1230()
	{
		sv.contitem.set(t5679rec.contitem);
		
	}

	protected void initialise1001() {
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setDataArea(SPACES);
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			itemIO.setStatuz(e031);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(), SPACES)) {
			goTo(GotoLabel.cont1190);
		}
	}

	protected void cont1190() {
		sv.cnPremStats.set(t5679rec.cnPremStats);
		sv.cnRiskStats.set(t5679rec.cnRiskStats);
		sv.covPremStats.set(t5679rec.covPremStats);
		sv.covRiskStats.set(t5679rec.covRiskStats);
		sv.jlifeStats.set(t5679rec.jlifeStats);
		sv.lifeStats.set(t5679rec.lifeStats);
		sv.ridPremStats.set(t5679rec.ridPremStats);
		sv.ridRiskStats.set(t5679rec.ridRiskStats);
		sv.setCnPremStat.set(t5679rec.setCnPremStat);
		sv.setCnRiskStat.set(t5679rec.setCnRiskStat);
		sv.setCovPremStat.set(t5679rec.setCovPremStat);
		sv.setCovRiskStat.set(t5679rec.setCovRiskStat);
		sv.setJlifeStat.set(t5679rec.setJlifeStat);
		sv.setLifeStat.set(t5679rec.setLifeStat);
		sv.setRidPremStat.set(t5679rec.setRidPremStat);
		sv.setRidRiskStat.set(t5679rec.setRidRiskStat);
		sv.setSngpCnStat.set(t5679rec.setSngpCnStat);
		sv.setSngpCovStat.set(t5679rec.setSngpCovStat);
		sv.setSngpRidStat.set(t5679rec.setSngpRidStat);
	}

	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

	protected void screenEdit2000() {
		try {
			screenIo2001();
		} catch (GOTOException e) {
		}
	}

	protected void screenIo2001() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		} else {
			wsspcomn.edterror.set(varcom.oK);
		}
		/* VALIDATE */
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
	}

	protected void update3000() {
		try {
			loadWsspFields3100();
			//endPara3250();
		} catch (GOTOException e) {
		}
	}



	protected void loadWsspFields3100() {
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3900);
		}
		updateRec5000();
		/* COMMIT */
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}

	protected void updateRec5000() {
		try {
			// IJTI-327 STARTS
			// para5000();
			compareFields5020();
			// writeRecord5080();
			// IJTI-327 ENDS
		} catch (GOTOException e) {
		}
	}

	protected void para5000() {
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		/* READ-RECORD */
		callItemio5100();
	}

	protected void compareFields5020() {
		wsaaUpdateFlag = "N";
		checkChanges5300();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.exit5090);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
		// IJTI-327 STARTS
		// itemIO.setGenarea(t5679rec.t5679Rec);
		Itempf itempf = SmartTableUtility.getItempfByItemTableDAM(itemIO);
		itempf.setGenarea(t5679rec.t5679Rec.toString().getBytes());
		itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
				.getGENAREAJString(t5679rec.t5679Rec.toString().getBytes(), t5679rec));
		itemDAO.updateSmartTableItem(itempf);
		// IJTI-327 ENDS
	}
	

	protected void writeRecord5080() {
		itemIO.setFunction(varcom.rewrt);
		callItemio5100();
	}

	protected void callItemio5100() {
		/* PARA */
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(itemIO.getParams());
			fatalError600();
		}
		/* EXIT */
	}

	protected void checkChanges5300() {
		para5300();
	}

	protected void para5300() {
		
		
		if (isNE(sv.contitem,t5679rec.contitem)) {
			t5679rec.contitem.set(sv.contitem);
			wsaaUpdateFlag = "Y";
		}
		
		if (isNE(sv.cnPremStats, t5679rec.cnPremStats)) {
			t5679rec.cnPremStats.set(sv.cnPremStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cnRiskStats, t5679rec.cnRiskStats)) {
			t5679rec.cnRiskStats.set(sv.cnRiskStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.covPremStats, t5679rec.covPremStats)) {
			t5679rec.covPremStats.set(sv.covPremStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.covRiskStats, t5679rec.covRiskStats)) {
			t5679rec.covRiskStats.set(sv.covRiskStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.jlifeStats, t5679rec.jlifeStats)) {
			t5679rec.jlifeStats.set(sv.jlifeStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lifeStats, t5679rec.lifeStats)) {
			t5679rec.lifeStats.set(sv.lifeStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ridPremStats, t5679rec.ridPremStats)) {
			t5679rec.ridPremStats.set(sv.ridPremStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ridRiskStats, t5679rec.ridRiskStats)) {
			t5679rec.ridRiskStats.set(sv.ridRiskStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCnPremStat, t5679rec.setCnPremStat)) {
			t5679rec.setCnPremStat.set(sv.setCnPremStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCnRiskStat, t5679rec.setCnRiskStat)) {
			t5679rec.setCnRiskStat.set(sv.setCnRiskStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCovPremStat, t5679rec.setCovPremStat)) {
			t5679rec.setCovPremStat.set(sv.setCovPremStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCovRiskStat, t5679rec.setCovRiskStat)) {
			t5679rec.setCovRiskStat.set(sv.setCovRiskStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setJlifeStat, t5679rec.setJlifeStat)) {
			t5679rec.setJlifeStat.set(sv.setJlifeStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setLifeStat, t5679rec.setLifeStat)) {
			t5679rec.setLifeStat.set(sv.setLifeStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setRidPremStat, t5679rec.setRidPremStat)) {
			t5679rec.setRidPremStat.set(sv.setRidPremStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setRidRiskStat, t5679rec.setRidRiskStat)) {
			t5679rec.setRidRiskStat.set(sv.setRidRiskStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setSngpCnStat, t5679rec.setSngpCnStat)) {
			t5679rec.setSngpCnStat.set(sv.setSngpCnStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setSngpCovStat, t5679rec.setSngpCovStat)) {
			t5679rec.setSngpCovStat.set(sv.setSngpCovStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setSngpRidStat, t5679rec.setSngpRidStat)) {
			t5679rec.setSngpRidStat.set(sv.setSngpRidStat);
			wsaaUpdateFlag = "Y";
		}
	}
}
