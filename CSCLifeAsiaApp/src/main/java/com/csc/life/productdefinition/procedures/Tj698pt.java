/*
 * File: Tj698pt.java
 * Date: 30 August 2009 2:38:13
 * Author: Quipoz Limited
 * 
 * Class transformed from TJ698PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tj698rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TJ698.
*
*
*****************************************************************
* </pre>
*/
public class Tj698pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Rebate Based on Frequency                   SJ698");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(48);
	private FixedLengthStringData filler9 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine004, 23, FILLER).init("Frequency          Rebate");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(48);
	private FixedLengthStringData filler11 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 26);
	private FixedLengthStringData filler12 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine005, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(48);
	private FixedLengthStringData filler13 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 26);
	private FixedLengthStringData filler14 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine006, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(48);
	private FixedLengthStringData filler15 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 26);
	private FixedLengthStringData filler16 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine007, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(48);
	private FixedLengthStringData filler17 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 26);
	private FixedLengthStringData filler18 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine008, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(48);
	private FixedLengthStringData filler19 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 26);
	private FixedLengthStringData filler20 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine009, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(48);
	private FixedLengthStringData filler21 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 26);
	private FixedLengthStringData filler22 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine010, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(48);
	private FixedLengthStringData filler23 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 26);
	private FixedLengthStringData filler24 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine011, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(48);
	private FixedLengthStringData filler25 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 26);
	private FixedLengthStringData filler26 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine012, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(48);
	private FixedLengthStringData filler27 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 26);
	private FixedLengthStringData filler28 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine013, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(48);
	private FixedLengthStringData filler29 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 26);
	private FixedLengthStringData filler30 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine014, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(48);
	private FixedLengthStringData filler31 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 26);
	private FixedLengthStringData filler32 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine015, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine015, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(48);
	private FixedLengthStringData filler33 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 26);
	private FixedLengthStringData filler34 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine016, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine016, 41).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(28);
	private FixedLengthStringData filler35 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tj698rec tj698rec = new Tj698rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tj698pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tj698rec.tj698Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tj698rec.freqcy01);
		fieldNo009.set(tj698rec.freqcy02);
		fieldNo011.set(tj698rec.freqcy03);
		fieldNo015.set(tj698rec.freqcy05);
		fieldNo010.set(tj698rec.zlfact02);
		fieldNo008.set(tj698rec.zlfact01);
		fieldNo013.set(tj698rec.freqcy04);
		fieldNo019.set(tj698rec.freqcy07);
		fieldNo012.set(tj698rec.zlfact03);
		fieldNo014.set(tj698rec.zlfact04);
		fieldNo016.set(tj698rec.zlfact05);
		fieldNo018.set(tj698rec.zlfact06);
		fieldNo017.set(tj698rec.freqcy06);
		fieldNo021.set(tj698rec.freqcy08);
		fieldNo023.set(tj698rec.freqcy09);
		fieldNo020.set(tj698rec.zlfact07);
		fieldNo022.set(tj698rec.zlfact08);
		fieldNo024.set(tj698rec.zlfact09);
		fieldNo025.set(tj698rec.freqcy10);
		fieldNo027.set(tj698rec.freqcy11);
		fieldNo029.set(tj698rec.freqcy12);
		fieldNo026.set(tj698rec.zlfact10);
		fieldNo028.set(tj698rec.zlfact11);
		fieldNo030.set(tj698rec.zlfact12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
