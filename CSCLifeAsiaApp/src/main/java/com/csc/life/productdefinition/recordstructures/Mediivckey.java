package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:43
 * Description:
 * Copybook name: MEDIIVCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mediivckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mediivcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mediivcKey = new FixedLengthStringData(256).isAPartOf(mediivcFileKey, 0, REDEFINE);
  	public FixedLengthStringData mediivcChdrcoy = new FixedLengthStringData(1).isAPartOf(mediivcKey, 0);
  	public FixedLengthStringData mediivcInvref = new FixedLengthStringData(15).isAPartOf(mediivcKey, 1);
  	public FixedLengthStringData mediivcChdrnum = new FixedLengthStringData(8).isAPartOf(mediivcKey, 16);
  	public PackedDecimalData mediivcSeqno = new PackedDecimalData(2, 0).isAPartOf(mediivcKey, 24);
  	public FixedLengthStringData filler = new FixedLengthStringData(230).isAPartOf(mediivcKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mediivcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mediivcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}