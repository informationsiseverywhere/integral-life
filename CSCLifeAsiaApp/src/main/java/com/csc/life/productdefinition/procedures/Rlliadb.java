/*
 * File: Rlliadb.java
 * Date: 30 August 2009 2:13:02
 * Author: Quipoz Limited
 *
 * Class transformed from RLLIADB.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovttrmTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.life.productdefinition.recordstructures.Rlliadbrec;
import com.csc.life.productdefinition.tablestructures.T5651rec;
import com.csc.life.productdefinition.tablestructures.Th602rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This subroutine is called by various online transactions
*  to update the LIA and Hospitalisation Details in MLIA file.
*
*  Logic:
*  . Read TH602 to determine if updating to LIA is required
*
*  . Set up LIA record with information from risk flag from TH602
*    and company representation code from TH605.
*
*****************************************************************
* </pre>
*/
public class Rlliadb extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("RLLIADB");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT5657Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5657Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5657Key, 0);
	private FixedLengthStringData wsaaT5657Opcda = new FixedLengthStringData(2).isAPartOf(wsaaT5657Key, 4);
	private String wsaaFound = "";
	private String wsaaWrite = "";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaOpcdas = new FixedLengthStringData(9);
	private FixedLengthStringData[] wsaaOpcda = FLSArrayPartOfStructure(3, 3, wsaaOpcdas, 0);

	private FixedLengthStringData wsaaOpcdaArea = new FixedLengthStringData(9).isAPartOf(wsaaOpcdas, 0, REDEFINE);
	private FixedLengthStringData wsaaOpcda01 = new FixedLengthStringData(3).isAPartOf(wsaaOpcdaArea, 0);
	private FixedLengthStringData wsaaOpcda02 = new FixedLengthStringData(3).isAPartOf(wsaaOpcdaArea, 3);
	private FixedLengthStringData wsaaOpcda03 = new FixedLengthStringData(3).isAPartOf(wsaaOpcdaArea, 6);
	private FixedLengthStringData wsaaWholeValue = new FixedLengthStringData(1000);

	private FixedLengthStringData filler = new FixedLengthStringData(994).isAPartOf(wsaaWholeValue, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaCompany = new ZonedDecimalData(2, 0).isAPartOf(filler, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
		/* FORMATS */
	private String chdrlnbrec = "CHDRLNBREC";
	private String mliarec = "MLIAREC";
	private String itemrec = "ITEMREC";
	private String t5657 = "T5657";
	private String th602 = "TH602";
	private String th605 = "TH605";
	private String t5651 = "T5651";
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage transactions - term*/
	private CovttrmTableDAM covttrmIO = new CovttrmTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Contract Additional Details*/
	private HpadTableDAM hpadIO = new HpadTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Substd & Hosp view by IC no. and Contrac*/
	private MliaTableDAM mliaIO = new MliaTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Rlliadbrec rlliadbrec = new Rlliadbrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5651rec t5651rec = new T5651rec();
	private T5657rec t5657rec = new T5657rec();
	private Th602rec th602rec = new Th602rec();
	private Th605rec th605rec = new Th605rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr280,
		exit290,
		exit390,
		exit6000
	}

	public Rlliadb() {
		super();
	}

public void mainline(Object... parmArray)
	{
		rlliadbrec.rlliaRec = convertAndSetParam(rlliadbrec.rlliaRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main100()
	{
		main110();
		exitProgram190();
	}

protected void main110()
	{
		rlliadbrec.statuz.set(varcom.oK);
		wsaaOpcda01.set(SPACES);
		wsaaOpcda02.set(SPACES);
		wsaaOpcda03.set(SPACES);
		wsaaCount.set(ZERO);
		wsaaIndex.set(ZERO);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rlliadbrec.chdrcoy);
		itemIO.setItemtabl(th602);
		itemIO.setItemitem(rlliadbrec.batctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError6000();
		}
		th602rec.th602Rec.set(itemIO.getGenarea());
		chdrlnbIO.setChdrcoy(rlliadbrec.chdrcoy);
		chdrlnbIO.setChdrnum(rlliadbrec.chdrnum);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError6000();
		}
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife(SPACES);
		lifelnbIO.setJlife(SPACES);
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(lifelnbIO.getStatuz(),varcom.endp))) {
			readLifelnb200();
		}

	}

protected void exitProgram190()
	{
		exitProgram();
	}

protected void readLifelnb200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start200();
				}
				case nextr280: {
					nextr280();
				}
				case exit290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start200()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError6000();
		}
		if (isNE(lifelnbIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(),chdrlnbIO.getChdrnum())) {
			lifelnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit290);
		}
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(rlliadbrec.fsucoy);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError6000();
		}
		if (isEQ(cltsIO.getSecuityno(),SPACES)) {
			goTo(GotoLabel.nextr280);
		}
		mliaIO.setSecurityno(cltsIO.getSecuityno());
		mliaIO.setMlentity(chdrlnbIO.getChdrnum());
		mliaIO.setFormat(mliarec);
		mliaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),varcom.oK)
		&& isNE(mliaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError6000();
		}
		wsaaWrite = "N";
		wsaaCount.set(ZERO);
		lextIO.setStatuz(varcom.oK);
		lextIO.setChdrcoy(lifelnbIO.getChdrcoy());
		lextIO.setChdrnum(lifelnbIO.getChdrnum());
		lextIO.setLife(lifelnbIO.getLife());
		lextIO.setCoverage(SPACES);
		lextIO.setRider(SPACES);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp)
		|| isGTE(wsaaCount,3))) {
			readLext300();
		}

		if (isEQ(th602rec.mind,"Y")) {
			if (isEQ(wsaaWrite,"Y")) {
				updateMlia400();
			}
		}
		else {
			updateMlia400();
		}
	}

protected void nextr280()
	{
		lifelnbIO.setFunction(varcom.nextr);
	}

protected void readLext300()
	{
		try {
			start300();
		}
		catch (GOTOException e){
		}
	}

protected void start300()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lextIO.getStatuz());
			syserrrec.params.set(lextIO.getParams());
			fatalError6000();
		}
		if (isNE(lextIO.getChdrcoy(),lifelnbIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),lifelnbIO.getChdrnum())
		|| isNE(lextIO.getLife(),lifelnbIO.getLife())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit390);
		}
		covttrmIO.setChdrcoy(lextIO.getChdrcoy());
		covttrmIO.setChdrnum(lextIO.getChdrnum());
		covttrmIO.setLife(lextIO.getLife());
		covttrmIO.setCoverage(lextIO.getCoverage());
		covttrmIO.setRider(lextIO.getRider());
		covttrmIO.setSeqnbr(lextIO.getSeqnbr());
		covttrmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(),varcom.oK)
		&& isNE(covttrmIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(covttrmIO.getStatuz());
			syserrrec.params.set(covttrmIO.getParams());
			fatalError6000();
		}
		
		//ILIFE-5872
		if (isEQ(covttrmIO.getStatuz(),varcom.mrnf)) {
            lextIO.setFunction(varcom.nextr);
            return;
		}
		//ILIFE-5872
		
		wsaaT5657Crtable.set(covttrmIO.getCrtable());
		wsaaT5657Opcda.set(lextIO.getOpcda());
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(t5657);
		itemIO.setItemitem(wsaaT5657Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError6000();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			wsaaT5657Crtable.set("****");
			itemIO.setParams(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
			itemIO.setItemtabl(t5657);
			itemIO.setItemitem(wsaaT5657Key);
			itemIO.setFormat(itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError6000();
			}
			t5657rec.t5657Rec.set(itemIO.getGenarea());
		}
		else {
			t5657rec.t5657Rec.set(itemIO.getGenarea());
		}
		if (isEQ(t5657rec.sbstdl,"Y")) {
			wsaaFound = "N";
			itemIO.setParams(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
			itemIO.setItemtabl(t5651);
			itemIO.setItemitem(lextIO.getOpcda());
			itemIO.setFormat(itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
			&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError6000();
			}
			t5651rec.t5651Rec.set(itemIO.getGenarea());
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,3)
			|| isEQ(wsaaFound,"Y")); wsaaIndex.add(1)){
				if (isNE(t5651rec.mlmedcde,SPACES)) {
					if (isEQ(t5651rec.mlmedcde,wsaaOpcda[wsaaIndex.toInt()])) {
						wsaaFound = "Y";
					}
				}
			}
			if (isNE(wsaaFound,"Y")) {
				wsaaCount.add(1);
				wsaaOpcda[wsaaCount.toInt()].set(t5651rec.mlmedcde);
				wsaaWrite = "Y";
			}
		}
		lextIO.setFunction(varcom.nextr);
	}

protected void updateMlia400()
	{
		start400();
	}

protected void start400()
	{
		getLiacoy410();
		mliaIO.setMlcoycde(wsaaCompany);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)
		&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError6000();
		}
		if (isEQ(hpadIO.getStatuz(),varcom.oK)) {
			mliaIO.setHpropdte(hpadIO.getHpropdte());
		}
		else {
			mliaIO.setHpropdte(varcom.vrcmMaxDate);
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(rlliadbrec.fsucoy);
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.language.set(rlliadbrec.language);
		namadrsrec.function.set("LGNMS");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError6000();
		}
		mliaIO.setClntnaml(namadrsrec.name);
		mliaIO.setRskflg(th602rec.rskflg);
		mliaIO.setSecurityno(cltsIO.getSecuityno());
		mliaIO.setDob(cltsIO.getCltdob());
		mliaIO.setMlentity(chdrlnbIO.getChdrnum());
		mliaIO.setMloldic(SPACES);
		mliaIO.setMlothic(SPACES);
		mliaIO.setMlhsperd(ZERO);
		mliaIO.setMlmedcde01(wsaaOpcda01);
		mliaIO.setMlmedcde02(wsaaOpcda02);
		mliaIO.setMlmedcde03(wsaaOpcda03);
		mliaIO.setIndc("Y");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		mliaIO.setEffdate(wsaaToday);
		if (isNE(mliaIO.getStatuz(),varcom.oK)
		|| isEQ(mliaIO.getActn(),"3")) {
			mliaIO.setFunction(varcom.updat);
			mliaIO.setMind("N");
			mliaIO.setActn("1");
		}
		else {
			mliaIO.setFunction(varcom.rewrt);
			if (isEQ(mliaIO.getMind(),"Y")) {
				mliaIO.setActn("2");
			}
			mliaIO.setMind("N");
		}
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),varcom.oK)
		&& isNE(mliaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError6000();
		}
	}

protected void getLiacoy410()
	{
		read411();
	}

protected void read411()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rlliadbrec.chdrcoy);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(rlliadbrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError6000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		wsaaCompany.set(th605rec.liacoy);
	}

protected void fatalError6000()
	{
		try {
			fatalErrors6000();
		}
		catch (GOTOException e){
		}
		finally{
			exit6000();
		}
	}

protected void fatalErrors6000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit6000);
		}
		rlliadbrec.statuz.set(syserrrec.statuz);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit6000()
	{
		exitProgram();
	}
}
