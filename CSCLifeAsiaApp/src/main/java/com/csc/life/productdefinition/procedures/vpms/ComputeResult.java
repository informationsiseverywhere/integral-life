/**
 * 
 */
package com.csc.life.productdefinition.procedures.vpms;

import java.io.Serializable;

/**
 * @author sle3
 *
 */
public class ComputeResult implements Serializable {
	private String name;
	private String value;
	private String message;
	private String referenceField;
	private String returnCode;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2531344159854206614L;

	/**
	 * 
	 */
	public ComputeResult() {
		super();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setReferenceField(String referenceField) {
		this.referenceField = referenceField;
	}

	public String getReferenceField() {
		return referenceField;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnCode() {
		return returnCode;
	}
}
