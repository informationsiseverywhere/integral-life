package com.csc.life.productdefinition.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR51Q.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr51qReport extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private ZonedDecimalData dura = new ZonedDecimalData(4, 0);
	private ZonedDecimalData origamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData resndesc = new FixedLengthStringData(50);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData zcurprmbal = new ZonedDecimalData(17, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr51qReport() {
		super();
	}


	/**
	 * Print the XML for Rr51qd01
	 */
	public void printRr51qd01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 9, 3));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 12, 3));
		origamt.setFieldName("origamt");
		origamt.setInternal(subString(recordData, 15, 17));
		dura.setFieldName("dura");
		dura.setInternal(subString(recordData, 32, 4));
		zcurprmbal.setFieldName("zcurprmbal");
		zcurprmbal.setInternal(subString(recordData, 36, 17));
		resndesc.setFieldName("resndesc");
		resndesc.setInternal(subString(recordData, 53, 50));
		printLayout("Rr51qd01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				cnttype,
				cntcurr,
				origamt,
				dura,
				zcurprmbal,
				resndesc
			}
		);

	}

	/**
	 * Print the XML for Rr51qh01
	 */
	public void printRr51qh01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 42, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 52, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 54, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr51qh01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(11);
	}


}
