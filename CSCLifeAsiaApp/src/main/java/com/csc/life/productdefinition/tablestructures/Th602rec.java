package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:32
 * Description:
 * Copybook name: TH602REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th602rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th602Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData mind = new FixedLengthStringData(2).isAPartOf(th602Rec, 0);
  	public FixedLengthStringData rskflg = new FixedLengthStringData(2).isAPartOf(th602Rec, 2);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(th602Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th602Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th602Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}