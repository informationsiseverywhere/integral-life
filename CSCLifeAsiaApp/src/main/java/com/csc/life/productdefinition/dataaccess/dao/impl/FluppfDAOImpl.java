package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.FluupfDAO;
import com.csc.life.productdefinition.dataaccess.model.Fluupf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class FluppfDAOImpl extends BaseDAOImpl<Fluupf> implements FluupfDAO {
	

	private static final Logger LOGGER = LoggerFactory.getLogger(FluppfDAOImpl.class);	
	private static final String getDateQuery = "select CHDRCOY, CHDRNUM, FUPNO, TRANNO, FUPTYP, LIFE, JLIFE, FUPCDE, FUPSTS, FUPDT, UNIQUE_NUMBER from vm1dta.FLUPPF  where FUPSTS = ? OR FUPSTS = ? AND CHDRCOY = ? ORDER BY CHDRCOY, CHDRNUM, LIFE";
 	   	
 
	
	public boolean updateByFluppfID(Fluupf obj)
	{		
		StringBuilder sb = new StringBuilder("");
		sb.append(" UPDATE VM1DTA.FLUPPF  ");
		sb.append(" SET ");
		sb.append(" FUPSTS=?   ");
		sb.append(" WHERE UNIQUE_NUMBER=? AND CHDRNUM=? ");
		
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		boolean isUpdated = false;
		try{
				ps = getPrepareStatement(sb.toString());	
				ps.setString(1, obj.getFupsts() );
             	ps.setLong(2, obj.getUniqueNumber());
             	ps.setString(3, obj.getChdrnum());
			    ps.addBatch();	
			 	
			ps.executeBatch();
			isUpdated = true; 
			//IJTI-851-Overly Broad Catch
		}catch ( SQLException e) 
		{
			LOGGER.error("updateMedipf()", e);//IJTI-1561
		} finally 
		{
			close(ps, null);			
		}		
		return isUpdated;
	}

	
	public List<Fluupf> loadDataByBatch(int batchID) {
		List<Fluupf> list = new ArrayList<Fluupf>();
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM FLUPPFDATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		 
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				Fluupf zw=new Fluupf();		
			 	zw.setChdrnum(rs.getString("CHDRNUM")); 
				zw.setChdrcoy(rs.getString("CHDRCOY"));
				zw.setTranno(rs.getLong("TRANNO"));
				zw.setFupno(rs.getString("FUPNO"));
				zw.setFuptyp(rs.getString("FUPTYP"));
				zw.setLife(rs.getString("LIFE"));
				zw.setJlife(rs.getString("JLIFE"));
				zw.setFupcde(rs.getString("FUPCDE"));
				zw.setFupsts(rs.getString("FUPSTS"));
				zw.setFupdt(rs.getLong("FUPDT"));
				zw.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				//zw.setUsrprf(rs.getString("USRPRF"));
				//zw.setJobnm(rs.getString("JOBNM"));
			 	list.add(zw);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return list;
	}	
	
	public int populateFluppfTemp(String company, int batchExtractSize, String Fstatus1, String Fstatus2) {
		initializeFluupfTemp();
		int rows = 0;
	 	StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.FLUPPFDATA "); //ILB-475
		sb.append("(BATCHID,CHDRCOY, CHDRNUM, FUPNO, TRANNO, FUPTYP, LIFE, JLIFE, FUPCDE, FUPSTS, FUPDT, UNIQUE_NUMBER)");
		sb.append("SELECT ");
		sb.append("floor((row_number() over (ORDER BY FLUPPF.CHDRNUM ASC)-1)/?) BATCHID,CHDRCOY, CHDRNUM, FUPNO, TRANNO, FUPTYP, LIFE, JLIFE, FUPCDE, FUPSTS, FUPDT, UNIQUE_NUMBER ");
		sb.append("FROM ");
		sb.append("VM1DTA.FLUPPF ");
		sb.append("WHERE  CHDRCOY=? ");
		sb.append("AND FUPSTS=? ");
		sb.append("OR  FUPSTS=? ");
		 
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;			
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, company);
			ps.setString(3, Fstatus1);
			ps.setString(4, Fstatus2);
			rows = ps.executeUpdate();	

		}catch (SQLException e) {
			LOGGER.error("populateBr644Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeFluupfTemp()
	 {
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append(" DELETE FROM FLUPPFDATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try
		{
			ps = getPrepareStatement(sb.toString());
			rows = ps.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("FLUPPFTEMP()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} 
		finally
		{
			close(ps, rs);			
		}		
	}	
	
	public void updateFlupRecord(List<Fluupf> fluplList){
		StringBuilder sb = new StringBuilder(" UPDATE VM1DTA.FLUPPF  ");
		sb.append(" SET ");
		sb.append(" FUPSTS=?   ");
		sb.append(" WHERE UNIQUE_NUMBER=? AND CHDRNUM=? ");
		
		PreparedStatement ps = null;
		try{
			for (Fluupf obj : fluplList) {
				ps = getPrepareStatement(sb.toString());	
				ps.setString(1, obj.getFupsts() );
             	ps.setLong(2, obj.getUniqueNumber());
             	ps.setString(3, obj.getChdrnum());
			    ps.addBatch();	
			} 
			if(ps!=null)
				ps.executeBatch();
			 
		}catch ( SQLException e) 
		{
			LOGGER.error("updateFlupRecord()",e);
		} finally 
		{
			close(ps, null);			
		}
	}
}
