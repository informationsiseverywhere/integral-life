/*
 * File: Pr636.java
 * Date: 30 August 2009 1:50:19
 * Author: Quipoz Limited
 * 
 * Class transformed from PR636.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.productdefinition.dataaccess.ZmpdTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpfTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvnamTableDAM;
import com.csc.life.productdefinition.screens.Sr636ScreenVars;
import com.csc.smart.recordstructures.Nxtprogrec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    This program is displayed when a Medical Provider Window is
*    the requested. The Screen shows the Medical Provider that
*    can be scrolled by using the Page Up/Down keys.
*
*    The list of medical provider can be sorted in two ways; by
*    Provider Code and by Name. 'Starting' field can be used to
*    search for the particular Medical Provider code or name.
*
***********************************************************************
* </pre>
*/
public class Pr636 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR636");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(3, 0);
	private String wsaaEof = "";
	private String wsaaMatch = "";
	private String wsaaLoadFunction = "";

	private FixedLengthStringData wsaaLoadBy = new FixedLengthStringData(1);
	private Validator validLoadOption = new Validator(wsaaLoadBy, "1","2");
	private Validator loadByCode = new Validator(wsaaLoadBy, "1");
	private Validator loadByName = new Validator(wsaaLoadBy, "2");
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String s003 = "S003";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private ZmpdTableDAM zmpdIO = new ZmpdTableDAM();
	private ZmpfTableDAM zmpfIO = new ZmpfTableDAM();
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private ZmpvnamTableDAM zmpvnamIO = new ZmpvnamTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Nxtprogrec nxtprogrec = new Nxtprogrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Sr636ScreenVars sv = getLScreenVars();

	protected Sr636ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars( Sr636ScreenVars.class);
	}
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nxtpGoingUp, 
		nxtpEnd, 
		nxtpExit, 
		exit2090, 
		readSubfile2120, 
		subfileUpdate2170, 
		exit2190, 
		readSubfile3120, 
		nextSubfile3180, 
		exit3190, 
		loading5010, 
		loadToSubfile5060, 
		loadNextRec5080, 
		exit5090, 
		readZmpf5620, 
		exit5690, 
		readZmpd5720, 
		exit5790
	}

	public Pr636() {
		super();
		screenVars = sv;
		new ScreenModel("Sr636", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void nxtprog()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					nxtpMainline();
					nxtpGoingDown();
				case nxtpGoingUp: 
					nxtpGoingUp();
				case nxtpEnd: 
					nxtpEnd();
				case nxtpExit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nxtpMainline()
	{
		nxtprogrec.statuz.set("****");
		if (isEQ(wsspcomn.submenu, "P0075")) {
			wsspcomn.programPtr.add(1);
			wsspcomn.nextprog.set(wsaaProg);
			goTo(GotoLabel.nxtpExit);
		}
		if (isEQ(wsspcomn.nextprog, scrnparams.scrname)) {
			wsspcomn.lastprog.set(wsaaProg);
		}
		else {
			wsspcomn.lastprog.set(wsspcomn.nextprog);
		}
		wsspcomn.nextprog.set(SPACES);
		if (isEQ(nxtprogrec.function, "U")) {
			goTo(GotoLabel.nxtpGoingUp);
		}
	}

protected void nxtpGoingDown()
	{
		if (isEQ(wsspcomn.lastprog, wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next4prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next1prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.submenu)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpGoingUp()
	{
		if (isEQ(wsspcomn.lastprog, wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next4prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpEnd()
	{
		if (isEQ(wsspcomn.nextprog, SPACES)
		&& isNE(wsspcomn.flag, "W")) {
			wsspcomn.nextprog.set(wsspcomn.submenu);
		}
	}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.sel01.set("1");
		sv.sel02.set("1");
		sv.search01.set(wsspwindow.value);
		sv.search02.set(wsspwindow.value);
		wsspwindow.value.set(SPACES);
		wsaaLoadFunction = "BEGN";
		loadOnePage5000();
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			loadOnePage5000();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		wsaaLoadBy.set(sv.sel01);
		if (!validLoadOption.isTrue()) {
			sv.sel01Err.set(s003);
		}
		if (isEQ(sv.errorIndicators, SPACES)) {
			if (isNE(sv.sel01, sv.sel02)
			|| isNE(sv.search01, sv.search02)
			|| isNE(sv.zaracde01, sv.zaracde02)
			|| isNE(sv.zmedtyp01, sv.zmedtyp02)
			|| isNE(sv.sex01, sv.sex02)
			|| isNE(sv.zmedsts01, sv.zmedsts02)) {
				sv.sel02.set(sv.sel01);
				sv.search02.set(sv.search01);
				sv.zaracde02.set(sv.zaracde01);
				sv.zmedtyp02.set(sv.zmedtyp01);
				sv.sex02.set(sv.sex01);
				sv.zmedsts02.set(sv.zmedsts01);
				wsaaLoadFunction = "BEGN";
				loadOnePage5000();
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		validateSubfile2100();
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2110();
				case readSubfile2120: 
					readSubfile2120();
				case subfileUpdate2170: 
					subfileUpdate2170();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2110()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSubfile2120()
	{
		processScreen("SR636", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(sv.select, SPACES)) {
			goTo(GotoLabel.subfileUpdate2170);
		}
		if (isNE(sv.select, "1")) {
			sv.selectErr.set(e005);
		}
	}

protected void subfileUpdate2170()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR636", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*NEXT-SUBFILE*/
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSubfile2120);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		pickSelectedLine3100();
		/*EXIT*/
	}

protected void pickSelectedLine3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3110();
				case readSubfile3120: 
					readSubfile3120();
				case nextSubfile3180: 
					nextSubfile3180();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3110()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSubfile3120()
	{
		processScreen("SR636", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(sv.select, SPACES)) {
			goTo(GotoLabel.nextSubfile3180);
		}
		if (isEQ(sv.select, "1")) {
			wsspwindow.value.set(sv.zmedprv);
			/*      GO TO 3090-EXIT                                          */
			goTo(GotoLabel.exit3190);
		}
	}

protected void nextSubfile3180()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSubfile3120);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		scrnparams.function.set("HIDEW");
		processScreen("SR636", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		nxtprogrec.function.set("U");
		nxtprog();
		/*EXIT*/
	}

protected void loadOnePage5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case loading5010: 
					loading5010();
					filter5050();
				case loadToSubfile5060: 
					loadToSubfile5060();
				case loadNextRec5080: 
					loadNextRec5080();
				case exit5090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loading5010()
	{
		if (isEQ(wsaaLoadFunction, "BEGN")) {
			clearSubfile5100();
		}
		if (loadByCode.isTrue()) {
			readZmpv5200();
		}
		if (loadByName.isTrue()) {
			readZmpvnam5300();
		}
		if (isEQ(wsaaEof, "Y")) {
			if (isEQ(scrnparams.subfileRrn, ZERO)) {
				loadBlankLine5400();
			}
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit5090);
		}
	}

protected void filter5050()
	{
		if (isEQ(sv.zaracde01, SPACES)
		&& isEQ(sv.zmedtyp01, SPACES)
		&& isEQ(sv.sex01, SPACES)
		&& isEQ(sv.zmedsts01, SPACES)) {
			goTo(GotoLabel.loadToSubfile5060);
		}
		wsaaMatch = "Y";
		if (isNE(sv.zaracde01, SPACES)) {
			matchAreaCode5500();
			if (isEQ(wsaaMatch, "N")) {
				goTo(GotoLabel.loadNextRec5080);
			}
		}
		if (isNE(sv.zmedtyp01, SPACES)) {
			matchMedicalTypes5600();
			if (isEQ(wsaaMatch, "N")) {
				goTo(GotoLabel.loadNextRec5080);
			}
		}
		if (isNE(sv.sex01, SPACES)) {
			matchDoctorsSex5700();
			if (isEQ(wsaaMatch, "N")) {
				goTo(GotoLabel.loadNextRec5080);
			}
		}
		if (isNE(sv.zmedsts01, SPACES)) {
			matchStatus5800();
			if (isEQ(wsaaMatch, "N")) {
				goTo(GotoLabel.loadNextRec5080);
			}
		}
	}

protected void loadToSubfile5060()
	{
		initialize(sv.subfileFields);
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.zmedprv.set(zmpvIO.getZmedprv());
		sv.givname.set(zmpvIO.getZmednam());
		/* Get address.*/
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(zmpvIO.getZmednum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		sv.addrss.set(cltsIO.getCltaddr01());
		sv.selectOut[varcom.pr.toInt()].set("N");
		scrnparams.function.set(varcom.sadd);
		processScreen("SR636", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		compute(wsaaIy, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		wsaaIx.setRemainder(wsaaIy);
		if (isEQ(wsaaIx, ZERO)) {
			goTo(GotoLabel.exit5090);
		}
	}

protected void loadNextRec5080()
	{
		wsaaLoadFunction = "NEXT";
		goTo(GotoLabel.loading5010);
	}

protected void clearSubfile5100()
	{
		/*START*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR636", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaLoadBy.set(sv.sel01);
		wsaaEof = "N";
		scrnparams.subfileMore.set("Y");
		/*EXIT*/
	}

protected void readZmpv5200()
	{
		start5210();
		readZmpv5220();
	}

protected void start5210()
	{
		if (isEQ(wsaaLoadFunction, "BEGN")) {
			zmpvIO.setParams(SPACES);
			zmpvIO.setZmedcoy(wsspcomn.company);
			zmpvIO.setZmedprv(sv.search01);
			zmpvIO.setFunction(varcom.begn);
		}
		if (isEQ(wsaaLoadFunction, "NEXT")) {
			zmpvIO.setFunction(varcom.nextr);
		}
	}

protected void readZmpv5220()
	{
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(), varcom.oK)
		&& isNE(zmpvIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zmpvIO.getStatuz(), varcom.endp)
		|| isNE(zmpvIO.getZmedcoy(), wsspcomn.company)) {
			wsaaEof = "Y";
		}
		/*EXIT*/
	}

protected void readZmpvnam5300()
	{
		start5310();
		readZmpvnam5220();
	}

protected void start5310()
	{
		if (isEQ(wsaaLoadFunction, "BEGN")) {
			zmpvnamIO.setParams(SPACES);
			zmpvnamIO.setZmedcoy(wsspcomn.company);
			zmpvnamIO.setZmednam(sv.search01);
			zmpvnamIO.setFunction(varcom.begn);
		}
		if (isEQ(wsaaLoadFunction, "NEXT")) {
			zmpvnamIO.setFunction(varcom.nextr);
		}
	}

protected void readZmpvnam5220()
	{
		SmartFileCode.execute(appVars, zmpvnamIO);
		if (isNE(zmpvnamIO.getStatuz(), varcom.oK)
		&& isNE(zmpvnamIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zmpvnamIO.getParams());
			syserrrec.statuz.set(zmpvnamIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zmpvnamIO.getStatuz(), varcom.endp)
		|| isNE(zmpvnamIO.getZmedcoy(), wsspcomn.company)) {
			wsaaEof = "Y";
			return ;
		}
		zmpvIO.setZmedcoy(zmpvnamIO.getZmedcoy());
		zmpvIO.setZmedprv(zmpvnamIO.getZmedprv());
		zmpvIO.setZmednum(zmpvnamIO.getZmednum());
		zmpvIO.setZmednam(zmpvnamIO.getZmednam());
		zmpvIO.setZmedsts(zmpvnamIO.getZmedsts());
		zmpvIO.setDteapp(zmpvnamIO.getDteapp());
		zmpvIO.setDtetrm(zmpvnamIO.getDtetrm());
		zmpvIO.setZdesc(zmpvnamIO.getZdesc());
		zmpvIO.setPaymth(zmpvnamIO.getPaymth());
		zmpvIO.setBankkey(zmpvnamIO.getBankkey());
		zmpvIO.setBankacckey(zmpvnamIO.getBankacckey());
		zmpvIO.setZaracde(zmpvnamIO.getZaracde());
	}

protected void loadBlankLine5400()
	{
		/*START*/
		initialize(sv.subfileFields);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		scrnparams.function.set(varcom.sadd);
		processScreen("SR636", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void matchAreaCode5500()
	{
		/*START*/
		wsaaMatch = "N";
		if (isEQ(sv.zaracde01, zmpvIO.getZaracde())) {
			wsaaMatch = "Y";
		}
		/*EXIT*/
	}

protected void matchMedicalTypes5600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5610();
				case readZmpf5620: 
					readZmpf5620();
					nextZmpf5680();
				case exit5690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5610()
	{
		wsaaMatch = "N";
		zmpfIO.setParams(SPACES);
		zmpfIO.setZmedcoy(zmpvIO.getZmedcoy());
		zmpfIO.setZmedprv(zmpvIO.getZmedprv());
		zmpfIO.setSeqno(ZERO);
		zmpfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zmpfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zmpfIO.setFitKeysSearch("ZMEDCOY", "ZMEDPRV");
	}

protected void readZmpf5620()
	{
		SmartFileCode.execute(appVars, zmpfIO);
		if (isNE(zmpfIO.getStatuz(), varcom.oK)
		&& isNE(zmpfIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zmpfIO.getParams());
			syserrrec.statuz.set(zmpfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zmpfIO.getStatuz(), varcom.endp)
		|| isNE(zmpfIO.getZmedcoy(), zmpvIO.getZmedcoy())
		|| isNE(zmpfIO.getZmedprv(), zmpvIO.getZmedprv())) {
			goTo(GotoLabel.exit5690);
		}
		if (isEQ(zmpfIO.getZmedtyp(), sv.zmedtyp01)) {
			wsaaMatch = "Y";
			goTo(GotoLabel.exit5690);
		}
	}

protected void nextZmpf5680()
	{
		zmpfIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readZmpf5620);
	}

protected void matchDoctorsSex5700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5710();
				case readZmpd5720: 
					readZmpd5720();
					nextZmpd5780();
				case exit5790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5710()
	{
		wsaaMatch = "N";
		zmpdIO.setParams(SPACES);
		zmpdIO.setZmedcoy(zmpvIO.getZmedcoy());
		zmpdIO.setZmedprv(zmpvIO.getZmedprv());
		zmpdIO.setSeqno(ZERO);
		zmpdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zmpdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zmpdIO.setFitKeysSearch("ZMEDCOY", "ZMEDPRV");
	}

protected void readZmpd5720()
	{
		SmartFileCode.execute(appVars, zmpdIO);
		if (isNE(zmpdIO.getStatuz(), varcom.oK)
		&& isNE(zmpdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zmpdIO.getParams());
			syserrrec.statuz.set(zmpdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zmpdIO.getStatuz(), varcom.endp)
		|| isNE(zmpdIO.getZmedcoy(), zmpvIO.getZmedcoy())
		|| isNE(zmpdIO.getZmedprv(), zmpvIO.getZmedprv())) {
			goTo(GotoLabel.exit5790);
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(zmpdIO.getZdocnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(cltsIO.getCltsex(), sv.sex01)) {
			wsaaMatch = "Y";
			goTo(GotoLabel.exit5790);
		}
	}

protected void nextZmpd5780()
	{
		zmpdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readZmpd5720);
	}

protected void matchStatus5800()
	{
		/*START*/
		wsaaMatch = "N";
		if (isEQ(sv.zmedsts01, zmpvIO.getZmedsts())) {
			wsaaMatch = "Y";
		}
		/*EXIT*/
	}
}
