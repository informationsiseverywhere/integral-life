/*
 * File: Vpusurc.java
 * Date: 3 Oct 2012 22:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* VPMS - Full Surrender Clculation Update Routine
*
* VPMFMT Fields :
* VPMFMT-AMOUNT-01  Converted surrender value
* </pre>
*/
public class Vpusurc extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private String wsaaSubr = "VPUSURC";
	public FixedLengthStringData wsaaItemitem = new FixedLengthStringData(7).init(SPACES);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaTotEstVal = new ZonedDecimalData(17, 2);
	
	public FixedLengthStringData wsaaFirst = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirst, "Y");
	public FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq, "Y");
	
	private FixedLengthStringData wsaaConstants = new FixedLengthStringData(9);
	private FixedLengthStringData wsccC = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 0).init("C");
	private FixedLengthStringData wsccFee = new FixedLengthStringData(3).isAPartOf(wsaaConstants, 1).init("FEE");
	private FixedLengthStringData wsccJ = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 4).init("J");
	private FixedLengthStringData wsccL = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 5).init("L");
	private FixedLengthStringData wsccN = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 6).init("N");
	private FixedLengthStringData wsccT = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 7).init("T");
	private FixedLengthStringData wsccY = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 8).init("Y");
	
		/* FORMATS */
	private String descrec = "DESCREC";
	private String t5542 = "T5542";
	private String t5687 = "T5687";
	private String t6644 = "T6644";
		/* ERRORS */
	private String h141 = "H141";
		/* COPYBOOK */
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5542rec t5542rec = new T5542rec();
	private T5687rec t5687rec = new T5687rec();

	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit100, 
		exit0190, 
		next150, 
		setType190, 
		exit9020, exit9090
	}

	public Vpusurc() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		vpmfmtrec = (Vpmfmtrec) parmArray[1];		
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
		try {
			main100();
			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start0110();
				}
				case exit0190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start0110()
{
	if (firstTime.isTrue()){
		vpmfmtrec.statuz.set(SPACES);
		wsaaTotEstVal.set(ZERO);
	}
	if (isNE(srcalcpy.status, varcom.endp)){
		compute(wsaaTotEstVal, 2).set(add(wsaaTotEstVal, vpmfmtrec.amount01));
		wsaaFirst.set(wsccN);
		if (isEQ(srcalcpy.type, wsccJ)){
			vpmfmtrec.statuz.set(varcom.endp);
			vpmfmtrec.amount02.set(wsaaTotEstVal.getbigdata().toString());
		}
	} else {
		wsaaFirst.set(wsccY);
		if (isEQ(srcalcpy.type, wsccC)){
			vpmfmtrec.statuz.set(varcom.endp);
			vpmfmtrec.amount02.set(wsaaTotEstVal.getbigdata().toString());
		}
		if (isEQ(srcalcpy.type, wsccL)){
			srcalcpy.type.set(SPACES);
			vpmfmtrec.statuz.set(SPACES);
			getFee100();
		}
	}
	
}
protected void getFee100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start100();
				}
				case next150: {
					next150();
				}
				case setType190: {
					setType190();
				}
				case exit100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start100()
{
	//Read T5687 to get the surrender method.
	initialize(itdmIO.getDataArea());
	itdmIO.setItemtabl(t5687);
	itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
	itdmIO.setItemitem(srcalcpy.crtable);
	itdmIO.setItmfrm(srcalcpy.crrcd);
	itdmIO.setFunction(varcom.begn);
//	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//	itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.statuz, varcom.oK)
	|| isNE(srcalcpy.chdrChdrcoy, itdmIO.getItemcoy())
	|| isNE(t5687, itdmIO.getItemtabl())
	|| isNE(srcalcpy.crtable, itdmIO.getItemitem())) {
		syserrrec.params.set(itdmIO.getParams());
		fatalError9000();
	}
	t5687rec.t5687Rec.set(itdmIO.getGenarea());
	//READ T5542
	if (isEQ(t5687rec.partsurr, SPACES)){
		srcalcpy.psNotAllwd.set(wsccT);
		goTo(GotoLabel.exit100);
	}
	
	initialize(itdmIO.getDataArea());
	itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
	itdmIO.setItemtabl(t5542);
	StringBuilder stringVariable = new StringBuilder();
	stringVariable.append(t5687rec.partsurr.toString());
	stringVariable.append(srcalcpy.chdrCurr.toString());
	wsaaItemitem.setLeft(stringVariable.toString());
	itdmIO.setItemitem(wsaaItemitem);
	itdmIO.setItmfrm(srcalcpy.crrcd);
	itdmIO.setFunction(varcom.begn);
//	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//	itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	SmartFileCode.execute(appVars, itdmIO);
	
	if (isNE(itdmIO.statuz, varcom.oK)
			&& isNE(itdmIO.statuz, varcom.endp)){
		syserrrec.params.set(itdmIO.getParams());
		fatalError9000();
	}
	if (isEQ(itdmIO.statuz, varcom.endp)
			|| isNE(srcalcpy.chdrChdrcoy, itdmIO.getItemcoy())
			|| isNE(t5542, itdmIO.getItemtabl())
			|| isNE(wsaaItemitem, itdmIO.getItemitem())) {
		srcalcpy.psNotAllwd.set(wsccT);
		goTo(GotoLabel.exit100);
	} else {
		t5542rec.t5542Rec.set(itdmIO.getGenarea());
	}
	
	wsaaGotFreq.set(wsccY);
	for (wsaaSub.set(1);!(!gotFreq.isTrue() 
			|| isEQ(srcalcpy.billfreq, t5542rec.billfreq[wsaaSub.toInt()])); wsaaSub.add(1)){
		if (isGT(wsaaSub, 6)){
			wsaaGotFreq.set(wsccN);
			syserrrec.statuz.set(h141);
		}
	}
	
	if (!gotFreq.isTrue()){
		goTo(GotoLabel.next150);
	}
	if (isNE(t5542rec.ffamt[wsaaSub.toInt()], ZERO)){
		srcalcpy.actualVal.set(t5542rec.ffamt[wsaaSub.toInt()]);
		goTo(GotoLabel.next150);
	}
	if (isEQ(t5542rec.feepc[wsaaSub.toInt()], ZERO)){
		srcalcpy.actualVal.set(ZERO);
		goTo(GotoLabel.next150);
	} else {
		srcalcpy.actualVal.set(t5542rec.feepc[wsaaSub.toInt()]);
		goTo(GotoLabel.next150);
	}
	compute(srcalcpy.actualVal, 2).set(div(mult(wsaaTotEstVal, t5542rec.feepc[wsaaSub.toInt()]), 100));
	
	if (isLT(srcalcpy.actualVal, t5542rec.feemin[wsaaSub.toInt()])){
		srcalcpy.actualVal.set(t5542rec.feemin[wsaaSub.toInt()]);
	} else if (isGT(srcalcpy.actualVal, t5542rec.feemax[wsaaSub.toInt()])) {
		srcalcpy.actualVal.set(t5542rec.feemax[wsaaSub.toInt()]);
	}
}

protected void next150()
{
	if (!gotFreq.isTrue()){
		wsaaGotFreq.set(wsccY);
		srcalcpy.actualVal.set(ZERO);
		goTo(GotoLabel.setType190);
	}
	if (isNE(t5542rec.wdlFreq[wsaaSub.toInt()], ZERO)){
		checkDatesFrmRcd200();
	}
	if (isNE(t5542rec.wdlAmount[wsaaSub.toInt()], ZERO)){
		if (isGT(t5542rec.wdlAmount[wsaaSub.toInt()], wsaaTotEstVal)){
			srcalcpy.neUnits.set(wsccY);
		}
	}
	if (isNE(t5542rec.wdrem[wsaaSub.toInt()], ZERO)){
		if (isLT(t5542rec.wdrem[wsaaSub.toInt()], wsaaTotEstVal)){
			srcalcpy.tmUnits.set(wsccY);
		}
	}
}


protected void setType190()
{
	srcalcpy.type.set(wsccC);
	srcalcpy.description.set(SPACES);
	initialize(descIO.getDataArea());
	descIO.setDescitem(wsccFee);
	descIO.setDesctabl(t6644);
	descIO.setDescpfx(smtpfxcpy.item);
	descIO.setDesccoy(srcalcpy.chdrChdrcoy);
	descIO.setLanguage(srcalcpy.language);
	descIO.setFunction(varcom.readr);
	descIO.setFormat(descrec);
	SmartFileCode.execute(appVars, descIO);
	if (isEQ(descIO.getStatuz(),varcom.oK)){
		srcalcpy.description.set(descIO.getShortdesc());
	}else {
		srcalcpy.description.set(SPACES);
	}
}

protected void checkDatesFrmRcd200()
{
	//FREQUENCY FACTOR
	if (isLT(vpmfmtrec.rate01, t5542rec.wdlFreq[wsaaSub.toInt()])){
		srcalcpy.psNotAllwd.set(wsccY);
	}
}

protected void exit0190()
{
	exitProgram();
	goBack();
	stopRun();
}

protected void fatalError9000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				error9010();
			}
			case exit9020: {
				exit9020();
			}
			case exit9090: {
				exit9090();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void error9010()
{
	if (isEQ(srcalcpy.status,"BOMB")) {
		goTo(GotoLabel.exit9020);
	}
	syserrrec.syserrStatuz.set(srcalcpy.status);
	if (isNE(syserrrec.syserrType,"2")) {
		syserrrec.syserrType.set("1");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
}

protected void exit9020()
{
	srcalcpy.status.set("BOMB");
	/*EXIT*/
}

protected void exit9090()
{
	exitProgram();
}

}


