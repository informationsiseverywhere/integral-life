package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:47
 * Description:
 * Copybook name: PH551PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ph551par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(23);
  	public ZonedDecimalData efdate = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData flnam = new FixedLengthStringData(10).isAPartOf(parmRecord, 8);
  	public FixedLengthStringData itemtabl = new FixedLengthStringData(5).isAPartOf(parmRecord, 18);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}