package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br559DAO extends BaseDAO<MedxTemppf> {
	public List<MedxTemppf> loadDataByBatch(int batchID);
	public int populateBr559Temp(int batchExtractSize, String medxtempTable, String chdrfrom, String chdrto);
}
