package com.csc.life.productdefinition.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.common.DD;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR559.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr559Report extends SMARTReportLayout { 

	private FixedLengthStringData alname = new FixedLengthStringData(47);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cltaddr01 = new FixedLengthStringData(DD.cltaddr.length);//pmujavadiya//ILIFE-3222
	private FixedLengthStringData cltaddr02 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData cltaddr03 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData cltaddr04 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData cltaddr05 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData cltpcode = new FixedLengthStringData(10);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData descrn = new FixedLengthStringData(15);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private FixedLengthStringData exmcode = new FixedLengthStringData(10);
	private FixedLengthStringData invref = new FixedLengthStringData(15);
	private FixedLengthStringData lname = new FixedLengthStringData(40);
	private ZonedDecimalData nofmbr = new ZonedDecimalData(7, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData zmedfee = new ZonedDecimalData(17, 2);
	private FixedLengthStringData zmedtyp = new FixedLengthStringData(8);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr559Report() {
		super();
	}


	/**
	 * Print the XML for Rr559d01
	 */
	public void printRr559d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		nofmbr.setFieldName("nofmbr");
		nofmbr.setInternal(subString(recordData, 1, 7));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 8, 8));
		zmedtyp.setFieldName("zmedtyp");
		zmedtyp.setInternal(subString(recordData, 16, 8));
		lname.setFieldName("lname");
		lname.setInternal(subString(recordData, 24, 40));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 64, 10));
		invref.setFieldName("invref");
		invref.setInternal(subString(recordData, 74, 15));
		zmedfee.setFieldName("zmedfee");
		zmedfee.setInternal(subString(recordData, 89, 17));
		printLayout("Rr559d01",			// Record name
			new BaseData[]{			// Fields:
				nofmbr,
				chdrnum,
				zmedtyp,
				lname,
				effdate,
				invref,
				zmedfee
			}
		);

	}

	/**
	 * Print the XML for Rr559e01
	 */
	public void printRr559e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rr559e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr559h01
	 */
	public void printRr559h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.set(3);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		time.setFieldName("time");
		time.set(getTime());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 42, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 45, 30));
		exmcode.setFieldName("exmcode");
		exmcode.setInternal(subString(recordData, 75, 10));
		alname.setFieldName("alname");
		alname.setInternal(subString(recordData, 85, 47));
		cltaddr01.setFieldName("cltaddr01");
		cltaddr01.setInternal(subString(recordData, 132, DD.cltaddr.length));//pmujavadiya//ILIFE-3222
		cltaddr02.setFieldName("cltaddr02");
		cltaddr02.setInternal(subString(recordData, 132+DD.cltaddr.length, DD.cltaddr.length));
		cltaddr03.setFieldName("cltaddr03");
		cltaddr03.setInternal(subString(recordData, 132+DD.cltaddr.length*2, DD.cltaddr.length));
		cltaddr04.setFieldName("cltaddr04");
		cltaddr04.setInternal(subString(recordData, 132+DD.cltaddr.length*3, DD.cltaddr.length));
		cltpcode.setFieldName("cltpcode");
		cltpcode.setInternal(subString(recordData, 132+DD.cltaddr.length*4, 10));
		cltaddr05.setFieldName("cltaddr05");
		cltaddr05.setInternal(subString(recordData, 142+DD.cltaddr.length*4, DD.cltaddr.length));
		printLayout("Rr559h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				time,
				company,
				companynm,
				pagnbr,
				currcode,
				currencynm,
				exmcode,
				alname,
				cltaddr01,
				cltaddr02,
				cltaddr03,
				cltaddr04,
				cltpcode,
				cltaddr05
			}
		);

		currentPrintLine.add(18);
	}

	/**
	 * Print the XML for Rr559t01
	 */
	public void printRr559t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		descrn.setFieldName("descrn");
		descrn.setInternal(subString(recordData, 1, 15));
		zmedfee.setFieldName("zmedfee");
		zmedfee.setInternal(subString(recordData, 16, 17));
		printLayout("Rr559t01",			// Record name
			new BaseData[]{			// Fields:
				descrn,
				zmedfee
			}
		);

		currentPrintLine.add(3);
	}


}
