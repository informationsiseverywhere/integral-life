/*
 * File: T5667pt.java
 * Date: 30 August 2009 2:25:26
 * Author: Quipoz Limited
 * 
 * Class transformed from T5667PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5667.
*
*
*
***********************************************************************
* </pre>
*/
public class T5667pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Tolerance Limits                        S5667");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 26, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(75);
	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(62).isAPartOf(wsaaPrtLine003, 13, FILLER).init("<       1st Shortfall       >    <       2nd Shortfall       >");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(75);
	private FixedLengthStringData filler9 = new FixedLengthStringData(75).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Frequency    (%)               Max Amount     (%)     Max Amount");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(75);
	private FixedLengthStringData filler10 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 4);
	private FixedLengthStringData filler11 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine005, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine005, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine005, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine005, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(75);
	private FixedLengthStringData filler15 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 4);
	private FixedLengthStringData filler16 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine006, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine006, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(75);
	private FixedLengthStringData filler20 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 4);
	private FixedLengthStringData filler21 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine007, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine007, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler25 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 4);
	private FixedLengthStringData filler26 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine008, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine008, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(75);
	private FixedLengthStringData filler30 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 4);
	private FixedLengthStringData filler31 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine009, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine009, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(75);
	private FixedLengthStringData filler35 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 4);
	private FixedLengthStringData filler36 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine010, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine010, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(75);
	private FixedLengthStringData filler40 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 4);
	private FixedLengthStringData filler41 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine011, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine011, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(75);
	private FixedLengthStringData filler45 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 4);
	private FixedLengthStringData filler46 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine012, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine012, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(75);
	private FixedLengthStringData filler50 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 4);
	private FixedLengthStringData filler51 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine013, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine013, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(75);
	private FixedLengthStringData filler55 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 4);
	private FixedLengthStringData filler56 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine014, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine014, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(75);
	private FixedLengthStringData filler60 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 4);
	private FixedLengthStringData filler61 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine015, 13).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 24).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine015, 46).setPattern("ZZ.ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 57).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(38);
	private FixedLengthStringData filler65 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Shortfall Ind For Terminated Agent");
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 37);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5667rec t5667rec = new T5667rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5667pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5667rec.t5667Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo006.set(t5667rec.prmtol01);
		fieldNo007.set(t5667rec.maxAmount01);
		fieldNo011.set(t5667rec.prmtol02);
		fieldNo012.set(t5667rec.maxAmount02);
		fieldNo016.set(t5667rec.prmtol03);
		fieldNo017.set(t5667rec.maxAmount03);
		fieldNo021.set(t5667rec.prmtol04);
		fieldNo022.set(t5667rec.maxAmount04);
		fieldNo026.set(t5667rec.prmtol05);
		fieldNo027.set(t5667rec.maxAmount05);
		fieldNo031.set(t5667rec.prmtol06);
		fieldNo032.set(t5667rec.maxAmount06);
		fieldNo036.set(t5667rec.prmtol07);
		fieldNo037.set(t5667rec.maxAmount07);
		fieldNo041.set(t5667rec.prmtol08);
		fieldNo042.set(t5667rec.maxAmount08);
		fieldNo046.set(t5667rec.prmtol09);
		fieldNo047.set(t5667rec.maxAmount09);
		fieldNo051.set(t5667rec.prmtol10);
		fieldNo052.set(t5667rec.maxAmount10);
		fieldNo056.set(t5667rec.prmtol11);
		fieldNo057.set(t5667rec.maxAmount11);
		fieldNo005.set(t5667rec.freq01);
		fieldNo010.set(t5667rec.freq02);
		fieldNo015.set(t5667rec.freq03);
		fieldNo020.set(t5667rec.freq04);
		fieldNo025.set(t5667rec.freq05);
		fieldNo030.set(t5667rec.freq06);
		fieldNo035.set(t5667rec.freq07);
		fieldNo040.set(t5667rec.freq08);
		fieldNo045.set(t5667rec.freq09);
		fieldNo050.set(t5667rec.freq10);
		fieldNo055.set(t5667rec.freq11);
		fieldNo008.set(t5667rec.prmtoln01);
		fieldNo009.set(t5667rec.maxamt01);
		fieldNo013.set(t5667rec.prmtoln02);
		fieldNo014.set(t5667rec.maxamt02);
		fieldNo018.set(t5667rec.prmtoln03);
		fieldNo019.set(t5667rec.maxamt03);
		fieldNo023.set(t5667rec.prmtoln04);
		fieldNo024.set(t5667rec.maxamt04);
		fieldNo028.set(t5667rec.prmtoln05);
		fieldNo029.set(t5667rec.maxamt05);
		fieldNo033.set(t5667rec.prmtoln06);
		fieldNo034.set(t5667rec.maxamt06);
		fieldNo038.set(t5667rec.prmtoln07);
		fieldNo039.set(t5667rec.maxamt07);
		fieldNo043.set(t5667rec.prmtoln08);
		fieldNo044.set(t5667rec.maxamt08);
		fieldNo048.set(t5667rec.prmtoln09);
		fieldNo049.set(t5667rec.maxamt09);
		fieldNo053.set(t5667rec.prmtoln10);
		fieldNo054.set(t5667rec.maxamt10);
		fieldNo058.set(t5667rec.prmtoln11);
		fieldNo059.set(t5667rec.maxamt11);
		fieldNo060.set(t5667rec.sfind);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
