package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClexTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Vpxubbl extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String VPXUBBLREC = "VPXUBBLREC";

	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(10).init("VPXUBBL");
	private ZonedDecimalData ia = new ZonedDecimalData(5, 0);
	private ZonedDecimalData ib = new ZonedDecimalData(5, 0);
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0);
	private ZonedDecimalData iy = new ZonedDecimalData(3, 0);

	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanSuffixx = new FixedLengthStringData(4).isAPartOf(wsaaPlanSuffix, 0, REDEFINE);
	private ZonedDecimalData filter = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffixx, 0).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffixx, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private FixedLengthStringData wsaaStart = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaSvFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(1).isAPartOf(wsaaT5551Item, 4);
	private FixedLengthStringData wsaaT5551Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);
	
	private FixedLengthStringData wsaaConstants = new FixedLengthStringData(7);
	private FixedLengthStringData wsccA = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 0).init("A");
	private FixedLengthStringData wsccAsterisk = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 1).init("*");
	private FixedLengthStringData wsccD = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 2).init("D");
	private FixedLengthStringData wsccI = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 3).init("I");
	private FixedLengthStringData wsccLe = new FixedLengthStringData(2).isAPartOf(wsaaConstants, 4).init("LE");
	private FixedLengthStringData wsccNegative = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 6).init("-");

	private FixedLengthStringData errors = new FixedLengthStringData(12);
	private FixedLengthStringData e049 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("E049");
	private FixedLengthStringData f401 = new FixedLengthStringData(4).isAPartOf(errors, 4).init("F401");
	private FixedLengthStringData h832 = new FixedLengthStringData(4).isAPartOf(errors, 8).init("H832");

    // Format
	private FixedLengthStringData formats = new FixedLengthStringData(120);
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).isAPartOf(formats, 0).init("ACBLREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).isAPartOf(formats, 10).init("CHDRLIFREC");
	private FixedLengthStringData clexrec = new FixedLengthStringData(10).isAPartOf(formats, 20).init("CLEXREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).isAPartOf(formats, 30).init("COVRREC");
	private FixedLengthStringData covrpenrec = new FixedLengthStringData(10).isAPartOf(formats, 40).init("COVRPENREC");
	private FixedLengthStringData covrsurrec = new FixedLengthStringData(10).isAPartOf(formats, 50).init("COVRSURREC");
	private FixedLengthStringData hitsrec = new FixedLengthStringData(10).isAPartOf(formats, 60).init("HITSREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(formats, 70).init("ITEMREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).isAPartOf(formats, 80).init("LEXTREC");
	private FixedLengthStringData lifernlrec = new FixedLengthStringData(10).isAPartOf(formats, 90).init("LIFERNLREC");
	private FixedLengthStringData utrsrec = new FixedLengthStringData(10).isAPartOf(formats, 100).init("UTRSREC");
	private FixedLengthStringData zrstnudrec = new FixedLengthStringData(10).isAPartOf(formats, 110).init("ZRSTNUDREC");

	private FixedLengthStringData tables = new FixedLengthStringData(20);
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).isAPartOf(tables, 0).init("T5515");
	private FixedLengthStringData t5551 = new FixedLengthStringData(5).isAPartOf(tables, 5).init("T5551");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).isAPartOf(tables, 10).init("T5645");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).isAPartOf(tables, 15).init("T6598");

	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClexTableDAM clexIO = new ClexTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	//private CovrpenTableDAM covrpenIO = new CovrpenTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	//private LifernlTableDAM lifernlIO = new LifernlTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private T5551rec t5551rec = new T5551rec();
	private T5645rec t5645rec = new T5645rec();
	private T6598rec t6598rec = new T6598rec();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	//private ZrstnudTableDAM zrstnudIO = new ZrstnudTableDAM();

	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Freqcpy freqcpy = new Freqcpy();

	private Ubblallpar ubblallpar = new Ubblallpar();
	private Vpxubblrec vpxubblrec = new Vpxubblrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	
	private Lifepf lifepf = null;
	private Zrstpf zrstpf = null;
	private Covrpf covrpf = null;
	
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class); 
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class); 
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit000,
		exit490,
		exit890,
		b550Exit,
		nextRecord420
	}

    public Vpxubbl () {
        super();
    }

    public void mainline(Object... parmArray) {
    	try {
    		vpxubblrec = (Vpxubblrec) parmArray[1];
    		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
    		ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
            main000();
    		vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);
    	} catch (COBOLExitProgramException e) {
		} catch (Exception e) {
 			systemError900();
 		}

    }


    protected void main000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para000();
				}
				case exit000: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
    
    protected void para000() {
    	syserrrec.subrname.set(wsaaSubr);
    	vpxubblrec.statuz.set(Varcom.oK);
    	if (isEQ(vpxubblrec.function,"INIT")) {
    		init050();
    		//ILIFE-7442
    		ubblallpar.benfunc.set(ubblallpar.function);
    		ubblallpar.polcurr.set(ubblallpar.cntcurr);
    		ubblallpar.sbillfreq.set(ubblallpar.billfreq);
    		
	    	if(!isEQ(ubblallpar.svMethod,SPACES)){
	    		findCovrsur100();
	    	}
	    	findLifernl150();
	    	getTotalCd200();
		    // Check!!!!!!!!!
	    	if(isEQ(ubblallpar.function , "ISSUE")){
	    		getCovrSinglePrem250();
	    	}
	    	findLext400();
	    	findChdrlif500();
	    	calcDuration810();

    	} 
    	/*else if (isEQ(vpxubblrec.function,"GETV")){
    		returnValue1000();
    	} */
    	else {
	    	syserrrec.statuz.set(e049);
	    	vpxubblrec.statuz.set(e049);
	    	systemError900();
    	}
//    	exitProgram();
    }

	private void init050() {
		initialize(vpxubblrec.dataRec);
		vpxubblrec.crtable.set(SPACES);
		vpxubblrec.pstatcode.set(SPACES);
		vpxubblrec.status.set(SPACES);
		vpxubblrec.lcltsex.set(SPACES);
		vpxubblrec.jcltsex.set(SPACES);
		vpxubblrec.premMeth.set(SPACES);
		vpxubblrec.rstaflag.set(SPACES);
		vpxubblrec.reasind.set(SPACES);
		vpxubblrec.chargeCalc.set(SPACES);
		vpxubblrec.lcltdob.set(varcom.maxdate);
		vpxubblrec.jcltdob.set(varcom.maxdate);
		vpxubblrec.crrcd.set(ZERO);
		vpxubblrec.convUnits.set(ZERO);
		vpxubblrec.singp.set(ZERO);
		vpxubblrec.sumin.set(ZERO);
		vpxubblrec.totalCd.set(ZERO);
		vpxubblrec.duration.set(ZERO);
		vpxubblrec.count.set(ZERO);
		vpxubblrec.covrSingp.set(ZERO);
		vpxubblrec.modPrem.set(ZERO);
		vpxubblrec.paidPrem.set(ZERO);
		vpxubblrec.recordCount.set(ZERO);
		vpxubblrec.lage.set(ZERO);
		vpxubblrec.jlage.set(ZERO);
//start
		/*for (ix.set(1); isLTE(ix, 8); ix.add(1)) {
			vpxubblrec.opcda[ix.toInt()].set(SPACES);
			vpxubblrec.oppc[ix.toInt()].set(ZERO);
			vpxubblrec.zmortpct[ix.toInt()].set(ZERO);
			vpxubblrec.agerate[ix.toInt()].set(ZERO);
			vpxubblrec.insprm[ix.toInt()].set(ZERO);
		}

		for (ix.set(1); isLTE(ix, 10); ix.add(1)) {
			vpxubblrec.fund[ix.toInt()].set(SPACES);
			vpxubblrec.type[ix.toInt()].set(SPACES);
			vpxubblrec.currcode[ix.toInt()].set(SPACES);
			vpxubblrec.curduntbal[ix.toInt()].set(ZERO);
		}
		*/
		
		vpxubblrec.oppc01.set(ZERO);
		vpxubblrec.zmortpct01.set(ZERO);
		vpxubblrec.opcda01.set(SPACE);
		vpxubblrec.agerate01.set(ZERO);
		vpxubblrec.insprm01.set(ZERO);
		
		vpxubblrec.oppc02.set(ZERO);
		vpxubblrec.zmortpct02.set(ZERO);
		vpxubblrec.opcda02.set(SPACE);
		vpxubblrec.agerate02.set(ZERO);
		vpxubblrec.insprm02.set(ZERO);
		
		vpxubblrec.oppc03.set(ZERO);
		vpxubblrec.zmortpct03.set(ZERO);
		vpxubblrec.opcda03.set(SPACE);
		vpxubblrec.agerate03.set(ZERO);
		vpxubblrec.insprm03.set(ZERO);
		
		vpxubblrec.oppc04.set(ZERO);
		vpxubblrec.zmortpct04.set(ZERO);
		vpxubblrec.opcda04.set(SPACE);
		vpxubblrec.agerate04.set(ZERO);
		vpxubblrec.insprm04.set(ZERO);
		
		vpxubblrec.oppc05.set(ZERO);
		vpxubblrec.zmortpct05.set(ZERO);
		vpxubblrec.opcda05.set(SPACE);
		vpxubblrec.agerate05.set(ZERO);
		vpxubblrec.insprm05.set(ZERO);
		
		vpxubblrec.oppc06.set(ZERO);
		vpxubblrec.zmortpct06.set(ZERO);
		vpxubblrec.opcda06.set(SPACE);
		vpxubblrec.agerate06.set(ZERO);
		vpxubblrec.insprm06.set(ZERO);
		
		vpxubblrec.oppc07.set(ZERO);
		vpxubblrec.zmortpct07.set(ZERO);
		vpxubblrec.opcda07.set(SPACE);
		vpxubblrec.agerate07.set(ZERO);
		vpxubblrec.insprm07.set(ZERO);
		
		vpxubblrec.oppc08.set(ZERO);
		vpxubblrec.zmortpct08.set(ZERO);
		vpxubblrec.opcda08.set(SPACE);
		vpxubblrec.agerate08.set(ZERO);
		vpxubblrec.insprm08.set(ZERO);
		
		
		vpxubblrec.fund01.set(SPACE);
		vpxubblrec.type01.set(SPACE);
		vpxubblrec.currcode01.set(SPACE);
		vpxubblrec.curduntbal01.set(ZERO);		
		
		vpxubblrec.fund02.set(SPACE);
		vpxubblrec.type02.set(SPACE);
		vpxubblrec.currcode02.set(SPACE);
		vpxubblrec.curduntbal02.set(ZERO);		
		
		vpxubblrec.fund03.set(SPACE);
		vpxubblrec.type03.set(SPACE);
		vpxubblrec.currcode03.set(SPACE);
		vpxubblrec.curduntbal03.set(ZERO);		
		
		vpxubblrec.fund04.set(SPACE);
		vpxubblrec.type04.set(SPACE);
		vpxubblrec.currcode04.set(SPACE);
		vpxubblrec.curduntbal04.set(ZERO);		
		
		vpxubblrec.fund05.set(SPACE);
		vpxubblrec.type05.set(SPACE);
		vpxubblrec.currcode05.set(SPACE);
		vpxubblrec.curduntbal05.set(ZERO);		
		
		vpxubblrec.fund06.set(SPACE);
		vpxubblrec.type06.set(SPACE);
		vpxubblrec.currcode06.set(SPACE);
		vpxubblrec.curduntbal06.set(ZERO);		
		
		vpxubblrec.fund07.set(SPACE);
		vpxubblrec.type07.set(SPACE);
		vpxubblrec.currcode07.set(SPACE);
		vpxubblrec.curduntbal07.set(ZERO);		
		
		vpxubblrec.fund08.set(SPACE);
		vpxubblrec.type08.set(SPACE);
		vpxubblrec.currcode08.set(SPACE);
		vpxubblrec.curduntbal08.set(ZERO);		
		
		vpxubblrec.fund09.set(SPACE);
		vpxubblrec.type09.set(SPACE);
		vpxubblrec.currcode09.set(SPACE);
		vpxubblrec.curduntbal09.set(ZERO);		
		
		vpxubblrec.fund10.set(SPACE);
		vpxubblrec.type10.set(SPACE);
		vpxubblrec.currcode10.set(SPACE);
		vpxubblrec.curduntbal10.set(ZERO);		
		
		///end
		
		vpxubblrec.copybook.set(VPXUBBLREC);
		wsaaSub.set(ZERO);

	}
	
/*	private void returnValue1000() {
		vpxubblrec.resultValue.set(SPACES);
		String[] field = VPMSUtils.separateNameIndex(vpxubblrec.resultField.getData().toString().trim());
		String fieldName = field[0];
		String index = field[1];
		
		if (isEQ(fieldName, "crtable")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.crtable);
		} else if (isEQ(fieldName, "pstatcode")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.pstatcode);
		} else if (isEQ(fieldName, "status")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.status);
		} else if (isEQ(fieldName, "lcltdob")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.lcltdob);
		} else if (isEQ(fieldName, "jcltdob")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.jcltdob);
		} else if (isEQ(fieldName, "lcltsex")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.lcltsex);
		} else if (isEQ(fieldName, "jcltsex")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.jcltsex);
		} else if (isEQ(fieldName, "premMeth")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.premMeth);
		} else if (isEQ(fieldName, "rstaflag")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.rstaflag);
		} else if (isEQ(fieldName, "reasind")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.reasind);
		} else if (isEQ(fieldName, "opcda")) {
			
			vpxubblrec.resultValue.set(vpxubblrec.opcda[new Integer(index)]);
		} else if (isEQ(fieldName, "chargeCalc")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.chargeCalc);
		} else if (isEQ(fieldName, "crrcd")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.crrcd);
		} else if (isEQ(fieldName, "convUnits")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.convUnits));
		} else if (isEQ(fieldName, "singp")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.singp));
		} else if (isEQ(fieldName, "totalCd")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.totalCd));
		} else if (isEQ(fieldName, "covrSingp")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.covrSingp));
		} else if (isEQ(fieldName, "count")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.count));
		} else if (isEQ(fieldName, "oppc")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.oppc[new Integer(index)].getbigdata().toString());
		} else if (isEQ(fieldName, "zmortpct")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.zmortpct[new Integer(index)].getbigdata().toString());
		} else if (isEQ(fieldName, "agerate")) {
			
			vpxubblrec.resultValue.set(vpxubblrec.agerate[new Integer(index)].getbigdata().toString());
		} else if (isEQ(fieldName, "insprm")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.insprm[new Integer(index)].getbigdata().toString());
		} else if (isEQ(fieldName, "modPrem")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.modPrem));
		} else if (isEQ(fieldName, "paidPrem")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.paidPrem));
		} else if (isEQ(fieldName, "sumin")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.sumin));
		} else if (isEQ(fieldName, "recordCount")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.recordCount));
		} else if (isEQ(fieldName, "lage")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.lage));
		} else if (isEQ(fieldName, "jlage")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.jlage));
		} else if (isEQ(fieldName, "duration")) {
	
			vpxubblrec.resultValue.set(VPMSUtils.returnValue(vpxubblrec.duration));
		} else if (isEQ(fieldName, "curduntbal")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.curduntbal[new Integer(index)].getbigdata().toString());
		} else if (isEQ(fieldName, "currcode")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.currcode[new Integer(index)]);
		} else if (isEQ(fieldName, "fund")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.fund[new Integer(index)]);
		} else if (isEQ(fieldName, "type")) {
	
			vpxubblrec.resultValue.set(vpxubblrec.type[new Integer(index)]);
		} else {
	
			vpxubblrec.statuz.set(h832);
		}
	}*/


	private void findCovrsur100() {
		covrsurIO.setParams(SPACES);
		covrsurIO.function.set(Varcom.readr);
		covrsurIO.format.set(covrsurrec);
		covrsurIO.chdrcoy.set(ubblallpar.chdrChdrcoy);
		covrsurIO.chdrnum.set(ubblallpar.chdrChdrnum);
		covrsurIO.life.set(ubblallpar.lifeLife);
		covrsurIO.coverage.set(ubblallpar.covrCoverage);
		covrsurIO.rider.set(ubblallpar.covrRider);
		covrsurIO.planSuffix.set(ubblallpar.planSuffix);
		SmartFileCode.execute(appVars, covrsurIO);
		if(!isEQ(covrsurIO.statuz , Varcom.oK)){
			syserrrec.params.set(covrsurIO.getParams());
			dbError999();
		}
		if(isEQ(ubblallpar.function , "ISSUE")){
			vpxubblrec.singp.set(covrsurIO.singp);
			//No need to call surrender calculation
			return;
		}
		//Set up linkage area to Call Surrender value calculation
	    //subroutines.
		vpxubblrec.crtable.set(covrsurIO.crtable);
		vpxubblrec.crrcd.set(covrsurIO.crrcd);
		vpxubblrec.convUnits.set(covrsurIO.convertInitialUnits);
		vpxubblrec.pstatcode.set(covrsurIO.pstatcode);
		if(isGT(covrsurIO.instprem, ZERO)){
			vpxubblrec.singp.set(covrsurIO.instprem);
		} else {
			vpxubblrec.singp.set(covrsurIO.singp);
		}
		vpxubblrec.status.set(SPACES);
		readUtrsB100();
		readHitsB200();
		chargesParamsB300();

	}
	
	private void findLifernl150() {
		//Set-up keys
		lifepf = new Lifepf();		
		lifepf.setChdrcoy(ubblallpar.chdrChdrcoy.toString());
		lifepf.setChdrnum(ubblallpar.chdrChdrnum.toString());
		lifepf.setLife(ubblallpar.lifeLife.toString());
		lifepf.setJlife("00");		
		List<Lifepf> lifepfList = lifepfDAO.findByKey(lifepfDAO.getDBSchema(),lifepf, "CHDRCOY ASC,CHDRNUM ASC,LIFE ASC,JLIFE ASC,UNIQUE_NUMBER DESC");		
		if(lifepfList == null || lifepfList.isEmpty())
		{
			vpxubblrec.statuz.set(Varcom.mrnf);
			return;
		}
		lifepf = lifepfList.get(0);
//		//Read the file
//		readLifeX100();
//		
//		if(!isEQ(lifernlIO.statuz , Varcom.oK) 
//				&& !isEQ(lifernlIO.statuz , Varcom.endp)){
//			syserrrec.statuz.set(lifernlIO.statuz);
//			syserrrec.params.set(lifernlIO.getParams());
//			dbError999();
//		}
//		//Record not found
//		//TODO Check!!!!!!!!
//		if(isEQ(lifernlIO.statuz , Varcom.endp)
//				|| !isEQ(lifernlIO.chdrnum , ubblallpar.chdrChdrnum)
//				&& !isEQ(lifernlIO.chdrcoy , ubblallpar.chdrChdrcoy )
//				&& !isEQ(lifernlIO.life , ubblallpar.lifeLife)
//				&& !isEQ(lifernlIO.jlife, "00")) {
//			vpxubblrec.statuz.set(Varcom.mrnf);
//			return;
//		}

		vpxubblrec.premMeth.set(ubblallpar.premMeth);
		//Set up output field for Life
		vpxubblrec.lcltsex.set(lifepf.getCltsex());
		vpxubblrec.lcltdob.set(lifepf.getCltdob());
		calcAge820();
		findJlife175();
		if(lifepf == null)
		{
			return;
		}
		calcAge820();
	}
	
	private void findJlife175() {
		//Set-up keys
		lifepf = new Lifepf();		
		lifepf.setChdrcoy(ubblallpar.chdrChdrcoy.toString());
		lifepf.setChdrnum(ubblallpar.chdrChdrnum.toString());
		lifepf.setLife(ubblallpar.lifeLife.toString());
		lifepf.setJlife("01");
		
		List<Lifepf> lifepfList = lifepfDAO.findByKey(lifepfDAO.getDBSchema(),lifepf, "CHDRCOY ASC,CHDRNUM ASC,LIFE ASC,JLIFE ASC,UNIQUE_NUMBER DESC");
		
		if(lifepfList == null || lifepfList.isEmpty())
		{		
			lifepf = null;
			return;
		}
		lifepf = lifepfList.get(0);
				
		//Set up output field for JLife
		vpxubblrec.premMeth.set(ubblallpar.jlifePremMeth);
		vpxubblrec.jcltsex.set(lifepf.getCltsex());
		vpxubblrec.jcltdob.set(lifepf.getCltdob());
	}	
	
	private void getTotalCd200() {
//		zrstnudIO.setParams(SPACES);
//		zrstnudIO.chdrcoy.set(ubblallpar.chdrChdrcoy);
//		zrstnudIO.chdrnum.set(ubblallpar.chdrChdrnum);
//		zrstnudIO.life.set(ubblallpar.lifeLife);
//		zrstnudIO.format.set(zrstnudrec);
//		zrstnudIO.function.set(Varcom.begn);
		zrstpf = new Zrstpf();
		zrstpf.setChdrcoy(ubblallpar.chdrChdrcoy.toString());
		zrstpf.setChdrnum(ubblallpar.chdrChdrnum.toString());
		zrstpf.setLife(ubblallpar.lifeLife.toString());
		zrstpf.setCoverage(ubblallpar.covrCoverage.toString());
		zrstpf.setFeedbackInd(" ");
		List<Zrstpf> zrstpfList = zrstpfDAO.findByKey(zrstpfDAO.getDBSchema(),zrstpf,"CHDRCOY ASC,CHDRNUM ASC,LIFE ASC,COVERAGE ASC,RIDER ASC,UNIQUE_NUMBER DESC");
		
		if(zrstpfList == null || zrstpfList.isEmpty())
		{			
			return;
		}
		Iterator<Zrstpf> zrstpfIT = zrstpfList.iterator();
		while (zrstpfIT.hasNext()) {
			zrstpf = zrstpfIT.next();
			checkZrstExists300();
		}
	}

	private void getCovrSinglePrem250() {
		covrIO.setDataArea(SPACE);
		covrIO.chdrcoy.set(ubblallpar.chdrChdrcoy);
		covrIO.chdrnum.set(ubblallpar.chdrChdrnum);
		covrIO.life.set(ubblallpar.lifeLife);
		covrIO.coverage.set(ubblallpar.covrCoverage);
		covrIO.rider.set("00");
		covrIO.planSuffix.set(ubblallpar.planSuffix);
		covrIO.function.set(Varcom.readr);
		
		covrIO.format.set(covrrec);
		
		SmartFileCode.execute(appVars, covrIO);
		if (!isEQ(covrIO.statuz, Varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			dbError999();
		}
		vpxubblrec.covrSingp.set(covrIO.singp);

	}
	
	private void checkZrstExists300() {
//		SmartFileCode.execute(appVars, zrstnudIO);
//		if(!isEQ(zrstnudIO.statuz , Varcom.oK) 
//				&& !isEQ(zrstnudIO.statuz , Varcom.endp)){
//			syserrrec.params.set(zrstnudIO.getParams());
//			syserrrec.statuz.set(zrstnudIO.getStatuz());
//			dbError999();
//		}
//
//		if(isEQ(zrstnudIO.statuz , Varcom.endp)
//				|| !isEQ(zrstnudIO.chdrcoy , ubblallpar.chdrChdrcoy)
//				|| !isEQ(zrstnudIO.chdrnum , ubblallpar.chdrChdrnum)
//				|| !isEQ(zrstnudIO.life , ubblallpar.lifeLife)){
//			zrstnudIO.statuz.set(Varcom.endp);
//			return;
//		}

//		if(isEQ(zrstnudIO.coverage , ubblallpar.covrCoverage)){
			vpxubblrec.totalCd.add(zrstpf.getZramount01().doubleValue());
//		}
//		zrstnudIO.function.set(Varcom.nextr);
	}
	
	private void findLext400() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					search400();
				}
				case nextRecord420: {
					nextRecord420();
				}
				case exit490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	private void search400() {
		//Set-up keys
		lextIO.setDataKey(SPACES);
		lextIO.chdrcoy.set(ubblallpar.chdrChdrcoy);
		lextIO.chdrnum.set(ubblallpar.chdrChdrnum);
		lextIO.life.set(ubblallpar.lifeLife);
		lextIO.coverage.set(ubblallpar.covrCoverage);
		lextIO.rider.set(ubblallpar.covrRider);
		lextIO.seqnbr.set(ZERO);
		
		//Read the file
		lextIO.function.set(Varcom.begn);
		lextIO.format.set(lextrec);
	}
	
	private void nextRecord420() {
			SmartFileCode.execute(appVars, lextIO);
			if(!isEQ(lextIO.statuz , Varcom.oK) 
					&& !isEQ(lextIO.statuz , Varcom.endp)){
				syserrrec.params.set(lextIO.getParams());
				dbError999();
			}
			if(!isEQ(lextIO.statuz , Varcom.oK)
					|| !isEQ(lextIO.chdrcoy , ubblallpar.chdrChdrcoy)
					|| !isEQ(lextIO.chdrnum , ubblallpar.chdrChdrnum)
					|| !isEQ(lextIO.life , ubblallpar.lifeLife)
					|| !isEQ(lextIO.coverage , ubblallpar.covrCoverage)
					|| !isEQ(lextIO.rider , ubblallpar.covrRider)){
				goTo(GotoLabel.exit490);
			}

			if(isEQ(vpxubblrec.reasind , "2") && isEQ(lextIO.reasind , "1")) {
				goTo(GotoLabel.exit490);
			}

			if(!isEQ(vpxubblrec.reasind , "2") && isEQ(lextIO.reasind , "2")){
				goTo(GotoLabel.exit490);
			}
			// Skip any expired special terms.
			lextIO.function.set(Varcom.nextr);
			if (isLTE(lextIO.extCessDate, ubblallpar.occdate)) {
				goTo(GotoLabel.nextRecord420);
			}
			//Set up output fields
			wsaaSub.add(1);
			/*vpxubblrec.oppc[wsaaSub.toInt()].set(lextIO.oppc);
			vpxubblrec.zmortpct[wsaaSub.toInt()].set(lextIO.zmortpct);
			vpxubblrec.opcda[wsaaSub.toInt()].set(lextIO.opcda);
			vpxubblrec.agerate[wsaaSub.toInt()].set(lextIO.agerate);
			vpxubblrec.insprm[wsaaSub.toInt()].set(lextIO.insprm);

			*/
			
			if(wsaaSub.equals(1)){
				vpxubblrec.oppc01.set(sub(lextIO.getOppc(),100));
				vpxubblrec.zmortpct01.set(lextIO.getZmortpct());
				vpxubblrec.opcda01.set(lextIO.getOpcda());
				vpxubblrec.agerate01.set(lextIO.getAgerate());
				vpxubblrec.insprm01.set(lextIO.getInsprm());
				
			}
			else if(wsaaSub.equals(2)){
				vpxubblrec.oppc02.set(lextIO.getOppc());
				vpxubblrec.zmortpct02.set(lextIO.getZmortpct());
				vpxubblrec.opcda02.set(lextIO.getOpcda());
				vpxubblrec.agerate02.set(lextIO.getAgerate());
				vpxubblrec.insprm02.set(lextIO.getInsprm());				
			}
			else if(wsaaSub.equals(3)){
				vpxubblrec.oppc03.set(lextIO.getOppc());
				vpxubblrec.zmortpct03.set(lextIO.getZmortpct());
				vpxubblrec.opcda03.set(lextIO.getOpcda());
				vpxubblrec.agerate03.set(lextIO.getAgerate());
				vpxubblrec.insprm03.set(lextIO.getInsprm());				
			}
			else if(wsaaSub.equals(4)){
				vpxubblrec.oppc04.set(lextIO.getOppc());
				vpxubblrec.zmortpct04.set(lextIO.getZmortpct());
				vpxubblrec.opcda04.set(lextIO.getOpcda());
				vpxubblrec.agerate04.set(lextIO.getAgerate());
				vpxubblrec.insprm04.set(lextIO.getInsprm());				
			}
			else if(wsaaSub.equals(5)){
				vpxubblrec.oppc05.set(lextIO.getOppc());
				vpxubblrec.zmortpct05.set(lextIO.getZmortpct());
				vpxubblrec.opcda05.set(lextIO.getOpcda());
				vpxubblrec.agerate05.set(lextIO.getAgerate());
				vpxubblrec.insprm05.set(lextIO.getInsprm());				
			}
			else if(wsaaSub.equals(6)){
				vpxubblrec.oppc06.set(lextIO.getOppc());
				vpxubblrec.zmortpct06.set(lextIO.getZmortpct());
				vpxubblrec.opcda06.set(lextIO.getOpcda());
				vpxubblrec.agerate06.set(lextIO.getAgerate());
				vpxubblrec.insprm06.set(lextIO.getInsprm());				
			}
			else if(wsaaSub.equals(7)){
				vpxubblrec.oppc07.set(lextIO.getOppc());
				vpxubblrec.zmortpct07.set(lextIO.getZmortpct());
				vpxubblrec.opcda07.set(lextIO.getOpcda());
				vpxubblrec.agerate07.set(lextIO.getAgerate());
				vpxubblrec.insprm07.set(lextIO.getInsprm());				
			}
			else if(wsaaSub.equals(8)){
				vpxubblrec.oppc08.set(lextIO.getOppc());
				vpxubblrec.zmortpct08.set(lextIO.getZmortpct());
				vpxubblrec.opcda08.set(lextIO.getOpcda());
				vpxubblrec.agerate08.set(lextIO.getAgerate());
				vpxubblrec.insprm08.set(lextIO.getInsprm());				
			}
			vpxubblrec.count.set(wsaaSub);
			goTo(GotoLabel.nextRecord420);
	}

	private void findChdrlif500() {
		chdrlifIO.setDataKey(SPACES);
		chdrlifIO.chdrcoy.set(ubblallpar.chdrChdrcoy);
		chdrlifIO.chdrnum.set(ubblallpar.chdrChdrnum);
		chdrlifIO.statuz.set(Varcom.oK);
		chdrlifIO.function.set(Varcom.readr);
		chdrlifIO.format.set(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if(!isEQ(chdrlifIO.statuz , Varcom.oK) && !isEQ(chdrlifIO.statuz , Varcom.mrnf)){
			syserrrec.statuz.set(chdrlifIO.statuz);
			syserrrec.params.set(chdrlifIO.getParams());
			dbError999();
		}
		if(isEQ(chdrlifIO.statuz , Varcom.mrnf)){
			vpxubblrec.statuz.set(Varcom.mrnf);
			// TODO: Check it.
			return;
		}
		//Set up output fields
		vpxubblrec.copybook.set(VPXUBBLREC);
		if(!isEQ(chdrlifIO.cownnum , SPACES)){
			//Get Staff Flag
			findClex600();
		}
	}

	private void findClex600() {
		//To check staff flag - Read the client extra details.
		clexIO.setDataKey(SPACES);
		clexIO.clntcoy.set(chdrlifIO.cowncoy);
		clexIO.clntnum.set(chdrlifIO.cownnum);
		clexIO.clntpfx.set(chdrlifIO.cownpfx);
		clexIO.function.set(Varcom.readr);
		clexIO.format.set(clexrec);
		SmartFileCode.execute(appVars, clexIO);
		if (!isEQ(clexIO.statuz, Varcom.oK)
				&& !isEQ(clexIO.statuz, Varcom.mrnf)) {
			syserrrec.statuz.set(clexIO.statuz);
			syserrrec.params.set(clexIO.getParams());
			dbError999();
		}
		if (!isEQ(clexIO.rstaflag, "Y") 
				&& !isEQ(chdrlifIO.jownnum, SPACES)) {
			clexIO.clntcoy.set(chdrlifIO.cowncoy);
			clexIO.clntnum.set(chdrlifIO.jownnum);
			clexIO.clntpfx.set(chdrlifIO.cownpfx);
			clexIO.format.set(clexrec);
			clexIO.function.set(Varcom.readr);
			SmartFileCode.execute(appVars, clexIO);
			if (!isEQ(clexIO.statuz, Varcom.oK)
					&& !isEQ(clexIO.statuz, Varcom.mrnf)) {
				syserrrec.statuz.set(clexIO.statuz);
				syserrrec.params.set(clexIO.getParams());
				dbError999();
			}
		}
		/*  Set up output fields */
		if(isEQ(clexIO.rstaflag , "Y")){
			vpxubblrec.rstaflag.set("Y");
		} else {
			vpxubblrec.rstaflag.set("N");
		}
	}

	
	
	private void calcDuration810() {
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(ubblallpar.effdate);
		datcon3rec.intDate2.set(ubblallpar.premCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (!isEQ(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError900();
		}
		datcon3rec.freqFactor.add(0.99999);
		vpxubblrec.duration.set(datcon3rec.freqFactor);
	}
	
	private void calcAge820() {
		// Compute AGE
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(ubblallpar.language);
		agecalcrec.cnttype.set(ubblallpar.cnttype);
		agecalcrec.intDate1.set(lifepf.getCltdob());
		agecalcrec.intDate2.set(ubblallpar.effdate);
		agecalcrec.company.set("9");
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isEQ(agecalcrec.statuz, "IVFD")) {
			syserrrec.statuz.set(f401);
			systemError900();
			goTo(GotoLabel.exit890);
		}
		if (!isEQ(agecalcrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError900();
		}
		if (isEQ(lifepf.getJlife(), "01")) {
			vpxubblrec.jlage.set(agecalcrec.agerating);
		} else {
			vpxubblrec.lage.set(agecalcrec.agerating);
		}
	}

	
	private void readUtrsB100(){
		initialize(utrsIO.getDataArea());
		utrsIO.chdrcoy.set(covrsurIO.chdrcoy);
		utrsIO.chdrnum.set(covrsurIO.chdrnum);
		utrsIO.life.set(covrsurIO.life);
		utrsIO.coverage.set(covrsurIO.coverage);
		utrsIO.rider.set(covrsurIO.rider);
		utrsIO.planSuffix.set(ZERO);
		utrsIO.unitVirtualFund.set(SPACES);
		utrsIO.unitType.set(SPACES);
		utrsIO.function.set(Varcom.begn);
		ix.set(ZERO);
		wsaaSvFund.set(SPACES);
		
		utrsX200();

		while (isEQ(utrsIO.statuz, Varcom.oK)) {
			if (isEQ(utrsIO.unitVirtualFund, wsaaSvFund)) {
				if(ix.equals(1))
				{
					vpxubblrec.curduntbal01.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(2))
				{
					vpxubblrec.curduntbal02.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(3))
				{
					vpxubblrec.curduntbal03.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(4))
				{
					vpxubblrec.curduntbal04.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(5))
				{
					vpxubblrec.curduntbal05.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(6))
				{
					vpxubblrec.curduntbal06.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(7))
				{
					vpxubblrec.curduntbal07.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(8))
				{
					vpxubblrec.curduntbal08.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(9))
				{
					vpxubblrec.curduntbal09.add(utrsIO.currentDunitBal);
				}
				else if(ix.equals(10))
				{
					vpxubblrec.curduntbal10.add(utrsIO.currentDunitBal);
				}
			} else {
				ix.add(1);
				vpxubblrec.recordCount.add(1);
				/*vpxubblrec.curduntbal[ix.toInt()].set(utrsIO.currentDunitBal);
				vpxubblrec.fund[ix.toInt()].set(utrsIO.unitVirtualFund);
				wsaaSvFund.set(utrsIO.unitVirtualFund);
				vpxubblrec.type[ix.toInt()].set(wsaaType);*/
				wsaaSvFund.set(utrsIO.unitVirtualFund);
				if(ix.equals(1))
				{
					vpxubblrec.curduntbal01.add(utrsIO.currentDunitBal);
					vpxubblrec.fund01.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type01.set(wsaaType);
				}
				else if(ix.equals(2))
				{
					vpxubblrec.curduntbal02.add(utrsIO.currentDunitBal);
					vpxubblrec.fund02.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type02.set(wsaaType);
				}
				else if(ix.equals(3))
				{
					vpxubblrec.curduntbal03.add(utrsIO.currentDunitBal);
					vpxubblrec.fund03.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type03.set(wsaaType);
				}
				else if(ix.equals(4))
				{
					vpxubblrec.curduntbal04.add(utrsIO.currentDunitBal);
					vpxubblrec.fund04.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type04.set(wsaaType);
				}
				else if(ix.equals(5))
				{
					vpxubblrec.curduntbal05.add(utrsIO.currentDunitBal);
					vpxubblrec.fund05.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type05.set(wsaaType);
				}
				else if(ix.equals(6))
				{
					vpxubblrec.curduntbal06.add(utrsIO.currentDunitBal);
					vpxubblrec.fund06.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type06.set(wsaaType);
				}
				else if(ix.equals(7))
				{
					vpxubblrec.curduntbal07.add(utrsIO.currentDunitBal);
					vpxubblrec.fund07.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type07.set(wsaaType);
				}
				else if(ix.equals(8))
				{
					vpxubblrec.curduntbal08.add(utrsIO.currentDunitBal);
					vpxubblrec.fund08.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type08.set(wsaaType);
				}
				else if(ix.equals(9))
				{
					vpxubblrec.curduntbal09.add(utrsIO.currentDunitBal);
					vpxubblrec.fund09.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type09.set(wsaaType);
				}
				else if(ix.equals(10))
				{
					vpxubblrec.curduntbal10.add(utrsIO.currentDunitBal);
					vpxubblrec.fund10.set(utrsIO.unitVirtualFund);					
					vpxubblrec.type10.set(wsaaType);
				}
				
				getCurrB110();
			}
			utrsIO.function.set(Varcom.nextr);
			utrsX200();
		}
	}

	private void getCurrB110() {
		initialize(itdmIO.getDataArea());
		itdmIO.itemtabl.set(t5515);
		itdmIO.itemcoy.set(covrsurIO.chdrcoy);
		itdmIO.itemitem.set(wsaaSvFund);
		itdmIO.itmfrm.set(ubblallpar.effdate);
		itdmIO.function.set(Varcom.begn);
		
		callItdmX400();
		
		t5515rec.t5515Rec.set(itdmIO.genarea);
		//vpxubblrec.currcode[ix.toInt()].set(t5515rec.currcode);
		if(ix.equals(1))
		{
			vpxubblrec.currcode01.set(t5515rec.currcode);
		}
		else if(ix.equals(2))
		{
			vpxubblrec.currcode02.set(t5515rec.currcode);
		}
		else if(ix.equals(3))
		{
			vpxubblrec.currcode03.set(t5515rec.currcode);
		}
		else if(ix.equals(4))
		{
			vpxubblrec.currcode04.set(t5515rec.currcode);
		}
		else if(ix.equals(5))
		{
			vpxubblrec.currcode05.set(t5515rec.currcode);
		}
		else if(ix.equals(6))
		{
			vpxubblrec.currcode06.set(t5515rec.currcode);
		}
		else if(ix.equals(7))
		{
			vpxubblrec.currcode07.set(t5515rec.currcode);
		}
		else if(ix.equals(8))
		{
			vpxubblrec.currcode08.set(t5515rec.currcode);
		}
		else if(ix.equals(9))
		{
			vpxubblrec.currcode09.set(t5515rec.currcode);
		}
		else if(ix.equals(10))
		{
			vpxubblrec.currcode10.set(t5515rec.currcode);
		}
	}

	private void readHitsB200(){
		initialize(hitsIO.getDataArea());
		hitsIO.chdrcoy.set(covrsurIO.chdrcoy);
		hitsIO.chdrnum.set(covrsurIO.chdrnum);
		hitsIO.life.set(covrsurIO.life);
		hitsIO.coverage.set(covrsurIO.coverage);
		hitsIO.rider.set(covrsurIO.rider);
		hitsIO.planSuffix.set(ZERO);
		hitsIO.zintbfnd.set(SPACES);
		hitsIO.function.set(Varcom.begn);
		
		//TODO Baonht added to reset ix
//		ix.set(ZERO);
		
		hitsX300();
		
		while (isEQ(hitsIO.statuz, Varcom.oK)){
			if (isEQ(hitsIO.zintbfnd, wsaaSvFund)) {
				//vpxubblrec.curduntbal[ix.toInt()].add(hitsIO.zcurprmbal);
				if(ix.equals(1))
				{
					vpxubblrec.curduntbal01.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(2))
				{
					vpxubblrec.curduntbal02.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(3))
				{
					vpxubblrec.curduntbal03.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(4))
				{
					vpxubblrec.curduntbal04.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(5))
				{
					vpxubblrec.curduntbal05.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(6))
				{
					vpxubblrec.curduntbal06.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(7))
				{
					vpxubblrec.curduntbal07.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(8))
				{
					vpxubblrec.curduntbal08.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(9))
				{
					vpxubblrec.curduntbal09.add(hitsIO.zcurprmbal);
				}
				else if(ix.equals(10))
				{
					vpxubblrec.curduntbal10.add(hitsIO.zcurprmbal);
				}
			} else {
				ix.add(1);
				vpxubblrec.recordCount.add(1);
				/*vpxubblrec.curduntbal[ix.toInt()].set(hitsIO.zcurprmbal);
				vpxubblrec.fund[ix.toInt()].set(hitsIO.zintbfnd);
				wsaaSvFund.set(hitsIO.zintbfnd);
				vpxubblrec.type[ix.toInt()].set(wsccD);*/
				wsaaSvFund.set(hitsIO.zintbfnd);
				if(ix.equals(1))
				{
					vpxubblrec.curduntbal01.set(hitsIO.zcurprmbal);
					vpxubblrec.fund01.set(hitsIO.zintbfnd);					
					vpxubblrec.type01.set(wsccD);
				}
				else if(ix.equals(2))
				{
					vpxubblrec.curduntbal02.set(hitsIO.zcurprmbal);
					vpxubblrec.fund02.set(hitsIO.zintbfnd);					
					vpxubblrec.type02.set(wsccD);
				}
				else if(ix.equals(3))
				{
					vpxubblrec.curduntbal03.set(hitsIO.zcurprmbal);
					vpxubblrec.fund03.set(hitsIO.zintbfnd);					
					vpxubblrec.type03.set(wsccD);
				}
				else if(ix.equals(4))
				{
					vpxubblrec.curduntbal04.set(hitsIO.zcurprmbal);
					vpxubblrec.fund04.set(hitsIO.zintbfnd);					
					vpxubblrec.type04.set(wsccD);
				}
				else if(ix.equals(5))
				{
					vpxubblrec.curduntbal05.set(hitsIO.zcurprmbal);
					vpxubblrec.fund05.set(hitsIO.zintbfnd);					
					vpxubblrec.type05.set(wsccD);
				}
				else if(ix.equals(6))
				{
					vpxubblrec.curduntbal06.set(hitsIO.zcurprmbal);
					vpxubblrec.fund06.set(hitsIO.zintbfnd);					
					vpxubblrec.type06.set(wsccD);
				}
				else if(ix.equals(7))
				{
					vpxubblrec.curduntbal07.set(hitsIO.zcurprmbal);
					vpxubblrec.fund07.set(hitsIO.zintbfnd);					
					vpxubblrec.type07.set(wsccD);
				}
				else if(ix.equals(8))
				{
					vpxubblrec.curduntbal08.set(hitsIO.zcurprmbal);
					vpxubblrec.fund08.set(hitsIO.zintbfnd);					
					vpxubblrec.type08.set(wsccD);
				}
				else if(ix.equals(9))
				{
					vpxubblrec.curduntbal09.set(hitsIO.zcurprmbal);
					vpxubblrec.fund09.set(hitsIO.zintbfnd);					
					vpxubblrec.type09.set(wsccD);
				}
				else if(ix.equals(10))
				{
					vpxubblrec.curduntbal10.set(hitsIO.zcurprmbal);
					vpxubblrec.fund10.set(hitsIO.zintbfnd);					
					vpxubblrec.type10.set(wsccD);
				}
				getCurrB110();
			}
			hitsIO.function.set(Varcom.nextr);
			hitsX300();
		}
	}

	private void chargesParamsB300() {
//		itdmIO.getDataArea().initialize();
		initialize(itdmIO.getDataArea());
		itdmIO.itemcoy.set(covrsurIO.chdrcoy);
		itdmIO.itemtabl.set(t5551);
		wsaaT5551Crtable.set(covrsurIO.crtable);
		wsaaT5551Currcode.set(ubblallpar.cntcurr);
		itdmIO.itemitem.set(wsaaT5551Item);
		itdmIO.itmfrm.set(covrsurIO.crrcd);
		itdmIO.function.set(Varcom.begn);
		
		callItdmX400();
		
		if(isEQ(itdmIO.statuz , Varcom.oK)
				&& !isEQ(itdmIO.itemitem , wsaaT5551Item)) {
			wsaaT5551Key.set(wsccAsterisk);
			itdmIO.itemitem.set(wsaaT5551Item);//TODO Baonht added to fix
			callItdmX400();
		}
		if(!isEQ(itdmIO.statuz , Varcom.oK)
				|| !isEQ(itdmIO.itemtabl , t5551)
				|| !isEQ(itdmIO.itemitem , wsaaT5551Item)){
			return;
		}else{
			t5551rec.t5551Rec.set(itdmIO.genarea);
		}
		if(isEQ(t5551rec.svcmeth,SPACES)){
			return;
		}
		
		initialize(itemIO.getDataArea());
		itemIO.itemcoy.set(covrsurIO.chdrcoy);
		itemIO.itempfx.set(smtpfxcpy.item);
		itemIO.itemtabl.set(t6598);
		itemIO.itemitem.set(t5551rec.svcmeth);
		itemIO.function.set(Varcom.readr);
		itemIO.format.set(itemrec);
		
		SmartFileCode.execute(appVars, itemIO);
		
		if(!isEQ(itemIO.statuz , Varcom.oK)
				&& !isEQ(itemIO.statuz , Varcom.mrnf)){
			syserrrec.statuz.set(itemIO.statuz);
			syserrrec.params.set(itemIO.getParams());
			dbError999();
		}
		if(isEQ(itemIO.statuz , Varcom.mrnf)){
			t6598rec.t6598Rec.set(SPACES);
		} else {
			t6598rec.t6598Rec.set(itemIO.genarea);
		}
		
		if(!isEQ(t6598rec.calcprog , SPACES)){
			vpxubblrec.chargeCalc.set(itemIO.itemitem);
		}
		if(!isEQ(vpxubblrec.chargeCalc , SPACES)){
			getModalPremB400();
			getPaidPremB500();
		}
	}
	private void getModalPremB400() {
		if(isEQ(covrsurIO.planSuffix , ZERO)) {
			// DO NOTHING
		} else {
			vpxubblrec.modPrem.set(vpxubblrec.singp);
			return;
		}
//		initialize(covrpenIO.getDataArea());
//		covrpenIO.statuz.set(Varcom.oK);
//		covrpenIO.chdrcoy.set(covrsurIO.chdrcoy);
//		covrpenIO.chdrnum.set(covrsurIO.chdrnum);
//		covrpenIO.life.set(covrsurIO.life);
//		covrpenIO.coverage.set(covrsurIO.coverage);
//		covrpenIO.rider.set(covrsurIO.rider);
//		covrpenIO.function.set(Varcom.begn);
//		covrpenIO.format.set(covrpenrec);
		
		covrpf = new Covrpf();
		covrpf.setChdrcoy(covrsurIO.chdrcoy.toString());
		covrpf.setChdrnum(covrsurIO.chdrnum.toString());
		covrpf.setLife(covrsurIO.life.toString());
		covrpf.setCoverage(covrsurIO.coverage.toString());
		covrpf.setRider(covrsurIO.rider.toString());
		//callCovrpenIOX500();
		List<Covrpf> covrpfList = covrpfDAO.getInstPrem(covrsurIO.chdrcoy.toString(), covrsurIO.chdrnum.toString(), covrsurIO.life.toString(), covrsurIO.coverage.toString(), covrsurIO.rider.toString());
		if(covrpfList == null)
		{
			return;
		}
		Iterator<Covrpf> covrpfIT = covrpfList.iterator();
		while (covrpfIT.hasNext()) {
			covrpf =covrpfIT.next();
			compute(vpxubblrec.modPrem, 2).set(add(vpxubblrec.modPrem, covrpf.getInstprem()));
//			covrpenIO.function.set(Varcom.nextr);
//			callCovrpenIOX500();
		}
	}

	private void getPaidPremB500() {
		initialize(itemIO.getDataArea());
		itemIO.statuz.set(Varcom.oK);
		itemIO.itempfx.set(smtpfxcpy.item);
		itemIO.itemcoy.set(covrsurIO.chdrcoy);
		itemIO.itemtabl.set(t5645);
		itemIO.itemitem.set(t6598rec.calcprog);
		itemIO.function.set(Varcom.readr);
		itemIO.format.set(itemrec);

		SmartFileCode.execute(appVars, itemIO);
		if(!isEQ(itemIO.statuz , Varcom.oK)
				&& !isEQ(itemIO.statuz , Varcom.mrnf)){
			syserrrec.statuz.set(itemIO.statuz);
			syserrrec.params.set(itemIO.getParams());
			dbError999();
		}
		if(isEQ(itemIO.statuz , Varcom.oK)){
			t5645rec.t5645Rec.set(itemIO.genarea);
		} else {
			t5645rec.t5645Rec.set(SPACES);
		}

		for (iy.set(1); isLTE(iy, 15) && isNE(t5645rec.sacscode[iy.toInt()], SPACES); iy.add(1)) {
			accumAcblB550();
		}
	}

	private void accumAcblB550() {
		initialize(acblIO.getDataArea());
		acblIO.rldgcoy.set(covrsurIO.chdrcoy);
		acblIO.sacscode.set(t5645rec.sacscode[iy.toInt()]);
		acblIO.origcurr.set(ubblallpar.cntcurr);
		acblIO.sacstyp.set(t5645rec.sacstype[iy.toInt()]);
		if(isEQ(acblIO.sacscode , wsccLe)){
			wsaaRldgChdrnum.set(covrsurIO.chdrnum);
			wsaaRldgLife.set(covrsurIO.life);
			wsaaRldgCoverage.set(covrsurIO.coverage);
			wsaaRldgRider.set(covrsurIO.rider);
			wsaaPlanSuffix.set(covrsurIO.planSuffix);
			wsaaRldgPlnsfx.set(wsaaPlnsfx);
			acblIO.rldgacct.set(wsaaRldgacct);
		} else {
			acblIO.rldgacct.set(covrsurIO.chdrnum);
		}
		acblIO.function.set(Varcom.readr);
		acblIO.format.set(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if(!isEQ(acblIO.statuz , Varcom.oK) && !isEQ(acblIO.statuz , Varcom.mrnf)){
			syserrrec.statuz.set(acblIO.statuz);
			syserrrec.params.set(acblIO.getParams());
			dbError999();
		}
		if(isEQ(acblIO.statuz , Varcom.mrnf)){
			return;
		}

		if(isEQ(t5645rec.sign[iy.toInt()] , wsccNegative)){
			compute(vpxubblrec.paidPrem, 2).set(sub(vpxubblrec.paidPrem, acblIO.sacscurbal));
		}else{
			compute(vpxubblrec.paidPrem, 2).set(add(vpxubblrec.paidPrem, acblIO.sacscurbal));
		}
	}

	
//	private void readLifeX100() {
//		//Read the file
//		lifernlIO.statuz.set(Varcom.oK);
//		lifernlIO.function.set(Varcom.begn);
//		lifernlIO.format.set(lifernlrec);
//		SmartFileCode.execute(appVars, lifernlIO);
//	}
	
	private void utrsX200() {
		utrsIO.format.set(utrsrec);
		SmartFileCode.execute(appVars, utrsIO);
		
		if(!isEQ(utrsIO.statuz , Varcom.oK) 
				&& !isEQ(utrsIO.statuz , Varcom.endp)){
			syserrrec.statuz.set(utrsIO.statuz);
			syserrrec.params.set(utrsIO.getParams());
			dbError999();
		}
		
		if(isEQ(utrsIO.statuz , Varcom.endp)
				|| !isEQ(utrsIO.chdrcoy , covrsurIO.chdrcoy)
				|| !isEQ(utrsIO.chdrnum , covrsurIO.chdrnum)
				|| !isEQ(utrsIO.life , covrsurIO.life)
				|| !isEQ(utrsIO.coverage,covrsurIO.coverage)
				|| !isEQ(utrsIO.rider , covrsurIO.rider)){
			utrsIO.statuz.set(Varcom.endp);
			return;
		}
		if(isEQ(utrsIO.unitType , wsccI)){
			wsaaType.set(wsccI);
		} else {
			wsaaType.set(wsccA);
		}
	}
	
	private void hitsX300() {
		hitsIO.format.set(hitsrec);
		SmartFileCode.execute(appVars, hitsIO);
		
		if(!isEQ(hitsIO.statuz , Varcom.oK)
				&& !isEQ(hitsIO.statuz , Varcom.endp)){
			syserrrec.statuz.set(hitsIO.statuz);
			syserrrec.params.set(hitsIO.getParams());
			dbError999();
		}
		if(isEQ(hitsIO.statuz , Varcom.endp)
				|| !isEQ(hitsIO.chdrcoy , covrsurIO.chdrcoy)
				|| !isEQ(hitsIO.chdrnum , covrsurIO.chdrnum)
				|| !isEQ(hitsIO.life,covrsurIO.life)
				|| !isEQ(hitsIO.coverage , covrsurIO.coverage)
				|| !isEQ(hitsIO.rider , covrsurIO.rider)){
			hitsIO.statuz.set(Varcom.endp);
		}
	}

	private void callItdmX400() {
		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.statuz , Varcom.oK)
				&& !isEQ(itdmIO.statuz , Varcom.endp)){
			syserrrec.params.set(itdmIO.getParams());
			dbError999();
		}
	}
//	private void callCovrpenIOX500 () {
//		SmartFileCode.execute(appVars, covrpenIO);
//		if(!isEQ(covrpenIO.statuz , Varcom.oK) 
//				&& !isEQ(covrpenIO.statuz , Varcom.endp)){
//			syserrrec.statuz.set(covrpenIO.statuz);
//			syserrrec.params.set(covrpenIO.statuz);
//			dbError999();
//		}
//		if(isEQ(covrpenIO.statuz , Varcom.endp)
//				|| !isEQ(covrpenIO.chdrcoy , covrsurIO.chdrcoy)
//				|| !isEQ(covrpenIO.chdrnum , covrsurIO.chdrnum)
//				|| !isEQ(covrpenIO.life , covrsurIO.life)
//				|| !isEQ(covrpenIO.coverage , covrsurIO.coverage)
//				|| !isEQ(covrpenIO.rider , covrsurIO.rider)){
//			covrpenIO.statuz.set(Varcom.endp);
//		}
//	}

	
	private void systemError900() {
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(2);
		callProgram(Syserr.class, syserrrec.syserrRec);
		goTo(GotoLabel.exit000);
	}

    protected void dbError999() {
        syserrrec.syserrType.set("1");
        callProgram(Syserr.class, syserrrec.syserrRec);
        goTo(GotoLabel.exit000);
    }

    protected void exit000()
    {
    	exitProgram();
    }

}