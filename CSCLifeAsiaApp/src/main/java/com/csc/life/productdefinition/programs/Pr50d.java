/*
 * File: Pr50d.java
 * Date: 30 August 2009 1:31:11
 * Author: Quipoz Limited
 *
 * Class transformed from PR50D.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.PnteTableDAM;
import com.csc.life.productdefinition.screens.Sr50dScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    Policy Notes Create
*
***********************************************************************
* </pre>
*/
public class Pr50d extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50D");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaUpdatePtrn = new FixedLengthStringData(1);
	private Validator updatePtrn = new Validator(wsaaUpdatePtrn, "Y");

	private FixedLengthStringData wsaaInforceContract = new FixedLengthStringData(1);
	private Validator inforceContract = new Validator(wsaaInforceContract, "1");

	private FixedLengthStringData wsaaTransactionCode = new FixedLengthStringData(4);
	private Validator policyNotesCreate = new Validator(wsaaTransactionCode, "TR6T");
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDesctabl = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaDescitem = new FixedLengthStringData(8);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(4, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaSystemDate = new ZonedDecimalData(8, 0);
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String tr50f = "TR50F";
		/* FORMATS */
	private static final String chdrrec = "CHDRREC";
	private static final String liferec = "LIFEREC";
	private static final String descrec = "DESCREC";
	private static final String pnterec = "PNTEREC";
	private static final String ptrnrec = "PTRNREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private PnteTableDAM pnteIO = new PnteTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Optswchrec optswchrec = new Optswchrec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sr50dScreenVars sv = ScreenProgram.getScreenVars( Sr50dScreenVars.class);
	private ItemTableDAM itemIO = new ItemTableDAM();
	boolean ispermission = false;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		cont3080,
		exit3090
	}

	public Pr50d() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50d", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		callSr50dio1900();
		scrnparams.subfileRrn.set(1);
		wsaaSeqno.set(ZERO);
		wsaaUpdatePtrn.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaTransactionCode.set(wsaaBatckey.batcBatctrcde);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaSystemDate.set(varcom.vrcmDate);
		wsaaSystemDate.add(20000000);
		initOptswch1100();
		readChdr1200();
		readLife1300();
		sv.chdrnum.set(chdrIO.getChdrnum());
		sv.cnttype.set(chdrIO.getCnttype());
		sv.cownnum.set(chdrIO.getCownnum());
		sv.lifcnum.set(lifeIO.getLifcnum());
		sv.pnotecat.set(wsspUserArea);
		wsaaInforceContract.set(chdrIO.getValidflag());
		if (isEQ(sv.pnotecat,SPACES)) {
			sv.pnotecat.set("UW");
		}
		wsaaDesctabl.set(t5688);
		wsaaDescitem.set(chdrIO.getCnttype());
		readDesc1400();
		sv.ctypdesc.set(descIO.getLongdesc());
		wsaaDesctabl.set(t3623);
		wsaaDescitem.set(chdrIO.getStatcode());
		readDesc1400();
		sv.rstate.set(descIO.getShortdesc());
		wsaaDesctabl.set(t3588);
		wsaaDescitem.set(chdrIO.getPstatcode());
		readDesc1400();
		sv.pstate.set(descIO.getShortdesc());
		wsaaClntnum.set(chdrIO.getCownnum());
		callNamadrs1500();
		sv.jlname01.set(namadrsrec.name);
		wsaaClntnum.set(lifeIO.getLifcnum());
		callNamadrs1500();
		sv.jlname02.set(namadrsrec.name);
		wsaaDesctabl.set(tr50f);
		wsaaDescitem.set(sv.pnotecat);
		readDesc1400();
		sv.desc.set(descIO.getLongdesc());
		loadSubfile1600();
		
		

		ispermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "VOICTX01", appVars, "IT");

		/*--- Read TR2D0 XDS to get the details */

		if (ispermission) 
		{
			sv.feature.set("Y");
		}
		else
		{
			sv.feature.set("N");
		}
	}

protected void initOptswch1100()
	{
		start1110();
	}

protected void start1110()
	{
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsLanguage.set(wsspcomn.language);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsFunction.set(varcom.init);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.params.set(optswchrec.rec);
			fatalError600();
		}
		sv.optdsc.set(optswchrec.optsDsc[1]);
	}

protected void readChdr1200()
	{
		start1210();
	}

protected void start1210()
	{
		chdrIO.setDataKey(SPACES);
		chdrIO.setStatuz(varcom.oK);
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		chdrIO.setChdrnum(wsspcomn.chdrChdrnum);
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM");
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(chdrIO.getStatuz());
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.endp)
		|| isNE(chdrIO.getChdrpfx(),fsupfxcpy.chdr)
		|| isNE(chdrIO.getChdrcoy(),wsspcomn.chdrChdrcoy)
		|| isNE(chdrIO.getChdrnum(),wsspcomn.chdrChdrnum)) {
			chdrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(chdrlnbIO.getStatuz());
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			chdrIO.setChdrnum(chdrlnbIO.getChdrnum());
			chdrIO.setCnttype(chdrlnbIO.getCnttype());
			chdrIO.setCownnum(chdrlnbIO.getCownnum());
			chdrIO.setStatcode(chdrlnbIO.getStatcode());
			chdrIO.setPstatcode(chdrlnbIO.getPstatcode());
			chdrIO.setValidflag(chdrlnbIO.getValidflag());
		}
	}

protected void readLife1300()
	{
		start1310();
	}

protected void start1310()
	{
		lifeIO.setDataKey(SPACES);
		lifeIO.setStatuz(varcom.oK);
		lifeIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		lifeIO.setChdrnum(wsspcomn.chdrChdrnum);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lifeIO.getStatuz());
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.endp)
		|| isNE(lifeIO.getChdrcoy(),wsspcomn.chdrChdrcoy)
		|| isNE(lifeIO.getChdrnum(),wsspcomn.chdrChdrnum)) {
			lifeIO.setLifcnum(SPACES);
		}
	}

protected void readDesc1400()
	{
		start1410();
	}

protected void start1410()
	{
		descIO.setDataKey(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaDesctabl);
		descIO.setDescitem(wsaaDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setShortdesc("??????????");
			descIO.setLongdesc("??????????????????????????????");
		}
	}

protected void callNamadrs1500()
	{
			start1510();
		}

protected void start1510()
	{
		if (isEQ(wsaaClntnum,SPACES)) {
			namadrsrec.name.set(SPACES);
			return ;
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void loadSubfile1600()
	{
		/*START*/
		for (ix.set(1); !(isGT(ix,sv.subfilePage)); ix.add(1)){
			sv.message.set(SPACES);
			sv.word.set(SPACES);
			scrnparams.function.set(varcom.sadd);
			callSr50dio1900();
		} 
		scrnparams.subfileMore.set("Y");
		/*EXIT*/
	}

protected void callSr50dio1900()
	{
		/*START*/
		processScreen("SR50D", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			loadSubfile1600();
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,"SWCH")) {
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsLanguage.set(wsspcomn.language);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelType.set("F");
			optswchrec.optsSelCode.set(scrnparams.statuz);
			optswchrec.optsSelOptno.set(ZERO);
			optswchrec.optsFunction.set("CHCK");
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				syserrrec.statuz.set(optswchrec.optsStatuz);
				syserrrec.params.set(optswchrec.rec);
				fatalError600();
			}
		}
	}

protected void validateScreen2010()
	{
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateDatabase3010();
				case cont3080:
					cont3080();
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.cont3080);
		}
		if (isEQ(scrnparams.statuz,"SWCH")) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.function.set(varcom.sstrt);
		callSr50dio3900();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			writePnte3100();
		}
		 
		if (ispermission) 
		{
			storevoicetext();
		}
		
		if (updatePtrn.isTrue()
		&& inforceContract.isTrue()) {
			writeChdr3200();
			writePtrn3300();
		}
	}

protected void cont3080()
	{
		releaseSoftlock3400();
	}

protected void writePnte3100()
	{
					start3110();
					nextSubfile3180();
				}

protected void start3110()
	{
		if (isEQ(sv.message,SPACES)) {
			return ;
		}
		wsaaSeqno.add(1);
		wsaaUpdatePtrn.set("Y");
		pnteIO.setDataKey(SPACES);
		pnteIO.setStatuz(varcom.oK);
		pnteIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		pnteIO.setChdrnum(wsspcomn.chdrChdrnum);
		pnteIO.setTrandate(wsaaSystemDate);
		pnteIO.setTrantime(varcom.vrcmTime);
		pnteIO.setUserid(wsspcomn.userid);
		pnteIO.setMessage(sv.message);
		pnteIO.setSeqno(wsaaSeqno);
		pnteIO.setPnotecat(sv.pnotecat);
		pnteIO.setFunction(varcom.writr);
		pnteIO.setFormat(pnterec);
		SmartFileCode.execute(appVars, pnteIO);
		if (isNE(pnteIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(pnteIO.getStatuz());
			syserrrec.params.set(pnteIO.getParams());
			fatalError600();
		}
	}


protected void storevoicetext()
{
	/*String[] toppings = new String[10];
	char[] chars=sv.word.getFormData().toString().toCharArray();
	int start=0;
	for(int i=0 ;i<=10; i++)
	{
		toppings[i]=new String();
		toppings[i].getChars(start, 78, chars, 0);
		start=start+78;
	}*/
	
	
	String str = sv.word.getFormData();/* IJTI-1523 */
	//string to char array
	char[] chars = str.toCharArray();
	//System.out.println(chars.length);
	
	int no=(chars.length/75);
	//char at specific index
	//char c = str.charAt(2);
	//System.out.println(c);
	char[] chars1;
	//Copy string characters to char array
	int start=0;
	int end=0;
	if (chars.length<75)
	{
		chars1 = new char[75];		 
		str.getChars(0, chars.length, chars1, 0);
	 	wsaaSeqno.add(1);
		wsaaUpdatePtrn.set("Y");
		pnteIO.setDataKey(SPACES);
		pnteIO.setStatuz(varcom.oK);
		pnteIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		pnteIO.setChdrnum(wsspcomn.chdrChdrnum);
		pnteIO.setTrandate(wsaaSystemDate);
		pnteIO.setTrantime(varcom.vrcmTime);
		pnteIO.setUserid(wsspcomn.userid);
		String smsg = String.valueOf(chars1);
		pnteIO.setMessage(smsg);
		pnteIO.setSeqno(wsaaSeqno);
		pnteIO.setPnotecat(sv.pnotecat);
		pnteIO.setFunction(varcom.writr);
		pnteIO.setFormat(pnterec);
		SmartFileCode.execute(appVars, pnteIO);
		if (isNE(pnteIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(pnteIO.getStatuz());
			syserrrec.params.set(pnteIO.getParams());
			fatalError600();
		}
		
	}
	else
	{
		for(int i=0;i<no;i++)
		{
			    chars1 = new char[75];
				end=start+75;
			    str.getChars(start, end, chars1, 0);
				System.out.println(chars1);
				start=start+75;			 
			 	wsaaSeqno.add(1);
				wsaaUpdatePtrn.set("Y");
				pnteIO.setDataKey(SPACES);
				pnteIO.setStatuz(varcom.oK);
				pnteIO.setChdrcoy(wsspcomn.chdrChdrcoy);
				pnteIO.setChdrnum(wsspcomn.chdrChdrnum);
				pnteIO.setTrandate(wsaaSystemDate);
				pnteIO.setTrantime(varcom.vrcmTime);
				pnteIO.setUserid(wsspcomn.userid);
				String smsg = String.valueOf(chars1);
				pnteIO.setMessage(smsg);
				pnteIO.setSeqno(wsaaSeqno);
				pnteIO.setPnotecat(sv.pnotecat);
				pnteIO.setFunction(varcom.writr);
				pnteIO.setFormat(pnterec);
				SmartFileCode.execute(appVars, pnteIO);
				if (isNE(pnteIO.getStatuz(),varcom.oK)) {
					syserrrec.statuz.set(pnteIO.getStatuz());
					syserrrec.params.set(pnteIO.getParams());
					fatalError600();
				}
		}
		
		if (chars.length>end)
		{
			    chars1 = new char[75];
				end=chars.length;
			    str.getChars(start, end, chars1, 0);
				System.out.println(chars1);				 	 
			 	wsaaSeqno.add(1);
				wsaaUpdatePtrn.set("Y");
				pnteIO.setDataKey(SPACES);
				pnteIO.setStatuz(varcom.oK);
				pnteIO.setChdrcoy(wsspcomn.chdrChdrcoy);
				pnteIO.setChdrnum(wsspcomn.chdrChdrnum);
				pnteIO.setTrandate(wsaaSystemDate);
				pnteIO.setTrantime(varcom.vrcmTime);
				pnteIO.setUserid(wsspcomn.userid);
				String smsg = String.valueOf(chars1);
				pnteIO.setMessage(smsg);
				pnteIO.setSeqno(wsaaSeqno);
				pnteIO.setPnotecat(sv.pnotecat);
				pnteIO.setFunction(varcom.writr);
				pnteIO.setFormat(pnterec);
				SmartFileCode.execute(appVars, pnteIO);
				if (isNE(pnteIO.getStatuz(),varcom.oK)) {
					syserrrec.statuz.set(pnteIO.getStatuz());
					syserrrec.params.set(pnteIO.getParams());
					fatalError600();
				}
		}
		
	}
	
	
}
protected void nextSubfile3180()
	{
		scrnparams.function.set(varcom.srdn);
		callSr50dio3900();
		/*EXIT*/
	}

protected void writeChdr3200()
	{
		/*START*/
		setPrecision(chdrIO.getTranno(), 0);
		chdrIO.setTranno(add(chdrIO.getTranno(),1));
		chdrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrIO.getStatuz());
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writePtrn3300()
	{
		start3310();
	}

protected void start3310()
	{
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setStatuz(varcom.oK);
		ptrnIO.setChdrpfx(fsupfxcpy.chdr);
		ptrnIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		ptrnIO.setChdrnum(wsspcomn.chdrChdrnum);
		ptrnIO.setTranno(chdrIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);
		ptrnIO.setBatcpfx(wsaaBatckey.batcBatcpfx);
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setValidflag("1");
		
		//ILIFE-6511 Start Transaction date update
		ptrnIO.setDatesub(wsaaToday);
		//ILIFE-6511 end
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void releaseSoftlock3400()
	{
			start3410();
		}

protected void start3410()
	{
		if (!policyNotesCreate.isTrue()) {
			return ;
		}
		initialize(sftlockrec.sftlockRec);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.entity.set(wsspcomn.chdrChdrnum);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void callSr50dio3900()
	{
		/*START*/
		processScreen("SR50D", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
					nextProgram4010();
					switch4070();
				}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"SWCH")) {
			optswchrec.optsSelType.set("F");
			optswchrec.optsSelCode.set(scrnparams.statuz);
			optswchrec.optsSelOptno.set(ZERO);
			return ;
		}
	}

protected void switch4070()
	{
		optswchrec.optsFunction.set(varcom.stck);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.params.set(optswchrec.rec);
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		/*EXIT*/
	}
}
