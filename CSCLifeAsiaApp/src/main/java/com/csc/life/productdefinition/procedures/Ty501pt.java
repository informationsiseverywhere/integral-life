/*
 * File: Ty501pt.java
 * Date: 30 August 2009 2:25:55
 * Author: Quipoz Limited
 * 
 * Class transformed from Ty501PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Ty501rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR Ty501.
*
*
*****************************************************************
* </pre>
*/
public class Ty501pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(51).isAPartOf(wsaaPrtLine001, 25, FILLER).init("Component Alternate Statuses For Transactions           SY501");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(73);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 43);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(75);
	private FixedLengthStringData filler7 = new FixedLengthStringData(75).isAPartOf(wsaaPrtLine003, 0, FILLER).init("Status Item               Current Status Code  Set status to");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler8 = new FixedLengthStringData(62).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 62, FILLER).init("Risk   Premium");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(78);
	private FixedLengthStringData filler10 = new FixedLengthStringData(68).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 68, FILLER).init("Reg Single");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(65);
	private FixedLengthStringData filler12 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine006, 0, FILLER).init("Contract Risk");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 21);
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 24);
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 27);
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 30);
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 36);
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 39);
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 42);
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 45);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 48);
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 51);
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 54);
	private FixedLengthStringData filler24 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 63);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(76);
	private FixedLengthStringData filler25 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 0, FILLER).init("Contract Premium");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 21);
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 24);
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 27);
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 30);
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 36);
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 39);
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 42);
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 45);
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 48);
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 54);
	private FixedLengthStringData filler37 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 69);
	private FixedLengthStringData filler38 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 74);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(65);
	private FixedLengthStringData filler39 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine008, 0, FILLER).init("Life");
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 21);
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 24);
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 27);
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 30);
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 36);
	private FixedLengthStringData filler45 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 63);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(65);
	private FixedLengthStringData filler46 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine009, 0, FILLER).init("Joint Life");
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 21);
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 24);
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 27);
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 30);
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 36);
	private FixedLengthStringData filler52 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 63);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(65);
	private FixedLengthStringData filler53 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine010, 0, FILLER).init("Coverage Risk");
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 21);
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 24);
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 27);
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 30);
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 36);
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 39);
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 42);
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 45);
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 48);
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 54);
	private FixedLengthStringData filler65 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 63);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(76);
	private FixedLengthStringData filler66 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 0, FILLER).init("Coverage Premium");
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 21);
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 24);
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 27);
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 30);
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 36);
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 39);
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 42);
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 45);
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 48);
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 51);
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 54);
	private FixedLengthStringData filler78 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 69);
	private FixedLengthStringData filler79 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 74);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(65);
	private FixedLengthStringData filler80 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 0, FILLER).init("Rider Risk");
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 21);
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 24);
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 27);
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 30);
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 33);
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 36);
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 39);
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 42);
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 45);
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 48);
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 51);
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 54);
	private FixedLengthStringData filler92 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 63);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(76);
	private FixedLengthStringData filler93 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine013, 0, FILLER).init("Rider Premium");
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 21);
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 24);
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 27);
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 30);
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 33);
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 36);
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 39);
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo093 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 42);
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 45);
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 48);
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 51);
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 54);
	private FixedLengthStringData filler105 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 69);
	private FixedLengthStringData filler106 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 74);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Ty501rec ty501rec = new Ty501rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Ty501pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		ty501rec.ty501Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(ty501rec.cnRiskStat01);
		fieldNo006.set(ty501rec.cnRiskStat02);
		fieldNo007.set(ty501rec.cnRiskStat03);
		fieldNo008.set(ty501rec.cnRiskStat04);
		fieldNo009.set(ty501rec.cnRiskStat05);
		fieldNo010.set(ty501rec.cnRiskStat06);
		fieldNo011.set(ty501rec.cnRiskStat07);
		fieldNo012.set(ty501rec.cnRiskStat08);
		fieldNo013.set(ty501rec.cnRiskStat09);
		fieldNo014.set(ty501rec.cnRiskStat10);
		fieldNo015.set(ty501rec.cnRiskStat11);
		fieldNo016.set(ty501rec.cnRiskStat12);
		fieldNo017.set(ty501rec.setCnRiskStat);
		fieldNo018.set(ty501rec.cnPremStat01);
		fieldNo019.set(ty501rec.cnPremStat02);
		fieldNo020.set(ty501rec.cnPremStat03);
		fieldNo021.set(ty501rec.cnPremStat04);
		fieldNo022.set(ty501rec.cnPremStat05);
		fieldNo023.set(ty501rec.cnPremStat06);
		fieldNo024.set(ty501rec.cnPremStat07);
		fieldNo025.set(ty501rec.cnPremStat08);
		fieldNo026.set(ty501rec.cnPremStat09);
		fieldNo027.set(ty501rec.cnPremStat10);
		fieldNo028.set(ty501rec.cnPremStat11);
		fieldNo029.set(ty501rec.cnPremStat12);
		fieldNo030.set(ty501rec.setCnPremStat);
		fieldNo031.set(ty501rec.setSngpCnStat);
		fieldNo032.set(ty501rec.lifeStat01);
		fieldNo033.set(ty501rec.lifeStat02);
		fieldNo034.set(ty501rec.lifeStat03);
		fieldNo035.set(ty501rec.lifeStat04);
		fieldNo036.set(ty501rec.lifeStat05);
		fieldNo037.set(ty501rec.lifeStat06);
		fieldNo038.set(ty501rec.setLifeStat);
		fieldNo039.set(ty501rec.jlifeStat01);
		fieldNo040.set(ty501rec.jlifeStat02);
		fieldNo041.set(ty501rec.jlifeStat03);
		fieldNo042.set(ty501rec.jlifeStat04);
		fieldNo043.set(ty501rec.jlifeStat05);
		fieldNo044.set(ty501rec.jlifeStat06);
		fieldNo045.set(ty501rec.setJlifeStat);
		fieldNo046.set(ty501rec.covRiskStat01);
		fieldNo047.set(ty501rec.covRiskStat02);
		fieldNo048.set(ty501rec.covRiskStat03);
		fieldNo049.set(ty501rec.covRiskStat04);
		fieldNo050.set(ty501rec.covRiskStat05);
		fieldNo051.set(ty501rec.covRiskStat06);
		fieldNo052.set(ty501rec.covRiskStat07);
		fieldNo053.set(ty501rec.covRiskStat08);
		fieldNo054.set(ty501rec.covRiskStat09);
		fieldNo055.set(ty501rec.covRiskStat10);
		fieldNo056.set(ty501rec.covRiskStat11);
		fieldNo057.set(ty501rec.covRiskStat12);
		fieldNo058.set(ty501rec.setCovRiskStat);
		fieldNo059.set(ty501rec.covPremStat01);
		fieldNo060.set(ty501rec.covPremStat02);
		fieldNo061.set(ty501rec.covPremStat03);
		fieldNo062.set(ty501rec.covPremStat04);
		fieldNo063.set(ty501rec.covPremStat05);
		fieldNo064.set(ty501rec.covPremStat06);
		fieldNo065.set(ty501rec.covPremStat07);
		fieldNo066.set(ty501rec.covPremStat08);
		fieldNo067.set(ty501rec.covPremStat09);
		fieldNo068.set(ty501rec.covPremStat10);
		fieldNo069.set(ty501rec.covPremStat11);
		fieldNo070.set(ty501rec.covPremStat12);
		fieldNo071.set(ty501rec.setCovPremStat);
		fieldNo072.set(ty501rec.setSngpCovStat);
		fieldNo073.set(ty501rec.ridRiskStat01);
		fieldNo074.set(ty501rec.ridRiskStat02);
		fieldNo075.set(ty501rec.ridRiskStat03);
		fieldNo076.set(ty501rec.ridRiskStat04);
		fieldNo077.set(ty501rec.ridRiskStat05);
		fieldNo078.set(ty501rec.ridRiskStat06);
		fieldNo079.set(ty501rec.ridRiskStat07);
		fieldNo080.set(ty501rec.ridRiskStat08);
		fieldNo081.set(ty501rec.ridRiskStat09);
		fieldNo082.set(ty501rec.ridRiskStat10);
		fieldNo083.set(ty501rec.ridRiskStat11);
		fieldNo084.set(ty501rec.ridRiskStat12);
		fieldNo085.set(ty501rec.setRidRiskStat);
		fieldNo086.set(ty501rec.ridPremStat01);
		fieldNo087.set(ty501rec.ridPremStat02);
		fieldNo088.set(ty501rec.ridPremStat03);
		fieldNo089.set(ty501rec.ridPremStat04);
		fieldNo090.set(ty501rec.ridPremStat05);
		fieldNo091.set(ty501rec.ridPremStat06);
		fieldNo092.set(ty501rec.ridPremStat07);
		fieldNo093.set(ty501rec.ridPremStat08);
		fieldNo094.set(ty501rec.ridPremStat09);
		fieldNo095.set(ty501rec.ridPremStat10);
		fieldNo096.set(ty501rec.ridPremStat11);
		fieldNo097.set(ty501rec.ridPremStat12);
		fieldNo098.set(ty501rec.setRidPremStat);
		fieldNo099.set(ty501rec.setSngpRidStat);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
