package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:44
 * Description:
 * Copybook name: MLIAAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mliaafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mliaafiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mliaafiKey = new FixedLengthStringData(64).isAPartOf(mliaafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData mliaafiSecurityno = new FixedLengthStringData(15).isAPartOf(mliaafiKey, 0);
  	public FixedLengthStringData mliaafiMlentity = new FixedLengthStringData(16).isAPartOf(mliaafiKey, 15);
  	public FixedLengthStringData mliaafiActn = new FixedLengthStringData(1).isAPartOf(mliaafiKey, 31);
  	public PackedDecimalData mliaafiEffdate = new PackedDecimalData(8, 0).isAPartOf(mliaafiKey, 32);
  	public FixedLengthStringData filler = new FixedLengthStringData(27).isAPartOf(mliaafiKey, 37, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mliaafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mliaafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}