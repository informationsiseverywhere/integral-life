package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:43
 * Description:
 * Copybook name: MEDIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Medikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mediFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mediKey = new FixedLengthStringData(256).isAPartOf(mediFileKey, 0, REDEFINE);
  	public FixedLengthStringData mediChdrcoy = new FixedLengthStringData(1).isAPartOf(mediKey, 0);
  	public FixedLengthStringData mediChdrnum = new FixedLengthStringData(8).isAPartOf(mediKey, 1);
  	public PackedDecimalData mediSeqno = new PackedDecimalData(2, 0).isAPartOf(mediKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(mediKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mediFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mediFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}