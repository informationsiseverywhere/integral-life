/*********************  */
/*Author  :Liwei						*/
/*Purpose :Instead of subroutine rlpdlon*/
/*Date    :2018.10.26					*/
package com.csc.life.productdefinition.procedures;

public interface RlpdlonUtils {
	public void callRlpdlon(RlpdlonPojo rlpdlonPojo);
}
