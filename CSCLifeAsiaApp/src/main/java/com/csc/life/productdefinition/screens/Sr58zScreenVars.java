package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
//import com.csc.fsu.common.CustomDD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5689
 * @version 1.0 generated on 17/08/2016 06:49
 * @author CSCUSER
 */
public class Sr58zScreenVars extends SmartVarModel { 

	

	public FixedLengthStringData dataArea = new FixedLengthStringData(696);
	public FixedLengthStringData dataFields = new FixedLengthStringData(120).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreqs = new FixedLengthStringData(60).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(30, 2, billfreqs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01 = DD.zaftitem.copy().isAPartOf(filler,0);
	public FixedLengthStringData billfreq02 = DD.zaftitem.copy().isAPartOf(filler,2);
	public FixedLengthStringData billfreq03 = DD.zaftitem.copy().isAPartOf(filler,4);
	public FixedLengthStringData billfreq04 = DD.zaftitem.copy().isAPartOf(filler,6);
	public FixedLengthStringData billfreq05 = DD.zaftitem.copy().isAPartOf(filler,8);
	public FixedLengthStringData billfreq06 = DD.zaftitem.copy().isAPartOf(filler,10);
	public FixedLengthStringData billfreq07 = DD.zaftitem.copy().isAPartOf(filler,12);
	public FixedLengthStringData billfreq08 = DD.zaftitem.copy().isAPartOf(filler,14);
	public FixedLengthStringData billfreq09 = DD.zaftitem.copy().isAPartOf(filler,16);
	public FixedLengthStringData billfreq10 = DD.zaftitem.copy().isAPartOf(filler,18);
	public FixedLengthStringData billfreq11 = DD.zaftitem.copy().isAPartOf(filler,20);
	public FixedLengthStringData billfreq12 = DD.zaftitem.copy().isAPartOf(filler,22);
	public FixedLengthStringData billfreq13 = DD.zaftitem.copy().isAPartOf(filler,24);
	public FixedLengthStringData billfreq14 = DD.zaftitem.copy().isAPartOf(filler,26);
	public FixedLengthStringData billfreq15 = DD.zaftitem.copy().isAPartOf(filler,28);
	public FixedLengthStringData billfreq16 = DD.zaftitem.copy().isAPartOf(filler,30);
	public FixedLengthStringData billfreq17 = DD.zaftitem.copy().isAPartOf(filler,32);
	public FixedLengthStringData billfreq18 = DD.zaftitem.copy().isAPartOf(filler,34);
	public FixedLengthStringData billfreq19 = DD.zaftitem.copy().isAPartOf(filler,36);
	public FixedLengthStringData billfreq20 = DD.zaftitem.copy().isAPartOf(filler,38);
	public FixedLengthStringData billfreq21 = DD.zaftitem.copy().isAPartOf(filler,40);
	public FixedLengthStringData billfreq22 = DD.zaftitem.copy().isAPartOf(filler,42);
	public FixedLengthStringData billfreq23 = DD.zaftitem.copy().isAPartOf(filler,44);
	public FixedLengthStringData billfreq24 = DD.zaftitem.copy().isAPartOf(filler,46);
	public FixedLengthStringData billfreq25 = DD.zaftitem.copy().isAPartOf(filler,48);
	public FixedLengthStringData billfreq26 = DD.zaftitem.copy().isAPartOf(filler,50);
	public FixedLengthStringData billfreq27 = DD.zaftitem.copy().isAPartOf(filler,52);
	public FixedLengthStringData billfreq28 = DD.zaftitem.copy().isAPartOf(filler,54);
	public FixedLengthStringData billfreq29 = DD.zaftitem.copy().isAPartOf(filler,56);
	public FixedLengthStringData billfreq30 = DD.zaftitem.copy().isAPartOf(filler,58);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,61);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,69);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,77);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,115);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 120);
	public FixedLengthStringData billfreqsErr = new FixedLengthStringData(120).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] billfreqErr = FLSArrayPartOfStructure(30, 4, billfreqsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(120).isAPartOf(billfreqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData billfreq02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData billfreq03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData billfreq04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData billfreq05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData billfreq06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData billfreq07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData billfreq08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData billfreq09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData billfreq10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData billfreq11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData billfreq12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData billfreq13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData billfreq14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData billfreq15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData billfreq16Err = new FixedLengthStringData(4).isAPartOf(filler3, 60);
	public FixedLengthStringData billfreq17Err = new FixedLengthStringData(4).isAPartOf(filler3, 64);
	public FixedLengthStringData billfreq18Err = new FixedLengthStringData(4).isAPartOf(filler3, 68);
	public FixedLengthStringData billfreq19Err = new FixedLengthStringData(4).isAPartOf(filler3, 72);
	public FixedLengthStringData billfreq20Err = new FixedLengthStringData(4).isAPartOf(filler3, 76);
	public FixedLengthStringData billfreq21Err = new FixedLengthStringData(4).isAPartOf(filler3, 80);
	public FixedLengthStringData billfreq22Err = new FixedLengthStringData(4).isAPartOf(filler3, 84);
	public FixedLengthStringData billfreq23Err = new FixedLengthStringData(4).isAPartOf(filler3, 88);
	public FixedLengthStringData billfreq24Err = new FixedLengthStringData(4).isAPartOf(filler3, 92);
	public FixedLengthStringData billfreq25Err = new FixedLengthStringData(4).isAPartOf(filler3, 96);
	public FixedLengthStringData billfreq26Err = new FixedLengthStringData(4).isAPartOf(filler3, 100);
	public FixedLengthStringData billfreq27Err = new FixedLengthStringData(4).isAPartOf(filler3, 104);
	public FixedLengthStringData billfreq28Err = new FixedLengthStringData(4).isAPartOf(filler3, 108);
	public FixedLengthStringData billfreq29Err = new FixedLengthStringData(4).isAPartOf(filler3, 112);
	public FixedLengthStringData billfreq30Err = new FixedLengthStringData(4).isAPartOf(filler3, 116);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(432).isAPartOf(dataArea, 264);
	public FixedLengthStringData billfreqsOut = new FixedLengthStringData(360).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(30, 12, billfreqsOut, 0);
	public FixedLengthStringData[][] billfreqO = FLSDArrayPartOfArrayStructure(12, 1, billfreqOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(360).isAPartOf(billfreqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] billfreq01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] billfreq02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] billfreq03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] billfreq04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] billfreq05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] billfreq06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] billfreq07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] billfreq08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] billfreq09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] billfreq10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] billfreq11Out = FLSArrayPartOfStructure(12, 1, filler6, 120);
	public FixedLengthStringData[] billfreq12Out = FLSArrayPartOfStructure(12, 1, filler6, 132);
	public FixedLengthStringData[] billfreq13Out = FLSArrayPartOfStructure(12, 1, filler6, 144);
	public FixedLengthStringData[] billfreq14Out = FLSArrayPartOfStructure(12, 1, filler6, 156);
	public FixedLengthStringData[] billfreq15Out = FLSArrayPartOfStructure(12, 1, filler6, 168);
	public FixedLengthStringData[] billfreq16Out = FLSArrayPartOfStructure(12, 1, filler6, 180);
	public FixedLengthStringData[] billfreq17Out = FLSArrayPartOfStructure(12, 1, filler6, 192);
	public FixedLengthStringData[] billfreq18Out = FLSArrayPartOfStructure(12, 1, filler6, 204);
	public FixedLengthStringData[] billfreq19Out = FLSArrayPartOfStructure(12, 1, filler6, 216);
	public FixedLengthStringData[] billfreq20Out = FLSArrayPartOfStructure(12, 1, filler6, 228);
	public FixedLengthStringData[] billfreq21Out = FLSArrayPartOfStructure(12, 1, filler6, 240);
	public FixedLengthStringData[] billfreq22Out = FLSArrayPartOfStructure(12, 1, filler6, 252);
	public FixedLengthStringData[] billfreq23Out = FLSArrayPartOfStructure(12, 1, filler6, 264);
	public FixedLengthStringData[] billfreq24Out = FLSArrayPartOfStructure(12, 1, filler6, 276);
	public FixedLengthStringData[] billfreq25Out = FLSArrayPartOfStructure(12, 1, filler6, 288);
	public FixedLengthStringData[] billfreq26Out = FLSArrayPartOfStructure(12, 1, filler6, 300);
	public FixedLengthStringData[] billfreq27Out = FLSArrayPartOfStructure(12, 1, filler6, 312);
	public FixedLengthStringData[] billfreq28Out = FLSArrayPartOfStructure(12, 1, filler6, 324);
	public FixedLengthStringData[] billfreq29Out = FLSArrayPartOfStructure(12, 1, filler6, 336);
	public FixedLengthStringData[] billfreq30Out = FLSArrayPartOfStructure(12, 1, filler6, 348);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr58zscreenWritten = new LongData(0);
	public LongData Sr58zprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr58zScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(billfreq01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq03Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq04Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq05Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq06Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq07Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq08Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq09Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq10Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq11Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq12Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq13Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq14Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq15Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq16Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq17Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq18Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq19Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq20Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq21Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq22Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq23Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq24Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq25Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq26Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq27Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq28Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq29Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq30Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, billfreq01, billfreq02, billfreq03, billfreq04, billfreq05, billfreq06, billfreq07, billfreq08, billfreq09, billfreq10, billfreq11, billfreq12, billfreq13, billfreq14, billfreq15, billfreq16, billfreq17, billfreq18, billfreq19, billfreq20, billfreq21, billfreq22, billfreq23, billfreq24, billfreq25, billfreq26, billfreq27, billfreq28, billfreq29, billfreq30};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, billfreq01Out, billfreq02Out, billfreq03Out, billfreq04Out, billfreq05Out, billfreq06Out, billfreq07Out, billfreq08Out, billfreq09Out, billfreq10Out, billfreq11Out, billfreq12Out, billfreq13Out, billfreq14Out, billfreq15Out, billfreq16Out, billfreq17Out, billfreq18Out, billfreq19Out, billfreq20Out, billfreq21Out, billfreq22Out, billfreq23Out, billfreq24Out, billfreq25Out, billfreq26Out, billfreq27Out, billfreq28Out, billfreq29Out, billfreq30Out};
		screenErrFields = new BaseData[]   {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, billfreq01Err, billfreq02Err, billfreq03Err, billfreq04Err, billfreq05Err, billfreq06Err, billfreq07Err, billfreq08Err, billfreq09Err, billfreq10Err, billfreq11Err, billfreq12Err, billfreq13Err, billfreq14Err, billfreq15Err, billfreq16Err, billfreq17Err, billfreq18Err, billfreq19Err, billfreq20Err, billfreq21Err, billfreq22Err, billfreq23Err, billfreq24Err, billfreq25Err, billfreq26Err, billfreq27Err, billfreq28Err, billfreq29Err, billfreq30Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr58zscreen.class;
		protectRecord = Sr58zprotect.class;
	}

}
