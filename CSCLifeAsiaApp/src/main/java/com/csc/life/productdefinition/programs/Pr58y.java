/*
 * File: Pr58y.java
 * Date: 07 August 2016 2:00:25
 * Author: CSC Limited
 * 
 * Class transformed from Pr58y
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.life.productdefinition.tablestructures.Tr58yrec;
import com.csc.life.productdefinition.procedures.Tr58ypt;
import com.csc.life.productdefinition.screens.Sr58yScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* AGED BASED NON-ANNUAL PREMIUM RATES
*
*
*****************************************************************
* </pre>
*/
public class Pr58y extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR58Y");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr58yrec Tr58yrec = new Tr58yrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr58yScreenVars sv = ScreenProgram.getScreenVars( Sr58yScreenVars.class);
	private static final String af01 = "AFR1";
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Pr58y() {
		super();
		screenVars = sv;
		new ScreenModel("Sr58y", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setParams(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

	protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

	protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		sv.longdesc.set(SPACES);
	}
		Tr58yrec.tr58yrec.set(itemIO.getGenarea());
	if (isNE(itemIO.getGenarea(),SPACES)) {
		goTo(GotoLabel.generalArea1045);
	}
		Tr58yrec.unitPremPercent01.set(ZERO);
		Tr58yrec.unitPremPercent02.set(ZERO);
		Tr58yrec.unitPremPercent03.set(ZERO);
		Tr58yrec.unitPremPercent04.set(ZERO);
		Tr58yrec.unitPremPercent05.set(ZERO);
		Tr58yrec.unitPremPercent06.set(ZERO);
		Tr58yrec.unitPremPercent07.set(ZERO);
		Tr58yrec.unitPremPercent08.set(ZERO);
		Tr58yrec.unitPremPercent09.set(ZERO);
		Tr58yrec.unitPremPercent10.set(ZERO);
	}


	protected void generalArea1045()
	{
		
		sv.ageIssageFrms.set(Tr58yrec.ageIssageFrms);
		sv.ageIssageTos.set(Tr58yrec.ageIssageTos);
		sv.unitVirtualFunds.set(Tr58yrec.unitVirtualFunds);
		sv.unitPremPercents.set(Tr58yrec.unitPremPercents);
		}

	protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

	protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}
	protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		int allco = 0;
		for(int  i =0;i <9; i++){
			if(sv.ageIssageFrm[i+1].toString()!="" && sv.ageIssageTo[i+1].toString()!=""
				&& sv.ageIssageTo[i+1].toInt()>0){
				for(int j =1; j<=5 ; j++){
					if(!sv.unitVirtualFund[(i*5)+j].toString().trim().equals("")){
						allco = allco + sv.unitPremPercent[(i*5)+j].toInt();
					}
				}
				if(allco != 100){
					wsspcomn.edterror.set("Y");
					syserrrec.statuz.set(af01);
					sv.vrtfndErr[i+1].set(af01);
					break;
				}
				allco =0;
			}
		}
		/*OTHER*/
	}

	protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	protected void update3000()
	{
		try {
			preparation3010();
			updatePrimaryRecord3050();
			updateRecord3055();
		}
		catch (GOTOException e){
		}
	}

	protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
	}

	protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

	protected void updateRecord3055()
	{
		checkChanges3100();
		itemIO.setGenarea(Tr58yrec.tr58yrec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*OTHER*/
	}

	protected void checkChanges3100()
	{
		check3100();
	}

	protected void check3100()
	{
		Tr58yrec.ageIssageFrms.set(sv.ageIssageFrms);
		Tr58yrec.ageIssageTos.set(sv.ageIssageTos);
		Tr58yrec.unitVirtualFunds.set(sv.unitVirtualFunds);
		Tr58yrec.unitPremPercents.set(sv.unitPremPercents);
	}

	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
	}
