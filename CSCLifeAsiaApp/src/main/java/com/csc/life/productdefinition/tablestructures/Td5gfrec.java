package com.csc.life.productdefinition.tablestructures;


import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Td5gfrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData td5gfRec = new FixedLengthStringData(41);
  
  	public FixedLengthStringData ratebas = new FixedLengthStringData(1).isAPartOf(td5gfRec, 0); 
  	
  	public FixedLengthStringData gender = new FixedLengthStringData(4).isAPartOf(td5gfRec, 1);
 	public FixedLengthStringData[] gnder = FLSArrayPartOfStructure(4, 1, gender, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(gender, 0, FILLER_REDEFINE);
  	public FixedLengthStringData gender01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData gender02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData gender03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData gender04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	
  	public FixedLengthStringData frmages = new FixedLengthStringData(12).isAPartOf(td5gfRec, 5);
  	public ZonedDecimalData[] frmage = ZDArrayPartOfStructure(4, 3, 0, frmages, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(frmages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData frmage01 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData frmage02 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 3);
  	public ZonedDecimalData frmage03 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData frmage04 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 9);
   	
  	public FixedLengthStringData toages = new FixedLengthStringData(12).isAPartOf(td5gfRec, 17);
  	public ZonedDecimalData[] toage = ZDArrayPartOfStructure(4, 3, 0, toages, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(toages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData toage01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData toage02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData toage03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData toage04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);  	
  	
  	public FixedLengthStringData cooloff = new FixedLengthStringData(12).isAPartOf(td5gfRec, 29);
  	public ZonedDecimalData[] coolingoff = ZDArrayPartOfStructure(4, 3, 0, cooloff, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(12).isAPartOf(cooloff, 0, FILLER_REDEFINE);
  	public ZonedDecimalData coolingoff1 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData coolingoff2 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 3);
  	public ZonedDecimalData coolingoff3 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 6);
  	public ZonedDecimalData coolingoff4 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 9);

	public void initialize() {
		COBOLFunctions.initialize(td5gfRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			td5gfRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}