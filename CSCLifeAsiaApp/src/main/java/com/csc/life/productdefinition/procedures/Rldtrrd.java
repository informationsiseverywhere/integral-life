/*
 * File: Rldtrrd.java
 * Date: 30 August 2009 2:13:00
 * Author: Quipoz Limited
 *
 * Class transformed from RLDTRRD.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.newbusiness.recordstructures.Rlrdtrmrec;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  A Subroutine to calculate the benefit schedule for Decreasing
*  Term Rider for Annual Rest.
*
*  Logic:
*  . Read TH615 with rest indicator to determine if automatic
*    calculation of benefit schedule is required.
*
*  . If interest rate PRAT is captured in MRTA, use this formula
*      define  VJ = 1 / (1 + (PRAT / 100))
*      loop index for the duration of risk term,
*      derive P = Risk Term - index + 1
*      derive Int Rate = ((1- VJ exp P) / (1 - VJ exp Risk Term))
*         Reduced SA = SA * Int Rate
*
*  . If interest rate is NOT captured in MRTA, the Reduced SA is
*      gradually scaling down for the duration of risk term.
*      loop index for the duration of risk term,
*         Reduced SA = ((Risk Term - index + 1) / Risk Term) * SA
*
*  . Each calculated Reduced SA is written to MBNS.
*
*****************************************************************
* </pre>
*/
public class Rldtrrd extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRiskTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaIntins = new PackedDecimalData(6, 3).setUnsigned();
	private PackedDecimalData wsaaSumins1 = new PackedDecimalData(17, 7).setUnsigned();
	private PackedDecimalData wsaaRdtsum = new PackedDecimalData(15, 0).setUnsigned();
	private PackedDecimalData wsaaVj = new PackedDecimalData(10, 7);
	private PackedDecimalData wsaaP = new PackedDecimalData(15, 7);
		/* TABLES */
	private String th615 = "TH615";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String mrtarec = "MRTAREC";
	private String mbnsrec = "MBNSREC";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Benefit Schedule Details*/
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
		/*Reducing Term Assurance details*/
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private Rlrdtrmrec rlrdtrmrec = new Rlrdtrmrec();
	private Th615rec th615rec = new Th615rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit090
	}

	public Rldtrrd() {
		super();
	}

public void mainline(Object... parmArray)
	{
		rlrdtrmrec.rlrdtrmRec = convertAndSetParam(rlrdtrmrec.rlrdtrmRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		initialize(wsaaFlag);
		initialize(wsaaIndex);
		initialize(wsaaSumins);
		initialize(wsaaSumins1);
		initialize(wsaaIntins);
		initialize(wsaaRdtsum);
		initialize(wsaaVj);
		initialize(wsaaP);
		initialize(wsaaRiskTerm);
		rlrdtrmrec.statuz.set(varcom.oK);
		readMrta300();
		if (isEQ(wsaaFlag,"Y")) {
			goTo(GotoLabel.exit090);
		}
		readTh615Table200();
		if (isNE(rlrdtrmrec.statuz,varcom.oK)
		|| isEQ(rlrdtrmrec.statuz,varcom.endp)
		|| isEQ(rlrdtrmrec.statuz,varcom.mrnf)) {
			rlrdtrmrec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit090);
		}
		if (isEQ(th615rec.premind,"Y")) {
			goTo(GotoLabel.exit090);
		}
		wsaaRiskTerm.set(rlrdtrmrec.riskTerm);
		if (isEQ(mrtaIO.getPrat(),ZERO)) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaRiskTerm)); wsaaIndex.add(1)){
				calc400();
			}
		}
		else {
			assign500();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTh615Table200()
	{
		/*START*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rlrdtrmrec.chdrcoy);
		itemIO.setItemtabl(th615);
		itemIO.setItemitem(mrtaIO.getMlresind());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			rlrdtrmrec.statuz.set(itemIO.getStatuz());
		}
		th615rec.th615Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void readMrta300()
	{
		/*START*/
		mrtaIO.setChdrcoy(rlrdtrmrec.chdrcoy);
		mrtaIO.setChdrnum(rlrdtrmrec.chdrnum);
		mrtaIO.setFormat(mrtarec);
		mrtaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mrtaIO);
		if (isNE(mrtaIO.getStatuz(),varcom.oK)) {
			wsaaFlag.set("Y");
		}
		/*EXIT*/
	}

protected void calc400()
	{
		/*START*/
		compute(wsaaIntins, 4).setRounded((div((add(sub(rlrdtrmrec.riskTerm,wsaaIndex),1)),rlrdtrmrec.riskTerm)));
		compute(wsaaSumins, 4).setRounded(mult(10000,wsaaIntins));
		compute(wsaaSumins1, 7).set(div(wsaaSumins,10000));
		compute(wsaaRdtsum, 7).set(mult(rlrdtrmrec.sumins,wsaaSumins1));
		write600();
		/*EXIT*/
	}

protected void assign500()
	{
		/*START*/
		compute(wsaaVj, 7).set(div(1,(add(1,(div(mrtaIO.getPrat(),100))))));
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaRiskTerm)); wsaaIndex.add(1)){
			calc550();
		}
		/*EXIT*/
	}

protected void calc550()
	{
		/*START*/
		initialize(wsaaP);
		compute(wsaaP, 7).set(add(sub(rlrdtrmrec.riskTerm,wsaaIndex),1));
		compute(wsaaIntins, 8).setRounded((div((sub(1,power(wsaaVj,wsaaP))),(sub(1,power(wsaaVj,rlrdtrmrec.riskTerm))))));
		compute(wsaaSumins, 4).setRounded(mult(10000,wsaaIntins));
		compute(wsaaSumins1, 7).set(div(wsaaSumins,10000));
		compute(wsaaRdtsum, 7).set(mult(rlrdtrmrec.sumins,wsaaSumins1));
		write600();
		/*EXIT*/
	}

protected void write600()
	{
		start600();
	}

protected void start600()
	{
		mbnsIO.setChdrcoy(rlrdtrmrec.chdrcoy);
		mbnsIO.setChdrnum(rlrdtrmrec.chdrnum);
		mbnsIO.setLife(rlrdtrmrec.life);
		mbnsIO.setCoverage(rlrdtrmrec.coverage);
		mbnsIO.setRider(rlrdtrmrec.rider);
		mbnsIO.setYrsinf(wsaaIndex);
		mbnsIO.setFormat(mbnsrec);
		mbnsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)
		|| isEQ(mbnsIO.getStatuz(),varcom.mrnf)) {
			mbnsIO.setFunction(varcom.writr);
		}
		else {
			mbnsIO.setFunction(varcom.rewrt);
		}
		mbnsIO.setSumins(wsaaRdtsum);
		mbnsIO.setSurrval(ZERO);
		mbnsIO.setFormat(mbnsrec);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)) {
			rlrdtrmrec.statuz.set(mbnsIO.getStatuz());
		}
	}
}
