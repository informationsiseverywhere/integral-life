package com.csc.life.productdefinition.procedures;

import java.util.HashMap;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.util.QPUtilities;

public class Calccwth extends Vpmcalc {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private Vpxutrsrec vpxutrsrec = new Vpxutrsrec();

	public Calccwth() {
		super();
	}

	public void mainline(Object... parmArray)
	{	
		recordMap = new HashMap<String, ExternalData>();
		recordMap.put("VPMFMTREC", vpmfmtrec);
		
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		
		recordMap.put("VPXUTRSREC", vpxutrsrec);
		
		try {
			startSubr010();
			
		}catch (Exception e) {
		}		
	}
	
}
