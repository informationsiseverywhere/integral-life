package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5f7ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(240);
	public FixedLengthStringData dataFields = new FixedLengthStringData(68).isAPartOf(dataArea, 0);	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);		
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);	
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);	
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,44);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,52);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData annind =  DD.ind.copy().isAPartOf(dataFields,62);	
	public ZonedDecimalData cmpday = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,63);
	public ZonedDecimalData cmpmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,65);
	public FixedLengthStringData transind =  DD.ind.copy().isAPartOf(dataFields,67);	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 68);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,0);		
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,4);	
	public FixedLengthStringData longdescErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,12);		
  	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
  	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
  	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,24);	
	public FixedLengthStringData annindErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,28);
	public FixedLengthStringData cmpdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,32);		
  	public FixedLengthStringData cmpmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 34);
  	public FixedLengthStringData transindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
		
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 108);	
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);	
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);		
  	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
  	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);  	
  	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] annindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cmpdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);		
  	public FixedLengthStringData[] cmpmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
  	public FixedLengthStringData[] transindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public LongData Sd5f7screenWritten = new LongData(0);
	public LongData Sd5f7protectWritten = new LongData(0);
	
	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5f7ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, billfreq, annind, cmpday, cmpmonth, transind };
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, billfreqOut, annindOut, cmpdayOut, cmpmonthOut, transindOut };
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, billfreqErr, annindErr, cmpdayErr, cmpmonthErr, transindErr };
		
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5f7screen.class;
		protectRecord = Sd5f7protect.class;
	}

}
