package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.unitlinkedprocessing.recordstructures.Rwcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

public class Vpmcwth extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	
	private String wsaaSubr = "VPMCWTH";
	
	private String t5540 = "T5540";
	private String tr28x = "TR28X";
	
	private String chdrrec = "CHDRREC";
	private String itemrec = "ITEMREC";	
	
	private String h055 = "H055";
	
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Rwcalcpy rwcalcpy = new Rwcalcpy();	
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5540rec t5540rec = new T5540rec();
	private static final String SUBROUTINE_PREFIX = "CWTH";
	
	public Vpmcwth(){
		super();
	}
	
	public void mainline(Object... parmArray){
		syserrrec.subrname.set(wsaaSubr);
		
		rwcalcpy.withdrawRec = convertAndSetParam(rwcalcpy.withdrawRec, parmArray, 0); 		
	 	
		vpmcalcrec.set(SPACES);
		rwcalcpy.status.set(varcom.oK);
		vpmcalcrec.statuz.set(varcom.oK);
		setupKey1000();
		if(!isEQ(rwcalcpy.status,varcom.oK)){
			return;

		}
		vpmcalcrec.linkageArea.set(rwcalcpy.withdrawRec);
		
		callProgram(Calccwth.class, vpmcalcrec.vpmcalcRec);
		
		rwcalcpy.withdrawRec.set(vpmcalcrec.linkageArea);
		if(!isEQ(vpmcalcrec.statuz,varcom.oK)){
			rwcalcpy.status.set(vpmcalcrec.statuz);
		}		
 	}
	
	private void setupKey1000(){

		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(rwcalcpy.crtable);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)||!isEQ(itdmIO.getItemcoy(),rwcalcpy.chdrChdrcoy)||
				!isEQ(itdmIO.getItemtabl(),t5540)||!isEQ(itdmIO.getItemitem(),rwcalcpy.crtable)){
			rwcalcpy.status.set(h055);
			return;
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		vpmcalcrec.vpmskey.set(SPACES);
		vpmcalcrec.vpmscoy.set(rwcalcpy.chdrChdrcoy);
		vpmcalcrec.vpmstabl.set(tr28x);
		readChdr1100();
		vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX + chdrIO.getCnttype().toString().trim());
		vpmcalcrec.extkey.set(t5540rec.wdmeth);
	}
	
	private void readChdr1100(){

		initialize(chdrIO.getParams());
		chdrIO.setStatuz(varcom.oK);
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(rwcalcpy.chdrChdrcoy);
		chdrIO.setChdrnum(rwcalcpy.chdrChdrnum);
		chdrIO.setValidflag("1");
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.begn);
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if(!isEQ(chdrIO.getStatuz(),varcom.oK)||!isEQ(chdrIO.getChdrnum(),rwcalcpy.chdrChdrnum)){
			chdrIO.setCnttype(SPACES);
		}
	}
}
