package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5567screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5567ScreenVars sv = (S5567ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5567screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5567ScreenVars screenVars = (S5567ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.billfreq01.setClassString("");
		screenVars.billfreq02.setClassString("");
		screenVars.billfreq03.setClassString("");
		screenVars.billfreq04.setClassString("");
		screenVars.billfreq05.setClassString("");
		screenVars.billfreq06.setClassString("");
		screenVars.billfreq07.setClassString("");
		screenVars.billfreq08.setClassString("");
		screenVars.billfreq09.setClassString("");
		screenVars.billfreq10.setClassString("");
		screenVars.cntfee01.setClassString("");
		screenVars.cntfee02.setClassString("");
		screenVars.cntfee03.setClassString("");
		screenVars.cntfee04.setClassString("");
		screenVars.cntfee05.setClassString("");
		screenVars.cntfee06.setClassString("");
		screenVars.cntfee07.setClassString("");
		screenVars.cntfee08.setClassString("");
		screenVars.cntfee09.setClassString("");
		screenVars.cntfee10.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
	}

/**
 * Clear all the variables in S5567screen
 */
	public static void clear(VarModel pv) {
		S5567ScreenVars screenVars = (S5567ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.billfreq01.clear();
		screenVars.billfreq02.clear();
		screenVars.billfreq03.clear();
		screenVars.billfreq04.clear();
		screenVars.billfreq05.clear();
		screenVars.billfreq06.clear();
		screenVars.billfreq07.clear();
		screenVars.billfreq08.clear();
		screenVars.billfreq09.clear();
		screenVars.billfreq10.clear();
		screenVars.cntfee01.clear();
		screenVars.cntfee02.clear();
		screenVars.cntfee03.clear();
		screenVars.cntfee04.clear();
		screenVars.cntfee05.clear();
		screenVars.cntfee06.clear();
		screenVars.cntfee07.clear();
		screenVars.cntfee08.clear();
		screenVars.cntfee09.clear();
		screenVars.cntfee10.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
	}
}
