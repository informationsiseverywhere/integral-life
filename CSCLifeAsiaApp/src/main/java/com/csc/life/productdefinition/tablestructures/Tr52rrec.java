package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:26
 * Description:
 * Copybook name: TR52RREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52rrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52rRec = new FixedLengthStringData(500);
  	public ZonedDecimalData agelimit = new ZonedDecimalData(3, 0).isAPartOf(tr52rRec, 0);
  	public ZonedDecimalData dsifact = new ZonedDecimalData(7, 4).isAPartOf(tr52rRec, 3);
  	public FixedLengthStringData factorsas = new FixedLengthStringData(70).isAPartOf(tr52rRec, 10);
  	public ZonedDecimalData[] factorsa = ZDArrayPartOfStructure(10, 7, 4, factorsas, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(70).isAPartOf(factorsas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData factorsa01 = new ZonedDecimalData(7, 4).isAPartOf(filler, 0);
  	public ZonedDecimalData factorsa02 = new ZonedDecimalData(7, 4).isAPartOf(filler, 7);
  	public ZonedDecimalData factorsa03 = new ZonedDecimalData(7, 4).isAPartOf(filler, 14);
  	public ZonedDecimalData factorsa04 = new ZonedDecimalData(7, 4).isAPartOf(filler, 21);
  	public ZonedDecimalData factorsa05 = new ZonedDecimalData(7, 4).isAPartOf(filler, 28);
  	public ZonedDecimalData factorsa06 = new ZonedDecimalData(7, 4).isAPartOf(filler, 35);
  	public ZonedDecimalData factorsa07 = new ZonedDecimalData(7, 4).isAPartOf(filler, 42);
  	public ZonedDecimalData factorsa08 = new ZonedDecimalData(7, 4).isAPartOf(filler, 49);
  	public ZonedDecimalData factorsa09 = new ZonedDecimalData(7, 4).isAPartOf(filler, 56);
  	public ZonedDecimalData factorsa10 = new ZonedDecimalData(7, 4).isAPartOf(filler, 63);
  	public FixedLengthStringData factsamns = new FixedLengthStringData(70).isAPartOf(tr52rRec, 80);
  	public ZonedDecimalData[] factsamn = ZDArrayPartOfStructure(10, 7, 4, factsamns, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(70).isAPartOf(factsamns, 0, FILLER_REDEFINE);
  	public ZonedDecimalData factsamn01 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 0);
  	public ZonedDecimalData factsamn02 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 7);
  	public ZonedDecimalData factsamn03 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 14);
  	public ZonedDecimalData factsamn04 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 21);
  	public ZonedDecimalData factsamn05 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 28);
  	public ZonedDecimalData factsamn06 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 35);
  	public ZonedDecimalData factsamn07 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 42);
  	public ZonedDecimalData factsamn08 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 49);
  	public ZonedDecimalData factsamn09 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 56);
  	public ZonedDecimalData factsamn10 = new ZonedDecimalData(7, 4).isAPartOf(filler1, 63);
  	public FixedLengthStringData factsamxs = new FixedLengthStringData(70).isAPartOf(tr52rRec, 150);
  	public ZonedDecimalData[] factsamx = ZDArrayPartOfStructure(10, 7, 4, factsamxs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(70).isAPartOf(factsamxs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData factsamx01 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 0);
  	public ZonedDecimalData factsamx02 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 7);
  	public ZonedDecimalData factsamx03 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 14);
  	public ZonedDecimalData factsamx04 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 21);
  	public ZonedDecimalData factsamx05 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 28);
  	public ZonedDecimalData factsamx06 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 35);
  	public ZonedDecimalData factsamx07 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 42);
  	public ZonedDecimalData factsamx08 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 49);
  	public ZonedDecimalData factsamx09 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 56);
  	public ZonedDecimalData factsamx10 = new ZonedDecimalData(7, 4).isAPartOf(filler2, 63);
  	public FixedLengthStringData toterms = new FixedLengthStringData(30).isAPartOf(tr52rRec, 220);
  	public ZonedDecimalData[] toterm = ZDArrayPartOfStructure(10, 3, 0, toterms, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(toterms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData toterm01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData toterm02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData toterm03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData toterm04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData toterm05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData toterm06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData toterm07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
  	public ZonedDecimalData toterm08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
  	public ZonedDecimalData toterm09 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 24);
  	public ZonedDecimalData toterm10 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 27);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(250).isAPartOf(tr52rRec, 250, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52rRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52rRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}