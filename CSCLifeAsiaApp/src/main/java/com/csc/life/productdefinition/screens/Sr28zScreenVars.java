package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sr28z
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class Sr28zScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(253);
	public FixedLengthStringData dataFields = new FixedLengthStringData(109).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData dataextract = DD.dataextract.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData function = DD.function.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData copybk = DD.copybk.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData maptofield = DD.maptofield.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData numoccurs = DD.numoccurs.copy().isAPartOf(dataFields,106);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 109);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData dataextractErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData functionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData copybkErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData maptofieldErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData numoccursErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 145);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] dataextractOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] functionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] copybkOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] maptofieldOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] numoccursOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr28zscreenWritten = new LongData(0);
	public LongData Sr28zprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr28zScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dataextractOut,new String[] {"20",null,"-20",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {company, tabl, item, longdesc, dataextract, function, copybk, maptofield, numoccurs};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, dataextractOut, functionOut, copybkOut, maptofieldOut, numoccursOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, dataextractErr, functionErr, copybkErr, maptofieldErr, numoccursErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr28zscreen.class;
		protectRecord = Sr28zprotect.class;
	}

}
