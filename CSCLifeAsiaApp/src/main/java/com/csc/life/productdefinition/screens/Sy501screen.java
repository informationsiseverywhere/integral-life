package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sy501screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static final RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sy501ScreenVars sv = (Sy501ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sy501screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sy501ScreenVars screenVars = (Sy501ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.cnRiskStat01.setClassString("");
		screenVars.cnRiskStat02.setClassString("");
		screenVars.cnRiskStat03.setClassString("");
		screenVars.cnRiskStat04.setClassString("");
		screenVars.cnRiskStat05.setClassString("");
		screenVars.cnRiskStat06.setClassString("");
		screenVars.cnRiskStat07.setClassString("");
		screenVars.cnRiskStat08.setClassString("");
		screenVars.cnRiskStat09.setClassString("");
		screenVars.cnRiskStat10.setClassString("");
		screenVars.cnRiskStat11.setClassString("");
		screenVars.cnRiskStat12.setClassString("");
		screenVars.setCnRiskStat.setClassString("");
		screenVars.cnPremStat01.setClassString("");
		screenVars.cnPremStat02.setClassString("");
		screenVars.cnPremStat03.setClassString("");
		screenVars.cnPremStat04.setClassString("");
		screenVars.cnPremStat05.setClassString("");
		screenVars.cnPremStat06.setClassString("");
		screenVars.cnPremStat07.setClassString("");
		screenVars.cnPremStat08.setClassString("");
		screenVars.cnPremStat09.setClassString("");
		screenVars.cnPremStat10.setClassString("");
		screenVars.cnPremStat11.setClassString("");
		screenVars.cnPremStat12.setClassString("");
		screenVars.setCnPremStat.setClassString("");
		screenVars.setSngpCnStat.setClassString("");
		screenVars.lifeStat01.setClassString("");
		screenVars.lifeStat02.setClassString("");
		screenVars.lifeStat03.setClassString("");
		screenVars.lifeStat04.setClassString("");
		screenVars.lifeStat05.setClassString("");
		screenVars.lifeStat06.setClassString("");
		screenVars.setLifeStat.setClassString("");
		screenVars.jlifeStat01.setClassString("");
		screenVars.jlifeStat02.setClassString("");
		screenVars.jlifeStat03.setClassString("");
		screenVars.jlifeStat04.setClassString("");
		screenVars.jlifeStat05.setClassString("");
		screenVars.jlifeStat06.setClassString("");
		screenVars.setJlifeStat.setClassString("");
		screenVars.covRiskStat01.setClassString("");
		screenVars.covRiskStat02.setClassString("");
		screenVars.covRiskStat03.setClassString("");
		screenVars.covRiskStat04.setClassString("");
		screenVars.covRiskStat05.setClassString("");
		screenVars.covRiskStat06.setClassString("");
		screenVars.covRiskStat07.setClassString("");
		screenVars.covRiskStat08.setClassString("");
		screenVars.covRiskStat09.setClassString("");
		screenVars.covRiskStat10.setClassString("");
		screenVars.covRiskStat11.setClassString("");
		screenVars.covRiskStat12.setClassString("");
		screenVars.setCovRiskStat.setClassString("");
		screenVars.covPremStat01.setClassString("");
		screenVars.covPremStat02.setClassString("");
		screenVars.covPremStat03.setClassString("");
		screenVars.covPremStat04.setClassString("");
		screenVars.covPremStat05.setClassString("");
		screenVars.covPremStat06.setClassString("");
		screenVars.covPremStat07.setClassString("");
		screenVars.covPremStat08.setClassString("");
		screenVars.covPremStat09.setClassString("");
		screenVars.covPremStat10.setClassString("");
		screenVars.covPremStat11.setClassString("");
		screenVars.covPremStat12.setClassString("");
		screenVars.setCovPremStat.setClassString("");
		screenVars.setSngpCovStat.setClassString("");
		screenVars.ridRiskStat01.setClassString("");
		screenVars.ridRiskStat02.setClassString("");
		screenVars.ridRiskStat03.setClassString("");
		screenVars.ridRiskStat04.setClassString("");
		screenVars.ridRiskStat05.setClassString("");
		screenVars.ridRiskStat06.setClassString("");
		screenVars.ridRiskStat07.setClassString("");
		screenVars.ridRiskStat08.setClassString("");
		screenVars.ridRiskStat09.setClassString("");
		screenVars.ridRiskStat10.setClassString("");
		screenVars.ridRiskStat11.setClassString("");
		screenVars.ridRiskStat12.setClassString("");
		screenVars.setRidRiskStat.setClassString("");
		screenVars.ridPremStat01.setClassString("");
		screenVars.ridPremStat02.setClassString("");
		screenVars.ridPremStat03.setClassString("");
		screenVars.ridPremStat04.setClassString("");
		screenVars.ridPremStat05.setClassString("");
		screenVars.ridPremStat06.setClassString("");
		screenVars.ridPremStat07.setClassString("");
		screenVars.ridPremStat08.setClassString("");
		screenVars.ridPremStat09.setClassString("");
		screenVars.ridPremStat10.setClassString("");
		screenVars.ridPremStat11.setClassString("");
		screenVars.ridPremStat12.setClassString("");
		screenVars.setRidPremStat.setClassString("");
		screenVars.setSngpRidStat.setClassString("");
	}

/**
 * Clear all the variables in Sy501screen
 */
	public static void clear(VarModel pv) {
		Sy501ScreenVars screenVars = (Sy501ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.cnRiskStat01.clear();
		screenVars.cnRiskStat02.clear();
		screenVars.cnRiskStat03.clear();
		screenVars.cnRiskStat04.clear();
		screenVars.cnRiskStat05.clear();
		screenVars.cnRiskStat06.clear();
		screenVars.cnRiskStat07.clear();
		screenVars.cnRiskStat08.clear();
		screenVars.cnRiskStat09.clear();
		screenVars.cnRiskStat10.clear();
		screenVars.cnRiskStat11.clear();
		screenVars.cnRiskStat12.clear();
		screenVars.setCnRiskStat.clear();
		screenVars.cnPremStat01.clear();
		screenVars.cnPremStat02.clear();
		screenVars.cnPremStat03.clear();
		screenVars.cnPremStat04.clear();
		screenVars.cnPremStat05.clear();
		screenVars.cnPremStat06.clear();
		screenVars.cnPremStat07.clear();
		screenVars.cnPremStat08.clear();
		screenVars.cnPremStat09.clear();
		screenVars.cnPremStat10.clear();
		screenVars.cnPremStat11.clear();
		screenVars.cnPremStat12.clear();
		screenVars.setCnPremStat.clear();
		screenVars.setSngpCnStat.clear();
		screenVars.lifeStat01.clear();
		screenVars.lifeStat02.clear();
		screenVars.lifeStat03.clear();
		screenVars.lifeStat04.clear();
		screenVars.lifeStat05.clear();
		screenVars.lifeStat06.clear();
		screenVars.setLifeStat.clear();
		screenVars.jlifeStat01.clear();
		screenVars.jlifeStat02.clear();
		screenVars.jlifeStat03.clear();
		screenVars.jlifeStat04.clear();
		screenVars.jlifeStat05.clear();
		screenVars.jlifeStat06.clear();
		screenVars.setJlifeStat.clear();
		screenVars.covRiskStat01.clear();
		screenVars.covRiskStat02.clear();
		screenVars.covRiskStat03.clear();
		screenVars.covRiskStat04.clear();
		screenVars.covRiskStat05.clear();
		screenVars.covRiskStat06.clear();
		screenVars.covRiskStat07.clear();
		screenVars.covRiskStat08.clear();
		screenVars.covRiskStat09.clear();
		screenVars.covRiskStat10.clear();
		screenVars.covRiskStat11.clear();
		screenVars.covRiskStat12.clear();
		screenVars.setCovRiskStat.clear();
		screenVars.covPremStat01.clear();
		screenVars.covPremStat02.clear();
		screenVars.covPremStat03.clear();
		screenVars.covPremStat04.clear();
		screenVars.covPremStat05.clear();
		screenVars.covPremStat06.clear();
		screenVars.covPremStat07.clear();
		screenVars.covPremStat08.clear();
		screenVars.covPremStat09.clear();
		screenVars.covPremStat10.clear();
		screenVars.covPremStat11.clear();
		screenVars.covPremStat12.clear();
		screenVars.setCovPremStat.clear();
		screenVars.setSngpCovStat.clear();
		screenVars.ridRiskStat01.clear();
		screenVars.ridRiskStat02.clear();
		screenVars.ridRiskStat03.clear();
		screenVars.ridRiskStat04.clear();
		screenVars.ridRiskStat05.clear();
		screenVars.ridRiskStat06.clear();
		screenVars.ridRiskStat07.clear();
		screenVars.ridRiskStat08.clear();
		screenVars.ridRiskStat09.clear();
		screenVars.ridRiskStat10.clear();
		screenVars.ridRiskStat11.clear();
		screenVars.ridRiskStat12.clear();
		screenVars.setRidRiskStat.clear();
		screenVars.ridPremStat01.clear();
		screenVars.ridPremStat02.clear();
		screenVars.ridPremStat03.clear();
		screenVars.ridPremStat04.clear();
		screenVars.ridPremStat05.clear();
		screenVars.ridPremStat06.clear();
		screenVars.ridPremStat07.clear();
		screenVars.ridPremStat08.clear();
		screenVars.ridPremStat09.clear();
		screenVars.ridPremStat10.clear();
		screenVars.ridPremStat11.clear();
		screenVars.ridPremStat12.clear();
		screenVars.setRidPremStat.clear();
		screenVars.setSngpRidStat.clear();
	}
}
