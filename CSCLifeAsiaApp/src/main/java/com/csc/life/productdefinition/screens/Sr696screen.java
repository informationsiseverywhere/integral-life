package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr696screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr696ScreenVars sv = (Sr696ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr696screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr696ScreenVars screenVars = (Sr696ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.simcrtable.setClassString("");
		screenVars.frsumins01.setClassString("");
		screenVars.tosumins01.setClassString("");
		screenVars.saband01.setClassString("");
		screenVars.tosumins02.setClassString("");
		screenVars.tosumins03.setClassString("");
		screenVars.tosumins04.setClassString("");
		screenVars.tosumins05.setClassString("");
		screenVars.tosumins06.setClassString("");
		screenVars.tosumins07.setClassString("");
		screenVars.tosumins08.setClassString("");
		screenVars.tosumins09.setClassString("");
		screenVars.tosumins10.setClassString("");
		screenVars.frsumins02.setClassString("");
		screenVars.frsumins03.setClassString("");
		screenVars.frsumins04.setClassString("");
		screenVars.frsumins05.setClassString("");
		screenVars.frsumins06.setClassString("");
		screenVars.frsumins07.setClassString("");
		screenVars.frsumins08.setClassString("");
		screenVars.frsumins09.setClassString("");
		screenVars.frsumins10.setClassString("");
		screenVars.saband02.setClassString("");
		screenVars.saband03.setClassString("");
		screenVars.saband04.setClassString("");
		screenVars.saband05.setClassString("");
		screenVars.saband06.setClassString("");
		screenVars.saband07.setClassString("");
		screenVars.saband08.setClassString("");
		screenVars.saband09.setClassString("");
		screenVars.saband10.setClassString("");
	}

/**
 * Clear all the variables in Sr696screen
 */
	public static void clear(VarModel pv) {
		Sr696ScreenVars screenVars = (Sr696ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.simcrtable.clear();
		screenVars.frsumins01.clear();
		screenVars.tosumins01.clear();
		screenVars.saband01.clear();
		screenVars.tosumins02.clear();
		screenVars.tosumins03.clear();
		screenVars.tosumins04.clear();
		screenVars.tosumins05.clear();
		screenVars.tosumins06.clear();
		screenVars.tosumins07.clear();
		screenVars.tosumins08.clear();
		screenVars.tosumins09.clear();
		screenVars.tosumins10.clear();
		screenVars.frsumins02.clear();
		screenVars.frsumins03.clear();
		screenVars.frsumins04.clear();
		screenVars.frsumins05.clear();
		screenVars.frsumins06.clear();
		screenVars.frsumins07.clear();
		screenVars.frsumins08.clear();
		screenVars.frsumins09.clear();
		screenVars.frsumins10.clear();
		screenVars.saband02.clear();
		screenVars.saband03.clear();
		screenVars.saband04.clear();
		screenVars.saband05.clear();
		screenVars.saband06.clear();
		screenVars.saband07.clear();
		screenVars.saband08.clear();
		screenVars.saband09.clear();
		screenVars.saband10.clear();
	}
}
