package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MerxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:50
 * Class transformed from MERXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MerxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 147;
	public FixedLengthStringData merxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData merxpfRecord = merxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(merxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(merxrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(merxrec);
	public FixedLengthStringData paidby = DD.paidby.copy().isAPartOf(merxrec);
	public FixedLengthStringData exmcode = DD.exmcode.copy().isAPartOf(merxrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(merxrec);
	public FixedLengthStringData name = DD.name.copy().isAPartOf(merxrec);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(merxrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(merxrec);
	public FixedLengthStringData invref = DD.invref.copy().isAPartOf(merxrec);
	public PackedDecimalData zmedfee = DD.zmedfee.copy().isAPartOf(merxrec);
	public FixedLengthStringData desc = DD.desc.copy().isAPartOf(merxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MerxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for MerxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MerxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MerxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MerxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MerxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MerxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MERXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"SEQNO, " +
							"PAIDBY, " +
							"EXMCODE, " +
							"CLNTNUM, " +
							"NAME, " +
							"ZMEDTYP, " +
							"EFFDATE, " +
							"INVREF, " +
							"ZMEDFEE, " +
							"DESC_T, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     seqno,
                                     paidby,
                                     exmcode,
                                     clntnum,
                                     name,
                                     zmedtyp,
                                     effdate,
                                     invref,
                                     zmedfee,
                                     desc,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		seqno.clear();
  		paidby.clear();
  		exmcode.clear();
  		clntnum.clear();
  		name.clear();
  		zmedtyp.clear();
  		effdate.clear();
  		invref.clear();
  		zmedfee.clear();
  		desc.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMerxrec() {
  		return merxrec;
	}

	public FixedLengthStringData getMerxpfRecord() {
  		return merxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMerxrec(what);
	}

	public void setMerxrec(Object what) {
  		this.merxrec.set(what);
	}

	public void setMerxpfRecord(Object what) {
  		this.merxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(merxrec.getLength());
		result.set(merxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}