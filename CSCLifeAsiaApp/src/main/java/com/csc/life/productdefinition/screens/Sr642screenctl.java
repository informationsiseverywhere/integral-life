package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr642screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 7, 16, 1, 2, 12, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr642screensfl";
		lrec.subfileClass = Sr642screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 5;
		lrec.pageSubfile = 4;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 13, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr642ScreenVars sv = (Sr642ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr642screenctlWritten, sv.Sr642screensflWritten, av, sv.sr642screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr642ScreenVars screenVars = (Sr642ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifedesc.setClassString("");
		screenVars.jlifeno.setClassString("");
		screenVars.jlifedesc.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownerdesc.setClassString("");
		screenVars.jownnum.setClassString("");
		screenVars.lname.setClassString("");
		screenVars.servagnt.setClassString("");
		screenVars.servagnam.setClassString("");
		screenVars.servbr.setClassString("");
		screenVars.brchname.setClassString("");
	}

/**
 * Clear all the variables in Sr642screenctl
 */
	public static void clear(VarModel pv) {
		Sr642ScreenVars screenVars = (Sr642ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifedesc.clear();
		screenVars.jlifeno.clear();
		screenVars.jlifedesc.clear();
		screenVars.cownnum.clear();
		screenVars.ownerdesc.clear();
		screenVars.jownnum.clear();
		screenVars.lname.clear();
		screenVars.servagnt.clear();
		screenVars.servagnam.clear();
		screenVars.servbr.clear();
		screenVars.brchname.clear();
	}
}
