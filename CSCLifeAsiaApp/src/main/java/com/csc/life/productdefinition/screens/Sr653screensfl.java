package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr653screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 12, 3, 21}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 12, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr653ScreenVars sv = (Sr653ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr653screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr653screensfl, 
			sv.Sr653screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr653ScreenVars sv = (Sr653ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr653screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr653ScreenVars sv = (Sr653ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr653screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr653screensflWritten.gt(0))
		{
			sv.sr653screensfl.setCurrentIndex(0);
			sv.Sr653screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr653ScreenVars sv = (Sr653ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr653screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr653ScreenVars screenVars = (Sr653ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqno.setFieldName("seqno");
				screenVars.validflag.setFieldName("validflag");
				screenVars.select.setFieldName("select");
				screenVars.mdblact.setFieldName("mdblact");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.invref.setFieldName("invref");
				screenVars.payto.setFieldName("payto");
				screenVars.ent.setFieldName("ent");
				screenVars.descript.setFieldName("descript");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.zmedtyp.setFieldName("zmedtyp");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.premium.setFieldName("premium");
				screenVars.paydteDisp.setFieldName("paydteDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.seqno.set(dm.getField("seqno"));
			screenVars.validflag.set(dm.getField("validflag"));
			screenVars.select.set(dm.getField("select"));
			screenVars.mdblact.set(dm.getField("mdblact"));
			screenVars.shortdesc.set(dm.getField("shortdesc"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.invref.set(dm.getField("invref"));
			screenVars.payto.set(dm.getField("payto"));
			screenVars.ent.set(dm.getField("ent"));
			screenVars.descript.set(dm.getField("descript"));
			screenVars.life.set(dm.getField("life"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.zmedtyp.set(dm.getField("zmedtyp"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.premium.set(dm.getField("premium"));
			screenVars.paydteDisp.set(dm.getField("paydteDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr653ScreenVars screenVars = (Sr653ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqno.setFieldName("seqno");
				screenVars.validflag.setFieldName("validflag");
				screenVars.select.setFieldName("select");
				screenVars.mdblact.setFieldName("mdblact");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.invref.setFieldName("invref");
				screenVars.payto.setFieldName("payto");
				screenVars.ent.setFieldName("ent");
				screenVars.descript.setFieldName("descript");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.zmedtyp.setFieldName("zmedtyp");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.premium.setFieldName("premium");
				screenVars.paydteDisp.setFieldName("paydteDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("seqno").set(screenVars.seqno);
			dm.getField("validflag").set(screenVars.validflag);
			dm.getField("select").set(screenVars.select);
			dm.getField("mdblact").set(screenVars.mdblact);
			dm.getField("shortdesc").set(screenVars.shortdesc);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("invref").set(screenVars.invref);
			dm.getField("payto").set(screenVars.payto);
			dm.getField("ent").set(screenVars.ent);
			dm.getField("descript").set(screenVars.descript);
			dm.getField("life").set(screenVars.life);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("zmedtyp").set(screenVars.zmedtyp);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("premium").set(screenVars.premium);
			dm.getField("paydteDisp").set(screenVars.paydteDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr653screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr653ScreenVars screenVars = (Sr653ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.seqno.clearFormatting();
		screenVars.validflag.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.mdblact.clearFormatting();
		screenVars.shortdesc.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.invref.clearFormatting();
		screenVars.payto.clearFormatting();
		screenVars.ent.clearFormatting();
		screenVars.descript.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.zmedtyp.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.premium.clearFormatting();
		screenVars.paydteDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr653ScreenVars screenVars = (Sr653ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.seqno.setClassString("");
		screenVars.validflag.setClassString("");
		screenVars.select.setClassString("");
		screenVars.mdblact.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.invref.setClassString("");
		screenVars.payto.setClassString("");
		screenVars.ent.setClassString("");
		screenVars.descript.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.zmedtyp.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.premium.setClassString("");
		screenVars.paydteDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr653screensfl
 */
	public static void clear(VarModel pv) {
		Sr653ScreenVars screenVars = (Sr653ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.seqno.clear();
		screenVars.validflag.clear();
		screenVars.select.clear();
		screenVars.mdblact.clear();
		screenVars.shortdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.invref.clear();
		screenVars.payto.clear();
		screenVars.ent.clear();
		screenVars.descript.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.zmedtyp.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.premium.clear();
		screenVars.paydteDisp.clear();
		screenVars.paydte.clear();
	}
}
