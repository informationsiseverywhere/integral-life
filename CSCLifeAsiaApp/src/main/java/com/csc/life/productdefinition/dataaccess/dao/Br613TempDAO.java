package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br613TempDAO extends BaseDAO<Covrpf> {
    public Map<String, List<Covrpf>> searchCovrRecord(String coy, List<String> chdrnumList);
    public List<Covrpf> searchCovrRecord(String coy,String chdrnum);
    public Covrpf getCovrRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plansuffix, String validflag);
    public Covrpf getCovrRecord(Covrpf covr);
	public boolean insertCovrList(List<Covrpf> insertCovrpfList);
	public boolean insertCovrRecord(Covrpf covrpf);
	public void updateCovrRecord(List<Covrpf> updateCovrpflist);
	public boolean updateCovrRecord(Covrpf covrpf);
	public boolean insertAgcmRecord(Agcmpf agcmpf);
	public boolean updateAgcmRecord(Agcmpf agcmpf);
	public boolean insertAgcmRecord(List<Agcmpf> agcmpfList);
	public boolean updateAgcmRecord(List<Agcmpf> agcmpfList);
	public Map<String, List<Agcmpf>> getAgcmRecords(String coy, List<String> chdrnumList);
    public Agcmpf getAgcmpf(Agcmpf agcm);
    public Map<String,List<Pcddpf>> getPcddpfRecords(String coy,List<String> chdrnum); 
    //public Map<String,List<Lextpf>> getLextRecords(String coy,List<String> chdrnum);
}