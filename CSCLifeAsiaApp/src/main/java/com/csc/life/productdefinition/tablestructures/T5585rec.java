package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:04
 * Description:
 * Copybook name: T5585REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5585rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5585Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData addages = new FixedLengthStringData(54).isAPartOf(t5585Rec, 0);
  	public ZonedDecimalData[] addage = ZDArrayPartOfStructure(18, 3, 0, addages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(addages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData addage01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData addage02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData addage03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData addage04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData addage05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData addage06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData addage07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData addage08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData addage09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData addage10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public ZonedDecimalData addage11 = new ZonedDecimalData(3, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData addage12 = new ZonedDecimalData(3, 0).isAPartOf(filler, 33);
  	public ZonedDecimalData addage13 = new ZonedDecimalData(3, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData addage14 = new ZonedDecimalData(3, 0).isAPartOf(filler, 39);
  	public ZonedDecimalData addage15 = new ZonedDecimalData(3, 0).isAPartOf(filler, 42);
  	public ZonedDecimalData addage16 = new ZonedDecimalData(3, 0).isAPartOf(filler, 45);
  	public ZonedDecimalData addage17 = new ZonedDecimalData(3, 0).isAPartOf(filler, 48);
  	public ZonedDecimalData addage18 = new ZonedDecimalData(3, 0).isAPartOf(filler, 51);
  	public FixedLengthStringData ageadjs = new FixedLengthStringData(18).isAPartOf(t5585Rec, 54);
  	public ZonedDecimalData[] ageadj = ZDArrayPartOfStructure(9, 2, 0, ageadjs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(18).isAPartOf(ageadjs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageadj01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData ageadj02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData ageadj03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData ageadj04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData ageadj05 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData ageadj06 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData ageadj07 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData ageadj08 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
  	public ZonedDecimalData ageadj09 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
  	public FixedLengthStringData agedifs = new FixedLengthStringData(36).isAPartOf(t5585Rec, 72);
  	public ZonedDecimalData[] agedif = ZDArrayPartOfStructure(18, 2, 0, agedifs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(agedifs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData agedif01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData agedif02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
  	public ZonedDecimalData agedif03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData agedif04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData agedif05 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData agedif06 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 10);
  	public ZonedDecimalData agedif07 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData agedif08 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 14);
  	public ZonedDecimalData agedif09 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 16);
  	public ZonedDecimalData agedif10 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 18);
  	public ZonedDecimalData agedif11 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 20);
  	public ZonedDecimalData agedif12 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 22);
  	public ZonedDecimalData agedif13 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 24);
  	public ZonedDecimalData agedif14 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 26);
  	public ZonedDecimalData agedif15 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 28);
  	public ZonedDecimalData agedif16 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 30);
  	public ZonedDecimalData agedif17 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 32);
  	public ZonedDecimalData agedif18 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 34);
  	public FixedLengthStringData agelimits = new FixedLengthStringData(27).isAPartOf(t5585Rec, 108);
  	public ZonedDecimalData[] agelimit = ZDArrayPartOfStructure(9, 3, 0, agelimits, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(27).isAPartOf(agelimits, 0, FILLER_REDEFINE);
  	public ZonedDecimalData agelimit01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData agelimit02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData agelimit03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData agelimit04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData agelimit05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData agelimit06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData agelimit07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
  	public ZonedDecimalData agelimit08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
  	public ZonedDecimalData agelimit09 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 24);
  	public FixedLengthStringData hghlowage = new FixedLengthStringData(1).isAPartOf(t5585Rec, 135);
  	public FixedLengthStringData sexageadj = new FixedLengthStringData(1).isAPartOf(t5585Rec, 136);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(363).isAPartOf(t5585Rec, 137, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5585Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5585Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}