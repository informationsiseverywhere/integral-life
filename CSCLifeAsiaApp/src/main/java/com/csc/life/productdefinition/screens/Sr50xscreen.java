package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50xscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50xScreenVars sv = (Sr50xScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50xscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50xScreenVars screenVars = (Sr50xScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.premsubr01.setClassString("");
		screenVars.premsubr02.setClassString("");
		screenVars.premsubr03.setClassString("");
		screenVars.premsubr04.setClassString("");
		screenVars.premsubr05.setClassString("");
		screenVars.premsubr06.setClassString("");
		screenVars.premsubr07.setClassString("");
		screenVars.premsubr08.setClassString("");
		screenVars.premsubr09.setClassString("");
		screenVars.premsubr10.setClassString("");
		screenVars.premsubr11.setClassString("");
		screenVars.premsubr12.setClassString("");
		screenVars.premsubr13.setClassString("");
		screenVars.premsubr14.setClassString("");
		screenVars.premsubr15.setClassString("");
	}

/**
 * Clear all the variables in Sr50xscreen
 */
	public static void clear(VarModel pv) {
		Sr50xScreenVars screenVars = (Sr50xScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.premsubr01.clear();
		screenVars.premsubr02.clear();
		screenVars.premsubr03.clear();
		screenVars.premsubr04.clear();
		screenVars.premsubr05.clear();
		screenVars.premsubr06.clear();
		screenVars.premsubr07.clear();
		screenVars.premsubr08.clear();
		screenVars.premsubr09.clear();
		screenVars.premsubr10.clear();
		screenVars.premsubr11.clear();
		screenVars.premsubr12.clear();
		screenVars.premsubr13.clear();
		screenVars.premsubr14.clear();
		screenVars.premsubr15.clear();
	}
}
