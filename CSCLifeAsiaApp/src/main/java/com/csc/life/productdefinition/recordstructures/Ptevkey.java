package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:13
 * Description:
 * Copybook name: PTEVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptevKey = new FixedLengthStringData(64).isAPartOf(ptevFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptevChdrcoy = new FixedLengthStringData(1).isAPartOf(ptevKey, 0);
  	public FixedLengthStringData ptevChdrnum = new FixedLengthStringData(8).isAPartOf(ptevKey, 1);
  	public PackedDecimalData ptevEffdate = new PackedDecimalData(8, 0).isAPartOf(ptevKey, 9);
  	public PackedDecimalData ptevPtrneff = new PackedDecimalData(8, 0).isAPartOf(ptevKey, 14);
  	public FixedLengthStringData ptevValidflag = new FixedLengthStringData(1).isAPartOf(ptevKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(ptevKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}