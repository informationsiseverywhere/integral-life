package com.csc.life.productdefinition.screens;


import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5h2screen extends ScreenRecord{

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5h2ScreenVars sv = (Sd5h2ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5h2screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5h2ScreenVars screenVars = (Sd5h2ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.agntsdets.setClassString("");
		screenVars.payind.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.grpind.setClassString("");
		screenVars.accInd.setClassString("");
		screenVars.triggerRequired.setClassString("");
		screenVars.sacscode.setClassString("");
		screenVars.sacstyp.setClassString("");
		screenVars.mediaRequired.setClassString("");
		screenVars.printLocation.setClassString("");
		screenVars.crcind.setClassString("");
		screenVars.rollovrind.setClassString("");
	}


	public static void clear(VarModel pv) {
		Sd5h2ScreenVars screenVars = (Sd5h2ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.agntsdets.clear();
		screenVars.payind.clear();
		screenVars.ddind.clear();
		screenVars.grpind.clear();
		screenVars.accInd.clear();
		screenVars.triggerRequired.clear();
		screenVars.sacscode.clear();
		screenVars.sacstyp.clear();
		screenVars.mediaRequired.clear();
		screenVars.printLocation.clear();
		screenVars.crcind.clear();
		screenVars.rollovrind.clear();
	}
}
