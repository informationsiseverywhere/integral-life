package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:23
 * Description:
 * Copybook name: CHDRZPRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrzprkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrzprFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData chdrzprKey = new FixedLengthStringData(256).isAPartOf(chdrzprFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrzprChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrzprKey, 0);
  	public FixedLengthStringData chdrzprChdrnum = new FixedLengthStringData(8).isAPartOf(chdrzprKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(chdrzprKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrzprFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrzprFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}