/*
 * File: Tr51bpt.java
 * Date: 30 August 2009 2:41:59
 * Author: Quipoz Limited
 * 
 * Class transformed from TR51BPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr51brec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR51B.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr51bpt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Comp. SA must be < Other Comp.             SR51B");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(47);
	private FixedLengthStringData filler7 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 7, FILLER).init("Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 21);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 31, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 37);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(62);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(55).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Age Band      Operator    SA        Maximum Sum Assured");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(38);
	private FixedLengthStringData filler12 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine005, 10, FILLER).init("<=                  Up to(%)");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(61);
	private FixedLengthStringData filler14 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 9).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 24);
	private FixedLengthStringData filler16 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 32).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(61);
	private FixedLengthStringData filler18 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 9).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 24);
	private FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 32).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(61);
	private FixedLengthStringData filler22 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 9).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 24);
	private FixedLengthStringData filler24 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 32).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(61);
	private FixedLengthStringData filler26 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 9).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 24);
	private FixedLengthStringData filler28 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 32).setPattern("ZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(61);
	private FixedLengthStringData filler30 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 9).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 24);
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 32).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(61);
	private FixedLengthStringData filler34 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 9).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 24);
	private FixedLengthStringData filler36 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 32).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(61);
	private FixedLengthStringData filler38 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 9).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 24);
	private FixedLengthStringData filler40 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 32).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(61);
	private FixedLengthStringData filler42 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 9).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 24);
	private FixedLengthStringData filler44 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 32).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(61);
	private FixedLengthStringData filler46 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 9).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 24);
	private FixedLengthStringData filler48 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 32).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(61);
	private FixedLengthStringData filler50 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 9).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 24);
	private FixedLengthStringData filler52 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 32).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 43).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr51brec tr51brec = new Tr51brec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr51bpt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr51brec.tr51bRec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tr51brec.age01);
		fieldNo009.set(tr51brec.overdueMina01);
		fieldNo010.set(tr51brec.sumins01);
		fieldNo011.set(tr51brec.age02);
		fieldNo013.set(tr51brec.overdueMina02);
		fieldNo014.set(tr51brec.sumins02);
		fieldNo015.set(tr51brec.age03);
		fieldNo017.set(tr51brec.overdueMina03);
		fieldNo018.set(tr51brec.sumins03);
		fieldNo019.set(tr51brec.age04);
		fieldNo021.set(tr51brec.overdueMina04);
		fieldNo022.set(tr51brec.sumins04);
		fieldNo023.set(tr51brec.age05);
		fieldNo025.set(tr51brec.overdueMina05);
		fieldNo026.set(tr51brec.sumins05);
		fieldNo027.set(tr51brec.age06);
		fieldNo029.set(tr51brec.overdueMina06);
		fieldNo030.set(tr51brec.sumins06);
		fieldNo031.set(tr51brec.age07);
		fieldNo033.set(tr51brec.overdueMina07);
		fieldNo034.set(tr51brec.sumins07);
		fieldNo035.set(tr51brec.age08);
		fieldNo037.set(tr51brec.overdueMina08);
		fieldNo038.set(tr51brec.sumins08);
		fieldNo039.set(tr51brec.age09);
		fieldNo041.set(tr51brec.overdueMina09);
		fieldNo042.set(tr51brec.sumins09);
		fieldNo043.set(tr51brec.age10);
		fieldNo045.set(tr51brec.overdueMina10);
		fieldNo046.set(tr51brec.sumins10);
		fieldNo008.set(tr51brec.indc01);
		fieldNo012.set(tr51brec.indc02);
		fieldNo016.set(tr51brec.indc03);
		fieldNo020.set(tr51brec.indc04);
		fieldNo024.set(tr51brec.indc05);
		fieldNo028.set(tr51brec.indc06);
		fieldNo032.set(tr51brec.indc07);
		fieldNo036.set(tr51brec.indc08);
		fieldNo040.set(tr51brec.indc09);
		fieldNo044.set(tr51brec.indc10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
