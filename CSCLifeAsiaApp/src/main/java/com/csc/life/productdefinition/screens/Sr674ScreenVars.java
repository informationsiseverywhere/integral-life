package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR674
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr674ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData aiind = DD.aiind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData amtflds = new FixedLengthStringData(60).isAPartOf(dataFields, 1);
	public ZonedDecimalData[] amtfld = ZDArrayPartOfStructure(5, 12, 2, amtflds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(amtflds, 0, FILLER_REDEFINE);
	public ZonedDecimalData amtfld01 = DD.amtfld.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData amtfld02 = DD.amtfld.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData amtfld03 = DD.amtfld.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData amtfld04 = DD.amtfld.copyToZonedDecimal().isAPartOf(filler,36);
	public ZonedDecimalData amtfld05 = DD.amtfld.copyToZonedDecimal().isAPartOf(filler,48);//BRD-NBP-010
	public FixedLengthStringData crcind = DD.crcind.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData grpind = DD.grpind.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData payind = DD.payind.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData totflds = new FixedLengthStringData(60).isAPartOf(dataFields, 65);
	public ZonedDecimalData[] totfld = ZDArrayPartOfStructure(5, 12, 2, totflds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(totflds, 0, FILLER_REDEFINE);
	public ZonedDecimalData totfld01 = DD.totfld.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData totfld02 = DD.totfld.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData totfld03 = DD.totfld.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData totfld04 = DD.totfld.copyToZonedDecimal().isAPartOf(filler1,36);
	public ZonedDecimalData totfld05 = DD.totfld.copyToZonedDecimal().isAPartOf(filler1,48);//BRD-NBP-010
	public FixedLengthStringData zchgamts = new FixedLengthStringData(60).isAPartOf(dataFields, 125);
	public ZonedDecimalData[] zchgamt = ZDArrayPartOfStructure(5, 12, 2, zchgamts, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(zchgamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData zchgamt01 = DD.zchgamt.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData zchgamt02 = DD.zchgamt.copyToZonedDecimal().isAPartOf(filler2,12);
	public ZonedDecimalData zchgamt03 = DD.zchgamt.copyToZonedDecimal().isAPartOf(filler2,24);
	public ZonedDecimalData zchgamt04 = DD.zchgamt.copyToZonedDecimal().isAPartOf(filler2,36);
	public ZonedDecimalData zchgamt05 = DD.zchgamt.copyToZonedDecimal().isAPartOf(filler2,48);//BRD-NBP-010
	public FixedLengthStringData zgrsamts = new FixedLengthStringData(60).isAPartOf(dataFields, 185);
	public ZonedDecimalData[] zgrsamt = ZDArrayPartOfStructure(5, 12, 2, zgrsamts, 0);
	//public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(zgrsamts, 0, FILLER_REDEFINE);
	public FixedLengthStringData filler3 = new FixedLengthStringData(60).isAPartOf(zgrsamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData zgrsamt01 = DD.zgrsamt.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData zgrsamt02 = DD.zgrsamt.copyToZonedDecimal().isAPartOf(filler3,12);
	public ZonedDecimalData zgrsamt03 = DD.zgrsamt.copyToZonedDecimal().isAPartOf(filler3,24);
	public ZonedDecimalData zgrsamt04 = DD.zgrsamt.copyToZonedDecimal().isAPartOf(filler3,36);
	public ZonedDecimalData zgrsamt05 = DD.zgrsamt.copyToZonedDecimal().isAPartOf(filler3, 48);//BRD-NBP-010
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,245);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,247);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,255);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,263);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,273);
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(dataFields,276);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,279);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,287);
	public FixedLengthStringData effdates = new FixedLengthStringData(40).isAPartOf(dataFields, 317);
	public ZonedDecimalData[] effdate = ZDArrayPartOfStructure(5, 8, 0, effdates, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(effdates, 0, FILLER_REDEFINE);
	public ZonedDecimalData effdate01 = DD.effdate.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData effdate02 = DD.effdate.copyToZonedDecimal().isAPartOf(filler4,8);
	public ZonedDecimalData effdate03 = DD.effdate.copyToZonedDecimal().isAPartOf(filler4,16);
	public ZonedDecimalData effdate04 = DD.effdate.copyToZonedDecimal().isAPartOf(filler4,24);
	public ZonedDecimalData effdate05 = DD.effdate.copyToZonedDecimal().isAPartOf(filler4,32);
	public FixedLengthStringData freqdesc = DD.freqdesc.copy().isAPartOf(dataFields,357);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,387);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,434);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,442);
	public FixedLengthStringData mopdesc = DD.mopdesc.copy().isAPartOf(dataFields,443);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,473);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,481);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,528);
	public FixedLengthStringData prtshd = DD.prtshd.copy().isAPartOf(dataFields,538);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,539);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,547);
	public FixedLengthStringData zloanamts = new FixedLengthStringData(72).isAPartOf(dataFields, 550);
	public ZonedDecimalData[] zloanamt = ZDArrayPartOfStructure(6, 12, 2, zloanamts, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(72).isAPartOf(zloanamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData zloanamt01 = DD.zloanamt.copyToZonedDecimal().isAPartOf(filler5,0);
	public ZonedDecimalData zloanamt02 = DD.zloanamt.copyToZonedDecimal().isAPartOf(filler5,12);
	public ZonedDecimalData zloanamt03 = DD.zloanamt.copyToZonedDecimal().isAPartOf(filler5,24);
	public ZonedDecimalData zloanamt04 = DD.zloanamt.copyToZonedDecimal().isAPartOf(filler5,36);
	public ZonedDecimalData zloanamt05 = DD.zloanamt.copyToZonedDecimal().isAPartOf(filler5,48);
	public ZonedDecimalData zloanamt06 = DD.zloanamt.copyToZonedDecimal().isAPartOf(filler5,60); 
	public FixedLengthStringData billday = DD.billday.copy().isAPartOf(dataFields,622);//ILIFE-3735
	public FixedLengthStringData actionflag =DD.action.copy().isAPartOf(dataFields,624);
	public FixedLengthStringData stampduty = new FixedLengthStringData(85).isAPartOf(dataFields, 625);
	public ZonedDecimalData[] zstpduty = ZDArrayPartOfStructure(5, 12, 2, stampduty, 0);
	public FixedLengthStringData filler36 = new FixedLengthStringData(85).isAPartOf(stampduty, 0, FILLER_REDEFINE);
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(filler36,0);
	public ZonedDecimalData zstpduty02 = DD.zstpduty.copyToZonedDecimal().isAPartOf(filler36,17);
	public ZonedDecimalData zstpduty03 = DD.zstpduty.copyToZonedDecimal().isAPartOf(filler36,34);
	public ZonedDecimalData zstpduty04 = DD.zstpduty.copyToZonedDecimal().isAPartOf(filler36,51);
	public ZonedDecimalData zstpduty05 = DD.zstpduty.copyToZonedDecimal().isAPartOf(filler36,68);
	public ZonedDecimalData nextinsdte = DD.nextinsdte.copyToZonedDecimal().isAPartOf(dataFields,710);//IBPLIFE-4822
	public FixedLengthStringData tmpChg = new FixedLengthStringData(1).isAPartOf(dataFields, 718);//IBPLIFE-4822
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData aiindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData amtfldsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] amtfldErr = FLSArrayPartOfStructure(5, 4, amtfldsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(20).isAPartOf(amtfldsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData amtfld01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData amtfld02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData amtfld03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData amtfld04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData amtfld05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData crcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData grpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData payindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData totfldsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] totfldErr = FLSArrayPartOfStructure(5, 4, totfldsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(20).isAPartOf(totfldsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData totfld01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData totfld02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData totfld03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData totfld04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData totfld05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData zchgamtsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] zchgamtErr = FLSArrayPartOfStructure(5, 4, zchgamtsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(20).isAPartOf(zchgamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zchgamt01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData zchgamt02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData zchgamt03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData zchgamt04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData zchgamt05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData zgrsamtsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData[] zgrsamtErr = FLSArrayPartOfStructure(5, 4, zgrsamtsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(20).isAPartOf(zgrsamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zgrsamt01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData zgrsamt02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData zgrsamt03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData zgrsamt04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData zgrsamt05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,104);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData cnttypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData effdatesErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData[] effdateErr = FLSArrayPartOfStructure(5, 4, effdatesErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(20).isAPartOf(effdatesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData effdate01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData effdate02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData effdate03Err = new FixedLengthStringData(4).isAPartOf(filler10, 8);
	public FixedLengthStringData effdate04Err = new FixedLengthStringData(4).isAPartOf(filler10, 12);
	public FixedLengthStringData effdate05Err = new FixedLengthStringData(4).isAPartOf(filler10, 16);
	public FixedLengthStringData freqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData mopdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData prtshdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData zloanamtsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData[] zloanamtErr = FLSArrayPartOfStructure(6, 4, zloanamtsErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(24).isAPartOf(zloanamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zloanamt01Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData zloanamt02Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData zloanamt03Err = new FixedLengthStringData(4).isAPartOf(filler11, 8);
	public FixedLengthStringData zloanamt04Err = new FixedLengthStringData(4).isAPartOf(filler11, 12);
	public FixedLengthStringData zloanamt05Err = new FixedLengthStringData(4).isAPartOf(filler11, 16);
	public FixedLengthStringData zloanamt06Err = new FixedLengthStringData(4).isAPartOf(filler11, 20);
	public FixedLengthStringData billdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData stampdutyErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData[] zstpdutyErr = FLSArrayPartOfStructure(5, 4, stampdutyErr, 0);
	public FixedLengthStringData filler37 = new FixedLengthStringData(20).isAPartOf(stampdutyErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(filler37, 0);
	public FixedLengthStringData zstpduty02Err = new FixedLengthStringData(4).isAPartOf(filler37, 4);
	public FixedLengthStringData zstpduty03Err = new FixedLengthStringData(4).isAPartOf(filler37, 8);
	public FixedLengthStringData zstpduty04Err = new FixedLengthStringData(4).isAPartOf(filler37, 12);
	public FixedLengthStringData zstpduty05Err = new FixedLengthStringData(4).isAPartOf(filler37, 16);
	public FixedLengthStringData nextinsdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData tmpChgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] aiindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData amtfldsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] amtfldOut = FLSArrayPartOfStructure(5, 12, amtfldsOut, 0);
	public FixedLengthStringData[][] amtfldO = FLSDArrayPartOfArrayStructure(12, 1, amtfldOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(60).isAPartOf(amtfldsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] amtfld01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] amtfld02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] amtfld03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] amtfld04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] amtfld05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData[] crcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] grpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] payindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData totfldsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] totfldOut = FLSArrayPartOfStructure(5, 12, totfldsOut, 0);
	public FixedLengthStringData[][] totfldO = FLSDArrayPartOfArrayStructure(12, 1, totfldOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(60).isAPartOf(totfldsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] totfld01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] totfld02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] totfld03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] totfld04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] totfld05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData zchgamtsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] zchgamtOut = FLSArrayPartOfStructure(5, 12, zchgamtsOut, 0);
	public FixedLengthStringData[][] zchgamtO = FLSDArrayPartOfArrayStructure(12, 1, zchgamtOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(60).isAPartOf(zchgamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zchgamt01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] zchgamt02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] zchgamt03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] zchgamt04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] zchgamt05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData zgrsamtsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 240);
	public FixedLengthStringData[] zgrsamtOut = FLSArrayPartOfStructure(5, 12, zgrsamtsOut, 0);
	public FixedLengthStringData[][] zgrsamtO = FLSDArrayPartOfArrayStructure(12, 1, zgrsamtOut, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(60).isAPartOf(zgrsamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zgrsamt01Out = FLSArrayPartOfStructure(12, 1, filler15, 0);
	public FixedLengthStringData[] zgrsamt02Out = FLSArrayPartOfStructure(12, 1, filler15, 12);
	public FixedLengthStringData[] zgrsamt03Out = FLSArrayPartOfStructure(12, 1, filler15, 24);
	public FixedLengthStringData[] zgrsamt04Out = FLSArrayPartOfStructure(12, 1, filler15, 36);
	public FixedLengthStringData[] zgrsamt05Out = FLSArrayPartOfStructure(12, 1, filler15, 48);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] cnttypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData effdatesOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 396);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(5, 12, effdatesOut, 0);
	public FixedLengthStringData[][] effdateO = FLSDArrayPartOfArrayStructure(12, 1, effdateOut, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(60).isAPartOf(effdatesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] effdate01Out = FLSArrayPartOfStructure(12, 1, filler16, 0);
	public FixedLengthStringData[] effdate02Out = FLSArrayPartOfStructure(12, 1, filler16, 12);
	public FixedLengthStringData[] effdate03Out = FLSArrayPartOfStructure(12, 1, filler16, 24);
	public FixedLengthStringData[] effdate04Out = FLSArrayPartOfStructure(12, 1, filler16, 36);
	public FixedLengthStringData[] effdate05Out = FLSArrayPartOfStructure(12, 1, filler16, 48);
	public FixedLengthStringData[] freqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] mopdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] prtshdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData zloanamtsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 588);
	public FixedLengthStringData[] zloanamtOut = FLSArrayPartOfStructure(6, 12, zloanamtsOut, 0);
	public FixedLengthStringData[][] zloanamtO = FLSDArrayPartOfArrayStructure(12, 1, zloanamtOut, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(72).isAPartOf(zloanamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zloanamt01Out = FLSArrayPartOfStructure(12, 1, filler17, 0);
	public FixedLengthStringData[] zloanamt02Out = FLSArrayPartOfStructure(12, 1, filler17, 12);
	public FixedLengthStringData[] zloanamt03Out = FLSArrayPartOfStructure(12, 1, filler17, 24);
	public FixedLengthStringData[] zloanamt04Out = FLSArrayPartOfStructure(12, 1, filler17, 36);
	public FixedLengthStringData[] zloanamt05Out = FLSArrayPartOfStructure(12, 1, filler17, 48);
	public FixedLengthStringData[] zloanamt06Out = FLSArrayPartOfStructure(12, 1, filler17, 60);
	public FixedLengthStringData[] billdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData stampdutyOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 672);
	public FixedLengthStringData[] zstpdutyOut = FLSArrayPartOfStructure(5, 12, stampdutyOut, 0);
	public FixedLengthStringData[][] zstpdutyO = FLSDArrayPartOfArrayStructure(12, 1, zstpdutyOut, 0);
	public FixedLengthStringData filler38 = new FixedLengthStringData(60).isAPartOf(stampdutyOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zstpduty01Out = FLSArrayPartOfStructure(12, 1, filler38, 0);
	public FixedLengthStringData[] zstpduty02Out = FLSArrayPartOfStructure(12, 1, filler38, 12);
	public FixedLengthStringData[] zstpduty03Out = FLSArrayPartOfStructure(12, 1, filler38, 24);
	public FixedLengthStringData[] zstpduty04Out = FLSArrayPartOfStructure(12, 1, filler38, 36);
	public FixedLengthStringData[] zstpduty05Out = FLSArrayPartOfStructure(12, 1, filler38, 48);
	public FixedLengthStringData[] nextinsdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] tmpChgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	public FixedLengthStringData subfileArea = new FixedLengthStringData(3245);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(2666).isAPartOf(subfileArea, 0);
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData basprms = new FixedLengthStringData(65).isAPartOf(subfileFields, 1);
	public ZonedDecimalData[] basprm = ZDArrayPartOfStructure(5, 13, 2, basprms, 0);
	public FixedLengthStringData filler18 = new FixedLengthStringData(65).isAPartOf(basprms, 0, FILLER_REDEFINE);
	public ZonedDecimalData basprm01 = DD.basprm.copyToZonedDecimal().isAPartOf(filler18,0);
	public ZonedDecimalData basprm02 = DD.basprm.copyToZonedDecimal().isAPartOf(filler18,13);
	public ZonedDecimalData basprm03 = DD.basprm.copyToZonedDecimal().isAPartOf(filler18,26);
	public ZonedDecimalData basprm04 = DD.basprm.copyToZonedDecimal().isAPartOf(filler18,39);
	public ZonedDecimalData basprm05 = DD.basprm.copyToZonedDecimal().isAPartOf(filler18,52);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,66);
	public FixedLengthStringData datakey = DD.datakey.copy().isAPartOf(subfileFields,70);
	public FixedLengthStringData entity = DD.entity1.copy().isAPartOf(subfileFields,326);
	public FixedLengthStringData premind = DD.premind.copy().isAPartOf(subfileFields,358);
	public FixedLengthStringData prems = new FixedLengthStringData(85).isAPartOf(subfileFields, 359);
	public ZonedDecimalData[] prem = ZDArrayPartOfStructure(5, 17, 2, prems, 0);
	public FixedLengthStringData filler19 = new FixedLengthStringData(85).isAPartOf(prems, 0, FILLER_REDEFINE);
	public ZonedDecimalData prem01 = DD.prem.copyToZonedDecimal().isAPartOf(filler19,0);
	public ZonedDecimalData prem02 = DD.prem.copyToZonedDecimal().isAPartOf(filler19,17);
	public ZonedDecimalData prem03 = DD.prem.copyToZonedDecimal().isAPartOf(filler19,34);
	public ZonedDecimalData prem04 = DD.prem.copyToZonedDecimal().isAPartOf(filler19,51);
	public ZonedDecimalData prem05 = DD.prem.copyToZonedDecimal().isAPartOf(filler19,68);
	public FixedLengthStringData prmtrbs = new FixedLengthStringData(60).isAPartOf(subfileFields, 444);
	public ZonedDecimalData[] prmtrb = ZDArrayPartOfStructure(5, 12, 2, prmtrbs, 0);
	public FixedLengthStringData filler20 = new FixedLengthStringData(60).isAPartOf(prmtrbs, 0, FILLER_REDEFINE);
	public ZonedDecimalData prmtrb01 = DD.prmtrb.copyToZonedDecimal().isAPartOf(filler20,0);
	public ZonedDecimalData prmtrb02 = DD.prmtrb.copyToZonedDecimal().isAPartOf(filler20,12);
	public ZonedDecimalData prmtrb03 = DD.prmtrb.copyToZonedDecimal().isAPartOf(filler20,24);
	public ZonedDecimalData prmtrb04 = DD.prmtrb.copyToZonedDecimal().isAPartOf(filler20,36);
	public ZonedDecimalData prmtrb05 = DD.prmtrb.copyToZonedDecimal().isAPartOf(filler20,48);
	public ZonedDecimalData pstatdate = DD.pstdat.copyToZonedDecimal().isAPartOf(subfileFields,504);
	public FixedLengthStringData suminss = new FixedLengthStringData(85).isAPartOf(subfileFields, 512);
	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(5, 17, 2, suminss, 0);
	public FixedLengthStringData filler21 = new FixedLengthStringData(85).isAPartOf(suminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumins01 = DD.sumins.copyToZonedDecimal().isAPartOf(filler21,0);
	public ZonedDecimalData sumins02 = DD.sumins.copyToZonedDecimal().isAPartOf(filler21,17);
	public ZonedDecimalData sumins03 = DD.sumins.copyToZonedDecimal().isAPartOf(filler21,34);
	public ZonedDecimalData sumins04 = DD.sumins.copyToZonedDecimal().isAPartOf(filler21,51);
	public ZonedDecimalData sumins05 = DD.sumins.copyToZonedDecimal().isAPartOf(filler21,68);
	public FixedLengthStringData workAreaData = DD.workarea.copy().isAPartOf(subfileFields,597);
	public FixedLengthStringData zloprms = new FixedLengthStringData(65).isAPartOf(subfileFields, 2597);
	public ZonedDecimalData[] zloprm = ZDArrayPartOfStructure(5, 13, 2, zloprms, 0);
	public FixedLengthStringData filler22 = new FixedLengthStringData(65).isAPartOf(zloprms, 0, FILLER_REDEFINE);
	public ZonedDecimalData zloprm01 = DD.zloprm.copyToZonedDecimal().isAPartOf(filler22,0);
	public ZonedDecimalData zloprm02 = DD.zloprm.copyToZonedDecimal().isAPartOf(filler22,13);
	public ZonedDecimalData zloprm03 = DD.zloprm.copyToZonedDecimal().isAPartOf(filler22,26);
	public ZonedDecimalData zloprm04 = DD.zloprm.copyToZonedDecimal().isAPartOf(filler22,39);
	public ZonedDecimalData zloprm05 = DD.zloprm.copyToZonedDecimal().isAPartOf(filler22,52);
	public FixedLengthStringData zrwvflgs = new FixedLengthStringData(4).isAPartOf(subfileFields, 2662);
	public FixedLengthStringData[] zrwvflg = FLSArrayPartOfStructure(4, 1, zrwvflgs, 0);
	public FixedLengthStringData filler23 = new FixedLengthStringData(4).isAPartOf(zrwvflgs, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01 = DD.zrwvflg.copy().isAPartOf(filler23,0);
	public FixedLengthStringData zrwvflg02 = DD.zrwvflg.copy().isAPartOf(filler23,1);
	public FixedLengthStringData zrwvflg03 = DD.zrwvflg.copy().isAPartOf(filler23,2);
	public FixedLengthStringData zrwvflg04 = DD.zrwvflg.copy().isAPartOf(filler23,3);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 2666);
	public FixedLengthStringData actindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData basprmsErr = new FixedLengthStringData(20).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData[] basprmErr = FLSArrayPartOfStructure(5, 4, basprmsErr, 0);
	public FixedLengthStringData filler24 = new FixedLengthStringData(20).isAPartOf(basprmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData basprm01Err = new FixedLengthStringData(4).isAPartOf(filler24, 0);
	public FixedLengthStringData basprm02Err = new FixedLengthStringData(4).isAPartOf(filler24, 4);
	public FixedLengthStringData basprm03Err = new FixedLengthStringData(4).isAPartOf(filler24, 8);
	public FixedLengthStringData basprm04Err = new FixedLengthStringData(4).isAPartOf(filler24, 12);
	public FixedLengthStringData basprm05Err = new FixedLengthStringData(4).isAPartOf(filler24, 16);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData datakeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData premindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData premsErr = new FixedLengthStringData(20).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData[] premErr = FLSArrayPartOfStructure(5, 4, premsErr, 0);
	public FixedLengthStringData filler25 = new FixedLengthStringData(20).isAPartOf(premsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData prem01Err = new FixedLengthStringData(4).isAPartOf(filler25, 0);
	public FixedLengthStringData prem02Err = new FixedLengthStringData(4).isAPartOf(filler25, 4);
	public FixedLengthStringData prem03Err = new FixedLengthStringData(4).isAPartOf(filler25, 8);
	public FixedLengthStringData prem04Err = new FixedLengthStringData(4).isAPartOf(filler25, 12);
	public FixedLengthStringData prem05Err = new FixedLengthStringData(4).isAPartOf(filler25, 16);
	public FixedLengthStringData prmtrbsErr = new FixedLengthStringData(20).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData[] prmtrbErr = FLSArrayPartOfStructure(5, 4, prmtrbsErr, 0);
	public FixedLengthStringData filler26 = new FixedLengthStringData(20).isAPartOf(prmtrbsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData prmtrb01Err = new FixedLengthStringData(4).isAPartOf(filler26, 0);
	public FixedLengthStringData prmtrb02Err = new FixedLengthStringData(4).isAPartOf(filler26, 4);
	public FixedLengthStringData prmtrb03Err = new FixedLengthStringData(4).isAPartOf(filler26, 8);
	public FixedLengthStringData prmtrb04Err = new FixedLengthStringData(4).isAPartOf(filler26, 12);
	public FixedLengthStringData prmtrb05Err = new FixedLengthStringData(4).isAPartOf(filler26, 16);
	public FixedLengthStringData pstdatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 80);
	public FixedLengthStringData suminssErr = new FixedLengthStringData(20).isAPartOf(errorSubfile, 84);
	public FixedLengthStringData[] suminsErr = FLSArrayPartOfStructure(5, 4, suminssErr, 0);
	public FixedLengthStringData filler27 = new FixedLengthStringData(20).isAPartOf(suminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumins01Err = new FixedLengthStringData(4).isAPartOf(filler27, 0);
	public FixedLengthStringData sumins02Err = new FixedLengthStringData(4).isAPartOf(filler27, 4);
	public FixedLengthStringData sumins03Err = new FixedLengthStringData(4).isAPartOf(filler27, 8);
	public FixedLengthStringData sumins04Err = new FixedLengthStringData(4).isAPartOf(filler27, 12);
	public FixedLengthStringData sumins05Err = new FixedLengthStringData(4).isAPartOf(filler27, 16);
	public FixedLengthStringData workareaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 104);
	public FixedLengthStringData zloprmsErr = new FixedLengthStringData(20).isAPartOf(errorSubfile, 108);
	public FixedLengthStringData[] zloprmErr = FLSArrayPartOfStructure(5, 4, zloprmsErr, 0);
	public FixedLengthStringData filler28 = new FixedLengthStringData(20).isAPartOf(zloprmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zloprm01Err = new FixedLengthStringData(4).isAPartOf(filler28, 0);
	public FixedLengthStringData zloprm02Err = new FixedLengthStringData(4).isAPartOf(filler28, 4);
	public FixedLengthStringData zloprm03Err = new FixedLengthStringData(4).isAPartOf(filler28, 8);
	public FixedLengthStringData zloprm04Err = new FixedLengthStringData(4).isAPartOf(filler28, 12);
	public FixedLengthStringData zloprm05Err = new FixedLengthStringData(4).isAPartOf(filler28, 16);
	public FixedLengthStringData zrwvflgsErr = new FixedLengthStringData(16).isAPartOf(errorSubfile, 128);
	public FixedLengthStringData[] zrwvflgErr = FLSArrayPartOfStructure(4, 4, zrwvflgsErr, 0);
	public FixedLengthStringData filler29 = new FixedLengthStringData(16).isAPartOf(zrwvflgsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01Err = new FixedLengthStringData(4).isAPartOf(filler29, 0);
	public FixedLengthStringData zrwvflg02Err = new FixedLengthStringData(4).isAPartOf(filler29, 4);
	public FixedLengthStringData zrwvflg03Err = new FixedLengthStringData(4).isAPartOf(filler29, 8);
	public FixedLengthStringData zrwvflg04Err = new FixedLengthStringData(4).isAPartOf(filler29, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(432).isAPartOf(subfileArea, 2810);
	public FixedLengthStringData[] actindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData basprmsOut = new FixedLengthStringData(60).isAPartOf(outputSubfile, 12);
	public FixedLengthStringData[] basprmOut = FLSArrayPartOfStructure(5, 12, basprmsOut, 0);
	public FixedLengthStringData[][] basprmO = FLSDArrayPartOfArrayStructure(12, 1, basprmOut, 0);
	public FixedLengthStringData filler30 = new FixedLengthStringData(60).isAPartOf(basprmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] basprm01Out = FLSArrayPartOfStructure(12, 1, filler30, 0);
	public FixedLengthStringData[] basprm02Out = FLSArrayPartOfStructure(12, 1, filler30, 12);
	public FixedLengthStringData[] basprm03Out = FLSArrayPartOfStructure(12, 1, filler30, 24);
	public FixedLengthStringData[] basprm04Out = FLSArrayPartOfStructure(12, 1, filler30, 36);
	public FixedLengthStringData[] basprm05Out = FLSArrayPartOfStructure(12, 1, filler30, 48);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile,72);
	public FixedLengthStringData[] datakeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] premindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData premsOut = new FixedLengthStringData(60).isAPartOf(outputSubfile, 120);
	public FixedLengthStringData[] premOut = FLSArrayPartOfStructure(5, 12, premsOut, 0);
	public FixedLengthStringData[][] premO = FLSDArrayPartOfArrayStructure(12, 1, premOut, 0);
	public FixedLengthStringData filler31 = new FixedLengthStringData(60).isAPartOf(premsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] prem01Out = FLSArrayPartOfStructure(12, 1, filler31, 0);
	public FixedLengthStringData[] prem02Out = FLSArrayPartOfStructure(12, 1, filler31, 12);
	public FixedLengthStringData[] prem03Out = FLSArrayPartOfStructure(12, 1, filler31, 24);
	public FixedLengthStringData[] prem04Out = FLSArrayPartOfStructure(12, 1, filler31, 36);
	public FixedLengthStringData[] prem05Out = FLSArrayPartOfStructure(12, 1, filler31, 48);
	public FixedLengthStringData prmtrbsOut = new FixedLengthStringData(60).isAPartOf(outputSubfile, 180);
	public FixedLengthStringData[] prmtrbOut = FLSArrayPartOfStructure(5, 12, prmtrbsOut, 0);
	public FixedLengthStringData[][] prmtrbO = FLSDArrayPartOfArrayStructure(12, 1, prmtrbOut, 0);
	public FixedLengthStringData filler32 = new FixedLengthStringData(60).isAPartOf(prmtrbsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] prmtrb01Out = FLSArrayPartOfStructure(12, 1, filler32, 0);
	public FixedLengthStringData[] prmtrb02Out = FLSArrayPartOfStructure(12, 1, filler32, 12);
	public FixedLengthStringData[] prmtrb03Out = FLSArrayPartOfStructure(12, 1, filler32, 24);
	public FixedLengthStringData[] prmtrb04Out = FLSArrayPartOfStructure(12, 1, filler32, 36);
	public FixedLengthStringData[] prmtrb05Out = FLSArrayPartOfStructure(12, 1, filler32, 48);
	public FixedLengthStringData[] pstdatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 240);
	public FixedLengthStringData suminssOut = new FixedLengthStringData(60).isAPartOf(outputSubfile, 252);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(5, 12, suminssOut, 0);
	public FixedLengthStringData[][] suminsO = FLSDArrayPartOfArrayStructure(12, 1, suminsOut, 0);
	public FixedLengthStringData filler33 = new FixedLengthStringData(60).isAPartOf(suminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumins01Out = FLSArrayPartOfStructure(12, 1, filler33, 0);
	public FixedLengthStringData[] sumins02Out = FLSArrayPartOfStructure(12, 1, filler33, 12);
	public FixedLengthStringData[] sumins03Out = FLSArrayPartOfStructure(12, 1, filler33, 24);
	public FixedLengthStringData[] sumins04Out = FLSArrayPartOfStructure(12, 1, filler33, 36);
	public FixedLengthStringData[] sumins05Out = FLSArrayPartOfStructure(12, 1, filler33, 48);
	public FixedLengthStringData[] workareaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 312);
	public FixedLengthStringData zloprmsOut = new FixedLengthStringData(60).isAPartOf(outputSubfile, 324);
	public FixedLengthStringData[] zloprmOut = FLSArrayPartOfStructure(5, 12, zloprmsOut, 0);
	public FixedLengthStringData[][] zloprmO = FLSDArrayPartOfArrayStructure(12, 1, zloprmOut, 0);
	public FixedLengthStringData filler34 = new FixedLengthStringData(60).isAPartOf(zloprmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zloprm01Out = FLSArrayPartOfStructure(12, 1, filler34, 0);
	public FixedLengthStringData[] zloprm02Out = FLSArrayPartOfStructure(12, 1, filler34, 12);
	public FixedLengthStringData[] zloprm03Out = FLSArrayPartOfStructure(12, 1, filler34, 24);
	public FixedLengthStringData[] zloprm04Out = FLSArrayPartOfStructure(12, 1, filler34, 36);
	public FixedLengthStringData[] zloprm05Out = FLSArrayPartOfStructure(12, 1, filler34, 48);
	public FixedLengthStringData zrwvflgsOut = new FixedLengthStringData(48).isAPartOf(outputSubfile, 384);
	public FixedLengthStringData[] zrwvflgOut = FLSArrayPartOfStructure(4, 12, zrwvflgsOut, 0);
	public FixedLengthStringData[][] zrwvflgO = FLSDArrayPartOfArrayStructure(12, 1, zrwvflgOut, 0);
	public FixedLengthStringData filler35 = new FixedLengthStringData(48).isAPartOf(zrwvflgsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrwvflg01Out = FLSArrayPartOfStructure(12, 1, filler35, 0);
	public FixedLengthStringData[] zrwvflg02Out = FLSArrayPartOfStructure(12, 1, filler35, 12);
	public FixedLengthStringData[] zrwvflg03Out = FLSArrayPartOfStructure(12, 1, filler35, 24);
	public FixedLengthStringData[] zrwvflg04Out = FLSArrayPartOfStructure(12, 1, filler35, 36);

	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 3242);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdate01Disp = new FixedLengthStringData(10);
	public FixedLengthStringData effdate02Disp = new FixedLengthStringData(10);
	public FixedLengthStringData effdate03Disp = new FixedLengthStringData(10);
	public FixedLengthStringData effdate04Disp = new FixedLengthStringData(10);
	public FixedLengthStringData effdate05Disp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextinsdteDisp = new FixedLengthStringData(10);

	public LongData Sr674screensflWritten = new LongData(0);
	public LongData Sr674screenctlWritten = new LongData(0);
	public LongData Sr674screenWritten = new LongData(0);
	public LongData Sr674protectWritten = new LongData(0);
	public GeneralTable sr674screensfl = new GeneralTable(AppVars.getInstance());

	public FixedLengthStringData csbil003flag = new FixedLengthStringData(1);
	
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr674screensfl;
	}

	public Sr674ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(billfreqOut,new String[] {"01","15","-01","04", null, null, null, null, null, null, null, null});
		fieldIndMap.put(mopOut,new String[] {"02","15","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prtshdOut,new String[] {"03","88","-03","88",null, null, null, null, null, null, null, null});
		fieldIndMap.put(totfld01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totfld02Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totfld03Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totfld04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totfld05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payindOut,new String[] {null, "77",null, "77",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {null, "77",null, "77",null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcindOut,new String[] {null, "77",null, "77",null, null, null, null, null, null, null, null});
		fieldIndMap.put(grpindOut,new String[] {null, "77",null, "77",null, null, null, null, null, null, null, null});
		fieldIndMap.put(aiindOut,new String[] {null, "10",null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(amtfld01Out,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(amtfld02Out,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(amtfld03Out,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(billdayOut,new String[] {"72","73", "-72","74", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "42", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty02Out,new String[] {null, null, null, "42", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty03Out,new String[] {null, null, null, "42", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty04Out,new String[] {null, null, null, "42", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty05Out,new String[] {null, null, null, "42", null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		fieldIndMap.put(nextinsdteOut, new String[] {null,null, null,"29", null, null, null, null, null, null, null, null});//IBPLIFE-4822
		fieldIndMap.put(tmpChgOut,new String[] {"25","26", "-25","28", null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {datakey, pstatdate, sumins01, sumins02, sumins03, sumins04, sumins05, premind, activeInd, zrwvflg01, zrwvflg02, zrwvflg03, zrwvflg04, workAreaData, basprm01, basprm02, basprm03, basprm04, basprm05, zloprm01, zloprm02, zloprm03, zloprm04, zloprm05, prem01, prem02, prem03, prem04, prem05,crtable, entity, prmtrb01, prmtrb02, prmtrb03, prmtrb04, prmtrb05};
		screenSflOutFields = new BaseData[][] {datakeyOut, pstdatOut, sumins01Out, sumins02Out, sumins03Out, sumins04Out, sumins05Out, premindOut, actindOut, zrwvflg01Out, zrwvflg02Out, zrwvflg03Out, zrwvflg04Out, workareaOut, basprm01Out, basprm02Out, basprm03Out, basprm04Out, basprm05Out, zloprm01Out, zloprm02Out, zloprm03Out, zloprm04Out, zloprm05Out, zloprm05Out, prem01Out, prem02Out, prem03Out, prem04Out, prem05Out, crtableOut, entityOut, prmtrb01Out, prmtrb02Out, prmtrb03Out, prmtrb04Out, prmtrb05Out};
		screenSflErrFields = new BaseData[] {datakeyErr, pstdatErr, sumins01Err, sumins02Err, sumins03Err, sumins04Err, sumins05Err, premindErr, actindErr, zrwvflg01Err, zrwvflg02Err, zrwvflg03Err, zrwvflg04Err, workareaErr, basprm01Err, basprm02Err, basprm03Err, basprm04Err, basprm05Err, zloprm01Err, zloprm02Err, zloprm03Err, zloprm04Err, zloprm05Err, prem01Err, prem02Err, prem03Err, prem04Err, prem05Err, crtableErr, entityErr, prmtrb01Err, prmtrb02Err, prmtrb03Err, prmtrb04Err, prmtrb05Err};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		//screenFields = new BaseData[] {chdrnum, cnttyp, ctypedes, cntcurr, chdrstatus, premstatus, register, occdate, ptdate, btdate, cownnum, ownername, lifenum, lifename, billfreq, freqdesc, mop, mopdesc, prtshd, zloanamt01, zloanamt02, zloanamt03, zloanamt04, zloanamt05, zloanamt06, effdate01, effdate02, effdate03, effdate04, effdate05, zgrsamt01, zgrsamt02, zgrsamt03, zgrsamt04, zgrsamt05, zchgamt01, zchgamt02, zchgamt03, zchgamt04, zchgamt05, totfld01, totfld02, totfld03, totfld04, totfld05, payind, ddind, crcind, grpind, aiind, amtfld01, amtfld02, amtfld03, amtfld04, amtfld05,billday};
		screenFields=getscreenFields();
		//screenOutFields = new BaseData[][] {chdrnumOut, cnttypOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, occdateOut, ptdateOut, btdateOut, cownnumOut, ownernameOut, lifenumOut, lifenameOut, billfreqOut, freqdescOut, mopOut, mopdescOut, prtshdOut, zloanamt01Out, zloanamt02Out, zloanamt03Out, zloanamt04Out, zloanamt05Out, zloanamt06Out, effdate01Out, effdate02Out, effdate03Out, effdate04Out, effdate05Out, zgrsamt01Out, zgrsamt02Out, zgrsamt03Out, zgrsamt04Out, zgrsamt05Out, zchgamt01Out, zchgamt02Out, zchgamt03Out, zchgamt04Out, zchgamt05Out, totfld01Out, totfld02Out, totfld03Out, totfld04Out, totfld05Out, payindOut, ddindOut, crcindOut, grpindOut, aiindOut, amtfld01Out, amtfld02Out, amtfld03Out, amtfld04Out, amtfld05Out,billdayOut};
		screenOutFields=getscreenOutFields();
		//screenErrFields = new BaseData[] {chdrnumErr, cnttypErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, occdateErr, ptdateErr, btdateErr, cownnumErr, ownernameErr, lifenumErr, lifenameErr, billfreqErr, freqdescErr, mopErr, mopdescErr, prtshdErr, zloanamt01Err, zloanamt02Err, zloanamt03Err, zloanamt04Err, zloanamt05Err, zloanamt06Err, effdate01Err, effdate02Err, effdate03Err, effdate04Err, effdate05Err, zgrsamt01Err, zgrsamt02Err, zgrsamt03Err, zgrsamt04Err, zgrsamt05Err, zchgamt01Err, zchgamt02Err, zchgamt03Err, zchgamt04Err, zchgamt05Err, totfld01Err, totfld02Err, totfld03Err, totfld04Err, totfld05Err, payindErr, ddindErr, crcindErr, grpindErr, aiindErr, amtfld01Err, amtfld02Err, amtfld03Err, amtfld04Err, amtfld05Err,billdayErr};
		screenErrFields=getscreenErrFields();
		/*screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate01, effdate02, effdate03, effdate04, effdate05};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdate01Err, effdate02Err, effdate03Err, effdate04Err, effdate05Err};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdate01Disp, effdate02Disp, effdate03Disp, effdate04Disp, effdate05Disp};
        */
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields(); 
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr674screen.class;
		screenSflRecord = Sr674screensfl.class;
		screenCtlRecord = Sr674screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr674protect.class;
	}
	
	public int getDataAreaSize() {
		return 1727;
	}
	
	public int getDataFieldsSize(){
		return 719;
	}

	public int getErrorIndicatorSize(){
		return 252 ; 
	}
	
	public int getOutputFieldSize(){
		return 756; 
	}

	public BaseData[] getscreenFields() {
		return new BaseData[] { chdrnum, cnttyp, ctypedes, cntcurr, chdrstatus, premstatus, register, occdate, ptdate,
				btdate, cownnum, ownername, lifenum, lifename, billfreq, freqdesc, mop, mopdesc, prtshd, zloanamt01,
				zloanamt02, zloanamt03, zloanamt04, zloanamt05, zloanamt06, effdate01, effdate02, effdate03, effdate04,
				effdate05, zgrsamt01, zgrsamt02, zgrsamt03, zgrsamt04, zgrsamt05, zchgamt01, zchgamt02, zchgamt03,
				zchgamt04, zchgamt05, totfld01, totfld02, totfld03, totfld04, totfld05, payind, ddind, crcind, grpind,
				aiind, amtfld01, amtfld02, amtfld03, amtfld04, amtfld05, billday, zstpduty01, zstpduty02, zstpduty03,
				zstpduty04, zstpduty05, nextinsdte, tmpChg};
	}

	public BaseData[][] getscreenOutFields() {
		return new BaseData[][] { chdrnumOut, cnttypOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut,
				registerOut, occdateOut, ptdateOut, btdateOut, cownnumOut, ownernameOut, lifenumOut, lifenameOut,
				billfreqOut, freqdescOut, mopOut, mopdescOut, prtshdOut, zloanamt01Out, zloanamt02Out, zloanamt03Out,
				zloanamt04Out, zloanamt05Out, zloanamt06Out, effdate01Out, effdate02Out, effdate03Out, effdate04Out,
				effdate05Out, zgrsamt01Out, zgrsamt02Out, zgrsamt03Out, zgrsamt04Out, zgrsamt05Out, zchgamt01Out,
				zchgamt02Out, zchgamt03Out, zchgamt04Out, zchgamt05Out, totfld01Out, totfld02Out, totfld03Out,
				totfld04Out, totfld05Out, payindOut, ddindOut, crcindOut, grpindOut, aiindOut, amtfld01Out, amtfld02Out,
				amtfld03Out, amtfld04Out, amtfld05Out, billdayOut, zstpduty01Out, zstpduty02Out, zstpduty03Out,
				zstpduty04Out, zstpduty05Out, nextinsdteOut, tmpChgOut};
	}
	
	public BaseData[] getscreenErrFields() {
		return new BaseData[] { chdrnumErr, cnttypErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr,
				registerErr, occdateErr, ptdateErr, btdateErr, cownnumErr, ownernameErr, lifenumErr, lifenameErr,
				billfreqErr, freqdescErr, mopErr, mopdescErr, prtshdErr, zloanamt01Err, zloanamt02Err, zloanamt03Err,
				zloanamt04Err, zloanamt05Err, zloanamt06Err, effdate01Err, effdate02Err, effdate03Err, effdate04Err,
				effdate05Err, zgrsamt01Err, zgrsamt02Err, zgrsamt03Err, zgrsamt04Err, zgrsamt05Err, zchgamt01Err,
				zchgamt02Err, zchgamt03Err, zchgamt04Err, zchgamt05Err, totfld01Err, totfld02Err, totfld03Err,
				totfld04Err, totfld05Err, payindErr, ddindErr, crcindErr, grpindErr, aiindErr, amtfld01Err, amtfld02Err,
				amtfld03Err, amtfld04Err, amtfld05Err, billdayErr, zstpduty01Err, zstpduty02Err, zstpduty03Err, 
				zstpduty04Err, zstpduty05Err,nextinsdteErr, tmpChgErr };

	}
	
	public BaseData[] getscreenDateFields() {
		return new BaseData[] {occdate, ptdate, btdate, effdate01, effdate02, effdate03, effdate04, effdate05, nextinsdte};
	}

	public BaseData[] getscreenDateDispFields() {
		return new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdate01Disp, effdate02Disp, effdate03Disp, effdate04Disp, effdate05Disp, nextinsdteDisp};
	}

	public BaseData[] getscreenDateErrFields() {
		return new BaseData[] {occdateErr, ptdateErr, btdateErr, effdate01Err, effdate02Err, effdate03Err, effdate04Err, effdate05Err, nextinsdteErr};
	}
	
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr674screenctl.lrec.pageSubfile);
	}
}
