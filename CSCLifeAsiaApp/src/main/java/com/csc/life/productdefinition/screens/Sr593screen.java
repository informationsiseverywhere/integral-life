package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr593screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 13, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr593ScreenVars sv = (Sr593ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr593screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr593ScreenVars screenVars = (Sr593ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.chdrtype01.setClassString("");
		screenVars.chdrtype02.setClassString("");
		screenVars.chdrtype03.setClassString("");
		screenVars.chdrtype04.setClassString("");
		screenVars.chdrtype05.setClassString("");
		screenVars.chdrtype06.setClassString("");
		screenVars.chdrtype07.setClassString("");
		screenVars.chdrtype08.setClassString("");
		screenVars.chdrtype09.setClassString("");
		screenVars.chdrtype10.setClassString("");
		screenVars.chdrtype11.setClassString("");
		screenVars.chdrtype12.setClassString("");
		screenVars.chdrtype13.setClassString("");
		screenVars.chdrtype14.setClassString("");
		screenVars.chdrtype15.setClassString("");
		screenVars.chdrtype16.setClassString("");
		screenVars.chdrtype17.setClassString("");
		screenVars.chdrtype18.setClassString("");
		screenVars.chdrtype19.setClassString("");
		screenVars.chdrtype20.setClassString("");
		screenVars.chdrtype21.setClassString("");
		screenVars.chdrtype22.setClassString("");
		screenVars.chdrtype23.setClassString("");
		screenVars.chdrtype24.setClassString("");
		screenVars.chdrtype25.setClassString("");
		screenVars.chdrtype26.setClassString("");
		screenVars.chdrtype27.setClassString("");
		screenVars.chdrtype28.setClassString("");
		screenVars.chdrtype29.setClassString("");
		screenVars.chdrtype30.setClassString("");
		screenVars.chdrtype31.setClassString("");
		screenVars.chdrtype32.setClassString("");
		screenVars.chdrtype33.setClassString("");
		screenVars.chdrtype34.setClassString("");
		screenVars.chdrtype35.setClassString("");
		screenVars.chdrtype36.setClassString("");
		screenVars.chdrtype37.setClassString("");
		screenVars.chdrtype38.setClassString("");
		screenVars.chdrtype39.setClassString("");
		screenVars.chdrtype40.setClassString("");
	}

/**
 * Clear all the variables in Sr593screen
 */
	public static void clear(VarModel pv) {
		Sr593ScreenVars screenVars = (Sr593ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.chdrtype01.clear();
		screenVars.chdrtype02.clear();
		screenVars.chdrtype03.clear();
		screenVars.chdrtype04.clear();
		screenVars.chdrtype05.clear();
		screenVars.chdrtype06.clear();
		screenVars.chdrtype07.clear();
		screenVars.chdrtype08.clear();
		screenVars.chdrtype09.clear();
		screenVars.chdrtype10.clear();
		screenVars.chdrtype11.clear();
		screenVars.chdrtype12.clear();
		screenVars.chdrtype13.clear();
		screenVars.chdrtype14.clear();
		screenVars.chdrtype15.clear();
		screenVars.chdrtype16.clear();
		screenVars.chdrtype17.clear();
		screenVars.chdrtype18.clear();
		screenVars.chdrtype19.clear();
		screenVars.chdrtype20.clear();
		screenVars.chdrtype21.clear();
		screenVars.chdrtype22.clear();
		screenVars.chdrtype23.clear();
		screenVars.chdrtype24.clear();
		screenVars.chdrtype25.clear();
		screenVars.chdrtype26.clear();
		screenVars.chdrtype27.clear();
		screenVars.chdrtype28.clear();
		screenVars.chdrtype29.clear();
		screenVars.chdrtype30.clear();
		screenVars.chdrtype31.clear();
		screenVars.chdrtype32.clear();
		screenVars.chdrtype33.clear();
		screenVars.chdrtype34.clear();
		screenVars.chdrtype35.clear();
		screenVars.chdrtype36.clear();
		screenVars.chdrtype37.clear();
		screenVars.chdrtype38.clear();
		screenVars.chdrtype39.clear();
		screenVars.chdrtype40.clear();
	}
}
