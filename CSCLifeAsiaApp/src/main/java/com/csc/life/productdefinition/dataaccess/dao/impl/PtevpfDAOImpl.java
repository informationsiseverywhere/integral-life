package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.PtevpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Ptevpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PtevpfDAOImpl extends BaseDAOImpl<Ptevpf> implements PtevpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(PtevpfDAOImpl.class);

    public void insertPtevpfRecord(List<Ptevpf> ptevpfList) {

        if (ptevpfList != null) {
            String SQL_PTEV_INSERT = "INSERT INTO PTEVPF(CHDRCOY,CHDRNUM,EFFDATE,PTRNEFF,TRANNO,VALIDFLAG,BATCACTYR,BATCACTMN,BATCTRCDE,USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement psPtevInsert = getPrepareStatement(SQL_PTEV_INSERT);
            try {
                for (Ptevpf p : ptevpfList) {
                    psPtevInsert.setString(1, p.getChdrcoy());
                    psPtevInsert.setString(2, p.getChdrnum());
                    psPtevInsert.setInt(3, p.getEffdate());
                    psPtevInsert.setInt(4, p.getPtrneff());
                    psPtevInsert.setInt(5, p.getTranno());
                    psPtevInsert.setString(6, p.getValidflag());
                    psPtevInsert.setInt(7, p.getBatcactyr());
                    psPtevInsert.setInt(8, p.getBatcactmn());
                    psPtevInsert.setString(9, p.getBatctrcde());
                    psPtevInsert.setString(10, getUsrprf());
                    psPtevInsert.setString(11, getJobnm());
                    psPtevInsert.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
                    psPtevInsert.addBatch();
                }
                psPtevInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertPtevpfRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psPtevInsert, null);
            }
        }
    }
}