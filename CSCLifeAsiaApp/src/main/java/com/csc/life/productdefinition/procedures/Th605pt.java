/*
 * File: Th605pt.java
 * Date: 30 August 2009 2:36:39
 * Author: Quipoz Limited
 * 
 * Class transformed from TH605PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH605.
*
*
*
***********************************************************************
* </pre>
*/
public class Th605pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Th605rec th605rec = new Th605rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th605pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th605rec.th605Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo006.set(th605rec.liacoy);
		generalCopyLinesInner.fieldNo007.set(th605rec.intRate);
		generalCopyLinesInner.fieldNo005.set(th605rec.indic);
		generalCopyLinesInner.fieldNo008.set(th605rec.uwind);
		generalCopyLinesInner.fieldNo009.set(th605rec.crtind);
		generalCopyLinesInner.fieldNo010.set(th605rec.tsalesind);
		generalCopyLinesInner.fieldNo011.set(th605rec.agccqind);
		generalCopyLinesInner.fieldNo012.set(th605rec.bonusInd);
		generalCopyLinesInner.fieldNo013.set(th605rec.comind);
		generalCopyLinesInner.fieldNo014.set(th605rec.tdayno);
		generalCopyLinesInner.fieldNo015.set(th605rec.ratebas);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Company Defaults                         SH605");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(56);
	private FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine003, 0, FILLER).init("    Override based on Agent OR Details .......");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine003, 48);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 49, FILLER).init("  (Y/N)");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(50);
	private FixedLengthStringData filler9 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine004, 0, FILLER).init("    Life Insurance Association Company Code ..");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 48);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(57);
	private FixedLengthStringData filler10 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine005, 0, FILLER).init("    Reinstatement Interest Rate ..............");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(8, 5).isAPartOf(wsaaPrtLine005, 48).setPattern("ZZZ.ZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(49);
	private FixedLengthStringData filler11 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine006, 0, FILLER).init("    Underwriting required ....................");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 48);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(49);
	private FixedLengthStringData filler12 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine007, 0, FILLER).init("    Enable Agency Movement ...................");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 48);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(49);
	private FixedLengthStringData filler13 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine008, 0, FILLER).init("    Sales Unit Required ......................");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 48);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(55);
	private FixedLengthStringData filler14 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine009, 0, FILLER).init("    Agent Maintenance Consolidated Cheq Default");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 48);
	private FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 49, FILLER).init(" (Y/N)");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(55);
	private FixedLengthStringData filler16 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine010, 0, FILLER).init("    Bonus Workbench Extraction");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 48);
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 49, FILLER).init(" (Y/N)");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(55);
	private FixedLengthStringData filler18 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine011, 0, FILLER).init("    Commission Rls upon Acknowledgement Slip .");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 48);
	private FixedLengthStringData filler19 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 49, FILLER).init(" (Y/N)");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(49);
	private FixedLengthStringData filler20 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine012, 0, FILLER).init("    Cooling off period.");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 24).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 27, FILLER).init(" days. Date Basis .");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 48);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(58);
	private FixedLengthStringData filler22 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine013, 34, FILLER).init("1-Risk commencement date");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(46);
	private FixedLengthStringData filler24 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler25 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 34, FILLER).init("2-Issue date");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(78);
	private FixedLengthStringData filler26 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler27 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine015, 34, FILLER).init("3-Max(Money receive date, U/W approval date)");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(75);
	private FixedLengthStringData filler28 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine016, 34, FILLER).init("4-Acknowledgement or Deemed Received date");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(80);
	private FixedLengthStringData filler30 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler31 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine017, 34, FILLER).init("5-Acknowledgement or Deemed Rcvd or Issue date");
}
}
