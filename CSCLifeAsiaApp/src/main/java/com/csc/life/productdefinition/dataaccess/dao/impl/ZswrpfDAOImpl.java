package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZswrpfDAOImpl extends BaseDAOImpl<Zswrpf> implements ZswrpfDAO {
	

	private static final Logger LOGGER = LoggerFactory.getLogger(ZswrpfDAOImpl.class);	
	private static final String getDateQuery = "select * from vm1dta.Zswrpf where CHDRNUM=? and CHDRCOY=? and CHDRPFX=? and VALIDFLAG=?";
	private static final String getDateQuery2 = "select * from vm1dta.Zswrpf where CHDRNUM>=? and CHDRCOY=? and CHDRPFX=? and VALIDFLAG=? AND CHDRNUM<=? ";
	
	public boolean updateZswrpf(List<Zswrpf> zswrpfiList){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE vm1dta.Zswrpf  ");
		sb.append("SET ");
		sb.append("CHDRPFX=?, ");
		sb.append("CHDRCOY=?, ");	
		sb.append("CNTTYPE=?, ");
		sb.append("VALIDFLAG=?, ");
		sb.append("CRTABLE=?, ");
		sb.append("ZAFROPT=?, ");
		sb.append("ZCHKOPT=?, ");
		sb.append("ZREGDTE=?, ");
		sb.append("AGE=?, ");
		sb.append("TRANNO=?, ");
		sb.append("ZAFRFREQ=?, ");
		sb.append("VRTFUND=?, ");
		sb.append("UALFND01=?, ");
		sb.append("UALFND02=?, ");
		sb.append("UALFND03=?, ");
		sb.append("UALFND04=?, ");
		sb.append("UALFND05=? , ");
		sb.append("UALFND06=? , ");
		sb.append("UALFND07=? , ");
		sb.append("UALFND08=? , ");
		sb.append("UALFND09=? , ");
		sb.append("UALFND10=? , ");
		sb.append("UALPRC01=? , ");
		sb.append("UALPRC02=? , ");
		sb.append("UALPRC03=? , ");
		sb.append("UALPRC04=? , ");
		sb.append("UALPRC05=? , ");
		sb.append("UALPRC06=? , ");
		sb.append("UALPRC07=? , ");
		sb.append("UALPRC08=? , ");
		sb.append("UALPRC09=? , ");
		sb.append("UALPRC10=? , ");
		sb.append("ZWDWLRSN=? , ");
		sb.append("ZLASTDTE=? , ");
		sb.append("ZNEXTDTE=? , ");
		sb.append("ZWDLDDTE=?  ,");
		sb.append("ZENABLE=? , ");
		sb.append("ZLSTUPDTE=? , ");
		sb.append("USRPRF=? , ");
		sb.append("JOBNM=? , ");
		//sb.append("DATIME=?  ");
		sb.append("ZAFRITEM=?,  ");
		sb.append("ZLSTUPDUSR=?  ");
		sb.append("WHERE CHDRNUM=?");
		
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());	
			 for (Zswrpf p : zswrpfiList) {
				 
				 
				ps.setString(1, p.getChdrpfx() );
             	ps.setString(2, p.getChdrcoy());             	
             	ps.setString(3, p.getCnntype());
             	ps.setString(4, p.getValidflag());
             	ps.setString(5, p.getCrtable());
             	ps.setString(6, p.getZafropt());
             	ps.setString(7, p.getZchkopt());
             	ps.setInt(8, p.getZregdte());
             	ps.setInt(9, p.getAge());
             	ps.setInt(10, p.getTranno());
             	ps.setString(11, p.getZafrfreq());
             	ps.setString(12, p.getVrtfund());
             	ps.setString(13, p.getUalfnd01());
             	ps.setString(14, p.getUalfnd02());
             	ps.setString(15, p.getUalfnd03());
             	ps.setString(16, p.getUalfnd04());
             	ps.setString(17, p.getUalfnd05());
             	ps.setString(18, p.getUalfnd06());
             	ps.setString(19, p.getUalfnd07());                	
             	ps.setString(20, p.getUalfnd08());
             	ps.setString(21, p.getUalfnd09());
             	ps.setString(22, p.getUalfnd10());
             	ps.setLong(23, p.getUalprc01());
             	ps.setLong(24, p.getUalprc02());
             	ps.setLong(25, p.getUalprc03());
             	ps.setLong(26, p.getUalprc04());
             	ps.setLong(27, p.getUalprc05());
             	ps.setLong(28, p.getUalprc06());
             	ps.setLong(29, p.getUalprc07());
             	ps.setLong(30, p.getUalprc08());	                	
             	ps.setLong(31, p.getUalprc09());
             	ps.setLong(32, p.getUalprc10());	                	
             	ps.setString(33,p.getZwdwlrsn());
             	ps.setInt(34,p.getZlastdte() );
             	ps.setInt(35,p.getZnextdte());
             	ps.setInt(36,p.getZwdlddte() );
             	ps.setString(37,p.getZenable());	                	
             	ps.setInt(38,p.getZlstupdate());
             	ps.setString(39, getUsrprf());
                 ps.setString(40, getJobnm());
//               ps.setTimestamp(42, new Timestamp(System.currentTimeMillis()));
                 ps.setString(41,p.getZafritem());
                 ps.setString(42,p.getZlstupdusr());
                 ps.setString(43, p.getChdrnum());
                 
			    ps.addBatch();	
			    			    
			}		
			ps.executeBatch();
			isUpdated = true; 
		}//IJTI-851-Overly Broad Catch
		catch ( SQLException e) {
			LOGGER.error("updateMedipf()",e);//IJTI-1485
			//throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}		
		return isUpdated;
	}

	 
	@Override
	public List<Zswrpf> getZswrpfData(String chdrnum,String companyno,String prefix,String validflag) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		List<Zswrpf> list = new ArrayList<Zswrpf>();
		Chdrpf chdpf;

		try {
			PreparedStatement stmt = getPrepareStatement(getDateQuery);
			stmt.setString(1, chdrnum);
			stmt.setString(2, companyno);	
			stmt.setString(3, prefix);	
			stmt.setString(4, validflag);	
			
			rs = stmt.executeQuery(); 

			while (rs.next()) 
			{	
				Zswrpf zw=new Zswrpf();				
				zw.setZafrfreq(rs.getString("ZAFRFREQ") );
				zw.setZafritem(rs.getString("ZAFRITEM"));
				zw.setZafropt(rs.getString("ZAFROPT"));
				zw.setChdrnum(rs.getString("CHDRNUM")); 
				zw.setChdrcoy(rs.getString("CHDRCOY"));
				zw.setZlstupdusr(rs.getString("Zlstupdusr") );//MPTD-1088
				zw.setZlstupdate(rs.getInt("Zlstupdte") ); 
				zw.setZlastdte(rs.getInt("Zlastdte"));  
				zw.setZnextdte(rs.getInt("Znextdte"));  ;  
				list.add(zw);
			}
			
		}//IJTI-851-Overly Broad Catch
		catch (SQLException e) 
		{
			LOGGER.error("error has occured in ChdrpfDAOImpl.findDate", e);
		}
		return list;
	}

	@Override
	public void insertZswrpfListRecord(List<Zswrpf> zswrpfiList) {
		 
		if (zswrpfiList != null && zswrpfiList.size() > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("insert into Zswrpf(CHDRPFX,CHDRCOY,CHDRNUM,CNTTYPE,VALIDFLAG, CRTABLE,ZAFROPT,ZCHKOPT,ZREGDTE,AGE,TRANNO,ZAFRFREQ, VRTFUND,UALFND01 ,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,ZWDWLRSN,ZLASTDTE,ZNEXTDTE,ZWDLDDTE,ZENABLE,ZLSTUPDTE,USRPRF,JOBNM,ZAFRITEM,ZLSTUPDUSR )")
			.append("values(?,?,?,?,?, ?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )");
			 PreparedStatement ps = getPrepareStatement(sb.toString());
			 try {
	                for (Zswrpf p : zswrpfiList) {
	                	ps.setString(1, p.getChdrpfx() );
	                	ps.setString(2, p.getChdrcoy());
	                	ps.setString(3, p.getChdrnum());
	                	ps.setString(4, p.getCnntype());
	                	ps.setString(5, p.getValidflag());
	                	ps.setString(6, p.getCrtable());
	                	ps.setString(7, p.getZafropt());
	                	ps.setString(8, p.getZchkopt());
	                	ps.setInt(9, p.getZregdte());
	                	ps.setInt(10, p.getAge());
	                	ps.setInt(11, p.getTranno());
	                	ps.setString(12, p.getZafrfreq());
	                	ps.setString(13, p.getVrtfund());
	                	ps.setString(14, p.getUalfnd01());
	                	ps.setString(15, p.getUalfnd02());
	                	ps.setString(16, p.getUalfnd03());
	                	ps.setString(17, p.getUalfnd04());
	                	ps.setString(18, p.getUalfnd05());
	                	ps.setString(19, p.getUalfnd06());
	                	ps.setString(20, p.getUalfnd07());                	
	                	ps.setString(21, p.getUalfnd08());
	                	ps.setString(22, p.getUalfnd09());
	                	ps.setString(23, p.getUalfnd10());
	                	ps.setLong(24, p.getUalprc01());
	                	ps.setLong(25, p.getUalprc02());
	                	ps.setLong(26, p.getUalprc03());
	                	ps.setLong(27, p.getUalprc04());
	                	ps.setLong(28, p.getUalprc05());
	                	ps.setLong(29, p.getUalprc06());
	                	ps.setLong(30, p.getUalprc07());
	                	ps.setLong(31, p.getUalprc08());	                	
	                	ps.setLong(32, p.getUalprc09());
	                	ps.setLong(33, p.getUalprc10());	                	
	                	ps.setString(34,p.getZwdwlrsn());
	                	ps.setInt(35,p.getZlastdte() );
	                	ps.setInt(36,p.getZnextdte());
	                	ps.setInt(37,p.getZwdlddte() );
	                	ps.setString(38,p.getZenable());	                	
	                	ps.setInt(39,p.getZlstupdate());
	                	ps.setString(40, getUsrprf());
	                    ps.setString(41, getJobnm());
//	                    ps.setTimestamp(42, new Timestamp(System.currentTimeMillis()));
	                    ps.setString(42,p.getZafritem());
	                    ps.setString(43,p.getZlstupdusr());
	                    ps.executeUpdate();			
	                    
	                }
	                
			}catch (SQLException e) {
	            LOGGER.error("insertAcmxBulk()", e);//IJTI-1485
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, null);
	        
			}

		
	}
		
	}

	@Override
	public boolean updateZswrpfvalidflag(Zswrpf obj){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE vm1dta.Zswrpf  ");
		sb.append("SET ");		 
		sb.append("VALIDFLAG=?, ");
		sb.append("ZREGDTE=?, ");
		sb.append("TRANNO=?, ");
		sb.append("ZNEXTDTE=?, ");
		sb.append("ZENABLE=?  ");
		sb.append("WHERE CHDRNUM=? ");
		sb.append("  AND CHDRCOY=? ");
		
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		boolean isUpdated = false;
		try{
				ps = getPrepareStatement(sb.toString());
				ps.setString(1, obj.getValidflag() );             	         	
             	ps.setLong(2, obj.getZregdte());
             	ps.setLong(3, obj.getTranno());    
             	ps.setLong(4, obj.getZnextdte());
             	ps.setString(5, obj.getZenable() );
             	ps.setString(6, obj.getChdrnum());
             	ps.setString(7, obj.getChdrcoy());
			    ps.addBatch();	
			 		
			    ps.executeBatch();
			    isUpdated = true; 
		}//IJTI-851-Overly Broad Catch
		catch ( SQLException e) {
			LOGGER.error("updateMedipf()",e);//IJTI-1485
			//throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}		
		return isUpdated;
	}
	
	public boolean deleteZswrpf(String chdrnum,String companyno,String prefix){		
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM vm1dta.Zswrpf  ");		
		sb.append(" WHERE CHDRNUM=? ");
		sb.append("  AND CHDRCOY=? ");
		sb.append("  AND CHDRPFX=? ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		boolean isUpdated = false;
		try{
				ps = getPrepareStatement(sb.toString());
				ps.setString(1, chdrnum );             	         	
             	ps.setString(2, companyno);
             	ps.setString(3, prefix);
			    ps.addBatch();	
			    ps.executeBatch();
			    isUpdated = true; 
		}//IJTI-851-Overly Broad Catch
		catch ( SQLException e) {
			LOGGER.error("delete()",e);//IJTI-1485
			//throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}		
		return isUpdated;
	}
	
	public List<Zswrpf> getAllZswrpfData(String chdrnum,String companyno,String prefix,String validflag,String chdrnum1) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		List<Zswrpf> list = new ArrayList<Zswrpf>();
		Chdrpf chdpf;

		try {
			PreparedStatement stmt = getPrepareStatement(getDateQuery2);
			stmt.setString(1, chdrnum);
			stmt.setString(2, companyno);	
			stmt.setString(3, prefix);	
			stmt.setString(4, validflag);	
			stmt.setString(5, chdrnum1);
			
			rs = stmt.executeQuery(); 

			while (rs.next()) 
			{	
				Zswrpf zw=new Zswrpf();				
				zw.setZafrfreq(rs.getString("ZAFRFREQ") );
				zw.setZafritem(rs.getString("ZAFRITEM"));
				zw.setZafropt(rs.getString("ZAFROPT"));
				zw.setChdrnum(rs.getString("CHDRNUM")); 
				zw.setChdrcoy(rs.getString("CHDRCOY"));
				zw.setZlstupdusr(rs.getString("Zlstupdusr") );//MPTD-1088
				zw.setZlstupdate(rs.getInt("Zlstupdte") ); 
				zw.setZlastdte(rs.getInt("Zlastdte"));  
				zw.setZnextdte(rs.getInt("Znextdte"));  
				zw.setCrtable(rs.getString("crtable"));
				zw.setUalfnd01(rs.getString("UALFND01"));
				zw.setUalfnd02(rs.getString("UALFND02"));
				zw.setUalfnd03(rs.getString("UALFND03"));
				zw.setUalfnd04(rs.getString("UALFND04"));
				zw.setUalfnd05(rs.getString("UALFND05"));
				zw.setUalfnd06(rs.getString("UALFND06"));
				zw.setUalfnd07(rs.getString("UALFND07"));
				zw.setUalfnd08(rs.getString("UALFND08"));
				zw.setUalfnd09(rs.getString("UALFND09"));
				zw.setUalfnd10(rs.getString("UALFND10"));
				zw.setUalprc01(rs.getInt("UALPRC01"));
				zw.setUalprc02(rs.getInt("UALPRC02"));
				zw.setUalprc03(rs.getInt("UALPRC03"));
				zw.setUalprc04(rs.getInt("UALPRC04"));
				zw.setUalprc05(rs.getInt("UALPRC05"));
				zw.setUalprc06(rs.getInt("UALPRC06"));
				zw.setUalprc07(rs.getInt("UALPRC07"));
				zw.setUalprc08(rs.getInt("UALPRC08"));
				zw.setUalprc09(rs.getInt("UALPRC09"));
				zw.setUalprc10(rs.getInt("UALPRC10"));
				zw.setZchkopt(rs.getString("ZCHKOPT"));
				zw.setZregdte(rs.getInt("ZREGDTE"));
				zw.setCnntype(rs.getString("CNTTYPE"));
				zw.setAge(rs.getInt("AGE"));
				zw.setVrtfund(rs.getString("VRTFUND"));
				zw.setZwdwlrsn(rs.getString("ZWDWLRSN"));
				list.add(zw);
			}
			
		}catch (Exception e) 
		{
			LOGGER.error("error has occured in ChdrpfDAOImpl.findDate", e);
		}
		return list;
	}
	
	public List<Zswrpf> loadDataByBatch(int batchID) {
		List<Zswrpf> list = new ArrayList<Zswrpf>();
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM ZSWRPFTEMP ");
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		 
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				Zswrpf zw=new Zswrpf();		
				zw.setChdrpfx(rs.getString("CHDRPFX") );
				zw.setZafrfreq(rs.getString("ZAFRFREQ") );
				zw.setZafritem(rs.getString("ZAFRITEM"));
				zw.setZafropt(rs.getString("ZAFROPT"));
				zw.setChdrnum(rs.getString("CHDRNUM")); 
				zw.setChdrcoy(rs.getString("CHDRCOY"));
				zw.setZlstupdusr(rs.getString("Zlstupdusr") );//MPTD-1088
				zw.setZlstupdate(rs.getInt("Zlstupdte") ); 
				zw.setZlastdte(rs.getInt("Zlastdte"));  
				zw.setZnextdte(rs.getInt("Znextdte"));  
				zw.setCrtable(rs.getString("crtable"));
				zw.setUalfnd01(rs.getString("UALFND01"));
				zw.setUalfnd02(rs.getString("UALFND02"));
				zw.setUalfnd03(rs.getString("UALFND03"));
				zw.setUalfnd04(rs.getString("UALFND04"));
				zw.setUalfnd05(rs.getString("UALFND05"));
				zw.setUalfnd06(rs.getString("UALFND06"));
				zw.setUalfnd07(rs.getString("UALFND07"));
				zw.setUalfnd08(rs.getString("UALFND08"));
				zw.setUalfnd09(rs.getString("UALFND09"));
				zw.setUalfnd10(rs.getString("UALFND10"));
				zw.setUalprc01(rs.getInt("UALPRC01"));
				zw.setUalprc02(rs.getInt("UALPRC02"));
				zw.setUalprc03(rs.getInt("UALPRC03"));
				zw.setUalprc04(rs.getInt("UALPRC04"));
				zw.setUalprc05(rs.getInt("UALPRC05"));
				zw.setUalprc06(rs.getInt("UALPRC06"));
				zw.setUalprc07(rs.getInt("UALPRC07"));
				zw.setUalprc08(rs.getInt("UALPRC08"));
				zw.setUalprc09(rs.getInt("UALPRC09"));
				zw.setUalprc10(rs.getInt("UALPRC10"));
				zw.setZchkopt(rs.getString("ZCHKOPT"));
				zw.setZregdte(rs.getInt("ZREGDTE"));
				zw.setCnntype(rs.getString("CNTTYPE"));
				zw.setAge(rs.getInt("AGE"));
				zw.setVrtfund(rs.getString("VRTFUND"));
				zw.setZwdwlrsn(rs.getString("ZWDWLRSN"));
				list.add(zw);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()",e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return list;
	}	
	
	public int populateZrstpfTemp(String company, int batchExtractSize, String chdrfrom, String chdrto) {
		initializeZrstPfTemp();
		int rows = 0;
		boolean isParameterized = false;
		boolean isParamFromOnly = false;
		boolean isParamToOnly = false;
		if (!("".equals(chdrfrom.trim())) && !("".equals(chdrto.trim()))) {
			if (Integer.parseInt(chdrto) >= Integer.parseInt(chdrfrom)) isParameterized = true;
		}
		
		if (!("".equals(chdrfrom.trim())) && "".equals(chdrto.trim())) isParamFromOnly = true;
		if (!("".equals(chdrto.trim())) && "".equals(chdrfrom.trim())) isParamToOnly = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.ZSWRPFTEMP ");
		sb.append("(BATCHID,CHDRPFX,CHDRCOY,CHDRNUM,CNTTYPE,VALIDFLAG,CRTABLE,ZAFROPT,ZCHKOPT,ZREGDTE,AGE,TRANNO,ZAFRFREQ ,VRTFUND,UALFND01,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,ZWDWLRSN,ZLASTDTE,ZNEXTDTE,ZWDLDDTE,ZENABLE,ZLSTUPDTE,USRPRF,JOBNM,ZAFRITEM,ZLSTUPDUSR)");
		sb.append("SELECT ");
		sb.append("floor((row_number() over (ORDER BY ZSWRPF.CHDRNUM ASC)-1)/?) BATCHID,CHDRPFX,CHDRCOY,CHDRNUM,CNTTYPE,VALIDFLAG,CRTABLE,ZAFROPT,ZCHKOPT,ZREGDTE,AGE,TRANNO,ZAFRFREQ ,VRTFUND,UALFND01,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,ZWDWLRSN,ZLASTDTE,ZNEXTDTE,ZWDLDDTE,ZENABLE,ZLSTUPDTE,USRPRF,JOBNM,ZAFRITEM,ZLSTUPDUSR ");
		sb.append("FROM ");
		sb.append("VM1DTA.ZSWRPF ");
		sb.append("WHERE  CHDRCOY=? ");
		if (isParameterized)
			sb.append(" AND  CHDRNUM BETWEEN ? and ? ");	
		if (isParamFromOnly || isParamToOnly)
			sb.append("AND  CHDRNUM = ? ");	
	 

		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;			
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, company);
			if (isParameterized){
				ps.setString(3, chdrfrom);
				ps.setString(4, chdrto);
			}
			if (isParamFromOnly) ps.setString(3, chdrfrom);
			if (isParamToOnly) ps.setString(3, chdrto);
			
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBr644Temp()",e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeZrstPfTemp() {
		
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM ZSWRPFTEMP ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("ZSWRPFTEMP()",e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}	
	
}

