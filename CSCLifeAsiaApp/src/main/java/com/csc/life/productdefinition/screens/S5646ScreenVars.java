package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5646
 * @version 1.0 generated on 30/08/09 06:46
 * @author Quipoz
 */
public class S5646ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(907);
	public FixedLengthStringData dataFields = new FixedLengthStringData(219).isAPartOf(dataArea, 0);
	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(36).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(12, 3, 0, ageIssageFrms, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
	public ZonedDecimalData ageIssageFrm01 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData ageIssageFrm02 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData ageIssageFrm03 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData ageIssageFrm04 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData ageIssageFrm05 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData ageIssageFrm06 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData ageIssageFrm07 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData ageIssageFrm08 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData ageIssageFrm09 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData ageIssageFrm10 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,27);
	public ZonedDecimalData ageIssageFrm11 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData ageIssageFrm12 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,33);
	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(36).isAPartOf(dataFields, 36);
	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(12, 3, 0, ageIssageTos, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
	public ZonedDecimalData ageIssageTo01 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData ageIssageTo02 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,3);
	public ZonedDecimalData ageIssageTo03 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData ageIssageTo04 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,9);
	public ZonedDecimalData ageIssageTo05 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData ageIssageTo06 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData ageIssageTo07 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData ageIssageTo08 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,21);
	public ZonedDecimalData ageIssageTo09 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData ageIssageTo10 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,27);
	public ZonedDecimalData ageIssageTo11 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData ageIssageTo12 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,33);
	public ZonedDecimalData agelimit = DD.agelimit.copyToZonedDecimal().isAPartOf(dataFields,72);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData factorsas = new FixedLengthStringData(84).isAPartOf(dataFields, 76);
	public ZonedDecimalData[] factorsa = ZDArrayPartOfStructure(12, 7, 4, factorsas, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(84).isAPartOf(factorsas, 0, FILLER_REDEFINE);
	public ZonedDecimalData factorsa01 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData factorsa02 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,7);
	public ZonedDecimalData factorsa03 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,14);
	public ZonedDecimalData factorsa04 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,21);
	public ZonedDecimalData factorsa05 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,28);
	public ZonedDecimalData factorsa06 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,35);
	public ZonedDecimalData factorsa07 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,42);
	public ZonedDecimalData factorsa08 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,49);
	public ZonedDecimalData factorsa09 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,56);
	public ZonedDecimalData factorsa10 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,63);
	public ZonedDecimalData factorsa11 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,70);
	public ZonedDecimalData factorsa12 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler2,77);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,160);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,168);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,176);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,184);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(172).isAPartOf(dataArea, 219);
	public FixedLengthStringData aagefrmsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] aagefrmErr = FLSArrayPartOfStructure(12, 4, aagefrmsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(aagefrmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData aagefrm01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData aagefrm02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData aagefrm03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData aagefrm04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData aagefrm05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData aagefrm06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData aagefrm07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData aagefrm08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData aagefrm09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData aagefrm10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData aagefrm11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData aagefrm12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData aagetosErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData[] aagetoErr = FLSArrayPartOfStructure(12, 4, aagetosErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(48).isAPartOf(aagetosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData aageto01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData aageto02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData aageto03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData aageto04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData aageto05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData aageto06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData aageto07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData aageto08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData aageto09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData aageto10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData aageto11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData aageto12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData agelimitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData factorsasErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData[] factorsaErr = FLSArrayPartOfStructure(12, 4, factorsasErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(48).isAPartOf(factorsasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData factorsa01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData factorsa02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData factorsa03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData factorsa04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData factorsa05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData factorsa06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData factorsa07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData factorsa08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData factorsa09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData factorsa10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData factorsa11Err = new FixedLengthStringData(4).isAPartOf(filler5, 40);
	public FixedLengthStringData factorsa12Err = new FixedLengthStringData(4).isAPartOf(filler5, 44);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(516).isAPartOf(dataArea, 391);
	public FixedLengthStringData aagefrmsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] aagefrmOut = FLSArrayPartOfStructure(12, 12, aagefrmsOut, 0);
	public FixedLengthStringData[][] aagefrmO = FLSDArrayPartOfArrayStructure(12, 1, aagefrmOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(144).isAPartOf(aagefrmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] aagefrm01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] aagefrm02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] aagefrm03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] aagefrm04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] aagefrm05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] aagefrm06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] aagefrm07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] aagefrm08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] aagefrm09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] aagefrm10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] aagefrm11Out = FLSArrayPartOfStructure(12, 1, filler6, 120);
	public FixedLengthStringData[] aagefrm12Out = FLSArrayPartOfStructure(12, 1, filler6, 132);
	public FixedLengthStringData aagetosOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 144);
	public FixedLengthStringData[] aagetoOut = FLSArrayPartOfStructure(12, 12, aagetosOut, 0);
	public FixedLengthStringData[][] aagetoO = FLSDArrayPartOfArrayStructure(12, 1, aagetoOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(144).isAPartOf(aagetosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] aageto01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] aageto02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] aageto03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] aageto04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] aageto05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] aageto06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] aageto07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] aageto08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] aageto09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] aageto10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] aageto11Out = FLSArrayPartOfStructure(12, 1, filler7, 120);
	public FixedLengthStringData[] aageto12Out = FLSArrayPartOfStructure(12, 1, filler7, 132);
	public FixedLengthStringData[] agelimitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData factorsasOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 312);
	public FixedLengthStringData[] factorsaOut = FLSArrayPartOfStructure(12, 12, factorsasOut, 0);
	public FixedLengthStringData[][] factorsaO = FLSDArrayPartOfArrayStructure(12, 1, factorsaOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(144).isAPartOf(factorsasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] factorsa01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] factorsa02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] factorsa03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] factorsa04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] factorsa05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] factorsa06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] factorsa07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] factorsa08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] factorsa09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] factorsa10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] factorsa11Out = FLSArrayPartOfStructure(12, 1, filler8, 120);
	public FixedLengthStringData[] factorsa12Out = FLSArrayPartOfStructure(12, 1, filler8, 132);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5646screenWritten = new LongData(0);
	public LongData S5646protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5646ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, agelimit, ageIssageFrm01, ageIssageTo01, factorsa01, ageIssageFrm02, ageIssageTo02, factorsa02, ageIssageFrm03, ageIssageTo03, factorsa03, ageIssageFrm04, ageIssageTo04, factorsa04, ageIssageFrm05, ageIssageTo05, factorsa05, ageIssageFrm06, ageIssageTo06, factorsa06, ageIssageFrm07, ageIssageTo07, factorsa07, ageIssageFrm08, ageIssageTo08, factorsa08, ageIssageFrm09, ageIssageTo09, factorsa09, ageIssageFrm10, ageIssageTo10, factorsa10, ageIssageFrm11, ageIssageTo11, factorsa11, ageIssageFrm12, ageIssageTo12, factorsa12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, agelimitOut, aagefrm01Out, aageto01Out, factorsa01Out, aagefrm02Out, aageto02Out, factorsa02Out, aagefrm03Out, aageto03Out, factorsa03Out, aagefrm04Out, aageto04Out, factorsa04Out, aagefrm05Out, aageto05Out, factorsa05Out, aagefrm06Out, aageto06Out, factorsa06Out, aagefrm07Out, aageto07Out, factorsa07Out, aagefrm08Out, aageto08Out, factorsa08Out, aagefrm09Out, aageto09Out, factorsa09Out, aagefrm10Out, aageto10Out, factorsa10Out, aagefrm11Out, aageto11Out, factorsa11Out, aagefrm12Out, aageto12Out, factorsa12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, agelimitErr, aagefrm01Err, aageto01Err, factorsa01Err, aagefrm02Err, aageto02Err, factorsa02Err, aagefrm03Err, aageto03Err, factorsa03Err, aagefrm04Err, aageto04Err, factorsa04Err, aagefrm05Err, aageto05Err, factorsa05Err, aagefrm06Err, aageto06Err, factorsa06Err, aagefrm07Err, aageto07Err, factorsa07Err, aagefrm08Err, aageto08Err, factorsa08Err, aagefrm09Err, aageto09Err, factorsa09Err, aagefrm10Err, aageto10Err, factorsa10Err, aagefrm11Err, aageto11Err, factorsa11Err, aagefrm12Err, aageto12Err, factorsa12Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5646screen.class;
		protectRecord = S5646protect.class;
	}

}
