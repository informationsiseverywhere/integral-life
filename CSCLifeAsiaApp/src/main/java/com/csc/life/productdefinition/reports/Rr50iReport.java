package com.csc.life.productdefinition.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR50I.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr50iReport extends SMARTReportLayout { 

	private FixedLengthStringData acdes = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData cntcount = new ZonedDecimalData(6, 0);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData desc = new FixedLengthStringData(50);
	private FixedLengthStringData eror = new FixedLengthStringData(4);
	private FixedLengthStringData hprrcvdt = new FixedLengthStringData(10);
	private ZonedDecimalData instprem = new ZonedDecimalData(17, 2);
	private FixedLengthStringData occdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData premsusp = new ZonedDecimalData(17, 2);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData statcode = new FixedLengthStringData(2);
	private ZonedDecimalData tdayno = new ZonedDecimalData(3, 0);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totalfee = new ZonedDecimalData(17, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr50iReport() {
		super();
	}


	/**
	 * Print the XML for Rr50id01
	 */
	public void printRr50id01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		hprrcvdt.setFieldName("hprrcvdt");
		hprrcvdt.setInternal(subString(recordData, 1, 10));
		tdayno.setFieldName("tdayno");
		tdayno.setInternal(subString(recordData, 11, 3));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 14, 8));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 22, 3));
		acdes.setFieldName("acdes");
		acdes.setInternal(subString(recordData, 25, 30));
		occdate.setFieldName("occdate");
		occdate.setInternal(subString(recordData, 55, 10));
		statcode.setFieldName("statcode");
		statcode.setInternal(subString(recordData, 65, 2));
		instprem.setFieldName("instprem");
		instprem.setInternal(subString(recordData, 67, 17));
		premsusp.setFieldName("premsusp");
		premsusp.setInternal(subString(recordData, 84, 17));
		totalfee.setFieldName("totalfee");
		totalfee.setInternal(subString(recordData, 101, 17));
		eror.setFieldName("eror");
		eror.setInternal(subString(recordData, 118, 4));
		desc.setFieldName("desc");
		desc.setInternal(subString(recordData, 122, 50));
		printLayout("Rr50id01",			// Record name
			new BaseData[]{			// Fields:
				hprrcvdt,
				tdayno,
				chdrnum,
				cnttype,
				acdes,
				occdate,
				statcode,
				instprem,
				premsusp,
				totalfee,
				eror,
				desc
			}
		);

	}

	/**
	 * Print the XML for Rr50ie01
	 */
	public void printRr50ie01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("Rr50ie01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rr50ih01
	 */
	public void printRr50ih01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.set(3);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 42, 10));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr50ih01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				company,
				companynm,
				repdate,
				time,
				pagnbr
			}
		);

		currentPrintLine.add(7);
	}

	/**
	 * Print the XML for Rr50it01
	 */
	public void printRr50it01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(3);

		cntcount.setFieldName("cntcount");
		cntcount.setInternal(subString(recordData, 1, 6));
		printLayout("Rr50it01",			// Record name
			new BaseData[]{			// Fields:
				cntcount
			}
		);

	}


}
