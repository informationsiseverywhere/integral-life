package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:22
 * Description:
 * Copybook name: STDTALLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Stdtallrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData stdt001Rec = new FixedLengthStringData(97);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(stdt001Rec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(stdt001Rec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(stdt001Rec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(stdt001Rec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(stdt001Rec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(stdt001Rec, 17);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(stdt001Rec, 19);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(stdt001Rec, 22);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(stdt001Rec, 25).setUnsigned();
  	public ZonedDecimalData stampDuty = new ZonedDecimalData(17, 2).isAPartOf(stdt001Rec, 33);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(stdt001Rec, 50, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(stdt001Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		stdt001Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}