package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:46
 * Description:
 * Copybook name: T5671REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5671rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5671Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData edtitms = new FixedLengthStringData(20).isAPartOf(t5671Rec, 0);
  	public FixedLengthStringData[] edtitm = FLSArrayPartOfStructure(4, 5, edtitms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(edtitms, 0, FILLER_REDEFINE);
  	public FixedLengthStringData edtitm01 = new FixedLengthStringData(5).isAPartOf(filler, 0);
  	public FixedLengthStringData edtitm02 = new FixedLengthStringData(5).isAPartOf(filler, 5);
  	public FixedLengthStringData edtitm03 = new FixedLengthStringData(5).isAPartOf(filler, 10);
  	public FixedLengthStringData edtitm04 = new FixedLengthStringData(5).isAPartOf(filler, 15);
  	public FixedLengthStringData pgms = new FixedLengthStringData(20).isAPartOf(t5671Rec, 20);
  	public FixedLengthStringData[] pgm = FLSArrayPartOfStructure(4, 5, pgms, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(pgms, 0, FILLER_REDEFINE);
  	public FixedLengthStringData pgm01 = new FixedLengthStringData(5).isAPartOf(filler1, 0);
  	public FixedLengthStringData pgm02 = new FixedLengthStringData(5).isAPartOf(filler1, 5);
  	public FixedLengthStringData pgm03 = new FixedLengthStringData(5).isAPartOf(filler1, 10);
  	public FixedLengthStringData pgm04 = new FixedLengthStringData(5).isAPartOf(filler1, 15);
  	public FixedLengthStringData subprogs = new FixedLengthStringData(40).isAPartOf(t5671Rec, 40);
  	public FixedLengthStringData[] subprog = FLSArrayPartOfStructure(4, 10, subprogs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(subprogs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData subprog01 = new FixedLengthStringData(10).isAPartOf(filler2, 0);
  	public FixedLengthStringData subprog02 = new FixedLengthStringData(10).isAPartOf(filler2, 10);
  	public FixedLengthStringData subprog03 = new FixedLengthStringData(10).isAPartOf(filler2, 20);
  	public FixedLengthStringData subprog04 = new FixedLengthStringData(10).isAPartOf(filler2, 30);
  	public FixedLengthStringData trevsubs = new FixedLengthStringData(40).isAPartOf(t5671Rec, 80);
  	public FixedLengthStringData[] trevsub = FLSArrayPartOfStructure(4, 10, trevsubs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(trevsubs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData trevsub01 = new FixedLengthStringData(10).isAPartOf(filler3, 0);
  	public FixedLengthStringData trevsub02 = new FixedLengthStringData(10).isAPartOf(filler3, 10);
  	public FixedLengthStringData trevsub03 = new FixedLengthStringData(10).isAPartOf(filler3, 20);
  	public FixedLengthStringData trevsub04 = new FixedLengthStringData(10).isAPartOf(filler3, 30);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(380).isAPartOf(t5671Rec, 120, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5671Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5671Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}