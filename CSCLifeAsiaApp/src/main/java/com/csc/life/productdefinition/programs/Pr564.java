/*
 * File: Pr564.java
 * Date: 30 August 2009 1:41:33
 * Author: Quipoz Limited
 * 
 * Class transformed from PR564.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;

import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsJniSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.runtime.base.VpmsTcpSessionFactory;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovttrmTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.screens.Sr564ScreenVars;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This is a skeleton for a batch mainline program.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*    Read
*     - read first primary file record.
*
*    Perform    Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock the record if it is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
*****************************************************************
* </pre>
*/
public class Pr564 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pr564.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR564");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final int wsaaMaxSubfile = 99;
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRiskTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCoverc = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaMlresind = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private static final String rl18 = "RL18";
		/* TABLES */
	private static final String t5687 = "T5687";
	private static final String th615 = "TH615";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private CovttrmTableDAM covttrmIO = new CovttrmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Th615rec th615rec = new Th615rec();
	private Sr564ScreenVars sv = ScreenProgram.getScreenVars( Sr564ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	//BRD-139-STARTS
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private boolean mrt1Permission = false;
	//BRD-139-ENDS
	private boolean benSchdPermission = false;//ILIFE-7521

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		srdn2100, 
		exit2100, 
		nextSubfileRecord3100, 
		exit3100
	}

	public Pr564() {
		super();
		screenVars = sv;
		new ScreenModel("Sr564", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaRiskTerm.set(ZERO);
		wsaaMlresind.set(ZERO);
		wsaaIndex.set(ZERO);
		wsaaCoverc.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR564", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		//ILIFE-7521
		benSchdPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP093", appVars, "IT");
		//ILIFE-7521-starts
		if(benSchdPermission && isEQ(chdrlnbIO.cnttype,"LCP")){
			sv.policyType.set("Y");
		}
		else
			sv.policyType.set("N");
		//ILIFE-7521-ends
		//BRD-139-STARTS
				mrt1Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars,smtpfxcpy.item.toString()) && isEQ(chdrlnbIO.getCnttype(),"MRT");/*ILIFE-4548*/
				if(!mrt1Permission){
					sv.mrt1Flag.set("N");
				}
				else {
					sv.mrt1Flag.set("Y");
				}
				//BRD-139-ENDS
				
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		sv.life.set(covtlnbIO.getLife());
		sv.coverage.set(covtlnbIO.getCoverage());
		sv.rider.set(covtlnbIO.getRider());
		sv.crcode.set(covtlnbIO.getCrtable());
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setSeqnbr(ZERO);
		covttrmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covttrmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covttrmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(),varcom.oK)
		&& isNE(covttrmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(),covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(),covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(covtlnbIO.getChdrcoy());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covtlnbIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.coverDesc.fill("?");
		}
		else {
			sv.coverDesc.set(descIO.getLongdesc());
		}
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		/* Obtain Life Assured Name.*/
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMF");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.lifename.set(namadrsrec.name);
		readMrta1500();
		if (isNE(wsaaMlresind,SPACES)) {
			readTh615Table2300();
		}
		if (isEQ(covttrmIO.getRiskCessTerm(),ZERO)) {
			compute(wsaaRiskTerm, 0).set((add(sub(covttrmIO.getRiskCessAge(),covttrmIO.getAnbAtCcd01()),wsaaCoverc)));
		}
		else {
			compute(wsaaRiskTerm, 0).set(add(covttrmIO.getRiskCessTerm(),wsaaCoverc));
		}
		mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
		mbnsIO.setLife(covtlnbIO.getLife());
		mbnsIO.setCoverage(covtlnbIO.getCoverage());
		mbnsIO.setRider(covtlnbIO.getRider());
		mbnsIO.setYrsinf(ZERO);
		mbnsIO.setFormat(formatsInner.mbnsrec);
		mbnsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		mbnsIO.setStatuz(varcom.oK);
		while ( !(isEQ(mbnsIO.getStatuz(),varcom.endp))) {
			loadSubfile1200();
		}
		if(!benSchdPermission){
			//ILIFE-3686-starts
			if(!mrt1Permission){
				while ( !(isEQ(scrnparams.subfileRrn,wsaaMaxSubfile))) {
				loadBlankSubfile1300();
				}
			}
			else{
				while ( !(isGTE(scrnparams.subfileRrn,wsaaMaxSubfile))) {
				loadBlankSubfile1300();
				}
			}
			//ILIFE-3686-ends
		}
		
		
		scrnparams.subfileMore.set("N");
		scrnparams.subfileRrn.set(1);
	}

protected void loadSubfile1200()
	{
			start1200();
		}

protected void start1200()
	{
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)
		&& isNE(mbnsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		if (isNE(mbnsIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(mbnsIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isNE(mbnsIO.getLife(),covtlnbIO.getLife())
		|| isNE(mbnsIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(mbnsIO.getRider(),covtlnbIO.getRider())) {
			mbnsIO.setStatuz(varcom.endp);
		}
		if (isEQ(mbnsIO.getStatuz(),varcom.endp)) {
			return ;
		}
		sv.yrsinf.set(mbnsIO.getYrsinf());
		sv.sumins.set(mbnsIO.getSumins());
		sv.surrval.set(mbnsIO.getSurrval());
		scrnparams.function.set(varcom.sadd);
		processScreen("SR564", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		mbnsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			fatalError600();
		}
		mbnsIO.setFunction(varcom.nextr);
	}

protected void assignPolyr1600()
	{
		/*START*/
		sv.yrsinf.set(wsaaIndex);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR564", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadBlankSubfile1300()
	{
		/*START*/
		initialize(sv.yrsinf);
		initialize(sv.sumins);
		initialize(sv.surrval);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR564", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void readMrta1500()
	{
		/*START*/
		mrtaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mrtaIO.setChdrnum(chdrlnbIO.getChdrnum());
		mrtaIO.setFormat(formatsInner.mrtarec);
		mrtaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mrtaIO);
		if (isEQ(mrtaIO.getStatuz(),varcom.oK)) {
			wsaaCoverc.set(mrtaIO.getCoverc());
			wsaaMlresind.set(mrtaIO.getMlresind());
			sv.unitval.set(mrtaIO.getPhunitvalue());
			sv.redfreq.set(mrtaIO.getMlresind());//ILIFE-3714
		}
		else {
			wsaaCoverc.set(ZERO);
			wsaaMlresind.set(SPACES);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		if (isNE(th615rec.premind,"Y")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaScrnStatuz.set(scrnparams.statuz);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set(SPACES);
			scrnparams.subfileRrn.set(1);
		}
		if (isEQ(wsspcomn.flag,"Y")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		scrnparams.statuz.set(varcom.oK);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsaaScrnStatuz,varcom.calc)) {
			scrnparams.subfileRrn.set(1);
		}
	}

protected void readTh615Table2300()
	{
		start2300();
	}

protected void start2300()
	{	if(!mrt1Permission){//brd-139
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(th615);
		itemIO.setItemitem(mrtaIO.getMlresind());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th615rec.th615Rec.set(itemIO.getGenarea());
	}
		
	}


//		//BRD-139-STARTS
//		descIO.setDataKey(SPACES);
//		descIO.setDescpfx("IT");
//		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
//		descIO.setDesctabl(th615);
//		descIO.setDescitem(mrtaIO.getMlresind());
//		descIO.setLanguage(wsspcomn.language);
//		descIO.setFunction("READR");
//		SmartFileCode.execute(appVars, descIO);
//		if (isNE(descIO.getStatuz(), varcom.oK)
//				&& isNE(descIO.getStatuz(), varcom.mrnf)) {
//					syserrrec.params.set(descIO.getParams());
//					fatalError600();
//				}
//		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
//			sv.redfreq.fill("?");
//		}
//		else {
//			sv.redfreq.set(descIO.getLongdesc());
//		}
//		//BRD-139-ENDS



protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2100();
				}
				case srdn2100: {
					srdn2100();
				}
				case exit2100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2100()
	{
		processScreen("SR564", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2100);
		}
		if (isEQ(sv.yrsinf,ZERO)
		&& isEQ(sv.sumins,ZERO)
		&& isEQ(sv.surrval,ZERO)) {
			goTo(GotoLabel.srdn2100);
		}
		if(!mrt1Permission){//brd-139
		if (isGT(sv.yrsinf,wsaaRiskTerm) && isNE(chdrlnbIO.cnttype,"LCP")) {
			sv.yrsinfErr.set(rl18);
			wsspcomn.edterror.set(SPACES);
			scrnparams.function.set(varcom.supd);
			processScreen("SR564", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			}
		}
	}

protected void srdn2100()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.flag,"Y")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			updateRecord3100();
		}
		
	}

protected void updateRecord3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3100();
				}
				case nextSubfileRecord3100: {
					nextSubfileRecord3100();
				}
				case exit3100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3100()
	{
		processScreen("SR564", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3100);
		}
		if (isEQ(sv.yrsinf,ZERO)
		&& isEQ(sv.sumins,ZERO)
		&& isEQ(sv.surrval,ZERO)) {
			goTo(GotoLabel.nextSubfileRecord3100);
		}
		writeRecord3200();
	}

protected void nextSubfileRecord3100()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void writeRecord3200()
	{
		start3200();
	}

protected void start3200()
	{
		mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
		mbnsIO.setLife(covtlnbIO.getLife());
		mbnsIO.setCoverage(covtlnbIO.getCoverage());
		mbnsIO.setRider(covtlnbIO.getRider());
		mbnsIO.setYrsinf(sv.yrsinf);
		mbnsIO.setSumins(sv.sumins);
		mbnsIO.setSurrval(sv.surrval);
		mbnsIO.setFormat(formatsInner.mbnsrec);
		mbnsIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
	}
	public static Map<String,Map<String,String>> f4(COBOLAppVars appVars, String field) throws VpmsLoadFailedException{
		
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();
		Map<String, String> itemDesc=new HashMap<String,String>();
		IVpmsBaseSession session;
		//ILIFE-5605 start
		IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();
		if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
			factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
			session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
		}
		else
		{
			session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
		}
		//ILIFE-5605 end
		session.setAttribute("Input Contract Type", "mrt");
		session.setAttribute("Input Region", " "); 
		session.setAttribute("Input Locale", " "); 
		session.setAttribute("Input TransEffDate", "20161010");
		session.setAttribute("Input MTL PLan Cd", "MAS05S"); 
		session.setAttribute("Input Prod Code", "GS1"); 
		session.setAttribute("Input Coverage Code", "mrt1");
	
		VpmsComputeResult result = session.computeUsingDefaults("Output Fetch MRTA Frequency");
		//sv.mlresind.set(result.getResult());
		String temp1=result.getResult();
	    temp1=temp1.replace("||","/n");
	  
	    String ReducingFreq[] = temp1.split("/n");
	    ArrayList<String> ReducingFreqList = new ArrayList<String>();
	    ArrayList<String> ReducingFreqList1 = new ArrayList<String>();
	    LOGGER.info("parts1={}", Arrays.toString(ReducingFreq));//IJTI-1561
		 
	    for (int i = 0; i < ReducingFreq.length; i =i+2) {
	    	ReducingFreqList.add(ReducingFreq[i]);
	    	ReducingFreqList1.add(ReducingFreq[i+1]);
	
	    }
	    for (int i = 0; i < ReducingFreqList.size(); i++) {
	    	itemDesc.put( ReducingFreqList.get(i).trim(),  ReducingFreqList1.get(i).trim());
			
	        }
		
		f4map.put(field, itemDesc);
		
	    return f4map;
	    
	} 
/*ILIFE-3714 ends*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData mbnsrec = new FixedLengthStringData(10).init("MBNSREC");
	private FixedLengthStringData mrtarec = new FixedLengthStringData(10).init("MRTAREC");
}
}
