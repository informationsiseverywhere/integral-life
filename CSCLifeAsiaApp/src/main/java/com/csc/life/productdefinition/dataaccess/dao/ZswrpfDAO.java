package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZswrpfDAO extends BaseDAO<Zswrpf> {
	public boolean updateZswrpf(List<Zswrpf> zswrpfiList);	
	public List<Zswrpf> getZswrpfData(String chdrnum,String companyno,String prefix,String validflag);
    public void insertZswrpfListRecord(List<Zswrpf> zswrpfiList );    
    public boolean updateZswrpfvalidflag(Zswrpf obj);
    public boolean deleteZswrpf(String chdrnum,String companyno,String prefix);
    public List<Zswrpf> getAllZswrpfData(String chdrnum,String companyno,String prefix,String validflag,String chdrnum1);
    public List<Zswrpf> loadDataByBatch(int batchID);
    public int populateZrstpfTemp(String company, int batchExtractSize, String chdrfrom, String chdrto);
}
