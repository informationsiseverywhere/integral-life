package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR53B
 * @version 1.0 generated on 12/3/13 4:19 AM
 * @author CSC
 */
public class Sr53bScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1202);
	public FixedLengthStringData dataFields = new FixedLengthStringData(498).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData cntdesc = DD.cntdesc.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData contractType = DD.cntyp.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData crcodes = new FixedLengthStringData(40).isAPartOf(dataFields, 63);
	public FixedLengthStringData[] crcode = FLSArrayPartOfStructure(10, 4, crcodes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(crcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData crcode01 = DD.crcode.copy().isAPartOf(filler,0);
	public FixedLengthStringData crcode02 = DD.crcode.copy().isAPartOf(filler,4);
	public FixedLengthStringData crcode03 = DD.crcode.copy().isAPartOf(filler,8);
	public FixedLengthStringData crcode04 = DD.crcode.copy().isAPartOf(filler,12);
	public FixedLengthStringData crcode05 = DD.crcode.copy().isAPartOf(filler,16);
	public FixedLengthStringData crcode06 = DD.crcode.copy().isAPartOf(filler,20);
	public FixedLengthStringData crcode07 = DD.crcode.copy().isAPartOf(filler,24);
	public FixedLengthStringData crcode08 = DD.crcode.copy().isAPartOf(filler,28);
	public FixedLengthStringData crcode09 = DD.crcode.copy().isAPartOf(filler,32);
	public FixedLengthStringData crcode10 = DD.crcode.copy().isAPartOf(filler,36);
	public FixedLengthStringData crtableds = new FixedLengthStringData(300).isAPartOf(dataFields, 103);
	public FixedLengthStringData[] crtabled = FLSArrayPartOfStructure(10, 30, crtableds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(300).isAPartOf(crtableds, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtabled01 = DD.crtabled.copy().isAPartOf(filler1,0);
	public FixedLengthStringData crtabled02 = DD.crtabled.copy().isAPartOf(filler1,30);
	public FixedLengthStringData crtabled03 = DD.crtabled.copy().isAPartOf(filler1,60);
	public FixedLengthStringData crtabled04 = DD.crtabled.copy().isAPartOf(filler1,90);
	public FixedLengthStringData crtabled05 = DD.crtabled.copy().isAPartOf(filler1,120);
	public FixedLengthStringData crtabled06 = DD.crtabled.copy().isAPartOf(filler1,150);
	public FixedLengthStringData crtabled07 = DD.crtabled.copy().isAPartOf(filler1,180);
	public FixedLengthStringData crtabled08 = DD.crtabled.copy().isAPartOf(filler1,210);
	public FixedLengthStringData crtabled09 = DD.crtabled.copy().isAPartOf(filler1,240);
	public FixedLengthStringData crtabled10 = DD.crtabled.copy().isAPartOf(filler1,270);
	public FixedLengthStringData crtables = new FixedLengthStringData(40).isAPartOf(dataFields, 403);
	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(10, 4, crtables, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(crtables, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01 = DD.crtable.copy().isAPartOf(filler2,0);
	public FixedLengthStringData crtable02 = DD.crtable.copy().isAPartOf(filler2,4);
	public FixedLengthStringData crtable03 = DD.crtable.copy().isAPartOf(filler2,8);
	public FixedLengthStringData crtable04 = DD.crtable.copy().isAPartOf(filler2,12);
	public FixedLengthStringData crtable05 = DD.crtable.copy().isAPartOf(filler2,16);
	public FixedLengthStringData crtable06 = DD.crtable.copy().isAPartOf(filler2,20);
	public FixedLengthStringData crtable07 = DD.crtable.copy().isAPartOf(filler2,24);
	public FixedLengthStringData crtable08 = DD.crtable.copy().isAPartOf(filler2,28);
	public FixedLengthStringData crtable09 = DD.crtable.copy().isAPartOf(filler2,32);
	public FixedLengthStringData crtable10 = DD.crtable.copy().isAPartOf(filler2,36);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,443);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,473);
	public FixedLengthStringData extrval = DD.extrval.copy().isAPartOf(dataFields,481);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,482);
	public FixedLengthStringData worku = DD.worku.copy().isAPartOf(dataFields,492);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(176).isAPartOf(dataArea, 498);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crcodesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] crcodeErr = FLSArrayPartOfStructure(10, 4, crcodesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(crcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crcode01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData crcode02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData crcode03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData crcode04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData crcode05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData crcode06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData crcode07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData crcode08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData crcode09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData crcode10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData crtabledsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData[] crtabledErr = FLSArrayPartOfStructure(10, 4, crtabledsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(crtabledsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtabled01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData crtabled02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData crtabled03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData crtabled04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData crtabled05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData crtabled06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData crtabled07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData crtabled08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData crtabled09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData crtabled10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData crtablesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData[] crtableErr = FLSArrayPartOfStructure(10, 4, crtablesErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(crtablesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData crtable02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData crtable03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData crtable04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData crtable05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData crtable06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData crtable07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData crtable08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData crtable09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData crtable10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData extrvalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData workuErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(528).isAPartOf(dataArea, 674);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData crcodesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] crcodeOut = FLSArrayPartOfStructure(10, 12, crcodesOut, 0);
	public FixedLengthStringData[][] crcodeO = FLSDArrayPartOfArrayStructure(12, 1, crcodeOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(crcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crcode01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] crcode02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] crcode03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] crcode04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] crcode05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] crcode06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] crcode07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] crcode08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] crcode09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] crcode10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData crtabledsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 228);
	public FixedLengthStringData[] crtabledOut = FLSArrayPartOfStructure(10, 12, crtabledsOut, 0);
	public FixedLengthStringData[][] crtabledO = FLSDArrayPartOfArrayStructure(12, 1, crtabledOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(crtabledsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crtabled01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] crtabled02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] crtabled03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] crtabled04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] crtabled05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] crtabled06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] crtabled07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] crtabled08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] crtabled09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] crtabled10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData crtablesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 348);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(10, 12, crtablesOut, 0);
	public FixedLengthStringData[][] crtableO = FLSDArrayPartOfArrayStructure(12, 1, crtableOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(crtablesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crtable01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] crtable02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] crtable03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] crtable04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] crtable05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] crtable06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] crtable07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] crtable08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] crtable09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] crtable10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] extrvalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] workuOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sr53bscreenWritten = new LongData(0);
	public LongData Sr53bprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr53bScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(workuOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(extrvalOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntypOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnttypOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypdescOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable01Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode01Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled01Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode02Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled02Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable03Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode03Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled03Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable04Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode04Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled04Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable05Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode05Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled05Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable06Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode06Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled06Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable07Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode07Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled07Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable08Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode08Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled08Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable09Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode09Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled09Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable10Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcode10Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtabled10Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {scheduleName, acctmonth, scheduleNumber, acctyear, effdate, bcompany, jobq, bbranch, worku, extrval, contractType, cntdesc, cnttyp, ctypdesc, crtable01, crcode01, crtabled01, crtable02, crcode02, crtabled02, crtable03, crcode03, crtabled03, crtable04, crcode04, crtabled04, crtable05, crcode05, crtabled05, crtable06, crcode06, crtabled06, crtable07, crcode07, crtabled07, crtable08, crcode08, crtabled08, crtable09, crcode09, crtabled09, crtable10, crcode10, crtabled10};
		screenOutFields = new BaseData[][] {bschednamOut, acctmonthOut, bschednumOut, acctyearOut, effdateOut, bcompanyOut, jobqOut, bbranchOut, workuOut, extrvalOut, cntypOut, cntdescOut, cnttypOut, ctypdescOut, crtable01Out, crcode01Out, crtabled01Out, crtable02Out, crcode02Out, crtabled02Out, crtable03Out, crcode03Out, crtabled03Out, crtable04Out, crcode04Out, crtabled04Out, crtable05Out, crcode05Out, crtabled05Out, crtable06Out, crcode06Out, crtabled06Out, crtable07Out, crcode07Out, crtabled07Out, crtable08Out, crcode08Out, crtabled08Out, crtable09Out, crcode09Out, crtabled09Out, crtable10Out, crcode10Out, crtabled10Out};
		screenErrFields = new BaseData[] {bschednamErr, acctmonthErr, bschednumErr, acctyearErr, effdateErr, bcompanyErr, jobqErr, bbranchErr, workuErr, extrvalErr, cntypErr, cntdescErr, cnttypErr, ctypdescErr, crtable01Err, crcode01Err, crtabled01Err, crtable02Err, crcode02Err, crtabled02Err, crtable03Err, crcode03Err, crtabled03Err, crtable04Err, crcode04Err, crtabled04Err, crtable05Err, crcode05Err, crtabled05Err, crtable06Err, crcode06Err, crtabled06Err, crtable07Err, crcode07Err, crtabled07Err, crtable08Err, crcode08Err, crtabled08Err, crtable09Err, crcode09Err, crtabled09Err, crtable10Err, crcode10Err, crtabled10Err};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr53bscreen.class;
		protectRecord = Sr53bprotect.class;
	}

}
