package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr633screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr633screensfl";
		lrec.subfileClass = Sr633screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 7;
		lrec.pageSubfile = 6;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 15, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr633ScreenVars sv = (Sr633ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr633screenctlWritten, sv.Sr633screensflWritten, av, sv.sr633screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr633ScreenVars screenVars = (Sr633ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.zmednum.setClassString("");
		screenVars.zmedsts.setClassString("");
		screenVars.dteappDisp.setClassString("");
		screenVars.dtetrmDisp.setClassString("");
		screenVars.zdesc.setClassString("");
		screenVars.statdets.setClassString("");
		screenVars.xoptind.setClassString("");
		screenVars.zmedprv.setClassString("");
		screenVars.optdscx.setClassString("");
		screenVars.zmednam.setClassString("");
		screenVars.paymth.setClassString("");
		screenVars.pymdesc.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.zaracde.setClassString("");
		screenVars.coverDesc.setClassString("");
	}

/**
 * Clear all the variables in Sr633screenctl
 */
	public static void clear(VarModel pv) {
		Sr633ScreenVars screenVars = (Sr633ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.zmednum.clear();
		screenVars.zmedsts.clear();
		screenVars.dteappDisp.clear();
		screenVars.dteapp.clear();
		screenVars.dtetrmDisp.clear();
		screenVars.dtetrm.clear();
		screenVars.zdesc.clear();
		screenVars.statdets.clear();
		screenVars.xoptind.clear();
		screenVars.zmedprv.clear();
		screenVars.optdscx.clear();
		screenVars.zmednam.clear();
		screenVars.paymth.clear();
		screenVars.pymdesc.clear();
		screenVars.bankkey.clear();
		screenVars.bankacckey.clear();
		screenVars.zaracde.clear();
		screenVars.coverDesc.clear();
	}
}
