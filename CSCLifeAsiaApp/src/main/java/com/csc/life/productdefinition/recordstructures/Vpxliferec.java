package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Tue, 04 Sep 2012 15:34:00
 * Description:  VPMS General Data Extract - LIFE
 * Copybook name: vpxlifeRec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxliferec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
  	public FixedLengthStringData vpxlifeRec = new FixedLengthStringData(150);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxlifeRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxlifeRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxlifeRec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxlifeRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(14).isAPartOf(vpxlifeRec, 136);
  	public FixedLengthStringData lsSmoking = new FixedLengthStringData(2).isAPartOf(dataRec, 0);
  	public FixedLengthStringData jsSmoking = new FixedLengthStringData(2).isAPartOf(dataRec, 2);
  	public PackedDecimalData lcltdob = new PackedDecimalData(8, 0).isAPartOf(dataRec, 4);
  	public PackedDecimalData jcltdob = new PackedDecimalData(8, 0).isAPartOf(dataRec, 9);
  	
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxlifeRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxlifeRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}