package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:19
 * Description:
 * Copybook name: ZMPVNAMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zmpvnamkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zmpvnamFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zmpvnamKey = new FixedLengthStringData(256).isAPartOf(zmpvnamFileKey, 0, REDEFINE);
  	public FixedLengthStringData zmpvnamZmedcoy = new FixedLengthStringData(1).isAPartOf(zmpvnamKey, 0);
  	public FixedLengthStringData zmpvnamZmednam = new FixedLengthStringData(60).isAPartOf(zmpvnamKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(195).isAPartOf(zmpvnamKey, 61, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zmpvnamFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zmpvnamFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}