package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Br50hDTO {

	private long chdrUniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String cntcurr;
	private String statcode;
	private String pstcde;
	private String cnttype;
	private String covtflag;
	private long covtUniqueNumber;
	private int payrseqno;
	private BigDecimal instprem;
	private BigDecimal singp;
	private long hpadUniqueNumber;
	private int hprrcvdt;
	private int hpropdte;
	private int huwdcdte;
	private String billfreq;
	private int btdate;
	private int incseqno;
	private int payrPayrseqno;
	private String clntcoy;
	private String clntnum;
	private String clntpfx;
	private String clrrrole;
	private String forecoy;
	private String forenum;
	private String crtable;
	private int effdate;
	private BigDecimal sumins;
	private BigDecimal zlinstprem;
	private int crrcd;
	private String pstatcode;
	private String recordtype;
	private int decldate;

	public long getChdrUniqueNumber() {
		return chdrUniqueNumber;
	}
	public void setChdrUniqueNumber(long chdrUniqueNumber) {
		this.chdrUniqueNumber = chdrUniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getPstcde() {
		return pstcde;
	}
	public void setPstcde(String pstcde) {
		this.pstcde = pstcde;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getCovtflag() {
		return covtflag;
	}
	public void setCovtflag(String covtflag) {
		this.covtflag = covtflag;
	}
	public long getCovtUniqueNumber() {
		return covtUniqueNumber;
	}
	public void setCovtUniqueNumber(long covtUniqueNumber) {
		this.covtUniqueNumber = covtUniqueNumber;
	}
	public int getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}
	public BigDecimal getInstprem() {
		return instprem;
	}
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}
	public BigDecimal getSingp() {
		return singp;
	}
	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}
	public long getHpadUniqueNumber() {
		return hpadUniqueNumber;
	}
	public void setHpadUniqueNumber(long hpadUniqueNumber) {
		this.hpadUniqueNumber = hpadUniqueNumber;
	}
	public int getHprrcvdt() {
		return hprrcvdt;
	}
	public void setHprrcvdt(int hprrcvdt) {
		this.hprrcvdt = hprrcvdt;
	}
	public int getHpropdte() {
		return hpropdte;
	}
	public void setHpropdte(int hpropdte) {
		this.hpropdte = hpropdte;
	}
	public int getHuwdcdte() {
		return huwdcdte;
	}
	public void setHuwdcdte(int huwdcdte) {
		this.huwdcdte = huwdcdte;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public int getBtdate() {
		return btdate;
	}
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}
	public int getIncseqno() {
		return incseqno;
	}
	public void setIncseqno(int incseqno) {
		this.incseqno = incseqno;
	}
	public int getPayrPayrseqno() {
		return payrPayrseqno;
	}
	public void setPayrPayrseqno(int payrPayrseqno) {
		this.payrPayrseqno = payrPayrseqno;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(String clntpfx) {
		this.clntpfx = clntpfx;
	}
	public String getClrrrole() {
		return clrrrole;
	}
	public void setClrrrole(String clrrrole) {
		this.clrrrole = clrrrole;
	}
	public String getForecoy() {
		return forecoy;
	}
	public void setForecoy(String forecoy) {
		this.forecoy = forecoy;
	}
	public String getForenum() {
		return forenum;
	}
	public void setForenum(String forenum) {
		this.forenum = forenum;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public BigDecimal getZlinstprem() {
		return zlinstprem;
	}
	public void setZlinstprem(BigDecimal zlinstprem) {
		this.zlinstprem = zlinstprem;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getRecordtype() {
		return recordtype;
	}
	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}
	public int getDecldate() {
		return decldate;
	}
	public void setDecldate(int decldate) {
		this.decldate = decldate;
	}
}
