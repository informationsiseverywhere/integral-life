package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Sd5gfScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(421);
	public FixedLengthStringData dataFields = new FixedLengthStringData(85).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData ratebas = DD.datebasis.copy().isAPartOf(dataFields,44);		

	public FixedLengthStringData gender = new FixedLengthStringData(4).isAPartOf(dataFields, 45);
	public FixedLengthStringData[] gnder = FLSArrayPartOfStructure(4, 1, gender, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(gender, 0, FILLER_REDEFINE);
	public FixedLengthStringData gender01 = DD.gender.copy().isAPartOf(filler1,0);
	public FixedLengthStringData gender02 = DD.gender.copy().isAPartOf(filler1,1);
	public FixedLengthStringData gender03 = DD.gender.copy().isAPartOf(filler1,2);
	public FixedLengthStringData gender04 = DD.gender.copy().isAPartOf(filler1,3);
		
	public FixedLengthStringData frmages = new FixedLengthStringData(12).isAPartOf(dataFields,49);
	public ZonedDecimalData[] frmage = ZDArrayPartOfStructure(4, 3, 0, frmages, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(frmages, 0, FILLER_REDEFINE);
	public ZonedDecimalData frmage01 = DD.frmage.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData frmage02 = DD.frmage.copyToZonedDecimal().isAPartOf(filler2,3);
	public ZonedDecimalData frmage03 = DD.frmage.copyToZonedDecimal().isAPartOf(filler2,6);
	public ZonedDecimalData frmage04 = DD.frmage.copyToZonedDecimal().isAPartOf(filler2,9);
	
	public FixedLengthStringData toages = new FixedLengthStringData(12).isAPartOf(dataFields, 61);
	public ZonedDecimalData[] toage = ZDArrayPartOfStructure(4, 3, 0, toages, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(toages, 0, FILLER_REDEFINE);
	public ZonedDecimalData toage01 = DD.age.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData toage02 = DD.age.copyToZonedDecimal().isAPartOf(filler3,3);
	public ZonedDecimalData toage03 = DD.age.copyToZonedDecimal().isAPartOf(filler3,6);
	public ZonedDecimalData toage04 = DD.age.copyToZonedDecimal().isAPartOf(filler3,9);
	
	public FixedLengthStringData cooloff = new FixedLengthStringData(12).isAPartOf(dataFields, 73);
	public ZonedDecimalData[] coolingoff = ZDArrayPartOfStructure(4, 3 , 0, cooloff, 0);
	public FixedLengthStringData filler4= new FixedLengthStringData(12).isAPartOf(cooloff, 0, FILLER_REDEFINE);
	public ZonedDecimalData coolingoff1 = DD.coolingoff.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData coolingoff2 = DD.coolingoff.copyToZonedDecimal().isAPartOf(filler4,3);
	public ZonedDecimalData coolingoff3 = DD.coolingoff.copyToZonedDecimal().isAPartOf(filler4,6);
	public ZonedDecimalData coolingoff4 = DD.coolingoff.copyToZonedDecimal().isAPartOf(filler4,9);	
			
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea,85);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ratebasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData gender01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData gender02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData gender03Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData gender04Err = new FixedLengthStringData(4).isAPartOf(errorIndicators,32);
	public FixedLengthStringData frmage01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData frmage02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData frmage03Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData frmage04Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData toage01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData toage02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData toage03Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData toage04Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData coolingoff1Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData coolingoff2Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData coolingoff3Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData coolingoff4Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
		
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 169);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ratebasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] gender01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] gender02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] gender03Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] gender04Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] frmage01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] frmage02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] frmage03Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] frmage04Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] toage01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] toage02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] toage03Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] toage04Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] coolingoff1Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] coolingoff2Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] coolingoff3Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] coolingoff4Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	
	public boolean isCreateMode =false;
	
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sd5gfscreenWritten = new LongData(0);
	public LongData Sd5gfprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5gfScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(gender01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gender02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gender03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gender04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratebasOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, ratebas, gender01, gender02, gender03, gender04, frmage01, frmage02, frmage03, frmage04, toage01, toage01,toage03,toage04,coolingoff1,coolingoff2,coolingoff3,coolingoff4};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, ratebasOut, gender01Out, gender02Out, gender03Out, gender04Out, frmage01Out, frmage02Out, frmage03Out, frmage04Out, toage01Out,toage02Out,toage03Out,toage04Out,coolingoff1Out,coolingoff2Out,coolingoff3Out,coolingoff4Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, ratebasErr, gender01Err, gender02Err, gender03Err, gender04Err, frmage01Err, frmage02Err, frmage03Err, frmage04Err, toage01Err, toage02Err,toage03Err,toage04Err,coolingoff1Err,coolingoff2Err,coolingoff3Err,coolingoff4Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5gfscreen.class;
		protectRecord = Sd5gfprotect.class;
	}

}
