package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:18
 * Description:
 * Copybook name: ZMPDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zmpdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zmpdFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zmpdKey = new FixedLengthStringData(256).isAPartOf(zmpdFileKey, 0, REDEFINE);
  	public FixedLengthStringData zmpdZmedcoy = new FixedLengthStringData(1).isAPartOf(zmpdKey, 0);
  	public FixedLengthStringData zmpdZmedprv = new FixedLengthStringData(10).isAPartOf(zmpdKey, 1);
  	public PackedDecimalData zmpdSeqno = new PackedDecimalData(2, 0).isAPartOf(zmpdKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(zmpdKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zmpdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zmpdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}