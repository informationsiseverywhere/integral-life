package com.csc.life.productdefinition.dataaccess.dao.impl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;
public class ExclpfDAOImpl extends BaseDAOImpl<Exclpf> implements ExclpfDAO{
private static final Logger LOGGER = LoggerFactory.getLogger(ExclpfDAOImpl.class);
	
	public Exclpf readRecord(String chdrcoy,String chdrnum ,String comp, String life, String coverage, String rider ){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"select * From EXCLPF where CHDRCOY=? and CHDRNUM=? and RTRIM(CRTABLE)=? and LIFE=? and COVERAGE=? and RIDER=? and (VALIDFLAG='1' OR VALIDFLAG='3')");
		Exclpf exclpfRec = null;
		
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		  ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, comp);
			ps.setString(4,life);
			ps.setString(5,coverage);
			ps.setString(6,rider);
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    exclpfRec = new Exclpf();
			    exclpfRec.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
			    exclpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    exclpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    exclpfRec.setLife(rs.getString("LIFE"));
			    exclpfRec.setCoverage(rs.getString("COVERAGE"));
			    exclpfRec.setRider(rs.getString("RIDER"));
			    exclpfRec.setPlnsfx(rs.getInt("PLNSFX"));
			    exclpfRec.setValidflag(rs.getString("VALIDFLAG"));
			    exclpfRec.setSeqno(rs.getInt("SEQNBR"));
			    }
		}
			catch(SQLException e)
			{
				LOGGER.error(" ExclpfDAOImpl.readExclpfData()",e);//IJTI-1485
				throw new SQLRuntimeException(e);	
			}
			
			return exclpfRec;

		
	}

	public LinkedList<Exclpf> getExclpfRecord(String chdrnum, String component, String life,String coverage,String rider) {
		StringBuilder sqlSchemeSelect1 = new StringBuilder("select * From EXCLPF where CHDRNUM=?  and LIFE=? and COVERAGE=? and RIDER=? and (VALIDFLAG='1' OR VALIDFLAG='3')");
	LinkedList<Exclpf> exclpflist = new LinkedList<>();
	try {
		PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		ps.setString(1, chdrnum);
		ps.setString(2,life);
		ps.setString(3,coverage);
		ps.setString(4,rider);
		ResultSet rs = null;
		rs = ps.executeQuery();
		while (rs.next()) {
			Exclpf exclpfRec = new Exclpf();
			exclpfRec.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
			exclpfRec.setChdrnum(rs.getNString("CHDRNUM"));
			exclpfRec.setCrtable(rs.getString("CRTABLE"));
			exclpfRec.setExcda(rs.getString("EXCDA"));
			exclpfRec.setExadxt(rs.getString("EXADTXT"));
			exclpfRec.setPrntstat(rs.getString("PRNTSTAT"));
			exclpfRec.setValidflag(rs.getString("VALIDFLAG"));
			exclpfRec.setSeqno(rs.getInt("SEQNBR"));
			exclpfRec.setLife(rs.getString("LIFE"));
		    exclpfRec.setCoverage(rs.getString("COVERAGE"));
		    exclpfRec.setRider(rs.getString("RIDER"));
		    exclpfRec.setPlnsfx(rs.getInt("PLNSFX"));
			exclpflist.add(exclpfRec);
		}
	} catch (SQLException e) {
		LOGGER.error("Error occurred while reading EXCLPF", e);// IJTI-1485
		throw new SQLRuntimeException(e);
	}

	return exclpflist;
}
	public Exclpf readRecordByCode(String chdrnum,String comp,String excode,String life,String coverage,String rider){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"select * From EXCLPF where CHDRNUM=? and RTRIM(CRTABLE)=? and RTRIM(EXCDA)=? and LIFE=? and COVERAGE=? and RIDER=?");
		Exclpf exclpfRec = null;
		
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
			ps.setString(1, chdrnum);
			ps.setString(2, comp);
			ps.setString(3, excode);
			ps.setString(4,life);
			ps.setString(5,coverage);
			ps.setString(6,rider);
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    exclpfRec = new Exclpf();
			    exclpfRec.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
			    exclpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    exclpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    exclpfRec.setCrtable(rs.getString("CRTABLE"));
			    exclpfRec.setExadxt(rs.getString("EXADTXT"));
			    exclpfRec.setExcda(rs.getString("EXCDA"));
			    exclpfRec.setValidflag(rs.getString("VALIDFLAG"));
			    exclpfRec.setPrntstat(rs.getString("PRNTSTAT"));
			    exclpfRec.setSeqno(rs.getInt("SEQNBR"));
			    exclpfRec.setLife(rs.getString("LIFE"));
			    exclpfRec.setCoverage(rs.getString("COVERAGE"));
			    exclpfRec.setRider(rs.getString("RIDER"));
			    exclpfRec.setPlnsfx(rs.getInt("PLNSFX"));
			    }
		}
			catch(SQLException e)
			{
				LOGGER.error("Error Occurred while reading EXCLPF",e);//IJTI-1485
				throw new SQLRuntimeException(e);	
			}
			
			return exclpfRec;

		
	}
	public void insertRecord(Exclpf exclpf){
		if (exclpf != null) {
		      
			StringBuilder sql_exclpf_insert = new StringBuilder();
			sql_exclpf_insert.append("INSERT INTO EXCLPF");
			sql_exclpf_insert.append("(CHDRCOY,CHDRNUM,CRTABLE,SEQNBR,EXCDA,EFFDATE,EXADTXT,PRNTSTAT,USRPRF,JOBNM,DATIME,VALIDFLAG,TRANNO,LIFE,COVERAGE,RIDER,PLNSFX)");
			sql_exclpf_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement psInsert = getPrepareStatement(sql_exclpf_insert.toString());
			try
			{
					psInsert.setString(1, exclpf.getChdrcoy());
					psInsert.setString(2, exclpf.getChdrnum());
					psInsert.setString(3, exclpf.getCrtable());
					psInsert.setInt(4, exclpf.getSeqno());
					psInsert.setString(5, exclpf.getExcda());
					psInsert.setInt(6, exclpf.getEffdate());
					psInsert.setString(7, exclpf.getExadtxt());
					psInsert.setString(8, exclpf.getPrntstat());
					psInsert.setString(9, this.getUsrprf());
					psInsert.setString(10, this.getJobnm());
					psInsert.setTimestamp(11, new Timestamp(System.currentTimeMillis()));
					psInsert.setString(12,exclpf.getValidflag());
					psInsert.setInt(13, exclpf.getTranno());
					psInsert.setString(14,exclpf.getLife());
					psInsert.setString(15,exclpf.getCoverage());
					psInsert.setString(16,exclpf.getRider());
					psInsert.setInt(17,exclpf.getPlnsfx());
					psInsert.executeUpdate();
			}
			catch (SQLException e) {
                LOGGER.error("insertExclpfRecord()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, null);
            }
		}

	}
	public void updateRecord(Exclpf exclpf,String excda){
		StringBuilder sql_exclpf_update = new StringBuilder("");
		sql_exclpf_update.append("UPDATE EXCLPF  ");
		sql_exclpf_update.append("SET ");		 
		sql_exclpf_update.append("EXADTXT=?,EXCDA=?,USRPRF=?,JOBNM=?,DATIME=?,TRANNO=?");
		sql_exclpf_update.append(" WHERE UNIQUE_NUMBER=?");
		PreparedStatement psUpdate = null;
		try{
			psUpdate = getPrepareStatement(sql_exclpf_update.toString());
			
			psUpdate.setString(1, exclpf.getExadtxt());
			psUpdate.setString(2, excda);
			psUpdate.setString(3, this.getUsrprf());
			psUpdate.setString(4, this.getJobnm());
			psUpdate.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			psUpdate.setInt(6, exclpf.getTranno());
			psUpdate.setLong(7, exclpf.getUnique_number());
			psUpdate.executeUpdate();	
		}
		//IJTI-851-Overly Broad Catch
		catch ( SQLException e) {
			LOGGER.error("updateexclpf()",e);//IJTI-1485
			
		} finally {
			close(psUpdate, null);			
		}		
		return;
	
	}
	public void deleteRecord(Exclpf exclpf){
		StringBuilder sql_exclpf_delete = new StringBuilder("");
		sql_exclpf_delete.append("DELETE FROM EXCLPF  ");
		sql_exclpf_delete.append("WHERE CHDRNUM='"+exclpf.getChdrnum()+"'");
		sql_exclpf_delete.append("AND CRTABLE='"+exclpf.getCrtable()+"'");
		sql_exclpf_delete.append("AND EXCDA='"+exclpf.getExcda()+"'");
		sql_exclpf_delete.append("AND LIFE='"+exclpf.getLife()+"'");
		sql_exclpf_delete.append("AND COVERAGE='"+exclpf.getCoverage()+"'");
		sql_exclpf_delete.append("AND RIDER='"+exclpf.getRider()+"'");
		PreparedStatement preparedStmt = getPrepareStatement(sql_exclpf_delete.toString());
        try{
        	preparedStmt.executeUpdate();
        }catch (SQLException e) {
            LOGGER.error("deleteRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
        	close(preparedStmt,null);
        }
	}

	public LinkedList<Exclpf> getRecordByContract(String chdrnum) {
		StringBuilder sqlSchemeSelect1 = new StringBuilder("select * From EXCLPF where CHDRNUM=? and (VALIDFLAG='1' OR VALIDFLAG='3')");
		LinkedList<Exclpf> exclpflist = new LinkedList<>();
		try {
			PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
			ps.setString(1, chdrnum);

			ResultSet rs = null;
			rs = ps.executeQuery();
			while (rs.next()) {
				Exclpf exclpfRec = new Exclpf();
				exclpfRec.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				exclpfRec.setChdrnum(rs.getNString("CHDRNUM"));
				exclpfRec.setExcda(rs.getString("EXCDA"));
				exclpfRec.setCrtable(rs.getString("CRTABLE"));
				exclpfRec.setValidflag(rs.getString("VALIDFLAG"));
				exclpfRec.setSeqno(rs.getInt("SEQNBR"));
				exclpfRec.setLife(rs.getString("LIFE"));
			    exclpfRec.setCoverage(rs.getString("COVERAGE"));
			    exclpfRec.setRider(rs.getString("RIDER"));
			    exclpfRec.setPlnsfx(rs.getInt("PLNSFX"));
				exclpflist.add(exclpfRec);

			}
		} catch (SQLException e) {
			LOGGER.error(" ExclpfDAOImpl.readExclpfData()", e);// IJTI-1485
			throw new SQLRuntimeException(e);
		}

		return exclpflist;

	}
	public void updateStatus(Exclpf exclpf){
		StringBuilder sql_exclpf_update = new StringBuilder("");
		sql_exclpf_update.append("UPDATE EXCLPF  ");
		sql_exclpf_update.append("SET ");		 
		sql_exclpf_update.append("PRNTSTAT='Printed',USRPRF=?,JOBNM=?,DATIME=?,TRANNO=?,VALIDFLAG='1' ");
		sql_exclpf_update.append("WHERE UNIQUE_NUMBER=?");
		PreparedStatement psUpdate = null;
		try{
			psUpdate = getPrepareStatement(sql_exclpf_update.toString());
			psUpdate.setString(1, this.getUsrprf());
			psUpdate.setString(2, this.getJobnm());
			psUpdate.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			psUpdate.setInt(4, exclpf.getTranno());
			psUpdate.setLong(5, exclpf.getUnique_number());
			psUpdate.executeUpdate();
		}
		//IJTI-851-Overly Broad Catch
		catch ( SQLException e) {
			LOGGER.error("updateexclpf()",e);//IJTI-1485
			
		} finally {
			close(psUpdate, null);			
		}		
		return;
	
	}
	public void updateRecordToInvalidate(Exclpf exclpf, boolean trannoUpdate){
		StringBuilder sql_exclpf_update = new StringBuilder("");
		sql_exclpf_update.append("UPDATE EXCLPF  ");
		sql_exclpf_update.append("SET ");		 
		sql_exclpf_update.append("USRPRF=?,JOBNM=?,DATIME=?,VALIDFLAG=?");
		if(trannoUpdate)
		{
		sql_exclpf_update.append(",TRANNO=?");
		}
		sql_exclpf_update.append(" WHERE UNIQUE_NUMBER=?");
		PreparedStatement psUpdate = null;
		try{
			psUpdate = getPrepareStatement(sql_exclpf_update.toString());
			
			psUpdate.setString(1, this.getUsrprf());
			psUpdate.setString(2, this.getJobnm());
			psUpdate.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			psUpdate.setString(4, exclpf.getValidflag());
			if(trannoUpdate) {
			psUpdate.setInt(5, exclpf.getTranno());
			psUpdate.setLong(6, exclpf.getUnique_number());
			}
			else
			{
				psUpdate.setLong(5, exclpf.getUnique_number());

			}
			psUpdate.executeUpdate();	
		}
		//IJTI-851-Overly Broad Catch
		catch ( SQLException e) {
			LOGGER.error("updateexclpf()",e);//IJTI-1485
			
		} finally {
			close(psUpdate, null);			
		}		
		return;
	
	}
	
	public List<Exclpf> getExclpfRecordInReverseOrder(String chdrcoy, String chdrnum, Integer tranno, String life, String coverage, String rider) {
		StringBuilder sqlSchemeSelect1 = new StringBuilder("select * From EXCLPF where CHDRCOY=? and CHDRNUM=? and LIFE=? and COVERAGE=? and RIDER=? ORDER BY SEQNBR, TRANNO DESC, UNIQUE_NUMBER DESC");
		List<Exclpf> exclpfList = new ArrayList<Exclpf>();
		try {
			PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3,life);
			ps.setString(4,coverage);
			ps.setString(5,rider);
			ResultSet rs = null;
			
			rs = ps.executeQuery();
			while (rs.next()) {
				Exclpf exclpf = new Exclpf();
				exclpf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				exclpf.setChdrcoy(rs.getString("CHDRCOY"));
				exclpf.setChdrnum(rs.getString("CHDRNUM"));
				exclpf.setValidflag(rs.getString("VALIDFLAG"));
				exclpf.setTranno(rs.getInt("TRANNO"));
				exclpf.setSeqno(rs.getInt("SEQNBR"));
				exclpf.setLife(rs.getString("LIFE"));
				exclpf.setCoverage(rs.getString("COVERAGE"));
				exclpf.setRider(rs.getString("RIDER"));
				exclpfList.add(exclpf);
			}
		} catch (SQLException e) {
			LOGGER.error("Error occurred while reading EXCLPF", e);// IJTI-1485
			throw new SQLRuntimeException(e);
		}

		return exclpfList;

	}
	public void deleteRecordByUniqueNumber(Exclpf exclpf){
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM EXCLPF  ");
		sb.append("WHERE UNIQUE_NUMBER=?");
		
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
        try{
        	ps = getPrepareStatement(sb.toString());	

		    ps.setLong(1,  exclpf.getUnique_number());
		    	    
		    ps.executeUpdate();	
        }catch (SQLException e) {
            LOGGER.error("deleteRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
        	close(ps,null);
        }
	}
	
	public int getMaxSeqnbr(String chdrcoy, String chdrnum, String life, String coverage, String rider) {
		String sql = "SELECT MAX(SEQNBR) SEQNBR FROM EXCLPF WHERE CHDRCOY=? AND CHDRNUM=? and LIFE=? and COVERAGE=? and RIDER=?  GROUP BY CHDRCOY,CHDRNUM ";					
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	Integer seqno = 0;
    	try {
    		ps = getPrepareStatement(sql);
    		ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum.trim());
			ps.setString(3,life);
			ps.setString(4,coverage);
			ps.setString(5,rider);
        	rs = ps.executeQuery();
    		if (rs.next()) {
    			Exclpf excl = new Exclpf();
    			seqno = rs.getInt("SEQNBR");
    			excl.setSeqno(seqno);
    		}	
    	}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps, rs);
		}
    	return seqno;
	}
	
}