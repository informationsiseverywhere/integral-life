package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CoevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:34:22
 * Class transformed from COEV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CoevTableDAM extends CoevpfTableDAM {

	public CoevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("COEV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", EFFDATE"
		             + ", CURRFROM"
		             + ", VALIDFLAG";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "EFFDATE, " +
		            "CURRFROM, " +
		            "VALIDFLAG, " +
		            "CRTABLE, " +
		            "SUMINS, " +
		            "RCESTRM, " +
		            "PCESTRM, " +
		            "CRRCD, " +
		            "ZLINSTPREM, " +
		            "INSTPREM, " +
		            "AEXTPRM, " +
		            "APREM, " +
		            "OPCDA01, " +
		            "OPCDA02, " +
		            "OPCDA03, " +
		            "OPCDA04, " +
		            "OPCDA05, " +
		            "OPPC01, " +
		            "OPPC02, " +
		            "OPPC03, " +
		            "OPPC04, " +
		            "OPPC05, " +
		            "AGERATE01, " +
		            "AGERATE02, " +
		            "AGERATE03, " +
		            "AGERATE04, " +
		            "AGERATE05, " +
		            "INSPRM01, " +
		            "INSPRM02, " +
		            "INSPRM03, " +
		            "INSPRM04, " +
		            "INSPRM05, " +
		            "ECESTRM01, " +
		            "ECESTRM02, " +
		            "ECESTRM03, " +
		            "ECESTRM04, " +
		            "ECESTRM05, " +
		            "VRTFND01, " +
		            "VRTFND02, " +
		            "VRTFND03, " +
		            "VRTFND04, " +
		            "VRTFND05, " +
		            "FUNDAMNT01, " +
		            "FUNDAMNT02, " +
		            "FUNDAMNT03, " +
		            "FUNDAMNT04, " +
		            "FUNDAMNT05, " +
		            "RGPYMOP01, " +
		            "RGPYMOP02, " +
		            "RGPYMOP03, " +
		            "RGPYMOP04, " +
		            "RGPYMOP05, " +
		            "ANNAME01, " +
		            "ANNAME02, " +
		            "ANNAME03, " +
		            "ANNAME04, " +
		            "ANNAME05, " +
		            "ANNAGE01, " +
		            "ANNAGE02, " +
		            "ANNAGE03, " +
		            "ANNAGE04, " +
		            "ANNAGE05, " +
		            "SANNAME01, " +
		            "SANNAME02, " +
		            "SANNAME03, " +
		            "SANNAME04, " +
		            "SANNAME05, " +
		            "SANNAGE01, " +
		            "SANNAGE02, " +
		            "SANNAGE03, " +
		            "SANNAGE04, " +
		            "SANNAGE05, " +
		            "FPAYDATE01, " +
		            "FPAYDATE02, " +
		            "FPAYDATE03, " +
		            "FPAYDATE04, " +
		            "FPAYDATE05, " +
		            "NPAYDATE01, " +
		            "NPAYDATE02, " +
		            "NPAYDATE03, " +
		            "NPAYDATE04, " +
		            "NPAYDATE05, " +
		            "EPAYDATE01, " +
		            "EPAYDATE02, " +
		            "EPAYDATE03, " +
		            "EPAYDATE04, " +
		            "EPAYDATE05, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "PYMT01, " +
		            "PYMT02, " +
		            "PYMT03, " +
		            "PYMT04, " +
		            "PYMT05, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "EFFDATE DESC, " +
		            "CURRFROM DESC, " +
		            "VALIDFLAG ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "EFFDATE ASC, " +
		            "CURRFROM ASC, " +
		            "VALIDFLAG DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               effdate,
                               currfrom,
                               validflag,
                               crtable,
                               sumins,
                               riskCessTerm,
                               premCessTerm,
                               crrcd,
                               zlinstprem,
                               instprem,
                               aextprm,
                               aprem,
                               opcda01,
                               opcda02,
                               opcda03,
                               opcda04,
                               opcda05,
                               oppc01,
                               oppc02,
                               oppc03,
                               oppc04,
                               oppc05,
                               agerate01,
                               agerate02,
                               agerate03,
                               agerate04,
                               agerate05,
                               insprm01,
                               insprm02,
                               insprm03,
                               insprm04,
                               insprm05,
                               extCessTerm01,
                               extCessTerm02,
                               extCessTerm03,
                               extCessTerm04,
                               extCessTerm05,
                               unitVirtualFund01,
                               unitVirtualFund02,
                               unitVirtualFund03,
                               unitVirtualFund04,
                               unitVirtualFund05,
                               fundAmount01,
                               fundAmount02,
                               fundAmount03,
                               fundAmount04,
                               fundAmount05,
                               rgpymop01,
                               rgpymop02,
                               rgpymop03,
                               rgpymop04,
                               rgpymop05,
                               anname01,
                               anname02,
                               anname03,
                               anname04,
                               anname05,
                               annage01,
                               annage02,
                               annage03,
                               annage04,
                               annage05,
                               sanname01,
                               sanname02,
                               sanname03,
                               sanname04,
                               sanname05,
                               sannage01,
                               sannage02,
                               sannage03,
                               sannage04,
                               sannage05,
                               firstPaydate01,
                               firstPaydate02,
                               firstPaydate03,
                               firstPaydate04,
                               firstPaydate05,
                               nextPaydate01,
                               nextPaydate02,
                               nextPaydate03,
                               nextPaydate04,
                               nextPaydate05,
                               finalPaydate01,
                               finalPaydate02,
                               finalPaydate03,
                               finalPaydate04,
                               finalPaydate05,
                               jobName,
                               userProfile,
                               datime,
                               pymt01,
                               pymt02,
                               pymt03,
                               pymt04,
                               pymt05,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(38);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getEffdate().toInternal()
					+ getCurrfrom().toInternal()
					+ getValidflag().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(effdate.toInternal());
	nonKeyFiller70.setInternal(currfrom.toInternal());
	nonKeyFiller80.setInternal(validflag.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(883);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ getCrtable().toInternal()
					+ getSumins().toInternal()
					+ getRiskCessTerm().toInternal()
					+ getPremCessTerm().toInternal()
					+ getCrrcd().toInternal()
					+ getZlinstprem().toInternal()
					+ getInstprem().toInternal()
					+ getAextprm().toInternal()
					+ getAprem().toInternal()
					+ getOpcda01().toInternal()
					+ getOpcda02().toInternal()
					+ getOpcda03().toInternal()
					+ getOpcda04().toInternal()
					+ getOpcda05().toInternal()
					+ getOppc01().toInternal()
					+ getOppc02().toInternal()
					+ getOppc03().toInternal()
					+ getOppc04().toInternal()
					+ getOppc05().toInternal()
					+ getAgerate01().toInternal()
					+ getAgerate02().toInternal()
					+ getAgerate03().toInternal()
					+ getAgerate04().toInternal()
					+ getAgerate05().toInternal()
					+ getInsprm01().toInternal()
					+ getInsprm02().toInternal()
					+ getInsprm03().toInternal()
					+ getInsprm04().toInternal()
					+ getInsprm05().toInternal()
					+ getExtCessTerm01().toInternal()
					+ getExtCessTerm02().toInternal()
					+ getExtCessTerm03().toInternal()
					+ getExtCessTerm04().toInternal()
					+ getExtCessTerm05().toInternal()
					+ getUnitVirtualFund01().toInternal()
					+ getUnitVirtualFund02().toInternal()
					+ getUnitVirtualFund03().toInternal()
					+ getUnitVirtualFund04().toInternal()
					+ getUnitVirtualFund05().toInternal()
					+ getFundAmount01().toInternal()
					+ getFundAmount02().toInternal()
					+ getFundAmount03().toInternal()
					+ getFundAmount04().toInternal()
					+ getFundAmount05().toInternal()
					+ getRgpymop01().toInternal()
					+ getRgpymop02().toInternal()
					+ getRgpymop03().toInternal()
					+ getRgpymop04().toInternal()
					+ getRgpymop05().toInternal()
					+ getAnname01().toInternal()
					+ getAnname02().toInternal()
					+ getAnname03().toInternal()
					+ getAnname04().toInternal()
					+ getAnname05().toInternal()
					+ getAnnage01().toInternal()
					+ getAnnage02().toInternal()
					+ getAnnage03().toInternal()
					+ getAnnage04().toInternal()
					+ getAnnage05().toInternal()
					+ getSanname01().toInternal()
					+ getSanname02().toInternal()
					+ getSanname03().toInternal()
					+ getSanname04().toInternal()
					+ getSanname05().toInternal()
					+ getSannage01().toInternal()
					+ getSannage02().toInternal()
					+ getSannage03().toInternal()
					+ getSannage04().toInternal()
					+ getSannage05().toInternal()
					+ getFirstPaydate01().toInternal()
					+ getFirstPaydate02().toInternal()
					+ getFirstPaydate03().toInternal()
					+ getFirstPaydate04().toInternal()
					+ getFirstPaydate05().toInternal()
					+ getNextPaydate01().toInternal()
					+ getNextPaydate02().toInternal()
					+ getNextPaydate03().toInternal()
					+ getNextPaydate04().toInternal()
					+ getNextPaydate05().toInternal()
					+ getFinalPaydate01().toInternal()
					+ getFinalPaydate02().toInternal()
					+ getFinalPaydate03().toInternal()
					+ getFinalPaydate04().toInternal()
					+ getFinalPaydate05().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ getPymt01().toInternal()
					+ getPymt02().toInternal()
					+ getPymt03().toInternal()
					+ getPymt04().toInternal()
					+ getPymt05().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, riskCessTerm);
			what = ExternalData.chop(what, premCessTerm);
			what = ExternalData.chop(what, crrcd);
			what = ExternalData.chop(what, zlinstprem);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, aextprm);
			what = ExternalData.chop(what, aprem);
			what = ExternalData.chop(what, opcda01);
			what = ExternalData.chop(what, opcda02);
			what = ExternalData.chop(what, opcda03);
			what = ExternalData.chop(what, opcda04);
			what = ExternalData.chop(what, opcda05);
			what = ExternalData.chop(what, oppc01);
			what = ExternalData.chop(what, oppc02);
			what = ExternalData.chop(what, oppc03);
			what = ExternalData.chop(what, oppc04);
			what = ExternalData.chop(what, oppc05);
			what = ExternalData.chop(what, agerate01);
			what = ExternalData.chop(what, agerate02);
			what = ExternalData.chop(what, agerate03);
			what = ExternalData.chop(what, agerate04);
			what = ExternalData.chop(what, agerate05);
			what = ExternalData.chop(what, insprm01);
			what = ExternalData.chop(what, insprm02);
			what = ExternalData.chop(what, insprm03);
			what = ExternalData.chop(what, insprm04);
			what = ExternalData.chop(what, insprm05);
			what = ExternalData.chop(what, extCessTerm01);
			what = ExternalData.chop(what, extCessTerm02);
			what = ExternalData.chop(what, extCessTerm03);
			what = ExternalData.chop(what, extCessTerm04);
			what = ExternalData.chop(what, extCessTerm05);
			what = ExternalData.chop(what, unitVirtualFund01);
			what = ExternalData.chop(what, unitVirtualFund02);
			what = ExternalData.chop(what, unitVirtualFund03);
			what = ExternalData.chop(what, unitVirtualFund04);
			what = ExternalData.chop(what, unitVirtualFund05);
			what = ExternalData.chop(what, fundAmount01);
			what = ExternalData.chop(what, fundAmount02);
			what = ExternalData.chop(what, fundAmount03);
			what = ExternalData.chop(what, fundAmount04);
			what = ExternalData.chop(what, fundAmount05);
			what = ExternalData.chop(what, rgpymop01);
			what = ExternalData.chop(what, rgpymop02);
			what = ExternalData.chop(what, rgpymop03);
			what = ExternalData.chop(what, rgpymop04);
			what = ExternalData.chop(what, rgpymop05);
			what = ExternalData.chop(what, anname01);
			what = ExternalData.chop(what, anname02);
			what = ExternalData.chop(what, anname03);
			what = ExternalData.chop(what, anname04);
			what = ExternalData.chop(what, anname05);
			what = ExternalData.chop(what, annage01);
			what = ExternalData.chop(what, annage02);
			what = ExternalData.chop(what, annage03);
			what = ExternalData.chop(what, annage04);
			what = ExternalData.chop(what, annage05);
			what = ExternalData.chop(what, sanname01);
			what = ExternalData.chop(what, sanname02);
			what = ExternalData.chop(what, sanname03);
			what = ExternalData.chop(what, sanname04);
			what = ExternalData.chop(what, sanname05);
			what = ExternalData.chop(what, sannage01);
			what = ExternalData.chop(what, sannage02);
			what = ExternalData.chop(what, sannage03);
			what = ExternalData.chop(what, sannage04);
			what = ExternalData.chop(what, sannage05);
			what = ExternalData.chop(what, firstPaydate01);
			what = ExternalData.chop(what, firstPaydate02);
			what = ExternalData.chop(what, firstPaydate03);
			what = ExternalData.chop(what, firstPaydate04);
			what = ExternalData.chop(what, firstPaydate05);
			what = ExternalData.chop(what, nextPaydate01);
			what = ExternalData.chop(what, nextPaydate02);
			what = ExternalData.chop(what, nextPaydate03);
			what = ExternalData.chop(what, nextPaydate04);
			what = ExternalData.chop(what, nextPaydate05);
			what = ExternalData.chop(what, finalPaydate01);
			what = ExternalData.chop(what, finalPaydate02);
			what = ExternalData.chop(what, finalPaydate03);
			what = ExternalData.chop(what, finalPaydate04);
			what = ExternalData.chop(what, finalPaydate05);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, pymt01);
			what = ExternalData.chop(what, pymt02);
			what = ExternalData.chop(what, pymt03);
			what = ExternalData.chop(what, pymt04);
			what = ExternalData.chop(what, pymt05);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	public PackedDecimalData getRiskCessTerm() {
		return riskCessTerm;
	}
	public void setRiskCessTerm(Object what) {
		setRiskCessTerm(what, false);
	}
	public void setRiskCessTerm(Object what, boolean rounded) {
		if (rounded)
			riskCessTerm.setRounded(what);
		else
			riskCessTerm.set(what);
	}	
	public PackedDecimalData getPremCessTerm() {
		return premCessTerm;
	}
	public void setPremCessTerm(Object what) {
		setPremCessTerm(what, false);
	}
	public void setPremCessTerm(Object what, boolean rounded) {
		if (rounded)
			premCessTerm.setRounded(what);
		else
			premCessTerm.set(what);
	}	
	public PackedDecimalData getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Object what) {
		setCrrcd(what, false);
	}
	public void setCrrcd(Object what, boolean rounded) {
		if (rounded)
			crrcd.setRounded(what);
		else
			crrcd.set(what);
	}	
	public PackedDecimalData getZlinstprem() {
		return zlinstprem;
	}
	public void setZlinstprem(Object what) {
		setZlinstprem(what, false);
	}
	public void setZlinstprem(Object what, boolean rounded) {
		if (rounded)
			zlinstprem.setRounded(what);
		else
			zlinstprem.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public PackedDecimalData getAextprm() {
		return aextprm;
	}
	public void setAextprm(Object what) {
		setAextprm(what, false);
	}
	public void setAextprm(Object what, boolean rounded) {
		if (rounded)
			aextprm.setRounded(what);
		else
			aextprm.set(what);
	}	
	public PackedDecimalData getAprem() {
		return aprem;
	}
	public void setAprem(Object what) {
		setAprem(what, false);
	}
	public void setAprem(Object what, boolean rounded) {
		if (rounded)
			aprem.setRounded(what);
		else
			aprem.set(what);
	}	
	public FixedLengthStringData getOpcda01() {
		return opcda01;
	}
	public void setOpcda01(Object what) {
		opcda01.set(what);
	}	
	public FixedLengthStringData getOpcda02() {
		return opcda02;
	}
	public void setOpcda02(Object what) {
		opcda02.set(what);
	}	
	public FixedLengthStringData getOpcda03() {
		return opcda03;
	}
	public void setOpcda03(Object what) {
		opcda03.set(what);
	}	
	public FixedLengthStringData getOpcda04() {
		return opcda04;
	}
	public void setOpcda04(Object what) {
		opcda04.set(what);
	}	
	public FixedLengthStringData getOpcda05() {
		return opcda05;
	}
	public void setOpcda05(Object what) {
		opcda05.set(what);
	}	
	public PackedDecimalData getOppc01() {
		return oppc01;
	}
	public void setOppc01(Object what) {
		setOppc01(what, false);
	}
	public void setOppc01(Object what, boolean rounded) {
		if (rounded)
			oppc01.setRounded(what);
		else
			oppc01.set(what);
	}	
	public PackedDecimalData getOppc02() {
		return oppc02;
	}
	public void setOppc02(Object what) {
		setOppc02(what, false);
	}
	public void setOppc02(Object what, boolean rounded) {
		if (rounded)
			oppc02.setRounded(what);
		else
			oppc02.set(what);
	}	
	public PackedDecimalData getOppc03() {
		return oppc03;
	}
	public void setOppc03(Object what) {
		setOppc03(what, false);
	}
	public void setOppc03(Object what, boolean rounded) {
		if (rounded)
			oppc03.setRounded(what);
		else
			oppc03.set(what);
	}	
	public PackedDecimalData getOppc04() {
		return oppc04;
	}
	public void setOppc04(Object what) {
		setOppc04(what, false);
	}
	public void setOppc04(Object what, boolean rounded) {
		if (rounded)
			oppc04.setRounded(what);
		else
			oppc04.set(what);
	}	
	public PackedDecimalData getOppc05() {
		return oppc05;
	}
	public void setOppc05(Object what) {
		setOppc05(what, false);
	}
	public void setOppc05(Object what, boolean rounded) {
		if (rounded)
			oppc05.setRounded(what);
		else
			oppc05.set(what);
	}	
	public PackedDecimalData getAgerate01() {
		return agerate01;
	}
	public void setAgerate01(Object what) {
		setAgerate01(what, false);
	}
	public void setAgerate01(Object what, boolean rounded) {
		if (rounded)
			agerate01.setRounded(what);
		else
			agerate01.set(what);
	}	
	public PackedDecimalData getAgerate02() {
		return agerate02;
	}
	public void setAgerate02(Object what) {
		setAgerate02(what, false);
	}
	public void setAgerate02(Object what, boolean rounded) {
		if (rounded)
			agerate02.setRounded(what);
		else
			agerate02.set(what);
	}	
	public PackedDecimalData getAgerate03() {
		return agerate03;
	}
	public void setAgerate03(Object what) {
		setAgerate03(what, false);
	}
	public void setAgerate03(Object what, boolean rounded) {
		if (rounded)
			agerate03.setRounded(what);
		else
			agerate03.set(what);
	}	
	public PackedDecimalData getAgerate04() {
		return agerate04;
	}
	public void setAgerate04(Object what) {
		setAgerate04(what, false);
	}
	public void setAgerate04(Object what, boolean rounded) {
		if (rounded)
			agerate04.setRounded(what);
		else
			agerate04.set(what);
	}	
	public PackedDecimalData getAgerate05() {
		return agerate05;
	}
	public void setAgerate05(Object what) {
		setAgerate05(what, false);
	}
	public void setAgerate05(Object what, boolean rounded) {
		if (rounded)
			agerate05.setRounded(what);
		else
			agerate05.set(what);
	}	
	public PackedDecimalData getInsprm01() {
		return insprm01;
	}
	public void setInsprm01(Object what) {
		setInsprm01(what, false);
	}
	public void setInsprm01(Object what, boolean rounded) {
		if (rounded)
			insprm01.setRounded(what);
		else
			insprm01.set(what);
	}	
	public PackedDecimalData getInsprm02() {
		return insprm02;
	}
	public void setInsprm02(Object what) {
		setInsprm02(what, false);
	}
	public void setInsprm02(Object what, boolean rounded) {
		if (rounded)
			insprm02.setRounded(what);
		else
			insprm02.set(what);
	}	
	public PackedDecimalData getInsprm03() {
		return insprm03;
	}
	public void setInsprm03(Object what) {
		setInsprm03(what, false);
	}
	public void setInsprm03(Object what, boolean rounded) {
		if (rounded)
			insprm03.setRounded(what);
		else
			insprm03.set(what);
	}	
	public PackedDecimalData getInsprm04() {
		return insprm04;
	}
	public void setInsprm04(Object what) {
		setInsprm04(what, false);
	}
	public void setInsprm04(Object what, boolean rounded) {
		if (rounded)
			insprm04.setRounded(what);
		else
			insprm04.set(what);
	}	
	public PackedDecimalData getInsprm05() {
		return insprm05;
	}
	public void setInsprm05(Object what) {
		setInsprm05(what, false);
	}
	public void setInsprm05(Object what, boolean rounded) {
		if (rounded)
			insprm05.setRounded(what);
		else
			insprm05.set(what);
	}	
	public PackedDecimalData getExtCessTerm01() {
		return extCessTerm01;
	}
	public void setExtCessTerm01(Object what) {
		setExtCessTerm01(what, false);
	}
	public void setExtCessTerm01(Object what, boolean rounded) {
		if (rounded)
			extCessTerm01.setRounded(what);
		else
			extCessTerm01.set(what);
	}	
	public PackedDecimalData getExtCessTerm02() {
		return extCessTerm02;
	}
	public void setExtCessTerm02(Object what) {
		setExtCessTerm02(what, false);
	}
	public void setExtCessTerm02(Object what, boolean rounded) {
		if (rounded)
			extCessTerm02.setRounded(what);
		else
			extCessTerm02.set(what);
	}	
	public PackedDecimalData getExtCessTerm03() {
		return extCessTerm03;
	}
	public void setExtCessTerm03(Object what) {
		setExtCessTerm03(what, false);
	}
	public void setExtCessTerm03(Object what, boolean rounded) {
		if (rounded)
			extCessTerm03.setRounded(what);
		else
			extCessTerm03.set(what);
	}	
	public PackedDecimalData getExtCessTerm04() {
		return extCessTerm04;
	}
	public void setExtCessTerm04(Object what) {
		setExtCessTerm04(what, false);
	}
	public void setExtCessTerm04(Object what, boolean rounded) {
		if (rounded)
			extCessTerm04.setRounded(what);
		else
			extCessTerm04.set(what);
	}	
	public PackedDecimalData getExtCessTerm05() {
		return extCessTerm05;
	}
	public void setExtCessTerm05(Object what) {
		setExtCessTerm05(what, false);
	}
	public void setExtCessTerm05(Object what, boolean rounded) {
		if (rounded)
			extCessTerm05.setRounded(what);
		else
			extCessTerm05.set(what);
	}	
	public FixedLengthStringData getUnitVirtualFund01() {
		return unitVirtualFund01;
	}
	public void setUnitVirtualFund01(Object what) {
		unitVirtualFund01.set(what);
	}	
	public FixedLengthStringData getUnitVirtualFund02() {
		return unitVirtualFund02;
	}
	public void setUnitVirtualFund02(Object what) {
		unitVirtualFund02.set(what);
	}	
	public FixedLengthStringData getUnitVirtualFund03() {
		return unitVirtualFund03;
	}
	public void setUnitVirtualFund03(Object what) {
		unitVirtualFund03.set(what);
	}	
	public FixedLengthStringData getUnitVirtualFund04() {
		return unitVirtualFund04;
	}
	public void setUnitVirtualFund04(Object what) {
		unitVirtualFund04.set(what);
	}	
	public FixedLengthStringData getUnitVirtualFund05() {
		return unitVirtualFund05;
	}
	public void setUnitVirtualFund05(Object what) {
		unitVirtualFund05.set(what);
	}	
	public PackedDecimalData getFundAmount01() {
		return fundAmount01;
	}
	public void setFundAmount01(Object what) {
		setFundAmount01(what, false);
	}
	public void setFundAmount01(Object what, boolean rounded) {
		if (rounded)
			fundAmount01.setRounded(what);
		else
			fundAmount01.set(what);
	}	
	public PackedDecimalData getFundAmount02() {
		return fundAmount02;
	}
	public void setFundAmount02(Object what) {
		setFundAmount02(what, false);
	}
	public void setFundAmount02(Object what, boolean rounded) {
		if (rounded)
			fundAmount02.setRounded(what);
		else
			fundAmount02.set(what);
	}	
	public PackedDecimalData getFundAmount03() {
		return fundAmount03;
	}
	public void setFundAmount03(Object what) {
		setFundAmount03(what, false);
	}
	public void setFundAmount03(Object what, boolean rounded) {
		if (rounded)
			fundAmount03.setRounded(what);
		else
			fundAmount03.set(what);
	}	
	public PackedDecimalData getFundAmount04() {
		return fundAmount04;
	}
	public void setFundAmount04(Object what) {
		setFundAmount04(what, false);
	}
	public void setFundAmount04(Object what, boolean rounded) {
		if (rounded)
			fundAmount04.setRounded(what);
		else
			fundAmount04.set(what);
	}	
	public PackedDecimalData getFundAmount05() {
		return fundAmount05;
	}
	public void setFundAmount05(Object what) {
		setFundAmount05(what, false);
	}
	public void setFundAmount05(Object what, boolean rounded) {
		if (rounded)
			fundAmount05.setRounded(what);
		else
			fundAmount05.set(what);
	}	
	public FixedLengthStringData getRgpymop01() {
		return rgpymop01;
	}
	public void setRgpymop01(Object what) {
		rgpymop01.set(what);
	}	
	public FixedLengthStringData getRgpymop02() {
		return rgpymop02;
	}
	public void setRgpymop02(Object what) {
		rgpymop02.set(what);
	}	
	public FixedLengthStringData getRgpymop03() {
		return rgpymop03;
	}
	public void setRgpymop03(Object what) {
		rgpymop03.set(what);
	}	
	public FixedLengthStringData getRgpymop04() {
		return rgpymop04;
	}
	public void setRgpymop04(Object what) {
		rgpymop04.set(what);
	}	
	public FixedLengthStringData getRgpymop05() {
		return rgpymop05;
	}
	public void setRgpymop05(Object what) {
		rgpymop05.set(what);
	}	
	public FixedLengthStringData getAnname01() {
		return anname01;
	}
	public void setAnname01(Object what) {
		anname01.set(what);
	}	
	public FixedLengthStringData getAnname02() {
		return anname02;
	}
	public void setAnname02(Object what) {
		anname02.set(what);
	}	
	public FixedLengthStringData getAnname03() {
		return anname03;
	}
	public void setAnname03(Object what) {
		anname03.set(what);
	}	
	public FixedLengthStringData getAnname04() {
		return anname04;
	}
	public void setAnname04(Object what) {
		anname04.set(what);
	}	
	public FixedLengthStringData getAnname05() {
		return anname05;
	}
	public void setAnname05(Object what) {
		anname05.set(what);
	}	
	public PackedDecimalData getAnnage01() {
		return annage01;
	}
	public void setAnnage01(Object what) {
		setAnnage01(what, false);
	}
	public void setAnnage01(Object what, boolean rounded) {
		if (rounded)
			annage01.setRounded(what);
		else
			annage01.set(what);
	}	
	public PackedDecimalData getAnnage02() {
		return annage02;
	}
	public void setAnnage02(Object what) {
		setAnnage02(what, false);
	}
	public void setAnnage02(Object what, boolean rounded) {
		if (rounded)
			annage02.setRounded(what);
		else
			annage02.set(what);
	}	
	public PackedDecimalData getAnnage03() {
		return annage03;
	}
	public void setAnnage03(Object what) {
		setAnnage03(what, false);
	}
	public void setAnnage03(Object what, boolean rounded) {
		if (rounded)
			annage03.setRounded(what);
		else
			annage03.set(what);
	}	
	public PackedDecimalData getAnnage04() {
		return annage04;
	}
	public void setAnnage04(Object what) {
		setAnnage04(what, false);
	}
	public void setAnnage04(Object what, boolean rounded) {
		if (rounded)
			annage04.setRounded(what);
		else
			annage04.set(what);
	}	
	public PackedDecimalData getAnnage05() {
		return annage05;
	}
	public void setAnnage05(Object what) {
		setAnnage05(what, false);
	}
	public void setAnnage05(Object what, boolean rounded) {
		if (rounded)
			annage05.setRounded(what);
		else
			annage05.set(what);
	}	
	public FixedLengthStringData getSanname01() {
		return sanname01;
	}
	public void setSanname01(Object what) {
		sanname01.set(what);
	}	
	public FixedLengthStringData getSanname02() {
		return sanname02;
	}
	public void setSanname02(Object what) {
		sanname02.set(what);
	}	
	public FixedLengthStringData getSanname03() {
		return sanname03;
	}
	public void setSanname03(Object what) {
		sanname03.set(what);
	}	
	public FixedLengthStringData getSanname04() {
		return sanname04;
	}
	public void setSanname04(Object what) {
		sanname04.set(what);
	}	
	public FixedLengthStringData getSanname05() {
		return sanname05;
	}
	public void setSanname05(Object what) {
		sanname05.set(what);
	}	
	public PackedDecimalData getSannage01() {
		return sannage01;
	}
	public void setSannage01(Object what) {
		setSannage01(what, false);
	}
	public void setSannage01(Object what, boolean rounded) {
		if (rounded)
			sannage01.setRounded(what);
		else
			sannage01.set(what);
	}	
	public PackedDecimalData getSannage02() {
		return sannage02;
	}
	public void setSannage02(Object what) {
		setSannage02(what, false);
	}
	public void setSannage02(Object what, boolean rounded) {
		if (rounded)
			sannage02.setRounded(what);
		else
			sannage02.set(what);
	}	
	public PackedDecimalData getSannage03() {
		return sannage03;
	}
	public void setSannage03(Object what) {
		setSannage03(what, false);
	}
	public void setSannage03(Object what, boolean rounded) {
		if (rounded)
			sannage03.setRounded(what);
		else
			sannage03.set(what);
	}	
	public PackedDecimalData getSannage04() {
		return sannage04;
	}
	public void setSannage04(Object what) {
		setSannage04(what, false);
	}
	public void setSannage04(Object what, boolean rounded) {
		if (rounded)
			sannage04.setRounded(what);
		else
			sannage04.set(what);
	}	
	public PackedDecimalData getSannage05() {
		return sannage05;
	}
	public void setSannage05(Object what) {
		setSannage05(what, false);
	}
	public void setSannage05(Object what, boolean rounded) {
		if (rounded)
			sannage05.setRounded(what);
		else
			sannage05.set(what);
	}	
	public PackedDecimalData getFirstPaydate01() {
		return firstPaydate01;
	}
	public void setFirstPaydate01(Object what) {
		setFirstPaydate01(what, false);
	}
	public void setFirstPaydate01(Object what, boolean rounded) {
		if (rounded)
			firstPaydate01.setRounded(what);
		else
			firstPaydate01.set(what);
	}	
	public PackedDecimalData getFirstPaydate02() {
		return firstPaydate02;
	}
	public void setFirstPaydate02(Object what) {
		setFirstPaydate02(what, false);
	}
	public void setFirstPaydate02(Object what, boolean rounded) {
		if (rounded)
			firstPaydate02.setRounded(what);
		else
			firstPaydate02.set(what);
	}	
	public PackedDecimalData getFirstPaydate03() {
		return firstPaydate03;
	}
	public void setFirstPaydate03(Object what) {
		setFirstPaydate03(what, false);
	}
	public void setFirstPaydate03(Object what, boolean rounded) {
		if (rounded)
			firstPaydate03.setRounded(what);
		else
			firstPaydate03.set(what);
	}	
	public PackedDecimalData getFirstPaydate04() {
		return firstPaydate04;
	}
	public void setFirstPaydate04(Object what) {
		setFirstPaydate04(what, false);
	}
	public void setFirstPaydate04(Object what, boolean rounded) {
		if (rounded)
			firstPaydate04.setRounded(what);
		else
			firstPaydate04.set(what);
	}	
	public PackedDecimalData getFirstPaydate05() {
		return firstPaydate05;
	}
	public void setFirstPaydate05(Object what) {
		setFirstPaydate05(what, false);
	}
	public void setFirstPaydate05(Object what, boolean rounded) {
		if (rounded)
			firstPaydate05.setRounded(what);
		else
			firstPaydate05.set(what);
	}	
	public PackedDecimalData getNextPaydate01() {
		return nextPaydate01;
	}
	public void setNextPaydate01(Object what) {
		setNextPaydate01(what, false);
	}
	public void setNextPaydate01(Object what, boolean rounded) {
		if (rounded)
			nextPaydate01.setRounded(what);
		else
			nextPaydate01.set(what);
	}	
	public PackedDecimalData getNextPaydate02() {
		return nextPaydate02;
	}
	public void setNextPaydate02(Object what) {
		setNextPaydate02(what, false);
	}
	public void setNextPaydate02(Object what, boolean rounded) {
		if (rounded)
			nextPaydate02.setRounded(what);
		else
			nextPaydate02.set(what);
	}	
	public PackedDecimalData getNextPaydate03() {
		return nextPaydate03;
	}
	public void setNextPaydate03(Object what) {
		setNextPaydate03(what, false);
	}
	public void setNextPaydate03(Object what, boolean rounded) {
		if (rounded)
			nextPaydate03.setRounded(what);
		else
			nextPaydate03.set(what);
	}	
	public PackedDecimalData getNextPaydate04() {
		return nextPaydate04;
	}
	public void setNextPaydate04(Object what) {
		setNextPaydate04(what, false);
	}
	public void setNextPaydate04(Object what, boolean rounded) {
		if (rounded)
			nextPaydate04.setRounded(what);
		else
			nextPaydate04.set(what);
	}	
	public PackedDecimalData getNextPaydate05() {
		return nextPaydate05;
	}
	public void setNextPaydate05(Object what) {
		setNextPaydate05(what, false);
	}
	public void setNextPaydate05(Object what, boolean rounded) {
		if (rounded)
			nextPaydate05.setRounded(what);
		else
			nextPaydate05.set(what);
	}	
	public PackedDecimalData getFinalPaydate01() {
		return finalPaydate01;
	}
	public void setFinalPaydate01(Object what) {
		setFinalPaydate01(what, false);
	}
	public void setFinalPaydate01(Object what, boolean rounded) {
		if (rounded)
			finalPaydate01.setRounded(what);
		else
			finalPaydate01.set(what);
	}	
	public PackedDecimalData getFinalPaydate02() {
		return finalPaydate02;
	}
	public void setFinalPaydate02(Object what) {
		setFinalPaydate02(what, false);
	}
	public void setFinalPaydate02(Object what, boolean rounded) {
		if (rounded)
			finalPaydate02.setRounded(what);
		else
			finalPaydate02.set(what);
	}	
	public PackedDecimalData getFinalPaydate03() {
		return finalPaydate03;
	}
	public void setFinalPaydate03(Object what) {
		setFinalPaydate03(what, false);
	}
	public void setFinalPaydate03(Object what, boolean rounded) {
		if (rounded)
			finalPaydate03.setRounded(what);
		else
			finalPaydate03.set(what);
	}	
	public PackedDecimalData getFinalPaydate04() {
		return finalPaydate04;
	}
	public void setFinalPaydate04(Object what) {
		setFinalPaydate04(what, false);
	}
	public void setFinalPaydate04(Object what, boolean rounded) {
		if (rounded)
			finalPaydate04.setRounded(what);
		else
			finalPaydate04.set(what);
	}	
	public PackedDecimalData getFinalPaydate05() {
		return finalPaydate05;
	}
	public void setFinalPaydate05(Object what) {
		setFinalPaydate05(what, false);
	}
	public void setFinalPaydate05(Object what, boolean rounded) {
		if (rounded)
			finalPaydate05.setRounded(what);
		else
			finalPaydate05.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getPymt01() {
		return pymt01;
	}
	public void setPymt01(Object what) {
		setPymt01(what, false);
	}
	public void setPymt01(Object what, boolean rounded) {
		if (rounded)
			pymt01.setRounded(what);
		else
			pymt01.set(what);
	}	
	public PackedDecimalData getPymt02() {
		return pymt02;
	}
	public void setPymt02(Object what) {
		setPymt02(what, false);
	}
	public void setPymt02(Object what, boolean rounded) {
		if (rounded)
			pymt02.setRounded(what);
		else
			pymt02.set(what);
	}	
	public PackedDecimalData getPymt03() {
		return pymt03;
	}
	public void setPymt03(Object what) {
		setPymt03(what, false);
	}
	public void setPymt03(Object what, boolean rounded) {
		if (rounded)
			pymt03.setRounded(what);
		else
			pymt03.set(what);
	}	
	public PackedDecimalData getPymt04() {
		return pymt04;
	}
	public void setPymt04(Object what) {
		setPymt04(what, false);
	}
	public void setPymt04(Object what, boolean rounded) {
		if (rounded)
			pymt04.setRounded(what);
		else
			pymt04.set(what);
	}	
	public PackedDecimalData getPymt05() {
		return pymt05;
	}
	public void setPymt05(Object what) {
		setPymt05(what, false);
	}
	public void setPymt05(Object what, boolean rounded) {
		if (rounded)
			pymt05.setRounded(what);
		else
			pymt05.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getVrtfnds() {
		return new FixedLengthStringData(unitVirtualFund01.toInternal()
										+ unitVirtualFund02.toInternal()
										+ unitVirtualFund03.toInternal()
										+ unitVirtualFund04.toInternal()
										+ unitVirtualFund05.toInternal());
	}
	public void setVrtfnds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getVrtfnds().getLength()).init(obj);
	
		what = ExternalData.chop(what, unitVirtualFund01);
		what = ExternalData.chop(what, unitVirtualFund02);
		what = ExternalData.chop(what, unitVirtualFund03);
		what = ExternalData.chop(what, unitVirtualFund04);
		what = ExternalData.chop(what, unitVirtualFund05);
	}
	public FixedLengthStringData getVrtfnd(BaseData indx) {
		return getVrtfnd(indx.toInt());
	}
	public FixedLengthStringData getVrtfnd(int indx) {

		switch (indx) {
			case 1 : return unitVirtualFund01;
			case 2 : return unitVirtualFund02;
			case 3 : return unitVirtualFund03;
			case 4 : return unitVirtualFund04;
			case 5 : return unitVirtualFund05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setVrtfnd(BaseData indx, Object what) {
		setVrtfnd(indx.toInt(), what);
	}
	public void setVrtfnd(int indx, Object what) {

		switch (indx) {
			case 1 : setUnitVirtualFund01(what);
					 break;
			case 2 : setUnitVirtualFund02(what);
					 break;
			case 3 : setUnitVirtualFund03(what);
					 break;
			case 4 : setUnitVirtualFund04(what);
					 break;
			case 5 : setUnitVirtualFund05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSannames() {
		return new FixedLengthStringData(sanname01.toInternal()
										+ sanname02.toInternal()
										+ sanname03.toInternal()
										+ sanname04.toInternal()
										+ sanname05.toInternal());
	}
	public void setSannames(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSannames().getLength()).init(obj);
	
		what = ExternalData.chop(what, sanname01);
		what = ExternalData.chop(what, sanname02);
		what = ExternalData.chop(what, sanname03);
		what = ExternalData.chop(what, sanname04);
		what = ExternalData.chop(what, sanname05);
	}
	public FixedLengthStringData getSanname(BaseData indx) {
		return getSanname(indx.toInt());
	}
	public FixedLengthStringData getSanname(int indx) {

		switch (indx) {
			case 1 : return sanname01;
			case 2 : return sanname02;
			case 3 : return sanname03;
			case 4 : return sanname04;
			case 5 : return sanname05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSanname(BaseData indx, Object what) {
		setSanname(indx.toInt(), what);
	}
	public void setSanname(int indx, Object what) {

		switch (indx) {
			case 1 : setSanname01(what);
					 break;
			case 2 : setSanname02(what);
					 break;
			case 3 : setSanname03(what);
					 break;
			case 4 : setSanname04(what);
					 break;
			case 5 : setSanname05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSannages() {
		return new FixedLengthStringData(sannage01.toInternal()
										+ sannage02.toInternal()
										+ sannage03.toInternal()
										+ sannage04.toInternal()
										+ sannage05.toInternal());
	}
	public void setSannages(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSannages().getLength()).init(obj);
	
		what = ExternalData.chop(what, sannage01);
		what = ExternalData.chop(what, sannage02);
		what = ExternalData.chop(what, sannage03);
		what = ExternalData.chop(what, sannage04);
		what = ExternalData.chop(what, sannage05);
	}
	public PackedDecimalData getSannage(BaseData indx) {
		return getSannage(indx.toInt());
	}
	public PackedDecimalData getSannage(int indx) {

		switch (indx) {
			case 1 : return sannage01;
			case 2 : return sannage02;
			case 3 : return sannage03;
			case 4 : return sannage04;
			case 5 : return sannage05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSannage(BaseData indx, Object what) {
		setSannage(indx, what, false);
	}
	public void setSannage(BaseData indx, Object what, boolean rounded) {
		setSannage(indx.toInt(), what, rounded);
	}
	public void setSannage(int indx, Object what) {
		setSannage(indx, what, false);
	}
	public void setSannage(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSannage01(what, rounded);
					 break;
			case 2 : setSannage02(what, rounded);
					 break;
			case 3 : setSannage03(what, rounded);
					 break;
			case 4 : setSannage04(what, rounded);
					 break;
			case 5 : setSannage05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getRgpymops() {
		return new FixedLengthStringData(rgpymop01.toInternal()
										+ rgpymop02.toInternal()
										+ rgpymop03.toInternal()
										+ rgpymop04.toInternal()
										+ rgpymop05.toInternal());
	}
	public void setRgpymops(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getRgpymops().getLength()).init(obj);
	
		what = ExternalData.chop(what, rgpymop01);
		what = ExternalData.chop(what, rgpymop02);
		what = ExternalData.chop(what, rgpymop03);
		what = ExternalData.chop(what, rgpymop04);
		what = ExternalData.chop(what, rgpymop05);
	}
	public FixedLengthStringData getRgpymop(BaseData indx) {
		return getRgpymop(indx.toInt());
	}
	public FixedLengthStringData getRgpymop(int indx) {

		switch (indx) {
			case 1 : return rgpymop01;
			case 2 : return rgpymop02;
			case 3 : return rgpymop03;
			case 4 : return rgpymop04;
			case 5 : return rgpymop05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setRgpymop(BaseData indx, Object what) {
		setRgpymop(indx.toInt(), what);
	}
	public void setRgpymop(int indx, Object what) {

		switch (indx) {
			case 1 : setRgpymop01(what);
					 break;
			case 2 : setRgpymop02(what);
					 break;
			case 3 : setRgpymop03(what);
					 break;
			case 4 : setRgpymop04(what);
					 break;
			case 5 : setRgpymop05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getPymts() {
		return new FixedLengthStringData(pymt01.toInternal()
										+ pymt02.toInternal()
										+ pymt03.toInternal()
										+ pymt04.toInternal()
										+ pymt05.toInternal());
	}
	public void setPymts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPymts().getLength()).init(obj);
	
		what = ExternalData.chop(what, pymt01);
		what = ExternalData.chop(what, pymt02);
		what = ExternalData.chop(what, pymt03);
		what = ExternalData.chop(what, pymt04);
		what = ExternalData.chop(what, pymt05);
	}
	public PackedDecimalData getPymt(BaseData indx) {
		return getPymt(indx.toInt());
	}
	public PackedDecimalData getPymt(int indx) {

		switch (indx) {
			case 1 : return pymt01;
			case 2 : return pymt02;
			case 3 : return pymt03;
			case 4 : return pymt04;
			case 5 : return pymt05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPymt(BaseData indx, Object what) {
		setPymt(indx, what, false);
	}
	public void setPymt(BaseData indx, Object what, boolean rounded) {
		setPymt(indx.toInt(), what, rounded);
	}
	public void setPymt(int indx, Object what) {
		setPymt(indx, what, false);
	}
	public void setPymt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setPymt01(what, rounded);
					 break;
			case 2 : setPymt02(what, rounded);
					 break;
			case 3 : setPymt03(what, rounded);
					 break;
			case 4 : setPymt04(what, rounded);
					 break;
			case 5 : setPymt05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getOppcs() {
		return new FixedLengthStringData(oppc01.toInternal()
										+ oppc02.toInternal()
										+ oppc03.toInternal()
										+ oppc04.toInternal()
										+ oppc05.toInternal());
	}
	public void setOppcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getOppcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, oppc01);
		what = ExternalData.chop(what, oppc02);
		what = ExternalData.chop(what, oppc03);
		what = ExternalData.chop(what, oppc04);
		what = ExternalData.chop(what, oppc05);
	}
	public PackedDecimalData getOppc(BaseData indx) {
		return getOppc(indx.toInt());
	}
	public PackedDecimalData getOppc(int indx) {

		switch (indx) {
			case 1 : return oppc01;
			case 2 : return oppc02;
			case 3 : return oppc03;
			case 4 : return oppc04;
			case 5 : return oppc05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setOppc(BaseData indx, Object what) {
		setOppc(indx, what, false);
	}
	public void setOppc(BaseData indx, Object what, boolean rounded) {
		setOppc(indx.toInt(), what, rounded);
	}
	public void setOppc(int indx, Object what) {
		setOppc(indx, what, false);
	}
	public void setOppc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setOppc01(what, rounded);
					 break;
			case 2 : setOppc02(what, rounded);
					 break;
			case 3 : setOppc03(what, rounded);
					 break;
			case 4 : setOppc04(what, rounded);
					 break;
			case 5 : setOppc05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getOpcdas() {
		return new FixedLengthStringData(opcda01.toInternal()
										+ opcda02.toInternal()
										+ opcda03.toInternal()
										+ opcda04.toInternal()
										+ opcda05.toInternal());
	}
	public void setOpcdas(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getOpcdas().getLength()).init(obj);
	
		what = ExternalData.chop(what, opcda01);
		what = ExternalData.chop(what, opcda02);
		what = ExternalData.chop(what, opcda03);
		what = ExternalData.chop(what, opcda04);
		what = ExternalData.chop(what, opcda05);
	}
	public FixedLengthStringData getOpcda(BaseData indx) {
		return getOpcda(indx.toInt());
	}
	public FixedLengthStringData getOpcda(int indx) {

		switch (indx) {
			case 1 : return opcda01;
			case 2 : return opcda02;
			case 3 : return opcda03;
			case 4 : return opcda04;
			case 5 : return opcda05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setOpcda(BaseData indx, Object what) {
		setOpcda(indx.toInt(), what);
	}
	public void setOpcda(int indx, Object what) {

		switch (indx) {
			case 1 : setOpcda01(what);
					 break;
			case 2 : setOpcda02(what);
					 break;
			case 3 : setOpcda03(what);
					 break;
			case 4 : setOpcda04(what);
					 break;
			case 5 : setOpcda05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getNpaydates() {
		return new FixedLengthStringData(nextPaydate01.toInternal()
										+ nextPaydate02.toInternal()
										+ nextPaydate03.toInternal()
										+ nextPaydate04.toInternal()
										+ nextPaydate05.toInternal());
	}
	public void setNpaydates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getNpaydates().getLength()).init(obj);
	
		what = ExternalData.chop(what, nextPaydate01);
		what = ExternalData.chop(what, nextPaydate02);
		what = ExternalData.chop(what, nextPaydate03);
		what = ExternalData.chop(what, nextPaydate04);
		what = ExternalData.chop(what, nextPaydate05);
	}
	public PackedDecimalData getNpaydate(BaseData indx) {
		return getNpaydate(indx.toInt());
	}
	public PackedDecimalData getNpaydate(int indx) {

		switch (indx) {
			case 1 : return nextPaydate01;
			case 2 : return nextPaydate02;
			case 3 : return nextPaydate03;
			case 4 : return nextPaydate04;
			case 5 : return nextPaydate05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setNpaydate(BaseData indx, Object what) {
		setNpaydate(indx, what, false);
	}
	public void setNpaydate(BaseData indx, Object what, boolean rounded) {
		setNpaydate(indx.toInt(), what, rounded);
	}
	public void setNpaydate(int indx, Object what) {
		setNpaydate(indx, what, false);
	}
	public void setNpaydate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setNextPaydate01(what, rounded);
					 break;
			case 2 : setNextPaydate02(what, rounded);
					 break;
			case 3 : setNextPaydate03(what, rounded);
					 break;
			case 4 : setNextPaydate04(what, rounded);
					 break;
			case 5 : setNextPaydate05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInsprms() {
		return new FixedLengthStringData(insprm01.toInternal()
										+ insprm02.toInternal()
										+ insprm03.toInternal()
										+ insprm04.toInternal()
										+ insprm05.toInternal());
	}
	public void setInsprms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInsprms().getLength()).init(obj);
	
		what = ExternalData.chop(what, insprm01);
		what = ExternalData.chop(what, insprm02);
		what = ExternalData.chop(what, insprm03);
		what = ExternalData.chop(what, insprm04);
		what = ExternalData.chop(what, insprm05);
	}
	public PackedDecimalData getInsprm(BaseData indx) {
		return getInsprm(indx.toInt());
	}
	public PackedDecimalData getInsprm(int indx) {

		switch (indx) {
			case 1 : return insprm01;
			case 2 : return insprm02;
			case 3 : return insprm03;
			case 4 : return insprm04;
			case 5 : return insprm05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInsprm(BaseData indx, Object what) {
		setInsprm(indx, what, false);
	}
	public void setInsprm(BaseData indx, Object what, boolean rounded) {
		setInsprm(indx.toInt(), what, rounded);
	}
	public void setInsprm(int indx, Object what) {
		setInsprm(indx, what, false);
	}
	public void setInsprm(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInsprm01(what, rounded);
					 break;
			case 2 : setInsprm02(what, rounded);
					 break;
			case 3 : setInsprm03(what, rounded);
					 break;
			case 4 : setInsprm04(what, rounded);
					 break;
			case 5 : setInsprm05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getFundamnts() {
		return new FixedLengthStringData(fundAmount01.toInternal()
										+ fundAmount02.toInternal()
										+ fundAmount03.toInternal()
										+ fundAmount04.toInternal()
										+ fundAmount05.toInternal());
	}
	public void setFundamnts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getFundamnts().getLength()).init(obj);
	
		what = ExternalData.chop(what, fundAmount01);
		what = ExternalData.chop(what, fundAmount02);
		what = ExternalData.chop(what, fundAmount03);
		what = ExternalData.chop(what, fundAmount04);
		what = ExternalData.chop(what, fundAmount05);
	}
	public PackedDecimalData getFundamnt(BaseData indx) {
		return getFundamnt(indx.toInt());
	}
	public PackedDecimalData getFundamnt(int indx) {

		switch (indx) {
			case 1 : return fundAmount01;
			case 2 : return fundAmount02;
			case 3 : return fundAmount03;
			case 4 : return fundAmount04;
			case 5 : return fundAmount05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setFundamnt(BaseData indx, Object what) {
		setFundamnt(indx, what, false);
	}
	public void setFundamnt(BaseData indx, Object what, boolean rounded) {
		setFundamnt(indx.toInt(), what, rounded);
	}
	public void setFundamnt(int indx, Object what) {
		setFundamnt(indx, what, false);
	}
	public void setFundamnt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setFundAmount01(what, rounded);
					 break;
			case 2 : setFundAmount02(what, rounded);
					 break;
			case 3 : setFundAmount03(what, rounded);
					 break;
			case 4 : setFundAmount04(what, rounded);
					 break;
			case 5 : setFundAmount05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getFpaydates() {
		return new FixedLengthStringData(firstPaydate01.toInternal()
										+ firstPaydate02.toInternal()
										+ firstPaydate03.toInternal()
										+ firstPaydate04.toInternal()
										+ firstPaydate05.toInternal());
	}
	public void setFpaydates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getFpaydates().getLength()).init(obj);
	
		what = ExternalData.chop(what, firstPaydate01);
		what = ExternalData.chop(what, firstPaydate02);
		what = ExternalData.chop(what, firstPaydate03);
		what = ExternalData.chop(what, firstPaydate04);
		what = ExternalData.chop(what, firstPaydate05);
	}
	public PackedDecimalData getFpaydate(BaseData indx) {
		return getFpaydate(indx.toInt());
	}
	public PackedDecimalData getFpaydate(int indx) {

		switch (indx) {
			case 1 : return firstPaydate01;
			case 2 : return firstPaydate02;
			case 3 : return firstPaydate03;
			case 4 : return firstPaydate04;
			case 5 : return firstPaydate05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setFpaydate(BaseData indx, Object what) {
		setFpaydate(indx, what, false);
	}
	public void setFpaydate(BaseData indx, Object what, boolean rounded) {
		setFpaydate(indx.toInt(), what, rounded);
	}
	public void setFpaydate(int indx, Object what) {
		setFpaydate(indx, what, false);
	}
	public void setFpaydate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setFirstPaydate01(what, rounded);
					 break;
			case 2 : setFirstPaydate02(what, rounded);
					 break;
			case 3 : setFirstPaydate03(what, rounded);
					 break;
			case 4 : setFirstPaydate04(what, rounded);
					 break;
			case 5 : setFirstPaydate05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getEpaydates() {
		return new FixedLengthStringData(finalPaydate01.toInternal()
										+ finalPaydate02.toInternal()
										+ finalPaydate03.toInternal()
										+ finalPaydate04.toInternal()
										+ finalPaydate05.toInternal());
	}
	public void setEpaydates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getEpaydates().getLength()).init(obj);
	
		what = ExternalData.chop(what, finalPaydate01);
		what = ExternalData.chop(what, finalPaydate02);
		what = ExternalData.chop(what, finalPaydate03);
		what = ExternalData.chop(what, finalPaydate04);
		what = ExternalData.chop(what, finalPaydate05);
	}
	public PackedDecimalData getEpaydate(BaseData indx) {
		return getEpaydate(indx.toInt());
	}
	public PackedDecimalData getEpaydate(int indx) {

		switch (indx) {
			case 1 : return finalPaydate01;
			case 2 : return finalPaydate02;
			case 3 : return finalPaydate03;
			case 4 : return finalPaydate04;
			case 5 : return finalPaydate05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setEpaydate(BaseData indx, Object what) {
		setEpaydate(indx, what, false);
	}
	public void setEpaydate(BaseData indx, Object what, boolean rounded) {
		setEpaydate(indx.toInt(), what, rounded);
	}
	public void setEpaydate(int indx, Object what) {
		setEpaydate(indx, what, false);
	}
	public void setEpaydate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setFinalPaydate01(what, rounded);
					 break;
			case 2 : setFinalPaydate02(what, rounded);
					 break;
			case 3 : setFinalPaydate03(what, rounded);
					 break;
			case 4 : setFinalPaydate04(what, rounded);
					 break;
			case 5 : setFinalPaydate05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getEcestrms() {
		return new FixedLengthStringData(extCessTerm01.toInternal()
										+ extCessTerm02.toInternal()
										+ extCessTerm03.toInternal()
										+ extCessTerm04.toInternal()
										+ extCessTerm05.toInternal());
	}
	public void setEcestrms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getEcestrms().getLength()).init(obj);
	
		what = ExternalData.chop(what, extCessTerm01);
		what = ExternalData.chop(what, extCessTerm02);
		what = ExternalData.chop(what, extCessTerm03);
		what = ExternalData.chop(what, extCessTerm04);
		what = ExternalData.chop(what, extCessTerm05);
	}
	public PackedDecimalData getEcestrm(BaseData indx) {
		return getEcestrm(indx.toInt());
	}
	public PackedDecimalData getEcestrm(int indx) {

		switch (indx) {
			case 1 : return extCessTerm01;
			case 2 : return extCessTerm02;
			case 3 : return extCessTerm03;
			case 4 : return extCessTerm04;
			case 5 : return extCessTerm05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setEcestrm(BaseData indx, Object what) {
		setEcestrm(indx, what, false);
	}
	public void setEcestrm(BaseData indx, Object what, boolean rounded) {
		setEcestrm(indx.toInt(), what, rounded);
	}
	public void setEcestrm(int indx, Object what) {
		setEcestrm(indx, what, false);
	}
	public void setEcestrm(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setExtCessTerm01(what, rounded);
					 break;
			case 2 : setExtCessTerm02(what, rounded);
					 break;
			case 3 : setExtCessTerm03(what, rounded);
					 break;
			case 4 : setExtCessTerm04(what, rounded);
					 break;
			case 5 : setExtCessTerm05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAnnames() {
		return new FixedLengthStringData(anname01.toInternal()
										+ anname02.toInternal()
										+ anname03.toInternal()
										+ anname04.toInternal()
										+ anname05.toInternal());
	}
	public void setAnnames(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnnames().getLength()).init(obj);
	
		what = ExternalData.chop(what, anname01);
		what = ExternalData.chop(what, anname02);
		what = ExternalData.chop(what, anname03);
		what = ExternalData.chop(what, anname04);
		what = ExternalData.chop(what, anname05);
	}
	public FixedLengthStringData getAnname(BaseData indx) {
		return getAnname(indx.toInt());
	}
	public FixedLengthStringData getAnname(int indx) {

		switch (indx) {
			case 1 : return anname01;
			case 2 : return anname02;
			case 3 : return anname03;
			case 4 : return anname04;
			case 5 : return anname05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnname(BaseData indx, Object what) {
		setAnname(indx.toInt(), what);
	}
	public void setAnname(int indx, Object what) {

		switch (indx) {
			case 1 : setAnname01(what);
					 break;
			case 2 : setAnname02(what);
					 break;
			case 3 : setAnname03(what);
					 break;
			case 4 : setAnname04(what);
					 break;
			case 5 : setAnname05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAnnages() {
		return new FixedLengthStringData(annage01.toInternal()
										+ annage02.toInternal()
										+ annage03.toInternal()
										+ annage04.toInternal()
										+ annage05.toInternal());
	}
	public void setAnnages(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnnages().getLength()).init(obj);
	
		what = ExternalData.chop(what, annage01);
		what = ExternalData.chop(what, annage02);
		what = ExternalData.chop(what, annage03);
		what = ExternalData.chop(what, annage04);
		what = ExternalData.chop(what, annage05);
	}
	public PackedDecimalData getAnnage(BaseData indx) {
		return getAnnage(indx.toInt());
	}
	public PackedDecimalData getAnnage(int indx) {

		switch (indx) {
			case 1 : return annage01;
			case 2 : return annage02;
			case 3 : return annage03;
			case 4 : return annage04;
			case 5 : return annage05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnnage(BaseData indx, Object what) {
		setAnnage(indx, what, false);
	}
	public void setAnnage(BaseData indx, Object what, boolean rounded) {
		setAnnage(indx.toInt(), what, rounded);
	}
	public void setAnnage(int indx, Object what) {
		setAnnage(indx, what, false);
	}
	public void setAnnage(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnnage01(what, rounded);
					 break;
			case 2 : setAnnage02(what, rounded);
					 break;
			case 3 : setAnnage03(what, rounded);
					 break;
			case 4 : setAnnage04(what, rounded);
					 break;
			case 5 : setAnnage05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAgerates() {
		return new FixedLengthStringData(agerate01.toInternal()
										+ agerate02.toInternal()
										+ agerate03.toInternal()
										+ agerate04.toInternal()
										+ agerate05.toInternal());
	}
	public void setAgerates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAgerates().getLength()).init(obj);
	
		what = ExternalData.chop(what, agerate01);
		what = ExternalData.chop(what, agerate02);
		what = ExternalData.chop(what, agerate03);
		what = ExternalData.chop(what, agerate04);
		what = ExternalData.chop(what, agerate05);
	}
	public PackedDecimalData getAgerate(BaseData indx) {
		return getAgerate(indx.toInt());
	}
	public PackedDecimalData getAgerate(int indx) {

		switch (indx) {
			case 1 : return agerate01;
			case 2 : return agerate02;
			case 3 : return agerate03;
			case 4 : return agerate04;
			case 5 : return agerate05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAgerate(BaseData indx, Object what) {
		setAgerate(indx, what, false);
	}
	public void setAgerate(BaseData indx, Object what, boolean rounded) {
		setAgerate(indx.toInt(), what, rounded);
	}
	public void setAgerate(int indx, Object what) {
		setAgerate(indx, what, false);
	}
	public void setAgerate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAgerate01(what, rounded);
					 break;
			case 2 : setAgerate02(what, rounded);
					 break;
			case 3 : setAgerate03(what, rounded);
					 break;
			case 4 : setAgerate04(what, rounded);
					 break;
			case 5 : setAgerate05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		effdate.clear();
		currfrom.clear();
		validflag.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		crtable.clear();
		sumins.clear();
		riskCessTerm.clear();
		premCessTerm.clear();
		crrcd.clear();
		zlinstprem.clear();
		instprem.clear();
		aextprm.clear();
		aprem.clear();
		opcda01.clear();
		opcda02.clear();
		opcda03.clear();
		opcda04.clear();
		opcda05.clear();
		oppc01.clear();
		oppc02.clear();
		oppc03.clear();
		oppc04.clear();
		oppc05.clear();
		agerate01.clear();
		agerate02.clear();
		agerate03.clear();
		agerate04.clear();
		agerate05.clear();
		insprm01.clear();
		insprm02.clear();
		insprm03.clear();
		insprm04.clear();
		insprm05.clear();
		extCessTerm01.clear();
		extCessTerm02.clear();
		extCessTerm03.clear();
		extCessTerm04.clear();
		extCessTerm05.clear();
		unitVirtualFund01.clear();
		unitVirtualFund02.clear();
		unitVirtualFund03.clear();
		unitVirtualFund04.clear();
		unitVirtualFund05.clear();
		fundAmount01.clear();
		fundAmount02.clear();
		fundAmount03.clear();
		fundAmount04.clear();
		fundAmount05.clear();
		rgpymop01.clear();
		rgpymop02.clear();
		rgpymop03.clear();
		rgpymop04.clear();
		rgpymop05.clear();
		anname01.clear();
		anname02.clear();
		anname03.clear();
		anname04.clear();
		anname05.clear();
		annage01.clear();
		annage02.clear();
		annage03.clear();
		annage04.clear();
		annage05.clear();
		sanname01.clear();
		sanname02.clear();
		sanname03.clear();
		sanname04.clear();
		sanname05.clear();
		sannage01.clear();
		sannage02.clear();
		sannage03.clear();
		sannage04.clear();
		sannage05.clear();
		firstPaydate01.clear();
		firstPaydate02.clear();
		firstPaydate03.clear();
		firstPaydate04.clear();
		firstPaydate05.clear();
		nextPaydate01.clear();
		nextPaydate02.clear();
		nextPaydate03.clear();
		nextPaydate04.clear();
		nextPaydate05.clear();
		finalPaydate01.clear();
		finalPaydate02.clear();
		finalPaydate03.clear();
		finalPaydate04.clear();
		finalPaydate05.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();
		pymt01.clear();
		pymt02.clear();
		pymt03.clear();
		pymt04.clear();
		pymt05.clear();		
	}


}