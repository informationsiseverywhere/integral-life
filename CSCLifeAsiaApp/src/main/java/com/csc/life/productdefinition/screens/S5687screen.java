package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5687screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5687ScreenVars sv = (S5687ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5687screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5687ScreenVars screenVars = (S5687ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.premmeth.setClassString("");
		screenVars.jlPremMeth.setClassString("");
		screenVars.jlifePresent.setClassString("");
		screenVars.premGuarPeriod.setClassString("");
		screenVars.rtrnwfreq.setClassString("");
		screenVars.singlePremInd.setClassString("");
		screenVars.stampDutyMeth.setClassString("");
		screenVars.sdtyPayMeth.setClassString("");
		screenVars.zsredtrm.setClassString("");
		screenVars.bbmeth.setClassString("");
		screenVars.nonForfeitMethod.setClassString("");
		screenVars.riind.setClassString("");
		screenVars.anniversaryMethod.setClassString("");
		screenVars.partsurr.setClassString("");
		screenVars.basicCommMeth.setClassString("");
		screenVars.basscmth.setClassString("");
		screenVars.bastcmth.setClassString("");
		screenVars.bascpy.setClassString("");
		screenVars.basscpy.setClassString("");
		screenVars.bastcpy.setClassString("");
		screenVars.srvcpy.setClassString("");
		screenVars.rnwcpy.setClassString("");
		screenVars.defFupMeth.setClassString("");
		screenVars.schedCode.setClassString("");
		screenVars.zszprmcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubSect.setClassString("");
		screenVars.reptcd01.setClassString("");
		screenVars.reptcd02.setClassString("");
		screenVars.reptcd03.setClassString("");
		screenVars.reptcd04.setClassString("");
		screenVars.reptcd05.setClassString("");
		screenVars.reptcd06.setClassString("");
		screenVars.maturityCalcMeth.setClassString("");
		screenVars.svMethod.setClassString("");
		screenVars.dcmeth.setClassString("");
		screenVars.pumeth.setClassString("");
		screenVars.xfreqAltBasis.setClassString("");
		screenVars.loanmeth.setClassString("");
		screenVars.zrorcmrg.setClassString("");
		screenVars.zrorpmrg.setClassString("");
		screenVars.zrorcmsp.setClassString("");
		screenVars.zrorpmsp.setClassString("");
		screenVars.zrorcmtu.setClassString("");
		screenVars.zrorpmtu.setClassString("");
		screenVars.zrrcombas.setClassString("");
		screenVars.zsbsmeth.setClassString("");
		screenVars.lnkgind.setClassString("");
	}

/**
 * Clear all the variables in S5687screen
 */
	public static void clear(VarModel pv) {
		S5687ScreenVars screenVars = (S5687ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.premmeth.clear();
		screenVars.jlPremMeth.clear();
		screenVars.jlifePresent.clear();
		screenVars.premGuarPeriod.clear();
		screenVars.rtrnwfreq.clear();
		screenVars.singlePremInd.clear();
		screenVars.stampDutyMeth.clear();
		screenVars.sdtyPayMeth.clear();
		screenVars.zsredtrm.clear();
		screenVars.bbmeth.clear();
		screenVars.nonForfeitMethod.clear();
		screenVars.riind.clear();
		screenVars.anniversaryMethod.clear();
		screenVars.partsurr.clear();
		screenVars.basicCommMeth.clear();
		screenVars.basscmth.clear();
		screenVars.bastcmth.clear();
		screenVars.bascpy.clear();
		screenVars.basscpy.clear();
		screenVars.bastcpy.clear();
		screenVars.srvcpy.clear();
		screenVars.rnwcpy.clear();
		screenVars.defFupMeth.clear();
		screenVars.schedCode.clear();
		screenVars.zszprmcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubSect.clear();
		screenVars.reptcd01.clear();
		screenVars.reptcd02.clear();
		screenVars.reptcd03.clear();
		screenVars.reptcd04.clear();
		screenVars.reptcd05.clear();
		screenVars.reptcd06.clear();
		screenVars.maturityCalcMeth.clear();
		screenVars.svMethod.clear();
		screenVars.dcmeth.clear();
		screenVars.pumeth.clear();
		screenVars.xfreqAltBasis.clear();
		screenVars.loanmeth.clear();
		screenVars.zrorcmrg.clear();
		screenVars.zrorpmrg.clear();
		screenVars.zrorcmsp.clear();
		screenVars.zrorpmsp.clear();
		screenVars.zrorcmtu.clear();
		screenVars.zrorpmtu.clear();
		screenVars.zrrcombas.clear();
		screenVars.zsbsmeth.clear();
		screenVars.lnkgind.clear();
	}
}
