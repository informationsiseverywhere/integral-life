package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:41
 * Description:
 * Copybook name: ACMVBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvbrkKey = new FixedLengthStringData(64).isAPartOf(acmvbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvbrkRldgcoy = new FixedLengthStringData(1).isAPartOf(acmvbrkKey, 0);
  	public FixedLengthStringData acmvbrkRldgacct = new FixedLengthStringData(16).isAPartOf(acmvbrkKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(acmvbrkKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}