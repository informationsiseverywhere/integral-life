/*
 * File: Prmpm03.java
 * Date: 30 August 2009 1:58:18
 * Author: Quipoz Limited
 * 
 * Class transformed from PRMPM03.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5533rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*          PREMIUM CALCULATION METHOD 03
*
* This method will be used for calculating a sum insured from
* the premium presented in the linkage.
*
* Check the premium against limits from T5533 (keyed on coverage/
* rider currency). The table is looked-up by frequency to
* determine the range limits that are used, the premium should
* be within the min/max range. If not, and this program is not
* call by a INCREASE module then report as an error.
*
* Obtain the age of the youngest life(s).
*
*         Read the life details using 'LIFELNB' (the life number
*         is passed in the linkage, joint-life number will be
*         '00'). For joint-life details the joint-life number
*         will be '01'.
*
*
*         If it is a joint life case, then take the younger age.
*
* Calculate total premium.
*
* The total premium is calculated by either of the following
* methods, whichever is the earliest:
*
*         - 'table' age or
*         - premium cessation date.
*         - access table T5646, if there is a table age present,
*           process as follows:
*
*           - calculate the youngest life's birthday date at the
*             "table age" (DATCON2 with DoB, age, frequency 01).
*             This will give a date, which is then compared
*             against the premium cessation date.
*
*             The earlier date date is selected and this
*             date is then used to calculate the total premiums
*             to be paid (see below).
*
*         - if the table age entry is blank:
*
*           - calculate the number of instalments to be paid
*             by calling DATCON3 with the Risk Commencement Date
*             and the Premium cessation date with the contract
*             billing frequency (round up to nearest).
*
* Determine sum assured factor
*
*         - from table T5646 use the ANB @ RCD for the youngest
*           life (if a joint-life case) to look up a 'from' and
*           'to' age range. A factor will be associated with
*           each range, store the appropriate range factor.
*
* Calculate sum assured.
*
*         - multiply the calculated total premium by the factor
*           and round up to the next unit, the rounding factor
*           is supplied on table T5533 (the key is coverage/
*           rider and currency). For rounding details see
*           PRMPM01.
*
*****************************************************************
* </pre>
*/
public class Prmpm03 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PRMPM03";

	private FixedLengthStringData wsaaT5533Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5533Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 0);
	private FixedLengthStringData wsaaT5533Currcode = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 4);
	private PackedDecimalData wsaaAge00 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaAge01 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaDob00 = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaDob01 = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaYoungerLife = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaYoungerDob = new PackedDecimalData(8, 0).init(ZERO);
	private ZonedDecimalData wsaaRndfact = new ZonedDecimalData(6, 0);
	private PackedDecimalData wsaaTotPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(7, 4);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(6, 0).init(ZERO);
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1000 = new ZonedDecimalData(6, 2).isAPartOf(filler2, 7).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 12).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler6, 13).setUnsigned();

	private FixedLengthStringData filler8 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler8, 14).setUnsigned();

	private FixedLengthStringData filler10 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler10, 15);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaDateToUse = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
		/* ERRORS */
	private static final String g070 = "G070";
	private static final String g071 = "G071";
	private static final String g072 = "G072";
	private static final String h068 = "H068";
	private static final String t041 = "T041";
		/* TABLES */
	private static final String t5646 = "T5646";
	private static final String t5533 = "T5533";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5533rec t5533rec = new T5533rec();
	private T5646rec t5646rec = new T5646rec();
	private Premiumrec premiumrec = new Premiumrec();

	public Prmpm03() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*INIT*/
		premiumrec.statuz.set("****");
		syserrrec.subrname.set(wsaaSubr);
		main100();
		/*EXIT*/
		exitProgram();
	}

protected void main100()
	{
		para100();
	}

protected void para100()
	{
		initialize200();
		if (isEQ(premiumrec.statuz, "****")) {
			readTablT5533250();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			readLifeDetails300();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			readTablT5646400();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			premiumMethods500();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			determineFactor600();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			suminRounded700();
		}
	}

protected void initialize200()
	{
		/*PARA*/
		wsaaYoungerLife.set(ZERO);
		wsaaFactor.set(ZERO);
		wsaaTotPremium.set(ZERO);
		wsaaAge00.set(ZERO);
		wsaaAge01.set(ZERO);
		wsaaDob00.set(ZERO);
		wsaaDob01.set(ZERO);
		wsaaDateToUse.set(ZERO);
		wsaaRndfact.set(ZERO);
		wsaaSub.set(ZERO);
		/*EXIT*/
	}

protected void readTablT5533250()
	{
		para250();
	}

protected void para250()
	{
		/*    CHECK PREMIUM AGAINST THE LIMITS SET IN T5533 ACCORDING TO*/
		/*    THE RANGE LIMITS FOR BILLING FREQUENCY.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5533);
		wsaaT5533Crtable.set(premiumrec.crtable);
		wsaaT5533Currcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5533Item);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT5533Item, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5533)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			premiumrec.statuz.set(g071);
			return ;
		}
		else {
			t5533rec.t5533Rec.set(itdmIO.getGenarea());
		}
		/*    Process to find MAX/MIN limits for frequency required.*/
		wsaaRndfact.set(t5533rec.rndfact);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 8)
		|| isEQ(premiumrec.billfreq, t5533rec.frequency[wsaaSub.toInt()])); wsaaSub.add(1)){
			findBillfreq270();
		}
		/*    If a range is not found or premium is out with the range*/
		/*    then error.*/
		/*    If this program is called by a INCREASE module, skip this    */
		/*    test because the CPRM-CALC-PREM holds the increased part of  */
		/*    the premium and not the actual premium.                      */
		if (isGT(wsaaSub, 8)) {
			premiumrec.statuz.set(g072);
		}
		else {
			if (((isLT(premiumrec.calcPrem, t5533rec.cmin[wsaaSub.toInt()]))
			|| (isGT(premiumrec.calcPrem, t5533rec.cmax[wsaaSub.toInt()])))
			&& isNE(premiumrec.function, "INCR")) {
				premiumrec.statuz.set(g070);
			}
		}
	}

protected void findBillfreq270()
	{
		/*READ*/
		/**    Dummy paragraph to find billing frequency.*/
		/*EXIT*/
	}

protected void readLifeDetails300()
	{
		read310();
	}

protected void read310()
	{
		/*    Obtain jointlife and life date of birth*/
		/*    from the LIFELNB file.*/
		/*    Read the first life.*/
		lifelnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lifelnbIO.setChdrnum(premiumrec.chdrChdrnum);
		lifelnbIO.setLife(premiumrec.lifeLife);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), "****")) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError9000();
		}
		wsaaAge00.set(premiumrec.lage);
		wsaaDob00.set(lifelnbIO.getCltdob());
		/*    Ascertain if joint life present*/
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), "****")
		&& isNE(lifelnbIO.getStatuz(), "MRNF")) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError9000();
		}
		if (isEQ(lifelnbIO.getStatuz(), "****")) {
			/*    MOVE CPRM-LAGE           TO WSAA-AGE-01                   */
			wsaaAge01.set(premiumrec.jlage);
			wsaaDob01.set(lifelnbIO.getCltdob());
		}
		/*    Store the younger of the life and joint life ages(when joint*/
		/*    life present) or the main life age if joint life not present*/
		if (isLT(wsaaAge01, wsaaAge00)
		&& isNE(wsaaAge01, ZERO)) {
			wsaaYoungerLife.set(wsaaAge01);
			wsaaYoungerDob.set(wsaaDob01);
		}
		else {
			wsaaYoungerDob.set(wsaaDob00);
			wsaaYoungerLife.set(wsaaAge00);
		}
	}

protected void readTablT5646400()
	{
		para410();
	}

protected void para410()
	{
		/*    Read table T5646 to see if there is an age limit present.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5646);
		itdmIO.setItemitem(premiumrec.crtable);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(premiumrec.crtable, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5646)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			/*     MOVE U036                TO CPRM-STATUZ                   */
			premiumrec.statuz.set(h068);
		}
		else {
			t5646rec.t5646Rec.set(itdmIO.getGenarea());
		}
	}

protected void premiumMethods500()
	{
		para510();
	}

protected void para510()
	{
		/*    Age limit not present in table T5646 so use the premium*/
		/*    cessation date from linkage area for further calculations.*/
		if (isEQ(t5646rec.agelimit, ZERO)
		|| isEQ(premiumrec.billfreq, "00")) {
			wsaaDateToUse.set(premiumrec.termdate);
			calcNumInstalments550();
			return ;
		}
		/*    Age limit present - calculate younger life's birthday*/
		/*    (obtained from life file) at age limit. Compare this date*/
		/*    with the premium cessation date and use the lower of these*/
		/*    for further calculations*/
		/*    MOVE CPRM-TERMDATE          TO DTC2-INT-DATE-1.              */
		datcon2rec.intDate1.set(wsaaYoungerDob);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t5646rec.agelimit);
		datcon2rec.function.set(SPACES);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		/*  IF DTC2-STATUZ              = BOMB                           */
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError9000();
		}
		/*    We now have the Clients birthday date at the agelimit        */
		/*    taken from T5646 (stored in WSAA-DATE-TO-USE)                */
		wsaaDateToUse.set(datcon2rec.intDate2);
		/*    MULTIPLY WSAA-YOUNGER-LIFE  BY -1 GIVING WSAA-ANB-RCD.       */
		/*    Check that the Date to use is not > Risk Comm Date           */
		/*    if so display error message 'T5646 Age Limit < ANB'          */
		if (isLT(wsaaDateToUse, premiumrec.effectdt)) {
			premiumrec.statuz.set(t041);
			return ;
		}
		/*    MOVE WSAA-DATE-TO-USE       TO DTC2-INT-DATE-1.              */
		/*    MOVE '01'                   TO DTC2-FREQUENCY.               */
		/*    MOVE WSAA-ANB-RCD           TO DTC2-FREQ-FACTOR.             */
		/*    MOVE SPACES                 TO DTC2-FUNCTION.                */
		/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.          */
		/*  IF DTC2-STATUZ              = BOMB                           */
		/*    IF DTC2-STATUZ          NOT = O-K                       <005>*/
		/*       MOVE DTC2-DATCON2-REC    TO SYSR-PARAMS                   */
		/*       MOVE DTC2-STATUZ         TO SYSR-STATUZ              <005>*/
		/*       PERFORM 9000-FATAL-ERROR.                                 */
		/*    Take the earliest date.*/
		/*    IF CPRM-TERMDATE            < DTC2-INT-DATE-2                */
		if (isLT(premiumrec.termdate, wsaaDateToUse)) {
			wsaaDateToUse.set(premiumrec.termdate);
		}
		/*    ELSE                                                         */
		/*       MOVE DTC2-INT-DATE-2     TO WSAA-DATE-TO-USE.             */
		calcNumInstalments550();
	}

protected void calcNumInstalments550()
	{
		para560();
	}

protected void para560()
	{
		/*    Calculate no of instalments to be paid by calling DATCON3*/
		/*    passing the premium cessation date, the risk commencement*/
		/*    date (or date calculated ) and the billing frequency.*/
		/* For Single Premium cases no Freq Factor needs to be applied.   */
		if (isEQ(premiumrec.billfreq, "00")) {
			wsaaTotPremium.set(premiumrec.calcPrem);
			return ;
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(wsaaDateToUse);
		datcon3rec.frequency.set(premiumrec.billfreq);
		/*    MOVE 'CMDF'                 TO DTC3-FUNCTION.*/
		datcon3rec.function.set(SPACES);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		/*  IF DTC3-STATUZ              = BOMB                           */
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		/* Round up the frequency factor to the higher whole unit.*/
		compute(wsaaFreqFactor, 5).set((add(datcon3rec.freqFactor, 0.99999)));
		compute(wsaaTotPremium, 2).set((mult(premiumrec.calcPrem, wsaaFreqFactor)));
	}

protected void determineFactor600()
	{
		/*PARA*/
		/*    Find the sum assured factor which will determine the sum*/
		/*    assured according to the Premium.*/
		wsaaSub.set(0);
		findFactor650();
		if (isGT(wsaaSub, 12)) {
			/*     MOVE U036                TO CPRM-STATUZ                   */
			premiumrec.statuz.set(h068);
		}
		else {
			wsaaFactor.set(t5646rec.factorsa[wsaaSub.toInt()]);
		}
		/*EXIT*/
	}

protected void findFactor650()
	{
		para660();
	}

protected void para660()
	{
		/*    Routine to search through table T5646 to find factor accord-*/
		/*    to the age input.*/
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 12)) {
			/*       IF WSAA-YOUNGER-LIFE     NOT < T5646-AGFRM(WSAA-SUB)      */
			/*                            AND NOT > T5646-AGTO(WSAA-SUB)       */
			if (isGTE(wsaaYoungerLife, t5646rec.ageIssageFrm[wsaaSub.toInt()])
			&& isLTE(wsaaYoungerLife, t5646rec.ageIssageTo[wsaaSub.toInt()])) {
				/*NEXT_SENTENCE*/
			}
			else {
				/*     ADD 1 TO WSAA-SUB                                      */
				para660();
				return ;
			}
		}
		/*EXIT*/
	}

protected void suminRounded700()
	{
		para710();
	}

protected void para710()
	{
		/*    Multiply the tot premium over the term by the factor found.*/
		compute(wsaaTotPremium, 4).set((mult(wsaaTotPremium, wsaaFactor)));
		/*    Round up the Sum insured amount to its nearest whole unit.*/
		wsaaRoundNum.set(wsaaTotPremium);
		if (isEQ(wsaaRndfact, 1)
		|| isEQ(wsaaRndfact, 0)) {
			if (isLT(wsaaRoundDec, .5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
			}
		}
		if (isEQ(wsaaRndfact, 10)) {
			if (isLT(wsaaRound1, 5)) {
				wsaaRound1.set(0);
			}
			else {
				wsaaRoundNum.add(10);
				wsaaRound1.set(0);
			}
		}
		if (isEQ(wsaaRndfact, 100)) {
			if (isLT(wsaaRound10, 50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(wsaaRndfact, 1000)) {
			if (isLT(wsaaRound100, 500)) {
				wsaaRound100.set(0);
			}
			else {
				/*          ADD 100               TO WSAA-ROUND-NUM                */
				wsaaRoundNum.add(1000);
				wsaaRound100.set(0);
			}
		}
		if (isEQ(wsaaRndfact, 10000)) {
			if (isLT(wsaaRound1000, 5000)) {
				wsaaRound1000.set(0);
			}
			else {
				wsaaRoundNum.add(10000);
				wsaaRound1000.set(0);
			}
		}
		/*    Return the Sum insured amount to the Linkage area.*/
		premiumrec.sumin.set(wsaaRoundNum);
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		/*    System error.*/
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
