package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 30 July 2012 03:09:00
 * Description:
 * Copybook name: VPXCHDRREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxchdrrec extends ExternalData {

  
  	public FixedLengthStringData vpxchdrRec = new FixedLengthStringData(167);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxchdrRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxchdrRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxchdrRec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxchdrRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(31).isAPartOf(vpxchdrRec, 136);
  	public ZonedDecimalData occdate = new ZonedDecimalData(8, 0).isAPartOf(dataRec, 0);
  	public FixedLengthStringData billchnl = new FixedLengthStringData(2).isAPartOf(dataRec, 8);
  	public FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(dataRec, 10);
  	public FixedLengthStringData jownnum = new FixedLengthStringData(8).isAPartOf(dataRec, 18);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(dataRec, 26);
  	public FixedLengthStringData rstaflag = new FixedLengthStringData(2).isAPartOf(dataRec, 29);
  	
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxchdrRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxchdrRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}