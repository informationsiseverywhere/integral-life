package com.csc.life.productdefinition.dataaccess.model;


public class Lfevpf {

    public String chdrcoy;
    public String chdrnum;
    public String life;
    public int effdate;
    public String alname01;
    public String alname02;
    public String alname03;
    public String alname04;
    public String alname05;
    public String jlname01;
    public String jlname02;
    public String jlname03;
    public String jlname04;
    public String jlname05;
    public String smokeind01;
    public String smokeind02;
    public String smokeind03;
    public String smokeind04;
    public String smokeind05;
    public String smkrqd01;
    public String smkrqd02;
    public String smkrqd03;
    public String smkrqd04;
    public String smkrqd05;
    public int anbAtCcd01;
    public int anbAtCcd02;
    public int anbAtCcd03;
    public int anbAtCcd04;
    public int anbAtCcd05;
    public int janbccd01;
    public int janbccd02;
    public int janbccd03;
    public int janbccd04;
    public int janbccd05;
    public int anbisd01;
    public int anbisd02;
    public int anbisd03;
    public int anbisd04;
    public int anbisd05;
    public int janbisd01;
    public int janbisd02;
    public int janbisd03;
    public int janbisd04;
    public int janbisd05;
    public String sex01;
    public String sex02;
    public String sex03;
    public String sex04;
    public String sex05;
    public String jlsex01;
    public String jlsex02;
    public String jlsex03;
    public String jlsex04;
    public String jlsex05;
    public int currfrom;
    public String validflag;
    public String userProfile;
    public String jobName;
    public String datime;

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public int getEffdate() {
        return effdate;
    }

    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }

    public String getAlname01() {
        return alname01;
    }

    public void setAlname01(String alname01) {
        this.alname01 = alname01;
    }

    public String getAlname02() {
        return alname02;
    }

    public void setAlname02(String alname02) {
        this.alname02 = alname02;
    }

    public String getAlname03() {
        return alname03;
    }

    public void setAlname03(String alname03) {
        this.alname03 = alname03;
    }

    public String getAlname04() {
        return alname04;
    }

    public void setAlname04(String alname04) {
        this.alname04 = alname04;
    }

    public String getAlname05() {
        return alname05;
    }

    public void setAlname05(String alname05) {
        this.alname05 = alname05;
    }

    public String getJlname01() {
        return jlname01;
    }

    public void setJlname01(String jlname01) {
        this.jlname01 = jlname01;
    }

    public String getJlname02() {
        return jlname02;
    }

    public void setJlname02(String jlname02) {
        this.jlname02 = jlname02;
    }

    public String getJlname03() {
        return jlname03;
    }

    public void setJlname03(String jlname03) {
        this.jlname03 = jlname03;
    }

    public String getJlname04() {
        return jlname04;
    }

    public void setJlname04(String jlname04) {
        this.jlname04 = jlname04;
    }

    public String getJlname05() {
        return jlname05;
    }

    public void setJlname05(String jlname05) {
        this.jlname05 = jlname05;
    }

    public String getSmokeind01() {
        return smokeind01;
    }

    public void setSmokeind01(String smokeind01) {
        this.smokeind01 = smokeind01;
    }

    public String getSmokeind02() {
        return smokeind02;
    }

    public void setSmokeind02(String smokeind02) {
        this.smokeind02 = smokeind02;
    }

    public String getSmokeind03() {
        return smokeind03;
    }

    public void setSmokeind03(String smokeind03) {
        this.smokeind03 = smokeind03;
    }

    public String getSmokeind04() {
        return smokeind04;
    }

    public void setSmokeind04(String smokeind04) {
        this.smokeind04 = smokeind04;
    }

    public String getSmokeind05() {
        return smokeind05;
    }

    public void setSmokeind05(String smokeind05) {
        this.smokeind05 = smokeind05;
    }

    public String getSmkrqd01() {
        return smkrqd01;
    }

    public void setSmkrqd01(String smkrqd01) {
        this.smkrqd01 = smkrqd01;
    }

    public String getSmkrqd02() {
        return smkrqd02;
    }

    public void setSmkrqd02(String smkrqd02) {
        this.smkrqd02 = smkrqd02;
    }

    public String getSmkrqd03() {
        return smkrqd03;
    }

    public void setSmkrqd03(String smkrqd03) {
        this.smkrqd03 = smkrqd03;
    }

    public String getSmkrqd04() {
        return smkrqd04;
    }

    public void setSmkrqd04(String smkrqd04) {
        this.smkrqd04 = smkrqd04;
    }

    public String getSmkrqd05() {
        return smkrqd05;
    }

    public void setSmkrqd05(String smkrqd05) {
        this.smkrqd05 = smkrqd05;
    }

    public int getAnbAtCcd01() {
        return anbAtCcd01;
    }

    public void setAnbAtCcd01(int anbAtCcd01) {
        this.anbAtCcd01 = anbAtCcd01;
    }

    public int getAnbAtCcd02() {
        return anbAtCcd02;
    }

    public void setAnbAtCcd02(int anbAtCcd02) {
        this.anbAtCcd02 = anbAtCcd02;
    }

    public int getAnbAtCcd03() {
        return anbAtCcd03;
    }

    public void setAnbAtCcd03(int anbAtCcd03) {
        this.anbAtCcd03 = anbAtCcd03;
    }

    public int getAnbAtCcd04() {
        return anbAtCcd04;
    }

    public void setAnbAtCcd04(int anbAtCcd04) {
        this.anbAtCcd04 = anbAtCcd04;
    }

    public int getAnbAtCcd05() {
        return anbAtCcd05;
    }

    public void setAnbAtCcd05(int anbAtCcd05) {
        this.anbAtCcd05 = anbAtCcd05;
    }

    public int getJanbccd01() {
        return janbccd01;
    }

    public void setJanbccd01(int janbccd01) {
        this.janbccd01 = janbccd01;
    }

    public int getJanbccd02() {
        return janbccd02;
    }

    public void setJanbccd02(int janbccd02) {
        this.janbccd02 = janbccd02;
    }

    public int getJanbccd03() {
        return janbccd03;
    }

    public void setJanbccd03(int janbccd03) {
        this.janbccd03 = janbccd03;
    }

    public int getJanbccd04() {
        return janbccd04;
    }

    public void setJanbccd04(int janbccd04) {
        this.janbccd04 = janbccd04;
    }

    public int getJanbccd05() {
        return janbccd05;
    }

    public void setJanbccd05(int janbccd05) {
        this.janbccd05 = janbccd05;
    }

    public int getAnbisd01() {
        return anbisd01;
    }

    public void setAnbisd01(int anbisd01) {
        this.anbisd01 = anbisd01;
    }

    public int getAnbisd02() {
        return anbisd02;
    }

    public void setAnbisd02(int anbisd02) {
        this.anbisd02 = anbisd02;
    }

    public int getAnbisd03() {
        return anbisd03;
    }

    public void setAnbisd03(int anbisd03) {
        this.anbisd03 = anbisd03;
    }

    public int getAnbisd04() {
        return anbisd04;
    }

    public void setAnbisd04(int anbisd04) {
        this.anbisd04 = anbisd04;
    }

    public int getAnbisd05() {
        return anbisd05;
    }

    public void setAnbisd05(int anbisd05) {
        this.anbisd05 = anbisd05;
    }

    public int getJanbisd01() {
        return janbisd01;
    }

    public void setJanbisd01(int janbisd01) {
        this.janbisd01 = janbisd01;
    }

    public int getJanbisd02() {
        return janbisd02;
    }

    public void setJanbisd02(int janbisd02) {
        this.janbisd02 = janbisd02;
    }

    public int getJanbisd03() {
        return janbisd03;
    }

    public void setJanbisd03(int janbisd03) {
        this.janbisd03 = janbisd03;
    }

    public int getJanbisd04() {
        return janbisd04;
    }

    public void setJanbisd04(int janbisd04) {
        this.janbisd04 = janbisd04;
    }

    public int getJanbisd05() {
        return janbisd05;
    }

    public void setJanbisd05(int janbisd05) {
        this.janbisd05 = janbisd05;
    }

    public String getSex01() {
        return sex01;
    }

    public void setSex01(String sex01) {
        this.sex01 = sex01;
    }

    public String getSex02() {
        return sex02;
    }

    public void setSex02(String sex02) {
        this.sex02 = sex02;
    }

    public String getSex03() {
        return sex03;
    }

    public void setSex03(String sex03) {
        this.sex03 = sex03;
    }

    public String getSex04() {
        return sex04;
    }

    public void setSex04(String sex04) {
        this.sex04 = sex04;
    }

    public String getSex05() {
        return sex05;
    }

    public void setSex05(String sex05) {
        this.sex05 = sex05;
    }

    public String getJlsex01() {
        return jlsex01;
    }

    public void setJlsex01(String jlsex01) {
        this.jlsex01 = jlsex01;
    }

    public String getJlsex02() {
        return jlsex02;
    }

    public void setJlsex02(String jlsex02) {
        this.jlsex02 = jlsex02;
    }

    public String getJlsex03() {
        return jlsex03;
    }

    public void setJlsex03(String jlsex03) {
        this.jlsex03 = jlsex03;
    }

    public String getJlsex04() {
        return jlsex04;
    }

    public void setJlsex04(String jlsex04) {
        this.jlsex04 = jlsex04;
    }

    public String getJlsex05() {
        return jlsex05;
    }

    public void setJlsex05(String jlsex05) {
        this.jlsex05 = jlsex05;
    }

    public int getCurrfrom() {
        return currfrom;
    }

    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }

    public void setAlname(int indx, String what) {

        switch (indx) {
        case 1:
            setAlname01(what);
            break;
        case 2:
            setAlname02(what);
            break;
        case 3:
            setAlname03(what);
            break;
        case 4:
            setAlname04(what);
            break;
        case 5:
            setAlname05(what);
            break;
        default:
            return;
        }

    }

    public void setAnbccd(int indx, int what) {

        switch (indx) {
        case 1:
            setAnbAtCcd01(what);
            break;
        case 2:
            setAnbAtCcd02(what);
            break;
        case 3:
            setAnbAtCcd03(what);
            break;
        case 4:
            setAnbAtCcd04(what);
            break;
        case 5:
            setAnbAtCcd05(what);
            break;
        default:
            return;
        }

    }

    public void setSmokeind(int indx, String what) {

        switch (indx) {
        case 1:
            setSmokeind01(what);
            break;
        case 2:
            setSmokeind02(what);
            break;
        case 3:
            setSmokeind03(what);
            break;
        case 4:
            setSmokeind04(what);
            break;
        case 5:
            setSmokeind05(what);
            break;
        default:
            return;
        }

    }

    public void setAnbisd(int indx, int what) {

        switch (indx) {
        case 1:
            setAnbisd01(what);
            break;
        case 2:
            setAnbisd02(what);
            break;
        case 3:
            setAnbisd03(what);
            break;
        case 4:
            setAnbisd04(what);
            break;
        case 5:
            setAnbisd05(what);
            break;
        default:
            return;
        }

    }

    public void setSex(int indx, String what) {

        switch (indx) {
        case 1:
            setSex01(what);
            break;
        case 2:
            setSex02(what);
            break;
        case 3:
            setSex03(what);
            break;
        case 4:
            setSex04(what);
            break;
        case 5:
            setSex05(what);
            break;
        default:
            return;
        }

    }

    public void setJlsex(int indx, String what) {

        switch (indx) {
        case 1:
            setJlsex01(what);
            break;
        case 2:
            setJlsex02(what);
            break;
        case 3:
            setJlsex03(what);
            break;
        case 4:
            setJlsex04(what);
            break;
        case 5:
            setJlsex05(what);
            break;
        default:
            return;
        }

    }

    public void setJanbccd(int indx, int what) {

        switch (indx) {
        case 1:
            setJanbccd01(what);
            break;
        case 2:
            setJanbccd02(what);
            break;
        case 3:
            setJanbccd03(what);
            break;
        case 4:
            setJanbccd04(what);
            break;
        case 5:
            setJanbccd05(what);
            break;
        default:
            return;
        }

    }

    public void setJanbisd(int indx, int what) {

        switch (indx) {
        case 1:
            setJanbisd01(what);
            break;
        case 2:
            setJanbisd02(what);
            break;
        case 3:
            setJanbisd03(what);
            break;
        case 4:
            setJanbisd04(what);
            break;
        case 5:
            setJanbisd05(what);
            break;
        default:
            return;
        }

    }

    public void setJlname(int indx, String what) {

        switch (indx) {
        case 1:
            setJlname01(what);
            break;
        case 2:
            setJlname02(what);
            break;
        case 3:
            setJlname03(what);
            break;
        case 4:
            setJlname04(what);
            break;
        case 5:
            setJlname05(what);
            break;
        default:
            return;
        }

    }

    public void setSmkrqd(int indx, String what) {

        switch (indx) {
        case 1:
            setSmkrqd01(what);
            break;
        case 2:
            setSmkrqd02(what);
            break;
        case 3:
            setSmkrqd03(what);
            break;
        case 4:
            setSmkrqd04(what);
            break;
        case 5:
            setSmkrqd05(what);
            break;
        default:
            return;
        }

    }

}