package com.csc.life.productdefinition.dataaccess.model;

public class Br644DTO {
	
	private String clntnum;
	private int date1;
	private int date2;
	private String cltstat;
	
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public int getDate1() {
		return date1;
	}
	public void setDate1(int date1) {
		this.date1 = date1;
	}
	public int getDate2() {
		return date2;
	}
	public void setDate2(int date2) {
		this.date2 = date2;
	}
	public String getCltstat() {
		return cltstat;
	}
	public void setCltstat(String cltstat) {
		this.cltstat = cltstat;
	}
	

}
