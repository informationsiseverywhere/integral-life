package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList; //IBPLIFE-3963
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PcddlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.LifernlTableDAM;
import com.csc.life.regularprocessing.recordstructures.FMCCalcrec;
import com.csc.life.regularprocessing.recordstructures.Fmcvpmsrec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO; //IBPLIFE-3963
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf; //IBPLIFE-3963
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Vpuubbl extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();

	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(10).init("VPUUBBL");
	private PackedDecimalData wsaaPerFee = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIniFee = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAdmChgs = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaTopupFee = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaTopupAdmChgs = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaTotalCd = new PackedDecimalData(15, 2);
	private ZonedDecimalData wsaaCovrTot = new ZonedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaMgmFee = new PackedDecimalData(17, 2);//ALS-4706

	private PackedDecimalData wsaaSeqno = new PackedDecimalData(2, 0);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTranref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTranref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 15);
	private FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(wsaaTranref, 17, FILLER);

    private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(8, 0).setUnsigned();
    private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanSuffix, 0).setUnsigned();
    private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
    private ZonedDecimalData wsaaFiller = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 0).setUnsigned();
    private ZonedDecimalData wsaaPlanSuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	/*Ticket #IVE-869 Start*/
	private ZonedDecimalData wsaaTotalTax01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotalTax02 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAccumSurrenderValue = new ZonedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaPrevRegister = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaPrevTxcode = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevCrtable = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private FixedLengthStringData wsaaCallSubr = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaPremMeth = new FixedLengthStringData(4).init(SPACES);
	/*Ticket #IVE-869 End*/
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaBatckey = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaBatckey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaBatckey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaBatckey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaBatckey, 14);


	private static final String E308 = "E308";
	private static final String H134 = "H134";

	private static final String T1688 = "T1688";
	private static final String T5645 = "T5645";
	private static final String T5688 = "T5688";
	private static final String t6598 = "T6598";
	/*Ticket #IVE-869 Start*/
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private static final String t5675 = "T5675";
	private static final String f401 = "F401";
	/*Ticket #IVE-869 END*/
	private static final String DESCREC = "DESCREC";
	private static final String ITDMREC = "ITEMREC";
	private static final String ITEMREC = "ITEMREC";
	private static final String COVRREC = "COVRREC";
	private static final String ZRSTREC = "ZRSTREC";
	private static final String covrsurrec = "COVRSURREC";
	/*Ticket #IVE-869 Start*/
	private static final String taxdrec = "TAXDREC";
	private static final String lifernlrec = "LIFERNLREC";
	/*Ticket #IVE-869 End*/	
    private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();

	private Itemkey wsaaItemkey = new Itemkey();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
    private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
    /*Ticket #IVE-869 Start*/
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private LifernlTableDAM lifernlIO = new LifernlTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	/*Ticket #IVE-869 End*/
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	/*Ticket #IVE-869 Start*/
	private T6598rec t6598rec = new T6598rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	/*Ticket #IVE-869 End*/
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
	/*Ticket #IVE-869 Start*/
	private Premiumrec premiumrec = new Premiumrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Freqcpy freqcpy = new Freqcpy();
	private T5675rec t5675rec = new T5675rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private ExternalisedRules er = new ExternalisedRules();
	/*Ticket #IVE-869 End*/
	private PackedDecimalData wsaaStampduty = new PackedDecimalData(17, 2);
	private boolean stampDutyflag = false;
	
	/*ILIFE-9449 start*/
	private final String wsaaSubrUbblmt3 = "UBBLMT3";
	private static final String e308 = "E308";
	private static final String e103 = "E103";
	private PcddlnbTableDAM pcddlnbIO = new PcddlnbTableDAM();
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private PackedDecimalData wsaaRegPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegPremFirst = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegPremRenewal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegIntmDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCommDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommEarn = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommPay = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaServDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaServEarn = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRenlDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRenlEarn = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaPtdate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaBillfreqC = new FixedLengthStringData(2);
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 17, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 17, 2);
	private ZonedDecimalData wsaaAgcmIy = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSubRound = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqC, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqN = new ZonedDecimalData(2, 0).isAPartOf(filler3, 0).setUnsigned();
	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaSubRound, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaSubRoundC = new FixedLengthStringData(2).isAPartOf(filler4, 0);
	private ZonedDecimalData wsaaPlanSuffixN = new ZonedDecimalData(4, 0);
	private FixedLengthStringData filler5 = new FixedLengthStringData(4).isAPartOf(wsaaPlanSuffixN, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaPlanSuffixC = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	private PackedDecimalData wsaaZctnAnnprem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(2, 0);
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private PackedDecimalData wsaaZctnInstprem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private TablesInner tablesInner = new TablesInner();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0);
	private FormatsInner formatsInner = new FormatsInner();
	private ZonedDecimalData wsaaSubNo = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private static final int wsaaAgcmIySize = 500;
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");
	/* WSAA-AGCM-SUMMARY */
	private FixedLengthStringData[] wsaaAgcmPremRec = FLSInittedArray (500, 17);
	private ZonedDecimalData[] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0, UNSIGNED_TRUE);
	private PackedDecimalData[] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3, UNSIGNED_TRUE);
	private PackedDecimalData[] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
	private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfq, 0, REDEFINE).setUnsigned();
	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");

	private T5534rec t5534rec = new T5534rec();
	private T5647rec t5647rec = new T5647rec();
	private Th605rec th605rec = new Th605rec();
	private T5687rec t5687rec = new T5687rec();
	private T5644rec t5644rec = new T5644rec();
	/*ILIFE-9449 end*/
	
	private FMCCalcrec fmcCalcrec = new FMCCalcrec();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private VprcpfDAO vprcpfDAO = getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class);
	private PackedDecimalData fmcCharge = new PackedDecimalData(17, 2);
	private PackedDecimalData allottedFee = new PackedDecimalData(17, 2);
	private static final String  FMC_FEATURE_ID = "BTPRO026";
	private boolean fmcOnFlag = false;
	private ZonedDecimalData znofdue = new ZonedDecimalData(4, 0).init(1).setUnsigned();
	private ZonedDecimalData znofdue2 = new ZonedDecimalData(4, 0).init(2).setUnsigned();
	private static final String  RFMC = "RFMC";
	private PackedDecimalData coiCharge = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0);
	private boolean coiFeature = false;
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);	//IBPLIFE-1937
	private PackedDecimalData totalFundValue = new PackedDecimalData(17, 2);
	private Fmcvpmsrec fmcrec = new Fmcvpmsrec();
	private boolean fmcapplicable = false;
 //IBPLIFE-3963 start
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	private List<Zrstpf> zrstList;
	private List<Zrstpf> zrstUpdateList =  new ArrayList<>();
	private PackedDecimalData tempFundValue = new PackedDecimalData(17, 2);
 //IBPLIFE-3963 end

	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit090,
		exit190,
		exit390,
		exit590,
		postPrem320,
		postFee330,
		perfeeamn335,
		inifeeamn350,
		postAdminCharges360,
		postFee340,
		skipReadingT5688050,
		skipReadingT1688060,
		skipTr52d3400, 
		skipTr52e3400, 
		periodicFeeTax3400, 
		issueFeeTax3400, 
		adminChargeTax3400, 
		topupAdminTax3400, 
		topupFeeTax3400,
		mgmFeeTax3400,
		continue3400, 
		findPremCalcSubroutine2400, 
		skipReadingT56752450, 
		postPrem4020, 
		postFee4030, 
		perfeeamn4200, 
		postInitFee4050, 
		inifeeamn4300, 
		postAdminCharges4400,
		mgMntFee4500,
		skipReadingT65982150,
		postAdvisorFees4500,
		exit3400,
		exit2900,
		callPcddlnbio7020, 
		callCommPay7060, 
		setAgcmValue7070, 
		calcServComm7080, 
		calcRenlComm7090, 
		writeAgcm7100, 
		nextPcddlnb7180, 
		exit7190,
		postCommEarn7620, 
		postCommPay7630,
		postServEarn7720,
		postRenlEarn7820,
		nextr8120, 
		exit8190,
		exit8290,
		exit1490,
		c220Call,
		c280Next, 
		c290Exit,
		c320Loop, 
		c380Next, 
		c390Exit,
		coiPosting,
	}

	public Vpuubbl() {
		super();
	}

	public void mainline(Object... parmArray){
		try{
			vpmfmtrec = (Vpmfmtrec) parmArray[1];
			vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
			ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
			main000();
			vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);
			exit090();
		}catch (COBOLExitProgramException e) {
		}catch (Exception e) {
			systemError900();
 		}
	}

	private void main000 () {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para000();
				}
				case skipReadingT5688050: {
					skipReadingT5688050();
				}
				case skipReadingT1688060: {
					skipReadingT1688060();
				} case exit090: {
					// TODO: Check
//					exit090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	private void para000() {
		stampDutyflag = FeaConfg.isFeatureExist(ubblallpar.chdrChdrcoy.toString().trim(), "NBPROP01", appVars, "IT");
		fmcOnFlag = FeaConfg.isFeatureExist(ubblallpar.chdrChdrcoy.toString().trim(), FMC_FEATURE_ID, appVars, "IT");
		syserrrec.subrname.set(wsaaSubr);
		ubblallpar.statuz.set(Varcom.oK);
		wsaaSequenceNo.set(1);
		if(fmcOnFlag){
			coiFeature = isEQ(ubblallpar.function , "RENL") && isEQ(ubblallpar.znofdue , znofdue2);
		}
		varcom.vrcmTime.set(getCobolTime());
		
		readT5645(SPACES.toString());

		//Read T5688 to find out if contract type wants Component level Accounting.
		if(!isEQ(ubblallpar.comlvlacc , SPACES)){
			t5688rec.comlvlacc.set(ubblallpar.comlvlacc);
			// Skip Reading T5688
			goTo(GotoLabel.skipReadingT5688050);
		}
		// Read T5688
		wsaaItemkey.set(SPACES);
		itdmIO.itempfx.set(smtpfxcpy.item);
		itdmIO.itemcoy.set(ubblallpar.chdrChdrcoy);
		itdmIO.itemtabl.set(T5688);
		itdmIO.itmfrm.set(ubblallpar.effdate);
		itdmIO.itemitem.set(ubblallpar.cnttype);
		itdmIO.function.set(Varcom.begn);
		
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		
		if(!isEQ(itdmIO.statuz , Varcom.oK)
				&& !isEQ(itdmIO.statuz , Varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError999();
		}

		if(!isEQ(itdmIO.itemcoy,ubblallpar.chdrChdrcoy)
				|| !isEQ(itdmIO.itemtabl , T5688)
				|| !isEQ(itdmIO.itemitem , ubblallpar.cnttype)
				|| isEQ(itdmIO.statuz , Varcom.endp)){
			ubblallpar.statuz.set(E308);
			goTo(GotoLabel.exit090);
		} else {
			t5688rec.t5688Rec.set(itdmIO.genarea);
		}
	}
	
	private void readT5645(String seqno) {
		//READ T5645 ITEM UBBLMT2
				//DON'T ASK WHY, BUT THE ITEM IS HARDCODED OR READ T5534?
				itemIO.setItempfx(smtpfxcpy.item);
				itemIO.itemcoy.set(ubblallpar.chdrChdrcoy);
				itemIO.itemtabl.set(T5645);

				// TODO: Check
				itemIO.itemitem.set("UBBLMT2");
				itemIO.itemseq.set(seqno);
				itemIO.itempfx.set(smtpfxcpy.item);
				
				itemIO.format.set(ITEMREC);
				itemIO.function.set(Varcom.readr);
				SmartFileCode.execute(appVars, itemIO);
				if(!isEQ(itemIO.statuz , Varcom.oK) 
						&& !isEQ(itemIO.statuz , Varcom.mrnf)){
					syserrrec.params.set(itemIO.getParams());
					dbError999();
				}
				if(!isEQ(itemIO.statuz , Varcom.oK)){
					ubblallpar.statuz.set(H134);
					goTo(GotoLabel.exit090);
				}
				t5645rec.t5645Rec.set(itemIO.genarea);
	}

	private void skipReadingT5688050 () {
		//GET ITEM DESCRIPTION
		//Get transaction description.
		if(isNE(ubblallpar.trandesc , SPACES)){
			getdescrec.longdesc.set(ubblallpar.trandesc);
			// Skip Reading T1688
			goTo(GotoLabel.skipReadingT1688060);
		}
		
		wsaaItemkey.set(SPACES);		
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(ubblallpar.chdrChdrcoy);
		wsaaItemkey.itemItemtabl.set(T1688);
		wsaaItemkey.itemItemitem.set(ubblallpar.batctrcde);		
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(ubblallpar.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class , getdescrec.getdescRec);
		if(!isEQ(getdescrec.statuz , Varcom.oK)){
			getdescrec.longdesc.set(SPACES);
		}
	}
	
	private  void skipReadingT1688060 () {
		/*Ticket #IVE-869 Start*/
		findSurrenderSubroutine2100();
		if (isNE(ubblallpar.svMethod,SPACES))
		{		
			if(isEQ(wsaaAccumSurrenderValue,ZERO))
			{
				skipReadingT65982150();
			}
		}
		else
		{
			wsaaAccumSurrenderValue.set(ZERO);
		}		
		determinePremCalcMeth1500();
		findPremCalcSubroutine2400();
		skipReadingT56752450();
		calculatePremium2500();
		/*Ticket #IVE-869 End*/
		//Get Total CD from ZRST
		wsaaSeqno.set(ZERO);
		wsaaTotalCd.set(ZERO);
		/*
		 * IBPLIFE-3963 start, instead of DAM begn which was taking time to fetch record, DAO
		 * used which will fetch record in one read and update the totalcd.
		 */
		zrstList = zrstpfDAO.searchZrstReccordByChdrnumLife(ubblallpar.chdrChdrcoy.toString(), 
				ubblallpar.chdrChdrnum.toString(), ubblallpar.lifeLife.toString(),
				ubblallpar.covrCoverage.toString());
		if(!zrstList.isEmpty()) {
			for(Zrstpf zrst: zrstList) {
			wsaaTotalCd.add(zrst.getZramount01().doubleValue());
		}
	}
		//IBPLIFE-3963 end
		readT5687();
		//Update COVERAGE-DEBT field of COVR
		updateCoverage200();
		/*Ticket #IVE-869 Start*/
		processTax3400();
		rewriteCovrRecord3500();
		/*Ticket #IVE-869 End*/
		//POST VARIOUS AMOUNT
		//postAmounts300();
		
		updateLedgerAccounts4000();
		/*ILIFE-9449 start*/
		calcBenefitBilling();
		/*ILIFE-9449 End*/
		//UPDATE ZRSTNUD
		
		/*
		 * IBPLIFE-3963 start, again read because zrstpf will insert another record in
		 * b200WriteZrst method which will be later update the Zramount02.
		 */
		zrstList = new ArrayList<Zrstpf>();
		  zrstList =zrstpfDAO.searchZrstReccordByChdrnumLife(ubblallpar.chdrChdrcoy.toString(),
		  ubblallpar.chdrChdrnum.toString(), ubblallpar.lifeLife.toString(),
		  ubblallpar.covrCoverage.toString());
		 
		if(!zrstList.isEmpty()) {
			for(Zrstpf zrst: zrstList) {
				zrst.setZramount02(wsaaTotalCd.getbigdata());
				zrstUpdateList.add(zrst);	
				zrstpfDAO.updateZrst(zrstUpdateList);
		
			}
			//IBPLIFE-3963 end
		}
	}
	
	/*ILIFE-9449 start*/
	protected void readT5687(){ 
		/* Read T5687 to get the benefit billing type*/
		wsaaItemkey.set(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItmfrm(ubblallpar.effdate);
		itdmIO.setItemitem(ubblallpar.crtable);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),ubblallpar.crtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(ubblallpar.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError580();
			goTo(GotoLabel.exit1490);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

 protected void calcBenefitBilling() 
	{
		if(isEQ(t5687rec.bbmeth,"UB09")) {
			callCommMeth();
		}
	}	
	
protected void callCommMeth() 
	{
		/* Read T5534 to get the benefit billing frequency*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5534);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError570();
		}
		t5534rec.t5534Rec.set(itemIO.getGenarea());
		//Read table Th605
		readTableTh6051400();
		/* Bonus Workbench *                                               */
		if (isEQ(th605rec.bonusInd,"Y") && isNE(premiumrec.calcPrem,ZERO)) {
			if (isEQ(ubblallpar.function,"ISSUE")) {
				c500WriteZptn();
			}
			else {
				c200PremiumHistory();
				c300WriteArrays();
			}
		}
		calcCommission6000();
	}

protected void readTableTh6051400()
	{
		start1411();
	}
protected void start1411()
	{
		/* Read TH605 to see whether system is "Override based on Agent    */
		/* Details"                                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(ubblallpar.chdrChdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			systemError570();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}
	
protected void calcCommission6000()
	{
		/*CALC*/
		if (isEQ(ubblallpar.function,"ISSUE")) {
			createAgcm7000();
		}
		else {
			updateAgcmbch8000();
		}
		/*EXIT*/
	}

protected void createAgcm7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begn7010();
				case callPcddlnbio7020: 
					callPcddlnbio7020();
					readAglf7030();
					initializeLinkage7040();
					calcBasicComm7050();
				case callCommPay7060: 
					callCommPay7060();
				case setAgcmValue7070: 
					setAgcmValue7070();
				case calcServComm7080: 
					calcServComm7080();
				case calcRenlComm7090: 
					calcRenlComm7090();
				case writeAgcm7100: 
					writeAgcm7100();
				case nextPcddlnb7180: 
					nextPcddlnb7180();
				case exit7190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begn7010()
	{
		pcddlnbIO.setDataKey(SPACES);
		pcddlnbIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		pcddlnbIO.setChdrnum(ubblallpar.chdrChdrnum);
		pcddlnbIO.setFormat(formatsInner.pcddlnbrec);
		pcddlnbIO.setFunction(varcom.begn);
	}	

protected void callPcddlnbio7020()
	{//performance improvement --  atiwari23 
	pcddlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	pcddlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, pcddlnbIO);
		if (isNE(pcddlnbIO.getStatuz(),varcom.oK)
		&& isNE(pcddlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcddlnbIO.getParams());
			syserrrec.statuz.set(pcddlnbIO.getStatuz());
			systemError570();
		}
		if (isEQ(pcddlnbIO.getStatuz(),varcom.endp)
		|| isNE(pcddlnbIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(pcddlnbIO.getChdrnum(),ubblallpar.chdrChdrnum)) {
			pcddlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit7190);
		}
	}

protected void readAglf7030()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(pcddlnbIO.getChdrcoy());
		aglflnbIO.setAgntnum(pcddlnbIO.getAgntnum());
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			systemError570();
		}
		if (isEQ(aglflnbIO.getStatuz(),varcom.mrnf)) {
			aglflnbIO.setDataArea(SPACES);
			aglflnbIO.setOvcpc(ZERO);
		}
	}

protected void initializeLinkage7040()
	{
		comlinkrec.chdrcoy.set(ubblallpar.chdrChdrcoy);
		comlinkrec.chdrnum.set(ubblallpar.chdrChdrnum);
		comlinkrec.life.set(ubblallpar.lifeLife);
		comlinkrec.coverage.set(ubblallpar.covrCoverage);
		comlinkrec.effdate.set(ubblallpar.effdate);
		comlinkrec.currto.set(covrIO.getCurrto());
		comlinkrec.rider.set(ubblallpar.covrRider);
		comlinkrec.planSuffix.set(ubblallpar.planSuffix);
		comlinkrec.agent.set(aglflnbIO.getAgntnum());
		comlinkrec.jlife.set(ubblallpar.lifeJlife);
		comlinkrec.crtable.set(ubblallpar.crtable);
		comlinkrec.language.set(ubblallpar.language);
		comlinkrec.agentClass.set(aglflnbIO.getAgentClass());
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		comlinkrec.annprem.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.commprem.set(ZERO);   // IBPLIFE-5237
		agcmIO.setAnnprem(ZERO);
		agcmIO.setInitcom(ZERO);
		agcmIO.setCompay(ZERO);
		agcmIO.setComern(ZERO);
		agcmIO.setScmdue(ZERO);
		agcmIO.setScmearn(ZERO);
		agcmIO.setRnlcdue(ZERO);
		agcmIO.setRnlcearn(ZERO);
		wsaaCommDue.set(ZERO);
		wsaaCommEarn.set(ZERO);
		wsaaCommPay.set(ZERO);
		wsaaServDue.set(ZERO);
		wsaaServEarn.set(ZERO);
		wsaaRenlDue.set(ZERO);
		wsaaRenlEarn.set(ZERO);
		wsaaBillfreqC.set(ubblallpar.billfreq);
		comlinkrec.billfreq.set(ubblallpar.billfreq);
		comlinkrec.seqno.set(1);
		compute(comlinkrec.annprem, 2).set(mult(mult(premiumrec.calcPrem,wsaaBillfreqN),(div(pcddlnbIO.getSplitBcomm(),100))));
		if (isEQ(th605rec.bonusInd,"Y")) {
			compute(wsaaZctnAnnprem, 2).set(mult(premiumrec.calcPrem,wsaaBillfreqN));
		}
		compute(comlinkrec.instprem, 2).set(mult(premiumrec.calcPrem,(div(pcddlnbIO.getSplitBcomm(),100))));
		if (isEQ(th605rec.bonusInd,"Y")) {
			wsaaZctnInstprem.set(premiumrec.calcPrem);
		}
		/* IBPLIFE-5237 starts */
		if (isGT(premiumrec.commissionPrem,ZERO)){
			comlinkrec.commprem.set(premiumrec.commissionPrem);
		}
		/* IBPLIFE-5237 ends */
		
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.frequency.set(t5534rec.unitFreq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(ubblallpar.effdate);
		datcon2rec.intDate2.set(ZERO);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError570();
		}
		comlinkrec.ptdate.set(datcon2rec.intDate2);
	}

protected void calcBasicComm7050()
	{
		agcmIO.setAnnprem(comlinkrec.annprem);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5647);
		itemIO.setItemitem(t5687rec.basicCommMeth);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.callCommPay7060);
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
		if (isEQ(t5647rec.commsubr,SPACES)) {
			goTo(GotoLabel.callCommPay7060);
		}
		comlinkrec.method.set(t5687rec.basicCommMeth);
		callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		callRounding8000();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		callRounding8000();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		callRounding8000();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
	}

protected void callCommPay7060()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		if (isNE(t5687rec.bascpy,SPACES)) {
			itemIO.setItemitem(t5687rec.bascpy);
		}
		else {
			itemIO.setItemitem(aglflnbIO.getBcmtab());
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.setAgcmValue7070);
		}
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.setAgcmValue7070);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isEQ(t5644rec.compysubr,SPACES)) {
			goTo(GotoLabel.setAgcmValue7070);
		}
		comlinkrec.method.set(itemIO.getItemitem());
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		callRounding8000();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		callRounding8000();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		callRounding8000();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
	}

protected void setAgcmValue7070()
	{
		wsaaCommDue.set(comlinkrec.payamnt);
		wsaaCommEarn.set(comlinkrec.erndamt);
		compute(wsaaCommPay, 2).set(sub(wsaaCommDue,wsaaCommEarn));
		agcmIO.setInitcom(comlinkrec.icommtot);
		agcmIO.setCompay(comlinkrec.payamnt);
		agcmIO.setComern(comlinkrec.erndamt);
		agcmIO.setBasicCommMeth(t5687rec.basicCommMeth);
		agcmIO.setBascpy(t5687rec.bascpy);
		if (isEQ(t5687rec.bascpy,SPACES)) {
			agcmIO.setBascpy(aglflnbIO.getBcmtab());
		}
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.calcServComm7080);
		}
		if (isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(pcddlnbIO.getChdrcoy());
			zctnIO.setAgntnum(pcddlnbIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			if (isGT(comlinkrec.instprem,comlinkrec.annprem)) {
				zctnIO.setPremium(wsaaZctnAnnprem);
			}
			else {
				zctnIO.setPremium(wsaaZctnInstprem);
			}
			zctnIO.setSplitBcomm(pcddlnbIO.getSplitBcomm());
			zctnIO.setZprflg("I");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(ubblallpar.tranno);
			zctnIO.setTransCode(ubblallpar.batctrcde);
			zctnIO.setEffdate(ubblallpar.effdate);
			zctnIO.setTrandate(wsaaToday);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				systemError570();
			}
		}
	}

protected void calcServComm7080()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		if (isNE(t5687rec.srvcpy,SPACES)) {
			itemIO.setItemitem(t5687rec.srvcpy);
		}
		else {
			itemIO.setItemitem(aglflnbIO.getScmtab());
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.calcRenlComm7090);
		}
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.calcRenlComm7090);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		if (isEQ(t5644rec.compysubr,SPACES)) {
			goTo(GotoLabel.calcRenlComm7090);
		}
		comlinkrec.method.set(itemIO.getItemitem());
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		callRounding8000();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		callRounding8000();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		callRounding8000();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaServDue.set(comlinkrec.payamnt);
		wsaaServEarn.set(comlinkrec.erndamt);
		agcmIO.setScmdue(comlinkrec.payamnt);
		agcmIO.setScmearn(comlinkrec.erndamt);
		agcmIO.setSrvcpy(t5687rec.srvcpy);
		if (isEQ(t5687rec.srvcpy,SPACES)) {
			agcmIO.setSrvcpy(aglflnbIO.getScmtab());
		}
	}

protected void calcRenlComm7090()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		if (isNE(t5687rec.rnwcpy,SPACES)) {
			itemIO.setItemitem(t5687rec.rnwcpy);
		}
		else {
			itemIO.setItemitem(aglflnbIO.getRcmtab());
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.writeAgcm7100);
		}
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.writeAgcm7100);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isEQ(t5644rec.compysubr,SPACES)) {
			goTo(GotoLabel.writeAgcm7100);
		}
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.method.set(itemIO.getItemitem());
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		callRounding8000();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		callRounding8000();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		callRounding8000();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaRenlDue.set(comlinkrec.payamnt);
		agcmIO.setRnlcdue(comlinkrec.payamnt);
		wsaaRenlEarn.set(comlinkrec.erndamt);
		agcmIO.setRnlcearn(comlinkrec.erndamt);
		agcmIO.setRnwcpy(t5687rec.rnwcpy);
		if (isEQ(t5687rec.rnwcpy,SPACES)) {
			agcmIO.setRnwcpy(aglflnbIO.getRcmtab());
		}
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.writeAgcm7100);
		}
		if (isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(pcddlnbIO.getChdrcoy());
			zctnIO.setAgntnum(pcddlnbIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(sub(wsaaZctnInstprem,wsaaZctnAnnprem), true);
			zctnIO.setSplitBcomm(pcddlnbIO.getSplitBcomm());
			zctnIO.setZprflg("R");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(ubblallpar.tranno);
			zctnIO.setTransCode(ubblallpar.batctrcde);
			zctnIO.setEffdate(ubblallpar.effdate);
			zctnIO.setTrandate(wsaaToday);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				systemError570();
			}
		}
	}

protected void writeAgcm7100()
	{
		if (isEQ(agcmIO.getAnnprem(),ZERO)
		&& isEQ(agcmIO.getInitcom(),ZERO)
		&& isEQ(agcmIO.getCompay(),ZERO)
		&& isEQ(agcmIO.getComern(),ZERO)
		&& isEQ(agcmIO.getScmdue(),ZERO)
		&& isEQ(agcmIO.getScmearn(),ZERO)
		&& isEQ(agcmIO.getRnlcdue(),ZERO)
		&& isEQ(agcmIO.getRnlcearn(),ZERO)) {
			goTo(GotoLabel.nextPcddlnb7180);
		}
		agcmIO.setTransactionDate(ubblallpar.effdate);
		agcmIO.setTransactionTime(varcom.vrcmTime);
		agcmIO.setUser(ubblallpar.user);
		agcmIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		agcmIO.setChdrnum(ubblallpar.chdrChdrnum);
		agcmIO.setAgntnum(aglflnbIO.getAgntnum());
		agcmIO.setAgentClass(aglflnbIO.getAgentClass());
		agcmIO.setLife(ubblallpar.lifeLife);
		agcmIO.setCoverage(ubblallpar.covrCoverage);
		agcmIO.setRider(ubblallpar.covrRider);
		agcmIO.setPlanSuffix(ubblallpar.planSuffix);
		agcmIO.setTranno(ubblallpar.tranno);
		agcmIO.setEfdate(ubblallpar.effdate);
		agcmIO.setCurrfrom(covrIO.getCurrfrom());
		agcmIO.setCurrto(covrIO.getCurrto());
		agcmIO.setOvrdcat("B");
		agcmIO.setCedagent(SPACES);
		agcmIO.setValidflag("1");
		agcmIO.setPtdate(ubblallpar.ptdate);
		agcmIO.setSeqno(1);
		agcmIO.setTermid("1");
		agcmIO.setFormat(formatsInner.agcmrec);
		agcmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			systemError570();
		}
	}

protected void nextPcddlnb7180()
	{
		postingCommission7500();
		pcddlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callPcddlnbio7020);
	}

protected void postingCommission7500()
	{
		setLifacmv7510();
	}

protected void setLifacmv7510()
	{
		lifacmvrec.batccoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.rldgcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.genlcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.rdocnum.set(ubblallpar.chdrChdrnum);
		lifacmvrec.batcactyr.set(ubblallpar.batcactyr);
		lifacmvrec.batcactmn.set(ubblallpar.batcactmn);
		lifacmvrec.batcbatch.set(ubblallpar.batch);
		lifacmvrec.batcbrn.set(ubblallpar.batcbrn);
		lifacmvrec.origcurr.set(ubblallpar.cntcurr);
		lifacmvrec.tranno.set(ubblallpar.tranno);
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		lifacmvrec.effdate.set(ubblallpar.effdate);
		lifacmvrec.user.set(ubblallpar.user);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(ubblallpar.cnttype);
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		lifacmvrec.transactionDate.set(ubblallpar.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		postInitialCommission7600();
		postServiceCommission7700();
		postRenewalCommission7800();
	}

protected void postInitialCommission7600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					postCommDue7610();
				case postCommEarn7620: 
					postCommEarn7620();
				case postCommPay7630: 
					postCommPay7630();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void postCommDue7610()
	{
		if (isEQ(wsaaCommDue,ZERO)) {
			goTo(GotoLabel.postCommEarn7620);
		}
		wsaaSubNo.set(11);
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rldgacct.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaCommDue);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.tranref);
		if (isEQ(ubblallpar.function,"RENL")) {
			lifacmvrec.function.set("NPSTW");
		}
		if (isEQ(ubblallpar.function,"DRENL")) {
			lifacmvrec.function.set("NPSTD");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function,"RENL")) {
			d000WriteAcag();
			lifacmvrec.function.set("PSTW");
		}
		/*                                                         <DRYAP2>*/
		if (isEQ(ubblallpar.function, "DRENL")) {
			d000WriteDacm();
			lifacmvrec.function.set("PSTD");
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			if (isEQ(ubblallpar.function, "ISSUE")) {
				zorlnkrec.annprem.set(agcmIO.getAnnprem());
				zorlnkrec.effdate.set(agcmIO.getEfdate());
			}
			else {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			}
			d500CallZorcompy();
		}
	}

protected void postCommEarn7620()
	{
		if (isEQ(wsaaCommEarn,ZERO)) {
			goTo(GotoLabel.postCommPay7630);
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSubNo.set(14);
		}
		else {
			wsaaSubNo.set(12);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaCommEarn);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void postCommPay7630()
	{
		if (isEQ(wsaaCommPay,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub.set(15);
		}
		else {
			wsaaSub.set(13);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rldgacct.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaCommPay);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.tranref);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
}

protected void postServiceCommission7700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					postServDue7710();
				case postServEarn7720: 
					postServEarn7720();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void postServDue7710()
	{
		if (isEQ(wsaaServDue,ZERO)) {
			goTo(GotoLabel.postServEarn7720);
		}
		wsaaSubNo.set(16);
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaServDue);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		if (isEQ(ubblallpar.function,"RENL")) {
			lifacmvrec.function.set("NPSTW");
		}
		if (isEQ(ubblallpar.function,"DRENL")) {
			lifacmvrec.function.set("NPSTD");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function,"RENL")) {
			d000WriteAcag();
			lifacmvrec.function.set("PSTW");
		}
		if (isEQ(ubblallpar.function, "DRENL")) {
			d000WriteDacm();
			lifacmvrec.function.set("PSTD");
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			if (isEQ(ubblallpar.function, "ISSUE")) {
				zorlnkrec.annprem.set(agcmIO.getAnnprem());
				zorlnkrec.effdate.set(agcmIO.getEfdate());
			}
			else {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			}
			d500CallZorcompy();
		}
	}

protected void postServEarn7720()
	{
		if (isEQ(wsaaServEarn,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSubNo.set(18);
		}
		else {
			wsaaSubNo.set(17);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaServEarn);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void postRenewalCommission7800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					postRenlDue7810();
				case postRenlEarn7820: 
					postRenlEarn7820();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void postRenlDue7810()
	{
		if (isEQ(wsaaRenlDue,ZERO)) {
			goTo(GotoLabel.postRenlEarn7820);
		}
		wsaaSubNo.set(19);
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rldgacct.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaRenlDue);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.tranref);
		if (isEQ(ubblallpar.function,"RENL")) {
			lifacmvrec.function.set("NPSTW");
		}
		if (isEQ(ubblallpar.function,"DRENL")) {
			lifacmvrec.function.set("NPSTD");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function,"RENL")) {
			d000WriteAcag();
			lifacmvrec.function.set("PSTW");
		}
		if (isEQ(ubblallpar.function, "DRENL")) {
			d000WriteDacm();
			lifacmvrec.function.set("PSTD");
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			if (isEQ(ubblallpar.function, "ISSUE")) {
				zorlnkrec.annprem.set(agcmIO.getAnnprem());
				zorlnkrec.effdate.set(agcmIO.getEfdate());
			}
			else {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			}
			d500CallZorcompy();
		}
	}

protected void postRenlEarn7820()
	{
		if (isEQ(wsaaRenlEarn,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSubNo.set(21);
		}
		else {
			wsaaSubNo.set(20);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaRenlEarn);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void updateAgcmbch8000()
	{
		begn8010();
	}

protected void begn8010()
	{
		agcmbchIO.setDataKey(SPACES);
		agcmbchIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		agcmbchIO.setChdrnum(ubblallpar.chdrChdrnum);
		agcmbchIO.setLife(ubblallpar.lifeLife);
		agcmbchIO.setCoverage(ubblallpar.covrCoverage);
		agcmbchIO.setRider(ubblallpar.covrRider);
		agcmbchIO.setPlanSuffix(ubblallpar.planSuffix);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		agcmbchIO.setStatuz(varcom.oK);
		while ( !(isEQ(agcmbchIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
			agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			agcmbchIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
			processAgcm8100();
		}
		
	}

protected void processAgcm8100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					callAgcmbchio8110();
				case nextr8120: 
					nextr8120();
				case exit8190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callAgcmbchio8110()
	{
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			systemError570();
		}
		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)
		|| isNE(agcmbchIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(agcmbchIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(agcmbchIO.getLife(),ubblallpar.lifeLife)
		|| isNE(agcmbchIO.getCoverage(),ubblallpar.covrCoverage)
		|| isNE(agcmbchIO.getRider(),ubblallpar.covrRider)
		|| isNE(agcmbchIO.getPlanSuffix(),ubblallpar.planSuffix)) {
			agcmbchIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit8190);
		}
		if (isEQ(agcmbchIO.getDormantFlag(),"Y")
		|| isEQ(agcmbchIO.getPtdate(),ZERO)) {
			goTo(GotoLabel.nextr8120);
		}
		comlinkrec.chdrcoy.set(ubblallpar.chdrChdrcoy);
		comlinkrec.chdrnum.set(ubblallpar.chdrChdrnum);
		comlinkrec.life.set(ubblallpar.lifeLife);
		comlinkrec.coverage.set(ubblallpar.covrCoverage);
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.currto.set(covrIO.getCurrto());
		comlinkrec.rider.set(ubblallpar.covrRider);
		comlinkrec.planSuffix.set(ubblallpar.planSuffix);
		comlinkrec.jlife.set(ubblallpar.lifeJlife);
		comlinkrec.crtable.set(ubblallpar.crtable);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		comlinkrec.annprem.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.instprem.set(premiumrec.calcPrem);
		comlinkrec.billfreq.set(ubblallpar.billfreq);
		comlinkrec.seqno.set(1);
		wsaaCommDue.set(ZERO);
		wsaaCommEarn.set(ZERO);
		wsaaCommPay.set(ZERO);
		wsaaServDue.set(ZERO);
		wsaaServEarn.set(ZERO);
		wsaaRenlDue.set(ZERO);
		wsaaRenlEarn.set(ZERO);
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.frequency.set(t5534rec.unitFreq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(ubblallpar.effdate);
		datcon2rec.intDate2.set(ZERO);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError570();
		}
		comlinkrec.ptdate.set(datcon2rec.intDate2);
		for (wsaaCommType.set(1); !(isGT(wsaaCommType,3)); wsaaCommType.add(1)){
			callSubroutine8200();
		}
		postingCommission7500();
		rewriteAgcm8300();
	}

protected void nextr8120()
	{
		agcmbchIO.setFunction(varcom.nextr);
	}

protected void callSubroutine8200()
	{
		try {
			callSubr8210();
			setUpPostingValues8220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void callSubr8210()
	{
		initialize(comlinkrec.function);
		initialize(comlinkrec.statuz);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		wsaaPayamnt[wsaaCommType.toInt()].set(ZERO);
		wsaaErndamt[wsaaCommType.toInt()].set(ZERO);
		itemIO.setDataKey(SPACES);
		if (isEQ(wsaaCommType,1)) {
			if (isEQ(agcmbchIO.getBascpy(),SPACES)
			|| (isEQ(agcmbchIO.getCompay(),agcmbchIO.getInitcom())
			&& isEQ(agcmbchIO.getComern(),agcmbchIO.getInitcom()))) {
				goTo(GotoLabel.exit8290);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getBascpy());
			}
		}
		if (isEQ(wsaaCommType,2)) {
			if (isEQ(agcmbchIO.getRnwcpy(),SPACES)) {
				goTo(GotoLabel.exit8290);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getRnwcpy());
			}
		}
		if (isEQ(wsaaCommType,3)) {
			if (isEQ(agcmbchIO.getSrvcpy(),SPACES)
			|| isGT(agcmbchIO.getSeqno(),1)) {
				goTo(GotoLabel.exit8290);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getSrvcpy());
			}
		}
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError570();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit8290);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		comlinkrec.method.set(itemIO.getItemitem());
		if (isEQ(wsaaCommType,1)) {
			comlinkrec.icommtot.set(agcmbchIO.getInitcom());
			comlinkrec.icommpd.set(agcmbchIO.getCompay());
			comlinkrec.icommernd.set(agcmbchIO.getComern());
		}
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		callRounding8000();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		callRounding8000();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		callRounding8000();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaPayamnt[wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaErndamt[wsaaCommType.toInt()].set(comlinkrec.erndamt);
	}

protected void readT5645ExactItem9000()
	{
		read9010();
	}

protected void read9010()
	{
		wsaaItemkey.itemItemseq.set(SPACES);
		if (isGT(wsaaSubNo,15)) {
			compute(wsaaSubRound, 0).setDivide(wsaaSubNo, (15));
			wsaaSub.setRemainder(wsaaSubRound);
			if (isEQ(wsaaSub,ZERO)) {
				wsaaSub.set(15);
				wsaaSubRound.subtract(1);
			}
			wsaaItemkey.itemItemseq.set(wsaaSubRoundC);
		}
		else {
			wsaaSub.set(wsaaSubNo);
		}
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(ubblallpar.chdrChdrcoy);
		wsaaItemkey.itemItemtabl.set(tablesInner.t5645);
		wsaaItemkey.itemItemitem.set(wsaaSubrUbblmt3);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void d000WriteAcag()
	{
		d010WriteRecord();
	}

protected void d010WriteRecord()
	{
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		acagrnlIO.setFunction(varcom.writr);
		acagrnlIO.setFormat(formatsInner.acagrnlrec);
		SmartFileCode.execute(appVars, acagrnlIO);
		if (isNE(acagrnlIO.getStatuz(),varcom.oK)
		&& isNE(acagrnlIO.getStatuz(),varcom.dupr)) {
			syserrrec.params.set(acagrnlIO.getParams());
			systemError570();
		}
	}

protected void d000WriteDacm()
	{
		d100Write();
	}

protected void d100Write()
	{
		/* A deferred ACBL update has been found.                          */
		/* Start by completeing the fields on the ACBL data area.          */
		/* Once this is complete, write a new DACM record using the        */
		/* ACBL data area.                                                 */
		acagrnlIO.setParams(SPACES);
		acagrnlIO.setRecKeyData(SPACES);
		acagrnlIO.setRecNonKeyData(SPACES);
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		/* Set up the DACM record, used for deferred processing            */
		/* within the diary system.                                        */
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(ubblallpar.threadNumber);
		dacmIO.setCompany(lifacmvrec.batccoy);
		dacmIO.setRecformat(formatsInner.acagrnlrec);
		dacmIO.setDataarea(acagrnlIO.getDataArea());
		dacmIO.setFormat(formatsInner.dacmrec);
		dacmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), varcom.oK)) {
			ubblallpar.statuz.set(dacmIO.getStatuz());
			syserrrec.statuz.set(dacmIO.getStatuz());
			systemError570();
		}
	}

protected void d500CallZorcompy()
	{
		d510Start();
	}

protected void d510Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(ubblallpar.ptdate);
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(ubblallpar.tranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			systemError570();
		}
	}

protected void setUpPostingValues8220()
	{
		/*  Basic Commission*/
		if (isEQ(wsaaCommType,1)) {
			/*       IF AGCMBCH-OVRDCAT       = 'O'*/
			wsaaCommDue.set(comlinkrec.payamnt);
			wsaaCommEarn.set(comlinkrec.erndamt);
			compute(wsaaCommPay, 2).set(add(wsaaCommPay,(sub(comlinkrec.payamnt,comlinkrec.erndamt))));
			/**       ELSE*/
			/**          MOVE CLNK-PAYAMNT           TO WSAA-BASCPY-DUE*/
			/**          MOVE CLNK-ERNDAMT           TO WSAA-BASCPY-ERN*/
			/**          COMPUTE WSAA-BASCPY-PAY = WSAA-BASCPY-PAY +*/
			/**                              (CLNK-PAYAMNT - CLNK-ERNDAMT)*/
			/**       END-IF*/
		}
		/*  Service Commission*/
		if (isEQ(wsaaCommType,3)) {
			wsaaServDue.set(comlinkrec.payamnt);
			wsaaServEarn.set(comlinkrec.erndamt);
		}
		/*  Renewal Commission*/
		if (isEQ(wsaaCommType,2)) {
			wsaaRenlDue.set(comlinkrec.payamnt);
			wsaaRenlEarn.set(comlinkrec.erndamt);
		}
		if (isNE(th605rec.bonusInd,"Y")) {
			return ;
		}
		/* Bonus Workbench *                                               */
		/* Skip overriding commission                                      */
		if (isNE(agcmbchIO.getOvrdcat(),"B")) {
			return ;
		}
		if (isEQ(wsaaCommType,1)){
			firstPrem.setTrue();
		}
		else if (isEQ(wsaaCommType,2)){
			renPrem.setTrue();
		}
		else{
			return ;
		}
		if (isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			c400LocatePremium();
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(div(wsaaAgcmPremium,wsaaBillfq9), true);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(),100),wsaaAgcmPremium), true);
			zctnIO.setZprflg(wsaaPremType);
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(ubblallpar.tranno);
			zctnIO.setTransCode(ubblallpar.batctrcde);
			zctnIO.setEffdate(ubblallpar.effdate);
			zctnIO.setTrandate(wsaaToday);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				systemError570();
			}
		}
	}

protected void rewriteAgcm8300()
	{
		rewrite8310();
	}

protected void rewrite8310()
	{
		agcmbchIO.setFunction(varcom.readh);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			systemError570();
		}
		if (isNE(agcmbchIO.getBascpy(),SPACES)) {
			setPrecision(agcmbchIO.getCompay(), 2);
			agcmbchIO.setCompay(add(agcmbchIO.getCompay(),wsaaPayamnt[1]));
			setPrecision(agcmbchIO.getComern(), 2);
			agcmbchIO.setComern(add(agcmbchIO.getComern(),wsaaErndamt[1]));
		}
		if (isNE(agcmbchIO.getRnwcpy(),SPACES)) {
			setPrecision(agcmbchIO.getRnlcdue(), 2);
			agcmbchIO.setRnlcdue(add(agcmbchIO.getRnlcdue(),wsaaPayamnt[2]));
			setPrecision(agcmbchIO.getRnlcearn(), 2);
			agcmbchIO.setRnlcearn(add(agcmbchIO.getRnlcearn(),wsaaErndamt[2]));
		}
		if (isNE(agcmbchIO.getSrvcpy(),SPACES)) {
			setPrecision(agcmbchIO.getScmdue(), 2);
			agcmbchIO.setScmdue(add(agcmbchIO.getScmdue(),wsaaPayamnt[3]));
			setPrecision(agcmbchIO.getScmearn(), 2);
			agcmbchIO.setScmearn(add(agcmbchIO.getScmearn(),wsaaErndamt[3]));
		}
		agcmbchIO.setPtdate(ubblallpar.ptdate);
		agcmbchIO.setFunction(varcom.rewrt);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			systemError570();
		}
	}

protected void c400LocatePremium()
	{
		/*C410-INIT*/
		wsaaAgcmFound.set("N");
		wsaaAgcmPremium.set(ZERO);
		for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb,wsaaAgcmIySize)
		|| agcmFound.isTrue()); wsaaAgcmIb.add(1)){
			if (isEQ(agcmbchIO.getSeqno(),wsaaAgcmSeqno[wsaaAgcmIb.toInt()])) {
				agcmFound.setTrue();
				wsaaAgcmPremium.set(wsaaAgcmAnnprem[wsaaAgcmIb.toInt()]);
			}
		}
		/*C490-EXIT*/
	}

protected void c500WriteZptn()
	{
		c510Writ();
	}

protected void c510Writ()
	{
		compute(wsaaRegPrem, 2).set(mult(premiumrec.calcPrem,wsaaBillfq9));
		if (isGT(premiumrec.calcPrem,wsaaRegPrem)) {
			wsaaRegPremFirst.set(wsaaRegPrem);
			compute(wsaaRegPremRenewal, 2).set(sub(premiumrec.calcPrem,wsaaRegPrem));
			initialize(datcon2rec.datcon2Rec);
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(ubblallpar.effdate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.params.set(datcon2rec.datcon2Rec);
				systemError570();
			}
			wsaaRegIntmDate.set(datcon2rec.intDate2);
		}
		else {
			wsaaRegPremFirst.set(premiumrec.calcPrem);
			wsaaRegPremRenewal.set(ZERO);
			wsaaRegIntmDate.set(wsaaPtdate);
		}
		/* Regular Premium - First Year                                    */
		if (isNE(wsaaRegPremFirst,0)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(ubblallpar.chdrChdrcoy);
			zptnIO.setChdrnum(ubblallpar.chdrChdrnum);
			zptnIO.setLife(ubblallpar.lifeLife);
			zptnIO.setCoverage(ubblallpar.covrCoverage);
			zptnIO.setRider(ubblallpar.covrRider);
			zptnIO.setTranno(ubblallpar.tranno);
			zptnIO.setOrigamt(wsaaRegPremFirst);
			zptnIO.setTransCode(ubblallpar.batctrcde);
			zptnIO.setEffdate(ubblallpar.effdate);
			zptnIO.setBillcd(ubblallpar.effdate);
			zptnIO.setInstfrom(ubblallpar.effdate);
			zptnIO.setInstto(wsaaRegIntmDate);
			zptnIO.setTrandate(wsaaToday);
			zptnIO.setZprflg("I");
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				systemError570();
			}
		}
		/* Regular Premium - Renewal Year                                  */
		if (isNE(wsaaRegPremRenewal,0)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(ubblallpar.chdrChdrcoy);
			zptnIO.setChdrnum(ubblallpar.chdrChdrnum);
			zptnIO.setLife(ubblallpar.lifeLife);
			zptnIO.setCoverage(ubblallpar.covrCoverage);
			zptnIO.setRider(ubblallpar.covrRider);
			zptnIO.setTranno(ubblallpar.tranno);
			zptnIO.setOrigamt(wsaaRegPremRenewal);
			zptnIO.setTransCode(ubblallpar.batctrcde);
			zptnIO.setBillcd(ubblallpar.effdate);
			zptnIO.setEffdate(wsaaRegIntmDate);
			zptnIO.setInstfrom(wsaaRegIntmDate);
			zptnIO.setInstto(wsaaPtdate);
			zptnIO.setTrandate(wsaaToday);
			zptnIO.setZprflg("R");
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				systemError570();
			}
		}
	}

protected void c200PremiumHistory()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					c210Init();
				case c220Call: 
					c220Call();
					c230Summary();
				case c280Next: 
					c280Next();
				case c290Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void c210Init()
	{
		/* Read the AGCM for differentiating the First & Renewal Year      */
		/* Premium                                                         */
		agcmseqIO.setDataArea(SPACES);
		agcmseqIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		agcmseqIO.setChdrnum(ubblallpar.chdrChdrnum);
		agcmseqIO.setLife(ubblallpar.lifeLife);
		agcmseqIO.setCoverage(ubblallpar.covrCoverage);
		agcmseqIO.setRider(ubblallpar.covrRider);
		agcmseqIO.setPlanSuffix(ubblallpar.planSuffix);
		agcmseqIO.setSeqno(ZERO);
		agcmseqIO.setFormat(formatsInner.agcmseqrec);
		agcmseqIO.setFunction(varcom.begn);
	}

protected void c220Call()
	{
	//performance improvement --  atiwari23 
	agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	agcmseqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
			systemError570();
		}
		if (isEQ(agcmseqIO.getStatuz(),varcom.endp)
		|| isNE(agcmseqIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(agcmseqIO.getLife(),ubblallpar.lifeLife)
		|| isNE(agcmseqIO.getCoverage(),ubblallpar.covrCoverage)
		|| isNE(agcmseqIO.getRider(),ubblallpar.covrRider)) {
			agcmseqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.c290Exit);
		}
		/* Skip those irrelevant records                                   */
		if (isNE(agcmseqIO.getOvrdcat(),"B")
		|| isEQ(agcmseqIO.getPtdate(),ZERO)
		|| isEQ(agcmseqIO.getEfdate(),ZERO)
		|| isEQ(agcmseqIO.getEfdate(),varcom.vrcmMaxDate)) {
			goTo(GotoLabel.c280Next);
		}
	}

protected void c230Summary()
	{
		wsaaAgcmFound.set("N");
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy,wsaaAgcmIySize)
		|| isEQ(wsaaAgcmEfdate[wsaaAgcmIy.toInt()],ZERO)
		|| agcmFound.isTrue()); wsaaAgcmIy.add(1)){
			if (isEQ(agcmseqIO.getSeqno(),wsaaAgcmSeqno[wsaaAgcmIy.toInt()])) {
				wsaaAgcmAnnprem[wsaaAgcmIy.toInt()].add(agcmseqIO.getAnnprem());
				agcmFound.setTrue();
			}
		}
		if (!agcmFound.isTrue()) {
			if (isGT(wsaaAgcmIy,wsaaAgcmIySize)) {
				syserrrec.statuz.set(e103);
				systemError570();
			}
			wsaaAgcmSeqno[wsaaAgcmIy.toInt()].set(agcmseqIO.getSeqno());
			wsaaAgcmEfdate[wsaaAgcmIy.toInt()].set(agcmseqIO.getEfdate());
			wsaaAgcmAnnprem[wsaaAgcmIy.toInt()].set(agcmseqIO.getAnnprem());
		}
	}
protected void c280Next()
	{
		agcmseqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.c220Call);
	}

protected void c300WriteArrays()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					c310Init();
				case c320Loop: 
					c320Loop();
					c330Writ();
				case c380Next: 
					c380Next();
				case c390Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void c310Init()
	{
		wsaaAgcmIy.set(1);
	}

protected void c320Loop()
	{
		if (isEQ(wsaaAgcmEfdate[wsaaAgcmIy.toInt()],ZERO)) {
			goTo(GotoLabel.c390Exit);
		}
		else {
			if (isGT(wsaaAgcmEfdate[wsaaAgcmIy.toInt()],ubblallpar.effdate)) {
				goTo(GotoLabel.c380Next);
			}
		}
	}

protected void c330Writ()
	{
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(wsaaAgcmEfdate[wsaaAgcmIy.toInt()]);
		datcon3rec.intDate2.set(ubblallpar.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError570();
		}
		if (isGTE(datcon3rec.freqFactor,1)) {
			renPrem.setTrue();
		}
		else {
			firstPrem.setTrue();
		}
		if (isNE(wsaaAgcmAnnprem[wsaaAgcmIy.toInt()],ZERO)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(ubblallpar.chdrChdrcoy);
			zptnIO.setChdrnum(ubblallpar.chdrChdrnum);
			zptnIO.setLife(ubblallpar.lifeLife);
			zptnIO.setCoverage(ubblallpar.covrCoverage);
			zptnIO.setRider(ubblallpar.covrRider);
			zptnIO.setTranno(ubblallpar.tranno);
			setPrecision(zptnIO.getOrigamt(), 3);
			zptnIO.setOrigamt(div(wsaaAgcmAnnprem[wsaaAgcmIy.toInt()],wsaaBillfq9), true);
			zrdecplrec.amountIn.set(zptnIO.getOrigamt());
			callRounding8000();
			zptnIO.setOrigamt(zrdecplrec.amountOut);
			zptnIO.setTransCode(ubblallpar.batctrcde);
			zptnIO.setEffdate(ubblallpar.effdate);
			zptnIO.setInstfrom(ubblallpar.effdate);
			zptnIO.setBillcd(ubblallpar.effdate);
			zptnIO.setInstto(wsaaPtdate);
			zptnIO.setTrandate(wsaaToday);
			zptnIO.setZprflg(wsaaPremType);
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				systemError570();
			}
		}
	}
	
protected void c380Next()
	{
		wsaaAgcmIy.add(1);
		goTo(GotoLabel.c320Loop);
	}
	/*ILIFE-9449 End*/

	protected void findSurrenderSubroutine2100()
	{
		/* Use UBBL-SV-METHOD to access T6598 for the surrender value*/
		/* calculation subroutine.*/
		/* Check first if a surrender value exists.*/
		if (isNE(ubblallpar.svCalcprog,SPACES)) {
			t6598rec.calcprog.set(ubblallpar.svCalcprog);
			return;
		}
		if (isEQ(ubblallpar.svMethod,SPACES)) {
			wsaaAccumSurrenderValue.set(ZERO);
			return;
		}
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemitem(ubblallpar.svMethod);
		itemIO.setItemtabl(t6598);
		itemIO.setFormat(ITEMREC);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}
	
	protected void skipReadingT65982150()
	{
		if (isEQ(t6598rec.calcprog,SPACES)) {
			wsaaAccumSurrenderValue.set(0);
			return;
		}
		/*CALCULATE-SURRENDER-VALUE*/
		/* To be put in!!*/
		/* As of 10/7/92, it has been put it !!                            */
		callSurrSubroutine5000();
	}
	
	protected void callSurrSubroutine5000()
	{
			start5000();
	}

	protected void start5000()
	{
		/* Read the Coverage file to get Coverage/Rider details for        */
		/*  call to Surrender value calc. subroutine.                      */
		/*  If no valid COVRSUR record found, then exit program.           */
		wsaaAccumSurrenderValue.set(ZERO);
		covrsurIO.setParams(SPACES);
		covrsurIO.setFunction(varcom.readr);
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		covrsurIO.setChdrnum(ubblallpar.chdrChdrnum);
		covrsurIO.setLife(ubblallpar.lifeLife);
		covrsurIO.setCoverage(ubblallpar.covrCoverage);
		covrsurIO.setRider(ubblallpar.covrRider);
		covrsurIO.setPlanSuffix(ubblallpar.planSuffix);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			databaseError580();
		}
		if (isEQ(ubblallpar.function,"ISSUE")) {
			wsaaAccumSurrenderValue.set(covrsurIO.getSingp());
			return ;
		}
		/* We have at found the COVR record.                               */
		/* Set up linkage area to Call Surrender value calculation         */
		/*  subroutines.                                                   */
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		srcalcpy.polsum.set(ubblallpar.polsum);
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.ptdate.set(ubblallpar.ptdate);
		srcalcpy.effdate.set(ubblallpar.effdate);
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.language.set(ubblallpar.language);
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		srcalcpy.chdrCurr.set(ubblallpar.cntcurr);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		if (isGT(covrsurIO.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}
		srcalcpy.billfreq.set(ubblallpar.billfreq);
		srcalcpy.status.set(SPACES);
		srcalcpy.type.set("F");
		/* Now call the Surrender value calculation subroutine.            */
		while ( !(isEQ(srcalcpy.status,varcom.endp))) {
			getSurrValue5100();
		}
		
	}

protected void getSurrValue5100()
	{
		start5100();
	}

protected void start5100()
	{
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
		
			vpxsurcrec.totalEstSurrValue.set(wsaaAccumSurrenderValue);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaAccumSurrenderValue);
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);	

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
							
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
			
			/*ILIFE-3292 Start */
			if(vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
			/*ILIFE-3292 End */
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			systemError570();
		}
		/* Add values returned to our running Surrender value total        */
		/* The Surrender value of the units is returned in the estimated   */
		/*  variable.....                                                  */
		/* Convert the Estimated value to the fund currency.               */
		zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
		callRounding8000();
		srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		callRounding8000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* ADD SURC-ESTIMATED-VAL      TO WSAA-ACCUM-SURRENDER-VALUE.005*/
		if (isNE(srcalcpy.estimatedVal,ZERO)) {
			/* AND SURC-CURRCODE        NOT = UBBL-CNTCURR         <V4LAQR> */
			if (isNE(srcalcpy.currcode,ubblallpar.cntcurr)) {
				/*    MOVE SPACES              TO CLNK-CLNK002-REC      <V4LAQR>*/
				/*    MOVE SURC-ESTIMATED-VAL  TO CLNK-AMOUNT-IN        <V4LAQR>*/
				/*    PERFORM 5200-CONVERT-VALUE                        <V4LAQR>*/
				/*                                                      <V4LAQR>*/
				/*    ADD CLNK-AMOUNT-OUT TO WSAA-ACCUM-ESTIMATED-VALUE <V4LAQR>*/
				/*                         WSAA-ACCUM-SURRENDER-VALUE   <V4LAQR>*/
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountIn.set(srcalcpy.estimatedVal);
				convertValue5200();
				wsaaAccumSurrenderValue.add(conlinkrec.amountOut);
			}
			else {
				wsaaAccumSurrenderValue.add(srcalcpy.estimatedVal);
			}
		}
		/*  The ACTUAL-VALue returned from the subroutine called is the    */
		/*   PENALTY value - if you want the PENALTY included in the       */
		/*   Surrender value figures,remove the comment on the line below. */
		/* ADD SURC-ACTUAL-VAL         TO WSAA-ACCUM-SURRENDER-VALUE.   */
		/* As of change number <009>, we do want to include the Penalty    */
		/*  value.                                                         */
		/* Convert the Actual value to the fund currency.                  */
		/* ADD SURC-ACTUAL-VAL         TO WSAA-ACCUM-SURRENDER-VALUE.009*/
		wsaaAccumSurrenderValue.add(srcalcpy.actualVal);
	}
	protected void convertValue5200()
	{
		call5210();
	}
	
	protected void call5210()
	{
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(ubblallpar.effdate);
		conlinkrec.currIn.set(srcalcpy.currcode);
		conlinkrec.currOut.set(ubblallpar.cntcurr);
		conlinkrec.company.set(ubblallpar.chdrChdrcoy);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding8000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

	private void updateCoverage200() {
		//The amount should add to the CRDEBT field in the COVERAGE
		//record even the benefit billed amount is for a attached rider.
		covrIO.setDataArea(SPACES);
		covrIO.chdrcoy.set(ubblallpar.chdrChdrcoy);
		covrIO.chdrnum.set(ubblallpar.chdrChdrnum);
		covrIO.life.set(ubblallpar.lifeLife);
		covrIO.coverage.set(ubblallpar.covrCoverage);
		covrIO.rider.set("00");
		covrIO.planSuffix.set(ubblallpar.planSuffix);
		
		covrIO.function.set(Varcom.readh);
		covrIO.format.set(COVRREC);
		
		SmartFileCode.execute(appVars, covrIO);
		
		if(!isEQ(covrIO.statuz , Varcom.oK)){
			syserrrec.statuz.set(covrIO.statuz);
			syserrrec.params.set(covrIO.getParams());
			dbError999();
		}
		
		wsaaIniFee.set(ZERO);
		wsaaPerFee.set(ZERO);
		wsaaAdmChgs.set(ZERO);
		wsaaTopupFee.set(ZERO);
		wsaaTopupAdmChgs.set(ZERO);
		wsaaCovrTot.set(ZERO);
		wsaaMgmFee.set(ZERO);
		wsaaStampduty.set(ZERO);
		fmcCharge.set(ZERO);
		allottedFee.set(ZERO);

		if(!isEQ(ubblallpar.function , "TOPUP") && isNE(ubblallpar.function , RFMC) && !coiFeature){
			//CPRM-CALC-PREM / Loaded Premium
			/*Ticket #IVE-869 Start*/
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), premiumrec.calcPrem));
			/*Ticket #IVE-869 End*/
			wsaaCovrTot.set(premiumrec.calcPrem);
		}
		//Check if this subroutine is called from the Issue program or
		//from the Renewals program. Add the Administration Fee amount
		//to the coverage debt.
		if(isEQ(ubblallpar.function , "ISSUE")){
			wsaaIniFee.set(vpmfmtrec.amount01);
			//Add the initial fee to the total Coverage debt
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount01));
			wsaaCovrTot.add(vpmfmtrec.amount01);
			
			//Add the Administrative Charges to the Total Coverage Debt
			wsaaAdmChgs.set(vpmfmtrec.amount02);
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount02));
			wsaaCovrTot.add(vpmfmtrec.amount02);
			//Add Management Fee to the total Coverage debt..ALS-4706
			if(isNE(vpmfmtrec.amount09,ZERO)){
				wsaaMgmFee.set(vpmfmtrec.amount09);
				setPrecision(covrIO.getCoverageDebt(), 2);
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount09));
				wsaaCovrTot.add(vpmfmtrec.amount09);
			}
		}
		if(stampDutyflag){
			if(isEQ(ubblallpar.function , "ISSUE") || isEQ(ubblallpar.function , "RENL")){
			if(isNE(vpmfmtrec.amount12,ZERO)){
				wsaaStampduty.set(vpmfmtrec.amount12);
				setPrecision(covrIO.getCoverageDebt(), 2);
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount12));
				wsaaCovrTot.add(vpmfmtrec.amount12);
				}
			}
		}

		//TopUp Fees & Admin Charges.
		if(isEQ(ubblallpar.function , "TOPUP")){
			//Add the TopUp fee to the total Coverage debt
			wsaaTopupFee.set(vpmfmtrec.amount04);
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount04));
			wsaaCovrTot.add(vpmfmtrec.amount04);

			//Add TopUp Administrative Charges to Total Coverage Debt
			wsaaTopupAdmChgs.set(vpmfmtrec.amount05);
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount05));
			wsaaCovrTot.add(vpmfmtrec.amount05);
		}

		//For TopUp, there is no periodic fee.
		if(!isEQ(ubblallpar.function , "TOPUP") && !coiFeature){
			wsaaPerFee.set(vpmfmtrec.amount03);
			// Add the periodic fee to the total Coverage debt
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount03));
			if(isEQ(ubblallpar.function , RFMC)) {
				allottedFee.add(wsaaPerFee);
			}
			else {
				wsaaCovrTot.add(vpmfmtrec.amount03);
			}
			//Add Management Fee to the total Coverage debt-ALS-4706
			if(isNE(vpmfmtrec.amount10,ZERO)){
			wsaaMgmFee.set(vpmfmtrec.amount10);
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount10));
			wsaaCovrTot.add(vpmfmtrec.amount10);
		}
			if(isEQ(ubblallpar.cntAdvisorFessReq,"Y"))
			{
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount11));
				wsaaCovrTot.add(vpmfmtrec.amount11);
			}
		}	
		if(fmcOnFlag && isEQ(ubblallpar.function , RFMC)){
			if(isEQ(ubblallpar.znofdue, znofdue)) {
				wsaaIniFee.set(vpmfmtrec.amount01);
				//Add the initial fee to the total Coverage debt
				setPrecision(covrIO.getCoverageDebt(), 2);
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount01));
				//wsaaCovrTot.add(vpmfmtrec.amount01);
				allottedFee.add(wsaaIniFee);
			}
				//Add the Administrative Charges to the Total Coverage Debt
				wsaaAdmChgs.set(vpmfmtrec.amount02);
				setPrecision(covrIO.getCoverageDebt(), 2);
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount02));
				//wsaaCovrTot.add(vpmfmtrec.amount02);
				allottedFee.add(wsaaAdmChgs);
				/*// Add the periodic fee to the total Coverage debt
				setPrecision(covrIO.getCoverageDebt(), 2);
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), vpmfmtrec.amount03));
				wsaaCovrTot.add(vpmfmtrec.amount03);*/
				
				//Add FMC to the total Coverage debt
				calcFMC();
				fmcCharge.set(fmcCalcrec.totalFundChg);
				setPrecision(covrIO.getCoverageDebt(), 2);
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), fmcCharge));
				//wsaaCovrTot.add(fmcCharge);
				allottedFee.add(fmcCharge);
			}
			if(fmcOnFlag && coiFeature){
				//Add COI to the total Coverage debt
				calcFMC();
				coiCharge.set(fmcCalcrec.totalMortality);
				setPrecision(covrIO.getCoverageDebt(), 2);
				covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), coiCharge));
				wsaaCovrTot.add(coiCharge);
			}
	}
	
	
	/*Ticket #IVE-869 Start*/
	protected void rewriteCovrRecord3500()
		{
			/*START*/
			covrIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(covrIO.getStatuz());
				syserrrec.params.set(covrIO.getParams());
				databaseError580();
			}
			/*EXIT*/
		}
	/*Ticket #IVE-869 End*/	
	/*Ticket #IVE-869 Start*/	
	protected void b200WriteZrst()
	{  
	    if(isGT(lifacmvrec.origamt,0))   //ILIFE-7691
		writeZrst400();
	}
	/*Ticket #IVE-869 End*/
	private void writeZrst400() {
		initialize(zrstIO.getParams());
		zrstIO.chdrcoy.set(ubblallpar.chdrChdrcoy);
		zrstIO.chdrnum.set(ubblallpar.chdrChdrnum);
		zrstIO.life.set(ubblallpar.lifeLife);
		zrstIO.jlife.set(SPACES);
		zrstIO.coverage.set(ubblallpar.covrCoverage);
		zrstIO.rider.set(ubblallpar.covrRider);
		zrstIO.batctrcde.set(ubblallpar.batctrcde);
		zrstIO.tranno.set(ubblallpar.tranno);
		zrstIO.trandate.set(ubblallpar.effdate);
		zrstIO.zramount01.set(lifacmvrec.origamt);
		wsaaTotalCd.add(lifacmvrec.origamt);
		wsaaSeqno.add(1);
		zrstIO.zramount02.set(ZERO);
		zrstIO.xtranno.set(ZERO);
		zrstIO.seqno.set(wsaaSeqno);
		zrstIO.sacstyp.set(lifacmvrec.sacstyp);
		zrstIO.ustmno.set(ZERO);
		zrstIO.format.set(ZRSTREC);
		zrstIO.function.set(Varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		
		if(!isEQ(zrstIO.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrstIO.statuz);
			syserrrec.params.set(zrstIO.getParams());
			dbError999();
		}
		
	}

	
	/*Ticket #IVE-869 Start*/
	protected void processTax3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3400();
				case skipTr52d3400: 
					skipTr52d3400();
					readTaxControl3400();
				case skipTr52e3400: 
					skipTr52e3400();
					mortalityChargeTax3400();
				case periodicFeeTax3400: 
					periodicFeeTax3400();
				case issueFeeTax3400: 
					issueFeeTax3400();
				case adminChargeTax3400: 
					adminChargeTax3400();
				case topupAdminTax3400: 
					topupAdminTax3400();
				case topupFeeTax3400: 
					topupFeeTax3400();
				case mgmFeeTax3400:
					mgmFeeTax3400();
				case continue3400: 
					continue3400();
				case exit3400: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void start3400()
	{
		wsaaTotalTax01.set(0);
		wsaaTotalTax02.set(0);		
		
		if (isEQ(premiumrec.calcPrem, 0)
		&& isEQ(wsaaIniFee, 0)
		&& isEQ(wsaaAdmChgs, 0)
		&& isEQ(wsaaTopupFee, 0)
		&& isEQ(wsaaTopupAdmChgs, 0)
		&& isEQ(wsaaPerFee, 0)) {
			goTo(GotoLabel.exit3400);
		}
		if (isEQ(wsaaPrevRegister, ubblallpar.chdrRegister)) {
			goTo(GotoLabel.skipTr52d3400);
		}
		wsaaPrevRegister.set(ubblallpar.chdrRegister);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(ubblallpar.chdrRegister);
		itemIO.setFormat(ITEMREC);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				databaseError580();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void skipTr52d3400()
	{
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit3400);
		}
	}

protected void readTaxControl3400()
	{
		if (isEQ(wsaaPrevTxcode, tr52drec.txcode)
		&& isEQ(wsaaPrevCnttype, ubblallpar.cnttype)
		&& isEQ(wsaaPrevCrtable, ubblallpar.crtable)) {
			goTo(GotoLabel.skipTr52e3400);
		}
		wsaaPrevTxcode.set(tr52drec.txcode);
		wsaaPrevCnttype.set(ubblallpar.cnttype);
		wsaaPrevCrtable.set(ubblallpar.crtable);
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(ubblallpar.cnttype);
		wsaaTr52eCrtable.set(ubblallpar.crtable);
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(ITEMREC);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(ubblallpar.cnttype);
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
	}

protected void skipTr52e3400()
	{
		/*  Setup the common variables used in calculating the tax         */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(ubblallpar.chdrChdrcoy);
		txcalcrec.chdrnum.set(ubblallpar.chdrChdrnum);
		txcalcrec.life.set(ubblallpar.lifeLife);
		txcalcrec.coverage.set(ubblallpar.covrCoverage);
		txcalcrec.rider.set(ubblallpar.covrRider);
		txcalcrec.planSuffix.set(ubblallpar.planSuffix);
		txcalcrec.crtable.set(ubblallpar.crtable);
		txcalcrec.cnttype.set(ubblallpar.cnttype);
		txcalcrec.register.set(ubblallpar.chdrRegister);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaCntCurr.set(ubblallpar.cntcurr);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		wsaaBatccoy.set(ubblallpar.batccoy);
		wsaaBatcbrn.set(ubblallpar.batcbrn);
		wsaaBatcactyr.set(ubblallpar.batcactyr);
		wsaaBatcactmn.set(ubblallpar.batcactmn);
		wsaaBatctrcde.set(ubblallpar.batctrcde);
		wsaaBatcbatch.set(ubblallpar.batch);
		txcalcrec.batckey.set(wsaaBatckey);
		txcalcrec.ccy.set(ubblallpar.cntcurr);
		txcalcrec.effdate.set(ubblallpar.effdate);
		txcalcrec.taxAmt[1].set(0);
		txcalcrec.taxAmt[2].set(0);
		txcalcrec.tranno.set(ubblallpar.tranno);
		txcalcrec.language.set(ubblallpar.language);
		/*  Set the value of TAXD record.                                  */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		taxdIO.setChdrnum(ubblallpar.chdrChdrnum);
		taxdIO.setLife(ubblallpar.lifeLife);
		taxdIO.setCoverage(ubblallpar.covrCoverage);
		taxdIO.setRider(ubblallpar.covrRider);
		taxdIO.setPlansfx(ubblallpar.planSuffix);
		taxdIO.setEffdate(ubblallpar.effdate);
		taxdIO.setInstfrom(ubblallpar.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(ubblallpar.tranno);
	}

	protected void mortalityChargeTax3400()
	{
		if(isEQ(ubblallpar.function , "RFMC"))
			return;
		if (isEQ(tr52erec.taxind06, "Y")
		&& isNE(premiumrec.calcPrem, 0)
		&& isNE(ubblallpar.function, "TOPUP")) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.periodicFeeTax3400);
		}
		if(coiFeature) {
			txcalcrec.amountIn.set(coiCharge);
		}
		else{
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(premiumrec.calcBasPrem);
			}
			else {
				txcalcrec.amountIn.set(premiumrec.calcPrem);
			}
		}
		txcalcrec.transType.set("MCHG");
		calcTax3600();
	}

protected void periodicFeeTax3400()
	{
		if (isEQ(tr52erec.taxind07, "Y")
		&& isNE(wsaaPerFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.issueFeeTax3400);
		}
		txcalcrec.amountIn.set(wsaaPerFee);
		txcalcrec.transType.set("PERF");
		calcTax3600();
	}

protected void issueFeeTax3400()
	{
		if (isEQ(tr52erec.taxind08, "Y")
		&& isNE(wsaaIniFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.adminChargeTax3400);
		}
		txcalcrec.amountIn.set(wsaaIniFee);
		txcalcrec.transType.set("ISSF");
		calcTax3600();
	}

protected void adminChargeTax3400()
	{
		if (isEQ(tr52erec.taxind09, "Y")
		&& isNE(wsaaAdmChgs, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.topupAdminTax3400);
		}
		txcalcrec.amountIn.set(wsaaAdmChgs);
		txcalcrec.transType.set("ADMF");
		calcTax3600();
	}

protected void topupAdminTax3400()
	{
		if (isEQ(tr52erec.taxind10, "Y")
		&& isNE(wsaaTopupAdmChgs, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.topupFeeTax3400);
		}
		txcalcrec.amountIn.set(wsaaTopupAdmChgs);
		txcalcrec.transType.set("TOPA");
		calcTax3600();
	}

protected void topupFeeTax3400()
	{
		if (isEQ(tr52erec.taxind10, "Y")
		&& isNE(wsaaTopupFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.mgmFeeTax3400);
		}
		txcalcrec.amountIn.set(wsaaTopupFee);
		txcalcrec.transType.set("TOPF");
		calcTax3600();
	}
protected void mgmFeeTax3400()
{
	if (isEQ(tr52erec.taxind13, "Y")
	&& isNE(wsaaMgmFee, 0)){
		/*NEXT_SENTENCE*/
	}
	else {
		goTo(GotoLabel.continue3400);
	}
	txcalcrec.amountIn.set(wsaaMgmFee);
	txcalcrec.transType.set("MGMF");
		calcTax3600();
	}

protected void calcTax3600()
{
	start3600();
	writeTaxdRecord3600();
}

protected void start3600()
{
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.taxAmt[1].set(0);
	txcalcrec.taxAmt[2].set(0);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		systemError570();
	}
}

protected void writeTaxdRecord3600()
{
	taxdIO.setTrantype(txcalcrec.transType);
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(txcalcrec.taxrule);
	stringVariable1.addExpression(txcalcrec.rateItem);
	stringVariable1.setStringInto(wsaaTranref);
	taxdIO.setTranref(wsaaTranref);
	taxdIO.setBaseamt(txcalcrec.amountIn);
	taxdIO.setBaseamt(txcalcrec.amountIn);
	taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
	taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
	taxdIO.setTaxamt03(0);
	taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
	taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
	taxdIO.setTxtype03(SPACES);
	taxdIO.setTxtype01(txcalcrec.taxType[1]);
	taxdIO.setTxtype02(txcalcrec.taxType[2]);
	taxdIO.setPostflg(" ");
	taxdIO.setFormat(taxdrec);
	taxdIO.setFunction(varcom.writr);
	SmartFileCode.execute(appVars, taxdIO);
	if (isNE(taxdIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(taxdIO.getParams());
		syserrrec.statuz.set(taxdIO.getStatuz());
		databaseError580();
	}
	if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
		wsaaTotalTax01.add(txcalcrec.taxAmt[1]);
	}
	if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
		wsaaTotalTax02.add(txcalcrec.taxAmt[2]);
	}
}


protected void continue3400()
	{
		setPrecision(covrIO.getCoverageDebt(), 2);
		covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), add(wsaaTotalTax01, wsaaTotalTax02)));
		if(isEQ(ubblallpar.function , "RFMC")) {
			compute(allottedFee, 2).add(add(wsaaTotalTax01, wsaaTotalTax02));
		}
		else{
			compute(wsaaCovrTot, 2).add(add(wsaaTotalTax01, wsaaTotalTax02));
		}
		if (isNE(wsaaTotalTax01, 0)) {
			lifacmvrec.sacstyp.set(txcalcrec.taxType[1]);
			lifacmvrec.origamt.set(wsaaTotalTax01);
			b200WriteZrst();
		}
		if (isNE(wsaaTotalTax02, 0)) {
			lifacmvrec.sacstyp.set(txcalcrec.taxType[2]);
			lifacmvrec.origamt.set(wsaaTotalTax02);
			b200WriteZrst();
		}
	}
protected void determinePremCalcMeth1500()
{
		para1505();
		para1510();
	}

/**
* <pre>
* Read LIFERNL to obtain sex & age for prem calculation
* </pre>
*/
protected void para1505()
{
	premiumrec.premiumRec.set(SPACES);
	lifernlIO.setDataArea(SPACES);
	lifernlIO.setChdrcoy(ubblallpar.chdrChdrcoy);
	lifernlIO.setChdrnum(ubblallpar.chdrChdrnum);
	lifernlIO.setLife(ubblallpar.lifeLife);
	lifernlIO.setJlife(ZERO);
	lifernlIO.setFormat(lifernlrec);
	lifernlIO.setFunction(varcom.begn);
	lifernlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	lifernlIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");

	SmartFileCode.execute(appVars, lifernlIO);
	if (isNE(lifernlIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(lifernlIO.getParams());
		databaseError580();
	}
	if ((isNE(lifernlIO.getChdrcoy(),ubblallpar.chdrChdrcoy))
	|| (isNE(lifernlIO.getChdrnum(),ubblallpar.chdrChdrnum))
	|| (isNE(lifernlIO.getLife(),ubblallpar.lifeLife))) {
		lifernlIO.setStatuz(varcom.endp);
		syserrrec.params.set(lifernlIO.getParams());
		databaseError580();
	}
	premiumrec.lsex.set(lifernlIO.getCltsex());
	/* MOVE LIFERNL-ANB-AT-CCD     TO CPRM-LAGE.                    */
	/* get correct age                                                 */
	/* PERFORM 1700-READ-CONTRACT.                          <V65L16>*/
	calcCorrectAge1800();
}

protected void para1510()
{
	/* Read LIFERNL to see if a joint life exists.*/
	/* Which benefit billing to be used is dependent on whether there*/
	/* is a joint life.*/
	lifernlIO.setDataArea(SPACES);
	lifernlIO.setChdrcoy(ubblallpar.chdrChdrcoy);
	lifernlIO.setChdrnum(ubblallpar.chdrChdrnum);
	lifernlIO.setLife(ubblallpar.lifeLife);
	lifernlIO.setJlife("01");
	lifernlIO.setFormat(lifernlrec);
	lifernlIO.setFunction(varcom.begn);
 
	lifernlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	lifernlIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
	
	SmartFileCode.execute(appVars, lifernlIO);
	if ((isNE(lifernlIO.getStatuz(),varcom.oK))
	&& (isNE(lifernlIO.getStatuz(),varcom.endp))) {
		syserrrec.statuz.set(lifernlIO.getStatuz());
		syserrrec.params.set(lifernlIO.getParams());
		databaseError580();
	}
	/* End of file, hence no joint life.*/
	if ((isEQ(lifernlIO.getStatuz(),varcom.endp))) {
		wsaaPremMeth.set(ubblallpar.premMeth);
		premiumrec.jlage.set(ZERO);
		premiumrec.jlsex.set(SPACES);
		return ;
	}
	/*     GO TO 1900-EXIT.                                         */
	/* If key breaks, no joint life else joint life exists.*/
	if ((isNE(lifernlIO.getChdrcoy(),ubblallpar.chdrChdrcoy))
	|| (isNE(lifernlIO.getChdrnum(),ubblallpar.chdrChdrnum))
	|| (isNE(lifernlIO.getLife(),ubblallpar.lifeLife))) {
		wsaaPremMeth.set(ubblallpar.premMeth);
		premiumrec.jlage.set(ZERO);
		premiumrec.jlsex.set(SPACES);
	}
	else {
		wsaaPremMeth.set(ubblallpar.jlifePremMeth);
		premiumrec.jlsex.set(lifernlIO.getCltsex());
		/*     MOVE LIFERNL-ANB-AT-CCD   TO CPRM-JLAGE.                 */
		calcCorrectAge1800();
	}
}

/**
* <pre>
*1700-READ-CONTRACT SECTION.                              <V65L16>
****                                                      <V65L16>
*1700-START.                                              <V65L16>
****                                                      <V65L16>
**** MOVE SPACES                 TO CHDRENQ-PARAMS.       <V65L16>
**** MOVE UBBL-CHDR-CHDRCOY      TO CHDRENQ-CHDRCOY.      <V65L16>
**** MOVE UBBL-CHDR-CHDRNUM      TO CHDRENQ-CHDRNUM.      <V65L16>
**** MOVE CHDRENQREC             TO CHDRENQ-FORMAT.       <V65L16>
**** MOVE READR                  TO CHDRENQ-FUNCTION.     <V65L16>
****                                                      <V65L16>
**** CALL 'CHDRENQIO'            USING CHDRENQ-PARAMS.    <V65L16>
****                                                      <V65L16>
**** IF CHDRENQ-STATUZ           NOT = O-K                <V65L16>
****     MOVE CHDRENQ-PARAMS     TO SYSR-PARAMS           <V65L16>
****     MOVE CHDRENQ-STATUZ     TO SYSR-STATUZ           <V65L16>
****     PERFORM 570-SYSTEM-ERROR                         <V65L16>
**** END-IF.                                              <V65L16>
****                                                      <V65L16>
*1790-EXIT.                                               <V65L16>
****  EXIT.                                               <V65L16>
* </pre>
*/
protected void calcCorrectAge1800()
{
		start1800();
	}

protected void start1800()
{
	/*                                                         <CAS1.0>*/
	/* MOVE SPACES                 TO DTC3-DATCON3-REC.     <CAS1.0>*/
	/* MOVE CHDRENQ-OCCDATE        TO DTC3-INT-DATE-1.      <CAS1.0>*/
	/* MOVE UBBL-EFFDATE           TO DTC3-INT-DATE-2.      <CAS1.0>*/
	/* MOVE '01'                   TO DTC3-FREQUENCY.       <CAS1.0>*/
	/* MOVE ZEROS                  TO DTC3-FREQ-FACTOR.     <CAS1.0>*/
	/*                                                      <CAS1.0>*/
	/* CALL 'DATCON3'              USING DTC3-DATCON3-REC.  <CAS1.0>*/
	/*                                                      <CAS1.0>*/
	/* IF DTC3-STATUZ              NOT = O-K                <CAS1.0>*/
	/*     MOVE DTC3-DATCON3-REC   TO SYSR-PARAMS           <CAS1.0>*/
	/*     MOVE DTC3-STATUZ        TO SYSR-STATUZ           <CAS1.0>*/
	/*     PERFORM 570-SYSTEM-ERROR                         <CAS1.0>*/
	/* END-IF.                                              <CAS1.0>*/
	/*                                                      <CAS1.0>*/
	/* IF LIFERNL-JLIFE            = '01'                   <CAS1.0>*/
	/*     ADD LIFERNL-ANB-AT-CCD  DTC3-FREQ-FACTOR         <CAS1.0>*/
	/*                             GIVING CPRM-JLAGE        <CAS1.0>*/
	/*                                                      <CAS1.0>*/
	/* ELSE                                                 <CAS1.0>*/
	/*     ADD LIFERNL-ANB-AT-CCD  DTC3-FREQ-FACTOR         <CAS1.0>*/
	/*                             GIVING CPRM-LAGE         <CAS1.0>*/
	/* END-IF.                                              <CAS1.0>*/
	/*                                                      <CAS1.0>*/
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(ubblallpar.language);
	/* MOVE CHDRENQ-CNTTYPE        TO AGEC-CNTTYPE.         <V65L16>*/
	agecalcrec.cnttype.set(ubblallpar.cnttype);
	agecalcrec.intDate1.set(lifernlIO.getCltdob());
	agecalcrec.intDate2.set(ubblallpar.effdate);
	agecalcrec.company.set("9");
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isEQ(agecalcrec.statuz,"IVFD")) {
		syserrrec.statuz.set(f401);
		systemError570();
		return ;
	}
	if (isNE(agecalcrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		systemError570();
	}
	if (isEQ(lifernlIO.getJlife(),"01")) {
		premiumrec.jlage.set(agecalcrec.agerating);
	}
	else {
		premiumrec.lage.set(agecalcrec.agerating);
	}
}
	protected void findPremCalcSubroutine2400()
	{
		/* Use UBBL-PREM-METH or UBBL-JLIFE-PREM-METH T5675 for the*/
		/* premium calculation subroutine.*/
		/* Check if UBBL-PREM-METH or UBBL-JLIFE-PREM-METH exists.*/
		if (isNE(ubblallpar.premsubr,SPACES)
		&& isEQ(premiumrec.jlsex,SPACES)) {
			t5675rec.premsubr.set(ubblallpar.premsubr);
			//goTo(GotoLabel.skipReadingT56752450);
			return;
		}
		if (isNE(ubblallpar.jpremsubr,SPACES)
		&& isNE(premiumrec.jlsex,SPACES)) {
			t5675rec.premsubr.set(ubblallpar.jpremsubr);
			//goTo(GotoLabel.skipReadingT56752450);
			return;
		}
		if (isEQ(wsaaPremMeth,SPACES)) {
			premiumrec.calcPrem.set(0);
			premiumrec.calcBasPrem.set(0);
			premiumrec.calcLoaPrem.set(0);
			goTo(GotoLabel.exit2900);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(t5675);
		itemIO.setItemitem(wsaaPremMeth);
		itemIO.setFormat(ITEMREC);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

	protected void skipReadingT56752450()
	{
		if (isNE(t5675rec.premsubr,SPACES)) {
			wsaaCallSubr.set(t5675rec.premsubr);
		}
		else {
			premiumrec.calcPrem.set(0);
			premiumrec.calcBasPrem.set(0);
			premiumrec.calcLoaPrem.set(0);
			goTo(GotoLabel.exit2900);
		}
	}

	protected void calculatePremium2500()
	{
		//wsaaAccumSurrenderValue.set(vpmfmtrec.amount06);
		/* Calculation routine is known, so set up parameters and called*/
		/* the subroutine.*/
		premiumrec.effectdt.set(0);
		premiumrec.calcPrem.set(0);
		premiumrec.calcBasPrem.set(0);
		premiumrec.calcLoaPrem.set(0);
		premiumrec.reRateDate.set(0);
		premiumrec.ratingdate.set(0);
		premiumrec.effectdt.set(ubblallpar.effdate);
		premiumrec.reRateDate.set(ubblallpar.effdate);
		premiumrec.ratingdate.set(ubblallpar.effdate);
		premiumrec.function.set(varcom.calc);
		premiumrec.crtable.set(ubblallpar.crtable);
		premiumrec.chdrChdrcoy.set(ubblallpar.chdrChdrcoy);
		premiumrec.chdrChdrnum.set(ubblallpar.chdrChdrnum);
		premiumrec.lifeLife.set(ubblallpar.lifeLife);
		premiumrec.lifeJlife.set(ubblallpar.lifeJlife);
		premiumrec.covrCoverage.set(ubblallpar.covrCoverage);
		premiumrec.covrRider.set(ubblallpar.covrRider);
		premiumrec.termdate.set(ubblallpar.premCessDate);
		premiumrec.billfreq.set(ubblallpar.billfreq);
		premiumrec.plnsfx.set(ubblallpar.planSuffix);
		premiumrec.effectdt.set(ubblallpar.effdate);
		premiumrec.currcode.set(ubblallpar.cntcurr);
		premiumrec.mop.set(ubblallpar.billchnl);
		premiumrec.mortcls.set(ubblallpar.mortcls);
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError570();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Work out CPRM-SUMIN as the sum at risk.*/
		/* Check if Sum at risk is negative - if so zeroise it.            */
		compute(premiumrec.sumin, 3).setRounded(sub(ubblallpar.sumins,wsaaAccumSurrenderValue));
		if (isLT(premiumrec.sumin,0)) {
			premiumrec.sumin.set(ZERO);
		}
		/* As this is a  Unit Linked Program, there is no               */
		/* need for the Linkage to be set up with ANNY details.         */
		/* Therefore, all fields are initialised before the call        */
		/* to the Subroutine.                                           */
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(ubblallpar.language);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(wsaaCallSubr.toString())))
		{
			callProgram(wsaaCallSubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(ubblallpar.cnttype);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);			
			premiumrec.premMethod.set(wsaaCallSubr.toString().substring(3));
			callProgram(wsaaCallSubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(premiumrec.calcPrem);
		callRounding8000();
		premiumrec.calcPrem.set(zrdecplrec.amountOut);
	}
	
	protected void updateLedgerAccounts4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4010();
				case postPrem4020: 
					postPrem4020();
					if(isNE(ubblallpar.function , "RFMC")) {
						calcPrem4100();
					}
				case postFee4030: 
					postFee4030();
				case perfeeamn4200: 
					perfeeamn4200();
				case postInitFee4050: 
					postInitFee4050();
				case inifeeamn4300: 
					inifeeamn4300();
				case postAdminCharges4400: 
					postAdminCharges4400();
				case mgMntFee4500: 
					mgMntFee4500();
				case postAdvisorFees4500: 
					postAdvisorFees4500();
				case coiPosting: 
					if(fmcOnFlag)
						coiPosting();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4010()
	{
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account*/
		/* movements.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(ubblallpar.chdrChdrnum);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifacmvrec.batccoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.rldgcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.genlcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.batcactyr.set(ubblallpar.batcactyr);
		lifacmvrec.batctrcde.set(ubblallpar.batctrcde);
		lifacmvrec.batcactmn.set(ubblallpar.batcactmn);
		lifacmvrec.batcbatch.set(ubblallpar.batch);
		lifacmvrec.batcbrn.set(ubblallpar.batcbrn);
		/* MOVE T5645-SACSCODE-01      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-01      TO LIFA-SACSTYP.                 */
		lifacmvrec.origcurr.set(ubblallpar.cntcurr);
		/* Intead of sending 3 times values to CONT-DEBT, send the total*/
		/* amount (WSAA-COVR-TOT) just once.*/
		/*  MOVE CPRM-CALC-PREM         TO LIFA-ORIGAMT.*/
		
		lifacmvrec.origamt.set(wsaaCovrTot);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(ubblallpar.tranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(ubblallpar.effdate);
		/* MOVE UBBL-CHDR-CHDRNUM      TO LIFA-TRANREF.                 */
		/* Set up TRANREF to contain the Full component key of the         */
		/*  Coverage/Rider that is Creating the debt so that if a          */
		/*  reversal is required at a later date, the correct component    */
		/*  can be identified.                                             */
		wsaaTranref.set(SPACES);
		wsaaTrefChdrcoy.set(ubblallpar.chdrChdrcoy);
		wsaaTrefChdrnum.set(ubblallpar.chdrChdrnum);
		wsaaPlan.set(ubblallpar.planSuffix);
		wsaaTrefLife.set(ubblallpar.lifeLife);
		wsaaTrefCoverage.set(ubblallpar.covrCoverage);
		wsaaTrefRider.set(ubblallpar.covrRider);
		wsaaTrefPlanSuffix.set(wsaaPlanSuff);
		lifacmvrec.tranref.set(wsaaTranref);
		/* MOVE T5645-SIGN-01          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-GLMAP-01         TO LIFA-GLCODE.                  */
		lifacmvrec.user.set(ubblallpar.user);
		/* MOVE T5645-CNTTOT-01        TO LIFA-CONTOT.                  */
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/* MOVE UBBL-CHDR-CHDRNUM      TO LIFA-RLDGACCT.                */
		/* MOVE UBBL-CHDR-CHDRNUM      TO WSAA-RLDG-CHDRNUM.       <001>*/
		/* MOVE UBBL-PLAN-SUFFIX       TO WSAA-PLAN.               <001>*/
		/* MOVE UBBL-LIFE-LIFE         TO WSAA-RLDG-LIFE.          <001>*/
		/* MOVE UBBL-COVR-COVERAGE     TO WSAA-RLDG-COVERAGE.      <001>*/
		/* MOVE UBBL-COVR-RIDER        TO WSAA-RLDG-RIDER.         <001>*/
		/* MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.   <001>*/
		/* MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.           <001>*/
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(ubblallpar.chdrChdrnum);
			wsaaPlan.set(ubblallpar.planSuffix);
			wsaaRldgLife.set(ubblallpar.lifeLife);
			wsaaRldgCoverage.set(ubblallpar.covrCoverage);
			/*     MOVE UBBL-COVR-RIDER        TO WSAA-RLDG-RIDER      <007>*/
			/*        Post ACMV against the Coverage, not Rider since the      */
			/*         debt is also set against the Coverage                   */
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlanSuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/*     MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6)<7>*/
			/*        Set correct substitution code                            */
			lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(ubblallpar.chdrChdrnum);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/* MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <002>*/
		lifacmvrec.substituteCode[1].set(ubblallpar.cnttype);
		lifacmvrec.transactionDate.set(ubblallpar.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		/* Write a Coverage/Debt ACMV in all cases. Billing details will   */
		/* not be updated during Contract Reversal if this ACMV is missing */
		/*                                                            <003>*/
		/* IF WSAA-COVR-TOT            = ZERO                      <003>*/
		/*    GO                       TO 4100-CALC-PREM.          <003>*/
		if (isEQ(wsaaCovrTot,ZERO)
		&& isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postPrem4020);
		}
		if(isNE(ubblallpar.function , "RFMC")) {
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(lifacmvrec.statuz);
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				systemError570();
			}
		}
	}

protected void postPrem4020()
	{
		if (isEQ(premiumrec.calcPrem,ZERO)
		|| isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postFee4030);
		}
		if(fmcOnFlag)
			fmcCharge4600();
	}

protected void calcPrem4100()
	{
		if(coiFeature){
			return;
		}
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifacmvrec.origamt.set(premiumrec.calcPrem);
		/* MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6)  <014>*/
		/* MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-02         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-02          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.                  */
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )    */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
		}
		if (isEQ(premiumrec.calcPrem,ZERO)) {
			if ((isEQ(t5688rec.comlvlacc,"Y"))) {
				wsaaRldgRider.set("00");
				lifacmvrec.rldgacct.set(wsaaRldgacct);
			}
			goTo(GotoLabel.perfeeamn4200);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set("00");
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
		txcalcrec.transType.set("MCHG");
		b600PostTax();
		b200WriteZrst();
	}

protected void postFee4030()
	{
		if(isEQ(wsaaPerFee , ZERO) 
				|| isEQ(ubblallpar.function , "TOPUP")) {
			goTo(GotoLabel.postInitFee4050);
		}
	}

protected void perfeeamn4200()
	{
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		/* MOVE T5691-PERFEEAMN        TO LIFA-ORIGAMT.                 */
		lifacmvrec.origamt.set(wsaaPerFee);
		/*        Post ACMV against the Coverage, not Rider since the      */
		/*         Fee is also set against the Coverage                    */
		/* MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <014>*/
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		/* MOVE T5645-SACSCODE-03      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-03      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-03         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-03          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-03        TO LIFA-CONTOT.                  */
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )    */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.contot.set(t5645rec.cnttot07);
			lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
		}
		if (isEQ(wsaaPerFee,ZERO)) {
			goTo(GotoLabel.inifeeamn4300);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		txcalcrec.transType.set("PERF");
		b600PostTax();
		b200WriteZrst();
	}

protected void postInitFee4050()
	{
		/* IF T5691-PERFEEAMN           = ZERO                  <CAS1.0>*/
		if(isEQ(wsaaIniFee, ZERO) && isEQ(wsaaTopupFee, ZERO)){
			goTo(GotoLabel.postAdminCharges4400);
			/*****    GO TO 4900-EXIT                                           */
		}
		if (isNE(ubblallpar.function,"ISSUE")
		&& isNE(ubblallpar.function,"TOPUP") && isNE(ubblallpar.function , "RFMC")) {
			goTo(GotoLabel.postAdminCharges4400);
			/*****    GO TO 4900-EXIT                                           */
		}
		
	}

protected void inifeeamn4300()
	{
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		/* MOVE T5691-INIFEEAMN        TO LIFA-ORIGAMT.                 */
		/* MOVE WSAA-INI-FEE           TO LIFA-ORIGAMT.                 */
		if (isEQ(ubblallpar.function,"TOPUP")) {
			lifacmvrec.origamt.set(wsaaTopupFee);
		}
		else {
			lifacmvrec.origamt.set(wsaaIniFee);
		}
		/*        Post ACMV against the Coverage, not Rider since the      */
		/*         Fee is also set against the Coverage                    */
		/* MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <014>*/
		/* MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-04         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-04          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-04        TO LIFA-CONTOT.                  */
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )    */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
		}
		/* IF T5691-INIFEEAMN          = ZERO                   <CAS1.0>*/
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			/*    GO                       TO 4900-EXIT.            <CAS1.0>*/
			goTo(GotoLabel.postAdminCharges4400);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function, "TOPUP")) {
			txcalcrec.transType.set("TOPF");
		}
		else {
			txcalcrec.transType.set("ISSF");
		}
		b600PostTax();
		b200WriteZrst();
	}

protected void postAdminCharges4400()
	{
		if(isEQ(wsaaAdmChgs,ZERO) 
			&& isEQ(wsaaTopupAdmChgs , ZERO)){
			return ;
		}
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		if (isEQ(ubblallpar.function,"TOPUP")) {
			lifacmvrec.origamt.set(wsaaTopupAdmChgs);
		}
		else {
			lifacmvrec.origamt.set(wsaaAdmChgs);
		}
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		
		
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			lifacmvrec.contot.set(t5645rec.cnttot09);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function, "TOPUP")) {
			txcalcrec.transType.set("TOPA");
		}
		else {
			txcalcrec.transType.set("ADMF");
		}
		b600PostTax();
		b200WriteZrst();
	}

//ALS-4706-starts
protected void mgMntFee4500()
{
	if(stampDutyflag) {
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifacmvrec.origamt.set(vpmfmtrec.amount12);
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode15);
			lifacmvrec.sacstyp.set(t5645rec.sacstype15);
			lifacmvrec.glcode.set(t5645rec.glmap15);
			lifacmvrec.glsign.set(t5645rec.sign15);
			lifacmvrec.contot.set(t5645rec.cnttot15);
			lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		}
	}
	if(isEQ(wsaaMgmFee,ZERO)){
		return ;
	}
	lifacmvrec.function.set("PSTW");
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(lifacmvrec.statuz);
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		systemError570();
	}
	b200WriteZrst();
	
	
	lifacmvrec.jrnseq.set(wsaaSequenceNo);
	wsaaSequenceNo.add(1);
	lifacmvrec.origamt.set(wsaaMgmFee);
	lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
	if ((isEQ(t5688rec.comlvlacc,"Y"))) {
		wsaaRldgRider.set(ubblallpar.covrRider);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode11);
		lifacmvrec.sacstyp.set(t5645rec.sacstype11);
		lifacmvrec.glcode.set(t5645rec.glmap11);
		lifacmvrec.glsign.set(t5645rec.sign11);
		lifacmvrec.contot.set(t5645rec.cnttot11);
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
	}
	else {
		lifacmvrec.sacscode.set(t5645rec.sacscode12);
		lifacmvrec.sacstyp.set(t5645rec.sacstype12);
		lifacmvrec.glcode.set(t5645rec.glmap12);
		lifacmvrec.glsign.set(t5645rec.sign12);
		lifacmvrec.contot.set(t5645rec.cnttot12);
	}
	lifacmvrec.function.set("PSTW");
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(lifacmvrec.statuz);
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		systemError570();
	}
	txcalcrec.transType.set("MGMF");
	b600PostTax();
	b200WriteZrst();
}

//ALS-4706--ends---

protected void b600PostTax()
{
	b600Start();
}

protected void b600Start()
{
	/*  Set the value of TAXD record.                                  */
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	taxdIO.setDataArea(SPACES);
	taxdIO.setChdrcoy(ubblallpar.chdrChdrcoy);
	taxdIO.setChdrnum(ubblallpar.chdrChdrnum);
	taxdIO.setLife(ubblallpar.lifeLife);
	taxdIO.setCoverage(ubblallpar.covrCoverage);
	taxdIO.setRider(ubblallpar.covrRider);
	taxdIO.setPlansfx(ubblallpar.planSuffix);
	taxdIO.setInstfrom(ubblallpar.effdate);
	taxdIO.setTrantype(txcalcrec.transType);
	taxdIO.setStatuz(varcom.oK);
	taxdIO.setFormat(taxdrec);
	taxdIO.setFunction(varcom.readh);
	SmartFileCode.execute(appVars, taxdIO);
	if (isNE(taxdIO.getStatuz(), varcom.oK)
	&& isNE(taxdIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(taxdIO.getParams());
		syserrrec.statuz.set(taxdIO.getStatuz());
		databaseError580();
	}
	if (isEQ(taxdIO.getStatuz(), varcom.mrnf)) {
		return ;
	}
	/* Call tax subroutine                                             */
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("POST");
	txcalcrec.statuz.set(varcom.oK);
	txcalcrec.chdrcoy.set(taxdIO.getChdrcoy());
	txcalcrec.chdrnum.set(taxdIO.getChdrnum());
	txcalcrec.life.set(taxdIO.getLife());
	txcalcrec.coverage.set(taxdIO.getCoverage());
	txcalcrec.rider.set(taxdIO.getRider());
	txcalcrec.planSuffix.set(taxdIO.getPlansfx());
	txcalcrec.crtable.set(ubblallpar.crtable);
	txcalcrec.cnttype.set(ubblallpar.cnttype);
	txcalcrec.register.set(ubblallpar.chdrRegister);
	txcalcrec.taxrule.set(subString(taxdIO.getTranref(), 1, 8));
	txcalcrec.rateItem.set(subString(taxdIO.getTranref(), 9, 8));
	txcalcrec.amountIn.set(taxdIO.getBaseamt());
	txcalcrec.transType.set(taxdIO.getTrantype());
	txcalcrec.effdate.set(taxdIO.getEffdate());
	txcalcrec.tranno.set(taxdIO.getTranno());
	txcalcrec.jrnseq.set(wsaaSequenceNo);
	txcalcrec.batckey.set(wsaaBatckey);
	txcalcrec.ccy.set(ubblallpar.cntcurr);
	txcalcrec.taxType[1].set(taxdIO.getTxtype01());
	txcalcrec.taxType[2].set(taxdIO.getTxtype02());
	txcalcrec.taxAmt[1].set(taxdIO.getTaxamt01());
	txcalcrec.taxAmt[2].set(taxdIO.getTaxamt02());
	txcalcrec.taxAbsorb[1].set(taxdIO.getTxabsind01());
	txcalcrec.taxAbsorb[2].set(taxdIO.getTxabsind02());
	txcalcrec.ccy.set(ubblallpar.cntcurr);
	txcalcrec.language.set(ubblallpar.language);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		databaseError580();
	}
	wsaaSequenceNo.set(txcalcrec.jrnseq);
	/* Update TAXD record                                              */
	taxdIO.setPostflg("P");
	taxdIO.setFunction(varcom.rewrt);
	SmartFileCode.execute(appVars, taxdIO);
	if (isNE(taxdIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(taxdIO.getParams());
		syserrrec.statuz.set(taxdIO.getStatuz());
		databaseError580();
	}
}

	
	protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(ubblallpar.chdrChdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(ubblallpar.cntcurr);
		zrdecplrec.batctrcde.set(ubblallpar.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError580();
		}
		/*EXIT*/
	}

	protected void databaseError580()
	{
			para580();
			exit589();
		}
	
	protected void para580()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}
	
	protected void exit589()
	{
		ubblallpar.statuz.set(varcom.bomb);
		exit090();
	}
	
	protected void systemError570()
	{
			para570();
			exit579();
		}

	protected void para570()
		{
			if (isEQ(syserrrec.statuz,varcom.bomb)) {
				return ;
			}
			syserrrec.syserrStatuz.set(syserrrec.statuz);
			syserrrec.syserrType.set("2");
			callProgram(Syserr.class, syserrrec.syserrRec);
		}
	
	protected void exit579()
		{
			ubblallpar.statuz.set(varcom.bomb);
			exit090();
		}
	/*Ticket #IVE-869 End*/
	
	private void systemError900() {
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(2);
		callProgram(Syserr.class, syserrrec.syserrRec);
		goTo(GotoLabel.exit090);
	}

    protected void dbError999() {
        syserrrec.syserrType.set("1");
        callProgram(Syserr.class, syserrrec.syserrRec);
        goTo(GotoLabel.exit090);
    }

    protected void exit090() {
    	exitProgram();
    }

protected void postAdvisorFees4500()
{
	if (isNE(ubblallpar.cntAdvisorFessReq,"Y")) {
		return ;
	}
	lifacmvrec.jrnseq.set(wsaaSequenceNo);
	wsaaSequenceNo.add(1);
	lifacmvrec.origamt.set(vpmfmtrec.amount11);
	/* MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6)*/
	/* MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.*/
	/* MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.*/
	/* MOVE T5645-GLMAP-02         TO LIFA-GLCODE.*/
	/* MOVE T5645-SIGN-02          TO LIFA-GLSIGN.*/
	/* MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.*/
	/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )*/
	
	if ((isEQ(t5688rec.comlvlacc,"Y"))) {
		wsaaRldgRider.set(ubblallpar.covrRider);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode14);
		lifacmvrec.sacstyp.set(t5645rec.sacstype14);
		lifacmvrec.glcode.set(t5645rec.glmap14);
		lifacmvrec.glsign.set(t5645rec.sign14);
		lifacmvrec.contot.set(t5645rec.cnttot14);
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
	}
	else {
		lifacmvrec.sacscode.set(t5645rec.sacscode13);
		lifacmvrec.sacstyp.set(t5645rec.sacstype13);
		lifacmvrec.glcode.set(t5645rec.glmap13);
		lifacmvrec.glsign.set(t5645rec.sign13);
		lifacmvrec.contot.set(t5645rec.cnttot13);
	}
	lifacmvrec.function.set("PSTW");
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(lifacmvrec.statuz);
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		systemError570();
	}

//	txcalcrec.transType.set("ADVF");
//	b600PostTax();
	b200WriteZrst();
}

protected void calcFMC() {
	Vprcpf vprcPf = null;
	String applicble;
	Map<String, BigDecimal> fundMap = new HashMap<String, BigDecimal>();
	fmcCalcrec.totalMortality.set(ZERO);
	fmcCalcrec.totalFundChg.set(ZERO);
	
	List<Lifepf> lifePfList = lifepfDAO.getLifeData(ubblallpar.chdrChdrcoy.toString(), ubblallpar.chdrChdrnum.toString(), "01", "00");
	fmcCalcrec.cnttype.set(ubblallpar.cnttype);
	fmcCalcrec.crtable.set(covrIO.getCrtable());
	fmcCalcrec.lage.set(lifePfList.get(0).getAnbAtCcd());
	fmcCalcrec.gender.set(lifePfList.get(0).getCltsex());
	fmcCalcrec.mortality.set(covrIO.getMortcls());
	fmcCalcrec.sumins.set(covrIO.getSumins());
	fmcCalcrec.company.set(ubblallpar.chdrChdrcoy);
	fmcCalcrec.deathCalcMethod.set(t5687rec.dcmeth.trim());
	//Get Funds details from DB
	List<Utrspf> utrspfList = utrspfDAO.searchUtrsRecord(ubblallpar.chdrChdrcoy.toString(), ubblallpar.chdrChdrnum.toString());
	if (utrspfList != null && !utrspfList.isEmpty()) {
		for (Utrspf u : utrspfList) {
			if(fundMap.containsKey(u.getUnitVirtualFund())){
				tempFundValue.set(fundMap.get(u.getUnitVirtualFund()));
				tempFundValue.add(u.getCurrentUnitBal().doubleValue());
				fundMap.put(u.getUnitVirtualFund(),tempFundValue.getbigdata());
			}
			else{
				fundMap.put(u.getUnitVirtualFund(), u.getCurrentUnitBal());
			}	
		}
		for(String fund : fundMap.keySet()){
			if(fund != null && !fund.trim().equals("")) {
				vprcPf = vprcpfDAO.getNowPrice(ubblallpar.chdrChdrcoy.toString(), fund, "A", ubblallpar.batchrundate.toInt(), "1");
				fmcCalcrec.fund.set(fund);
				fmcCalcrec.fundValue.set(mult(fundMap.get(fund), vprcPf.getUnitBidPrice()));
				fmcApplicable(fund);
				if(fmcapplicable && !coiFeature){
					callProgram("FMCCHARGECALC",fmcCalcrec.fmcCalcRec);
					fmcCalcrec.totalFundChg.add(fmcCalcrec.outputFundMgtChg);
				}
				else{
					totalFundValue.add(fmcCalcrec.fundValue);
				}
			}
		}
		if(coiFeature){
			fmcCalcrec.fundValue.set(totalFundValue);
			callProgram("FMCCHARGECALC",fmcCalcrec.fmcCalcRec);
			fmcCalcrec.totalMortality.set(fmcCalcrec.outputMortality);
		}
		
	} else {	//IBPLIFE-1937
		Hitspf hitspf = new Hitspf();
		hitspf.setChdrcoy(ubblallpar.chdrChdrcoy.toString());
		hitspf.setChdrnum(ubblallpar.chdrChdrnum.toString());
		hitspf.setLife(ubblallpar.lifeLife.toString());
		hitspf.setCoverage(ubblallpar.covrCoverage.toString());
		hitspf.setRider(ubblallpar.covrRider.toString());
		List<Hitspf> hitspfList = hitspfDAO.searchHitsRecord(hitspf);
		Map<String, BigDecimal> hitsFundMap = new HashMap<String, BigDecimal>();
		if(!hitspfList.isEmpty()) {
			for(Hitspf hits : hitspfList) {
				if(hitsFundMap.containsKey(hits.getZintbfnd())){
					tempFundValue.set(fundMap.get(hits.getZintbfnd()));
					tempFundValue.add(hits.getZcurprmbal().doubleValue());
					hitsFundMap.put(hits.getZintbfnd(),tempFundValue.getbigdata());
				}
				else{
					hitsFundMap.put(hits.getZintbfnd(), hits.getZcurprmbal());
				}
			}
			for(String hitsFund : hitsFundMap.keySet()){
				if(hitsFund != null && !hitsFund.trim().equals("")) {
					fmcCalcrec.fund.set(hitsFund);
					fmcCalcrec.fundValue.set(hitsFundMap.get(hitsFund));
					fmcApplicable(hitsFund);
					if(fmcapplicable && !coiFeature){
						callProgram("FMCCHARGECALC",fmcCalcrec.fmcCalcRec);
						fmcCalcrec.totalFundChg.add(fmcCalcrec.outputFundMgtChg);
					}
					else{
						totalFundValue.add(fmcCalcrec.fundValue);
					}
				}
			}
			if(coiFeature){
				fmcCalcrec.fundValue.set(totalFundValue);
				callProgram("FMCCHARGECALC",fmcCalcrec.fmcCalcRec);
				fmcCalcrec.totalMortality.set(fmcCalcrec.outputMortality);
			}
		}
	}
}

protected void fmcApplicable(String fund){
	String applicble = "N";
	fmcapplicable = false;
	fmcrec.product.set(ubblallpar.cnttype);
	fmcrec.covrCoverage.set(ubblallpar.crtable);
	fmcrec.fund.set(fund);
	callProgram("FMCAPPCB", fmcrec);
	applicble = fmcrec.fmcapplicable.toString();// Value will be Y!N
	if (applicble != null && applicble.length() > 2) {
	if (applicble.trim().equals("Y")) 
		fmcapplicable = true;
	} else {
		syserrrec.params.set("VPMS Erros");
		syserrrec.statuz.set("ERROR");
		systemError570();
	}
}


protected void fmcCharge4600(){
	
	if(isNE(allottedFee,ZERO)){
		// Allotted Fee posting
		readT5645("01");
		wsaaSub1.set(01);
		wsaaSub2.set(02);
		lifacmvrec.origamt.set(allottedFee);
		posting3000();
	}
	readT5645(SPACES.toString());
	if(isNE(fmcCharge,ZERO)){
		// FMC posting
		wsaaSub1.set(13);
		wsaaSub2.set(14);
		lifacmvrec.origamt.set(fmcCharge);
		posting3000();
		b200WriteZrst();
	}
}

private void coiPosting(){

	if(isNE(coiCharge,ZERO)){
		// COI Fee posting
		wsaaSub1.set(06);
		wsaaSub2.set(02);
		lifacmvrec.origamt.set(coiCharge);
		posting3000();
		txcalcrec.transType.set("MCHG");
		b600PostTax();
		b200WriteZrst();
	}
}

private void posting3000(){
	
	lifacmvrec.jrnseq.set(wsaaSequenceNo);
	wsaaSequenceNo.add(1);

	lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
	if ((isEQ(t5688rec.comlvlacc,"Y"))) {
		wsaaRldgRider.set(ubblallpar.covrRider);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
	}
	else {
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub2.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub2.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub2.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub2.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub2.toInt()]);
	}
	lifacmvrec.function.set("PSTW");
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(lifacmvrec.statuz);
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		systemError570();
	}
}

private static final class TablesInner { 
	/* TABLES */
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5647 = new FixedLengthStringData(5).init("T5647");
	private FixedLengthStringData t5644 = new FixedLengthStringData(5).init("T5644");
	private FixedLengthStringData t5534 = new FixedLengthStringData(5).init("T5534");
	private FixedLengthStringData th605 = new FixedLengthStringData(6).init("TH605");
}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData pcddlnbrec = new FixedLengthStringData(10).init("PCDDLNBREC");
	private FixedLengthStringData agcmrec = new FixedLengthStringData(10).init("AGCMREC");
	private FixedLengthStringData dacmrec = new FixedLengthStringData(10).init("DACMREC");
	private FixedLengthStringData acagrnlrec = new FixedLengthStringData(10).init("ACAGRNLREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData agcmseqrec = new FixedLengthStringData(10).init("AGCMSEQREC");
}
    
}