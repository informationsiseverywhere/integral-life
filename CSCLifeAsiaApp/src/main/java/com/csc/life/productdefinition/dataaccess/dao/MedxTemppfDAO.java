package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MedxTemppfDAO extends BaseDAO<MedxTemppf> {
	
	public boolean insertMedxTempRecords(List<MedxTemppf> medxList, String tempTableName);

}
