package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:27
 * Description:
 * Copybook name: T5620REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5620rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5620Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(t5620Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(497).isAPartOf(t5620Rec, 3, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5620Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5620Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}