package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class St503screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St503ScreenVars sv = (St503ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St503screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St503ScreenVars screenVars = (St503ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.riskunit.setClassString("");
		screenVars.bvolume01.setClassString("");
		screenVars.insprm01.setClassString("");
		screenVars.bvolume02.setClassString("");
		screenVars.insprm02.setClassString("");
		screenVars.bvolume03.setClassString("");
		screenVars.insprm03.setClassString("");
		screenVars.bvolume04.setClassString("");
		screenVars.insprm04.setClassString("");
		screenVars.bvolume05.setClassString("");
		screenVars.insprm05.setClassString("");
		screenVars.bvolume06.setClassString("");
		screenVars.insprm06.setClassString("");
		screenVars.bvolume07.setClassString("");
		screenVars.insprm07.setClassString("");
		screenVars.bvolume08.setClassString("");
		screenVars.insprm08.setClassString("");
		screenVars.bvolume09.setClassString("");
		screenVars.insprm09.setClassString("");
		screenVars.bvolume10.setClassString("");
		screenVars.insprm10.setClassString("");
		screenVars.bvolume11.setClassString("");
		screenVars.insprm11.setClassString("");
		screenVars.bvolume12.setClassString("");
		screenVars.insprm12.setClassString("");
		screenVars.bvolume13.setClassString("");
		screenVars.insprm13.setClassString("");
		screenVars.bvolume14.setClassString("");
		screenVars.insprm14.setClassString("");
		screenVars.bvolume15.setClassString("");
		screenVars.insprm15.setClassString("");
		screenVars.bvolume16.setClassString("");
		screenVars.insprm16.setClassString("");
	}

/**
 * Clear all the variables in St503screen
 */
	public static void clear(VarModel pv) {
		St503ScreenVars screenVars = (St503ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.riskunit.clear();
		screenVars.bvolume01.clear();
		screenVars.insprm01.clear();
		screenVars.bvolume02.clear();
		screenVars.insprm02.clear();
		screenVars.bvolume03.clear();
		screenVars.insprm03.clear();
		screenVars.bvolume04.clear();
		screenVars.insprm04.clear();
		screenVars.bvolume05.clear();
		screenVars.insprm05.clear();
		screenVars.bvolume06.clear();
		screenVars.insprm06.clear();
		screenVars.bvolume07.clear();
		screenVars.insprm07.clear();
		screenVars.bvolume08.clear();
		screenVars.insprm08.clear();
		screenVars.bvolume09.clear();
		screenVars.insprm09.clear();
		screenVars.bvolume10.clear();
		screenVars.insprm10.clear();
		screenVars.bvolume11.clear();
		screenVars.insprm11.clear();
		screenVars.bvolume12.clear();
		screenVars.insprm12.clear();
		screenVars.bvolume13.clear();
		screenVars.insprm13.clear();
		screenVars.bvolume14.clear();
		screenVars.insprm14.clear();
		screenVars.bvolume15.clear();
		screenVars.insprm15.clear();
		screenVars.bvolume16.clear();
		screenVars.insprm16.clear();
	}
}
