package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Td5g4rec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData td5g4Rec = new FixedLengthStringData(700);


		public FixedLengthStringData riskclasss = new FixedLengthStringData(40).isAPartOf(td5g4Rec, 0);
		public FixedLengthStringData[] riskclass = FLSArrayPartOfStructure(10, 4, riskclasss, 0);
		public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(riskclasss, 0, FILLER_REDEFINE);
		public FixedLengthStringData riskclass01 = new FixedLengthStringData(4).isAPartOf(filler,0);
		public FixedLengthStringData riskclass02 = new FixedLengthStringData(4).isAPartOf(filler,4);
		public FixedLengthStringData riskclass03 = new FixedLengthStringData(4).isAPartOf(filler,8);
		public FixedLengthStringData riskclass04 = new FixedLengthStringData(4).isAPartOf(filler,12);
		public FixedLengthStringData riskclass05 = new FixedLengthStringData(4).isAPartOf(filler,16);
		public FixedLengthStringData riskclass06 = new FixedLengthStringData(4).isAPartOf(filler,20);
		public FixedLengthStringData riskclass07 = new FixedLengthStringData(4).isAPartOf(filler,24);
		public FixedLengthStringData riskclass08 = new FixedLengthStringData(4).isAPartOf(filler,28);
		public FixedLengthStringData riskclass09 = new FixedLengthStringData(4).isAPartOf(filler,32);
		public FixedLengthStringData riskclass10 = new FixedLengthStringData(4).isAPartOf(filler,36);

		
		
		public FixedLengthStringData riskclassdescs = new FixedLengthStringData(450).isAPartOf(td5g4Rec, 40);
		public FixedLengthStringData[] riskclassdesc = FLSArrayPartOfStructure(10, 45, riskclassdescs, 0);
		public FixedLengthStringData filler1 = new FixedLengthStringData(450).isAPartOf(riskclassdescs, 0, FILLER_REDEFINE);
		public FixedLengthStringData riskclassdesc01 = new FixedLengthStringData(45).isAPartOf(filler1,0);
		public FixedLengthStringData riskclassdesc02 = new FixedLengthStringData(45).isAPartOf(filler1,45);
		public FixedLengthStringData riskclassdesc03 = new FixedLengthStringData(45).isAPartOf(filler1,90);
		public FixedLengthStringData riskclassdesc04 = new FixedLengthStringData(45).isAPartOf(filler1,135);
		public FixedLengthStringData riskclassdesc05 = new FixedLengthStringData(45).isAPartOf(filler1,180);
		public FixedLengthStringData riskclassdesc06 = new FixedLengthStringData(45).isAPartOf(filler1,225);
		public FixedLengthStringData riskclassdesc07 = new FixedLengthStringData(45).isAPartOf(filler1,270);
		public FixedLengthStringData riskclassdesc08 = new FixedLengthStringData(45).isAPartOf(filler1,315);
		public FixedLengthStringData riskclassdesc09 = new FixedLengthStringData(45).isAPartOf(filler1,360);
		public FixedLengthStringData riskclassdesc10 = new FixedLengthStringData(45).isAPartOf(filler1,405);

		
	
		
		public FixedLengthStringData formulas = new FixedLengthStringData(100).isAPartOf(td5g4Rec, 490);
		public FixedLengthStringData[] formula = FLSArrayPartOfStructure(10, 10, formulas, 0);
		public FixedLengthStringData filler2 = new FixedLengthStringData(100).isAPartOf(formulas, 0, FILLER_REDEFINE);
		public FixedLengthStringData formula01 = new FixedLengthStringData(10).isAPartOf(filler2,0);
		public FixedLengthStringData formula02 = new FixedLengthStringData(10).isAPartOf(filler2,10);
		public FixedLengthStringData formula03 = new FixedLengthStringData(10).isAPartOf(filler2,20);
		public FixedLengthStringData formula04 = new FixedLengthStringData(10).isAPartOf(filler2,30);
		public FixedLengthStringData formula05 = new FixedLengthStringData(10).isAPartOf(filler2,40);
		public FixedLengthStringData formula06 = new FixedLengthStringData(10).isAPartOf(filler2,50);
		public FixedLengthStringData formula07 = new FixedLengthStringData(10).isAPartOf(filler2,60);
		public FixedLengthStringData formula08 = new FixedLengthStringData(10).isAPartOf(filler2,70);
		public FixedLengthStringData formula09 = new FixedLengthStringData(10).isAPartOf(filler2,80);
		public FixedLengthStringData formula10 = new FixedLengthStringData(10).isAPartOf(filler2,90);

		public FixedLengthStringData factors = new FixedLengthStringData(50).isAPartOf(td5g4Rec, 590);
		public ZonedDecimalData[] factor = ZDArrayPartOfStructure(10, 5, 2, factors, 3);  
		public FixedLengthStringData filler3 = new FixedLengthStringData(50).isAPartOf(factors, 0, FILLER_REDEFINE);
		public ZonedDecimalData factor01 = new ZonedDecimalData(5, 2).isAPartOf(filler3,0);
		public ZonedDecimalData factor02 = new ZonedDecimalData(5, 2).isAPartOf(filler3,5);
		public ZonedDecimalData factor03 = new ZonedDecimalData(5, 2).isAPartOf(filler3,10);
		public ZonedDecimalData factor04 = new ZonedDecimalData(5, 2).isAPartOf(filler3,15);
		public ZonedDecimalData factor05 = new ZonedDecimalData(5, 2).isAPartOf(filler3,20);
		public ZonedDecimalData factor06 = new ZonedDecimalData(5, 2).isAPartOf(filler3,25);
		public ZonedDecimalData factor07 = new ZonedDecimalData(5, 2).isAPartOf(filler3,30);
		public ZonedDecimalData factor08 = new ZonedDecimalData(5, 2).isAPartOf(filler3,35);
		public ZonedDecimalData factor09 = new ZonedDecimalData(5, 2).isAPartOf(filler3,40);
		public ZonedDecimalData factor10 = new ZonedDecimalData(5, 2).isAPartOf(filler3,45);
		
		public FixedLengthStringData filler5 = new FixedLengthStringData(90).isAPartOf(td5g4Rec, 610, FILLER);


		public void initialize() {
			COBOLFunctions.initialize(td5g4Rec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	    		td5g4Rec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
