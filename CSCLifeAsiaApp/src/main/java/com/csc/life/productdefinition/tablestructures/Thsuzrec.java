package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:26
 * Description:
 * Copybook name: TH584REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Thsuzrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData thsuzRec = new FixedLengthStringData(500);
  	public FixedLengthStringData flag = new FixedLengthStringData(1).isAPartOf(thsuzRec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(499).isAPartOf(thsuzRec, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(thsuzRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			thsuzRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}