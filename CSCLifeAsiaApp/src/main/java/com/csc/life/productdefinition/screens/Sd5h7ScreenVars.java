package com.csc.life.productdefinition.screens;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Sd5h7ScreenVars extends SmartVarModel {
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(216);
	public FixedLengthStringData dataFields = new FixedLengthStringData(72).isAPartOf(dataArea, 0);
	
	public ZonedDecimalData intrate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData intcalfreq = DD.regpayfreq.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData intcapfreq = DD.regpayfreq.copy().isAPartOf(dataFields,10);
  	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,12);
  	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,13);
  	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,21);
  	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,51);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,56);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,64);
  	
  	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea,72);
  	public FixedLengthStringData intrateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData intcalfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData intcapfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 108);
	
	public FixedLengthStringData[] intrateOut = FLSArrayPartOfStructure	(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] intcalfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] intcapfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);
	public LongData Sd5h7screenWritten = new LongData(0);
	public LongData Sd5h7protectWritten = new LongData(0);
	
	public FixedLengthStringData salelicensetypeDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return false;
	}
	
	public Sd5h7ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(intrateOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(intcalfreqOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(intcapfreqOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, itmfrm, itmto, longdesc, intrate, intcalfreq,intcapfreq};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut,itmfrmOut, itmtoOut, longdescOut, intrateOut, intcalfreqOut,intcapfreqOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr,itmfrmErr, itmtoErr, longdescErr, intrateErr, intcalfreqErr,intcapfreqErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp,salelicensetypeDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5h7Screen.class;
		protectRecord = Sd5h7protect.class;
	}
}
