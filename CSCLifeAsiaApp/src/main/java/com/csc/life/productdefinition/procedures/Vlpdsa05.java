/*
 * File: Vlpdsa05.java
 * Date: 30 August 2009 2:54:10
 * Author: Quipoz Limited
 * 
 * Class transformed from VLPDSA05.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List; //ILB-1062

import com.csc.fsu.general.dataaccess.dao.ItempfDAO; //ILB-1062
import com.csc.fsu.general.dataaccess.model.Itempf; //ILB-1062
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51frec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil; //ILB-1062
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* Subroutine VLPDSA05.
* ====================
*
*    This subroutine will be processed to validate SA agaist
*    table TR51F
*    by check following,
*      Sum assured of all component in TR51F must <= TR51F-SUMINS
*
*    - Read table TR51F, using VLSB-CNTTYPE as keys, if not found then
*      read again by use '***'
*    - Calculate age by using call 'AGECALC'
*    - Accumulate SA of all riders (VLSB-INPUT-SA(xx)) that
*      VLSB-INPUT-CRTABLE(xx) in list of rider(TR51F-CRTABLE) in TR51F
*    - Get maximum SA (TR51F-SA) and operator (TR51F-INDC) from compare
*      AGEC-AGERATING with TR51F-AGE
*    - Compare  total of SA with condition from TR51F
*
***********************************************************************
* </pre>
*/
public class Vlpdsa05 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VLPDSA05";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaErdSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaVlsbSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr51fSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaInputAllSpace = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaInputCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaTr51fFound = new FixedLengthStringData(1);
	private int wsaaMaxRow = 10;
	private int wsaaMaxCrtable = 20;
	private int wsaaMaxError = 20;
	private int wsaaMaxInput = 50;
	private ZonedDecimalData wsaaInputZsa = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData wsaaErrDetails = new FixedLengthStringData(80);
	private FixedLengthStringData[] wsaaErrRec = FLSArrayPartOfStructure(20, 4, wsaaErrDetails, 0);
	private FixedLengthStringData[] wsaaErrDetail = FLSDArrayPartOfArrayStructure(4, wsaaErrRec, 0);

	private FixedLengthStringData wsaaTr51fItmkey = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaTr51fCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr51fItmkey, 0).init(SPACES);

	private FixedLengthStringData wsaaTr51fArray = new FixedLengthStringData(80);
	private FixedLengthStringData[] wsaaTr51fRec = FLSArrayPartOfStructure(20, 4, wsaaTr51fArray, 0);
	private FixedLengthStringData[] wsaaTr51fKey = FLSDArrayPartOfArrayStructure(4, wsaaTr51fRec, 0);
	private FixedLengthStringData[] wsaaTr51fCrtable = FLSDArrayPartOfArrayStructure(4, wsaaTr51fKey, 0, HIVALUES);
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String lifelnbrec = "LIFELNBREC";
		/* TABLES */
	private String tr51f = "TR51F";
	private String t1693 = "T1693";
		/* ERRORS */
	private String rlbl = "RLBL";
	private IntegerData wsaaTr51fIx = new IntegerData();
	private Agecalcrec agecalcrec = new Agecalcrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private Tr51frec tr51frec = new Tr51frec();
	private Varcom varcom = new Varcom();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();
	
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class); //ILB-1062

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		nextTr51f380, 
		exit390, 
		exit490, 
		exit590, 
		exit690
	}

	public Vlpdsa05() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		try {
			begin010();
			xxxComponent040();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		wsaaTr51fFound.set(SPACES);
		if (isEQ(vlpdsubrec.cnttype,SPACES)) {
			goTo(GotoLabel.exit090);
		}
		readTable100();
		ageCalculate200();
		/*CONTRCTYP-COMPONENT*/
		wsaaTr51fCnttype.set(vlpdsubrec.cnttype);
		callRoutineTr51f210();
		if (isEQ(wsaaTr51fFound,"Y")) {
			goTo(GotoLabel.exit090);
		}
	}

protected void xxxComponent040()
	{
		wsaaTr51fCnttype.set("***");
		callRoutineTr51f210();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTable100()
	{
		begin110();
	}

protected void begin110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(vlpdsubrec.chdrcoy);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
	}

protected void ageCalculate200()
	{
		begin201();
	}

protected void begin201()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(vlpdsubrec.chdrcoy);
		lifelnbIO.setChdrnum(vlpdsubrec.chdrnum);
		lifelnbIO.setLife(vlpdsubrec.inputLife[1]);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		initialize(agecalcrec.agecalcRec);
		agecalcrec.statuz.set(varcom.oK);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(vlpdsubrec.language);
		agecalcrec.cnttype.set(vlpdsubrec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(vlpdsubrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
	}

protected void callRoutineTr51f210()
	{
		/*BEGIN*/
 		//ILB-1062	start
		List<Itempf> itempfList = itempfDAO.findItem("IT", vlpdsubrec.chdrcoy.toString(), tr51f, wsaaTr51fItmkey.toString());
		if(!itempfList.isEmpty()) {
			for (Itempf item : itempfList) {
				wsaaTr51fFound.set("Y");
				tr51frec.tr51fRec.set(StringUtil.rawToString(item.getGenarea()));
				for (wsaaTr51fSeq.set(1); !(isGT(wsaaTr51fSeq,wsaaMaxCrtable)); wsaaTr51fSeq.add(1)){
					wsaaTr51fCrtable[wsaaTr51fSeq.toInt()].set(tr51frec.crtable[wsaaTr51fSeq.toInt()]);
				}
				initialize(wsaaErrDetails);
				wsaaInputAllSpace.set(SPACES);
				wsaaErdSeq.set(0);
				wsaaInputZsa.set(0);
				for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq,wsaaMaxInput)
				|| isEQ(wsaaInputAllSpace,"Y")); wsaaVlsbSeq.add(1)){
					sumassured400();
				}
				if (isEQ(wsaaInputZsa,0)) {
					continue;
				}
				wsaaErrCode.set(SPACES);
				for (wsaaTr51fSeq.set(1); !(isGT(wsaaTr51fSeq,wsaaMaxRow)); wsaaTr51fSeq.add(1)){
					if (isLTE(agecalcrec.agerating,tr51frec.age[wsaaTr51fSeq.toInt()])) {
						chkSaLimit500();
						wsaaTr51fSeq.set(wsaaMaxRow);
					}
				}
			}
		}
		//ILB-1062 end
	}

protected void sumassured400()
	{
		try {
			begin410();
		}
		catch (GOTOException e){
		}
	}

protected void begin410()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()],SPACES)) {
			wsaaInputAllSpace.set("Y");
			goTo(GotoLabel.exit490);
		}
		wsaaInputCrtable.set(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()]);
		wsaaTr51fIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaTr51fIx,wsaaTr51fRec.length); wsaaTr51fIx.add(1)){
				if (isEQ(wsaaTr51fKey[wsaaTr51fIx.toInt()],wsaaInputCrtable)) {
					break searchlabel1;
				}
			}
			goTo(GotoLabel.exit490);
		}
		wsaaErdSeq.add(1);
		wsaaErrDetail[wsaaErdSeq.toInt()].set(wsaaInputCrtable);
		compute(wsaaInputZsa, 2).set(add(wsaaInputZsa,vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]));
	}

protected void chkSaLimit500()
	{
		try {
			begin510();
		}
		catch (GOTOException e){
		}
	}

protected void begin510()
	{
		if (isGT(wsaaInputZsa,tr51frec.sumins[wsaaTr51fSeq.toInt()])) {
			wsaaErrCode.set(rlbl);
		}
		if (isEQ(wsaaErrCode,SPACES)) {
			goTo(GotoLabel.exit590);
		}
		wsaaErdSeq.set(0);
		for (wsaaErrSeq.set(1); !(isGT(wsaaErrSeq,wsaaMaxError)); wsaaErrSeq.add(1)){
			if (isEQ(vlpdsubrec.errCode[wsaaErrSeq.toInt()],SPACES)) {
				wsaaErdSeq.add(1);
				if (isEQ(wsaaErrDetail[wsaaErdSeq.toInt()],SPACES)) {
					wsaaErrSeq.set(wsaaMaxError);
				}
				else {
					vlpdsubrec.errCode[wsaaErrSeq.toInt()].set(wsaaErrCode);
					vlpdsubrec.errDet[wsaaErrSeq.toInt()].set(wsaaErrDetail[wsaaErdSeq.toInt()]);
				}
			}
		}
	}

protected void fatalError600()
	{
		try {
			fatalErrors610();
		}
		catch (GOTOException e){
		}
		finally{
			exit690();
		}
	}

protected void fatalErrors610()
	{
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit690()
	{
		exitProgram();
	}
}
