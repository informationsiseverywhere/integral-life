package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import com.csc.life.productdefinition.tablestructures.Td5h2rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Td5h2pt  extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Td5h2rec td5h2rec = new Td5h2rec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();


	public Td5h2pt() {
		super();
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		td5h2rec.td5h2Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(td5h2rec.agntsdets);
		generalCopyLinesInner.fieldNo006.set(td5h2rec.payind);
		generalCopyLinesInner.fieldNo007.set(td5h2rec.ddind);
		generalCopyLinesInner.fieldNo008.set(td5h2rec.grpind);
		generalCopyLinesInner.fieldNo009.set(td5h2rec.accInd);
		generalCopyLinesInner.fieldNo010.set(td5h2rec.triggerRequired);
		generalCopyLinesInner.fieldNo011.set(td5h2rec.sacscode);
		generalCopyLinesInner.fieldNo012.set(td5h2rec.sacstyp);
		generalCopyLinesInner.fieldNo013.set(td5h2rec.mediaRequired);
		generalCopyLinesInner.fieldNo014.set(td5h2rec.printLocation);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		endUp090();
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES_INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Loan Repayment Method               Sd5h2");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(32);
	private FixedLengthStringData filler7 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Entity Rules and Data Capture:-");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(39);
	private FixedLengthStringData filler8 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine004, 8, FILLER).init("Agent Details required . . .");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 38);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(39);
	private FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine005, 8, FILLER).init("Payer Details required . . .");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 38);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(39);
	private FixedLengthStringData filler12 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine006, 8, FILLER).init("Bank  Details required . . .");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 38);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(39);
	private FixedLengthStringData filler14 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine007, 8, FILLER).init("Group Details required . . .");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 38);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(21);
	private FixedLengthStringData filler16 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" Billing Processing:-");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(39);
	private FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine009, 8, FILLER).init("On Account . . . . . . . . .");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 38);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(39);
	private FixedLengthStringData filler19 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 8, FILLER).init("Trigger required . . . . . .");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 38);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(78);
	private FixedLengthStringData filler21 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine011, 8, FILLER).init("Sub Ledger Code and Type . .");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 38);
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 42);
	private FixedLengthStringData filler24 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine011, 44, FILLER).init("   Enter only when used for Groups");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(39);
	private FixedLengthStringData filler25 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine012, 8, FILLER).init("Media required . . . . . . .");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 38);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(79);
	private FixedLengthStringData filler27 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine013, 8, FILLER).init("Debit Note Print Location. .");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 38);
	private FixedLengthStringData filler29 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine013, 39, FILLER).init(" I/B (Applicable to Polisy product only)");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(36);
	private FixedLengthStringData filler30 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" Any fields not required leave blank");
}
}
