package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LextpfDAOImpl extends BaseDAOImpl<Lextpf> implements LextpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LifepfDAOImpl.class);

    public Map<Long, List<Lextpf>> searchLextRecord(List<Long> covrUQ) {
        StringBuilder sqlLextSelect1 = new StringBuilder(
                "SELECT L.CURRFROM, L.OPCDA, L.OPPC, L.AGERATE, L.INSPRM, L.ECESTRM, C.UNIQUE_NUMBER ");
        sqlLextSelect1
                .append("FROM LEXTPF L,COVRPF C WHERE L.VALIDFLAG = '1' AND L.CHDRCOY=C.CHDRCOY AND L.CHDRNUM = C.CHDRNUM AND L.LIFE=C.LIFE AND L.COVERAGE=C.COVERAGE AND L.RIDER=C.RIDER AND ");
        sqlLextSelect1.append(getSqlInLong("C.UNIQUE_NUMBER", covrUQ));

        sqlLextSelect1
                .append(" ORDER BY L.CHDRCOY ASC, L.CHDRNUM ASC, L.LIFE ASC, L.COVERAGE ASC, L.RIDER ASC, L.SEQNBR ASC, L.UNIQUE_NUMBER DESC ");

        PreparedStatement psLextSelect = getPrepareStatement(sqlLextSelect1.toString());
        ResultSet sqllextpf1rs = null;
        Map<Long, List<Lextpf>> lextpfSearchResult = new HashMap<Long, List<Lextpf>>();
        try {
            sqllextpf1rs = executeQuery(psLextSelect);
            while (sqllextpf1rs.next()) {
                Lextpf lextpf = new Lextpf();
                lextpf.setCurrfrom(sqllextpf1rs.getInt(1));
                lextpf.setOpcda(sqllextpf1rs.getString(2));
                lextpf.setOppc(sqllextpf1rs.getBigDecimal(3));
                lextpf.setAgerate(sqllextpf1rs.getInt(4));
                lextpf.setInsprm(sqllextpf1rs.getInt(5));
                lextpf.setExtCessTerm(sqllextpf1rs.getInt(6));
                long uniqueNumber = sqllextpf1rs.getLong(7);
                if (lextpfSearchResult.containsKey(uniqueNumber)) {
                    lextpfSearchResult.get(uniqueNumber).add(lextpf);
                } else {
                    List<Lextpf> lextpfList = new ArrayList<Lextpf>();
                    lextpfList.add(lextpf);
                    lextpfSearchResult.put(uniqueNumber, lextpfList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchLextRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psLextSelect, sqllextpf1rs);
        }
        return lextpfSearchResult;

    }
	public List<Lextpf> searchLextRecord(Lextpf lextRec) {
        StringBuilder sqlLextSelect1 = new StringBuilder(
                "SELECT CHDRCOY, CHDRNUM, CURRFROM, OPCDA ");
        sqlLextSelect1
                .append("FROM LEXTPF WHERE VALIDFLAG = '1' AND CHDRCOY = ? AND CHDRNUM = ?");

        sqlLextSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC ");
        PreparedStatement psLextSelect = getPrepareStatement(sqlLextSelect1.toString());
        ResultSet sqllextpf1rs = null;
        List<Lextpf> lextpfList = new ArrayList<Lextpf>();
        try {
	        psLextSelect.setString(1, lextRec.getChdrcoy());//IJS-183
	        psLextSelect.setString(2, lextRec.getChdrnum());
            sqllextpf1rs = executeQuery(psLextSelect);
            while (sqllextpf1rs.next()) {
                Lextpf lextpf = new Lextpf();
                lextpf.setChdrcoy(sqllextpf1rs.getString(1));
                lextpf.setChdrnum(sqllextpf1rs.getString(2));
                lextpf.setCurrfrom(sqllextpf1rs.getInt(3));
                lextpf.setOpcda(sqllextpf1rs.getString(4));
                lextpfList.add(lextpf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchLextRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psLextSelect, sqllextpf1rs);
        }
        return lextpfList;

    }

	   public Map<String, List<Lextpf>> searchLextbrrMap(List<String> chdrnumList) {
	        StringBuilder sqlLextSelect1 = new StringBuilder(
	                "SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,SEQNBR,VALIDFLAG,TRANNO,CURRFROM,CURRTO,OPCDA,OPPC,INSPRM,AGERATE,SBSTDL,TERMID,TRDT,TRTM,USER_T,JLIFE,EXTCD,ECESTRM,ECESDTE,REASIND,ZNADJPERC,UNIQUE_NUMBER ");
	        sqlLextSelect1.append("FROM LEXTBRR WHERE  ");
	        sqlLextSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	        sqlLextSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, ECESDTE ASC, UNIQUE_NUMBER DESC ");

	        PreparedStatement psLextSelect = getPrepareStatement(sqlLextSelect1.toString());
	        ResultSet sqllextpf1rs = null;
	        Map<String, List<Lextpf>> lextMap = new HashMap<String, List<Lextpf>>();
	        try {
	            sqllextpf1rs = executeQuery(psLextSelect);

	            while (sqllextpf1rs.next()) {
	                Lextpf lextpf = new Lextpf();
	                lextpf.setChdrcoy(sqllextpf1rs.getString("chdrcoy"));
	                lextpf.setChdrnum(sqllextpf1rs.getString("chdrnum"));
	                lextpf.setLife(sqllextpf1rs.getString("life"));
	                lextpf.setCoverage(sqllextpf1rs.getString("coverage"));
	                lextpf.setRider(sqllextpf1rs.getString("rider"));
	                lextpf.setSeqnbr(sqllextpf1rs.getInt("seqnbr"));
	                lextpf.setValidflag(sqllextpf1rs.getString("validflag"));
	                lextpf.setTranno(sqllextpf1rs.getInt("tranno"));
	                lextpf.setCurrfrom(sqllextpf1rs.getInt("currfrom"));
	                lextpf.setCurrto(sqllextpf1rs.getInt("currto"));
	                lextpf.setOpcda(sqllextpf1rs.getString("opcda"));
	                lextpf.setOppc(sqllextpf1rs.getBigDecimal("oppc"));
	                lextpf.setInsprm(sqllextpf1rs.getInt("insprm"));
	                lextpf.setAgerate(sqllextpf1rs.getInt("agerate"));
	                lextpf.setSbstdl(sqllextpf1rs.getString("sbstdl"));
	                lextpf.setTermid(sqllextpf1rs.getString("termid"));
	                lextpf.setTransactionDate(sqllextpf1rs.getInt("trdt"));
	                lextpf.setTransactionTime(sqllextpf1rs.getInt("trtm"));
	                lextpf.setUser(sqllextpf1rs.getInt("user_t"));
	                lextpf.setJlife(sqllextpf1rs.getString("jlife"));
	                lextpf.setExtCommDate(sqllextpf1rs.getInt("extcd"));
	                lextpf.setExtCessTerm(sqllextpf1rs.getInt("ecestrm"));
	                lextpf.setExtCessDate(sqllextpf1rs.getInt("ecesdte"));
	                lextpf.setReasind(sqllextpf1rs.getString("reasind"));
	                lextpf.setZnadjperc(sqllextpf1rs.getBigDecimal("znadjperc"));
	                
	                if (lextMap.containsKey(lextpf.getChdrnum())) {
	                    lextMap.get(lextpf.getChdrnum()).add(lextpf);
	                } else {
	                    List<Lextpf> chdrList = new ArrayList<Lextpf>();
	                    chdrList.add(lextpf);
	                    lextMap.put(lextpf.getChdrnum(), chdrList);
	                }
	            }

	        } catch (SQLException e) {
	            LOGGER.error("searchLextMap()", e);//IJTI-1485
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psLextSelect, sqllextpf1rs);
	        }
	        return lextMap;

	    }
	 //ILIFE-4970 start
		public List<Lextpf> getLoadAgeLextpf(Lextpf lextRec){
			List<Lextpf> lextpfList = new ArrayList<Lextpf>();
			String query = "select chdrcoy,chdrnum,life,coverage,rider,validflag,currfrom,tranno,currto,seqnbr,Opcda,Oppc,Agerate,Insprm,"
					+ "usrprf,jobnm,datime from lextpf  where chdrcoy = ? and chdrnum = ?  order by chdrcoy ASC, chdrnum ASC, "
					+ "life ASC, coverage ASC, rider ASC, seqnbr ASC, UNIQUE_NUMBER DESC";
			PreparedStatement stmt ;  	

			ResultSet rs = null;
			Lextpf LextpfData = null;
			stmt = null;
			try {
						stmt = getConnection().prepareStatement(query);/* IJTI-1523 */
						stmt.setString(1, lextRec.getChdrcoy());
						stmt.setString(2, lextRec.getChdrnum());
	   
						rs = stmt.executeQuery();
						while (rs.next()) {
						LextpfData= new Lextpf();
						 if (rs.getString(1) != null)
						 {
							 LextpfData.setChdrcoy(rs.getString(1).trim());
							 LextpfData.setChdrnum(rs.getString(2));
							 LextpfData.setLife(rs.getString(3));
							 LextpfData.setCoverage(rs.getString(4));
							 LextpfData.setRider(rs.getString(5));
							 LextpfData.setValidflag(rs.getString(6));
							 LextpfData.setCurrfrom(rs.getInt(7));
							 LextpfData.setTranno(rs.getInt(8));
							 LextpfData.setCurrto(rs.getInt(9));
							 LextpfData.setSeqnbr(rs.getInt(10));
							 LextpfData.setOpcda(rs.getString(11));
						     LextpfData.setOppc(rs.getBigDecimal(12));
						     LextpfData.setAgerate(rs.getInt(13));
						     LextpfData.setInsprm(rs.getInt(14));
						     lextpfList.add(LextpfData);
						 }
					}
			}
			catch (SQLException e) {
			       LOGGER.error("useLextpf()", e);//IJTI-1485
			       throw new SQLRuntimeException(e);
			} finally {
			       close(stmt, rs);
			}

			return lextpfList;
	}
		//ILIFE-4970 end

	   public Map<String, List<Lextpf>> searchLextpfMap(String coy, List<String> chdrnumList) {
	        StringBuilder sqlLextSelect1 = new StringBuilder(
	                "SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,SEQNBR,VALIDFLAG,TRANNO,CURRFROM,CURRTO,OPCDA,OPPC,INSPRM,AGERATE,SBSTDL,TERMID,TRDT,TRTM,USER_T,JLIFE,EXTCD,ECESTRM,ECESDTE,REASIND,ZNADJPERC,UNIQUE_NUMBER ");
	        sqlLextSelect1.append("FROM LEXTPF WHERE ");
	        sqlLextSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	        sqlLextSelect1.append(" AND VALIDFLAG = '1' AND CHDRCOY=? ");
	        sqlLextSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC ");
	        PreparedStatement psLextSelect = getPrepareStatement(sqlLextSelect1.toString());
	        ResultSet sqllextpf1rs = null;
	        Map<String, List<Lextpf>> lextMap = new HashMap<String, List<Lextpf>>();
	        try {
	        	psLextSelect.setString(1, coy);
	            sqllextpf1rs = executeQuery(psLextSelect);

	            while (sqllextpf1rs.next()) {
	                Lextpf lextpf = new Lextpf();
	                lextpf.setChdrcoy(sqllextpf1rs.getString("chdrcoy"));
	                lextpf.setChdrnum(sqllextpf1rs.getString("chdrnum"));
	                lextpf.setLife(sqllextpf1rs.getString("life"));
	                lextpf.setCoverage(sqllextpf1rs.getString("coverage"));
	                lextpf.setRider(sqllextpf1rs.getString("rider"));
	                lextpf.setSeqnbr(sqllextpf1rs.getInt("seqnbr"));
	                lextpf.setValidflag(sqllextpf1rs.getString("validflag"));
	                lextpf.setTranno(sqllextpf1rs.getInt("tranno"));
	                lextpf.setCurrfrom(sqllextpf1rs.getInt("currfrom"));
	                lextpf.setCurrto(sqllextpf1rs.getInt("currto"));
	                lextpf.setOpcda(sqllextpf1rs.getString("opcda"));
	                lextpf.setOppc(sqllextpf1rs.getBigDecimal("oppc"));
	                lextpf.setInsprm(sqllextpf1rs.getInt("insprm"));
	                lextpf.setAgerate(sqllextpf1rs.getInt("agerate"));
	                lextpf.setSbstdl(sqllextpf1rs.getString("sbstdl"));
	                lextpf.setTermid(sqllextpf1rs.getString("termid"));
	                lextpf.setTransactionDate(sqllextpf1rs.getInt("trdt"));
	                lextpf.setTransactionTime(sqllextpf1rs.getInt("trtm"));
	                lextpf.setUser(sqllextpf1rs.getInt("user_t"));
	                lextpf.setJlife(sqllextpf1rs.getString("jlife"));
	                lextpf.setExtCommDate(sqllextpf1rs.getInt("extcd"));
	                lextpf.setExtCessTerm(sqllextpf1rs.getInt("ecestrm"));
	                lextpf.setExtCessDate(sqllextpf1rs.getInt("ecesdte"));
	                lextpf.setReasind(sqllextpf1rs.getString("reasind"));
	                lextpf.setZnadjperc(sqllextpf1rs.getBigDecimal("znadjperc"));
	                lextpf.setUniqueNumber(sqllextpf1rs.getLong("UNIQUE_NUMBER"));
	                if (lextMap.containsKey(lextpf.getChdrnum())) {
	                    lextMap.get(lextpf.getChdrnum()).add(lextpf);
	                } else {
	                    List<Lextpf> chdrList = new ArrayList<Lextpf>();
	                    chdrList.add(lextpf);
	                    lextMap.put(lextpf.getChdrnum(), chdrList);
	                }
	            }

	        } catch (SQLException e) {
	            LOGGER.error("searchLextpfMap()", e);//IJTI-1485
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psLextSelect, sqllextpf1rs);
	        }
	        return lextMap;

	    }	   
	   public Lextpf getLextpfRecord(Lextpf lextpf){
		    Lextpf lextpfrec=null;
		    PreparedStatement psLextSelect = null;
			ResultSet sqllextpf1rs = null;
			StringBuilder sqlLextSelectBuilder = new StringBuilder();
			sqlLextSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER");
			sqlLextSelectBuilder.append(" FROM LEXTPF WHERE CHDRCOY = ? AND CHDRNUM = ? and LIFE=? and COVERAGE=? and RIDER=? ");
			sqlLextSelectBuilder.append(" ORDER BY ");
			sqlLextSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC");
			psLextSelect = getPrepareStatement(sqlLextSelectBuilder.toString());
			try {
				psLextSelect.setString(1, lextpf.getChdrcoy());
				psLextSelect.setString(2, lextpf.getChdrnum());
				psLextSelect.setString(3, lextpf.getLife());
				psLextSelect.setString(4, lextpf.getCoverage());
				psLextSelect.setString(5, lextpf.getRider());
				sqllextpf1rs = executeQuery(psLextSelect);
				while (sqllextpf1rs.next()) {
					lextpfrec = new Lextpf();
					lextpfrec.setChdrcoy(sqllextpf1rs.getString("CHDRCOY"));
					lextpfrec.setChdrnum(sqllextpf1rs.getString("CHDRNUM"));
					lextpfrec.setLife(sqllextpf1rs.getString("LIFE"));
					lextpfrec.setCoverage(sqllextpf1rs.getString("COVERAGE"));
					lextpfrec.setRider(sqllextpf1rs.getString("RIDER"));
					
				}

			} catch (SQLException e) {
				LOGGER.error("getLextpfRecord()", e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(psLextSelect, sqllextpf1rs);
			}
			return lextpfrec;

	   }
   
	   public List<Lextpf> getLextpfListRecord(Lextpf lextpf){
		   	List<Lextpf> lextpfList = new ArrayList<Lextpf>();
		    PreparedStatement psLextSelect = null;
			ResultSet sqllextpf1rs = null;
			StringBuilder sqlLextSelectBuilder = new StringBuilder();
			sqlLextSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, TRANNO ");
			sqlLextSelectBuilder.append(" FROM LEXTPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE=? AND COVERAGE=? AND RIDER=? ");
			sqlLextSelectBuilder.append(" ORDER BY ");
			sqlLextSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, TRANNO ASC, UNIQUE_NUMBER DESC");
			psLextSelect = getPrepareStatement(sqlLextSelectBuilder.toString());
			try {
				psLextSelect.setString(1, lextpf.getChdrcoy());
				psLextSelect.setString(2, lextpf.getChdrnum());
				psLextSelect.setString(3, lextpf.getLife());
				psLextSelect.setString(4, lextpf.getCoverage());
				psLextSelect.setString(5, lextpf.getRider());
				sqllextpf1rs = executeQuery(psLextSelect);
				while (sqllextpf1rs.next()) {
					Lextpf lextpfrec = new Lextpf();
					lextpfrec.setChdrcoy(sqllextpf1rs.getString("CHDRCOY"));
					lextpfrec.setChdrnum(sqllextpf1rs.getString("CHDRNUM"));
					lextpfrec.setLife(sqllextpf1rs.getString("LIFE"));
					lextpfrec.setCoverage(sqllextpf1rs.getString("COVERAGE"));
					lextpfrec.setRider(sqllextpf1rs.getString("RIDER"));
					lextpfrec.setTranno(sqllextpf1rs.getInt("TRANNO"));
					lextpfList.add(lextpfrec);
				}

			} catch (SQLException e) {
				LOGGER.error("getLextpfListRecord()", e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(psLextSelect, sqllextpf1rs);
			}
			return lextpfList;
	   }
       
         public Lextpf getLextbrrRecord(Lextpf lextpf){
		    Lextpf lextpfrec=null;
		    PreparedStatement psLextSelect = null;
			ResultSet sqllextpf1rs = null;
			StringBuilder sqlLextSelectBuilder = new StringBuilder();
			sqlLextSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER");
			sqlLextSelectBuilder.append(" FROM LEXTBRR WHERE LTRIM(RTRIM(CHDRCOY)) = ? AND LTRIM(RTRIM(CHDRNUM)) = ? and LTRIM(RTRIM(LIFE))=? and LTRIM(RTRIM(COVERAGE))=? ");
			sqlLextSelectBuilder.append(" and LTRIM(RTRIM(IDER))=? and EXTCD=? ORDER BY ");
			sqlLextSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC");
			psLextSelect = getPrepareStatement(sqlLextSelectBuilder.toString());
			try {
				psLextSelect.setString(1, lextpf.getChdrcoy());
				psLextSelect.setString(2, lextpf.getChdrnum());
				psLextSelect.setString(3, lextpf.getLife());
				psLextSelect.setString(4, lextpf.getCoverage());
				psLextSelect.setString(5, lextpf.getRider());
				psLextSelect.setInt(6, lextpf.getExtCessDate());
				sqllextpf1rs = executeQuery(psLextSelect);
				while (sqllextpf1rs.next()) {
					lextpfrec = new Lextpf();
					lextpfrec.setChdrcoy(sqllextpf1rs.getString("CHDRCOY"));
					lextpfrec.setChdrnum(sqllextpf1rs.getString("CHDRNUM"));
					lextpfrec.setLife(sqllextpf1rs.getString("LIFE"));
					lextpfrec.setCoverage(sqllextpf1rs.getString("COVERAGE"));
					lextpfrec.setRider(sqllextpf1rs.getString("RIDER"));
					
				}

			} catch (SQLException e) {
				LOGGER.error("getLextpfRecord()", e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(psLextSelect, sqllextpf1rs);
			}
			return lextpfrec;

	   }
	    public List<Lextpf> getLextpfData(Lextpf lext){
		   StringBuilder sb = new StringBuilder();
		   sb.append("SELECT * FROM LEXTPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1' ORDER BY SEQNBR ASC"); //ILIFE-8901
		   List<Lextpf> lextList = new ArrayList<>();
		   Lextpf rsLext=null;
		   PreparedStatement ps = null;
		   ResultSet rs = null;
		   try{
			   ps = getConnection().prepareStatement(sb.toString());
			   ps.setString(1, lext.getChdrcoy());
			   ps.setString(2, lext.getChdrnum());
			   rs = ps.executeQuery();
			   while(rs.next()){
				   rsLext = new Lextpf();
				   rsLext.setOpcda(rs.getString("OPCDA"));
				   rsLext.setOppc(rs.getBigDecimal("OPPC"));
				   rsLext.setInsprm(rs.getInt("INSPRM"));
				   rsLext.setAgerate(rs.getInt("AGERATE"));
				   rsLext.setExtCessDate(rs.getInt("EXTCD"));
				   rsLext.setReasind(rs.getString("REASIND"));
				   rsLext.setZmortpct(rs.getInt("ZMORTPCT"));
				   rsLext.setLife(rs.getString("LIFE"));
				   rsLext.setCoverage(rs.getString("COVERAGE"));
				   rsLext.setRider(rs.getString("RIDER"));
				   rsLext.setSeqnbr(rs.getInt("SEQNBR"));
				   rsLext.setChdrcoy(rs.getString("CHDRCOY"));
				   rsLext.setChdrnum(rs.getString("CHDRNUM"));
				   rsLext.setPremadj(rs.getBigDecimal("PREMADJ"));
				   rsLext.setExtCessTerm(rs.getInt("ECESTRM"));
				   rsLext.setZnadjperc(rs.getBigDecimal("ZNADJPERC"));
				   lextList.add(rsLext);
			   }
		   }
		   catch(SQLException e){
			   LOGGER.error("getLextpfData()", e);//IJTI-1485
				throw new SQLRuntimeException(e);
		   }
		   finally {
				close(ps, rs);
			}
		   return lextList;
	   }
//ILIFE-8901 start
		@Override
		public boolean checkLextRecord(String chdrcoy, String chdrNum) {

			String sqlLextSelect = "SELECT count(1) FROM LEXT WHERE 1=1  and CHDRCOY=? and CHDRNUM=? ";					
			
			PreparedStatement psLextSelect = getPrepareStatement(sqlLextSelect);
			ResultSet sqlLextRs = null;
			try {
				psLextSelect.setString(1, chdrcoy);
				psLextSelect.setString(2, chdrNum);
				sqlLextRs = executeQuery(psLextSelect);
				sqlLextRs.next();
				if(sqlLextRs.getInt(1) > 0) {
					return true;
				}
				return false;
			} catch (SQLException e) {
				LOGGER.error("checkLextRecord()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psLextSelect, sqlLextRs);
			}
		
		}
//ILIFE-8901 end

		 public List<Lextpf> getLextData(Lextpf lext){
			   StringBuilder sb = new StringBuilder("SELECT * FROM LEXT WHERE CHDRCOY=? and CHDRNUM=? and LIFE=? and COVERAGE=? and RIDER=? ");			   
			   sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC"); //ILB-1222
			   List<Lextpf> lextList = new ArrayList<>();
			   Lextpf rsLext=null;
			   PreparedStatement ps = null;
			   ResultSet rs = null;
			   try{
				   ps = getConnection().prepareStatement(sb.toString());
				   ps.setString(1, lext.getChdrcoy());
				   ps.setString(2, lext.getChdrnum());
				   ps.setString(3, lext.getLife());
				   ps.setString(4, lext.getCoverage());
				   ps.setString(5, lext.getRider());
				   rs = ps.executeQuery();
				   while(rs.next()){
					   rsLext = new Lextpf();
					   rsLext.setOpcda(rs.getString("OPCDA"));
					   rsLext.setOppc(rs.getBigDecimal("OPPC"));
					   rsLext.setInsprm(rs.getInt("INSPRM"));
					   rsLext.setAgerate(rs.getInt("AGERATE"));
					   rsLext.setExtCessDate(rs.getInt("EXTCD"));
					   rsLext.setReasind(rs.getString("REASIND"));
					   rsLext.setZmortpct(rs.getInt("ZMORTPCT"));
					   rsLext.setLife(rs.getString("LIFE"));
					   rsLext.setCoverage(rs.getString("COVERAGE"));
					   rsLext.setRider(rs.getString("RIDER"));
					   rsLext.setSeqnbr(rs.getInt("SEQNBR"));
					   rsLext.setChdrcoy(rs.getString("CHDRCOY"));
					   rsLext.setChdrnum(rs.getString("CHDRNUM"));
					   rsLext.setTranno(rs.getInt("TRANNO"));
					   rsLext.setJlife(rs.getString("JLIFE"));
					   rsLext.setExtCessDate(rs.getInt("ECESDTE"));
					   rsLext.setZmortpct(rs.getInt("ZMORTPCT"));
					   rsLext.setPremadj(rs.getBigDecimal("PREMADJ"));
					   lextList.add(rsLext);
				   }
			   }
			   catch(SQLException e){
				   LOGGER.error("getLextData()", e);
					throw new SQLRuntimeException(e);
			   }
			   finally {
					close(ps, rs);
				}
			   return lextList;
		   }
		 public int deleteByAutoRecord(String chdrcoy, String chdrnum,String flag) {
			 String sql = "";
			 if("Q".equals(flag)){
				 sql = "DELETE FROM LEXTPF WHERE CHDRCOY=? AND CHDRNUM=? AND UWOVERWRITE <> '' and (OPCDA LIKE ('R%')  OR OPCDA LIKE ('L%') OR OPCDA LIKE ('M%'))";
			 }else if("O".equals(flag)){
				 sql = "DELETE FROM LEXTPF WHERE CHDRCOY=? AND CHDRNUM=? AND UWOVERWRITE <> '' and (OPCDA LIKE ('A%')  OR OPCDA LIKE ('B%'))";
			 }
			LOGGER.info(sql);
			PreparedStatement ps = null;
			ResultSet rs = null;
			int i = 0;
			try{
				ps = getPrepareStatement(sql);	
			    ps.setString(1, chdrcoy);
			    ps.setString(2, chdrnum.trim());
			    i = ps.executeUpdate();				
			}catch (SQLException e) {
				LOGGER.error("deleteByFlag()", e); 
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return i;
		 }
	
		@Override
		public int getMaxSeqnbr(String chdrcoy, String chdrnum) {
			String sql = "SELECT MAX(SEQNBR) SEQNBR FROM LEXT WHERE 1=1  AND CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1'";					
			PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	Integer seqno = 0;
	    	try {
	    		ps = getPrepareStatement(sql);
	    		ps.setString(1, chdrcoy);
				ps.setString(2, chdrnum.trim());
	        	rs = ps.executeQuery();
	    		if (rs.next()) {
	    			seqno = rs.getInt("SEQNBR");
	    		}	
	    	}catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}finally {
				close(ps, rs);
			}
	    	return seqno;
		}
}