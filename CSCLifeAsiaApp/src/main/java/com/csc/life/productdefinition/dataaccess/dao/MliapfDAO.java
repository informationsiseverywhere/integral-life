package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Mliapf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MliapfDAO extends BaseDAO<Mliapf> {
	public boolean  isExistMliasec(String securityNo);
 //ILIFE-8901 start
	public void bulkupdateMliaafi(List<Mliapf> mliaBulkupdateList);
	public void bulkdeleteMlicdByUnique(List<Long> uniquenumberList);
	public Map<String, List<Mliapf>> selectMliaRecord(List<String> chdrnumList);
 //ILIFE-8901 end
	
}