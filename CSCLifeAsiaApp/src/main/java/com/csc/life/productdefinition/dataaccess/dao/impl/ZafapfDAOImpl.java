package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.productdefinition.dataaccess.dao.ZafapfDAO;
import com.csc.life.productdefinition.dataaccess.model.Zafapf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZafapfDAOImpl extends BaseDAOImpl<Zafapf> implements ZafapfDAO
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ZafapfDAOImpl.class);
	
	 public void insertZafapfRecord(List<Zafapf> zafapfList)
	 {
		 if (zafapfList != null) {
	      
			StringBuilder sql_zafapf_insert = new StringBuilder();
			sql_zafapf_insert.append("INSERT INTO ZAFAPF");
			sql_zafapf_insert.append("(CHDRPFX,CHDRCOY,CHDRNUM,CNTTYPE,VALIDFLAG,CRTABLE,ZAFROPT,ZCHKOPT,ZREGDTE,AGE,TRANNO,ZAFRFREQ,VRTFUND,");
			sql_zafapf_insert.append("UALFND01,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,");
			sql_zafapf_insert.append("UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,");
			sql_zafapf_insert.append("FUNDAMNT01,FUNDAMNT02,FUNDAMNT03,FUNDAMNT04,FUNDAMNT05,FUNDAMNT06,FUNDAMNT07,FUNDAMNT08,FUNDAMNT09,FUNDAMNT10,");
			sql_zafapf_insert.append("ZWTAVGA01,ZWTAVGA02,ZWTAVGA03,ZWTAVGA04,ZWTAVGA05,ZWTAVGA06,ZWTAVGA07,ZWTAVGA08,ZWTAVGA09,ZWTAVGA10,");
			sql_zafapf_insert.append("ZDEVITA01,ZDEVITA02,ZDEVITA03,ZDEVITA04,ZDEVITA05,ZDEVITA06,ZDEVITA07,ZDEVITA08,ZDEVITA09,ZDEVITA10,");
			sql_zafapf_insert.append("ZBUYSELL01,ZBUYSELL02,ZBUYSELL03,ZBUYSELL04,ZBUYSELL05,ZBUYSELL06,ZBUYSELL07,ZBUYSELL08,ZBUYSELL09,ZBUYSELL10,");
			sql_zafapf_insert.append("ZBSAMNT01,ZBSAMNT02,ZBSAMNT03,ZBSAMNT04,ZBSAMNT05,ZBSAMNT06,ZBSAMNT07,ZBSAMNT08,ZBSAMNT09,ZBSAMNT10,");
			sql_zafapf_insert.append("LINEDETAIL,ZWDWLRSN,ZLASTDTE,ZNEXTDTE,ZWDLDDTE,ZENABLE,ZLSTUPDTE,USRPRF,JOBNM,DATIME,ZAFRITEM)");
			sql_zafapf_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,");
			sql_zafapf_insert.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement psInsert = getPrepareStatement(sql_zafapf_insert.toString());
			try
			{
				for (Zafapf z : zafapfList) {
					psInsert.setString(1, z.getChdrpfx());
					psInsert.setString(2, z.getChdrcoy());
					psInsert.setString(3, z.getChdrnum());
					psInsert.setString(4, z.getCnttype());
					psInsert.setString(5, z.getValidflag());
					psInsert.setString(6, z.getCrtable());
					psInsert.setString(7, z.getZafropt());
					psInsert.setString(8, z.getZchkopt());
					psInsert.setInt(9, z.getZregdte());
					psInsert.setInt(10, z.getAge());
					psInsert.setInt(11, z.getTranno());
					psInsert.setString(12, z.getZafrfreq());
					psInsert.setString(13, z.getVrtfund());
					psInsert.setString(14, z.getUalfnd01());
					psInsert.setString(15, z.getUalfnd02());
					psInsert.setString(16, z.getUalfnd03());
					psInsert.setString(17, z.getUalfnd04());
					psInsert.setString(18, z.getUalfnd05());
					psInsert.setString(19, z.getUalfnd06());
					psInsert.setString(20, z.getUalfnd07());
					psInsert.setString(21, z.getUalfnd08());
					psInsert.setString(22, z.getUalfnd09());
					psInsert.setString(23, z.getUalfnd10());
					psInsert.setDouble(24, z.getUalprc01());
					psInsert.setDouble(25, z.getUalprc02());
					psInsert.setDouble(26, z.getUalprc03());
					psInsert.setDouble(27, z.getUalprc04());
					psInsert.setDouble(28, z.getUalprc05());
					psInsert.setDouble(29, z.getUalprc06());
					psInsert.setDouble(30, z.getUalprc07());
					psInsert.setDouble(31, z.getUalprc08());
					psInsert.setDouble(32, z.getUalprc09());
					psInsert.setDouble(33, z.getUalprc10());
					psInsert.setDouble(34, z.getFundamnt01());
					psInsert.setDouble(35, z.getFundamnt02());
					psInsert.setDouble(36, z.getFundamnt03());
					psInsert.setDouble(37, z.getFundamnt04());
					psInsert.setDouble(38, z.getFundamnt05());
					psInsert.setDouble(39, z.getFundamnt06());
					psInsert.setDouble(40, z.getFundamnt07());
					psInsert.setDouble(41, z.getFundamnt08());
					psInsert.setDouble(42, z.getFundamnt09());
					psInsert.setDouble(43, z.getFundamnt10());
					psInsert.setDouble(44, z.getZwtavga01());
					psInsert.setDouble(45, z.getZwtavga02());
					psInsert.setDouble(46, z.getZwtavga03());
					psInsert.setDouble(47, z.getZwtavga04());
					psInsert.setDouble(48, z.getZwtavga05());
					psInsert.setDouble(49, z.getZwtavga06());
					psInsert.setDouble(50, z.getZwtavga07());
					psInsert.setDouble(51, z.getZwtavga08());
					psInsert.setDouble(52, z.getZwtavga09());
					psInsert.setDouble(53, z.getZwtavga10());
					psInsert.setDouble(54, z.getZdevita01());
					psInsert.setDouble(55, z.getZdevita02());
					psInsert.setDouble(56, z.getZdevita03());
					psInsert.setDouble(57, z.getZdevita04());
					psInsert.setDouble(58, z.getZdevita05());
					psInsert.setDouble(59, z.getZdevita06());
					psInsert.setDouble(60, z.getZdevita07());
					psInsert.setDouble(61, z.getZdevita08());
					psInsert.setDouble(62, z.getZdevita09());
					psInsert.setDouble(63, z.getZdevita10());
					psInsert.setString(64, z.getZbuysell01());
					psInsert.setString(65, z.getZbuysell02());
					psInsert.setString(66, z.getZbuysell03());
					psInsert.setString(67, z.getZbuysell04());
					psInsert.setString(68, z.getZbuysell05());
					psInsert.setString(69, z.getZbuysell06());
					psInsert.setString(70, z.getZbuysell07());
					psInsert.setString(71, z.getZbuysell08());
					psInsert.setString(72, z.getZbuysell09());
					psInsert.setString(73, z.getZbuysell10());
					psInsert.setDouble(74, z.getZbsamnt01());
					psInsert.setDouble(75, z.getZbsamnt02());
					psInsert.setDouble(76, z.getZbsamnt03());
					psInsert.setDouble(77, z.getZbsamnt04());
					psInsert.setDouble(78, z.getZbsamnt05());
					psInsert.setDouble(79, z.getZbsamnt06());
					psInsert.setDouble(80, z.getZbsamnt07());
					psInsert.setDouble(81, z.getZbsamnt08());
					psInsert.setDouble(82, z.getZbsamnt09());
					psInsert.setDouble(83, z.getZbsamnt10());
					psInsert.setString(84, z.getLinedetail());
					psInsert.setString(85, z.getZwdwlrsn());
					psInsert.setInt(86, z.getZlastdte());
					psInsert.setInt(87, z.getZnextdte());
					psInsert.setInt(88, z.getZwdlddte());
					psInsert.setString(89, z.getZenable());
					psInsert.setInt(90, z.getZlstupdte());
					psInsert.setString(91, z.getUsrprf());
					psInsert.setString(92, z.getJobnm());
					psInsert.setTimestamp(93, z.getDatime());
					psInsert.setString(94, z.getZafritem());
				
					psInsert.addBatch();	
				}
				psInsert.executeBatch();
			}
			catch (SQLException e) {
                LOGGER.error("insertZafapfRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, null);
            }
	
		 }
	 }	 

}
