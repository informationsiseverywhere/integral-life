package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UtrspfDAOImpl extends BaseDAOImpl<Utrspf> implements UtrspfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(UtrspfDAOImpl.class);

    public Map<Long, List<Utrspf>> searchUtrsRecord(List<Long> covrUQ) {
        StringBuilder sqlUtrsSelect1 = new StringBuilder(
                "SELECT U.VRTFND, U.UNITYP, U.CURDUNTBAL, U.PLNSFX, C.UNIQUE_NUMBER ");
        sqlUtrsSelect1
                .append("FROM Utrspf U,COVRPF C WHERE U.CHDRCOY=C.CHDRCOY AND U.CHDRNUM = C.CHDRNUM AND U.LIFE=C.LIFE AND U.COVERAGE=C.COVERAGE AND U.RIDER=C.RIDER AND ");
        sqlUtrsSelect1.append(getSqlInLong("C.UNIQUE_NUMBER", covrUQ));

        sqlUtrsSelect1
                .append(" ORDER BY U.CHDRCOY ASC, U.CHDRNUM ASC, U.LIFE ASC, U.COVERAGE ASC, U.RIDER ASC, U.VRTFND ASC, U.UNITYP ASC, U.PLNSFX ASC, U.UNIQUE_NUMBER DESC");

        PreparedStatement psUtrsSelect = getPrepareStatement(sqlUtrsSelect1.toString());
        ResultSet sqlutrspf1rs = null;
        Map<Long, List<Utrspf>> utrspfSearchResult = new HashMap<>();
        try {
            sqlutrspf1rs = executeQuery(psUtrsSelect);
            while (sqlutrspf1rs.next()) {
                Utrspf utrspf = new Utrspf();
                utrspf.setUnitVirtualFund(sqlutrspf1rs.getString(1));
                utrspf.setUnitType(sqlutrspf1rs.getString(2));
                utrspf.setCurrentDunitBal(sqlutrspf1rs.getBigDecimal(3));
                utrspf.setPlanSuffix(sqlutrspf1rs.getInt(4));
                long uniqueNumber = sqlutrspf1rs.getLong(5);
                if (utrspfSearchResult.containsKey(uniqueNumber)) {
                    utrspfSearchResult.get(uniqueNumber).add(utrspf);
                } else {
                    List<Utrspf> utrspfList = new ArrayList<>();
                    utrspfList.add(utrspf);
                    utrspfSearchResult.put(uniqueNumber, utrspfList);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("searchUtrsRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psUtrsSelect, sqlutrspf1rs);
        }
        return utrspfSearchResult;

    }
	public List<Utrspf> searchUtrsRecord(Utrspf utrspfData) {
		StringBuilder sqlUtrsSelect1 = new StringBuilder();
		sqlUtrsSelect1.append("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,VRTFND, UNITYP, CURDUNTBAL, CURUNTBAL, PLNSFX ");
		sqlUtrsSelect1.append(" FROM UTRSPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE= ? AND COVERAGE = ? AND RIDER= ? ");
        
        if(utrspfData.getUnitType()!=null && !utrspfData.getUnitType().trim().equals("")){
	        	sqlUtrsSelect1.append(" AND UNITYP=? ");
		}
        if(utrspfData.getUnitVirtualFund()!=null && !utrspfData.getUnitVirtualFund().trim().equals(""))
		{
	        	sqlUtrsSelect1.append(" AND VRTFND= ? ");
		}
        sqlUtrsSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, VRTFND ASC, UNITYP ASC, UNIQUE_NUMBER DESC ");

        PreparedStatement psUtrsSelect = getPrepareStatement(sqlUtrsSelect1.toString());
        ResultSet sqlutrspf1rs = null;
        List<Utrspf> utrspfList = new ArrayList<>();
        try {
        	psUtrsSelect.setString(1, utrspfData.getChdrcoy());
        	psUtrsSelect.setString(2, utrspfData.getChdrnum());
        	psUtrsSelect.setString(3, utrspfData.getLife());
        	psUtrsSelect.setString(4, utrspfData.getCoverage());
        	psUtrsSelect.setString(5, utrspfData.getRider());
        	int i=5;
        	 if(utrspfData.getUnitType()!=null && !utrspfData.getUnitType().trim().equals(""))
			{
				i = i+1;
				psUtrsSelect.setString(i, utrspfData.getUnitType());
			}
        	  if(utrspfData.getUnitVirtualFund()!=null && !utrspfData.getUnitVirtualFund().trim().equals(""))
			{
				i = i+1;
				psUtrsSelect.setString(i, utrspfData.getUnitVirtualFund());
			}
            sqlutrspf1rs = executeQuery(psUtrsSelect);
            while (sqlutrspf1rs.next()) {
                Utrspf utrspf = new Utrspf();
                utrspf.setChdrcoy(sqlutrspf1rs.getString(1));
                utrspf.setChdrnum(sqlutrspf1rs.getString(2));
                utrspf.setLife(sqlutrspf1rs.getString(3));
                utrspf.setCoverage(sqlutrspf1rs.getString(4));
                utrspf.setRider(sqlutrspf1rs.getString(5));
                utrspf.setUnitVirtualFund(sqlutrspf1rs.getString(6));
                utrspf.setUnitType(sqlutrspf1rs.getString(7));
                utrspf.setCurrentDunitBal(sqlutrspf1rs.getBigDecimal(8));
                utrspf.setCurrentUnitBal(sqlutrspf1rs.getBigDecimal(9));
                utrspf.setPlanSuffix(sqlutrspf1rs.getInt(10));
                utrspfList.add(utrspf);
            }
        } catch (SQLException e) {
            LOGGER.error("searchUtrsRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psUtrsSelect, sqlutrspf1rs);
        }
        return utrspfList;

    }
	
	public Map<String, List<Utrspf>> searchUtrsRecordByChdrnum(List<String> chdrnumList){

		if(chdrnumList == null || chdrnumList.isEmpty()){
			return null;
		}
		StringBuilder sqlSelect1 = new StringBuilder();
    	sqlSelect1.append("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,VRTFND,PLNSFX,UNITYP,CURUNTBAL,CURDUNTBAL,UNIQUE_NUMBER ,FUNDPOOL ");
        sqlSelect1.append(" FROM UTRSPF WHERE ");
        sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, VRTFND ASC, UNITYP ASC, UNIQUE_NUMBER DESC ");
        
        
        PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
        ResultSet sqlrs = null;
        Map<String,List<Utrspf>> utrspfMap = new HashMap<>();
        
        try {
        	sqlrs = executeQuery(psSelect);

            while (sqlrs.next()) {
            	Utrspf u = new Utrspf();
            	int i=1;
            	u.setChdrcoy(sqlrs.getString(i++));
            	u.setChdrnum(sqlrs.getString(i++));
            	u.setLife(sqlrs.getString(i++));
            	u.setCoverage(sqlrs.getString(i++));
            	u.setRider(sqlrs.getString(i++));
            	u.setUnitVirtualFund(sqlrs.getString(i++));
            	u.setPlanSuffix(sqlrs.getInt(i++));
            	u.setUnitType(sqlrs.getString(i++));
            	u.setCurrentUnitBal(sqlrs.getBigDecimal(i++));
            	u.setCurrentDunitBal(sqlrs.getBigDecimal(i++));
            	u.setUniqueNumber(sqlrs.getLong(i++));
            	u.setFundPool(sqlrs.getString(i++));
                if(utrspfMap.containsKey(u.getChdrnum())){
                	utrspfMap.get(u.getChdrnum()).add(u);
                }else{
                	List<Utrspf> utrspfList=new ArrayList<>();
                	utrspfList.add(u);
                	utrspfMap.put(u.getChdrnum(), utrspfList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchUtrsRecordByChdrnum()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psSelect, sqlrs);
        }
		return utrspfMap;
	}
	
	public void insertUtrspfRecord(List<Utrspf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO UTRSPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,VRTFND,PLNSFX,UNITYP,CURUNTBAL,CURDUNTBAL,FUNDPOOL)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		
		try {
			for (Utrspf u : urList) {
				int i = 1;
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getLife());
				ps.setString(i++, u.getCoverage());
				ps.setString(i++, u.getRider());
				ps.setString(i++, u.getUnitVirtualFund());
				ps.setInt(i++, u.getPlanSuffix());
				ps.setString(i++, u.getUnitType());
				ps.setBigDecimal(i++, u.getCurrentUnitBal());
				ps.setBigDecimal(i++, u.getCurrentDunitBal());
				ps.setString(i++, u.getFundPool());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertUtrspfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public void updateUtrspfRecord(List<Utrspf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE UTRSPF SET CHDRCOY=?,CHDRNUM=?,LIFE=?,COVERAGE=?,RIDER=?,VRTFND=?,PLNSFX=?,UNITYP=?,CURUNTBAL=?,CURDUNTBAL=?");
		sql.append(" WHERE UNIQUE_NUMBER=? ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		
		try {
			for (Utrspf u : urList) {
				int i = 1;
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getLife());
				ps.setString(i++, u.getCoverage());
				ps.setString(i++, u.getRider());
				ps.setString(i++, u.getUnitVirtualFund());
				ps.setInt(i++, u.getPlanSuffix());
				ps.setString(i++, u.getUnitType());
				ps.setBigDecimal(i++, u.getCurrentUnitBal());
				ps.setBigDecimal(i++, u.getCurrentDunitBal());
				ps.setLong(i++, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateUtrspfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public Map<String, List<Utrspf>> searchUtrs5104Record(List<String> chdrnumList){

		StringBuilder sqlSelect1 = new StringBuilder();
    	sqlSelect1.append("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,VRTFND,PLNSFX,UNITYP,CURUNTBAL,CURDUNTBAL,UNIQUE_NUMBER,fundpool ");
        sqlSelect1.append("FROM UTRS WHERE ");
        sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlSelect1.append(" ORDER BY CHDRCOY ASC,CHDRNUM ASC,LIFE ASC,COVERAGE ASC,RIDER ASC,PLNSFX ASC,VRTFND ASC,UNITYP ASC,UNIQUE_NUMBER DESC");
        
        
        PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
        ResultSet sqlrs = null;
        Map<String,List<Utrspf>> utrspfMap = new HashMap<>();
        
        try {
        	sqlrs = executeQuery(psSelect);

            while (sqlrs.next()) {
            	Utrspf u = new Utrspf();
            	
            	u.setChdrcoy(sqlrs.getString("chdrcoy"));
            	u.setChdrnum(sqlrs.getString("chdrnum"));
            	u.setLife(sqlrs.getString("life"));
            	u.setCoverage(sqlrs.getString("coverage"));
            	u.setRider(sqlrs.getString("rider"));
            	u.setUnitVirtualFund(sqlrs.getString("vrtfnd"));
            	u.setPlanSuffix(sqlrs.getInt("plnsfx"));
            	u.setUnitType(sqlrs.getString("unityp"));
            	u.setCurrentUnitBal(sqlrs.getBigDecimal("curuntbal"));
            	u.setCurrentDunitBal(sqlrs.getBigDecimal("curduntbal"));
            	u.setUniqueNumber(sqlrs.getLong("UNIQUE_NUMBER"));
            	u.setFundPool(sqlrs.getString("fundpool"));
                if(utrspfMap.containsKey(u.getChdrnum())){
                	utrspfMap.get(u.getChdrnum()).add(u);
                }else{
                	List<Utrspf> utrspfList=new ArrayList<>();
                	utrspfList.add(u);
                	utrspfMap.put(u.getChdrnum(), utrspfList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchUtrs5104Record()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psSelect, sqlrs);
        }
		return utrspfMap;
	}
	
	public Map<String, List<Utrspf>> searchUtrsRecord(String chdrcoy, List<String> chdrnumList){

		StringBuilder sqlSelect1 = new StringBuilder();
    	sqlSelect1.append("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,VRTFND,PLNSFX,UNITYP,CURUNTBAL,CURDUNTBAL,UNIQUE_NUMBER ");
        sqlSelect1.append("FROM UTRS WHERE CHDRCOY = ? AND ");
        sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlSelect1.append(" ORDER BY CHDRCOY ASC,CHDRNUM ASC,LIFE ASC,COVERAGE ASC,RIDER ASC,PLNSFX ASC,VRTFND ASC,UNITYP ASC,UNIQUE_NUMBER DESC");
        
        
        PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
        ResultSet sqlrs = null;
        Map<String,List<Utrspf>> utrspfMap = new HashMap<>();
        
        try {
        	psSelect.setString(1, chdrcoy);
        	sqlrs = executeQuery(psSelect);

            while (sqlrs.next()) {
            	Utrspf u = new Utrspf();
            	
            	u.setChdrcoy(sqlrs.getString("chdrcoy"));
            	u.setChdrnum(sqlrs.getString("chdrnum"));
            	u.setLife(sqlrs.getString("life"));
            	u.setCoverage(sqlrs.getString("coverage"));
            	u.setRider(sqlrs.getString("rider"));
            	u.setUnitVirtualFund(sqlrs.getString("vrtfnd"));
            	u.setPlanSuffix(sqlrs.getInt("plnsfx"));
            	u.setUnitType(sqlrs.getString("unityp"));
            	u.setCurrentUnitBal(sqlrs.getBigDecimal("curuntbal"));
            	u.setCurrentDunitBal(sqlrs.getBigDecimal("curduntbal"));
            	u.setUniqueNumber(sqlrs.getLong("UNIQUE_NUMBER"));
                if(utrspfMap.containsKey(u.getChdrnum())){
                	utrspfMap.get(u.getChdrnum()).add(u);
                }else{
                	List<Utrspf> utrspfList=new ArrayList<>();
                	utrspfList.add(u);
                	utrspfMap.put(u.getChdrnum(), utrspfList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchUtrs5104Record()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psSelect, sqlrs);
        }
		return utrspfMap;
	}	
	
	public List<Utrspf> searchUtrsRecord(String chdrcoy, String chdrnum) {

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,VRTFND,PLNSFX,UNITYP,CURUNTBAL,CURDUNTBAL ");
		sqlSelect1.append(" FROM UTRS WHERE CHDRCOY=? AND CHDRNUM=? ");
		sqlSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, VRTFND ASC, UNITYP ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<Utrspf> utrspfList = new LinkedList<>();
		try {
			psSelect.setString(1, chdrcoy);
			psSelect.setString(2, chdrnum);
			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrspf u = new Utrspf();
				u.setChdrcoy(sqlrs.getString("chdrcoy"));
				u.setChdrnum(sqlrs.getString("chdrnum"));
				u.setLife(sqlrs.getString("life"));
				u.setCoverage(sqlrs.getString("coverage"));
				u.setRider(sqlrs.getString("rider"));
				u.setUnitVirtualFund(sqlrs.getString("vrtfnd"));
				u.setPlanSuffix(sqlrs.getInt("plnsfx"));
				u.setUnitType(sqlrs.getString("unityp"));
				u.setCurrentUnitBal(sqlrs.getBigDecimal("CURUNTBAL"));
				u.setCurrentDunitBal(sqlrs.getBigDecimal("CURDUNTBAL"));
				utrspfList.add(u);
			}

		} catch (SQLException e) {
			LOGGER.error("searchUtrsRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrspfList;
	}	
	
	public List<Utrspf> getUtrsRecord(String chdrcoy, String chdrnum,String life, String coverage,String rider) {

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT DISTINCT VRTFND, CURUNTBAL");
		sqlSelect1.append(" FROM UTRS WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND UNITYP=? ");
		//sqlSelect1.append(" ORDER BY  VRTFND ASC, CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNITYP ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<Utrspf> utrspfList = new LinkedList<>();
		try {
			psSelect.setString(1, chdrcoy);
			psSelect.setString(2, chdrnum);
			psSelect.setString(3, life);
			psSelect.setString(4, coverage);
			psSelect.setString(5, rider);
			psSelect.setString(6, "A");

			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrspf u = new Utrspf();
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));	
				u.setCurrentUnitBal(sqlrs.getBigDecimal("CURUNTBAL"));
				utrspfList.add(u);
			} 

		} catch (SQLException e) {
			LOGGER.error("getUtrsRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrspfList;
	}
	 //ILB-1097 start

	@Override
	public List<Utrspf> searchUtrssurRecord(Utrspf utrspfData) {
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, "
				+ "RIDER, VRTFND, PLNSFX, UNITYP, CURUNTBAL,CURDUNTBAL "); //ILIFE-9538
		sqlSelect1.append(" FROM UTRSPF WHERE CHDRCOY=? AND CHDRNUM=? AND "
				+ "LIFE=? AND COVERAGE=? AND RIDER=? AND VRTFND=? AND UNITYP=? ");

		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<Utrspf> utrspfList = new LinkedList<>();
		try {
			psSelect.setString(1, utrspfData.getChdrcoy());
			psSelect.setString(2, utrspfData.getChdrnum());
			psSelect.setString(3, utrspfData.getLife());
			psSelect.setString(4, utrspfData.getCoverage());
			psSelect.setString(5, utrspfData.getRider());
			psSelect.setString(6, utrspfData.getUnitVirtualFund());
			psSelect.setString(7, utrspfData.getUnitType());

			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrspf u = new Utrspf();
				u.setUniqueNumber(sqlrs.getLong("UNIQUE_NUMBER"));
				u.setChdrcoy(sqlrs.getString("CHDRCOY"));
				u.setChdrnum(sqlrs.getString("CHDRNUM"));
				u.setLife(sqlrs.getString("LIFE"));
				u.setCoverage(sqlrs.getString("COVERAGE"));
				u.setRider(sqlrs.getString("RIDER"));
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));
				u.setUnitType(sqlrs.getString("UNITYP"));
				u.setCurrentUnitBal(sqlrs.getBigDecimal("CURUNTBAL"));
				u.setCurrentDunitBal(sqlrs.getBigDecimal("CURDUNTBAL")); //ILIFE-9538
				utrspfList.add(u);
			} 

		} catch (SQLException e) {
			LOGGER.error("getUtrssurRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrspfList;
 //ILB-1097 end
	}	
}