package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 30 July 2012 03:09:00
 * Description:
 * Copybook name: VPMCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpmcalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData vpmcalcRec = new FixedLengthStringData(5053);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpmcalcRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpmcalcRec, 5);
  	public FixedLengthStringData vpmskey = new FixedLengthStringData(14).isAPartOf(vpmcalcRec, 9);
  	public FixedLengthStringData vpmscoy = new FixedLengthStringData(1).isAPartOf(vpmskey, 0);
  	public FixedLengthStringData vpmstabl = new FixedLengthStringData(5).isAPartOf(vpmskey, 1);
  	public FixedLengthStringData vpmsitem = new FixedLengthStringData(8).isAPartOf(vpmskey, 6);
  	public FixedLengthStringData extkey = new FixedLengthStringData(30).isAPartOf(vpmcalcRec, 23);
  	public FixedLengthStringData linkageArea = new FixedLengthStringData(5000).isAPartOf(vpmcalcRec, 53);
  	
	public void initialize() {
		COBOLFunctions.initialize(vpmcalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpmcalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}