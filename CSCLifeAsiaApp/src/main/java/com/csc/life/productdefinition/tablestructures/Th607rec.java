package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:36
 * Description:
 * Copybook name: TH607REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th607rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th607Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData mind = new FixedLengthStringData(2).isAPartOf(th607Rec, 0);
  	public FixedLengthStringData rskflg = new FixedLengthStringData(2).isAPartOf(th607Rec, 2);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(th607Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th607Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th607Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}