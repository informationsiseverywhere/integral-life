/*
 * File: Pr565.java
 * Date: 30 August 2009 1:41:43
 * Author: Quipoz Limited
 * 
 * Class transformed from PR565.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsJniSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.runtime.base.VpmsTcpSessionFactory;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.screens.Sr565ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  A sub-file screen windowed from Component Screen to display
*  Benefit Schedule Details. This program does not provide surrender
*  values for MRTA products. As such, the CSV Column will not be
*  filled until a specific surrender calculation routine is provided.
*
*****************************************************************
* </pre>
*/
public class Pr565 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pr565.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR565");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final int wsaaMaxSubfile = 99;
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4).init(SPACES);
		/* TABLES */
	private static final String t5687 = "T5687";
	private static final String mbnsrec = "MBNSREC";
	private static final String mrtarec ="MRTAREC";//ILIFE-3737
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction  Starts */
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private Namadrsrec namadrsrec1 = new Namadrsrec();
	//ILIFE-3686-STARTS
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private boolean mrt1Permission = false;
	//ILIFE-3686-ENDS
	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
	private Mrtapf mrtapf=null;
	private boolean benSchdPermission = false;//ILIFE-7521
	private Sr565ScreenVars sv = ScreenProgram.getScreenVars( Sr565ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public Pr565() {
		super();
		screenVars = sv;
		new ScreenModel("Sr565", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR565", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction  Starts */
		chdrpf = chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		covrpf = covrDao.getCacheObject(covrpf);
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.crcode.set(covrpf.getCrtable());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(covrpf.getChdrcoy());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.coverDesc.fill("?");
		}
		else {
			sv.coverDesc.set(descIO.getLongdesc());
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction  Starts */
		lifeenqIO.setChdrcoy(covrpf.getChdrcoy());
		lifeenqIO.setChdrnum(covrpf.getChdrnum());
		lifeenqIO.setLife(covrpf.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		namadrsrec1.namadrsRec.set(SPACES);
		namadrsrec1.clntNumber.set(lifeenqIO.getLifcnum());
		namadrsrec1.clntPrefix.set("CN");
		namadrsrec1.clntCompany.set(wsspcomn.fsuco);
		namadrsrec1.language.set(wsspcomn.language);
		namadrsrec1.function.set("LGNMF");
		callProgram(Namadrs.class, namadrsrec1.namadrsRec);
		if (isNE(namadrsrec1.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec1.statuz);
			fatalError600();
		}
		sv.lifename.set(namadrsrec1.name);
		//ILIFE-7521
		benSchdPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP093", appVars, "IT");
		//ILIFE-7521-starts
		if(benSchdPermission && chdrpf.getCnttype().equals("LCP")){
			sv.policyType.set("Y");
		}
		else
			sv.policyType.set("N");
		//ILIFE-7521-ends
		/*ILIFE-3737 starts*/
		mrt1Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars,smtpfxcpy.item.toString());
		if(mrt1Permission){
			readMrta1500();
		}
		/*ILIFE-3737 ends*/
		
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction  Starts */
		mbnsIO.setChdrcoy(chdrpf.getChdrcoy());
		mbnsIO.setChdrnum(chdrpf.getChdrnum());
		mbnsIO.setLife(covrpf.getLife());
		mbnsIO.setCoverage(covrpf.getCoverage());
		mbnsIO.setRider(covrpf.getRider());
		mbnsIO.setYrsinf(ZERO);
		mbnsIO.setFormat(mbnsrec);
		mbnsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		mbnsIO.setStatuz(varcom.oK);
		while ( !(isEQ(mbnsIO.getStatuz(),varcom.endp))) {
			loadSubfile1200();
		}
		//ILIFE-3686-STARTS
		if(!benSchdPermission){
			if(!mrt1Permission){
				sv.mrt1Flag.set("N");//ILIFE-3737
				while ( !(isEQ(scrnparams.subfileRrn,wsaaMaxSubfile))) {
					loadBlankSubfile1300();
						}
					}
			else{
				sv.mrt1Flag.set("Y");//ILIFE-3737
				while ( !(isGTE(scrnparams.subfileRrn,wsaaMaxSubfile))) {
						loadBlankSubfile1300();
					}
				}
		//ILIFE-3686-ends
		}
		else
		{
			if(!mrt1Permission){
				sv.mrt1Flag.set("N");
			}
		}
		
		
		
		scrnparams.subfileMore.set("N");
		scrnparams.subfileRrn.set(1);
	}
/*ILIFE-3737 starts*/
protected void readMrta1500(){
	/*START*/
	/*ILIFE-3754 starts*/
	/*mrtaIO.setChdrcoy(chdrenqIO.getChdrcoy());
	mrtaIO.setChdrnum(chdrenqIO.getChdrnum());
	mrtaIO.setFormat(mrtarec);
	mrtaIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, mrtaIO);
	if (isEQ(mrtaIO.getStatuz(),varcom.oK)) {*/
	mrtapf = new Mrtapf();
	mrtapf=mrtapfDAO.getMrtaRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	if ((mrtapf !=null)) {
		sv.unitval.set(mrtapf.getPhunitvalue());
		
		sv.redfreq.set(mrtapf.getMlresind());
	}
	/*ILIFE-3754 ends*/
	
}
/*ILIFE-3737 ends*/

protected void loadSubfile1200()
	{
			start1200();
		}

protected void start1200()
	{
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)
		&& isNE(mbnsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction  Starts */
		if (isNE(mbnsIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(mbnsIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(mbnsIO.getLife(),covrpf.getLife())
		|| isNE(mbnsIO.getCoverage(),covrpf.getCoverage())
		|| isNE(mbnsIO.getRider(),covrpf.getRider())) {
			mbnsIO.setStatuz(varcom.endp);
		}
		if (isEQ(mbnsIO.getStatuz(),varcom.endp)) {
			return ;
		}
		sv.yrsinf.set(mbnsIO.getYrsinf());
		sv.sumins.set(mbnsIO.getSumins());
		sv.surrval.set(mbnsIO.getSurrval());
		scrnparams.function.set(varcom.sadd);
		processScreen("SR565", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		mbnsIO.setFunction(varcom.nextr);
	}

protected void loadBlankSubfile1300()
	{
		/*START*/
		initialize(sv.yrsinf);
		initialize(sv.sumins);
		initialize(sv.surrval);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR565", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		scrnparams.function.set(varcom.prot);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaScrnStatuz.set(scrnparams.statuz);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
			scrnparams.subfileRrn.set(1);
		}
		if (isEQ(wsspcomn.flag,"Y")) {
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SCREEN*/
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsaaScrnStatuz,varcom.calc)) {
			scrnparams.subfileRrn.set(1);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.flag,"Y")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

public static Map<String,Map<String,String>> f4(COBOLAppVars appVars, String field) throws VpmsLoadFailedException{
	
	Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();
	Map<String, String> itemDesc=new HashMap<String,String>();
	IVpmsBaseSession session;
	//ILIFE-5605 start
	IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();
	if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
		factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
		session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
	}
	else
	{
		session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
	}
	//ILIFE-5605 end
	session.setAttribute("Input Contract Type", "mrt");
	session.setAttribute("Input Region", " "); 
	session.setAttribute("Input Locale", " "); 
	session.setAttribute("Input TransEffDate", "20161010");
	session.setAttribute("Input MTL PLan Cd", "MAS05S"); 
	session.setAttribute("Input Prod Code", "GS1"); 
	session.setAttribute("Input Coverage Code", "mrt1");

	VpmsComputeResult result = session.computeUsingDefaults("Output Fetch MRTA Frequency");
	//sv.mlresind.set(result.getResult());
	String temp1=result.getResult();
    temp1=temp1.replace("||","/n");
  
    String ReducingFreq[] = temp1.split("/n");
    ArrayList<String> ReducingFreqList = new ArrayList<String>();
    ArrayList<String> ReducingFreqList1 = new ArrayList<String>();
    LOGGER.info("parts1={}", Arrays.toString(ReducingFreq));//IJTI-1561
	 
    for (int i = 0; i < ReducingFreq.length; i =i+2) {
    	ReducingFreqList.add(ReducingFreq[i]);
    	ReducingFreqList1.add(ReducingFreq[i+1]);

    }
    for (int i = 0; i < ReducingFreqList.size(); i++) {
    	itemDesc.put( ReducingFreqList.get(i).trim(),  ReducingFreqList1.get(i).trim());
		
        }
	
	f4map.put(field, itemDesc);
	
    return f4map;
    
}
}
