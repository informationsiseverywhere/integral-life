/*
 * File: Vlpdrule.java
 * Date: 30 August 2009 2:53:45
 * Author: Quipoz Limited
 * 
 * Class transformed from VLPDRULE.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Vlpdrulrec;
import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr50xrec;
import com.csc.life.productdefinition.tablestructures.Tr50yrec;
import com.csc.life.productdefinition.tablestructures.Tr50zrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *    This subroutine is main routine to cross check product
 *    validation, this program will process following,
 *    - To store all detail of component
 *      LIFE, JLIFE, COVERAGE AND RIDER
 *    - Call routines set in TR50Y,TR50Z,TR50X to validate product
 *    - Set error to output
 *
 * VLPD-FUNCTION :
 *    PROP - Cross check from Pre-Issue
 *    ADCH - Cross check from Add/Change component
 *
 * VLPD-STATUZ :
 *    O-K  - No Error
 *    INVF - Invalid function
 *
 ***********************************************************************
 * </pre>
 */
public class Vlpdrule extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("VLPDRULE");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNofCovt = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");

	private FixedLengthStringData wsaaValidPstatus = new FixedLengthStringData(1);
	private Validator validPstatus = new Validator(wsaaValidPstatus, "Y");
	private String wsaaFirstRecord = "";

	private FixedLengthStringData wsaaCovr1Details = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaCovr1Life = new FixedLengthStringData(2).isAPartOf(wsaaCovr1Details, 0);
	private FixedLengthStringData wsaaCovr1Jlife = new FixedLengthStringData(2).isAPartOf(wsaaCovr1Details, 2);
	private FixedLengthStringData wsaaCovr1Coverage = new FixedLengthStringData(2).isAPartOf(wsaaCovr1Details, 4);
	private FixedLengthStringData wsaaCovr1Rider = new FixedLengthStringData(2).isAPartOf(wsaaCovr1Details, 6);
	private FixedLengthStringData wsaaCovr1Crtable = new FixedLengthStringData(4).isAPartOf(wsaaCovr1Details, 8);

	private FixedLengthStringData wsaaCovr2Details = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaCovr2Life = new FixedLengthStringData(2).isAPartOf(wsaaCovr2Details, 0);
	private FixedLengthStringData wsaaCovr2Jlife = new FixedLengthStringData(2).isAPartOf(wsaaCovr2Details, 2);
	private FixedLengthStringData wsaaCovr2Coverage = new FixedLengthStringData(2).isAPartOf(wsaaCovr2Details, 4);
	private FixedLengthStringData wsaaCovr2Rider = new FixedLengthStringData(2).isAPartOf(wsaaCovr2Details, 6);
	private FixedLengthStringData wsaaCovr2Crtable = new FixedLengthStringData(4).isAPartOf(wsaaCovr2Details, 8);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIxc = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaErr = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaErrNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr50yCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr50zCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr50xCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaProd = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaMaxProd = 50;
	private int wsaaMaxErrDet = 20;
	private int wsaaMaxRoutine = 15;

	private FixedLengthStringData wsaaCompDetails = new FixedLengthStringData(35);
	private FixedLengthStringData wsaaCompLife = new FixedLengthStringData(2).isAPartOf(wsaaCompDetails, 0);
	private FixedLengthStringData wsaaCompJlife = new FixedLengthStringData(2).isAPartOf(wsaaCompDetails, 2);
	private FixedLengthStringData wsaaCompCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCompDetails, 4);
	private FixedLengthStringData wsaaCompRider = new FixedLengthStringData(2).isAPartOf(wsaaCompDetails, 6);
	private FixedLengthStringData wsaaCompCrtable = new FixedLengthStringData(4).isAPartOf(wsaaCompDetails, 8);
	private ZonedDecimalData wsaaCompRtrm = new ZonedDecimalData(3, 0).isAPartOf(wsaaCompDetails, 12).setUnsigned();
	private ZonedDecimalData wsaaCompPtrm = new ZonedDecimalData(3, 0).isAPartOf(wsaaCompDetails, 15).setUnsigned();
	private ZonedDecimalData wsaaCompSa = new ZonedDecimalData(17, 2).isAPartOf(wsaaCompDetails, 18).setUnsigned();

	private FixedLengthStringData wsaaTr50yItemitem = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr50yCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr50yItemitem, 0);
	private FixedLengthStringData wsaaTr50yCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr50yItemitem, 3);

	private FixedLengthStringData wsaaTr50zItemitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr50zCrtable1 = new FixedLengthStringData(4).isAPartOf(wsaaTr50zItemitem, 0);
	private FixedLengthStringData wsaaTr50zCrtable2 = new FixedLengthStringData(4).isAPartOf(wsaaTr50zItemitem, 4);

	private FixedLengthStringData wsaaTr50xItemitem = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaTr50xCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr50xItemitem, 0);
	/* FORMATS */
	private String itemrec = "ITEMREC";
	private String covtlnbrec = "COVTLNBREC";
	private String covrlnbrec = "COVRLNBREC";
	/* TABLES */
	private String tr50y = "TR50Y";
	private String tr50z = "TR50Z";
	private String tr50x = "TR50X";
	private String t5679 = "T5679";
	/* ERRORS */
	private String invf = "INVF";
	/*Coverage/rider - new business*/
	//private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	/*Coverage/rider transactions - new busine*/
	//private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5679rec t5679rec = new T5679rec();
	private Tr50xrec tr50xrec = new Tr50xrec();
	private Tr50yrec tr50yrec = new Tr50yrec();
	private Tr50zrec tr50zrec = new Tr50zrec();
	private Varcom varcom = new Varcom();
	private Vlpdrulrec vlpdrulrec = new Vlpdrulrec();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();
	private boolean wsaaEof;
	private CovtpfDAO covtlnbDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Covtpf covtpf = null;
	private List<Covtpf> covrlnbList =new ArrayList<Covtpf>(); 
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private Itempf itempf = null;
	Iterator<Covtpf> iterator;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf = null;
	private List<Covrpf> covrpfList =new ArrayList<Covrpf>(); 
	Iterator<Covrpf> iteratorCovr;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit229, 
		exit239, 
		exit249, 
		exit309, 
		exit409, 
		readTr50z503, 
		exit509, 
		exit519, 
		exit529, 
		exit539, 
		exit549, 
		a390Exit, 
		errorProg610
	}

	public Vlpdrule() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		vlpdrulrec.validRec = convertAndSetParam(vlpdrulrec.validRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void mainline000()
	{
		try {
			begin010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

	protected void begin010()
	{
		initialize100();
		if (isNE(vlpdrulrec.function,"PROP")
				&& isNE(vlpdrulrec.function,"ADCH")) {
			vlpdrulrec.statuz.set(invf);
			goTo(GotoLabel.exit090);
		}
		setVariable200();
		validateProduct400();
	}

	protected void exit090()
	{
		exitProgram();
	}

	protected void initialize100()
	{
		/*BEGIN*/
		vlpdrulrec.statuz.set(varcom.oK);
		vlpdrulrec.errors.set(SPACES);
		wsaaProd.set(ZERO);
		readT5679140();
		wsaaNofCovt.set(ZERO);
		initialize(vlpdsubrec.validRec);
		wsaaEof = false;
		/*EXIT*/
	}

	protected void readTr50y110()
	{
		/*BEGIN*/

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(vlpdrulrec.chdrcoy.toString());
		itempf.setItemtabl(tr50y);
		itempf.setItemitem(wsaaTr50yItemitem.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (itempf != null) {
			tr50yrec.tr50yRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			tr50yrec.tr50yRec.set(SPACES);
		}
		/*EXIT*/
	}

	protected void readTr50z120()
	{
		/*BEGIN*/


		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(vlpdrulrec.chdrcoy.toString());
		itempf.setItemtabl(tr50z);
		itempf.setItemitem(wsaaTr50zItemitem.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (itempf != null) {
			tr50zrec.tr50zRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			tr50zrec.tr50zRec.set(SPACES);
		}
		/*EXIT*/
	}

	protected void readTr50x130()
	{

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(vlpdrulrec.chdrcoy.toString());
		itempf.setItemtabl(tr50x);
		itempf.setItemitem(wsaaTr50xItemitem.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (itempf != null) {
			tr50xrec.tr50xRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			tr50xrec.tr50xRec.set(SPACES);
		}
		/*EXIT*/
	}

	protected void readT5679140()
	{

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(vlpdrulrec.chdrcoy.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(vlpdrulrec.batctrcde.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (itempf != null) {
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			t5679rec.t5679Rec.set(SPACES);
		}
		/*EXIT*/
	}

	protected void setVariable200()
	{
		begin201();
		keepCovt202();
		keepCovr203();
	}

	protected void begin201()
	{
		if (isEQ(vlpdrulrec.function,"ADCH")) {
			wsaaCompLife.set(vlpdrulrec.inputLife);
			wsaaCompJlife.set(vlpdrulrec.inputJlife);
			wsaaCompCoverage.set(vlpdrulrec.inputCoverage);
			wsaaCompRider.set(vlpdrulrec.inputRider);
			wsaaCompCrtable.set(vlpdrulrec.inputCrtable);
			wsaaCompSa.set(vlpdrulrec.inputSa);
			wsaaCompRtrm.set(vlpdrulrec.inputRtrm);
			wsaaCompPtrm.set(vlpdrulrec.inputPtrm);
			writeInput300();
		}
	}

	protected void keepCovt202()
	{
		if (isEQ(vlpdrulrec.function,"PROP")
				|| isEQ(vlpdrulrec.function,"ADCH")) {



			covrlnbList = covtlnbDAO.searchCovtRecordByCoyNumDescUniquNo(vlpdrulrec.chdrcoy.toString(),vlpdrulrec.chdrnum.toString());
			iterator = covrlnbList.iterator();

			while ( !(wsaaEof == true )) {
				callCovtlnb220();
			}

		}
	}

	protected void keepCovr203()
	{   
		wsaaEof = false;
		if (isEQ(vlpdrulrec.function,"ADCH")
				&& isNE(wsaaNofCovt,ZERO)) {

			covrpfList	= covrpfDAO.readCovrForBilling(vlpdrulrec.chdrcoy.toString(),vlpdrulrec.chdrnum.toString());
			iteratorCovr = covrpfList.iterator();	

			while ( !(wsaaEof == true)) {
				callCovrlnb230();
			}

		}
		/*EXIT*/
	}

	protected void callCovtlnb220()
	{
		try {
			begin221();
		}
		catch (GOTOException e){
		}
	}

	protected void begin221()
	{

		if(iterator.hasNext())
			covtpf = iterator.next();
		else {
			wsaaEof = true;
			goTo(GotoLabel.exit229); }

		wsaaCompLife.set(covtpf.getLife());
		wsaaCompJlife.set(covtpf.getJlife());
		wsaaCompCoverage.set(covtpf.getCoverage());
		wsaaCompRider.set(covtpf.getRider());
		wsaaCompCrtable.set(covtpf.getCrtable());
		wsaaCompSa.set(covtpf.getSumins());
		datcon3rec.intDate1.set(covtpf.getEffdate());
		datcon3rec.intDate2.set(covtpf.getRcesdte());
		a200CallDatcon3();
		wsaaCompRtrm.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(covtpf.getEffdate());
		datcon3rec.intDate2.set(covtpf.getPcesdte());
		a200CallDatcon3();
		wsaaCompPtrm.set(datcon3rec.freqFactor);
		writeInput300();
		wsaaNofCovt.add(1);

	}

	protected void callCovrlnb230()
	{
		try {
			begin231();
		}
		catch (GOTOException e){
		}
	}

	protected void begin231()
	{

		if(iteratorCovr.hasNext())
			covrpf = iteratorCovr.next();
		else {
			wsaaEof = true;
			goTo(GotoLabel.exit239); }

		if (isEQ(covrpf.getValidflag(),"2")) {
			goTo(GotoLabel.exit239);
		}
		checkStatcode240();
		if (!validStatus.isTrue()) {
			goTo(GotoLabel.exit239);
		}
		wsaaCompLife.set(covrpf.getLife());
		wsaaCompJlife.set(covrpf.getJlife());
		wsaaCompCoverage.set(covrpf.getCoverage());
		wsaaCompRider.set(covrpf.getRider());
		wsaaCompCrtable.set(covrpf.getCrtable());
		wsaaCompSa.set(covrpf.getSumins());
		datcon3rec.intDate1.set(covrpf.getCrrcd());
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		a200CallDatcon3();
		wsaaCompRtrm.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(covrpf.getCrrcd());
		datcon3rec.intDate2.set(covrpf.getPremCessDate());
		a200CallDatcon3();
		wsaaCompPtrm.set(datcon3rec.freqFactor);
		writeInput300();
	}

	protected void checkStatcode240()
	{
		try {
			begin241();
		}
		catch (GOTOException e){
		}
	}

	protected void begin241()
	{
		wsaaValidPstatus.set("N");
		wsaaValidStatus.set("N");
		if (isEQ(covrpf.getRider(),"00")) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
					|| validPstatus.isTrue()); wsaaSub1.add(1)){
				if (isEQ(covrpf.getPstatcode(),t5679rec.covPremStat[wsaaSub1.toInt()])) {
					wsaaValidPstatus.set("Y");
				}
			}
		}
		else {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
					|| validPstatus.isTrue()); wsaaSub1.add(1)){
				if (isEQ(covrpf.getPstatcode(),t5679rec.ridPremStat[wsaaSub1.toInt()])) {
					wsaaValidPstatus.set("Y");
				}
			}
		}
		if (isEQ(wsaaValidPstatus,"N")) {
			goTo(GotoLabel.exit249);
		}
		if (isEQ(covrpf.getRider(),"00")) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
					|| validStatus.isTrue()); wsaaSub1.add(1)){
				if (isEQ(covrpf.getStatcode(),t5679rec.covRiskStat[wsaaSub1.toInt()])) {
					wsaaValidStatus.set("Y");
				}
			}
		}
		else {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
					|| validStatus.isTrue()); wsaaSub1.add(1)){
				if (isEQ(covrpf.getStatcode(),t5679rec.ridRiskStat[wsaaSub1.toInt()])) {
					wsaaValidStatus.set("Y");
				}
			}
		}
	}

	protected void writeInput300()
	{
		try {
			begin301();
		}
		catch (GOTOException e){
		}
	}

	protected void begin301()
	{
		for (wsaaIy.set(1); !(isGT(wsaaIy,50)); wsaaIy.add(1)){
			if (isEQ(wsaaCompLife,vlpdsubrec.inputLife[wsaaIy.toInt()])
					&& isEQ(wsaaCompJlife,vlpdsubrec.inputJlife[wsaaIy.toInt()])
					&& isEQ(wsaaCompCoverage,vlpdsubrec.inputCoverage[wsaaIy.toInt()])
					&& isEQ(wsaaCompRider,vlpdsubrec.inputRider[wsaaIy.toInt()])) {
				goTo(GotoLabel.exit309);
			}
		}
		wsaaProd.add(1);
		vlpdsubrec.inputs[wsaaProd.toInt()].set(wsaaCompDetails);
	}

	protected void validateProduct400()
	{
		try {
			begin401();
			readTr50x402();
		}
		catch (GOTOException e){
		}
	}

	protected void begin401()
	{
		wsaaErr.set(ZERO);
		vlpdsubrec.function.set(vlpdrulrec.function);
		vlpdsubrec.cnttype.set(vlpdrulrec.cnttype);
		vlpdsubrec.srcebus.set(vlpdrulrec.srcebus);
		vlpdsubrec.cownnum.set(vlpdrulrec.cownnum);
		vlpdsubrec.billfreq.set(vlpdrulrec.billfreq);
		vlpdsubrec.effdate.set(vlpdrulrec.effdate);
		vlpdsubrec.language.set(vlpdrulrec.language);
		vlpdsubrec.chdrcoy.set(vlpdrulrec.chdrcoy);
		vlpdsubrec.chdrnum.set(vlpdrulrec.chdrnum);
		for (wsaaIx.set(1); !(isGT(wsaaIx,wsaaProd)); wsaaIx.add(1)){
			propRoutine500();
		}
	}

	protected void readTr50x402()
	{
		wsaaTr50xCnttype.set(vlpdsubrec.cnttype);
		readTr50x130();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaTr50xCnttype.set("***");
			readTr50x130();
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.exit409);
			}
		}
		for (wsaaTr50xCount.set(1); !(isGT(wsaaTr50xCount,wsaaMaxRoutine)); wsaaTr50xCount.add(1)){
			routineTr50x540();
		}
	}

	protected void propRoutine500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin501();
				}
				case readTr50z503: {
					readTr50z503();
				}
				case exit509: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void begin501()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaIx.toInt()],SPACES)
				|| isEQ(vlpdsubrec.inputCoverage[wsaaIx.toInt()],SPACES)
				|| isEQ(vlpdsubrec.inputRider[wsaaIx.toInt()],SPACES)) {
			wsaaIx.set(wsaaProd);
			goTo(GotoLabel.exit509);
		}
		wsaaCompDetails.set(vlpdsubrec.inputs[wsaaIx.toInt()]);
		/*READ-TR50Y*/
		wsaaTr50yCnttype.set(vlpdsubrec.cnttype);
		wsaaTr50yCrtable.set(vlpdsubrec.inputCrtable[wsaaIx.toInt()]);
		readTr50y110();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.readTr50z503);
		}
		for (wsaaTr50yCount.set(1); !(isGT(wsaaTr50yCount,wsaaMaxRoutine)); wsaaTr50yCount.add(1)){
			routineTr50y510();
		}
	}

	protected void readTr50z503()
	{
		for (wsaaIxc.set(1); !(isGT(wsaaIxc,wsaaMaxProd)); wsaaIxc.add(1)){
			if (isNE(vlpdsubrec.inputCrtable[wsaaIxc.toInt()],SPACES)) {
				validateCompComp520();
			}
			else {
				wsaaIxc.set(wsaaMaxProd);
			}
		}
	}

	protected void routineTr50y510()
	{
		try {
			begin511();
		}
		catch (GOTOException e){
		}
	}

	protected void begin511()
	{
		if (isEQ(tr50yrec.premsubr[wsaaTr50yCount.toInt()],SPACES)) {
			wsaaTr50yCount.set(wsaaMaxRoutine);
			goTo(GotoLabel.exit519);
		}
		vlpdsubrec.rid1Life.set(wsaaCompLife);
		vlpdsubrec.rid1Jlife.set(wsaaCompJlife);
		vlpdsubrec.rid1Coverage.set(wsaaCompCoverage);
		vlpdsubrec.rid1Rider.set(wsaaCompRider);
		vlpdsubrec.rid1Crtable.set(wsaaCompCrtable);
		vlpdsubrec.rid2Details.set(SPACES);
		vlpdsubrec.errDetails.set(SPACES);
		callProgram(tr50yrec.premsubr[wsaaTr50yCount.toInt()], vlpdsubrec.validRec);
		if (isNE(vlpdsubrec.statuz,varcom.oK)) {
			syserrrec.params.set(vlpdsubrec.validRec);
			fatalError600();
		}
		wsaaErr.set(ZERO);
		a300SetupError();
	}

	protected void validateCompComp520()
	{
		try {
			begin521();
		}
		catch (GOTOException e){
		}
	}

	protected void begin521()
	{
		if (isEQ(wsaaCompLife,vlpdsubrec.inputLife[wsaaIxc.toInt()])
				&& isEQ(wsaaCompJlife,vlpdsubrec.inputJlife[wsaaIxc.toInt()])
				&& isEQ(wsaaCompCoverage,vlpdsubrec.inputCoverage[wsaaIxc.toInt()])
				&& isEQ(wsaaCompRider,vlpdsubrec.inputRider[wsaaIxc.toInt()])) {
			goTo(GotoLabel.exit529);
		}
		if (isNE(wsaaCompLife,vlpdsubrec.inputLife[wsaaIxc.toInt()])
				|| isNE(wsaaCompJlife,vlpdsubrec.inputJlife[wsaaIxc.toInt()])
				|| isNE(wsaaCompCoverage,vlpdsubrec.inputCoverage[wsaaIxc.toInt()])) {
			goTo(GotoLabel.exit529);
		}
		wsaaTr50zCrtable1.set(wsaaCompCrtable);
		wsaaTr50zCrtable2.set(vlpdsubrec.inputCrtable[wsaaIxc.toInt()]);
		readTr50z120();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit529);
		}
		for (wsaaTr50zCount.set(1); !(isGT(wsaaTr50zCount,wsaaMaxRoutine)); wsaaTr50zCount.add(1)){
			routineTr50z530();
		}
	}

	protected void routineTr50z530()
	{
		try {
			begin531();
		}
		catch (GOTOException e){
		}
	}

	protected void begin531()
	{
		if (isEQ(tr50zrec.premsubr[wsaaTr50zCount.toInt()],SPACES)) {
			wsaaTr50zCount.set(wsaaMaxRoutine);
			goTo(GotoLabel.exit539);
		}
		vlpdsubrec.rid1Life.set(wsaaCompLife);
		vlpdsubrec.rid1Jlife.set(wsaaCompJlife);
		vlpdsubrec.rid1Coverage.set(wsaaCompCoverage);
		vlpdsubrec.rid1Rider.set(wsaaCompRider);
		vlpdsubrec.rid1Crtable.set(wsaaCompCrtable);
		vlpdsubrec.rid2Life.set(vlpdsubrec.inputLife[wsaaIxc.toInt()]);
		vlpdsubrec.rid2Jlife.set(vlpdsubrec.inputJlife[wsaaIxc.toInt()]);
		vlpdsubrec.rid2Coverage.set(vlpdsubrec.inputCoverage[wsaaIxc.toInt()]);
		vlpdsubrec.rid2Rider.set(vlpdsubrec.inputRider[wsaaIxc.toInt()]);
		vlpdsubrec.rid2Crtable.set(vlpdsubrec.inputCrtable[wsaaIxc.toInt()]);
		vlpdsubrec.errDetails.set(SPACES);
		callProgram(tr50zrec.premsubr[wsaaTr50zCount.toInt()], vlpdsubrec.validRec);
		if (isNE(vlpdsubrec.statuz,varcom.oK)) {
			syserrrec.params.set(vlpdsubrec.validRec);
			fatalError600();
		}
		wsaaErr.set(ZERO);
		a300SetupError();
	}

	protected void routineTr50x540()
	{
		try {
			begin541();
		}
		catch (GOTOException e){
		}
	}

	protected void begin541()
	{
		if (isEQ(tr50xrec.premsubr[wsaaTr50xCount.toInt()],SPACES)) {
			wsaaTr50xCount.set(wsaaMaxRoutine);
			goTo(GotoLabel.exit549);
		}
		wsaaCompDetails.set(SPACES);
		vlpdsubrec.rid1Details.set(SPACES);
		vlpdsubrec.rid2Details.set(SPACES);
		vlpdsubrec.errDetails.set(SPACES);
		callProgram(tr50xrec.premsubr[wsaaTr50xCount.toInt()], vlpdsubrec.validRec);
		if (isNE(vlpdsubrec.statuz,varcom.oK)) {
			syserrrec.params.set(vlpdsubrec.validRec);
			fatalError600();
		}
		wsaaErr.set(ZERO);
		a300SetupError();
	}

	protected void a100CallItemio()
	{
		/*A110-BEGIN*/
		/*
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*A190-EXIT*/
	}

	protected void a200CallDatcon3()
	{
		/*A210-BEGIN*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		/*A290-EXIT*/
	}

	protected void a300SetupError()
	{
		try {
			a310Begin();
		}
		catch (GOTOException e){
		}
	}

	protected void a310Begin()
	{
		wsaaErr.add(1);
		if (isGT(wsaaErr,wsaaMaxErrDet)) {
			goTo(GotoLabel.a390Exit);
		}
		if (isEQ(vlpdsubrec.errCode[wsaaErr.toInt()],SPACES)) {
			goTo(GotoLabel.a390Exit);
		}
		wsaaErrNo.set(ZERO);
		for (wsaaIy.set(1); !(isGT(wsaaIy,wsaaMaxProd)); wsaaIy.add(1)){
			if (isEQ(vlpdrulrec.errCnttype[wsaaIy.toInt()],SPACES)
					|| (isEQ(wsaaCompLife,vlpdrulrec.errLife[wsaaIy.toInt()])
							&& isEQ(wsaaCompJlife,vlpdrulrec.errJlife[wsaaIy.toInt()])
							&& isEQ(wsaaCompCoverage,vlpdrulrec.errCoverage[wsaaIy.toInt()])
							&& isEQ(wsaaCompRider,vlpdrulrec.errRider[wsaaIy.toInt()]))) {
				wsaaErrNo.set(wsaaIy);
				wsaaIy.set(wsaaMaxProd);
			}
		}
		if (isEQ(wsaaErrNo,ZERO)) {
			goTo(GotoLabel.a390Exit);
		}
		wsaaErr.set(ZERO);
		for (wsaaIy.set(1); !(isGT(wsaaIy,wsaaMaxErrDet)); wsaaIy.add(1)){
			if (isEQ(vlpdrulrec.errCode[wsaaErrNo.toInt()][wsaaIy.toInt()],SPACES)) {
				wsaaErr.set(wsaaIy);
				wsaaIy.set(wsaaMaxErrDet);
			}
		}
		if (isEQ(wsaaErr,ZERO)) {
			goTo(GotoLabel.a390Exit);
		}
		vlpdrulrec.errCnttype[wsaaErrNo.toInt()].set(vlpdrulrec.cnttype);
		vlpdrulrec.errLife[wsaaErrNo.toInt()].set(wsaaCompLife);
		vlpdrulrec.errJlife[wsaaErrNo.toInt()].set(wsaaCompJlife);
		vlpdrulrec.errCoverage[wsaaErrNo.toInt()].set(wsaaCompCoverage);
		vlpdrulrec.errRider[wsaaErrNo.toInt()].set(wsaaCompRider);
		for (wsaaIy.set(1); !(isGT(wsaaIy,wsaaMaxErrDet)
				|| isGT(wsaaErr,wsaaMaxErrDet)); wsaaIy.add(1)){
			if (isNE(vlpdsubrec.errCode[wsaaIy.toInt()],SPACES)) {
				vlpdrulrec.errCode[wsaaErrNo.toInt()][wsaaErr.toInt()].set(vlpdsubrec.errCode[wsaaIy.toInt()]);
				vlpdrulrec.errDet[wsaaErrNo.toInt()][wsaaErr.toInt()].set(vlpdsubrec.errDet[wsaaIy.toInt()]);
				wsaaErr.add(1);
			}
			else {
				wsaaIy.set(wsaaMaxErrDet);
			}
		}
	}

	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void errorProg610()
	{
		vlpdrulrec.statuz.set(varcom.bomb);
		exitProgram();
	}

	protected void exit690()
	{
		exitProgram();
	}
}
