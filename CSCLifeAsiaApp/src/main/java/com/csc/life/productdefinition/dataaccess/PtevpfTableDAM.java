package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PtevpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:08
 * Class transformed from PTEVPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PtevpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 78;
	public FixedLengthStringData ptevrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ptevpfRecord = ptevrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ptevrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ptevrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(ptevrec);
	public PackedDecimalData ptrneff = DD.ptrneff.copy().isAPartOf(ptevrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ptevrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(ptevrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(ptevrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(ptevrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(ptevrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ptevrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ptevrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ptevrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PtevpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PtevpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PtevpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PtevpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PtevpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PtevpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PtevpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PTEVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"EFFDATE, " +
							"PTRNEFF, " +
							"TRANNO, " +
							"VALIDFLAG, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     effdate,
                                     ptrneff,
                                     tranno,
                                     validflag,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		effdate.clear();
  		ptrneff.clear();
  		tranno.clear();
  		validflag.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPtevrec() {
  		return ptevrec;
	}

	public FixedLengthStringData getPtevpfRecord() {
  		return ptevpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPtevrec(what);
	}

	public void setPtevrec(Object what) {
  		this.ptevrec.set(what);
	}

	public void setPtevpfRecord(Object what) {
  		this.ptevpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ptevrec.getLength());
		result.set(ptevrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}