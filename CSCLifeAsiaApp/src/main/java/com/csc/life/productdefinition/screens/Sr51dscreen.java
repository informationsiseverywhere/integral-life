package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr51dscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 13, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51dScreenVars sv = (Sr51dScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr51dscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr51dScreenVars screenVars = (Sr51dScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.srcebus01.setClassString("");
		screenVars.srcebus02.setClassString("");
		screenVars.srcebus03.setClassString("");
		screenVars.srcebus04.setClassString("");
		screenVars.srcebus05.setClassString("");
		screenVars.srcebus06.setClassString("");
		screenVars.srcebus07.setClassString("");
		screenVars.srcebus08.setClassString("");
		screenVars.srcebus09.setClassString("");
		screenVars.srcebus10.setClassString("");
		screenVars.srcebus11.setClassString("");
		screenVars.srcebus12.setClassString("");
		screenVars.srcebus13.setClassString("");
		screenVars.srcebus14.setClassString("");
		screenVars.srcebus15.setClassString("");
		screenVars.srcebus16.setClassString("");
		screenVars.srcebus17.setClassString("");
		screenVars.srcebus18.setClassString("");
		screenVars.srcebus19.setClassString("");
		screenVars.srcebus20.setClassString("");
		screenVars.srcebus21.setClassString("");
		screenVars.srcebus22.setClassString("");
		screenVars.srcebus23.setClassString("");
		screenVars.srcebus24.setClassString("");
		screenVars.srcebus25.setClassString("");
		screenVars.srcebus26.setClassString("");
		screenVars.srcebus27.setClassString("");
		screenVars.srcebus28.setClassString("");
		screenVars.srcebus29.setClassString("");
		screenVars.srcebus30.setClassString("");
		screenVars.srcebus31.setClassString("");
		screenVars.srcebus32.setClassString("");
		screenVars.srcebus33.setClassString("");
		screenVars.srcebus34.setClassString("");
		screenVars.srcebus35.setClassString("");
		screenVars.srcebus36.setClassString("");
		screenVars.srcebus37.setClassString("");
		screenVars.srcebus38.setClassString("");
		screenVars.srcebus39.setClassString("");
		screenVars.srcebus40.setClassString("");
		screenVars.srcebus41.setClassString("");
		screenVars.srcebus42.setClassString("");
		screenVars.srcebus43.setClassString("");
		screenVars.srcebus44.setClassString("");
		screenVars.srcebus45.setClassString("");
		screenVars.srcebus46.setClassString("");
		screenVars.srcebus47.setClassString("");
		screenVars.srcebus48.setClassString("");
		screenVars.srcebus49.setClassString("");
		screenVars.srcebus50.setClassString("");
	}

/**
 * Clear all the variables in Sr51dscreen
 */
	public static void clear(VarModel pv) {
		Sr51dScreenVars screenVars = (Sr51dScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.srcebus01.clear();
		screenVars.srcebus02.clear();
		screenVars.srcebus03.clear();
		screenVars.srcebus04.clear();
		screenVars.srcebus05.clear();
		screenVars.srcebus06.clear();
		screenVars.srcebus07.clear();
		screenVars.srcebus08.clear();
		screenVars.srcebus09.clear();
		screenVars.srcebus10.clear();
		screenVars.srcebus11.clear();
		screenVars.srcebus12.clear();
		screenVars.srcebus13.clear();
		screenVars.srcebus14.clear();
		screenVars.srcebus15.clear();
		screenVars.srcebus16.clear();
		screenVars.srcebus17.clear();
		screenVars.srcebus18.clear();
		screenVars.srcebus19.clear();
		screenVars.srcebus20.clear();
		screenVars.srcebus21.clear();
		screenVars.srcebus22.clear();
		screenVars.srcebus23.clear();
		screenVars.srcebus24.clear();
		screenVars.srcebus25.clear();
		screenVars.srcebus26.clear();
		screenVars.srcebus27.clear();
		screenVars.srcebus28.clear();
		screenVars.srcebus29.clear();
		screenVars.srcebus30.clear();
		screenVars.srcebus31.clear();
		screenVars.srcebus32.clear();
		screenVars.srcebus33.clear();
		screenVars.srcebus34.clear();
		screenVars.srcebus35.clear();
		screenVars.srcebus36.clear();
		screenVars.srcebus37.clear();
		screenVars.srcebus38.clear();
		screenVars.srcebus39.clear();
		screenVars.srcebus40.clear();
		screenVars.srcebus41.clear();
		screenVars.srcebus42.clear();
		screenVars.srcebus43.clear();
		screenVars.srcebus44.clear();
		screenVars.srcebus45.clear();
		screenVars.srcebus46.clear();
		screenVars.srcebus47.clear();
		screenVars.srcebus48.clear();
		screenVars.srcebus49.clear();
		screenVars.srcebus50.clear();
	}
}
