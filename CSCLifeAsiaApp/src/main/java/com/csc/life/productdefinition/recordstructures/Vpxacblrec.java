package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date:
 * Description:
 * Copybook name: Vpxacblrec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxacblrec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
	public FixedLengthStringData vpxacblRec = new FixedLengthStringData(55);
	
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxacblRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxacblRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxacblRec, 9);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(34).isAPartOf(vpxacblRec, 21);
  	
  	public PackedDecimalData totRegPrem = new PackedDecimalData(17,2).isAPartOf(dataRec, 0);
  	public PackedDecimalData totSingPrem = new PackedDecimalData(17,2).isAPartOf(dataRec, 16);
  	
  	  	
	public void initialize() {
		COBOLFunctions.initialize(vpxacblRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxacblRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}