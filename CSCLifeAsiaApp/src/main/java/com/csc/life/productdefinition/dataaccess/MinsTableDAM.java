package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MinsTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:35
 * Class transformed from MINS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MinsTableDAM extends MinspfTableDAM {

	public MinsTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MINS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "LINSTAMT01, " +
		            "LINSTAMT02, " +
		            "LINSTAMT03, " +
		            "LINSTAMT04, " +
		            "LINSTAMT05, " +
		            "LINSTAMT06, " +
		            "LINSTAMT07, " +
		            "DATEDUE01, " +
		            "DATEDUE02, " +
		            "DATEDUE03, " +
		            "DATEDUE04, " +
		            "DATEDUE05, " +
		            "DATEDUE06, " +
		            "DATEDUE07, " +
		            //ILIFE-2745-STARTS
		            "USRPRF, " +
					"JOBNM, " +
					"DATIME, " +
					//ILIFE-2745-ENDS
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               linstamt01,
                               linstamt02,
                               linstamt03,
                               linstamt04,
                               linstamt05,
                               linstamt06,
                               linstamt07,
                               datedue01,
                               datedue02,
                               datedue03,
                               datedue04,
                               datedue05,
                               datedue06,
                               datedue07,
                               //ILIFE-2745-STARTS
                               userProfile,
                               jobName,
                               datime,
                               //ILIFE-2745-ENDS
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(138);//ILIFE-2745
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getLinstamt01().toInternal()
					+ getLinstamt02().toInternal()
					+ getLinstamt03().toInternal()
					+ getLinstamt04().toInternal()
					+ getLinstamt05().toInternal()
					+ getLinstamt06().toInternal()
					+ getLinstamt07().toInternal()
					+ getDatedue01().toInternal()
					+ getDatedue02().toInternal()
					+ getDatedue03().toInternal()
					+ getDatedue04().toInternal()
					+ getDatedue05().toInternal()
					+ getDatedue06().toInternal()
					+ getDatedue07().toInternal()
					//ILIFE-2745-STARTS
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
					//ILIFE-2745-ENDS
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, linstamt01);
			what = ExternalData.chop(what, linstamt02);
			what = ExternalData.chop(what, linstamt03);
			what = ExternalData.chop(what, linstamt04);
			what = ExternalData.chop(what, linstamt05);
			what = ExternalData.chop(what, linstamt06);
			what = ExternalData.chop(what, linstamt07);
			what = ExternalData.chop(what, datedue01);
			what = ExternalData.chop(what, datedue02);
			what = ExternalData.chop(what, datedue03);
			what = ExternalData.chop(what, datedue04);
			what = ExternalData.chop(what, datedue05);
			what = ExternalData.chop(what, datedue06);
			what = ExternalData.chop(what, datedue07);
			//ILIFE-2745-STARTS
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);
			//ILIFE-2745-ENDS
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getLinstamt01() {
		return linstamt01;
	}
	public void setLinstamt01(Object what) {
		setLinstamt01(what, false);
	}
	public void setLinstamt01(Object what, boolean rounded) {
		if (rounded)
			linstamt01.setRounded(what);
		else
			linstamt01.set(what);
	}	
	public PackedDecimalData getLinstamt02() {
		return linstamt02;
	}
	public void setLinstamt02(Object what) {
		setLinstamt02(what, false);
	}
	public void setLinstamt02(Object what, boolean rounded) {
		if (rounded)
			linstamt02.setRounded(what);
		else
			linstamt02.set(what);
	}	
	public PackedDecimalData getLinstamt03() {
		return linstamt03;
	}
	public void setLinstamt03(Object what) {
		setLinstamt03(what, false);
	}
	public void setLinstamt03(Object what, boolean rounded) {
		if (rounded)
			linstamt03.setRounded(what);
		else
			linstamt03.set(what);
	}	
	public PackedDecimalData getLinstamt04() {
		return linstamt04;
	}
	public void setLinstamt04(Object what) {
		setLinstamt04(what, false);
	}
	public void setLinstamt04(Object what, boolean rounded) {
		if (rounded)
			linstamt04.setRounded(what);
		else
			linstamt04.set(what);
	}	
	public PackedDecimalData getLinstamt05() {
		return linstamt05;
	}
	public void setLinstamt05(Object what) {
		setLinstamt05(what, false);
	}
	public void setLinstamt05(Object what, boolean rounded) {
		if (rounded)
			linstamt05.setRounded(what);
		else
			linstamt05.set(what);
	}	
	public PackedDecimalData getLinstamt06() {
		return linstamt06;
	}
	public void setLinstamt06(Object what) {
		setLinstamt06(what, false);
	}
	public void setLinstamt06(Object what, boolean rounded) {
		if (rounded)
			linstamt06.setRounded(what);
		else
			linstamt06.set(what);
	}	
	public PackedDecimalData getLinstamt07() {
		return linstamt07;
	}
	public void setLinstamt07(Object what) {
		setLinstamt07(what, false);
	}
	public void setLinstamt07(Object what, boolean rounded) {
		if (rounded)
			linstamt07.setRounded(what);
		else
			linstamt07.set(what);
	}	
	public PackedDecimalData getDatedue01() {
		return datedue01;
	}
	public void setDatedue01(Object what) {
		setDatedue01(what, false);
	}
	public void setDatedue01(Object what, boolean rounded) {
		if (rounded)
			datedue01.setRounded(what);
		else
			datedue01.set(what);
	}	
	public PackedDecimalData getDatedue02() {
		return datedue02;
	}
	public void setDatedue02(Object what) {
		setDatedue02(what, false);
	}
	public void setDatedue02(Object what, boolean rounded) {
		if (rounded)
			datedue02.setRounded(what);
		else
			datedue02.set(what);
	}	
	public PackedDecimalData getDatedue03() {
		return datedue03;
	}
	public void setDatedue03(Object what) {
		setDatedue03(what, false);
	}
	public void setDatedue03(Object what, boolean rounded) {
		if (rounded)
			datedue03.setRounded(what);
		else
			datedue03.set(what);
	}	
	public PackedDecimalData getDatedue04() {
		return datedue04;
	}
	public void setDatedue04(Object what) {
		setDatedue04(what, false);
	}
	public void setDatedue04(Object what, boolean rounded) {
		if (rounded)
			datedue04.setRounded(what);
		else
			datedue04.set(what);
	}	
	public PackedDecimalData getDatedue05() {
		return datedue05;
	}
	public void setDatedue05(Object what) {
		setDatedue05(what, false);
	}
	public void setDatedue05(Object what, boolean rounded) {
		if (rounded)
			datedue05.setRounded(what);
		else
			datedue05.set(what);
	}	
	public PackedDecimalData getDatedue06() {
		return datedue06;
	}
	public void setDatedue06(Object what) {
		setDatedue06(what, false);
	}
	public void setDatedue06(Object what, boolean rounded) {
		if (rounded)
			datedue06.setRounded(what);
		else
			datedue06.set(what);
	}	
	public PackedDecimalData getDatedue07() {
		return datedue07;
	}
	public void setDatedue07(Object what) {
		setDatedue07(what, false);
	}
	public void setDatedue07(Object what, boolean rounded) {
		if (rounded)
			datedue07.setRounded(what);
		else
			datedue07.set(what);
	}	
	//ILIFE-2745-STARTS
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}
	//ILIFE-2745-ENDS

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getLinstamts() {
		return new FixedLengthStringData(linstamt01.toInternal()
										+ linstamt02.toInternal()
										+ linstamt03.toInternal()
										+ linstamt04.toInternal()
										+ linstamt05.toInternal()
										+ linstamt06.toInternal()
										+ linstamt07.toInternal());
	}
	public void setLinstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getLinstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, linstamt01);
		what = ExternalData.chop(what, linstamt02);
		what = ExternalData.chop(what, linstamt03);
		what = ExternalData.chop(what, linstamt04);
		what = ExternalData.chop(what, linstamt05);
		what = ExternalData.chop(what, linstamt06);
		what = ExternalData.chop(what, linstamt07);
	}
	public PackedDecimalData getLinstamt(BaseData indx) {
		return getLinstamt(indx.toInt());
	}
	public PackedDecimalData getLinstamt(int indx) {

		switch (indx) {
			case 1 : return linstamt01;
			case 2 : return linstamt02;
			case 3 : return linstamt03;
			case 4 : return linstamt04;
			case 5 : return linstamt05;
			case 6 : return linstamt06;
			case 7 : return linstamt07;
			default: return null; // Throw error instead?
		}
	
	}
	public void setLinstamt(BaseData indx, Object what) {
		setLinstamt(indx, what, false);
	}
	public void setLinstamt(BaseData indx, Object what, boolean rounded) {
		setLinstamt(indx.toInt(), what, rounded);
	}
	public void setLinstamt(int indx, Object what) {
		setLinstamt(indx, what, false);
	}
	public void setLinstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setLinstamt01(what, rounded);
					 break;
			case 2 : setLinstamt02(what, rounded);
					 break;
			case 3 : setLinstamt03(what, rounded);
					 break;
			case 4 : setLinstamt04(what, rounded);
					 break;
			case 5 : setLinstamt05(what, rounded);
					 break;
			case 6 : setLinstamt06(what, rounded);
					 break;
			case 7 : setLinstamt07(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getDatedues() {
		return new FixedLengthStringData(datedue01.toInternal()
										+ datedue02.toInternal()
										+ datedue03.toInternal()
										+ datedue04.toInternal()
										+ datedue05.toInternal()
										+ datedue06.toInternal()
										+ datedue07.toInternal());
	}
	public void setDatedues(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDatedues().getLength()).init(obj);
	
		what = ExternalData.chop(what, datedue01);
		what = ExternalData.chop(what, datedue02);
		what = ExternalData.chop(what, datedue03);
		what = ExternalData.chop(what, datedue04);
		what = ExternalData.chop(what, datedue05);
		what = ExternalData.chop(what, datedue06);
		what = ExternalData.chop(what, datedue07);
	}
	public PackedDecimalData getDatedue(BaseData indx) {
		return getDatedue(indx.toInt());
	}
	public PackedDecimalData getDatedue(int indx) {

		switch (indx) {
			case 1 : return datedue01;
			case 2 : return datedue02;
			case 3 : return datedue03;
			case 4 : return datedue04;
			case 5 : return datedue05;
			case 6 : return datedue06;
			case 7 : return datedue07;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDatedue(BaseData indx, Object what) {
		setDatedue(indx, what, false);
	}
	public void setDatedue(BaseData indx, Object what, boolean rounded) {
		setDatedue(indx.toInt(), what, rounded);
	}
	public void setDatedue(int indx, Object what) {
		setDatedue(indx, what, false);
	}
	public void setDatedue(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setDatedue01(what, rounded);
					 break;
			case 2 : setDatedue02(what, rounded);
					 break;
			case 3 : setDatedue03(what, rounded);
					 break;
			case 4 : setDatedue04(what, rounded);
					 break;
			case 5 : setDatedue05(what, rounded);
					 break;
			case 6 : setDatedue06(what, rounded);
					 break;
			case 7 : setDatedue07(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		linstamt01.clear();
		linstamt02.clear();
		linstamt03.clear();
		linstamt04.clear();
		linstamt05.clear();
		linstamt06.clear();
		linstamt07.clear();
		datedue01.clear();
		datedue02.clear();
		datedue03.clear();
		datedue04.clear();
		datedue05.clear();
		datedue06.clear();
		datedue07.clear();		
		//ILIFE-2745-STARTS
		userProfile.clear();
		jobName.clear();
		datime.clear();	
		//ILIFE-2745-ENDS
	}


}