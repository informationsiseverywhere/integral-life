package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class Sd5g4screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5g4ScreenVars sv = (Sd5g4ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5g4screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5g4ScreenVars screenVars = (Sd5g4ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.riskclass01.setClassString("");
		screenVars.riskclass02.setClassString("");	
		screenVars.riskclass03.setClassString("");	
		screenVars.riskclass04.setClassString("");	
		screenVars.riskclass05.setClassString("");	
		screenVars.riskclass06.setClassString("");	
		screenVars.riskclass07.setClassString("");	
		screenVars.riskclass08.setClassString("");	
		screenVars.riskclass09.setClassString("");	
		screenVars.riskclass10.setClassString("");	
		
		screenVars.factor11.setClassString("");
		screenVars.factor02.setClassString("");		
		screenVars.factor03.setClassString("");		
		screenVars.factor04.setClassString("");		
		screenVars.factor05.setClassString("");		
		screenVars.factor06.setClassString("");		
		screenVars.factor07.setClassString("");		
		screenVars.factor08.setClassString("");		
		screenVars.factor09.setClassString("");		
		screenVars.factor10.setClassString("");		
		screenVars.formula01.setClassString("");
		screenVars.formula02.setClassString("");	
		screenVars.formula03.setClassString("");	
		
		screenVars.formula04.setClassString("");	
		screenVars.formula05.setClassString("");	
		screenVars.formula06.setClassString("");	
		screenVars.formula07.setClassString("");	
		screenVars.formula08.setClassString("");	
		screenVars.formula09.setClassString("");	
		screenVars.formula10.setClassString("");
		
		screenVars.riskclassdesc01.setClassString("");
		screenVars.riskclassdesc02.setClassString("");
		screenVars.riskclassdesc03.setClassString("");
		screenVars.riskclassdesc04.setClassString("");
		screenVars.riskclassdesc05.setClassString("");
		screenVars.riskclassdesc06.setClassString("");
		screenVars.riskclassdesc07.setClassString("");
		screenVars.riskclassdesc08.setClassString("");
		screenVars.riskclassdesc09.setClassString("");
		screenVars.riskclassdesc10.setClassString("");
	}

/**
 * Clear all the variables in Sd5g4screen
 */
	public static void clear(VarModel pv) {
		Sd5g4ScreenVars screenVars = (Sd5g4ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		
		screenVars.riskclass01.clear();
		screenVars.riskclass02.clear();	
		screenVars.riskclass03.clear();	
		screenVars.riskclass04.clear();	
		screenVars.riskclass05.clear();	
		screenVars.riskclass06.clear();	
		screenVars.riskclass07.clear();	
		screenVars.riskclass08.clear();	
		screenVars.riskclass09.clear();	
		screenVars.riskclass10.clear();	
		
		screenVars.factor11.clear();
		screenVars.factor02.clear();		
		screenVars.factor03.clear();		
		screenVars.factor04.clear();		
		screenVars.factor05.clear();		
		screenVars.factor06.clear();		
		screenVars.factor07.clear();		
		screenVars.factor08.clear();		
		screenVars.factor09.clear();		
		screenVars.factor10.clear();		
		screenVars.formula01.clear();
		screenVars.formula02.clear();	
		screenVars.formula03.clear();	
		
		screenVars.formula04.clear();	
		screenVars.formula05.clear();	
		screenVars.formula06.clear();	
		screenVars.formula07.clear();	
		screenVars.formula08.clear();	
		screenVars.formula09.clear();	
		screenVars.formula10.clear();
		
		screenVars.riskclassdesc01.clear();
		screenVars.riskclassdesc02.clear();
		screenVars.riskclassdesc03.clear();
		screenVars.riskclassdesc04.clear();
		screenVars.riskclassdesc05.clear();
		screenVars.riskclassdesc06.clear();
		screenVars.riskclassdesc07.clear();
		screenVars.riskclassdesc08.clear();
		screenVars.riskclassdesc09.clear();
		screenVars.riskclassdesc10.clear();
		
	}
}
