/*
 * File: Vpxchdr.java
 * Date: 20 August 2012 22:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClexTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   This subroutine extracts policy header info using PREMIUMREC copybook.
*
*
*   Functions :
*     INIT - Initialize and read/extract all necessary data and store it 
*     into memory.
*
*	  GETV Function.
*          This function will return a specific value from VPXCHDR-REC
*          It will accept the name of variable requested, and then return 
*          it as part of VPXCHDRREC
*   Status :
*     **** - O-K
*     E882 - Invalid function
*     H211 - Invalid prefix
*     Unknown - Status returned as a result of database
*     error.
*
*****************************************************************
* </pre>
*/
public class Vpxchdr extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPXCHDR";
		/* FORMATS */
	private String chdrlnbrec = "CHDRLNBREC";
	private String clexrec = "CLEXREC";
		/* ERRORS */
	private String e049 = "E049";
	private String h832 = "H832";
		/* COPYBOOK */
	private Premiumrec premiumrec = new Premiumrec();
	private Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();

	/*Contract Header Life Fields*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	/*Client Extra Details*/
	private ClexTableDAM clexIO = new ClexTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit100
	}

	public Vpxchdr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vpxchdrrec = (Vpxchdrrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		premiumrec.premiumRec.set(vpmcalcrec.linkageArea);
		try {
			para000();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			exit000();
		}
		catch (COBOLExitProgramException e) {
		}
	}


protected void exit1010()
	{
		exitProgram();
	}

protected void para000()
	{
		syserrrec.subrname.set(wsaaSubr);
		vpxchdrrec.statuz.set(varcom.oK);
		if (isEQ(vpxchdrrec.function,"INIT")){
			initialize050();
			findChdrlif100();
		}
		else if (isEQ(vpxchdrrec.function,"GETV")) {
			returnValue200();
		}
		else {
			syserrrec.statuz.set(e049);
			systemError900();
		}
//		exit000();
	}

protected void initialize050()
{
	vpxchdrrec.occdate.set(varcom.maxdate);
	vpxchdrrec.billchnl.set(SPACE);
	vpxchdrrec.cownnum.set(SPACE);
	vpxchdrrec.jownnum.set(SPACE);
	vpxchdrrec.cnttype.set(SPACE);
}

protected void findChdrlif100()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				search100();
			}
			case exit100: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void search100()
{
	chdrlnbIO.setDataKey(SPACES);
	chdrlnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
	chdrlnbIO.setChdrnum(premiumrec.chdrChdrnum);
	
	chdrlnbIO.setStatuz(varcom.oK);
	chdrlnbIO.setFunction(varcom.readr);
	chdrlnbIO.setFormat(chdrlnbrec);
	SmartFileCode.execute(appVars, chdrlnbIO);
	//Unexpected result
	if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
	&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(chdrlnbIO.getStatuz());
		syserrrec.params.set(chdrlnbIO.getParams());
		dbError999();
	}
	
	chdrlnbIO.setFunction(varcom.retrv);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
	&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		dbError999();
	}
	if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)) {
		chdrlnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
		chdrlnbIO.setChdrnum(premiumrec.chdrChdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		//Record not found
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			vpxchdrrec.statuz.set(varcom.mrnf);
			goTo(GotoLabel.exit100);
		}
	}
	
	//Set up output fields
	vpxchdrrec.copybook.set("VPXCHDRREC");
	vpxchdrrec.occdate.set(chdrlnbIO.occdate);
	vpxchdrrec.billchnl.set(chdrlnbIO.billchnl);
	vpxchdrrec.cownnum.set(chdrlnbIO.cownnum);
	vpxchdrrec.jownnum.set(chdrlnbIO.jownnum);
	vpxchdrrec.cnttype.set(chdrlnbIO.cnttype);
	
	if (isNE(chdrlnbIO.getCownnum(), SPACES)){
		findClex300();
	}
	
}
protected void returnValue200()
	{
		start210();
	}


protected void start210()
{
	vpxchdrrec.resultValue.set(SPACES);
	String field = vpxchdrrec.resultField.getData().trim();/* IJTI-1523 */
	if ("occdate".equals(field)){
		vpxchdrrec.resultValue.set(vpxchdrrec.occdate.getbigdata().toString());
	}
	else if("billchnl".equals(field)){
		vpxchdrrec.resultValue.set(vpxchdrrec.billchnl);
	}
	else if("cownnum".equals(field)){
		vpxchdrrec.resultValue.set(vpxchdrrec.cownnum);
	}
	else if("jownnum".equals(field)){
		vpxchdrrec.resultValue.set(vpxchdrrec.jownnum);
	}
	else if("cnttype".equals(field)){
		vpxchdrrec.resultValue.set(vpxchdrrec.cnttype);
	}
	else if("rstaflag".equals(field)){
		vpxchdrrec.resultValue.set(vpxchdrrec.rstaflag);
	}
	else {
		vpxchdrrec.statuz.set(h832);
	}
}

protected void findClex300()
{
	clexIO.setDataKey(SPACES);
	clexIO.setClntcoy(chdrlnbIO.getCowncoy());
	clexIO.setClntnum(chdrlnbIO.getCownnum());
	clexIO.setClntpfx(chdrlnbIO.getCownpfx());
	clexIO.setFunction(varcom.readr);
	clexIO.setFormat(clexrec);
	SmartFileCode.execute(appVars, clexIO);
	if (isNE(clexIO.getStatuz(),varcom.oK)
			&& isNE(clexIO.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(clexIO.statuz);
		syserrrec.params.set(clexIO.getParams());
		dbError999();
	}
	if (isNE(clexIO.getRstaflag(),"Y")
			&& isNE(chdrlnbIO.getJownnum(),SPACES)) {
		clexIO.setClntcoy(chdrlnbIO.getCowncoy());
		clexIO.setClntnum(chdrlnbIO.getJownnum());
		clexIO.setClntpfx(chdrlnbIO.getCownpfx());
		clexIO.setFormat(clexrec);
		clexIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clexIO);
		if (isNE(clexIO.getStatuz(),varcom.oK)
				&& isNE(clexIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(clexIO.statuz);
			syserrrec.params.set(clexIO.getParams());
			dbError999();
		}
	}
	//Set up output fields
	if (isEQ(clexIO.getRstaflag(),"Y")){
		vpxchdrrec.rstaflag.set("Y");
	} else {
		vpxchdrrec.rstaflag.set("N");
	}
	/* EXIT */
}

protected void systemError900()
{
	/*PARA*/
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}
protected void dbError999()
{
	/*PARA*/
	syserrrec.syserrType.set("1");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}

protected void exit000()
{
	exitProgram();
}

}


