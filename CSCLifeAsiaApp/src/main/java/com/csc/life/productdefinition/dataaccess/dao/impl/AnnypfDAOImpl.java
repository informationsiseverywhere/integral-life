package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AnnypfDAOImpl extends BaseDAOImpl<Annypf> implements AnnypfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnnypfDAOImpl.class);

    public Map<Long, List<Annypf>> searchAnnyRecord(List<Long> covrUQ) {
        StringBuilder sqlAnnySelect1 = new StringBuilder("SELECT AY.UNIQUE_NUMBER,AY.NOMLIFE, CV.UNIQUE_NUMBER ");
        sqlAnnySelect1.append("FROM ANNYPF AY,COVRPF CV ");
        sqlAnnySelect1.append(" WHERE AY.VALIDFLAG='1' AND CV.CHDRCOY=AY.CHDRCOY AND CV.CHDRNUM=AY.CHDRNUM ");
        sqlAnnySelect1
                .append(" AND AY.LIFE>=CV.LIFE AND CV.COVERAGE=AY.COVERAGE AND CV.RIDER=AY.RIDER AND CV.PLNSFX=AY.PLNSFX ");
        sqlAnnySelect1.append("AND ");

        sqlAnnySelect1.append(getSqlInLong("CV.UNIQUE_NUMBER", covrUQ));
        sqlAnnySelect1
                .append(" ORDER BY AY.CHDRCOY ASC, AY.CHDRNUM ASC, AY.LIFE ASC, AY.COVERAGE ASC, AY.RIDER ASC, AY.PLNSFX ASC, AY.UNIQUE_NUMBER DESC ");

        PreparedStatement psAnnySelect = getPrepareStatement(sqlAnnySelect1.toString());
        ResultSet sqlannypf1rs = null;
        Map<Long, List<Annypf>> annypfSearchResult = new HashMap<Long, List<Annypf>>();
        try {
            sqlannypf1rs = executeQuery(psAnnySelect);
            while (sqlannypf1rs.next()) {
                Annypf annypf = new Annypf();
                annypf.setUniqueNumber(sqlannypf1rs.getLong(1));
                annypf.setNomlife(sqlannypf1rs.getString(2));
                long uniqueNumber = sqlannypf1rs.getLong(3);
                if (annypfSearchResult.containsKey(uniqueNumber)) {
                    annypfSearchResult.get(uniqueNumber).add(annypf);
                } else {
                    List<Annypf> annypfList = new ArrayList<Annypf>();
                    annypfList.add(annypf);
                    annypfSearchResult.put(uniqueNumber, annypfList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchAnnyRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psAnnySelect, sqlannypf1rs);
        }
        return annypfSearchResult;
    }
    
    public Map<String, List<Annypf>> searchAnnyRecordByChdrnum(List<String> chdrnumList){
    	
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,GUARPERD,FREQANN,ARREARS,ADVANCE,DTHPERCN,DTHPERCO,INTANNY,WITHPROP,WITHOPROP,PPIND,CAPCONT,VALIDFLAG,TRANNO,NOMLIFE  ");
	    sqlSelect1.append("FROM ANNYPF WHERE  ");
	    sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

	    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
	    ResultSet sqlrs = null;
	    Map<String, List<Annypf>> resultMap = new HashMap<>();
	    
	    try {
	    	sqlrs = executeQuery(psSelect);
	
	        while (sqlrs.next()) {
	        	
				Annypf annypf = new Annypf();
				annypf.setUniqueNumber(sqlrs.getLong("Unique_number"));
				annypf.setChdrcoy(sqlrs.getString("chdrcoy"));
				annypf.setChdrnum(sqlrs.getString("chdrnum"));
				annypf.setLife(sqlrs.getString("life"));
				annypf.setCoverage(sqlrs.getString("coverage"));
				annypf.setRider(sqlrs.getString("rider"));
				annypf.setPlanSuffix(sqlrs.getInt("plnsfx"));
				annypf.setGuarperd(sqlrs.getInt("guarperd"));
				annypf.setFreqann(sqlrs.getString("freqann"));
				annypf.setArrears(sqlrs.getString("arrears"));
				annypf.setAdvance(sqlrs.getString("advance"));
				annypf.setDthpercn(sqlrs.getBigDecimal("dthpercn"));
				annypf.setDthperco(sqlrs.getBigDecimal("dthperco"));
				annypf.setIntanny(sqlrs.getBigDecimal("intanny"));
				annypf.setWithprop(sqlrs.getString("withprop"));
				annypf.setWithoprop(sqlrs.getString("withoprop"));
				annypf.setPpind(sqlrs.getString("ppind"));
				annypf.setCapcont(sqlrs.getBigDecimal("capcont"));
				annypf.setValidflag(sqlrs.getString("validflag"));
				annypf.setTranno(sqlrs.getInt("tranno"));
				annypf.setNomlife(sqlrs.getString("nomlife"));
				
				if (resultMap.containsKey(annypf.getChdrnum())) {
					resultMap.get(annypf.getChdrnum()).add(annypf);
				} else {
					List<Annypf> annypfList = new ArrayList<>();
					annypfList.add(annypf);
					resultMap.put(annypf.getChdrnum(), annypfList);
				}
	        }
	    } catch (SQLException e) {
	        LOGGER.error("searchAnnyRecordByChdrnum()", e);//IJTI-1561
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(psSelect, sqlrs);
	    }
		return resultMap;
	}
    
    public Annypf getAnnyRecordByAnnyKey(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plansuffix){
    	   	
		StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, GUARPERD, FREQANN, ARREARS, ADVANCE,DTHPERCN, ");
		sql.append("DTHPERCO, INTANNY, WITHPROP, WITHOPROP, PPIND, CAPCONT, VALIDFLAG, TRANNO, NOMLIFE FROM ANNYPF ");
	    sql.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ");
	    sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

	    PreparedStatement ps = getPrepareStatement(sql.toString());
	    ResultSet rs = null;
	    Annypf annypf = null;
	    
	    try {
	    	ps.setString(1, chdrcoy);
	    	ps.setString(2, chdrnum);
	    	ps.setString(3, life);
	    	ps.setString(4, coverage);
	    	ps.setString(5, rider);
	    	ps.setInt(6, plansuffix);
	    	
	    	rs = executeQuery(ps);
	
	        if (rs.next()) {
	        	
				annypf = new Annypf();
				annypf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				annypf.setChdrcoy(rs.getString("CHDRCOY"));
				annypf.setChdrnum(rs.getString("CHDRNUM"));
				annypf.setLife(rs.getString("LIFE"));
				annypf.setCoverage(rs.getString("COVERAGE"));
				annypf.setRider(rs.getString("RIDER"));
				annypf.setPlanSuffix(rs.getInt("PLNSFX"));
				annypf.setGuarperd(rs.getInt("GUARPERD"));
				annypf.setFreqann(rs.getString("FREQANN"));
				annypf.setArrears(rs.getString("ARREARS"));
				annypf.setAdvance(rs.getString("ADVANCE"));
				annypf.setDthpercn(rs.getBigDecimal("DTHPERCN"));
				annypf.setDthperco(rs.getBigDecimal("DTHPERCO"));
				annypf.setIntanny(rs.getBigDecimal("INTANNY"));
				annypf.setWithprop(rs.getString("WITHPROP"));
				annypf.setWithoprop(rs.getString("WITHOPROP"));
				annypf.setPpind(rs.getString("PPIND"));
				annypf.setCapcont(rs.getBigDecimal("CAPCONT"));
				annypf.setValidflag(rs.getString("VALIDFLAG"));
				annypf.setTranno(rs.getInt("TRANNO"));
				annypf.setNomlife(rs.getString("NOMLIFE"));
				
	        }
	    } catch (SQLException e) {
	        LOGGER.error("getAnnyRecordByAnnyKey()", e);//IJTI-1561
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps, rs);
	    }
		return annypf;
	}

}