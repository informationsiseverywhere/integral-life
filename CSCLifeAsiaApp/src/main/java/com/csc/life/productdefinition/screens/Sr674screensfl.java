package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr674screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 4;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2, 3, 15, 88}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 17, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr674ScreenVars sv = (Sr674ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr674screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr674screensfl, 
			sv.Sr674screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr674ScreenVars sv = (Sr674ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr674screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr674ScreenVars sv = (Sr674ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr674screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr674screensflWritten.gt(0))
		{
			sv.sr674screensfl.setCurrentIndex(0);
			sv.Sr674screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr674ScreenVars sv = (Sr674ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr674screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr674ScreenVars screenVars = (Sr674ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.datakey.setFieldName("datakey");
				screenVars.pstatdate.setFieldName("pstatdate");
				screenVars.sumins01.setFieldName("sumins01");
				screenVars.sumins02.setFieldName("sumins02");
				screenVars.sumins03.setFieldName("sumins03");
				screenVars.sumins04.setFieldName("sumins04");
				screenVars.sumins05.setFieldName("sumins05");
				screenVars.premind.setFieldName("premind");
				screenVars.activeInd.setFieldName("activeInd");
				screenVars.zrwvflg01.setFieldName("zrwvflg01");
				screenVars.zrwvflg02.setFieldName("zrwvflg02");
				screenVars.zrwvflg03.setFieldName("zrwvflg03");
				screenVars.zrwvflg04.setFieldName("zrwvflg04");
				screenVars.workAreaData.setFieldName("workAreaData");
				screenVars.basprm01.setFieldName("basprm01");
				screenVars.basprm02.setFieldName("basprm02");
				screenVars.basprm03.setFieldName("basprm03");
				screenVars.basprm04.setFieldName("basprm04");
				screenVars.basprm05.setFieldName("basprm05");
				screenVars.zloprm01.setFieldName("zloprm01");
				screenVars.zloprm02.setFieldName("zloprm02");
				screenVars.zloprm03.setFieldName("zloprm03");
				screenVars.zloprm04.setFieldName("zloprm04");
				screenVars.zloprm05.setFieldName("zloprm05");
				screenVars.prem01.setFieldName("prem01");
				screenVars.prem02.setFieldName("prem02");
				screenVars.prem03.setFieldName("prem03");
				screenVars.prem04.setFieldName("prem04");
				screenVars.prem05.setFieldName("prem05");
				screenVars.crtable.setFieldName("crtable");
				screenVars.entity.setFieldName("entity");
				screenVars.prmtrb01.setFieldName("prmtrb01");
				screenVars.prmtrb02.setFieldName("prmtrb02");
				screenVars.prmtrb03.setFieldName("prmtrb03");
				screenVars.prmtrb04.setFieldName("prmtrb04");
				screenVars.prmtrb05.setFieldName("prmtrb05");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.datakey.set(dm.getField("datakey"));
			screenVars.pstatdate.set(dm.getField("pstatdate"));
			screenVars.sumins01.set(dm.getField("sumins01"));
			screenVars.sumins02.set(dm.getField("sumins02"));
			screenVars.sumins03.set(dm.getField("sumins03"));
			screenVars.sumins04.set(dm.getField("sumins04"));
			screenVars.sumins05.set(dm.getField("sumins05"));
			screenVars.premind.set(dm.getField("premind"));
			screenVars.activeInd.set(dm.getField("activeInd"));
			screenVars.zrwvflg01.set(dm.getField("zrwvflg01"));
			screenVars.zrwvflg02.set(dm.getField("zrwvflg02"));
			screenVars.zrwvflg03.set(dm.getField("zrwvflg03"));
			screenVars.zrwvflg04.set(dm.getField("zrwvflg04"));
			screenVars.workAreaData.set(dm.getField("workAreaData"));
			screenVars.basprm01.set(dm.getField("basprm01"));
			screenVars.basprm02.set(dm.getField("basprm02"));
			screenVars.basprm03.set(dm.getField("basprm03"));
			screenVars.basprm04.set(dm.getField("basprm04"));
			screenVars.basprm05.set(dm.getField("basprm05"));
			screenVars.zloprm01.set(dm.getField("zloprm01"));
			screenVars.zloprm02.set(dm.getField("zloprm02"));
			screenVars.zloprm03.set(dm.getField("zloprm03"));
			screenVars.zloprm04.set(dm.getField("zloprm04"));
			screenVars.zloprm05.set(dm.getField("zloprm05"));
			screenVars.prem01.set(dm.getField("prem01"));
			screenVars.prem02.set(dm.getField("prem02"));
			screenVars.prem03.set(dm.getField("prem03"));
			screenVars.prem04.set(dm.getField("prem04"));
			screenVars.prem05.set(dm.getField("prem05"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.entity.set(dm.getField("entity"));
			screenVars.prmtrb01.set(dm.getField("prmtrb01"));
			screenVars.prmtrb02.set(dm.getField("prmtrb02"));
			screenVars.prmtrb03.set(dm.getField("prmtrb03"));
			screenVars.prmtrb04.set(dm.getField("prmtrb04"));
			screenVars.prmtrb05.set(dm.getField("prmtrb05"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr674ScreenVars screenVars = (Sr674ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.datakey.setFieldName("datakey");
				screenVars.pstatdate.setFieldName("pstatdate");
				screenVars.sumins01.setFieldName("sumins01");
				screenVars.sumins02.setFieldName("sumins02");
				screenVars.sumins03.setFieldName("sumins03");
				screenVars.sumins04.setFieldName("sumins04");
				screenVars.sumins05.setFieldName("sumins05");
				screenVars.premind.setFieldName("premind");
				screenVars.activeInd.setFieldName("activeInd");
				screenVars.zrwvflg01.setFieldName("zrwvflg01");
				screenVars.zrwvflg02.setFieldName("zrwvflg02");
				screenVars.zrwvflg03.setFieldName("zrwvflg03");
				screenVars.zrwvflg04.setFieldName("zrwvflg04");
				screenVars.workAreaData.setFieldName("workAreaData");
				screenVars.basprm01.setFieldName("basprm01");
				screenVars.basprm02.setFieldName("basprm02");
				screenVars.basprm03.setFieldName("basprm03");
				screenVars.basprm04.setFieldName("basprm04");
				screenVars.basprm05.setFieldName("basprm05");
				screenVars.zloprm01.setFieldName("zloprm01");
				screenVars.zloprm02.setFieldName("zloprm02");
				screenVars.zloprm03.setFieldName("zloprm03");
				screenVars.zloprm04.setFieldName("zloprm04");
				screenVars.zloprm05.setFieldName("zloprm05");
				screenVars.prem01.setFieldName("prem01");
				screenVars.prem02.setFieldName("prem02");
				screenVars.prem03.setFieldName("prem03");
				screenVars.prem04.setFieldName("prem04");
				screenVars.prem05.setFieldName("prem05");
				screenVars.crtable.setFieldName("crtable");
				screenVars.entity.setFieldName("entity");
				screenVars.prmtrb01.setFieldName("prmtrb01");
				screenVars.prmtrb02.setFieldName("prmtrb02");
				screenVars.prmtrb03.setFieldName("prmtrb03");
				screenVars.prmtrb04.setFieldName("prmtrb04");
				screenVars.prmtrb05.setFieldName("prmtrb05");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("datakey").set(screenVars.datakey);
			dm.getField("pstatdate").set(screenVars.pstatdate);
			dm.getField("sumins01").set(screenVars.sumins01);
			dm.getField("sumins02").set(screenVars.sumins02);
			dm.getField("sumins03").set(screenVars.sumins03);
			dm.getField("sumins04").set(screenVars.sumins04);
			dm.getField("sumins05").set(screenVars.sumins05);
			dm.getField("premind").set(screenVars.premind);
			dm.getField("activeInd").set(screenVars.activeInd);
			dm.getField("zrwvflg01").set(screenVars.zrwvflg01);
			dm.getField("zrwvflg02").set(screenVars.zrwvflg02);
			dm.getField("zrwvflg03").set(screenVars.zrwvflg03);
			dm.getField("zrwvflg04").set(screenVars.zrwvflg04);
			dm.getField("workAreaData").set(screenVars.workAreaData);
			dm.getField("basprm01").set(screenVars.basprm01);
			dm.getField("basprm02").set(screenVars.basprm02);
			dm.getField("basprm03").set(screenVars.basprm03);
			dm.getField("basprm04").set(screenVars.basprm04);
			dm.getField("basprm05").set(screenVars.basprm05);
			dm.getField("zloprm01").set(screenVars.zloprm01);
			dm.getField("zloprm02").set(screenVars.zloprm02);
			dm.getField("zloprm03").set(screenVars.zloprm03);
			dm.getField("zloprm04").set(screenVars.zloprm04);
			dm.getField("zloprm05").set(screenVars.zloprm05);
			dm.getField("prem01").set(screenVars.prem01);
			dm.getField("prem02").set(screenVars.prem02);
			dm.getField("prem03").set(screenVars.prem03);
			dm.getField("prem04").set(screenVars.prem04);
			dm.getField("prem05").set(screenVars.prem05);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("entity").set(screenVars.entity);
			dm.getField("prmtrb01").set(screenVars.prmtrb01);
			dm.getField("prmtrb02").set(screenVars.prmtrb02);
			dm.getField("prmtrb03").set(screenVars.prmtrb03);
			dm.getField("prmtrb04").set(screenVars.prmtrb04);
			dm.getField("prmtrb05").set(screenVars.prmtrb05);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr674screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr674ScreenVars screenVars = (Sr674ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.datakey.clearFormatting();
		screenVars.pstatdate.clearFormatting();
		screenVars.sumins01.clearFormatting();
		screenVars.sumins02.clearFormatting();
		screenVars.sumins03.clearFormatting();
		screenVars.sumins04.clearFormatting();
		screenVars.sumins05.clearFormatting();
		screenVars.premind.clearFormatting();
		screenVars.activeInd.clearFormatting();
		screenVars.zrwvflg01.clearFormatting();
		screenVars.zrwvflg02.clearFormatting();
		screenVars.zrwvflg03.clearFormatting();
		screenVars.zrwvflg04.clearFormatting();
		screenVars.workAreaData.clearFormatting();
		screenVars.basprm01.clearFormatting();
		screenVars.basprm02.clearFormatting();
		screenVars.basprm03.clearFormatting();
		screenVars.basprm04.clearFormatting();
		screenVars.basprm05.clearFormatting();
		screenVars.zloprm01.clearFormatting();
		screenVars.zloprm02.clearFormatting();
		screenVars.zloprm03.clearFormatting();
		screenVars.zloprm04.clearFormatting();
		screenVars.zloprm05.clearFormatting();
		screenVars.prem01.clearFormatting();
		screenVars.prem02.clearFormatting();
		screenVars.prem03.clearFormatting();
		screenVars.prem04.clearFormatting();
		screenVars.prem05.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.entity.clearFormatting();
		screenVars.prmtrb01.clearFormatting();
		screenVars.prmtrb02.clearFormatting();
		screenVars.prmtrb03.clearFormatting();
		screenVars.prmtrb04.clearFormatting();
		screenVars.prmtrb05.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr674ScreenVars screenVars = (Sr674ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.datakey.setClassString("");
		screenVars.pstatdate.setClassString("");
		screenVars.sumins01.setClassString("");
		screenVars.sumins02.setClassString("");
		screenVars.sumins03.setClassString("");
		screenVars.sumins04.setClassString("");
		screenVars.sumins05.setClassString("");
		screenVars.premind.setClassString("");
		screenVars.activeInd.setClassString("");
		screenVars.zrwvflg01.setClassString("");
		screenVars.zrwvflg02.setClassString("");
		screenVars.zrwvflg03.setClassString("");
		screenVars.zrwvflg04.setClassString("");
		screenVars.workAreaData.setClassString("");
		screenVars.basprm01.setClassString("");
		screenVars.basprm02.setClassString("");
		screenVars.basprm03.setClassString("");
		screenVars.basprm04.setClassString("");
		screenVars.basprm05.setClassString("");
		screenVars.zloprm01.setClassString("");
		screenVars.zloprm02.setClassString("");
		screenVars.zloprm03.setClassString("");
		screenVars.zloprm04.setClassString("");
		screenVars.zloprm05.setClassString("");
		screenVars.prem01.setClassString("");
		screenVars.prem02.setClassString("");
		screenVars.prem03.setClassString("");
		screenVars.prem04.setClassString("");
		screenVars.prem05.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.entity.setClassString("");
		screenVars.prmtrb01.setClassString("");
		screenVars.prmtrb02.setClassString("");
		screenVars.prmtrb03.setClassString("");
		screenVars.prmtrb04.setClassString("");
		screenVars.prmtrb05.setClassString("");
	}

/**
 * Clear all the variables in Sr674screensfl
 */
	public static void clear(VarModel pv) {
		Sr674ScreenVars screenVars = (Sr674ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.datakey.clear();
		screenVars.pstatdate.clear();
		screenVars.sumins01.clear();
		screenVars.sumins02.clear();
		screenVars.sumins03.clear();
		screenVars.sumins04.clear();
		screenVars.sumins05.clear();
		screenVars.premind.clear();
		screenVars.activeInd.clear();
		screenVars.zrwvflg01.clear();
		screenVars.zrwvflg02.clear();
		screenVars.zrwvflg03.clear();
		screenVars.zrwvflg04.clear();
		screenVars.workAreaData.clear();
		screenVars.basprm01.clear();
		screenVars.basprm02.clear();
		screenVars.basprm03.clear();
		screenVars.basprm04.clear();
		screenVars.basprm05.clear();
		screenVars.zloprm01.clear();
		screenVars.zloprm02.clear();
		screenVars.zloprm03.clear();
		screenVars.zloprm04.clear();
		screenVars.zloprm05.clear();
		screenVars.prem01.clear();
		screenVars.prem02.clear();
		screenVars.prem03.clear();
		screenVars.prem04.clear();
		screenVars.prem05.clear();
		screenVars.crtable.clear();
		screenVars.entity.clear();
		screenVars.prmtrb01.clear();
		screenVars.prmtrb02.clear();
		screenVars.prmtrb03.clear();
		screenVars.prmtrb04.clear();
		screenVars.prmtrb05.clear();
	}
}
