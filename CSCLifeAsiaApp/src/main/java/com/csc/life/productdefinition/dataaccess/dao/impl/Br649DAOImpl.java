package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.Br649DAO;
import com.csc.life.productdefinition.dataaccess.model.MerxTemppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br649DAOImpl extends BaseDAOImpl<MerxTemppf>implements Br649DAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br649DAOImpl.class);

	public List<MerxTemppf> loadDataByBatch(int batchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM BR649DATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, SEQNO DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<MerxTemppf> merxList = new ArrayList<MerxTemppf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				MerxTemppf merxpf = new MerxTemppf();
				merxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				merxpf.setChdrcoy(rs.getString("CHDRCOY").trim());
				merxpf.setChdrnum(rs.getString("CHDRNUM").trim());
				merxpf.setSeqno(rs.getInt("SEQNO"));
				merxpf.setPaidby(rs.getString("PAIDBY").trim());
				merxpf.setExmcode(rs.getString("EXMCODE").trim());
				merxpf.setZmedtyp(rs.getString("ZMEDTYP").trim());
				merxpf.setEffdate(rs.getInt("EFFDATE"));
				merxpf.setInvref(rs.getString("INVREF").trim());
				merxpf.setZmedfee(rs.getBigDecimal("ZMEDFEE"));
				merxpf.setMemberName(rs.getString("MEMBER_NAME").trim());
				merxList.add(merxpf);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return merxList;
	}	
	
	public int populateBr649Temp(int batchExtractSize, String merxtempTable) {
		initializeBr649Temp();
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.BR649DATA "); //ILB-475
		sb.append("(BATCHID, UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SEQNO, PAIDBY, EXMCODE, CLNTNUM, NAME, ZMEDTYP, EFFDATE, INVREF, ZMEDFEE, DESC_T, MEMBER_NAME)  ");
		sb.append("SELECT * FROM(  ");
		sb.append("SELECT floor((row_number()over(ORDER BY MAIN.CHDRCOY, MAIN.CHDRNUM, MAIN.SEQNO DESC)-1)/?) BATCHID, MAIN.* FROM (  ");
		sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SEQNO, PAIDBY, EXMCODE, CLNTNUM, NAME, ZMEDTYP, EFFDATE, INVREF, ZMEDFEE, DESC_T, MEMBER_NAME "); 
		sb.append("FROM  ");
		sb.append(merxtempTable); 
		sb.append(" MERX ) MAIN ");
		sb.append(") MAIN ORDER BY MAIN.CHDRCOY, MAIN.CHDRNUM, MAIN.SEQNO DESC");


		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBr649Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeBr649Temp() {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM BR649DATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			int rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBr649Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}		


}