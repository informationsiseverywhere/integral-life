/*
 * File: Th611pt.java
 * Date: 30 August 2009 2:36:59
 * Author: Quipoz Limited
 * 
 * Class transformed from TH611PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Th611rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH611.
*
*
*****************************************************************
* </pre>
*/
public class Th611pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Minimum/Maximum Modal Premium               SH611");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 25, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(44);
	private FixedLengthStringData filler9 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine004, 25, FILLER).init("Contribution Limits");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(55);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine005, 11, FILLER).init("Frequency            Min                 Max");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(64);
	private FixedLengthStringData filler13 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 14);
	private FixedLengthStringData filler14 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(64);
	private FixedLengthStringData filler16 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 14);
	private FixedLengthStringData filler17 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(64);
	private FixedLengthStringData filler19 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 14);
	private FixedLengthStringData filler20 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(64);
	private FixedLengthStringData filler22 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 14);
	private FixedLengthStringData filler23 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(64);
	private FixedLengthStringData filler25 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 14);
	private FixedLengthStringData filler26 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(64);
	private FixedLengthStringData filler28 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 14);
	private FixedLengthStringData filler29 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(64);
	private FixedLengthStringData filler31 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 14);
	private FixedLengthStringData filler32 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(64);
	private FixedLengthStringData filler34 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 14);
	private FixedLengthStringData filler35 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 26).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 46).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th611rec th611rec = new Th611rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th611pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th611rec.th611Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(th611rec.frequency01);
		fieldNo008.set(th611rec.cmin01);
		fieldNo009.set(th611rec.cmax01);
		fieldNo010.set(th611rec.frequency02);
		fieldNo011.set(th611rec.cmin02);
		fieldNo012.set(th611rec.cmax02);
		fieldNo013.set(th611rec.frequency03);
		fieldNo014.set(th611rec.cmin03);
		fieldNo015.set(th611rec.cmax03);
		fieldNo016.set(th611rec.frequency04);
		fieldNo017.set(th611rec.cmin04);
		fieldNo018.set(th611rec.cmax04);
		fieldNo019.set(th611rec.frequency05);
		fieldNo020.set(th611rec.cmin05);
		fieldNo021.set(th611rec.cmax05);
		fieldNo022.set(th611rec.frequency06);
		fieldNo023.set(th611rec.cmin06);
		fieldNo024.set(th611rec.cmax06);
		fieldNo025.set(th611rec.frequency07);
		fieldNo026.set(th611rec.cmin07);
		fieldNo027.set(th611rec.cmax07);
		fieldNo028.set(th611rec.frequency08);
		fieldNo029.set(th611rec.cmin08);
		fieldNo030.set(th611rec.cmax08);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
