package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:45
 * Description:
 * Copybook name: MLIAENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mliaenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mliaenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mliaenqKey = new FixedLengthStringData(64).isAPartOf(mliaenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData mliaenqSecurityno = new FixedLengthStringData(15).isAPartOf(mliaenqKey, 0);
  	public FixedLengthStringData mliaenqMlentity = new FixedLengthStringData(16).isAPartOf(mliaenqKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(33).isAPartOf(mliaenqKey, 31, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mliaenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mliaenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}