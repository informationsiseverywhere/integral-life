package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:05
 * Description:
 * Copybook name: VLPDSUBREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vlpdsubrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData validRec = new FixedLengthStringData(1975);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(validRec, 0);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(validRec, 4);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(validRec, 8);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(validRec, 9);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(validRec, 17);
  	public FixedLengthStringData srcebus = new FixedLengthStringData(2).isAPartOf(validRec, 20);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(validRec, 22).setUnsigned();
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(validRec, 30);
  	public FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(validRec, 31);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(validRec, 39);
  	public FixedLengthStringData rid1Details = new FixedLengthStringData(12).isAPartOf(validRec, 41);
  	public FixedLengthStringData rid1Life = new FixedLengthStringData(2).isAPartOf(rid1Details, 0);
  	public FixedLengthStringData rid1Jlife = new FixedLengthStringData(2).isAPartOf(rid1Details, 2);
  	public FixedLengthStringData rid1Coverage = new FixedLengthStringData(2).isAPartOf(rid1Details, 4);
  	public FixedLengthStringData rid1Rider = new FixedLengthStringData(2).isAPartOf(rid1Details, 6);
  	public FixedLengthStringData rid1Crtable = new FixedLengthStringData(4).isAPartOf(rid1Details, 8);
  	public FixedLengthStringData rid2Details = new FixedLengthStringData(12).isAPartOf(validRec, 53);
  	public FixedLengthStringData rid2Life = new FixedLengthStringData(2).isAPartOf(rid2Details, 0);
  	public FixedLengthStringData rid2Jlife = new FixedLengthStringData(2).isAPartOf(rid2Details, 2);
  	public FixedLengthStringData rid2Coverage = new FixedLengthStringData(2).isAPartOf(rid2Details, 4);
  	public FixedLengthStringData rid2Rider = new FixedLengthStringData(2).isAPartOf(rid2Details, 6);
  	public FixedLengthStringData rid2Crtable = new FixedLengthStringData(4).isAPartOf(rid2Details, 8);
  	public FixedLengthStringData inputDetail = new FixedLengthStringData(1750).isAPartOf(validRec, 65);
  	public FixedLengthStringData[] inputs = FLSArrayPartOfStructure(50, 35, inputDetail, 0);
  	public FixedLengthStringData[] inputLife = FLSDArrayPartOfArrayStructure(2, inputs, 0);
  	public FixedLengthStringData[] inputJlife = FLSDArrayPartOfArrayStructure(2, inputs, 2);
  	public FixedLengthStringData[] inputCoverage = FLSDArrayPartOfArrayStructure(2, inputs, 4);
  	public FixedLengthStringData[] inputRider = FLSDArrayPartOfArrayStructure(2, inputs, 6);
  	public FixedLengthStringData[] inputCrtable = FLSDArrayPartOfArrayStructure(4, inputs, 8);
  	public ZonedDecimalData[] inputRtrm = ZDArrayPartOfArrayStructure(3, 0, inputs, 12, UNSIGNED_TRUE);
  	public ZonedDecimalData[] inputPtrm = ZDArrayPartOfArrayStructure(3, 0, inputs, 15, UNSIGNED_TRUE);
  	public ZonedDecimalData[] inputSa = ZDArrayPartOfArrayStructure(17, 2, inputs, 18, UNSIGNED_TRUE);
  	public FixedLengthStringData errDetails = new FixedLengthStringData(160).isAPartOf(validRec, 1815);
  	public FixedLengthStringData[] errDetail = FLSArrayPartOfStructure(20, 8, errDetails, 0);
  	public FixedLengthStringData[] errCode = FLSDArrayPartOfArrayStructure(4, errDetail, 0);
  	public FixedLengthStringData[] errDet = FLSDArrayPartOfArrayStructure(4, errDetail, 4);


	public void initialize() {
		COBOLFunctions.initialize(validRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		validRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}