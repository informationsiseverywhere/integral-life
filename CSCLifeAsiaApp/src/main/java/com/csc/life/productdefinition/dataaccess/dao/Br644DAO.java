package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br644DAO extends BaseDAO<Medipf> {
	public List<Medipf> loadDataByBatch(int batchID);
	public int populateBr644Temp(String company, int batchExtractSize, String chdrfrom, String chdrto);
}
