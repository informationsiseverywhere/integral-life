/*
 * File: Pr556.java
 * Date: 30 August 2009 1:40:33
 * Author: Quipoz Limited
 *
 * Class transformed from PR556.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClftpfDAO;
import com.csc.fsu.clients.dataaccess.dao.FluppfDAO;
import com.csc.fsu.clients.dataaccess.dao.impl.ClftpfDAOImpl;
import com.csc.fsu.clients.dataaccess.model.Clftpf;
import com.csc.fsu.clients.dataaccess.model.Fluppf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.clients.tablestructures.Tr5awrec;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Billreq1;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Billreqrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.FluprevTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.AcblenqTableDAM;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.enquiries.procedures.Uwlmtchk;
import com.csc.life.enquiries.recordstructures.Uwlmtrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.general.dataaccess.dao.DshnpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Th609rec;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.screens.Sr556ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5651rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.underwriting.dataaccess.dao.UndlpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undlpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!
*REMARKS.
*
* This program is the capture underwriting approval Update.
*
* Introduce Underwriting Approval as a Separate Transaction.
*
* With this funcitionaly, risks above pre-ascertained levels
* must be underwritten otherwise the contract cannot be issued.
*
*****************************************************************
* </pre>
*/
public class Pr556 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR556");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaWorkArea = new FixedLengthStringData(90);
	protected FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8).isAPartOf(wsaaWorkArea, 0);
	protected FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2).isAPartOf(wsaaWorkArea, 8);
	protected FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1).isAPartOf(wsaaWorkArea, 10);
	protected FixedLengthStringData wsaaName = new FixedLengthStringData(50).isAPartOf(wsaaWorkArea, 11);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).isAPartOf(wsaaWorkArea, 61).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaWorkArea, 69).setUnsigned();
	private ZonedDecimalData wsaaOldTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaWorkArea, 74).setUnsigned();
	protected FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaWorkArea, 79);
	protected FixedLengthStringData wsaaItem = new FixedLengthStringData(8).isAPartOf(wsaaWorkArea, 82);
	private String wsaaFoundLext = "";

	private FixedLengthStringData wsaaErrorlineSs = new FixedLengthStringData(88);
	private FixedLengthStringData wsaaLeveldss = new FixedLengthStringData(6).isAPartOf(wsaaErrorlineSs, 0);
	private FixedLengthStringData wsaaLevelss = new FixedLengthStringData(3).isAPartOf(wsaaErrorlineSs, 6);
	private FixedLengthStringData wsaaErrss1 = new FixedLengthStringData(20).isAPartOf(wsaaErrorlineSs, 9);
	private ZonedDecimalData wsaaTrsaSs = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineSs, 29).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaErrss2 = new FixedLengthStringData(19).isAPartOf(wsaaErrorlineSs, 49);
	private ZonedDecimalData wsaaUwlmtSs = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineSs, 68).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");

	private FixedLengthStringData wsaaErrorlineS = new FixedLengthStringData(80);
	private FixedLengthStringData wsaaLevelds = new FixedLengthStringData(6).isAPartOf(wsaaErrorlineS, 0);
	private FixedLengthStringData wsaaLevels = new FixedLengthStringData(3).isAPartOf(wsaaErrorlineS, 6);
	private FixedLengthStringData wsaaErrs1 = new FixedLengthStringData(16).isAPartOf(wsaaErrorlineS, 9);
	private ZonedDecimalData wsaaTrsaS = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineS, 25).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaErrs2 = new FixedLengthStringData(15).isAPartOf(wsaaErrorlineS, 45);
	private ZonedDecimalData wsaaUwlmtS = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineS, 60).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private static final String e032 = "E032";
	private static final String hl03 = "HL03";
	private static final String hl01 = "HL01";
	private static final String t5679 = "T5679";
	private static final String t5661 = "T5661";
	private static final String th609 = "TH609";
	private static final String th506="TH506";
	private static final String tr384 = "TR384";
	private static final String h017 = "H017";
	protected FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private Th609rec th609rec = new Th609rec();
	private T5651rec t5651rec = new T5651rec();
	private T5661rec t5661rec = new T5661rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Th506rec th506rec = new Th506rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Namadrsrec namadrsrec = new Namadrsrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Uwlmtrec uwlmtrec = new Uwlmtrec();
	private Sr556ScreenVars sv = getLScreenVars();
	protected FormatsInner formatsInner = new FormatsInner();
private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> t5651List;
	private UndlpfDAO undldao =getApplicationContext().getBean("undlpfDAO",UndlpfDAO.class);
	private List<Undlpf> undlList=null;
	 private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	 private List<Lextpf> lextList;
	 private Lextpf lextpfObj=null;

	private ClftpfDAO clftpfdao=new ClftpfDAOImpl();
	private boolean isFatcaAllowed=false;
	private DescDAO descdao =new DescDAOImpl(); 
	private Map<String,Descpf> Fatcastatus;
	
	
	
	



	private IntegerData wsaaTr5awIx = new IntegerData();
	private static final int wsaaTr5awSize = 1000;
	private WsaaTr5awArrayInner wsaaTr5awArrayInner = new WsaaTr5awArrayInner();
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("flupDAO", FluppfDAO.class);
	private Fluppf fluppf;
	private static final String tr5aw = "TR5AW";
	private Tr5awrec tr5awrec = new Tr5awrec();
	private List<Fluppf> flupList;
	//ILIFE-6289
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private Chdrpf chdrpf = null;
	private Ptrnpf ptrnpf = null;
	private List<Ptrnpf> ptrnpfList = null;
	private List<Itempf> itdmList = new ArrayList();
	private Itempf itdmpf = null;
	/*
	 * nbs009 fwang3 start
	 */
	private TablesInner tablesInner = new TablesInner();
	private ZonedDecimalData wsbbSub = new ZonedDecimalData(2, 0).setUnsigned();
	private WsaaBillingInformationInner wsaaBillingInformationInner = new WsaaBillingInformationInner();
	private String wsaaSingPrmInd = "N";
	private PackedDecimalData wsaaModalPremium = new PackedDecimalData(15, 2);
	
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private Txcalcrec txcalcrec = new Txcalcrec();
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	private ZonedDecimalData wsaaTotlprm = new ZonedDecimalData(18,2); 
	private ZonedDecimalData wsaaCntfee = new ZonedDecimalData(17, 2);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSuspense = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSingpFee = new ZonedDecimalData(17, 2);
	private String wsaaSuspInd = "N";
	/*01  WSAA-T3625-KEY.                                         <004>*/
	private FixedLengthStringData wsaaPayrkey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrkey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrkey, 8);

	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfeeTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingpfeeTax = new PackedDecimalData(17, 2);

	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
	private ClblTableDAM clblIO = new ClblTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T3695rec t3695rec = new T3695rec();
	private T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	private T6687rec t6687rec = new T6687rec();
	private T6654rec t6654rec = new T6654rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private T3629rec t3629rec = new T3629rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Prasrec prasrec = new Prasrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();

	private PayrpfDAO payrpfFSUDAO = getApplicationContext().getBean("payrpfFSUDAO", PayrpfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private boolean isbillday = false; //ILIFE-4128
	private List<Itempf> itdmpfList;
	private Itempf itempf;
	private Billreqrec billreqrec = new Billreqrec();
	private boolean nbs009Permission;
	private ZonedDecimalData cntfee = new ZonedDecimalData(17,2);
	private static final String NBS009_FEATRUE_ID = "NBPRP105"; 
	private static final String T5674 = "T5674"; 
	private BextpfDAO bextpfDAO = getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private DshnpfDAO dshnpfDAO = getApplicationContext().getBean("dshnpfDAO", DshnpfDAO.class);
	// nbs009 fwang3 end

	//ILB-555 start
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private Lifepf lifepf = new Lifepf();
	//end
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	//IBPLIFE-2274 Starts
	private Gensswrec gensswrec = new Gensswrec();	
	private static final String ONE_P_CASHLESS="NBPRP124"; 
	private boolean onePflag = false;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);	
	private FixedLengthStringData wsaa1Pflag = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private ErrorsInner errorsInner = new ErrorsInner();
	private T3615rec t3615rec = new T3615rec();		
	//IBPLIFE-2274 Ends
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		undlIo2040,
		printLetter3030a,
		exit3090
	}

	public Pr556() {
		super();
		screenVars = sv;
		new ScreenModel("Sr556", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		initialise1010();
		retrvChdrlnb1020();
		getHpadrec1030();
		getCtypedes1040();
		getLinsname1050();
		getJlinsname1055();
		getCownname1060();
		getJownname1065();
		displayFld1070();
		FatcaStatus2290();
	}

protected void initialise1010()
	{	
		initialize(sv.dataArea);
		initialize(wsaaWorkArea);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		onePflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ONE_P_CASHLESS, appVars, "IT"); //IBPLIFE-2274		
		wsaaBatckey.set(wsspcomn.batchkey);		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.huwdcdte.set(datcon1rec.intDate);
		wsaaToday.set(datcon1rec.intDate);				
		isFatcaAllowed= FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "CLMTN008", appVars, "IT");
		if(!isFatcaAllowed){
			sv.fatcastatusFlag.set("Y");
		}
		else{
			sv.fatcastatusFlag.set("N");
		}
		if (onePflag && isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		wsaa1Pflag.set("X");
	}

protected void loadTables() {
	/*    Read table TR52D using CHDRLNB-REGISTER as key               */
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tablesInner.tr52d.toString());
	itempf.setItemitem(chdrlnbIO.getRegister().toString());
	itempf = itemDAO.getItempfRecord(itempf);
	if (null == itempf) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr52d.toString());
		itempf.setItemitem("***");
		itempf = itemDAO.getItempfRecord(itempf);
		if (null == itempf) {
			syserrrec.params.set(tablesInner.tr52d);
			fatalError600();
		}
	}
	tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	/*    Read the Program  table T5645 for the Financial Accounting*/
	/*    Rules for the transaction.*/
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tablesInner.t5645.toString());
	itempf.setItemitem(wsaaProg.toString());
	itempf = itemDAO.getItempfRecord(itempf);
	if(null == itempf) {
		syserrrec.params.set(tablesInner.t5645);
		fatalError600();
	}
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}

private void checkSuspense() {
	wsaaSuspInd = "N";
	acblenqIO.setDataArea(SPACES);
	acblenqIO.setRldgacct(ZERO);
	acblenqIO.setRldgcoy(wsspcomn.company);
	acblenqIO.setRldgacct(chdrlnbIO.getChdrnum());
	acblenqIO.setSacscode(t5645rec.sacscode01);
	acblenqIO.setSacstyp(t5645rec.sacstype01);
	for (wsaaSub.set(1); !(isGT(wsaaSub, 3)
			|| isEQ(wsaaSuspInd, "Y")); wsaaSub.add(1)){
		checkSuspense1c100();
	}
}


protected void retrvChdrlnb1020()
	{
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		wsaaCnttype.set(chdrlnbIO.getCnttype());		
	}

protected void getHpadrec1030()
	{
		hpadIO.setDataKey(SPACES);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFunction("READR");
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
	}

protected void getCtypedes1040()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
		descIO.setDesctabl("T5688");
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
	}
//modified for ILB-555
protected void getLinsname1050()
	{
		lifepf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		lifepf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		lifepf.setLife("01");
		lifepf.setJlife(ZERO.stringValue(2));
		lifepf = lifepfDAO.getLifepf(lifepf);
		if (null==lifepf) {
			fatalError600();
		}
		wsaaClntNumber.set(lifepf.getLifcnum());
		wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
		wsaaClntCompany.set(chdrlnbIO.getCowncoy());
		getName1100();
		sv.lifcnum.set(lifepf.getLifcnum());
		sv.linsname.set(wsaaName);
	}

protected void getJlinsname1055()
	{
	lifepf = new Lifepf();
	lifepf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
	lifepf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	lifepf.setLife("01");
	lifepf.setJlife("01");
	lifepf = lifepfDAO.getLifepf(lifepf);
	if (null == lifepf.getLifcnum()) {
		sv.jlifcnum.set(SPACES);
		sv.jlinsname.set(SPACES);
	}
	else {
		wsaaClntNumber.set(lifepf.getLifcnum());//IBPLIFE-6537  
		wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
		wsaaClntCompany.set(chdrlnbIO.getCowncoy());
		getName1100();
		sv.jlifcnum.set(lifepf.getLifcnum());//IBPLIFE-6537  
		sv.jlinsname.set(wsaaName);
	}
	/*IBPLIFE-6432 ends */
	}

protected void getCownname1060()
	{
		wsaaClntNumber.set(chdrlnbIO.getCownnum());
		wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
		wsaaClntCompany.set(chdrlnbIO.getCowncoy());
		getName1100();
		sv.cownnum.set(chdrlnbIO.getCownnum());
		sv.ownername.set(wsaaName);
	}

protected void getJownname1065()
	{
		if (isNE(chdrlnbIO.getJownnum(),SPACES)) {
			wsaaClntNumber.set(chdrlnbIO.getJownnum());
			wsaaClntPrefix.set(chdrlnbIO.getCownpfx());
			wsaaClntCompany.set(chdrlnbIO.getCowncoy());
			getName1100();
			sv.jownnum.set(chdrlnbIO.getJownnum());
			sv.jownname.set(wsaaName);
		}
		else {
			sv.jownname.set(SPACES);
		}
	}

protected void displayFld1070()
	{
		sv.occdate.set(chdrlnbIO.getOccdate());
		sv.hpropdte.set(hpadIO.getHpropdte());
		sv.hprrcvdt.set(hpadIO.getHprrcvdt());
		/*PROTECT*/
		if (isEQ(wsspcomn.flag,"R")) {
			sv.huwdcdte.set(hpadIO.getHuwdcdte());
			sv.huwdcdteOut[varcom.pr.toInt()].set("Y");
		}
		/*EXIT*/
	}
protected void FatcaStatus2290(){
	
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setItemtabl(th506);
	itemIO.setItemitem(chdrlnbIO.getCnttype());
	//itemIO.setFormat(formatsInner.itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		if (isNE(itemIO.getItemitem(), "***")) {
			itemIO.setItemitem("***");
			//itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
	}
	if (isEQ(itemIO.getStatuz(), varcom.oK)) {
		th506rec.th506Rec.set(itemIO.getGenarea());
	}
	else {
		th506rec.th506Rec.set(SPACES);
	}	
	
	if(isFatcaAllowed){
		Fatcastatus= descdao.getItems("IT", wsspcomn.fsuco.toString(), "TR2DH", wsspcomn.language.toString());
	     if(isEQ(th506rec.fatcaFlag,"Y"))	{
			Clftpf clftValue=clftpfdao.getRecordByClntnum("CN", wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
			if(clftValue==null){
				
				 sv.fatcastatus.set(Fatcastatus.get("NUSWOI").getDescitem());
				 sv.fatcastatusdesc.set(Fatcastatus.get("NUSWOI").getLongdesc());
				
			}else{
			 sv.fatcastatus.set(clftValue.getFfsts().trim());
			 sv.fatcastatusdesc.set(Fatcastatus.get(clftValue.getFfsts().trim()).getLongdesc());
			 }
			}
			else{
				sv.fatcastatus.set(Fatcastatus.get("N/A").getDescitem());
				 sv.fatcastatusdesc.set(Fatcastatus.get("N/A").getLongdesc());
			}
			}
	if(onePflag) {				
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t3615.toString());
		itempf.setItemitem(chdrlnbIO.getSrcebus().toString());
		itempf.setValidflag("1");
		itempf = itemDAO.getItemRecordByItemkey(itempf);			
		if (itempf != null) {
			t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));				
		}		
		if(isEQ(t3615rec.onepcashless,"Y") && isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")){
			if (isNE(chdrlnbIO.getAvlisu(),"Y")) {				
				sv.chdrnumErr.set(errorsInner.rul2);
			}
			if (isNE(sv.errorIndicators,SPACES)) {
				wsspcomn.edterror.set("Y");
			}
		}		
	}

}

protected void getName1100()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		wsaaName.set(namadrsrec.name);
	}

protected void preScreenEdit()
	{
	if (onePflag && isEQ(t3615rec.onepcashless,"Y") && isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		wsspcomn.edterror.set(Varcom.oK);
		wsspcomn.sectionno.set("3000");
		return;
	}
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case undlIo2040: {
					undlIo2040();
					checkForErrors2080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);		
	}

protected void validate2020()
	{
		if (isNE(sv.huwdcdte,varcom.vrcmMaxDate)) {
			if (isEQ(sv.huwdcdte,0)) {
				sv.huwdcdteErr.set(e032);
			}
			else {
				if (isLT(sv.huwdcdte,sv.hprrcvdt)) {
					sv.huwdcdteErr.set(hl03);
				}
				if (isGT(sv.huwdcdte,wsaaToday)) {
					sv.huwdcdteErr.set(hl01);
				}
			}
		}
		if(onePflag && isEQ(t3615rec.onepcashless,"Y") && (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")||isEQ(wsaa1Pflag, "+"))) {			
			if (isNE(chdrlnbIO.getAvlisu(),"Y")) {				                         
				sv.chdrnumErr.set(errorsInner.rul2);
			}			
			if (isNE(sv.errorIndicators,SPACES)) {
				wsspcomn.edterror.set("Y");
			}
			if (isEQ(scrnparams.statuz, "CALC")) {
				wsspcomn.edterror.set("Y");
			}
		}
				
		/*undlIO.setParams(SPACES);
		undlIO.setChdrcoy(lifelnbIO.getChdrcoy());
		undlIO.setChdrnum(lifelnbIO.getChdrnum());
		undlIO.setLife(lifelnbIO.getLife());
		undlIO.setJlife("00");
		undlIO.setFormat(formatsInner.undlrec);
		undlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");*/
		if(isFatcaAllowed){
			if(isEQ(th506rec.fatcaFlag,"Y"))	{
				Clftpf clftValue=clftpfdao.getRecordByClntnum("CN", wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
				Fatcastatus= descdao.getItems("IT", wsspcomn.fsuco.toString(), "TR2DH", wsspcomn.language.toString());
				
				if(clftValue==null){
					
					 sv.fatcastatus.set(Fatcastatus.get("NUSWOI").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("NUSWOI").getLongdesc());
					
				}else{
				 sv.fatcastatus.set(clftValue.getFfsts());
				 //sv.fatcastatusdesc.set(Fatcastatus.get(clftValue.getFfsts()).getLongdesc());
				 
				 }
				}
				else{
					sv.fatcastatus.set(Fatcastatus.get("N/A").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("N/A").getLongdesc());
				}
			
			// Checking Follow Ups for Client
			
			
			itdmList = itemDAO.getIdtmByPfxAndCoyAndTabl(wsspcomn.company.toString(), tr5aw);
			wsaaTr5awIx.set(1);
			if(itdmList==null || itdmList.size() == 0){
				syserrrec.statuz.set("MRNF");
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(tr5aw));
				fatalError600();
			}else{
				for(int i=0; i < itdmList.size(); i++){
					itdmpf = itdmList.get(i);
					loadTr5aw1500();
				}
			}
			
			
			wsaaTr5awIx.set(1);
			fluppf = new Fluppf();
			
			flupList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
			for(Fluppf flup: flupList)
			{
				
				
				for (; isLT(wsaaTr5awIx, wsaaTr5awArrayInner.wsaaTr5awRec.length); wsaaTr5awIx.add(1)){

				    if(isEQ(wsaaTr5awArrayInner.wsaaFupCde[wsaaTr5awIx.toInt()],flup.getFupCde()))
				    { 
				    	if(isNE(flup.getFupSts(),'R'))
				    	{ sv.fatcastatusErr.set(h017); }
					
				    }
			     }
			
			}
			 
			
			
		}	
		checkAuthority2120();		
		//
	}

protected void undlIo2040()
	{
		/*SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(),varcom.oK)
		&& isNE(undlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(undlIO.getParams());
			syserrrec.statuz.set(undlIO.getStatuz());
			fatalError600();
		}
		if (isEQ(undlIO.getStatuz(),varcom.oK)
		&& isEQ(undlIO.getChdrcoy(),lifelnbIO.getChdrcoy())
		&& isEQ(undlIO.getChdrnum(),lifelnbIO.getChdrnum())
		&& isEQ(undlIO.getLife(),lifelnbIO.getLife())) {
			if (isNE(undlIO.getUndwflag(),"Y")) {
				sv.lifcnumErr.set("E964");
			}
			else {
				undlIO.setFunction(varcom.nextr);
				goTo(GotoLabel.undlIo2040);
			}
		}
		else {
			undlIO.setStatuz(varcom.endp);
		}*/
	Undlpf undlObj=new Undlpf();
	undlObj.setChdrcoy(lifepf.getChdrcoy().trim());/* IJTI-1523 */
	undlObj.setChdrnum(lifepf.getChdrnum().trim());/* IJTI-1523 */
	undlObj.setLife(lifepf.getLife().trim());/* IJTI-1523 */
	undlList=undldao.searchUndlpfRecords(undlObj);
	for (int intCounter = 0; intCounter < undlList.size(); intCounter++) {
		undlObj=undlList.get(intCounter);
		if (undlObj.getUndwflag()!=null && !undlObj.getUndwflag().equals("Y")) {
			sv.lifcnumErr.set("E964");
		}
	}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void checkAuthority2120()
	{
		uwlmtrec.function.set("CHCK");
		uwlmtrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		uwlmtrec.chdrnum.set(chdrlnbIO.getChdrnum());
		uwlmtrec.cnttyp.set(chdrlnbIO.getCnttype());
		uwlmtrec.cownpfx.set(chdrlnbIO.getCownpfx());
		uwlmtrec.lifcnum.set(sv.lifcnum);
		uwlmtrec.language.set(wsspcomn.language);
		uwlmtrec.fsuco.set(wsspcomn.fsuco);
		uwlmtrec.userid.set(wsspcomn.userid);
		callProgram(Uwlmtchk.class, uwlmtrec.rec);
		if (isNE(uwlmtrec.statuz,varcom.oK)) {
			if (isEQ(uwlmtrec.statuz,"SS")) {
				wsaaLevelss.set(uwlmtrec.uwlevel);
				wsaaLeveldss.set("Level:");
				wsaaErrss1.set(". Sub Stnd TRSA Amt ");
				wsaaTrsaSs.set(uwlmtrec.sumins);
				wsaaErrss2.set(" > Sub Stnd UW Lmt ");
				wsaaUwlmtSs.set(uwlmtrec.undwrlim);
				scrnparams.errorline.set(wsaaErrorlineSs);
			}
			else {
				wsaaLevels.set(uwlmtrec.uwlevel);
				wsaaLevelds.set("Level:");
				wsaaErrs1.set(". Stnd TRSA Amt ");
				wsaaTrsaS.set(uwlmtrec.sumins);
				wsaaErrs2.set(" > Stnd UW Lmt ");
				wsaaUwlmtS.set(uwlmtrec.undwrlim);
				scrnparams.errorline.set(wsaaErrorlineS);
			}
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
{
	if (onePflag && isEQ(t3615rec.onepcashless,"Y") && (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")||isEQ(wsaa1Pflag, "+"))) {
		return ;	
	}                                                                    //IBPLIFE-2274
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				updateChdrlnb3010();
				updateHpad3020();
				createPtrn3030();
				checkLext3030();
				processBext(); //NBS009 
			}
			case printLetter3030a: {
				printLetter3030a();
				rlseSoftlock3040();
			}
			case exit3090: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void updateChdrlnb3010()
	{
	
		readT56793100();
		
		if (isEQ(wsspcomn.flag,"R")) {
			reversal3200();
			goTo(GotoLabel.exit3090);
		}
	
		if(onePflag) {
			chdrpf = chdrpfDAO.updateChdrpfStatcodePstcde(wsspcomn.company.toString(), sv.chdrnum.toString(), 
					t5679rec.setCnRiskStat.toString(), t5679rec.setCnPremStat.toString());
		}
		else {
			chdrpf = chdrpfDAO.updateChdrlnbTranlusedStatcodeTranno(wsspcomn.company.toString(), 
					sv.chdrnum.toString(), t5679rec.setCnRiskStat.toString());
		}
		
		if (chdrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat(sv.chdrnum.toString()));
			fatalError600();
		}
	}

protected void updateHpad3020()
	{
	    hpadpfDAO.updateHuwdcdte(wsspcomn.company.toString(), sv.chdrnum.toString(), sv.huwdcdte.toInt());
	}

protected void createPtrn3030()
	{

		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());		
		ptrnpf.setTranno(chdrpf.getTranno());
		ptrnpf.setPtrneff(sv.huwdcdte.toInt());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setValidflag("1");
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setChdrpfx("  ");
		ptrnpf.setPrtflg(" ");
		ptrnpf.setCrtuser(" ");
		ptrnpf.setRecode(" ");
		
		ptrnpfList = new ArrayList<Ptrnpf>();
		ptrnpfList.add(ptrnpf);
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
	}

protected void checkLext3030()
	{
		//wsaaFoundLext = "Y";
		t5651List = itemDAO.getAllitems("IT", wsspcomn.company.toString(), "t5651");
	/*	lextIO.setDataKey(SPACES);
		lextIO.setChdrcoy(wsspcomn.company);
		lextIO.setChdrnum(sv.chdrnum);
		lextIO.setLife(SPACES);
		lextIO.setCoverage(SPACES);
		lextIO.setRider(SPACES);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRNUM");
		lextIO.setStatuz(varcom.oK);*/
		
		/*
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, lextIO);
			if (isNE(lextIO.getStatuz(),varcom.oK)
			&& isNE(lextIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(lextIO.getStatuz());
				syserrrec.params.set(lextIO.getParams());
				fatalError600();
			}
			if (isNE(lextIO.getChdrnum(),sv.chdrnum)
			|| isNE(lextIO.getStatuz(),varcom.oK)) {
				lextIO.setStatuz(varcom.endp);
				wsaaFoundLext = "N";
			}*/
		lextpfObj =new Lextpf();
		lextpfObj.setChdrcoy(wsspcomn.company.toString().trim());
		lextpfObj.setChdrnum(sv.chdrnum.toString().trim());
		lextList = lextpfDAO.searchLextRecord(lextpfObj);
		if(lextList.isEmpty()){
			wsaaFoundLext = "N";
		}
		else {
			for (int lextCounter = 0; lextCounter < lextList.size() ; lextCounter++) {
				wsaaFoundLext = "N";//APA Change
				lextpfObj=lextList.get(lextCounter);
				Iterator<Itempf> itemIterator = t5651List.iterator();
				while(itemIterator.hasNext()){
						Itempf itempf=itemIterator.next();
						if(itempf.getItemitem().trim().equals(lextpfObj.getOpcda().trim())){/* IJTI-1523 */
							t5651rec.t5651Rec.set(StringUtil.rawToString(itempf.getGenarea()));
							break;
						}
				}
				if (isEQ(t5651rec.ccode,"Y")) {
					break;
				}
			}

				/*itemIO.setDataKey(SPACES);
				itemIO.setItempfx("IT");
				itemIO.setItemcoy(wsspcomn.company);
				itemIO.setItemtabl(t5651);
				itemIO.setItemitem(lextIO.getOpcda());
				itemIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
					syserrrec.statuz.set(itemIO.getStatuz());
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
				}
				t5651rec.t5651Rec.set(itemIO.getGenarea());*/

				
				/*if (isEQ(t5651rec.ccode,"Y")) {
					///lextIO.setStatuz(varcom.endp);

				}*/
			}
			//lextIO.setFunction(varcom.nextr);

		/*}*/

		if (isEQ(wsaaFoundLext,"Y")) {
			goTo(GotoLabel.printLetter3030a);
		}
	}

/**
 * @author fwang3
 * Process Bext billing, produce or remove.
 */
private void processBext() {
	nbs009Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), NBS009_FEATRUE_ID, appVars, "IT");
	if (nbs009Permission && isEQ("D", chdrlnbIO.getBillchnl().trim())) {
		if (isEQ(wsspcomn.flag, "R")) {
			removeOrUpdateBext();
			removeDishonour();
		} else if (isEQ(wsspcomn.flag, "M")) {
			initArray();
			loadTables();
			loadPayerDetails1035();
			calcPremium1090();
			checkSuspense();
			produceBextRecord();
		}
	}
}

private void initArray() {
	wsbbSub.set(1);
	while ( !(isGT(wsbbSub, 9))) {
		wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(ZERO);
		wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].set(ZERO);
		wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].set(ZERO);
		wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].set(ZERO);
		wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].set(ZERO);
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].set(ZERO);
		wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()].set(ZERO);
		wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(SPACES);
		wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(SPACES);
		wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(SPACES);
		wsbbSub.add(1);
	}
}

private void removeOrUpdateBext() {
	bextpfDAO.deleteBextpfRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
	bextpfDAO.updateBextpfRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
}

private void removeDishonour() {
	dshnpfDAO.removeRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
}

protected void loadPayerDetails1035()
{
	List<Payrpf> payrpfList = null;

	/* Load the WS table by reading all the payer                      */
	/* and payer role recored for this contract.                       */

	payrpfList = payrpfFSUDAO.searchPayrRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

	for(Payrpf payrpf : payrpfList){

		if(!(isNE(payrpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
				|| isNE(payrpf.getChdrnum(), chdrlnbIO.getChdrnum()))){

			loadPayerDetails1b00(payrpf);
		}else{
			break;
		}
	}
}

protected void produceBextRecord(){
	/* Do not produce bext if there is enough money in the suspense*/
	/* account to cover the instalment amount.*/
	if (isGTE(wsaaTotalSuspense, wsaaTotlprm)) {
		return;
	}
	callBillreq();
}

protected void callBillreq() {
	Payrpf payrpf = payrpfFSUDAO.getpayrRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
	Mandpf mandpf = mandpfDAO.searchMandpfRecordData(clrfIO.getClntcoy().toString(), clrfIO.getClntnum().toString(), payrpf.getMandref(), "MANDPF");
	billreqrec.mandstat.set(mandpf.getMandstat());
	if (isGT(sv.huwdcdte, chdrlnbIO.getOccdate())) {
		billreqrec.billdate.set(sv.huwdcdte);
	}
	else {
		billreqrec.billdate.set(chdrlnbIO.getOccdate());
	} 
	/* Search the T3629 array to obtain the required bank code.*/
	readT3629();
	billreqrec.bankcode.set(t3629rec.bankcode);
	billreqrec.user.set(0);
	billreqrec.contot01.set(0);
	billreqrec.contot02.set(0);
	billreqrec.instjctl.set(SPACES);
	billreqrec.payflag.set(SPACES);
	billreqrec.bilflag.set(SPACES);
	billreqrec.outflag.set(SPACES);
	billreqrec.supflag.set("N");
	billreqrec.company.set(wsspcomn.company);
	billreqrec.branch.set(wsspcomn.branch);
	billreqrec.language.set(wsspcomn.language);
	billreqrec.effdate.set(sv.huwdcdte);
	billreqrec.acctyear.set(wsaaBatckey.batcBatcactyr);
	billreqrec.acctmonth.set(wsaaBatckey.batcBatcactmn);
	billreqrec.trancode.set(wsaaBatckey.batcBatctrcde);
	billreqrec.batch.set(wsaaBatckey.batcBatcbatch);
	billreqrec.fsuco.set(wsspcomn.fsuco);
	billreqrec.modeInd.set("ONLIN");
	billreqrec.termid.set(SPACES);
	billreqrec.time.set(ZERO);
	billreqrec.date_var.set(ZERO);
	billreqrec.tranno.set(chdrpf.getTranno());
	billreqrec.chdrpfx.set(chdrlnbIO.getChdrpfx());
	billreqrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
	billreqrec.chdrnum.set(chdrlnbIO.getChdrnum());
	billreqrec.servunit.set(chdrlnbIO.getServunit());
	billreqrec.cnttype.set(chdrlnbIO.getCnttype());
	billreqrec.occdate.set(chdrlnbIO.getOccdate());
	billreqrec.ccdate.set(chdrlnbIO.getCcdate());
	billreqrec.instcchnl.set(chdrlnbIO.instcchnl);
	billreqrec.cownpfx.set(chdrlnbIO.getCownpfx());
	billreqrec.cowncoy.set(chdrlnbIO.getCowncoy());
	billreqrec.cownnum.set(chdrlnbIO.getCownnum());
	billreqrec.cntbranch.set(chdrlnbIO.getCntbranch());
	billreqrec.agntpfx.set(chdrlnbIO.getAgntpfx());
	billreqrec.agntcoy.set(chdrlnbIO.getAgntcoy());
	billreqrec.agntnum.set(chdrlnbIO.getAgntnum());
	billreqrec.cntcurr.set(chdrlnbIO.getCntcurr());
	billreqrec.billcurr.set(chdrlnbIO.getBillcurr());
	billreqrec.ptdate.set(chdrlnbIO.getPtdate());
	billreqrec.instto.set(chdrlnbIO.getOccdate());
	billreqrec.instbchnl.set(chdrlnbIO.getBillchnl());
	billreqrec.billchnl.set(chdrlnbIO.getBillchnl());
	billreqrec.instfreq.set(chdrlnbIO.getBillfreq());
	billreqrec.grpscoy.set(payrpf.getGrupcoy());
	billreqrec.grpsnum.set(payrpf.getGrupnum());
	billreqrec.membsel.set(payrpf.getMembsel());
	billreqrec.mandref.set(payrpf.getMandref());
	billreqrec.nextdate.set(payrpf.getNextdate());
	billreqrec.payrpfx.set(chdrlnbIO.getCownpfx());
	billreqrec.payrcoy.set(chdrlnbIO.getCowncoy());
	billreqrec.payrnum.set(chdrlnbIO.getCownnum());
	
	clblIO.setParams(SPACES);
	clblIO.setBankkey(mandpf.getBankkey());
	clblIO.setBankacckey(mandpf.getBankacckey());
	clblIO.setClntcoy(wsspcomn.fsuco);
	clblIO.setClntnum(chdrlnbIO.getCownnum());
	clblIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, clblIO);
	if (isNE(clblIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(clblIO.getParams());
		fatalError600();
	}
	
	billreqrec.facthous.set(clblIO.getFacthous());
	billreqrec.bankkey.set(clblIO.getBankkey());
	billreqrec.bankacckey.set(clblIO.getBankacckey());
	//ICIL-1382 Starts
	billreqrec.instfrom.set(chdrlnbIO.getOccdate());
	billreqrec.btdate.set(chdrlnbIO.getOccdate());
	billreqrec.duedate.set(chdrlnbIO.getOccdate());
	billreqrec.billcd.set(chdrlnbIO.getOccdate());
	//ICIL-1382 End
	billreqrec.nextdate.set(payrpf.getNextdate());
	billreqrec.sacscode01.set(t5645rec.sacscode01);
	billreqrec.sacstype01.set(t5645rec.sacstype01);
	billreqrec.glmap01.set(t5645rec.glmap01);
	billreqrec.glsign01.set(t5645rec.sign01);
	billreqrec.sacscode02.set(t5645rec.sacscode02);
	billreqrec.sacstype02.set(t5645rec.sacstype02);
	billreqrec.glmap02.set(t5645rec.glmap02);
	billreqrec.glsign02.set(t5645rec.sign02);
	/* If BILLREQ1 needs to know the PAYER NAME then it will be looked*/
	/* up using the PAYRPFX*/
	billreqrec.payername.set(SPACES);
	
	compute(billreqrec.instamt01, 2).set((sub(wsaaTotlprm, wsaaTotalSuspense)));
	billreqrec.instamt02.set(ZERO);
	billreqrec.instamt03.set(ZERO);
	billreqrec.instamt04.set(ZERO);
	billreqrec.instamt05.set(ZERO);
	compute(billreqrec.instamt06, 2).set((sub(wsaaTotlprm, wsaaTotalSuspense)));
	billreqrec.instamt07.set(ZERO);
	billreqrec.instamt08.set(ZERO);
	billreqrec.instamt09.set(ZERO);
	billreqrec.instamt10.set(ZERO);
	billreqrec.instamt11.set(ZERO);
	billreqrec.instamt12.set(ZERO);
	billreqrec.instamt13.set(ZERO);
	billreqrec.instamt14.set(ZERO);
	billreqrec.instamt15.set(ZERO);
	
	callProgram(Billreq1.class, billreqrec.billreqRec);
	if (isNE(billreqrec.statuz, varcom.oK)) {
		syserrrec.params.set(billreqrec.billreqRec);
		syserrrec.statuz.set(billreqrec.statuz);
		fatalError600();
	}
}

protected void readT3629(){
	Map<String, List<Itempf>> t3629ListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T3629");
	String keyItemitem = chdrpf.getBillcurr().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3629ListMap.containsKey(keyItemitem)){	
		itempfList = t3629ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		String strEffDate = wsaaToday.toString();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}else{
				t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}	
	if (!itemFound) {
		StringBuffer stringVariable1 = new StringBuffer();
		stringVariable1.append("T3629");
		stringVariable1.append(chdrpf.getBillcurr());
		syserrrec.params.set(stringVariable1.toString());
		fatalError600();		
	}
}

protected void printLetter3030a()
	{
		if (isEQ(wsaaFoundLext,"N")) {
			acptLetr3300();
		}
		else {
			condLetr3400();
		}
	}

protected void rlseSoftlock3040()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrpf.getChdrcoy());
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set(chdrpf.getChdrpfx());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void readT56793100()
	{	
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void reversal3200()
	{
		processChdrlnb3210();
		updateHpad3220();
		processPtrn3230();
		processBext(); // NBS009
		rlseSoftlock3240();
	}

protected void processChdrlnb3210()
	{
		if(onePflag) {
			chdrpf = chdrpfDAO.updateChdrpfStatcodePstcde(wsspcomn.company.toString(), sv.chdrnum.toString(), 
					t5679rec.setCnRiskStat.toString(), t5679rec.setCnPremStat.toString());
		}
		else {
			chdrpf = chdrpfDAO.updateChdrlnbTranlusedStatcodeTranno(wsspcomn.company.toString(),
					sv.chdrnum.toString(), t5679rec.setCnRiskStat.toString());
		}
		if (chdrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat(sv.chdrnum.toString()));
			fatalError600();
		}
		readT56793100();		
		wsaaOldTranno.set(chdrpf.getTranno()-1);
	}

protected void updateHpad3220()
	{
	   hpadpfDAO.updateHuwdcdte(wsspcomn.company.toString(), sv.chdrnum.toString(), varcom.vrcmMaxDate.toInt());

		fluprevIO.setDataKey(SPACES);
		fluprevIO.setChdrcoy(chdrpf.getChdrcoy());
		fluprevIO.setChdrnum(chdrpf.getChdrnum());
		fluprevIO.setTranno(wsaaOldTranno);
		fluprevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(),varcom.oK)
		&& isNE(fluprevIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(fluprevIO.getStatuz());
			syserrrec.params.set(fluprevIO.getParams());
			fatalError600();
		}
		if (isEQ(fluprevIO.getStatuz(),varcom.oK)) {
			fluprevIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, fluprevIO);
			if (isNE(fluprevIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(fluprevIO.getStatuz());
				syserrrec.params.set(fluprevIO.getParams());
				fatalError600();
			}
		}
	}

protected void processPtrn3230()
	{
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		ptrnpf.setTranno(chdrpf.getTranno());
		ptrnpf.setPtrneff(sv.huwdcdte.toInt());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setValidflag("1");
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setChdrpfx("  ");
		ptrnpf.setPrtflg(" ");
		ptrnpf.setCrtuser(" ");
		ptrnpf.setRecode(" ");
		
		ptrnpfList = new ArrayList<Ptrnpf>();
		ptrnpfList.add(ptrnpf);
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
	}

protected void rlseSoftlock3240()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrpf.getChdrcoy());
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set(chdrpf.getChdrpfx());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}



protected void loadTr5aw1500()
{
	/* If the number of items stored exceeds the working storage*/
	/* array size, this is a fatal error.*/
	if (isGT(wsaaTr5awIx,wsaaTr5awSize)) {
		fatalError600();
	}
	/* Move the necessary data from the table to the working*/
	/* storage array.*/
	tr5awrec.tr5awRec.set(StringUtil.rawToString(itdmpf.getGenarea()));
	wsaaTr5awArrayInner.wsaacngfrmtaxpyr[wsaaTr5awIx.toInt()].set(tr5awrec.cngfrmtaxpyr);
	wsaaTr5awArrayInner.wsaacngfrmbrthctry[wsaaTr5awIx.toInt()].set(tr5awrec.cngfrmbrthctry);
	wsaaTr5awArrayInner.wsaacngfrmnatlty[wsaaTr5awIx.toInt()].set(tr5awrec.cngfrmnatlty);
	wsaaTr5awArrayInner.wsaacngtotaxpyr[wsaaTr5awIx.toInt()].set(tr5awrec.cngtotaxpyr);
	wsaaTr5awArrayInner.wsaacngtobrthctry[wsaaTr5awIx.toInt()].set(tr5awrec.cngtobrthctry);
	wsaaTr5awArrayInner.wsaacngtonatlty[wsaaTr5awIx.toInt()].set(tr5awrec.cngtonatlty);
	wsaaTr5awArrayInner.wsaaFupCde[wsaaTr5awIx.toInt()].set(itdmpf.getItemitem());
	wsaaTr5awIx.add(1);
}		


protected void acptLetr3300()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaCnttype, SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaCnttype.set("***");
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(wsaaCnttype, SPACES);
			stringVariable2.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
			stringVariable2.setStringInto(itemIO.getItemitem());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
	}

protected void condLetr3400()
	{
		condLetr3410();
	}

protected void condLetr3410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th609);
		itemIO.setItemitem(chdrpf.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setItemitem("***");
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		th609rec.th609Rec.set(itemIO.getGenarea());
		wsaaItem.set(th609rec.fupcdes);
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5661);
		descIO.setLanguage(wsspcomn.language);
		/*    MOVE WSAA-ITEM              TO DESC-DESCITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(wsaaItem);
		descIO.setDescitem(wsaaT5661Key);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("??????????????????????????????");
		}
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(th609rec.fupcdes);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		fluplnbIO.setDataKey(SPACES);
		fluplnbIO.setChdrcoy(chdrpf.getChdrcoy());
		fluplnbIO.setChdrnum(chdrpf.getChdrnum());
		fluplnbIO.setFupno(99);
		fluplnbIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)
		&& isNE(fluplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		if (isEQ(fluplnbIO.getStatuz(),varcom.endp)
		|| isNE(fluplnbIO.getChdrnum(),chdrpf.getChdrnum())) {
			fluplnbIO.setLife("01");
			fluplnbIO.setFupno(1);
			fluplnbIO.setChdrcoy(chdrpf.getChdrcoy());
			fluplnbIO.setChdrnum(chdrpf.getChdrnum());
		}
		else {
			setPrecision(fluplnbIO.getFupno(), 0);
			fluplnbIO.setFupno(add(fluplnbIO.getFupno(),1));
		}
		fluplnbIO.setFuptype("P");
		fluplnbIO.setTranno(chdrpf.getTranno());
		fluplnbIO.setFupstat(t5661rec.fupstat);
		fluplnbIO.setFupremdt(wsaaToday);
		fluplnbIO.setFupcode(th609rec.fupcdes);
		fluplnbIO.setFupremk(descIO.getLongdesc());
		fluplnbIO.setTermid(varcom.vrcmTermid);
		fluplnbIO.setUser(varcom.vrcmUser);
		fluplnbIO.setTransactionDate(varcom.vrcmDate);
		fluplnbIO.setTransactionTime(varcom.vrcmTime);
		fluplnbIO.setEffdate(datcon1rec.intDate);
		fluplnbIO.setCrtdate(datcon1rec.intDate);
		fluplnbIO.setFuprcvd(varcom.vrcmMaxDate);
		fluplnbIO.setExprdate(varcom.vrcmMaxDate);
		fluplnbIO.setZlstupdt(varcom.vrcmMaxDate);
		fluplnbIO.setFormat(formatsInner.fluplnbrec);
		fluplnbIO.setStatuz(SPACES);
		fluplnbIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
	}

/**
 * processing total prem fwang3
 * 
 * 
 */
protected void calcPremium1090()
{
	List<Covtpf> covtpfList = null;
	covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
	for(Covtpf covtpf : covtpfList){
		calcPremium1300(covtpf);
	}
	if(isbillday){
		if( (isEQ(chdrlnbIO.getBillchnl(),'D') || isEQ(chdrlnbIO.getBillchnl(),'R') || isEQ(chdrlnbIO.getBillchnl(),'A')
				|| isEQ(chdrlnbIO.getBillchnl(),'B')) && isEQ(t6654rec.renwdate1,"Y"))//ILIFE-7745,8583
			return;
	}
	adjustRegularPrem1100();
}

protected void adjustRegularPrem1100()
{
	/* Read the RTRN file to see if a cash receipt has been*/
	/* created for this contract.*/
	rtrnsacIO.setRldgcoy(wsspcomn.company);
	rtrnsacIO.setRldgacct(chdrlnbIO.getChdrnum());
	/* MOVE CHDRLNB-CNTCURR        TO RTRNSAC-ORIGCCY.         <014>*/
	rtrnsacIO.setOrigccy(acblenqIO.getOrigcurr());
	rtrnsacIO.setSacscode(t5645rec.sacscode01);
	rtrnsacIO.setSacstyp(t5645rec.sacstype01);
	rtrnsacIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, rtrnsacIO);
	if (isNE(rtrnsacIO.getStatuz(), Varcom.oK)
			&& isNE(rtrnsacIO.getStatuz(), Varcom.mrnf)) {
		syserrrec.params.set(rtrnsacIO.getParams());
		fatalError600();
	}
	if (isEQ(rtrnsacIO.getStatuz(), Varcom.mrnf)) {
		rtrnsacIO.setEffdate(varcom.vrcmMaxDate);
	}
	/* Look up the tax relief method on T5688.                         */
	itdmpf = new Itempf();
	itdmpfList = itemDAO.getItdmByFrmdate(chdrlnbIO.getChdrcoy().toString(), tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), chdrlnbIO.getOccdate().toInt());

	if(itdmpfList != null && itdmpfList.size() !=0 ){
		itdmpf = itdmpfList.get(0);
	}

	t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
	/* Look up the subroutine on T6687.                                */
	if (isNE(t5688rec.feemeth, SPACES)) {
		calcFee1200();
	}

	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tablesInner.t6687.toString());
	itempf.setItemitem(t5688rec.taxrelmth.toString());
	itempf = itemDAO.getItempfRecord(itempf);

	if (null != itempf) {
		t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else {
		t6687rec.t6687Rec.set(SPACES);
	}
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	/* Adjust the regular premiums (if not zero) by the number         */
	/* of frequencies required prior to issue.                         */
	/* If the contract has a tax relief method deduct the tax          */
	/* relief amount from the amount due.                              */
	/* Also check if there is enough money in suspense to issue        */
	/* this contract.                                                  */
	wsbbSub.set(1);
	while ( !(isGT(wsbbSub, 9)
			|| isEQ(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()], SPACES))) {
		adjustPremium1c00();
	}
}

protected void calcFee1200(){
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(T5674);
	itempf.setItemitem(t5688rec.feemeth.toString());
	itempf = itemDAO.getItempfRecord(itempf);

	if (null == itempf) {
		syserrrec.params.set("T5674 : "+t5688rec.feemeth);
		fatalError600();
	}
	t5674rec.t5674Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	if (isEQ(t5674rec.commsubr, SPACES)) {
		return ;
	}
	int index = 1;
	mgfeelrec.mgfeelRec.set(SPACES);
	mgfeelrec.effdate.set(ZERO);
	mgfeelrec.mgfee.set(ZERO);
	mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
	/* MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.                */
	mgfeelrec.billfreq.set(wsaaBillingInformationInner.wsaaBillfreq[index]);
	mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
	mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
	mgfeelrec.company.set(wsspcomn.company);
	callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
	if (isNE(mgfeelrec.statuz, Varcom.oK)
			&& isNE(mgfeelrec.statuz, Varcom.endp)) {
		syserrrec.params.set(mgfeelrec.mgfeelRec);
		fatalError600();
	}
	zrdecplrec.amountIn.set(mgfeelrec.mgfee);
	zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
	callRounding8000();
	mgfeelrec.mgfee.set(zrdecplrec.amountOut);
	wsaaCntfee.set(mgfeelrec.mgfee);
	cntfee.set(mgfeelrec.mgfee);
	if (isGT(wsaaCntfee, ZERO)) {
		checkCalcContTax7100();
		wsaaCntfeeTax.set(wsaaTax);
	}
}

protected void calcPremium1300(Covtpf covtpf)
{
	readseqCovtlnb1310(covtpf);
}

protected void readseqCovtlnb1310(Covtpf covtpf)
{
	/*    Read each record and accumulate all single and regular*/
	/*    premiums payable.*/
	if (isNE(chdrlnbIO.getChdrcoy(), covtpf.getChdrcoy())
			|| isNE(chdrlnbIO.getChdrnum(), covtpf.getChdrnum())) {
		return;
	}
	/*    Single or Regular Premium?*/
	/*    Single and Regular Premiums are now held in separate fields  */
	/*    on COVT, so just move corresponding fields. If the COVT Inst */
	/*    amount is zero, then don't bother to apply the frequency     */
	/*    factor etc. Both fields may be non-zero!                     */
	wsbbSub.set(covtpf.getPayrseqno());
	/* ADD COVTLNB-SINGP           TO WSAA-SINGP.                   */
	wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].add(PackedDecimalData.parseObject(covtpf.getSingp()));
	if (isNE(covtpf.getSingp(), 0)) {
		wsaaSingPrmInd = "Y";
	}
	if (isEQ(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], "00")) {
		wsaaModalPremium.add(PackedDecimalData.parseObject(covtpf.getSingp()));
	}
	if (isEQ(covtpf.getInstprem(), 0)) {
		continue1380(covtpf);
		return;
	}
	/* Get the frequency Factor from DATCON3 for Regular premiums.*/
	/* If the Freq has been changed to SP and screens have not been    */
	/* revisited COVT will still show it has an INSTPREM when in fact  */
	/* this is not true. A IVFQ status will be returned from a later   */
	/* call to DATCON3 without the following code                      */
	/* IF CHDRLNB-BILLFREQ         = '00'                      <014>*/
	/*    GO TO 1380-CONTINUE.                                 <014>*/
	/* ADD COVTLNB-INSTPREM     TO WSAA-REGPREM.                    */
	if (isEQ(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], "00")) {
		continue1380(covtpf);
		return;
	}
	wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].add(PackedDecimalData.parseObject(covtpf.getInstprem()));
	wsaaModalPremium.add(PackedDecimalData.parseObject(covtpf.getInstprem()));
	continue1380(covtpf);
}

protected void continue1380(Covtpf covtpf)
{
	checkCalcCompTax7000(covtpf);
}

protected void loadPayerDetails1b00(Payrpf payrpf)
{
	loadPayer1b10(payrpf);
}

protected void loadPayer1b10(Payrpf payrpf)
{
	/* Read the client role file to get the payer number.              */
	clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
	clrfIO.setForecoy(payrpf.getChdrcoy());
	wsaaChdrnum.set(payrpf.getChdrnum());
	wsaaPayrseqno.set(payrpf.getPayrseqno());
	clrfIO.setForenum(wsaaPayrkey);
	clrfIO.setClrrrole("PY");
	clrfIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, clrfIO);
	if (isNE(clrfIO.getStatuz(), Varcom.oK)) {
		syserrrec.statuz.set(clrfIO.getStatuz());
		syserrrec.params.set(clrfIO.getParams());
		fatalError600();
	}
	/* Load the working storage table using the payer sequence         */
	/* number as the subscript.                                        */
	wsbbSub.set(payrpf.getPayrseqno());
	wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(payrpf.getIncomeSeqNo());
	wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(payrpf.getBillfreq());
	/* MOVE PAYR-GRUPKEY       TO WSAA-GRUPKEY(WSBB-SUB).      <014>*/
	wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(clrfIO.getClntnum());
	wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(clrfIO.getClntcoy());
}

protected void adjustPremium1c00()
{
	adjustPremiumCustomerSpecific();
	wsbbSub.add(1);
}

protected void adjustPremiumCustomerSpecific()
{
	adjustPremium1c10();
}
protected void adjustPremium1c10()
{
	/*    Get the frequency Factor from DATCON3.                       */
	if (isNE(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], ZERO)) {
		datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon3rec.intDate2.set(chdrlnbIO.getBillcd());
		datcon3rec.frequency.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*    If the risk commencment is 28, 29, 30, of january,           */
		/*    30 march, 30 may, 30 august or the 30 of october             */
		/*    the initial instalment required is incorrectly               */
		/*    calculated as 1 month + 1 day * premium instead of           */
		/*    1 month(frequency 12)                                        */
		/*    MOVE CHDRLNB-OCCDATE            TO WSAA-PREM-DATE1      <022>*/
		/*    MOVE CHDRLNB-BILLCD             TO WSAA-PREM-DATE2      <022>*/
		/*    PERFORM 1900-CHECK-FREQ-DATE                            <022>*/
		/* Calculate the instalment premium.                               */
		wsaaFactor.set(datcon3rec.freqFactor);
		compute(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], 5).set(mult(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], wsaaFactor));
	}
	/* Add in the single premium.                                      */
	wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()]);
	/* If the tax relief method is not spaces calculate the tax        */
	/* relief amount and deduct it from the premium.                   */
	if (isNE(t5688rec.taxrelmth, SPACES) && isNE(t6687rec.taxrelsub, SPACES)) {
		prasrec.clntnum.set(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()]);
		prasrec.clntcoy.set(wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()]);
		prasrec.incomeSeqNo.set(wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()]);
		prasrec.cnttype.set(chdrlnbIO.getCnttype());
		prasrec.taxrelmth.set(t5688rec.taxrelmth);
		/* Use the due date unless a receipt exists with a date later      */
		/* then the due date.                                              */
		if (isEQ(rtrnsacIO.getEffdate(), varcom.vrcmMaxDate)) {
			prasrec.effdate.set(chdrlnbIO.getOccdate());
		}
		else {
			if (isGT(chdrlnbIO.getOccdate(), rtrnsacIO.getEffdate())) {
				prasrec.effdate.set(chdrlnbIO.getOccdate());
			}
			else {
				prasrec.effdate.set(rtrnsacIO.getEffdate());
			}
		}
		prasrec.company.set(chdrlnbIO.getChdrcoy());
		prasrec.grossprem.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
		prasrec.statuz.set(Varcom.oK);
		callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
		if (isNE(prasrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(prasrec.statuz);
			syserrrec.subrname.set(t6687rec.taxrelsub);
			fatalError600();
		}
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].subtract(prasrec.taxrelamt);
	}
	wsaaTotalPremium.add(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
	/* Add the contract fee to the instalment premium for              */
	/* payer No. 1.                                                    */
	/* IF SINGLE PREMIUM IS APPLIED CHECK IF ANY FEE IS NEEDED         */
	singlePremiumFeeCustomerSpecific();
	compute(wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()], 6).setRounded(add((mult(wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()], wsaaFactor)), wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()]));
	compute(wsaaTotalTax, 3).setRounded(add(wsaaTotalTax, wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()]));
	if (isEQ(wsbbSub, 1)) {
		if (isEQ(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], ZERO)) {
			cntfee.set(ZERO);
		}
		else {
			compute(cntfee, 5).set(mult(cntfee, wsaaFactor));
		}
		cntfee.add(wsaaSingpFee);
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(cntfee);
		if (isNE(wsaaSingpFee, ZERO)) {
			wsaaCntfee.set(wsaaSingpFee);
			checkCalcContTax7100();
			wsaaSingpfeeTax.set(wsaaTax);
		}
		compute(wsaaCntfeeTax, 6).setRounded(mult(wsaaCntfeeTax, wsaaFactor));
		compute(wsaaTotalTax, 3).setRounded(add(add(wsaaTotalTax, wsaaSingpfeeTax), wsaaCntfeeTax));
	}
	zrdecplrec.amountIn.set(wsaaTotalTax);
	zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
	callRounding8000();
	wsaaTotalTax.set(zrdecplrec.amountOut);
	wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(wsaaTotalTax);
	compute(wsaaTotlprm, 2).set(add(wsaaTotalPremium,cntfee));
	compute(wsaaTotlprm, 2).add(wsaaTotalTax);
}

protected void singlePremiumFeeCustomerSpecific() {
	if (isEQ(wsaaSingPrmInd, "Y") && isNE(t5688rec.feemeth, SPACES)) {
		singPremFee1e00();
	}
}

protected void checkSuspense1c100()
{
	/*    Read the Suspense file to see if any money has been          */
	/*    received for this contract. The search order for Suspense    */
	/*    details is Contract Currency; Billing Currency; Any          */
	/*    Currency. If Suspense is found set appropriate values.       */
	/*    But only if the Suspense amount is not zero.                 */
	if (isEQ(wsaaSub, 1)) {
		acblenqIO.setOrigcurr(chdrlnbIO.getCntcurr());
		acblenqIO.setFunction(Varcom.readr);
	}
	else {
		if (isEQ(wsaaSub, 2)) {
			acblenqIO.setOrigcurr(chdrlnbIO.getBillcurr());
			acblenqIO.setFunction(Varcom.readr);
		}
		else {
			acblenqIO.setOrigcurr(SPACES);
			acblenqIO.setFunction(varcom.begn);
		}
	}
	SmartFileCode.execute(appVars, acblenqIO);
	if ((isNE(acblenqIO.getStatuz(), Varcom.oK))
			&& (isNE(acblenqIO.getStatuz(), Varcom.mrnf))
			&& (isNE(acblenqIO.getStatuz(), Varcom.endp))) {
		syserrrec.params.set(acblenqIO.getParams());
		syserrrec.statuz.set(acblenqIO.getStatuz());
		fatalError600();
	}
	if ((isEQ(acblenqIO.getStatuz(), Varcom.oK))
			&& (isEQ(acblenqIO.getRldgcoy(), wsspcomn.company))
			&& (isEQ(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum()))
			&& (isEQ(acblenqIO.getSacscode(), t5645rec.sacscode01))
			&& (isEQ(acblenqIO.getSacstyp(), t5645rec.sacstype01))
			&& (isNE(acblenqIO.getSacscurbal(), ZERO))) {
		wsaaSuspInd = "Y";
		if (isEQ(t3695rec.sign, "-")) {
			compute(wsaaTotalSuspense, 2).set(mult(acblenqIO.getSacscurbal(), -1));
		}
		else {
			wsaaTotalSuspense.set(acblenqIO.getSacscurbal());
		}
		compute(wsaaTotalSuspense, 2).set(mult(wsaaTotalSuspense, -1));
	}
}

protected void singPremFee1e00()
{
	singPremFeePara1e00();
}

protected void singPremFeePara1e00()
{
	if (isEQ(t5674rec.commsubr, SPACES)) {
		return ;
	}
	mgfeelrec.mgfeelRec.set(SPACES);
	mgfeelrec.effdate.set(ZERO);
	mgfeelrec.mgfee.set(ZERO);
	mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
	mgfeelrec.billfreq.set("00");
	mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
	mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
	mgfeelrec.company.set(wsspcomn.company);
	callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
	if (isNE(mgfeelrec.statuz, Varcom.oK)
			&& isNE(mgfeelrec.statuz, Varcom.endp)) {
		syserrrec.params.set(mgfeelrec.mgfeelRec);
		fatalError600();
	}
	wsaaSingpFee.set(mgfeelrec.mgfee);
}

protected void checkCalcCompTax7000(Covtpf covtpf)
{
	start7010(covtpf);
}

protected void start7010(Covtpf covtpf)
{
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	if (covtpf.getSingp().compareTo(BigDecimal.ZERO)==0
			&& covtpf.getInstprem().compareTo(BigDecimal.ZERO)==0) {
		return ;
	}
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
	wsaaTr52eCrtable.set(covtpf.getCrtable());
	readTr52e7200();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e7200();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		readTr52e7200();
	}
	/* Call TR52D tax subroutine                                       */
	if (isNE(tr52erec.taxind01, "Y")) {
		return ;
	}
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
	txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
	txcalcrec.life.set(covtpf.getLife());
	txcalcrec.coverage.set(covtpf.getCoverage());
	txcalcrec.rider.set(covtpf.getRider());
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.crtable.set(covtpf.getCrtable());
	txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
	txcalcrec.register.set(chdrlnbIO.getRegister());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
	wsaaCntCurr.set(chdrlnbIO.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.effdate.set(chdrlnbIO.getOccdate());
	txcalcrec.transType.set("PREM");
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	if (isNE(covtpf.getInstprem(), ZERO)) {
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(covtpf.getZbinstprem());
		}
		else {
			txcalcrec.amountIn.set(covtpf.getInstprem());
		}
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
			}
		}
	}
	if (covtpf.getSingp().compareTo(BigDecimal.ZERO)!=0) {
		txcalcrec.amountIn.set(covtpf.getSingp());
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
			}
		}
	}
}

protected void checkCalcContTax7100()
{
	wsaaTax.set(0);
	if (isEQ(mgfeelrec.mgfee, ZERO)) {
		return ;
	}
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
	wsaaTr52eCrtable.set("****");
	readTr52e7200();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		readTr52e7200();
	}
	if (isNE(tr52erec.taxind02, "Y")) {
		return ;
	}
	/* Call TR52D tax subroutine                                       */
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
	txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
	txcalcrec.life.set(SPACES);
	txcalcrec.coverage.set(SPACES);
	txcalcrec.rider.set(SPACES);
	txcalcrec.crtable.set(SPACES);
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
	txcalcrec.register.set(chdrlnbIO.getRegister());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
	wsaaCntCurr.set(chdrlnbIO.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.amountIn.set(wsaaCntfee);
	txcalcrec.effdate.set(chdrlnbIO.getOccdate());
	txcalcrec.transType.set("CNTF");
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, Varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[2]);
		}
	}
}

protected void readTr52e7200()
{
	tr52erec.tr52eRec.set(SPACES);
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItmfrm(new BigDecimal(chdrlnbIO.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(chdrlnbIO.getOccdate().toString()));
	itempf.setItemtabl(tablesInner.tr52e.toString());
	itempf.setItemitem( wsaaTr52eKey.toString());
	itempf.setValidflag("1");
	List<Itempf> itempfList = itemDAO.findByItemDates(itempf);		 

	if ((itempfList == null || itempfList.size() == 0 )
			&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
		syserrrec.params.set(wsaaTr52eKey);
		syserrrec.statuz.set("MRNF");
		fatalError600();
	}
	if (itempfList != null && itempfList.size() != 0 ) {
		tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}
}

protected void callRounding8000()
{
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(wsspcomn.company);
	zrdecplrec.statuz.set(Varcom.oK);
	/*    MOVE CHDRLNB-CNTCURR        TO ZRDP-CURRENCY.                */
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		fatalError600();
	}
	/*EXIT*/
}

protected void whereNext4000()
	{
	/* IBPLIFE-2274 - Starts */
	String t6A0 = "T6A0";
	if(onePflag && isEQ(t3615rec.onepcashless,"Y") && t6A0.equals(wsaaBatckey.batcBatctrcde.toString())){		
		nextProgram4010();		
	}else   /* IBPLIFE-2274 - Ends */
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

/* IBPLIFE-2274 - Starts */
protected void nextProgram4010() {
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(0);
		for (int loopVar1 = 0; loopVar1 != 8; loopVar1 += 1) {
			saveProgram4100();
		}
	}
	if (isEQ(wsaa1Pflag, "X")) {
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		wsaa1Pflag.set("?");
		gensswrec.function.set("A");
		callGenssw4300();
		return ;
	}
	if (isEQ(wsaa1Pflag, "?")) {
		onePCashRet4900();
		endChoices4050();
	}			
}

protected void onePCashRet4900()
{
	/*PARA*/
	/* On return from this  request, the current stack "action" will   */
	/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
	/* handle the return from options and extras:                      */
	/*  - calculate  the  premium  as  described  above  in  the       */
	/*       'Validation' section, and  check  that it is within       */
	/*       the tolerance limit,                                      */
	wsaa1Pflag.set("+");
	
	/*  - restore the saved programs to the program stack              */
	compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar10 = 0; loopVar10 != 8; loopVar10 += 1){
		restore4610();
	}
	/*  - blank  out  the  stack  "action"                             */
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	/*       Set  WSSP-NEXTPROG to  the current screen                 */
	/*       name (thus returning to re-display the screen).           */
	wsspcomn.nextprog.set(scrnparams.scrname);
	/*EXIT*/
}

protected void restore4610()
{
	/*PARA*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaY.add(1);
	wsaaX.add(1);
	/*EXIT*/
}

protected void endChoices4050() {
	/*    No more selected (or none)*/
	/*       - restore stack form wsaa to wssp*/
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(0);
		for (int loopVar2 = 0; loopVar2 != 8; loopVar2 += 1){
			restoreProgram4200();
		}
	}
	/*    If current stack action is * then re-display screen*/
	/*       (in this case, some other option(s) were requested)*/
	/*    Otherwise continue as normal.*/


	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	else {
		wsspcomn.programPtr.add(1);
	}
}

protected void restoreProgram4200()
{
	/*RESTORE*/
	wsaaX.add(1);
	wsaaY.add(1);
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	/*EXIT*/
}


protected void callGenssw4300(){	
	callSubroutine4310();
}

protected void callSubroutine4310(){	
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz,Varcom.oK)
	&& isNE(gensswrec.statuz,Varcom.mrnf)) {
		syserrrec.params.set(gensswrec.gensswRec);
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by genswch redisplay the scre*/
	/* with an error and the options and extras indicator*/
	/* with its initial load value*/
	if (isEQ(gensswrec.statuz,Varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*     MOVE V045                TO SCRN-ERROR-CODE               */
		scrnparams.errorCode.set(errorsInner.h093);
		wsspcomn.nextprog.set(scrnparams.scrname);
		return ;
	}
	/*    load from gensw to wssp*/
	compute(wsaaX, 0).set(add(1,wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		loadProgram4400();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}

protected void loadProgram4400(){	
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}

protected void saveProgram4100(){	
	/*SAVE*/
	wsaaX.add(1);
	wsaaY.add(1);
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	/*EXIT*/
}
/* IBPLIFE-2274 - Ends */

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData fluplnbrec = new FixedLengthStringData(10).init("FLUPLNBREC");
	private FixedLengthStringData dshnrec1 = new FixedLengthStringData(10).init("DSHNREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
}

private static final class WsaaTr5awArrayInner {

	private FixedLengthStringData wsaaTr5awArray = new FixedLengthStringData(36);
	private FixedLengthStringData[] wsaaTr5awRec = FLSArrayPartOfStructure(4, 9, wsaaTr5awArray, 0);
//private PackedDecimalData[] wsaaT6658Currfrom = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Rec, 0);
//	private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 10, HIVALUES);
//	private FixedLengthStringData[] wsaaT6658Annmthd = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0);
//	private FixedLengthStringData[] wsaaTr5awkey = FLSDArrayPartOfArrayStructure(3, wsaaTr5awRec, 14);
	
	private FixedLengthStringData[] wsaacngfrmtaxpyr = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 0);
	private FixedLengthStringData[] wsaacngfrmbrthctry = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 1);
	private FixedLengthStringData[] wsaacngfrmnatlty = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 2);
	private FixedLengthStringData[] wsaacngtotaxpyr = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 3);
	private FixedLengthStringData[] wsaacngtobrthctry = FLSDArrayPartOfArrayStructure(1,wsaaTr5awRec, 4);
	private FixedLengthStringData[] wsaacngtonatlty = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 5);
	private FixedLengthStringData[] wsaaFupCde = FLSDArrayPartOfArrayStructure(3, wsaaTr5awRec, 6);
}

/*
 * Class transformed  from Data Structure WSAA-BILLING-INFORMATION--INNER
 */
protected static final class WsaaBillingInformationInner {

	/* WSAA-BILLING-INFORMATION */
	private FixedLengthStringData[] wsaaBillingDetails = FLSInittedArray (9, 116);
	private ZonedDecimalData[] wsaaIncomeSeqNo = ZDArrayPartOfArrayStructure(2, 0, wsaaBillingDetails, 0, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaBillfreq = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 2);
	private FixedLengthStringData[] wsaaBillchnl = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 4);
	private PackedDecimalData[] wsaaBillcd = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 6);
	private PackedDecimalData[] wsaaBtdate = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 11);
	private FixedLengthStringData[] wsaaBillcurr = FLSDArrayPartOfArrayStructure(3, wsaaBillingDetails, 16);
	private FixedLengthStringData[] wsaaClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaBillingDetails, 19);
	private FixedLengthStringData[] wsaaClntnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 20);
	private FixedLengthStringData[] wsaaMandref = FLSDArrayPartOfArrayStructure(5, wsaaBillingDetails, 28);
	private FixedLengthStringData[] wsaaGrupkey = FLSDArrayPartOfArrayStructure(12, wsaaBillingDetails, 33);
	private PackedDecimalData[] wsaaRegprem = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 45);
	private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 54);
	private PackedDecimalData[] wsaaSpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 63);
	private PackedDecimalData[] wsaaRpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 72);
	private PackedDecimalData[] wsaaInstprm = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 81);
	private PackedDecimalData[] wsaaPremTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 90);
	private PackedDecimalData[] wsaaTaxrelamt = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 99);
	private FixedLengthStringData[] wsaaInrevnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 108);
	public PackedDecimalData[] getWsaaInstprm() {
		return wsaaInstprm;
	}
	public void setWsaaInstprm(PackedDecimalData[] wsaaInstprm) {
		this.wsaaInstprm = wsaaInstprm;
	}
	public FixedLengthStringData[] getWsaaBillfreq() {
		return wsaaBillfreq;
	}
	public ZonedDecimalData[] getWsaaIncomeSeqNo() {
		return wsaaIncomeSeqNo;
	}
	public void setWsaaIncomeSeqNo(ZonedDecimalData[] wsaaIncomeSeqNo) {
		this.wsaaIncomeSeqNo = wsaaIncomeSeqNo;
	}
	public void setWsaaBillfreq(FixedLengthStringData[] wsaaBillfreq) {
		this.wsaaBillfreq = wsaaBillfreq;
	}
	public PackedDecimalData[] getWsaaRegprem() {
		return wsaaRegprem;
	}
	public void setWsaaRegprem(PackedDecimalData[] wsaaRegprem) {
		this.wsaaRegprem = wsaaRegprem;
	}
	public PackedDecimalData[] getWsaaSingp() {
		return wsaaSingp;
	}
	public void setWsaaSingp(PackedDecimalData[] wsaaSingp) {
		this.wsaaSingp = wsaaSingp;
	}
	public FixedLengthStringData[] getWsaaClntnum() {
		return wsaaClntnum;
	}
	public void setWsaaClntnum(FixedLengthStringData[] wsaaClntnum) {
		this.wsaaClntnum = wsaaClntnum;
	}
	public FixedLengthStringData[] getWsaaClntcoy() {
		return wsaaClntcoy;
	}
	public void setWsaaClntcoy(FixedLengthStringData[] wsaaClntcoy) {
		this.wsaaClntcoy = wsaaClntcoy;
	}
	public PackedDecimalData[] getWsaaPremTax() {
		return wsaaPremTax;
	}
	public void setWsaaPremTax(PackedDecimalData[] wsaaPremTax) {
		this.wsaaPremTax = wsaaPremTax;
	}
	public PackedDecimalData[] getWsaaRpTax() {
		return wsaaRpTax;
	}
	public void setWsaaRpTax(PackedDecimalData[] wsaaRpTax) {
		this.wsaaRpTax = wsaaRpTax;
	}
	public PackedDecimalData[] getWsaaSpTax() {
		return wsaaSpTax;
	}
	public void setWsaaSpTax(PackedDecimalData[] wsaaSpTax) {
		this.wsaaSpTax = wsaaSpTax;
	}
}

/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
	/* TABLES */
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t3615 = new FixedLengthStringData(5).init("T3615");
}


/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");	
	private FixedLengthStringData rul2 = new FixedLengthStringData(4).init("RUL2");
}

protected Sr556ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(Sr556ScreenVars.class);
}




}
