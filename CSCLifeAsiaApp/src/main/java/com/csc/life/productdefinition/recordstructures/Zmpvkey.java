package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:19
 * Description:
 * Copybook name: ZMPVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zmpvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zmpvFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zmpvKey = new FixedLengthStringData(256).isAPartOf(zmpvFileKey, 0, REDEFINE);
  	public FixedLengthStringData zmpvZmedcoy = new FixedLengthStringData(1).isAPartOf(zmpvKey, 0);
  	public FixedLengthStringData zmpvZmedprv = new FixedLengthStringData(10).isAPartOf(zmpvKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(zmpvKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zmpvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zmpvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}