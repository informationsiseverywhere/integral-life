
package com.csc.life.productdefinition.programs;



import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.productdefinition.screens.Sd5h7ScreenVars;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;


public class Pd5h7 extends ScreenProgCS {
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5H7");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	/* ERRORS */
	private String e186 = "E186";
	
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Td5h7rec td5h7rec = new Td5h7rec();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itmdkey wsaaItmdkey = new Itmdkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5h7ScreenVars sv = ScreenProgram.getScreenVars(Sd5h7ScreenVars.class);

	private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);	
	private Itempf itempf = new Itempf();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();

	public Pd5h7() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5h7", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void initialise1000() {
		initialise1010();
		cont1190();
	}

	protected void initialise1010() {
		/* INITIALISE-SCREEN */
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.dataArea.set(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		
		descpf=descDAO.getdescData("IT", wsaaItemkey.itemItemtabl.toString(), wsaaItemkey.itemItemitem.toString(), wsaaItemkey.itemItemcoy.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			fatalError600();
		}
		sv.longdesc.set(descpf.getLongdesc());

		itempf = itempfDAO.readItdmpf("IT",wsaaItemkey.itemItemcoy.toString(), wsaaItemkey.itemItemtabl.toString(),
				itmdIO.getItemItmfrm().toInt(),wsaaItemkey.itemItemitem.toString());

		if (itempf == null ) {
			fatalError600();
		}
		td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		if (!itempf.getGenarea().equals(SPACES)) {
			cont1190();
		}
	}

	protected void cont1190() {
		sv.intrate.set(td5h7rec.intrate);
		sv.intcalfreq.set(td5h7rec.intcalfreq);
		sv.intcapfreq.set(td5h7rec.intcapfreq);
	}

	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (wsspcomn.flag.equals("I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
	}

	protected void screenEdit2000() {
		screenIo2010();
		exit2090();
	}

	protected void screenIo2010() {
//		if (sv.intrate.equals(SPACES)) {
		if(sv.intrate != null && sv.intrate.toString().trim() == " "){
//			if (sv.intrate.equals(SPACES)) {
			sv.intrateErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (!sv.errorIndicators.equals(SPACES)) {
			wsspcomn.edterror.set("Y");
		} else {
			wsspcomn.edterror.set(varcom.oK);
		}
		/* VALIDATE */
		if (wsspcomn.flag.equals("I")) {
			exit2090();
		}
	}

	protected void exit2090() {
		if (!sv.errorIndicators.equals(SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* EXIT */
		/** UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION */
	}

	protected void update3000() {
		loadWsspFields3100();
	}

	protected void loadWsspFields3100() {
		if (wsspcomn.flag.equals("I")) {
			return;

		}
		updateRec5000();
		/* COMMIT */
	}

	protected void updateRec5000() {
		try {
			
			compareFields5020();
		
		} catch (GOTOException e) {
		}
	}

	protected void compareFields5020() {
		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (!wsaaUpdateFlag.equals("Y")) {
			return;
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);

		itempf.setGenarea(td5h7rec.td5h7Rec.toString().getBytes());
		itempf.setItempfx(wsaaItemkey.itemItempfx.toString());
		itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM, ITMFRM, ITMTO ");
	}

	protected void checkChanges3100() {
		check3100();
	}

	protected void check3100() {
		if (!sv.itmfrm.equals(itempf.getItmfrm())) {
			itempf.setItmfrm(sv.itmfrm.toInt());
			wsaaUpdateFlag = "Y";
		}
		if (!sv.itmto.equals(itempf.getItmto())) {
			itempf.setItmto(sv.itmto.toInt());
			wsaaUpdateFlag = "Y";
		}
		
		if (!sv.intrate.equals(td5h7rec.intrate)) {
			td5h7rec.intrate.set(sv.intrate);
			wsaaUpdateFlag = "Y";
		}
		if (!sv.intcalfreq.equals(td5h7rec.intcalfreq)) {
			td5h7rec.intcalfreq.set(sv.intcalfreq);
			wsaaUpdateFlag = "Y";
		}
		if (!sv.intcapfreq.equals(td5h7rec.intcapfreq)) {
			td5h7rec.intcapfreq.set(sv.intcapfreq);
			wsaaUpdateFlag = "Y";
		}
		
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}

}
