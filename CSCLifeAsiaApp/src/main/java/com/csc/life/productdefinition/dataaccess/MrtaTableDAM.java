package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;


/**
 * 	
 * File: MrtaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:41
 * Class transformed from MRTA.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MrtaTableDAM extends MrtapfTableDAM {

	public MrtaTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MRTA");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "PRAT, " +
		            "COVERC, " +
		            "PRMDETAILS, " +
		            "MLINSOPT, " +
		            "MLRESIND, " +
		            "MBNKREF, " +
					"LOANDUR, " +  //BRD-139
			        "INTCALTYPE, " + //BRD-139
					"PHUNITVALUE," +
		            //ILIFE-2745-STARTS
		            "USRPRF, " +
					"JOBNM, " +
					"DATIME, " +
					//ILIFE-2745-ENDS
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               prat,
                               coverc,
                               prmdetails,
                               mlinsopt,
                               mlresind,
                               mbnkref,
						       loandur, //BRD-139
					       	   intcaltype, //BRD-139
					           phunitvalue,
                               //ILIFE-2745-STARTS
                               userProfile,
                               jobName,
                               datime,
                             //ILIFE-2745-ENDS
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(108);//ILIFE-2745
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getPrat().toInternal()
					+ getCoverc().toInternal()
					+ getPrmdetails().toInternal()
					+ getMlinsopt().toInternal()
					+ getMlresind().toInternal()
					+ getMbnkref().toInternal()
					+ getLoandur().toInternal()//BRD-139
					+ getIntCalType().toInternal()//BRD-139
					+ getPhunitvalue().toInternal()
					//ILIFE-2745-STARTS
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
					//ILIFE-2745-ENDS
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, prat);
			what = ExternalData.chop(what, coverc);
			what = ExternalData.chop(what, prmdetails);
			what = ExternalData.chop(what, mlinsopt);
			what = ExternalData.chop(what, mlresind);
			what = ExternalData.chop(what, mbnkref);
			what = ExternalData.chop(what, loandur);
			what = ExternalData.chop(what, intcaltype);
			what = ExternalData.chop(what, phunitvalue);
		
			//ILIFE-2745-STARTS
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			//ILIFE-2745-ENDS
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getPrat() {
		return prat;
	}
	public void setPrat(Object what) {
		setPrat(what, false);
	}
	public void setPrat(Object what, boolean rounded) {
		if (rounded)
			prat.setRounded(what);
		else
			prat.set(what);
	}	
	public PackedDecimalData getCoverc() {
		return coverc;
	}
	public void setCoverc(Object what) {
		setCoverc(what, false);
	}
	public void setCoverc(Object what, boolean rounded) {
		if (rounded)
			coverc.setRounded(what);
		else
			coverc.set(what);
	}	
	public FixedLengthStringData getPrmdetails() {
		return prmdetails;
	}
	public void setPrmdetails(Object what) {
		prmdetails.set(what);
	}	
	public FixedLengthStringData getMlinsopt() {
		return mlinsopt;
	}
	public void setMlinsopt(Object what) {
		mlinsopt.set(what);
	}	
	public FixedLengthStringData getMlresind() {
		return mlresind;
	}
	public void setMlresind(Object what) {
		mlresind.set(what);
	}	
	public FixedLengthStringData getMbnkref() {
		return mbnkref;
	}
	public void setMbnkref(Object what) {
		mbnkref.set(what);
	}	
	//BRD-139:Start
	public PackedDecimalData getLoandur() {
		return loandur;
	}
	public void setLoandur(Object what) {
		loandur.set(what);
	}
	public FixedLengthStringData getIntCalType() {
		return intcaltype;
	}
	public void setIntCalType(Object what) {
		intcaltype.set(what);
	}
	public PackedDecimalData getPhunitvalue() {
		return phunitvalue;
	}
	public void setPhunitvalue(Object what) {
		phunitvalue.set(what);
	}
	//BRD-139:End
	//ILIFE-2745-STARTS
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//ILIFE-2745-ENDS

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		prat.clear();
		coverc.clear();
		prmdetails.clear();
		mlinsopt.clear();
		mlresind.clear();
		mbnkref.clear();		
		loandur.clear();
		intcaltype.clear();
		phunitvalue.clear();
		//ILIFE-2745-STARTS
		userProfile.clear();
		jobName.clear();
		datime.clear();	
		//ILIFE-2745-ENDS
	}


}