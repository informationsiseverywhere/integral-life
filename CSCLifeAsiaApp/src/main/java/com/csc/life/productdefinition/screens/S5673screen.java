package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5673screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 4, 22, 17, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5673ScreenVars sv = (S5673ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5673screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5673ScreenVars screenVars = (S5673ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.ctable01.setClassString("");
		screenVars.creq01.setClassString("");
		screenVars.rtable01.setClassString("");
		screenVars.rreq01.setClassString("");
		screenVars.rtable02.setClassString("");
		screenVars.rreq02.setClassString("");
		screenVars.rtable03.setClassString("");
		screenVars.rreq03.setClassString("");
		screenVars.rtable04.setClassString("");
		screenVars.rreq04.setClassString("");
		screenVars.rtable05.setClassString("");
		screenVars.rreq05.setClassString("");
		screenVars.rtable06.setClassString("");
		screenVars.rreq06.setClassString("");
		screenVars.ctmaxcov01.setClassString("");
		screenVars.ctable02.setClassString("");
		screenVars.creq02.setClassString("");
		screenVars.rtable07.setClassString("");
		screenVars.rreq07.setClassString("");
		screenVars.rtable08.setClassString("");
		screenVars.rreq08.setClassString("");
		screenVars.rtable09.setClassString("");
		screenVars.rreq09.setClassString("");
		screenVars.rtable10.setClassString("");
		screenVars.rreq10.setClassString("");
		screenVars.rtable11.setClassString("");
		screenVars.rreq11.setClassString("");
		screenVars.rtable12.setClassString("");
		screenVars.rreq12.setClassString("");
		screenVars.ctmaxcov02.setClassString("");
		screenVars.ctable03.setClassString("");
		screenVars.creq03.setClassString("");
		screenVars.rtable13.setClassString("");
		screenVars.rreq13.setClassString("");
		screenVars.rtable14.setClassString("");
		screenVars.rreq14.setClassString("");
		screenVars.rtable15.setClassString("");
		screenVars.rreq15.setClassString("");
		screenVars.rtable16.setClassString("");
		screenVars.rreq16.setClassString("");
		screenVars.rtable17.setClassString("");
		screenVars.rreq17.setClassString("");
		screenVars.rtable18.setClassString("");
		screenVars.rreq18.setClassString("");
		screenVars.ctmaxcov03.setClassString("");
		screenVars.ctable04.setClassString("");
		screenVars.creq04.setClassString("");
		screenVars.rtable19.setClassString("");
		screenVars.rreq19.setClassString("");
		screenVars.rtable20.setClassString("");
		screenVars.rreq20.setClassString("");
		screenVars.rtable21.setClassString("");
		screenVars.rreq21.setClassString("");
		screenVars.rtable22.setClassString("");
		screenVars.rreq22.setClassString("");
		screenVars.rtable23.setClassString("");
		screenVars.rreq23.setClassString("");
		screenVars.rtable24.setClassString("");
		screenVars.rreq24.setClassString("");
		screenVars.ctmaxcov04.setClassString("");
		screenVars.ctable05.setClassString("");
		screenVars.creq05.setClassString("");
		screenVars.rtable25.setClassString("");
		screenVars.rreq25.setClassString("");
		screenVars.rtable26.setClassString("");
		screenVars.rreq26.setClassString("");
		screenVars.rtable27.setClassString("");
		screenVars.rreq27.setClassString("");
		screenVars.rtable28.setClassString("");
		screenVars.rreq28.setClassString("");
		screenVars.rtable29.setClassString("");
		screenVars.rreq29.setClassString("");
		screenVars.rtable30.setClassString("");
		screenVars.rreq30.setClassString("");
		screenVars.ctmaxcov05.setClassString("");
		screenVars.ctable06.setClassString("");
		screenVars.creq06.setClassString("");
		screenVars.rtable31.setClassString("");
		screenVars.rreq31.setClassString("");
		screenVars.rtable32.setClassString("");
		screenVars.rreq32.setClassString("");
		screenVars.rtable33.setClassString("");
		screenVars.rreq33.setClassString("");
		screenVars.rtable34.setClassString("");
		screenVars.rreq34.setClassString("");
		screenVars.rtable35.setClassString("");
		screenVars.rreq35.setClassString("");
		screenVars.rtable36.setClassString("");
		screenVars.rreq36.setClassString("");
		screenVars.ctmaxcov06.setClassString("");
		screenVars.ctable07.setClassString("");
		screenVars.creq07.setClassString("");
		screenVars.rtable37.setClassString("");
		screenVars.rreq37.setClassString("");
		screenVars.rtable38.setClassString("");
		screenVars.rreq38.setClassString("");
		screenVars.rtable39.setClassString("");
		screenVars.rreq39.setClassString("");
		screenVars.rtable40.setClassString("");
		screenVars.rreq40.setClassString("");
		screenVars.rtable41.setClassString("");
		screenVars.rreq41.setClassString("");
		screenVars.rtable42.setClassString("");
		screenVars.rreq42.setClassString("");
		screenVars.ctmaxcov07.setClassString("");
		screenVars.ctable08.setClassString("");
		screenVars.creq08.setClassString("");
		screenVars.rtable43.setClassString("");
		screenVars.rreq43.setClassString("");
		screenVars.rtable44.setClassString("");
		screenVars.rreq44.setClassString("");
		screenVars.rtable45.setClassString("");
		screenVars.rreq45.setClassString("");
		screenVars.rtable46.setClassString("");
		screenVars.rreq46.setClassString("");
		screenVars.rtable47.setClassString("");
		screenVars.rreq47.setClassString("");
		screenVars.rtable48.setClassString("");
		screenVars.rreq48.setClassString("");
		screenVars.ctmaxcov08.setClassString("");
		screenVars.gitem.setClassString("");
		screenVars.zrlifind01.setClassString("");
		screenVars.zrlifind02.setClassString("");
		screenVars.zrlifind03.setClassString("");
		screenVars.zrlifind04.setClassString("");
		screenVars.zrlifind05.setClassString("");
		screenVars.zrlifind06.setClassString("");
		screenVars.zrlifind07.setClassString("");
		screenVars.zrlifind08.setClassString("");
	}

/**
 * Clear all the variables in S5673screen
 */
	public static void clear(VarModel pv) {
		S5673ScreenVars screenVars = (S5673ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.ctable01.clear();
		screenVars.creq01.clear();
		screenVars.rtable01.clear();
		screenVars.rreq01.clear();
		screenVars.rtable02.clear();
		screenVars.rreq02.clear();
		screenVars.rtable03.clear();
		screenVars.rreq03.clear();
		screenVars.rtable04.clear();
		screenVars.rreq04.clear();
		screenVars.rtable05.clear();
		screenVars.rreq05.clear();
		screenVars.rtable06.clear();
		screenVars.rreq06.clear();
		screenVars.ctmaxcov01.clear();
		screenVars.ctable02.clear();
		screenVars.creq02.clear();
		screenVars.rtable07.clear();
		screenVars.rreq07.clear();
		screenVars.rtable08.clear();
		screenVars.rreq08.clear();
		screenVars.rtable09.clear();
		screenVars.rreq09.clear();
		screenVars.rtable10.clear();
		screenVars.rreq10.clear();
		screenVars.rtable11.clear();
		screenVars.rreq11.clear();
		screenVars.rtable12.clear();
		screenVars.rreq12.clear();
		screenVars.ctmaxcov02.clear();
		screenVars.ctable03.clear();
		screenVars.creq03.clear();
		screenVars.rtable13.clear();
		screenVars.rreq13.clear();
		screenVars.rtable14.clear();
		screenVars.rreq14.clear();
		screenVars.rtable15.clear();
		screenVars.rreq15.clear();
		screenVars.rtable16.clear();
		screenVars.rreq16.clear();
		screenVars.rtable17.clear();
		screenVars.rreq17.clear();
		screenVars.rtable18.clear();
		screenVars.rreq18.clear();
		screenVars.ctmaxcov03.clear();
		screenVars.ctable04.clear();
		screenVars.creq04.clear();
		screenVars.rtable19.clear();
		screenVars.rreq19.clear();
		screenVars.rtable20.clear();
		screenVars.rreq20.clear();
		screenVars.rtable21.clear();
		screenVars.rreq21.clear();
		screenVars.rtable22.clear();
		screenVars.rreq22.clear();
		screenVars.rtable23.clear();
		screenVars.rreq23.clear();
		screenVars.rtable24.clear();
		screenVars.rreq24.clear();
		screenVars.ctmaxcov04.clear();
		screenVars.ctable05.clear();
		screenVars.creq05.clear();
		screenVars.rtable25.clear();
		screenVars.rreq25.clear();
		screenVars.rtable26.clear();
		screenVars.rreq26.clear();
		screenVars.rtable27.clear();
		screenVars.rreq27.clear();
		screenVars.rtable28.clear();
		screenVars.rreq28.clear();
		screenVars.rtable29.clear();
		screenVars.rreq29.clear();
		screenVars.rtable30.clear();
		screenVars.rreq30.clear();
		screenVars.ctmaxcov05.clear();
		screenVars.ctable06.clear();
		screenVars.creq06.clear();
		screenVars.rtable31.clear();
		screenVars.rreq31.clear();
		screenVars.rtable32.clear();
		screenVars.rreq32.clear();
		screenVars.rtable33.clear();
		screenVars.rreq33.clear();
		screenVars.rtable34.clear();
		screenVars.rreq34.clear();
		screenVars.rtable35.clear();
		screenVars.rreq35.clear();
		screenVars.rtable36.clear();
		screenVars.rreq36.clear();
		screenVars.ctmaxcov06.clear();
		screenVars.ctable07.clear();
		screenVars.creq07.clear();
		screenVars.rtable37.clear();
		screenVars.rreq37.clear();
		screenVars.rtable38.clear();
		screenVars.rreq38.clear();
		screenVars.rtable39.clear();
		screenVars.rreq39.clear();
		screenVars.rtable40.clear();
		screenVars.rreq40.clear();
		screenVars.rtable41.clear();
		screenVars.rreq41.clear();
		screenVars.rtable42.clear();
		screenVars.rreq42.clear();
		screenVars.ctmaxcov07.clear();
		screenVars.ctable08.clear();
		screenVars.creq08.clear();
		screenVars.rtable43.clear();
		screenVars.rreq43.clear();
		screenVars.rtable44.clear();
		screenVars.rreq44.clear();
		screenVars.rtable45.clear();
		screenVars.rreq45.clear();
		screenVars.rtable46.clear();
		screenVars.rreq46.clear();
		screenVars.rtable47.clear();
		screenVars.rreq47.clear();
		screenVars.rtable48.clear();
		screenVars.rreq48.clear();
		screenVars.ctmaxcov08.clear();
		screenVars.gitem.clear();
		screenVars.zrlifind01.clear();
		screenVars.zrlifind02.clear();
		screenVars.zrlifind03.clear();
		screenVars.zrlifind04.clear();
		screenVars.zrlifind05.clear();
		screenVars.zrlifind06.clear();
		screenVars.zrlifind07.clear();
		screenVars.zrlifind08.clear();
	}
}
