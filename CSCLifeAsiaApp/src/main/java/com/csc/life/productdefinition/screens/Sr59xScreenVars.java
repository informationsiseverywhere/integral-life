package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR59X
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class Sr59xScreenVars extends SmartVarModel { 

	

	public FixedLengthStringData dataArea = new FixedLengthStringData(328);
	public FixedLengthStringData dataFields = new FixedLengthStringData(104).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,14);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,22);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData savsrisk = DD.savsrisk.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData otherscheallowed = DD.otherscheallowed.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData defaultScheme= DD.schemekey.copy().isAPartOf(dataFields,62);
	//*ILIFE-3693 -nnaveenkumar 
	public FixedLengthStringData dfcontrn=DD.dfcontrn.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData recoveryType = DD.recoveryType.copy().isAPartOf(dataFields,100);
	
	public FixedLengthStringData cocontpay = DD.cocontpay.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData liscpay = DD.liscpay.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData polfee = DD.polfee.copy().isAPartOf(dataFields,103);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 104);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData savsriskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData otherscheallowedErr =new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData defaultSchemeErr= new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	//*ILIFE-3693 -nnaveenkumar 
	public FixedLengthStringData dfcontrnErr= new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData recoveryTypeErr= new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	
	public FixedLengthStringData cocontpayErr =new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData liscpayErr =new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData polfeeErr =new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 160);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[]  savsriskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[]  otherscheallowedOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[]  defaultSchemeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	//*ILIFE-3693 -nnaveenkumar 
	public FixedLengthStringData[]  dfcontrnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[]  recoveryTypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	
	public FixedLengthStringData[]  cocontpayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[]  liscpayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[]  polfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr59xscreenWritten = new LongData(0);
	public LongData Sr59xprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr59xScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {

		fieldIndMap.put(savsriskOut,new String[] {"91",null, "-91","102", null, null, null, null, null, null, null, null});
		fieldIndMap.put(otherscheallowedOut,new String[] {"92",null, "-92","103", null, null, null, null, null, null, null, null});
		fieldIndMap.put(defaultSchemeOut,new String[] {"93",null, "-93","104", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto,savsrisk,otherscheallowed,defaultScheme,dfcontrn,recoveryType,cocontpay,liscpay,polfee};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut,savsriskOut,otherscheallowedOut,defaultSchemeOut,dfcontrnOut,recoveryTypeOut,cocontpayOut,liscpayOut,polfeeOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr,savsriskErr,otherscheallowedErr,defaultSchemeErr,dfcontrnErr,recoveryTypeErr,cocontpayErr,liscpayErr,polfeeErr};
		//ILIFE-1137 ends
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr59xscreen.class;
		protectRecord = Sr59xprotect.class;
	}

}
