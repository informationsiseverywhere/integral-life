package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Fluupf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface FluupfDAO extends BaseDAO<Fluupf> 
{
	     public int populateFluppfTemp(String company, int batchExtractSize, String Fstatus1, String Fstatus2);

		public List<Fluupf> loadDataByBatch(int intBatchID);
		public boolean updateByFluppfID(Fluupf  Fluupfobj);
		public void updateFlupRecord(List<Fluupf> fluplList);
}
