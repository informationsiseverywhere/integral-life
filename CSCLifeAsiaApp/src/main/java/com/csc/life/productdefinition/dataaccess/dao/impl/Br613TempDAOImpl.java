package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.productdefinition.dataaccess.dao.Br613TempDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br613TempDAOImpl extends BaseDAOImpl<Covrpf> implements Br613TempDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(Br613TempDAOImpl.class);

	public Map<String, List<Covrpf>> searchCovrRecord(String coy, List<String> chdrnumList) {
		StringBuilder sqlCovrSelect1 = new StringBuilder();
    	sqlCovrSelect1.append("SELECT CHDRCOY,CHDRNUM,CBCVIN,COVERAGE,CRRCD,CRTABLE,CURRFROM,INSTPREM,");
        sqlCovrSelect1.append("JLIFE,LIFE,PLNSFX,PCESTRM,PRMCUR,PSTATCODE,RRTDAT,RIDER,RCESTRM,SINGP,");
        sqlCovrSelect1.append("STATCODE,SUMINS,UNIQUE_NUMBER,VALIDFLAG,ZLINSTPREM ");
        sqlCovrSelect1.append(", TRANNO, CURRTO, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS, ");
        sqlCovrSelect1.append("REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T, ");
        sqlCovrSelect1.append("LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, "); 
        sqlCovrSelect1.append("CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR,  CBRVPR, ");
        sqlCovrSelect1.append("CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZCLSTATE, LNKGNO, LNKGSUBREFNO, REINSTATED, ZSTPDUTY01, TPDTYPE, PRORATEPREM ");
        sqlCovrSelect1.append("FROM COVRPF WHERE CHDRCOY=? AND ");
		sqlCovrSelect1.append("VALIDFLAG='1' AND ");
		sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList)); 
		sqlCovrSelect1.append(
				" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		ResultSet sqlcovrpf1rs = null;
		List<Covrpf> covrpfSearchResult = new LinkedList<Covrpf>();
		Map<String, List<Covrpf>> covrMap = new HashMap<String, List<Covrpf>>();
		try {
			psCovrSelect.setInt(1, Integer.parseInt(coy));
			sqlcovrpf1rs = executeQuery(psCovrSelect);

			while (sqlcovrpf1rs.next()) {
				Covrpf covrpf = new Covrpf();
				covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(3));
                covrpf.setCoverage(sqlcovrpf1rs.getString(4));
                covrpf.setCrrcd(sqlcovrpf1rs.getInt(5));
                covrpf.setCrtable(sqlcovrpf1rs.getString(6));
                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(7));
                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(8));
                covrpf.setJlife(sqlcovrpf1rs.getString(9));
                covrpf.setLife(sqlcovrpf1rs.getString(10));
                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(11));
                covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(12));
                covrpf.setPremCurrency(sqlcovrpf1rs.getString(13));
                covrpf.setPstatcode(sqlcovrpf1rs.getString(14));
                covrpf.setRerateDate(sqlcovrpf1rs.getInt(15));
                covrpf.setRider(sqlcovrpf1rs.getString(16));
                covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(17));
                covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(18));
                covrpf.setStatcode(sqlcovrpf1rs.getString(19));
                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(20));
                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(21));
                covrpf.setValidflag(sqlcovrpf1rs.getString(22));
                covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(23));
				covrpf.setTranno(sqlcovrpf1rs.getInt(24));
                covrpf.setCurrto(sqlcovrpf1rs.getInt(25));
                covrpf.setStatFund(sqlcovrpf1rs.getString(26));
                covrpf.setStatSect(sqlcovrpf1rs.getString(27));
                covrpf.setStatSubsect(sqlcovrpf1rs.getString(28));
                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(29));
                covrpf.setPremCessDate(sqlcovrpf1rs.getInt(30));
                covrpf.setBenCessDate(sqlcovrpf1rs.getInt(31));
                covrpf.setNextActDate(sqlcovrpf1rs.getInt(32));
                covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(33));
                covrpf.setPremCessAge(sqlcovrpf1rs.getInt(34));
                covrpf.setBenCessAge(sqlcovrpf1rs.getInt(35));
                covrpf.setBenCessTerm(sqlcovrpf1rs.getInt(36));
                covrpf.setSicurr(sqlcovrpf1rs.getString(37));
                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal(38));
                covrpf.setMortcls(sqlcovrpf1rs.getString(39));
                covrpf.setReptcd01(sqlcovrpf1rs.getString(40));
                covrpf.setReptcd02(sqlcovrpf1rs.getString(41));
                covrpf.setReptcd03(sqlcovrpf1rs.getString(42));
                covrpf.setReptcd04(sqlcovrpf1rs.getString(43));
                covrpf.setReptcd05(sqlcovrpf1rs.getString(44));
                covrpf.setReptcd06(sqlcovrpf1rs.getString(45));
                covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal(46));
                covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal(47));
                covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal(48));
                covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal(49));
                covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal(50));
                covrpf.setTermid(sqlcovrpf1rs.getString(51));
                covrpf.setTransactionDate(sqlcovrpf1rs.getInt(52));
                covrpf.setTransactionTime(sqlcovrpf1rs.getInt(53));
                covrpf.setUser(sqlcovrpf1rs.getInt(54));
                covrpf.setLiencd(sqlcovrpf1rs.getString(55));
                covrpf.setRatingClass(sqlcovrpf1rs.getString(56));
                covrpf.setIndexationInd(sqlcovrpf1rs.getString(57));
                covrpf.setBonusInd(sqlcovrpf1rs.getString(58));
                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString(59));
                covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal(60));
                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString(61));
                covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal(62));
                covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal(63));
                covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal(64));
                covrpf.setEstMatDate01(sqlcovrpf1rs.getInt(65));
                covrpf.setEstMatDate02(sqlcovrpf1rs.getInt(66));
                covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal(67));
                covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal(68));
                covrpf.setCampaign(sqlcovrpf1rs.getString(69));
                covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal(70));
                covrpf.setRtrnyrs(sqlcovrpf1rs.getInt(71));
                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString(72));
                covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt(73));
                covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString(74));
                covrpf.setFundSplitPlan(sqlcovrpf1rs.getString(75));
                covrpf.setStatreasn(sqlcovrpf1rs.getString(76));
                covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(77));
                covrpf.setSex(sqlcovrpf1rs.getString(78));
                covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt(79));
                covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt(80));
                covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt(81));
                covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt(82));
                covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt(83));
                covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt(84));
                covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt(85));
                covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt(86));
                covrpf.setJlLsInd(sqlcovrpf1rs.getString(87));
                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(88));
                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(89));
                covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(90));
                covrpf.setReviewProcessing(sqlcovrpf1rs.getInt(91));
                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(92));
                covrpf.setCpiDate(sqlcovrpf1rs.getInt(93));
                covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt(94));
                covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt(95));
                covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt(96));
                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(97));
                covrpf.setPayrseqno(sqlcovrpf1rs.getInt(98));
                covrpf.setBappmeth(sqlcovrpf1rs.getString(99));
                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(100));
                covrpf.setLoadper(sqlcovrpf1rs.getBigDecimal(101));
                covrpf.setRateadj(sqlcovrpf1rs.getBigDecimal(102));
                covrpf.setFltmort(sqlcovrpf1rs.getBigDecimal(103));
                covrpf.setPremadj(sqlcovrpf1rs.getBigDecimal(104));
                covrpf.setAgeadj(sqlcovrpf1rs.getBigDecimal(105));
                covrpf.setZclstate(sqlcovrpf1rs.getString(106));
                covrpf.setLnkgno(sqlcovrpf1rs.getString(107));//ILIFE-8248
                covrpf.setLnkgsubrefno(sqlcovrpf1rs.getString(108));//ILIFE-8248
                covrpf.setReinstated(sqlcovrpf1rs.getString(109));//ILIFE-8509
                covrpf.setZstpduty01(sqlcovrpf1rs.getBigDecimal(110));
                covrpf.setTpdtype(sqlcovrpf1rs.getString(111));
                covrpf.setProrateprem(sqlcovrpf1rs.getBigDecimal(112));

				String chdrnum = covrpf.getChdrnum();
				if (covrMap.containsKey(chdrnum)) {
					covrMap.get(chdrnum).add(covrpf);
				} else {
					covrpfSearchResult = new LinkedList<Covrpf>();
					covrpfSearchResult.add(covrpf);
					covrMap.put(chdrnum, covrpfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrSelect, sqlcovrpf1rs);
		}
		return covrMap;
	}
	public Covrpf getCovrRecord(Covrpf covr) {
        StringBuilder sqlCovrSelect1 = new StringBuilder();
        
    	sqlCovrSelect1.append("SELECT CHDRCOY,CHDRNUM,CBCVIN,COVERAGE,CRRCD,CRTABLE,CURRFROM,INSTPREM,");
        sqlCovrSelect1.append("JLIFE,LIFE,PLNSFX,PCESTRM,PRMCUR,PSTATCODE,RRTDAT,RIDER,RCESTRM,SINGP,");
        sqlCovrSelect1.append("STATCODE,SUMINS,UNIQUE_NUMBER,VALIDFLAG,ZLINSTPREM ");
        sqlCovrSelect1.append(", TRANNO, CURRTO, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS, ");
        sqlCovrSelect1.append("REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T, ");
        sqlCovrSelect1.append("LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, "); 
        sqlCovrSelect1.append("CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR,  CBRVPR, ");
        sqlCovrSelect1.append("CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ ,UNIQUE_NUMBER ");	
        sqlCovrSelect1.append("FROM COVRPF WHERE CHDRCOY=? AND ");
        sqlCovrSelect1.append("VALIDFLAG='1' AND CHDRNUM = ? AND LIFE =? AND COVERAGE= ? AND RIDER =? AND PLNSFX=?");
		sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
		ResultSet sqlcovrpf1rs = null;
		Covrpf covrpf = new Covrpf();
		try {
			ps.setInt(1, Integer.parseInt(covr.getChdrcoy()));
			ps.setString(2, covr.getChdrnum());
			ps.setString(3, covr.getLife());
			ps.setString(4, covr.getCoverage());
			ps.setString(5, covr.getRider());
			ps.setInt(6,covr.getPlanSuffix());

			sqlcovrpf1rs = executeQuery(ps);
			if(sqlcovrpf1rs.next()) {
				covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(3));
                covrpf.setCoverage(sqlcovrpf1rs.getString(4));
                covrpf.setCrrcd(sqlcovrpf1rs.getInt(5));
                covrpf.setCrtable(sqlcovrpf1rs.getString(6));
                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(7));
                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(8));
                covrpf.setJlife(sqlcovrpf1rs.getString(9));
                covrpf.setLife(sqlcovrpf1rs.getString(10));
                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(11));
                covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(12));
                covrpf.setPremCurrency(sqlcovrpf1rs.getString(13));
                covrpf.setPstatcode(sqlcovrpf1rs.getString(14));
                covrpf.setRerateDate(sqlcovrpf1rs.getInt(15));
                covrpf.setRider(sqlcovrpf1rs.getString(16));
                covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(17));
                covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(18));
                covrpf.setStatcode(sqlcovrpf1rs.getString(19));
                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(20));
                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(21));
                covrpf.setValidflag(sqlcovrpf1rs.getString(22));
                covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(23));
				covrpf.setTranno(sqlcovrpf1rs.getInt(24));
                covrpf.setCurrto(sqlcovrpf1rs.getInt(25));
                covrpf.setStatFund(sqlcovrpf1rs.getString(26));
                covrpf.setStatSect(sqlcovrpf1rs.getString(27));
                covrpf.setStatSubsect(sqlcovrpf1rs.getString(28));
                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(29));
                covrpf.setPremCessDate(sqlcovrpf1rs.getInt(30));
                covrpf.setBenCessDate(sqlcovrpf1rs.getInt(31));
                covrpf.setNextActDate(sqlcovrpf1rs.getInt(32));
                covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(33));
                covrpf.setPremCessAge(sqlcovrpf1rs.getInt(34));
                covrpf.setBenCessAge(sqlcovrpf1rs.getInt(35));
                covrpf.setBenCessTerm(sqlcovrpf1rs.getInt(36));
                covrpf.setSicurr(sqlcovrpf1rs.getString(37));
                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal(38));
                covrpf.setMortcls(sqlcovrpf1rs.getString(39));
                covrpf.setReptcd01(sqlcovrpf1rs.getString(40));
                covrpf.setReptcd02(sqlcovrpf1rs.getString(41));
                covrpf.setReptcd03(sqlcovrpf1rs.getString(42));
                covrpf.setReptcd04(sqlcovrpf1rs.getString(43));
                covrpf.setReptcd05(sqlcovrpf1rs.getString(44));
                covrpf.setReptcd06(sqlcovrpf1rs.getString(45));
                covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal(46));
                covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal(47));
                covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal(48));
                covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal(49));
                covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal(50));
                covrpf.setTermid(sqlcovrpf1rs.getString(51));
                covrpf.setTransactionDate(sqlcovrpf1rs.getInt(52));
                covrpf.setTransactionTime(sqlcovrpf1rs.getInt(53));
                covrpf.setUser(sqlcovrpf1rs.getInt(54));
                covrpf.setLiencd(sqlcovrpf1rs.getString(55));
                covrpf.setRatingClass(sqlcovrpf1rs.getString(56));
                covrpf.setIndexationInd(sqlcovrpf1rs.getString(57));
                covrpf.setBonusInd(sqlcovrpf1rs.getString(58));
                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString(59));
                covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal(60));
                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString(61));
                covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal(62));
                covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal(63));
                covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal(64));
                covrpf.setEstMatDate01(sqlcovrpf1rs.getInt(65));
                covrpf.setEstMatDate02(sqlcovrpf1rs.getInt(66));
                covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal(67));
                covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal(68));
                covrpf.setCampaign(sqlcovrpf1rs.getString(69));
                covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal(70));
                covrpf.setRtrnyrs(sqlcovrpf1rs.getInt(71));
                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString(72));
                covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt(73));
                covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString(74));
                covrpf.setFundSplitPlan(sqlcovrpf1rs.getString(75));
                covrpf.setStatreasn(sqlcovrpf1rs.getString(76));
                covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(77));
                covrpf.setSex(sqlcovrpf1rs.getString(78));
                covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt(79));
                covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt(80));
                covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt(81));
                covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt(82));
                covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt(83));
                covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt(84));
                covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt(85));
                covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt(86));
                covrpf.setJlLsInd(sqlcovrpf1rs.getString(87));
                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(88));
                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(89));
                covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(90));
                covrpf.setReviewProcessing(sqlcovrpf1rs.getInt(91));
                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(92));
                covrpf.setCpiDate(sqlcovrpf1rs.getInt(93));
                covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt(94));
                covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt(95));
                covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt(96));
                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(97));
                covrpf.setPayrseqno(sqlcovrpf1rs.getInt(98));
                covrpf.setBappmeth(sqlcovrpf1rs.getString(99));
                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(100));
                covrpf.setLoadper(sqlcovrpf1rs.getBigDecimal(101));
                covrpf.setRateadj(sqlcovrpf1rs.getBigDecimal(102));
                covrpf.setFltmort(sqlcovrpf1rs.getBigDecimal(103));
                covrpf.setPremadj(sqlcovrpf1rs.getBigDecimal(104));
                covrpf.setAgeadj(sqlcovrpf1rs.getBigDecimal(105));
                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(106));
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, sqlcovrpf1rs);
		}
		return covrpf;
	}

	public List<Covrpf> searchCovrRecord(String coy, String chdrnum) {
		 StringBuilder sb = new StringBuilder();
	        
	    	sb.append("SELECT CHDRCOY,CHDRNUM,CBCVIN,COVERAGE,CRRCD,CRTABLE,CURRFROM,INSTPREM,");
	        sb.append("JLIFE,LIFE,PLNSFX,PCESTRM,PRMCUR,PSTATCODE,RRTDAT,RIDER,RCESTRM,SINGP,");
	        sb.append("STATCODE,SUMINS,UNIQUE_NUMBER,VALIDFLAG,ZLINSTPREM ");
	        sb.append(", TRANNO, CURRTO, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS, ");
	        sb.append("REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T, ");
	        sb.append("LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, "); 
	        sb.append("CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR,  CBRVPR, ");
	        sb.append("CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ ,UNIQUE_NUMBER ");
	        sb.append("FROM COVRPF WHERE CHDRCOY=? AND ");
		sb.append("VALIDFLAG='1' AND CHDRNUM =?");

		sb.append(
				" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		PreparedStatement psCovrSelect = getPrepareStatement(sb.toString());
		ResultSet sqlcovrpf1rs = null;
		List<Covrpf> covrpfSearchResult = new ArrayList<Covrpf>();
		try {
			psCovrSelect.setInt(1, Integer.parseInt(coy));
			psCovrSelect.setString(2, chdrnum);

			sqlcovrpf1rs = executeQuery(psCovrSelect);

			while (sqlcovrpf1rs.next()) {
				Covrpf covrpf = new Covrpf();
				covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(3));
                covrpf.setCoverage(sqlcovrpf1rs.getString(4));
                covrpf.setCrrcd(sqlcovrpf1rs.getInt(5));
                covrpf.setCrtable(sqlcovrpf1rs.getString(6));
                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(7));
                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(8));
                covrpf.setJlife(sqlcovrpf1rs.getString(9));
                covrpf.setLife(sqlcovrpf1rs.getString(10));
                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(11));
                covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(12));
                covrpf.setPremCurrency(sqlcovrpf1rs.getString(13));
                covrpf.setPstatcode(sqlcovrpf1rs.getString(14));
                covrpf.setRerateDate(sqlcovrpf1rs.getInt(15));
                covrpf.setRider(sqlcovrpf1rs.getString(16));
                covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(17));
                covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(18));
                covrpf.setStatcode(sqlcovrpf1rs.getString(19));
                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(20));
                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(21));
                covrpf.setValidflag(sqlcovrpf1rs.getString(22));
                covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(23));
				covrpf.setTranno(sqlcovrpf1rs.getInt(24));
                covrpf.setCurrto(sqlcovrpf1rs.getInt(25));
                covrpf.setStatFund(sqlcovrpf1rs.getString(26));
                covrpf.setStatSect(sqlcovrpf1rs.getString(27));
                covrpf.setStatSubsect(sqlcovrpf1rs.getString(28));
                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(29));
                covrpf.setPremCessDate(sqlcovrpf1rs.getInt(30));
                covrpf.setBenCessDate(sqlcovrpf1rs.getInt(31));
                covrpf.setNextActDate(sqlcovrpf1rs.getInt(32));
                covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(33));
                covrpf.setPremCessAge(sqlcovrpf1rs.getInt(34));
                covrpf.setBenCessAge(sqlcovrpf1rs.getInt(35));
                covrpf.setBenCessTerm(sqlcovrpf1rs.getInt(36));
                covrpf.setSicurr(sqlcovrpf1rs.getString(37));
                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal(38));
                covrpf.setMortcls(sqlcovrpf1rs.getString(39));
                covrpf.setReptcd01(sqlcovrpf1rs.getString(40));
                covrpf.setReptcd02(sqlcovrpf1rs.getString(41));
                covrpf.setReptcd03(sqlcovrpf1rs.getString(42));
                covrpf.setReptcd04(sqlcovrpf1rs.getString(43));
                covrpf.setReptcd05(sqlcovrpf1rs.getString(44));
                covrpf.setReptcd06(sqlcovrpf1rs.getString(45));
                covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal(46));
                covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal(47));
                covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal(48));
                covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal(49));
                covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal(50));
                covrpf.setTermid(sqlcovrpf1rs.getString(51));
                covrpf.setTransactionDate(sqlcovrpf1rs.getInt(52));
                covrpf.setTransactionTime(sqlcovrpf1rs.getInt(53));
                covrpf.setUser(sqlcovrpf1rs.getInt(54));
                covrpf.setLiencd(sqlcovrpf1rs.getString(55));
                covrpf.setRatingClass(sqlcovrpf1rs.getString(56));
                covrpf.setIndexationInd(sqlcovrpf1rs.getString(57));
                covrpf.setBonusInd(sqlcovrpf1rs.getString(58));
                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString(59));
                covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal(60));
                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString(61));
                covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal(62));
                covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal(63));
                covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal(64));
                covrpf.setEstMatDate01(sqlcovrpf1rs.getInt(65));
                covrpf.setEstMatDate02(sqlcovrpf1rs.getInt(66));
                covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal(67));
                covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal(68));
                covrpf.setCampaign(sqlcovrpf1rs.getString(69));
                covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal(70));
                covrpf.setRtrnyrs(sqlcovrpf1rs.getInt(71));
                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString(72));
                covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt(73));
                covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString(74));
                covrpf.setFundSplitPlan(sqlcovrpf1rs.getString(75));
                covrpf.setStatreasn(sqlcovrpf1rs.getString(76));
                covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(77));
                covrpf.setSex(sqlcovrpf1rs.getString(78));
                covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt(79));
                covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt(80));
                covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt(81));
                covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt(82));
                covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt(83));
                covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt(84));
                covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt(85));
                covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt(86));
                covrpf.setJlLsInd(sqlcovrpf1rs.getString(87));
                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(88));
                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(89));
                covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(90));
                covrpf.setReviewProcessing(sqlcovrpf1rs.getInt(91));
                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(92));
                covrpf.setCpiDate(sqlcovrpf1rs.getInt(93));
                covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt(94));
                covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt(95));
                covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt(96));
                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(97));
                covrpf.setPayrseqno(sqlcovrpf1rs.getInt(98));
                covrpf.setBappmeth(sqlcovrpf1rs.getString(99));
                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(100));
                covrpf.setLoadper(sqlcovrpf1rs.getBigDecimal(101));
                covrpf.setRateadj(sqlcovrpf1rs.getBigDecimal(102));
                covrpf.setFltmort(sqlcovrpf1rs.getBigDecimal(103));
                covrpf.setPremadj(sqlcovrpf1rs.getBigDecimal(104));
                covrpf.setAgeadj(sqlcovrpf1rs.getBigDecimal(105));
                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(106));
				covrpfSearchResult.add(covrpf);

			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrSelect, sqlcovrpf1rs);
		}
		return covrpfSearchResult;
	}

	public Covrpf getCovrRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plansuffix, String validflag) {

		Covrpf covrpf = new Covrpf();
		StringBuilder sql = new StringBuilder("SELECT * FROM ");
		sql.append(" COVRPF ");
		sql.append(" WHERE VALIDFLAG=? AND CHDRNUM=? AND CHDRCOY=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?");
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, validflag);
			ps.setString(2, chdrnum);
			ps.setString(3, chdrcoy);
			ps.setString(4, life);
			ps.setString(5, coverage);
			ps.setString(6, rider);
			ps.setInt(7, plansuffix);
			rs = ps.executeQuery();
			while (rs.next()) {
				covrpf.setUniqueNumber(rs.getLong(1));
				covrpf.setChdrcoy(rs.getObject(2) != null ? (rs.getString(2)) : "");
				covrpf.setChdrnum(rs.getObject(3) != null ? (rs.getString(3)) : "");
				covrpf.setLife(rs.getObject(4) != null ? (rs.getString(4)) : "");
				covrpf.setJlife(rs.getObject(5) != null ? (rs.getString(5)) : "");
				covrpf.setCoverage(rs.getObject(6) != null ? (rs.getString(6)) : "");
				covrpf.setRider(rs.getObject(7) != null ? (rs.getString(7)) : "");
				covrpf.setPlanSuffix(rs.getObject(8) != null ? (rs.getInt(8)) : 0);
				covrpf.setValidflag(rs.getObject(9) != null ? (rs.getString(9)) : "");
				covrpf.setTranno(rs.getObject(10) != null ? (rs.getInt(10)) : 0);
				covrpf.setCurrfrom(rs.getObject(11) != null ? (rs.getInt(11)) : 0);
				covrpf.setCurrto(rs.getObject(12) != null ? (rs.getInt(12)) : 0);
				covrpf.setStatcode(rs.getObject(13) != null ? (rs.getString(13)) : "");
				covrpf.setPstatcode(rs.getObject(14) != null ? (rs.getString(14)) : "");
				covrpf.setStatreasn(rs.getObject(15) != null ? (rs.getString(15)) : "");
				covrpf.setCrrcd(rs.getObject(16) != null ? (rs.getInt(16)) : 0);
				covrpf.setAnbAtCcd(rs.getObject(17) != null ? (rs.getInt(17)) : 0);
				covrpf.setSex(rs.getObject(18) != null ? (rs.getString(18)) : "");
				covrpf.setReptcd01(rs.getObject(19) != null ? (rs.getString(19)) : "");
				covrpf.setReptcd02(rs.getObject(20) != null ? (rs.getString(20)) : "");
				covrpf.setReptcd03(rs.getObject(21) != null ? (rs.getString(21)) : "");
				covrpf.setReptcd04(rs.getObject(22) != null ? (rs.getString(22)) : "");
				covrpf.setReptcd05(rs.getObject(23) != null ? (rs.getString(23)) : "");
				covrpf.setReptcd06(rs.getObject(24) != null ? (rs.getString(24)) : "");
				covrpf.setCrInstamt01(rs.getObject(25) != null ? (rs.getBigDecimal(25)) : BigDecimal.ZERO);
				covrpf.setCrInstamt02(rs.getObject(26) != null ? (rs.getBigDecimal(26)) : BigDecimal.ZERO);
				covrpf.setCrInstamt03(rs.getObject(27) != null ? (rs.getBigDecimal(27)) : BigDecimal.ZERO);
				covrpf.setCrInstamt04(rs.getObject(28) != null ? (rs.getBigDecimal(28)) : BigDecimal.ZERO);
				covrpf.setCrInstamt05(rs.getObject(29) != null ? (rs.getBigDecimal(29)) : BigDecimal.ZERO);
				covrpf.setPremCurrency(rs.getObject(30) != null ? (rs.getString(30)) : "");
				covrpf.setTermid(rs.getObject(31) != null ? (rs.getString(31)) : "");
				covrpf.setTransactionDate(rs.getObject(32) != null ? (rs.getInt(32)) : 0);
				covrpf.setTransactionTime(rs.getObject(33) != null ? (rs.getInt(33)) : 0);
				covrpf.setUser(rs.getObject(34) != null ? (rs.getInt(34)) : 0);
				covrpf.setStatFund(rs.getObject(35) != null ? (rs.getString(35)) : "");
				covrpf.setStatSect(rs.getObject(36) != null ? (rs.getString(36)) : "");
				covrpf.setStatSubsect(rs.getObject(37) != null ? (rs.getString(37)) : "");
				covrpf.setCrtable(rs.getObject(38) != null ? (rs.getString(38)) : "");
				covrpf.setRerateDate(rs.getObject(39) != null ? (rs.getInt(39)) : 0);
				covrpf.setPremCessDate(rs.getObject(40) != null ? (rs.getInt(40)) : 0);
				covrpf.setBenCessDate(rs.getObject(41) != null ? (rs.getInt(41)) : 0);
				covrpf.setNextActDate(rs.getObject(42) != null ? (rs.getInt(42)) : 0);
				covrpf.setRiskCessAge(rs.getObject(43) != null ? (rs.getInt(74)) : 0);
				covrpf.setPremCessAge(rs.getObject(44) != null ? (rs.getInt(44)) : 0);
				covrpf.setBenCessAge(rs.getObject(45) != null ? (rs.getInt(45)) : 0);
				covrpf.setRiskCessTerm(rs.getObject(46) != null ? (rs.getInt(46)) : 0);
				covrpf.setPremCessTerm(rs.getObject(47) != null ? (rs.getInt(47)) : 0);
				covrpf.setBenCessTerm(rs.getObject(48) != null ? (rs.getInt(48)) : 0);
				covrpf.setSumins(rs.getObject(49) != null ? (rs.getBigDecimal(49)) : BigDecimal.ZERO);
				covrpf.setSicurr(rs.getObject(50) != null ? (rs.getString(50)) : "");
				covrpf.setVarSumInsured(rs.getObject(51) != null ? (rs.getBigDecimal(51)) : BigDecimal.ZERO);
				covrpf.setMortcls(rs.getObject(52) != null ? (rs.getString(52)) : "");
				covrpf.setLiencd(rs.getObject(53) != null ? (rs.getString(53)) : "");
				covrpf.setRatingClass(rs.getObject(54) != null ? (rs.getString(54)) : "");
				covrpf.setIndexationInd(rs.getObject(55) != null ? (rs.getString(55)) : "");
				covrpf.setBonusInd(rs.getObject(56) != null ? (rs.getString(56)) : "");
				covrpf.setDeferPerdCode(rs.getObject(57) != null ? (rs.getString(57)) : "");
				covrpf.setDeferPerdAmt(rs.getObject(58) != null ? (rs.getBigDecimal(58)) : BigDecimal.ZERO);
				covrpf.setDeferPerdInd(rs.getObject(59) != null ? (rs.getString(59)) : "");
				covrpf.setTotMthlyBenefit(rs.getObject(60) != null ? (rs.getBigDecimal(60)) : BigDecimal.ZERO);
				covrpf.setEstMatValue01(rs.getObject(61) != null ? (rs.getBigDecimal(61)) : BigDecimal.ZERO);
				covrpf.setEstMatValue02(rs.getObject(62) != null ? (rs.getBigDecimal(62)) : BigDecimal.ZERO);
				covrpf.setEstMatDate01(rs.getObject(63) != null ? (rs.getInt(63)) : 0);
				covrpf.setEstMatDate02(rs.getObject(64) != null ? (rs.getInt(64)) : 0);
				covrpf.setEstMatInt01(rs.getObject(65) != null ? (rs.getBigDecimal(65)) : BigDecimal.ZERO);
				covrpf.setEstMatInt02(rs.getObject(66) != null ? (rs.getBigDecimal(66)) : BigDecimal.ZERO);
				covrpf.setCampaign(rs.getObject(67) != null ? (rs.getString(67)) : "");
				covrpf.setStatSumins(rs.getObject(68) != null ? (rs.getBigDecimal(68)) : BigDecimal.ZERO);
				covrpf.setRtrnyrs(rs.getByte(69));
				covrpf.setReserveUnitsInd(rs.getObject(70) != null ? (rs.getString(70)) : "");
				covrpf.setReserveUnitsDate(rs.getObject(71) != null ? (rs.getInt(71)) : 0);
				covrpf.setChargeOptionsInd(rs.getObject(72) != null ? (rs.getString(72)) : "");
				covrpf.setFundSplitPlan(rs.getObject(73) != null ? (rs.getString(73)) : "");
				covrpf.setPremCessAgeMth(rs.getObject(74) != null ? (rs.getShort(74)) : 0);
				covrpf.setPremCessAgeDay(rs.getObject(75) != null ? (rs.getShort(75)) : 0);
				covrpf.setPremCessTermMth(rs.getObject(76) != null ? (rs.getShort(76)) : 0);
				covrpf.setPremCessTermDay(rs.getObject(77) != null ? (rs.getShort(77)) : 0);
				covrpf.setRiskCessAgeMth(rs.getObject(78) != null ? (rs.getShort(78)) : 0);
				covrpf.setRiskCessAgeDay(rs.getObject(79) != null ? (rs.getShort(79)) : 0);
				covrpf.setRiskCessTermMth(rs.getObject(80) != null ? (rs.getShort(80)) : 0);
				covrpf.setRiskCessTermDay(rs.getObject(81) != null ? (rs.getShort(81)) : 0);
				covrpf.setJlLsInd(rs.getObject(82) != null ? (rs.getString(82)) : "");
				covrpf.setInstprem(rs.getObject(83) != null ? (rs.getBigDecimal(83)) : BigDecimal.ZERO);
				covrpf.setSingp(rs.getObject(84) != null ? (rs.getBigDecimal(84)) : BigDecimal.ZERO);
				covrpf.setRerateDate(rs.getObject(85) != null ? (rs.getInt(85)) : 0);
				//covrpf.setOldrrtdat(covrpf.getRerateDate()());
				covrpf.setRerateFromDate(rs.getObject(86) != null ? (rs.getInt(86)) : 0);
				covrpf.setBenBillDate(rs.getObject(87) != null ? (rs.getInt(87)) : 0);
				covrpf.setAnnivProcDate(rs.getObject(88) != null ? (rs.getInt(88)) : 0);
				covrpf.setConvertInitialUnits(rs.getObject(89) != null ? (rs.getInt(89)) : 0);
				covrpf.setReviewProcessing(rs.getObject(90) != null ? (rs.getInt(90)) : 0);
				covrpf.setUnitStatementDate(rs.getObject(91) != null ? (rs.getInt(91)) : 0);
				covrpf.setCpiDate(rs.getObject(92) != null ? (rs.getInt(92)) : 0);
				covrpf.setInitUnitCancDate(rs.getObject(93) != null ? (rs.getInt(93)) : 0);
				covrpf.setExtraAllocDate(rs.getObject(94) != null ? (rs.getInt(94)) : 0);
				covrpf.setInitUnitIncrsDate(rs.getObject(95) != null ? (rs.getInt(95)) : 0);
				covrpf.setCoverageDebt(rs.getObject(96) != null ? (rs.getBigDecimal(96)) : BigDecimal.ZERO);
				covrpf.setPayrseqno(rs.getObject(97) != null ? (rs.getInt(97)) : 0);
				covrpf.setBappmeth(rs.getObject(98) != null ? (rs.getString(98)) : "");
				covrpf.setZbinstprem(rs.getObject(99) != null ? (rs.getBigDecimal(99)) : BigDecimal.ZERO);
				covrpf.setZlinstprem(rs.getObject(100) != null ? (rs.getBigDecimal(100)) : BigDecimal.ZERO);
				covrpf.setUserProfile(rs.getObject(101) != null ? (rs.getString(101)) : "");
				covrpf.setJobName(rs.getObject(102) != null ? (rs.getString(102)) : "");
				covrpf.setDatime(rs.getString(103));
				covrpf.setLoadper(rs.getObject(104) != null ? (rs.getBigDecimal(104)) : BigDecimal.ZERO);
				covrpf.setRateadj(rs.getObject(105) != null ? (rs.getBigDecimal(105)) : BigDecimal.ZERO);
				covrpf.setFltmort(rs.getObject(106) != null ? (rs.getBigDecimal(106)) : BigDecimal.ZERO);
				covrpf.setPremadj(rs.getObject(107) != null ? (rs.getBigDecimal(107)) : BigDecimal.ZERO);
			}
		} catch (SQLException e) {
			LOGGER.error("getCovrLifeenqRecords()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return covrpf;
	}

	@Override
	public boolean insertCovrList(List<Covrpf> insertCovrpfList) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO COVRPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,USRPRF,JOBNM,DATIME,LOADPER,RATEADJ,FLTMORT,PREMADJ,RISKPREM,ZSTPDUTY01,ZCLSTATE,REINSTATED,TPDTYPE,PRORATEPREM) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Covrpf covr : insertCovrpfList) {
				ps.setString(1, covr.getChdrcoy());
				ps.setString(2, covr.getChdrnum());
				ps.setString(3, covr.getLife());
				ps.setString(4, covr.getJlife());
				ps.setString(5, covr.getCoverage());
				ps.setString(6, covr.getRider());
				ps.setInt(7, covr.getPlanSuffix());
				ps.setString(8, covr.getValidflag());
				ps.setInt(9, covr.getTranno());
				ps.setInt(10, covr.getCurrfrom());
				ps.setInt(11, covr.getCurrto());
				ps.setString(12, covr.getStatcode());
				ps.setString(13, covr.getPstatcode());
				ps.setString(14, covr.getStatreasn());
				ps.setInt(15, covr.getCrrcd());
				ps.setInt(16, covr.getAnbAtCcd());
				ps.setString(17, covr.getSex());
				ps.setString(18, covr.getReptcd01());
				ps.setString(19, covr.getReptcd02());
				ps.setString(20, covr.getReptcd03());
				ps.setString(21, covr.getReptcd04());
				ps.setString(22, covr.getReptcd05());
				ps.setString(23, covr.getReptcd06());
				ps.setBigDecimal(24, covr.getCrInstamt01());
				ps.setBigDecimal(25, covr.getCrInstamt02());
				ps.setBigDecimal(26, covr.getCrInstamt03());
				ps.setBigDecimal(27, covr.getCrInstamt04());
				ps.setBigDecimal(28, covr.getCrInstamt05());
				ps.setString(29, covr.getPremCurrency());
				ps.setString(30, covr.getTermid());
				ps.setInt(31, covr.getTransactionDate());
				ps.setInt(32, covr.getTransactionTime());
				ps.setInt(33, covr.getUser());
				ps.setString(34, covr.getStatFund());
				ps.setString(35, covr.getStatSect());
				ps.setString(36, covr.getStatSubsect());
				ps.setString(37, covr.getCrtable());
				ps.setInt(38, covr.getRiskCessDate());
				ps.setInt(39, covr.getPremCessDate());
				ps.setInt(40, covr.getBenCessDate());
				ps.setInt(41, covr.getNextActDate());// extActDate());
				ps.setInt(42, covr.getRiskCessAge());
				ps.setInt(43, covr.getPremCessAge());
				ps.setInt(44, covr.getBenCessAge());
				ps.setInt(45, covr.getRiskCessTerm());
				ps.setInt(46, covr.getPremCessTerm());
				ps.setInt(47, covr.getBenCessTerm());
				ps.setBigDecimal(48, covr.getSumins());
				ps.setString(49, covr.getSicurr());
				ps.setBigDecimal(50, covr.getVarSumInsured());// arSumInsured());
				ps.setString(51, covr.getMortcls());
				ps.setString(52, covr.getLiencd());
				ps.setString(53, covr.getRatingClass());// atingClass());
				ps.setString(54, covr.getIndexationInd());// ndexationInd());
				ps.setString(55, covr.getBonusInd());// onusInd());
				ps.setString(56, covr.getDeferPerdCode());// eferPerdCode());
				ps.setBigDecimal(57, covr.getDeferPerdAmt());// eferPerdAmt());
				ps.setString(58, covr.getDeferPerdInd());// eferPerdInd());
				ps.setBigDecimal(59, covr.getTotMthlyBenefit());// otMthlyBenefit());
				ps.setBigDecimal(60, covr.getEstMatValue01());// stMatValue01());
				ps.setBigDecimal(61, covr.getEstMatValue02());// stMatValue02());
				ps.setInt(62, covr.getEstMatDate01());// stMatDate01());
				ps.setInt(63, covr.getEstMatDate02());// stMatDate02());
				ps.setBigDecimal(64, covr.getEstMatInt01());// stMatInt01());
				ps.setBigDecimal(65, covr.getEstMatInt02());// stMatInt02());
				ps.setString(66, covr.getCampaign());
				ps.setBigDecimal(67, covr.getSumins());// atSumins());
				ps.setInt(68, covr.getRtrnyrs());
				ps.setString(69, covr.getReserveUnitsInd());// eserveUnitsInd());
				ps.setInt(70, covr.getReserveUnitsDate());// eserveUnitsDate());
				ps.setString(71, covr.getChargeOptionsInd());// hargeOptionsInd());
				ps.setString(72, covr.getFundSplitPlan());// undSplitPlan());
				ps.setInt(73, covr.getPremCessAgeMth());// remCessAgeMth());
				ps.setInt(74, covr.getPremCessAgeDay());// remCessAgeDay());
				ps.setInt(75, covr.getPremCessTermMth());// remCessTermMth());
				ps.setInt(76, covr.getPremCessTermDay());// remCessTermDay());
				ps.setInt(77, covr.getRiskCessAgeMth());// iskCessAgeMth());
				ps.setInt(78, covr.getRiskCessAgeDay());// iskCessAgeDay());
				ps.setInt(79, covr.getRiskCessTermMth());// iskCessTermMth());
				ps.setInt(80, covr.getRiskCessTermDay());// iskCessTermDay());
				ps.setString(81, covr.getJlLsInd());// lLsInd());
				ps.setBigDecimal(82, covr.getInstprem());
				ps.setBigDecimal(83, covr.getSingp());
				ps.setInt(84, covr.getRerateDate());// ateDate());
				ps.setInt(85, covr.getRerateFromDate());// erateFromDate());
				ps.setInt(86, covr.getBenBillDate());// enBillDate());
				ps.setInt(87, covr.getAnnivProcDate());// AnnivProcDate());
				ps.setInt(88, covr.getConvertInitialUnits());// onvertInitialUnits());
				ps.setInt(89, covr.getReviewProcessing());// ReviewProcessing());
				ps.setInt(90, covr.getUnitStatementDate());// UnitStatementDate());
				ps.setInt(91, covr.getCpiDate());
				ps.setInt(92, covr.getInitUnitCancDate());// nitUnitCancDate());
				ps.setInt(93, covr.getExtraAllocDate());// traAllocDate());
				ps.setInt(94, covr.getInitUnitIncrsDate());// nitUnitIncrsDate());
				ps.setBigDecimal(95, covr.getCoverageDebt());// overageDebt());
				ps.setLong(96, covr.getPayrseqno());
				ps.setString(97, covr.getBappmeth());
				ps.setBigDecimal(98, covr.getZbinstprem());
				ps.setBigDecimal(99, covr.getZlinstprem());
				ps.setString(100, this.getUsrprf());
				ps.setString(101, this.getJobnm());
				ps.setTimestamp(102, new Timestamp(System.currentTimeMillis()));
				
				ps.setBigDecimal(103, covr.getLoadper());
				ps.setBigDecimal(104, covr.getRateadj());
				ps.setBigDecimal(105, covr.getFltmort());
				ps.setBigDecimal(106, covr.getPremadj());
				ps.setBigDecimal(107, covr.getRiskprem()); //ILIFE-7845 
				ps.setBigDecimal(108, covr.getZstpduty01() == null ? BigDecimal.ZERO : covr.getZstpduty01());
				ps.setString(109, covr.getZclstate());
				ps.setString(110, covr.getReinstated());//ILIFE-8509
				ps.setString(111, covr.getTpdtype());
				ps.setBigDecimal(112, covr.getProrateprem() != null ? covr.getProrateprem() : BigDecimal.ZERO);
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertCovrpfList()", e); /* IJTI-1479 */
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return isInsertSuccessful;
	}

	@Override
	public boolean insertCovrRecord(Covrpf covr) {
		boolean isInsertSuccessful = false;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO COVRPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,USRPRF,JOBNM,DATIME,LOADPER,RATEADJ,FLTMORT,PREMADJ) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, covr.getChdrcoy());
			ps.setString(2, covr.getChdrnum());
			ps.setString(3, covr.getLife());
			ps.setString(4, covr.getJlife());
			ps.setString(5, covr.getCoverage());
			ps.setString(6, covr.getRider());
			ps.setInt(7, covr.getPlanSuffix());
			ps.setString(8, covr.getValidflag());
			ps.setInt(9, covr.getTranno());
			ps.setInt(10, covr.getCurrfrom());
			ps.setInt(11, covr.getCurrto());
			ps.setString(12, covr.getStatcode());
			ps.setString(13, covr.getPstatcode());
			ps.setString(14, covr.getStatreasn());
			ps.setInt(15, covr.getCrrcd());
			ps.setInt(16, covr.getAnbAtCcd());
			ps.setString(17, covr.getSex());
			ps.setString(18, covr.getReptcd01());
			ps.setString(19, covr.getReptcd02());
			ps.setString(20, covr.getReptcd03());
			ps.setString(21, covr.getReptcd04());
			ps.setString(22, covr.getReptcd05());
			ps.setString(23, covr.getReptcd06());
			ps.setBigDecimal(24, covr.getCrInstamt01());
			ps.setBigDecimal(25, covr.getCrInstamt02());
			ps.setBigDecimal(26, covr.getCrInstamt03());
			ps.setBigDecimal(27, covr.getCrInstamt04());
			ps.setBigDecimal(28, covr.getCrInstamt05());
			ps.setString(29, covr.getPremCurrency());
			ps.setString(30, covr.getTermid());
			ps.setInt(31, covr.getTransactionDate());
			ps.setInt(32, covr.getTransactionTime());
			ps.setInt(33, covr.getUser());
			ps.setString(34, covr.getStatFund());
			ps.setString(35, covr.getStatSect());
			ps.setString(36, covr.getStatSubsect());
			ps.setString(37, covr.getCrtable());
			ps.setInt(38, covr.getRiskCessDate());
			ps.setInt(39, covr.getPremCessDate());
			ps.setInt(40, covr.getBenCessDate());
			ps.setInt(41, covr.getNextActDate());// extActDate());
			ps.setInt(42, covr.getRiskCessAge());
			ps.setInt(43, covr.getPremCessAge());
			ps.setInt(44, covr.getBenCessAge());
			ps.setInt(45, covr.getRiskCessTerm());
			ps.setInt(46, covr.getPremCessTerm());
			ps.setInt(47, covr.getBenCessTerm());
			ps.setBigDecimal(48, covr.getSumins());
			ps.setString(49, covr.getSicurr());
			ps.setBigDecimal(50, covr.getVarSumInsured());// arSumInsured());
			ps.setString(51, covr.getMortcls());
			ps.setString(52, covr.getLiencd());
			ps.setString(53, covr.getRatingClass());// atingClass());
			ps.setString(54, covr.getIndexationInd());// ndexationInd());
			ps.setString(55, covr.getBonusInd());// onusInd());
			ps.setString(56, covr.getDeferPerdCode());// eferPerdCode());
			ps.setBigDecimal(57, covr.getDeferPerdAmt());// eferPerdAmt());
			ps.setString(58, covr.getDeferPerdInd());// eferPerdInd());
			ps.setBigDecimal(59, covr.getTotMthlyBenefit());// otMthlyBenefit());
			ps.setBigDecimal(60, covr.getEstMatValue01());// stMatValue01());
			ps.setBigDecimal(61, covr.getEstMatValue02());// stMatValue02());
			ps.setInt(62, covr.getEstMatDate01());// stMatDate01());
			ps.setInt(63, covr.getEstMatDate02());// stMatDate02());
			ps.setBigDecimal(64, covr.getEstMatInt01());// stMatInt01());
			ps.setBigDecimal(65, covr.getEstMatInt02());// stMatInt02());
			ps.setString(66, covr.getCampaign());
			ps.setBigDecimal(67, covr.getSumins());// atSumins());
			ps.setInt(68, covr.getRtrnyrs());
			ps.setString(69, covr.getReserveUnitsInd());// eserveUnitsInd());
			ps.setInt(70, covr.getReserveUnitsDate());// eserveUnitsDate());
			ps.setString(71, covr.getChargeOptionsInd());// hargeOptionsInd());
			ps.setString(72, covr.getFundSplitPlan());// undSplitPlan());
			ps.setInt(73, covr.getPremCessAgeMth());// remCessAgeMth());
			ps.setInt(74, covr.getPremCessAgeDay());// remCessAgeDay());
			ps.setInt(75, covr.getPremCessTermMth());// remCessTermMth());
			ps.setInt(76, covr.getPremCessTermDay());// remCessTermDay());
			ps.setInt(77, covr.getRiskCessAgeMth());// iskCessAgeMth());
			ps.setInt(78, covr.getRiskCessAgeDay());// iskCessAgeDay());
			ps.setInt(79, covr.getRiskCessTermMth());// iskCessTermMth());
			ps.setInt(80, covr.getRiskCessTermDay());// iskCessTermDay());
			ps.setString(81, covr.getJlLsInd());// lLsInd());
			ps.setBigDecimal(82, covr.getInstprem());
			ps.setBigDecimal(83, covr.getSingp());
			ps.setInt(84, covr.getRerateDate());// ateDate());
			ps.setInt(85, covr.getRerateFromDate());// erateFromDate());
			ps.setInt(86, covr.getBenBillDate());// enBillDate());
			ps.setInt(87, covr.getAnnivProcDate());// AnnivProcDate());
			ps.setInt(88, covr.getConvertInitialUnits());// onvertInitialUnits());
			ps.setInt(89, covr.getReviewProcessing());// ReviewProcessing());
			ps.setInt(90, covr.getUnitStatementDate());// UnitStatementDate());
			ps.setInt(91, covr.getCpiDate());
			ps.setInt(92, covr.getInitUnitCancDate());// nitUnitCancDate());
			ps.setInt(93, covr.getExtraAllocDate());// traAllocDate());
			ps.setInt(94, covr.getInitUnitIncrsDate());// nitUnitIncrsDate());
			ps.setBigDecimal(95, covr.getCoverageDebt());// overageDebt());
			ps.setLong(96, covr.getPayrseqno());
			ps.setString(97, covr.getBappmeth());
			ps.setBigDecimal(98, covr.getZbinstprem());
			ps.setBigDecimal(99, covr.getZlinstprem());
			ps.setString(100, this.getUsrprf());
			ps.setString(101, this.getJobnm());
			ps.setTimestamp(102, new Timestamp(System.currentTimeMillis()));
			
			ps.setBigDecimal(103, covr.getLoadper());
			ps.setBigDecimal(104, covr.getRateadj());
			ps.setBigDecimal(105, covr.getFltmort());
			ps.setBigDecimal(106, covr.getPremadj());
			ps.execute();
			isInsertSuccessful = true;
		} catch (SQLException e) {
			LOGGER.error("insertCovrpfList()", e); /* IJTI-1479 */
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return isInsertSuccessful;
	}

	public void updateCovrRecord(List<Covrpf> updateCovrpflist) {
		if (updateCovrpflist != null && updateCovrpflist.size() > 0) {
			String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG=? ,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
			try {
				for (Covrpf c : updateCovrpflist) {
					psCovrUpdate.setString(1, c.getValidflag());
					psCovrUpdate.setInt(2, c.getCurrto());
					psCovrUpdate.setString(3, getJobnm());
					psCovrUpdate.setString(4, getUsrprf());
					// psCovrUpdate.setString(5,Integer.toString( busDate));
					// //ILIFE-5538
					psCovrUpdate.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
					psCovrUpdate.setLong(6, c.getUniqueNumber());
					psCovrUpdate.addBatch();
				}
				psCovrUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateCovrRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrUpdate, null);
			}
		}
	}

	public boolean updateCovrRecord(Covrpf covrpf) {
		boolean isSuccessful = false;
		String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG=? ,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

		PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
		try {
			psCovrUpdate.setString(1, covrpf.getValidflag());
			psCovrUpdate.setInt(2, covrpf.getCurrto());
			psCovrUpdate.setString(3, covrpf.getJobName());
			psCovrUpdate.setString(4, covrpf.getUserProfile());
			// psCovrUpdate.setString(5,Integer.toString( busDate));
			// //ILIFE-5538
			psCovrUpdate.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			psCovrUpdate.setLong(6, covrpf.getUniqueNumber());

			psCovrUpdate.execute();
			isSuccessful = true;
		} catch (SQLException e) {
			isSuccessful = false;
			LOGGER.error("updateCovrRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrUpdate, null);
		}
		return isSuccessful;
	}

	/* Agcmpf Method */
	public Map<String, List<Agcmpf>> getAgcmRecords(String coy, List<String> chdrnumList) {
		StringBuilder sql = new StringBuilder();
		sql.append(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,USER_T,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG FROM AGCMPF WHERE CHDRCOY=? AND ");
		sql.append(getSqlInStr("CHDRNUM", chdrnumList));
		sql.append(" AND (VALIDFLAG = '1' OR VALIDFLAG = ' ') ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,AGNTNUM ASC ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		Map<String, List<Agcmpf>> agcmpfMap = new HashMap<String, List<Agcmpf>>();
		try {
			ps.setInt(1, Integer.parseInt(coy));
			rs = executeQuery(ps);

			while (rs.next()) {
				Agcmpf agcmpf = new Agcmpf();
				agcmpf.setUniqueNumber(rs.getLong(1));
				agcmpf.setChdrcoy(rs.getString(2));
				agcmpf.setChdrnum(rs.getString(3));
				agcmpf.setAgntnum(rs.getString(4));
				agcmpf.setLife(rs.getString(5));
				agcmpf.setJlife(rs.getString(6));
				agcmpf.setCoverage(rs.getString(7));
				agcmpf.setRider(rs.getString(8));
				agcmpf.setPlanSuffix(rs.getInt(9));
				agcmpf.setTranno(rs.getInt(10));
				agcmpf.setEfdate(rs.getInt(11));
				agcmpf.setAnnprem(rs.getBigDecimal(12));
				agcmpf.setBasicCommMeth(rs.getString(13));
				agcmpf.setInitcom(rs.getBigDecimal(14));
				agcmpf.setBascpy(rs.getString(15));
				agcmpf.setCompay(rs.getBigDecimal(16));
				agcmpf.setComern(rs.getBigDecimal(17));
				agcmpf.setSrvcpy(rs.getString(18));
				agcmpf.setScmdue(rs.getBigDecimal(19));
				agcmpf.setScmearn(rs.getBigDecimal(20));
				agcmpf.setRnwcpy(rs.getString(21));
				agcmpf.setRnlcdue(rs.getBigDecimal(22));
				agcmpf.setRnlcearn(rs.getBigDecimal(23));
				agcmpf.setAgentClass(rs.getString(24));
				agcmpf.setTermid(rs.getString(25));
				agcmpf.setTransactionDate(rs.getInt(26));
				agcmpf.setTransactionTime(rs.getInt(27));
				agcmpf.setUser(rs.getInt(28));
				agcmpf.setCrtable(rs.getString(29));
				agcmpf.setCurrfrom(rs.getInt(30));
				agcmpf.setCurrto(rs.getInt(31));
				agcmpf.setValidflag(rs.getString(32));
				agcmpf.setSeqno(rs.getInt(33));
				agcmpf.setPtdate(rs.getInt(34));
				agcmpf.setCedagent(rs.getString(35));
				agcmpf.setOvrdcat(rs.getString(36));
				agcmpf.setDormantFlag(rs.getString(37));

				String chdrnum = agcmpf.getChdrnum();
				if (agcmpfMap.containsKey(chdrnum)) {
					agcmpfMap.get(chdrnum).add(agcmpf);
				} else {
					List<Agcmpf> agcmpfSearchResult = new LinkedList<Agcmpf>();
					agcmpfSearchResult.add(agcmpf);
					agcmpfMap.put(chdrnum, agcmpfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchAgcmpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return agcmpfMap;
	}

	public boolean insertAgcmRecord(Agcmpf agcmpf) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append(
				"INSERT INTO AGCMPF(CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,USER_T,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG,USRPRF,JOBNM,DATIME) ");
		sb.append(
				"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, agcmpf.getChdrcoy());// getString(2));
			ps.setString(2, agcmpf.getChdrnum());// getString(3));
			ps.setString(3, agcmpf.getAgntnum());// getString(4));
			ps.setString(4, agcmpf.getLife());// getString(5));
			ps.setString(5, agcmpf.getJlife());// getString(6));
			ps.setString(6, agcmpf.getCoverage());// getString(7));
			ps.setString(7, agcmpf.getRider());// getString(8));
			ps.setInt(8, agcmpf.getPlanSuffix());// getInt(9));
			ps.setInt(9, agcmpf.getTranno());// getInt(10));
			ps.setInt(10, agcmpf.getEfdate());// getInt(11));
			ps.setBigDecimal(11, agcmpf.getAnnprem());// getBigDecimal(12));
			ps.setString(12, agcmpf.getBasicCommMeth());// getString(13));
			ps.setBigDecimal(13, agcmpf.getInitcom());// getBigDecimal(14));
			ps.setString(14, agcmpf.getBascpy());// getString(15));
			ps.setBigDecimal(15, agcmpf.getCompay());// getBigDecimal(16));
			ps.setBigDecimal(16, agcmpf.getComern());// getBigDecimal(17));
			ps.setString(17, agcmpf.getSrvcpy());// getString(18));
			ps.setBigDecimal(18, agcmpf.getScmdue());// getBigDecimal(19));
			ps.setBigDecimal(19, agcmpf.getScmearn());// getBigDecimal(20));
			ps.setString(20, agcmpf.getRnwcpy());// getString(21));
			ps.setBigDecimal(21, agcmpf.getRnlcdue());// getBigDecimal(22));
			ps.setBigDecimal(22, agcmpf.getRnlcearn());// getBigDecimal(23));
			ps.setString(23, agcmpf.getAgentClass());// getString(24));
			ps.setString(24, agcmpf.getTermid());// getString(25));
			ps.setInt(25, agcmpf.getTransactionDate());// getInt(26));
			ps.setInt(26, agcmpf.getTransactionTime());// getInt(27));
			ps.setInt(27, agcmpf.getUser());// getInt(28));
			ps.setString(28, agcmpf.getCrtable());// getString(29));
			ps.setInt(29, agcmpf.getCurrfrom());// getInt(30));
			ps.setInt(30, agcmpf.getCurrto());// getInt(31));
			ps.setString(31, agcmpf.getValidflag());// getString(32));
			ps.setInt(32, agcmpf.getSeqno());// getInt(33));
			ps.setInt(33, agcmpf.getPtdate());// getInt(34));
			ps.setString(34, agcmpf.getCedagent());// getString(35));
			ps.setString(35, agcmpf.getOvrdcat());// getString(36));
			ps.setString(36, agcmpf.getDormantFlag());// getString(37));
			ps.setString(37, this.getUsrprf());
			ps.setString(38, this.getJobnm());
			ps.setTimestamp(39, new Timestamp(System.currentTimeMillis()));
			ps.execute();
		} catch (SQLException e) {
			isInsertSuccessful = false;
			LOGGER.error("updateAgcmByUniqNum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

		return isInsertSuccessful;
	}

	public boolean insertAgcmRecord(List<Agcmpf> agcmppfListInst) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append(
				"INSERT INTO AGCMPF(CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,USER_T,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG,USRPRF,JOBNM,DATIME) ");
		sb.append(
				"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Agcmpf agcmpf : agcmppfListInst){
				ps.setString(1, agcmpf.getChdrcoy());// getString(2));
				ps.setString(2, agcmpf.getChdrnum());// getString(3));
				ps.setString(3, agcmpf.getAgntnum());// getString(4));
				ps.setString(4, agcmpf.getLife());// getString(5));
				ps.setString(5, agcmpf.getJlife());// getString(6));
				ps.setString(6, agcmpf.getCoverage());// getString(7));
				ps.setString(7, agcmpf.getRider());// getString(8));
				ps.setInt(8, agcmpf.getPlanSuffix());// getInt(9));
				ps.setInt(9, agcmpf.getTranno());// getInt(10));
				ps.setInt(10, agcmpf.getEfdate());// getInt(11));
				ps.setBigDecimal(11, agcmpf.getAnnprem());// getBigDecimal(12));
				ps.setString(12, agcmpf.getBasicCommMeth());// getString(13));
				ps.setBigDecimal(13, agcmpf.getInitcom());// getBigDecimal(14));
				ps.setString(14, agcmpf.getBascpy());// getString(15));
				ps.setBigDecimal(15, agcmpf.getCompay());// getBigDecimal(16));
				ps.setBigDecimal(16, agcmpf.getComern());// getBigDecimal(17));
				ps.setString(17, agcmpf.getSrvcpy());// getString(18));
				ps.setBigDecimal(18, agcmpf.getScmdue());// getBigDecimal(19));
				ps.setBigDecimal(19, agcmpf.getScmearn());// getBigDecimal(20));
				ps.setString(20, agcmpf.getRnwcpy());// getString(21));
				ps.setBigDecimal(21, agcmpf.getRnlcdue());// getBigDecimal(22));
				ps.setBigDecimal(22, agcmpf.getRnlcearn());// getBigDecimal(23));
				ps.setString(23, agcmpf.getAgentClass());// getString(24));
				ps.setString(24, agcmpf.getTermid());// getString(25));
				ps.setInt(25, agcmpf.getTransactionDate());// getInt(26));
				ps.setInt(26, agcmpf.getTransactionTime());// getInt(27));
				ps.setInt(27, agcmpf.getUser());// getInt(28));
				ps.setString(28, agcmpf.getCrtable());// getString(29));
				ps.setInt(29, agcmpf.getCurrfrom());// getInt(30));
				ps.setInt(30, agcmpf.getCurrto());// getInt(31));
				ps.setString(31, agcmpf.getValidflag());// getString(32));
				ps.setInt(32, agcmpf.getSeqno());// getInt(33));
				ps.setInt(33, agcmpf.getPtdate());// getInt(34));
				ps.setString(34, agcmpf.getCedagent());// getString(35));
				ps.setString(35, agcmpf.getOvrdcat());// getString(36));
				ps.setString(36, agcmpf.getDormantFlag());// getString(37));
				ps.setString(37, this.getUsrprf());
				ps.setString(38, this.getJobnm());
				ps.setTimestamp(39, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			isInsertSuccessful = false;
			LOGGER.error("updateAgcmByUniqNum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

		return isInsertSuccessful;
	}
	public boolean updateAgcmRecord(Agcmpf agcmpf) {
		boolean isSuccessful = true;
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE AGCMPF SET VALIDFLAG=?,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		try {
			ps.setString(1, agcmpf.getValidflag());
			ps.setInt(2, agcmpf.getCurrto());
			ps.setString(3, getJobnm());
			ps.setString(4, getUsrprf());
			ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			ps.setLong(6, agcmpf.getUniqueNumber());
			// ps.addBatch();
			ps.execute();
		} catch (SQLException e) {
			isSuccessful = false;
			LOGGER.error("updateAgcmByUniqNum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

		return isSuccessful;
	}
	public boolean updateAgcmRecord(List<Agcmpf> agcmpfListupdate) {
		boolean isSuccessful = true;
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE AGCMPF SET VALIDFLAG=?,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		try {
			for(Agcmpf agcmpf : agcmpfListupdate){
				ps.setString(1, agcmpf.getValidflag());
				ps.setInt(2, agcmpf.getCurrto());
				ps.setString(3, getJobnm());
				ps.setString(4, getUsrprf());
				ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				ps.setLong(6, agcmpf.getUniqueNumber());
				ps.addBatch();	
			}
			
			ps.executeBatch();
		} catch (SQLException e) {
			isSuccessful = false;
			LOGGER.error("updateAgcmRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

		return isSuccessful;
	}

	/* Agcmpf Method */
	/* Pcddpf Method */
	public Map<String, List<Pcddpf>> getPcddpfRecords(String coy, List<String> chdrnumList) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,SPLITC,SPLITB,VALIDFLAG,TRANNO,CURRFROM,CURRTO,USER_T,TERMID,TRTM,TRDT FROM PCDDPF WHERE CHDRCOY=? AND ");
		sb.append(getSqlInStr("CHDRNUM", chdrnumList));
		sb.append(" AND VALIDFLAG = '1' ");
		sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,AGNTNUM ASC,UNIQUE_NUMBER DESC ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		Pcddpf pcddpf = null;
		Map<String, List<Pcddpf>> pcddpfMap = new HashMap<String, List<Pcddpf>>();
		try {
			ps.setInt(1, Integer.parseInt(coy));
			rs = executeQuery(ps);

			while (rs.next()) {
				pcddpf = new Pcddpf();
				pcddpf.setUniqueNumber(rs.getLong(1));
				pcddpf.setChdrcoy(rs.getString(2).charAt(0));
				pcddpf.setChdrnum(rs.getString(3));
				pcddpf.setAgntNum(rs.getString(4));
				pcddpf.setSplitC(rs.getBigDecimal(5));
				pcddpf.setSplitB(rs.getBigDecimal(6));
				pcddpf.setValidFlag(rs.getString(7).charAt(0));
				pcddpf.setTranNo(rs.getInt(8));
				pcddpf.setCurrFrom(rs.getInt(9));
				pcddpf.setCurrTo(rs.getInt(10));
				pcddpf.setUserT(rs.getInt(11));
				pcddpf.setTermId(rs.getString(12));
				pcddpf.setTrtM(rs.getInt(13));
				pcddpf.setTrtD(rs.getInt(14));

				String chdrnum = pcddpf.getChdrnum();
				if (pcddpfMap.containsKey(chdrnum)) {
					pcddpfMap.get(chdrnum).add(pcddpf);
				} else {
					List<Pcddpf> pcddpfSearchResult = new LinkedList<Pcddpf>();
					pcddpfSearchResult.add(pcddpf);
					pcddpfMap.put(chdrnum, pcddpfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchPcddpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return pcddpfMap;
	}
	/* Pcddpf Method */
	/* Lextpf Method */
//	public Map<String, List<Lextpf>> getLextRecords(String coy,List<String>chdrnum){
//	 StringBuilder sb = new StringBuilder("");
//	 sb.append("SELECT L.CURRFROM, L.OPCDA, L.OPPC, L.AGERATE, L.INSPRM,L.ECESTRM, C.UNIQUE_NUMBER ");
//	 sb.append("FROM LEXTPF L,COVRPF C WHERE L.VALIDFLAG = '1' AND L.CHDRCOY=C.CHDRCOY AND L.CHDRNUM = C.CHDRNUM AND L.LIFE=C.LIFE AND L.COVERAGE=C.COVERAGE AND L.RIDER=C.RIDER AND ");
//	 sb.append(getSqlInLong("C.UNIQUE_NUMBER", covrUQ));
//	 sb.append(" ORDER BY L.CHDRCOY ASC, L.CHDRNUM ASC, L.LIFE ASC, L.COVERAGE ASC, L.RIDER ASC, L.SEQNBR ASC, L.UNIQUE_NUMBER DESC ");
//	
//	 PreparedStatement psLextSelect = getPrepareStatement(sb.toString());
//	 ResultSet sqllextpf1rs = null;
//	 Map<Long, List<Lextpf>> lextpfSearchResult = new HashMap<Long,
//	 List<Lextpf>>();
//	 try {
//	 sqllextpf1rs = executeQuery(psLextSelect);
//	 while (sqllextpf1rs.next()) {
//	 Lextpf lextpf = new Lextpf();
//	 lextpf.setCurrfrom(sqllextpf1rs.getInt(1));
//	 lextpf.setOpcda(sqllextpf1rs.getString(2));
//	 lextpf.setOppc(sqllextpf1rs.getBigDecimal(3));
//	 lextpf.setAgerate(sqllextpf1rs.getInt(4));
//	 lextpf.setInsprm(sqllextpf1rs.getInt(5));
//	 lextpf.setExtCessTerm(sqllextpf1rs.getInt(6));
//	 long uniqueNumber = sqllextpf1rs.getLong(7);
//	 if (lextpfSearchResult.containsKey(uniqueNumber)) {
//	 lextpfSearchResult.get(uniqueNumber).add(lextpf);
//	 } else {
//	 List<Lextpf> lextpfList = new ArrayList<Lextpf>();
//	 lextpfList.add(lextpf);
//	 lextpfSearchResult.put(uniqueNumber, lextpfList);
//	 }
//	 }
//	
//	 } catch (SQLException e) {
//	 LOGGER.error("searchLextRecord()" + e);
//	 throw new SQLRuntimeException(e);
//	 } finally {
//	 close(psLextSelect, sqllextpf1rs);
//	 }
//	 return lextpfSearchResult;
//	
//	 }
	/* Lextpf Method */
	 public Agcmpf getAgcmpf(Agcmpf agcm) {
	        StringBuilder sql = new StringBuilder();
	        Agcmpf agcmpf = new Agcmpf();
	        sql.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,USER_T,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG FROM AGCMPF WHERE CHDRCOY=? AND ");
	        sql.append("CHDRNUM =? AND AGNTNUM =? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX =? AND VALIDFLAG='1' ");
	        sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,SEQNO ASC, AGNTNUM ASC,UNIQUE_NUMBER ASC ");
	        PreparedStatement ps = getPrepareStatement(sql.toString());
	        ResultSet rs = null;
	        Map<String, List<Agcmpf>> agcmpfMap = new HashMap<String, List<Agcmpf>>();
	        try {
	            ps.setInt(1, Integer.parseInt(agcm.getChdrcoy()));
	            ps.setString(2, agcm.getChdrnum());
	            ps.setString(3, agcm.getAgntnum());
	            ps.setString(4, agcm.getLife());
	            ps.setString(5, agcm.getCoverage());
	            ps.setString(6, agcm.getRider());
	            ps.setString(7, String.valueOf(agcm.getPlanSuffix()));

	            rs = executeQuery(ps);

	            if(rs.next()) {
	                agcmpf.setUniqueNumber(rs.getLong(1));
	                agcmpf.setChdrcoy(rs.getString(2));
	                agcmpf.setChdrnum(rs.getString(3));
	                agcmpf.setAgntnum(rs.getString(4));
	                agcmpf.setLife(rs.getString(5));
	                agcmpf.setJlife(rs.getString(6));
	                agcmpf.setCoverage(rs.getString(7));
	                agcmpf.setRider(rs.getString(8));
	                agcmpf.setPlanSuffix(rs.getInt(9));
	                agcmpf.setTranno(rs.getInt(10));
	                agcmpf.setEfdate(rs.getInt(11));
	                agcmpf.setAnnprem(rs.getBigDecimal(12));
	                agcmpf.setBasicCommMeth(rs.getString(13));
	                agcmpf.setInitcom(rs.getBigDecimal(14));
	                agcmpf.setBascpy(rs.getString(15));
	                agcmpf.setCompay(rs.getBigDecimal(16));
	                agcmpf.setComern(rs.getBigDecimal(17));
	                agcmpf.setSrvcpy(rs.getString(18));
	                agcmpf.setScmdue(rs.getBigDecimal(19));
	                agcmpf.setScmearn(rs.getBigDecimal(20));
	                agcmpf.setRnwcpy(rs.getString(21));
	                agcmpf.setRnlcdue(rs.getBigDecimal(22));
	                agcmpf.setRnlcearn(rs.getBigDecimal(23));
	                agcmpf.setAgentClass(rs.getString(24));
	                agcmpf.setTermid(rs.getString(25));
	                agcmpf.setTransactionDate(rs.getInt(26));
	                agcmpf.setTransactionTime(rs.getInt(27));
	                agcmpf.setUser(rs.getInt(28));
	                agcmpf.setCrtable(rs.getString(29));
	                agcmpf.setCurrfrom(rs.getInt(30));
	                agcmpf.setCurrto(rs.getInt(31));
	                agcmpf.setValidflag(rs.getString(32));
	                agcmpf.setSeqno(rs.getInt(33));
	                agcmpf.setPtdate(rs.getInt(34));
	                agcmpf.setCedagent(rs.getString(35));
	                agcmpf.setOvrdcat(rs.getString(36));
	                agcmpf.setDormantFlag(rs.getString(37));

	                String chdrnum = agcmpf.getChdrnum();
	                if (agcmpfMap.containsKey(chdrnum)) {
	                    agcmpfMap.get(chdrnum).add(agcmpf);
	                } else {
	                    List<Agcmpf> agcmpfSearchResult = new LinkedList<Agcmpf>();
	                    agcmpfSearchResult.add(agcmpf);
	                    agcmpfMap.put(chdrnum, agcmpfSearchResult);
	                }
	            }

	        } catch (SQLException e) {
	        	LOGGER.error("searchAgcmpf()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
	        return agcmpf;
	    }
}