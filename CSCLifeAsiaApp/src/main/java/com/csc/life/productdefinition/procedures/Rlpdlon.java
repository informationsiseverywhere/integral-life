/*
 * File: Rlpdlon.java
 * Date: 30 August 2009 2:13:16
 * Author: Quipoz Limited
 * 
 * Class transformed from RLPDLON.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.Iterator;
import java.util.List;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Th623rec;
import com.csc.life.regularprocessing.procedures.Crtloan;
import com.csc.life.regularprocessing.recordstructures.Crtloanrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 * REMARKS.
 *
 * Overview.
 * =========
 * This subroutine is to handle the Advance Premium Deposit (APA)
 * processing.
 * Depending on the function passing in it perform the following :
 * If T5645 item not set then BOMB.
 *
 * FUNCTION
 * ========
 * When RLPDLON-FUNCTION = INFO
 *    1. Read the ACMV record to retrieve the balance of advance premium
 *       deposit.
 * When RLPDLON-FUNCTION = INSR
 *    1. Check based on TH623 to post the additional premium suspense
 *       into advance premium deposit account. The check should depends
 *       on RLPDLON-VALIDATE. Default is 'yes'.
 *    2. Call 'CRTLOAN' to create the LOAN record (type 'D' for advance
 *       premium) and write and APL ACMV record.
 *    3. If RLPDLON-PSTW = 'NPSTW' then not ACMV positing.
 *       If RLPDLON-PSTW = 'PSLN'  then post principal loan only.
 *       If RLPDLON-PSTW = 'PSTW'  then proceed ACMV positing (DEFAULT).
 * When RLPDLON-FUNCTION = DELTS
 *    1. Delete LOAN record (type 'D').
 *    2. Knock-off the amount in advance premium and put the amount into
 *       premium suspense.
 *    3. If RLPDLON-PSTW = 'NPSTW' then not ACMV positing.
 *       If RLPDLON-PSTW = 'PSTW'  then proceed ACMV positing (DEFAULT).
 *
 * STATUZ
 * ======
 * '****' - O-K
 * 'FUNC' - Invalid function.
 * 'IVDE' - Invalid data entered.
 *
 ***********************************************************************
 *                                                                     *
 * </pre>
 */
public class Rlpdlon extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("RLPDLON");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final String wsaaLoantype = "D";
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaT5645Sacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sacstyp = new FixedLengthStringData(2);
	/* FORMATS */
	private static final String itdmrec = "ITEMREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String payrrec = "PAYRREC";
	private static final String acblenqrec = "ACBLREC";
	/* TABLES */
	private static final String th623 = "TH623";
	private static final String t5645 = "T5645";
	/* ERRORS */
	private static final String g450 = "G450";
	private static final String h134 = "H134";

	private FixedLengthStringData wsaaTh623Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh623Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTh623Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(5).isAPartOf(wsaaTh623Key, 3, FILLER).init(SPACES);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private FixedLengthStringData wsaaRldgXtra = new FixedLengthStringData(4).isAPartOf(wsaaRldgacct, 10);

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlPrefix = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Th623rec th623rec = new Th623rec();
	private T5645rec t5645rec = new T5645rec();
	private Crtloanrec crtloanrec = new Crtloanrec();
	private Cashedrec cashedrec = new Cashedrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Chdrpf chdrenq;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	boolean wsaaEof = false;

	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private List<Acblpf> acblpfList;
	private Acblpf acblpf = null;
	boolean endOfFile ;
	Iterator<Acblpf> iterator ;

	//ILIFE-7107
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Payrpf payrpf = null;

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090
	}

	public Rlpdlon() {
		super();
	}



	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		rlpdlonrec.rec = convertAndSetParam(rlpdlonrec.rec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

	protected void mainlineStart010()
	{
		initialize1000();
		if ((null == chdrenq)
				&& isEQ(rlpdlonrec.function, varcom.info)){
			return ;
		}
		/* Exit the prgroam if an item was not found on TH623              */
		if (wsaaEof == true) {
			return ;
		}
		if (isEQ(rlpdlonrec.function, varcom.info)){
			checkApa2000();
		}
		else if (isEQ(rlpdlonrec.function, varcom.insr)){
			insrApa3000();
		}
		else if (isEQ(rlpdlonrec.function, varcom.delt)){
			deltApa4000();
			rlpdlonrec.transeq.set(wsaaSequenceNo);
		}
		else{
			rlpdlonrec.statuz.set("FUNC");
		}
	}

	protected void mainlineExit010()
	{
		exitProgram();
	}

	protected void initialize1000()
	{
		try {
			init1010();
			retreiveHeader1020();
			readContractPayer1030();
			readAccRuleTable1040();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void init1010()
	{
		wsaaAmount.set(ZERO);
		wsaaSequenceNo.set(ZERO);
		rlpdlonrec.processed.set("Y");
		wsaaSequenceNo.set(rlpdlonrec.transeq);
		rlpdlonrec.statuz.set(varcom.oK);
		varcom.vrcmTranid.set(rlpdlonrec.tranid);
		endOfFile = false;
		wsaaEof = false;
	}

	/**
	 * <pre>
	 * Read CHDRENQ in order to obtain the contract header.
	 * </pre>
	 */
	protected void retreiveHeader1020()
	{
		/* Read TH623 using Contract Type                                  */

		chdrenq = chdrpfDAO.getChdrenqRecord(rlpdlonrec.chdrcoy.toString(),rlpdlonrec.chdrnum.toString());

		if (null == chdrenq) {
			if (isEQ(rlpdlonrec.function, varcom.info)) {
				goTo(GotoLabel.exit1090);
			}

			//syserrrec.statuz.set(chdrenqIO.getStatuz());
			//syserrrec.params.set(chdrenqIO.getParams());
			//dbError580();
		}

		readTh6233100();
	}

	/**
	 * <pre>
	 * Read the PAYR file for billing information.
	 * </pre>
	 */
	protected void readContractPayer1030()
	{
		//ILIFE-7107
		payrpf = payrpfDAO.getpayrRecord(chdrenq.getChdrcoy().toString(),chdrenq.getChdrnum(),1);/* IJTI-1523 */
		if(payrpf == null){
			syserr570();
		}
	}

	/**
	 * <pre>
	 * Read the Program  table T5645 for the Financial Accounting
	 * Rules for the transaction.
	 * </pre>
	 */
	protected void readAccRuleTable1040()
	{

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(rlpdlonrec.chdrcoy.toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf = itempfDAO. getItempfRecord(itempf);
		if(null != itempf)
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

	protected void checkApa2000()
	{
		readAcbl2010();
		readPrincipalApa2020();
		readInterestAccruel2030();
	}

	/**
	 * <pre>
	 * Take the SACS CODE and the SACS TYPE from the T5645 line 1 and
	 * read all records on ACBLENQ that have matching codes. The
	 * key is Company, Contract No. and Trans. No. (descending).
	 * </pre>
	 */
	protected void readAcbl2010()
	{
		//acblenqIO.setParams(SPACES);
		wsaaRldgacct.set(SPACES);
		rlpdlonrec.currency.set(SPACES);
		rlpdlonrec.prmdepst.set(ZERO);
		if (isEQ(t5645rec.sacscode02, SPACES)
				|| isEQ(t5645rec.sacstype02, SPACES)
				|| isEQ(t5645rec.sacscode05, SPACES)
				|| isEQ(t5645rec.sacstype05, SPACES)) {
			syserrrec.statuz.set(g450);
			dbError580();
		}
	}

	protected void readPrincipalApa2020()
	{

		wsaaRldgChdrnum.set(rlpdlonrec.chdrnum);
		wsaaRldgLoanno.set("00");
		wsaaT5645Sacscode.set(t5645rec.sacscode02);
		wsaaT5645Sacstyp.set(t5645rec.sacstype02);
		acblpfList = acblpfDAO.getAcblenqRecord(rlpdlonrec.chdrcoy.toString(),wsaaRldgacct.toString(),t5645rec.sacscode02.toString(),t5645rec.sacstype02.toString());


		while (!(endOfFile == true)) {
			processAcbls2100();
		}

	}

	protected void readInterestAccruel2030()
	{

		acblpfList.clear();
		endOfFile = false;
		wsaaRldgChdrnum.set(rlpdlonrec.chdrnum);
		wsaaRldgLoanno.set("00");
		wsaaT5645Sacscode.set(t5645rec.sacscode05);
		wsaaT5645Sacstyp.set(t5645rec.sacstype05);
		acblpfList = acblpfDAO.getAcblenqRecord(rlpdlonrec.chdrcoy.toString(),wsaaRldgacct.toString(),t5645rec.sacscode05.toString(),t5645rec.sacstype05.toString());

		while (!(endOfFile == true)) {
			processAcbls2100();
		}

	}

	protected void processAcbls2100()
	{


		iterator = acblpfList.iterator();

		if(iterator.hasNext())
		{
			acblpf = iterator.next();
			wsaaRldgacct.set(acblpf.getRldgacct());	
		}
		else{
			endOfFile = true;
			return;	
		}

		/* Check that the ACBL balance is not Zero*/
		if (isEQ(acblpf.getSacscurbal(), ZERO)) {
			if(iterator.hasNext())
				acblpf = iterator.next();
			else
			{endOfFile = true;
			return ;
			}
		}
		/* Negate the Account balances if it is negative (for the*/
		/* case of Cash accounts).*/
		if (isLT(acblpf.getSacscurbal(), 0)) {
			setPrecision(acblpf.getSacscurbal(), 2);
			acblpf.setSacscurbal(mult(acblpf.getSacscurbal(), (-1)).getbigdata());
		}
		rlpdlonrec.currency.set(acblpf.getOrigcurr());
		rlpdlonrec.prmdepst.set(acblpf.getSacscurbal().toString() + rlpdlonrec.prmdepst);
		if(iterator.hasNext())
			acblpf = iterator.next();
		else
			endOfFile = true;
	}

	protected void insrApa3000()
	{
		validate3010();
	}

	protected void validate3010()
	{
		if (isLT(rlpdlonrec.prmdepst, ZERO)) {
			rlpdlonrec.statuz.set("IVDE");
			return ;
		}
		if (isEQ(rlpdlonrec.prmdepst, ZERO)) {
			return ;
		}
		rlpdlonrec.processed.set("Y");
		if (isNE(rlpdlonrec.validate, "N")) {
			/*    PERFORM 3100-READ-TH623                                   */
			validateRules3200();
			if (isEQ(rlpdlonrec.processed, "N")) {
				return ;
			}
		}
		if (isEQ(t5645rec.sacscode01, SPACES)
				|| isEQ(t5645rec.sacstype01, SPACES)
				|| isEQ(t5645rec.sacscode02, SPACES)
				|| isEQ(t5645rec.sacstype02, SPACES)) {
			syserrrec.statuz.set(g450);
			dbError580();
		}
		/* Set up linkage to CRTLOAN, which will write LOAN and ACMV record*/
		crtloanrec.function.set(rlpdlonrec.pstw);
		crtloanrec.chdrcoy.set(chdrenq.getChdrcoy());
		crtloanrec.chdrnum.set(chdrenq.getChdrnum());
		crtloanrec.cnttype.set(chdrenq.getCnttype());
		crtloanrec.occdate.set(chdrenq.getOccdate());
		crtloanrec.loantype.set(wsaaLoantype);
		crtloanrec.cntcurr.set(rlpdlonrec.currency);
		crtloanrec.billcurr.set(payrpf.getBillcurr());
		crtloanrec.tranno.set(rlpdlonrec.tranno);
		crtloanrec.longdesc.set(rlpdlonrec.longdesc);
		crtloanrec.effdate.set(rlpdlonrec.effdate);
		crtloanrec.authCode.set(rlpdlonrec.authCode);
		crtloanrec.language.set(rlpdlonrec.language);
		crtloanrec.batchkey.set(rlpdlonrec.batchkey);
		crtloanrec.outstamt.set(rlpdlonrec.prmdepst);
		crtloanrec.cbillamt.set(rlpdlonrec.prmdepst);
		crtloanrec.sacscode01.set(t5645rec.sacscode01);
		crtloanrec.sacstyp01.set(t5645rec.sacstype01);
		crtloanrec.glcode01.set(t5645rec.glmap01);
		crtloanrec.glsign01.set(t5645rec.sign01);
		crtloanrec.sacscode02.set(t5645rec.sacscode02);
		crtloanrec.sacstyp02.set(t5645rec.sacstype02);
		crtloanrec.glcode02.set(t5645rec.glmap02);
		crtloanrec.glsign02.set(t5645rec.sign02);
		crtloanrec.sacscode03.set(t5645rec.sacscode03);
		crtloanrec.sacstyp03.set(t5645rec.sacstype03);
		crtloanrec.glcode03.set(t5645rec.glmap03);
		crtloanrec.glsign03.set(t5645rec.sign03);
		crtloanrec.sacscode04.set(t5645rec.sacscode04);
		crtloanrec.sacstyp04.set(t5645rec.sacstype04);
		crtloanrec.glcode04.set(t5645rec.glmap04);
		crtloanrec.glsign04.set(t5645rec.sign04);
		callProgram(Crtloan.class, crtloanrec.crtloanRec);
		if (isNE(crtloanrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(crtloanrec.statuz);
			syserrrec.params.set(crtloanrec.crtloanRec);
			dbError580();
		}
		rlpdlonrec.loanno.set(crtloanrec.loanno);
	}

	protected void readTh6233100()
	{
		setup3110();
	}

	protected void setup3110()
	{

		wsaaTh623Cnttype.set(chdrenq.getCnttype());
		itempfList = itempfDAO.getItdmByFrmdate(rlpdlonrec.chdrcoy.toString(),"th623",wsaaTh623Key.toString(),rlpdlonrec.effdate.toInt()); 

		if (itempfList.size() <1 || null == itempfList) {
			wsaaEof = true;
			return ;
		}
		else {
			th623rec.th623Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}


		/* IF ITDM-ITEMCOY             NOT = RLPDLON-CHDRCOY            */
		/* OR ITDM-ITEMTABL            NOT = TH623                      */
		/* OR ITDM-ITEMITEM            NOT = WSAA-TH623-KEY             */
		/* OR ITDM-STATUZ              = ENDP                           */
		/*    MOVE SPACES              TO ITDM-DATA-AREA                */
		/*    MOVE RLPDLON-CHDRCOY     TO ITDM-ITEMCOY                  */
		/*    MOVE TH623               TO ITDM-ITEMTABL                 */
		/*    MOVE '***'               TO WSAA-TH623-CNTTYPE            */
		/*    MOVE WSAA-TH623-KEY      TO ITDM-ITEMITEM                 */
		/*    MOVE RLPDLON-EFFDATE     TO ITDM-ITMFRM                   */
		/*    MOVE BEGN                TO ITDM-FUNCTION                 */
		/*    MOVE ITDMREC             TO ITDM-FORMAT                   */
		/*    CALL 'ITDMIO'            USING ITDM-PARAMS                */
		/*    IF ITDM-STATUZ           NOT = O-K AND ENDP               */
		/*        MOVE ITDM-STATUZ     TO SYSR-STATUZ                   */
		/*        MOVE ITDM-PARAMS     TO SYSR-PARAMS                   */
		/*        PERFORM 570-SYSERR                                    */
		/*    END-IF                                                    */
		/*    IF ITDM-ITEMCOY          NOT = RLPDLON-CHDRCOY            */
		/*    OR ITDM-ITEMTABL         NOT = TH623                      */
		/*    OR ITDM-ITEMITEM         NOT = WSAA-TH623-KEY             */
		/*    OR ITDM-STATUZ           = ENDP                           */
		/*       MOVE ITDM-STATUZ      TO SYSR-STATUZ                   */
		/*       MOVE ITDM-PARAMS      TO SYSR-PARAMS                   */
		/*    END-IF                                                    */
		/* END-IF.                                                      */

	}

	protected void validateRules3200()
	{
		/*START*/
		compute(wsaaAmount, 2).set(mult(payrpf.getSinstamt06(), th623rec.noofinst));
		if (isEQ(th623rec.noofinst, ZERO)
				&& isNE(th623rec.prmdepst, ZERO)
				&& isLT(rlpdlonrec.prmdepst, th623rec.prmdepst)) {
			rlpdlonrec.processed.set("N");
		}
		else {
			if (isNE(th623rec.noofinst, ZERO)
					&& isEQ(th623rec.prmdepst, ZERO)
					&& isLT(rlpdlonrec.prmdepst, wsaaAmount)) {
				rlpdlonrec.processed.set("N");
			}
			else {
				if (isNE(th623rec.noofinst, ZERO)
						&& isNE(th623rec.prmdepst, ZERO)
						&& (isLT(rlpdlonrec.prmdepst, wsaaAmount)
								|| isLT(rlpdlonrec.prmdepst, th623rec.prmdepst))) {
					rlpdlonrec.processed.set("N");
				}
			}
		}
		/*EXIT*/
	}

	protected void deltApa4000()
	{
		updateLoanRec4010();
		apaInterest4020();
		apaPrincipal4030();
		updateSuspense4040();
	}

	protected void updateLoanRec4010()
	{
		if (isEQ(t5645rec.sacscode05, SPACES)
				|| isEQ(t5645rec.sacstype05, SPACES)
				|| isEQ(t5645rec.sacscode06, SPACES)
				|| isEQ(t5645rec.sacstype06, SPACES)
				|| isEQ(t5645rec.sacscode07, SPACES)
				|| isEQ(t5645rec.sacstype07, SPACES)) {
			syserrrec.statuz.set(g450);
			dbError580();
		}
		initialize(cashedrec.cashedRec);
		cashedrec.docamt.set(ZERO);
		cashedrec.contot.set(ZERO);
		cashedrec.acctamt.set(ZERO);
		cashedrec.dissrate.set(ZERO);
		cashedrec.function.set("RESID");
		cashedrec.doctCompany.set(fsupfxcpy.chdr);
		cashedrec.doctNumber.set(chdrenq.getChdrnum());
		cashedrec.chdrpfx.set(fsupfxcpy.chdr);
		cashedrec.chdrcoy.set(chdrenq.getChdrcoy());
		cashedrec.chdrnum.set(chdrenq.getChdrnum());
		cashedrec.batchkey.set(rlpdlonrec.batchkey);
		cashedrec.doctkey.set(rlpdlonrec.doctkey);
		cashedrec.trancode.set(rlpdlonrec.authCode);
		cashedrec.tranid.set(rlpdlonrec.tranid);
		cashedrec.tranno.set(rlpdlonrec.tranno);
		cashedrec.transeq.set(wsaaSequenceNo);
		cashedrec.trandesc.set(rlpdlonrec.longdesc);
		cashedrec.trandate.set(rlpdlonrec.effdate);
		wsaaTrankey.set(SPACES);
		wsaaTranPrefix.set(fsupfxcpy.chdr);
		wsaaTranCompany.set(chdrenq.getChdrcoy());
		wsaaTranEntity.set(chdrenq.getChdrnum());
		cashedrec.trankey.set(wsaaTrankey);
	}

	/**
	 * <pre>
	 * Update APA interest account.
	 * </pre>
	 */
	protected void apaInterest4020()
	{
		/*    IF RLPDLON-SACSTYPE         = T5645-SACSTYPE-06*/
		/*       MOVE RLPDLON-PRMDEPST    TO CSHD-DOCAMT*/
		/*       COMPUTE CSHD-DOCAMT = RLPDLON-PRMDEPST * -1*/
		/*       GO TO 4030-APA-PRINCIPAL*/
		/*    END-IF.*/
		wsaaGenlkey.set(SPACES);
		wsaaGlCompany.set(chdrenq.getChdrcoy());
		wsaaGlCurrency.set(SPACES);
		wsaaGlMap.set(t5645rec.glmap05);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.sacscode.set(t5645rec.sacscode05);
		cashedrec.sacstyp.set(t5645rec.sacstype05);
		cashedrec.sign.set(t5645rec.sign05);
		cashedrec.origccy.set(rlpdlonrec.currency);
		/*    MOVE RLPDLON-PRMDEPST       TO CSHD-ORIGAMT.*/
		cashedrec.language.set(rlpdlonrec.language);
		cashedrec.origamt.set(rlpdlonrec.prmdepst);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			syserrrec.params.set(cashedrec.cashedRec);
			dbError580();
		}
	}

	/**
	 * <pre>
	 *    MOVE CSHD-DOCAMT            TO RLPDLON-PRMDEPST.
	 * Update APA principal account.
	 * </pre>
	 */
	protected void apaPrincipal4030()
	{
		/*    IF RLPDLON-SACSTYPE         = T5645-SACSTYPE-05*/
		/*       GO TO 4040-UPDATE-SUSPENSE*/
		/*    END-IF.*/
		if (isEQ(cashedrec.docamt, ZERO)) {
			return ;
		}
		wsaaGenlkey.set(SPACES);
		wsaaGlCompany.set(chdrenq.getChdrcoy());
		wsaaGlCurrency.set(SPACES);
		wsaaGlMap.set(t5645rec.glmap06);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.sacscode.set(t5645rec.sacscode06);
		cashedrec.sacstyp.set(t5645rec.sacstype06);
		cashedrec.sign.set(t5645rec.sign06);
		cashedrec.origccy.set(rlpdlonrec.currency);
		/*    MOVE CSHD-DOCAMT            TO CSHD-ORIGAMT.*/
		cashedrec.language.set(rlpdlonrec.language);
		cashedrec.origamt.set(cashedrec.docamt);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			syserrrec.params.set(cashedrec.cashedRec);
			dbError580();
		}
	}

	protected void updateSuspense4040()
	{
		wsaaSequenceNo.set(cashedrec.transeq);
		if (isEQ(rlpdlonrec.pstw, "NPSTW")) {
			return ;
		}
		writeAplAcmv4100();
		/*EXIT*/
	}

	protected void writeAplAcmv4100()
	{
		start4110();
	}

	protected void start4110()
	{
		lifrtrnrec.lifrtrnRec.set(SPACES);
		cashedrec.chdrpfx.set(fsupfxcpy.chdr);
		lifrtrnrec.batccoy.set(chdrenq.getChdrcoy());
		lifrtrnrec.rldgcoy.set(chdrenq.getChdrcoy());
		lifrtrnrec.genlcoy.set(chdrenq.getChdrcoy());
		lifrtrnrec.batckey.set(rlpdlonrec.batchkey);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.crate.set(ZERO);
		lifrtrnrec.acctamt.set(ZERO);
		lifrtrnrec.user.set(ZERO);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.transactionTime.set(rlpdlonrec.time);
		lifrtrnrec.transactionDate.set(rlpdlonrec.date_var);
		lifrtrnrec.trandesc.set(rlpdlonrec.longdesc);
		lifrtrnrec.origamt.set(rlpdlonrec.prmdepst);
		/* MOVE CHDRENQ-CHDRNUM        TO LIFR-RDOCNUM*/
		/*                                LIFR-RLDGACCT*/
		lifrtrnrec.rdocnum.set(rlpdlonrec.doctNumber);
		lifrtrnrec.rldgacct.set(chdrenq.getChdrnum());
		lifrtrnrec.tranref.set(chdrenq.getChdrnum());
		wsaaSequenceNo.add(1);
		lifrtrnrec.jrnseq.set(wsaaSequenceNo);
		lifrtrnrec.tranno.set(rlpdlonrec.tranno);
		lifrtrnrec.origcurr.set(rlpdlonrec.currency);
		lifrtrnrec.effdate.set(rlpdlonrec.effdate);
		lifrtrnrec.substituteCode[1].set(chdrenq.getCnttype());
		lifrtrnrec.sacscode.set(t5645rec.sacscode07);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype07);
		lifrtrnrec.glsign.set(t5645rec.sign07);
		lifrtrnrec.glcode.set(t5645rec.glmap07);
		lifrtrnrec.contot.set(t5645rec.cnttot07);
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifrtrnrec.statuz);
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserr570();
		}
	}

	protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rlpdlonrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}

	protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rlpdlonrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}
}
