package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr642screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 7, 16, 1, 2, 12, 3, 21}; 
	public static int maxRecords = 5;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {10, 8, 4, 5, 6, 7, 1, 2, 3}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {14, 17, 4, 70}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr642ScreenVars sv = (Sr642ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr642screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr642screensfl, 
			sv.Sr642screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr642ScreenVars sv = (Sr642ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr642screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr642ScreenVars sv = (Sr642ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr642screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr642screensflWritten.gt(0))
		{
			sv.sr642screensfl.setCurrentIndex(0);
			sv.Sr642screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr642ScreenVars sv = (Sr642ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr642screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr642ScreenVars screenVars = (Sr642ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.pgmnam.setFieldName("pgmnam");
				screenVars.seqno.setFieldName("seqno");
				screenVars.zmedtyp.setFieldName("zmedtyp");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.paidby.setFieldName("paidby");
				screenVars.exmcode.setFieldName("exmcode");
				screenVars.name.setFieldName("name");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.zmedfee.setFieldName("zmedfee");
				screenVars.invref.setFieldName("invref");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.pgmnam.set(dm.getField("pgmnam"));
			screenVars.seqno.set(dm.getField("seqno"));
			screenVars.zmedtyp.set(dm.getField("zmedtyp"));
			screenVars.life.set(dm.getField("life"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.paidby.set(dm.getField("paidby"));
			screenVars.exmcode.set(dm.getField("exmcode"));
			screenVars.name.set(dm.getField("name"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.zmedfee.set(dm.getField("zmedfee"));
			screenVars.invref.set(dm.getField("invref"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr642ScreenVars screenVars = (Sr642ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.pgmnam.setFieldName("pgmnam");
				screenVars.seqno.setFieldName("seqno");
				screenVars.zmedtyp.setFieldName("zmedtyp");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.paidby.setFieldName("paidby");
				screenVars.exmcode.setFieldName("exmcode");
				screenVars.name.setFieldName("name");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.zmedfee.setFieldName("zmedfee");
				screenVars.invref.setFieldName("invref");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("pgmnam").set(screenVars.pgmnam);
			dm.getField("seqno").set(screenVars.seqno);
			dm.getField("zmedtyp").set(screenVars.zmedtyp);
			dm.getField("life").set(screenVars.life);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("paidby").set(screenVars.paidby);
			dm.getField("exmcode").set(screenVars.exmcode);
			dm.getField("name").set(screenVars.name);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("zmedfee").set(screenVars.zmedfee);
			dm.getField("invref").set(screenVars.invref);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr642screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr642ScreenVars screenVars = (Sr642ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.pgmnam.clearFormatting();
		screenVars.seqno.clearFormatting();
		screenVars.zmedtyp.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.paidby.clearFormatting();
		screenVars.exmcode.clearFormatting();
		screenVars.name.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.zmedfee.clearFormatting();
		screenVars.invref.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr642ScreenVars screenVars = (Sr642ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.pgmnam.setClassString("");
		screenVars.seqno.setClassString("");
		screenVars.zmedtyp.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.paidby.setClassString("");
		screenVars.exmcode.setClassString("");
		screenVars.name.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.zmedfee.setClassString("");
		screenVars.invref.setClassString("");
	}

/**
 * Clear all the variables in Sr642screensfl
 */
	public static void clear(VarModel pv) {
		Sr642ScreenVars screenVars = (Sr642ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.pgmnam.clear();
		screenVars.seqno.clear();
		screenVars.zmedtyp.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.paidby.clear();
		screenVars.exmcode.clear();
		screenVars.name.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.zmedfee.clear();
		screenVars.invref.clear();
	}
}
