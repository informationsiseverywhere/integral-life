package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.Br644DAO;
import com.csc.life.productdefinition.dataaccess.model.Br644DTO;
import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br644DAOImpl extends BaseDAOImpl<Medipf> implements Br644DAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(Br644DAOImpl.class);
	
	public List<Medipf> loadDataByBatch(int batchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM BR644DATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, SEQNO DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Medipf> mediList = new ArrayList<Medipf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				Medipf medipf = new Medipf();
				Br644DTO br644DTO = new Br644DTO();
				medipf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				medipf.setChdrcoy(rs.getString("CHDRCOY").trim());
				medipf.setChdrnum(rs.getString("CHDRNUM").trim());
				medipf.setSeqno(rs.getInt("SEQNO"));
				medipf.setPaidby(rs.getString("PAIDBY").trim());
				medipf.setExmcode(rs.getString("EXMCODE").trim());
				medipf.setZmedtyp(rs.getString("ZMEDTYP").trim());
				medipf.setEffdate(rs.getInt("EFFDATE"));
				medipf.setInvref(rs.getString("INVREF").trim());
				medipf.setZmedfee(rs.getBigDecimal("ZMEDFEE"));
				medipf.setLife(rs.getString("LIFE").trim());
				medipf.setJlife(rs.getString("JLIFE").trim());
				br644DTO.setClntnum(rs.getString("CLNTNUM").trim());
				br644DTO.setDate1(String.valueOf(rs.getInt("DATE1")) == null ? 0 : rs.getInt("DATE1"));
				br644DTO.setDate2(String.valueOf(rs.getInt("DATE2")) == null ? 0 : rs.getInt("DATE2"));
				br644DTO.setCltstat(rs.getString("CLTSTAT") == null ? "": rs.getString("CLTSTAT").trim());
				medipf.setBr644DTO(br644DTO);
				mediList.add(medipf);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return mediList;
	}	
	
	public int populateBr644Temp(String company, int batchExtractSize, String chdrfrom, String chdrto) {
		initializeBr644Temp();
		int rows = 0;
		boolean isParameterized = false;
		boolean isParamFromOnly = false;
		boolean isParamToOnly = false;
		if (!("".equals(chdrfrom.trim())) && !("".equals(chdrto.trim()))) {
			if (Integer.parseInt(chdrto) >= Integer.parseInt(chdrfrom)) isParameterized = true;
		}
		
		if (!("".equals(chdrfrom.trim())) && "".equals(chdrto.trim())) isParamFromOnly = true;
		if (!("".equals(chdrto.trim())) && "".equals(chdrfrom.trim())) isParamToOnly = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.BR644DATA "); //ILB-475
		sb.append("(BATCHID, UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SEQNO, PAIDBY, EXMCODE, ZMEDTYP, EFFDATE, INVREF, ZMEDFEE, LIFE, JLIFE, CLNTNUM, DATE1, DATE2, CLTSTAT)  ");
		sb.append("SELECT * FROM( ");
		sb.append("SELECT ");
		sb.append("floor((row_number()over(ORDER BY MAIN.CHDRCOY ASC, MAIN.CHDRNUM ASC, MAIN.SEQNO DESC)-1)/?) BATCHID, MAIN.* FROM ( ");
		sb.append("SELECT MEDI.UNIQUE_NUMBER, MEDI.CHDRCOY, MEDI.CHDRNUM, MEDI.SEQNO, MEDI.PAIDBY, MEDI.EXMCODE,  MEDI.ZMEDTYP, MEDI.EFFDATE, MEDI.INVREF, MEDI.ZMEDFEE, ");
		sb.append("MEDI.LIFE, MEDI.JLIFE, AG.CLNTNUM, NULL AS DATE1, NULL AS DATE2, NULL CLTSTAT ");
		sb.append("FROM MEDIPF MEDI  ");
		sb.append("INNER JOIN AGNTPF AG ON MEDI.CHDRCOY=AG.AGNTCOY AND AG.AGNTPFX='AG' AND MEDI.EXMCODE=AG.AGNTNUM  ");
		sb.append("AND MEDI.PAIDBY='A' AND MEDI.ACTIND='1' ");
		sb.append("UNION ALL ");
		sb.append("SELECT MEDI.UNIQUE_NUMBER, MEDI.CHDRCOY, MEDI.CHDRNUM, MEDI.SEQNO, MEDI.PAIDBY, MEDI.EXMCODE, MEDI.ZMEDTYP, MEDI.EFFDATE, MEDI.INVREF, MEDI.ZMEDFEE, ");
		sb.append("MEDI.LIFE, MEDI.JLIFE, ZMPV.ZMEDNUM, ZMPV.DTETRM AS DATE1, NULL AS DATE2, NULL CLTSTAT ");
		sb.append("FROM MEDIPF MEDI  ");
		sb.append("INNER JOIN ZMPVPF ZMPV ON MEDI.CHDRCOY=ZMPV.ZMEDCOY AND MEDI.EXMCODE=ZMPV.ZMEDPRV  ");
		sb.append("AND MEDI.PAIDBY='D' AND MEDI.ACTIND='1' ");
		sb.append("UNION ALL ");
		sb.append("SELECT MEDI.UNIQUE_NUMBER, MEDI.CHDRCOY, MEDI.CHDRNUM, MEDI.SEQNO, MEDI.PAIDBY, MEDI.EXMCODE, MEDI.ZMEDTYP, MEDI.EFFDATE, MEDI.INVREF, MEDI.ZMEDFEE, ");
		sb.append("MEDI.LIFE, MEDI.JLIFE, MEDI.EXMCODE CLNTNUM, CN.CLTDOD AS DATE1, MAX(BRUP.DISCHDT) AS DATE2, CN.CLTSTAT ");
		sb.append("FROM MEDIPF MEDI  ");
		sb.append("INNER JOIN CLNTPF CN ON CN.CLNTPFX='CN' AND CN.CLNTCOY='9' AND MEDI.EXMCODE=CN.CLNTNUM AND MEDI.PAIDBY='C' AND MEDI.ACTIND='1' ");
		sb.append("LEFT JOIN BRUPPF BRUP ON BRUP.CLNTCOY='9' AND MEDI.EXMCODE = BRUP.CLNTNUM ");
		sb.append("GROUP BY MEDI.UNIQUE_NUMBER, MEDI.CHDRCOY, MEDI.CHDRNUM, MEDI.PAIDBY, MEDI.EXMCODE, MEDI.SEQNO, MEDI.ZMEDTYP, MEDI.EFFDATE, MEDI.INVREF, MEDI.ZMEDFEE, MEDI.LIFE, MEDI.JLIFE, CN.CLNTNUM, CN.CLTDOD, CLTSTAT ");
		sb.append(") MAIN WHERE MAIN.CHDRCOY=? ");
		if (isParameterized)
			sb.append("AND MAIN.CHDRNUM BETWEEN ? and ? ");	
		if (isParamFromOnly || isParamToOnly)
			sb.append("AND MAIN.CHDRNUM = ? ");	
		sb.append(") MAIN ORDER BY MAIN.CHDRCOY ASC, MAIN.CHDRNUM ASC, MAIN.SEQNO  ");

		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, company);
			if (isParameterized){
				ps.setString(3, chdrfrom);
				ps.setString(4, chdrto);
			}
			if (isParamFromOnly) ps.setString(3, chdrfrom);
			if (isParamToOnly) ps.setString(3, chdrto);
			
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBr644Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeBr644Temp() {
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM BR644DATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBr644Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}		

}