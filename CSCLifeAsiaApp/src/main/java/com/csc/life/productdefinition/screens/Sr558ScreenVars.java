package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR558
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr558ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1010);
	public FixedLengthStringData dataFields = new FixedLengthStringData(482).isAPartOf(dataArea, 0);
	public ZonedDecimalData aplamt = DD.aplamt.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData aplint = DD.aplint.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,28);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,30);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData frequency = DD.frequency.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,102);
	public ZonedDecimalData instpramt = DD.instpramt.copyToZonedDecimal().isAPartOf(dataFields,103);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,167);
	public FixedLengthStringData jowner = DD.jowner.copy().isAPartOf(dataFields,175);
	public FixedLengthStringData jownername = DD.jownername.copy().isAPartOf(dataFields,183);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,230);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,277);
	public ZonedDecimalData loanvalue = DD.loanvalue.copyToZonedDecimal().isAPartOf(dataFields,285);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,302);
	public ZonedDecimalData nextinsamt = DD.nextinsamt.copyToZonedDecimal().isAPartOf(dataFields,303);
	public ZonedDecimalData nextinsdte = DD.nextinsdte.copyToZonedDecimal().isAPartOf(dataFields,320);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,328);
	public ZonedDecimalData outstamt = DD.outstamt.copyToZonedDecimal().isAPartOf(dataFields,336);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,353);
	public ZonedDecimalData plint = DD.plint.copyToZonedDecimal().isAPartOf(dataFields,400);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,411);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,421);
	public FixedLengthStringData pymdesc = DD.pymdesc.copy().isAPartOf(dataFields,429);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,459);
	public FixedLengthStringData shortdss = new FixedLengthStringData(20).isAPartOf(dataFields, 462);
	public FixedLengthStringData[] shortds = FLSArrayPartOfStructure(2, 10, shortdss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(shortdss, 0, FILLER_REDEFINE);
	public FixedLengthStringData shortds01 = DD.shortds.copy().isAPartOf(filler,0);
	public FixedLengthStringData shortds02 = DD.shortds.copy().isAPartOf(filler,10);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 482);
	public FixedLengthStringData aplamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData aplintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData frequencyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData instpramtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jownerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData jownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData loanvalueErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData nextinsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData nextinsdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData outstamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData plintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData pymdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData shortdssErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData[] shortdsErr = FLSArrayPartOfStructure(2, 4, shortdssErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(shortdssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData shortds01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData shortds02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(396).isAPartOf(dataArea, 614);
	public FixedLengthStringData[] aplamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] aplintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] frequencyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] instpramtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jownerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] jownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] loanvalueOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] nextinsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] nextinsdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] outstamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] plintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] pymdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData shortdssOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 372);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(2, 12, shortdssOut, 0);
	public FixedLengthStringData[][] shortdsO = FLSDArrayPartOfArrayStructure(12, 1, shortdsOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(shortdssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] shortds01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] shortds02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextinsdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr558screenWritten = new LongData(0);
	public LongData Sr558protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr558ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifename, jlife, jlifename, cownnum, ownername, jowner, jownername, btdate, occdate, ptdate, nextinsdte, aplint, plint, aplamt, loanvalue, billfreq, instpramt, shortds01, mop, pymdesc, indic, lifenum, outstamt, frequency, shortds02, nextinsamt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenameOut, jlifenumOut, jlifenameOut, cownnumOut, ownernameOut, jownerOut, jownernameOut, btdateOut, occdateOut, ptdateOut, nextinsdteOut, aplintOut, plintOut, aplamtOut, loanvalueOut, billfreqOut, instpramtOut, shortds01Out, mopOut, pymdescOut, indicOut, lifenumOut, outstamtOut, frequencyOut, shortds02Out, nextinsamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenameErr, jlifenumErr, jlifenameErr, cownnumErr, ownernameErr, jownerErr, jownernameErr, btdateErr, occdateErr, ptdateErr, nextinsdteErr, aplintErr, plintErr, aplamtErr, loanvalueErr, billfreqErr, instpramtErr, shortds01Err, mopErr, pymdescErr, indicErr, lifenumErr, outstamtErr, frequencyErr, shortds02Err, nextinsamtErr};
		screenDateFields = new BaseData[] {btdate, occdate, ptdate, nextinsdte};
		screenDateErrFields = new BaseData[] {btdateErr, occdateErr, ptdateErr, nextinsdteErr};
		screenDateDispFields = new BaseData[] {btdateDisp, occdateDisp, ptdateDisp, nextinsdteDisp};
		
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr558screen.class;
		protectRecord = Sr558protect.class;
	}

}
