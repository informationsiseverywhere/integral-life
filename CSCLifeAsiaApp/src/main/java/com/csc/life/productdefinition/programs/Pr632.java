/*
 * File: Pr632.java
 * Date: 30 August 2009 1:49:36
 * Author: Quipoz Limited
 * 
 * Class transformed from PR632.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.screens.Sr632ScreenVars;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   Medical Provider Register Submenu
*   ---------------------------------
*   This program is used to create, modify, enquire upon medical
*   provider register by provider code. There are 3 options
*
*   Action A - Medical Provider Registration
*              Register the medical provider details
*
*   Action B - Medical Provider Maintenance
*              Modify the medical provider details
*
*   Action C - Medical Provider Enquiry
*              Enquiry the medical provider details from ZMPD file
*
***********************************************************************
* </pre>
*/
public class Pr632 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR632");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String e070 = "E070";
	private String e073 = "E073";
	private String e186 = "E186";
	private String f978 = "F978";
	private String rl63 = "RL63";
	private String rl68 = "RL68";
		/* FORMATS */
	private String zmpvrec = "ZMPVREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Batckey wsaaBatchkey = new Batckey();
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Sr632ScreenVars sv = ScreenProgram.getScreenVars( Sr632ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2190, 
		exit2990, 
		exit3090, 
		exit3490
	}

	public Pr632() {
		super();
		screenVars = sv;
		new ScreenModel("Sr632", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		validateKey12210();
	}

protected void validateKey12210()
	{
		if (isEQ(sv.action,"A")){
			readZmpvFile2300();
			if (isEQ(zmpvIO.getStatuz(),varcom.oK)) {
				sv.zmedprvErr.set(f978);
			}
			if (isEQ(sv.zmedprv,SPACES)) {
				sv.zmedprvErr.set(e186);
			}
		}
		else if (isEQ(sv.action,"B")){
			readZmpvFile2300();
			if (isEQ(zmpvIO.getStatuz(),varcom.mrnf)) {
				sv.zmedprvErr.set(rl63);
			}
		}
		else if (isEQ(sv.action,"C")){
			readZmpvFile2300();
			if (isEQ(zmpvIO.getStatuz(),varcom.mrnf)) {
				sv.zmedprvErr.set(rl63);
			}
		}
	}

protected void readZmpvFile2300()
	{
		start2310();
	}

protected void start2310()
	{
		zmpvIO.setRecKeyData(SPACES);
		zmpvIO.setRecNonKeyData(SPACES);
		zmpvIO.setDteapp(varcom.vrcmMaxDate);
		zmpvIO.setDtetrm(varcom.vrcmMaxDate);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.zmedprv);
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)
		&& isNE(zmpvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit2990);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			goTo(GotoLabel.exit2990);
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		try {
			updateWssp3010();
			batching3080();
		}
		catch (GOTOException e){
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(sv.action,"A")){
			wsspcomn.flag.set("C");
			keepsNewZmpv3200();
		}
		else if (isEQ(sv.action,"B")){
			wsspcomn.flag.set("M");
			keepsOldZmpv3300();
		}
		else if (isEQ(sv.action,"C")){
			wsspcomn.flag.set("I");
			keepsOldZmpv3300();
		}
		softLock3400();
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void keepsNewZmpv3200()
	{
		start3210();
	}

protected void start3210()
	{
		zmpvIO.setRecKeyData(SPACES);
		zmpvIO.setRecNonKeyData(SPACES);
		zmpvIO.setDteapp(varcom.vrcmMaxDate);
		zmpvIO.setDtetrm(varcom.vrcmMaxDate);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.zmedprv);
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
	}

protected void keepsOldZmpv3300()
	{
		/*START*/
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void softLock3400()
	{
		try {
			start3410();
		}
		catch (GOTOException e){
		}
	}

protected void start3410()
	{
		if (isEQ(sv.action,"C")) {
			goTo(GotoLabel.exit3490);
		}
		initialize(sftlockrec.sftlockRec);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("MP");
		sftlockrec.entity.set(sv.zmedprv);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.date_var.set(varcom.vrcmDate);
		sftlockrec.time.set(varcom.vrcmTime);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("LOCK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.zmedprvErr.set(rl68);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
