/*
 * File: Pr642.java
 * Date: 30 August 2009 1:50:42
 * Author: Quipoz Limited
 * 
 * Class transformed from PR642.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.MediTableDAM;
import com.csc.life.productdefinition.dataaccess.MediinvTableDAM;
import com.csc.life.productdefinition.dataaccess.MediseqTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpfTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.screens.Sr642ScreenVars;
import com.csc.life.productdefinition.tablestructures.Tr640rec;
import com.csc.life.productdefinition.tablestructures.Tr650rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Medical Bill Data Capture by Contract
* -------------------------------------
*
***********************************************************************
* </pre>
*/
public class Pr642 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR642");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSubfileCnt = new ZonedDecimalData(5, 0).setUnsigned();
	private final String wsaaLargeName = "LGNMS";
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaEntityDesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(2, 0);
	private ZonedDecimalData wsaaMedFee = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaBlankRec = new FixedLengthStringData(1);
	private Validator blankRec = new Validator(wsaaBlankRec, "Y");

	private FixedLengthStringData wsaaFoundSubfileRec = new FixedLengthStringData(1);
	private Validator foundSubfileRec = new Validator(wsaaFoundSubfileRec, "Y");

	private FixedLengthStringData wsaaFoundLoadFromMedi = new FixedLengthStringData(1);
	private Validator foundLoadFromMedi = new Validator(wsaaFoundLoadFromMedi, "Y");
		/* ERRORS */
	private static final String e186 = "E186";
	private static final String e207 = "E207";
	private static final String e305 = "E305";
	private static final String e655 = "E655";
	private static final String rl70 = "RL70";
	private static final String rl69 = "RL69";
	private static final String f189 = "F189";
	private static final String h143 = "H143";
	private static final String rfik = "RFIK";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String tr650 = "TR650";
	private static final String tr640 = "TR640";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private MediTableDAM mediIO = new MediTableDAM();
	private MediinvTableDAM mediinvIO = new MediinvTableDAM();
	private MediseqTableDAM mediseqIO = new MediseqTableDAM();
	private ZmpfTableDAM zmpfIO = new ZmpfTableDAM();
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Tr640rec tr640rec = new Tr640rec();
	private Tr650rec tr650rec = new Tr650rec();
	private Sr642ScreenVars sv = ScreenProgram.getScreenVars( Sr642ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		name1300, 
		exit2090, 
		nextRec3100, 
		exit3100
	}

	public Pr642() {
		super();
		screenVars = sv;
		new ScreenModel("Sr642", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaBatchkey.set(wsspcomn.batchkey);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		mediinvIO.setFormat(formatsInner.mediinvrec);
		mediinvIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, mediinvIO);
		if (isNE(mediinvIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mediinvIO.getStatuz());
			syserrrec.params.set(mediinvIO.getParams());
			fatalError600();
		}
		setScreen1100();
		mediIO.setDataArea(SPACES);
		mediIO.setStatuz(varcom.oK);
		mediIO.setChdrcoy(mediinvIO.getChdrcoy());
		mediIO.setChdrnum(mediinvIO.getChdrnum());
		mediIO.setSeqno(ZERO);
		mediIO.setFormat(formatsInner.medirec);
		mediIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mediIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(), varcom.oK)
		&& isNE(mediIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		if (isNE(mediIO.getStatuz(), varcom.oK)
		|| isNE(mediIO.getChdrcoy(), mediinvIO.getChdrcoy())
		|| isNE(mediIO.getChdrnum(), mediinvIO.getChdrnum())) {
			mediIO.setStatuz(varcom.endp);
		}
		wsaaSubfileCnt.set(1);
		while ( !(isEQ(mediIO.getStatuz(), varcom.endp))) {
			loadMedi1200();
		}
		
		if (isGT(wsaaSubfileCnt, sv.subfilePage)) {
			wsaaSubfileCnt.set(1);
		}
		while ( !(isGT(wsaaSubfileCnt, sv.subfilePage))) {
			blankSubfile1250();
		}
		
		scrnparams.subfileMore.set("Y");
		scrnparams.subfileRrn.set(1);
	}

protected void setScreen1100()
	{
		begin1100();
	}

protected void begin1100()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setStatuz(varcom.oK);
		chdrlifIO.setChdrcoy(mediinvIO.getChdrcoy());
		chdrlifIO.setChdrnum(mediinvIO.getChdrnum());
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlifIO.getChdrnum());
		sv.cntcurr.set(chdrlifIO.getCntcurr());
		sv.register.set(chdrlifIO.getRegister());
		sv.cnttype.set(chdrlifIO.getCnttype());
		/*  Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlifIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*  Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlifIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*  Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrlifIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.set(SPACES);
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Obtain Life No. & Name*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlifIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.lifedesc.set(namadrsrec.name);
		/* Obtain Joint Life No. & Name*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlifIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.oK)) {
			sv.jlifeno.set(lifelnbIO.getLifcnum());
			namadrsrec.namadrsRec.set(SPACES);
			namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
			namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
			namadrsrec.clntCompany.set(wsspcomn.fsuco);
			namadrsrec.language.set(wsspcomn.language);
			namadrsrec.function.set("LGNMN");
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz, varcom.oK)) {
				syserrrec.params.set(namadrsrec.statuz);
				fatalError600();
			}
			sv.jlifedesc.set(namadrsrec.name);
		}
		else {
			sv.jlifeno.set(SPACES);
			sv.jlifedesc.set(SPACES);
		}
		/* Obtain Owner No. & Name*/
		sv.cownnum.set(chdrlifIO.getCownnum());
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(chdrlifIO.getCownnum());
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.ownerdesc.set(namadrsrec.name);
		/* Obtain Joint Owner No. & Name*/
		if (isNE(chdrlifIO.getJownnum(), SPACES)) {
			sv.jownnum.set(chdrlifIO.getJownnum());
			namadrsrec.namadrsRec.set(SPACES);
			namadrsrec.clntNumber.set(chdrlifIO.getJownnum());
			namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
			namadrsrec.clntCompany.set(wsspcomn.fsuco);
			namadrsrec.language.set(wsspcomn.language);
			namadrsrec.function.set("LGNMN");
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz, varcom.oK)) {
				syserrrec.params.set(namadrsrec.statuz);
				fatalError600();
			}
			sv.lname.set(namadrsrec.name);
		}
		else {
			sv.jownnum.set(SPACES);
			sv.lname.set(SPACES);
		}
		/*  Obtain the Agent Name*/
		sv.servagnt.set(chdrlifIO.getAgntnum());
		readAgnt1110();
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.servagnam.set(namadrsrec.name);
		/*  Obtain the Servicing Branch description from T1692.*/
		sv.servbr.set(chdrlifIO.getCntbranch());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrlifIO.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.brchname.set(SPACES);
		}
		else {
			sv.brchname.set(descIO.getLongdesc());
		}
	}

protected void readAgnt1110()
	{
		begin1110();
	}

protected void begin1110()
	{
		agntIO.setDataArea(SPACES);
		agntIO.setStatuz(varcom.oK);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(chdrlifIO.getAgntnum());
		agntIO.setFormat(formatsInner.agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
	}

protected void loadMedi1200()
	{
		begin1200();
		nextMedi1200();
	}

protected void begin1200()
	{
		if (isNE(mediIO.getActiveInd(), "0")) {
			return ;
		}
		sv.initialiseSubfileArea();
		sv.zmedtyp.set(mediIO.getZmedtyp());
		sv.life.set(mediIO.getLife());
		sv.jlife.set(mediIO.getJlife());
		sv.paidby.set(mediIO.getPaidby());
		sv.exmcode.set(mediIO.getExmcode());
		getEntityDesc1300();
		sv.name.set(wsaaEntityDesc);
		sv.invref.set(mediIO.getInvref());
		sv.effdate.set(mediIO.getEffdate());
		sv.zmedfee.set(mediIO.getZmedfee());
		sv.pgmnam.set(wsaaProg);
		sv.seqno.set(mediIO.getSeqno());
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaSubfileCnt.add(1);
	}

protected void nextMedi1200()
	{
		mediIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(), varcom.oK)
		&& isNE(mediIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		if (isNE(mediIO.getStatuz(), varcom.oK)
		|| isNE(mediIO.getChdrcoy(), mediinvIO.getChdrcoy())
		|| isNE(mediIO.getChdrnum(), mediinvIO.getChdrnum())) {
			mediIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void blankSubfile1250()
	{
		/*BEGIN*/
		sv.initialiseSubfileArea();
		sv.pgmnam.set(wsaaProg);
		sv.zmedtyp.set(SPACES);
		sv.life.set(SPACES);
		sv.jlife.set(SPACES);
		sv.paidby.set(SPACES);
		sv.exmcode.set(SPACES);
		sv.name.set(SPACES);
		sv.invref.set(SPACES);
		sv.zmedfee.set(ZERO);
		sv.seqno.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaSubfileCnt.add(1);
		/*EXIT*/
	}

protected void getEntityDesc1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin1300();
				case name1300: 
					name1300();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1300()
	{
		wsaaClntnum.set(SPACES);
		if (isEQ(sv.paidby, "A")){
			agntClntnum1360();
			if (isNE(agntIO.getStatuz(), varcom.oK)) {
				sv.exmcodeErr.set(e305);
				goTo(GotoLabel.name1300);
			}
			aglfRec1390();
			if (isNE(aglfIO.getDtetrm(), varcom.vrcmMaxDate)) {
				sv.exmcodeErr.set(f189);
			}
		}
		else if (isEQ(sv.paidby, "D")){
			medProvClntnum1370();
			if (isNE(zmpvIO.getStatuz(), varcom.oK)) {
				sv.exmcodeErr.set(rl69);
				goTo(GotoLabel.name1300);
			}
			if (isNE(zmpvIO.getDtetrm(), varcom.vrcmMaxDate)) {
				sv.exmcodeErr.set(rl70);
			}
		}
		else if (isEQ(sv.paidby, "C")){
			clntClntnum1380();
			if (isNE(clntIO.getStatuz(), varcom.oK)) {
				sv.exmcodeErr.set(e655);
			}
		}
	}

protected void name1300()
	{
		if (isEQ(wsaaClntnum, SPACES)) {
			wsaaEntityDesc.set(SPACES);
			return ;
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.statuz.set(varcom.oK);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
		wsaaEntityDesc.set(namadrsrec.name);
	}

protected void agntClntnum1360()
	{
		begin1360();
	}

protected void begin1360()
	{
		agntIO.setDataArea(SPACES);
		agntIO.setStatuz(varcom.oK);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(sv.exmcode);
		agntIO.setFormat(formatsInner.agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isEQ(agntIO.getStatuz(), varcom.oK)) {
			wsaaClntnum.set(agntIO.getClntnum());
		}
	}

protected void medProvClntnum1370()
	{
		begin1370();
	}

protected void begin1370()
	{
		zmpvIO.setDataArea(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.exmcode);
		zmpvIO.setFormat(formatsInner.zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(), varcom.oK)
		&& isNE(zmpvIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpvIO.getStatuz(), varcom.oK)) {
			wsaaClntnum.set(zmpvIO.getZmednum());
		}
	}

protected void clntClntnum1380()
	{
		begin1380();
	}

protected void begin1380()
	{
		clntIO.setDataArea(SPACES);
		clntIO.setStatuz(varcom.oK);
		clntIO.setClntpfx(fsupfxcpy.clnt);
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setClntnum(sv.exmcode);
		clntIO.setFormat(formatsInner.clntrec);
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)
		&& isNE(clntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		if (isEQ(clntIO.getStatuz(), varcom.oK)) {
			wsaaClntnum.set(sv.exmcode);
		}
	}

protected void aglfRec1390()
	{
		begin1390();
	}

protected void begin1390()
	{
		aglfIO.setDataArea(SPACES);
		aglfIO.setStatuz(varcom.oK);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.exmcode);
		aglfIO.setFormat(formatsInner.aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaSubfileCnt.set(1);
			while ( !(isGT(wsaaSubfileCnt, sv.subfilePage))) {
				blankSubfile1250();
			}
			
			scrnparams.subfileMore.set("Y");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		wsaaFoundLoadFromMedi.set("N");
		wsaaFoundSubfileRec.set("N");
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		if (!foundSubfileRec.isTrue()
		&& !foundLoadFromMedi.isTrue()) {
			scrnparams.subfileRrn.set(1);
			scrnparams.function.set(varcom.sread);
			screenIo9000();
			sv.zmedtypErr.set(e207);
			sv.lifeErr.set(e207);
			sv.jlifeErr.set(e207);
			sv.paidbyErr.set(e207);
			sv.exmcodeErr.set(e207);
			sv.effdateErr.set(e207);
			sv.zmedfeeErr.set(e207);
			sv.invrefErr.set(e207);
			scrnparams.function.set(varcom.supd);
			screenIo9000();
			wsspcomn.edterror.set("Y");
		}
		scrnparams.subfileRrn.set(1);
	}

protected void validateSubfile2600()
	{
		validation2610();
		updateErrorIndicators2670();
	}

protected void validation2610()
	{
		sv.pgmnam.set(wsaaProg);
		if (isEQ(sv.zmedtyp, SPACES)
		&& isEQ(sv.life, SPACES)
		&& isEQ(sv.jlife, SPACES)
		&& isEQ(sv.paidby, SPACES)
		&& isEQ(sv.exmcode, SPACES)
		&& isEQ(sv.invref, SPACES)
		&& isEQ(sv.effdate, varcom.vrcmMaxDate)
		&& isEQ(sv.zmedfee, ZERO)) {
			if (isNE(sv.seqno, ZERO)) {
				wsaaFoundLoadFromMedi.set("Y");
			}
			return ;
		}
		wsaaFoundSubfileRec.set("Y");
		if (isEQ(sv.zmedtyp, SPACES)) {
			sv.zmedtypErr.set(e186);
		}
		else {
			if (isEQ(sv.life, SPACES)) {
				sv.lifeErr.set(e207);
			}
			if (isEQ(sv.jlife, SPACES)) {
				sv.jlifeErr.set(e207);
			}
		}
		if (isNE(sv.life, SPACES)
		&& isNE(sv.jlife, SPACES)) {
			readLifelnb2640();
			if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
				sv.lifeErr.set(h143);
				sv.jlifeErr.set(h143);
			}
		}
		if (isEQ(sv.paidby, SPACES)) {
			sv.paidbyErr.set(e186);
		}
		else {
			readTr6502650();
			if (isEQ(tr650rec.ynflag, "Y")
			&& isEQ(sv.invref, SPACES)) {
				sv.invrefErr.set(e186);
			}
		}
		if (isEQ(sv.exmcode, SPACES)) {
			sv.exmcodeErr.set(e186);
		}
		if (isNE(sv.exmcode, SPACES)) {
			getEntityDesc1300();
			sv.name.set(wsaaEntityDesc);
		}
		if (isEQ(sv.paidby, "A")
		|| isEQ(sv.paidby, "C")) {
			if (isEQ(sv.zmedfee, ZERO)) {
				defaultFeeTr6402620();
			}
		}
		else {
			if (isEQ(sv.paidby, "D")
			&& isEQ(sv.zmedfee, ZERO)) {
				defaultFeeZmpf2630();
			}
		}
		if (isEQ(sv.zmedfee, ZERO)) {
			sv.zmedfeeErr.set(e186);
		}
		else {
			zrdecplrec.amountIn.set(sv.zmedfee);
			zrdecplrec.currency.set(sv.cntcurr);
			a100CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.zmedfee)) {
				sv.zmedfeeErr.set(rfik);
			}
		}
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			sv.effdateErr.set(e186);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void defaultFeeTr6402620()
	{
		start2620();
	}

protected void start2620()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr640);
		itemIO.setItemitem(sv.zmedtyp);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			tr640rec.tr640Rec.set(itemIO.getGenarea());
			sv.zmedfee.set(tr640rec.zmedfee);
		}
	}

protected void defaultFeeZmpf2630()
	{
		start2630();
	}

protected void start2630()
	{
		wsaaMedFee.set(ZERO);
		zmpfIO.setDataKey(SPACES);
		zmpfIO.setZmedcoy(wsspcomn.company);
		zmpfIO.setZmedprv(sv.exmcode);
		zmpfIO.setSeqno(1);
		zmpfIO.setFormat(formatsInner.zmpfrec);
		zmpfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zmpfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zmpfIO.setFitKeysSearch("ZMEDCOY", "ZMEDPRV");
		zmpfIO.setStatuz(varcom.oK);
		while ( !(isEQ(zmpfIO.getStatuz(), varcom.endp))) {
			loopZmpf2635();
		}
		
		if (isNE(wsaaMedFee, ZERO)) {
			sv.zmedfee.set(wsaaMedFee);
		}
		else {
			defaultFeeTr6402620();
		}
	}

protected void loopZmpf2635()
	{
		/*START*/
		SmartFileCode.execute(appVars, zmpfIO);
		if (isNE(zmpfIO.getStatuz(), varcom.oK)
		&& isNE(zmpfIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zmpfIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpfIO.getStatuz(), varcom.endp)
		|| isNE(zmpfIO.getZmedcoy(), wsspcomn.company)
		|| isNE(zmpfIO.getZmedprv(), sv.exmcode)) {
			zmpfIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zmpfIO.getZmedtyp(), sv.zmedtyp)) {
			wsaaMedFee.set(zmpfIO.getZmedfee());
		}
		zmpfIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void readLifelnb2640()
	{
		start2640();
	}

protected void start2640()
	{
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlifIO.getChdrnum());
		lifelnbIO.setLife(sv.life);
		lifelnbIO.setJlife(sv.jlife);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

protected void readTr6502650()
	{
		start2650();
	}

protected void start2650()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr650);
		itemIO.setItemitem(sv.paidby);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			tr650rec.tr650Rec.set(itemIO.getGenarea());
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			updateSubfile3100();
		}
		
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(sv.chdrnum);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void updateSubfile3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin3100();
				case nextRec3100: 
					nextRec3100();
				case exit3100: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3100()
	{
		processScreen("SR642", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit3100);
		}
		if (isEQ(sv.zmedtyp, SPACES)
		&& isEQ(sv.life, SPACES)
		&& isEQ(sv.jlife, SPACES)
		&& isEQ(sv.paidby, SPACES)
		&& isEQ(sv.exmcode, SPACES)
		&& isEQ(sv.invref, SPACES)
		&& isEQ(sv.effdate, varcom.vrcmMaxDate)
		&& isEQ(sv.zmedfee, ZERO)) {
			if (isEQ(sv.seqno, ZERO)) {
				goTo(GotoLabel.nextRec3100);
			}
			else {
				wsaaBlankRec.set("Y");
			}
		}
		else {
			wsaaBlankRec.set("N");
		}
		mediIO.setDataArea(SPACES);
		mediIO.setChdrcoy(wsspcomn.company);
		mediIO.setChdrnum(sv.chdrnum);
		mediIO.setSeqno(sv.seqno);
		mediIO.setFormat(formatsInner.medirec);
		mediIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(), varcom.oK)
		&& isNE(mediIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		mediIO.setLife(sv.life);
		mediIO.setJlife(sv.jlife);
		mediIO.setZmedtyp(sv.zmedtyp);
		mediIO.setExmcode(sv.exmcode);
		mediIO.setZmedfee(sv.zmedfee);
		mediIO.setPaidby(sv.paidby);
		mediIO.setInvref(sv.invref);
		mediIO.setEffdate(sv.effdate);
		mediIO.setActiveInd("0");
		mediIO.setPaydte(varcom.vrcmMaxDate);
		if (isEQ(mediIO.getStatuz(), varcom.oK)) {
			if (blankRec.isTrue()) {
				mediIO.setFunction(varcom.delet);
			}
			else {
				mediIO.setFunction(varcom.rewrt);
			}
		}
		else {
			readMediseq3200();
			mediIO.setChdrcoy(wsspcomn.company);
			mediIO.setChdrnum(sv.chdrnum);
			mediIO.setSeqno(wsaaSeqno);
			mediIO.setTranno(ZERO);
			mediIO.setFunction(varcom.writr);
		}
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
	}

protected void nextRec3100()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void readMediseq3200()
	{
		begin3200();
	}

protected void begin3200()
	{
		mediseqIO.setDataArea(SPACES);
		mediseqIO.setStatuz(varcom.oK);
		mediseqIO.setChdrcoy(wsspcomn.company);
		mediseqIO.setChdrnum(sv.chdrnum);
		mediseqIO.setSeqno(99999);
		mediseqIO.setFormat(formatsInner.mediseqrec);
		mediseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mediseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediseqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, mediseqIO);
		if (isNE(mediseqIO.getStatuz(), varcom.oK)
		&& isNE(mediseqIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mediseqIO.getStatuz());
			syserrrec.params.set(mediseqIO.getParams());
			fatalError600();
		}
		if (isNE(mediseqIO.getStatuz(), varcom.oK)
		|| isNE(mediseqIO.getChdrcoy(), wsspcomn.company)
		|| isNE(mediseqIO.getChdrnum(), sv.chdrnum)) {
			mediseqIO.setStatuz(varcom.endp);
		}
		if (isEQ(mediseqIO.getStatuz(), varcom.oK)) {
			compute(wsaaSeqno, 0).set(add(mediseqIO.getSeqno(), 1));
		}
		else {
			wsaaSeqno.set(1);
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen("SR642", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a100CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A190-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	private FixedLengthStringData agntrec = new FixedLengthStringData(10).init("AGNTREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData clntrec = new FixedLengthStringData(10).init("CLNTREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData medirec = new FixedLengthStringData(10).init("MEDIREC");
	private FixedLengthStringData mediinvrec = new FixedLengthStringData(10).init("MEDIINVREC");
	private FixedLengthStringData mediseqrec = new FixedLengthStringData(10).init("MEDISEQREC");
	private FixedLengthStringData zmpfrec = new FixedLengthStringData(10).init("ZMPFREC");
	private FixedLengthStringData zmpvrec = new FixedLengthStringData(10).init("ZMPVREC");
}
}
