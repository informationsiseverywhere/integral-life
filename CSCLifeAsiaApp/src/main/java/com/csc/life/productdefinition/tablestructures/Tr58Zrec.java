package com.csc.life.productdefinition.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:00
 * Description:
 * Copybook name: T5689REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr58Zrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr58zRec = new FixedLengthStringData(500);
  	public FixedLengthStringData billfreqs = new FixedLengthStringData(60).isAPartOf(tr58zRec, 0);
  	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(30, 2, billfreqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData billfreq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData billfreq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData billfreq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData billfreq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData billfreq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData billfreq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData billfreq07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData billfreq08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData billfreq09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData billfreq10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData billfreq11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData billfreq12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData billfreq13 = new FixedLengthStringData(2).isAPartOf(filler, 24);
  	public FixedLengthStringData billfreq14 = new FixedLengthStringData(2).isAPartOf(filler, 26);
  	public FixedLengthStringData billfreq15 = new FixedLengthStringData(2).isAPartOf(filler, 28);
  	public FixedLengthStringData billfreq16 = new FixedLengthStringData(2).isAPartOf(filler, 30);
  	public FixedLengthStringData billfreq17 = new FixedLengthStringData(2).isAPartOf(filler, 32);
  	public FixedLengthStringData billfreq18 = new FixedLengthStringData(2).isAPartOf(filler, 34);
  	public FixedLengthStringData billfreq19 = new FixedLengthStringData(2).isAPartOf(filler, 36);
  	public FixedLengthStringData billfreq20 = new FixedLengthStringData(2).isAPartOf(filler, 38);
  	public FixedLengthStringData billfreq21 = new FixedLengthStringData(2).isAPartOf(filler, 40);
  	public FixedLengthStringData billfreq22 = new FixedLengthStringData(2).isAPartOf(filler, 42);
  	public FixedLengthStringData billfreq23 = new FixedLengthStringData(2).isAPartOf(filler, 44);
  	public FixedLengthStringData billfreq24 = new FixedLengthStringData(2).isAPartOf(filler, 46);
  	public FixedLengthStringData billfreq25 = new FixedLengthStringData(2).isAPartOf(filler, 48);
  	public FixedLengthStringData billfreq26 = new FixedLengthStringData(2).isAPartOf(filler, 50);
  	public FixedLengthStringData billfreq27 = new FixedLengthStringData(2).isAPartOf(filler, 52);
  	public FixedLengthStringData billfreq28 = new FixedLengthStringData(2).isAPartOf(filler, 54);
  	public FixedLengthStringData billfreq29 = new FixedLengthStringData(2).isAPartOf(filler, 56);
  	public FixedLengthStringData billfreq30 = new FixedLengthStringData(2).isAPartOf(filler, 58);
  	/*public FixedLengthStringData mops = new FixedLengthStringData(30).isAPartOf(t5689Rec, 60);
  	public FixedLengthStringData[] mop = FLSArrayPartOfStructure(30, 1, mops, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(mops, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mop01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData mop02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData mop03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData mop04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData mop05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData mop06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData mop07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
  	public FixedLengthStringData mop08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
  	public FixedLengthStringData mop09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
  	public FixedLengthStringData mop10 = new FixedLengthStringData(1).isAPartOf(filler1, 9);
  	public FixedLengthStringData mop11 = new FixedLengthStringData(1).isAPartOf(filler1, 10);
  	public FixedLengthStringData mop12 = new FixedLengthStringData(1).isAPartOf(filler1, 11);
  	public FixedLengthStringData mop13 = new FixedLengthStringData(1).isAPartOf(filler1, 12);
  	public FixedLengthStringData mop14 = new FixedLengthStringData(1).isAPartOf(filler1, 13);
  	public FixedLengthStringData mop15 = new FixedLengthStringData(1).isAPartOf(filler1, 14);
  	public FixedLengthStringData mop16 = new FixedLengthStringData(1).isAPartOf(filler1, 15);
  	public FixedLengthStringData mop17 = new FixedLengthStringData(1).isAPartOf(filler1, 16);
  	public FixedLengthStringData mop18 = new FixedLengthStringData(1).isAPartOf(filler1, 17);
  	public FixedLengthStringData mop19 = new FixedLengthStringData(1).isAPartOf(filler1, 18);
  	public FixedLengthStringData mop20 = new FixedLengthStringData(1).isAPartOf(filler1, 19);
  	public FixedLengthStringData mop21 = new FixedLengthStringData(1).isAPartOf(filler1, 20);
  	public FixedLengthStringData mop22 = new FixedLengthStringData(1).isAPartOf(filler1, 21);
  	public FixedLengthStringData mop23 = new FixedLengthStringData(1).isAPartOf(filler1, 22);
  	public FixedLengthStringData mop24 = new FixedLengthStringData(1).isAPartOf(filler1, 23);
  	public FixedLengthStringData mop25 = new FixedLengthStringData(1).isAPartOf(filler1, 24);
  	public FixedLengthStringData mop26 = new FixedLengthStringData(1).isAPartOf(filler1, 25);
  	public FixedLengthStringData mop27 = new FixedLengthStringData(1).isAPartOf(filler1, 26);
  	public FixedLengthStringData mop28 = new FixedLengthStringData(1).isAPartOf(filler1, 27);
  	public FixedLengthStringData mop29 = new FixedLengthStringData(1).isAPartOf(filler1, 28);
  	public FixedLengthStringData mop30 = new FixedLengthStringData(1).isAPartOf(filler1, 29);
  	public FixedLengthStringData rendds = new FixedLengthStringData(60).isAPartOf(t5689Rec, 90);
  	public ZonedDecimalData[] rendd = ZDArrayPartOfStructure(30, 2, 0, rendds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(rendds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData rendd01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData rendd02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
  	public ZonedDecimalData rendd03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData rendd04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData rendd05 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData rendd06 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 10);
  	public ZonedDecimalData rendd07 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData rendd08 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 14);
  	public ZonedDecimalData rendd09 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 16);
  	public ZonedDecimalData rendd10 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 18);
  	public ZonedDecimalData rendd11 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 20);
  	public ZonedDecimalData rendd12 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 22);
  	public ZonedDecimalData rendd13 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 24);
  	public ZonedDecimalData rendd14 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 26);
  	public ZonedDecimalData rendd15 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 28);
  	public ZonedDecimalData rendd16 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 30);
  	public ZonedDecimalData rendd17 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 32);
  	public ZonedDecimalData rendd18 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 34);
  	public ZonedDecimalData rendd19 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 36);
  	public ZonedDecimalData rendd20 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 38);
  	public ZonedDecimalData rendd21 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 40);
  	public ZonedDecimalData rendd22 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 42);
  	public ZonedDecimalData rendd23 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 44);
  	public ZonedDecimalData rendd24 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 46);
  	public ZonedDecimalData rendd25 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 48);
  	public ZonedDecimalData rendd26 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 50);
  	public ZonedDecimalData rendd27 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 52);
  	public ZonedDecimalData rendd28 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 54);
  	public ZonedDecimalData rendd29 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 56);
  	public ZonedDecimalData rendd30 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 58);*/
  	public FixedLengthStringData filler3 = new FixedLengthStringData(440).isAPartOf(tr58zRec, 60, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr58zRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr58zRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}