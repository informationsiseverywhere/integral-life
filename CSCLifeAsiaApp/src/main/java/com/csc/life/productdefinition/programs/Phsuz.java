/*
 * File: P7000.java
 * Date: 3 December 2011 11:03:45 PM
 * Author: Quipoz Limited
 * 
 * Class transformed from PR29S.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.ShsuzScreenVars;
import com.csc.life.productdefinition.tablestructures.Thsuzrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
*REMARKS.
*                  Receipt Parameters
*                  ==================
* This  Table  will be used to handle
* Email, SMS for the various
* transaction codes.
****************************************************************** ****
* </pre>
*/
public class Phsuz extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PHSUZ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Thsuzrec thsuzrec = new Thsuzrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private ShsuzScreenVars sv = ScreenProgram.getScreenVars( ShsuzScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Phsuz() {
		super();
		screenVars = sv;
		new ScreenModel("Shsuz", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	@Override
	protected void initialise1000() {
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

	protected void initialise1010() {
		/* INITIALISE-SCREEN */
		sv.dataArea.set(SPACES);
		/* READ-PRIMARY-RECORD */
		/* READ-RECORD */
		itemIO.setParams(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

	protected void readRecord1031() {
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), Varcom.oK) && isNE(descIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

	protected void moveToScreen1040() {
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), Varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		thsuzrec.thsuzRec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			return;
		}
	}

protected void generalArea1045()
	{
		sv.flag.set(thsuzrec.flag);
		
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

	protected void preScreenEdit() {
		/* PRE-START */
		/* This section will handle any action required on the screen **/
		/* before the screen is painted. **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(Varcom.prot);
		}
		return;
		/* PRE-EXIT */
	}

	/**
	 * <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	@Override
	protected void screenEdit2000() {
		screenIo2010();
		exit2090();
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(Varcom.oK);
		/* VALIDATE */
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		/*OTHER*/
	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	@Override
	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isEQ(wsaaUpdateFlag, "Y")) {
			updatePrimaryRecord3050();
			updateRecord3055();
		}
	}

	protected void updatePrimaryRecord3050() {
		itemIO.setFunction(Varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

	protected void updateRecord3055() {
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(thsuzrec.thsuzRec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

	protected void checkChanges3100() {
		/* CHECK */
		if (isNE(sv.flag, thsuzrec.flag)) {
			thsuzrec.flag.set(sv.flag);
			wsaaUpdateFlag = "Y";
		}
		/* EXIT */
	}

	/**
	 * <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	@Override
	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}


}
