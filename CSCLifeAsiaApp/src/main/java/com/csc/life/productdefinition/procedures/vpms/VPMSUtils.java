package com.csc.life.productdefinition.procedures.vpms;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.utility.ConversionUtil;
import com.csc.smart400framework.utility.ExitSection;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class VPMSUtils {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(VPMSUtils.class);
	
	public static final String MOTOR_QUOTE = "MOTOR_QUOTE";
	public static final String MUANG_THAI = "Muang_Thai";
	public static final String TRAD_ENDOWMENT = "Trad_Endowment";
	public static final String LIFE_TRM = "Life_TRM";
	public static final String LIFE_RUL = "Life_RUL";
	public static final String LIFE_INTBEARING = "Life_IntBearing";
	public static final Map<String, String> modelMap = new HashMap<String, String>();

	public static final List<String> SUPPORTED_RECORDS = Arrays.asList("PREMIUMREC", "VPMFMTREC", "MGFEELREC",
							"TXCALCREC", "POLCALCREC", "BONUSREC", "DEATHREC", "EXTPRMREC",
							"HDIVDREC", "HDVDCWCREC", "HDVDINTREC", "IBINCALREC", "INCRSREC",
							"MATCCPY", "OVRDUEREC", "PR676CPY", "PRASREC", "RLRDTRMREC",
							"RWCALCPY", "SRCALCPY", "STDTALLREC", "TAXCPY", "TSDTCALREC",
							"UBBLALLPAR", "VCALCPY", "VSTAREC", "VSTCCPY", "ACTCALCREC",
							"INTCALCREC", "VPXMATCREC");
	
	public VPMSUtils() {

	}

	public static String formatYearManufacture(Double year) {
		String result = String.valueOf(year.intValue());
		return result;
	}

	public static String formatInceptionDate(Double date) throws ParseException {
		String result = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat inceptionDate = new SimpleDateFormat("dd.MM.yyyy");
		Date ddate = simpleDateFormat.parse(String.valueOf(date.intValue()));
		result = inceptionDate.format(ddate);
		return result;
	}
	
	public static String formatDate(String date) throws ParseException {
		String result = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat inceptionDate = new SimpleDateFormat("dd.MM.yyyy");
		Date ddate = simpleDateFormat.parse(date);
		result = inceptionDate.format(ddate);
		return result;
	}
	
	public static String[] separateNameIndex(String field){
		if (field.indexOf('[') == -1){
			return new String[]{field, "-1"};
		}
		String fieldName = field.substring(0, field.indexOf('['));
		String index = field.substring(field.indexOf('[') + 1, field.indexOf(']') );
		return new String[]{fieldName, index};
	}
	
	public static Double parsePremium(String value) {
	    Double result = new Double(0.0);
	    if (StringUtils.isNotEmpty(value)) {
	        try {
	            result = Double.valueOf(value);
	        } catch (Exception e) {
	            LOGGER.error("", e);
	        }
	    }
	    return result;
	}

	/*public static void serializeMap(Map<String, String> calcMap, String fileName)
			throws Exception {
		FileOutputStream fos = new FileOutputStream(fileName);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(calcMap);
		oos.close();
	}*/

	
	/*public static Map<String, String> readMap(String fileName) throws Exception {
		Map<String, String> calcMap = null;
		FileInputStream fis = new FileInputStream(fileName);
		ObjectInputStream ois = new ObjectInputStream(fis);
		calcMap = (Map<String, String>) ois.readObject();
		return calcMap;
	}*/
	
	public static  Map<String, ComputeResult>  callVPMSAPI(COBOLAppVars appVars, Map<String, String> inputs, 
									Map<String, String> calcMap, String calcModel, Collection<String> outputs) throws ExitSection{
		modelMap.put("LifeRUL", LIFE_RUL);
		modelMap.put("LifeTRM", LIFE_TRM);
		modelMap.put("LifeIntBearing", LIFE_INTBEARING);
		
		String vpmsUrl = appVars.getAppConfig().vpmsUrl;
		
		IVpmsCalculator vpmsCalculator = new VpmsWebServiceProviderImpl(
												modelMap.get(calcModel), vpmsUrl);
		
		Map<String, ComputeResult> results = null;
		
		try {
			LOGGER.debug("Call VPMS Service!!!!!!!!!");
			LOGGER.debug("====Input values:");
			LOGGER.debug(inputs.toString());
			LOGGER.debug("==========================");
			results = vpmsCalculator.compute(inputs, null, outputs);
		} catch (Exception e) {
			LOGGER.error("VPMS call error");
			throw new ExitSection();
		}
		
		LOGGER.debug("Return values: ");		
		for (ComputeResult outEntry : results.values()) {
			LOGGER.debug("===== Key : {}, value: {}", outEntry.getName(), outEntry.getValue());//IJTI-1561
		}
		
		return results;				
	}
	
	public static void updateRecordField(Object recordObj, String field, Object value){
	    try {
	        Class cls = recordObj.getClass();
	        Field fld = cls.getField(field);
	        Class fieldClass = fld.getType();
	        if (fieldClass.equals(FixedLengthStringData.class)){
	        	FixedLengthStringData fldObj = (FixedLengthStringData)fld.get(recordObj);
	        	fldObj.set(ConversionUtil.toFixedLengthString(value.toString().trim()));
	        }else if (fieldClass.equals(PackedDecimalData.class)){
	        	PackedDecimalData fldObj = (PackedDecimalData)fld.get(recordObj);
	        	fldObj.set(toPackedDecimal(fldObj.getPrecision(), fldObj.getScale(), value.toString().trim()));
	        } else if (fieldClass.equals(ZonedDecimalData.class)) {
	        	ZonedDecimalData fldObj = (ZonedDecimalData)fld.get(recordObj);
	        	fldObj.set(toZonedDecimal(fldObj.getPrecision(), fldObj.getScale(), value.toString().trim()));
	        }
	     }
	     catch (Throwable e) {
	    	 LOGGER.error("Unable to return value");	
	     }
	}
	
	public static Object returnValue(Object value){
        Class cls = value.getClass();
        if (cls.equals(ZonedDecimalData.class)) {
        	return ((ZonedDecimalData)value).getbigdata().toString();
        } 
        return value;
	}
	
	public static PackedDecimalData toPackedDecimal(int pre, int scale, String value) {
		if (value == null) {
			return new PackedDecimalData(0);
		} else {
			return new PackedDecimalData(pre, scale).init(value);
		}
	}
	
	public static ZonedDecimalData toZonedDecimal(int precision, int scale, String value) {
		if (value == null) {
			return new ZonedDecimalData(0);
		} else {
			return new ZonedDecimalData(precision, scale).init(value);
		}
	}
	
	public static Object getRecordField(Object record, String field){
		try {
			Class cls = record.getClass();
			Field fld = cls.getField(field);
			return fld.get(record);
		}
		catch (Throwable e) {
			LOGGER.error("{} doesn't exist in {}", field, record.toString());//IJTI-1561
		}
		return null;
		
	}
	
	public static boolean checkFieldExist(Object recordObj, String field){
		if (recordObj != null){
			try {
				Field fld = recordObj.getClass().getField(field);
				return true;
			} catch (NoSuchFieldException e) {
				return false;
			}
			//IJTI-851-Overly Broad Catch
			catch (SecurityException e) {
				LOGGER.error("Unable to check existance of object field ", field);//IJTI-1561
				return false;
			}
		}
		return false;

	}
	
	public static void setSubrFunction(Object record, Object value){
		updateRecordField(record,"function", value );
	}
	
	public static void setSubrCopybook(Object record, Object value){
		updateRecordField(record,"copybook", value );
	}
	
	public static void setSubrResultField(Object record, Object value){
		updateRecordField(record,"resultField", value );
	}
	
	public static void setSubrResultValue(Object record, Object value){
		updateRecordField(record,"resultValue", value );
	}
	
	public static Object getSubrResultValue(Object record){
		return getRecordField(record, "resultValue");
	}
	
	public static Object getSubrStatuz(Object record){
		return getRecordField(record, "statuz");
	}

}
