/*
 * File: Vlpdsa03.java
 * Date: 30 August 2009 2:54:02
 * Author: Quipoz Limited
 * 
 * Class transformed from VLPDSA03.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51crec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Subroutine VLPDSA03.
* ====================
*
*      This program will validate component's sum assured must be in
*      multiples of X amount as specified in TR51C
*
*      - Read table TR51C, using VLSB-CNTTYPE + VLSB-RID1-CRTABLE  as key
*      - Get Life Date of birth
*      - Calculate age by using call 'AGECALC'
*      - Validate the limit according to Table TR51C
*
***********************************************************************
* </pre>
*/
public class Vlpdsa03 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VLPDSA03";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaVlsbSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr51cSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaInputAllSpace = new FixedLengthStringData(1);
	private int wsaaMaxRow = 12;
	private int wsaaMaxInput = 50;
	private ZonedDecimalData wsaaBasicSa = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaCalcSa = new ZonedDecimalData(17, 2);

	private FixedLengthStringData filler = new FixedLengthStringData(17).isAPartOf(wsaaCalcSa, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaReminder = new ZonedDecimalData(2, 2).isAPartOf(filler, 15);

	private FixedLengthStringData wsaaTr51cItmkey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr51cCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr51cItmkey, 0).init(SPACES);
	private FixedLengthStringData wsaaTr51cComponent = new FixedLengthStringData(4).isAPartOf(wsaaTr51cItmkey, 3).init(SPACES);

	private FixedLengthStringData wsaaTr51cArray = new FixedLengthStringData(240);
	private FixedLengthStringData[] wsaaTr51cRec = FLSArrayPartOfStructure(12, 20, wsaaTr51cArray, 0);
	private ZonedDecimalData[] wsaaTr51cAge = ZDArrayPartOfArrayStructure(3, 0, wsaaTr51cRec, 0);
	private ZonedDecimalData[] wsaaTr51cSumins = ZDArrayPartOfArrayStructure(17, 2, wsaaTr51cRec, 3);
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String lifelnbrec = "LIFELNBREC";
		/* TABLES */
	private String tr51c = "TR51C";
	private String t1693 = "T1693";
		/* ERRORS */
	private String rlba = "RLBA";
	private Agecalcrec agecalcrec = new Agecalcrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private Tr51crec tr51crec = new Tr51crec();
	private Varcom varcom = new Varcom();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();
	private ZonedDecimalData wsaaCalcSa1 = new ZonedDecimalData(20, 5);
	private FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(wsaaCalcSa1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaReminder1 = new ZonedDecimalData(5, 5).isAPartOf(filler1, 15);
	private boolean NBPRP115Permission = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		loadTr51c180, 
		exit190, 
		exit490, 
		exit590, 
		exit690
	}

	public Vlpdsa03() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		try {
			begin010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		if (isEQ(vlpdsubrec.rid1Life,SPACES)
		&& isEQ(vlpdsubrec.rid1Jlife,SPACES)
		&& isEQ(vlpdsubrec.rid1Coverage,SPACES)
		&& isEQ(vlpdsubrec.rid1Rider,SPACES)
		&& isEQ(vlpdsubrec.rid1Crtable,SPACES)) {
			goTo(GotoLabel.exit090);
		}
		readTables100();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit090);
		}
		wsaaErrSeq.set(1);
		readLife200();
		validation300();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTables100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin110();
					contractTypeComponent120();
					xxxComponent130();
				}
				case loadTr51c180: {
					loadTr51c180();
				}
				case exit190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(vlpdsubrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
	}

protected void contractTypeComponent120()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51c);
		wsaaTr51cCnttype.set(vlpdsubrec.cnttype);
		wsaaTr51cComponent.set(vlpdsubrec.rid1Crtable);
		itemIO.setItemitem(wsaaTr51cItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.loadTr51c180);
		}
	}

protected void xxxComponent130()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51c);
		wsaaTr51cCnttype.set("***");
		wsaaTr51cComponent.set(vlpdsubrec.rid1Crtable);
		itemIO.setItemitem(wsaaTr51cItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
	}

protected void loadTr51c180()
	{
		tr51crec.tr51cRec.set(itemIO.getGenarea());
		for (wsaaTr51cSeq.set(1); !(isGT(wsaaTr51cSeq,wsaaMaxRow)); wsaaTr51cSeq.add(1)){
			wsaaTr51cAge[wsaaTr51cSeq.toInt()].set(tr51crec.age[wsaaTr51cSeq.toInt()]);
			wsaaTr51cSumins[wsaaTr51cSeq.toInt()].set(tr51crec.sumins[wsaaTr51cSeq.toInt()]);
		}
	}

protected void readLife200()
	{
		begin210();
	}

protected void begin210()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(vlpdsubrec.chdrcoy);
		lifelnbIO.setChdrnum(vlpdsubrec.chdrnum);
		lifelnbIO.setLife(vlpdsubrec.rid1Life);
		lifelnbIO.setJlife(vlpdsubrec.rid1Jlife);
		if (isEQ(vlpdsubrec.rid1Jlife,SPACES)) {
			lifelnbIO.setJlife("00");
		}
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

protected void validation300()
	{
		begin310();
	}

protected void begin310()
	{
		wsaaInputAllSpace.set(SPACES);
		for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq,wsaaMaxInput)
		|| isEQ(wsaaInputAllSpace,"Y")); wsaaVlsbSeq.add(1)){
			sumassured400();
		}
		initialize(agecalcrec.agecalcRec);
		agecalcrec.statuz.set(varcom.oK);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(vlpdsubrec.language);
		agecalcrec.cnttype.set(vlpdsubrec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(vlpdsubrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaErrCode.set(SPACES);
		for (wsaaTr51cSeq.set(1); !(isGT(wsaaTr51cSeq,wsaaMaxRow)); wsaaTr51cSeq.add(1)){
			if (isLTE(agecalcrec.agerating,wsaaTr51cAge[wsaaTr51cSeq.toInt()])) {
				chkSaLimit500();
				wsaaTr51cSeq.set(wsaaMaxRow);
			}
		}
	}

protected void sumassured400()
	{
		try {
			begin410();
		}
		catch (GOTOException e){
		}
	}

protected void begin410()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()],SPACES)) {
			wsaaInputAllSpace.set("Y");
			goTo(GotoLabel.exit490);
		}
		if (isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()],vlpdsubrec.rid1Crtable)) {
			wsaaBasicSa.set(vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]);
			wsaaVlsbSeq.set(wsaaMaxInput);
		}
	}

protected void chkSaLimit500()
	{
		try {
			begin510();
		}
		catch (GOTOException e){
		}
	}

protected void begin510()
	{
	NBPRP115Permission = FeaConfg.isFeatureExist("2", "NBPRP115", appVars, "IT");
	if(NBPRP115Permission) {
		compute(wsaaCalcSa1, 5).set(div(wsaaBasicSa,wsaaTr51cSumins[wsaaTr51cSeq.toInt()]));
		if (isNE(wsaaReminder1,0)) {
			wsaaErrCode.set(rlba);
		}
		else {
			goTo(GotoLabel.exit590);
		}
	}else {
		compute(wsaaCalcSa, 2).set(div(wsaaBasicSa,wsaaTr51cSumins[wsaaTr51cSeq.toInt()]));
		if (isNE(wsaaReminder,0)) {
			wsaaErrCode.set(rlba);
		}
		else {
			goTo(GotoLabel.exit590);
		}
	}
		vlpdsubrec.errCode[wsaaErrSeq.toInt()].set(wsaaErrCode);
		wsaaErrSeq.add(1);
	}

protected void fatalError600()
	{
		try {
			fatalErrors610();
		}
		catch (GOTOException e){
		}
		finally{
			exit690();
		}
	}

protected void fatalErrors610()
	{
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit690()
	{
		exitProgram();
	}
}
