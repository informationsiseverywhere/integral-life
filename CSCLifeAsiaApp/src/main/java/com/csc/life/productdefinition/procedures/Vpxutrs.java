package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrssurTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Rwcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Vpxutrs extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private String wsaaSubr = "VPXUTRS";
	private ZonedDecimalData ia = new ZonedDecimalData(5);
	private ZonedDecimalData ib = new ZonedDecimalData(5);
	private FixedLengthStringData wsaaStart = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaVariables = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(1).isAPartOf(wsaaVariables, 0);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).isAPartOf(wsaaVariables, 1);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).isAPartOf(wsaaVariables, 2);
	private FixedLengthStringData wsaaFileStatuz = new FixedLengthStringData(4).isAPartOf(wsaaVariables, 6).init(SPACES);


	private ZonedDecimalData wsaaccdate = new ZonedDecimalData(8);

	
	private FixedLengthStringData wsaaPlanSwitch = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaPlanSwitch,"1");
	private Validator partPlan = new Validator(wsaaPlanSwitch,"2");
	private Validator summaryPart = new Validator(wsaaPlanSwitch,"3");
	private FixedLengthStringData wsaaT5515Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5515Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5515Key, 4);
	private String wscc12 = "12";
	private String wsccA = "A";
	private String wsccC = "C";
	private String wsccI = "I";
	private String wsccY = "Y";
	private String e049 = "E049";
	private String g094 = "G094";
	private String h832 = "H832";
	private String t5515 = "T5515";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String utrsclmrec = "UTRSCLMREC";
	private String utrssurrec = "UTRSSURREC";

	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
	private UtrssurTableDAM utrssurIO = new UtrssurTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Rwcalcpy rwcalcpy = new Rwcalcpy();
	private Vpxutrsrec vpxutrsrec = new Vpxutrsrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();

	public Vpxutrs() {
		super();
	}
	
	public void mainline(Object... parmArray){		
		vpxutrsrec = (Vpxutrsrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		rwcalcpy.withdrawRec.set(vpmcalcrec.linkageArea);
		syserrrec.subrname.set(wsaaSubr);
		vpxutrsrec.statuz.set(varcom.oK);
		rwcalcpy.status.set(varcom.oK);
		try{
			if (isEQ(vpxutrsrec.function,"INIT")){
				initialize050();
				findUtrs100();
			}else if (isEQ(vpxutrsrec.function,"GETV")){
				returnValue200();
			}else {
				syserrrec.statuz.set(e049);
				vpxutrsrec.statuz.set(e049);
				systemError900();
			}
		}catch (COBOLExitProgramException e) {
		}catch (Exception e) {
 			systemError900();
 		}
		vpmcalcrec.linkageArea.set(rwcalcpy.withdrawRec);
	}

	private void initialize050(){
		vpxutrsrec.curduntbal.set(ZERO);
		vpxutrsrec.ccdate.set(ZERO);
		vpxutrsrec.frequency.set(SPACES);
	}
	
	private void findUtrs100(){

		if(isEQ(wsaaFileStatuz,varcom.endp)){
			setFlags150();
			return;

		}
		wsaaFileStatuz.set(varcom.oK);
		rwcalcpy.endf.set(SPACES);
		wsaaPlanSwitch.set(rwcalcpy.planType);
		if(wholePlan.isTrue()){
			initialize(utrssurIO.getDataArea());
			if(isEQ(rwcalcpy.function,varcom.begn)){
				rwcalcpy.function.set(varcom.nextr);
				utrssurIO.setUnitVirtualFund(SPACES);			
				utrssurIO.setUnitType(SPACES);
			}else{
				utrssurIO.setUnitVirtualFund(wsaaVirtualFund);
				utrssurIO.setUnitType(wsaaUnitType);
			}
			utrssurIO.setFunction(varcom.begn);
			utrssurIO.setChdrcoy(rwcalcpy.chdrChdrcoy);
			utrssurIO.setChdrnum(rwcalcpy.chdrChdrnum);
			utrssurIO.setPlanSuffix(rwcalcpy.planSuffix);
			utrssurIO.setLife(rwcalcpy.lifeLife);
			utrssurIO.setCoverage(rwcalcpy.covrCoverage);
			utrssurIO.setRider(rwcalcpy.covrRider);
			callUtrssurIOX100();
			processUtrssur110();
		}else{
			initialize(utrsclmIO.getDataArea());
			if(isEQ(rwcalcpy.function,varcom.begn)){
				rwcalcpy.function.set(varcom.nextr);
				utrsclmIO.setUnitVirtualFund(SPACES);
				utrsclmIO.setUnitType(SPACES);
			}else{
				utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
				utrsclmIO.setUnitType(wsaaUnitType);
			}
			utrsclmIO.setFunction(varcom.begn);
			utrsclmIO.setChdrcoy(rwcalcpy.chdrChdrcoy);
			utrsclmIO.setChdrnum(rwcalcpy.chdrChdrnum);
			utrsclmIO.setLife(rwcalcpy.lifeLife);
			utrsclmIO.setCoverage(rwcalcpy.covrCoverage);
			utrsclmIO.setRider(rwcalcpy.covrRider);
			utrsclmIO.setPlanSuffix(rwcalcpy.planSuffix);
			callUtrsclmIOX200();
			processUtrsclm130();
		}
	}
	
	private void processUtrssur110(){
		rwcalcpy.element.set(utrssurIO.getUnitVirtualFund());
		rwcalcpy.fund.set(utrssurIO.getUnitVirtualFund());
		rwcalcpy.type.set(wsaaType);

		while ( !(isNE(utrssurIO.getStatuz(),varcom.oK)
				|| isNE(utrssurIO.getUnitVirtualFund(),rwcalcpy.fund)
				|| isNE(wsaaType,rwcalcpy.type))) {
			
			vpxutrsrec.curduntbal.add(utrssurIO.getCurrentDunitBal());			
			utrssurIO.setFunction(varcom.nextr);
			
			callUtrssurIOX100();
		}
		
		wsaaVirtualFund.set(utrssurIO.getUnitVirtualFund());
		wsaaUnitType.set(utrssurIO.getUnitType());
		item140();
	}	
	
	private void processUtrsclm130(){
		rwcalcpy.element.set(utrsclmIO.getUnitVirtualFund());
		rwcalcpy.fund.set(utrsclmIO.getUnitVirtualFund());
		rwcalcpy.type.set(wsaaType);
		
		while ( !(isNE(utrsclmIO.getStatuz(),varcom.oK)
				|| isNE(utrsclmIO.getUnitVirtualFund(),rwcalcpy.fund)
				|| isNE(wsaaType,rwcalcpy.type))) {
			vpxutrsrec.curduntbal.add(utrsclmIO.getCurrentDunitBal());			
			utrssurIO.setFunction(varcom.nextr);
			callUtrsclmIOX200();
		}
		wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsclmIO.getUnitType());
		item140();
	}
	
	private void callUtrssurIOX100(){
		utrssurIO.setFormat(utrssurrec);
		SmartFileCode.execute(appVars, utrssurIO);
	
		if(!isEQ(utrssurIO.getStatuz(), varcom.oK) && !isEQ(utrssurIO.getStatuz(), varcom.endp)){
			syserrrec.statuz.set(utrssurIO.getStatuz());
			syserrrec.params.set(utrssurIO.getParams());
			dbError999();
		}

		if(isEQ(utrssurIO.getStatuz(), varcom.endp)||!isEQ(utrssurIO.getChdrcoy(), rwcalcpy.chdrChdrcoy)||
				!isEQ(utrssurIO.getChdrnum(), rwcalcpy.chdrChdrnum)||!isEQ(utrssurIO.getLife(), rwcalcpy.lifeLife)||
				!isEQ(utrssurIO.getCoverage(), rwcalcpy.covrCoverage)||!isEQ(utrssurIO.getRider(), rwcalcpy.covrRider)){
			utrssurIO.setStatuz(varcom.endp);
			wsaaFileStatuz.set(varcom.endp);
			return;
		}

		if(isEQ(utrssurIO.getUnitType(), wsccI)){
			wsaaType.set(wsccI);
		}else{
			wsaaType.set(wsccA);
		}

	}
	
	private void callUtrsclmIOX200(){
		utrsclmIO.setFormat(utrsclmrec);
		SmartFileCode.execute(appVars, utrsclmIO);

		if(!isEQ(utrsclmIO.getStatuz(), varcom.oK)&&!isEQ(utrsclmIO.getStatuz(), varcom.endp)){
			syserrrec.statuz.set(utrsclmIO.getStatuz());
			syserrrec.params.set(utrsclmIO.getParams());
			dbError999();
		}

		if(isEQ(utrsclmIO.getStatuz(), varcom.endp)||!isEQ(utrsclmIO.getChdrcoy(), rwcalcpy.chdrChdrcoy)||
				!isEQ(utrsclmIO.getChdrnum(), rwcalcpy.chdrChdrnum)||!isEQ(utrsclmIO.getLife(), rwcalcpy.lifeLife)||
				!isEQ(utrsclmIO.getCoverage(), rwcalcpy.covrCoverage)||!isEQ(utrsclmIO.getRider(), rwcalcpy.covrRider)||
				!isEQ(utrsclmIO.getPlanSuffix(), rwcalcpy.planSuffix)){
			utrsclmIO.setStatuz(varcom.endp);
			wsaaFileStatuz.set(varcom.endp);
			return;
		}

		if(isEQ(utrsclmIO.getUnitType(),wsccI)){
			wsaaType.set(wsccI);
		}else{
			wsaaType.set(wsccA);
		}
	}
	
	private void item140(){

		initialize(itdmIO.getDataArea());
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		wsaaT5515Key.set(SPACES);
		wsaaT5515Fund.set(rwcalcpy.fund);
		itdmIO.setItemitem(wsaaT5515Key);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);

		if(!isEQ(itdmIO.getStatuz(),varcom.oK) && !isEQ(itdmIO.getStatuz(), varcom.endp)){
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError999();
		}


		if(!isEQ(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)||!isEQ(itdmIO.getItemtabl(),t5515)||
				!isEQ(itdmIO.getItemitem(), wsaaT5515Key)||!isEQ(itdmIO.getStatuz(), varcom.oK)){
			syserrrec.statuz.set(itdmIO.getStatuz());
			vpxutrsrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			systemError900();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		rwcalcpy.fundCurrcode.set(t5515rec.currcode);
		initialize(descIO.getDataArea());
		descIO.setDescitem(rwcalcpy.fund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(rwcalcpy.chdrChdrcoy);
		descIO.setLanguage(rwcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);

		if(!isEQ(descIO.getStatuz(), varcom.oK) && !isEQ(descIO.getStatuz(), varcom.mrnf)){
			syserrrec.statuz.set(descIO.getStatuz());
			vpxutrsrec.statuz.set(descIO.getStatuz());
			rwcalcpy.status.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			systemError900();
		}

		if(isEQ(descIO.getStatuz(), varcom.oK)){
			rwcalcpy.description.set(descIO.getLongdesc());
		}else{
			rwcalcpy.description.set(SPACES);
		}
	}
	
	private void setFlags150(){
		vpxutrsrec.statuz.set(varcom.endp);
		rwcalcpy.status.set(varcom.endp);
		rwcalcpy.type.set(wsccC);
		rwcalcpy.endf.set(wsccY);				
		wsaaccdate.set(rwcalcpy.crrcd);
		vpxutrsrec.ccdate.set(wsaaccdate);
		vpxutrsrec.frequency.set(wscc12);
		vpxutrsrec.curduntbal.set(ZERO);
		wsaaFileStatuz.set(SPACES);
		wsaaVirtualFund.set(SPACES);
		wsaaUnitType.set(SPACES);
	}
	
	private void returnValue200(){
		vpxutrsrec.resultValue.set(SPACES);
		if (isEQ(vpxutrsrec.resultField,"curduntbal")){		
			vpxutrsrec.resultValue.set(vpxutrsrec.curduntbal.getbigdata().toString());
		}else if (isEQ(vpxutrsrec.resultField,"ccdate")){			
			vpxutrsrec.resultValue.set(vpxutrsrec.ccdate.getData().toString());
		}else if (isEQ(vpxutrsrec.resultField,"frequency")){
			vpxutrsrec.resultValue.set(vpxutrsrec.frequency);
		}else {
			vpxutrsrec.statuz.set(h832);
		}
	}
	
	protected void dbError999()
	{
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);		
		exitProgram();
	}
	
	protected void systemError900(){
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);		
		exitProgram();
	}
}
