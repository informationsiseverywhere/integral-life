package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:45
 * Description:
 * Copybook name: MLIANAMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mlianamkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mlianamFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mlianamKey = new FixedLengthStringData(64).isAPartOf(mlianamFileKey, 0, REDEFINE);
  	public FixedLengthStringData mlianamClntnaml = new FixedLengthStringData(51).isAPartOf(mlianamKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(mlianamKey, 51, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mlianamFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mlianamFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}