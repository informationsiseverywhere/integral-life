package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:44
 * Description:
 * Copybook name: MEDIREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Medirevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData medirevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData medirevKey = new FixedLengthStringData(256).isAPartOf(medirevFileKey, 0, REDEFINE);
  	public FixedLengthStringData medirevChdrcoy = new FixedLengthStringData(1).isAPartOf(medirevKey, 0);
  	public FixedLengthStringData medirevChdrnum = new FixedLengthStringData(8).isAPartOf(medirevKey, 1);
  	public PackedDecimalData medirevTranno = new PackedDecimalData(5, 0).isAPartOf(medirevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(medirevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(medirevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		medirevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}