package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import com.quipoz.framework.datatype.ZonedDecimalData;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Td5h7rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData td5h7Rec = new FixedLengthStringData(500);

	public ZonedDecimalData intrate = new ZonedDecimalData(8, 5).isAPartOf(td5h7Rec, 0);
	public FixedLengthStringData intcalfreq = new FixedLengthStringData(2).isAPartOf(td5h7Rec, 8);
	public FixedLengthStringData intcapfreq = new FixedLengthStringData(2).isAPartOf(td5h7Rec, 10);
	
	public FixedLengthStringData filler = new FixedLengthStringData(488).isAPartOf(td5h7Rec, 12, FILLER);

	public void initialize() {
		COBOLFunctions.initialize(td5h7Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			td5h7Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}
