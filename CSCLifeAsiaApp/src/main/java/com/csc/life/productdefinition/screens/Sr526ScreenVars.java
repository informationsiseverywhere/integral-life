package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR526
 * @version 1.0 generated on 30/08/09 07:18
 * @author Quipoz
 */
public class Sr526ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(31);
	public FixedLengthStringData dataFields = new FixedLengthStringData(15).isAPartOf(dataArea, 0);
	public FixedLengthStringData zdocno = DD.zdocno.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(4).isAPartOf(dataArea, 15);
	public FixedLengthStringData zdocnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 19);
	public FixedLengthStringData[] zdocnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(146);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(64).isAPartOf(subfileArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData exmcode = DD.exmcode.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData invref = DD.invref.copy().isAPartOf(subfileFields,18);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,33);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,63);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 64);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData exmcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData invrefErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 84);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] exmcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] invrefOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 144);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr526screensflWritten = new LongData(0);
	public LongData Sr526screenctlWritten = new LongData(0);
	public LongData Sr526screenWritten = new LongData(0);
	public LongData Sr526windowWritten = new LongData(0);
	public LongData Sr526hideWritten = new LongData(0);
	public LongData Sr526protectWritten = new LongData(0);
	public GeneralTable sr526screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr526screensfl;
	}

	public Sr526ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"12",null, "-12","13",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, invref, chdrnum, exmcode, longdesc};
		screenSflOutFields = new BaseData[][] {selectOut, invrefOut, chdrnumOut, exmcodeOut, longdescOut};
		screenSflErrFields = new BaseData[] {selectErr, invrefErr, chdrnumErr, exmcodeErr, longdescErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {zdocno};
		screenOutFields = new BaseData[][] {zdocnoOut};
		screenErrFields = new BaseData[] {zdocnoErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr526screen.class;
		screenSflRecord = Sr526screensfl.class;
		screenCtlRecord = Sr526screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr526protect.class;
		hideRecord = Sr526hide.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr526screenctl.lrec.pageSubfile);
	}
}
