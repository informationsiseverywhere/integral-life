package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:04
 * Description:
 * Copybook name: VLPDRULREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vlpdrulrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData validRec = new FixedLengthStringData(12095);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(validRec, 0);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(validRec, 4);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(validRec, 8);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(validRec, 12);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(validRec, 13);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(validRec, 21).setUnsigned();
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(validRec, 29);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(validRec, 30);
  	public FixedLengthStringData srcebus = new FixedLengthStringData(2).isAPartOf(validRec, 33);
  	public FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(validRec, 35);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(validRec, 43);
  	public FixedLengthStringData inputs = new FixedLengthStringData(35).isAPartOf(validRec, 45);
  	public FixedLengthStringData inputLife = new FixedLengthStringData(2).isAPartOf(inputs, 0);
  	public FixedLengthStringData inputJlife = new FixedLengthStringData(2).isAPartOf(inputs, 2);
  	public FixedLengthStringData inputCoverage = new FixedLengthStringData(2).isAPartOf(inputs, 4);
  	public FixedLengthStringData inputRider = new FixedLengthStringData(2).isAPartOf(inputs, 6);
  	public FixedLengthStringData inputCrtable = new FixedLengthStringData(4).isAPartOf(inputs, 8);
  	public ZonedDecimalData inputRtrm = new ZonedDecimalData(3, 0).isAPartOf(inputs, 12).setUnsigned();
  	public ZonedDecimalData inputPtrm = new ZonedDecimalData(3, 0).isAPartOf(inputs, 15).setUnsigned();
  	public ZonedDecimalData inputSa = new ZonedDecimalData(17, 2).isAPartOf(inputs, 18).setUnsigned();
  	public FixedLengthStringData[] arrayInputs = FLSArrayPartOfStructure(99, 35, validRec, 80);
  	public FixedLengthStringData[] arrayLife = FLSDArrayPartOfArrayStructure(2, arrayInputs, 0);
  	public FixedLengthStringData[] arrayJlife = FLSDArrayPartOfArrayStructure(2, arrayInputs, 2);
  	public FixedLengthStringData[] arrayCoverage = FLSDArrayPartOfArrayStructure(2, arrayInputs, 4);
  	public FixedLengthStringData[] arrayRider = FLSDArrayPartOfArrayStructure(2, arrayInputs, 6);
  	public FixedLengthStringData[] arrayCrtable = FLSDArrayPartOfArrayStructure(4, arrayInputs, 8);
  	public ZonedDecimalData[] arrayRtrm = ZDArrayPartOfArrayStructure(3, 0, arrayInputs, 12, UNSIGNED_TRUE);
  	public ZonedDecimalData[] arrayPtrm = ZDArrayPartOfArrayStructure(3, 0, arrayInputs, 15, UNSIGNED_TRUE);
  	public ZonedDecimalData[] arraySa = ZDArrayPartOfArrayStructure(17, 2, arrayInputs, 18, UNSIGNED_TRUE);
  	public FixedLengthStringData errors = new FixedLengthStringData(8550).isAPartOf(validRec, 3545);
  	public FixedLengthStringData[] error = FLSArrayPartOfStructure(50, 171, errors, 0);
  	public FixedLengthStringData[] errCnttype = FLSDArrayPartOfArrayStructure(3, error, 0);
  	public FixedLengthStringData[] errLife = FLSDArrayPartOfArrayStructure(2, error, 3);
  	public FixedLengthStringData[] errJlife = FLSDArrayPartOfArrayStructure(2, error, 5);
  	public FixedLengthStringData[] errCoverage = FLSDArrayPartOfArrayStructure(2, error, 7);
  	public FixedLengthStringData[] errRider = FLSDArrayPartOfArrayStructure(2, error, 9);
  	public FixedLengthStringData[][] errDetails = FLSDArrayPartOfArrayStructure(20, 8, error, 11);
  	public FixedLengthStringData[][] errCode = FLSDArrayPartOfArrayStructure(4, errDetails, 0);
  	public FixedLengthStringData[][] errDet = FLSDArrayPartOfArrayStructure(4, errDetails, 4);


	public void initialize() {
		COBOLFunctions.initialize(validRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		validRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}