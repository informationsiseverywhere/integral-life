package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:37
 * Description:
 * Copybook name: T5659REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5659rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5659Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData rndfact = new ZonedDecimalData(6, 0).isAPartOf(t5659Rec, 0);
  	public FixedLengthStringData volbanfrs = new FixedLengthStringData(60).isAPartOf(t5659Rec, 6);
  	public ZonedDecimalData[] volbanfr = ZDArrayPartOfStructure(4, 15, 0, volbanfrs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(volbanfrs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData volbanfr01 = new ZonedDecimalData(15, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData volbanfr02 = new ZonedDecimalData(15, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData volbanfr03 = new ZonedDecimalData(15, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData volbanfr04 = new ZonedDecimalData(15, 0).isAPartOf(filler, 45);
  	public FixedLengthStringData volbanles = new FixedLengthStringData(20).isAPartOf(t5659Rec, 66);
  	public ZonedDecimalData[] volbanle = ZDArrayPartOfStructure(4, 5, 0, volbanles, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(volbanles, 0, FILLER_REDEFINE);
  	public ZonedDecimalData volbanle01 = new ZonedDecimalData(5, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData volbanle02 = new ZonedDecimalData(5, 0).isAPartOf(filler1, 5);
  	public ZonedDecimalData volbanle03 = new ZonedDecimalData(5, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData volbanle04 = new ZonedDecimalData(5, 0).isAPartOf(filler1, 15);
  	public FixedLengthStringData volbantos = new FixedLengthStringData(60).isAPartOf(t5659Rec, 86);
  	public ZonedDecimalData[] volbanto = ZDArrayPartOfStructure(4, 15, 0, volbantos, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(volbantos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData volbanto01 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData volbanto02 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 15);
  	public ZonedDecimalData volbanto03 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 30);
  	public ZonedDecimalData volbanto04 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 45);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(354).isAPartOf(t5659Rec, 146, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5659Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5659Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}