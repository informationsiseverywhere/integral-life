package com.csc.life.productdefinition.dataaccess.model;

public class Br64xDTO {
	private long chdrUniqueNo;
	private int chdrtranno;
	private String chdrpfx;
	private String cnttype;
	private String cntcurr;
	private int occdate;
	private int ptdate;
	private int paydte;
	private String actind;
	private int meditranno;
	private long mediUniqueNo;
	
	public long getChdrUniqueNo() {
		return chdrUniqueNo;
	}
	public void setChdrUniqueNo(long chdrUniqueNo) {
		this.chdrUniqueNo = chdrUniqueNo;
	}
	
	public int getChdrtranno() {
		return chdrtranno;
	}
	public void setChdrtranno(int chdrtranno) {
		this.chdrtranno = chdrtranno;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public int getPaydte() {
		return paydte;
	}
	public void setPaydte(int paydte) {
		this.paydte = paydte;
	}
	public String getActind() {
		return actind;
	}
	public void setActind(String actid) {
		this.actind = actid;
	}
	public int getMeditranno() {
		return meditranno;
	}
	public void setMeditranno(int meditranno) {
		this.meditranno = meditranno;
	}
	public long getMediUniqueNo() {
		return mediUniqueNo;
	}
	public void setMediUniqueNo(long mediUniqueNo) {
		this.mediUniqueNo = mediUniqueNo;
	}
}
