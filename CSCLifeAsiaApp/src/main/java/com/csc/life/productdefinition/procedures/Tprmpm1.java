/*
 * File: Tprmpm1.java
 * Date: 30 August 2009 2:39:07
 * Author: Quipoz Limited
 * 
 * Class transformed from TPRMPM1.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.life.productdefinition.tablestructures.Tt502rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  PREMIUM CALCULATION METHOD 07 -
*  ACCIDENTAL HOSPITALIZATION PREMIUM CALCULATION SUBROUTINE
*
*
* PROCESSING.
* ----------
*
* Initialise all working storage fields and set keys to read
* tables. Include a table (occurs 8) to hold the Options/Extras
* (LEXT) record details.
*
* Build a key.  This key (see below) will read table TT502.
* This table contains the parameters to be used in the
* calculation of the Basic and Additional Premium for
* Coverage/Rider components (0505/0506).
*
* The key is a concatenation of the following fields:-
*
* Coverage/Rider table code
* Bill Frequency
*
* Access the required table by reading the dated table directly (ITDM)
* The contents of this table are consist of 2 parts:
*
*  1) Basic Level
*
* To get the Basic benefit range and Basic Premium rate by Mortality
* Class.
*
*  2) Additional Level
*
* To get the Additional Benefit Range and Additional Premium rate
* by Mortality Class.
*
* Validate sum assured amount whether it is between basic min benefit
* and additional max benefit, and the incremental of additional benefit
* is in step of risk unit or not.
*
* CALCULATE-PREMIUM.
*
* Basic premium  = Basic Premium Rate * Basic Max Benefit
*                                     / Risk Unit
* Additional premium = Additional premium rate * Additional Sum Assured
*                                     / Risk Unit
* Total premium = Basic premium + Additional premium.
*
* APPLY PERCENTAGE LOADING
*
* - from the LEXT working-storage (W/S) table apply the
* percentage loadings. For each loading entry on the table
* compute the PREMIUM AFTER PERCENTAGE LOADING  as follows:
*
*  PREMIUM AFTER LOADING = TOTAL PREMIUM * loading percentage / 100.
*
* CALCULATE-ROUNDING.
* - Calculate premium = Calculate premium / premium unit of TT502.
* - Calcualte basic premium = Basic premium / premium unit of TT502.
* - Read T5659 with discount method from TT502 & currency code.
* - round up depending on the rounding factor (obtained from
* the T5659 table).
*
*
*****************************************************************
* </pre>
*/
public class Tprmpm1 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "TPRMPM1";
	private String f264 = "F264";
	private String tl03 = "TL03";
	private String tl04 = "TL04";
		/* TABLES */
	private String tt502 = "TT502";
	private String t5659 = "T5659";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String lextrec = "LEXTREC";

	private FixedLengthStringData wsaaLextOppcRecs = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaLextOppcs = FLSArrayPartOfStructure(8, 3, wsaaLextOppcRecs, 0);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);

	private FixedLengthStringData wsaaLextZmortpctRecs = new FixedLengthStringData(16);
	private FixedLengthStringData[] wsaaLextZmortpcts = FLSArrayPartOfStructure(8, 2, wsaaLextZmortpctRecs, 0);
	private PackedDecimalData[] wsaaLextZmortpct = PDArrayPartOfArrayStructure(3, 0, wsaaLextZmortpcts, 0, UNSIGNED_TRUE);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(13, 2);
	private String wsaaBasicPremium = "";
	private PackedDecimalData wsaaBasicRate = new PackedDecimalData(7, 0);
	private PackedDecimalData wsaaAdditRate = new PackedDecimalData(7, 0);
	private ZonedDecimalData wsaaNofIncrt = new ZonedDecimalData(8, 3).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaNofIncrt, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaNofIncrt02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 5).setUnsigned();
	private String wsaaTableStop = "";
	private FixedLengthStringData wsaaTt502Key = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(13, 2).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 8).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler3, 9).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler5, 10).setUnsigned();

	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler7, 11);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
	private Premiumrec premiumrec = new Premiumrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5659rec t5659rec = new T5659rec();
	private Tt502rec tt502rec = new Tt502rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		tt502120, 
		readLext131, 
		loopForAdjustedAge132, 
		exit190, 
		exit290, 
		calculateLoadings410, 
		exit490, 
		exit890, 
		exit9020
	}

	public Tprmpm1() {
		super();
	}

public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		premiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBasicPremium = "N";
		initialize100();
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			validateSumins200();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			calculatePremium300();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			percentageLoadings400();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			rounding800();
		}
		compute(premiumrec.calcLoaPrem, 2).set(sub(premiumrec.calcPrem,premiumrec.calcBasPrem));
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para100();
				}
				case tt502120: {
					tt502120();
					setupLextKey130();
				}
				case readLext131: {
					readLext131();
				}
				case loopForAdjustedAge132: {
				}
				case exit190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		wsaaBap.set(ZERO);
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaNofIncrt.set(ZERO);
		wsaaBasicRate.set(ZERO);
		wsaaAdditRate.set(ZERO);
		premiumrec.calcPrem.set(ZERO);
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		wsaaT5659Key.set(SPACES);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			clearLextRecs110();
		}
		goTo(GotoLabel.tt502120);
	}

protected void clearLextRecs110()
	{
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
	}

protected void tt502120()
	{
		wsaaTt502Key.set(SPACES);
		itdmIO.setItemrecKeyData(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(premiumrec.crtable.toString());
		stringVariable1.append(premiumrec.billfreq.toString());
		wsaaTt502Key.setLeft(stringVariable1.toString());
		itdmIO.setItemitem(wsaaTt502Key);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(tt502);
		if (isEQ(premiumrec.ratingdate,ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaTt502Key,itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),tt502)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			premiumrec.statuz.set(tl03);
			goTo(GotoLabel.exit190);
		}
		else {
			tt502rec.tt502Rec.set(itdmIO.getGenarea());
		}
		wsaaTableStop = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,7)
		|| isEQ(wsaaTableStop,"Y")); wsaaSub.add(1)){
			if (isEQ(tt502rec.mortcls[wsaaSub.toInt()],SPACES)) {
				premiumrec.statuz.set(tl03);
				wsaaTableStop = "Y";
			}
			else {
				if (isEQ(tt502rec.mortcls[wsaaSub.toInt()],premiumrec.mortcls)) {
					wsaaBasicRate.set(tt502rec.insprm[wsaaSub.toInt()]);
					wsaaTableStop = "Y";
				}
			}
		}
		if (isEQ(wsaaTableStop,"N")) {
			premiumrec.statuz.set(tl03);
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
		wsaaTableStop = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,7)
		|| isEQ(wsaaTableStop,"Y")); wsaaSub.add(1)){
			if (isEQ(tt502rec.mortality[wsaaSub.toInt()],SPACES)) {
				wsaaTableStop = "Y";
			}
			else {
				if (isEQ(tt502rec.mortality[wsaaSub.toInt()],premiumrec.mortcls)) {
					wsaaAdditRate.set(tt502rec.instpr[wsaaSub.toInt()]);
					wsaaTableStop = "Y";
				}
			}
		}
	}

protected void setupLextKey130()
	{
		lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lextIO.setChdrnum(premiumrec.chdrChdrnum);
		lextIO.setLife(premiumrec.lifeLife);
		lextIO.setCoverage(premiumrec.covrCoverage);
		lextIO.setRider(premiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		wsaaSub.set(0);
	}

protected void readLext131()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError9000();
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		if (isEQ(lextIO.getChdrcoy(),premiumrec.chdrChdrcoy)
		&& isEQ(lextIO.getChdrnum(),premiumrec.chdrChdrnum)
		&& isEQ(lextIO.getLife(),premiumrec.lifeLife)
		&& isEQ(lextIO.getCoverage(),premiumrec.covrCoverage)
		&& isEQ(lextIO.getRider(),premiumrec.covrRider)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		if (isEQ(premiumrec.reasind,"2")
		&& isEQ(lextIO.getReasind(),"1")) {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		if (isNE(premiumrec.reasind,"2")
		&& isEQ(lextIO.getReasind(),"2")) {
			goTo(GotoLabel.loopForAdjustedAge132);
		}
		lextIO.setFunction(varcom.nextr);
		if (isLTE(lextIO.getExtCessDate(),premiumrec.reRateDate)) {
			goTo(GotoLabel.readLext131);
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub,8)) {
			wsaaLextOppc[wsaaSub.toInt()].set(lextIO.getOppc());
		}
		wsaaAgerateTot.add(lextIO.getAgerate());
		goTo(GotoLabel.readLext131);
	}

protected void validateSumins200()
	{
		try {
			validateBasic210();
			validateIncrement220();
		}
		catch (GOTOException e){
		}
	}

protected void validateBasic210()
	{
		if (isLT(premiumrec.sumin,tt502rec.benefitMin01)
		|| isGT(premiumrec.sumin,tt502rec.benefitMax02)) {
			premiumrec.statuz.set(tl04);
			goTo(GotoLabel.exit290);
		}
		if (isEQ(wsaaBasicRate,ZERO)) {
			premiumrec.statuz.set(tl03);
			goTo(GotoLabel.exit290);
		}
	}

protected void validateIncrement220()
	{
		if (isLTE(tt502rec.riskunit,0)) {
			premiumrec.statuz.set(tl03);
			goTo(GotoLabel.exit290);
		}
		if (isEQ(premiumrec.sumin,tt502rec.benefitMax01)) {
			goTo(GotoLabel.exit290);
		}
		compute(wsaaNofIncrt, 3).set(div((sub(premiumrec.sumin,tt502rec.benefitMax01)),tt502rec.riskunit));
		if (isNE(wsaaNofIncrt02,0)) {
			premiumrec.statuz.set(tl04);
			goTo(GotoLabel.exit290);
		}
	}

protected void calculatePremium300()
	{
		/*CALCULATE-PREMIUM*/
		compute(premiumrec.calcPrem, 2).set(add((div((mult(tt502rec.benefitMax01,wsaaBasicRate)),tt502rec.riskunit)),(div((mult((sub(premiumrec.sumin,tt502rec.benefitMax01)),wsaaAdditRate)),tt502rec.riskunit))));
		premiumrec.calcBasPrem.set(premiumrec.calcPrem);
		/*EXIT*/
	}

protected void percentageLoadings400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para400();
				}
				case calculateLoadings410: {
					calculateLoadings410();
				}
				case exit490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para400()
	{
		wsaaSub.set(0);
	}

protected void calculateLoadings410()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,8)) {
			goTo(GotoLabel.exit490);
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()],0)) {
			compute(premiumrec.calcPrem, 2).set((div((mult(premiumrec.calcPrem,wsaaLextOppc[wsaaSub.toInt()])),100)));
		}
		goTo(GotoLabel.calculateLoadings410);
	}

protected void premiumUnit500()
	{
		/*PARA*/
		if (isGT(tt502rec.premUnit,0)) {
			compute(premiumrec.calcPrem, 2).set(div(premiumrec.calcPrem,tt502rec.premUnit));
			compute(premiumrec.calcBasPrem, 2).set(div(premiumrec.calcBasPrem,tt502rec.premUnit));
		}
		/*EXIT*/
	}

protected void rounding800()
	{
		try {
			start810();
		}
		catch (GOTOException e){
		}
	}

protected void start810()
	{
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5659);
		wsaaDisccntmeth.set(tt502rec.disccntmeth);
		wsaaCurrcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5659Key);
		if (isEQ(premiumrec.ratingdate,ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5659)
		|| isNE(wsaaT5659Key,itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			premiumrec.statuz.set(f264);
			goTo(GotoLabel.exit890);
		}
		t5659rec.t5659Rec.set(itdmIO.getGenarea());
		wsaaBap.set(premiumrec.calcPrem);
		if (isEQ(wsaaBasicPremium,"Y")) {
			premiumrec.calcBasPrem.set(wsaaBap);
		}
		else {
			premiumrec.calcPrem.set(wsaaBap);
		}
		if (isEQ(wsaaBasicPremium,"Y")) {
			wsaaRoundNum.set(premiumrec.calcBasPrem);
			roundUp820();
			premiumrec.calcBasPrem.set(wsaaRoundNum);
			premiumrec.calcPrem.set(wsaaRoundNum);
		}
		else {
			wsaaRoundNum.set(premiumrec.calcPrem);
			roundUp820();
			premiumrec.calcPrem.set(wsaaRoundNum);
			premiumrec.calcBasPrem.set(wsaaRoundNum);
		}
		goTo(GotoLabel.exit890);
	}

protected void roundUp820()
	{
		if (isEQ(t5659rec.rndfact,1)
		|| isEQ(t5659rec.rndfact,0)) {
			if (isLT(wsaaRoundDec,.5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact,10)) {
			if (isLT(wsaaRound1,5)) {
				wsaaRound1.set(0);
			}
			else {
				wsaaRoundNum.add(10);
				wsaaRound1.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact,100)) {
			if (isLT(wsaaRound10,50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact,1000)) {
			if (isLT(wsaaRound100,500)) {
				wsaaRound100.set(0);
			}
			else {
				wsaaRoundNum.add(1000);
				wsaaRound100.set(0);
			}
		}
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
