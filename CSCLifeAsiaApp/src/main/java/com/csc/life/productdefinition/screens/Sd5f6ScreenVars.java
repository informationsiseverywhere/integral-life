package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class  Sd5f6ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(403);
	public FixedLengthStringData dataFields = new FixedLengthStringData(67).isAPartOf(dataArea, 0);
	public FixedLengthStringData fundPoolPris = new FixedLengthStringData(13).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] fundPoolPri = FLSArrayPartOfStructure(13, 1, fundPoolPris, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(fundPoolPris, 0, FILLER_REDEFINE);
	public FixedLengthStringData fundPoolPri01 = DD.fundPoolPriority.copy().isAPartOf(filler,0);
	public FixedLengthStringData fundPoolPri02 = DD.fundPoolPriority.copy().isAPartOf(filler,1);
	public FixedLengthStringData fundPoolPri03 = DD.fundPoolPriority.copy().isAPartOf(filler,2);
	public FixedLengthStringData fundPoolPri04 = DD.fundPoolPriority.copy().isAPartOf(filler,3);
	public FixedLengthStringData fundPoolPri05 = DD.fundPoolPriority.copy().isAPartOf(filler,4);
	public FixedLengthStringData fundPoolPri06 = DD.fundPoolPriority.copy().isAPartOf(filler,5);
	public FixedLengthStringData fundPoolPri07 = DD.fundPoolPriority.copy().isAPartOf(filler,6);
	public FixedLengthStringData fundPoolPri08 = DD.fundPoolPriority.copy().isAPartOf(filler,7);
	public FixedLengthStringData fundPoolPri09 = DD.fundPoolPriority.copy().isAPartOf(filler,8);
	public FixedLengthStringData fundPoolPri10 = DD.fundPoolPriority.copy().isAPartOf(filler,9);
	public FixedLengthStringData fundPoolPri11 = DD.fundPoolPriority.copy().isAPartOf(filler,10);
	public FixedLengthStringData fundPoolPri12 = DD.fundPoolPriority.copy().isAPartOf(filler,11);
	public FixedLengthStringData fhfp = DD.fundPoolPriority.copy().isAPartOf(filler, 12);
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,13);		
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,14);	
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,52);	
	
	public FixedLengthStringData fhbcl = DD.fhbcl.copy().isAPartOf(dataFields, 57);
  	public FixedLengthStringData fhscc = DD.fhscc.copy().isAPartOf(dataFields, 58);
	public FixedLengthStringData dhbcl = DD.dhbcl.copy().isAPartOf(dataFields, 62);
  	public FixedLengthStringData dhscc = DD.dhscc.copy().isAPartOf(dataFields, 63);
  	
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 67);
	
	public FixedLengthStringData fundPoolPrisErr = new FixedLengthStringData(52).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] fundPoolPriErr = FLSArrayPartOfStructure(13, 4, fundPoolPrisErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(52).isAPartOf(fundPoolPrisErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fundPoolPri01Err =new FixedLengthStringData(4).isAPartOf(filler5,0);
	public FixedLengthStringData fundPoolPri02Err = new FixedLengthStringData(4).isAPartOf(filler5,4);
	public FixedLengthStringData fundPoolPri03Err = new FixedLengthStringData(4).isAPartOf(filler5,8);
	public FixedLengthStringData fundPoolPri04Err = new FixedLengthStringData(4).isAPartOf(filler5,12);
	public FixedLengthStringData fundPoolPri05Err = new FixedLengthStringData(4).isAPartOf(filler5,16);
	public FixedLengthStringData fundPoolPri06Err = new FixedLengthStringData(4).isAPartOf(filler5,20);
	public FixedLengthStringData fundPoolPri07Err = new FixedLengthStringData(4).isAPartOf(filler5,24);
	public FixedLengthStringData fundPoolPri08Err = new FixedLengthStringData(4).isAPartOf(filler5,28);
	public FixedLengthStringData fundPoolPri09Err = new FixedLengthStringData(4).isAPartOf(filler5,32);
	public FixedLengthStringData fundPoolPri10Err = new FixedLengthStringData(4).isAPartOf(filler5,36);
	public FixedLengthStringData fundPoolPri11Err = new FixedLengthStringData(4).isAPartOf(filler5,40);
	public FixedLengthStringData fundPoolPri12Err = new FixedLengthStringData(4).isAPartOf(filler5,44);
	public FixedLengthStringData fhfpErr = new FixedLengthStringData(4).isAPartOf(filler5, 48);
	
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,52);		
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,56);	
	public FixedLengthStringData longdescErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,60);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,64);	
	
	public FixedLengthStringData fhbclErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData fhsccErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
  	public FixedLengthStringData dhbclErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
  	public FixedLengthStringData dhsccErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
  	
  	
  		
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 151);
	
	public FixedLengthStringData fundPoolPrisOut = new FixedLengthStringData(156).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] fundPoolPriOut = FLSArrayPartOfStructure(13, 12, fundPoolPrisOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(156).isAPartOf(fundPoolPrisOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fundPoolPri01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri02Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri03Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri04Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri05Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri06Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri07Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri08Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri09Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri10Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri11Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fundPoolPri12Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] fhfpOut = FLSArrayPartOfStructure(12, 1, filler10, 0);
	
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);	
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	
	
  	public FixedLengthStringData[] fhbclOut = FLSArrayPartOfStructure(12, 1, outputIndicators,  204);
	public FixedLengthStringData[] fhsccOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
  	public FixedLengthStringData[] dhbclOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
  	public FixedLengthStringData[] dhsccOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
  	
  	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public LongData  Sd5f6screenWritten = new LongData(0);
	public LongData  Sd5f6protectWritten = new LongData(0);
	

	public boolean hasSubfile() {
		return false;
	}


	public  Sd5f6ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		//fieldIndMap.put(autoTypeOut,new String[] {null, "01", null, "01", null, null, null, null, null, null, null, null});

		screenFields = new BaseData[] {company, tabl, item, longdesc, fundPoolPri01, fundPoolPri02, fundPoolPri03, fundPoolPri04, fundPoolPri05, fundPoolPri06, fundPoolPri07, fundPoolPri08, fundPoolPri09, fundPoolPri10, fundPoolPri11, fundPoolPri12, fhbcl,fhscc,fhfp,dhbcl,dhscc };
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, fundPoolPri01Out, fundPoolPri02Out, fundPoolPri03Out, fundPoolPri04Out, fundPoolPri05Out, fundPoolPri06Out, fundPoolPri07Out, fundPoolPri08Out, fundPoolPri09Out, fundPoolPri10Out, fundPoolPri11Out, fundPoolPri12Out,fhbclOut,fhsccOut,fhfpOut,dhbclOut,dhsccOut  };
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, fundPoolPri01Err, fundPoolPri02Err, fundPoolPri03Err, fundPoolPri04Err, fundPoolPri05Err, fundPoolPri06Err, fundPoolPri07Err, fundPoolPri08Err, fundPoolPri09Err, fundPoolPri10Err, fundPoolPri11Err, fundPoolPri12Err,fhbclErr,fhsccErr,fhfpErr,dhbclErr,dhsccErr};

		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord =  Sd5f6screen.class;
		protectRecord =  Sd5f6protect.class;
	}

}
