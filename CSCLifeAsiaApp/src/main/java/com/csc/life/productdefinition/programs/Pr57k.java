/*
 * File: Pr57k.java
*/
package com.csc.life.productdefinition.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.util.CLFunctions.queryUserJobInfo;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.financials.tablestructures.T3672rec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.screens.Sr57kScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr57k extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57K");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	

	private FixedLengthStringData wsaaSbmaction = new FixedLengthStringData(1);
	private Validator billingChange = new Validator(wsaaSbmaction, "A");
	private Validator quotation = new Validator(wsaaSbmaction, "B");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaNewMandref = new FixedLengthStringData(5);

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 0);
	private ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOkeys, 1).setUnsigned();
	private FixedLengthStringData wsaaRqstBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 6);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private BinaryData wsjobVarlen = new BinaryData(9, 0).init(101);
	private FixedLengthStringData wsjobFormat = new FixedLengthStringData(8).init("JOBI0600");
	private FixedLengthStringData wsjobQualjobname = new FixedLengthStringData(26).init("*");
	private FixedLengthStringData wsjobIntjobi = new FixedLengthStringData(26).init(SPACES);

	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Batckey wsaaBatchkey = new Batckey();
	private Clntkey wsaaClntkey = new Clntkey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr384rec tr384rec = new Tr384rec();
	private T3672rec t3672rec = new T3672rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Gensswrec gensswrec = new Gensswrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Sr57kScreenVars sv = ScreenProgram.getScreenVars( Sr57kScreenVars.class);
	private WsjobDataInner wsjobDataInner = new WsjobDataInner();
	
	/* TSD 321 Phase 2 Starts */
	private FixedLengthStringData wsaaNewPayclt = new FixedLengthStringData(8);
	/* TSD 321 Phase 2 Ends */
	private boolean isInforChanged;
	private FixedLengthStringData wsaaOldPayee = new FixedLengthStringData(8);
	
	//ILB-489 by wli31
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private Payrpf payrIO = null;
	private Clntpf cltsIO = null;
	private Itempf itempf = null;
	private Descpf descpf = null;
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1a90, 
		exit2090, 
		updateDatabase3040, 
		exit3090, 
		tr384Found3130, 
		exit3190, 
		exit3290, 
		exit3390, 
		exit3609, 
		exit3639, 
		exit5190, 
		exit5290, 
		exit5390, 
		exit6090, 
		exit7190, 
		exit7290, 
		exit8290, 
		exit8390
	}

	public Pr57k() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57k", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

	@Override
	protected void initialise1000()	{
		initialise1010();
	}

	protected void initialise1010()	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("4000");
			return ;
		}
		isInforChanged = false;
		syserrrec.subrname.set(wsaaProg);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaSbmaction.set(wsspcomn.sbmaction);
		wsaaFirstTime.set("Y");
		initialize(sv.dataArea);
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if (chdrpf == null) {
			fatalError600();
		}
		
		payrIO = payrpfDAO.getpayrRecord(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
		if (payrIO == null) {
			fatalError600();
		}
		/*    Set screen fields*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttyp.set(chdrpf.getCnttype());
		
		/* TSD 321 Starts */
		sv.reqntype02.set(chdrpf.getReqntype());
		/* TSD 321 Ends */
		
		readDesc1100("T5688",sv.cnttyp.toString().trim());
		sv.ctypedes.set(descpf.getLongdesc());

		readDesc1100("T3623",chdrpf.getStatcode());
		sv.chdrstatus.set(descpf.getShortdesc());

		readDesc1100("T3588",payrIO.getPstatcode());
		sv.premstatus.set(descpf.getShortdesc());


		cltsIO = clntpfDAO.searchClntRecord(chdrpf.getCownpfx(), chdrpf.getCowncoy().toString(), chdrpf.getCownnum());
		if(cltsIO == null){
			fatalError600();	
		}
		sv.cownnum.set(chdrpf.getCownnum());
		plainname();
		sv.ownername.set(wsspcomn.longconfname);

		Lifepf lifemja = lifepfDAO.getLifeEnqRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
		if(lifemja == null){
			fatalError600();
		}
		cltsIO = clntpfDAO.searchClntRecord(chdrpf.getCownpfx(), chdrpf.getCowncoy().toString(), lifemja.getLifcnum());
		if (cltsIO == null) {
			fatalError600();
		}
		sv.lifenum.set(lifemja.getLifcnum());
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* TSD 321 Phase 2 Starts */
		sv.payrnum.set(chdrpf.getPayclt() == null ? "" : chdrpf.getPayclt() );
		wsaaOldPayee.set(sv.payrnum);
		if (null != chdrpf.getPayclt() && !"".equals(chdrpf.getPayclt().trim()) ) {
			cltsIO = clntpfDAO.searchClntRecord(chdrpf.getCownpfx(), chdrpf.getCowncoy().toString(), chdrpf.getPayclt());
			if (cltsIO == null) {
				fatalError600();
			}
			plainname();
			sv.payorname.set(wsspcomn.longconfname);	
		} else {
			sv.payorname.set(SPACES);	
		}
		/* TSD 321 Phase 2 Ends */

		sv.reqntype01.set(chdrpf.getReqntype());

		readDesc1100("T3672",sv.reqntype01.toString().trim());
		//MTL TSD 321
		sv.mopdesc.set(descpf.getLongdesc());   //Check with TEch team on implementation of Drop down for payout method.
		
		//SML321-STARTS
		if (isEQ(sv.reqntype02, "C")) {
			if (isEQ(sv.crcind, SPACES)) {
				sv.crcind.set("+");
			}
		} else {
			sv.crcind.set(SPACES);
		}
		if (isEQ(sv.reqntype02, "4")) {
			if (isEQ(sv.ddind, SPACES)) {
				sv.ddind.set("+");
			}
		} else {
			sv.ddind.set(SPACES);
		}
		//SML321-ENDS
		
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void readDesc1100(String desctabl, String descitem){
	descpf = descDAO.getdescData(smtpfxcpy.item.toString(), desctabl,descitem,wsspcomn.company.toString(),wsspcomn.language.toString());
	if (descpf == null) {
		descpf = new Descpf();
		descpf.setLongdesc("??????????????????????????????");
		descpf.setShortdesc("??????????");
	}
}


protected void checkBoxAction1c00(){
	if (isEQ(sv.reqntype02, "C") || isEQ(sv.reqntype02, "4")){
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}
	if(isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		return;
	}
	if (isNE(chdrlnbIO.getZmandref(), SPACES)) {
		if (chdrpf.getReqntype().equals(sv.reqntype01.toString().trim())) 
		{
			if (isEQ(sv.reqntype02, "C")) {
				isInforChanged = true;
				sv.crcind.set("+");
				sv.payrnum.set(chdrlnbIO.getPayclt()); //ILIFE-2765
			}
			if (isEQ(sv.reqntype02, "4")) {
				isInforChanged = true;
				sv.ddind.set("+");
				sv.payrnum.set(chdrlnbIO.getPayclt()); //ILIFE-2765
			}
			if(isNE(sv.payrnum, SPACES)) {
				cltsIO = clntpfDAO.searchClntRecord(chdrpf.getCownpfx(), chdrpf.getCowncoy().toString(), chdrlnbIO.getPayclt().toString());
				if (cltsIO == null) {
					fatalError600();
				}
				plainname();
				sv.payorname.set(wsspcomn.longconfname);
			}
			chdrpf.setReqntype(sv.reqntype02.toString());
			chdrpf.setZmandref(chdrlnbIO.getZmandref().toString());
			chdrpf.setPayclt(chdrlnbIO.getPayclt().toString());
		}
		else {
			b200KeepsPayr();
		}
	}
	else {
		sv.ddind.set(SPACES);
		sv.crcind.set(SPACES);
	}
	
}

protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
	
		sv.ddindOut[varcom.pr.toInt()].set("N");
		sv.crcindOut[varcom.pr.toInt()].set("N");
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
	}

protected void validateScreen2010()
	{
		
		if (isEQ(sv.reqntype02, SPACES)) {
			sv.mopdesc02.set(SPACES); //MTL TSD 321
			sv.reqntype02Err.set("E186");
			wsspcomn.edterror.set("Y");
		}
		//ILIFE-2765 Starts
		if(isEQ(sv.reqntype02, sv.reqntype01) && isEQ(wsaaOldPayee, sv.payrnum)) {
			isInforChanged = false;
			sv.reqntype02Err.set("RFSH");
			sv.payrnumErr.set("RFSH");
			wsspcomn.edterror.set("Y");
		}
		//ILIFE-2765 Ends
		if (isEQ(sv.reqntype02Err, SPACES)) {
			readDesc1100("T3672",sv.reqntype02.toString().trim());
			sv.mopdesc02.set(descpf.getLongdesc()); //MTL TSD 321
		}
		
		if (isEQ(wsaaScrnStatuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.reqntype02,"4")) {
 			if (isEQ(sv.ddind, SPACES) || (isNE(wsaaOldPayee, sv.payrnum) && !isInforChanged)) {
				sv.ddind.set("X");
			}
 			if(isNE(sv.reqntype02Err, SPACES)) {
 				sv.ddind.set("+");
 			}
		}
		else {
			sv.ddind.set(SPACES);
		}

		/* Check that Credit Card Details are required. */
		if (isEQ(sv.reqntype02, "C")) {
			if (isEQ(sv.crcind, SPACES) || (isNE(wsaaOldPayee, sv.payrnum) && !isInforChanged)) {
				sv.crcind.set("X");
			}
			if(isNE(sv.reqntype02Err, SPACES)) {
				sv.crcind.set("+");
 			}
		} else {
			sv.crcind.set(SPACES);
		}

		if (isNE(sv.ddind, "X")
		&& isNE(sv.ddind, "+")
		&& isNE(sv.ddind, " ")) {
			sv.ddindErr.set("G620");
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.crcind, "X")
		&& isNE(sv.crcind, "+")
		&& isNE(sv.crcind, " ")) {
			sv.crcindErr.set("G620");
			wsspcomn.edterror.set("Y");
		}
		if(isEQ(sv.payrnum, SPACES)) {
			return;
		}
		cltsIO = clntpfDAO.searchClntRecord(chdrpf.getCownpfx(), chdrpf.getCowncoy().toString(), sv.payrnum.toString());
		
		if(cltsIO == null){
			sv.payrnumErr.set("RFSL");
			wsspcomn.edterror.set("Y");
			return;
		}
		plainname();
		sv.payorname.set(wsspcomn.longconfname);
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES))
		{
			wsspcomn.edterror.set("Y");
		}
		
	}


protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
					checkIncr3030();
				case updateDatabase3040: 
					updateDatabase3040();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		
		/*CHECK-BOX-SELECTED*/
		if (isEQ(sv.ddind, "X")
		|| isEQ(sv.crcind, "X")) {
			b300Keepschdrlnb();
			goTo(GotoLabel.exit3090);
		}
	}

	protected void checkIncr3030()	{
		if (!firstTime.isTrue()) {
			goTo(GotoLabel.updateDatabase3040);
		}
		wsaaFirstTime.set("N");
	}

	protected void updateDatabase3040()	{
		/* Save the after change info.*/
		wsaaNewMandref.set(chdrpf.getZmandref());
		
		/* TSD 321 Phase 2 Starts */
		wsaaNewPayclt.set(chdrpf.getPayclt() == null ? "" : chdrpf.getPayclt());

		itempf = new Itempf();
	    itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T3672");
		itempf.setItemitem(sv.reqntype02.toString().trim());
	    itempf = itemDAO.getItempfRecord(itempf);
	    if (itempf == null) {
			fatalError600();
		}
	    t3672rec.t3672Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* TSD 321 Phase 2 Starts */
		
		processChdr3400();
		processPayr3500();
		createPtrn3700();
		printLetter3100();
		statistic3800();
		releaseSoftlock3900();
	}

	protected void printLetter3100() {
	    itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TR384");
		String itemitem = chdrpf.getCnttype().concat(wsaaBatchkey.batcBatctrcde.toString());
		itempf.setItemitem(itemitem);
	    itempf = itemDAO.getItempfRecord(itempf);
	    if(itempf == null){
	    	itempf = new Itempf();
	    	itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("TR384");
			itemitem = "***".concat(wsaaBatchkey.batcBatctrcde.toString());
			itempf.setItemitem(itemitem);
		    itempf = itemDAO.getItempfRecord(itempf);
		    if(itempf == null){
		    	return;
		    }
	    }
	    tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isEQ(tr384rec.letterType, SPACES)) {
			return;
		}
		letterRequest3160();
	}

protected void letterRequest3160()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrpf.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		letrqstrec.rdocnum.set(chdrpf.getChdrnum());
		letrqstrec.branch.set(chdrpf.getCntbranch());
		wsaaOkey1.set(wsspcomn.language);
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrpf.getCowncoy());
		letrqstrec.clntnum.set(chdrpf.getCownnum());
		letrqstrec.chdrcoy.set(chdrpf.getChdrcoy());
		letrqstrec.chdrnum.set(chdrpf.getChdrnum());
		letrqstrec.tranno.set(chdrpf.getTranno());
		letrqstrec.despnum.set(chdrpf.getDespnum());
		letrqstrec.trcde.set(wsaaBatchkey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}


protected void processChdr3400(){
	List<Chdrpf> chdrList = new ArrayList<>();
	chdrpf.setValidflag('2');
	chdrpf.setCurrto(sv.ptdate.toInt()); 
	chdrList.add(chdrpf);
	chdrpfDAO.updateInvalidChdrRecords(chdrList);

	
	setPrecision(chdrpf.getTranno(), 0);
	chdrpf.setTranno(chdrpf.getTranno() + 1);
	chdrpf.setCurrfrom(wsaaToday.toInt());
	chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	chdrpf.setReqntype(sv.reqntype02.toString());
	chdrpf.setZmandref(wsaaNewMandref.toString());
	chdrpf.setPayclt(wsaaNewPayclt.toString());;

	if (isNE(t3672rec.bankaccreq, "Y")) {
		wsaaNewMandref.set(SPACES);
		wsaaNewPayclt.set(SPACES);
		chdrpf.setZmandref("");
		chdrpf.setPayclt("");
	}

	chdrpf.setValidflag('1');
	chdrpfDAO.insertChdrRecords(chdrpf);
	
}

	protected void processPayr3500() {
		List<Payrpf> payrList = new ArrayList<Payrpf>();
		payrList.add(payrIO);
		payrpfDAO.updatePayrRecord(payrList,0);
		payrList.clear();
		
		payrIO.setTranno(chdrpf.getTranno());
		payrIO.setTransactionDate(Integer.parseInt(wsaaToday.toString().substring(2)));
		payrIO.setValidflag("1");
		payrIO.setUser(varcom.vrcmUser.toInt());
		payrIO.setZmandref(wsaaNewMandref.toString());
		payrIO.setEffdate(wsaaToday.toInt());
		payrList.add(payrIO);
		payrpfDAO.insertPayrpfList(payrList);
	}

protected void createPtrn3700(){
	Ptrnpf ptrnIO = new Ptrnpf();
	ptrnIO.setChdrpfx(chdrpf.getChdrpfx());
	ptrnIO.setChdrcoy(chdrpf.getChdrcoy().toString());
	ptrnIO.setRecode(chdrpf.getRecode());
	ptrnIO.setChdrnum(chdrpf.getChdrnum());
	ptrnIO.setBatcactmn(wsaaBatchkey.batcBatcactmn.toInt());
	ptrnIO.setBatcactyr(wsaaBatchkey.batcBatcactyr.toInt());
	ptrnIO.setBatcbatch(wsaaBatchkey.batcBatcbatch.toString());
	ptrnIO.setBatcbrn(wsaaBatchkey.batcBatcbrn.toString());
	ptrnIO.setBatccoy(wsaaBatchkey.batcBatccoy.toString());
	ptrnIO.setBatcpfx(wsaaBatchkey.batcBatcpfx.toString());
	ptrnIO.setBatctrcde(wsaaBatchkey.batcBatctrcde.toString());
	ptrnIO.setTranno(chdrpf.getTranno());
	ptrnIO.setPtrneff(wsaaToday.toInt());
	ptrnIO.setDatesub(wsaaToday.toInt());
	ptrnIO.setTrdt(Integer.parseInt(wsaaToday.toString().substring(2)));
	ptrnIO.setTrtm(Integer.parseInt(getCobolTime()));
	ptrnIO.setUserT(varcom.vrcmUser.toInt());
	ptrnIO.setTermid(varcom.vrcmTermid.toInternal());
	getUser800();
	ptrnIO.setCrtuser(wsjobDataInner.wsjobUsername.toString());
	ptrnIO.setValidflag("1");
	ptrnpfDAO.insertPtrnPF(ptrnIO);
}

protected void statistic3800()
	{
		start3810();
	}

protected void start3810()
	{
		lifsttrrec.batccoy.set(wsaaBatchkey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatchkey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatchkey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatchkey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatchkey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrpf.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrpf.getChdrnum());
		lifsttrrec.tranno.set(chdrpf.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void releaseSoftlock3900()
	{
		start3910();
	}

protected void start3910()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			switchingReturn4500();
		}
		
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		wsaaBatchkey.set(wsspcomn.batchkey);
		gensswrec.transact.set(wsaaBatchkey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			ix.set(wsspcomn.programPtr);
			iy.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		
		if(isEQ(sv.ddind,"X") || isEQ(sv.crcind,"X")) 				
				b200KeepsPayr();			
		
		if (isEQ(sv.ddind, "X")) {
			gensswrec.function.set("A");
			sv.ddind.set(SPACES);
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.crcind, "X")) {
			gensswrec.function.set("B");
			wsaaClntkey.clntClntpfx.set("CN");
			wsaaClntkey.clntClntcoy.set(chdrpf.getCowncoy());
			wsaaClntkey.clntClntnum.set(chdrpf.getCownnum());
			wsspcomn.clntkey.set(wsaaClntkey);
			sv.crcind.set(SPACES);
			callGenssw4300();
			return ;
		}
		/*   No more selected (or none)*/
		/*      - restore stack from wsaa to wssp*/
		ix.set(wsspcomn.programPtr);
		iy.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		checkBoxAction1c00();
		/*   Set the next program name to the current screen*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[iy.toInt()].set(wsspcomn.secProg[ix.toInt()]);
		ix.add(1);
		iy.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[ix.toInt()].set(wsaaSecProg[iy.toInt()]);
		ix.add(1);
		iy.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		/*CALL-SUBROUTINE*/
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)) {
			syserrrec.params.set(gensswrec.gensswRec);
			fatalError600();
		}
		/*    load from gensw to wssp*/
		compute(ix, 0).set(add(1, wsspcomn.programPtr));
		iy.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[ix.toInt()].set(gensswrec.progOut[iy.toInt()]);
		ix.add(1);
		iy.add(1);
		/*EXIT*/
	}

protected void switchingReturn4500()
	{
	
		retrvChdrmja4510();
		b200KeepsPayr();//MPTD-129
		retrvPayr4520();
		
		
	}

protected void retrvChdrmja4510(){
	 chdrpf = chdrpfDAO.getChdrpf(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
	 if(chdrpf == null){
		 fatalError600();
	 }

}

protected void retrvPayr4520()
	{
		payrIO = payrpfDAO.getCacheObject(payrIO);
		if (payrIO == null) {
			fatalError600();
		}
		/*EXIT*/
	}



protected void getUser800()
	{
		/*START*/
		initialize(wsjobDataInner.wsjobData);
		queryUserJobInfo(wsjobDataInner.wsjobData, wsjobVarlen, wsjobFormat, wsjobQualjobname, wsjobIntjobi);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		zrdecplrec.currency.set(payrIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}


protected void b200KeepsPayr()
	{
		payrIO = payrpfDAO.getpayrRecord(wsspcomn.company.toString(),sv.chdrnum.toString());
		
		if (payrIO == null) {
			fatalError600();
		}
		payrIO.setChdrcoy(wsspcomn.company.toString());
		payrIO.setChdrnum(sv.chdrnum.toString());
		payrIO.setPayrseqno(1);
		payrIO.setMandref("");
		payrpfDAO.setCacheObject(payrIO);
	}
protected void b300Keepschdrlnb(){
	chdrlnbIO.setCownpfx("CN");
	chdrlnbIO.setCowncoy(wsspcomn.fsuco);
	chdrlnbIO.setCownnum(sv.cownnum);
	chdrlnbIO.setChdrnum(sv.chdrnum);
	chdrlnbIO.setChdrpfx(chdrpf.getChdrpfx());
	chdrlnbIO.setPayclt(sv.payrnum);
	chdrlnbIO.setBillcurr(chdrpf.getBillcurr());
	if((isEQ(sv.reqntype01, sv.reqntype02) && isEQ(wsaaOldPayee, sv.payrnum)) || isInforChanged) {
		chdrlnbIO.setZmandref(chdrpf.getZmandref());
	} else {
		isInforChanged = false;
		chdrlnbIO.setZmandref(SPACES);
	}
	chdrlnbIO.setFunction(varcom.keeps);
	chdrlnbIO.setFormat("CHDRLNBREC");
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		fatalError600();
	}
}


/*
 * Class transformed  from Data Structure WSJOB-DATA--INNER
 */
private static final class WsjobDataInner { 

	private FixedLengthStringData wsjobData = new FixedLengthStringData(101);
	private FixedLengthStringData wsjobUsername = new FixedLengthStringData(10).isAPartOf(wsjobData, 91);
}



}
