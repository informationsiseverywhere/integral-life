package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr51gscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51gScreenVars sv = (Sr51gScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr51gscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr51gScreenVars screenVars = (Sr51gScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.age01.setClassString("");
		screenVars.age02.setClassString("");
		screenVars.age03.setClassString("");
		screenVars.age04.setClassString("");
		screenVars.age05.setClassString("");
		screenVars.age06.setClassString("");
		screenVars.age07.setClassString("");
		screenVars.age08.setClassString("");
		screenVars.age09.setClassString("");
		screenVars.age10.setClassString("");
		screenVars.age11.setClassString("");
		screenVars.age12.setClassString("");
		screenVars.sumins01.setClassString("");
		screenVars.sumins02.setClassString("");
		screenVars.sumins03.setClassString("");
		screenVars.sumins04.setClassString("");
		screenVars.sumins05.setClassString("");
		screenVars.sumins06.setClassString("");
		screenVars.sumins07.setClassString("");
		screenVars.sumins08.setClassString("");
		screenVars.sumins09.setClassString("");
		screenVars.sumins10.setClassString("");
		screenVars.sumins11.setClassString("");
		screenVars.sumins12.setClassString("");
	}

/**
 * Clear all the variables in Sr51gscreen
 */
	public static void clear(VarModel pv) {
		Sr51gScreenVars screenVars = (Sr51gScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.age01.clear();
		screenVars.age02.clear();
		screenVars.age03.clear();
		screenVars.age04.clear();
		screenVars.age05.clear();
		screenVars.age06.clear();
		screenVars.age07.clear();
		screenVars.age08.clear();
		screenVars.age09.clear();
		screenVars.age10.clear();
		screenVars.age11.clear();
		screenVars.age12.clear();
		screenVars.sumins01.clear();
		screenVars.sumins02.clear();
		screenVars.sumins03.clear();
		screenVars.sumins04.clear();
		screenVars.sumins05.clear();
		screenVars.sumins06.clear();
		screenVars.sumins07.clear();
		screenVars.sumins08.clear();
		screenVars.sumins09.clear();
		screenVars.sumins10.clear();
		screenVars.sumins11.clear();
		screenVars.sumins12.clear();
	}
}
