package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for ST502
 * @version 1.0 generated on 30/08/09 07:25
 * @author Quipoz
 */
public class St502ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(855);
	public FixedLengthStringData dataFields = new FixedLengthStringData(199).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData disccntmeth = DD.discntmeth.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData insprms = new FixedLengthStringData(42).isAPartOf(dataFields, 5);
	public ZonedDecimalData[] insprm = ZDArrayPartOfStructure(7, 6, 0, insprms, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(insprms, 0, FILLER_REDEFINE);
	public ZonedDecimalData insprm01 = DD.insprm.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData insprm02 = DD.insprm.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData insprm03 = DD.insprm.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData insprm04 = DD.insprm.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData insprm05 = DD.insprm.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData insprm06 = DD.insprm.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData insprm07 = DD.insprm.copyToZonedDecimal().isAPartOf(filler,36);
	public FixedLengthStringData instprs = new FixedLengthStringData(42).isAPartOf(dataFields, 47);
	public ZonedDecimalData[] instpr = ZDArrayPartOfStructure(7, 6, 0, instprs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(42).isAPartOf(instprs, 0, FILLER_REDEFINE);
	public ZonedDecimalData instpr01 = DD.instpr.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData instpr02 = DD.instpr.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData instpr03 = DD.instpr.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData instpr04 = DD.instpr.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData instpr05 = DD.instpr.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData instpr06 = DD.instpr.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData instpr07 = DD.instpr.copyToZonedDecimal().isAPartOf(filler1,36);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,89);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,97);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,105);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,113);
	public FixedLengthStringData mortalitys = new FixedLengthStringData(7).isAPartOf(dataFields, 143);
	public FixedLengthStringData[] mortality = FLSArrayPartOfStructure(7, 1, mortalitys, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(7).isAPartOf(mortalitys, 0, FILLER_REDEFINE);
	public FixedLengthStringData mortality1 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	public FixedLengthStringData mortality2 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
	public FixedLengthStringData mortality3 = new FixedLengthStringData(1).isAPartOf(filler2, 2);
	public FixedLengthStringData mortality4 = new FixedLengthStringData(1).isAPartOf(filler2, 3);
	public FixedLengthStringData mortality5 = new FixedLengthStringData(1).isAPartOf(filler2, 4);
	public FixedLengthStringData mortality6 = new FixedLengthStringData(1).isAPartOf(filler2, 5);
	public FixedLengthStringData mortality7 = new FixedLengthStringData(1).isAPartOf(filler2, 6);
	public FixedLengthStringData mortclss = new FixedLengthStringData(7).isAPartOf(dataFields, 150);
	public FixedLengthStringData[] mortcls = FLSArrayPartOfStructure(7, 1, mortclss, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(7).isAPartOf(mortclss, 0, FILLER_REDEFINE);
	public FixedLengthStringData mortcls01 = DD.mortcls.copy().isAPartOf(filler3,0);
	public FixedLengthStringData mortcls02 = DD.mortcls.copy().isAPartOf(filler3,1);
	public FixedLengthStringData mortcls03 = DD.mortcls.copy().isAPartOf(filler3,2);
	public FixedLengthStringData mortcls04 = DD.mortcls.copy().isAPartOf(filler3,3);
	public FixedLengthStringData mortcls05 = DD.mortcls.copy().isAPartOf(filler3,4);
	public FixedLengthStringData mortcls06 = DD.mortcls.copy().isAPartOf(filler3,5);
	public FixedLengthStringData mortcls07 = DD.mortcls.copy().isAPartOf(filler3,6);
	public ZonedDecimalData premUnit = DD.pmunit.copyToZonedDecimal().isAPartOf(dataFields,157);
	public ZonedDecimalData riskunit = DD.riskunit.copyToZonedDecimal().isAPartOf(dataFields,160);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,166);
	public FixedLengthStringData benefitMaxs = new FixedLengthStringData(14).isAPartOf(dataFields, 171);
	public ZonedDecimalData[] benefitMax = ZDArrayPartOfStructure(2, 7, 0, benefitMaxs, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(14).isAPartOf(benefitMaxs, 0, FILLER_REDEFINE);
	public ZonedDecimalData benefitMax01 = DD.zbnfmax.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData benefitMax02 = DD.zbnfmax.copyToZonedDecimal().isAPartOf(filler4,7);
	public FixedLengthStringData benefitMins = new FixedLengthStringData(14).isAPartOf(dataFields, 185);
	public ZonedDecimalData[] benefitMin = ZDArrayPartOfStructure(2, 7, 0, benefitMins, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(14).isAPartOf(benefitMins, 0, FILLER_REDEFINE);
	public ZonedDecimalData benefitMin01 = DD.zbnfmin.copyToZonedDecimal().isAPartOf(filler5,0);
	public ZonedDecimalData benefitMin02 = DD.zbnfmin.copyToZonedDecimal().isAPartOf(filler5,7);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(164).isAPartOf(dataArea, 199);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData discntmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData insprmsErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] insprmErr = FLSArrayPartOfStructure(7, 4, insprmsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(28).isAPartOf(insprmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData insprm01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData insprm02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData insprm03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData insprm04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData insprm05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData insprm06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData insprm07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData instprsErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] instprErr = FLSArrayPartOfStructure(7, 4, instprsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(28).isAPartOf(instprsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData instpr01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData instpr02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData instpr03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData instpr04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData instpr05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData instpr06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData instpr07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData mortalitysErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData[] mortalityErr = FLSArrayPartOfStructure(7, 4, mortalitysErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(28).isAPartOf(mortalitysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mortality1Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData mortality2Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData mortality3Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData mortality4Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData mortality5Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData mortality6Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData mortality7Err = new FixedLengthStringData(4).isAPartOf(filler8, 24);
	public FixedLengthStringData mortclssErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] mortclsErr = FLSArrayPartOfStructure(7, 4, mortclssErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(28).isAPartOf(mortclssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mortcls01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData mortcls02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData mortcls03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData mortcls04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData mortcls05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData mortcls06Err = new FixedLengthStringData(4).isAPartOf(filler9, 20);
	public FixedLengthStringData mortcls07Err = new FixedLengthStringData(4).isAPartOf(filler9, 24);
	public FixedLengthStringData pmunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData riskunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData zbnfmaxsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData[] zbnfmaxErr = FLSArrayPartOfStructure(2, 4, zbnfmaxsErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(zbnfmaxsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zbnfmax01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData zbnfmax02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData zbnfminsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData[] zbnfminErr = FLSArrayPartOfStructure(2, 4, zbnfminsErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(8).isAPartOf(zbnfminsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zbnfmin01Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData zbnfmin02Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(492).isAPartOf(dataArea, 363);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] discntmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData insprmsOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] insprmOut = FLSArrayPartOfStructure(7, 12, insprmsOut, 0);
	public FixedLengthStringData[][] insprmO = FLSDArrayPartOfArrayStructure(12, 1, insprmOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(84).isAPartOf(insprmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] insprm01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] insprm02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] insprm03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] insprm04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] insprm05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData[] insprm06Out = FLSArrayPartOfStructure(12, 1, filler12, 60);
	public FixedLengthStringData[] insprm07Out = FLSArrayPartOfStructure(12, 1, filler12, 72);
	public FixedLengthStringData instprsOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] instprOut = FLSArrayPartOfStructure(7, 12, instprsOut, 0);
	public FixedLengthStringData[][] instprO = FLSDArrayPartOfArrayStructure(12, 1, instprOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(84).isAPartOf(instprsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] instpr01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] instpr02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] instpr03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] instpr04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] instpr05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData[] instpr06Out = FLSArrayPartOfStructure(12, 1, filler13, 60);
	public FixedLengthStringData[] instpr07Out = FLSArrayPartOfStructure(12, 1, filler13, 72);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData mortalitysOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 240);
	public FixedLengthStringData[] mortalityOut = FLSArrayPartOfStructure(7, 12, mortalitysOut, 0);
	public FixedLengthStringData[][] mortalityO = FLSDArrayPartOfArrayStructure(12, 1, mortalityOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(84).isAPartOf(mortalitysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mortality1Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] mortality2Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] mortality3Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] mortality4Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] mortality5Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] mortality6Out = FLSArrayPartOfStructure(12, 1, filler14, 60);
	public FixedLengthStringData[] mortality7Out = FLSArrayPartOfStructure(12, 1, filler14, 72);
	public FixedLengthStringData mortclssOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(7, 12, mortclssOut, 0);
	public FixedLengthStringData[][] mortclsO = FLSDArrayPartOfArrayStructure(12, 1, mortclsOut, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(84).isAPartOf(mortclssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mortcls01Out = FLSArrayPartOfStructure(12, 1, filler15, 0);
	public FixedLengthStringData[] mortcls02Out = FLSArrayPartOfStructure(12, 1, filler15, 12);
	public FixedLengthStringData[] mortcls03Out = FLSArrayPartOfStructure(12, 1, filler15, 24);
	public FixedLengthStringData[] mortcls04Out = FLSArrayPartOfStructure(12, 1, filler15, 36);
	public FixedLengthStringData[] mortcls05Out = FLSArrayPartOfStructure(12, 1, filler15, 48);
	public FixedLengthStringData[] mortcls06Out = FLSArrayPartOfStructure(12, 1, filler15, 60);
	public FixedLengthStringData[] mortcls07Out = FLSArrayPartOfStructure(12, 1, filler15, 72);
	public FixedLengthStringData[] pmunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] riskunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData zbnfmaxsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 444);
	public FixedLengthStringData[] zbnfmaxOut = FLSArrayPartOfStructure(2, 12, zbnfmaxsOut, 0);
	public FixedLengthStringData[][] zbnfmaxO = FLSDArrayPartOfArrayStructure(12, 1, zbnfmaxOut, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(24).isAPartOf(zbnfmaxsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zbnfmax01Out = FLSArrayPartOfStructure(12, 1, filler16, 0);
	public FixedLengthStringData[] zbnfmax02Out = FLSArrayPartOfStructure(12, 1, filler16, 12);
	public FixedLengthStringData zbnfminsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 468);
	public FixedLengthStringData[] zbnfminOut = FLSArrayPartOfStructure(2, 12, zbnfminsOut, 0);
	public FixedLengthStringData[][] zbnfminO = FLSDArrayPartOfArrayStructure(12, 1, zbnfminOut, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(24).isAPartOf(zbnfminsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zbnfmin01Out = FLSArrayPartOfStructure(12, 1, filler17, 0);
	public FixedLengthStringData[] zbnfmin02Out = FLSArrayPartOfStructure(12, 1, filler17, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData St502screenWritten = new LongData(0);
	public LongData St502protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public St502ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, riskunit, mortcls01, insprm01, instpr01, mortcls02, insprm02, instpr02, mortcls03, insprm03, instpr03, mortcls04, insprm04, instpr04, mortcls05, insprm05, instpr05, mortcls06, insprm06, instpr06, mortcls07, insprm07, instpr07, benefitMin01, benefitMax01, benefitMin02, benefitMax02, premUnit, disccntmeth};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, riskunitOut, mortcls01Out, insprm01Out, instpr01Out, mortcls02Out, insprm02Out, instpr02Out, mortcls03Out, insprm03Out, instpr03Out, mortcls04Out, insprm04Out, instpr04Out, mortcls05Out, insprm05Out, instpr05Out, mortcls06Out, insprm06Out, instpr06Out, mortcls07Out, insprm07Out, instpr07Out, zbnfmin01Out, zbnfmax01Out, zbnfmin02Out, zbnfmax02Out, pmunitOut, discntmethOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, riskunitErr, mortcls01Err, insprm01Err, instpr01Err, mortcls02Err, insprm02Err, instpr02Err, mortcls03Err, insprm03Err, instpr03Err, mortcls04Err, insprm04Err, instpr04Err, mortcls05Err, insprm05Err, instpr05Err, mortcls06Err, insprm06Err, instpr06Err, mortcls07Err, insprm07Err, instpr07Err, zbnfmin01Err, zbnfmax01Err, zbnfmin02Err, zbnfmax02Err, pmunitErr, discntmethErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = St502screen.class;
		protectRecord = St502protect.class;
	}

}
