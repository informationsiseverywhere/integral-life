/*********************  */
/*Author  :Liwei				  		*/
/*Purpose :Instead of subroutine Lifrtrn*/
/*Date    :2018.10.26				*/
package com.csc.life.productdefinition.procedures;

import java.util.List;

import com.csc.fsu.accounting.dataaccess.model.Acblpf;

public interface LifrtrnUtils {
	public void calcLitrtrn(LifrtrnPojo lifrtrnPojo) ;
	public List<Acblpf> calcLitrtrnAcbl(LifrtrnPojo lifrtrnPojo);
}
