package com.csc.life.productdefinition.tablestructures;
import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: CSC Limited
 * @version
 * Creation Date: Sun, 17 Aug 2016 03:20:55
 * Description:K
 * Copybook name: TT120REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr58yrec extends ExternalData {
	
	 	//*******************************
	  	//Attribute Declarations
	  	//*******************************
	  
	  	public FixedLengthStringData tr58yrec = new FixedLengthStringData(510);
	  	public FixedLengthStringData unitPremPercents = new FixedLengthStringData(225).isAPartOf(tr58yrec, 0);
	  	public ZonedDecimalData[] unitPremPercent = ZDArrayPartOfStructure(45, 5, 2, unitPremPercents, 0);
	  	public FixedLengthStringData filler = new FixedLengthStringData(225).isAPartOf(unitPremPercents, 0, FILLER_REDEFINE);
	  	public ZonedDecimalData unitPremPercent01 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
	  	public ZonedDecimalData unitPremPercent02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
	  	public ZonedDecimalData unitPremPercent03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
	  	public ZonedDecimalData unitPremPercent04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
	  	public ZonedDecimalData unitPremPercent05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
	  	public ZonedDecimalData unitPremPercent06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
	  	public ZonedDecimalData unitPremPercent07 = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
	  	public ZonedDecimalData unitPremPercent08 = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
	  	public ZonedDecimalData unitPremPercent09 = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
	  	public ZonedDecimalData unitPremPercent10 = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
	  	
	  	public ZonedDecimalData unitPremPercent11 = new ZonedDecimalData(5, 2).isAPartOf(filler, 50);
	  	public ZonedDecimalData unitPremPercent12 = new ZonedDecimalData(5, 2).isAPartOf(filler, 55);
	  	public ZonedDecimalData unitPremPercent13 = new ZonedDecimalData(5, 2).isAPartOf(filler, 60);
	  	public ZonedDecimalData unitPremPercent14 = new ZonedDecimalData(5, 2).isAPartOf(filler, 65);
	  	public ZonedDecimalData unitPremPercent15 = new ZonedDecimalData(5, 2).isAPartOf(filler, 70);
	  	public ZonedDecimalData unitPremPercent16 = new ZonedDecimalData(5, 2).isAPartOf(filler, 75);
	  	public ZonedDecimalData unitPremPercent17 = new ZonedDecimalData(5, 2).isAPartOf(filler, 80);
	  	public ZonedDecimalData unitPremPercent18 = new ZonedDecimalData(5, 2).isAPartOf(filler, 85);
	  	public ZonedDecimalData unitPremPercent19 = new ZonedDecimalData(5, 2).isAPartOf(filler, 90);
	  	public ZonedDecimalData unitPremPercent20 = new ZonedDecimalData(5, 2).isAPartOf(filler, 95);
	  	
	  	public ZonedDecimalData unitPremPercent21 = new ZonedDecimalData(5, 2).isAPartOf(filler, 100);
	  	public ZonedDecimalData unitPremPercent22 = new ZonedDecimalData(5, 2).isAPartOf(filler, 105);
	  	public ZonedDecimalData unitPremPercent23 = new ZonedDecimalData(5, 2).isAPartOf(filler, 110);
	  	public ZonedDecimalData unitPremPercent24 = new ZonedDecimalData(5, 2).isAPartOf(filler, 115);
	  	public ZonedDecimalData unitPremPercent25 = new ZonedDecimalData(5, 2).isAPartOf(filler, 120);
	  	public ZonedDecimalData unitPremPercent26 = new ZonedDecimalData(5, 2).isAPartOf(filler, 125);
	  	public ZonedDecimalData unitPremPercent27 = new ZonedDecimalData(5, 2).isAPartOf(filler, 130);
	  	public ZonedDecimalData unitPremPercent28 = new ZonedDecimalData(5, 2).isAPartOf(filler, 135);
	  	public ZonedDecimalData unitPremPercent29 = new ZonedDecimalData(5, 2).isAPartOf(filler, 140);
	  	public ZonedDecimalData unitPremPercent30 = new ZonedDecimalData(5, 2).isAPartOf(filler, 145);
	  	
	  	public ZonedDecimalData unitPremPercent31 = new ZonedDecimalData(5, 2).isAPartOf(filler, 150);
	  	public ZonedDecimalData unitPremPercent32 = new ZonedDecimalData(5, 2).isAPartOf(filler, 155);
	  	public ZonedDecimalData unitPremPercent33 = new ZonedDecimalData(5, 2).isAPartOf(filler, 160);
	  	public ZonedDecimalData unitPremPercent34 = new ZonedDecimalData(5, 2).isAPartOf(filler, 165);
	  	public ZonedDecimalData unitPremPercent35 = new ZonedDecimalData(5, 2).isAPartOf(filler, 170);
	  	public ZonedDecimalData unitPremPercent36 = new ZonedDecimalData(5, 2).isAPartOf(filler, 175);
	  	public ZonedDecimalData unitPremPercent37 = new ZonedDecimalData(5, 2).isAPartOf(filler, 180);
	  	public ZonedDecimalData unitPremPercent38 = new ZonedDecimalData(5, 2).isAPartOf(filler, 185);
	  	public ZonedDecimalData unitPremPercent39 = new ZonedDecimalData(5, 2).isAPartOf(filler, 190);
	  	public ZonedDecimalData unitPremPercent40 = new ZonedDecimalData(5, 2).isAPartOf(filler, 195);
	  	
	  	public ZonedDecimalData unitPremPercent41 = new ZonedDecimalData(5, 2).isAPartOf(filler, 200);
	  	public ZonedDecimalData unitPremPercent42 = new ZonedDecimalData(5, 2).isAPartOf(filler, 205);
	  	public ZonedDecimalData unitPremPercent43 = new ZonedDecimalData(5, 2).isAPartOf(filler, 210);
	  	public ZonedDecimalData unitPremPercent44 = new ZonedDecimalData(5, 2).isAPartOf(filler, 215);
	  	public ZonedDecimalData unitPremPercent45 = new ZonedDecimalData(5, 2).isAPartOf(filler, 220);

	  	
	  	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(180).isAPartOf(tr58yrec, 225);
	  	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(45, 4, unitVirtualFunds, 0);
	  	public FixedLengthStringData filler1 = new FixedLengthStringData(180).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
	  	public FixedLengthStringData unitVirtualFund01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	  	public FixedLengthStringData unitVirtualFund02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	  	public FixedLengthStringData unitVirtualFund03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	  	public FixedLengthStringData unitVirtualFund04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	  	public FixedLengthStringData unitVirtualFund05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	  	public FixedLengthStringData unitVirtualFund06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	  	public FixedLengthStringData unitVirtualFund07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	  	public FixedLengthStringData unitVirtualFund08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	  	public FixedLengthStringData unitVirtualFund09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	  	public FixedLengthStringData unitVirtualFund10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	  	
	  	public FixedLengthStringData unitVirtualFund11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	  	public FixedLengthStringData unitVirtualFund12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	  	public FixedLengthStringData unitVirtualFund13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	  	public FixedLengthStringData unitVirtualFund14 = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	  	public FixedLengthStringData unitVirtualFund15 = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	  	public FixedLengthStringData unitVirtualFund16 = new FixedLengthStringData(4).isAPartOf(filler1, 60);
	  	public FixedLengthStringData unitVirtualFund17 = new FixedLengthStringData(4).isAPartOf(filler1, 64);
	  	public FixedLengthStringData unitVirtualFund18 = new FixedLengthStringData(4).isAPartOf(filler1, 68);
	  	public FixedLengthStringData unitVirtualFund19 = new FixedLengthStringData(4).isAPartOf(filler1, 72);
	  	public FixedLengthStringData unitVirtualFund20 = new FixedLengthStringData(4).isAPartOf(filler1, 76);
	  	
	  	public FixedLengthStringData unitVirtualFund21 = new FixedLengthStringData(4).isAPartOf(filler1, 80);
	  	public FixedLengthStringData unitVirtualFund22 = new FixedLengthStringData(4).isAPartOf(filler1, 84);
	  	public FixedLengthStringData unitVirtualFund23 = new FixedLengthStringData(4).isAPartOf(filler1, 88);
	  	public FixedLengthStringData unitVirtualFund24 = new FixedLengthStringData(4).isAPartOf(filler1, 92);
	  	public FixedLengthStringData unitVirtualFund25 = new FixedLengthStringData(4).isAPartOf(filler1, 96);
	  	public FixedLengthStringData unitVirtualFund26 = new FixedLengthStringData(4).isAPartOf(filler1, 100);
	  	public FixedLengthStringData unitVirtualFund27 = new FixedLengthStringData(4).isAPartOf(filler1, 104);
	  	public FixedLengthStringData unitVirtualFund28 = new FixedLengthStringData(4).isAPartOf(filler1, 108);
	  	public FixedLengthStringData unitVirtualFund29 = new FixedLengthStringData(4).isAPartOf(filler1, 112);
	  	public FixedLengthStringData unitVirtualFund30 = new FixedLengthStringData(4).isAPartOf(filler1, 116);
	  	
	  	public FixedLengthStringData unitVirtualFund31 = new FixedLengthStringData(4).isAPartOf(filler1, 120);
	  	public FixedLengthStringData unitVirtualFund32 = new FixedLengthStringData(4).isAPartOf(filler1, 124);
	  	public FixedLengthStringData unitVirtualFund33 = new FixedLengthStringData(4).isAPartOf(filler1, 128);
	  	public FixedLengthStringData unitVirtualFund34 = new FixedLengthStringData(4).isAPartOf(filler1, 132);
	  	public FixedLengthStringData unitVirtualFund35 = new FixedLengthStringData(4).isAPartOf(filler1, 136);
	  	public FixedLengthStringData unitVirtualFund36 = new FixedLengthStringData(4).isAPartOf(filler1, 140);
	  	public FixedLengthStringData unitVirtualFund37 = new FixedLengthStringData(4).isAPartOf(filler1, 144);
	  	public FixedLengthStringData unitVirtualFund38 = new FixedLengthStringData(4).isAPartOf(filler1, 148);
	  	public FixedLengthStringData unitVirtualFund39 = new FixedLengthStringData(4).isAPartOf(filler1, 152);
	  	public FixedLengthStringData unitVirtualFund40 = new FixedLengthStringData(4).isAPartOf(filler1, 156);
	  	
	  	public FixedLengthStringData unitVirtualFund41 = new FixedLengthStringData(4).isAPartOf(filler1, 160);
	  	public FixedLengthStringData unitVirtualFund42 = new FixedLengthStringData(4).isAPartOf(filler1, 164);
	  	public FixedLengthStringData unitVirtualFund43 = new FixedLengthStringData(4).isAPartOf(filler1, 168);
	  	public FixedLengthStringData unitVirtualFund44 = new FixedLengthStringData(4).isAPartOf(filler1, 172);
	  	public FixedLengthStringData unitVirtualFund45 = new FixedLengthStringData(4).isAPartOf(filler1, 176);

	  	
	  	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(27).isAPartOf(tr58yrec, 405);
	  	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(9, 3, 0, ageIssageFrms, 0);
	  	public FixedLengthStringData filler11 = new FixedLengthStringData(27).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
	  	public ZonedDecimalData ageIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 0);
	  	public ZonedDecimalData ageIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 3);
	  	public ZonedDecimalData ageIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 6);
	  	public ZonedDecimalData ageIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 9);
	  	public ZonedDecimalData ageIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 12);
	  	public ZonedDecimalData ageIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 15);
	  	public ZonedDecimalData ageIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 18);
	  	public ZonedDecimalData ageIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 21);
	  	public ZonedDecimalData ageIssageFrm09 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 24);

	  	
	  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(27).isAPartOf(tr58yrec, 432);
	  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(9, 3, 0, ageIssageTos, 0);
	  	public FixedLengthStringData filler12 = new FixedLengthStringData(27).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
	  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 0);
	  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 3);
	  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 6);
	  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 9);
	  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 12);
	  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 15);
	  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 18);
	  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 21);
	  	public ZonedDecimalData ageIssageTo09 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 24);
	  	
	  	public FixedLengthStringData filler3 = new FixedLengthStringData(41).isAPartOf(tr58yrec, 459, FILLER);
	  	
		public void initialize() {
			COBOLFunctions.initialize(tr58yrec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			tr58yrec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}