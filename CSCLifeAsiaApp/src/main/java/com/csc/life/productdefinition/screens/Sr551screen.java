package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr551screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr551ScreenVars sv = (Sr551ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr551screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr551ScreenVars screenVars = (Sr551ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.reptname.setClassString("");
		screenVars.securityno.setClassString("");
		screenVars.mloldic.setClassString("");
		screenVars.mlothic.setClassString("");
		screenVars.mlentity.setClassString("");
		screenVars.hpropdteDisp.setClassString("");
		screenVars.clntname.setClassString("");
		screenVars.dobDisp.setClassString("");
		screenVars.mlcoycde.setClassString("");
		screenVars.desi.setClassString("");
		screenVars.rskflg.setClassString("");
		screenVars.mlhsperd.setClassString("");
		screenVars.mlmedcde01.setClassString("");
		screenVars.descrip01.setClassString("");
		screenVars.mlmedcde02.setClassString("");
		screenVars.descrip02.setClassString("");
		screenVars.mlmedcde03.setClassString("");
		screenVars.descrip03.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.indc.setClassString("");
		screenVars.mind.setClassString("");
		screenVars.actn.setClassString("");
	}

/**
 * Clear all the variables in Sr551screen
 */
	public static void clear(VarModel pv) {
		Sr551ScreenVars screenVars = (Sr551ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.reptname.clear();
		screenVars.securityno.clear();
		screenVars.mloldic.clear();
		screenVars.mlothic.clear();
		screenVars.mlentity.clear();
		screenVars.hpropdteDisp.clear();
		screenVars.hpropdte.clear();
		screenVars.clntname.clear();
		screenVars.dobDisp.clear();
		screenVars.dob.clear();
		screenVars.mlcoycde.clear();
		screenVars.desi.clear();
		screenVars.rskflg.clear();
		screenVars.mlhsperd.clear();
		screenVars.mlmedcde01.clear();
		screenVars.descrip01.clear();
		screenVars.mlmedcde02.clear();
		screenVars.descrip02.clear();
		screenVars.mlmedcde03.clear();
		screenVars.descrip03.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.indc.clear();
		screenVars.mind.clear();
		screenVars.actn.clear();
	}
}
