package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Hitdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HitdpfDAO extends BaseDAO<Hitdpf> {
    public Map<Long, List<Hitdpf>> searchHitdRecord(List<Long> covrUQ);
    public Map<String, List<Hitdpf>> searchHitdRecordByChdrnum(List<String> chdrnumList);
    public void updatedHitdRecord(List<Hitdpf> hitdBulkOpList);
    public void updatedHitdInvalidRecord(List<Hitdpf> hitdBulkOpList);
    public void insertHitdRecord(List<Hitdpf> hitdBulkOpList);
    
	public List<Hitdpf> searchHitdrevRecordForDel(String chdrcoy, String chdrnum);
	public void deleteHitdpfRecord(List<Hitdpf> HitdpfList);
	public List<Hitdpf> searchHitddryRecord(String chdrcoy, String chdrnum);
}