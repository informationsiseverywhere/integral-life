package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh605screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh605ScreenVars sv = (Sh605ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh605screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh605ScreenVars screenVars = (Sh605ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.liacoy.setClassString("");
		screenVars.intRate.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.uwind.setClassString("");
		screenVars.crtind.setClassString("");
		screenVars.tsalesind.setClassString("");
		screenVars.agccqind.setClassString("");
		screenVars.bonusInd.setClassString("");
		screenVars.comind.setClassString("");
		screenVars.tdayno.setClassString("");
		screenVars.ratebas.setClassString("");
	}

/**
 * Clear all the variables in Sh605screen
 */
	public static void clear(VarModel pv) {
		Sh605ScreenVars screenVars = (Sh605ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.liacoy.clear();
		screenVars.intRate.clear();
		screenVars.indic.clear();
		screenVars.uwind.clear();
		screenVars.crtind.clear();
		screenVars.tsalesind.clear();
		screenVars.agccqind.clear();
		screenVars.bonusInd.clear();
		screenVars.comind.clear();
		screenVars.tdayno.clear();
		screenVars.ratebas.clear();
	}
}
