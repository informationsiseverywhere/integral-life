package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sh5a6screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh5a6ScreenVars sv = (Sh5a6ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh5a6screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh5a6ScreenVars screenVars = (Sh5a6ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.hpropdteDisp.setClassString("");
		screenVars.hprrcvdtDisp.setClassString("");
		screenVars.huwdcdteDisp.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jownnum.setClassString("");
		screenVars.jownname.setClassString("");
	}

/**
 * Clear all the variables in Sh5a6screen
 */
	public static void clear(VarModel pv) {
		Sh5a6ScreenVars screenVars = (Sh5a6ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cownnum.clear();
		screenVars.lifcnum.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.hpropdteDisp.clear();
		screenVars.hpropdte.clear();
		screenVars.hprrcvdtDisp.clear();
		screenVars.hprrcvdt.clear();
		screenVars.huwdcdteDisp.clear();
		screenVars.huwdcdte.clear();
		screenVars.ctypedes.clear();
		screenVars.ownername.clear();
		screenVars.linsname.clear();
		screenVars.jlinsname.clear();
		screenVars.jlifcnum.clear();
		screenVars.jownnum.clear();
		screenVars.jownname.clear();
	}
}
