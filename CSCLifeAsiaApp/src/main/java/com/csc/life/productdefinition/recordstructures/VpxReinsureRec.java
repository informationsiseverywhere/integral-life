package com.csc.life.productdefinition.recordstructures;

import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class VpxReinsureRec extends ExternalData {
	//VpxReinsureRec
	private static final long serialVersionUID = 1L;
	
	//*******************************
	//Attribute Declarations
	//*******************************
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
	
	private String reinsCode;
	private List<List<String>> reinsSumins;
	private List<List<String>> reinsArngtyp;
	private List<List<String>> reinsTrtqtshrprcnt;
	private List<List<String>> reinsTrtrtnlmt;
	private List<List<String>> reinsRtncurr;
	private List<List<String>> reinsCostfreq;
	private List<List<String>> reinsRatfact;
	private List<List<String>> reinsPremrat;
	private List<List<String>> lifRiskcls;
	private List<List<String>> arngCode;
	private List<List<String>> reinsAccno;
	private List<List<String>> racdRCD;
	
	public String getReinsCode() {
		return reinsCode;
	}

	public void setReinsCode(String reinsCode) {
		this.reinsCode = reinsCode;
	}
	
	public List<List<String>> getReinsSumins() {
		return reinsSumins;
	}

	public void setReinsSumins(List<List<String>> reinsSumins) {
		this.reinsSumins = reinsSumins;
	}

	public List<List<String>> getReinsArngtyp() {
		return reinsArngtyp;
	}

	public void setReinsArngtyp(List<List<String>> reinsArngtyp) {
		this.reinsArngtyp = reinsArngtyp;
	}

	public List<List<String>> getReinsTrtqtshrprcnt() {
		return reinsTrtqtshrprcnt;
	}

	public void setReinsTrtqtshrprcnt(List<List<String>> reinsTrtqtshrprcnt) {
		this.reinsTrtqtshrprcnt = reinsTrtqtshrprcnt;
	}

	public List<List<String>> getReinsTrtrtnlmt() {
		return reinsTrtrtnlmt;
	}

	public void setReinsTrtrtnlmt(List<List<String>> reinsTrtrtnlmt) {
		this.reinsTrtrtnlmt = reinsTrtrtnlmt;
	}

	public List<List<String>> getReinsRtncurr() {
		return reinsRtncurr;
	}

	public void setReinsRtncurr(List<List<String>> reinsRtncurr) {
		this.reinsRtncurr = reinsRtncurr;
	}

	public List<List<String>> getReinsCostfreq() {
		return reinsCostfreq;
	}

	public void setReinsCostfreq(List<List<String>> reinsCostfreq) {
		this.reinsCostfreq = reinsCostfreq;
	}

	public List<List<String>> getReinsRatfact() {
		return reinsRatfact;
	}

	public void setReinsRatfact(List<List<String>> reinsRatfact) {
		this.reinsRatfact = reinsRatfact;
	}

	public List<List<String>> getReinsPremrat() {
		return reinsPremrat;
	}

	public void setReinsPremrat(List<List<String>> reinsPremrat) {
		this.reinsPremrat = reinsPremrat;
	}

	public List<List<String>> getLifRiskcls() {
		return lifRiskcls;
	}

	public void setLifRiskcls(List<List<String>> lifRiskcls) {
		this.lifRiskcls = lifRiskcls;
	}

	public List<List<String>> getArngCode() {
		return arngCode;
	}

	public void setArngCode(List<List<String>> arngCode) {
		this.arngCode = arngCode;
	}

	public List<List<String>> getReinsAccno() {
		return reinsAccno;
	}

	public void setReinsAccno(List<List<String>> reinsAccno) {
		this.reinsAccno = reinsAccno;
	}

	public List<List<String>> getRacdRCD() {
		return racdRCD;
	}

	public void setRacdRCD(List<List<String>> racdRCD) {
		this.racdRCD = racdRCD;
	}
	
	@Override
	public void initialize() {
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}