package com.csc.life.productdefinition.dataaccess.model;

public class Lifeclnt {
    private String lifcnum;
    private String life;
    private String jlife;
    private String clttype;
    private String clgivname;
    private String clsurname;
    private String cllgivname;
    private String cllsurname;
    public String getLifcnum() {
        return lifcnum;
    }
    public void setLifcnum(String lifcnum) {
        this.lifcnum = lifcnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getJlife() {
        return jlife;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    public String getClttype() {
        return clttype;
    }
    public void setClttype(String clttype) {
        this.clttype = clttype;
    }
    public String getClgivname() {
        return clgivname;
    }
    public void setClgivname(String clgivname) {
        this.clgivname = clgivname;
    }
    public String getClsurname() {
        return clsurname;
    }
    public void setClsurname(String clsurname) {
        this.clsurname = clsurname;
    }
    public String getCllgivname() {
        return cllgivname;
    }
    public void setCllgivname(String cllgivname) {
        this.cllgivname = cllgivname;
    }
    public String getCllsurname() {
        return cllsurname;
    }
    public void setCllsurname(String cllsurname) {
        this.cllsurname = cllsurname;
    }
}
