package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5661screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	//public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5661ScreenVars sv = (S5661ScreenVars) pv;
		clearInds(av, sv.getScreenPfInds());
		write(lrec, sv.S5661screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5661ScreenVars screenVars = (S5661ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		/*screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.fupstat.setClassString("");
		screenVars.fuposs01.setClassString("");
		screenVars.fuposs02.setClassString("");
		screenVars.fuposs03.setClassString("");
		screenVars.fuposs04.setClassString("");
		screenVars.fuposs05.setClassString("");
		screenVars.fuposs06.setClassString("");
		screenVars.fuposs07.setClassString("");
		screenVars.fuposs08.setClassString("");
		screenVars.fuposs09.setClassString("");
		screenVars.fuposs10.setClassString("");
		screenVars.zleadays.setClassString("");
		screenVars.zelpdays.setClassString("");
		screenVars.zlettype.setClassString("");
		screenVars.zlettyper.setClassString("");
		screenVars.zletstat.setClassString("");
		screenVars.zletstatr.setClassString("");
		screenVars.zdalind.setClassString("");
		screenVars.zdocind.setClassString("");
		screenVars.message01.setClassString("");
		screenVars.message02.setClassString("");
		screenVars.message03.setClassString("");
		screenVars.message04.setClassString("");
		screenVars.message05.setClassString("");*/
		
		ScreenRecord.setClassStringFormatting(pv);	
	}

/**
 * Clear all the variables in S5661screen
 */
	public static void clear(VarModel pv) {
		S5661ScreenVars screenVars = (S5661ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		/*screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.fupstat.clear();
		screenVars.fuposs01.clear();
		screenVars.fuposs02.clear();
		screenVars.fuposs03.clear();
		screenVars.fuposs04.clear();
		screenVars.fuposs05.clear();
		screenVars.fuposs06.clear();
		screenVars.fuposs07.clear();
		screenVars.fuposs08.clear();
		screenVars.fuposs09.clear();
		screenVars.fuposs10.clear();
		screenVars.zleadays.clear();
		screenVars.zelpdays.clear();
		screenVars.zlettype.clear();
		screenVars.zlettyper.clear();
		screenVars.zletstat.clear();
		screenVars.zletstatr.clear();
		screenVars.zdalind.clear();
		screenVars.zdocind.clear();
		screenVars.message01.clear();
		screenVars.message02.clear();
		screenVars.message03.clear();
		screenVars.message04.clear();
		screenVars.message05.clear();*/

		ScreenRecord.clear(pv);
		
	}
}
