package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR643
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr643ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(141);
	public FixedLengthStringData dataFields = new FixedLengthStringData(61).isAPartOf(dataArea, 0);
	public FixedLengthStringData exmcode = DD.exmcode.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData invref = DD.invref.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData name = DD.name.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData paidby = DD.paidby.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData pgmnam = DD.pgmnam.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 61);
	public FixedLengthStringData exmcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData invrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData nameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData paidbyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData pgmnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 81);
	public FixedLengthStringData[] exmcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] invrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] nameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] paidbyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] pgmnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(187);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(57).isAPartOf(subfileArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,26);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,28);
	public ZonedDecimalData seqno = DD.seqno.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData zmedfee = DD.zmedfee.copyToZonedDecimal().isAPartOf(subfileFields,32);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(subfileFields,49);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 57);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData seqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData zmedfeeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData zmedtypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 89);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] seqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] zmedfeeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] zmedtypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 185);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sr643screensflWritten = new LongData(0);
	public LongData Sr643screenctlWritten = new LongData(0);
	public LongData Sr643screenWritten = new LongData(0);
	public LongData Sr643protectWritten = new LongData(0);
	public GeneralTable sr643screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr643screensfl;
	}

	public Sr643ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"03","10", "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {"04","10", "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeOut,new String[] {"05","10", "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedtypOut,new String[] {"06","10", "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"07","10", "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedfeeOut,new String[] {"08","10", "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paidbyOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(exmcodeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {seqno, chdrnum, chdrsel, life, jlife, zmedtyp, effdate, zmedfee};
		screenSflOutFields = new BaseData[][] {seqnoOut, chdrnumOut, chdrselOut, lifeOut, jlifeOut, zmedtypOut, effdateOut, zmedfeeOut};
		screenSflErrFields = new BaseData[] {seqnoErr, chdrnumErr, chdrselErr, lifeErr, jlifeErr, zmedtypErr, effdateErr, zmedfeeErr};
		screenSflDateFields = new BaseData[] {effdate};
		screenSflDateErrFields = new BaseData[] {effdateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp};

		screenFields = new BaseData[] {pgmnam, invref, paidby, exmcode, name};
		screenOutFields = new BaseData[][] {pgmnamOut, invrefOut, paidbyOut, exmcodeOut, nameOut};
		screenErrFields = new BaseData[] {pgmnamErr, invrefErr, paidbyErr, exmcodeErr, nameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr643screen.class;
		screenSflRecord = Sr643screensfl.class;
		screenCtlRecord = Sr643screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr643protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr643screenctl.lrec.pageSubfile);
	}
}
