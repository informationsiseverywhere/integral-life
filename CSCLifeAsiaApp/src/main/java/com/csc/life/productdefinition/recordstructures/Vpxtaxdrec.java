package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 22 Aug 2012 14:07:00
 * Description:
 * Copybook name: Vpxtaxdrec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxtaxdrec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
	public FixedLengthStringData vpxtaxdRec = new FixedLengthStringData(36);
  	public PackedDecimalData premTax1 = new PackedDecimalData(15, 2).isAPartOf(vpxtaxdRec, 0);
  	public PackedDecimalData premTax2 = new PackedDecimalData(15, 2).isAPartOf(vpxtaxdRec, 8);
  	public PackedDecimalData premTax3 = new PackedDecimalData(15, 2).isAPartOf(vpxtaxdRec, 16);
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxtaxdRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxtaxdRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}