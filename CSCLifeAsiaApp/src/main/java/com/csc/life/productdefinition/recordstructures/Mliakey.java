package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:45
 * Description:
 * Copybook name: MLIAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mliakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mliaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mliaKey = new FixedLengthStringData(64).isAPartOf(mliaFileKey, 0, REDEFINE);
  	public FixedLengthStringData mliaSecurityno = new FixedLengthStringData(15).isAPartOf(mliaKey, 0);
  	public FixedLengthStringData mliaMlentity = new FixedLengthStringData(16).isAPartOf(mliaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(33).isAPartOf(mliaKey, 31, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mliaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mliaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}