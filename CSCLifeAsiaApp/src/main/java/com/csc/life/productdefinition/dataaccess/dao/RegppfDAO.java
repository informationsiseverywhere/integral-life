package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RegppfDAO extends BaseDAO<Regppf> {
    public Map<Long, List<Regppf>> searchRegpRecord(List<Long> annyUQ);
    public Map<String,List<Regppf>> searchRegppfByChdrnum(List<String> chdrnumList);
    public void updateRegpRecord(String validflag , List<Long> regpBulkOpList);
    public void updateRegpfRecord(List<Regppf> regpBulkOpList);
    public void insertRegppfRecord(List<Regppf> insertRegppfList);
    public void updateRegpStatRecord(List<Regppf> regpBulkOpList);
    public List<Regppf> searchRegppfRecord(String chdrcoy, int certdate, String validflag, String tableName, int batchExtractSize, int batchID);
    public Map<String, List<Regppf>> searchRecords(List<String> chdrNumList);
    void updateValidflag(List<Regppf> list);
    void insertRecords(List<Regppf> list);
    public List<Regppf> readRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider);
    public List<Regppf> readRegpRecord(String chdrcoy,String chdrnum);
    public Regppf getRegpRecord(Regppf regppf); 
    public void updateRegpWDDet(List<Regppf> regpBulkOpList) ;
    public boolean updateRegpValidFlag(Regppf regppf);
	public List<Regppf> readRegpByRgpytype(String chdrnum,String rgpytype);
	public void updatePymtPrcnt(List<Regppf> list);
	public List<Regppf> getReasonByKey(String string, String string2, String string3, String string4, String string5);//ILIFE-8299
	public List<Regppf> readRegpfRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider, String sacscode, String sacstype);
	public void deleteRegppfRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider, String sacscode, String sacstype);
	public void insertRegppfRecord(Regppf regppf);
	public void updateNextDate(Regppf regppf);
	public void updatePayee(Regppf regppf);
	public void updateNDate(Regppf regppf);
	public void updatepymnt(Regppf regppf);////ILJ-686 
}
