package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Zafapf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZafapfDAO extends BaseDAO<Zafapf>
{
	 public void insertZafapfRecord(List<Zafapf> zafapfList);
	 
}
