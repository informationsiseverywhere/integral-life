package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR51H
 * @version 1.0 generated on 30/08/09 07:17
 * @author Quipoz
 */
public class Sr51hScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(956);
	public FixedLengthStringData dataFields = new FixedLengthStringData(220).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData crtables = new FixedLengthStringData(80).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(20, 4, crtables, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(80).isAPartOf(crtables, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01 = DD.crtable.copy().isAPartOf(filler,0);
	public FixedLengthStringData crtable02 = DD.crtable.copy().isAPartOf(filler,4);
	public FixedLengthStringData crtable03 = DD.crtable.copy().isAPartOf(filler,8);
	public FixedLengthStringData crtable04 = DD.crtable.copy().isAPartOf(filler,12);
	public FixedLengthStringData crtable05 = DD.crtable.copy().isAPartOf(filler,16);
	public FixedLengthStringData crtable06 = DD.crtable.copy().isAPartOf(filler,20);
	public FixedLengthStringData crtable07 = DD.crtable.copy().isAPartOf(filler,24);
	public FixedLengthStringData crtable08 = DD.crtable.copy().isAPartOf(filler,28);
	public FixedLengthStringData crtable09 = DD.crtable.copy().isAPartOf(filler,32);
	public FixedLengthStringData crtable10 = DD.crtable.copy().isAPartOf(filler,36);
	public FixedLengthStringData crtable11 = DD.crtable.copy().isAPartOf(filler,40);
	public FixedLengthStringData crtable12 = DD.crtable.copy().isAPartOf(filler,44);
	public FixedLengthStringData crtable13 = DD.crtable.copy().isAPartOf(filler,48);
	public FixedLengthStringData crtable14 = DD.crtable.copy().isAPartOf(filler,52);
	public FixedLengthStringData crtable15 = DD.crtable.copy().isAPartOf(filler,56);
	public FixedLengthStringData crtable16 = DD.crtable.copy().isAPartOf(filler,60);
	public FixedLengthStringData crtable17 = DD.crtable.copy().isAPartOf(filler,64);
	public FixedLengthStringData crtable18 = DD.crtable.copy().isAPartOf(filler,68);
	public FixedLengthStringData crtable19 = DD.crtable.copy().isAPartOf(filler,72);
	public FixedLengthStringData crtable20 = DD.crtable.copy().isAPartOf(filler,76);
	public FixedLengthStringData ctables = new FixedLengthStringData(80).isAPartOf(dataFields, 81);
	public FixedLengthStringData[] ctable = FLSArrayPartOfStructure(20, 4, ctables, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(ctables, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctable01 = DD.ctable.copy().isAPartOf(filler1,0);
	public FixedLengthStringData ctable02 = DD.ctable.copy().isAPartOf(filler1,4);
	public FixedLengthStringData ctable03 = DD.ctable.copy().isAPartOf(filler1,8);
	public FixedLengthStringData ctable04 = DD.ctable.copy().isAPartOf(filler1,12);
	public FixedLengthStringData ctable05 = DD.ctable.copy().isAPartOf(filler1,16);
	public FixedLengthStringData ctable06 = DD.ctable.copy().isAPartOf(filler1,20);
	public FixedLengthStringData ctable07 = DD.ctable.copy().isAPartOf(filler1,24);
	public FixedLengthStringData ctable08 = DD.ctable.copy().isAPartOf(filler1,28);
	public FixedLengthStringData ctable09 = DD.ctable.copy().isAPartOf(filler1,32);
	public FixedLengthStringData ctable10 = DD.ctable.copy().isAPartOf(filler1,36);
	public FixedLengthStringData ctable11 = DD.ctable.copy().isAPartOf(filler1,40);
	public FixedLengthStringData ctable12 = DD.ctable.copy().isAPartOf(filler1,44);
	public FixedLengthStringData ctable13 = DD.ctable.copy().isAPartOf(filler1,48);
	public FixedLengthStringData ctable14 = DD.ctable.copy().isAPartOf(filler1,52);
	public FixedLengthStringData ctable15 = DD.ctable.copy().isAPartOf(filler1,56);
	public FixedLengthStringData ctable16 = DD.ctable.copy().isAPartOf(filler1,60);
	public FixedLengthStringData ctable17 = DD.ctable.copy().isAPartOf(filler1,64);
	public FixedLengthStringData ctable18 = DD.ctable.copy().isAPartOf(filler1,68);
	public FixedLengthStringData ctable19 = DD.ctable.copy().isAPartOf(filler1,72);
	public FixedLengthStringData ctable20 = DD.ctable.copy().isAPartOf(filler1,76);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,161);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,169);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,177);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(184).isAPartOf(dataArea, 220);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData crtablesErr = new FixedLengthStringData(80).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] crtableErr = FLSArrayPartOfStructure(20, 4, crtablesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(80).isAPartOf(crtablesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crtable01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData crtable02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData crtable03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData crtable04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData crtable05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData crtable06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData crtable07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData crtable08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData crtable09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData crtable10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData crtable11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData crtable12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData crtable13Err = new FixedLengthStringData(4).isAPartOf(filler2, 48);
	public FixedLengthStringData crtable14Err = new FixedLengthStringData(4).isAPartOf(filler2, 52);
	public FixedLengthStringData crtable15Err = new FixedLengthStringData(4).isAPartOf(filler2, 56);
	public FixedLengthStringData crtable16Err = new FixedLengthStringData(4).isAPartOf(filler2, 60);
	public FixedLengthStringData crtable17Err = new FixedLengthStringData(4).isAPartOf(filler2, 64);
	public FixedLengthStringData crtable18Err = new FixedLengthStringData(4).isAPartOf(filler2, 68);
	public FixedLengthStringData crtable19Err = new FixedLengthStringData(4).isAPartOf(filler2, 72);
	public FixedLengthStringData crtable20Err = new FixedLengthStringData(4).isAPartOf(filler2, 76);
	public FixedLengthStringData ctablesErr = new FixedLengthStringData(80).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData[] ctableErr = FLSArrayPartOfStructure(20, 4, ctablesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(80).isAPartOf(ctablesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctable01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData ctable02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData ctable03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData ctable04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData ctable05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData ctable06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData ctable07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData ctable08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData ctable09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData ctable10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData ctable11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData ctable12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData ctable13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData ctable14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData ctable15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData ctable16Err = new FixedLengthStringData(4).isAPartOf(filler3, 60);
	public FixedLengthStringData ctable17Err = new FixedLengthStringData(4).isAPartOf(filler3, 64);
	public FixedLengthStringData ctable18Err = new FixedLengthStringData(4).isAPartOf(filler3, 68);
	public FixedLengthStringData ctable19Err = new FixedLengthStringData(4).isAPartOf(filler3, 72);
	public FixedLengthStringData ctable20Err = new FixedLengthStringData(4).isAPartOf(filler3, 76);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(552).isAPartOf(dataArea, 404);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData crtablesOut = new FixedLengthStringData(240).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(20, 12, crtablesOut, 0);
	public FixedLengthStringData[][] crtableO = FLSDArrayPartOfArrayStructure(12, 1, crtableOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(240).isAPartOf(crtablesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crtable01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] crtable02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] crtable03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] crtable04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] crtable05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] crtable06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] crtable07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] crtable08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] crtable09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] crtable10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] crtable11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] crtable12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] crtable13Out = FLSArrayPartOfStructure(12, 1, filler4, 144);
	public FixedLengthStringData[] crtable14Out = FLSArrayPartOfStructure(12, 1, filler4, 156);
	public FixedLengthStringData[] crtable15Out = FLSArrayPartOfStructure(12, 1, filler4, 168);
	public FixedLengthStringData[] crtable16Out = FLSArrayPartOfStructure(12, 1, filler4, 180);
	public FixedLengthStringData[] crtable17Out = FLSArrayPartOfStructure(12, 1, filler4, 192);
	public FixedLengthStringData[] crtable18Out = FLSArrayPartOfStructure(12, 1, filler4, 204);
	public FixedLengthStringData[] crtable19Out = FLSArrayPartOfStructure(12, 1, filler4, 216);
	public FixedLengthStringData[] crtable20Out = FLSArrayPartOfStructure(12, 1, filler4, 228);
	public FixedLengthStringData ctablesOut = new FixedLengthStringData(240).isAPartOf(outputIndicators, 252);
	public FixedLengthStringData[] ctableOut = FLSArrayPartOfStructure(20, 12, ctablesOut, 0);
	public FixedLengthStringData[][] ctableO = FLSDArrayPartOfArrayStructure(12, 1, ctableOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(240).isAPartOf(ctablesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ctable01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] ctable02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] ctable03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] ctable04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] ctable05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] ctable06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] ctable07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] ctable08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] ctable09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] ctable10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] ctable11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] ctable12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] ctable13Out = FLSArrayPartOfStructure(12, 1, filler5, 144);
	public FixedLengthStringData[] ctable14Out = FLSArrayPartOfStructure(12, 1, filler5, 156);
	public FixedLengthStringData[] ctable15Out = FLSArrayPartOfStructure(12, 1, filler5, 168);
	public FixedLengthStringData[] ctable16Out = FLSArrayPartOfStructure(12, 1, filler5, 180);
	public FixedLengthStringData[] ctable17Out = FLSArrayPartOfStructure(12, 1, filler5, 192);
	public FixedLengthStringData[] ctable18Out = FLSArrayPartOfStructure(12, 1, filler5, 204);
	public FixedLengthStringData[] ctable19Out = FLSArrayPartOfStructure(12, 1, filler5, 216);
	public FixedLengthStringData[] ctable20Out = FLSArrayPartOfStructure(12, 1, filler5, 228);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr51hscreenWritten = new LongData(0);
	public LongData Sr51hprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr51hScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(crtable01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable13Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable14Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable15Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable16Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable17Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable18Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable19Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtable20Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable01Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable02Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable03Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable04Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable05Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable06Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable07Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable08Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable09Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable10Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable11Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable12Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable13Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable14Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable15Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable16Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable17Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable18Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable19Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable20Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, crtable01, crtable02, crtable03, crtable04, crtable05, crtable06, crtable07, crtable08, crtable09, crtable10, crtable11, crtable12, crtable13, crtable14, crtable15, crtable16, crtable17, crtable18, crtable19, crtable20, ctable01, ctable02, ctable03, ctable04, ctable05, ctable06, ctable07, ctable08, ctable09, ctable10, ctable11, ctable12, ctable13, ctable14, ctable15, ctable16, ctable17, ctable18, ctable19, ctable20};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, crtable01Out, crtable02Out, crtable03Out, crtable04Out, crtable05Out, crtable06Out, crtable07Out, crtable08Out, crtable09Out, crtable10Out, crtable11Out, crtable12Out, crtable13Out, crtable14Out, crtable15Out, crtable16Out, crtable17Out, crtable18Out, crtable19Out, crtable20Out, ctable01Out, ctable02Out, ctable03Out, ctable04Out, ctable05Out, ctable06Out, ctable07Out, ctable08Out, ctable09Out, ctable10Out, ctable11Out, ctable12Out, ctable13Out, ctable14Out, ctable15Out, ctable16Out, ctable17Out, ctable18Out, ctable19Out, ctable20Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, crtable01Err, crtable02Err, crtable03Err, crtable04Err, crtable05Err, crtable06Err, crtable07Err, crtable08Err, crtable09Err, crtable10Err, crtable11Err, crtable12Err, crtable13Err, crtable14Err, crtable15Err, crtable16Err, crtable17Err, crtable18Err, crtable19Err, crtable20Err, ctable01Err, ctable02Err, ctable03Err, ctable04Err, ctable05Err, ctable06Err, ctable07Err, ctable08Err, ctable09Err, ctable10Err, ctable11Err, ctable12Err, ctable13Err, ctable14Err, ctable15Err, ctable16Err, ctable17Err, ctable18Err, ctable19Err, ctable20Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr51hscreen.class;
		protectRecord = Sr51hprotect.class;
	}

}
