/*
 * File: Th550pt.java
 * Date: 30 August 2009 2:35:36
 * Author: Quipoz Limited
 * 
 * Class transformed from TH550PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Th550rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH550.
*
*
*****************************************************************
* </pre>
*/
public class Th550pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Original Term/Inforce Duration Keys            SH550");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 13);
	private FixedLengthStringData filler8 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 23, FILLER).init("  Valid to:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(75);
	private FixedLengthStringData filler9 = new FixedLengthStringData(75).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Inforce  Inforce  Inforce Inforce Inforce Inforce InforceInforce Inforce");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler10 = new FixedLengthStringData(74).isAPartOf(wsaaPrtLine005, 0, FILLER).init("   to yr    to yr    to yr   to yr   to yr   to yr   to yr to yr   to yr");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler11 = new FixedLengthStringData(73).isAPartOf(wsaaPrtLine006, 0, FILLER).init("    key      key      key     key     key     key     key  key     key");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(75);
	private FixedLengthStringData filler12 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 3).setPattern("ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 7);
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 12).setPattern("ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 16);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 21).setPattern("ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 25);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 29).setPattern("ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 37).setPattern("ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 49);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 57);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 61).setPattern("ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 65);
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 69).setPattern("ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 73);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 3).setPattern("ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 7);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 12).setPattern("ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 16);
	private FixedLengthStringData filler34 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 21).setPattern("ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 25);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 29).setPattern("ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 37).setPattern("ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 49);
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 57);
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 61).setPattern("ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 65);
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 69).setPattern("ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 73);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(75);
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 3).setPattern("ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 7);
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 12).setPattern("ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 16);
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 21).setPattern("ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 25);
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 29).setPattern("ZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 37).setPattern("ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41);
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 49);
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 53).setPattern("ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57);
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 61).setPattern("ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 65);
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 69).setPattern("ZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 73);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(75);
	private FixedLengthStringData filler66 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 3).setPattern("ZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 7);
	private FixedLengthStringData filler68 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 12).setPattern("ZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 16);
	private FixedLengthStringData filler70 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 21).setPattern("ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 25);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 29).setPattern("ZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41);
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 49);
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 57);
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 61).setPattern("ZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 65);
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 69).setPattern("ZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 73);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(75);
	private FixedLengthStringData filler84 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 3).setPattern("ZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 7);
	private FixedLengthStringData filler86 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 12).setPattern("ZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 16);
	private FixedLengthStringData filler88 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 21).setPattern("ZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 25);
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 29).setPattern("ZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler92 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 37).setPattern("ZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41);
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 45).setPattern("ZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 49);
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 53).setPattern("ZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 57);
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 61).setPattern("ZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 65);
	private FixedLengthStringData filler100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 69).setPattern("ZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 73);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(75);
	private FixedLengthStringData filler102 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 3).setPattern("ZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 7);
	private FixedLengthStringData filler104 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 12).setPattern("ZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 16);
	private FixedLengthStringData filler106 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 21).setPattern("ZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 25);
	private FixedLengthStringData filler108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 29).setPattern("ZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 33);
	private FixedLengthStringData filler110 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 37).setPattern("ZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 41);
	private FixedLengthStringData filler112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 45).setPattern("ZZ");
	private FixedLengthStringData filler113 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 49);
	private FixedLengthStringData filler114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 53).setPattern("ZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo110 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 57);
	private FixedLengthStringData filler116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 61).setPattern("ZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 65);
	private FixedLengthStringData filler118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 69).setPattern("ZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 73);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(75);
	private FixedLengthStringData filler120 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 3).setPattern("ZZ");
	private FixedLengthStringData filler121 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 7);
	private FixedLengthStringData filler122 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 12).setPattern("ZZ");
	private FixedLengthStringData filler123 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 16);
	private FixedLengthStringData filler124 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo119 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 21).setPattern("ZZ");
	private FixedLengthStringData filler125 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 25);
	private FixedLengthStringData filler126 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 29).setPattern("ZZ");
	private FixedLengthStringData filler127 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo122 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 33);
	private FixedLengthStringData filler128 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo123 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 37).setPattern("ZZ");
	private FixedLengthStringData filler129 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo124 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 41);
	private FixedLengthStringData filler130 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo125 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 45).setPattern("ZZ");
	private FixedLengthStringData filler131 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo126 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 49);
	private FixedLengthStringData filler132 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo127 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 53).setPattern("ZZ");
	private FixedLengthStringData filler133 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo128 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 57);
	private FixedLengthStringData filler134 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo129 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 61).setPattern("ZZ");
	private FixedLengthStringData filler135 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo130 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 65);
	private FixedLengthStringData filler136 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo131 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 69).setPattern("ZZ");
	private FixedLengthStringData filler137 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo132 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 73);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(75);
	private FixedLengthStringData filler138 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo133 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 3).setPattern("ZZ");
	private FixedLengthStringData filler139 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo134 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 7);
	private FixedLengthStringData filler140 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo135 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 12).setPattern("ZZ");
	private FixedLengthStringData filler141 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo136 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 16);
	private FixedLengthStringData filler142 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo137 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 21).setPattern("ZZ");
	private FixedLengthStringData filler143 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo138 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 25);
	private FixedLengthStringData filler144 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo139 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 29).setPattern("ZZ");
	private FixedLengthStringData filler145 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo140 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 33);
	private FixedLengthStringData filler146 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo141 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 37).setPattern("ZZ");
	private FixedLengthStringData filler147 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo142 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 41);
	private FixedLengthStringData filler148 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo143 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 45).setPattern("ZZ");
	private FixedLengthStringData filler149 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo144 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 49);
	private FixedLengthStringData filler150 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo145 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 53).setPattern("ZZ");
	private FixedLengthStringData filler151 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo146 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 57);
	private FixedLengthStringData filler152 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo147 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 61).setPattern("ZZ");
	private FixedLengthStringData filler153 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo148 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 65);
	private FixedLengthStringData filler154 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo149 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 69).setPattern("ZZ");
	private FixedLengthStringData filler155 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo150 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 73);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(75);
	private FixedLengthStringData filler156 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo151 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 3).setPattern("ZZ");
	private FixedLengthStringData filler157 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo152 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 7);
	private FixedLengthStringData filler158 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo153 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 12).setPattern("ZZ");
	private FixedLengthStringData filler159 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo154 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 16);
	private FixedLengthStringData filler160 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo155 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 21).setPattern("ZZ");
	private FixedLengthStringData filler161 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo156 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 25);
	private FixedLengthStringData filler162 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo157 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 29).setPattern("ZZ");
	private FixedLengthStringData filler163 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo158 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 33);
	private FixedLengthStringData filler164 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo159 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 37).setPattern("ZZ");
	private FixedLengthStringData filler165 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo160 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 41);
	private FixedLengthStringData filler166 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo161 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 45).setPattern("ZZ");
	private FixedLengthStringData filler167 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo162 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 49);
	private FixedLengthStringData filler168 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo163 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 53).setPattern("ZZ");
	private FixedLengthStringData filler169 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo164 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 57);
	private FixedLengthStringData filler170 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo165 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 61).setPattern("ZZ");
	private FixedLengthStringData filler171 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo166 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 65);
	private FixedLengthStringData filler172 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo167 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 69).setPattern("ZZ");
	private FixedLengthStringData filler173 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo168 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 73);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(67);
	private FixedLengthStringData filler174 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo169 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 3).setPattern("ZZ");
	private FixedLengthStringData filler175 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo170 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 7);
	private FixedLengthStringData filler176 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo171 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 12).setPattern("ZZ");
	private FixedLengthStringData filler177 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo172 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 16);
	private FixedLengthStringData filler178 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo173 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 21).setPattern("ZZ");
	private FixedLengthStringData filler179 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo174 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 25);
	private FixedLengthStringData filler180 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo175 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 29).setPattern("ZZ");
	private FixedLengthStringData filler181 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo176 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 33);
	private FixedLengthStringData filler182 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo177 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 37).setPattern("ZZ");
	private FixedLengthStringData filler183 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo178 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 41);
	private FixedLengthStringData filler184 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo179 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 45).setPattern("ZZ");
	private FixedLengthStringData filler185 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo180 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 49);
	private FixedLengthStringData filler186 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo181 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 53).setPattern("ZZ");
	private FixedLengthStringData filler187 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo182 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 57);
	private FixedLengthStringData filler188 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo183 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 61).setPattern("ZZ");
	private FixedLengthStringData filler189 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo184 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 65);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(67);
	private FixedLengthStringData filler190 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo185 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 3).setPattern("ZZ");
	private FixedLengthStringData filler191 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo186 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 7);
	private FixedLengthStringData filler192 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo187 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 12).setPattern("ZZ");
	private FixedLengthStringData filler193 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo188 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 16);
	private FixedLengthStringData filler194 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo189 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 21).setPattern("ZZ");
	private FixedLengthStringData filler195 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo190 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 25);
	private FixedLengthStringData filler196 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo191 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 29).setPattern("ZZ");
	private FixedLengthStringData filler197 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo192 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 33);
	private FixedLengthStringData filler198 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo193 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 37).setPattern("ZZ");
	private FixedLengthStringData filler199 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo194 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 41);
	private FixedLengthStringData filler200 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo195 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 45).setPattern("ZZ");
	private FixedLengthStringData filler201 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo196 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 49);
	private FixedLengthStringData filler202 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo197 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 53).setPattern("ZZ");
	private FixedLengthStringData filler203 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo198 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 57);
	private FixedLengthStringData filler204 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo199 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 61).setPattern("ZZ");
	private FixedLengthStringData filler205 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo200 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 65);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(28);
	private FixedLengthStringData filler206 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine018, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th550rec th550rec = new Th550rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th550pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th550rec.th550Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(th550rec.zrterm01);
		fieldNo008.set(th550rec.zkey01);
		fieldNo025.set(th550rec.zrterm02);
		fieldNo026.set(th550rec.zkey02);
		fieldNo043.set(th550rec.zrterm03);
		fieldNo044.set(th550rec.zkey03);
		fieldNo061.set(th550rec.zrterm04);
		fieldNo062.set(th550rec.zkey04);
		fieldNo079.set(th550rec.zrterm05);
		fieldNo080.set(th550rec.zkey05);
		fieldNo097.set(th550rec.zrterm06);
		fieldNo098.set(th550rec.zkey06);
		fieldNo115.set(th550rec.zrterm07);
		fieldNo116.set(th550rec.zkey07);
		fieldNo133.set(th550rec.zrterm08);
		fieldNo134.set(th550rec.zkey08);
		fieldNo151.set(th550rec.zrterm09);
		fieldNo152.set(th550rec.zkey09);
		fieldNo169.set(th550rec.zrterm10);
		fieldNo170.set(th550rec.zkey10);
		fieldNo185.set(th550rec.zrterm11);
		fieldNo186.set(th550rec.zkey11);
		fieldNo009.set(th550rec.zrterm12);
		fieldNo010.set(th550rec.zkey12);
		fieldNo027.set(th550rec.zrterm13);
		fieldNo028.set(th550rec.zkey13);
		fieldNo045.set(th550rec.zrterm14);
		fieldNo046.set(th550rec.zkey14);
		fieldNo063.set(th550rec.zrterm15);
		fieldNo064.set(th550rec.zkey15);
		fieldNo081.set(th550rec.zrterm16);
		fieldNo082.set(th550rec.zkey16);
		fieldNo099.set(th550rec.zrterm17);
		fieldNo100.set(th550rec.zkey17);
		fieldNo117.set(th550rec.zrterm18);
		fieldNo118.set(th550rec.zkey18);
		fieldNo135.set(th550rec.zrterm19);
		fieldNo136.set(th550rec.zkey19);
		fieldNo153.set(th550rec.zrterm20);
		fieldNo154.set(th550rec.zkey20);
		fieldNo171.set(th550rec.zrterm21);
		fieldNo172.set(th550rec.zkey21);
		fieldNo187.set(th550rec.zrterm22);
		fieldNo188.set(th550rec.zkey22);
		fieldNo011.set(th550rec.zrterm23);
		fieldNo012.set(th550rec.zkey23);
		fieldNo029.set(th550rec.zrterm24);
		fieldNo030.set(th550rec.zkey24);
		fieldNo047.set(th550rec.zrterm25);
		fieldNo048.set(th550rec.zkey25);
		fieldNo065.set(th550rec.zrterm26);
		fieldNo066.set(th550rec.zkey26);
		fieldNo083.set(th550rec.zrterm27);
		fieldNo084.set(th550rec.zkey27);
		fieldNo101.set(th550rec.zrterm28);
		fieldNo102.set(th550rec.zkey28);
		fieldNo119.set(th550rec.zrterm29);
		fieldNo120.set(th550rec.zkey29);
		fieldNo137.set(th550rec.zrterm30);
		fieldNo138.set(th550rec.zkey30);
		fieldNo155.set(th550rec.zrterm31);
		fieldNo156.set(th550rec.zkey31);
		fieldNo173.set(th550rec.zrterm32);
		fieldNo174.set(th550rec.zkey32);
		fieldNo189.set(th550rec.zrterm33);
		fieldNo190.set(th550rec.zkey33);
		fieldNo013.set(th550rec.zrterm34);
		fieldNo014.set(th550rec.zkey34);
		fieldNo031.set(th550rec.zrterm35);
		fieldNo032.set(th550rec.zkey35);
		fieldNo049.set(th550rec.zrterm36);
		fieldNo050.set(th550rec.zkey36);
		fieldNo067.set(th550rec.zrterm37);
		fieldNo068.set(th550rec.zkey37);
		fieldNo085.set(th550rec.zrterm38);
		fieldNo086.set(th550rec.zkey38);
		fieldNo103.set(th550rec.zrterm39);
		fieldNo104.set(th550rec.zkey39);
		fieldNo121.set(th550rec.zrterm40);
		fieldNo122.set(th550rec.zkey40);
		fieldNo139.set(th550rec.zrterm41);
		fieldNo140.set(th550rec.zkey41);
		fieldNo157.set(th550rec.zrterm42);
		fieldNo158.set(th550rec.zkey42);
		fieldNo175.set(th550rec.zrterm43);
		fieldNo176.set(th550rec.zkey43);
		fieldNo191.set(th550rec.zrterm44);
		fieldNo192.set(th550rec.zkey44);
		fieldNo015.set(th550rec.zrterm45);
		fieldNo016.set(th550rec.zkey45);
		fieldNo033.set(th550rec.zrterm46);
		fieldNo034.set(th550rec.zkey46);
		fieldNo051.set(th550rec.zrterm47);
		fieldNo052.set(th550rec.zkey47);
		fieldNo069.set(th550rec.zrterm48);
		fieldNo070.set(th550rec.zkey48);
		fieldNo087.set(th550rec.zrterm49);
		fieldNo088.set(th550rec.zkey49);
		fieldNo105.set(th550rec.zrterm50);
		fieldNo106.set(th550rec.zkey50);
		fieldNo123.set(th550rec.zrterm51);
		fieldNo124.set(th550rec.zkey51);
		fieldNo141.set(th550rec.zrterm52);
		fieldNo142.set(th550rec.zkey52);
		fieldNo159.set(th550rec.zrterm53);
		fieldNo160.set(th550rec.zkey53);
		fieldNo177.set(th550rec.zrterm54);
		fieldNo178.set(th550rec.zkey54);
		fieldNo193.set(th550rec.zrterm55);
		fieldNo194.set(th550rec.zkey55);
		fieldNo017.set(th550rec.zrterm56);
		fieldNo018.set(th550rec.zkey56);
		fieldNo035.set(th550rec.zrterm57);
		fieldNo036.set(th550rec.zkey57);
		fieldNo053.set(th550rec.zrterm58);
		fieldNo054.set(th550rec.zkey58);
		fieldNo071.set(th550rec.zrterm59);
		fieldNo072.set(th550rec.zkey59);
		fieldNo089.set(th550rec.zrterm60);
		fieldNo090.set(th550rec.zkey60);
		fieldNo107.set(th550rec.zrterm61);
		fieldNo108.set(th550rec.zkey61);
		fieldNo125.set(th550rec.zrterm62);
		fieldNo126.set(th550rec.zkey62);
		fieldNo143.set(th550rec.zrterm63);
		fieldNo144.set(th550rec.zkey63);
		fieldNo161.set(th550rec.zrterm64);
		fieldNo162.set(th550rec.zkey64);
		fieldNo179.set(th550rec.zrterm65);
		fieldNo180.set(th550rec.zkey65);
		fieldNo195.set(th550rec.zrterm66);
		fieldNo196.set(th550rec.zkey66);
		fieldNo019.set(th550rec.zrterm67);
		fieldNo020.set(th550rec.zkey67);
		fieldNo037.set(th550rec.zrterm68);
		fieldNo038.set(th550rec.zkey68);
		fieldNo055.set(th550rec.zrterm69);
		fieldNo056.set(th550rec.zkey69);
		fieldNo073.set(th550rec.zrterm70);
		fieldNo074.set(th550rec.zkey70);
		fieldNo091.set(th550rec.zrterm71);
		fieldNo092.set(th550rec.zkey71);
		fieldNo109.set(th550rec.zrterm72);
		fieldNo110.set(th550rec.zkey72);
		fieldNo127.set(th550rec.zrterm73);
		fieldNo128.set(th550rec.zkey73);
		fieldNo145.set(th550rec.zrterm74);
		fieldNo146.set(th550rec.zkey74);
		fieldNo163.set(th550rec.zrterm75);
		fieldNo164.set(th550rec.zkey75);
		fieldNo181.set(th550rec.zrterm76);
		fieldNo182.set(th550rec.zkey76);
		fieldNo197.set(th550rec.zrterm77);
		fieldNo198.set(th550rec.zkey77);
		fieldNo021.set(th550rec.zrterm78);
		fieldNo022.set(th550rec.zkey78);
		fieldNo039.set(th550rec.zrterm79);
		fieldNo040.set(th550rec.zkey79);
		fieldNo057.set(th550rec.zrterm80);
		fieldNo058.set(th550rec.zkey80);
		fieldNo075.set(th550rec.zrterm81);
		fieldNo076.set(th550rec.zkey81);
		fieldNo093.set(th550rec.zrterm82);
		fieldNo094.set(th550rec.zkey82);
		fieldNo111.set(th550rec.zrterm83);
		fieldNo112.set(th550rec.zkey83);
		fieldNo129.set(th550rec.zrterm84);
		fieldNo130.set(th550rec.zkey84);
		fieldNo147.set(th550rec.zrterm85);
		fieldNo148.set(th550rec.zkey85);
		fieldNo165.set(th550rec.zrterm86);
		fieldNo166.set(th550rec.zkey86);
		fieldNo183.set(th550rec.zrterm87);
		fieldNo184.set(th550rec.zkey87);
		fieldNo199.set(th550rec.zrterm88);
		fieldNo200.set(th550rec.zkey88);
		fieldNo023.set(th550rec.zrterm89);
		fieldNo024.set(th550rec.zkey89);
		fieldNo041.set(th550rec.zrterm90);
		fieldNo042.set(th550rec.zkey90);
		fieldNo059.set(th550rec.zrterm91);
		fieldNo060.set(th550rec.zkey91);
		fieldNo077.set(th550rec.zrterm92);
		fieldNo078.set(th550rec.zkey92);
		fieldNo095.set(th550rec.zrterm93);
		fieldNo096.set(th550rec.zkey93);
		fieldNo113.set(th550rec.zrterm94);
		fieldNo114.set(th550rec.zkey94);
		fieldNo131.set(th550rec.zrterm95);
		fieldNo132.set(th550rec.zkey95);
		fieldNo149.set(th550rec.zrterm96);
		fieldNo150.set(th550rec.zkey96);
		fieldNo167.set(th550rec.zrterm97);
		fieldNo168.set(th550rec.zkey97);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
