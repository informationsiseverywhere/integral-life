package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:54
 * Description:
 * Copybook name: T5552REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5552rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5552Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData bsmthsaf = new ZonedDecimalData(2, 0).isAPartOf(t5552Rec, 0);
  	public ZonedDecimalData bsmthsbf = new ZonedDecimalData(2, 0).isAPartOf(t5552Rec, 2);
  	public FixedLengthStringData bteqpt = new FixedLengthStringData(1).isAPartOf(t5552Rec, 4);
  	public ZonedDecimalData ptmthsaf = new ZonedDecimalData(2, 0).isAPartOf(t5552Rec, 5);
  	public ZonedDecimalData ptmthsbf = new ZonedDecimalData(2, 0).isAPartOf(t5552Rec, 7);
  	public FixedLengthStringData filler = new FixedLengthStringData(491).isAPartOf(t5552Rec, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5552Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5552Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}