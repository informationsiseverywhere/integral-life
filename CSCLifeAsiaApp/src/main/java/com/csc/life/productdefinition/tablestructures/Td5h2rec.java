package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Td5h2rec extends ExternalData{

	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData td5h2Rec = new FixedLengthStringData(501);
	  	public FixedLengthStringData accInd = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 0);
	  	public FixedLengthStringData agntsdets = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 1);
	  	public FixedLengthStringData ddind = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 2);
	  	public FixedLengthStringData grpind = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 3);
	  	public FixedLengthStringData mediaRequired = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 4);
	  	public FixedLengthStringData payind = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 5);
	  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(td5h2Rec, 6);
	  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(td5h2Rec, 8);
	  	public FixedLengthStringData triggerRequired = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 10);
	  	public FixedLengthStringData printLocation = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 11);
	  	public FixedLengthStringData crcind = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 12);
		public FixedLengthStringData rollovrind = new FixedLengthStringData(1).isAPartOf(td5h2Rec, 13);
	  	public FixedLengthStringData filler = new FixedLengthStringData(487).isAPartOf(td5h2Rec, 14, FILLER);


		public void initialize() {
			COBOLFunctions.initialize(td5h2Rec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			td5h2Rec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
