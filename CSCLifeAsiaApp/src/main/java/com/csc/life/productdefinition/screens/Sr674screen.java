package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr674screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {19, 23, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr674ScreenVars sv = (Sr674ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr674screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr674ScreenVars screenVars = (Sr674ScreenVars)pv;
		screenVars.zgrsamt01.setClassString("");
		screenVars.zgrsamt02.setClassString("");
		screenVars.zgrsamt03.setClassString("");
		screenVars.zgrsamt04.setClassString("");
		screenVars.zgrsamt05.setClassString("");
		screenVars.zchgamt01.setClassString("");
		screenVars.zchgamt02.setClassString("");
		screenVars.zchgamt03.setClassString("");
		screenVars.zchgamt04.setClassString("");
		screenVars.zchgamt05.setClassString("");
		screenVars.totfld01.setClassString("");
		screenVars.totfld02.setClassString("");
		screenVars.totfld03.setClassString("");
		screenVars.totfld04.setClassString("");
		screenVars.totfld05.setClassString("");
		screenVars.payind.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.crcind.setClassString("");
		screenVars.grpind.setClassString("");
		screenVars.aiind.setClassString("");
		screenVars.amtfld01.setClassString("");
		screenVars.amtfld02.setClassString("");
		screenVars.amtfld03.setClassString("");
		screenVars.amtfld04.setClassString("");
		screenVars.amtfld05.setClassString("");
		screenVars.billday.setClassString("");
		screenVars.zstpduty01.setClassString("");
		screenVars.zstpduty02.setClassString("");
		screenVars.zstpduty03.setClassString("");
		screenVars.zstpduty04.setClassString("");
		screenVars.zstpduty05.setClassString("");
		screenVars.nextinsdteDisp.setClassString("");
		screenVars.tmpChg.setClassString("");
	}

/**
 * Clear all the variables in Sr674screen
 */
	public static void clear(VarModel pv) {
		Sr674ScreenVars screenVars = (Sr674ScreenVars) pv;
		screenVars.zgrsamt01.clear();
		screenVars.zgrsamt02.clear();
		screenVars.zgrsamt03.clear();
		screenVars.zgrsamt04.clear();
		screenVars.zgrsamt05.clear();
		screenVars.zchgamt01.clear();
		screenVars.zchgamt02.clear();
		screenVars.zchgamt03.clear();
		screenVars.zchgamt04.clear();
		screenVars.zchgamt05.clear();
		screenVars.totfld01.clear();
		screenVars.totfld02.clear();
		screenVars.totfld03.clear();
		screenVars.totfld04.clear();
		screenVars.totfld05.clear();
		screenVars.payind.clear();
		screenVars.ddind.clear();
		screenVars.crcind.clear();
		screenVars.grpind.clear();
		screenVars.aiind.clear();
		screenVars.amtfld01.clear();
		screenVars.amtfld02.clear();
		screenVars.amtfld03.clear();
		screenVars.amtfld04.clear();
		screenVars.amtfld05.clear();
		screenVars.billday.clear();
		screenVars.zstpduty01.clear();
		screenVars.zstpduty02.clear();
		screenVars.zstpduty03.clear();
		screenVars.zstpduty04.clear();
		screenVars.zstpduty05.clear();
		screenVars.nextinsdte.clear();
		screenVars.nextinsdteDisp.clear();
		screenVars.tmpChg.clear();
	}
}
