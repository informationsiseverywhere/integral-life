/*
 * File: P5689.java
 * Date: 30 August 2009 0:34:16
 * Author: Quipoz Limited
 * 
 * Class transformed from P5689.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.screens.S5689ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5689 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5689");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5689rec t5689rec = new T5689rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5689ScreenVars sv = ScreenProgram.getScreenVars( S5689ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P5689() {
		super();
		screenVars = sv;
		new ScreenModel("S5689", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
		/*IBPLIFE-5785 start */
		boolean isBillDay = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP011", appVars, "IT");
		if(isBillDay) {
			sv.billdayFlag.set("Y");
		}
		/*IBPLIFE-5785 end */
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5689rec.t5689Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5689rec.rendd01.set(ZERO);
		t5689rec.rendd02.set(ZERO);
		t5689rec.rendd03.set(ZERO);
		t5689rec.rendd04.set(ZERO);
		t5689rec.rendd05.set(ZERO);
		t5689rec.rendd06.set(ZERO);
		t5689rec.rendd07.set(ZERO);
		t5689rec.rendd08.set(ZERO);
		t5689rec.rendd09.set(ZERO);
		t5689rec.rendd10.set(ZERO);
		t5689rec.rendd11.set(ZERO);
		t5689rec.rendd12.set(ZERO);
		t5689rec.rendd13.set(ZERO);
		t5689rec.rendd14.set(ZERO);
		t5689rec.rendd15.set(ZERO);
		t5689rec.rendd16.set(ZERO);
		t5689rec.rendd17.set(ZERO);
		t5689rec.rendd18.set(ZERO);
		t5689rec.rendd19.set(ZERO);
		t5689rec.rendd20.set(ZERO);
		t5689rec.rendd21.set(ZERO);
		t5689rec.rendd22.set(ZERO);
		t5689rec.rendd23.set(ZERO);
		t5689rec.rendd24.set(ZERO);
		t5689rec.rendd25.set(ZERO);
		t5689rec.rendd26.set(ZERO);
		t5689rec.rendd27.set(ZERO);
		t5689rec.rendd28.set(ZERO);
		t5689rec.rendd29.set(ZERO);
		t5689rec.rendd30.set(ZERO);
		// IBPLIFE-4825
		t5689rec.maxdd01.set(ZERO);
		t5689rec.maxdd02.set(ZERO);
		t5689rec.maxdd03.set(ZERO);
		t5689rec.maxdd04.set(ZERO);
		t5689rec.maxdd05.set(ZERO);
		t5689rec.maxdd06.set(ZERO);
		t5689rec.maxdd07.set(ZERO);
		t5689rec.maxdd08.set(ZERO);
		t5689rec.maxdd09.set(ZERO);
		t5689rec.maxdd10.set(ZERO);
		t5689rec.maxdd11.set(ZERO);
		t5689rec.maxdd12.set(ZERO);
		t5689rec.maxdd13.set(ZERO);
		t5689rec.maxdd14.set(ZERO);
		t5689rec.maxdd15.set(ZERO);
		t5689rec.maxdd16.set(ZERO);
		t5689rec.maxdd17.set(ZERO);
		t5689rec.maxdd18.set(ZERO);
		t5689rec.maxdd19.set(ZERO);
		t5689rec.maxdd20.set(ZERO);
		t5689rec.maxdd21.set(ZERO);
		t5689rec.maxdd22.set(ZERO);
		t5689rec.maxdd23.set(ZERO);
		t5689rec.maxdd24.set(ZERO);
		t5689rec.maxdd25.set(ZERO);
		t5689rec.maxdd26.set(ZERO);
		t5689rec.maxdd27.set(ZERO);
		t5689rec.maxdd28.set(ZERO);
		t5689rec.maxdd29.set(ZERO);
		t5689rec.maxdd30.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.billfreqs.set(t5689rec.billfreqs);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.mops.set(t5689rec.mops);
		sv.rendds.set(t5689rec.rendds);
		// IBPLIFE-4825
		sv.maxdds.set(t5689rec.maxdds);
		// IBPLIFE-5785
		sv.allowbds.set(t5689rec.allowBilldays);
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			preparation3010();
			updatePrimaryRecord3050();
			updateRecord3055();
		}
		catch (GOTOException e){
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		checkChanges3100();
		itmdIO.setItemGenarea(t5689rec.t5689Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*OTHER*/
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.billfreqs,t5689rec.billfreqs)) {
			t5689rec.billfreqs.set(sv.billfreqs);
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
		}
		if (isNE(sv.mops,t5689rec.mops)) {
			t5689rec.mops.set(sv.mops);
		}
		if (isNE(sv.rendds,t5689rec.rendds)) {
			t5689rec.rendds.set(sv.rendds);
		}
		// IBPLIFE-4825
		if (isNE(sv.maxdds,t5689rec.maxdds)) {
			t5689rec.maxdds.set(sv.maxdds);
		}
		// IBPLIFE-5785
		if (isNE(sv.allowbds,t5689rec.allowBilldays)) {
			t5689rec.allowBilldays.set(sv.allowbds);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
