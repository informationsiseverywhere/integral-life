package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.MliapfDAO;

import com.csc.life.productdefinition.dataaccess.model.Mliapf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MliapfDAOImpl extends BaseDAOImpl<Mliapf> implements MliapfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MliapfDAOImpl.class);
	
	public boolean  isExistMliasec(String securityNo){
	
		StringBuilder sql = new StringBuilder("SELECT * FROM MLIASEC WHERE SECURITYNO = ? ");
	    sql.append("ORDER BY SECURITYNO ASC, UNIQUE_NUMBER DESC ");

        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        
        boolean result = false;
    try {
    	ps.setString(1, securityNo);
        rs = ps.executeQuery();
        while (rs.next()) {
        	result = true;
            }
        } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return result;

 }
 //ILIFE-8901 start
	
	@Override
	public void bulkupdateMliaafi(List<Mliapf> mliaBulkupdateList) {
		if (mliaBulkupdateList != null && !mliaBulkupdateList.isEmpty()) {       
		String SQL_MLIAAFI_UPDATE = " UPDATE MLIA SET MIND = ?, ACTN = ?, EFFDATE = ?, JOBNM=?,USRPRF=?,DATIME=?  WHERE UNIQUE_NUMBER = ? ";
	    PreparedStatement ps = getPrepareStatement(SQL_MLIAAFI_UPDATE);
	    		
        try {
        	for (Mliapf mliaafi : mliaBulkupdateList) {
        	ps.setString(1, mliaafi.getMind());	
        	ps.setString(2, mliaafi.getActn());
        	ps.setInt(3, mliaafi.getEffdate());
            ps.setString(4, getUsrprf());
            ps.setString(5, getJobnm());
            ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
            ps.setLong(7, mliaafi.getUniqueNumber());
            ps.addBatch();
        	}
            ps.executeUpdate();	
        } catch (SQLException e) {
	LOGGER.error("bulkupdateMliaafi()", e); 
            throw new SQLRuntimeException(e);
        } finally {
			close(ps, null);
		}
		}
		
	}

	@Override
	public void bulkdeleteMlicdByUnique(List<Long> uniquenumberList) {
		if (uniquenumberList != null && !uniquenumberList.isEmpty()) {
			 String SQL_MLIA_DELETE = "DELETE FROM MLIA WHERE UNIQUE_NUMBER = ?";
			 PreparedStatement ps = getPrepareStatement(SQL_MLIA_DELETE);
			try{
				 for (Long mliapf : uniquenumberList) {
				    ps.setLong(1,  mliapf);
				    ps.addBatch();
				 }
				    ps.executeUpdate();				
				
			}catch (SQLException e) {
				LOGGER.error("bulkdeleteMlicdByUnique()",e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);			
			}			
		}
	}

	@Override
	public Map<String, List<Mliapf>> selectMliaRecord(List<String> chdrnumList) {
		
		 StringBuilder sql = new StringBuilder(
				 "SELECT UNIQUE_NUMBER, MLENTITY,MIND, USRPRF, JOBNM, DATIME ");
		 sql.append("FROM VM1DTA.MLIA WHERE ");
		 sql.append(getSqlInStr("MLENTITY", chdrnumList));
	     PreparedStatement psMliaSelect = getPrepareStatement(sql.toString());
	     ResultSet rs = null;
	     Map<String, List<Mliapf>> MliapfSearchResult = new HashMap<String, List<Mliapf>>();
	     try {
	          rs = executeQuery(psMliaSelect);
	          while (rs.next()) {
	            Mliapf  mliapf = new Mliapf();
	          	mliapf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
	          	mliapf.setMlentity(rs.getString("MLENTITY"));
	          	mliapf.setMind(rs.getString("MIND"));
	          	mliapf.setUserProfile(rs.getString("USRPRF"));
	          	mliapf.setJobName(rs.getString("JOBNM"));
	            mliapf.setDatime(rs.getString("DATIME"));
	              
	              if (MliapfSearchResult.containsKey(mliapf.getMlentity())) {
	            	  MliapfSearchResult.get(mliapf.getMlentity()).add(mliapf);
	              } else {
	                  List<Mliapf> mliapfList = new ArrayList<Mliapf>();
	                  mliapfList.add(mliapf);
	                  MliapfSearchResult.put(mliapf.getMlentity(), mliapfList);
	              }
	          }

	      } catch (SQLException e) {
				LOGGER.error("selectMliaRecord()", e); 
	          throw new SQLRuntimeException(e);
	      } finally {
	          close(psMliaSelect, rs);
	      }
	      return MliapfSearchResult;
	}
 //ILIFE-8901 end
}