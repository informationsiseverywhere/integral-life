package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Medipf {
	
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private int seqno;
	private String zmedtyp;
	private String exmcode;
	private BigDecimal zmedfee;
	private String paidby;
	private String invref;
	private int effdate;
	private String actind;
	private int paydte;
	private int tranno;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private Br644DTO br644DTO;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getZmedtyp() {
		return zmedtyp;
	}
	public void setZmedtyp(String zmedtyp) {
		this.zmedtyp = zmedtyp;
	}
	public String getExmcode() {
		return exmcode;
	}
	public void setExmcode(String exmcode) {
		this.exmcode = exmcode;
	}
	public BigDecimal getZmedfee() {
		return zmedfee;
	}
	public void setZmedfee(BigDecimal zmedfee) {
		this.zmedfee = zmedfee;
	}
	public String getPaidby() {
		return paidby;
	}
	public void setPaidby(String paidby) {
		this.paidby = paidby;
	}
	public String getInvref() {
		return invref;
	}
	public void setInvref(String invref) {
		this.invref = invref;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getActind() {
		return actind;
	}
	public void setActind(String actind) {
		this.actind = actind;
	}
	public int getPaydte() {
		return paydte;
	}
	public void setPaydte(int paydte) {
		this.paydte = paydte;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	public Br644DTO getBr644DTO() {
		return br644DTO;
	}
	public void setBr644DTO(Br644DTO br644dto) {
		br644DTO = br644dto;
	}

}