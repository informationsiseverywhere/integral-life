package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR635
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr635ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(96);
	public FixedLengthStringData dataFields = new FixedLengthStringData(48).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnam = DD.clntnam.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 48);
	public FixedLengthStringData clntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData zmedprvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 60);
	public FixedLengthStringData[] clntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] zmedprvOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(105);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(71).isAPartOf(subfileArea, 0);
	public FixedLengthStringData hflag = DD.hflag.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData txtline = DD.txtline.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(8).isAPartOf(subfileArea, 71);
	public FixedLengthStringData hflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData txtlineErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 79);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] txtlineOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 103);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr635screensflWritten = new LongData(0);
	public LongData Sr635screenctlWritten = new LongData(0);
	public LongData Sr635screenWritten = new LongData(0);
	public LongData Sr635protectWritten = new LongData(0);
	public GeneralTable sr635screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr635screensfl;
	}

	public Sr635ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(txtlineOut,new String[] {"50","01","-50",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hflag, txtline};
		screenSflOutFields = new BaseData[][] {hflagOut, txtlineOut};
		screenSflErrFields = new BaseData[] {hflagErr, txtlineErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntnum, zmedprv, clntnam};
		screenOutFields = new BaseData[][] {clntnumOut, zmedprvOut, clntnamOut};
		screenErrFields = new BaseData[] {clntnumErr, zmedprvErr, clntnamErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr635screen.class;
		screenSflRecord = Sr635screensfl.class;
		screenCtlRecord = Sr635screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr635protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr635screenctl.lrec.pageSubfile);
	}
}
