package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:11
 * Description:
 * Copybook name: TR593REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr593rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr593Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData chdrtypes = new FixedLengthStringData(120).isAPartOf(tr593Rec, 0);
  	public FixedLengthStringData[] chdrtype = FLSArrayPartOfStructure(40, 3, chdrtypes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(chdrtypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData chdrtype01 = new FixedLengthStringData(3).isAPartOf(filler, 0);
  	public FixedLengthStringData chdrtype02 = new FixedLengthStringData(3).isAPartOf(filler, 3);
  	public FixedLengthStringData chdrtype03 = new FixedLengthStringData(3).isAPartOf(filler, 6);
  	public FixedLengthStringData chdrtype04 = new FixedLengthStringData(3).isAPartOf(filler, 9);
  	public FixedLengthStringData chdrtype05 = new FixedLengthStringData(3).isAPartOf(filler, 12);
  	public FixedLengthStringData chdrtype06 = new FixedLengthStringData(3).isAPartOf(filler, 15);
  	public FixedLengthStringData chdrtype07 = new FixedLengthStringData(3).isAPartOf(filler, 18);
  	public FixedLengthStringData chdrtype08 = new FixedLengthStringData(3).isAPartOf(filler, 21);
  	public FixedLengthStringData chdrtype09 = new FixedLengthStringData(3).isAPartOf(filler, 24);
  	public FixedLengthStringData chdrtype10 = new FixedLengthStringData(3).isAPartOf(filler, 27);
  	public FixedLengthStringData chdrtype11 = new FixedLengthStringData(3).isAPartOf(filler, 30);
  	public FixedLengthStringData chdrtype12 = new FixedLengthStringData(3).isAPartOf(filler, 33);
  	public FixedLengthStringData chdrtype13 = new FixedLengthStringData(3).isAPartOf(filler, 36);
  	public FixedLengthStringData chdrtype14 = new FixedLengthStringData(3).isAPartOf(filler, 39);
  	public FixedLengthStringData chdrtype15 = new FixedLengthStringData(3).isAPartOf(filler, 42);
  	public FixedLengthStringData chdrtype16 = new FixedLengthStringData(3).isAPartOf(filler, 45);
  	public FixedLengthStringData chdrtype17 = new FixedLengthStringData(3).isAPartOf(filler, 48);
  	public FixedLengthStringData chdrtype18 = new FixedLengthStringData(3).isAPartOf(filler, 51);
  	public FixedLengthStringData chdrtype19 = new FixedLengthStringData(3).isAPartOf(filler, 54);
  	public FixedLengthStringData chdrtype20 = new FixedLengthStringData(3).isAPartOf(filler, 57);
  	public FixedLengthStringData chdrtype21 = new FixedLengthStringData(3).isAPartOf(filler, 60);
  	public FixedLengthStringData chdrtype22 = new FixedLengthStringData(3).isAPartOf(filler, 63);
  	public FixedLengthStringData chdrtype23 = new FixedLengthStringData(3).isAPartOf(filler, 66);
  	public FixedLengthStringData chdrtype24 = new FixedLengthStringData(3).isAPartOf(filler, 69);
  	public FixedLengthStringData chdrtype25 = new FixedLengthStringData(3).isAPartOf(filler, 72);
  	public FixedLengthStringData chdrtype26 = new FixedLengthStringData(3).isAPartOf(filler, 75);
  	public FixedLengthStringData chdrtype27 = new FixedLengthStringData(3).isAPartOf(filler, 78);
  	public FixedLengthStringData chdrtype28 = new FixedLengthStringData(3).isAPartOf(filler, 81);
  	public FixedLengthStringData chdrtype29 = new FixedLengthStringData(3).isAPartOf(filler, 84);
  	public FixedLengthStringData chdrtype30 = new FixedLengthStringData(3).isAPartOf(filler, 87);
  	public FixedLengthStringData chdrtype31 = new FixedLengthStringData(3).isAPartOf(filler, 90);
  	public FixedLengthStringData chdrtype32 = new FixedLengthStringData(3).isAPartOf(filler, 93);
  	public FixedLengthStringData chdrtype33 = new FixedLengthStringData(3).isAPartOf(filler, 96);
  	public FixedLengthStringData chdrtype34 = new FixedLengthStringData(3).isAPartOf(filler, 99);
  	public FixedLengthStringData chdrtype35 = new FixedLengthStringData(3).isAPartOf(filler, 102);
  	public FixedLengthStringData chdrtype36 = new FixedLengthStringData(3).isAPartOf(filler, 105);
  	public FixedLengthStringData chdrtype37 = new FixedLengthStringData(3).isAPartOf(filler, 108);
  	public FixedLengthStringData chdrtype38 = new FixedLengthStringData(3).isAPartOf(filler, 111);
  	public FixedLengthStringData chdrtype39 = new FixedLengthStringData(3).isAPartOf(filler, 114);
  	public FixedLengthStringData chdrtype40 = new FixedLengthStringData(3).isAPartOf(filler, 117);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(380).isAPartOf(tr593Rec, 120, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr593Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr593Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}