package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*   This routine performs data extraction from ACBL for the
*   purpose of passing the said data to the VP/MS calculation
*   subroutine.
*
* </pre>
*/
public class Vpxacbl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "VPXACBL";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

		/* FORMATS */
	private static final String acblrec = "ACBLREC";
	private static final String itdmrec = "ITEMREC";
	private static final String itemrec = "ITEMREC";
		/* ERRORS */
	private static final String e308 = "E308";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Premiumrec premiumrec = new Premiumrec();
	private Vpxacblrec vpxacblrec = new Vpxacblrec();
	
	public Vpxacbl() {
		super();
	}

	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray)
	{
		vpxacblrec.vpxacblRec = convertAndSetParam(vpxacblrec.vpxacblRec, parmArray, 1);
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	protected void main010(){
		initialize(vpxacblrec.vpxacblRec);
		vpxacblrec.statuz.set(varcom.oK);
		vpxacblrec.totRegPrem.set(ZERO);
		vpxacblrec.totSingPrem.set(ZERO);
		if(isEQ(premiumrec.function,"TOPU")&&!isEQ(premiumrec.billfreq,"00")){
			readT56881100();
			readT56451200();
			/*Get the regular premium collected to date from ACBL*/
			initialize(acblIO.getParams());
			if(isEQ(t5688rec.comlvlacc,"N")){
				wsaaRldgacct.set(premiumrec.chdrChdrnum);
			} else{
				wsaaRldgChdrnum.set(premiumrec.chdrChdrnum);
				wsaaRldgLife.set(premiumrec.lifeLife);
				wsaaRldgCoverage.set(premiumrec.covrCoverage);
				wsaaRldgRider.set(premiumrec.covrRider);
				wsaaRldgPlnsfx.set(premiumrec.plnsfx);
			}
			if(isEQ(t5688rec.comlvlacc,"N")){
				wsaaSub.set(1);
			}else{
				wsaaSub.set(2);
			}
			calcPrem1300();
			vpxacblrec.totRegPrem.set(acblIO.getSacscurbal());
			
			/*Get the single premium collected to date from ACBL*/
			initialize(acblIO.getParams());
			if(isEQ(t5688rec.comlvlacc,"N")){
				wsaaSub.set(3);
			}else{
				wsaaSub.set(4);
			}
			calcPrem1300();
			vpxacblrec.totSingPrem.set(acblIO.getSacscurbal());
		}
		exitProgram();
	}
	
	protected void readT56881100(){
		/*Read T5688*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(premiumrec.cnttype);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		
		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)&&!isEQ(itdmIO.getStatuz(),varcom.endp)){
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if(!isEQ(premiumrec.cnttype,itdmIO.getItemitem())
				||!isEQ(itdmIO.getItemcoy(),premiumrec.chdrChdrcoy)
				||!isEQ(itdmIO.getItemtabl(),t5688)
				||isEQ(itdmIO.getStatuz(),varcom.endp)){
			vpxacblrec.statuz.set(e308);
			return;
			} else {
				t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

	protected void readT56451200(){
		/*Read table T5645*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(premiumrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("PRMPM19");
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		
		SmartFileCode.execute(appVars, itemIO);
		if(!isEQ(itemIO.getStatuz(),varcom.oK)){
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}
	
	protected void calcPrem1300(){
		/*Read ACBL file with given parameters*/
		acblIO.setRldgcoy(premiumrec.chdrChdrcoy);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setSacscode(t5645rec.sacscode[wsaaSub.toInt()]);
		acblIO.setSacstyp(t5645rec.sacstype[wsaaSub.toInt()]);
		acblIO.setOrigcurr(premiumrec.currcode);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		
		SmartFileCode.execute(appVars, acblIO);
		if(!isEQ(acblIO.getStatuz(),varcom.oK) && !isEQ(acblIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError9000();
		}
		if(isEQ(acblIO.getStatuz(),varcom.mrnf)){
			acblIO.setSacscurbal(ZERO);
		}
		//if(isEQ(t5645recSign (wsaaSub),  subtract acbl.sacscurbal from 0 giving acblSacscurbal)){
		if(isEQ(t5645rec.sign[wsaaSub.toInt()], "-")) {
			compute(acblIO.sacscurbal, 2).set(sub(0, acblIO.getSacscurbal()));
		}
	}
	
	protected void fatalError9000(){
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(1);
		callProgram(Syserr.class,syserrrec.syserrRec);
		/*EXIT*/
		vpxacblrec.statuz.set(varcom.bomb);
		exitProgram();
	}	
}
