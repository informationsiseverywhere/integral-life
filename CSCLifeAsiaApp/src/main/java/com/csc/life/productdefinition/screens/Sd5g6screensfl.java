package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sd5g6screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 17;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 20, 4, 59}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5g6ScreenVars sv = (Sd5g6ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.Sd5g6screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.Sd5g6screensfl, 
			sv.Sd5g6screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sd5g6ScreenVars sv = (Sd5g6ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.Sd5g6screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sd5g6ScreenVars sv = (Sd5g6ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.Sd5g6screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sd5g6screensflWritten.gt(0))
		{
			sv.Sd5g6screensfl.setCurrentIndex(0);
			sv.Sd5g6screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sd5g6ScreenVars sv = (Sd5g6ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.Sd5g6screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5g6ScreenVars screenVars = (Sd5g6ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.statuz.setFieldName("statuz");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.payfreq.setFieldName("payfreq");
				screenVars.compdesc.setFieldName("compdesc");
				screenVars.sumin.setFieldName("sumin");
				screenVars.riskCessTerm.setFieldName("riskCessTerm");
				screenVars.premCessTerm.setFieldName("premCessTerm");
				screenVars.riskamnt.setFieldName("riskamnt");
				screenVars.modeprem.setFieldName("modeprem");
				screenVars.compcode.setFieldName("compcode");
				
					 
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.statuz.set(dm.getField("statuz"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.payfreq.set(dm.getField("payfreq"));
			screenVars.compdesc.set(dm.getField("compdesc"));
			screenVars.sumin.set(dm.getField("sumin"));
			screenVars.riskCessTerm.set(dm.getField("riskCessTerm"));
			screenVars.premCessTerm.set(dm.getField("premCessTerm"));
			screenVars.riskamnt.set(dm.getField("riskamnt"));
			screenVars.modeprem.set(dm.getField("modeprem"));
			screenVars.compcode.set(dm.getField("compcode"));
		
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5g6ScreenVars screenVars = (Sd5g6ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.statuz.setFieldName("statuz");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.payfreq.setFieldName("payfreq");
				screenVars.compdesc.setFieldName("compdesc");
				screenVars.sumin.setFieldName("sumin");
				screenVars.riskCessTerm.setFieldName("riskCessTerm");
				screenVars.premCessTerm.setFieldName("premCessTerm");
				screenVars.riskamnt.setFieldName("riskamnt");
				screenVars.modeprem.setFieldName("modeprem");
				screenVars.compcode.setFieldName("compcode");
				
				
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("statuz").set(screenVars.statuz);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("payfreq").set(screenVars.payfreq);
			dm.getField("compdesc").set(screenVars.compdesc);
			dm.getField("sumin").set(screenVars.sumin);
			dm.getField("riskCessTerm").set(screenVars.riskCessTerm);
			dm.getField("premCessTerm").set(screenVars.premCessTerm);
			dm.getField("riskamnt").set(screenVars.riskamnt);
			dm.getField("modeprem").set(screenVars.modeprem);
			dm.getField("compcode").set(screenVars.compcode);
			
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sd5g6screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sd5g6ScreenVars screenVars = (Sd5g6ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.statuz.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.payfreq.clearFormatting();
		screenVars.compdesc.clearFormatting();
		screenVars.sumin.clearFormatting();
		screenVars.riskCessTerm.clearFormatting();
		screenVars.premCessTerm.clearFormatting();
		screenVars.riskamnt.clearFormatting();
		screenVars.modeprem.clearFormatting();
		screenVars.compcode.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sd5g6ScreenVars screenVars = (Sd5g6ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.statuz.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.payfreq.setClassString("");
		screenVars.compdesc.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.riskamnt.setClassString("");
		screenVars.modeprem.setClassString("");
		screenVars.compcode.setClassString("");
	}

/**
 * Clear all the variables in Sd5g6screensfl
 */
	public static void clear(VarModel pv) {
		Sd5g6ScreenVars screenVars = (Sd5g6ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.statuz.clear();
		screenVars.chdrnum.clear();
		screenVars.payfreq.clear();
		screenVars.compdesc.clear();
		screenVars.sumin.clear();
		screenVars.riskCessTerm.clear();
		screenVars.premCessTerm.clear();
		screenVars.riskamnt.clear();
		screenVars.modeprem.clear();
		screenVars.compcode.setClassString("");
		
	}
}
