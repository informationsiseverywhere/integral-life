package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:32
 * Description:
 * Copybook name: ACBLBK2KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acblbk2key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acblbk2FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acblbk2Key = new FixedLengthStringData(64).isAPartOf(acblbk2FileKey, 0, REDEFINE);
  	public FixedLengthStringData acblbk2Rldgcoy = new FixedLengthStringData(1).isAPartOf(acblbk2Key, 0);
  	public FixedLengthStringData acblbk2Rldgacct = new FixedLengthStringData(16).isAPartOf(acblbk2Key, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(acblbk2Key, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acblbk2FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acblbk2FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}