/*
 * File: P5667.java
 * Date: 30 August 2009 0:33:36
 * Author: Quipoz Limited
 * 
 * Class transformed from P5667.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.procedures.T5667pt;
import com.csc.life.productdefinition.screens.S5667ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5667 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5667");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaTableLimit = 11;
	private String rl49 = "RL49";
	private String rl50 = "RL50";
	private String rl51 = "RL51";
	private String rl52 = "RL52";
	private String rl53 = "RL53";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5667rec t5667rec = new T5667rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5667ScreenVars sv = ScreenProgram.getScreenVars( S5667ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5667() {
		super();
		screenVars = sv;
		new ScreenModel("S5667", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5667rec.t5667Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5667rec.maxAmount01.set(ZERO);
		t5667rec.maxAmount02.set(ZERO);
		t5667rec.maxAmount03.set(ZERO);
		t5667rec.maxAmount04.set(ZERO);
		t5667rec.maxAmount05.set(ZERO);
		t5667rec.maxAmount06.set(ZERO);
		t5667rec.maxAmount07.set(ZERO);
		t5667rec.maxAmount08.set(ZERO);
		t5667rec.maxAmount09.set(ZERO);
		t5667rec.maxAmount10.set(ZERO);
		t5667rec.maxAmount11.set(ZERO);
		t5667rec.prmtol01.set(ZERO);
		t5667rec.prmtol02.set(ZERO);
		t5667rec.prmtol03.set(ZERO);
		t5667rec.prmtol04.set(ZERO);
		t5667rec.prmtol05.set(ZERO);
		t5667rec.prmtol06.set(ZERO);
		t5667rec.prmtol07.set(ZERO);
		t5667rec.prmtol08.set(ZERO);
		t5667rec.prmtol09.set(ZERO);
		t5667rec.prmtol10.set(ZERO);
		t5667rec.prmtol11.set(ZERO);
		t5667rec.maxamt01.set(ZERO);
		t5667rec.maxamt02.set(ZERO);
		t5667rec.maxamt03.set(ZERO);
		t5667rec.maxamt04.set(ZERO);
		t5667rec.maxamt05.set(ZERO);
		t5667rec.maxamt06.set(ZERO);
		t5667rec.maxamt07.set(ZERO);
		t5667rec.maxamt08.set(ZERO);
		t5667rec.maxamt09.set(ZERO);
		t5667rec.maxamt10.set(ZERO);
		t5667rec.maxamt11.set(ZERO);
		t5667rec.prmtoln01.set(ZERO);
		t5667rec.prmtoln02.set(ZERO);
		t5667rec.prmtoln03.set(ZERO);
		t5667rec.prmtoln04.set(ZERO);
		t5667rec.prmtoln05.set(ZERO);
		t5667rec.prmtoln06.set(ZERO);
		t5667rec.prmtoln07.set(ZERO);
		t5667rec.prmtoln08.set(ZERO);
		t5667rec.prmtoln09.set(ZERO);
		t5667rec.prmtoln10.set(ZERO);
		t5667rec.prmtoln11.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.freqs.set(t5667rec.freqs);
		sv.maxAmounts.set(t5667rec.maxAmounts);
		sv.prmtols.set(t5667rec.prmtols);
		sv.maxamts.set(t5667rec.maxamts);
		sv.prmtolns.set(t5667rec.prmtolns);
		sv.sfind.set(t5667rec.sfind);
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (!(isEQ(sv.sfind,"1")
		|| isEQ(sv.sfind,"2")
		|| isEQ(sv.sfind,SPACES))) {
			sv.sfindErr.set(rl49);
		}
		wsaaIx.set(1);
		for (wsaaIx.set(1); !(isGT(wsaaIx,wsaaTableLimit)); wsaaIx.add(1)){
			if (isEQ(sv.prmtoln[wsaaIx.toInt()],0)
			&& isNE(sv.maxamt[wsaaIx.toInt()],0)) {
				sv.maxamtErr[wsaaIx.toInt()].set(rl53);
				sv.prmtolnErr[wsaaIx.toInt()].set(rl53);
			}
			if (isEQ(sv.maxamt[wsaaIx.toInt()],0)
			&& isNE(sv.prmtoln[wsaaIx.toInt()],0)) {
				sv.maxamtErr[wsaaIx.toInt()].set(rl52);
				sv.prmtolnErr[wsaaIx.toInt()].set(rl52);
			}
			if (isNE(sv.prmtoln[wsaaIx.toInt()],0)
			&& isLT(sv.prmtoln[wsaaIx.toInt()],sv.prmtol[wsaaIx.toInt()])) {
				sv.prmtolnErr[wsaaIx.toInt()].set(rl50);
			}
			if (isNE(sv.maxamt[wsaaIx.toInt()],0)
			&& isLTE(sv.maxamt[wsaaIx.toInt()],sv.maxAmount[wsaaIx.toInt()])) {
				sv.maxamtErr[wsaaIx.toInt()].set(rl51);
			}
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(t5667rec.t5667Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.freqs,t5667rec.freqs)) {
			t5667rec.freqs.set(sv.freqs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxAmounts,t5667rec.maxAmounts)) {
			t5667rec.maxAmounts.set(sv.maxAmounts);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prmtols,t5667rec.prmtols)) {
			t5667rec.prmtols.set(sv.prmtols);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxamts,t5667rec.maxamts)) {
			t5667rec.maxamts.set(sv.maxamts);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prmtolns,t5667rec.prmtolns)) {
			t5667rec.prmtolns.set(sv.prmtolns);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sfind,t5667rec.sfind)) {
			t5667rec.sfind.set(sv.sfind);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5667pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
