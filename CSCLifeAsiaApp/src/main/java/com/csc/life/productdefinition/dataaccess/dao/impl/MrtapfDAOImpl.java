package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MrtapfDAOImpl extends BaseDAOImpl<Mrtapf> implements  MrtapfDAO {
	 private static final Logger LOGGER = LoggerFactory.getLogger(MrtapfDAOImpl.class);

	@Override
	public Mrtapf getMrtaRecord(String chdrcoy, String chdrnum) {
		StringBuilder sqlMrtaSelect1 = new StringBuilder(
				" SELECT CHDRCOY,CHDRNUM,PRAT,COVERC,PRMDETAILS,MLINSOPT,MLRESIND,MBNKREF,LOANDUR,INTCALTYPE,PHUNITVALUE,UNIQUE_NUMBER  ");
		sqlMrtaSelect1.append(" FROM MRTAPF WHERE CHDRCOY = ?  ");
		sqlMrtaSelect1.append(" AND CHDRNUM = ?  ");
		sqlMrtaSelect1
        .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sqlMrtaSelect1.toString());
		ResultSet rs = null;
		Mrtapf mrtapf = null;
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
		
			
			rs = ps.executeQuery();

			while (rs.next()) {
				mrtapf = new Mrtapf();
				mrtapf.setChdrcoy(rs.getString(1));
				mrtapf.setChdrnum(rs.getString(2));
				mrtapf.setPrat(rs.getBigDecimal(3));
				mrtapf.setCoverc(rs.getInt(4));
				mrtapf.setPrmdetails(rs.getString(5));
				mrtapf.setMlinsopt(rs.getString(6));
				mrtapf.setMlresind(rs.getString(7));
				mrtapf.setMbnkref(rs.getString(8));
				mrtapf.setLoandur(rs.getInt(9));
				mrtapf.setIntcalType(rs.getString(10));
				mrtapf.setPhunitvalue(rs.getInt(11));
				mrtapf.setUniqueNumber(rs.getLong(12));
				
	}
    }catch (SQLException e) {
		LOGGER.error("getMrtaRecord()", e);//IJTI-1561
		throw new SQLRuntimeException(e);
	} finally {
		close(ps, rs);
	}
		return mrtapf;
}

	@Override
	public boolean updateMrtaRecord(Mrtapf mrta) {
		boolean updateSuccess = false;
		 String SQL_MRTA_UPDATE = "UPDATE MRTAPF SET CHDRCOY=?,CHDRNUM=?,PRAT=?,COVERC=?,PRMDETAILS=?,MLINSOPT=?,MLRESIND=?,MBNKREF=?,LOANDUR=?,INTCALTYPE=?,PHUNITVALUE=?,USRPRF=?,JOBNM=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
		 PreparedStatement psMrtaUpdate = getPrepareStatement(SQL_MRTA_UPDATE);
      
         try {
        	 psMrtaUpdate.setString(1, mrta.getChdrcoy());
                 psMrtaUpdate.setString(2, mrta.getChdrnum());
                 psMrtaUpdate.setBigDecimal(3,  mrta.getPrat());
                 psMrtaUpdate.setInt(4, mrta.getCoverc());
                 psMrtaUpdate.setString(5, mrta.getPrmdetails());
                 psMrtaUpdate.setString(6, mrta.getMlinsopt());
                 psMrtaUpdate.setString(7, mrta.getMlresind());
                 psMrtaUpdate.setString(8, mrta.getMbnkref());
                 psMrtaUpdate.setInt(9, mrta.getLoandur());
                 psMrtaUpdate.setString(10, mrta.getIntcalType());
                 psMrtaUpdate.setInt(11, mrta.getPhunitvalue());      
                 psMrtaUpdate.setString(12, getJobnm());
                 psMrtaUpdate.setString(13, getUsrprf());
                 psMrtaUpdate.setTimestamp(14, new Timestamp(System.currentTimeMillis()));
                 psMrtaUpdate.setLong(15, mrta.getUniqueNumber()); 
                 //ILIFE-3998 changes started by bkonduru2
                 int updateSuccess1 = psMrtaUpdate.executeUpdate();
                 if(updateSuccess1==1)
                 updateSuccess = true;
                 //ILIFE-3998 changes ended by bkonduru2
         } catch (SQLException e) {
             LOGGER.error("updateMrtaRecord()", e);//IJTI-1561
             throw new SQLRuntimeException(e);
         } finally {
             close(psMrtaUpdate, null);
         }
		return updateSuccess;
	}
	
	public boolean insertMrtapfList(Mrtapf mrta) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO MRTAPF(CHDRCOY,CHDRNUM,PRAT,COVERC,PRMDETAILS,MLINSOPT,MLRESIND,MBNKREF,LOANDUR,INTCALTYPE,PHUNITVALUE,USRPRF,JOBNM,DATIME)");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");
		PreparedStatement mrtaInsert = getPrepareStatement(sb.toString());
		try {
		 mrtaInsert.setString(1, mrta.getChdrcoy());
         mrtaInsert.setString(2, mrta.getChdrnum());
         mrtaInsert.setBigDecimal(3,  mrta.getPrat());
         mrtaInsert.setInt(4, mrta.getCoverc());
         mrtaInsert.setString(5, mrta.getPrmdetails());
         mrtaInsert.setString(6, mrta.getMlinsopt());
         mrtaInsert.setString(7, mrta.getMlresind());
         mrtaInsert.setString(8, mrta.getMbnkref());
         mrtaInsert.setInt(9, mrta.getLoandur());
         mrtaInsert.setString(10, mrta.getIntcalType());
         mrtaInsert.setInt(11, mrta.getPhunitvalue());     
         mrtaInsert.setString(12, getUsrprf());
         mrtaInsert.setString(13, getJobnm());
         mrtaInsert.setTimestamp(14, new Timestamp(System.currentTimeMillis()));
	   	 mrtaInsert.execute();
	   	isInsertSuccessful = true;
		} catch (SQLException e) {
			LOGGER.error("insertSchmpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(mrtaInsert, null);
		}
		
		return isInsertSuccessful;
}
}
