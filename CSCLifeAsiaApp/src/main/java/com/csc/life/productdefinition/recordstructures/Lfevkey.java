package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:04
 * Description:
 * Copybook name: LFEVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lfevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lfevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lfevKey = new FixedLengthStringData(64).isAPartOf(lfevFileKey, 0, REDEFINE);
  	public FixedLengthStringData lfevChdrcoy = new FixedLengthStringData(1).isAPartOf(lfevKey, 0);
  	public FixedLengthStringData lfevChdrnum = new FixedLengthStringData(8).isAPartOf(lfevKey, 1);
  	public FixedLengthStringData lfevLife = new FixedLengthStringData(2).isAPartOf(lfevKey, 9);
  	public PackedDecimalData lfevEffdate = new PackedDecimalData(8, 0).isAPartOf(lfevKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(lfevKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lfevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lfevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}