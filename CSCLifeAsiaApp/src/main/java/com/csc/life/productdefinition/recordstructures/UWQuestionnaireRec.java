package com.csc.life.productdefinition.recordstructures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class UWQuestionnaireRec extends ExternalData{
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
	//Input Fields
	private String occupCode;
	private String contractType;
	private String covgCode;
	private String indusType;
	private List<String> riskClass = new ArrayList<>();
	private BigDecimal transEffdate;
	private List<String> splTerm = new ArrayList<>();
	//Output fields
	private List<String> outputUWDec = new ArrayList<>();
	private List<String> outputSplTermCode = new ArrayList<>();
	private List<String> outputSplTermSAPer = new ArrayList<>();
	private List<String> outputSplTermAge = new ArrayList<>();
	private List<String> outputSplTermLoadPer = new ArrayList<>();
	private List<String> outputSplTermRateAdj = new ArrayList<>();
	private List<String> outputMortPerc = new ArrayList<>();

	public String getOccupCode() {
		return occupCode;
	}

	public void setOccupCode(String occupCode) {
		this.occupCode = occupCode;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getCovgCode() {
		return covgCode;
	}

	public void setCovgCode(String covgCode) {
		this.covgCode = covgCode;
	}

	public String getIndusType() {
		return indusType;
	}

	public void setIndusType(String indusType) {
		this.indusType = indusType;
	}

	public List<String> getRiskClass() {
		return riskClass;
	}

	public void setRiskClass(List<String> riskClass) {
		this.riskClass = riskClass;
	}

	public BigDecimal getTransEffdate() {
		return transEffdate;
	}

	public void setTransEffdate(BigDecimal transEffdate) {
		this.transEffdate = transEffdate;
	}

	public List<String> getSplTerm() {
		return splTerm;
	}

	public void setSplTerm(List<String> splTerm) {
		this.splTerm = splTerm;
	}

	public List<String> getOutputUWDec() {
		return outputUWDec;
	}

	public void setOutputUWDec(List<String> outputUWDec) {
		this.outputUWDec = outputUWDec;
	}

	public List<String> getOutputSplTermCode() {
		return outputSplTermCode;
	}

	public void setOutputSplTermCode(List<String> outputSplTermCode) {
		this.outputSplTermCode = outputSplTermCode;
	}

	public List<String> getOutputSplTermSAPer() {
		return outputSplTermSAPer;
	}

	public void setOutputSplTermSAPer(List<String> outputSplTermSAPer) {
		this.outputSplTermSAPer = outputSplTermSAPer;
	}

	public List<String> getOutputSplTermAge() {
		return outputSplTermAge;
	}

	public void setOutputSplTermAge(List<String> outputSplTermAge) {
		this.outputSplTermAge = outputSplTermAge;
	}

	public List<String> getOutputSplTermLoadPer() {
		return outputSplTermLoadPer;
	}

	public void setOutputSplTermLoadPer(List<String> outputSplTermLoadPer) {
		this.outputSplTermLoadPer = outputSplTermLoadPer;
	}

	public List<String> getOutputSplTermRateAdj() {
		return outputSplTermRateAdj;
	}

	public void setOutputSplTermRateAdj(List<String> outputSplTermRateAdj) {
		this.outputSplTermRateAdj = outputSplTermRateAdj;
	}

	public List<String> getOutputMortPerc() {
		return outputMortPerc;
	}

	public void setOutputMortPerc(List<String> outputMortPerc) {
		this.outputMortPerc = outputMortPerc;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}
