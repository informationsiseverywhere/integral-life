/*
 * File: Pr53b.java
 * Date: December 3, 2013 3:28:38 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR53B.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.productdefinition.recordstructures.Pr53bpar;
import com.csc.life.productdefinition.screens.Sr53bScreenVars;
import com.csc.smart.dataaccess.BparTableDAM;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.dataaccess.DescsmtTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.SdftTableDAM;
import com.csc.smart.dataaccess.WrkelevTableDAM;
import com.csc.smart.dataaccess.WrkhmntTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.batch.cls.Getcurlev;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2002.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*            PR53B MAINLINE.
*
*   Parameter Prompt program  for Product Cloning
*
****************************************************************** ****
* </pre>
*/
public class Pr53b extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR53B");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaStart = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaDescDisplayed = "";
	private FixedLengthStringData wsaaContractType = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaOldCtr = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaNewCtr = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaGetcurlevParm = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGetcurlevLib = new FixedLengthStringData(10).isAPartOf(wsaaGetcurlevParm, 0);
	private FixedLengthStringData wsaaGetcurlevCurrent = new FixedLengthStringData(6).isAPartOf(wsaaGetcurlevParm, 10);
	private FixedLengthStringData wsaaGetcurlevStatuz = new FixedLengthStringData(4).isAPartOf(wsaaGetcurlevParm, 16);
		/* TABLES */
	private static final String t5688 = "T5688";
		/* FORMATS */
	private static final String bscdrec = "BSCDREC";
	private static final String bsscrec = "BSSCREC";
	private static final String buparec = "BUPAREC";
	private static final String bppdrec = "BPPDREC";
	private static final String bparrec = "BPARREC";
	private static final String descsmtrec = "DESCSMTREC";
	private static final String itemrec = "ITEMREC";
	private static final String sdftrec = "SDFTREC";
	private static final String wrkhmntrec = "WRKHMNTREC";
	private BparTableDAM bparIO = new BparTableDAM();
	private BppdTableDAM bppdIO = new BppdTableDAM();
	private BscdTableDAM bscdIO = new BscdTableDAM();
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private DescsmtTableDAM descsmtIO = new DescsmtTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private SdftTableDAM sdftIO = new SdftTableDAM();
	private WrkelevTableDAM wrkelevIO = new WrkelevTableDAM();
	private WrkhmntTableDAM wrkhmntIO = new WrkhmntTableDAM();
	private Pr53bpar pr53bpar = new Pr53bpar();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr53bScreenVars sv = ScreenProgram.getScreenVars( Sr53bScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkComponents2040, 
		exit2090, 
		exit2390
	}

	public Pr53b() {
		super();
		screenVars = sv;
		new ScreenModel("Sr53b", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Retrieve Schedule.*/
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		/* Retrieve Schedule Definition.*/
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		/* Retrieve Parameter Prompt Definition.*/
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
		sv.extrval.set("Y");
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "M")) {
			return ;
		}
		bparIO.setParams(SPACES);
		bparIO.setScheduleName(bsscIO.getScheduleName());
		bparIO.setCompany(bppdIO.getCompany());
		bparIO.setParmPromptProg(wsaaProg);
		bparIO.setBruntype(subString(wsspcomn.inqkey, 1, 8));
		bparIO.setBrunoccur(subString(wsspcomn.inqkey, 9, 3));
		bparIO.setEffectiveDate(bsscIO.getEffectiveDate());
		bparIO.setAcctmonth(bsscIO.getAcctMonth());
		bparIO.setAcctyear(bsscIO.getAcctYear());
		bparIO.setFunction(varcom.readr);
		bparIO.setFormat(bparrec);
		SmartFileCode.execute(appVars, bparIO);
		if (isNE(bparIO.getStatuz(), varcom.oK)
		&& isNE(bparIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(bparIO.getStatuz());
			syserrrec.params.set(bparIO.getParams());
			fatalError600();
		}
		if (isEQ(bparIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(errorsInner.a123);
			return ;
		}
		pr53bpar.parmRecord.set(bparIO.getParmarea());
		sv.cntdesc.set(pr53bpar.cntdesc);
		sv.cnttyp.set(pr53bpar.cnttyp);
		sv.contractType.set(pr53bpar.contractType);
		sv.crcodes.set(pr53bpar.crcodes);
		sv.crtableds.set(pr53bpar.crtableds);
		sv.crtables.set(pr53bpar.crtables);
		sv.ctypdesc.set(pr53bpar.ctypdesc);
		sv.extrval.set(pr53bpar.extrval);
		sv.worku.set(pr53bpar.worku);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
					checkDuplicate2030();
				case checkComponents2040: 
					checkComponents2040();
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		/*    Validate fields*/
/*		sdftIO.setDataKey(SPACES);
		sdftIO.setDftname("WORKLEV");
		sdftIO.setObjlevl("*ALL");
		sdftIO.setCoy("*");
		sdftIO.setFormat(sdftrec);
		sdftIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, sdftIO);
		if (isNE(sdftIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(sdftIO.getStatuz());
			syserrrec.params.set(sdftIO.getParams());
			fatalError600();
		}
		if (isNE(sdftIO.getDefaultVal(), "0")
		&& isEQ(sv.worku, SPACES)) {
			sv.workuErr.set(errorsInner.e186);
		}
		if (isNE(sv.worku, SPACES)) {
			checkWorkUnit2300();
		}
		*/
		if (isNE(sv.contractType, wsaaContractType)) {
			wsaaContractType.set(sv.contractType);
			wsaaDescDisplayed = "N";
		}
		if (isEQ(sv.contractType, SPACES)) {
			if (isNE(sv.cnttyp, SPACES)) {
				sv.cntypErr.set(errorsInner.e186);
			}
		}
		else {
			wsaaItemitem.set(sv.contractType);
			readT56882100();
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				sv.cntypErr.set(errorsInner.e308);
			}
			else {
				getDescription2200();
			}
		}
		if (isEQ(sv.cnttyp, SPACES)) {
			if (isNE(sv.contractType, SPACES)) {
				sv.cnttypErr.set(errorsInner.e186);
			}
		}
		else {
			wsaaItemitem.set(sv.cnttyp);
			readT56882100();
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				sv.cnttypErr.set(errorsInner.f918);
			}
		}
		if (isEQ(sv.ctypdesc, SPACES)
		&& isNE(sv.cnttyp, SPACES)) {
			sv.ctypdescErr.set(errorsInner.e186);
		}
		if (isNE(sv.contractType, SPACES)
		&& isNE(sv.cnttyp, SPACES)) {
			wsaaOldCtr.set(ZERO);
			wsaaNewCtr.set(ZERO);
			wsaaOldCtr.add(inspectTally(sv.contractType, " ", "CHARACTERS", SPACES.toString(), null));
			wsaaNewCtr.add(inspectTally(sv.cnttyp, " ", "CHARACTERS", SPACES.toString(), null));
			if (isNE(wsaaNewCtr, wsaaOldCtr)) {
				sv.cnttypErr.set(errorsInner.e760);
			}
		}
		if (isEQ(sv.cnttyp, SPACES)
		&& isEQ(sv.contractType, SPACES)
		&& isEQ(sv.crcodes, SPACES)
		&& isEQ(sv.crtables, SPACES)) {
			sv.cnttypErr.set(errorsInner.e207);
			sv.cntypErr.set(errorsInner.e207);
			sv.crcodesErr.set(errorsInner.e207);
			sv.crtablesErr.set(errorsInner.e207);
		}
		if (isNE(sv.extrval, "Y")
		&& isNE(sv.extrval, "N")) {
			sv.extrvalErr.set(errorsInner.f136);
		}
	}

protected void checkDuplicate2030()
	{
		if (isEQ(sv.crcodes, SPACES)) {
			goTo(GotoLabel.checkComponents2040);
		}
		for (ix.set(1); !(isEQ(ix, 10)); ix.add(1)){
			compute(wsaaStart, 0).set(add(ix, 1));
			for (iy.set(wsaaStart); !(isGT(iy, 10)); iy.add(1)){
				if (isEQ(sv.crcode[ix.toInt()], sv.crcode[iy.toInt()])
				&& isNE(sv.crcode[ix.toInt()], SPACES)) {
					sv.crcodeErr[ix.toInt()].set(errorsInner.h357);
					sv.crcodeErr[iy.toInt()].set(errorsInner.h357);
				}
			}
		}
	}

protected void checkComponents2040()
	{
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			if (isEQ(sv.crtable[ix.toInt()], SPACES)
			&& isNE(sv.crcode[ix.toInt()], SPACES)) {
				sv.crtableErr[ix.toInt()].set(errorsInner.e186);
			}
			if (isNE(sv.crtable[ix.toInt()], SPACES)
			&& isEQ(sv.crcode[ix.toInt()], SPACES)) {
				sv.crcodeErr[ix.toInt()].set(errorsInner.e186);
			}
			if (isNE(sv.crcode[ix.toInt()], SPACES)
			&& isEQ(sv.crtabled[ix.toInt()], SPACES)) {
				sv.crtabledErr[ix.toInt()].set(errorsInner.e186);
			}
			if (isEQ(sv.crcode[ix.toInt()], SPACES)
			&& isNE(sv.crtabled[ix.toInt()], SPACES)) {
				sv.crcodeErr[ix.toInt()].set(errorsInner.e186);
			}
			if (isNE(sv.crtable[ix.toInt()], SPACES)
			&& isNE(sv.crcode[ix.toInt()], SPACES)) {
				wsaaOldCtr.set(ZERO);
				wsaaNewCtr.set(ZERO);
				wsaaOldCtr.add(inspectTally(sv.crtable[ix.toInt()], " ", "CHARACTERS", SPACES.toString(), null));
				wsaaNewCtr.add(inspectTally(sv.crcode[ix.toInt()], " ", "CHARACTERS", SPACES.toString(), null));
				if (isNE(wsaaNewCtr, wsaaOldCtr)) {
					sv.crcodeErr[ix.toInt()].set(errorsInner.e760);
				}
			}
		}
		if (isEQ(wsaaDescDisplayed, "N")
		&& isEQ(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set(SPACES);
			wsaaDescDisplayed = "Y";
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readT56882100()
	{
		begin2110();
	}

protected void begin2110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5688);
		itemIO.setItemitem(wsaaItemitem);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void getDescription2200()
	{
		begin2210();
	}

protected void begin2210()
	{
		descsmtIO.setDataKey(SPACES);
		descsmtIO.setDescpfx(itemIO.getItempfx());
		descsmtIO.setDesccoy(itemIO.getItemcoy());
		descsmtIO.setDesctabl(itemIO.getItemtabl());
		descsmtIO.setDescitem(itemIO.getItemitem());
		descsmtIO.setItemseq(itemIO.getItemseq());
		descsmtIO.setLanguage("E");
		descsmtIO.setFormat(descsmtrec);
		descsmtIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descsmtIO);
		if (isNE(descsmtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(descsmtIO.getStatuz());
			syserrrec.params.set(descsmtIO.getParams());
			fatalError600();
		}
		sv.cntdesc.set(descsmtIO.getLongdesc());
	}

protected void checkWorkUnit2300()
	{
		try {
			begin2310();
			updateLevel2320();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begin2310()
	{
		wrkhmntIO.setRecKeyData(SPACES);
		wrkhmntIO.setWorku(sv.worku);
		wrkhmntIO.setFormat(wrkhmntrec);
		wrkhmntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, wrkhmntIO);
		if (isNE(wrkhmntIO.getStatuz(), varcom.oK)
		&& isNE(wrkhmntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(wrkhmntIO.getStatuz());
			syserrrec.params.set(wrkhmntIO.getParams());
			fatalError600();
		}
		if (isEQ(wrkhmntIO.getStatuz(), varcom.mrnf)) {
			sv.workuErr.set(errorsInner.h244);
			goTo(GotoLabel.exit2390);
		}
		if (isNE(wrkhmntIO.getStatz(), "10")
		&& isNE(wrkhmntIO.getStatz(), "50")) {
			sv.workuErr.set(errorsInner.h245);
			goTo(GotoLabel.exit2390);
		}
		/* GET THE CURRENT LEVEL*/
		wsaaGetcurlevParm.set(SPACES);
		callProgram(Getcurlev.class, wsaaGetcurlevLib, wsaaGetcurlevCurrent, wsaaGetcurlevStatuz);
		if (isNE(wsaaGetcurlevStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaGetcurlevStatuz);
			syserrrec.params.set(wsaaGetcurlevParm);
			fatalError600();
		}
		if (isEQ(wrkhmntIO.getObjlevl(), wsaaGetcurlevCurrent)) {
			goTo(GotoLabel.exit2390);
		}
	}

protected void updateLevel2320()
	{
		wrkhmntIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, wrkhmntIO);
		if (isNE(wrkhmntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(wrkhmntIO.getStatuz());
			syserrrec.params.set(wrkhmntIO.getParams());
			fatalError600();
		}
		wrkhmntIO.setObjlevl(wsaaGetcurlevCurrent);
		wrkhmntIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, wrkhmntIO);
		if (isNE(wrkhmntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(wrkhmntIO.getStatuz());
			syserrrec.params.set(wrkhmntIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATING SECTIONS
	* </pre>
	*/
protected void moveParameters3100()
	{
		/*MOVE-TO-PARM-RECORD*/
		pr53bpar.cntdesc.set(sv.cntdesc);
		pr53bpar.cnttyp.set(sv.cnttyp);
		pr53bpar.contractType.set(sv.contractType);
		pr53bpar.crcodes.set(sv.crcodes);
		pr53bpar.crtableds.set(sv.crtableds);
		pr53bpar.crtables.set(sv.crtables);
		pr53bpar.ctypdesc.set(sv.ctypdesc);
		pr53bpar.extrval.set(sv.extrval);
		pr53bpar.worku.set(sv.worku);
		/*EXIT*/
	}

protected void writeParamRecord3200()
	{
		writBupa3210();
	}

protected void writBupa3210()
	{
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(pr53bpar.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData a123 = new FixedLengthStringData(4).init("A123");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e207 = new FixedLengthStringData(4).init("E207");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e760 = new FixedLengthStringData(4).init("E760");
	private FixedLengthStringData f136 = new FixedLengthStringData(4).init("F136");
	private FixedLengthStringData f918 = new FixedLengthStringData(4).init("F918");
	private FixedLengthStringData h244 = new FixedLengthStringData(4).init("H244");
	private FixedLengthStringData h245 = new FixedLengthStringData(4).init("H245");
	private FixedLengthStringData h357 = new FixedLengthStringData(4).init("H357");
}
}
