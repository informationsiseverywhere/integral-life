package com.csc.life.productdefinition.dataaccess.dao.impl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RcvdpfDAOImpl extends BaseDAOImpl<Rcvdpf> implements RcvdpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RcvdpfDAOImpl.class);

	@Override
	public void insertRcvdpfRecord(Rcvdpf rcvdpf) {
		if (rcvdpf != null) {
		      
			StringBuilder sql_rcvdpf_insert = new StringBuilder();
			sql_rcvdpf_insert.append("INSERT INTO RCVDPF");
			sql_rcvdpf_insert.append("(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,CRTABLE,WAITPERIOD,BENTRM,POLTYP,PRMBASIS,STATCODE,USRPRF,JOBNM,DATIME,DIALDOWNOPTION)");
			sql_rcvdpf_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement psInsert = getPrepareStatement(sql_rcvdpf_insert.toString());
			try
			{
					psInsert.setString(1, rcvdpf.getChdrcoy());
					psInsert.setString(2, rcvdpf.getChdrnum());
					psInsert.setString(3, rcvdpf.getLife());
					psInsert.setString(4, rcvdpf.getCoverage());
					psInsert.setString(5, rcvdpf.getRider());
					psInsert.setString(6, rcvdpf.getCrtable());
					psInsert.setString(7, rcvdpf.getWaitperiod());
					psInsert.setString(8, rcvdpf.getBentrm());
					psInsert.setString(9, rcvdpf.getPoltyp());
					psInsert.setString(10, rcvdpf.getPrmbasis());
					psInsert.setString(11, rcvdpf.getStatcode());//BRD-009
					psInsert.setString(12, this.getUsrprf());
					psInsert.setString(13, this.getJobnm());
					psInsert.setTimestamp(14, new Timestamp(System.currentTimeMillis()));
					psInsert.setString(15, rcvdpf.getDialdownoption());
					//ILIFE-7061 start
					//psInsert.addBatch();
					/*int[] rowCount=*/
					psInsert.execute();	
					//ILIFE-7061 end
			}
			catch (SQLException e) {
                LOGGER.error("insertRcvdpfRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, null);
            }
		}
		
	}

	@Override
	public Rcvdpf readRcvdpf(Rcvdpf rcvdpf) {
		 Rcvdpf rcvdpfData = null;
		 StringBuilder sql_rcvdpf_select  = new StringBuilder("SELECT WAITPERIOD,BENTRM,POLTYP,PRMBASIS,STATCODE,DIALDOWNOPTION");
		    sql_rcvdpf_select.append(" FROM RCVDPF WHERE(CHDRCOY = '"+rcvdpf.getChdrcoy()+"'");
		    sql_rcvdpf_select.append("AND CHDRNUM = '"+rcvdpf.getChdrnum()+"'");
		    sql_rcvdpf_select.append("AND LIFE = '"+rcvdpf.getLife()+"'");
		    sql_rcvdpf_select.append("AND COVERAGE= '"+rcvdpf.getCoverage()+"'");
		    sql_rcvdpf_select.append("AND RIDER= '"+rcvdpf.getRider()+"'");
		    sql_rcvdpf_select.append("AND CRTABLE= '"+rcvdpf.getCrtable()+"')");
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {

		    	psSelect = getPrepareStatement(sql_rcvdpf_select.toString());
		    	/*psSelect.setString(1, rcvdpf.getChdrcoy());
		    	psSelect.setString(2, rcvdpf.getChdrnum());
		    	psSelect.setString(3, rcvdpf.getLife());
		    	psSelect.setString(4, rcvdpf.getCoverage());
		    	psSelect.setString(5, rcvdpf.getRider());
		    	psSelect.setString(6, rcvdpf.getCrtable());*/
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		                 if ((rs.getString(1) != null)||(rs.getString(4) != null) || rs.getString(6) != null) {
		                	 rcvdpfData= new Rcvdpf();
		                	 rcvdpfData.setWaitperiod(rs.getString(1));
		                	 rcvdpfData.setBentrm(rs.getString(2));
		                	 rcvdpfData.setPoltyp(rs.getString(3));
		                	 rcvdpfData.setPrmbasis(rs.getString(4));
		                	 rcvdpfData.setStatcode(rs.getString(5));//BRD-009
		                	 rcvdpfData.setDialdownoption(rs.getString(6));//BRD-NBP-011
		                }
		           }
		    } catch (SQLException e) {
		           LOGGER.error("readRcvdpf()", e);//IJTI-1561
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }

		    return rcvdpfData;
	
	}

	@Override
	public boolean updateIntoRcvdpf(Rcvdpf rcvdpf) {
		StringBuilder sql_rcvdpf_update = new StringBuilder("");
		sql_rcvdpf_update.append("UPDATE RCVDPF  ");
		sql_rcvdpf_update.append("SET ");		 
		sql_rcvdpf_update.append("WAITPERIOD=?,BENTRM=?,POLTYP=?,PRMBASIS=?,STATCODE=?,DIALDOWNOPTION=?,USRPRF=?,JOBNM=?,DATIME=? ");
		sql_rcvdpf_update.append("WHERE CHDRNUM='"+rcvdpf.getChdrnum()+"'");
		sql_rcvdpf_update.append("AND CHDRCOY='"+rcvdpf.getChdrcoy()+"'");
		sql_rcvdpf_update.append("AND LIFE='"+rcvdpf.getLife()+"'");
		sql_rcvdpf_update.append("AND COVERAGE='"+rcvdpf.getCoverage()+"'");
		sql_rcvdpf_update.append("AND RIDER='"+rcvdpf.getRider()+"'");
		sql_rcvdpf_update.append("AND CRTABLE='"+rcvdpf.getCrtable()+"'");
		
		
		PreparedStatement psUpdate = null;
		boolean isUpdated = false;
		try{
			psUpdate = getPrepareStatement(sql_rcvdpf_update.toString());
			
			psUpdate.setString(1, rcvdpf.getWaitperiod());
			psUpdate.setString(2, rcvdpf.getBentrm());
			psUpdate.setString(3, rcvdpf.getPoltyp());
			psUpdate.setString(4, rcvdpf.getPrmbasis());
			psUpdate.setString(5, rcvdpf.getStatcode());//BRD-009
			psUpdate.setString(6, rcvdpf.getDialdownoption());//BRD-NBP-011
			psUpdate.setString(7, this.getUsrprf());
			psUpdate.setString(8, this.getJobnm());
			psUpdate.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
			//ILIFE-7061 start
					//psInsert.addBatch();
					/*int[] rowCount=*/
			psUpdate.executeUpdate();
			//ILIFE-7061 end
			isUpdated = true; 
		}
		//IJTI-851-Overly Broad Catch
		catch ( SQLException e) {
			LOGGER.error("updateRcvdpf()", e);//IJTI-1561
			
		} finally {
			close(psUpdate, null);			
		}		
		return isUpdated;
	
	}
	
	public boolean updateOccpclass(Map<String, Rcvdpf> rcvdpf){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE RCVDPF SET STATCODE = ? WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ?");
		PreparedStatement ps = null;
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());
			for(Map.Entry<String, Rcvdpf> rcvdData : rcvdpf.entrySet()){
				ps.setString(1, rcvdData.getValue().getStatcode());
				ps.setString(2, rcvdData.getValue().getChdrcoy());
				ps.setString(3, rcvdData.getKey().substring(0, 8));
				ps.setString(4, rcvdData.getKey().substring(8, 10));
				ps.setString(5, rcvdData.getKey().substring(10, 12));
				ps.setString(6, rcvdData.getKey().substring(12, 14));
				ps.addBatch();
			}
			if(ps.executeBatch().length > 0)
				isUpdated = true;
		}
		catch(SQLException e){
			LOGGER.error("updateOccpclass()", e);//IJTI-1561
		}
		finally{
			close(ps, null);
		}
		return isUpdated;
	}
}
