package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:26
 * Description:
 * Copybook name: COVRSDCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrsdckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrsdcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrsdcKey = new FixedLengthStringData(64).isAPartOf(covrsdcFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrsdcChdrcoy = new FixedLengthStringData(1).isAPartOf(covrsdcKey, 0);
  	public FixedLengthStringData covrsdcChdrnum = new FixedLengthStringData(8).isAPartOf(covrsdcKey, 1);
  	public FixedLengthStringData covrsdcLife = new FixedLengthStringData(2).isAPartOf(covrsdcKey, 9);
  	public FixedLengthStringData covrsdcCoverage = new FixedLengthStringData(2).isAPartOf(covrsdcKey, 11);
  	public FixedLengthStringData covrsdcRider = new FixedLengthStringData(2).isAPartOf(covrsdcKey, 13);
  	public PackedDecimalData covrsdcPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrsdcKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrsdcKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrsdcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrsdcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}