/*
 * File: Calccsur
 * Date: 5 Oct 2012 5:48:03 PM
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import java.util.HashMap;

import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.util.QPUtilities;

public class Calccsur extends Vpmcalc {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
	public Calccsur() {
		super();
	}

public void mainline(Object... parmArray)
{
	recordMap = new HashMap<String, ExternalData>();
	recordMap.put("VPMFMTREC", vpmfmtrec);
	
	vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
	
	recordMap.put("VPXSURCREC", vpxsurcrec);
	
	try {
		startSubr010();
		
	}
	catch (COBOLExitProgramException e) {
	}
	
}
}
