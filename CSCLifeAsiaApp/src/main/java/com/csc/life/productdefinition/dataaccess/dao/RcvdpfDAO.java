package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RcvdpfDAO  extends BaseDAO<Rcvdpf> 
{
	public void insertRcvdpfRecord(Rcvdpf rcvdpf);
	public Rcvdpf readRcvdpf(Rcvdpf rcvdpf);
	public boolean updateIntoRcvdpf(Rcvdpf rcvdpf);
	public boolean updateOccpclass(Map<String, Rcvdpf> rcvdpf);	//ALS-4251
}
