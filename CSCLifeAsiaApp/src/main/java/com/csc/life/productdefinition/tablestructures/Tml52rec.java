package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:56
 * Description:
 * Copybook name: TML52REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tml52rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tml52Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData rskflg = new FixedLengthStringData(1).isAPartOf(tml52Rec, 0);
  	public FixedLengthStringData mind = new FixedLengthStringData(1).isAPartOf(tml52Rec, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(tml52Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tml52Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tml52Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}