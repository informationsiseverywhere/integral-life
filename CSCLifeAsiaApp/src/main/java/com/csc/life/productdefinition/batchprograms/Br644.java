/*
 * File: Br644.java
 * Date: 29 August 2009 22:30:47
 * Author: Quipoz Limited
 * 
 * Class transformed from BR644.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.productdefinition.dataaccess.dao.Br644DAO;
import com.csc.life.productdefinition.dataaccess.dao.MedipfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MedxTemppfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MerxTemppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Br644DTO;
import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.life.productdefinition.dataaccess.model.MerxTemppf;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.procedures.Contot;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  This is extraction program:
*  - Medical Bill Payment extraction and update into MEDXPF
*  - Medical Bill Payment Exception Extraction and update into
*    MEDRPF
*
*  CONTROL TOTALS:
*    01  -  Client not active
*    02  -  Medical Provider Terminated
*    03  -  Client Bankruptcy
*
***********************************************************************
* </pre>
*/
public class Br644 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR644");
	private final String wsaaLargeName = "LGNMS";
	private ZonedDecimalData wsaaDtetrm = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(50);
	private FixedLengthStringData wsaaExceptionCase = new FixedLengthStringData(1).init("N");
	private Validator exceptionCase = new Validator(wsaaExceptionCase, "Y");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private Clntkey wsaaClntkey = new Clntkey();
	private P6671par p6671par = new P6671par();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Tr386rec tr386rec = new Tr386rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	
	private List<Medipf> mediList;
	private Medipf medipf = new Medipf();
	private Br644DTO br644DTO = new Br644DTO();
	private MedipfDAO medipfDAO = getApplicationContext().getBean("medipfDAO", MedipfDAO.class);
	private int intBatchID;
	private int intBatchExtractSize;	
	private int intBatchStep;
	private String strCompany;
	private String strEffDate;
	private static final Logger LOGGER = LoggerFactory.getLogger(Br644.class);
	private Iterator<Medipf> iteratorList;
	private int ctrCT02; 
	private int ctrCT03;
	private int ctrCT04;
	private boolean noRecordFound;
	
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private MedxTemppfDAO medxpfTempDAO =  getApplicationContext().getBean("medxTemppfDAO", MedxTemppfDAO.class);
	private MerxTemppfDAO merxpfTempDAO =  getApplicationContext().getBean("merxTemppfDAO", MerxTemppfDAO.class);
	private Br644DAO br644DAO =  getApplicationContext().getBean("br644DAO", Br644DAO.class);
	private List<MedxTemppf> medxBulkInsList;
	private List<MerxTemppf> merxBulkInsList;
	private List<Medipf> mediBulkUpdList;
	private MedxTemppf medxTemppf;
	private MerxTemppf merxTemppf;
	private Map<String, List<Itempf>> tr386ListMap;
	private static final String rp50 = "RP50";
	private String threadNumber;


	public Br644() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900(){
	/*RESTART*/
	/*EXIT*/
}

protected void initialise1000(){
	p6671par.parmRecord.set(bupaIO.getParmarea());
	strCompany = bsprIO.getCompany().toString();
	strEffDate = bsscIO.getEffectiveDate().toString();	
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound = false;
	mediList = new ArrayList<Medipf>();

	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
	
	int extractRows = br644DAO.populateBr644Temp(strCompany, intBatchExtractSize, p6671par.chdrnum.toString(), p6671par.chdrnum1.toString());
	if (extractRows > 0) {
		performDataChunk();	
		
		if (!mediList.isEmpty()){
			initialise1010();
			tr386ListMap = itemDAO.loadSmartTable("IT", strCompany, "TR386");
			readTr386();
		}			
	}
	else{
		LOGGER.info("No Medipay records retrieved for processing.");	
		noRecordFound = true;
		return;
	}	
	
}

protected void performDataChunk(){
	intBatchID = intBatchStep;		
	if (mediList.size() > 0) mediList.clear();
	mediList = br644DAO.loadDataByBatch(intBatchID);
	if (mediList.isEmpty())	wsspEdterror.set(varcom.endp);
	else iteratorList = mediList.iterator();		
}

protected void initialise1010(){	
	wsspEdterror.set(varcom.oK);
	StringBuilder sb = new StringBuilder("");
	sb.append("THREAD");
	sb.append(String.format("%0"+ (3 - bsprIO.getProcessOccNum().toString().trim().length())+"d%s",0 , bsprIO.getProcessOccNum().toString().trim()));
	threadNumber = sb.toString();
	
	medxBulkInsList = new ArrayList<MedxTemppf>();
	merxBulkInsList = new ArrayList<MerxTemppf>();		
	mediBulkUpdList = new ArrayList<Medipf>();
	tr386ListMap =  new HashMap<String, List<Itempf>>();
	ctrCT02 = 0;
	ctrCT03 = 0;
	ctrCT04 = 0;
}

protected void readTr386(){
	String keyItemitem = bsscIO.getLanguage().toString().trim().concat(bprdIO.getBatchProgram().toString().trim());
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr386ListMap.containsKey(keyItemitem)){	
		itempfList = tr386ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(tr386rec.tr386Rec);
		syserrrec.statuz.set(rp50);
		fatalError600();		
	}	
}

protected void readFile2000()
{
	if (!mediList.isEmpty()){	
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!mediList.isEmpty()){
				medipf = new Medipf();
				medipf = iteratorList.next();
				br644DTO = medipf.getBr644DTO();			
			}
		}else {		
			medipf = new Medipf();
			medipf = iteratorList.next();
			br644DTO = medipf.getBr644DTO();
		}	
	}else wsspEdterror.set(varcom.endp);
}

protected void edit2500()
{
	/*EDIT*/
	wsspEdterror.set(varcom.oK);
	/*EXIT*/
	
	wsaaExceptionCase.set("N");
	if (isEQ(medipf.getPaidby(),"A")){
		wsaaDtetrm.set(varcom.vrcmMaxDate);
		wsaaDesc.set(SPACES);
		ctrCT02++;
	}
	else if (isEQ(medipf.getPaidby(),"D")){
		wsaaClntkey.clntClntnum.set(br644DTO.getClntnum()); //ILIFE-8032
		wsaaDtetrm.set(br644DTO.getDate1());
		if (isNE(wsaaDtetrm,varcom.vrcmMaxDate)) {
			wsaaDesc.set(tr386rec.progdesc01);
			wsaaExceptionCase.set("Y");
		}
		ctrCT04++;
	}
	else if (isEQ(medipf.getPaidby(),"C")){
		wsaaClntkey.clntClntnum.set(medipf.getExmcode());  /* (As before code enhancement, found from logs that wsaaClntkey.clntClntnum was set but later was not) ILIFE-6447 */
		wsaaDtetrm.set(br644DTO.getDate1());
		if (isNE(wsaaDtetrm,varcom.vrcmMaxDate)
		|| isNE(br644DTO.getCltstat(),"AC")) {
			wsaaDesc.set(tr386rec.progdesc02);
			wsaaExceptionCase.set("Y");
		}
		if (isEQ(br644DTO.getDate2(),varcom.vrcmMaxDate)) {
			wsaaDesc.set(tr386rec.progdesc03);
			wsaaExceptionCase.set("Y");
		}
		ctrCT03++;
	}		
}

protected void commitControlTotals(){
	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT02 = 0;
	
	//ct03
	contotrec.totno.set(ct03);
	contotrec.totval.set(ctrCT03);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT03 = 0;
	
	//ct04
	contotrec.totno.set(ct04);
	contotrec.totval.set(ctrCT04);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT04 = 0;	
}

protected void update3000(){
	if (exceptionCase.isTrue()) {
		writeMerxRecord();
		updateMediRecord();
	}
	else {
		writeMedxRecord();
	}	
}

protected void writeMerxRecord(){
	merxTemppf = new MerxTemppf();
	merxTemppf.setChdrcoy(medipf.getChdrcoy());
	merxTemppf.setChdrnum(medipf.getChdrnum());
	merxTemppf.setSeqno(medipf.getSeqno());
	merxTemppf.setPaidby(medipf.getPaidby());
	merxTemppf.setExmcode(medipf.getExmcode());
	merxTemppf.setClntnum(br644DTO.getClntnum());
	a1000CallNamadrs();
	merxTemppf.setName(namadrsrec.name.toString().trim());	
	merxTemppf.setZmedtyp(medipf.getZmedtyp());
	merxTemppf.setEffdate(medipf.getEffdate());
	merxTemppf.setInvref(medipf.getInvref());
	merxTemppf.setZmedfee(medipf.getZmedfee());
	merxTemppf.setDescT(wsaaDesc.toString().trim());
	merxTemppf.setMemberName(threadNumber);
	merxBulkInsList.add(merxTemppf);
}

protected void commitMerxBulkInsert(){	
	StringBuilder merxTempTableName = new StringBuilder("");
	merxTempTableName.append("MERX");
	merxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
	merxTempTableName.append(String.format("%0"+ (4 - bsscIO.getScheduleNumber().toString().trim().length())+"d%s",0 , bsscIO.getScheduleNumber().toString().trim()));
	
	boolean isInsertMerxpfTemp = merxpfTempDAO.insertMerxTempRecords(merxBulkInsList, merxTempTableName.toString());
	if (!isInsertMerxpfTemp) {
		LOGGER.error("Insert Merxmx record failed.");
		fatalError600();
	}else merxBulkInsList.clear();
}

protected void writeMedxRecord(){
	medxTemppf = new MedxTemppf();
	medxTemppf.setChdrcoy(medipf.getChdrcoy());
	medxTemppf.setChdrnum(medipf.getChdrnum());
	medxTemppf.setSeqno(medipf.getSeqno());
	medxTemppf.setExmcode(medipf.getExmcode());
	medxTemppf.setZmedtyp(medipf.getZmedtyp());
	medxTemppf.setPaidby(medipf.getPaidby());
	medxTemppf.setLife(medipf.getLife());
	medxTemppf.setJlife(medipf.getJlife());
	medxTemppf.setEffdate(medipf.getEffdate());
	medxTemppf.setInvref(medipf.getInvref());
	medxTemppf.setZmedfee(medipf.getZmedfee());
	medxTemppf.setClntnum(br644DTO.getClntnum());
	medxTemppf.setDtetrm(Integer.parseInt(wsaaDtetrm.toString()));
	medxTemppf.setMemberName(threadNumber);
	medxBulkInsList.add(medxTemppf);
}

protected void commitMedxBulkInsert(){	
	StringBuilder medxTempTableName = new StringBuilder("");
	medxTempTableName.append("MEDX");
	medxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
	medxTempTableName.append(String.format("%0"+ (4 - bsscIO.getScheduleNumber().toString().trim().length())+"d%s",0 , bsscIO.getScheduleNumber().toString().trim()));
	
	boolean isInsertMedxpfTemp = medxpfTempDAO.insertMedxTempRecords(medxBulkInsList, medxTempTableName.toString());
	if (!isInsertMedxpfTemp) {
		LOGGER.error("Insert Medxmx record failed.");
		fatalError600();
	}else medxBulkInsList.clear();
}

protected void updateMediRecord(){	
	medipf.setActind("3");
	mediBulkUpdList.add(medipf);
}

protected void commitMediUpdate(){
	boolean isUpdateMedipf;
	isUpdateMedipf = medipfDAO.updateMedipf(mediBulkUpdList);
	if (!isUpdateMedipf) {
		LOGGER.error("Update Medipf record failed.");
		fatalError600();
	}else mediBulkUpdList.clear();	
}


protected void commit3500()
{
	if (!noRecordFound){
		commitControlTotals();
		commitMerxBulkInsert();
		commitMedxBulkInsert();
		commitMediUpdate();
	}
}

protected void rollback3600(){
	/*ROLLBACK*/
	/*EXIT*/
}

protected void close4000(){
	if (!mediList.isEmpty()){
		tr386ListMap.clear();
		tr386ListMap = null;
		
		medxBulkInsList.clear();
		medxBulkInsList = null;
		
		merxBulkInsList.clear();	
		merxBulkInsList = null;
		
		mediBulkUpdList.clear();
		mediBulkUpdList = null;
		
		lsaaStatuz.set(varcom.oK);
	}
}

protected void a1000CallNamadrs(){
	a1000Start();
}

protected void a1000Start(){
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntNumber.set(wsaaClntkey.clntClntnum);
	namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
	namadrsrec.clntCompany.set(bsprIO.getFsuco());
	namadrsrec.language.set(bsscIO.getLanguage());
	namadrsrec.function.set(wsaaLargeName);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.params.set(namadrsrec.namadrsRec);
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError600();
	}
}

}