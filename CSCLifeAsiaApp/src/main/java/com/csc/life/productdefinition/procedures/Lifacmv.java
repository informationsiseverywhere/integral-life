/*
 * File: Lifacmv.java
 * Date: 29 August 2009 22:58:04
 * Author: Quipoz Limited
 * 
 * Class transformed from LIFACMV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.fsu.accounting.dataaccess.DglxTableDAM;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.accounting.tablestructures.Tr362rec;
import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.AcmvTableDAM;
import com.csc.fsu.general.procedures.Postyymm;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Postyymmr;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Trcdecpy;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.recordstructures.Rlagprdrec;
import com.csc.life.diary.procedures.Dryrlagprd;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.statistics.procedures.Rlagprd;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Getstamp;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Dryprcrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*         Life system version of Subsidary Ledger Posting
*
* WARNING !!!!!
* ¯¯¯¯¯¯¯¯¯¯¯¯¯
*  THE SUBROUTINE 'FASACMV' HAS BEEN CLONED FROM THIS
*  AND SO IF ANY CHANGES ARE MADE HERE, THEN MAYBE 'FASACMV'
*  WILL NEED TO CHANGE AS WELL......SIMILARLY, IF 'FASACMV'
*  IS CHANGED, THEN MAYBE THIS SUBROUTINE WILL NEED AMENDING.
*
*
* This  subroutine  updates  the  batch totals then writes an
* ACMV record. Depending on the calling function sub-accounts
* balance can also be posted to.
*
*  Functions:
*
*     PSTW  - Write the ACMV and update the ACBL balance.
*     NPSTW - Write the ACMV record only.
*     PSTD  - Calling from Diary subroutine to Write the ACMV
*             and update the ACBL balance.
*     NPSTD - Calling from Diary subroutine to Write the ACMV
*             record only.
*
*  Both functions update the running batch header totals kept
*  by calling BATCUP with a KEEPS function.
*
*  On completion of the transaction, BATCUP must be called with
*  a WRITS function to apply the accumulated totals to the
*  database.
*
*  Statuses:
*
*     **** - successful completion of routine
*     BOMB - database error
*     E049 - Invalid linkage into subroutine
*
*  Linkage Area:
*
*  LIFA-BATCKEY   The key of the opened batch.
*  LIFA-RDOCNUM   Document number for this record. Varies
*                 with the calling subsystem.
*  LIFA-TRANNO    This is the sequence number of the transaction
*                 being done to the document
*  LIFA-JRNSEQ    This is the sequence number user to uniquely
*                 identify a specific manual journal posting.
*  LIFA-RLDGCOY   Ledger kay for this record. Varies with
*  LIFA_RLDGACCT  the calling subsystem.
*  LIFA-GENLCOY   General ledger key. Set up from T5645 prior
*  LIFA_GLCODE    to calling this routine.
*  LIFA-GLSIGN
*  LIFA-CONTOT
*  LIFA-SACSCODE
*  LIFA-SACSTYP
*  LIFA-TRANREF   Internal reference number (e.g. secondary
*                 key). Varies with the calling subsystem.
*  LIFA-TRANDESC  Meaningful description of tranaction.
*  LIFA-ORIGCURR   Original (foreign) currency.
*  LIFA-ACCTCCY   Accounting (local) currency.
*  LIFA-CRATE     Exchange rate at time of transaction between
*                 original and accounting currency.
*  LIFA-EFFDATE   The effective date of the transaction.
*  LIFA-ORIGAMT   The transaction amount in original currency.
*  LIFA-ACCTAMT   The transaction amount in accounting currency.
*  LIFA-SUBSTITUTE-CODES
*                 Used to substitute in GL code if passed
*                     1 - @@@@ - Contract type
*                     2 - ****
*                     3 - %%%%
*                     4 - !!!!
*                     5 - ++++
*                     6 - ==== - Coverage/Rider table
*  "LIFA-TRANID"  Date/time stamp
*
/
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Lifacmv extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("LIFACMV");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* ERRORS */
	private static final String e049 = "E049";
	private static final String g418 = "G418";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t3695 = "T3695";
	private static final String tr362 = "TR362";
		/* FORMATS */
	private static final String acmvrec = "ACMVREC";
	private static final String itemrec = "ITEMREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String agpyagtrec = "AGPYAGTREC";
	private static final String dglxrec = "DGLXREC";
	private static final String dacmrec = "DACMREC";
	private static final String acagrnlrec = "ACAGRNLREC";
	private ZonedDecimalData wsaaPost = new ZonedDecimalData(4, 0).setUnsigned();

		/* WSAA-TABLE */
	private FixedLengthStringData[] wsaaT3695Entries = FLSInittedArray (50, 3);
	private FixedLengthStringData[] wsaaT3695Sacstyp = FLSDArrayPartOfArrayStructure(2, wsaaT3695Entries, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3695Intextind = FLSDArrayPartOfArrayStructure(1, wsaaT3695Entries, 2);
	private PackedDecimalData iz = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaUsrprf = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaJobnm = new FixedLengthStringData(10);
	private PackedDecimalData wsaaX = new PackedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaLastOrigCcy = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();
	private PackedDecimalData wsaaOrigamt = new PackedDecimalData(17, 2);

//	private FixedLengthStringData wsaaSubstituteCode = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaSubstituteCode = new FixedLengthStringData(4);   //COA
//	private FixedLengthStringData wsaaSubscode01 = new FixedLengthStringData(1).isAPartOf(wsaaSubstituteCode, 0);
	protected FixedLengthStringData wsaaSubscode01 = new FixedLengthStringData(1).isAPartOf(wsaaSubstituteCode, 0); //COA
	private FixedLengthStringData wsaaSubscode02 = new FixedLengthStringData(1).isAPartOf(wsaaSubstituteCode, 1);
	private FixedLengthStringData wsaaSubscode03 = new FixedLengthStringData(1).isAPartOf(wsaaSubstituteCode, 2);
	private FixedLengthStringData wsaaSubscode04 = new FixedLengthStringData(1).isAPartOf(wsaaSubstituteCode, 3);
	private ZonedDecimalData wsaaMachineDate = new ZonedDecimalData(6, 0).setUnsigned();

	private FixedLengthStringData wsaaMachineTimex = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaMachineTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaMachineTimex, 0).setUnsigned();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private AcmvTableDAM acmvIO = new AcmvTableDAM();
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private DglxTableDAM dglxIO = new DglxTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Postyymmr postyymmr = new Postyymmr();
	private Batcuprec batcuprec = new Batcuprec();
	private T3629rec t3629rec = new T3629rec();
	private T3695rec t3695rec = new T3695rec();
	private Tr362rec tr362rec = new Tr362rec();
	private Varcom varcom = new Varcom();
	private Rlagprdrec rlagprdrec1 = new Rlagprdrec();
	protected Syserrrec syserrrec = new Syserrrec();       /* MLIL-COA, change access modifier from private to protected */
	private Trcdecpy trcdecpy = new Trcdecpy();
	private Dryprcrec dryprcrec = new Dryprcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();   /* MLIL-COA, change access modifier from private to protected */
	protected AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); 
	protected Acblpf acblpf = null;
	private List<Acblpf> acblpfListparam = new ArrayList<Acblpf>();
	private List<Acblpf> acblpfUpdtListparam = new ArrayList<Acblpf>();
	private List<Acblpf> acblpfListAcbl = new ArrayList<Acblpf>();

/**
 * Contains all possible labels used by goTo action.
 */
	
	public Lifacmv() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		setParam(parmArray);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
		if ((isEQ(lifacmvrec.agnt, "Y"))){
	        if (parmArray != null && parmArray.length > 1) {
	            ((List<Acblpf>) parmArray[1]).addAll(acblpfListparam);
	        }       
	        if (parmArray != null && parmArray.length > 2) {
	            ((List<Acblpf>) parmArray[2]).addAll(acblpfUpdtListparam);
	        }	        
	        return;
		}
		if ((isEQ(lifacmvrec.agnt, "N"))){
	        if (parmArray != null && parmArray.length > 1) {
	            ((List<Acblpf>) parmArray[1]).addAll(acblpfListAcbl);
	        }               
	        return;
		}		
	}

protected void setParam(Object[] parmArray){
	lifacmvrec.lifacmvRec = convertAndSetParam(lifacmvrec.lifacmvRec, parmArray, 0);
	if ((isEQ(lifacmvrec.agnt, "Y"))){
		if(parmArray[1] instanceof List){
			acblpfListparam = (List) convertAndSetParam(acblpfListparam, parmArray, 1);		
		}				
		if(parmArray[2] instanceof List){
			acblpfUpdtListparam = (List) convertAndSetParam(acblpfUpdtListparam, parmArray, 2);				
		}						
	}
	if ((isEQ(lifacmvrec.agnt, "N"))){
		if(parmArray[1] instanceof List){
			acblpfListAcbl = (List) convertAndSetParam(acblpfListAcbl, parmArray, 1);		
		}										
	}
}
protected void main010()
	{
		para011();
		exit090();
	}

protected void para011()
	{
		lifacmvrec.statuz.set("****");
		syserrrec.subrname.set(wsaaProg);
		if (isNE(lifacmvrec.function, "PSTW ")
		&& isNE(lifacmvrec.function, "NPSTW")
		&& isNE(lifacmvrec.function, "PSTD ")
		&& isNE(lifacmvrec.function, "NPSTD")) {
			lifacmvrec.statuz.set(e049);
			return ;
		}
		/* PERFORM 800-READ-CONTRACT-DETAILS.                           */
		checkSchedule24x7Setup4000();
		writeAcmv1000();
		/* Write Agent Commission Payment record when T3695-COMIND = 'Y'   */
		if (isEQ(t3695rec.comind, "Y")
		&& isEQ(lifacmvrec.frcdate, 99999999)
		&& isEQ(t3695rec.intextind, "E")
		&& isEQ(lifacmvrec.sacscode, "LA")) {
			writeAgpyagt1300();
		}
		if (isEQ(t3695rec.tlprmflg, "Y")
		|| isEQ(t3695rec.tlcomflg, "Y")) {
			readContractDetails800();
			rlagprdrec1.agntcoy.set(lifacmvrec.batccoy);
			rlagprdrec1.agntnum.set(chdrlifIO.getAgntnum());
			rlagprdrec1.actyear.set(lifacmvrec.batcactyr);
			rlagprdrec1.actmnth.set(lifacmvrec.batcactmn);
			rlagprdrec1.agtype.set(SPACES);
			/* To use issue date to check against MACFPRD*/
			rlagprdrec1.chdrnum.set(chdrlifIO.getChdrnum());
			rlagprdrec1.occdate.set(chdrlifIO.getOccdate());
			rlagprdrec1.cnttype.set(chdrlifIO.getCnttype());
			rlagprdrec1.chdrcoy.set(chdrlifIO.getChdrcoy());
			rlagprdrec1.batctrcde.set(lifacmvrec.batctrcde);
			rlagprdrec1.effdate.set(lifacmvrec.effdate);
			/* Check the LIFA-GLSIGN.*/
			wsaaOrigamt.set(lifacmvrec.origamt);
			rlagprdrec1.statuz.set(varcom.oK);
			if (isEQ(t3695rec.tlprmflg, "Y")) {
				rlagprdrec1.prdflg.set("P");
				if (isEQ(lifacmvrec.glsign, "-")) {
					compute(wsaaOrigamt, 2).set(mult(wsaaOrigamt, -1));
				}
				compute(wsaaOrigamt, 2).set(mult(wsaaOrigamt, -1));
			}
			else {
				rlagprdrec1.prdflg.set("C");
			}
			rlagprdrec1.origamt.set(wsaaOrigamt);
			/*    To perform DRYRLAGPRD if LIFA-FUNCTION = 'PSTD'              */
			if (isEQ(lifacmvrec.function, "PSTD")) {
				dryprcrec.batchKey.set(lifacmvrec.batckey);
				dryprcrec.user.set(lifacmvrec.user);
				dryprcrec.company.set(chdrlifIO.getChdrcoy());
				dryprcrec.entity.set(chdrlifIO.getChdrnum());
				dryprcrec.branch.set(lifacmvrec.batcbrn);
				dryprcrec.entityType.set("CH");
				dryprcrec.effectiveDate.set(wsaaMachineDate);
				dryprcrec.runDate.set(wsaaMachineDate);
				dryprcrec.effectiveTime.set(ZERO);
				dryprcrec.threadNumber.set(lifacmvrec.threadNumber);
				callProgram(Dryrlagprd.class, rlagprdrec1.rlagprdRec, dryprcrec.dryprcRec);
				if (isNE(rlagprdrec1.statuz, varcom.oK)) {
					syserrrec.statuz.set(rlagprdrec1.statuz);
					syserr570();
				}
			}
			else {
				callProgram(Rlagprd.class, rlagprdrec1.rlagprdRec);
				if (isEQ(rlagprdrec1.statuz, varcom.bomb)) {
					syserrrec.statuz.set(rlagprdrec1.statuz);
					syserr570();
				}
			}
		}
		if (isEQ(lifacmvrec.function, "PSTW ")
		&& isNE(lifacmvrec.sacscode, SPACES)
		|| isEQ(lifacmvrec.function, "PSTD")
		&& isNE(lifacmvrec.sacscode, SPACES)) {
			updateAcbl2000();
		}
		if (isEQ(lifacmvrec.function, "PSTD ")
		&& isNE(lifacmvrec.sacscode, SPACES)
		&& isEQ(tr362rec.appflag, "Y"))
			writeDacm6000();
		
		callBatcup3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		lifacmvrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		lifacmvrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}

protected void readContractDetails800()
	{
		readDetails800();
	}

protected void readDetails800()
	{
		/* Read CHDRLIF for Agent Number, Contract Type, Original*/
		/* Commencment Date.*/
		trcdecpy.trancodeCpy.set(lifacmvrec.batctrcde);
		chdrlifIO.setStatuz(varcom.oK);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setChdrcoy(lifacmvrec.batccoy);
		chdrlifIO.setChdrnum(lifacmvrec.rdocnum);
		if (trcdecpy.cheqCodes.isTrue()
		|| trcdecpy.cashCodes.isTrue()
		|| trcdecpy.jrnlCodes.isTrue()) {
			chdrlifIO.setChdrnum(subString(lifacmvrec.rldgacct, 1, 8));
		}
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)
		&& isNE(chdrlifIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError580();
		}
	}

protected void writeAcmv1000()
	{
		glSubstitution1010();
	}

protected void glSubstitution1010()
	{
	    glSubstitution1011();   //COA
	    		
		/*  If accounting currency details not entered,*/
		/*   and the original currency has changed since the last time*/
		/*     - look up the nominal exchange rate.*/
		if (isEQ(lifacmvrec.genlcur, SPACES)
		&& isNE(lifacmvrec.origcurr, wsaaLastOrigCcy)) {
			getRate1100();
		}
		/*  Go and read T3695 to get the Internal/External Indicator for*/
		/*  the Subaccount Type being posted to.*/
		if (isNE(lifacmvrec.sacstyp, SPACES)) {
			getIntextind1200();
		}
		else {
			t3695rec.intextind.set(SPACES);
		}
		/*    Get the machine date and time to stamp ACMVs*/
		wsaaMachineDate.set(getCobolDate());
		wsaaMachineTimex.set(getCobolTime());
		/*  Write movement record.*/
		acmvIO.setBatccoy(lifacmvrec.batccoy);
		acmvIO.setBatcbrn(lifacmvrec.batcbrn);
		acmvIO.setBatcactyr(lifacmvrec.batcactyr);
		acmvIO.setBatcactmn(lifacmvrec.batcactmn);
		acmvIO.setBatctrcde(lifacmvrec.batctrcde);
		acmvIO.setBatcbatch(lifacmvrec.batcbatch);
		acmvIO.setRdoccoy(lifacmvrec.rldgcoy);
		acmvIO.setRdocnum(lifacmvrec.rdocnum);
		acmvIO.setTranno(lifacmvrec.tranno);
		acmvIO.setJrnseq(lifacmvrec.jrnseq);
		acmvIO.setRdocnum(lifacmvrec.rdocnum);
		acmvIO.setRldgcoy(lifacmvrec.rldgcoy);
		acmvIO.setSacscode(lifacmvrec.sacscode);
		acmvIO.setRldgacct(lifacmvrec.rldgacct);
		acmvIO.setOrigcurr(lifacmvrec.origcurr);
		acmvIO.setSacstyp(lifacmvrec.sacstyp);
		acmvIO.setOrigamt(lifacmvrec.origamt);
		acmvIO.setTranref(lifacmvrec.tranref);
		acmvIO.setTrandesc(lifacmvrec.trandesc);
		acmvIO.setCrate(lifacmvrec.crate);
		acmvIO.setAcctamt(lifacmvrec.acctamt);
		acmvIO.setGenlcoy(lifacmvrec.genlcoy);
		acmvIO.setGenlcur(lifacmvrec.genlcur);
		acmvIO.setGlcode(lifacmvrec.glcode);
		acmvIO.setGlsign(lifacmvrec.glsign);
		postyymmr.function.set("GET");
		postyymmr.batccoy.set(lifacmvrec.batccoy);
		postyymmr.batcbrn.set(lifacmvrec.batcbrn);
		postyymmr.batcactyr.set(lifacmvrec.batcactyr);
		postyymmr.batcactmn.set(lifacmvrec.batcactmn);
		postyymmr.batctrcde.set(lifacmvrec.batctrcde);
		postyymmr.batcbatch.set(lifacmvrec.batcbatch);
		postyymmr.effdate.set(lifacmvrec.effdate);
		callProgram(Postyymm.class, postyymmr.postyymmRec);
		if (isNE(postyymmr.statuz, "****")) {
			syserrrec.dbparams.set(postyymmr.postyymmRec);
			syserrrec.statuz.set(postyymmr.statuz);
			syserr570();
		}
		acmvIO.setPostyear(postyymmr.postyear);
		acmvIO.setPostmonth(postyymmr.postmonth);
		acmvIO.setEffdate(lifacmvrec.effdate);
		acmvIO.setRcamt(lifacmvrec.rcamt);
		acmvIO.setFrcdate(lifacmvrec.frcdate);
		acmvIO.setTransactionDate(wsaaMachineDate);
		acmvIO.setTransactionTime(wsaaMachineTime);
		acmvIO.setUser(lifacmvrec.user);
		acmvIO.setTermid(lifacmvrec.termid);
		acmvIO.setSuprflg(lifacmvrec.suprflag);
		acmvIO.setIntextind(t3695rec.intextind);
		if (isEQ(lifacmvrec.genlcur, SPACES)) {
			acmvIO.setCrate(wsaaNominalRate);
			acmvIO.setGenlcur(wsaaLedgerCcy);
			if (isEQ(lifacmvrec.acctamt, ZERO)) {
				setPrecision(acmvIO.getAcctamt(), 10);
				acmvIO.setAcctamt(mult(lifacmvrec.origamt, wsaaNominalRate), true);
				zrdecplrec.amountIn.set(acmvIO.getAcctamt());
				zrdecplrec.currency.set(acmvIO.getGenlcur());
				a000CallRounding();
				acmvIO.setAcctamt(zrdecplrec.amountOut);
			}
		}
		zrdecplrec.amountIn.set(acmvIO.getOrigamt());
		zrdecplrec.currency.set(acmvIO.getOrigcurr());
		a000CallRounding();
		acmvIO.setOrigamt(zrdecplrec.amountOut);
		acmvIO.setStmtsort(LOVALUE);
		// Start : Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
				if(isEQ(lifacmvrec.tranno,ZERO))
				{
					if(isEQ(lifacmvrec.ind,"G"))
					{
						acmvIO.rldgpfx.set(lifacmvrec.prefix);
					}
					if(isEQ(lifacmvrec.ind,"D"))
					{
						acmvIO.rdocpfx.set(lifacmvrec.prefix);
					}
				}
				// End : Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		acmvIO.setFormat(acmvrec);
		acmvIO.setFunction("WRITR");
		if (isEQ(acmvIO.getPostmonth(), SPACES)) {
			wsaaPost.set(acmvIO.getBatcactmn());
			acmvIO.setPostmonth(wsaaPost);
		}
		if (isEQ(acmvIO.getPostyear(), SPACES)) {
			wsaaPost.set(acmvIO.getBatcactyr());
			acmvIO.setPostyear(wsaaPost);
		}
		acmvIO.setReconref(ZERO);
		acmvIO.setCreddte(99999999);
		SmartFileCode.execute(appVars, acmvIO);
		if (isNE(acmvIO.getStatuz(), "****")) {
			syserrrec.params.set(acmvIO.getParams());
			dbError580();
		}
		if (isEQ(tr362rec.appflag, "Y")) {
			diaryUpdate5000();
		}
	}

protected void getRate1100()
	{
					readCurrencyRecord1110();
					reRead1120();
					findRate1150();
			
	}



protected void readCurrencyRecord1110()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lifacmvrec.batccoy);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(lifacmvrec.origcurr);
	}

protected void reRead1120()
	{
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLastOrigCcy.set(lifacmvrec.origcurr);
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaNominalRate, ZERO)
		|| isGT(wsaaX, 7))) {
			findRate1150();
		}
		
		if (isEQ(wsaaNominalRate, ZERO)) {
			if (isNE(t3629rec.contitem, SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				reRead1120();
				return ;
			}
			else {
				syserrrec.statuz.set(g418);
				syserr570();
			}
		}
		else {
			return;
		}
	}

protected void findRate1150()
	{
		if (isGTE(lifacmvrec.effdate, t3629rec.frmdate[wsaaX.toInt()])
		&& isLTE(lifacmvrec.effdate, t3629rec.todate[wsaaX.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
		}
		else {
			wsaaX.add(1);
		}
	}

protected void getIntextind1200()
	{
		try {
			checkTable1200();
			start1200();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkTable1200()
	{
		for (iz.set(1); !(isGT(iz, 50)
		|| isEQ(wsaaT3695Sacstyp[iz.toInt()], HIVALUE)
		|| isEQ(wsaaT3695Sacstyp[iz.toInt()], lifacmvrec.sacstyp)); iz.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT3695Intextind[iz.toInt()], lifacmvrec.sacstyp)) {
			t3695rec.intextind.set(wsaaT3695Intextind[iz.toInt()]);
			return;
		}
	}





protected void start1200()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lifacmvrec.batccoy);
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(lifacmvrec.sacstyp);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		if (isLT(iz, 51)) {
			wsaaT3695Sacstyp[iz.toInt()].set(itemIO.getItemitem());
			wsaaT3695Intextind[iz.toInt()].set(t3695rec.intextind);
		}
	}

protected void writeAgpyagt1300()
	{
		start1300();
	}

protected void start1300()
	{
		agpyagtIO.setParams(SPACES);
		agpyagtIO.setBatccoy(acmvIO.getBatccoy());
		agpyagtIO.setBatcbrn(acmvIO.getBatcbrn());
		agpyagtIO.setBatcactyr(acmvIO.getBatcactyr());
		agpyagtIO.setBatcactmn(acmvIO.getBatcactmn());
		agpyagtIO.setBatctrcde(acmvIO.getBatctrcde());
		agpyagtIO.setBatcbatch(acmvIO.getBatcbatch());
		agpyagtIO.setRdocnum(acmvIO.getRdocnum());
		agpyagtIO.setTranno(acmvIO.getTranno());
		agpyagtIO.setJrnseq(acmvIO.getJrnseq());
		agpyagtIO.setRldgcoy(acmvIO.getRldgcoy());
		agpyagtIO.setRldgacct(acmvIO.getRldgacct());
		agpyagtIO.setOrigcurr(acmvIO.getOrigcurr());
		agpyagtIO.setSacscode(acmvIO.getSacscode());
		agpyagtIO.setSacstyp(acmvIO.getSacstyp());
		agpyagtIO.setOrigamt(acmvIO.getOrigamt());
		agpyagtIO.setTranref(acmvIO.getTranref());
		agpyagtIO.setTrandesc(acmvIO.getTrandesc());
		agpyagtIO.setCrate(acmvIO.getCrate());
		agpyagtIO.setAcctamt(acmvIO.getAcctamt());
		agpyagtIO.setGenlcoy(acmvIO.getGenlcoy());
		agpyagtIO.setGenlcur(acmvIO.getGenlcur());
		agpyagtIO.setGlcode(acmvIO.getGlcode());
		agpyagtIO.setGlsign(acmvIO.getGlsign());
		agpyagtIO.setPostyear(acmvIO.getPostyear());
		agpyagtIO.setPostmonth(acmvIO.getPostmonth());
		agpyagtIO.setEffdate(acmvIO.getEffdate());
		agpyagtIO.setRcamt(acmvIO.getRcamt());
		agpyagtIO.setFrcdate(acmvIO.getFrcdate());
		agpyagtIO.setTransactionDate(acmvIO.getTransactionDate());
		agpyagtIO.setTransactionTime(acmvIO.getTransactionTime());
		agpyagtIO.setUser(acmvIO.getUser());
		agpyagtIO.setTermid(acmvIO.getTermid());
		agpyagtIO.setSuprflg(acmvIO.getSuprflg());
		agpyagtIO.setFormat(agpyagtrec);
		agpyagtIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agpyagtIO.getParams());
			syserrrec.statuz.set(agpyagtIO.getStatuz());
			dbError580();
		}
	}

protected void updateAcbl2000(){
	if ((isEQ(lifacmvrec.agnt, "N"))) {
		acblpf =  new Acblpf(); 
		acblpf.setRldgcoy(lifacmvrec.rldgcoy.toString());
		acblpf.setRldgacct(lifacmvrec.rldgacct.toString());
		acblpf.setOrigcurr(lifacmvrec.origcurr.toString());
		acblpf.setSacscode(lifacmvrec.sacscode.toString());
		acblpf.setSacstyp(lifacmvrec.sacstyp.toString());
		acblpf.setSacscurbal(BigDecimal.ZERO);
		getSacscurbal();
		List<Acblpf> acblpfList = new ArrayList<Acblpf>();
		acblpfList.add(acblpf);
		acblpfListAcbl = acblpfList;
	}
	else readRecord2010();
	
}

protected void readRecord2010()
	{
		boolean insertFlag = false;
		acblpf = acblDao.getAcblpfRecord(lifacmvrec.rldgcoy.toString().trim(), lifacmvrec.sacscode.toString().trim(), 
				lifacmvrec.rldgacct.toString(),	lifacmvrec.sacstyp.toString(),lifacmvrec.origcurr.toString().trim());  	
		if (acblpf == null) { 
			acblpf =  new Acblpf(); 
			acblpf.setRldgcoy(lifacmvrec.rldgcoy.toString());
			// MOVE SCPT-CHDRNUM           TO ACBL-RLDGACCT.
			acblpf.setRldgacct(lifacmvrec.rldgacct.toString());
			acblpf.setOrigcurr(lifacmvrec.origcurr.toString());
			acblpf.setSacscode(lifacmvrec.sacscode.toString());
			acblpf.setSacstyp(lifacmvrec.sacstyp.toString());
			acblpf.setSacscurbal(BigDecimal.ZERO);
			insertFlag = true;
		}
		
		getSacscurbal();
		
		List<Acblpf> acblpfList = new ArrayList<Acblpf>();
		acblpfList.add(acblpf);
		
		if(insertFlag){
			if ((isEQ(lifacmvrec.agnt, "Y"))){
				acblpfListparam = acblpfList;
			}
			else
			acblDao.insertAcblpfList(acblpfList);
		}else{
			if ((isEQ(lifacmvrec.agnt, "Y"))){
				acblpfUpdtListparam = acblpfList;
			}
			else
			acblDao.updateAcblRecord(acblpfList);
		}
	}

protected void getSacscurbal() {
	setPrecision(acblpf.getSacscurbal(), 2);
	if (isEQ(lifacmvrec.glsign, "-")) {			
		acblpf.setSacscurbal(new BigDecimal((sub(acblpf.getSacscurbal(), lifacmvrec.origamt).toString())));
	}
	else {			
		acblpf.setSacscurbal(new BigDecimal((add(acblpf.getSacscurbal(), lifacmvrec.origamt).toString())));
	}
		
	
	zrdecplrec.amountIn.set(acblpf.getSacscurbal());
	zrdecplrec.currency.set(acblpf.getOrigcurr());
	a000CallRounding();
	acblpf.setSacscurbal(zrdecplrec.amountOut.getbigdata());	
}

protected void callBatcup3000()
	{
		batcup3000();
	}

protected void batcup3000()
	{
		batcuprec.batcpfx.set("BA");
		batcuprec.batccoy.set(lifacmvrec.batccoy);
		batcuprec.batcbrn.set(lifacmvrec.batcbrn);
		batcuprec.batcactyr.set(lifacmvrec.batcactyr);
		batcuprec.batcactmn.set(lifacmvrec.batcactmn);
		batcuprec.batctrcde.set(lifacmvrec.batctrcde);
		batcuprec.batcbatch.set(lifacmvrec.batcbatch);
		batcuprec.trancnt.set(0);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(lifacmvrec.contot);
		batcuprec.ascnt.set(0);
		batcuprec.bcnt.set(1);
		batcuprec.bval.set(acmvIO.getAcctamt());
		batcuprec.function.set("KEEPS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, "****")) {
			lifacmvrec.statuz.set(batcuprec.statuz);
			syserrrec.statuz.set(batcuprec.statuz);
			syserr570();
		}
	}

protected void checkSchedule24x7Setup4000()
	{
		begin4010();
	}

protected void begin4010()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lifacmvrec.batccoy);
		itemIO.setItemseq(SPACES);
		itemIO.setItemtabl(tr362);
		itemIO.setItemitem("***");
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			tr362rec.tr362Rec.set(itemIO.getGenarea());
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				tr362rec.tr362Rec.set(SPACES);
			}
			else {
				lifacmvrec.statuz.set(itemIO.getStatuz());
				syserrrec.statuz.set(itemIO.getStatuz());
				syserr570();
			}
		}
	}

protected void diaryUpdate5000()
	{
		update5100();
	}

protected void update5100()
	{
		if (isEQ(acmvIO.getOrigamt(), ZERO)
		&& isEQ(acmvIO.getAcctamt(), ZERO)) {
			return ;
		}
		if (isEQ(acmvIO.getGlcode(), SPACES)) {
			return ;
		}
		dglxIO.setParams(SPACES);
		dglxIO.setBatccoy(acmvIO.getBatccoy());
		dglxIO.setBatcbrn(acmvIO.getBatcbrn());
		dglxIO.setBatcactyr(acmvIO.getBatcactyr());
		dglxIO.setBatcactmn(acmvIO.getBatcactmn());
		dglxIO.setBatctrcde(acmvIO.getBatctrcde());
		dglxIO.setBatcbatch(acmvIO.getBatcbatch());
		dglxIO.setGenlcoy(acmvIO.getGenlcoy());
		dglxIO.setGenlcur(acmvIO.getGenlcur());
		dglxIO.setGenlcde(acmvIO.getGlcode());
		dglxIO.setEffdate(acmvIO.getEffdate());
		dglxIO.setOrigcurr(acmvIO.getOrigcurr());
		dglxIO.setPostyear(acmvIO.getPostyear());
		dglxIO.setPostmonth(acmvIO.getPostmonth());
		dglxIO.setRldgacct(acmvIO.getRldgacct());
		if (isEQ(acmvIO.getGlsign(), "-")) {
			setPrecision(dglxIO.getOrigamt(), 2);
			dglxIO.setOrigamt(sub(0, acmvIO.getOrigamt()));
			setPrecision(dglxIO.getAcctamt(), 2);
			dglxIO.setAcctamt(sub(0, acmvIO.getAcctamt()));
		}
		else {
			dglxIO.setOrigamt(acmvIO.getOrigamt());
			dglxIO.setAcctamt(acmvIO.getAcctamt());
		}
		callProgram(Getstamp.class, wsaaUsrprf, wsaaJobnm, dglxIO.getCrtdatime());
		dglxIO.setFunction(varcom.writr);
		dglxIO.setFormat(dglxrec);
		SmartFileCode.execute(appVars, dglxIO);
		if (isNE(dglxIO.getStatuz(), varcom.oK)) {
			lifacmvrec.statuz.set(dglxIO.getStatuz());
			syserrrec.statuz.set(dglxIO.getStatuz());
			syserr570();
		}
	}

protected void writeDacm6000()
	{
		write6100();
	}

protected void write6100()
	{
		/* A deferred ACBL update has been found.                          */
		/* Start by completing the fields on the ACBL data area.           */
		/* Once this is complete, write a new DACM record using the        */
		/* ACBL data area.                                                 */
		acagrnlIO.setParams(SPACES);
		acagrnlIO.setRecKeyData(SPACES);
		acagrnlIO.setRecNonKeyData(SPACES);
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		/* Set up the DACM record, used for deferred processing            */
		/* within the diary system.                                        */
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(lifacmvrec.threadNumber);
		dacmIO.setCompany(lifacmvrec.batccoy);
		dacmIO.setRecformat(acagrnlrec);
		dacmIO.setDataarea(acagrnlIO.getDataArea());
		dacmIO.setFormat(dacmrec);
		dacmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), varcom.oK)) {
			lifacmvrec.statuz.set(dacmIO.getStatuz());
			syserrrec.statuz.set(dacmIO.getStatuz());
			syserr570();
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(lifacmvrec.batccoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(lifacmvrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr570();
		}
		/*A900-EXIT*/
	}


//COA
protected void glSubstitution1011() { 
	
	lifacmvrec.glcode.set(inspectReplaceAll(lifacmvrec.glcode, "?", lifacmvrec.batccoy));
	lifacmvrec.glcode.set(inspectReplaceAll(lifacmvrec.glcode, "##", lifacmvrec.batcbrn));
	lifacmvrec.glcode.set(inspectReplaceAll(lifacmvrec.glcode, "&&&", lifacmvrec.origcurr));
	/* NB. Assume LIFA-SUBSTITUTE-CODE is filled left justified,*/
	/* ie. 'A  ' NOT '  A'.*/
	if (isNE(lifacmvrec.substituteCode[1], SPACES)) {
		wsaaSubstituteCode.set(lifacmvrec.substituteCode[1]);
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "@", wsaaSubscode01));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "@", wsaaSubscode02));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "@", wsaaSubscode03));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "@", wsaaSubscode04));
	}
	if (isNE(lifacmvrec.substituteCode[2], SPACES)) {
		wsaaSubstituteCode.set(lifacmvrec.substituteCode[2]);
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "*", wsaaSubscode01));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "*", wsaaSubscode02));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "*", wsaaSubscode03));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "*", wsaaSubscode04));
	}
	if (isNE(lifacmvrec.substituteCode[3], SPACES)) {
		wsaaSubstituteCode.set(lifacmvrec.substituteCode[3]);
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "%", wsaaSubscode01));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "%", wsaaSubscode02));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "%", wsaaSubscode03));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "%", wsaaSubscode04));
	}
	if (isNE(lifacmvrec.substituteCode[4], SPACES)) {
		wsaaSubstituteCode.set(lifacmvrec.substituteCode[4]);
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "!", wsaaSubscode01));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "!", wsaaSubscode02));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "!", wsaaSubscode03));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "!", wsaaSubscode04));
	}
	if (isNE(lifacmvrec.substituteCode[5], SPACES)) {
		wsaaSubstituteCode.set(lifacmvrec.substituteCode[5]);
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "+", wsaaSubscode01));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "+", wsaaSubscode02));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "+", wsaaSubscode03));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "+", wsaaSubscode04));
	}
	/* New substitution code No 6  - Coverage/Rider table*/
	if (isNE(lifacmvrec.substituteCode[6], SPACES)) {
		wsaaSubstituteCode.set(lifacmvrec.substituteCode[6]);
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "=", wsaaSubscode01));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "=", wsaaSubscode02));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "=", wsaaSubscode03));
		lifacmvrec.glcode.set(inspectReplaceFirst(lifacmvrec.glcode, "=", wsaaSubscode04));
	}
		
}

}
