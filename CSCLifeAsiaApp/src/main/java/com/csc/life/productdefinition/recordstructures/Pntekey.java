package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:50
 * Description:
 * Copybook name: PNTEKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pntekey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData pnteFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData pnteKey = new FixedLengthStringData(256).isAPartOf(pnteFileKey, 0, REDEFINE);
  	public FixedLengthStringData pnteChdrcoy = new FixedLengthStringData(1).isAPartOf(pnteKey, 0);
  	public FixedLengthStringData pnteChdrnum = new FixedLengthStringData(8).isAPartOf(pnteKey, 1);
  	public PackedDecimalData pnteTrandate = new PackedDecimalData(8, 0).isAPartOf(pnteKey, 9);
  	public PackedDecimalData pnteTrantime = new PackedDecimalData(6, 0).isAPartOf(pnteKey, 14);
  	public PackedDecimalData pnteSeqno = new PackedDecimalData(4, 0).isAPartOf(pnteKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(235).isAPartOf(pnteKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(pnteFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		pnteFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}