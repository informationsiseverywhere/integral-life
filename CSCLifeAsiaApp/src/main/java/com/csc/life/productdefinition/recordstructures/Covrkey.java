package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:23
 * Description:
 * Copybook name: COVRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrKey = new FixedLengthStringData(64).isAPartOf(covrFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrChdrcoy = new FixedLengthStringData(1).isAPartOf(covrKey, 0);
  	public FixedLengthStringData covrChdrnum = new FixedLengthStringData(8).isAPartOf(covrKey, 1);
  	public FixedLengthStringData covrLife = new FixedLengthStringData(2).isAPartOf(covrKey, 9);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(covrKey, 11);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(covrKey, 13);
  	public PackedDecimalData covrPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}