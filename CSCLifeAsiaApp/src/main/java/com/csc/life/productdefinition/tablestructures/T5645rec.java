package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:28
 * Description:
 * Copybook name: T5645REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5645rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5645Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cnttots = new FixedLengthStringData(30).isAPartOf(t5645Rec, 0);
  	public ZonedDecimalData[] cnttot = ZDArrayPartOfStructure(15, 2, 0, cnttots, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(cnttots, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cnttot01 = new ZonedDecimalData(2, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData cnttot02 = new ZonedDecimalData(2, 0).isAPartOf(filler, 2);
  	public ZonedDecimalData cnttot03 = new ZonedDecimalData(2, 0).isAPartOf(filler, 4);
  	public ZonedDecimalData cnttot04 = new ZonedDecimalData(2, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData cnttot05 = new ZonedDecimalData(2, 0).isAPartOf(filler, 8);
  	public ZonedDecimalData cnttot06 = new ZonedDecimalData(2, 0).isAPartOf(filler, 10);
  	public ZonedDecimalData cnttot07 = new ZonedDecimalData(2, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData cnttot08 = new ZonedDecimalData(2, 0).isAPartOf(filler, 14);
  	public ZonedDecimalData cnttot09 = new ZonedDecimalData(2, 0).isAPartOf(filler, 16);
  	public ZonedDecimalData cnttot10 = new ZonedDecimalData(2, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData cnttot11 = new ZonedDecimalData(2, 0).isAPartOf(filler, 20);
  	public ZonedDecimalData cnttot12 = new ZonedDecimalData(2, 0).isAPartOf(filler, 22);
  	public ZonedDecimalData cnttot13 = new ZonedDecimalData(2, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData cnttot14 = new ZonedDecimalData(2, 0).isAPartOf(filler, 26);
  	public ZonedDecimalData cnttot15 = new ZonedDecimalData(2, 0).isAPartOf(filler, 28);
  	public FixedLengthStringData glmaps = new FixedLengthStringData(210).isAPartOf(t5645Rec, 30);
  	public FixedLengthStringData[] glmap = FLSArrayPartOfStructure(15, 14, glmaps, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(210).isAPartOf(glmaps, 0, FILLER_REDEFINE);
  	public FixedLengthStringData glmap01 = new FixedLengthStringData(14).isAPartOf(filler1, 0);
  	public FixedLengthStringData glmap02 = new FixedLengthStringData(14).isAPartOf(filler1, 14);
  	public FixedLengthStringData glmap03 = new FixedLengthStringData(14).isAPartOf(filler1, 28);
  	public FixedLengthStringData glmap04 = new FixedLengthStringData(14).isAPartOf(filler1, 42);
  	public FixedLengthStringData glmap05 = new FixedLengthStringData(14).isAPartOf(filler1, 56);
  	public FixedLengthStringData glmap06 = new FixedLengthStringData(14).isAPartOf(filler1, 70);
  	public FixedLengthStringData glmap07 = new FixedLengthStringData(14).isAPartOf(filler1, 84);
  	public FixedLengthStringData glmap08 = new FixedLengthStringData(14).isAPartOf(filler1, 98);
  	public FixedLengthStringData glmap09 = new FixedLengthStringData(14).isAPartOf(filler1, 112);
  	public FixedLengthStringData glmap10 = new FixedLengthStringData(14).isAPartOf(filler1, 126);
  	public FixedLengthStringData glmap11 = new FixedLengthStringData(14).isAPartOf(filler1, 140);
  	public FixedLengthStringData glmap12 = new FixedLengthStringData(14).isAPartOf(filler1, 154);
  	public FixedLengthStringData glmap13 = new FixedLengthStringData(14).isAPartOf(filler1, 168);
  	public FixedLengthStringData glmap14 = new FixedLengthStringData(14).isAPartOf(filler1, 182);
  	public FixedLengthStringData glmap15 = new FixedLengthStringData(14).isAPartOf(filler1, 196);
  	public FixedLengthStringData sacscodes = new FixedLengthStringData(30).isAPartOf(t5645Rec, 240);
  	public FixedLengthStringData[] sacscode = FLSArrayPartOfStructure(15, 2, sacscodes, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(30).isAPartOf(sacscodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData sacscode01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData sacscode02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData sacscode03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData sacscode04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData sacscode05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData sacscode06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData sacscode07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData sacscode08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData sacscode09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData sacscode10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData sacscode11 = new FixedLengthStringData(2).isAPartOf(filler2, 20);
  	public FixedLengthStringData sacscode12 = new FixedLengthStringData(2).isAPartOf(filler2, 22);
  	public FixedLengthStringData sacscode13 = new FixedLengthStringData(2).isAPartOf(filler2, 24);
  	public FixedLengthStringData sacscode14 = new FixedLengthStringData(2).isAPartOf(filler2, 26);
  	public FixedLengthStringData sacscode15 = new FixedLengthStringData(2).isAPartOf(filler2, 28);
  	public FixedLengthStringData sacstypes = new FixedLengthStringData(30).isAPartOf(t5645Rec, 270);
  	public FixedLengthStringData[] sacstype = FLSArrayPartOfStructure(15, 2, sacstypes, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(sacstypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData sacstype01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData sacstype02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData sacstype03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData sacstype04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData sacstype05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData sacstype06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData sacstype07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData sacstype08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData sacstype09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData sacstype10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	public FixedLengthStringData sacstype11 = new FixedLengthStringData(2).isAPartOf(filler3, 20);
  	public FixedLengthStringData sacstype12 = new FixedLengthStringData(2).isAPartOf(filler3, 22);
  	public FixedLengthStringData sacstype13 = new FixedLengthStringData(2).isAPartOf(filler3, 24);
  	public FixedLengthStringData sacstype14 = new FixedLengthStringData(2).isAPartOf(filler3, 26);
  	public FixedLengthStringData sacstype15 = new FixedLengthStringData(2).isAPartOf(filler3, 28);
  	public FixedLengthStringData signs = new FixedLengthStringData(15).isAPartOf(t5645Rec, 300);
  	public FixedLengthStringData[] sign = FLSArrayPartOfStructure(15, 1, signs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(15).isAPartOf(signs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData sign01 = new FixedLengthStringData(1).isAPartOf(filler4, 0);
  	public FixedLengthStringData sign02 = new FixedLengthStringData(1).isAPartOf(filler4, 1);
  	public FixedLengthStringData sign03 = new FixedLengthStringData(1).isAPartOf(filler4, 2);
  	public FixedLengthStringData sign04 = new FixedLengthStringData(1).isAPartOf(filler4, 3);
  	public FixedLengthStringData sign05 = new FixedLengthStringData(1).isAPartOf(filler4, 4);
  	public FixedLengthStringData sign06 = new FixedLengthStringData(1).isAPartOf(filler4, 5);
  	public FixedLengthStringData sign07 = new FixedLengthStringData(1).isAPartOf(filler4, 6);
  	public FixedLengthStringData sign08 = new FixedLengthStringData(1).isAPartOf(filler4, 7);
  	public FixedLengthStringData sign09 = new FixedLengthStringData(1).isAPartOf(filler4, 8);
  	public FixedLengthStringData sign10 = new FixedLengthStringData(1).isAPartOf(filler4, 9);
  	public FixedLengthStringData sign11 = new FixedLengthStringData(1).isAPartOf(filler4, 10);
  	public FixedLengthStringData sign12 = new FixedLengthStringData(1).isAPartOf(filler4, 11);
  	public FixedLengthStringData sign13 = new FixedLengthStringData(1).isAPartOf(filler4, 12);
  	public FixedLengthStringData sign14 = new FixedLengthStringData(1).isAPartOf(filler4, 13);
  	public FixedLengthStringData sign15 = new FixedLengthStringData(1).isAPartOf(filler4, 14);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(185).isAPartOf(t5645Rec, 315, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5645Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5645Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}