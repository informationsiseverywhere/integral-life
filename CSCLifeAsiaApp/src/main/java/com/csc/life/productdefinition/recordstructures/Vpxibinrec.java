package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Tue, 18 Sep 2012 03:09:00
 * Description: VPMS General Data Extract -  Interest Bearing Calculation Extract.                                                
 * Copybook name: VPXIBINREC
 *
 * Copyright (2012) CSC Asia, all rights reserved
 *
 */
public class Vpxibinrec extends ExternalData {

  
  	public FixedLengthStringData vpxibinrec = new FixedLengthStringData(164);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxibinrec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxibinrec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxibinrec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxibinrec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(20).isAPartOf(vpxibinrec, 136);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(dataRec, 0, FILLER);
  	
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxibinrec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxibinrec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}