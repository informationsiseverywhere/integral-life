package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5667screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5667ScreenVars sv = (S5667ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5667screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5667ScreenVars screenVars = (S5667ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.prmtol01.setClassString("");
		screenVars.maxAmount01.setClassString("");
		screenVars.prmtol02.setClassString("");
		screenVars.maxAmount02.setClassString("");
		screenVars.prmtol03.setClassString("");
		screenVars.maxAmount03.setClassString("");
		screenVars.prmtol04.setClassString("");
		screenVars.maxAmount04.setClassString("");
		screenVars.prmtol05.setClassString("");
		screenVars.maxAmount05.setClassString("");
		screenVars.prmtol06.setClassString("");
		screenVars.maxAmount06.setClassString("");
		screenVars.prmtol07.setClassString("");
		screenVars.maxAmount07.setClassString("");
		screenVars.prmtol08.setClassString("");
		screenVars.maxAmount08.setClassString("");
		screenVars.prmtol09.setClassString("");
		screenVars.maxAmount09.setClassString("");
		screenVars.prmtol10.setClassString("");
		screenVars.maxAmount10.setClassString("");
		screenVars.prmtol11.setClassString("");
		screenVars.maxAmount11.setClassString("");
		screenVars.freq01.setClassString("");
		screenVars.freq02.setClassString("");
		screenVars.freq03.setClassString("");
		screenVars.freq04.setClassString("");
		screenVars.freq05.setClassString("");
		screenVars.freq06.setClassString("");
		screenVars.freq07.setClassString("");
		screenVars.freq08.setClassString("");
		screenVars.freq09.setClassString("");
		screenVars.freq10.setClassString("");
		screenVars.freq11.setClassString("");
		screenVars.prmtoln01.setClassString("");
		screenVars.maxamt01.setClassString("");
		screenVars.prmtoln02.setClassString("");
		screenVars.maxamt02.setClassString("");
		screenVars.prmtoln03.setClassString("");
		screenVars.maxamt03.setClassString("");
		screenVars.prmtoln04.setClassString("");
		screenVars.maxamt04.setClassString("");
		screenVars.prmtoln05.setClassString("");
		screenVars.maxamt05.setClassString("");
		screenVars.prmtoln06.setClassString("");
		screenVars.maxamt06.setClassString("");
		screenVars.prmtoln07.setClassString("");
		screenVars.maxamt07.setClassString("");
		screenVars.prmtoln08.setClassString("");
		screenVars.maxamt08.setClassString("");
		screenVars.prmtoln09.setClassString("");
		screenVars.maxamt09.setClassString("");
		screenVars.prmtoln10.setClassString("");
		screenVars.maxamt10.setClassString("");
		screenVars.prmtoln11.setClassString("");
		screenVars.maxamt11.setClassString("");
		screenVars.sfind.setClassString("");
	}

/**
 * Clear all the variables in S5667screen
 */
	public static void clear(VarModel pv) {
		S5667ScreenVars screenVars = (S5667ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.prmtol01.clear();
		screenVars.maxAmount01.clear();
		screenVars.prmtol02.clear();
		screenVars.maxAmount02.clear();
		screenVars.prmtol03.clear();
		screenVars.maxAmount03.clear();
		screenVars.prmtol04.clear();
		screenVars.maxAmount04.clear();
		screenVars.prmtol05.clear();
		screenVars.maxAmount05.clear();
		screenVars.prmtol06.clear();
		screenVars.maxAmount06.clear();
		screenVars.prmtol07.clear();
		screenVars.maxAmount07.clear();
		screenVars.prmtol08.clear();
		screenVars.maxAmount08.clear();
		screenVars.prmtol09.clear();
		screenVars.maxAmount09.clear();
		screenVars.prmtol10.clear();
		screenVars.maxAmount10.clear();
		screenVars.prmtol11.clear();
		screenVars.maxAmount11.clear();
		screenVars.freq01.clear();
		screenVars.freq02.clear();
		screenVars.freq03.clear();
		screenVars.freq04.clear();
		screenVars.freq05.clear();
		screenVars.freq06.clear();
		screenVars.freq07.clear();
		screenVars.freq08.clear();
		screenVars.freq09.clear();
		screenVars.freq10.clear();
		screenVars.freq11.clear();
		screenVars.prmtoln01.clear();
		screenVars.maxamt01.clear();
		screenVars.prmtoln02.clear();
		screenVars.maxamt02.clear();
		screenVars.prmtoln03.clear();
		screenVars.maxamt03.clear();
		screenVars.prmtoln04.clear();
		screenVars.maxamt04.clear();
		screenVars.prmtoln05.clear();
		screenVars.maxamt05.clear();
		screenVars.prmtoln06.clear();
		screenVars.maxamt06.clear();
		screenVars.prmtoln07.clear();
		screenVars.maxamt07.clear();
		screenVars.prmtoln08.clear();
		screenVars.maxamt08.clear();
		screenVars.prmtoln09.clear();
		screenVars.maxamt09.clear();
		screenVars.prmtoln10.clear();
		screenVars.maxamt10.clear();
		screenVars.prmtoln11.clear();
		screenVars.maxamt11.clear();
		screenVars.sfind.clear();
	}
}
