/*
 * File: Tprmpm6.java
 * Date: 30 August 2009 2:39:41
 * Author: Quipoz Limited
 * 
 * Class transformed from TPRMPM6.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.tablestructures.Th606rec;
import com.csc.life.productdefinition.recordstructures.Extprmrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.life.productdefinition.tablestructures.Th549rec;
import com.csc.life.productdefinition.tablestructures.Tt507rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  CLONED FROM PRMPM04.
*  THAILAND NON-ANNUAL PREMIUM CALCULATION : METHOD 06
*  AGE BASED CALCULATION SUBROUTINE
*
*
* PROCESSING.
* ----------
*
* Initialise all working storage fields and set keys to read
* tables. Include a table (occurs 8) to hold the Options/Extras
* (LEXT) record details.
*
* Build a key.  This key (see below) will read table TT507.
* This table contains the parameters to be used in the
* calculation of the Basic Annual Premium for
* Coverage/Rider components.
*
* The key is a concatenation of the following fields:-
*
* Coverage/Rider table code
* Bill Frequency
*
* Access the required table by reading the table directly (ITDM).
* The contents of the table are then stored. This table is dated
* use:
*
*  1) Rating Date
*
* CALCULATE-BASIC-PREMIUM (and apply age rates)
* (Age, Sex & Duration taken from linkage)
*
* Obtain the age rates from the (LEXT) record.
*
*  - read all the LEXT records for this contract, life and
*  coverage into the working-storage table. Compute the
*  adjusted age as being the summation of the LEXT age
*  rates plus the ANB @ RCD.
*
* Use the age calculated above to access the table TT507 and
* check the following:
*
*  - that the basic premium (indexed by year) from
*  the TT507 table is not zero. If it is zero, then display
*  an error message and skip the additional procedures.
*  Otherwise store the premium as the (BAP).
*
* - we should now have an age rated BAP.
*
* APPLY-RATE-PER-MILLE-LOADINGS
*
* - sum the rates per mille from the LEXT W/S table.
*
*  - add rates per mille to the BAP
*
* - we should now have a BAP with rates / mille applied.
*
* APPLY-VOLUME-DISCOUNT.
* Access the discount table T5659 using the key:-
*
* - Discount method from TT507 concatenated with currency.
*
*  - check the sum insurred against the volume band ranges
*  and when within a range store the discount amount.
*  - compute the BAP as the BAP - discount
*
* APPLY-PREMIUM-UNIT
*
* - Obtain the risk-unit from TT507
*
*  - multiply BAP by the sum-insured and divide
*    by the risk-unit
*
* - we should now have a BAP with premium applied.
*
* APPLY-MORTALITY-LOADINGS
* - Get the mortality loading from LEXT
* - Get the reason code & subroutine, smoker/non-smoker code
*   by reading TH549.
*
* - Read TH606 using concatenated key Coverage + TH549-indic
*
* - Compute WSAA-BAP = WSAA-BAP + EXTP-LOADING
*
* APPLY-PERCENTAGE-LOADINGS
*
* - from the LEXT working-storage (W/S) table apply the
* percentage loadings. For each loading entry on the table
* compute the BAP as follows:
*
*  BAP = BAP * loading percentage / 100.
*
* - we should now have a loaded BAP.
*
* CALCULATE-ROUNDING.
*
* - round up depending on the rounding factor (obtained from
* the T5659 table).
*
* - if the prem-unit from TT507 is greater than zero, then
* compute the BIP as the rounded number / the premium-unit
* (from TT507). The premium unit is the quantity in which the
* currency is denominated.
*
*****************************************************************
* </pre>
*/
public class Tprmpm6 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "TPRMPM6";
		/* ERRORS */
	private static final String e107 = "E107";
	private static final String f264 = "F264";
	private static final String tl09 = "TL09";
	private static final String hl26 = "HL26";
	private static final String hl27 = "HL27";
	private static final String f272 = "F272";
		/* TABLES */
	private static final String tt507 = "TT507";
	private static final String t5659 = "T5659";
	private static final String th606 = "TH606";
	private static final String th549 = "TH549";
	private static final String lextrec = "LEXTREC";

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);

		/* WSAA-LEXT-ZMORTPCT-RECS */
	private FixedLengthStringData[] wsaaLextZmortpcts = FLSInittedArray (8, 2);
	private PackedDecimalData[] wsaaLextZmortpct = PDArrayPartOfArrayStructure(3, 0, wsaaLextZmortpcts, 0, UNSIGNED_TRUE);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(13, 2);
	private String wsaaBasicPremium = "";
	private String wsaaMortalityLoad = "";
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(13, 2).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler, 8).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 9).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler4, 10).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler6, 11);

	private FixedLengthStringData wsaaTt507Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTt507Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTt507Key, 0);
	private FixedLengthStringData wsaaTt507Billfreq = new FixedLengthStringData(2).isAPartOf(wsaaTt507Key, 4);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);

	private FixedLengthStringData wsaaTh606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh606Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh606Key, 0);
	private FixedLengthStringData wsaaTh606Ageterm = new FixedLengthStringData(2).isAPartOf(wsaaTh606Key, 4);
	private FixedLengthStringData wsaaTh606Indic = new FixedLengthStringData(2).isAPartOf(wsaaTh606Key, 6);

	private FixedLengthStringData wsaaTh549Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh549Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh549Key, 0);
	private ZonedDecimalData wsaaTh549Zmortpct = new ZonedDecimalData(3, 0).isAPartOf(wsaaTh549Key, 4).setUnsigned();
	private FixedLengthStringData wsaaTh549Zsexmort = new FixedLengthStringData(1).isAPartOf(wsaaTh549Key, 7);
	private PackedDecimalData wsaaMortRate = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaMortFactor = new PackedDecimalData(5, 4);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(1, 0).setUnsigned();

		/* WSAA-LEXT-OPCDA-RECS */
	private FixedLengthStringData[] wsaaLextOpcdas = FLSInittedArray (8, 2);
	private FixedLengthStringData[] wsaaLextOpcda = FLSDArrayPartOfArrayStructure(2, wsaaLextOpcdas, 0);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Tt507rec tt507rec = new Tt507rec();
	private T5659rec t5659rec = new T5659rec();
	private Extprmrec extprmrec = new Extprmrec();
	private Th549rec th549rec = new Th549rec();
	private Th606rec th606rec = new Th606rec();
	private Premiumrec premiumrec = new Premiumrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		tt507120, 
		readLext210, 
		loopForAdjustedAge220, 
		checkTt507Insprm230, 
		calculateLoadings610, 
		exit890, 
		calcMortLoadings950, 
		callSubroutine980, 
		exit950, 
		a150GetModalFactor, 
		a150Exit
	}

	public Tprmpm6() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		premiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBasicPremium = "N";
		initialize100();
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			basicAnnualPremium200();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			ratesPerMillieLoadings300();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			/*     PERFORM 900-MORTALITY-LOADINGS.                   <V42010>*/
			mortalityLoadings950();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			percentageLoadings600();
		}
		if (isEQ(premiumrec.statuz,varcom.oK)) {
			rounding800();
		}
		wsaaBasicPremium = "Y";
		if (isEQ(premiumrec.statuz,"****")) {
			basicAnnualPremium200();
		}
		if (isEQ(premiumrec.statuz,"****")) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz,"****")) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz,"****")) {
			rounding800();
		}
		/* Calculate the loaded premium as the gross premium minus the*/
		/* basic premium.*/
		compute(premiumrec.calcLoaPrem, 2).set(sub(premiumrec.calcPrem,premiumrec.calcBasPrem));
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para100();
				case tt507120: 
					tt507120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		/* Initialise all working storage fields and set keys to read*/
		/* tables. Include a table (occurs 8) to hold the Options/Extras*/
		/* (LEXT) record details.*/
		wsaaRatesPerMillieTot.set(ZERO);
		wsaaBap.set(ZERO);
		wsaaAdjustedAge.set(ZERO);
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaT5659Key.set(SPACES);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			clearLextRecs110();
		}
		goTo(GotoLabel.tt507120);
	}

protected void clearLextRecs110()
	{
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
		wsaaLextZmortpct[wsaaSub.toInt()].set(ZERO);
		wsaaLextOpcda[wsaaSub.toInt()].set(SPACES);
	}

protected void tt507120()
	{
		/* Build a key.*/
		/* This key (see below)*/
		/* will read table TT507. This table contains the parameters to be*/
		/* used in the calculation of the Basic Annual Premium for*/
		/* Coverage/Rider components.*/
		/* The key is a concatenation of the following fields:-*/
		/* Coverage/Rider table code*/
		/* Bill Frequency*/
		/* Access the required table by reading the table directly (ITDM).*/
		/* The contents of the table are then stored. This table is dated*/
		/* use:*/
		/*  1) Rating Date*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(tt507);
		wsaaTt507Crtable.set(premiumrec.crtable);
		wsaaTt507Billfreq.set(premiumrec.billfreq);
		itdmIO.setItemitem(wsaaTt507Key);
		if (isEQ(premiumrec.ratingdate,ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaTt507Key,itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),tt507)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			premiumrec.statuz.set(tl09);
		}
		else {
			tt507rec.tt507Rec.set(itdmIO.getGenarea());
		}
	}

protected void basicAnnualPremium200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey200();
				case readLext210: 
					readLext210();
				case loopForAdjustedAge220: 
					loopForAdjustedAge220();
				case checkTt507Insprm230: 
					checkTt507Insprm230();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupLextKey200()
	{
		/* If calculating the basic premium, do not adjust the age.*/
		if (isEQ(wsaaBasicPremium,"Y")) {
			wsaaAdjustedAge.set(premiumrec.lage);
			goTo(GotoLabel.checkTt507Insprm230);
		}
		/* Obtain the age rates from the (LEXT) record.*/
		/*  - read all the LEXT records for this contract, life and*/
		/*  coverage into the working-storage table. Compute the*/
		/*  adjusted age as being the summation of the LEXT age*/
		/*  rates plus the ANB @ RCD.*/
		lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lextIO.setChdrnum(premiumrec.chdrChdrnum);
		lextIO.setLife(premiumrec.lifeLife);
		lextIO.setCoverage(premiumrec.covrCoverage);
		lextIO.setRider(premiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		wsaaSub.set(0);
	}

protected void readLext210()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError9000();
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		if (isEQ(lextIO.getChdrcoy(),premiumrec.chdrChdrcoy)
		&& isEQ(lextIO.getChdrnum(),premiumrec.chdrChdrnum)
		&& isEQ(lextIO.getLife(),premiumrec.lifeLife)
		&& isEQ(lextIO.getCoverage(),premiumrec.covrCoverage)
		&& isEQ(lextIO.getRider(),premiumrec.covrRider)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		if (isEQ(premiumrec.reasind,"2")
		&& isEQ(lextIO.getReasind(),"1")) {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		if (isNE(premiumrec.reasind,"2")
		&& isEQ(lextIO.getReasind(),"2")) {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		/*  Skip any expired special terms.*/
		lextIO.setFunction(varcom.nextr);
		if (isLTE(lextIO.getExtCessDate(),premiumrec.reRateDate)) {
			goTo(GotoLabel.readLext210);
		}
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(lextIO.getOppc());
		wsaaLextZmortpct[wsaaSub.toInt()].set(lextIO.getZmortpct());
		wsaaLextOpcda[wsaaSub.toInt()].set(lextIO.getOpcda());
		wsaaRatesPerMillieTot.add(lextIO.getInsprm());
		wsaaAgerateTot.add(lextIO.getAgerate());
		goTo(GotoLabel.readLext210);
	}

protected void loopForAdjustedAge220()
	{
		compute(wsaaAdjustedAge, 0).set((add(wsaaAgerateTot,premiumrec.lage)));
	}

protected void checkTt507Insprm230()
	{
		/* Use the age calculated above to access the table TT507 and*/
		/* check the following:*/
		/*  - that the basic annual premium (indexed by year) from*/
		/*  the TT507 table is not zero. If it is zero, then display*/
		/*  an error message and skip the additional procedures.*/
		/*  Otherwise store the premium as the (BAP).*/
		/*    IF WSAA-ADJUSTED-AGE        < 1                              */
		if (isLT(wsaaAdjustedAge,0)) {
			/*       MOVE 100                 TO WSAA-ADJUSTED-AGE             */
			wsaaAdjustedAge.set(110);
		}
		/*    IF WSAA-ADJUSTED-AGE        < 1 OR                           */
		if (isLT(wsaaAdjustedAge,0)
		|| isGT(wsaaAdjustedAge, 110)) {
			premiumrec.statuz.set(e107);
			return ;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (isEQ(wsaaAdjustedAge,0)) {
			if (isEQ(tt507rec.insprem,ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(tt507rec.insprem);
			}
			return ;
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.*/
		/*    IF WSAA-ADJUSTED-AGE = 100                                   */
		/*       IF TT507-INSTPR = ZERO                                    */
		/*          MOVE E107                TO CPRM-STATUZ                */
		/*       ELSE                                                      */
		/*          MOVE TT507-INSTPR        TO WSAA-BAP                   */
		/* Extend the age to 110                                           */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			compute(wsaaIndex, 0).set(sub(wsaaAdjustedAge, 99));
			if (isEQ(tt507rec.instpr[wsaaIndex.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(tt507rec.instpr[wsaaIndex.toInt()]);
			}
		}
		else {
			if (isEQ(tt507rec.insprm[wsaaAdjustedAge.toInt()],ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(tt507rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
	}

protected void ratesPerMillieLoadings300()
	{
		/*PARA*/
		/* APPLY-RATE-PER-MILLE-LOADINGS*/
		/* - sum the rates per mille from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/
		compute(wsaaBap, 2).set(add(wsaaRatesPerMillieTot,wsaaBap));
		/*EXIT*/
	}

protected void volumeDiscountBap1400()
	{
			readT5659410();
		}

protected void readT5659410()
	{
		/* APPLY-DISCOUNT.*/
		/* Access the discount table T5659 using the key:-*/
		/* - Discount method from TT507 concatenated with currency.*/
		/*  - check the sum insurred against the volume band ranges*/
		/*  and when within a range store the discount amount.*/
		/*  - compute the BAP as the BAP - discount*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5659);
		wsaaDisccntmeth.set(tt507rec.disccntmeth);
		wsaaCurrcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5659Key);
		if (isEQ(premiumrec.ratingdate,ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5659)
		|| isNE(wsaaT5659Key,itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			premiumrec.statuz.set(f264);
			return ;
		}
		t5659rec.t5659Rec.set(itdmIO.getGenarea());
	}

protected void premiumUnit500()
	{
		/*PARA*/
		/* APPLY-PREMIUM-UNIT*/
		/* - Obtain the risk-unit from TT507*/
		/*  - multiply BAP by the sum-insured and divide*/
		/*    by the risk-unit*/
		compute(wsaaBap, 2).set((div((mult(wsaaBap,premiumrec.sumin)),tt507rec.unit)));
		/*EXIT*/
	}

protected void percentageLoadings600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para600();
				case calculateLoadings610: 
					calculateLoadings610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para600()
	{
		/* APPLY-PERCENTAGE-LOADINGS*/
		/* - from the LEXT working-storage (W/S) table apply the*/
		/* percentage loadings. For each loading entry on the table*/
		/* compute the BAP as follows:*/
		/*  BAP = BAP * loading percentage / 100.*/
		wsaaSub.set(0);
	}

protected void calculateLoadings610()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,8)) {
			return ;
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()],0)) {
			compute(wsaaBap, 2).set((div((mult(wsaaBap,wsaaLextOppc[wsaaSub.toInt()])),100)));
		}
		goTo(GotoLabel.calculateLoadings610);
	}

protected void rounding800()
	{
		try {
			calculatePremiumUnit810();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void calculatePremiumUnit810()
	{
		if (isEQ(tt507rec.premUnit,0)) {
			if (isEQ(wsaaBasicPremium,"Y")) {
				premiumrec.calcBasPrem.set(wsaaBap);
			}
			else {
				premiumrec.calcPrem.set(wsaaBap);
			}
		}
		else {
			if (isEQ(wsaaBasicPremium,"Y")) {
				compute(premiumrec.calcBasPrem, 2).set((div(wsaaBap,tt507rec.premUnit)));
			}
			else {
				compute(premiumrec.calcPrem, 2).set((div(wsaaBap,tt507rec.premUnit)));
			}
		}
		if (isEQ(wsaaBasicPremium,"Y")) {
			wsaaRoundNum.set(premiumrec.calcBasPrem);
			roundUp820();
			premiumrec.calcBasPrem.set(wsaaRoundNum);
		}
		else {
			wsaaRoundNum.set(premiumrec.calcPrem);
			roundUp820();
			premiumrec.calcPrem.set(wsaaRoundNum);
		}
		goTo(GotoLabel.exit890);
	}

protected void roundUp820()
	{
		if (isEQ(t5659rec.rndfact,1)
		|| isEQ(t5659rec.rndfact,0)) {
			if (isLT(wsaaRoundDec,.5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact,10)) {
			if (isLT(wsaaRound1,5)) {
				wsaaRound1.set(0);
			}
			else {
				wsaaRoundNum.add(10);
				wsaaRound1.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact,100)) {
			if (isLT(wsaaRound10,50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact,1000)) {
			if (isLT(wsaaRound100,500)) {
				wsaaRound100.set(0);
			}
			else {
				wsaaRoundNum.add(1000);
				wsaaRound100.set(0);
			}
		}
	}

	/**
	* <pre>
	*900-MORTALITY-LOADINGS SECTION.                                  
	*900-PARA.                                                        
	*  First check if there is a need to perform this section at all  
	*    MOVE 'N'                    TO WSAA-MORTALITY-LOAD.          
	*    PERFORM VARYING WSAA-SUB    FROM 1 BY 1 UNTIL WSAA-SUB > 8   
	*                                OR WSAA-MORTALITY-LOAD = 'Y'     
	*       IF  WSAA-LEXT-ZMORTPCT (WSAA-SUB) NOT = 0                 
	*           MOVE 'Y'             TO WSAA-MORTALITY-LOAD           
	*       END-IF                                                    
	*    END-PERFORM.                                                 
	*    IF  WSAA-MORTALITY-LOAD     = 'N'                            
	*        GO TO 990-EXIT                                           
	*    END-IF.                                                      
	* APPLY-SEX/MORTALITY LOADINGS                                    
	* Read TH606 using concatenated key CPRM-LSEX + CPRM-MORTPCT      
	* to obtain the Sex/Mortality Class Indicator.                    
	* For each LEXT-ZMORTPCT kept in working-storage (w/s) table      
	* read TH549 using concatenated key CPRM-CRTABLE + w/s-ZMORTPCT   
	* using concatenated key CPRM-CRTABLE + LEXT-ZMORTPCT +           
	* TH606-ZSEXMORT to compute BAP as:                               
	*  BAP = BAP + TH549-rate * sum insured / risk unit / prem unit   
	*    MOVE SPACES                 TO ITEM-DATA-KEY.                
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 
	*    MOVE CPRM-CHDR-CHDRCOY      TO ITEM-ITEMCOY.                 
	*    MOVE TH606                  TO ITEM-ITEMTABL.                
	*    MOVE CPRM-LSEX              TO WSAA-TH606-SEX.               
	*    MOVE CPRM-MORTCLS           TO WSAA-TH606-MORTCLS.           
	*    MOVE WSAA-TH606-KEY         TO ITEM-ITEMITEM.                
	*    MOVE ITEMREC                TO ITEM-FORMAT.                  
	*    MOVE 'READR'                TO ITEM-FUNCTION.                
	*    CALL 'ITEMIO'               USING ITEM-PARAMS.               
	*    IF  ITEM-STATUZ             NOT = '****'                     
	*    AND ITEM-STATUZ             NOT = 'MRNF'                     
	*       MOVE ITEM-PARAMS         TO SYSR-PARAMS                   
	*       PERFORM 9000-FATAL-ERROR                                  
	*    END-IF.                                                      
	*    IF  ITEM-STATUZ             = 'MRNF'                         
	*        MOVE HL26               TO CPRM-STATUZ                   
	*        GO TO 990-EXIT                                           
	*    END-IF.                                                      
	*    MOVE ITEM-GENAREA           TO TH606-TH606-REC.              
	*    MOVE 0                      TO WSAA-SUB.                     
	*910-CALC-MORT-LOADINGS.                                          
	*    ADD 1                       TO WSAA-SUB.                     
	*    IF  WSAA-SUB                > 8                              
	*        GO TO 990-EXIT                                           
	*    END-IF.                                                      
	*    IF  WSAA-LEXT-ZMORTPCT (WSAA-SUB) = 0                        
	*        GO TO 910-CALC-MORT-LOADINGS                             
	*    END-IF.                                                      
	*    MOVE SPACES                 TO ITDM-DATA-KEY.                
	*    MOVE CPRM-CHDR-CHDRCOY      TO ITDM-ITEMCOY.                 
	*    MOVE TH549                  TO ITDM-ITEMTABL.                
	*    MOVE CPRM-CRTABLE           TO WSAA-TH549-CRTABLE.           
	*    MOVE WSAA-LEXT-ZMORTPCT (WSAA-SUB)                           
	*                                TO WSAA-TH549-ZMORTPCT.          
	*    MOVE TH606-ZSEXMORT         TO WSAA-TH549-ZSEXMORT.          
	*    MOVE WSAA-TH549-KEY         TO ITDM-ITEMITEM.                
	*    IF  CPRM-RATINGDATE         = ZEROS                          
	*        MOVE  99999999          TO ITDM-ITMFRM                   
	*    ELSE                                                         
	*        MOVE CPRM-RATINGDATE    TO ITDM-ITMFRM                   
	*    END-IF.                                                      
	*    MOVE 'BEGN'                 TO ITDM-FUNCTION.                
	*    CALL 'ITDMIO' USING         ITDM-PARAMS.                     
	*    IF ITDM-STATUZ              NOT = '****' AND                 
	*       ITDM-STATUZ              NOT = 'ENDP'                     
	*       MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	*       PERFORM 9000-FATAL-ERROR.                                 
	*    IF WSAA-TH549-KEY           NOT = ITDM-ITEMITEM              
	*     OR CPRM-CHDR-CHDRCOY       NOT = ITDM-ITEMCOY               
	*     OR ITDM-ITEMTABL           NOT = TH549                      
	*     OR ITDM-STATUZ             = 'ENDP'                         
	*       MOVE HL27                TO CPRM-STATUZ                   
	*       GO TO 990-EXIT                                            
	*    ELSE                                                         
	*       MOVE ITDM-GENAREA        TO T5658-T5658-REC.              
	*    COMPUTE WSAA-BAP =                                           
	*            WSAA-BAP + (((T5658-INSPRM (CPRM-LAGE) *             
	*            CPRM-SUMIN) / T5658-UNIT) *                          
	*                        (WSAA-LEXT-ZMORTPCT (WSAA-SUB) / 100)).  
	*    GO TO 910-CALC-MORT-LOADINGS.                                
	*990-EXIT.                                                        
	*     EXIT.                                                       
	* </pre>
	*/
protected void mortalityLoadings950()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para950();
				case calcMortLoadings950: 
					calcMortLoadings950();
					calcMortLoadings960();
				case callSubroutine980: 
					callSubroutine980();
				case exit950: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para950()
	{
		/*  First check if there is a need to perform this section at all  */
		/*  SINCE THIS IS BASE ON TERM TH606 TABLE MUST BE SETUP W/ TERM   */
		wsaaMortalityLoad = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,8)
		|| isEQ(wsaaMortalityLoad,"Y")); wsaaSub.add(1)){
			if (isNE(wsaaLextZmortpct[wsaaSub.toInt()],0)) {
				wsaaMortalityLoad = "Y";
			}
		}
		if (isEQ(wsaaMortalityLoad,"N")) {
			goTo(GotoLabel.exit950);
		}
		wsaaSub.set(0);
	}

protected void calcMortLoadings950()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,8)) {
			goTo(GotoLabel.exit950);
		}
		if (isEQ(wsaaLextZmortpct[wsaaSub.toInt()],0)) {
			goTo(GotoLabel.calcMortLoadings950);
		}
		/*  Read TH549 table to get Calculation routine for Flat Mortality */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(th549);
		wsaaTh549Crtable.set(premiumrec.crtable);
		wsaaTh549Zmortpct.set(wsaaLextZmortpct[wsaaSub.toInt()]);
		wsaaTh549Zsexmort.set(premiumrec.lsex);
		itdmIO.setItemitem(wsaaTh549Key);
		if (isEQ(premiumrec.ratingdate,ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(wsaaTh549Key,itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),th549)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			premiumrec.statuz.set(hl27);
			goTo(GotoLabel.exit950);
		}
		th549rec.th549Rec.set(itdmIO.getGenarea());
		wsaaTh606Key.set(SPACES);
		a100ReadLife();
		/* Read TH606 using concatenated key Coverage + TH549-indic        */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(th606);
		wsaaTh606Ageterm.set("00");
		wsaaTh606Crtable.set(premiumrec.crtable);
		itdmIO.setItemitem(wsaaTh606Key);
		if (isEQ(premiumrec.ratingdate,ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"MRNF")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaTh606Key,itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),th606)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			premiumrec.statuz.set(hl26);
			wsaaMortFactor.set(ZERO);
			wsaaMortRate.set(ZERO);
			goTo(GotoLabel.callSubroutine980);
		}
		else {
			th606rec.th606Rec.set(itdmIO.getGenarea());
		}
	}

protected void calcMortLoadings960()
	{
		if (isEQ(wsaaLextZmortpct[wsaaSub.toInt()],0)) {
			goTo(GotoLabel.calcMortLoadings950);
		}
		a150GetValues();
	}

protected void callSubroutine980()
	{
		extprmrec.freqFactor.set(wsaaMortFactor);
		extprmrec.mortRate.set(wsaaMortRate);
		extprmrec.premium.set(premiumrec.calcPrem);
		extprmrec.sumass.set(premiumrec.sumin);
		/* MOVE    1                   TO EXTP-EX-FACTOR        <V42010>*/
		extprmrec.exFactor.set(th549rec.expfactor);
		extprmrec.percent.set(wsaaLextZmortpct[wsaaSub.toInt()]);
		extprmrec.term.set(premiumrec.duration);
		extprmrec.freq.set(premiumrec.billfreq);
		extprmrec.loading.set(ZERO);
		extprmrec.statuz.set("****");
		for (wsaaCount.set(1); !(isGT(wsaaCount,5)
		|| isEQ(th549rec.opcda[wsaaCount.toInt()],SPACES)); wsaaCount.add(1)){
			if (isEQ(th549rec.opcda[wsaaCount.toInt()],wsaaLextOpcda[wsaaSub.toInt()])) {
				callProgram(th549rec.subrtn[wsaaCount.toInt()], extprmrec.parmRec);
				wsaaCount.set(6);
			}
			else {
				if (isEQ(th549rec.opcda[wsaaCount.toInt()],"**")) {
					callProgram(th549rec.subrtn[wsaaCount.toInt()], extprmrec.parmRec);
					wsaaCount.set(6);
				}
			}
		}
		compute(wsaaBap, 2).set(add(wsaaBap,extprmrec.loading));
		goTo(GotoLabel.calcMortLoadings950);
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a100ReadLife()
	{
		a100ReadLifePara();
	}

protected void a100ReadLifePara()
	{
		lifeIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lifeIO.setChdrnum(premiumrec.chdrChdrnum);
		lifeIO.setLife(premiumrec.lifeLife);
		lifeIO.setJlife(premiumrec.lifeJlife);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		lifeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError9000();
		}
		if (isNE(lifeIO.getChdrnum(),premiumrec.chdrChdrnum)
		&& isNE(lifeIO.getChdrcoy(),premiumrec.chdrChdrcoy)
		&& isNE(lifeIO.getLife(),premiumrec.lifeLife)
		&& isNE(lifeIO.getJlife(),premiumrec.lifeJlife)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError9000();
		}
		if (isEQ(lifeIO.getSmoking(),"S")) {
			wsaaTh606Indic.set(th549rec.indc01);
		}
		else {
			wsaaTh606Indic.set(th549rec.indc02);
		}
	}

protected void a150GetValues()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a150GetValuesPara();
				case a150GetModalFactor: 
					a150GetModalFactor();
				case a150Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a150GetValuesPara()
	{
		/*  Get the Mortality Rate                                         */
		/*    IF WSAA-ADJUSTED-AGE        < 1                     V42L018  */
		if (isLT(wsaaAdjustedAge,0)) {
			/*       MOVE 100                 TO WSAA-ADJUSTED-AGE     <V73L03>*/
			wsaaAdjustedAge.set(110);
		}
		/*    IF WSAA-ADJUSTED-AGE        < 1 OR                   V42L018 */
		if (isLT(wsaaAdjustedAge,0)
		|| isGT(wsaaAdjustedAge, 110)) {
			premiumrec.statuz.set(e107);
			/*       GO TO 290-EXIT.                                  <V42L018>*/
			goTo(GotoLabel.a150Exit);
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (isEQ(wsaaAdjustedAge,0)) {
			if (isEQ(th606rec.insprem,ZERO)) {
				premiumrec.statuz.set(e107);
				goTo(GotoLabel.a150Exit);
			}
			else {
				wsaaMortRate.set(th606rec.insprem);
				goTo(GotoLabel.a150GetModalFactor);
			}
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.   */
		/*    IF WSAA-ADJUSTED-AGE = 100                           <V73L03>*/
		/*       IF TH606-INSTPR = ZERO                            <V73L03>*/
		/*          MOVE E107                TO CPRM-STATUZ        <V73L03>*/
		/*       ELSE                                              <V73L03>*/
		/*          MOVE TH606-INSTPR        TO WSAA-MORT-RATE     <V73L03>*/
		/*       END-IF                                            <V73L03>*/
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			compute(wsaaIndex, 0).set(sub(wsaaAdjustedAge, 99));
			if (isEQ(th606rec.instpr[wsaaIndex.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaMortRate.set(th606rec.instpr[wsaaIndex.toInt()]);
			}
		}
		else {
			if (isEQ(th606rec.insprm[wsaaAdjustedAge.toInt()],ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaMortRate.set(th606rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
	}

protected void a150GetModalFactor()
	{
		/*  Get the Modal Factor                                           */
		wsaaMortFactor.set(0);
		if (isEQ(premiumrec.billfreq,"01")
		|| isEQ(premiumrec.billfreq,"00")) {
			wsaaMortFactor.set(th606rec.mfacty);
		}
		else {
			if (isEQ(premiumrec.billfreq,"02")) {
				wsaaMortFactor.set(th606rec.mfacthy);
			}
			else {
				if (isEQ(premiumrec.billfreq,"04")) {
					wsaaMortFactor.set(th606rec.mfactq);
				}
				else {
					if (isEQ(premiumrec.billfreq,"12")) {
						wsaaMortFactor.set(th606rec.mfactm);
					}
					else {
						if (isEQ(premiumrec.billfreq,"13")) {
							wsaaMortFactor.set(th606rec.mfact4w);
						}
						else {
							if (isEQ(premiumrec.billfreq,"24")) {
								wsaaMortFactor.set(th606rec.mfacthm);
							}
							else {
								if (isEQ(premiumrec.billfreq,"26")) {
									wsaaMortFactor.set(th606rec.mfact2w);
								}
								else {
									if (isEQ(premiumrec.billfreq,"52")) {
										wsaaMortFactor.set(th606rec.mfactw);
									}
								}
							}
						}
					}
				}
			}
		}
		if (isEQ(wsaaMortFactor,0)) {
			premiumrec.statuz.set(f272);
		}
	}
}
