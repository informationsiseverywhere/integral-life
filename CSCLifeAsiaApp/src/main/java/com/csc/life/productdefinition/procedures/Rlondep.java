/*
 * File: Rlondep.java
 * Date: 30 August 2009 2:13:11
 * Author: Quipoz Limited
 *
 * Class transformed from RLONDEP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Rldgkey;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.dataaccess.AcblclmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This subroutine is called from online Receipt, when sub-
*   account code is 'LN'. It checks whether the payment
*   is a Premium Deposit or a Loan Repayment/Withdrawal
*   and calls the respective routines.
*
*   Logic:
*
*   . Check the receipt (dissection) amount.
*
*   . If the amount is < 0, call LOANPYMT for Loan
*     Repayment
*
*   . If the amount is >= 0, call RLPDLON for Advanced
*     Premium Deposit
*
*****************************************************************
* </pre>
*/
public class Rlondep extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "RLONDEP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private String wsaaSkip = "N";
		/* FORMATS */
	private String itemrec = "ITEMREC   ";
		/* TABLES */
	private String t5645 = "T5645";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*Account Balance for Claims*/
	private AcblclmTableDAM acblclmIO = new AcblclmTableDAM();
	private Cashedrec cashedrec = new Cashedrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Varcom varcom = new Varcom();
	private Rldgkey wsaaRldgkey = new Rldgkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		exit99490,
		exit99590
	}

	public Rlondep() {
		super();
	}

public void mainline(Object... parmArray)
	{
		cashedrec.cashedRec = convertAndSetParam(cashedrec.cashedRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		start100();
		exit190();
	}

protected void start100()
	{
		initialise1000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		try {
			start1000();
		}
		catch (GOTOException e){
		}
	}

protected void start1000()
	{
		cashedrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		readT56457000();
		if (isEQ(t5645rec.sacstype11,cashedrec.sacstyp)
		|| isEQ(t5645rec.sacstype10,cashedrec.sacstyp)) {
			if (isEQ(t5645rec.sacstype10,cashedrec.sacstyp)
			&& isEQ(t5645rec.sign10,"-")) {
				compute(cashedrec.origamt, 2).set(mult(cashedrec.origamt,-1));
			}
			if (isEQ(t5645rec.sacstype11,cashedrec.sacstyp)
			&& isEQ(t5645rec.sign11,"-")) {
				compute(cashedrec.origamt, 2).set(mult(cashedrec.origamt,-1));
			}
			if (isLT(cashedrec.origamt,0)) {
				compute(cashedrec.origamt, 2).set(mult(cashedrec.origamt,-1));
				premiumDeposit12000();
				goTo(GotoLabel.exit1090);
			}
		}
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			syserrrec.params.set(cashedrec.cashedRec);
			databaseError99500();
		}
	}

protected void readT56457000()
	{
		start7000();
	}

protected void start7000()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(cashedrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void premiumDeposit12000()
	{
		start12010();
	}

protected void start12010()
	{
		initialize(rlpdlonrec.rec);
		rlpdlonrec.function.set(varcom.insr);
		rlpdlonrec.prmdepst.set(cashedrec.origamt);
		rlpdlonrec.pstw.set("PSLN");
		wsaaRldgkey.rldgKey.set(cashedrec.trankey);
		rlpdlonrec.chdrcoy.set(wsaaRldgkey.rldgRldgcoy);
		rlpdlonrec.chdrnum.set(wsaaRldgkey.rldgRldgacct);
		rlpdlonrec.effdate.set(cashedrec.trandate);
		rlpdlonrec.currency.set(cashedrec.origccy);
		if (isEQ(cashedrec.function,"UPDAT")) {
			rlpdlonrec.tranno.set(ZERO);
		}
		else {
			rlpdlonrec.tranno.set(cashedrec.tranno);
		}
		rlpdlonrec.transeq.set(cashedrec.transeq);
		rlpdlonrec.longdesc.set(cashedrec.trandesc);
		rlpdlonrec.tranid.set(cashedrec.tranid);
		rlpdlonrec.batchkey.set(cashedrec.batchkey);
		rlpdlonrec.authCode.set(rlpdlonrec.trcde);
		rlpdlonrec.language.set(cashedrec.language);
		rlpdlonrec.validate.set("N");
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rlpdlonrec.statuz);
			syserrrec.params.set(rlpdlonrec.rec);
			databaseError99500();
		}
		cashedrec.transeq.set(rlpdlonrec.transeq);
	}

protected void systemError99000()
	{
		try {
			start99000();
		}
		catch (GOTOException e){
		}
		finally{
			exit99490();
		}
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		cashedrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		try {
			start99500();
		}
		catch (GOTOException e){
		}
		finally{
			exit99590();
		}
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		cashedrec.statuz.set(varcom.bomb);
		exit190();
	}
}
