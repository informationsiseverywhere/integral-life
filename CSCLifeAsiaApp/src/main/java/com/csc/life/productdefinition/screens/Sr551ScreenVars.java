package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR551
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr551ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(641);
	public FixedLengthStringData dataFields = new FixedLengthStringData(289).isAPartOf(dataArea, 0);
	public FixedLengthStringData actn = DD.actn.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData descrips = new FixedLengthStringData(90).isAPartOf(dataFields, 51);
	public FixedLengthStringData[] descrip = FLSArrayPartOfStructure(3, 30, descrips, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(90).isAPartOf(descrips, 0, FILLER_REDEFINE);
	public FixedLengthStringData descrip01 = DD.descrip.copy().isAPartOf(filler,0);
	public FixedLengthStringData descrip02 = DD.descrip.copy().isAPartOf(filler,30);
	public FixedLengthStringData descrip03 = DD.descrip.copy().isAPartOf(filler,60);
	public FixedLengthStringData desi = DD.desi.copy().isAPartOf(dataFields,141);
	public ZonedDecimalData dob = DD.dob.copyToZonedDecimal().isAPartOf(dataFields,171);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,179);
	public ZonedDecimalData hpropdte = DD.hpropdte.copyToZonedDecimal().isAPartOf(dataFields,187);
	public FixedLengthStringData indc = DD.indc.copy().isAPartOf(dataFields,195);
	public FixedLengthStringData mind = DD.mind.copy().isAPartOf(dataFields,197);
	public FixedLengthStringData mlcoycde = DD.mlcoycde.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData mlentity = DD.mlentity.copy().isAPartOf(dataFields,201);
	public ZonedDecimalData mlhsperd = DD.mlhsperd.copyToZonedDecimal().isAPartOf(dataFields,217);
	public FixedLengthStringData mlmedcdes = new FixedLengthStringData(12).isAPartOf(dataFields, 220);
	public FixedLengthStringData[] mlmedcde = FLSArrayPartOfStructure(3, 4, mlmedcdes, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(mlmedcdes, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlmedcde01 = DD.mlmedcde.copy().isAPartOf(filler1,0);
	public FixedLengthStringData mlmedcde02 = DD.mlmedcde.copy().isAPartOf(filler1,4);
	public FixedLengthStringData mlmedcde03 = DD.mlmedcde.copy().isAPartOf(filler1,8);
	public FixedLengthStringData mloldic = DD.mloldic.copy().isAPartOf(dataFields,232);
	public FixedLengthStringData mlothic = DD.mlothic.copy().isAPartOf(dataFields,248);
	public FixedLengthStringData reptname = DD.reptname.copy().isAPartOf(dataFields,264);
	public FixedLengthStringData rskflg = DD.rskflg.copy().isAPartOf(dataFields,272);
	public FixedLengthStringData securityno = DD.securityno.copy().isAPartOf(dataFields,274);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(88).isAPartOf(dataArea, 289);
	public FixedLengthStringData actnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData descripsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] descripErr = FLSArrayPartOfStructure(3, 4, descripsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(descripsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData descrip01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData descrip02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData descrip03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData desiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData dobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData hpropdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData indcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData mindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData mlcoycdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData mlentityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mlhsperdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mlmedcdesErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] mlmedcdeErr = FLSArrayPartOfStructure(3, 4, mlmedcdesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(mlmedcdesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlmedcde01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData mlmedcde02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData mlmedcde03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData mloldicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData mlothicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData reptnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData rskflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData securitynoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 377);
	public FixedLengthStringData[] actnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData descripsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(3, 12, descripsOut, 0);
	public FixedLengthStringData[][] descripO = FLSDArrayPartOfArrayStructure(12, 1, descripOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(descripsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] descrip01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] descrip02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] descrip03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] desiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] dobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] hpropdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] indcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] mindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] mlcoycdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] mlentityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mlhsperdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData mlmedcdesOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] mlmedcdeOut = FLSArrayPartOfStructure(3, 12, mlmedcdesOut, 0);
	public FixedLengthStringData[][] mlmedcdeO = FLSDArrayPartOfArrayStructure(12, 1, mlmedcdeOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(mlmedcdesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlmedcde01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] mlmedcde02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] mlmedcde03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] mloldicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] mlothicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] reptnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] rskflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] securitynoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dobDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hpropdteDisp = new FixedLengthStringData(10);

	public LongData Sr551screenWritten = new LongData(0);
	public LongData Sr551protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr551ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(mloldicOut,new String[] {"02","88","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlothicOut,new String[] {"03","88","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rskflgOut,new String[] {"05","88","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlhsperdOut,new String[] {"06","88","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlmedcde01Out,new String[] {"07","88","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlmedcde02Out,new String[] {"08","88","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlmedcde03Out,new String[] {"09","88","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"07","88","-07",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {reptname, securityno, mloldic, mlothic, mlentity, hpropdte, clntname, dob, mlcoycde, desi, rskflg, mlhsperd, mlmedcde01, descrip01, mlmedcde02, descrip02, mlmedcde03, descrip03, effdate, indc, mind, actn};
		screenOutFields = new BaseData[][] {reptnameOut, securitynoOut, mloldicOut, mlothicOut, mlentityOut, hpropdteOut, clntnameOut, dobOut, mlcoycdeOut, desiOut, rskflgOut, mlhsperdOut, mlmedcde01Out, descrip01Out, mlmedcde02Out, descrip02Out, mlmedcde03Out, descrip03Out, effdateOut, indcOut, mindOut, actnOut};
		screenErrFields = new BaseData[] {reptnameErr, securitynoErr, mloldicErr, mlothicErr, mlentityErr, hpropdteErr, clntnameErr, dobErr, mlcoycdeErr, desiErr, rskflgErr, mlhsperdErr, mlmedcde01Err, descrip01Err, mlmedcde02Err, descrip02Err, mlmedcde03Err, descrip03Err, effdateErr, indcErr, mindErr, actnErr};
		screenDateFields = new BaseData[] {hpropdte, dob, effdate};
		screenDateErrFields = new BaseData[] {hpropdteErr, dobErr, effdateErr};
		screenDateDispFields = new BaseData[] {hpropdteDisp, dobDisp, effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr551screen.class;
		protectRecord = Sr551protect.class;
	}

}
