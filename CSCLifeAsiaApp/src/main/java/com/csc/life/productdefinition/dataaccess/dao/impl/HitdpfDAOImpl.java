package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.HitdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Hitdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HitdpfDAOImpl extends BaseDAOImpl<Hitdpf> implements HitdpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(HitdpfDAOImpl.class);

    public Map<Long, List<Hitdpf>> searchHitdRecord(List<Long> covrUQ) {
        StringBuilder sqlHitdSelect1 = new StringBuilder(
                "SELECT HD.UNIQUE_NUMBER,HD.VALIDFLAG,HD.ZINTBFND,HD.ZLSTFNDVAL,HD.ZLSTINTDTE,HD.ZNXTINTDTE,HD.EFFDATE, C.UNIQUE_NUMBER ");
        sqlHitdSelect1
                .append("FROM HITDPF HD,COVRPF C WHERE HD.CHDRCOY=C.CHDRCOY AND HD.CHDRNUM = C.CHDRNUM AND HD.LIFE=C.LIFE AND HD.COVERAGE=C.COVERAGE AND HD.RIDER=C.RIDER AND HD.PLNSFX=C.PLNSFX AND ");
        sqlHitdSelect1.append(getSqlInLong("C.UNIQUE_NUMBER", covrUQ));

        sqlHitdSelect1
                .append(" ORDER BY HD.CHDRCOY ASC, HD.CHDRNUM ASC, HD.LIFE ASC, HD.COVERAGE ASC, HD.RIDER ASC, HD.PLNSFX ASC, HD.ZINTBFND ASC, HD.UNIQUE_NUMBER DESC ");

        PreparedStatement psHitdSelect = getPrepareStatement(sqlHitdSelect1.toString());
        ResultSet sqlhitdpf1rs = null;
        Map<Long, List<Hitdpf>> hitdpfSearchResult = new HashMap<>();
        try {
            sqlhitdpf1rs = executeQuery(psHitdSelect);
            while (sqlhitdpf1rs.next()) {
                Hitdpf hitdpf = new Hitdpf();
                hitdpf.setUniqueNumber(sqlhitdpf1rs.getLong(1));
                hitdpf.setValidflag(sqlhitdpf1rs.getString(2));
                hitdpf.setZintbfnd(sqlhitdpf1rs.getString(3));
                hitdpf.setZlstfndval(sqlhitdpf1rs.getBigDecimal(4));
                hitdpf.setZlstintdte(sqlhitdpf1rs.getInt(5));
                hitdpf.setZnxtintdte(sqlhitdpf1rs.getInt(6));
                hitdpf.setEffdate(sqlhitdpf1rs.getInt(7));
                long uniqueNumber = sqlhitdpf1rs.getLong(8);
                if (hitdpfSearchResult.containsKey(uniqueNumber)) {
                    hitdpfSearchResult.get(uniqueNumber).add(hitdpf);
                } else {
                    List<Hitdpf> hitdpfList = new ArrayList<>();
                    hitdpfList.add(hitdpf);
                    hitdpfSearchResult.put(uniqueNumber, hitdpfList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchHitdRecord()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(psHitdSelect, sqlhitdpf1rs);
        }
        return hitdpfSearchResult;

    }

	public Map<String, List<Hitdpf>> searchHitdRecordByChdrnum(List<String> chdrnumList) {

		StringBuilder sqlHitdSelect1 = new StringBuilder(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,ZINTBFND,TRANNO,EFFDATE,VALIDFLAG,ZLSTINTDTE,ZNXTINTDTE,ZLSTFNDVAL,ZLSTSMTDT,ZLSTSMTNO,ZLSTSMTBAL ");
		sqlHitdSelect1.append("FROM HITDPF WHERE ");
		sqlHitdSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlHitdSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitdSelect = getPrepareStatement(sqlHitdSelect1.toString());
		ResultSet sqlhitdpf1rs = null;
		Map<String, List<Hitdpf>> hitdpfSearchResult = new HashMap<>();
		try {
			sqlhitdpf1rs = executeQuery(psHitdSelect);
			while (sqlhitdpf1rs.next()) {
				Hitdpf hitdpf = new Hitdpf();
				hitdpf.setUniqueNumber(sqlhitdpf1rs.getLong("UNIQUE_NUMBER"));
				hitdpf.setChdrcoy(sqlhitdpf1rs.getString("CHDRCOY"));
				hitdpf.setChdrnum(sqlhitdpf1rs.getString("CHDRNUM"));
				hitdpf.setLife(sqlhitdpf1rs.getString("LIFE"));
				hitdpf.setCoverage(sqlhitdpf1rs.getString("COVERAGE"));
				hitdpf.setRider(sqlhitdpf1rs.getString("RIDER"));
				hitdpf.setPlanSuffix(sqlhitdpf1rs.getInt("PLNSFX"));
				hitdpf.setZintbfnd(sqlhitdpf1rs.getString("ZINTBFND"));
				hitdpf.setTranno(sqlhitdpf1rs.getInt("TRANNO"));
				hitdpf.setEffdate(sqlhitdpf1rs.getInt("EFFDATE"));
				hitdpf.setValidflag(sqlhitdpf1rs.getString("VALIDFLAG"));
				hitdpf.setZlstintdte(sqlhitdpf1rs.getInt("ZLSTINTDTE"));
				hitdpf.setZnxtintdte(sqlhitdpf1rs.getInt("ZNXTINTDTE"));
				hitdpf.setZlstfndval(sqlhitdpf1rs.getBigDecimal("ZLSTFNDVAL"));
				hitdpf.setZlstsmtdt(sqlhitdpf1rs.getInt("ZLSTSMTDT"));
				hitdpf.setZlstsmtno(sqlhitdpf1rs.getInt("ZLSTSMTNO"));
				hitdpf.setZlstsmtbal(sqlhitdpf1rs.getBigDecimal("ZLSTSMTBAL"));
				if (hitdpfSearchResult.containsKey(hitdpf.getChdrnum())) {
					hitdpfSearchResult.get(hitdpf.getChdrnum()).add(hitdpf);
				} else {
					List<Hitdpf> hitdpfList = new ArrayList<>();
					hitdpfList.add(hitdpf);
					hitdpfSearchResult.put(hitdpf.getChdrnum(), hitdpfList);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchHitdRecordByChdrnum()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitdSelect, sqlhitdpf1rs);
		}
		return hitdpfSearchResult;
	}
	
    public void updatedHitdRecord(List<Hitdpf> hitdBulkOpList) {
        if (hitdBulkOpList != null && !hitdBulkOpList.isEmpty()) {
            String SQL_HITD_UPDATE = "UPDATE HITDPF SET ZNXTINTDTE=?,ZLSTFNDVAL=?,ZLSTINTDTE=?,TRANNO=?,EFFDATE=?,VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=?";

            PreparedStatement psHitdUpdate = getPrepareStatement(SQL_HITD_UPDATE);
            try {
                for (Hitdpf hitdIO : hitdBulkOpList) {
                	psHitdUpdate.setInt(1, hitdIO.getZnxtintdte());
                	psHitdUpdate.setBigDecimal(2, hitdIO.getZlstfndval());
                	psHitdUpdate.setInt(3, hitdIO.getZlstintdte());
                	psHitdUpdate.setInt(4, hitdIO.getTranno());
                	psHitdUpdate.setInt(5, hitdIO.getEffdate());
                	psHitdUpdate.setString(6, hitdIO.getValidflag());
                    psHitdUpdate.setString(7, getJobnm());
                    psHitdUpdate.setString(8, getUsrprf());
                    psHitdUpdate.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
                    psHitdUpdate.setLong(10, hitdIO.getUniqueNumber());					
                    psHitdUpdate.addBatch();
                }
                psHitdUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updatedHitdRecord()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psHitdUpdate, null);
            }
        }
    }
    
    public void updatedHitdInvalidRecord(List<Hitdpf> hitdBulkOpList) {
        if (hitdBulkOpList != null && !hitdBulkOpList.isEmpty()) {
            String SQL_HITD_UPDATE = "UPDATE HITDPF SET VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=?";

            PreparedStatement psHitdUpdate = getPrepareStatement(SQL_HITD_UPDATE);
            try {
                for (Hitdpf hitdIO : hitdBulkOpList) {
                	psHitdUpdate.setString(1, hitdIO.getValidflag());
                    psHitdUpdate.setString(2, getJobnm());
                    psHitdUpdate.setString(3, getUsrprf());
                    psHitdUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                    psHitdUpdate.setLong(5, hitdIO.getUniqueNumber());					
                    psHitdUpdate.addBatch();
                }
                psHitdUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updatedHitdInvalidRecord()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psHitdUpdate, null);
            }
        }
    }
    
    public void insertHitdRecord(List<Hitdpf> hitdBulkOpList){

        if (hitdBulkOpList != null && !hitdBulkOpList.isEmpty()) {
           
            String SQL_HITD_INSERT = "INSERT INTO HITDPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,ZINTBFND,TRANNO,EFFDATE,VALIDFLAG,ZLSTINTDTE,ZNXTINTDTE,ZLSTFNDVAL,ZLSTSMTDT,ZLSTSMTNO,ZLSTSMTBAL,USRPRF,JOBNM,DATIME) SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,ZINTBFND,?,?,?,?,?,?,ZLSTSMTDT,ZLSTSMTNO,ZLSTSMTBAL,?,?,? FROM HITDPF WHERE UNIQUE_NUMBER=?";
       
            PreparedStatement psHitdInsert = getPrepareStatement(SQL_HITD_INSERT);
            try {
                for (Hitdpf hitdIO : hitdBulkOpList) {
                	
                    psHitdInsert.setInt(1, hitdIO.getTranno());
                    psHitdInsert.setInt(2, hitdIO.getEffdate());
                    psHitdInsert.setString(3, hitdIO.getValidflag());
                    psHitdInsert.setInt(4, hitdIO.getZlstintdte());
                    psHitdInsert.setInt(5, hitdIO.getZnxtintdte());
                    psHitdInsert.setBigDecimal(6, hitdIO.getZlstfndval());
                    // JOBNM,USRPRF,DATIME
                    psHitdInsert.setString(7, getUsrprf());
                    psHitdInsert.setString(8, getJobnm());
                    psHitdInsert.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
					psHitdInsert.setLong(10, hitdIO.getUniqueNumber());					
                    psHitdInsert.addBatch();
                }
                psHitdInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertHitdRecord()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psHitdInsert, null);
            }
        }
    
    }
    
	public List<Hitdpf> searchHitdrevRecordForDel(String chdrcoy, String chdrnum) {
		String sqlHitdSelect1 = "SELECT UNIQUE_NUMBER FROM HITDREV WHERE CHDRCOY=? AND CHDRNUM=? ";

		PreparedStatement psHitdSelect = getPrepareStatement(sqlHitdSelect1);
		ResultSet sqlHitdpf1rs = null;
		List<Hitdpf> HitdpfList = new ArrayList<>();
		try {
			psHitdSelect.setString(1, chdrcoy);
			psHitdSelect.setString(2, chdrnum);
			sqlHitdpf1rs = executeQuery(psHitdSelect);
			while (sqlHitdpf1rs.next()) {
				Hitdpf Hitdpf = new Hitdpf();
				Hitdpf.setUniqueNumber(sqlHitdpf1rs.getLong("UNIQUE_NUMBER"));
				HitdpfList.add(Hitdpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchHitdRecordForDel()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitdSelect, sqlHitdpf1rs);
		}
		return HitdpfList;
	}
	
	public void deleteHitdpfRecord(List<Hitdpf> HitdpfList) {
		String sql = "DELETE FROM HitdPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Hitdpf u:HitdpfList){
				ps.setLong(1, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteHitdpfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public List<Hitdpf> searchHitddryRecord(String chdrcoy, String chdrnum) {
		String sqlHitdSelect1 = "SELECT ZNXTINTDTE FROM HITDDRY WHERE CHDRCOY=? and CHDRNUM=? order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, UNIQUE_NUMBER DESC";
		PreparedStatement psHitdSelect = getPrepareStatement(sqlHitdSelect1);
		ResultSet sqlHitdpf1rs = null;
		List<Hitdpf> HitdpfList = new ArrayList<>();
		try {
			psHitdSelect.setString(1, chdrcoy);
			psHitdSelect.setString(2, chdrnum);
			sqlHitdpf1rs = executeQuery(psHitdSelect);
			while (sqlHitdpf1rs.next()) {
				Hitdpf Hitdpf = new Hitdpf();
				Hitdpf.setZnxtintdte(sqlHitdpf1rs.getInt("ZNXTINTDTE"));
				HitdpfList.add(Hitdpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchHitddryRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitdSelect, sqlHitdpf1rs);
		}
		return HitdpfList;
	}
}