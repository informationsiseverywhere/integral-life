package com.csc.life.productdefinition.dataaccess.dao;



import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MrtapfDAO extends BaseDAO<Mrtapf> {
	public Mrtapf getMrtaRecord(String chdrcoy, String chdrnum);
	public boolean updateMrtaRecord(Mrtapf mrta);
	public boolean insertMrtapfList(Mrtapf mrta);
}
