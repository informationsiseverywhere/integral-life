/*
 * File: T5645pt.java
 * Date: 30 August 2009 2:24:32
 * Author: Quipoz Limited
 * 
 * Class transformed from T5645PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5645.
*
*
*****************************************************************
* </pre>
*/
public class T5645pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Financial Transaction Accounting Rules          S5645");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(77);
	private FixedLengthStringData filler7 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(67).isAPartOf(wsaaPrtLine003, 10, FILLER).init("Subsidiary Ledger      General Ledger       DR/CR     Control Total");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(73);
	private FixedLengthStringData filler9 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(61).isAPartOf(wsaaPrtLine004, 12, FILLER).init("Code   Type      Account map         (+/-)        (1-12)");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(71);
	private FixedLengthStringData filler11 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 6, FILLER).init("1");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 13);
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 20);
	private FixedLengthStringData filler14 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine005, 33);
	private FixedLengthStringData filler15 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 56);
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(71);
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 6, FILLER).init("2");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 13);
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 20);
	private FixedLengthStringData filler20 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler21 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 56);
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(71);
	private FixedLengthStringData filler23 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 6, FILLER).init("3");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 13);
	private FixedLengthStringData filler25 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 20);
	private FixedLengthStringData filler26 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler27 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 56);
	private FixedLengthStringData filler28 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(71);
	private FixedLengthStringData filler29 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 6, FILLER).init("4");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 13);
	private FixedLengthStringData filler31 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 20);
	private FixedLengthStringData filler32 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler33 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 56);
	private FixedLengthStringData filler34 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(71);
	private FixedLengthStringData filler35 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler36 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 6, FILLER).init("5");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 13);
	private FixedLengthStringData filler37 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 20);
	private FixedLengthStringData filler38 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler39 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 56);
	private FixedLengthStringData filler40 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(71);
	private FixedLengthStringData filler41 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler42 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 6, FILLER).init("6");
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 13);
	private FixedLengthStringData filler43 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 20);
	private FixedLengthStringData filler44 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler45 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 56);
	private FixedLengthStringData filler46 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(71);
	private FixedLengthStringData filler47 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler48 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 6, FILLER).init("7");
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 13);
	private FixedLengthStringData filler49 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 20);
	private FixedLengthStringData filler50 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine011, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler51 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 56);
	private FixedLengthStringData filler52 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(71);
	private FixedLengthStringData filler53 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler54 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 6, FILLER).init("8");
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 13);
	private FixedLengthStringData filler55 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 20);
	private FixedLengthStringData filler56 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 33);
	private FixedLengthStringData filler57 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 56);
	private FixedLengthStringData filler58 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(71);
	private FixedLengthStringData filler59 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler60 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 6, FILLER).init("9");
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 13);
	private FixedLengthStringData filler61 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 20);
	private FixedLengthStringData filler62 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine013, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 33);
	private FixedLengthStringData filler63 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 56);
	private FixedLengthStringData filler64 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(71);
	private FixedLengthStringData filler65 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler66 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 5, FILLER).init("10");
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 13);
	private FixedLengthStringData filler67 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 20);
	private FixedLengthStringData filler68 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine014, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine014, 33);
	private FixedLengthStringData filler69 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 56);
	private FixedLengthStringData filler70 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(71);
	private FixedLengthStringData filler71 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler72 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 5, FILLER).init("11");
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 13);
	private FixedLengthStringData filler73 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 20);
	private FixedLengthStringData filler74 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine015, 33);
	private FixedLengthStringData filler75 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 56);
	private FixedLengthStringData filler76 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(71);
	private FixedLengthStringData filler77 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler78 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 5, FILLER).init("12");
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 13);
	private FixedLengthStringData filler79 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 20);
	private FixedLengthStringData filler80 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine016, 33);
	private FixedLengthStringData filler81 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 56);
	private FixedLengthStringData filler82 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(71);
	private FixedLengthStringData filler83 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler84 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 5, FILLER).init("13");
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 13);
	private FixedLengthStringData filler85 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 20);
	private FixedLengthStringData filler86 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine017, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine017, 33);
	private FixedLengthStringData filler87 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine017, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 56);
	private FixedLengthStringData filler88 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine017, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(71);
	private FixedLengthStringData filler89 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler90 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine018, 5, FILLER).init("14");
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 13);
	private FixedLengthStringData filler91 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine018, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 20);
	private FixedLengthStringData filler92 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine018, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine018, 33);
	private FixedLengthStringData filler93 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine018, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 56);
	private FixedLengthStringData filler94 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine018, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine018, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(71);
	private FixedLengthStringData filler95 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine019, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler96 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine019, 5, FILLER).init("15");
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine019, 13);
	private FixedLengthStringData filler97 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine019, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine019, 20);
	private FixedLengthStringData filler98 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine019, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine019, 33);
	private FixedLengthStringData filler99 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine019, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 56);
	private FixedLengthStringData filler100 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine019, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine019, 69).setPattern("ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5645rec t5645rec = new T5645rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5645pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5645rec.t5645Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t5645rec.sacscode01);
		fieldNo006.set(t5645rec.sacstype01);
		fieldNo007.set(t5645rec.glmap01);
		fieldNo008.set(t5645rec.sign01);
		fieldNo009.set(t5645rec.cnttot01);
		fieldNo010.set(t5645rec.sacscode02);
		fieldNo011.set(t5645rec.sacstype02);
		fieldNo012.set(t5645rec.glmap02);
		fieldNo013.set(t5645rec.sign02);
		fieldNo014.set(t5645rec.cnttot02);
		fieldNo015.set(t5645rec.sacscode03);
		fieldNo016.set(t5645rec.sacstype03);
		fieldNo017.set(t5645rec.glmap03);
		fieldNo018.set(t5645rec.sign03);
		fieldNo019.set(t5645rec.cnttot03);
		fieldNo020.set(t5645rec.sacscode04);
		fieldNo021.set(t5645rec.sacstype04);
		fieldNo022.set(t5645rec.glmap04);
		fieldNo023.set(t5645rec.sign04);
		fieldNo024.set(t5645rec.cnttot04);
		fieldNo025.set(t5645rec.sacscode05);
		fieldNo026.set(t5645rec.sacstype05);
		fieldNo027.set(t5645rec.glmap05);
		fieldNo028.set(t5645rec.sign05);
		fieldNo029.set(t5645rec.cnttot05);
		fieldNo030.set(t5645rec.sacscode06);
		fieldNo031.set(t5645rec.sacstype06);
		fieldNo032.set(t5645rec.glmap06);
		fieldNo033.set(t5645rec.sign06);
		fieldNo034.set(t5645rec.cnttot06);
		fieldNo035.set(t5645rec.sacscode07);
		fieldNo036.set(t5645rec.sacstype07);
		fieldNo037.set(t5645rec.glmap07);
		fieldNo038.set(t5645rec.sign07);
		fieldNo039.set(t5645rec.cnttot07);
		fieldNo040.set(t5645rec.sacscode08);
		fieldNo041.set(t5645rec.sacstype08);
		fieldNo042.set(t5645rec.glmap08);
		fieldNo043.set(t5645rec.sign08);
		fieldNo044.set(t5645rec.cnttot08);
		fieldNo045.set(t5645rec.sacscode09);
		fieldNo046.set(t5645rec.sacstype09);
		fieldNo047.set(t5645rec.glmap09);
		fieldNo048.set(t5645rec.sign09);
		fieldNo049.set(t5645rec.cnttot09);
		fieldNo050.set(t5645rec.sacscode10);
		fieldNo051.set(t5645rec.sacstype10);
		fieldNo052.set(t5645rec.glmap10);
		fieldNo053.set(t5645rec.sign10);
		fieldNo054.set(t5645rec.cnttot10);
		fieldNo055.set(t5645rec.sacscode11);
		fieldNo056.set(t5645rec.sacstype11);
		fieldNo057.set(t5645rec.glmap11);
		fieldNo058.set(t5645rec.sign11);
		fieldNo059.set(t5645rec.cnttot11);
		fieldNo060.set(t5645rec.sacscode12);
		fieldNo061.set(t5645rec.sacstype12);
		fieldNo062.set(t5645rec.glmap12);
		fieldNo063.set(t5645rec.sign12);
		fieldNo064.set(t5645rec.cnttot12);
		fieldNo065.set(t5645rec.sacscode13);
		fieldNo066.set(t5645rec.sacstype13);
		fieldNo067.set(t5645rec.glmap13);
		fieldNo068.set(t5645rec.sign13);
		fieldNo069.set(t5645rec.cnttot13);
		fieldNo070.set(t5645rec.sacscode14);
		fieldNo071.set(t5645rec.sacstype14);
		fieldNo072.set(t5645rec.glmap14);
		fieldNo073.set(t5645rec.sign14);
		fieldNo074.set(t5645rec.cnttot14);
		fieldNo075.set(t5645rec.sacscode15);
		fieldNo076.set(t5645rec.sacstype15);
		fieldNo077.set(t5645rec.glmap15);
		fieldNo078.set(t5645rec.sign15);
		fieldNo079.set(t5645rec.cnttot15);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine019);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
