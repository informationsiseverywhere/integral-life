package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class St502screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St502ScreenVars sv = (St502ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St502screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St502ScreenVars screenVars = (St502ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.riskunit.setClassString("");
		screenVars.mortcls01.setClassString("");
		screenVars.insprm01.setClassString("");
		screenVars.instpr01.setClassString("");
		screenVars.mortcls02.setClassString("");
		screenVars.insprm02.setClassString("");
		screenVars.instpr02.setClassString("");
		screenVars.mortcls03.setClassString("");
		screenVars.insprm03.setClassString("");
		screenVars.instpr03.setClassString("");
		screenVars.mortcls04.setClassString("");
		screenVars.insprm04.setClassString("");
		screenVars.instpr04.setClassString("");
		screenVars.mortcls05.setClassString("");
		screenVars.insprm05.setClassString("");
		screenVars.instpr05.setClassString("");
		screenVars.mortcls06.setClassString("");
		screenVars.insprm06.setClassString("");
		screenVars.instpr06.setClassString("");
		screenVars.mortcls07.setClassString("");
		screenVars.insprm07.setClassString("");
		screenVars.instpr07.setClassString("");
		screenVars.mortality1.setClassString("");
		screenVars.mortality2.setClassString("");
		screenVars.mortality3.setClassString("");
		screenVars.mortality4.setClassString("");
		screenVars.mortality5.setClassString("");
		screenVars.mortality6.setClassString("");
		screenVars.mortality7.setClassString("");
		screenVars.benefitMin01.setClassString("");
		screenVars.benefitMax01.setClassString("");
		screenVars.benefitMin02.setClassString("");
		screenVars.benefitMax02.setClassString("");
		screenVars.premUnit.setClassString("");
		screenVars.disccntmeth.setClassString("");
	}

/**
 * Clear all the variables in St502screen
 */
	public static void clear(VarModel pv) {
		St502ScreenVars screenVars = (St502ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.riskunit.clear();
		screenVars.mortcls01.clear();
		screenVars.insprm01.clear();
		screenVars.instpr01.clear();
		screenVars.mortcls02.clear();
		screenVars.insprm02.clear();
		screenVars.instpr02.clear();
		screenVars.mortcls03.clear();
		screenVars.insprm03.clear();
		screenVars.instpr03.clear();
		screenVars.mortcls04.clear();
		screenVars.insprm04.clear();
		screenVars.instpr04.clear();
		screenVars.mortcls05.clear();
		screenVars.insprm05.clear();
		screenVars.instpr05.clear();
		screenVars.mortcls06.clear();
		screenVars.insprm06.clear();
		screenVars.instpr06.clear();
		screenVars.mortcls07.clear();
		screenVars.insprm07.clear();
		screenVars.instpr07.clear();
		screenVars.mortality1.clear();
		screenVars.mortality2.clear();
		screenVars.mortality3.clear();
		screenVars.mortality4.clear();
		screenVars.mortality5.clear();
		screenVars.mortality6.clear();
		screenVars.mortality7.clear();
		screenVars.benefitMin01.clear();
		screenVars.benefitMax01.clear();
		screenVars.benefitMin02.clear();
		screenVars.benefitMax02.clear();
		screenVars.premUnit.clear();
		screenVars.disccntmeth.clear();
	}
}
