package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr652screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 7, 16, 1, 2, 12, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {20, 23, 2, 66}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr652ScreenVars sv = (Sr652ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr652screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr652ScreenVars screenVars = (Sr652ScreenVars)pv;
		screenVars.pgmnam.setClassString("");
		screenVars.mdblactf.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.zdocno.setClassString("");
		screenVars.paidby.setClassString("");
		screenVars.exmcode.setClassString("");
	}

/**
 * Clear all the variables in Sr652screen
 */
	public static void clear(VarModel pv) {
		Sr652ScreenVars screenVars = (Sr652ScreenVars) pv;
		screenVars.pgmnam.clear();
		screenVars.mdblactf.clear();
		screenVars.chdrsel.clear();
		screenVars.zdocno.clear();
		screenVars.paidby.clear();
		screenVars.exmcode.clear();
	}
}
