package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR51B
 * @version 1.0 generated on 30/08/09 07:16
 * @author Quipoz
 */
public class Sr51bScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1046);
	public FixedLengthStringData dataFields = new FixedLengthStringData(310).isAPartOf(dataArea, 0);
	public FixedLengthStringData ages = new FixedLengthStringData(30).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] age = ZDArrayPartOfStructure(10, 3, 0, ages, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ages, 0, FILLER_REDEFINE);
	public ZonedDecimalData age01 = DD.age.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData age02 = DD.age.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData age03 = DD.age.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData age04 = DD.age.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData age05 = DD.age.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData age06 = DD.age.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData age07 = DD.age.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData age08 = DD.age.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData age09 = DD.age.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData age10 = DD.age.copyToZonedDecimal().isAPartOf(filler,27);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData indcs = new FixedLengthStringData(20).isAPartOf(dataFields, 31);
	public FixedLengthStringData[] indc = FLSArrayPartOfStructure(10, 2, indcs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(indcs, 0, FILLER_REDEFINE);
	public FixedLengthStringData indc01 = DD.indc.copy().isAPartOf(filler1,0);
	public FixedLengthStringData indc02 = DD.indc.copy().isAPartOf(filler1,2);
	public FixedLengthStringData indc03 = DD.indc.copy().isAPartOf(filler1,4);
	public FixedLengthStringData indc04 = DD.indc.copy().isAPartOf(filler1,6);
	public FixedLengthStringData indc05 = DD.indc.copy().isAPartOf(filler1,8);
	public FixedLengthStringData indc06 = DD.indc.copy().isAPartOf(filler1,10);
	public FixedLengthStringData indc07 = DD.indc.copy().isAPartOf(filler1,12);
	public FixedLengthStringData indc08 = DD.indc.copy().isAPartOf(filler1,14);
	public FixedLengthStringData indc09 = DD.indc.copy().isAPartOf(filler1,16);
	public FixedLengthStringData indc10 = DD.indc.copy().isAPartOf(filler1,18);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,51);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,59);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,67);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData overdueMinas = new FixedLengthStringData(30).isAPartOf(dataFields, 105);
	public ZonedDecimalData[] overdueMina = ZDArrayPartOfStructure(10, 3, 0, overdueMinas, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(30).isAPartOf(overdueMinas, 0, FILLER_REDEFINE);
	public ZonedDecimalData overdueMina01 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData overdueMina02 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,3);
	public ZonedDecimalData overdueMina03 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,6);
	public ZonedDecimalData overdueMina04 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,9);
	public ZonedDecimalData overdueMina05 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,12);
	public ZonedDecimalData overdueMina06 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,15);
	public ZonedDecimalData overdueMina07 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,18);
	public ZonedDecimalData overdueMina08 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,21);
	public ZonedDecimalData overdueMina09 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,24);
	public ZonedDecimalData overdueMina10 = DD.oduemina.copyToZonedDecimal().isAPartOf(filler2,27);
	public FixedLengthStringData suminss = new FixedLengthStringData(170).isAPartOf(dataFields, 135);
	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(10, 17, 2, suminss, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(170).isAPartOf(suminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumins01 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData sumins02 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,17);
	public ZonedDecimalData sumins03 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,34);
	public ZonedDecimalData sumins04 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,51);
	public ZonedDecimalData sumins05 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,68);
	public ZonedDecimalData sumins06 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,85);
	public ZonedDecimalData sumins07 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,102);
	public ZonedDecimalData sumins08 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,119);
	public ZonedDecimalData sumins09 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,136);
	public ZonedDecimalData sumins10 = DD.sumins.copyToZonedDecimal().isAPartOf(filler3,153);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,305);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(184).isAPartOf(dataArea, 310);
	public FixedLengthStringData agesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] ageErr = FLSArrayPartOfStructure(10, 4, agesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(agesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData age01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData age02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData age03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData age04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData age05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData age06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData age07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData age08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData age09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData age10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData indcsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] indcErr = FLSArrayPartOfStructure(10, 4, indcsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(indcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData indc01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData indc02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData indc03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData indc04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData indc05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData indc06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData indc07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData indc08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData indc09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData indc10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData odueminasErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] odueminaErr = FLSArrayPartOfStructure(10, 4, odueminasErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(odueminasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData oduemina01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData oduemina02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData oduemina03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData oduemina04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData oduemina05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData oduemina06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData oduemina07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData oduemina08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData oduemina09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData oduemina10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData suminssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData[] suminsErr = FLSArrayPartOfStructure(10, 4, suminssErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(suminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumins01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData sumins02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData sumins03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData sumins04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData sumins05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData sumins06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData sumins07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData sumins08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData sumins09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData sumins10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(552).isAPartOf(dataArea, 494);
	public FixedLengthStringData agesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] ageOut = FLSArrayPartOfStructure(10, 12, agesOut, 0);
	public FixedLengthStringData[][] ageO = FLSDArrayPartOfArrayStructure(12, 1, ageOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(agesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] age01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] age02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] age03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] age04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] age05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] age06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] age07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] age08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] age09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] age10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData indcsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] indcOut = FLSArrayPartOfStructure(10, 12, indcsOut, 0);
	public FixedLengthStringData[][] indcO = FLSDArrayPartOfArrayStructure(12, 1, indcOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(120).isAPartOf(indcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] indc01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] indc02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] indc03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] indc04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] indc05Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData[] indc06Out = FLSArrayPartOfStructure(12, 1, filler9, 60);
	public FixedLengthStringData[] indc07Out = FLSArrayPartOfStructure(12, 1, filler9, 72);
	public FixedLengthStringData[] indc08Out = FLSArrayPartOfStructure(12, 1, filler9, 84);
	public FixedLengthStringData[] indc09Out = FLSArrayPartOfStructure(12, 1, filler9, 96);
	public FixedLengthStringData[] indc10Out = FLSArrayPartOfStructure(12, 1, filler9, 108);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData odueminasOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 300);
	public FixedLengthStringData[] odueminaOut = FLSArrayPartOfStructure(10, 12, odueminasOut, 0);
	public FixedLengthStringData[][] odueminaO = FLSDArrayPartOfArrayStructure(12, 1, odueminaOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(odueminasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] oduemina01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] oduemina02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] oduemina03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] oduemina04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] oduemina05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] oduemina06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] oduemina07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] oduemina08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] oduemina09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] oduemina10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData suminssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 420);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(10, 12, suminssOut, 0);
	public FixedLengthStringData[][] suminsO = FLSDArrayPartOfArrayStructure(12, 1, suminsOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(suminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumins01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] sumins02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] sumins03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] sumins04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] sumins05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] sumins06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] sumins07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] sumins08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] sumins09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] sumins10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr51bscreenWritten = new LongData(0);
	public LongData Sr51bprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr51bScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(age01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins01Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina02Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins02Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina03Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins03Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins04Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins05Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina06Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins06Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina07Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins07Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina08Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins08Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina09Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins09Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oduemina10Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins10Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, age01, overdueMina01, sumins01, age02, overdueMina02, sumins02, age03, overdueMina03, sumins03, age04, overdueMina04, sumins04, age05, overdueMina05, sumins05, age06, overdueMina06, sumins06, age07, overdueMina07, sumins07, age08, overdueMina08, sumins08, age09, overdueMina09, sumins09, age10, overdueMina10, sumins10, indc01, indc02, indc03, indc04, indc05, indc06, indc07, indc08, indc09, indc10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, age01Out, oduemina01Out, sumins01Out, age02Out, oduemina02Out, sumins02Out, age03Out, oduemina03Out, sumins03Out, age04Out, oduemina04Out, sumins04Out, age05Out, oduemina05Out, sumins05Out, age06Out, oduemina06Out, sumins06Out, age07Out, oduemina07Out, sumins07Out, age08Out, oduemina08Out, sumins08Out, age09Out, oduemina09Out, sumins09Out, age10Out, oduemina10Out, sumins10Out, indc01Out, indc02Out, indc03Out, indc04Out, indc05Out, indc06Out, indc07Out, indc08Out, indc09Out, indc10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, age01Err, oduemina01Err, sumins01Err, age02Err, oduemina02Err, sumins02Err, age03Err, oduemina03Err, sumins03Err, age04Err, oduemina04Err, sumins04Err, age05Err, oduemina05Err, sumins05Err, age06Err, oduemina06Err, sumins06Err, age07Err, oduemina07Err, sumins07Err, age08Err, oduemina08Err, sumins08Err, age09Err, oduemina09Err, sumins09Err, age10Err, oduemina10Err, sumins10Err, indc01Err, indc02Err, indc03Err, indc04Err, indc05Err, indc06Err, indc07Err, indc08Err, indc09Err, indc10Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr51bscreen.class;
		protectRecord = Sr51bprotect.class;
	}

}
