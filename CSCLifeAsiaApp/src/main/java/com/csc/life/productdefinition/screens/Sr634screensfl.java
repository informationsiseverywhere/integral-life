package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr634screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 12, 3, 21}; 
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {51, 52, 53, 50}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 18, 2, 73}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr634ScreenVars sv = (Sr634ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr634screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr634screensfl, 
			sv.Sr634screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr634ScreenVars sv = (Sr634ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr634screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr634ScreenVars sv = (Sr634ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr634screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr634screensflWritten.gt(0))
		{
			sv.sr634screensfl.setCurrentIndex(0);
			sv.Sr634screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr634ScreenVars sv = (Sr634ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr634screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr634ScreenVars screenVars = (Sr634ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hflag.setFieldName("hflag");
				screenVars.zdoctor.setFieldName("zdoctor");
				screenVars.name.setFieldName("name");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.dtetrmDisp.setFieldName("dtetrmDisp");
				screenVars.zmmccde.setFieldName("zmmccde");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hflag.set(dm.getField("hflag"));
			screenVars.zdoctor.set(dm.getField("zdoctor"));
			screenVars.name.set(dm.getField("name"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.dtetrmDisp.set(dm.getField("dtetrmDisp"));
			screenVars.zmmccde.set(dm.getField("zmmccde"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr634ScreenVars screenVars = (Sr634ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hflag.setFieldName("hflag");
				screenVars.zdoctor.setFieldName("zdoctor");
				screenVars.name.setFieldName("name");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.dtetrmDisp.setFieldName("dtetrmDisp");
				screenVars.zmmccde.setFieldName("zmmccde");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hflag").set(screenVars.hflag);
			dm.getField("zdoctor").set(screenVars.zdoctor);
			dm.getField("name").set(screenVars.name);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("dtetrmDisp").set(screenVars.dtetrmDisp);
			dm.getField("zmmccde").set(screenVars.zmmccde);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr634screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr634ScreenVars screenVars = (Sr634ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hflag.clearFormatting();
		screenVars.zdoctor.clearFormatting();
		screenVars.name.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.dtetrmDisp.clearFormatting();
		screenVars.zmmccde.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr634ScreenVars screenVars = (Sr634ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hflag.setClassString("");
		screenVars.zdoctor.setClassString("");
		screenVars.name.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.dtetrmDisp.setClassString("");
		screenVars.zmmccde.setClassString("");
	}

/**
 * Clear all the variables in Sr634screensfl
 */
	public static void clear(VarModel pv) {
		Sr634ScreenVars screenVars = (Sr634ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hflag.clear();
		screenVars.zdoctor.clear();
		screenVars.name.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.dtetrmDisp.clear();
		screenVars.dtetrm.clear();
		screenVars.zmmccde.clear();
	}
}
