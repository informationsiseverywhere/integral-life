package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.productdefinition.procedures.Td5h2pt;
import com.csc.life.productdefinition.screens.Sd5h2ScreenVars;
import com.csc.life.productdefinition.tablestructures.Td5h2rec;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5h2 extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5H2");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);	
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itempf itempf = new Itempf();
	private FixedLengthStringData wsaaPrintLocation = new FixedLengthStringData(1);
	private Validator validLocation = new Validator(wsaaPrintLocation, "I","B"," ");
	private static final String dp01 = "DP01";
	private Td5h2rec td5h2rec = new Td5h2rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5h2ScreenVars sv = ScreenProgram.getScreenVars(Sd5h2ScreenVars.class);

	
	public Pd5h2() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5h2", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
		
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaItemkey.set(wsspsmart.itemkey);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		itempf = itempfDAO.findItemByItdm(wsaaItemkey.itemItemcoy.toString(), wsaaItemkey.itemItemtabl.toString(),wsaaItemkey.itemItemitem.toString());

		if (itempf == null ) {
			syserrrec.params.set(itempf.getItemtabl() + itempf.getItemitem()); 
			fatalError600();
		}
	
	}

protected void readRecord1031()
	{
	wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
	wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
	wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
	wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
	wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
	wsaaDesckey.descLanguage.set(wsspcomn.language);
	
	descpf=descDAO.getdescData("IT", wsaaItemkey.itemItemtabl.toString(), wsaaItemkey.itemItemitem.toString(), wsaaItemkey.itemItemcoy.toString(), wsspcomn.language.toString());
	if (descpf==null) {
		syserrrec.params.set(descpf.getDesctabl() + descpf.getDescitem()); 
		fatalError600();
	}
	sv.longdesc.set(descpf.getLongdesc());

	}

protected void moveToScreen1040()
	{
	
		td5h2rec.td5h2Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (!itempf.getGenarea().equals(SPACES)) {
		generalArea1045();
		}
	
	}

protected void generalArea1045()
	{
		sv.accInd.set(td5h2rec.accInd);
		sv.agntsdets.set(td5h2rec.agntsdets);
		sv.ddind.set(td5h2rec.ddind);
		sv.grpind.set(td5h2rec.grpind);
		sv.mediaRequired.set(td5h2rec.mediaRequired);
		sv.payind.set(td5h2rec.payind);
		sv.sacscode.set(td5h2rec.sacscode);
		sv.sacstyp.set(td5h2rec.sacstyp);
		sv.triggerRequired.set(td5h2rec.triggerRequired);
		sv.printLocation.set(td5h2rec.printLocation);
		sv.crcind.set(td5h2rec.crcind);
		sv.rollovrind.set(td5h2rec.rollovrind);
		
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
		/**     RETRIEVE SCREEN FIELDS AND EDIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			exit2090();
		}
		/**    Enter screen validation here.                              **/
		/*OTHER*/
		wsaaPrintLocation.set(sv.printLocation);
		if (!validLocation.isTrue()) {
			sv.dbprtlocErr.set(dp01);
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
		/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/
	}

protected void update3000()
	{
		preparation3010();
		updatePrimaryRecord3050();
		updateRecord3055();
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		
	}

protected void updatePrimaryRecord3050()
	{
		
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);

	}

protected void updateRecord3055()
	{
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
			}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			return;
		}
		itempf.setGenarea(td5h2rec.td5h2Rec.toString().getBytes());
		itempf.setItempfx(wsaaItemkey.itemItempfx.toString());
		itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.accInd, td5h2rec.accInd)) {
			td5h2rec.accInd.set(sv.accInd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.agntsdets, td5h2rec.agntsdets)) {
			td5h2rec.agntsdets.set(sv.agntsdets);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ddind, td5h2rec.ddind)) {
			td5h2rec.ddind.set(sv.ddind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.grpind, td5h2rec.grpind)) {
			td5h2rec.grpind.set(sv.grpind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mediaRequired, td5h2rec.mediaRequired)) {
			td5h2rec.mediaRequired.set(sv.mediaRequired);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.payind, td5h2rec.payind)) {
			td5h2rec.payind.set(sv.payind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sacscode, td5h2rec.sacscode)) {
			td5h2rec.sacscode.set(sv.sacscode);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sacstyp, td5h2rec.sacstyp)) {
			td5h2rec.sacstyp.set(sv.sacstyp);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.triggerRequired, td5h2rec.triggerRequired)) {
			td5h2rec.triggerRequired.set(sv.triggerRequired);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.printLocation, td5h2rec.printLocation)) {
			td5h2rec.printLocation.set(sv.printLocation);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.crcind, td5h2rec.crcind)) {
			td5h2rec.crcind.set(sv.crcind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rollovrind, td5h2rec.rollovrind)) {
			td5h2rec.rollovrind.set(sv.rollovrind);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Td5h2pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
