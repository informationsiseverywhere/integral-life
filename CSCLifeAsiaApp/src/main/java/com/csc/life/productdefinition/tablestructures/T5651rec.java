package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:32
 * Description:
 * Copybook name: T5651REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5651rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5651Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ccode = new FixedLengthStringData(1).isAPartOf(t5651Rec, 0);
  	public FixedLengthStringData mlmedcde = new FixedLengthStringData(4).isAPartOf(t5651Rec, 1);
  	public FixedLengthStringData reasind = new FixedLengthStringData(1).isAPartOf(t5651Rec, 5);
  	public FixedLengthStringData filler = new FixedLengthStringData(494).isAPartOf(t5651Rec, 6, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5651Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5651Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}