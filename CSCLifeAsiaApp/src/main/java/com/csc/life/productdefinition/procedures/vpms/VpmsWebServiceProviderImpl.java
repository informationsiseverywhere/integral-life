package com.csc.life.productdefinition.procedures.vpms;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import com.csc.dip.webservice.core.ComputeAction;
import com.csc.dip.webservice.core.ProductRequest;
import com.csc.dip.webservice.core.ProductRequestProcessor;
import com.csc.dip.webservice.core.ProductRequestProcessorService;
import com.csc.dip.webservice.core.ProductRequestProcessorServiceLocator;
import com.csc.dip.webservice.core.ProductResponse;
import com.csc.dip.webservice.core.SetAction;
import com.csc.dip.webservice.core.VpmsAction;
import com.csc.dip.webservice.core.VpmsResult;

/**
 * @author sle3
 *
 */
public class VpmsWebServiceProviderImpl implements IVpmsCalculator {

	private static final String EMPTY_STRING = "";
	private String productname;
	private String endpoint;
	/**
	 * Constructor
	 */
	public VpmsWebServiceProviderImpl(String productname, String endpoint) {
		super();
		this.productname = productname;
		this.endpoint = endpoint;
	}

	/* (non-Javadoc)
	 * @see com.csc.aperio.vpms.service.IVpmsCalculator#compute(java.util.Map, java.util.Map, java.util.Collection)
	 */
	@Override
	public Map<String, ComputeResult> compute(Map<String, String> inputMap,
			Map<String, String> calcMap, Collection<String> outputs)
			throws MalformedURLException, ServiceException, RemoteException {
		
		ProductRequestProcessorService serv = new ProductRequestProcessorServiceLocator();
		ProductRequestProcessor reqProcessor = serv.getProductRequestProcessor(new URL(this.endpoint));
		
		ProductRequest prodReq = new ProductRequest();
		prodReq.setProductName(this.productname);
		
		ComputeAction computeAction = null;
		SetAction setAction = null;
		
		List<VpmsAction> vpmsActionList = new ArrayList<VpmsAction>();
		// set inputs
		for (Map.Entry<String, String> inputEntry : inputMap.entrySet()) {
			setAction = new SetAction();
			setAction.setName(inputEntry.getKey());
			setAction.setValue(inputEntry.getValue());
			vpmsActionList.add(setAction);
		}
		
		// set calcs
		if (calcMap != null) {
			for (Map.Entry<String, String> calcEntry : calcMap.entrySet()) {
				setAction = new SetAction();
				setAction.setName(calcEntry.getKey());
				setAction.setValue(calcEntry.getValue());
				vpmsActionList.add(setAction);
			}
		}
		
		// output params

		for (String param : outputs) {
			computeAction = new ComputeAction();
			computeAction.setName(param);
			vpmsActionList.add(computeAction);
		}
		
		// do compute
		prodReq.setActions(vpmsActionList.toArray(new VpmsAction[vpmsActionList.size()]));
		ProductResponse response = reqProcessor.process(prodReq);
		VpmsResult[] vpmsResults = response.getResults();
		
		return vpmsResultsToComputeResultMap(vpmsResults, outputs);
	}

	/**
	 * 
	 * @param vpmsResults
	 * @param outputs
	 * @return
	 * @throws VpmsComputeException
	 */
	private Map<String, ComputeResult> vpmsResultsToComputeResultMap(VpmsResult[] vpmsResults, Collection<String> outputs) {
		Map<String, ComputeResult> results = new HashMap<String, ComputeResult>(outputs.size());
		for (String name : outputs) {
			for (VpmsResult vpmsResult : vpmsResults) {
				if(vpmsResult != null){
					if(vpmsResult instanceof com.csc.dip.webservice.core.ComputeResult){
						com.csc.dip.webservice.core.ComputeResult computeResult = (com.csc.dip.webservice.core.ComputeResult)vpmsResult;						
						ComputeResult customComputeResult = new ComputeResult();
						customComputeResult.setName(computeResult.getName());
						customComputeResult.setValue(computeResult.getValue().trim());
						customComputeResult.setReturnCode(new Integer(computeResult.getReturnCode()).toString());
						customComputeResult.setReferenceField(computeResult.getMessage());
						customComputeResult.setReferenceField(computeResult.getRefField()!= null ? computeResult.getRefField().trim(): EMPTY_STRING);
						results.put(customComputeResult.getName(), customComputeResult);
					}
				}else {
//					throw new VpmsComputeException(name, -100, "Not found", "");
				}
			}
		}
		return results;
	}
}
