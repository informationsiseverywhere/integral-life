package com.csc.life.productdefinition.screens;

import com.csc.life.productdefinition.screens.Sd5gfScreenVars;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;


public class Sd5gfprotect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5gfScreenVars sv = (Sd5gfScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5gfprotectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5gfScreenVars screenVars = (Sd5gfScreenVars)pv;
	}

/**
 * Clear all the variables 
 */
	public static void clear(VarModel pv) {
		Sd5gfScreenVars screenVars = (Sd5gfScreenVars) pv;
	}
}
