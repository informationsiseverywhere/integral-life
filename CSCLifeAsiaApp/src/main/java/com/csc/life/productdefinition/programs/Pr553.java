/*
 * File: Pr553.java
 * Date: 30 August 2009 1:40:25
 * Author: Quipoz Limited
 * 
 * Class transformed from PR553.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.life.productdefinition.dataaccess.MliachdTableDAM;
import com.csc.life.productdefinition.dataaccess.MlianamTableDAM;
import com.csc.life.productdefinition.dataaccess.MliasecTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.screens.Sr553ScreenVars;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  Substandard & Hospitalisation
*
*****************************************************************
* </pre>
*/
public class Pr553 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR553");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0);
	private ZonedDecimalData iy = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaRestartFlag = new FixedLengthStringData(1);
	private Validator restart = new Validator(wsaaRestartFlag, "Y");
	private FixedLengthStringData wsaaLastClntnaml = new FixedLengthStringData(51);

	private FixedLengthStringData wsaaLastActn = new FixedLengthStringData(1);
	private Validator orderByName = new Validator(wsaaLastActn, "1");
	private Validator orderByIc = new Validator(wsaaLastActn, "2");
	private Validator orderByContract = new Validator(wsaaLastActn, "3");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaWsspFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsbbClntnaml = new FixedLengthStringData(51);
	private FixedLengthStringData wsbbSecurityno = new FixedLengthStringData(15);
	private FixedLengthStringData wsbbMlentity = new FixedLengthStringData(15);
	private String w400 = "W400";
	private String r006 = "R006";
		/* TABLES */
	private String th605 = "TH605";
	private String mliarec = "MLIAREC";
	private String mliasecrec = "MLIASECREC";
	private String mlianamrec = "MLIANAMREC";
	private String mliachdrec = "MLIACHDREC";

		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Substd & Hosp view by IC no. and Contrac*/
	private MliaTableDAM mliaIO = new MliaTableDAM();
		/*Substandard & Hospitalisatn by Contr No.*/
	private MliachdTableDAM mliachdIO = new MliachdTableDAM();
		/*Substd & Hospitalisatn by Client Name*/
	private MlianamTableDAM mlianamIO = new MlianamTableDAM();
		/*Substandard & Hospitalisation on IDNO*/
	private MliasecTableDAM mliasecIO = new MliasecTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
	private Th605rec th605rec = new Th605rec();
	private Wssplife wssplife = new Wssplife();
	private Sr553ScreenVars sv = getLScreenVars();

	protected Sr553ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars( Sr553ScreenVars.class);
	}
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit2090, 
		callNextSfl2120, 
		nextSfl2180, 
		exit2190, 
		callNextSfl4020, 
		nextSfl4080, 
		exit4090, 
		load5010, 
		exit5090, 
		exit5190, 
		exit5290, 
		exit5390
	}

	public Pr553() {
		super();
		screenVars = sv;
		new ScreenModel("Sr553", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//sv.clntnaml.set(LOVALUES); //ILIFE-6519 Start
		sv.clntnaml.set(SPACES);     //ILIFE-6519 End
		sv.reptname.set(wssplife.trandesc);
		wsaaWsspFlag.set(wsspcomn.flag);
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,Varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(wsspcomn.company);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),Varcom.oK)
		&& isNE(itemIO.getStatuz(),Varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),Varcom.mrnf)) {
			itemIO.setItemitem("**");
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),Varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		wsaaRestartFlag.set("Y");
		sv.actn.set("1");
		wsaaLastActn.set("1");
		wsaaLastClntnaml.set(LOVALUES);
		loadOnePage5000();
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(Varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(Varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.actn,SPACES)) {
			sv.actnErr.set(w400);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.actn,"1")
		&& isNE(sv.actn,"2")
		&& isNE(sv.actn,"3")
		&& isNE(sv.actn,"4")) {
			sv.actnErr.set(w400);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.actn,wsaaLastActn)
		|| isNE(sv.clntnaml,wsaaLastClntnaml)) {
			wsaaLastActn.set(sv.actn);
			wsaaLastClntnaml.set(sv.clntnaml);
			wsaaRestartFlag.set("Y");
			loadOnePage5000();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,Varcom.rolu)) {
			loadOnePage5000();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		validateSubfile2100();
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					n2110();
				}
				case callNextSfl2120: {
					callNextSfl2120();
				}
				case nextSfl2180: {
					nextSfl2180();
				}
				case exit2190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void n2110()
	{
		scrnparams.function.set(Varcom.sstrt);
	}

protected void callNextSfl2120()
	{
		processScreen("SR553", sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,Varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(sv.optind,SPACES)) {
			goTo(GotoLabel.nextSfl2180);
		}
		mliaIO.setSecurityno(sv.idno);
		mliaIO.setMlentity(sv.mlentity);
		mliaIO.setFormat(mliarec);
		mliaIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),Varcom.oK)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.flag,"D")) {
			if (isEQ(mliaIO.getMind(),"Y")) {
				sv.optindErr.set(r006);
				wsspcomn.edterror.set(SPACES);
			}
		}
		if (isEQ(wsspcomn.flag,"M")
		|| isEQ(wsspcomn.flag,"D")) {
			if (isNE(mliaIO.getMlcoycde(),th605rec.liacoy)) {
				sv.optindErr.set(r006);
				wsspcomn.edterror.set(SPACES);
			}
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.optind);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,Varcom.oK)) {
			sv.optindErr.set(optswchrec.optsStatuz);
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
			sv.optindOut[Varcom.pr.toInt()].set("N");
			scrnparams.function.set(Varcom.supd);
			processScreen("SR553", sv);
			if (isNE(scrnparams.statuz,Varcom.oK)) {
				syserrrec.params.set(scrnparams.screenParams);
				fatalError600();
			}
		}
	}

protected void nextSfl2180()
	{
		scrnparams.function.set(Varcom.srdn);
		goTo(GotoLabel.callNextSfl2120);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case callNextSfl4020: {
					callNextSfl4020();
				}
				case nextSfl4080: {
					nextSfl4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			updateSubfile7000();
			scrnparams.function.set(Varcom.srdn);
		}
		else {
			scrnparams.function.set(Varcom.sstrt);
		}
	}

protected void callNextSfl4020()
	{
		processScreen("SR553", sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,Varcom.endp)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
				optswchrec.optsSelType.set(SPACES);
				optswchrec.optsSelOptno.set(ZERO);
				optswchCall4500();
			}
			else {
				wsspcomn.flag.set(wsaaWsspFlag);
				wsspcomn.programPtr.add(1);
			}
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.optind,SPACES)) {
			goTo(GotoLabel.nextSfl4080);
		}
		optswchrec.optsSelOptno.set(sv.optind);
		optswchrec.optsSelType.set("L");
		optswchCall4500();
		mliaIO.setSecurityno(sv.idno);
		mliaIO.setMlentity(sv.mlentity);
		mliaIO.setFormat(mliarec);
		mliaIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),Varcom.oK)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		mliaIO.setFunction(Varcom.keeps);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),Varcom.oK)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		wssplife.usmeth.set(SPACES);
		goTo(GotoLabel.exit4090);
	}

protected void nextSfl4080()
	{
		scrnparams.function.set(Varcom.srdn);
		goTo(GotoLabel.callNextSfl4020);
	}

protected void optswchCall4500()
	{
		call4510();
	}

protected void call4510()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,Varcom.oK))
		&& (isNE(optswchrec.optsStatuz,Varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,Varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void loadOnePage5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case load5010: {
					load5010();
				}
				case exit5090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void load5010()
	{
		if (restart.isTrue()) {
			wsaaStatuz.set(Varcom.oK);
			scrnparams.function.set(Varcom.sclr);
			processScreen("SR553", sv);
			if (isNE(scrnparams.statuz,Varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.subfileRrn.set(1);
			scrnparams.subfileMore.set("Y");
		}
		if (orderByName.isTrue()) {
			if (restart.isTrue()) {
				wsaaRestartFlag.set("N");
				mlianamIO.setClntnaml(sv.clntnaml);
				mlianamIO.setFunction(Varcom.begn);
			}
			orderByName5100();
		}
		if (orderByIc.isTrue()) {
			if (restart.isTrue()) {
				wsaaRestartFlag.set("N");
				mliasecIO.setSecurityno(sv.clntnaml);
				mliasecIO.setFunction(Varcom.begn);
			}
			orderByIc5200();
		}
		if (orderByContract.isTrue()) {
			if (restart.isTrue()) {
				wsaaRestartFlag.set("N");
				mliachdIO.setMlentity(sv.clntnaml);
				mliachdIO.setFunction(Varcom.begn);
			}
			orderByContract5300();
		}
		if (isEQ(wsaaStatuz,Varcom.endp)) {
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit5090);
		}
		sv.optindOut[Varcom.pr.toInt()].set("N");
		sv.optind.set(SPACES);
		sv.flag.set(SPACES);
		sv.clntlname.set(wsbbClntnaml);
		sv.idno.set(wsbbSecurityno);
		sv.mlentity.set(wsbbMlentity);
		scrnparams.function.set(Varcom.sadd);
		processScreen("SR553", sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		compute(ix, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		iy.setRemainder(ix);
		if (isEQ(iy,0)) {
			goTo(GotoLabel.exit5090);
		}
		goTo(GotoLabel.load5010);
	}

protected void orderByName5100()
	{
		try {
			n5110();
		}
		catch (GOTOException e){
		}
	}

protected void n5110()
	{
		mlianamIO.setFormat(mlianamrec);
		SmartFileCode.execute(appVars, mlianamIO);
		if (isNE(mlianamIO.getStatuz(),Varcom.oK)
		&& isNE(mlianamIO.getStatuz(),Varcom.endp)) {
			syserrrec.statuz.set(mlianamIO.getStatuz());
			syserrrec.params.set(mlianamIO.getParams());
			fatalError600();
		}
		if (isEQ(mlianamIO.getStatuz(),Varcom.endp)) {
			if (isEQ(mlianamIO.getFunction(),Varcom.begn)) {
				sflAddDummy6000();
			}
			wsaaStatuz.set(Varcom.endp);
			goTo(GotoLabel.exit5190);
		}
		wsbbClntnaml.set(mlianamIO.getClntnaml());
		wsbbSecurityno.set(mlianamIO.getSecurityno());
		wsbbMlentity.set(mlianamIO.getMlentity());
		mlianamIO.setFunction(Varcom.nextr);
	}

protected void orderByIc5200()
	{
		try {
			n5210();
		}
		catch (GOTOException e){
		}
	}

protected void n5210()
	{
		mliasecIO.setFormat(mliasecrec);
		SmartFileCode.execute(appVars, mliasecIO);
		if (isNE(mliasecIO.getStatuz(),Varcom.oK)
		&& isNE(mliasecIO.getStatuz(),Varcom.endp)) {
			syserrrec.statuz.set(mliasecIO.getStatuz());
			syserrrec.params.set(mliasecIO.getParams());
			fatalError600();
		}
		if (isEQ(mliasecIO.getStatuz(),Varcom.endp)) {
			if (isEQ(mliasecIO.getFunction(),Varcom.begn)) {
				sflAddDummy6000();
			}
			wsaaStatuz.set(Varcom.endp);
			goTo(GotoLabel.exit5290);
		}
		wsbbClntnaml.set(mliasecIO.getClntnaml());
		wsbbSecurityno.set(mliasecIO.getSecurityno());
		wsbbMlentity.set(mliasecIO.getMlentity());
		mliasecIO.setFunction(Varcom.nextr);
	}

protected void orderByContract5300()
	{
		try {
			n5310();
		}
		catch (GOTOException e){
		}
	}

protected void n5310()
	{
		mliachdIO.setFormat(mliachdrec);
		SmartFileCode.execute(appVars, mliachdIO);
		if (isNE(mliachdIO.getStatuz(),Varcom.oK)
		&& isNE(mliachdIO.getStatuz(),Varcom.endp)) {
			syserrrec.statuz.set(mliachdIO.getStatuz());
			syserrrec.params.set(mliachdIO.getParams());
			fatalError600();
		}
		if (isEQ(mliachdIO.getStatuz(),Varcom.endp)) {
			if (isEQ(mliachdIO.getFunction(),Varcom.begn)) {
				sflAddDummy6000();
			}
			wsaaStatuz.set(Varcom.endp);
			goTo(GotoLabel.exit5390);
		}
		wsbbClntnaml.set(mliachdIO.getClntnaml());
		wsbbSecurityno.set(mliachdIO.getSecurityno());
		wsbbMlentity.set(mliachdIO.getMlentity());
		mliachdIO.setFunction(Varcom.nextr);
	}

protected void sflAddDummy6000()
	{
		n6010();
	}

protected void n6010()
	{
		sv.optindOut[Varcom.pr.toInt()].set("Y");
		sv.optind.set(SPACES);
		sv.flag.set(SPACES);
		sv.clntlname.set(SPACES);
		sv.idno.set(SPACES);
		sv.mlentity.set(SPACES);
		scrnparams.function.set(Varcom.sadd);
		processScreen("SR553", sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void updateSubfile7000()
	{
		/*UPD-SFL*/
		sv.optind.set(SPACES);
		if (isEQ(wssplife.usmeth,Varcom.oK)) {
			setScreenFields7100();
		}
		scrnparams.function.set(Varcom.supd);
		processScreen("SR553", sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void setScreenFields7100()
	{
		start7110();
	}

protected void start7110()
	{
		if (isEQ(wsspcomn.flag,"D")) {
			sv.optindOut[Varcom.pr.toInt()].set("Y");
		}
		if (isNE(wsspcomn.flag,"I")) {
			sv.flag.set("*");
		}
		if (isEQ(wsspcomn.flag,"M")) {
			mliaIO.setSecurityno(sv.idno);
			mliaIO.setMlentity(sv.mlentity);
			mliaIO.setFormat(mliarec);
			mliaIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, mliaIO);
			if (isNE(mliaIO.getStatuz(),Varcom.oK)) {
				syserrrec.statuz.set(mliaIO.getStatuz());
				syserrrec.params.set(mliaIO.getParams());
				fatalError600();
			}
			sv.clntlname.set(mliaIO.getClntnaml());
		}
	}
}
