package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:44
 * Description:
 * Copybook name: MINSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Minskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData minsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData minsKey = new FixedLengthStringData(64).isAPartOf(minsFileKey, 0, REDEFINE);
  	public FixedLengthStringData minsChdrcoy = new FixedLengthStringData(1).isAPartOf(minsKey, 0);
  	public FixedLengthStringData minsChdrnum = new FixedLengthStringData(8).isAPartOf(minsKey, 1);
  	public FixedLengthStringData minsLife = new FixedLengthStringData(2).isAPartOf(minsKey, 9);
  	public FixedLengthStringData minsCoverage = new FixedLengthStringData(2).isAPartOf(minsKey, 11);
  	public FixedLengthStringData minsRider = new FixedLengthStringData(2).isAPartOf(minsKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(minsKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(minsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		minsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}