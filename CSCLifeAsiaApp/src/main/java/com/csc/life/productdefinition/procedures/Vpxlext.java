/*
 * File: Vpxchdr.java
 * Date: 20 August 2012 22:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;

import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.procedures.vpms.VPMSUtils;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* VPXLIFEALL All LIFE Data Extract Subroutine
* This subroutine will extract the necessary details of ALL the LIFE
* records of a particular policy. 
* Assumption is that there will be a maximum of 10 Life/Joint Life 
* per policy.
*  
* This data extract subroutine will cater for the requirements of 
* the following Policy Level Calculation Programs: P6378, P5074 and 
* P5074AT. This subroutine will be performed via the TR28X set-up for 
* item PL01.  	 
* 
* New Copybooks POLCALCREC and vpxlextrec will be the linkages of 
* this subroutine.
* 
* vpxlextrec Copybook
*   1.	LIFE			X(02)	Occurs 10
*   2.	JLIFE			X(02)	Occurs 10
*   3.	SMOKING			X(02)	Occurs 10
*   4.	CLTSEX			X(01)	Occurs 10
*   5.	CLTDOB			S9(08) 	Occurs 10
*   6.	OCCUPATION		X(04)	Occurs 10
* 
* 
* Processing
* 
* 	Read LIFELNB until end of file is reached
* 	For each LIFELNB record found
* 	   MOVE corresponding LIFELNB fields to VPXLIFEAREC counterparts 
* 	End-For
* 
*   Status :
*     **** - O-K
*     E882 - Invalid function
*     H211 - Invalid prefix
*     Unknown - Status returned as a result of database
*     error.
*
*****************************************************************
* </pre>
*/
public class Vpxlext extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPXLEXT";
		/* FORMATS */
	private String lextrec = "LEXTREC";
		/* ERRORS */
	private String e049 = "E049";
	private String h832 = "H832";
		/*Agent header*/
	protected Premiumrec premiumrec = new Premiumrec();
	protected Vpxlextrec vpxlextrec= new Vpxlextrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	
	protected LextTableDAM lextIO = new LextTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	
	protected PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190, 
		nextRecord120
	}

	public Vpxlext() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vpxlextrec = (Vpxlextrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		premiumrec.premiumRec.set(vpmcalcrec.linkageArea);

		try {
			para000();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			exit000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void para000()
{
	syserrrec.subrname.set(wsaaSubr);
	vpxlextrec.statuz.set(varcom.oK);
	if (isEQ(vpxlextrec.function,"INIT")){
		initialize050();
		findLext100();
		//ILIFE-3292 
		vpxlextrec.count.set(sub(vpxlextrec.count,1));
		//ILIFE-7528
		vpxlextrec.tabsorsel.set("N");
		
	}
	/*
		Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation]
	else if (isEQ(vpxlextrec.function,"GETV")) {
		returnValue200();
	}
	*/
	else {
		syserrrec.statuz.set(e049);
		vpxlextrec.statuz.set(e049);
		systemError900();
	}
//	para010();
//	exit000();
}

protected void initialize050()
{
	//ILIFE-3975
	if(isEQ(premiumrec.dialdownoption,SPACES))
		premiumrec.dialdownoption.set("100");
	vpxlextrec.count.set(ZERO);
	wsaaSub.set(0);
	/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
	changes related to TRM calculation] Start
	*/
	
/*	for (int loopVar1 = 0; ! (loopVar1 > 7); loopVar1 += 1){
		clearDataRec120();
	}
*/
	vpxlextrec.oppc01.set(ZERO);
	vpxlextrec.zmortpct01.set(ZERO);
	vpxlextrec.opcda01.set(SPACE);
	vpxlextrec.agerate01.set(ZERO);
	vpxlextrec.insprm01.set(ZERO);
	
	vpxlextrec.oppc02.set(ZERO);
	vpxlextrec.zmortpct02.set(ZERO);
	vpxlextrec.opcda02.set(SPACE);
	vpxlextrec.agerate02.set(ZERO);
	vpxlextrec.insprm02.set(ZERO);
	
	vpxlextrec.oppc03.set(ZERO);
	vpxlextrec.zmortpct03.set(ZERO);
	vpxlextrec.opcda03.set(SPACE);
	vpxlextrec.agerate03.set(ZERO);
	vpxlextrec.insprm03.set(ZERO);
	
	vpxlextrec.oppc04.set(ZERO);
	vpxlextrec.zmortpct04.set(ZERO);
	vpxlextrec.opcda04.set(SPACE);
	vpxlextrec.agerate04.set(ZERO);
	vpxlextrec.insprm04.set(ZERO);
	
	vpxlextrec.oppc05.set(ZERO);
	vpxlextrec.zmortpct05.set(ZERO);
	vpxlextrec.opcda05.set(SPACE);
	vpxlextrec.agerate05.set(ZERO);
	vpxlextrec.insprm05.set(ZERO);
	
	vpxlextrec.oppc06.set(ZERO);
	vpxlextrec.zmortpct06.set(ZERO);
	vpxlextrec.opcda06.set(SPACE);
	vpxlextrec.agerate06.set(ZERO);
	vpxlextrec.insprm06.set(ZERO);
	
	vpxlextrec.oppc07.set(ZERO);
	vpxlextrec.zmortpct07.set(ZERO);
	vpxlextrec.opcda07.set(SPACE);
	vpxlextrec.agerate07.set(ZERO);
	vpxlextrec.insprm07.set(ZERO);
	
	vpxlextrec.oppc08.set(ZERO);
	vpxlextrec.zmortpct08.set(ZERO);
	vpxlextrec.opcda08.set(SPACE);
	vpxlextrec.agerate08.set(ZERO);
	vpxlextrec.insprm08.set(ZERO);

	vpxlextrec.noOfCoverages.set(ZERO);
	vpxlextrec.aidsCoverInd.set(SPACE);
	vpxlextrec.ncdCode.set(SPACE);
	vpxlextrec.claimAutoIncrease.set(SPACE);
	vpxlextrec.syndicateCode.set(SPACE);
	vpxlextrec.riskExpiryAge.set(SPACE);
	vpxlextrec.totalSumInsured.set(premiumrec.sumin);
	
}

/*protected void clearDataRec120() {
	wsaaSub.add(1);
	vpxlextrec.oppc[wsaaSub.toInt()].set(ZERO);
	vpxlextrec.zmortpct[wsaaSub.toInt()].set(ZERO);
	vpxlextrec.opcda[wsaaSub.toInt()].set(SPACE);
	vpxlextrec.agerate[wsaaSub.toInt()].set(ZERO);
	vpxlextrec.insprm[wsaaSub.toInt()].set(ZERO);
}
*/
/*Ticket #ILIFE-2005 - End*/

protected void findLext100()
{
	wsaaSub.set(0);
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				search100();
			}
			case nextRecord120: {
				nextRecord120();
			}
			case exit190: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void search100()
{
	//Set-up keys
	lextIO.setDataKey(SPACES);
	lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
	lextIO.setChdrnum(premiumrec.chdrChdrnum);
	lextIO.setCoverage(premiumrec.covrCoverage);
	lextIO.setRider(premiumrec.covrRider);
	lextIO.setLife(premiumrec.lifeLife);
	lextIO.setSeqnbr(ZERO);
	//Read the file
	lextIO.setFunction(varcom.begn);
	lextIO.setFormat(lextrec);
}

protected void nextRecord120()
{
	
	SmartFileCode.execute(appVars, lextIO);
	//Unexpected result
	if (isNE(lextIO.getStatuz(),varcom.oK)
			&& isNE(lextIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(lextIO.getParams());
		dbError999();
	}
	
	if (isNE(lextIO.statuz, varcom.oK)
			|| isNE(lextIO.getChdrcoy(),premiumrec.chdrChdrcoy)
			|| isNE(lextIO.getChdrnum(),premiumrec.chdrChdrnum)
			|| isNE(lextIO.getLife(),premiumrec.lifeLife)
			|| isNE(lextIO.getCoverage(),premiumrec.covrCoverage)
			|| isNE(lextIO.getRider(),premiumrec.covrRider)) {
		goTo(GotoLabel.exit190);
	}
	//Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models
	if(isEQ(lextIO.getFunction(),varcom.begn)){
		compute(vpxlextrec.znadjperc, 0).set(div(lextIO.znadjperc, 100));
	}
	//Ticket #IVE-792 - END
	if (isEQ(premiumrec.reasind, "2")
			&& isEQ(lextIO.getReasind(), "1")){
		goTo(GotoLabel.exit190);
	}
	if (isNE(premiumrec.reasind, "2")
			&& isEQ(lextIO.getReasind(), "2")){
		goTo(GotoLabel.exit190);
	}
	
	//Skip any expired special terms.
	lextIO.setFunction(varcom.nextr);
	if (! isGT(lextIO.extCessDate, premiumrec.reRateDate)){
		goTo(GotoLabel.nextRecord120);
	}
	
	//Set up output fields
	wsaaSub.add(1);
	/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
	changes related to TRM calculation] Start
	*/
	
	if(wsaaSub.equals(1)){
		/*ILIFE-3292 Start */
		//ILIFE-2287 started
		//vpxlextrec.oppc01.set(sub(lextIO.getOppc(),100));				
		//if(isGT(lextIO.getOppc(),100)){
		//            vpxlextrec.oppc01.set(sub(lextIO.getOppc(),100));
		//}  
		//ILIFE-2287 end
		//No Need to subtract from 100
		vpxlextrec.oppc01.set(lextIO.getOppc());
		/*ILIFE-3292 End */
		vpxlextrec.zmortpct01.set(lextIO.getZmortpct());
		vpxlextrec.opcda01.set(lextIO.getOpcda());
		vpxlextrec.agerate01.set(lextIO.getAgerate());
		vpxlextrec.insprm01.set(lextIO.getInsprm());
		
		vpxlextrec.premAdjusted01.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	else if(wsaaSub.equals(2)){
		vpxlextrec.oppc02.set(lextIO.getOppc());
		vpxlextrec.zmortpct02.set(lextIO.getZmortpct());
		vpxlextrec.opcda02.set(lextIO.getOpcda());
		vpxlextrec.agerate02.set(lextIO.getAgerate());
		vpxlextrec.insprm02.set(lextIO.getInsprm());
		
		vpxlextrec.premAdjusted02.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	else if(wsaaSub.equals(3)){
		vpxlextrec.oppc03.set(lextIO.getOppc());
		vpxlextrec.zmortpct03.set(lextIO.getZmortpct());
		vpxlextrec.opcda03.set(lextIO.getOpcda());
		vpxlextrec.agerate03.set(lextIO.getAgerate());
		vpxlextrec.insprm03.set(lextIO.getInsprm());
		
		vpxlextrec.premAdjusted03.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	else if(wsaaSub.equals(4)){
		vpxlextrec.oppc04.set(lextIO.getOppc());
		vpxlextrec.zmortpct04.set(lextIO.getZmortpct());
		vpxlextrec.opcda04.set(lextIO.getOpcda());
		vpxlextrec.agerate04.set(lextIO.getAgerate());
		vpxlextrec.insprm04.set(lextIO.getInsprm());
		
		vpxlextrec.premAdjusted04.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	else if(wsaaSub.equals(5)){
		vpxlextrec.oppc05.set(lextIO.getOppc());
		vpxlextrec.zmortpct05.set(lextIO.getZmortpct());
		vpxlextrec.opcda05.set(lextIO.getOpcda());
		vpxlextrec.agerate05.set(lextIO.getAgerate());
		vpxlextrec.insprm05.set(lextIO.getInsprm());
		
		vpxlextrec.premAdjusted05.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	else if(wsaaSub.equals(6)){
		vpxlextrec.oppc06.set(lextIO.getOppc());
		vpxlextrec.zmortpct06.set(lextIO.getZmortpct());
		vpxlextrec.opcda06.set(lextIO.getOpcda());
		vpxlextrec.agerate06.set(lextIO.getAgerate());
		vpxlextrec.insprm06.set(lextIO.getInsprm());
		
		vpxlextrec.premAdjusted06.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	else if(wsaaSub.equals(7)){
		vpxlextrec.oppc07.set(lextIO.getOppc());
		vpxlextrec.zmortpct07.set(lextIO.getZmortpct());
		vpxlextrec.opcda07.set(lextIO.getOpcda());
		vpxlextrec.agerate07.set(lextIO.getAgerate());
		vpxlextrec.insprm07.set(lextIO.getInsprm());
		
		vpxlextrec.premAdjusted07.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	else if(wsaaSub.equals(8)){
		vpxlextrec.oppc08.set(lextIO.getOppc());
		vpxlextrec.zmortpct08.set(lextIO.getZmortpct());
		vpxlextrec.opcda08.set(lextIO.getOpcda());
		vpxlextrec.agerate08.set(lextIO.getAgerate());
		vpxlextrec.insprm08.set(lextIO.getInsprm());
/*Ticket #ILIFE-2005 - End	*/
		
		vpxlextrec.premAdjusted08.set(lextIO.getPremadj());

		vpxlextrec.count.set(wsaaSub);
	}
	
	
	
	goTo(GotoLabel.nextRecord120);
}
/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
changes related to TRM calculation] Start
*/

/*
protected void returnValue200()
{
	start210();
}

protected void start210()
{
	vpxlextrec.resultValue.set(SPACES);
	String[] field = VPMSUtils.separateNameIndex(vpxlextrec.resultField.getData().toString().trim());
	String fieldName = field[0];
	String index = field[1];
	
	if ("oppc".equals(fieldName)){
		vpxlextrec.resultValue.set(vpxlextrec.oppc[new Integer(index)].getbigdata().toString());
	}else if("zmortpct".equals(fieldName)){
		vpxlextrec.resultValue.set(vpxlextrec.zmortpct[new Integer(index)].getbigdata().toString());
		
	}else if("opcda".equals(fieldName)){
		vpxlextrec.resultValue.set(vpxlextrec.opcda[new Integer(index)]);
		
	}else if("agerate".equals(fieldName)){
		vpxlextrec.resultValue.set(vpxlextrec.agerate[new Integer(index)].getbigdata().toString());
		
	}else if("insprm".equals(fieldName)){
		vpxlextrec.resultValue.set(vpxlextrec.insprm[new Integer(index)].getbigdata().toString());
		
	}else if("count".equals(fieldName)){
		vpxlextrec.resultValue.set(vpxlextrec.count.getbigdata().toString());
		
	}else {
		vpxlextrec.statuz.set(h832);
	}
		
}
*/
/*Ticket #ILIFE-2005 - End*/

protected void systemError900()
{
	/*PARA*/
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}
protected void dbError999()
{
	/*PARA*/
	syserrrec.syserrType.set("1");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}

protected void exit000()
{
	exitProgram();
}

}
