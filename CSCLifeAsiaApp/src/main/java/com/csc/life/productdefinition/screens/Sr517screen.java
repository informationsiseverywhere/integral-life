package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr517screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr517ScreenVars sv = (Sr517ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr517screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr517ScreenVars screenVars = (Sr517ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.zrwvflg01.setClassString("");
		screenVars.zrwvflg02.setClassString("");
		screenVars.ctable01.setClassString("");
		screenVars.ctable02.setClassString("");
		screenVars.ctable03.setClassString("");
		screenVars.ctable04.setClassString("");
		screenVars.ctable05.setClassString("");
		screenVars.ctable06.setClassString("");
		screenVars.ctable07.setClassString("");
		screenVars.ctable08.setClassString("");
		screenVars.ctable09.setClassString("");
		screenVars.ctable10.setClassString("");
		screenVars.ctable11.setClassString("");
		screenVars.ctable12.setClassString("");
		screenVars.ctable13.setClassString("");
		screenVars.ctable14.setClassString("");
		screenVars.ctable15.setClassString("");
		screenVars.ctable16.setClassString("");
		screenVars.ctable17.setClassString("");
		screenVars.ctable18.setClassString("");
		screenVars.ctable19.setClassString("");
		screenVars.ctable20.setClassString("");
		screenVars.ctable21.setClassString("");
		screenVars.ctable22.setClassString("");
		screenVars.ctable23.setClassString("");
		screenVars.ctable24.setClassString("");
		screenVars.ctable25.setClassString("");
		screenVars.ctable26.setClassString("");
		screenVars.ctable27.setClassString("");
		screenVars.ctable28.setClassString("");
		screenVars.ctable29.setClassString("");
		screenVars.ctable30.setClassString("");
		screenVars.ctable31.setClassString("");
		screenVars.ctable32.setClassString("");
		screenVars.ctable33.setClassString("");
		screenVars.ctable34.setClassString("");
		screenVars.ctable35.setClassString("");
		screenVars.ctable36.setClassString("");
		screenVars.ctable37.setClassString("");
		screenVars.ctable38.setClassString("");
		screenVars.ctable39.setClassString("");
		screenVars.ctable40.setClassString("");
		screenVars.ctable41.setClassString("");
		screenVars.ctable42.setClassString("");
		screenVars.ctable43.setClassString("");
		screenVars.ctable44.setClassString("");
		screenVars.ctable45.setClassString("");
		screenVars.ctable46.setClassString("");
		screenVars.ctable47.setClassString("");
		screenVars.ctable48.setClassString("");
		screenVars.ctable49.setClassString("");
		screenVars.ctable50.setClassString("");
		screenVars.zrwvflg03.setClassString("");
		screenVars.zrwvflg04.setClassString("");
		screenVars.zrwvflg05.setClassString("");
		screenVars.contitem.setClassString("");
	}

/**
 * Clear all the variables in Sr517screen
 */
	public static void clear(VarModel pv) {
		Sr517ScreenVars screenVars = (Sr517ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.zrwvflg01.clear();
		screenVars.zrwvflg02.clear();
		screenVars.ctable01.clear();
		screenVars.ctable02.clear();
		screenVars.ctable03.clear();
		screenVars.ctable04.clear();
		screenVars.ctable05.clear();
		screenVars.ctable06.clear();
		screenVars.ctable07.clear();
		screenVars.ctable08.clear();
		screenVars.ctable09.clear();
		screenVars.ctable10.clear();
		screenVars.ctable11.clear();
		screenVars.ctable12.clear();
		screenVars.ctable13.clear();
		screenVars.ctable14.clear();
		screenVars.ctable15.clear();
		screenVars.ctable16.clear();
		screenVars.ctable17.clear();
		screenVars.ctable18.clear();
		screenVars.ctable19.clear();
		screenVars.ctable20.clear();
		screenVars.ctable21.clear();
		screenVars.ctable22.clear();
		screenVars.ctable23.clear();
		screenVars.ctable24.clear();
		screenVars.ctable25.clear();
		screenVars.ctable26.clear();
		screenVars.ctable27.clear();
		screenVars.ctable28.clear();
		screenVars.ctable29.clear();
		screenVars.ctable30.clear();
		screenVars.ctable31.clear();
		screenVars.ctable32.clear();
		screenVars.ctable33.clear();
		screenVars.ctable34.clear();
		screenVars.ctable35.clear();
		screenVars.ctable36.clear();
		screenVars.ctable37.clear();
		screenVars.ctable38.clear();
		screenVars.ctable39.clear();
		screenVars.ctable40.clear();
		screenVars.ctable41.clear();
		screenVars.ctable42.clear();
		screenVars.ctable43.clear();
		screenVars.ctable44.clear();
		screenVars.ctable45.clear();
		screenVars.ctable46.clear();
		screenVars.ctable47.clear();
		screenVars.ctable48.clear();
		screenVars.ctable49.clear();
		screenVars.ctable50.clear();
		screenVars.zrwvflg03.clear();
		screenVars.zrwvflg04.clear();
		screenVars.zrwvflg05.clear(); 
		screenVars.contitem.clear();
	}
}
