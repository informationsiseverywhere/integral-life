package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:13
 * Description:
 * Copybook name: TR594REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr594rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr594Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData undlimits = new FixedLengthStringData(153).isAPartOf(tr594Rec, 0);
  	public ZonedDecimalData[] undlimit = ZDArrayPartOfStructure(9, 17, 0, undlimits, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(153).isAPartOf(undlimits, 0, FILLER_REDEFINE);
  	public ZonedDecimalData undlimit01 = new ZonedDecimalData(17, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData undlimit02 = new ZonedDecimalData(17, 0).isAPartOf(filler, 17);
  	public ZonedDecimalData undlimit03 = new ZonedDecimalData(17, 0).isAPartOf(filler, 34);
  	public ZonedDecimalData undlimit04 = new ZonedDecimalData(17, 0).isAPartOf(filler, 51);
  	public ZonedDecimalData undlimit05 = new ZonedDecimalData(17, 0).isAPartOf(filler, 68);
  	public ZonedDecimalData undlimit06 = new ZonedDecimalData(17, 0).isAPartOf(filler, 85);
  	public ZonedDecimalData undlimit07 = new ZonedDecimalData(17, 0).isAPartOf(filler, 102);
  	public ZonedDecimalData undlimit08 = new ZonedDecimalData(17, 0).isAPartOf(filler, 119);
  	public ZonedDecimalData undlimit09 = new ZonedDecimalData(17, 0).isAPartOf(filler, 136);
  	public FixedLengthStringData usundlims = new FixedLengthStringData(153).isAPartOf(tr594Rec, 153);
  	public ZonedDecimalData[] usundlim = ZDArrayPartOfStructure(9, 17, 0, usundlims, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(153).isAPartOf(usundlims, 0, FILLER_REDEFINE);
  	public ZonedDecimalData usundlim01 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData usundlim02 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 17);
  	public ZonedDecimalData usundlim03 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 34);
  	public ZonedDecimalData usundlim04 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 51);
  	public ZonedDecimalData usundlim05 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 68);
  	public ZonedDecimalData usundlim06 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 85);
  	public ZonedDecimalData usundlim07 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 102);
  	public ZonedDecimalData usundlim08 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 119);
  	public ZonedDecimalData usundlim09 = new ZonedDecimalData(17, 0).isAPartOf(filler1, 136);
  	public FixedLengthStringData uwplntyps = new FixedLengthStringData(18).isAPartOf(tr594Rec, 306);
  	public FixedLengthStringData[] uwplntyp = FLSArrayPartOfStructure(9, 2, uwplntyps, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(18).isAPartOf(uwplntyps, 0, FILLER_REDEFINE);
  	public FixedLengthStringData uwplntyp01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData uwplntyp02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData uwplntyp03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData uwplntyp04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData uwplntyp05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData uwplntyp06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData uwplntyp07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData uwplntyp08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData uwplntyp09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(176).isAPartOf(tr594Rec, 324, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr594Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr594Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}