package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifeclnt;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LifepfDAOImpl extends BaseDAOImpl<Lifepf> implements LifepfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LifepfDAOImpl.class);
    private static final String SPLIT_SIGN = "_";

    public Map<Long, List<Lifepf>> searchLifeRecord(List<Long> covrUQ) {
        StringBuilder sqlLifeSelect1 = new StringBuilder(
                "SELECT L.LIFCNUM,L.CURRFROM,L.LIFE,L.JLIFE,L.SMOKING,L.CLTSEX,L.ANBCCD, CL.UNIQUE_NUMBER ");
        sqlLifeSelect1
                .append("FROM LIFEPF L,COVRPF CL WHERE L.CHDRCOY=CL.CHDRCOY AND L.CHDRNUM = CL.CHDRNUM AND L.LIFE=CL.LIFE AND L.JLIFE=CL.JLIFE AND  ");
        sqlLifeSelect1.append(getSqlInLong("CL.UNIQUE_NUMBER", covrUQ));

        sqlLifeSelect1.append(" ORDER BY L.CHDRCOY ASC, L.CHDRNUM ASC, L.LIFE ASC, L.JLIFE ASC, L.UNIQUE_NUMBER DESC ");

        PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect1.toString());
        ResultSet sqllifepf1rs = null;
        Map<Long, List<Lifepf>> lifepfSearchResult = new HashMap<Long, List<Lifepf>>();
        try {
            sqllifepf1rs = executeQuery(psLifeSelect);
            while (sqllifepf1rs.next()) {
                Lifepf lifepf = new Lifepf();
                lifepf.setLifcnum(sqllifepf1rs.getString(1));
                lifepf.setCurrfrom(sqllifepf1rs.getInt(2));
                lifepf.setLife(sqllifepf1rs.getString(3));
                lifepf.setJlife(sqllifepf1rs.getString(4));
                lifepf.setSmoking(sqllifepf1rs.getString(5));
                lifepf.setCltsex(sqllifepf1rs.getString(6));
                lifepf.setAnbAtCcd(sqllifepf1rs.getInt(7));
                long uniqueNumber = sqllifepf1rs.getLong(8);
                if (lifepfSearchResult.containsKey(uniqueNumber)) {
                    lifepfSearchResult.get(uniqueNumber).add(lifepf);
                } else {
                    List<Lifepf> lifepfList = new ArrayList<Lifepf>();
                    lifepfList.add(lifepf);
                    lifepfSearchResult.put(uniqueNumber, lifepfList);
                }
            }

        } catch (SQLException e) {
			LOGGER.error("searchLifeRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psLifeSelect, sqllifepf1rs);
        }
        return lifepfSearchResult;

    }

    public Map<String, List<Lifepf>> searchLifeRecordByActx(String coy, List<String> chdrnumList) {
        StringBuilder sqlLifeSelect1 = new StringBuilder(
                "SELECT L.LIFCNUM,L.CURRFROM,L.LIFE,L.JLIFE,L.SMOKING,L.CLTSEX,L.ANBCCD,L.CHDRCOY,L.CHDRNUM ");
        sqlLifeSelect1.append("FROM LIFEPF L WHERE L.CHDRCOY=? AND ");

        sqlLifeSelect1.append(getSqlInStr("L.CHDRNUM", chdrnumList));
        sqlLifeSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC ");

        PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect1.toString());
        ResultSet sqllifepf1rs = null;
        Map<String, List<Lifepf>> lifepfSearchResult = new HashMap<String, List<Lifepf>>();
        try {
            psLifeSelect.setString(1, coy);
            sqllifepf1rs = executeQuery(psLifeSelect);
            while (sqllifepf1rs.next()) {
                Lifepf lifepf = new Lifepf();
                lifepf.setLifcnum(sqllifepf1rs.getString(1));
                lifepf.setCurrfrom(sqllifepf1rs.getInt(2));
                lifepf.setLife(sqllifepf1rs.getString(3));
                lifepf.setJlife(sqllifepf1rs.getString(4));
                lifepf.setSmoking(sqllifepf1rs.getString(5));
                lifepf.setCltsex(sqllifepf1rs.getString(6));
                lifepf.setAnbAtCcd(sqllifepf1rs.getInt(7));
                lifepf.setChdrcoy(sqllifepf1rs.getString(8));
                lifepf.setChdrnum(sqllifepf1rs.getString(9));
                String key = lifepf.getChdrcoy() + this.getSplitSign() + lifepf.getChdrnum();
                if (lifepfSearchResult.containsKey(key)) {
                    lifepfSearchResult.get(key).add(lifepf);
                } else {
                    List<Lifepf> lifepfList = new ArrayList<Lifepf>();
                    lifepfList.add(lifepf);
                    lifepfSearchResult.put(key, lifepfList);
                }
            }

        } catch (SQLException e) {
			LOGGER.error("searchLifeRecordByActx()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psLifeSelect, sqllifepf1rs);
        }
        return lifepfSearchResult;

    }

    public String getSplitSign() {
        return SPLIT_SIGN;
    }

	@Override
	public List<Lifepf> getLifeList(String chdrcoy, String chdrnum) {
		StringBuilder sqlLifeSelect1 = new StringBuilder(
                "SELECT CHDRCOY,CHDRNUM,LIFCNUM, CURRFROM,CURRTO,LIFE, JLIFE, VALIDFLAG, LCDTE,LIFEREL,CLTDOB,CLTSEX,ANBCCD,AGEADM,SELECTION,SMOKING,OCCUP,PURSUIT01,PURSUIT02,MARTAL,SBSTDL,STATCODE,TERMID,TRDT,TRTM,USER_T,RELATION,UNIQUE_NUMBER ");
		sqlLifeSelect1.append("FROM LIFEPF WHERE CHDRCOY = ? AND CHDRNUM = ? 	");
    
		sqlLifeSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC ");//ILIFE-7776
     
   	
        PreparedStatement ps = getPrepareStatement(sqlLifeSelect1.toString());
   	 	ResultSet sqllifepf1rs = null;
        List<Lifepf> lifepfList = null;
    	 try {
 			 ps.setString(1,chdrcoy);
 			 ps.setString(2, chdrnum);
 			
 			
 			sqllifepf1rs = executeQuery(ps);
    		 lifepfList = new ArrayList<Lifepf>();
    		 while(sqllifepf1rs.next()){
    			 Lifepf lifepf = new Lifepf();
    			 lifepf.setChdrcoy(sqllifepf1rs.getString(1));
                 lifepf.setChdrnum(sqllifepf1rs.getString(2));
    			 lifepf.setLifcnum(sqllifepf1rs.getString(3));
                 lifepf.setCurrfrom(sqllifepf1rs.getInt(4));
                 lifepf.setCurrto(sqllifepf1rs.getInt(5));
                 lifepf.setLife(sqllifepf1rs.getString(6));
                 lifepf.setJlife(sqllifepf1rs.getString(7));
                 lifepf.setValidflag(sqllifepf1rs.getString(8));
                 lifepf.setLifeCommDate(sqllifepf1rs.getInt(9));
                 lifepf.setLiferel(sqllifepf1rs.getString(10));
                 lifepf.setCltdob(sqllifepf1rs.getInt(11));
                 lifepf.setCltsex(sqllifepf1rs.getString(12));
                 lifepf.setAnbAtCcd(sqllifepf1rs.getInt(13));
                 lifepf.setAgeadm(sqllifepf1rs.getString(14));
                 lifepf.setSelection(sqllifepf1rs.getString(15));
                 lifepf.setSmoking(sqllifepf1rs.getString(16));
                 lifepf.setOccup(sqllifepf1rs.getString(17));
                 lifepf.setPursuit01(sqllifepf1rs.getString(18));
                 lifepf.setPursuit02(sqllifepf1rs.getString(19));
                 lifepf.setMaritalState(sqllifepf1rs.getString(20));
                 lifepf.setSbstdl(sqllifepf1rs.getString(21));
                 lifepf.setStatcode(sqllifepf1rs.getString(22));
                 lifepf.setTermid(sqllifepf1rs.getString(23));
                 lifepf.setTransactionDate(sqllifepf1rs.getInt(24));
                 lifepf.setTransactionTime(sqllifepf1rs.getInt(25));
                 lifepf.setUser(sqllifepf1rs.getInt(26));
                 lifepf.setRelation(sqllifepf1rs.getString(27));
                 lifepf.setUniqueNumber(sqllifepf1rs.getLong(28));
                 lifepfList.add(lifepf);
				}
			} catch (SQLException e) {
			LOGGER.error("getLifeList()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, null);
	        }
	    	 return lifepfList;
	    	 }

	@Override
	public void updateLifeRecord(List<Lifepf> updateLifepflist, int busDate) {
		
		        if (updateLifepflist != null && updateLifepflist.size() > 0) {
		            String SQL_LIFE_UPDATE = "UPDATE LIFEPF SET VALIDFLAG=? ,CURRTO=?,USER_T=?,TRDT=?,TRTM=?,DATIME=? WHERE UNIQUE_NUMBER = ? ";

		            PreparedStatement psLifeUpdate = getPrepareStatement(SQL_LIFE_UPDATE);
		            try {
		                for (Lifepf l : updateLifepflist) {
		                	psLifeUpdate.setString(1,l.getValidflag());
		                	psLifeUpdate.setInt(2, l.getCurrto());
		                  	psLifeUpdate.setInt(3, l.getUser());
		                	psLifeUpdate.setInt(4, busDate);
		                	psLifeUpdate.setInt(5, l.getTransactionTime());
		                	psLifeUpdate.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
		                	psLifeUpdate.setLong(7, l.getUniqueNumber());	
		                	psLifeUpdate.addBatch();
		                }
		                psLifeUpdate.executeBatch();
		            } catch (SQLException e) {
				LOGGER.error("updateLifeRecord()", e); /* IJTI-1479 */
		                throw new SQLRuntimeException(e);
		            } finally {
		                close(psLifeUpdate, null);
		            }
		        }
		    }

	@Override
	public boolean insertLifepfList(List<Lifepf> insertLifepflist) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO LIFEPF(CHDRCOY,CHDRNUM,VALIDFLAG, STATCODE,TRANNO,CURRFROM,CURRTO,LIFE,JLIFE,LCDTE,LIFCNUM,LIFEREL,CLTDOB,CLTSEX,ANBCCD,AGEADM,SELECTION,SMOKING,OCCUP,PURSUIT01,PURSUIT02,MARTAL,SBSTDL,TERMID,TRDT,TRTM,USER_T,DATIME)");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Lifepf life : insertLifepflist) {
				ps.setString(1, life.getChdrcoy());
				ps.setString(2, life.getChdrnum());
				ps.setString(3, life.getValidflag());	
				ps.setString(4, life.getStatcode());
				ps.setInt(5, life.getTranno());
				ps.setInt(6, life.getCurrfrom());
				ps.setInt(7, life.getCurrto());
				ps.setString(8, life.getLife());
				ps.setString(9, life.getJlife());
				ps.setInt(10, life.getLifeCommDate());
				ps.setString(11, life.getLifcnum());
				ps.setString(12, life.getLiferel());
				ps.setInt(13, life.getCltdob());
				ps.setString(14, life.getCltsex());
				ps.setInt(15, life.getAnbAtCcd());
				ps.setString(16, life.getAgeadm());
				ps.setString(17, life.getSelection());
				ps.setString(18, life.getSmoking());
				ps.setString(19, life.getOccup());
				ps.setString(20, life.getPursuit01());
				ps.setString(21, life.getPursuit02());
				ps.setString(22, life.getMaritalState());
				ps.setString(23, life.getSbstdl());
				ps.setString(24, life.getTermid());
				ps.setInt(25, life.getTransactionDate());
				ps.setInt(26,life.getTransactionTime());
				ps.setInt(27, life.getUser());
				ps.setTimestamp(28, new Timestamp(System.currentTimeMillis())); 
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertLifepfList()", e); /* IJTI-1479 */	
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
		return isInsertSuccessful;
	}
	public String getOccRecord(String chdrcoy, String chdrnum,String life) {
		 StringBuilder sqlCovrSelect1 = new StringBuilder(
               "SELECT OCCUP");
       sqlCovrSelect1.append(" FROM LIFEPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND JLIFE = '00' ");
       sqlCovrSelect1
               .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
    
  		 PreparedStatement ps = null;
			ResultSet rs = null;
			String Occupation=null;

		try {
			ps = getPrepareStatement(sqlCovrSelect1.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			
			rs = ps.executeQuery();

			while (rs.next()) {
			Occupation= rs.getString(1);
			}
		} catch (SQLException e) {
			LOGGER.error("getOccRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return Occupation;
	}
	 // Introduced for ILIFE-3439
    public List<Lifepf> searchLifeRecordByChdrNum(String chdrCoy, String chdrnum){
        
    	StringBuilder sqlLifeSelect = new StringBuilder();
    	
    	sqlLifeSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, ");
    	sqlLifeSelect.append("STATCODE, TRANNO, CURRFROM, CURRTO, LIFE, JLIFE, LCDTE, LIFCNUM, ");
    	sqlLifeSelect.append("LIFEREL, CLTDOB, CLTSEX, ANBCCD, AGEADM, SELECTION, SMOKING, PURSUIT01, ");
    	sqlLifeSelect.append("PURSUIT02, MARTAL, SBSTDL, TRDT, TRTM, OCCUP ");
    	sqlLifeSelect.append("FROM LIFEPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
    	sqlLifeSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC");

        PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect.toString());
        ResultSet sqlLifepfRs = null;
        List<Lifepf> lifepfSearchResult = new ArrayList<Lifepf>();
        
        try {
        	
            psLifeSelect.setString(1, chdrCoy);
            psLifeSelect.setString(2, chdrnum);
            
            sqlLifepfRs = executeQuery(psLifeSelect);
            
            while (sqlLifepfRs.next()) {
            	
                Lifepf lifepf = new Lifepf();
                
                lifepf.setUniqueNumber(sqlLifepfRs.getInt("UNIQUE_NUMBER"));
                lifepf.setChdrcoy(sqlLifepfRs.getString("CHDRCOY"));
                lifepf.setChdrnum(sqlLifepfRs.getString("CHDRNUM"));
                lifepf.setValidflag(sqlLifepfRs.getString("VALIDFLAG"));
                lifepf.setStatcode(sqlLifepfRs.getString("STATCODE"));
                lifepf.setTranno(sqlLifepfRs.getInt("TRANNO"));
                lifepf.setCurrfrom(sqlLifepfRs.getInt("CURRFROM"));
                lifepf.setCurrto(sqlLifepfRs.getInt("CURRTO"));
                lifepf.setLife(sqlLifepfRs.getString("LIFE"));
                lifepf.setJlife(sqlLifepfRs.getString("JLIFE"));
                lifepf.setLifeCommDate(sqlLifepfRs.getInt("LCDTE"));
                lifepf.setLifcnum(sqlLifepfRs.getString("LIFCNUM"));
                lifepf.setLiferel(sqlLifepfRs.getString("LIFEREL"));
                lifepf.setCltdob(sqlLifepfRs.getInt("CLTDOB"));
                lifepf.setCltsex(sqlLifepfRs.getString("CLTSEX"));
                lifepf.setAnbAtCcd(sqlLifepfRs.getInt("ANBCCD"));
                lifepf.setAgeadm(sqlLifepfRs.getString("AGEADM"));
                lifepf.setSelection(sqlLifepfRs.getString("SELECTION"));
                lifepf.setSmoking(sqlLifepfRs.getString("SMOKING"));
                lifepf.setPursuit01(sqlLifepfRs.getString("PURSUIT01"));
                lifepf.setPursuit02(sqlLifepfRs.getString("PURSUIT02"));
                lifepf.setMaritalState(sqlLifepfRs.getString("MARTAL"));
                lifepf.setSbstdl(sqlLifepfRs.getString("SBSTDL"));
                lifepf.setTransactionDate(sqlLifepfRs.getInt("TRDT"));
                lifepf.setTransactionTime(sqlLifepfRs.getInt("TRTM"));
                lifepf.setOccup(sqlLifepfRs.getString("OCCUP"));
                lifepfSearchResult.add(lifepf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchLifeRecordByChdrNum()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psLifeSelect, sqlLifepfRs);
        }
        return lifepfSearchResult;

    }
    
    // Introduced for ILIFE-3439
    public List<Lifepf> searchLifeRecordByLifcNum(String chdrCoy, String lifcNum){
    	
    	StringBuilder sqlLifeSelect = new StringBuilder();
    	
    	sqlLifeSelect.append("SELECT CHDRCOY, CHDRNUM, LIFCNUM, LIFE, VALIDFLAG ");
    	sqlLifeSelect.append("FROM LIFEPF WHERE CHDRCOY = ? AND LIFCNUM = ? ");
    	sqlLifeSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFCNUM ASC");

        PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect.toString());
        ResultSet sqlLifepfRs = null;
        List<Lifepf> lifepfSearchResult = new ArrayList<Lifepf>();
        
        try {
        	
            psLifeSelect.setString(1, chdrCoy.trim());
            psLifeSelect.setString(2, lifcNum.trim());
            
            sqlLifepfRs = executeQuery(psLifeSelect);
            
            while (sqlLifepfRs.next()) {
            	
                Lifepf lifepf = new Lifepf();
                
                lifepf.setChdrcoy(sqlLifepfRs.getString(1));
                lifepf.setChdrnum(sqlLifepfRs.getString(2));
                lifepf.setLifcnum(sqlLifepfRs.getString(3));
                lifepf.setLife(sqlLifepfRs.getString(4));
                lifepf.setValidflag(sqlLifepfRs.getString(5)); //ILJ-108
                
                lifepfSearchResult.add(lifepf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchLifeRecordByChdrNum()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psLifeSelect, sqlLifepfRs);
        }
        return lifepfSearchResult;
    }
    
    // Introduced for ILIFE-3439
    public List<Lifepf> searchLifeRecordByLife(String chdrCoy, String chdrnum, String life, String jLife) throws SQLRuntimeException{
        
    	StringBuilder sqlLifeSelect = new StringBuilder();
    	
    	sqlLifeSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, ");
    	sqlLifeSelect.append("STATCODE, TRANNO, CURRFROM, CURRTO, LIFE, JLIFE, LCDTE, LIFCNUM, ");
    	sqlLifeSelect.append("LIFEREL, CLTDOB, CLTSEX, ANBCCD, AGEADM, SELECTION, SMOKING, PURSUIT01, ");
    	sqlLifeSelect.append("PURSUIT02, MARTAL, SBSTDL, TRDT, TRTM ");
    	sqlLifeSelect.append("FROM LIFEPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE=? and JLIFE=? ");
    	sqlLifeSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC");

        PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect.toString());
        ResultSet sqlLifepfRs = null;
        List<Lifepf> lifepfSearchResult = new ArrayList<Lifepf>();
        
        try {
        	
            psLifeSelect.setString(1, chdrCoy);
            psLifeSelect.setString(2, chdrnum);
            psLifeSelect.setString(3, life);
            psLifeSelect.setString(4, jLife);
            
            sqlLifepfRs = executeQuery(psLifeSelect);
            
            while (sqlLifepfRs.next()) {
            	
                Lifepf lifepf = new Lifepf();
                
                lifepf.setUniqueNumber(sqlLifepfRs.getInt("UNIQUE_NUMBER"));
                lifepf.setChdrcoy(sqlLifepfRs.getString("CHDRCOY"));
                lifepf.setChdrnum(sqlLifepfRs.getString("CHDRNUM"));
                lifepf.setValidflag(sqlLifepfRs.getString("VALIDFLAG"));
                lifepf.setStatcode(sqlLifepfRs.getString("STATCODE"));
                lifepf.setTranno(sqlLifepfRs.getInt("TRANNO"));
                lifepf.setCurrfrom(sqlLifepfRs.getInt("CURRFROM"));
                lifepf.setCurrto(sqlLifepfRs.getInt("CURRTO"));
                lifepf.setLife(sqlLifepfRs.getString("LIFE"));
                lifepf.setJlife(sqlLifepfRs.getString("JLIFE"));
                lifepf.setLifeCommDate(sqlLifepfRs.getInt("LCDTE"));
                lifepf.setLifcnum(sqlLifepfRs.getString("LIFCNUM"));
                lifepf.setLiferel(sqlLifepfRs.getString("LIFEREL"));
                lifepf.setCltdob(sqlLifepfRs.getInt("CLTDOB"));
                lifepf.setCltsex(sqlLifepfRs.getString("CLTSEX"));
                lifepf.setAnbAtCcd(sqlLifepfRs.getInt("ANBCCD"));
                lifepf.setAgeadm(sqlLifepfRs.getString("AGEADM"));
                lifepf.setSelection(sqlLifepfRs.getString("SELECTION"));
                lifepf.setSmoking(sqlLifepfRs.getString("SMOKING"));
                lifepf.setPursuit01(sqlLifepfRs.getString("PURSUIT01"));
                lifepf.setPursuit02(sqlLifepfRs.getString("PURSUIT02"));
                lifepf.setMaritalState(sqlLifepfRs.getString("MARTAL"));
                lifepf.setSbstdl(sqlLifepfRs.getString("SBSTDL"));
                lifepf.setTransactionDate(sqlLifepfRs.getInt("TRDT"));
                lifepf.setTransactionTime(sqlLifepfRs.getInt("TRTM"));
                
                lifepfSearchResult.add(lifepf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchLifeRecordByLife()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psLifeSelect, sqlLifepfRs);
        }
        return lifepfSearchResult;

    }
	public List<Lifepf> selectLifepfRecord(Lifepf lifepf)
    {
    	List<Lifepf> lifeResult = new ArrayList<Lifepf>();
     	 StringBuilder sb = new StringBuilder("");
     	sb.append("SELECT LIFCNUM,CURRFROM,LIFE,JLIFE,SMOKING,CLTSEX,ANBCCD,CHDRCOY,CHDRNUM,VALIDFLAG,STATCODE ");
     	sb.append("FROM LIFEPF WHERE CHDRCOY=? AND CHDRNUM=? ");
     	 if(lifepf.getLife() != null){     	
     		sb.append("AND LIFE=? ");
     	 }
     	sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC");
     	PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet sql1rs = null;
        try {
       	
	       	 ps.setString(1, lifepf.getChdrcoy());
	       	 ps.setString(2, lifepf.getChdrnum());
	       	if(lifepf.getLife() != null){
	       		ps.setString(3, lifepf.getLife());
	       	}
	       	
	       	 sql1rs = executeQuery(ps);
	
	       	 while (sql1rs.next()) {  
		        	lifepf = new Lifepf();
		        	
		        	lifepf.setLifcnum(sql1rs.getString(1));
		        	lifepf.setCurrfrom(sql1rs.getInt(2));
		        	lifepf.setLife(sql1rs.getString(3));  	        	
		        	lifepf.setJlife(sql1rs.getString(4));
		        	lifepf.setSmoking(sql1rs.getString(5));
		        	lifepf.setCltsex(sql1rs.getString(6));
		        	lifepf.setAnbAtCcd(sql1rs.getInt(7));
		        	lifepf.setChdrcoy(sql1rs.getString(8));
		        	lifepf.setChdrnum(sql1rs.getString(9));
		        	lifepf.setValidflag(sql1rs.getString(10));
		        	lifepf.setStatcode(sql1rs.getString(11));
		        	lifeResult.add(lifepf);
	       	 }
        }
        catch (SQLException e) {
			LOGGER.error("searchLifeRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, sql1rs);
        }
     	
     	return lifeResult;
    }
    //Ilfe-3310 by liwei
    public List<Lifepf> getLifeData(String chdrcoy,String chdrnum,String life,String jlife){
    	 StringBuilder sqlLifeSelect1 = new StringBuilder(
                 "SELECT LIFCNUM,CURRFROM,LIFE,JLIFE,SMOKING,CLTSEX,ANBCCD,CHDRCOY,CHDRNUM,Cltdob,ANBCCD,OCCUP,VALIDFLAG ");
         sqlLifeSelect1.append("FROM LIFEPF  WHERE CHDRCOY=? AND  ");

         sqlLifeSelect1.append("CHDRNUM=? and life=? and Jlife=? and VALIDFLAG in ('1', '3') ");
         sqlLifeSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC ");

         PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect1.toString());
         ResultSet sqllifepf1rs = null;
         List<Lifepf> list = new ArrayList<>();
         Lifepf lifepf = null;
         try {
             psLifeSelect.setString(1, chdrcoy);
             psLifeSelect.setString(2, chdrnum);
             psLifeSelect.setString(3, life);
             psLifeSelect.setString(4, jlife);
             sqllifepf1rs = executeQuery(psLifeSelect);
             while (sqllifepf1rs.next()) {
                 lifepf = new Lifepf();
                 lifepf.setLifcnum(sqllifepf1rs.getString(1));
                 lifepf.setCurrfrom(sqllifepf1rs.getInt(2));
                 lifepf.setLife(sqllifepf1rs.getString(3));
                 lifepf.setJlife(sqllifepf1rs.getString(4));
                 lifepf.setSmoking(sqllifepf1rs.getString(5));
                 lifepf.setCltsex(sqllifepf1rs.getString(6));
                 lifepf.setAnbAtCcd(sqllifepf1rs.getInt(7));
                 lifepf.setChdrcoy(sqllifepf1rs.getString(8));
                 lifepf.setChdrnum(sqllifepf1rs.getString(9));
                 lifepf.setCltdob(sqllifepf1rs.getInt(10));
                 lifepf.setAnbAtCcd(sqllifepf1rs.getInt(11));
                 lifepf.setOccup(sqllifepf1rs.getString(12));
                 lifepf.setValidflag(sqllifepf1rs.getString(13));
                 list.add(lifepf);
             }

         } catch (SQLException e) {
			LOGGER.error("getLifeData()", e); /* IJTI-1479 */
             throw new SQLRuntimeException(e);
         } finally {
             close(psLifeSelect, sqllifepf1rs);
         }
         return list;
    }
    
  public List<Lifepf> selectLifeDetails(Lifepf lifepfData) {
    	
    	Lifepf lifepf = null;
		List<Lifepf> data = new LinkedList<Lifepf>();
		PreparedStatement psCovrSelect = null;
		ResultSet sqlcovrpf1rs = null;
		StringBuilder sqlCovrSelectBuilder = new StringBuilder();
		
		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovrSelectBuilder.append("SELECT LIFCNUM, CLTSEX, CLTDOB, CHDRCOY, CHDRNUM, LIFE, JLIFE");
	    sqlCovrSelectBuilder.append(" FROM LIFEMJA WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND VALIDFLAG = ?");
		sqlCovrSelectBuilder.append(" ORDER BY ");
	    sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC ");
		
		psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
		
		try {
			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovrSelect.setString(1, lifepfData.getChdrcoy());
			psCovrSelect.setString(2, lifepfData.getChdrnum());
			psCovrSelect.setString(3, lifepfData.getLife());
		//	psCovrSelect.setString(4, lifepfData.getJlife());
			psCovrSelect.setString(4, lifepfData.getValidflag());
		    
			
			
			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlcovrpf1rs = executeQuery(psCovrSelect);


			while (sqlcovrpf1rs.next()) {
				
				lifepf = new Lifepf();
				
				lifepf.setLifcnum(sqlcovrpf1rs.getString(1));
				lifepf.setCltsex(sqlcovrpf1rs.getString(2));
				lifepf.setCltdob(sqlcovrpf1rs.getInt(3));
				lifepf.setChdrcoy(sqlcovrpf1rs.getString(4));
				lifepf.setChdrnum(sqlcovrpf1rs.getString(5));
				lifepf.setLife(sqlcovrpf1rs.getString(6));
				lifepf.setJlife(sqlcovrpf1rs.getString(7));
	            data.add(lifepf);
				
               }

		} catch (SQLException e) {
			LOGGER.error("selectCovrClMData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrSelect,sqlcovrpf1rs);
			
			}
		return data;
	}
  
  public Map<String ,Lifepf> getLifeEnqData(Lifepf lifepfModel){
      
	     Map<String, Lifepf>   retentionMap = new HashMap<String, Lifepf>();
	         StringBuilder sqlLifeSelect1 = new StringBuilder(
	                 "SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,LIFCNUM,SMOKING,OCCUP,PURSUIT01,PURSUIT02,VALIDFLAG");
	         sqlLifeSelect1.append(" FROM LIFEENQ ");

	         sqlLifeSelect1.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND JLIFE = ? AND VALIDFLAG = '1' ");
	        sqlLifeSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, VALIDFLAG ASC ");

	         PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect1.toString());
	         
	         
	         ResultSet sqllifepf1rs = null;
	         try {
	             psLifeSelect.setString(1,lifepfModel.getChdrcoy());
	             psLifeSelect.setString(2,lifepfModel.getChdrnum());
	             psLifeSelect.setString(3,lifepfModel.getLife());
	             psLifeSelect.setString(4,lifepfModel.getJlife());
	          
	             
	             sqllifepf1rs = executeQuery(psLifeSelect);
	             while (sqllifepf1rs.next()) {
	                 Lifepf lifepf = new Lifepf();
	                 lifepf.setChdrcoy(sqllifepf1rs.getString(1));
	                 lifepf.setChdrnum(sqllifepf1rs.getString(2));
	                 lifepf.setLife(sqllifepf1rs.getString(3));
	                 lifepf.setJlife(sqllifepf1rs.getString(4));
	                 lifepf.setLifcnum(sqllifepf1rs.getString(5));
	                 lifepf.setSmoking(sqllifepf1rs.getString(6));
	                 lifepf.setOccup(sqllifepf1rs.getString(7));
	                 lifepf.setPursuit01(sqllifepf1rs.getString(8));
	                 lifepf.setPursuit02(sqllifepf1rs.getString(9));
	                 lifepf.setValidflag(sqllifepf1rs.getString(10));
	              //   lifepfSearchResult.add(lifepf);
	                  retentionMap.put(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim()+""+lifepf.getValidflag().trim() , lifepf); 
	                   
	             }

	         } catch (SQLException e) {
			LOGGER.error("getLifeEnqData()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(psLifeSelect, sqllifepf1rs);
	         }
	         return retentionMap;
	     }              
  public Lifepf getLifeRecord(String chdrcoy, String chdrnum, String life, String jlife) {
  	Lifepf lifepf = null;//ILB-1164
  	
  	StringBuilder sql = new StringBuilder("SELECT * FROM LIFEPF");
  	sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=?");
  	PreparedStatement ps=null;
  	ResultSet rs=null;
  	
  	try {
  		ps=getPrepareStatement(sql.toString());
  		ps.setString(1, chdrcoy);
  		ps.setString(2, chdrnum);
  		ps.setString(3, life);
  		ps.setString(4, jlife);
  		rs=ps.executeQuery();
  		while(rs.next()) {
  			lifepf = new Lifepf();//ILB-1164
  			lifepf.setUniqueNumber(rs.getLong(1));
  			lifepf.setChdrcoy(rs.getString(2));
  			lifepf.setChdrnum(rs.getString(3));
  			lifepf.setValidflag(rs.getString(4));
  			lifepf.setStatcode(rs.getString(5));
  			lifepf.setTranno(rs.getInt(6));
  			lifepf.setCurrfrom(rs.getInt(7));
  			lifepf.setCurrto(rs.getInt(8));
  			lifepf.setLife(rs.getString(9));
  			lifepf.setJlife(rs.getString(10));
  			lifepf.setLifeCommDate(rs.getInt(11));
  			lifepf.setLifcnum(rs.getString(12));
  			lifepf.setLiferel(rs.getString(13));
  			lifepf.setCltdob(rs.getInt(14));
  			lifepf.setCltsex(rs.getString(15));
  			lifepf.setAnbAtCcd(rs.getInt(16));
  			lifepf.setAgeadm(rs.getString(17));
  			lifepf.setSelection(rs.getString(18));
  			lifepf.setSmoking(rs.getString(19));
  			lifepf.setOccup(rs.getString(20));
  			lifepf.setPursuit01(rs.getString(21));
  			lifepf.setPursuit02(rs.getString(22));
  			lifepf.setMaritalState(rs.getString(23));
  			lifepf.setSbstdl(rs.getString(24));
  			lifepf.setTermid(rs.getString(25));
  			lifepf.setTransactionDate(rs.getInt(26));
  			lifepf.setTransactionTime(rs.getInt(27));
  			lifepf.setUser(rs.getInt(28));
  		}
  	}
  	catch (SQLException e) {
			LOGGER.error("searchLifeRecordByActx()", e); /* IJTI-1479 */
          throw new SQLRuntimeException(e);
      } finally {
          close(ps, rs);
      }
  	return lifepf;
  }
 
  
  public List<Lifepf> getLifeRecords(String chdrcoy, String chdrnum, String validflag) {
  	Lifepf lifepf;
  	List<Lifepf> lifeList = new ArrayList<Lifepf>();
  	StringBuilder sql = new StringBuilder("SELECT * FROM LIFEPF");
  	sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,UNIQUE_NUMBER");
  	PreparedStatement ps=null;
  	ResultSet rs=null;
  	
  	try {
  		ps=getPrepareStatement(sql.toString());
  		ps.setString(1, chdrcoy);
  		ps.setString(2, chdrnum);
  		ps.setString(3, validflag);
  		rs=ps.executeQuery();
  		while(rs.next()) {
  			lifepf = new Lifepf();
  			lifepf.setUniqueNumber(rs.getLong(1));
  			lifepf.setChdrcoy(rs.getString(2));
  			lifepf.setChdrnum(rs.getString(3));
  			lifepf.setValidflag(rs.getString(4));
  			lifepf.setStatcode(rs.getString(5));
  			lifepf.setTranno(rs.getInt(6));
  			lifepf.setCurrfrom(rs.getInt(7));
  			lifepf.setCurrto(rs.getInt(8));
  			lifepf.setLife(rs.getString(9));
  			lifepf.setJlife(rs.getString(10));
  			lifepf.setLifeCommDate(rs.getInt(11));
  			lifepf.setLifcnum(rs.getString(12));
  			lifepf.setLiferel(rs.getString(13));
  			lifepf.setCltdob(rs.getInt(14));
  			lifepf.setCltsex(rs.getString(15));
  			lifepf.setAnbAtCcd(rs.getInt(16));
  			lifepf.setAgeadm(rs.getString(17));
  			lifepf.setSelection(rs.getString(18));
  			lifepf.setSmoking(rs.getString(19));
  			lifepf.setOccup(rs.getString(20));
  			lifepf.setPursuit01(rs.getString(21));
  			lifepf.setPursuit02(rs.getString(22));
  			lifepf.setMaritalState(rs.getString(23));
  			lifepf.setSbstdl(rs.getString(24));
  			lifepf.setTermid(rs.getString(25));
  			lifepf.setTransactionDate(rs.getInt(26));
  			lifepf.setTransactionTime(rs.getInt(27));
  			lifepf.setUser(rs.getInt(28));
  			lifeList.add(lifepf);
  		}
  	}
  	catch (SQLException e) {
			LOGGER.error("searchLifeRecordByActx()", e); /* IJTI-1479 */
          throw new SQLRuntimeException(e);
      } finally {
          close(ps, rs);
      }
  	return lifeList;
  }
  
  public List<Lifeclnt> searchLifeClntRecord(String chdrcoy,String chdrnum,String jlife, String cltCoy, String life) {
      StringBuilder sqlLifeSelect1 = new StringBuilder("SELECT L.LIFCNUM,L.LIFE,L.JLIFE, C1.GIVNAME,C1.SURNAME,C1.CLTTYPE,C1.LSURNAME,C1.LGIVNAME FROM LIFEPF L,CLNTPF C1");
      sqlLifeSelect1.append(" WHERE L.VALIDFLAG='1' AND L.CHDRCOY=? AND L.CHDRNUM=? AND L.LIFE=? AND L.JLIFE=? AND C1.CLNTNUM=L.LIFCNUM AND C1.CLNTCOY=? AND C1.CLNTPFX='CN'");

      sqlLifeSelect1.append(" ORDER BY L.CHDRCOY, L.CHDRNUM, L.LIFE, L.JLIFE, L.UNIQUE_NUMBER DESC,C1.CLNTPFX ASC, C1.CLNTCOY ASC, C1.CLNTNUM ASC, C1.UNIQUE_NUMBER DESC ");

      PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect1.toString());
      ResultSet sqllifepf1rs = null;
      List<Lifeclnt> lifepfSearchResult = new LinkedList<Lifeclnt>();
      try {
          int i = 1;
          psLifeSelect.setString(i++, chdrcoy);
          psLifeSelect.setString(i++, chdrnum);
          psLifeSelect.setString(i++, life);
          psLifeSelect.setString(i++, jlife);
          psLifeSelect.setString(i++, cltCoy);
          
          sqllifepf1rs = executeQuery(psLifeSelect);
          
          while (sqllifepf1rs.next()) {
              Lifeclnt p6351DTO = new  Lifeclnt();
              p6351DTO.setLifcnum(sqllifepf1rs.getString(1));
              p6351DTO.setLife(sqllifepf1rs.getString(2));
              p6351DTO.setJlife(sqllifepf1rs.getString(3));
              p6351DTO.setClgivname(sqllifepf1rs.getString(4));
              p6351DTO.setClsurname(sqllifepf1rs.getString(5));
              p6351DTO.setClttype(sqllifepf1rs.getString(6));
              p6351DTO.setCllgivname(sqllifepf1rs.getString(7));
              p6351DTO.setCllsurname(sqllifepf1rs.getString(8));
              lifepfSearchResult.add(p6351DTO);
          }

      } catch (SQLException e) {
			LOGGER.error("searchLifeClntRecord()", e); /* IJTI-1479 */
          throw new SQLRuntimeException(e);
      } finally {
          close(psLifeSelect, sqllifepf1rs);
      }
      return lifepfSearchResult;
  }
  //ILIFE-4940 by liwei
  public List<Lifeclnt> searchLifeClntRecord(String chdrcoy,String chdrnum,String jlife,String life,String cowncoy,String cownpfx){
	  StringBuilder sqlLifeSelect1 = new StringBuilder("SELECT C.LSURNAME,C.LGIVNAME FROM VM1DTA.LIFELNB L LEFT JOIN CLNTPF C ");
      sqlLifeSelect1.append(" ON C.CLNTNUM=L.LIFCNUM WHERE L.CHDRCOY=? AND L.CHDRNUM=? AND L.LIFE=? AND L.JLIFE=? AND C.CLNTCOY=? AND CLNTPFX=?");

      sqlLifeSelect1.append(" ORDER BY L.CHDRCOY, L.CHDRNUM, L.LIFE, L.JLIFE");

      PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect1.toString());
      ResultSet sqllifepf1rs = null;
      List<Lifeclnt> lifepfSearchResult = new LinkedList<Lifeclnt>();
      try {
          int i = 1;
          psLifeSelect.setString(i++, chdrcoy);
          psLifeSelect.setString(i++, chdrnum);
          psLifeSelect.setString(i++, life);
          psLifeSelect.setString(i++, jlife);
          psLifeSelect.setString(i++, cowncoy);
          psLifeSelect.setString(i++, cownpfx);
          sqllifepf1rs = executeQuery(psLifeSelect);
          
          while (sqllifepf1rs.next()) {
              Lifeclnt lifeclnt = new  Lifeclnt();
              lifeclnt.setCllgivname(sqllifepf1rs.getString(1));
              lifeclnt.setCllsurname(sqllifepf1rs.getString(2));
              lifepfSearchResult.add(lifeclnt);
          }

      } catch (SQLException e) {
			LOGGER.error("searchLifeClntRecord()", e); /* IJTI-1479 */
          throw new SQLRuntimeException(e);
      } finally {
          close(psLifeSelect, sqllifepf1rs);
      }
      return lifepfSearchResult;
  }
  public Map<String, List<Lifepf>> searchLifelnbRecord(String chdrcoy, List<String> chdrnumList) {
      StringBuilder sqlLifeSelect1 = new StringBuilder(
              "SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,VALIDFLAG,STATCODE,TRANNO,CURRFROM,CURRTO,LIFE,JLIFE,LCDTE,LIFCNUM,LIFEREL,CLTDOB,CLTSEX,ANBCCD,AGEADM,SELECTION,SMOKING,OCCUP,PURSUIT01,PURSUIT02,MARTAL,SBSTDL ");
      sqlLifeSelect1.append("FROM LIFEPF WHERE ");
      sqlLifeSelect1.append("CHDRCOY=? AND ");  //ILIFE-8901
      sqlLifeSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
      sqlLifeSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC ");

      PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect1.toString());
      ResultSet sqllifepf1rs = null;
      Map<String, List<Lifepf>> lifepfSearchResult = new HashMap<String, List<Lifepf>>();
      try {
    	  psLifeSelect.setString(1, chdrcoy);
          sqllifepf1rs = executeQuery(psLifeSelect);
          while (sqllifepf1rs.next()) {
              Lifepf lifepf = new Lifepf();
              lifepf.setChdrcoy(sqllifepf1rs.getString("chdrcoy"));
              lifepf.setChdrnum(sqllifepf1rs.getString("chdrnum"));
              lifepf.setValidflag(sqllifepf1rs.getString("validflag"));
              lifepf.setStatcode(sqllifepf1rs.getString("statcode"));
              lifepf.setTranno(sqllifepf1rs.getInt("tranno"));
              lifepf.setCurrfrom(sqllifepf1rs.getInt("currfrom"));
              lifepf.setCurrto(sqllifepf1rs.getInt("currto"));
              lifepf.setLife(sqllifepf1rs.getString("life"));
              lifepf.setJlife(sqllifepf1rs.getString("jlife"));
              lifepf.setLifeCommDate(sqllifepf1rs.getInt("lcdte"));
              lifepf.setLifcnum(sqllifepf1rs.getString("lifcnum"));
              lifepf.setLiferel(sqllifepf1rs.getString("liferel"));
              lifepf.setCltdob(sqllifepf1rs.getInt("cltdob"));
              lifepf.setCltsex(sqllifepf1rs.getString("cltsex"));
              lifepf.setAnbAtCcd(sqllifepf1rs.getInt("anbccd"));
              lifepf.setAgeadm(sqllifepf1rs.getString("ageadm"));
              lifepf.setSelection(sqllifepf1rs.getString("selection"));
              lifepf.setSmoking(sqllifepf1rs.getString("smoking"));
              lifepf.setOccup(sqllifepf1rs.getString("occup"));
              lifepf.setPursuit01(sqllifepf1rs.getString("pursuit01"));
              lifepf.setPursuit02(sqllifepf1rs.getString("pursuit02"));
              lifepf.setMaritalState(sqllifepf1rs.getString("martal"));
              lifepf.setSbstdl(sqllifepf1rs.getString("sbstdl"));
              
              if (lifepfSearchResult.containsKey(lifepf.getChdrnum())) {
                  lifepfSearchResult.get(lifepf.getChdrnum()).add(lifepf);
              } else {
                  List<Lifepf> lifepfList = new ArrayList<Lifepf>();
                  lifepfList.add(lifepf);
                  lifepfSearchResult.put(lifepf.getChdrnum(), lifepfList);
              }
          }

      } catch (SQLException e) {
			LOGGER.error("searchLifelnbRecord()", e); /* IJTI-1479 */
          throw new SQLRuntimeException(e);
      } finally {
          close(psLifeSelect, sqllifepf1rs);
      }
      return lifepfSearchResult;

  }

  
  public List<Lifepf> getLifelnb(String chdrcoy, String chdrnum, String life, String[] jlife){
	  
	  
	    StringBuilder jlifecondition = new StringBuilder("");
	  
		for(int i =0; i <jlife.length;i++){
			if(i==0){
				jlifecondition.append("'").append(jlife[i]).append("'");
			}else{
				jlifecondition.append(",'").append(jlife[i]).append("'");
			}
		}
		
	    
		StringBuilder sql = new StringBuilder("SELECT * FROM LIFEPF  WHERE CHDRCOY=? and CHDRNUM=? and LIFE=? and JLIFE in (").append(jlifecondition);
	  	sql.append(") order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC");
	  	
	  	PreparedStatement ps=null;
	  	ResultSet rs=null;
	  	Lifepf lifepf = null;
	  	List<Lifepf> list = new ArrayList<Lifepf>();
	  	try {
	  		ps=getPrepareStatement(sql.toString());
	  		ps.setString(1, chdrcoy);
	  		ps.setString(2, chdrnum);
	  		ps.setString(3, life);
	  		rs=ps.executeQuery();
	  		
	  		while(rs.next()) {
	  			lifepf = new Lifepf();
	  			lifepf.setUniqueNumber(rs.getLong(1));
	  			lifepf.setChdrcoy(rs.getString(2));
	  			lifepf.setChdrnum(rs.getString(3));
	  			lifepf.setValidflag(rs.getString(4));
	  			lifepf.setStatcode(rs.getString(5));
	  			lifepf.setTranno(rs.getInt(6));
	  			lifepf.setCurrfrom(rs.getInt(7));
	  			lifepf.setCurrto(rs.getInt(8));
	  			lifepf.setLife(rs.getString(9));
	  			lifepf.setJlife(rs.getString(10));
	  			lifepf.setLifeCommDate(rs.getInt(11));
	  			lifepf.setLifcnum(rs.getString(12));
	  			lifepf.setLiferel(rs.getString(13));
	  			lifepf.setCltdob(rs.getInt(14));
	  			lifepf.setCltsex(rs.getString(15));
	  			lifepf.setAnbAtCcd(rs.getInt(16));
	  			lifepf.setAgeadm(rs.getString(17));
	  			lifepf.setSelection(rs.getString(18));
	  			lifepf.setSmoking(rs.getString(19));
	  			lifepf.setOccup(rs.getString(20));
	  			lifepf.setPursuit01(rs.getString(21));
	  			lifepf.setPursuit02(rs.getString(22));
	  			lifepf.setMaritalState(rs.getString(23));
	  			lifepf.setSbstdl(rs.getString(24));
	  			lifepf.setTermid(rs.getString(25));
	  			lifepf.setTransactionDate(rs.getInt(26));
	  			lifepf.setTransactionTime(rs.getInt(27));
	  			lifepf.setUser(rs.getInt(28));
	  			list.add(lifepf);
	  		}
	  	}catch (SQLException e) {
			LOGGER.error("searchLifeRecordByActx()", e); /* IJTI-1479 */
	          throw new SQLRuntimeException(e);
	      } finally {
	          close(ps, rs);
	      }
	  	
	  	return list;
  }
  
  public void updateLifeRecord(List<Lifepf> updateLifepflist) {
		
      if (updateLifepflist != null && !updateLifepflist.isEmpty()) {
          String SQL_LIFE_UPDATE = " UPDATE LIFEPF SET VALIDFLAG=?, TRANNO=? ,TERMID=?, USER_T=?,TRDT=?,TRTM=?,STATCODE=?,DATIME=? WHERE UNIQUE_NUMBER = ?";

          PreparedStatement psLifeUpdate = getPrepareStatement(SQL_LIFE_UPDATE);
          try {
              for (Lifepf l : updateLifepflist) {
            	psLifeUpdate.setString(1, l.getValidflag());
              	psLifeUpdate.setInt(2,l.getTranno());
              	psLifeUpdate.setString(3, l.getTermid());
                psLifeUpdate.setInt(4, l.getUser());
              	psLifeUpdate.setInt(5, l.getTransactionDate());
              	psLifeUpdate.setInt(6, l.getTransactionTime());
              	psLifeUpdate.setString(7, l.getStatcode());
              	psLifeUpdate.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
              	psLifeUpdate.setLong(9, l.getUniqueNumber());           	
              	psLifeUpdate.addBatch();
              }
              psLifeUpdate.executeBatch();
          } catch (SQLException e) {
				LOGGER.error("updateLifeRecord()", e); /* IJTI-1479 */
              throw new SQLRuntimeException(e);
          } finally {
              close(psLifeUpdate, null);
          }
      }
  }
	public List<Lifepf> getLfcllnbRecords(String chdrcoy, String chdrnum) {
		Lifepf lifepf;
		List<Lifepf> lifeList = new ArrayList<Lifepf>();
		StringBuilder sql = new StringBuilder("SELECT * FROM LFCLLNB");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? ");
		PreparedStatement ps=null;
		ResultSet rs=null;

		try {
			ps=getPrepareStatement(sql.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			rs=ps.executeQuery();
			while(rs.next()) {
				lifepf = new Lifepf();
				lifepf.setUniqueNumber(rs.getLong(1));
				lifepf.setChdrcoy(rs.getString(2));
				lifepf.setChdrnum(rs.getString(3));
				lifepf.setValidflag(rs.getString(4));
				lifepf.setStatcode(rs.getString(5));
				lifepf.setTranno(rs.getInt(6));
				lifepf.setCurrfrom(rs.getInt(7));
				lifepf.setCurrto(rs.getInt(8));
				lifepf.setLife(rs.getString(9));
				lifepf.setJlife(rs.getString(10));
				lifepf.setLifeCommDate(rs.getInt(11));
				lifepf.setLifcnum(rs.getString(12));
				lifepf.setLiferel(rs.getString(13));
				lifepf.setCltdob(rs.getInt(14));
				lifepf.setCltsex(rs.getString(15));
				lifepf.setAnbAtCcd(rs.getInt(16));
				lifepf.setAgeadm(rs.getString(17));
				lifepf.setSelection(rs.getString(18));
				lifepf.setSmoking(rs.getString(19));
				lifepf.setOccup(rs.getString(20));
				lifepf.setPursuit01(rs.getString(21));
				lifepf.setPursuit02(rs.getString(22));
				lifepf.setMaritalState(rs.getString(23));
				lifepf.setSbstdl(rs.getString(24));
				lifepf.setTermid(rs.getString(25));
				lifepf.setTransactionDate(rs.getInt(26));
				lifepf.setTransactionTime(rs.getInt(27));
				lifepf.setUser(rs.getInt(28));
				lifeList.add(lifepf);
			}
		}
		catch (SQLException e) {
			LOGGER.error("searchLifeRecordByActx()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return lifeList;
	}
	
  public Lifepf getLifeEnqRecord(String chdrcoy, String chdrnum, String life, String jlife){
	      
     StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, LIFCNUM, SMOKING, OCCUP, PURSUIT01, PURSUIT02, VALIDFLAG, CLTSEX,CLTDOB FROM LIFEPF ");
     sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? AND VALIDFLAG='1' ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC");

     PreparedStatement ps = getPrepareStatement(sb.toString());
	 ResultSet rs = null;
	 Lifepf lifepf = null;
	 try {
		 
	   ps.setString(1, chdrcoy);
	   ps.setString(2, chdrnum);
	   ps.setString(3, life);
	   ps.setString(4, jlife);
		          
	   rs = ps.executeQuery();
	   
	    if (rs.next()) {
		  
		   lifepf = new Lifepf();
		   lifepf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
		   lifepf.setChdrcoy(rs.getString("CHDRCOY"));
		   lifepf.setChdrnum(rs.getString("CHDRNUM"));
		   lifepf.setLife(rs.getString("LIFE"));
		   lifepf.setJlife(rs.getString("JLIFE"));
		   lifepf.setLifcnum(rs.getString("LIFCNUM"));
		   lifepf.setSmoking(rs.getString("SMOKING"));
		   lifepf.setOccup(rs.getString("OCCUP"));
		   lifepf.setPursuit01(rs.getString("PURSUIT01"));
		   lifepf.setPursuit02(rs.getString("PURSUIT02"));
		   lifepf.setValidflag(rs.getString("VALIDFLAG")); 
		   // ILIFE-7113 - Starts
		   lifepf.setCltsex(rs.getString("CLTSEX"));    
		   lifepf.setCltdob(rs.getInt("CLTDOB"));  //ILIFe-7113 -ends
	     }

	   } catch (SQLException e) {
			LOGGER.error("getLifeEnqRecord()", e); /* IJTI-1479 */
		  throw new SQLRuntimeException(e);
       } finally {
		 close(ps, rs);
      }
     return lifepf;
    }  
  
	public List<Lifepf> getLfRecords(String chdrcoy, String chdrnum, String jlife) {
		Lifepf lifepf;
		List<Lifepf> lifeList = new ArrayList<Lifepf>();
		StringBuilder sql = new StringBuilder("SELECT * FROM LIFEPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND JLIFE=?");
		PreparedStatement ps=null;
		ResultSet rs=null;

		try {
			ps=getPrepareStatement(sql.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, jlife);
			rs=ps.executeQuery();
			while(rs.next()) {
				lifepf = new Lifepf();
				lifepf.setUniqueNumber(rs.getLong(1));
				lifepf.setChdrcoy(rs.getString(2));
				lifepf.setChdrnum(rs.getString(3));
				lifepf.setValidflag(rs.getString(4));
				lifepf.setStatcode(rs.getString(5));
				lifepf.setTranno(rs.getInt(6));
				lifepf.setCurrfrom(rs.getInt(7));
				lifepf.setCurrto(rs.getInt(8));
				lifepf.setLife(rs.getString(9));
				lifepf.setJlife(rs.getString(10));
				lifepf.setLifeCommDate(rs.getInt(11));
				lifepf.setLifcnum(rs.getString(12));
				lifepf.setLiferel(rs.getString(13));
				lifepf.setCltdob(rs.getInt(14));
				lifepf.setCltsex(rs.getString(15));
				lifepf.setAnbAtCcd(rs.getInt(16));
				lifepf.setAgeadm(rs.getString(17));
				lifepf.setSelection(rs.getString(18));
				lifepf.setSmoking(rs.getString(19));
				lifepf.setOccup(rs.getString(20));
				lifepf.setPursuit01(rs.getString(21));
				lifepf.setPursuit02(rs.getString(22));
				lifepf.setMaritalState(rs.getString(23));
				lifepf.setSbstdl(rs.getString(24));
				lifepf.setTermid(rs.getString(25));
				lifepf.setTransactionDate(rs.getInt(26));
				lifepf.setTransactionTime(rs.getInt(27));
				lifepf.setUser(rs.getInt(28));
				lifeList.add(lifepf);
			}
		}
		catch (SQLException e) {
			LOGGER.error("getLfRecords()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return lifeList;
	}

	public List<Lifepf> getLifematRecords(String chdrcoy, String chdrnum,String life) {
		Lifepf lifepf;
		List<Lifepf> lifeList = new ArrayList<Lifepf>();
		StringBuilder sql = new StringBuilder("SELECT * FROM LIFEMAT");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=?");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			ps=getPrepareStatement(sql.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			rs=ps.executeQuery();
			while(rs.next()) {
				lifepf = new Lifepf();
				lifepf.setUniqueNumber(rs.getLong(1));
				lifepf.setChdrcoy(rs.getString(2));
				lifepf.setChdrnum(rs.getString(3));
				lifepf.setValidflag(rs.getString(4));
				lifepf.setStatcode(rs.getString(5));
				lifepf.setTranno(rs.getInt(6));
				lifepf.setCurrfrom(rs.getInt(7));
				lifepf.setCurrto(rs.getInt(8));
				lifepf.setLife(rs.getString(9));
				lifepf.setJlife(rs.getString(10));
				lifepf.setLifcnum(rs.getString(11));
				lifeList.add(lifepf);
			}
		}
		catch (SQLException e) {
			LOGGER.error("searchLifeRecordByActx()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return lifeList;
	}

	@Override
	public void updateOccup(Lifepf pf) {
		String SQL_LIFE_UPDATE = " UPDATE LIFEPF SET OCCUP=?, USER_T=?,TRDT=?,TRTM=?,DATIME=? WHERE UNIQUE_NUMBER = ?";

		PreparedStatement psLifeUpdate = getPrepareStatement(SQL_LIFE_UPDATE);
		try {
			psLifeUpdate.setString(1, pf.getOccup());
			psLifeUpdate.setInt(2, pf.getUser());
			psLifeUpdate.setInt(3, pf.getTransactionDate());
			psLifeUpdate.setInt(4, pf.getTransactionTime());
			psLifeUpdate.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			psLifeUpdate.setLong(6, pf.getUniqueNumber());		
			psLifeUpdate.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateOccup()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psLifeUpdate, null);
		}
	}

	
	/*	START OF ILIFE-6911 Retrieving data based on lifcnum and chdrnum*/
	public Lifepf getLifeRecordByLifcNum(String chdrcoy, String chdrnum, String lifecNum){
		Lifepf lifepf = null;
		String sql = "SELECT * FROM LIFEPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFCNUM=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps=getPrepareStatement(sql);
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, lifecNum);
			rs=ps.executeQuery();
			while(rs.next()) {
				lifepf = new Lifepf();
				lifepf.setUniqueNumber(rs.getLong(1));
				lifepf.setChdrcoy(rs.getString(2));
				lifepf.setChdrnum(rs.getString(3));
				lifepf.setValidflag(rs.getString(4));
				lifepf.setStatcode(rs.getString(5));
				lifepf.setTranno(rs.getInt(6));
				lifepf.setCurrfrom(rs.getInt(7));
				lifepf.setCurrto(rs.getInt(8));
				lifepf.setLife(rs.getString(9));
				lifepf.setJlife(rs.getString(10));
				lifepf.setLifeCommDate(rs.getInt(11));
				lifepf.setLifcnum(rs.getString(12));
				lifepf.setLiferel(rs.getString(13));
				lifepf.setCltdob(rs.getInt(14));
				lifepf.setCltsex(rs.getString(15));
				lifepf.setAnbAtCcd(rs.getInt(16));
				lifepf.setAgeadm(rs.getString(17));
				lifepf.setSelection(rs.getString(18));
				lifepf.setSmoking(rs.getString(19));
				lifepf.setOccup(rs.getString(20));
				lifepf.setPursuit01(rs.getString(21));
				lifepf.setPursuit02(rs.getString(22));
				lifepf.setMaritalState(rs.getString(23));
				lifepf.setSbstdl(rs.getString(24));
				lifepf.setTermid(rs.getString(25));
				lifepf.setTransactionDate(rs.getInt(26));
				lifepf.setTransactionTime(rs.getInt(27));
				lifepf.setUser(rs.getInt(28));
				lifepf.setRelation(rs.getString(29));//ICIL-357
			}
		}
		catch(SQLException e){
			LOGGER.error("getLifeRecordByLifcNum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}
		
		return lifepf;
	}
	@Override
	public List<Lifepf> getOccRecordJLife(String chdrcoy, String chdrnum,String life,String Jlife) {
		Lifepf lifepf;
		List<Lifepf> lifeList = new ArrayList<Lifepf>(); 
		StringBuilder sqlCovrSelect1 = new StringBuilder(
            "SELECT LIFCNUM,JLIFE");
    sqlCovrSelect1.append(" FROM LIFEPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? ");
    sqlCovrSelect1
            .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC");
 
		 PreparedStatement ps = null;
			ResultSet rs = null;
	
		try {
			ps = getPrepareStatement(sqlCovrSelect1.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			//ps.setString(4, Jlife);
			
			rs = ps.executeQuery();

			while (rs.next()) {
				lifepf = new Lifepf();
				lifepf.setLifcnum(rs.getString(1));
				lifepf.setJlife(rs.getString(2));
			lifeList.add(lifepf);
			
			}
		} catch (SQLException e) {
			LOGGER.error("getOccRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return lifeList;
	}

	
	/*	END OF ILIFE-6911 */
	  public Lifepf getLifelnbRecord(String chdrcoy, String chdrnum, String life, String jlife){
	      
		     StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, CLTSEX,CLTDOB FROM LIFELNB ");
		     sb.append("WHERE LTRIM(RTRIM(CHDRCOY))=? AND LTRIM(RTRIM(CHDRNUM))=? AND LTRIM(RTRIM(LIFE))=? AND LTRIM(RTRIM(JLIFE))=?  ORDER BY CHDRCOY ASC, CHDRNUM ASC ");

		     PreparedStatement ps = getPrepareStatement(sb.toString());
			 ResultSet rs = null;
			 Lifepf lifepf = null;
			 try {
			   ps.setString(1, chdrcoy);
			   ps.setString(2, chdrnum);
			   ps.setString(3, life);
			   ps.setString(4, jlife);
			   rs = ps.executeQuery();
			    if (rs.next()) {
				   lifepf = new Lifepf();
				   lifepf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				   lifepf.setChdrcoy(rs.getString("CHDRCOY"));
				   lifepf.setChdrnum(rs.getString("CHDRNUM"));
				   lifepf.setLife(rs.getString("LIFE"));
				   lifepf.setJlife(rs.getString("JLIFE"));
				   lifepf.setCltsex(rs.getString("CLTSEX"));     
				   lifepf.setCltdob(rs.getInt("CLTDOB"));   
			     }
			   } catch (SQLException e) {
			LOGGER.error("getLifelnbRecord()", e); /* IJTI-1479 */
				  throw new SQLRuntimeException(e);
		       } finally {
				 close(ps, rs);
		      }
		     return lifepf;
		    } 
			
			//ILIFE-8108
	  public List<Lifepf> getLifeDetails(Lifepf lifepfData) {
	    	Lifepf lifepf = null;
			List<Lifepf> data = new LinkedList<Lifepf>();
			PreparedStatement psCovrSelect = null;
			ResultSet sqlcovrpf1rs = null;
			StringBuilder sqlCovrSelectBuilder = new StringBuilder();
			sqlCovrSelectBuilder.append("SELECT LIFCNUM, CLTSEX, CLTDOB, CHDRCOY, CHDRNUM, LIFE, JLIFE");
		    sqlCovrSelectBuilder.append(" FROM LIFEMJA WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = ?");
			sqlCovrSelectBuilder.append(" ORDER BY ");
		    sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC ");
			psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
			try {
				psCovrSelect.setString(1, lifepfData.getChdrcoy());
				psCovrSelect.setString(2, lifepfData.getChdrnum());
				psCovrSelect.setString(3, lifepfData.getValidflag());
				sqlcovrpf1rs = executeQuery(psCovrSelect);
				while (sqlcovrpf1rs.next()) {
					lifepf = new Lifepf();
					lifepf.setLifcnum(sqlcovrpf1rs.getString(1));
					lifepf.setCltsex(sqlcovrpf1rs.getString(2));
					lifepf.setCltdob(sqlcovrpf1rs.getInt(3));
					lifepf.setChdrcoy(sqlcovrpf1rs.getString(4));
					lifepf.setChdrnum(sqlcovrpf1rs.getString(5));
					lifepf.setLife(sqlcovrpf1rs.getString(6));
					lifepf.setJlife(sqlcovrpf1rs.getString(7));
		            data.add(lifepf);
	               }

			} catch (SQLException e) {
			LOGGER.error("selectCovrClMData()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrSelect,sqlcovrpf1rs);
				
				}
			return data;
		}
	  
	 /* start*/
	  
	  
	  
	  
	  public void updateLifeandJointRecord(String chdrcoy,String chdrnum,String relationwithlife,String relationwithlifejoint) 
	  {			  
	          String SQL = " UPDATE LIFEPF SET RELATION=? , LIFEREL=? WHERE  CHDRCOY =? AND CHDRNUM =?";
	          PreparedStatement ps= getPrepareStatement(SQL);
	          try {             
	        	  ps.setString(1,relationwithlife);
	        	  ps.setString(2, relationwithlifejoint);
	        	  ps.setString(3, chdrcoy);
	        	  ps.setString(4, chdrnum);             
	        	  ps.executeUpdate();
	          } catch (SQLException e) {
			LOGGER.error("updateLifeandJointRecord()", e); /* IJTI-1479 */
	              throw new SQLRuntimeException(e);
	          } finally {
	              close(ps, null);
	          }	      
	  }
	  
	 
	 /* end*/
	  
	//added for ILB-555 start
	  public Lifepf getLifepf(Lifepf lifepf) {
	  	
	  	StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, STATCODE, TRANNO, CURRFROM, CURRTO, LIFE, JLIFE, LCDTE, LIFCNUM, LIFEREL, ANBCCD, CLTSEX, CLTDOB, AGEADM, SELECTION, SMOKING, OCCUP, PURSUIT01, PURSUIT02, MARTAL, SBSTDL, TERMID, TRDT, TRTM, USER_T, RELATION FROM LIFEPF");
	  	sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=?");
	  	PreparedStatement ps=null;
	  	ResultSet rs=null;
	  	
	  	try {
	  		ps=getPrepareStatement(sql.toString());
	  		ps.setString(1, lifepf.getChdrcoy());
	  		ps.setString(2, lifepf.getChdrnum());
	  		ps.setString(3, lifepf.getLife());
	  		ps.setString(4, lifepf.getJlife());
	  		rs=ps.executeQuery();
	  		while(rs.next()) {
	  			lifepf=new Lifepf();
	  			lifepf.setUniqueNumber(rs.getLong(1));
	  			lifepf.setChdrcoy(rs.getString(2));
	  			lifepf.setChdrnum(rs.getString(3));
	  			lifepf.setValidflag(rs.getString(4));
	  			lifepf.setStatcode(rs.getString(5));
	  			lifepf.setTranno(rs.getInt(6));
	  			lifepf.setCurrfrom(rs.getInt(7));
	  			lifepf.setCurrto(rs.getInt(8));
	  			lifepf.setLife(rs.getString(9));
	  		    lifepf.setJlife(rs.getString(10));
	  			lifepf.setLifeCommDate(rs.getInt(11));
	  			lifepf.setLifcnum(rs.getString(12));
	  			lifepf.setLiferel(rs.getString(13));
	  			lifepf.setAnbAtCcd(rs.getInt(14));
	  			lifepf.setCltsex(rs.getString(15));
	  			lifepf.setCltdob(rs.getInt(16));
	  			lifepf.setAgeadm(rs.getString(17));
	  			lifepf.setSelection(rs.getString(18));
	  			lifepf.setSmoking(rs.getString(19));
	  			lifepf.setOccup(rs.getString(20));
	  			lifepf.setPursuit01(rs.getString(21));
	  			lifepf.setPursuit02(rs.getString(22));
	  			lifepf.setMaritalState(rs.getString(23));
	  			lifepf.setSbstdl(rs.getString(24));
	  			lifepf.setTermid(rs.getString(25));
	  			lifepf.setTransactionDate(rs.getInt(26));
	  			lifepf.setTransactionTime(rs.getInt(27));
	  			lifepf.setUser(rs.getInt(28));
	  			lifepf.setRelation(rs.getString(29));
	  		}
	  	}
	  	catch (SQLException e) {
			LOGGER.error("searchLifeRecordByActx()", e); /* IJTI-1479 */
	          throw new SQLRuntimeException(e);
	      } finally {
	          close(ps, rs);
	      }
	  	return lifepf;
	  }
	  //end
	  
	  public List<Lifepf> searchLifeRecordByLifcNum(String chdrCoy, String lifcNum,String validflag, String jlife){
	    	
	    	StringBuilder sqlLifeSelect = new StringBuilder();
	    	
	    	sqlLifeSelect.append("SELECT CHDRCOY, CHDRNUM, LIFCNUM, LIFE, JLIFE , VALIDFLAG ");
	    	sqlLifeSelect.append("FROM LIFEPF WHERE CHDRCOY = ? AND LIFCNUM = ? AND VALIDFLAG != ? AND JLIFE = ?");

	        PreparedStatement psLifeSelect = getPrepareStatement(sqlLifeSelect.toString());
	        ResultSet sqlLifepfRs = null;
	        List<Lifepf> lifepfSearchResult = new ArrayList<Lifepf>();
	        
	        try {
	        	
	            psLifeSelect.setString(1, chdrCoy.trim());
	            psLifeSelect.setString(2, lifcNum.trim());
	            psLifeSelect.setString(3, validflag);
	            psLifeSelect.setString(4, jlife);
	            
	            sqlLifepfRs = executeQuery(psLifeSelect);
	            
	            while (sqlLifepfRs.next()) {
	            	
	                Lifepf lifepf = new Lifepf();
	                
	                lifepf.setChdrcoy(sqlLifepfRs.getString("CHDRCOY"));
	                lifepf.setChdrnum(sqlLifepfRs.getString("CHDRNUM"));
	                lifepf.setLifcnum(sqlLifepfRs.getString("LIFCNUM"));
	                lifepf.setLife(sqlLifepfRs.getString("LIFE"));
	                lifepf.setJlife(sqlLifepfRs.getString("JLIFE"));
	                lifepf.setValidflag(sqlLifepfRs.getString("VALIDFLAG"));
	                lifepfSearchResult.add(lifepf);
	            }

	        } catch (SQLException e) {
	            LOGGER.error("searchLifeRecordByChdrNum()", e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psLifeSelect, sqlLifepfRs);
	        }
	        return lifepfSearchResult;
	    }
		
	@Override
	public int deleteLifeRecord(String chdrcoy, String chdrnum, String life, String jlife) {
		String sql = "DELETE FROM LIFEPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=?";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, jlife);
			return executeUpdate(ps);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	  
	  public Lifepf getLifeRecordByKey(Lifepf life){
		  StringBuilder sql = new StringBuilder("SELECT * FROM LIFE WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? AND CURRFROM<=? ");
		  sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,LIFE ASC,JLIFE ASC,CURRFROM DESC,UNIQUE_NUMBER DESC");

	      PreparedStatement ps = getPrepareStatement(sql.toString());
	      ResultSet rs = null;
	      Lifepf lifepf = null;
	      try {
	    	  ps.setString(1, life.getChdrcoy());
	    	  ps.setString(2, life.getChdrnum());
	    	  ps.setString(3, life.getLife());
	    	  ps.setString(4, life.getJlife());
	    	  ps.setInt(5, life.getCurrfrom());
	    	  
	    	  rs = executeQuery(ps);
	    	  if(rs.next()) {
	    		  lifepf=new Lifepf();
	    		  lifepf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
	              lifepf.setChdrcoy(rs.getString("CHDRCOY"));
	              lifepf.setChdrnum(rs.getString("CHDRNUM"));
	              lifepf.setValidflag(rs.getString("VALIDFLAG"));
	              lifepf.setStatcode(rs.getString("STATCODE"));
	              lifepf.setTranno(rs.getInt("TRANNO"));
	              lifepf.setCurrfrom(rs.getInt("CURRFROM"));
	              lifepf.setCurrto(rs.getInt("CURRTO"));
	              lifepf.setLife(rs.getString("LIFE"));
	              lifepf.setJlife(rs.getString("JLIFE"));
	    		  lifepf.setSmoking(rs.getString("SMOKING"));

	    	  }
	          
	      } catch (SQLException e) {
	            LOGGER.error("getLifeRecordByKey()", e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
	      return lifepf;
		 
	  }

}