/*
 * File: Tt502pt.java
 * Date: 30 August 2009 2:46:20
 * Author: Quipoz Limited
 * 
 * Class transformed from TT502PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tt502rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TT502.
*
*
*****************************************************************
* </pre>
*/
public class Tt502pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Accidental Hospitalization Premium Amount      ST502");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(75);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("   Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 32, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 36);
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 46, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine003, 58, FILLER).init("Risk Unit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine003, 69).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(75);
	private FixedLengthStringData filler12 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(66).isAPartOf(wsaaPrtLine004, 6, FILLER).init("Benefit Range     Mortality     Premium          Prem Unit:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine004, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(75);
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(65).isAPartOf(wsaaPrtLine005, 6, FILLER).init("Min.        Max.         Class        Rate    Discount Method:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 71);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(48);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine006, 3).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine006, 15).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler19 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(48);
	private FixedLengthStringData filler20 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler21 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(48);
	private FixedLengthStringData filler22 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler23 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(48);
	private FixedLengthStringData filler24 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler25 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(48);
	private FixedLengthStringData filler26 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler27 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(48);
	private FixedLengthStringData filler28 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler29 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(48);
	private FixedLengthStringData filler30 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 33);
	private FixedLengthStringData filler31 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(48);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine013, 3).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine013, 15).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine013, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 33);
	private FixedLengthStringData filler35 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(48);
	private FixedLengthStringData filler36 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 33);
	private FixedLengthStringData filler37 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(48);
	private FixedLengthStringData filler38 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 33);
	private FixedLengthStringData filler39 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(48);
	private FixedLengthStringData filler40 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 33);
	private FixedLengthStringData filler41 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine016, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(48);
	private FixedLengthStringData filler42 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 33);
	private FixedLengthStringData filler43 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine017, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(48);
	private FixedLengthStringData filler44 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 33);
	private FixedLengthStringData filler45 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine018, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine018, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(48);
	private FixedLengthStringData filler46 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine019, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 33);
	private FixedLengthStringData filler47 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine019, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine019, 42).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine020 = new FixedLengthStringData(28);
	private FixedLengthStringData filler48 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine020, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tt502rec tt502rec = new Tt502rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tt502pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tt502rec.tt502Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tt502rec.riskunit);
		fieldNo012.set(tt502rec.mortcls01);
		fieldNo013.set(tt502rec.insprm01);
		fieldNo029.set(tt502rec.instpr01);
		fieldNo014.set(tt502rec.mortcls02);
		fieldNo015.set(tt502rec.insprm02);
		fieldNo031.set(tt502rec.instpr02);
		fieldNo016.set(tt502rec.mortcls03);
		fieldNo017.set(tt502rec.insprm03);
		fieldNo033.set(tt502rec.instpr03);
		fieldNo018.set(tt502rec.mortcls04);
		fieldNo019.set(tt502rec.insprm04);
		fieldNo035.set(tt502rec.instpr04);
		fieldNo020.set(tt502rec.mortcls05);
		fieldNo021.set(tt502rec.insprm05);
		fieldNo037.set(tt502rec.instpr05);
		fieldNo022.set(tt502rec.mortcls06);
		fieldNo023.set(tt502rec.insprm06);
		fieldNo039.set(tt502rec.instpr06);
		fieldNo024.set(tt502rec.mortcls07);
		fieldNo025.set(tt502rec.insprm07);
		fieldNo041.set(tt502rec.instpr07);
		fieldNo028.set(tt502rec.mortality1);
		fieldNo030.set(tt502rec.mortality2);
		fieldNo032.set(tt502rec.mortality3);
		fieldNo034.set(tt502rec.mortality4);
		fieldNo036.set(tt502rec.mortality5);
		fieldNo038.set(tt502rec.mortality6);
		fieldNo040.set(tt502rec.mortality7);
		fieldNo010.set(tt502rec.benefitMin01);
		fieldNo011.set(tt502rec.benefitMax01);
		fieldNo026.set(tt502rec.benefitMin02);
		fieldNo027.set(tt502rec.benefitMax02);
		fieldNo008.set(tt502rec.premUnit);
		fieldNo009.set(tt502rec.disccntmeth);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine019);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine020);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
