package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.productdefinition.dataaccess.dao.RgpdetpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RgpdetpfDAOImpl extends BaseDAOImpl<Rgpdetpf> implements RgpdetpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RgpdetpfDAOImpl.class);

	public List<Rgpdetpf> searchRpdetpfRecord(List<String> chdrnumList, String coy) {	

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, PLNSFX, LIFE, COVERAGE, ");
		sqlSelect.append("RIDER, APCAPLAMT, APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, ");
		sqlSelect.append("APNXTINTBDTE, VALIDFLAG, TRANNO, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM RGPDETPF WHERE  VALIDFLAG = '1' AND  CHDRCOY = ? AND ");
		sqlSelect.append(getSqlInStr(" CHDRNUM", chdrnumList));
		sqlSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,UNIQUE_NUMBER DESC ");


		PreparedStatement psRpdetpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlRpdetpfRs = null;
		List<Rgpdetpf> outputList = new ArrayList<Rgpdetpf>();

		try {

			psRpdetpfSelect.setString(1, coy);
			sqlRpdetpfRs = executeQuery(psRpdetpfSelect);

			while (sqlRpdetpfRs.next()) {

				Rgpdetpf rpdetpf = new Rgpdetpf();

				rpdetpf.setUniqueNumber(sqlRpdetpfRs.getInt("UNIQUE_NUMBER"));
				rpdetpf.setChdrcoy(sqlRpdetpfRs.getString("CHDRCOY"));
				rpdetpf.setChdrnum(sqlRpdetpfRs.getString("CHDRNUM"));
				rpdetpf.setPlnsfx(sqlRpdetpfRs.getInt("PLNSFX"));
				rpdetpf.setLife(sqlRpdetpfRs.getString("LIFE"));
				rpdetpf.setCoverage(sqlRpdetpfRs.getString("COVERAGE"));
				rpdetpf.setRider(sqlRpdetpfRs.getString("RIDER"));
				rpdetpf.setApcaplamt(sqlRpdetpfRs.getBigDecimal("APCAPLAMT"));
				rpdetpf.setAplstcapdate(sqlRpdetpfRs.getInt("APLSTCAPDATE"));
				rpdetpf.setApnxtcapdate(sqlRpdetpfRs.getInt("APNXTCAPDATE"));
				rpdetpf.setAplstintbdte(sqlRpdetpfRs.getInt("APLSTINTBDTE"));
				rpdetpf.setApnxtintbdte(sqlRpdetpfRs.getInt("APNXTINTBDTE"));
				rpdetpf.setValidflag(sqlRpdetpfRs.getString("VALIDFLAG"));
				rpdetpf.setTranno(sqlRpdetpfRs.getString("TRANNO"));
				rpdetpf.setUsrprf(sqlRpdetpfRs.getString("USRPRF"));
				rpdetpf.setJobnm(sqlRpdetpfRs.getString("JOBNM"));
				rpdetpf.setDatime(sqlRpdetpfRs.getDate("DATIME"));

				outputList.add(rpdetpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchRpdetpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psRpdetpfSelect, sqlRpdetpfRs);
		}

		return outputList;
	}

	public void insertIntoRpdetpf(Rgpdetpf rpdetpf) {

		StringBuilder sqlInsert = new StringBuilder();

		sqlInsert.append("INSERT INTO RGPDETPF (");
		sqlInsert.append("CHDRCOY, CHDRNUM, PLNSFX, LIFE, COVERAGE, ");
		sqlInsert.append("RIDER, APCAPLAMT, APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, ");
		sqlInsert.append("APNXTINTBDTE, VALIDFLAG, TRANNO, USRPRF, JOBNM, ");
		sqlInsert.append("DATIME ,APINTAMT,CURRCD  ");
		sqlInsert.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement psRpdetpfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;

		
		try {

			psRpdetpfInsert.setString(index++, rpdetpf.getChdrcoy());
			psRpdetpfInsert.setString(index++, rpdetpf.getChdrnum());
			psRpdetpfInsert.setInt(index++, rpdetpf.getPlnsfx());
			psRpdetpfInsert.setString(index++, rpdetpf.getLife());
			psRpdetpfInsert.setString(index++, rpdetpf.getCoverage());
			psRpdetpfInsert.setString(index++, rpdetpf.getRider());
			psRpdetpfInsert.setBigDecimal(index++, rpdetpf.getApcaplamt());
			psRpdetpfInsert.setInt(index++, rpdetpf.getAplstcapdate());
			psRpdetpfInsert.setInt(index++, rpdetpf.getApnxtcapdate());
			psRpdetpfInsert.setInt(index++, rpdetpf.getAplstintbdte());
			psRpdetpfInsert.setInt(index++, rpdetpf.getApnxtintbdte());
			psRpdetpfInsert.setString(index++, rpdetpf.getValidflag());
			psRpdetpfInsert.setString(index++, rpdetpf.getTranno());
			psRpdetpfInsert.setString(index++, getUsrprf());
			psRpdetpfInsert.setString(index++, getJobnm());
			psRpdetpfInsert.setTimestamp(index++, getDatime());
			psRpdetpfInsert.setBigDecimal(index++, rpdetpf.getApintamt());
			psRpdetpfInsert.setString(index++, rpdetpf.getCurrcd());

			int affectedCount = executeUpdate(psRpdetpfInsert);

			LOGGER.debug("insertIntoRpdetpf {} rows inserted.", affectedCount);//IJTI-1561

		} catch (SQLException e) {
			LOGGER.error("insertIntoRpdetpf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psRpdetpfInsert, null);
		}
	}

	public boolean updateRegpRecIntDate(List<Rgpdetpf> regpBulkOpList) {
		if (regpBulkOpList != null && !regpBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE RGPDETPF SET APNXTINTBDTE=?,APLSTINTBDTE=?,JOBNM=?,USRPRF=?,DATIME=?,APINTAMT=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Rgpdetpf r : regpBulkOpList) {
					psRegpUpdate.setInt(1, r.getApnxtintbdte());
					psRegpUpdate.setInt(2, r.getAplstintbdte());
					psRegpUpdate.setString(3, getJobnm());
					psRegpUpdate.setString(4, getUsrprf());
					psRegpUpdate.setTimestamp(5, getDatime());
					psRegpUpdate.setBigDecimal(6, r.getApintamt());
					psRegpUpdate.setLong(7, r.getUniqueNumber());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRegpRecIntDate()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
		return true;
	}
	
	public boolean updateRegpdet(List<Rgpdetpf> regpBulkOpList) {
		if (regpBulkOpList != null && !regpBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE RGPDETPF SET APNXTINTBDTE=?,APNXTCAPDATE=?,APCAPLAMT=?,JOBNM=?,USRPRF=?,DATIME=?,APINTAMT=?,APLSTINTBDTE=?,APLSTCAPDATE=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Rgpdetpf r : regpBulkOpList) {
					psRegpUpdate.setInt(1, r.getAplstcapdate());
					psRegpUpdate.setInt(2, r.getApnxtcapdate());
					psRegpUpdate.setBigDecimal(3, r.getApcaplamt());
					psRegpUpdate.setString(4, getJobnm());
					psRegpUpdate.setString(5, getUsrprf());
					psRegpUpdate.setTimestamp(6, getDatime());
					psRegpUpdate.setBigDecimal(7, r.getApintamt());
					psRegpUpdate.setInt(8, r.getAplstintbdte());
					psRegpUpdate.setInt(9, r.getAplstcapdate());
					psRegpUpdate.setLong(10, r.getUniqueNumber());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRegpRecIntDate()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
		return true;
	}
	
	public boolean updateRegpRecCapDate(List<Rgpdetpf> regpBulkOpList) {
		if (regpBulkOpList != null && !regpBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE RGPDETPF SET APNXTINTBDTE=?,APNXTCAPDATE=?,APCAPLAMT=?,JOBNM=?,USRPRF=?,DATIME=?,APINTAMT=?,VALIDFLAG='2' WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Rgpdetpf r : regpBulkOpList) {
					psRegpUpdate.setInt(1, r.getApnxtintbdte());
					psRegpUpdate.setInt(2, r.getApnxtcapdate());
					psRegpUpdate.setBigDecimal(3, r.getApcaplamt());
					psRegpUpdate.setString(4, getJobnm());
					psRegpUpdate.setString(5, getUsrprf());
					psRegpUpdate.setTimestamp(6, getDatime());
					psRegpUpdate.setBigDecimal(7, r.getApintamt());
					psRegpUpdate.setLong(8, r.getUniqueNumber());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRegpRecIntDate()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
		return true;
	}
}
