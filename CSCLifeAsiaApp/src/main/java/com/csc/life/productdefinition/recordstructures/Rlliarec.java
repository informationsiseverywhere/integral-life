package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:04
 * Description:
 * Copybook name: RLLIAREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rlliarec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rlliaRec = new FixedLengthStringData(14);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rlliaRec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rlliaRec, 1);
  	public FixedLengthStringData fsucoy = new FixedLengthStringData(1).isAPartOf(rlliaRec, 9);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(rlliaRec, 10);


	public void initialize() {
		COBOLFunctions.initialize(rlliaRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rlliaRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}