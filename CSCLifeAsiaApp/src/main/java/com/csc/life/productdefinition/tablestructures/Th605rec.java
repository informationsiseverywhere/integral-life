package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:32
 * Description:
 * Copybook name: TH605REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th605rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th605Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData agccqind = new FixedLengthStringData(1).isAPartOf(th605Rec, 0);
  	public FixedLengthStringData bonusInd = new FixedLengthStringData(1).isAPartOf(th605Rec, 1);
  	public FixedLengthStringData comind = new FixedLengthStringData(1).isAPartOf(th605Rec, 2);
  	public FixedLengthStringData crtind = new FixedLengthStringData(1).isAPartOf(th605Rec, 3);
  	public FixedLengthStringData indic = new FixedLengthStringData(1).isAPartOf(th605Rec, 4);
  	public ZonedDecimalData intRate = new ZonedDecimalData(8, 5).isAPartOf(th605Rec, 5);
  	public FixedLengthStringData liacoy = new FixedLengthStringData(2).isAPartOf(th605Rec, 13);
  	public FixedLengthStringData ratebas = new FixedLengthStringData(1).isAPartOf(th605Rec, 15);
  	public ZonedDecimalData tdayno = new ZonedDecimalData(3, 0).isAPartOf(th605Rec, 16);
  	public FixedLengthStringData tsalesind = new FixedLengthStringData(1).isAPartOf(th605Rec, 19);
  	public FixedLengthStringData uwind = new FixedLengthStringData(1).isAPartOf(th605Rec, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(479).isAPartOf(th605Rec, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th605Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th605Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}