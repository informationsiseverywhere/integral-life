/*
 * File: Pr50e.java
 * Date: 30 August 2009 1:31:22
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50E.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.PnteTableDAM;
import com.csc.life.productdefinition.screens.Sr50eScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.UsrlTableDAM;
import com.csc.smart.procedures.Usrname;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanckyr;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Usrnamerec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    Policy Notes Enquiry
*
***********************************************************************
* </pre>
*/
public class Pr50e extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50E");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaDesctabl = new FixedLengthStringData(5);
	protected FixedLengthStringData wsaaDescitem = new FixedLengthStringData(8);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0);
	protected ZonedDecimalData wsaaLastTrantime = new ZonedDecimalData(6, 0);
	protected ZonedDecimalData wsaaLastTrandate = new ZonedDecimalData(8, 0);
	protected String wsaaEof = "";
	private String wsaaMatch = "";
	private String wsaaLoadFunction = "";

	private FixedLengthStringData wsaaMessage = new FixedLengthStringData(69);
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(10).isAPartOf(wsaaMessage, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaMessage, 10, FILLER).init(" ");
	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8).isAPartOf(wsaaMessage, 12);
	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaMessage, 20, FILLER).init(" ");
	private FixedLengthStringData wsaaCat = new FixedLengthStringData(3).isAPartOf(wsaaMessage, 22);
	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaMessage, 25, FILLER).init(" ");
	private FixedLengthStringData wsaaUserid = new FixedLengthStringData(10).isAPartOf(wsaaMessage, 27);
	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaMessage, 37, FILLER).init(" ");
	private FixedLengthStringData wsaaUsername = new FixedLengthStringData(30).isAPartOf(wsaaMessage, 39);

	private FixedLengthStringData wsaaTrantime = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrantimeHh = new FixedLengthStringData(2).isAPartOf(wsaaTrantime, 0);
	private FixedLengthStringData filler4 = new FixedLengthStringData(1).isAPartOf(wsaaTrantime, 2, FILLER).init(":");
	private FixedLengthStringData wsaaTrantimeMm = new FixedLengthStringData(2).isAPartOf(wsaaTrantime, 3);
	private FixedLengthStringData filler5 = new FixedLengthStringData(1).isAPartOf(wsaaTrantime, 5, FILLER).init(":");
	private FixedLengthStringData wsaaTrantimeSs = new FixedLengthStringData(2).isAPartOf(wsaaTrantime, 6);
		/* TABLES */
	protected static final String t5688 = "T5688";
	protected static final String t3623 = "T3623";
	protected static final String t3588 = "T3588";
	protected static final String tr50f = "TR50F";
		/* FORMATS */
	private static final String chdrrec = "CHDRREC";
	private static final String liferec = "LIFEREC";
	private static final String descrec = "DESCREC";
	private static final String pnterec = "PNTEREC";
	private static final String usrlrec = "USRLREC";
	protected FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	protected ChdrTableDAM chdrIO = new ChdrTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected LifeTableDAM lifeIO = new LifeTableDAM();
	private PnteTableDAM pnteIO = new PnteTableDAM();
	private UsrlTableDAM usrlIO = new UsrlTableDAM();
	private Sanckyr wsaaSanckey = new Sanckyr();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Namadrsrec namadrsrec = new Namadrsrec();
	private Usrnamerec usrnamerec = new Usrnamerec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sr50eScreenVars sv = ScreenProgram.getScreenVars( Sr50eScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		readSubfile2120, 
		exit2190
	}

	public Pr50e() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50e", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
					initialise1010();
					load1050();
				}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaLastTrandate.set(ZERO);
		wsaaLastTrantime.set(ZERO);
		wsaaEof = "N";
		readChdr1100();
		readLife1200();
		sv.chdrnum.set(chdrIO.getChdrnum());
		sv.cnttype.set(chdrIO.getCnttype());
		sv.cownnum.set(chdrIO.getCownnum());
		sv.lifcnum.set(lifeIO.getLifcnum());
		//MIBT-62
		if (isEQ(wsspcomn.lastprog,"PR50C")) {
			sv.pnotecat.set(wsspUserArea);
			sv.actcode.set(wsspUserArea);
		}
		else {
			sv.pnotecat.set("   ");
			sv.actcode.set("   ");
		}
		sv.trandate.set(varcom.vrcmMaxDate);
		sv.trandatex.set(varcom.vrcmMaxDate);
		wsaaDesctabl.set(t5688);
		wsaaDescitem.set(chdrIO.getCnttype());
		readDesc1300();
		sv.ctypdesc.set(descIO.getLongdesc());
		wsaaDesctabl.set(t3623);
		wsaaDescitem.set(chdrIO.getStatcode());
		readDesc1300();
		sv.rstate.set(descIO.getShortdesc());
		wsaaDesctabl.set(t3588);
		wsaaDescitem.set(chdrIO.getPstatcode());
		readDesc1300();
		sv.pstate.set(descIO.getShortdesc());
		wsaaClntnum.set(chdrIO.getCownnum());
		callNamadrs1400();
		sv.jlname01.set(namadrsrec.name);
		wsaaClntnum.set(lifeIO.getLifcnum());
		callNamadrs1400();
		sv.jlname02.set(namadrsrec.name);
		wsaaDesctabl.set(tr50f);
		wsaaDescitem.set(sv.pnotecat);
		if (isEQ(sv.pnotecat,SPACES)) {
			sv.desc.set(SPACES);
			return ;
		}
		readDesc1300();
		sv.desc.set(descIO.getLongdesc());
	}

protected void load1050()
	{
		wsaaLoadFunction = "BEGN";
		readPnte5200();
		if (isEQ(wsaaEof,"Y")) {
			clearSubfile5100();
			loadBlankLine5400();
		}
		else {
			loadOnePage5000();
			scrnparams.subfileRrn.set(1);
		}
		/*EXIT*/
	}

protected void readChdr1100()
	{
		start1110();
	}

protected void start1110()
	{
		chdrIO.setDataKey(SPACES);
		chdrIO.setStatuz(varcom.oK);
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		chdrIO.setChdrnum(wsspcomn.chdrChdrnum);
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM");
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(chdrIO.getStatuz());
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.endp)
		|| isNE(chdrIO.getChdrpfx(),fsupfxcpy.chdr)
		|| isNE(chdrIO.getChdrcoy(),wsspcomn.chdrChdrcoy)
		|| isNE(chdrIO.getChdrnum(),wsspcomn.chdrChdrnum)) {
			chdrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(chdrlnbIO.getStatuz());
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			chdrIO.setChdrnum(chdrlnbIO.getChdrnum());
			chdrIO.setCnttype(chdrlnbIO.getCnttype());
			chdrIO.setCownnum(chdrlnbIO.getCownnum());
			chdrIO.setStatcode(chdrlnbIO.getStatcode());
			chdrIO.setPstatcode(chdrlnbIO.getPstatcode());
		}
	}

protected void readLife1200()
	{
		start1210();
	}

protected void start1210()
	{
		lifeIO.setDataKey(SPACES);
		lifeIO.setStatuz(varcom.oK);
		lifeIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		lifeIO.setChdrnum(wsspcomn.chdrChdrnum);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lifeIO.getStatuz());
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.endp)
		|| isNE(lifeIO.getChdrcoy(),wsspcomn.chdrChdrcoy)
		|| isNE(lifeIO.getChdrnum(),wsspcomn.chdrChdrnum)) {
			lifeIO.setLifcnum(SPACES);
		}
	}

protected void readDesc1300()
	{
		start1310();
	}

protected void start1310()
	{
		descIO.setDataKey(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaDesctabl);
		descIO.setDescitem(wsaaDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
	}

protected void callNamadrs1400()
	{
			start1410();
		}

protected void start1410()
	{
		if (isEQ(wsaaClntnum,SPACES)) {
			namadrsrec.name.set(SPACES);
			return ;
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			loadOnePage5000();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(sv.errorIndicators,SPACES)) {
			if (isNE(sv.pnotecat,sv.actcode)
			|| isNE(sv.trandate,sv.trandatex)) {
				wsaaEof = "N";
				sv.actcode.set(sv.pnotecat);
				sv.trandatex.set(sv.trandate);
				wsaaLastTrandate.set(ZERO);
				wsaaLastTrantime.set(ZERO);
				wsaaLoadFunction = "BEGN";
				readPnte5200();
				loadOnePage5000();
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		validateSubfile2100();
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2110();
				case readSubfile2120: 
					readSubfile2120();
					subfileUpdate2170();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2110()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSubfile2120()
	{
		processScreen("SR50E", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
	}

protected void subfileUpdate2170()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR50E", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*NEXT-SUBFILE*/
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSubfile2120);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadOnePage5000()
	{
			start5010();
		}

protected void start5010()
	{
		ix.set(1);
		while ( !(isGT(ix,sv.subfilePage)
		|| isEQ(pnteIO.getStatuz(),varcom.endp))) {
			if (isEQ(wsaaLoadFunction,"BEGN")) {
				clearSubfile5100();
			}
			if (isNE(sv.pnotecat,SPACES)) {
				wsaaDesctabl.set(tr50f);
				wsaaDescitem.set(sv.pnotecat);
				readDesc1300();
				sv.desc.set(descIO.getLongdesc());
			}
			else {
				sv.desc.set(SPACES);
			}
			if (isEQ(wsaaEof,"Y")) {
				if (isEQ(scrnparams.subfileRrn,ZERO)) {
					loadBlankLine5400();
				}
				scrnparams.subfileMore.set("N");
				return ;
			}
			wsaaMatch = "Y";
			if (isNE(sv.pnotecat,SPACES)) {
				matchPnote5500();
			}
			if (isEQ(wsaaMatch,"Y")) {
				if (isNE(sv.trandate,ZERO)
				&& isNE(sv.trandate,99999999)) {
					matchTrandate5600();
				}
			}
			if (isEQ(wsaaMatch,"N")) {
				wsaaLoadFunction = "NEXT";
				readPnte5200();
			}
			if (isEQ(wsaaMatch,"Y")) {
				if (isNE(pnteIO.getTrandate(),wsaaLastTrandate)
				|| isNE(pnteIO.getTrantime(),wsaaLastTrantime)) {
					setUpMessage5300();
					sv.message.set(wsaaMessage);
					sv.messageOut[varcom.hi.toInt()].set("Y");
					scrnparams.function.set(varcom.sadd);
					processScreen("SR50E", sv);
					if (isNE(scrnparams.statuz,varcom.oK)) {
						syserrrec.statuz.set(scrnparams.statuz);
						fatalError600();
					}
					ix.add(1);
				}
				if (isLTE(ix,sv.subfilePage)) {
					sv.message.set(pnteIO.getMessage());
					sv.messageOut[varcom.hi.toInt()].set("N");
					scrnparams.function.set(varcom.sadd);
					processScreen("SR50E", sv);
					if (isNE(scrnparams.statuz,varcom.oK)) {
						syserrrec.statuz.set(scrnparams.statuz);
						fatalError600();
					}
					ix.add(1);
					wsaaLoadFunction = "NEXT";
					readPnte5200();
				}
			}
		}
		
		if (isEQ(pnteIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
			if (isEQ(scrnparams.subfileRrn,ZERO)) {
				loadBlankLine5400();
			}
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	}

protected void clearSubfile5100()
	{
		/*START*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR50E", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void readPnte5200()
	{
		start5210();
		readPnte5220();
	}

protected void start5210()
	{
		if (isEQ(wsaaLoadFunction,"BEGN")) {
			pnteIO.setDataKey(SPACES);
			pnteIO.setStatuz(varcom.oK);
			pnteIO.setChdrcoy(wsspcomn.chdrChdrcoy);
			pnteIO.setChdrnum(wsspcomn.chdrChdrnum);
			pnteIO.setTrandate(varcom.vrcmMaxDate);
			pnteIO.setTrantime(varcom.maxtime);
			pnteIO.setSeqno(ZERO);
			pnteIO.setFunction(varcom.begn);
			pnteIO.setFormat(pnterec);
		}
		if (isEQ(wsaaLoadFunction,"NEXT")) {
			pnteIO.setStatuz(varcom.oK);
			pnteIO.setFunction(varcom.nextr);
			pnteIO.setFormat(pnterec);
		}
	}

protected void readPnte5220()
	{
		SmartFileCode.execute(appVars, pnteIO);
		if (isNE(pnteIO.getStatuz(),varcom.oK)
		&& isNE(pnteIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pnteIO.getParams());
			syserrrec.statuz.set(pnteIO.getStatuz());
			fatalError600();
		}
		if (isEQ(pnteIO.getStatuz(),varcom.endp)
		|| isNE(pnteIO.getChdrcoy(),wsspcomn.chdrChdrcoy)
		|| isNE(pnteIO.getChdrnum(),wsspcomn.chdrChdrnum)) {
			wsaaEof = "Y";
		}
		/*EXIT*/
	}

protected void setUpMessage5300()
	{
		start5310();
	}

protected void start5310()
	{
		wsaaLastTrandate.set(pnteIO.getTrandate());
		wsaaLastTrantime.set(pnteIO.getTrantime());
		datcon1rec.intDate.set(pnteIO.getTrandate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaTrantimeHh.set(subString(wsaaLastTrantime, 1, 2));
		wsaaTrantimeMm.set(subString(wsaaLastTrantime, 3, 2));
		wsaaTrantimeSs.set(subString(wsaaLastTrantime, 5, 2));
		usrlIO.setDataKey(SPACES);
		usrlIO.setStatuz(varcom.oK);
		usrlIO.setUserid(pnteIO.getUserid());
		usrlIO.setFunction(varcom.readr);
		usrlIO.setFormat(usrlrec);
		SmartFileCode.execute(appVars, usrlIO);
		if (isNE(usrlIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(usrlIO.getStatuz());
			syserrrec.params.set(usrlIO.getParams());
			fatalError600();
		}
		wsaaSanckey.sancSanccoy.set(usrlIO.getCompany());
		wsaaSanckey.sancSancusr.set(usrlIO.getUsernum());
		usrnamerec.sanckey.set(wsaaSanckey.sancKey);
		usrnamerec.function.set("USRNM");
		callProgram(Usrname.class, usrnamerec.usrnameRec);
		if (isNE(usrnamerec.statuz,varcom.oK)) {
			syserrrec.statuz.set(usrnamerec.statuz);
			syserrrec.params.set(usrnamerec.usrnameRec);
			fatalError600();
		}
		wsaaDate.set(datcon1rec.extDate);
		wsaaTime.set(wsaaTrantime);
		wsaaCat.set(pnteIO.getPnotecat());
		wsaaUserid.set(pnteIO.getUserid());
		wsaaUsername.set(usrnamerec.username);
	}

protected void loadBlankLine5400()
	{
		/*START*/
		initialize(sv.subfileFields);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR50E", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void matchPnote5500()
	{
		/*START*/
		wsaaMatch = "N";
		if (isEQ(sv.pnotecat,pnteIO.getPnotecat())
		|| isEQ(sv.pnotecat,"***")) {
			wsaaMatch = "Y";
		}
		/*EXIT*/
	}

protected void matchTrandate5600()
	{
		/*START*/
		wsaaMatch = "N";
		if (isEQ(sv.trandate,pnteIO.getTrandate())) {
			wsaaMatch = "Y";
		}
		/*EXIT*/
	}
}
