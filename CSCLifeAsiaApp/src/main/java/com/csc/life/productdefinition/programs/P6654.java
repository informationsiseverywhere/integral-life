/*
 * File: P6654.java
 * Date: 30 August 2009 0:48:28
 * Author: Quipoz Limited
 * 
 * Class transformed from P6654.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.procedures.T6654pt;
import com.csc.life.productdefinition.screens.S6654ScreenVars;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P6654 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6654");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected T6654rec t6654rec = getT6654rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6654ScreenVars sv = getLScreenVars();// ScreenProgram.getScreenVars( S6654ScreenVars.class);
	boolean BTPRO028Permission  = false;
	private String wsaaFreq ;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P6654() {
		super();
		screenVars = sv;
		new ScreenModel("S6654", AppVars.getInstance(), sv);
	}
	
	public T6654rec getT6654rec() {
		return new T6654rec();
	}
	

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	protected S6654ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6654ScreenVars.class);
	}


public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, "IT");
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		
		wsaaFreq=sv.item.substring(4);
		
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t6654rec.daexpy01.set(ZERO);
		t6654rec.daexpy02.set(ZERO);
		t6654rec.daexpy03.set(ZERO);
		t6654rec.leadDays.set(ZERO);
		t6654rec.nonForfietureDays.set(ZERO);
		t6654rec.zrincr01.set(ZERO);
		t6654rec.zrincr02.set(ZERO);
		t6654rec.zrincr03.set(ZERO);
		t6654rec.zrincr04.set(ZERO);
		t6654rec.zrincr05.set(ZERO);
		t6654rec.zrincr06.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.arrearsMethod.set(t6654rec.arrearsMethod);
		sv.collectsub.set(t6654rec.collectsub);
		sv.daexpys.set(t6654rec.daexpys);
		sv.doctids.set(t6654rec.doctids);
		sv.leadDays.set(t6654rec.leadDays);
		sv.nonForfietureDays.set(t6654rec.nonForfietureDays);
		sv.renwdates.set(t6654rec.renwdates);
		sv.zrfreqs.set(t6654rec.zrfreqs);
		sv.zrincrs.set(t6654rec.zrincrs);
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		if (BTPRO028Permission) {
			sv.zrfreq01Out[varcom.nd.toInt()].set("Y");
			sv.zrfreq02Out[varcom.nd.toInt()].set("Y");
			sv.zrfreq03Out[varcom.nd.toInt()].set("Y");
			sv.zrfreq04Out[varcom.nd.toInt()].set("Y");
			sv.zrfreq05Out[varcom.nd.toInt()].set("Y");
			sv.zrfreq06Out[varcom.nd.toInt()].set("Y");
			
			if(wsaaFreq.trim().equals("01"))
			{
				sv.zrincr02Out[varcom.nd.toInt()].set("Y");
				sv.zrincr03Out[varcom.nd.toInt()].set("Y");
				sv.zrincr04Out[varcom.nd.toInt()].set("Y");
				sv.zrincr05Out[varcom.nd.toInt()].set("Y");
				sv.zrincr06Out[varcom.nd.toInt()].set("Y");	
			}
			if(wsaaFreq.trim().equals("02"))
			{
				sv.zrincr01Out[varcom.nd.toInt()].set("Y");
				sv.zrincr03Out[varcom.nd.toInt()].set("Y");
				sv.zrincr04Out[varcom.nd.toInt()].set("Y");
				sv.zrincr05Out[varcom.nd.toInt()].set("Y");
				sv.zrincr06Out[varcom.nd.toInt()].set("Y");	
			}
			if(wsaaFreq.trim().equals("04"))
			{
				sv.zrincr01Out[varcom.nd.toInt()].set("Y");
				sv.zrincr02Out[varcom.nd.toInt()].set("Y");
				sv.zrincr04Out[varcom.nd.toInt()].set("Y");
				sv.zrincr05Out[varcom.nd.toInt()].set("Y");
				sv.zrincr06Out[varcom.nd.toInt()].set("Y");	
			}
			if(wsaaFreq.trim().equals("12"))
			{
				sv.zrincr01Out[varcom.nd.toInt()].set("Y");
				sv.zrincr02Out[varcom.nd.toInt()].set("Y");
				sv.zrincr03Out[varcom.nd.toInt()].set("Y");
				sv.zrincr05Out[varcom.nd.toInt()].set("Y");
				sv.zrincr06Out[varcom.nd.toInt()].set("Y");	
			}
			if(wsaaFreq.trim().equals("26"))
			{
				sv.zrincr01Out[varcom.nd.toInt()].set("Y");
				sv.zrincr02Out[varcom.nd.toInt()].set("Y");
				sv.zrincr03Out[varcom.nd.toInt()].set("Y");
				sv.zrincr04Out[varcom.nd.toInt()].set("Y");
				sv.zrincr06Out[varcom.nd.toInt()].set("Y");	
			}
			if(wsaaFreq.trim().equals("**"))
			{
				sv.zrincr02Out[varcom.nd.toInt()].set("Y");
				sv.zrincr03Out[varcom.nd.toInt()].set("Y");
				sv.zrincr04Out[varcom.nd.toInt()].set("Y");
				sv.zrincr05Out[varcom.nd.toInt()].set("Y");
				sv.zrincr06Out[varcom.nd.toInt()].set("Y");	
			}
		}
		
	
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(t6654rec.t6654Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.arrearsMethod,t6654rec.arrearsMethod)) {
			t6654rec.arrearsMethod.set(sv.arrearsMethod);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.collectsub,t6654rec.collectsub)) {
			t6654rec.collectsub.set(sv.collectsub);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.daexpys,t6654rec.daexpys)) {
			t6654rec.daexpys.set(sv.daexpys);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.doctids,t6654rec.doctids)) {
			t6654rec.doctids.set(sv.doctids);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.leadDays,t6654rec.leadDays)) {
			t6654rec.leadDays.set(sv.leadDays);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.nonForfietureDays,t6654rec.nonForfietureDays)) {
			t6654rec.nonForfietureDays.set(sv.nonForfietureDays);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.renwdates,t6654rec.renwdates)) {
			t6654rec.renwdates.set(sv.renwdates);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrfreqs,t6654rec.zrfreqs)) {
			t6654rec.zrfreqs.set(sv.zrfreqs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrincrs,t6654rec.zrincrs)) {
			t6654rec.zrincrs.set(sv.zrincrs);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T6654pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
