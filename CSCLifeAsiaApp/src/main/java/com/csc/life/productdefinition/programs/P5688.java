/*
 * File: P5688.java
 * Date: 30 August 2009 0:34:12
 * Author: Quipoz Limited
 * 
 * Class transformed from P5688.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.procedures.T5688pt;
import com.csc.life.productdefinition.screens.S5688ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5688 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5688");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5688rec t5688rec = new T5688rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5688ScreenVars sv = ScreenProgram.getScreenVars( S5688ScreenVars.class);
	boolean csbil003Permission = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5688() {
		super();
		screenVars = sv;
		new ScreenModel("S5688", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		csbil003Permission = FeaConfg.isFeatureExist("2", "CSBIL003", appVars, "IT");  //ICIL-298
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
		/*ICIL-298 starts*/
		if (!csbil003Permission) {
			sv.csbil003flag.set("N");
		}
		else {
			sv.csbil003flag.set("Y");
		}
		/*ICIL-298 ends*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5688rec.t5688Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5688rec.cfiLim.set(ZERO);
		t5688rec.jlifemax.set(ZERO);
		t5688rec.jlifemin.set(ZERO);
		t5688rec.lifemax.set(ZERO);
		t5688rec.lifemin.set(ZERO);
		t5688rec.poldef.set(ZERO);
		t5688rec.polmax.set(ZERO);
		t5688rec.polmin.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.anbrqd.set(t5688rec.anbrqd);
		sv.bankcode.set(t5688rec.bankcode);
		sv.cfiLim.set(t5688rec.cfiLim);
		sv.comlvlacc.set(t5688rec.comlvlacc);
		sv.defFupMeth.set(t5688rec.defFupMeth);
		sv.feemeth.set(t5688rec.feemeth);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.jlifemax.set(t5688rec.jlifemax);
		sv.jlifemin.set(t5688rec.jlifemin);
		sv.lifemax.set(t5688rec.lifemax);
		sv.lifemin.set(t5688rec.lifemin);
		sv.minDepMth.set(t5688rec.minDepMth);
		sv.nbusallw.set(t5688rec.nbusallw);
		sv.occrqd.set(t5688rec.occrqd);
		sv.poldef.set(t5688rec.poldef);
		sv.polmax.set(t5688rec.polmax);
		sv.polmin.set(t5688rec.polmin);
		sv.revacc.set(t5688rec.revacc);
		sv.smkrqd.set(t5688rec.smkrqd);
		sv.taxrelmth.set(t5688rec.taxrelmth);
		sv.zcertall.set(t5688rec.zcertall);
		sv.zcfidtall.set(t5688rec.zcfidtall);
		sv.znfopt.set(t5688rec.znfopt);
		//ILIFE-1137
		sv.dflexmeth.set(t5688rec.dflexmeth);
		/*ICIL-298 starts*/
		if(csbil003Permission)
		{
		sv.bfreqallw.set(t5688rec.bfreqallw);
		}
		/*ICIL-298 ends*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5688rec.t5688Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.anbrqd,t5688rec.anbrqd)) {
			t5688rec.anbrqd.set(sv.anbrqd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bankcode,t5688rec.bankcode)) {
			t5688rec.bankcode.set(sv.bankcode);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cfiLim,t5688rec.cfiLim)) {
			t5688rec.cfiLim.set(sv.cfiLim);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.comlvlacc,t5688rec.comlvlacc)) {
			t5688rec.comlvlacc.set(sv.comlvlacc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.defFupMeth,t5688rec.defFupMeth)) {
			t5688rec.defFupMeth.set(sv.defFupMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.feemeth,t5688rec.feemeth)) {
			t5688rec.feemeth.set(sv.feemeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.jlifemax,t5688rec.jlifemax)) {
			t5688rec.jlifemax.set(sv.jlifemax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.jlifemin,t5688rec.jlifemin)) {
			t5688rec.jlifemin.set(sv.jlifemin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lifemax,t5688rec.lifemax)) {
			t5688rec.lifemax.set(sv.lifemax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lifemin,t5688rec.lifemin)) {
			t5688rec.lifemin.set(sv.lifemin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.minDepMth,t5688rec.minDepMth)) {
			t5688rec.minDepMth.set(sv.minDepMth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.nbusallw,t5688rec.nbusallw)) {
			t5688rec.nbusallw.set(sv.nbusallw);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.occrqd,t5688rec.occrqd)) {
			t5688rec.occrqd.set(sv.occrqd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.poldef,t5688rec.poldef)) {
			t5688rec.poldef.set(sv.poldef);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.polmax,t5688rec.polmax)) {
			t5688rec.polmax.set(sv.polmax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.polmin,t5688rec.polmin)) {
			t5688rec.polmin.set(sv.polmin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.revacc,t5688rec.revacc)) {
			t5688rec.revacc.set(sv.revacc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.smkrqd,t5688rec.smkrqd)) {
			t5688rec.smkrqd.set(sv.smkrqd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.taxrelmth,t5688rec.taxrelmth)) {
			t5688rec.taxrelmth.set(sv.taxrelmth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zcertall,t5688rec.zcertall)) {
			t5688rec.zcertall.set(sv.zcertall);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zcfidtall,t5688rec.zcfidtall)) {
			t5688rec.zcfidtall.set(sv.zcfidtall);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.znfopt,t5688rec.znfopt)) {
			t5688rec.znfopt.set(sv.znfopt);
			wsaaUpdateFlag = "Y";
		}
		//ILIFE-1137 starts
		if (isNE(sv.dflexmeth,t5688rec.dflexmeth)) {
			t5688rec.dflexmeth.set(sv.dflexmeth);
			wsaaUpdateFlag = "Y";
		}
		//ILIFE-1137 ends
		/*ICIL-298 starts*/
		if(csbil003Permission)
		{
		if(isNE(sv.bfreqallw,t5688rec.bfreqallw)) 
			{
			t5688rec.bfreqallw.set(sv.bfreqallw);
			wsaaUpdateFlag = "Y";
			}
		}
		/*ICIL-298 ends*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5688pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
