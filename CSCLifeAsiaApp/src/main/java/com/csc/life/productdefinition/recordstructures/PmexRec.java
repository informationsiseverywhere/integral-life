package com.csc.life.productdefinition.recordstructures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class PmexRec extends ExternalData {
	//PmexRec
	private static final long serialVersionUID = 1L;
	
  //*******************************
  //Attribute Declarations
  //*******************************
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
  	private List<List<String>> lifeLife ;
  	private List<List<String>> covrCoverage ;
  	private List<List<String>> covrRiderId;
  	private BigDecimal plnsfx;
  	private String billfreq;
  	private String mop;
  	private List<String> lifeJlife ;
  	private List<List<String>> crtable;
  	private List<List<String>> mortcls;
  	private String currcode;
  	private BigDecimal effectdt;
  	private BigDecimal termdate;
  	private List<List<String>> sumin ;
  	private String ratingdate;
  	private BigDecimal reRateDate;
  	private List<String> lage ;
  	private List<String> jlage ;
  	private List<String> lsex ;
  	private List<String> jlsex;
  	private List<List<String>> duration ;
  	private List<List<String>> tpdtype;
  	private String cnttype;
  	private String language;
  	private String premMethod;
	private String rstaflag;	
	private List<List<String>> waitperiod;
	private List<List<String>> bentrm; 
	private List<List<String>> prmbasis;
	private List<List<String>> poltyp;
	private List<String> occpcode; 
	private List<List<String>> dialdownoption;
	private List<List<String>> linkcov; 
	private String liencd;
	private String clntnum;	//identify client.
	private String clntAge;	//ILIFE-7584
	private String clntSex;	//ILIFE-7584
	
	private List<List<String>> calcPrem;
  	private List<List<String>> calcBasPrem;
  	private List<List<String>> calcLoaPrem;
  	private List<List<String>> occpclass; 
  	
  	private List<List<String>> lnkgSubRefNo;
  	private List<List<String>> lnkgind;
  	private List<String> rstate01;
  	private List<List<String>> zstpduty01;
  	//ILIFE-8502-start
	private String commTaxInd;
	private String stateAtIncep;
	private List<List<String>> prevSumIns;
	//ILIFE-8502-end
	//ILIFE-8537-start
	private List<List<String>> inputPrevPrem;	
	private String zszprmcd="";
	private String prat="";
	private String coverc="";
	private String trmofcontract="";
	private BigDecimal hpropdte=BigDecimal.ZERO;
	private String premind="";
	//ILIFE-8537-end
	private List<List<String>> dob;
	private List<List<String>> covrcd;
	private String chdrnum;
	private List<List<String>> commPremium;  //IBPLIFE-5237
	private String validind;
	private String bilfrmdt;
	private String biltodt;

	public List<List<String>> getInputPrevPrem() {
		return inputPrevPrem;
	}

	public void setInputPrevPrem(List<List<String>> inputPrevPrem) {
		this.inputPrevPrem = inputPrevPrem;
	}
  	public String getCommTaxInd() {
		return commTaxInd;
	}
	
	public void setCommTaxInd(String commTaxInd) {
		this.commTaxInd = commTaxInd;
	}

	public String getStateAtIncep() {
		return stateAtIncep;
	}

	public void setStateAtIncep(String string) {
		this.stateAtIncep = string;
	}


	public List<List<String>> getPrevSumIns() {
		return prevSumIns;
	}

	public void setPrevSumIns(List<List<String>> prevSumIns) {
		this.prevSumIns = prevSumIns;
	}
	//ILIFE-8502-end
	public List<List<String>> getLnkgind() {
		return lnkgind;
	}

	public void setLnkgind(List<List<String>> lnkgind) {
		this.lnkgind = lnkgind;
	}

	//START OF ILIFE-7584
  	public String getClntAge() {
		return clntAge;
	}

	public void setClntAge(String clntAge) {
		this.clntAge = clntAge;
	}

	public String getClntSex() {
		return clntSex;
	}

	public void setClntSex(String clntSex) {
		this.clntSex = clntSex;
	}
//END OF ILIFE-7584

	public List<List<String>> getTpdtype() {
		return tpdtype;
	}

	public void setTpdtype(List<List<String>> tpdtype) {
		this.tpdtype = tpdtype;
	}

	public List<List<String>> getLifeLife() {
		return lifeLife;
	}

	public void setLifeLife(List<List<String>> lifeLife) {
		this.lifeLife = lifeLife;
	}

	public List<List<String>> getCovrCoverage() {
		return covrCoverage;
	}

	public void setCovrCoverage(List<List<String>> covrCoverage) {
		this.covrCoverage = covrCoverage;
	}

	public List<List<String>> getCovrRiderId() {
		return covrRiderId;
	}

	public void setCovrRiderId(List<List<String>> covrRiderId) {
		this.covrRiderId = covrRiderId;
	}

	public BigDecimal getPlnsfx() {
		return plnsfx;
	}

	public void setPlnsfx(BigDecimal plnsfx) {
		this.plnsfx = plnsfx;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public String getMop() {
		return mop;
	}

	public void setMop(String mop) {
		this.mop = mop;
	}

	public List<String> getLifeJlife() {
		return lifeJlife;
	}

	public void setLifeJlife(List<String> lifeJlife) {
		this.lifeJlife = lifeJlife;
	}

	public List<List<String>> getCrtable() {
		return crtable;
	}

	public void setCrtable(List<List<String>> crtable) {
		this.crtable = crtable;
	}

	public List<List<String>> getMortcls() {
		return mortcls;
	}

	public void setMortcls(List<List<String>> mortcls) {
		this.mortcls = mortcls;
	}

	public String getCurrcode() {
		return currcode;
	}

	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}

	public BigDecimal getEffectdt() {
		return effectdt;
	}

	public void setEffectdt(BigDecimal effectdt) {
		this.effectdt = effectdt;
	}

	public BigDecimal getTermdate() {
		return termdate;
	}

	public void setTermdate(BigDecimal termdate) {
		this.termdate = termdate;
	}

	public List<List<String>> getSumin() {
		return sumin;
	}

	public void setSumin(List<List<String>> sumin) {
		this.sumin = sumin;
	}

	public String getRatingdate() {
		return ratingdate;
	}

	public void setRatingdate(String ratingdate) {
		this.ratingdate = ratingdate;
	}

	public BigDecimal getReRateDate() {
		return reRateDate;
	}

	public void setReRateDate(BigDecimal reRateDate) {
		this.reRateDate = reRateDate;
	}

	public List<String> getLage() {
		return lage;
	}

	public void setLage(List<String> lage) {
		this.lage = lage;
	}

	public List<String> getJlage() {
		return jlage;
	}

	public void setJlage(List<String> jlage) {
		this.jlage = jlage;
	}

	public List<String> getLsex() {
		return lsex;
	}

	public void setLsex(List<String> lsex) {
		this.lsex = lsex;
	}

	public List<String> getJlsex() {
		return jlsex;
	}

	public void setJlsex(List<String> jlsex) {
		this.jlsex = jlsex;
	}

	public List<List<String>> getDuration() {
		return duration;
	}

	public void setDuration(List<List<String>> duration) {
		this.duration = duration;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPremMethod() {
		return premMethod;
	}

	public void setPremMethod(String premMethod) {
		this.premMethod = premMethod;
	}

	public String getRstaflag() {
		return rstaflag;
	}

	public void setRstaflag(String rstaflag) {
		this.rstaflag = rstaflag;
	}

	public List<List<String>> getWaitperiod() {
		return waitperiod;
	}

	public void setWaitperiod(List<List<String>> waitperiod) {
		this.waitperiod = waitperiod;
	}

	public List<List<String>> getBentrm() {
		return bentrm;
	}

	public void setBentrm(List<List<String>> bentrm) {
		this.bentrm = bentrm;
	}

	public List<List<String>> getPrmbasis() {
		return prmbasis;
	}

	public void setPrmbasis(List<List<String>> prmbasis) {
		this.prmbasis = prmbasis;
	}

	public List<List<String>> getPoltyp() {
		return poltyp;
	}

	public void setPoltyp(List<List<String>> poltyp) {
		this.poltyp = poltyp;
	}

	public List<String> getOccpcode() {
		return occpcode;
	}

	public void setOccpcode(List<String> occpcode) {
		this.occpcode = occpcode;
	}

	public List<List<String>> getDialdownoption() {
		return dialdownoption;
	}

	public void setDialdownoption(List<List<String>> dialdownoption) {
		this.dialdownoption = dialdownoption;
	}

	public List<List<String>> getLinkcov() {
		return linkcov;
	}

	public void setLinkcov(List<List<String>> linkcov) {
		this.linkcov = linkcov;
	}

	public String getLiencd() {
		return liencd;
	}

	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}

	public String getClntnum() {
		return clntnum;
	}

	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}

	public List<List<String>> getCalcPrem() {
		return calcPrem;
	}

	public void setCalcPrem(List<List<String>> calcPrem) {
		this.calcPrem = calcPrem;
	}

	public List<List<String>> getCalcBasPrem() {
		return calcBasPrem;
	}

	public void setCalcBasPrem(List<List<String>> calcBasPrem) {
		this.calcBasPrem = calcBasPrem;
	}

	public List<List<String>> getCalcLoaPrem() {
		return calcLoaPrem;
	}

	public void setCalcLoaPrem(List<List<String>> calcLoaPrem) {
		this.calcLoaPrem = calcLoaPrem;
	}

	public List<List<String>> getOccpclass() {
		return occpclass;
	}

	public void setOccpclass(List<List<String>> occpclass) {
		this.occpclass = occpclass;
	}

	public List<List<String>> getLnkgSubRefNo() {
		return lnkgSubRefNo;
	}

	public void setLnkgSubRefNo(List<List<String>> lnkgSubRefNo) {
		this.lnkgSubRefNo = lnkgSubRefNo;
	}

	public List<String> getRstate01() {
		return rstate01;
	}

	public void setRstate01(List<String> rstate01) {
		this.rstate01 = rstate01;
	}

	public List<List<String>> getZstpduty01() {
		return zstpduty01;
	}

	public void setZstpduty01(List<List<String>> zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public void initialise(){
		calcPrem = new ArrayList<>();
	  	calcBasPrem = new ArrayList<>();
	  	calcLoaPrem = new ArrayList<>();
	  	commPremium = new ArrayList<>();
	  	occpclass = new ArrayList<>(); 
	  	zstpduty01 = new ArrayList<>();
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
	}


	public String getZszprmcd() {
		return zszprmcd;
	}

	public void setZszprmcd(String zszprmcd) {
		this.zszprmcd = zszprmcd;
	}

	public String getPrat() {
		return prat;
	}

	public void setPrat(String prat) {
		this.prat = prat;
	}

	public String getCoverc() {
		return coverc;
	}

	public void setCoverc(String coverc) {
		this.coverc = coverc;
	}

	public String getTrmofcontract() {
		return trmofcontract;
	}

	public void setTrmofcontract(String trmofcontract) {
		this.trmofcontract = trmofcontract;
	}

	public BigDecimal getHpropdte() {
		return hpropdte;
	}

	public void setHpropdte(BigDecimal hpropdte) {
		this.hpropdte = hpropdte;
	}

	public String getPremind() {
		return premind;
	}

	public void setPremind(String premind) {
		this.premind = premind;
	}
	
	public List<List<String>> getDob() {
		return dob;
	}

	public void setDob(List<List<String>> dob) {
		this.dob = dob;
	}
	
	public List<List<String>> getCovrcd() {
		return covrcd;
	}

	public void setCovrcd(List<List<String>> covrcd) {
		this.covrcd = covrcd;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public List<List<String>> getCommPremium() {
		return commPremium;
	}

	public void setCommPremium(List<List<String>> commPremium) {
		this.commPremium = commPremium;
	}

	public String getValidind() {
		return validind;
	}

	public void setValidind(String validind) {
		this.validind = validind;
	}
	
	public String getBilfrmdt() {
		return bilfrmdt;
	}

	public void setBilfrmdt(String bilfrmdt) {
		this.bilfrmdt = bilfrmdt;
	}
	
	public String getBiltodt() {
		return biltodt;
	}

	public void setBiltodt(String biltodt) {
		this.biltodt = biltodt;
	}
	
}