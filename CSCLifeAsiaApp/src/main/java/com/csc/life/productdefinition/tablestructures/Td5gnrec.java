package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;


public class Td5gnrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData td5gnRec = new FixedLengthStringData(500);
  	public FixedLengthStringData risksubr = new FixedLengthStringData(7).isAPartOf(td5gnRec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(493).isAPartOf(td5gnRec, 7, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(td5gnRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			td5gnRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}