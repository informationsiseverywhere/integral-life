package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:48
 * Description:
 * Copybook name: TR51FREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr51frec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr51fRec = new FixedLengthStringData(500);
  	public FixedLengthStringData ages = new FixedLengthStringData(30).isAPartOf(tr51fRec, 0);
  	public ZonedDecimalData[] age = ZDArrayPartOfStructure(10, 3, 0, ages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData age01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData age02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData age03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData age04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData age05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData age06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData age07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData age08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData age09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData age10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public FixedLengthStringData crtables = new FixedLengthStringData(80).isAPartOf(tr51fRec, 30);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(20, 4, crtables, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData crtable11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData crtable12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData crtable13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
  	public FixedLengthStringData crtable14 = new FixedLengthStringData(4).isAPartOf(filler1, 52);
  	public FixedLengthStringData crtable15 = new FixedLengthStringData(4).isAPartOf(filler1, 56);
  	public FixedLengthStringData crtable16 = new FixedLengthStringData(4).isAPartOf(filler1, 60);
  	public FixedLengthStringData crtable17 = new FixedLengthStringData(4).isAPartOf(filler1, 64);
  	public FixedLengthStringData crtable18 = new FixedLengthStringData(4).isAPartOf(filler1, 68);
  	public FixedLengthStringData crtable19 = new FixedLengthStringData(4).isAPartOf(filler1, 72);
  	public FixedLengthStringData crtable20 = new FixedLengthStringData(4).isAPartOf(filler1, 76);
  	public FixedLengthStringData suminss = new FixedLengthStringData(170).isAPartOf(tr51fRec, 110);
  	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(10, 17, 2, suminss, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(170).isAPartOf(suminss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData sumins01 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData sumins02 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 17);
  	public ZonedDecimalData sumins03 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 34);
  	public ZonedDecimalData sumins04 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 51);
  	public ZonedDecimalData sumins05 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 68);
  	public ZonedDecimalData sumins06 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 85);
  	public ZonedDecimalData sumins07 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 102);
  	public ZonedDecimalData sumins08 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 119);
  	public ZonedDecimalData sumins09 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 136);
  	public ZonedDecimalData sumins10 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 153);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(220).isAPartOf(tr51fRec, 280, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr51fRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr51fRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}