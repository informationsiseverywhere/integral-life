package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HitrpfDAOImpl extends BaseDAOImpl<Hitrpf> implements HitrpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(HitrpfDAOImpl.class);

    public Map<Long, List<Hitrpf>> searchHitrRecord(List<Long> hitdUQ) {
        StringBuilder sqlHitrSelect1 = new StringBuilder(
                "SELECT HR.UNIQUE_NUMBER,HR.EFFDATE,HR.ZINTALLOC,HR.FUNDAMNT,HD.UNIQUE_NUMBER ");
        sqlHitrSelect1
                .append("FROM HITRPF HR,HITDPF HD WHERE HR.FDBKIND = 'Y' AND HR.ZINTAPPIND = ' ' AND HR.CHDRCOY=HD.CHDRCOY AND HR.CHDRNUM = HD.CHDRNUM AND HR.LIFE=HD.LIFE AND HR.COVERAGE=HD.COVERAGE AND HR.RIDER=HD.RIDER AND HR.PLNSFX=HD.PLNSFX AND HR.ZINTBFND=HD.ZINTBFND AND ");
        sqlHitrSelect1.append(getSqlInLong("HD.UNIQUE_NUMBER", hitdUQ));

        sqlHitrSelect1
                .append(" ORDER BY HR.CHDRCOY ASC, HR.CHDRNUM ASC, HR.LIFE ASC, HR.COVERAGE ASC, HR.RIDER ASC, HR.PLNSFX ASC, HR.ZINTBFND ASC, HR.EFFDATE DESC, HR.UNIQUE_NUMBER DESC ");

        PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1.toString());
        ResultSet sqlhitrpf1rs = null;
        Map<Long, List<Hitrpf>> hitrpfSearchResult = new HashMap<>();
        try {
            sqlhitrpf1rs = executeQuery(psHitrSelect);
            while (sqlhitrpf1rs.next()) {
                Hitrpf hitrpf = new Hitrpf();
                hitrpf.setUniqueNumber(sqlhitrpf1rs.getLong(1));
                hitrpf.setEffdate(sqlhitrpf1rs.getInt(2));
                hitrpf.setZintalloc(sqlhitrpf1rs.getString(3));
                hitrpf.setFundAmount(sqlhitrpf1rs.getBigDecimal(4));
                long uniqueNumber = sqlhitrpf1rs.getLong(5);
                if (hitrpfSearchResult.containsKey(uniqueNumber)) {
                    hitrpfSearchResult.get(uniqueNumber).add(hitrpf);
                } else {
                    List<Hitrpf> hitrpfList = new ArrayList<>();
                    hitrpfList.add(hitrpf);
                    hitrpfSearchResult.put(uniqueNumber, hitrpfList);
                }
            }

        } catch (SQLException e) {
			LOGGER.error("searchHitrRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psHitrSelect, sqlhitrpf1rs);
        }
        return hitrpfSearchResult;

    }
    
	public Map<String, List<Hitrpf>> searchHitrRecordByChdrnum(List<String> chdrnumList) {
		StringBuilder sqlHitrSelect1 = new StringBuilder(
				"SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,PRCSEQ,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,ZINTBFND,FUNDAMNT,FNDCURR,FUNDRATE,ZRECTYP,EFFDATE,TRANNO,CNTCURR,CNTAMNT,CNTTYP,CRTABLE,COVDBTIND,ZINTAPPIND,FDBKIND,TRIGER,TRIGKY,SWCHIND,PERSUR,SVP,ZLSTINTDTE,ZINTRATE,ZINTEFFDT,ZINTALLOC,INCINUM,INCIPERD01,INCIPERD02,INCIPRM01,INCIPRM02,USTMNO,SACSCODE,SACSTYP,GENLCDE ");
		sqlHitrSelect1.append("FROM HITRALO WHERE ");
		sqlHitrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlHitrSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, ZRECTYP ASC, PRCSEQ ASC, TRANNO ASC, EFFDATE ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1.toString());
		ResultSet sqlhitrpf1rs = null;
		Map<String, List<Hitrpf>> hitrpfSearchResult = new HashMap<>();
		try {
			sqlhitrpf1rs = executeQuery(psHitrSelect);
			while (sqlhitrpf1rs.next()) {
				Hitrpf hitrpf = new Hitrpf();
				hitrpf.setChdrcoy(sqlhitrpf1rs.getString("CHDRCOY"));
				hitrpf.setChdrnum(sqlhitrpf1rs.getString("CHDRNUM"));
				hitrpf.setLife(sqlhitrpf1rs.getString("LIFE"));
				hitrpf.setCoverage(sqlhitrpf1rs.getString("COVERAGE"));
				hitrpf.setRider(sqlhitrpf1rs.getString("RIDER"));
				hitrpf.setPlanSuffix(sqlhitrpf1rs.getInt("PLNSFX"));
				hitrpf.setProcSeqNo(sqlhitrpf1rs.getInt("PRCSEQ"));
				hitrpf.setBatccoy(sqlhitrpf1rs.getString("BATCCOY"));
				hitrpf.setBatcbrn(sqlhitrpf1rs.getString("BATCBRN"));
				hitrpf.setBatcactyr(sqlhitrpf1rs.getInt("BATCACTYR"));
				hitrpf.setBatcactmn(sqlhitrpf1rs.getInt("BATCACTMN"));
				hitrpf.setBatctrcde(sqlhitrpf1rs.getString("BATCTRCDE"));
				hitrpf.setBatcbatch(sqlhitrpf1rs.getString("BATCBATCH"));
				hitrpf.setZintbfnd(sqlhitrpf1rs.getString("ZINTBFND"));
				hitrpf.setFundAmount(sqlhitrpf1rs.getBigDecimal("FUNDAMNT"));
				hitrpf.setFundCurrency(sqlhitrpf1rs.getString("FNDCURR").trim());
				hitrpf.setFundRate(sqlhitrpf1rs.getBigDecimal("FUNDRATE"));
				hitrpf.setZrectyp(sqlhitrpf1rs.getString("ZRECTYP"));
				hitrpf.setEffdate(sqlhitrpf1rs.getInt("EFFDATE"));
				hitrpf.setTranno(sqlhitrpf1rs.getInt("TRANNO"));
				hitrpf.setCntcurr(sqlhitrpf1rs.getString("CNTCURR"));
				hitrpf.setContractAmount(sqlhitrpf1rs.getBigDecimal("CNTAMNT"));
				hitrpf.setCnttyp(sqlhitrpf1rs.getString("CNTTYP"));
				hitrpf.setCrtable(sqlhitrpf1rs.getString("CRTABLE"));
				hitrpf.setCovdbtind(sqlhitrpf1rs.getString("COVDBTIND"));
				hitrpf.setZintappind(sqlhitrpf1rs.getString("ZINTAPPIND"));
				hitrpf.setFeedbackInd(sqlhitrpf1rs.getString("FDBKIND"));
				hitrpf.setTriggerModule(sqlhitrpf1rs.getString("TRIGER"));
				hitrpf.setTriggerKey(sqlhitrpf1rs.getString("TRIGKY"));
				hitrpf.setSwitchIndicator(sqlhitrpf1rs.getString("SWCHIND"));
				hitrpf.setSurrenderPercent(sqlhitrpf1rs.getBigDecimal("PERSUR"));
				hitrpf.setSvp(sqlhitrpf1rs.getBigDecimal("SVP"));
				hitrpf.setZlstintdte(sqlhitrpf1rs.getInt("ZLSTINTDTE"));
				hitrpf.setZintrate(sqlhitrpf1rs.getBigDecimal("ZINTRATE"));
				hitrpf.setZinteffdt(sqlhitrpf1rs.getInt("ZINTEFFDT"));
				hitrpf.setZintalloc(sqlhitrpf1rs.getString("ZINTALLOC"));
				hitrpf.setInciNum(sqlhitrpf1rs.getInt("INCINUM"));
				hitrpf.setInciPerd01(sqlhitrpf1rs.getInt("INCIPERD01"));
				hitrpf.setInciPerd02(sqlhitrpf1rs.getInt("INCIPERD02"));
				hitrpf.setInciprm01(sqlhitrpf1rs.getBigDecimal("INCIPRM01"));
				hitrpf.setInciprm02(sqlhitrpf1rs.getBigDecimal("INCIPRM02"));
				hitrpf.setUstmno(sqlhitrpf1rs.getInt("USTMNO"));
				hitrpf.setSacscode(sqlhitrpf1rs.getString("SACSCODE"));
				hitrpf.setSacstyp(sqlhitrpf1rs.getString("SACSTYP"));
				hitrpf.setGenlcde(sqlhitrpf1rs.getString("GENLCDE"));

				if (hitrpfSearchResult.containsKey(hitrpf.getChdrnum())) {
					hitrpfSearchResult.get(hitrpf.getChdrnum()).add(hitrpf);
				} else {
					List<Hitrpf> hitrpfList = new ArrayList<>();
					hitrpfList.add(hitrpf);
					hitrpfSearchResult.put(hitrpf.getChdrnum(), hitrpfList);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchHitrRecordByChdrnum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitrSelect, sqlhitrpf1rs);
		}
		return hitrpfSearchResult;

	}
	public Map<String, List<Hitrpf>> searchHitrintRecordByChdrnum(List<String> chdrnumList){
	
		StringBuilder sqlHitrSelect1 = new StringBuilder(
				"SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,PRCSEQ,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,ZINTBFND,FUNDAMNT,FNDCURR,FUNDRATE,ZRECTYP,EFFDATE,TRANNO,CNTCURR,CNTAMNT,CNTTYP,CRTABLE,COVDBTIND,ZINTAPPIND,FDBKIND,TRIGER,TRIGKY,SWCHIND,PERSUR,SVP,ZLSTINTDTE,ZINTRATE,ZINTEFFDT,ZINTALLOC,INCINUM,INCIPERD01,INCIPERD02,INCIPRM01,INCIPRM02,USTMNO,SACSCODE,SACSTYP,GENLCDE,UNIQUE_NUMBER ");
		sqlHitrSelect1.append("FROM HITRINT WHERE ");
		sqlHitrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlHitrSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, EFFDATE DESC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1.toString());
		ResultSet sqlhitrpf1rs = null;
		Map<String, List<Hitrpf>> hitrpfSearchResult = new HashMap<>();
		try {
			sqlhitrpf1rs = executeQuery(psHitrSelect);
			while (sqlhitrpf1rs.next()) {
				Hitrpf hitrpf = new Hitrpf();
				hitrpf.setChdrcoy(sqlhitrpf1rs.getString("CHDRCOY"));
				hitrpf.setChdrnum(sqlhitrpf1rs.getString("CHDRNUM"));
				hitrpf.setLife(sqlhitrpf1rs.getString("LIFE"));
				hitrpf.setCoverage(sqlhitrpf1rs.getString("COVERAGE"));
				hitrpf.setRider(sqlhitrpf1rs.getString("RIDER"));
				hitrpf.setPlanSuffix(sqlhitrpf1rs.getInt("PLNSFX"));
				hitrpf.setProcSeqNo(sqlhitrpf1rs.getInt("PRCSEQ"));
				hitrpf.setBatccoy(sqlhitrpf1rs.getString("BATCCOY"));
				hitrpf.setBatcbrn(sqlhitrpf1rs.getString("BATCBRN"));
				hitrpf.setBatcactyr(sqlhitrpf1rs.getInt("BATCACTYR"));
				hitrpf.setBatcactmn(sqlhitrpf1rs.getInt("BATCACTMN"));
				hitrpf.setBatctrcde(sqlhitrpf1rs.getString("BATCTRCDE"));
				hitrpf.setBatcbatch(sqlhitrpf1rs.getString("BATCBATCH"));
				hitrpf.setZintbfnd(sqlhitrpf1rs.getString("ZINTBFND"));
				hitrpf.setFundAmount(sqlhitrpf1rs.getBigDecimal("FUNDAMNT"));
				hitrpf.setFundCurrency(sqlhitrpf1rs.getString("FNDCURR").trim());
				hitrpf.setFundRate(sqlhitrpf1rs.getBigDecimal("FUNDRATE"));
				hitrpf.setZrectyp(sqlhitrpf1rs.getString("ZRECTYP"));
				hitrpf.setEffdate(sqlhitrpf1rs.getInt("EFFDATE"));
				hitrpf.setTranno(sqlhitrpf1rs.getInt("TRANNO"));
				hitrpf.setCntcurr(sqlhitrpf1rs.getString("CNTCURR"));
				hitrpf.setContractAmount(sqlhitrpf1rs.getBigDecimal("CNTAMNT"));
				hitrpf.setCnttyp(sqlhitrpf1rs.getString("CNTTYP"));
				hitrpf.setCrtable(sqlhitrpf1rs.getString("CRTABLE"));
				hitrpf.setCovdbtind(sqlhitrpf1rs.getString("COVDBTIND"));
				hitrpf.setZintappind(sqlhitrpf1rs.getString("ZINTAPPIND"));
				hitrpf.setFeedbackInd(sqlhitrpf1rs.getString("FDBKIND"));
				hitrpf.setTriggerModule(sqlhitrpf1rs.getString("TRIGER"));
				hitrpf.setTriggerKey(sqlhitrpf1rs.getString("TRIGKY"));
				hitrpf.setSwitchIndicator(sqlhitrpf1rs.getString("SWCHIND"));
				hitrpf.setSurrenderPercent(sqlhitrpf1rs.getBigDecimal("PERSUR"));
				hitrpf.setSvp(sqlhitrpf1rs.getBigDecimal("SVP"));
				hitrpf.setZlstintdte(sqlhitrpf1rs.getInt("ZLSTINTDTE"));
				hitrpf.setZintrate(sqlhitrpf1rs.getBigDecimal("ZINTRATE"));
				hitrpf.setZinteffdt(sqlhitrpf1rs.getInt("ZINTEFFDT"));
				hitrpf.setZintalloc(sqlhitrpf1rs.getString("ZINTALLOC"));
				hitrpf.setInciNum(sqlhitrpf1rs.getInt("INCINUM"));
				hitrpf.setInciPerd01(sqlhitrpf1rs.getInt("INCIPERD01"));
				hitrpf.setInciPerd02(sqlhitrpf1rs.getInt("INCIPERD02"));
				hitrpf.setInciprm01(sqlhitrpf1rs.getBigDecimal("INCIPRM01"));
				hitrpf.setInciprm02(sqlhitrpf1rs.getBigDecimal("INCIPRM02"));
				hitrpf.setUstmno(sqlhitrpf1rs.getInt("USTMNO"));
				hitrpf.setSacscode(sqlhitrpf1rs.getString("SACSCODE"));
				hitrpf.setSacstyp(sqlhitrpf1rs.getString("SACSTYP"));
				hitrpf.setGenlcde(sqlhitrpf1rs.getString("GENLCDE"));
				hitrpf.setUniqueNumber(sqlhitrpf1rs.getLong("UNIQUE_NUMBER"));
				if (hitrpfSearchResult.containsKey(hitrpf.getChdrnum())) {
					hitrpfSearchResult.get(hitrpf.getChdrnum()).add(hitrpf);
				} else {
					List<Hitrpf> hitrpfList = new ArrayList<>();
					hitrpfList.add(hitrpf);
					hitrpfSearchResult.put(hitrpf.getChdrnum(), hitrpfList);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchHitrintRecordByChdrnum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitrSelect, sqlhitrpf1rs);
		}
		return hitrpfSearchResult;
	}
	
    public void updatedHitrRecord(List<Hitrpf> hitrBulkOpList) {
        if (hitrBulkOpList != null && !hitrBulkOpList.isEmpty()) {
            String SQL_Hitr_UPDATE = "UPDATE HITRPF SET ZINTAPPIND=?,ZLSTINTDTE=?,ZINTEFFDT=?,ZINTRATE=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE UNIQUE_NUMBER=?";

            PreparedStatement psHitrUpdate = getPrepareStatement(SQL_Hitr_UPDATE);
            try {
                for (Hitrpf c : hitrBulkOpList) {
                    psHitrUpdate.setString(1, c.getZintappind());
                    psHitrUpdate.setInt(2, c.getZlstintdte());
                    psHitrUpdate.setInt(3, c.getZinteffdt());
                    psHitrUpdate.setBigDecimal(4, c.getZintrate());
                    psHitrUpdate.setString(5, getJobnm());
                    psHitrUpdate.setString(6, getUsrprf());
                    psHitrUpdate.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
                    psHitrUpdate.setLong(8, c.getUniqueNumber());					
                    psHitrUpdate.addBatch();
                }
                psHitrUpdate.executeBatch();
            } catch (SQLException e) {
				LOGGER.error("updatedHitrRecord()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psHitrUpdate, null);
            }
        }
    }
    
	public void insertHitrpfRecord(List<Hitrpf> hitrBulkOpList) {
		if (hitrBulkOpList != null && !hitrBulkOpList.isEmpty()) {

			String SQL_HITR_INSERT = "INSERT INTO HITRPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,PRCSEQ,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,ZINTBFND,FUNDAMNT,FNDCURR,FUNDRATE,ZRECTYP,"
					+ "EFFDATE,TRANNO,CNTCURR,CNTAMNT,CNTTYP,CRTABLE,COVDBTIND,ZINTAPPIND,FDBKIND,TRIGER,TRIGKY,SWCHIND,PERSUR,SVP,ZLSTINTDTE,ZINTRATE,ZINTEFFDT,ZINTALLOC,INCINUM,INCIPERD01,INCIPERD02,"
					+ "INCIPRM01,INCIPRM02,USTMNO,SACSCODE,SACSTYP,GENLCDE,BSCHEDNAM,BSCHEDNUM,USRPRF,JOBNM,DATIME) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; //ILIFE-8786

			PreparedStatement psHitrInsert = getPrepareStatement(SQL_HITR_INSERT);
			try {
				for (Hitrpf h : hitrBulkOpList) {
					psHitrInsert.setString(1, h.getChdrcoy());
					psHitrInsert.setString(2, h.getChdrnum());
					psHitrInsert.setString(3, h.getLife());
					psHitrInsert.setString(4, h.getCoverage());
					psHitrInsert.setString(5, h.getRider());
					psHitrInsert.setInt(6, h.getPlanSuffix());
					psHitrInsert.setInt(7, h.getProcSeqNo());
					psHitrInsert.setString(8, h.getBatccoy());
					psHitrInsert.setString(9, h.getBatcbrn());
					psHitrInsert.setInt(10, h.getBatcactyr());
					psHitrInsert.setInt(11, h.getBatcactmn());
					psHitrInsert.setString(12, h.getBatctrcde());
					psHitrInsert.setString(13, h.getBatcbatch());
					psHitrInsert.setString(14, h.getZintbfnd());
					psHitrInsert.setBigDecimal(15, h.getFundAmount());
					psHitrInsert.setString(16, h.getFundCurrency().trim());
					psHitrInsert.setBigDecimal(17, h.getFundRate());
					psHitrInsert.setString(18, h.getZrectyp());
					psHitrInsert.setInt(19, h.getEffdate());
					psHitrInsert.setInt(20, h.getTranno());
					psHitrInsert.setString(21, h.getCntcurr());
					psHitrInsert.setBigDecimal(22, h.getContractAmount());
					psHitrInsert.setString(23, h.getCnttyp());
					psHitrInsert.setString(24, h.getCrtable());
					psHitrInsert.setString(25, h.getCovdbtind());
					psHitrInsert.setString(26, h.getZintappind());
					psHitrInsert.setString(27, h.getFeedbackInd());
					psHitrInsert.setString(28, h.getTriggerModule());
					psHitrInsert.setString(29, h.getTriggerKey());
					psHitrInsert.setString(30, h.getSwitchIndicator());
					psHitrInsert.setBigDecimal(31, h.getSurrenderPercent());
					psHitrInsert.setBigDecimal(32, h.getSvp());
					psHitrInsert.setInt(33, h.getZlstintdte());
					psHitrInsert.setBigDecimal(34, h.getZintrate());
					psHitrInsert.setInt(35, h.getZinteffdt());
					psHitrInsert.setString(36, h.getZintalloc());
					psHitrInsert.setInt(37, h.getInciNum());
					psHitrInsert.setInt(38, h.getInciPerd01());
					psHitrInsert.setInt(39, h.getInciPerd02());
					psHitrInsert.setBigDecimal(40, h.getInciprm01());
					psHitrInsert.setBigDecimal(41, h.getInciprm02());
					psHitrInsert.setInt(42, h.getUstmno());
					psHitrInsert.setString(43, h.getSacscode());
					psHitrInsert.setString(44, h.getSacstyp());
					psHitrInsert.setString(45, h.getGenlcde());
					psHitrInsert.setString(46, h.getScheduleName());
					psHitrInsert.setInt(47, h.getScheduleNumber());
					// JOBNM,USRPRF,DATIME
					psHitrInsert.setString(48, getUsrprf());
					psHitrInsert.setString(49, getJobnm());
					psHitrInsert.setTimestamp(50, new Timestamp(System.currentTimeMillis()));
					psHitrInsert.addBatch();
				}
				psHitrInsert.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("insertHitrpfRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psHitrInsert, null);
			}
		}
	}

	@Override
	public Hitrpf getHitrpf(Hitrpf hitrpf) {
		
		StringBuilder sqlHitrpf = new StringBuilder(
				"SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, PRCSEQ, BATCCOY, BATCBRN, BATCACTYR, "
				+ "BATCACTMN, BATCTRCDE, BATCBATCH, ZINTBFND, FUNDAMNT, FNDCURR, FUNDRATE, ZRECTYP, EFFDATE, TRANNO, "
				+ "CNTCURR, CNTAMNT, CNTTYP, CRTABLE, COVDBTIND, ZINTAPPIND, FDBKIND, TRIGER, TRIGKY, SWCHIND, PERSUR, SVP, "
				+ "ZLSTINTDTE, ZINTRATE, ZINTEFFDT, ZINTALLOC, INCINUM, INCIPERD01, INCIPERD02, INCIPRM01, INCIPRM02, USTMNO, "
				+ "SACSCODE, SACSTYP, GENLCDE, BSCHEDNAM, BSCHEDNUM, UNIQUE_NUMBER ");
		sqlHitrpf.append("FROM HITRALO WHERE ");
		sqlHitrpf.append("CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND ZINTBFND=? AND ZRECTYP=? AND PRCSEQ=? AND TRANNO=? AND EFFDATE=?");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hitrpf hitrpfObj = null;
		try {
			ps = getConnection().prepareStatement(sqlHitrpf.toString());
			ps.setString(1, hitrpf.getChdrcoy());
			ps.setString(2, hitrpf.getChdrnum());
			ps.setString(3, hitrpf.getLife());
			ps.setString(4, hitrpf.getCoverage());
			ps.setString(5, hitrpf.getRider());
			ps.setInt(6, hitrpf.getPlanSuffix());
			ps.setString(7, hitrpf.getZintbfnd());
			ps.setString(8, hitrpf.getZrectyp());
			ps.setInt(9, hitrpf.getProcSeqNo());
			ps.setInt(10, hitrpf.getTranno());
			ps.setInt(11, hitrpf.getEffdate());
			rs = ps.executeQuery();
			if(rs.next()){
				hitrpfObj = new Hitrpf();
				hitrpfObj.setChdrcoy(rs.getString("CHDRCOY"));
				hitrpfObj.setChdrnum(rs.getString("CHDRNUM"));
				hitrpfObj.setLife(rs.getString("LIFE"));
				hitrpfObj.setCoverage(rs.getString("COVERAGE"));
				hitrpfObj.setRider(rs.getString("RIDER"));
				hitrpfObj.setPlanSuffix(rs.getInt("PLNSFX"));
				hitrpfObj.setProcSeqNo(rs.getInt("PRCSEQ"));
				hitrpfObj.setBatccoy(rs.getString("BATCCOY"));
				hitrpfObj.setBatcbrn(rs.getString("BATCBRN"));
				hitrpfObj.setBatcactyr(rs.getInt("BATCACTYR"));
				hitrpfObj.setBatcactmn(rs.getInt("BATCACTMN"));
				hitrpfObj.setBatctrcde(rs.getString("BATCTRCDE"));
				hitrpfObj.setBatcbatch(rs.getString("BATCBATCH"));
				hitrpfObj.setZintbfnd(rs.getString("ZINTBFND"));
				hitrpfObj.setFundAmount(rs.getBigDecimal("FUNDAMNT"));
				hitrpfObj.setFundCurrency(rs.getString("FNDCURR").trim());
				hitrpfObj.setFundRate(rs.getBigDecimal("FUNDRATE"));
				hitrpfObj.setZrectyp(rs.getString("ZRECTYP"));
				hitrpfObj.setEffdate(rs.getInt("EFFDATE"));
				hitrpfObj.setTranno(rs.getInt("TRANNO"));
				hitrpfObj.setCntcurr(rs.getString("CNTCURR"));
				hitrpfObj.setContractAmount(rs.getBigDecimal("CNTAMNT"));
				hitrpfObj.setCnttyp(rs.getString("CNTTYP"));
				hitrpfObj.setCrtable(rs.getString("CRTABLE"));
				hitrpfObj.setCovdbtind(rs.getString("COVDBTIND"));
				hitrpfObj.setZintappind(rs.getString("ZINTAPPIND"));
				hitrpfObj.setFeedbackInd(rs.getString("FDBKIND"));
				hitrpfObj.setTriggerModule(rs.getString("TRIGER"));
				hitrpfObj.setTriggerKey(rs.getString("TRIGKY"));
				hitrpfObj.setSwitchIndicator(rs.getString("SWCHIND"));
				hitrpfObj.setSurrenderPercent(rs.getBigDecimal("PERSUR"));
				hitrpfObj.setSvp(rs.getBigDecimal("SVP"));
				hitrpfObj.setZlstintdte(rs.getInt("ZLSTINTDTE"));
				hitrpfObj.setZintrate(rs.getBigDecimal("ZINTRATE"));
				hitrpfObj.setZinteffdt(rs.getInt("ZINTEFFDT"));
				hitrpfObj.setZintalloc(rs.getString("ZINTALLOC"));
				hitrpfObj.setInciNum(rs.getInt("INCINUM"));
				hitrpfObj.setInciPerd01(rs.getInt("INCIPERD01"));
				hitrpfObj.setInciPerd02(rs.getInt("INCIPERD02"));
				hitrpfObj.setInciprm01(rs.getBigDecimal("INCIPRM01"));
				hitrpfObj.setInciprm02(rs.getBigDecimal("INCIPRM02"));
				hitrpfObj.setUstmno(rs.getInt("USTMNO"));
				hitrpfObj.setSacscode(rs.getString("SACSCODE"));
				hitrpfObj.setSacstyp(rs.getString("SACSTYP"));
				hitrpfObj.setGenlcde(rs.getString("GENLCDE"));
				hitrpfObj.setScheduleName(rs.getString("BSCHEDNAM"));
				hitrpfObj.setScheduleNumber(rs.getInt("BSCHEDNUM"));
				hitrpfObj.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
			}
		} catch (SQLException e) {
			LOGGER.error("getHitsRecord() ", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps ,null);
		}
		return hitrpfObj;
	}

	@Override
	public void bulkUpdatedHitralo(List<Hitrpf> hitrBulkOpList) {
		String sql = "UPDATE HITRALO SET FUNDAMNT=?, FDBKIND=?, FUNDRATE=?, CNTAMNT=?, BSCHEDNAM=?, BSCHEDNUM=?  WHERE UNIQUE_NUMBER=?";
		PreparedStatement psHitrpfUpdate = getPrepareStatement(sql);
        try {
        	for(Hitrpf hitralo : hitrBulkOpList){
        		psHitrpfUpdate.setBigDecimal(1, hitralo.getFundAmount());
        		psHitrpfUpdate.setString(2, hitralo.getFeedbackInd());
        		psHitrpfUpdate.setBigDecimal(3, hitralo.getFundRate());
        		psHitrpfUpdate.setBigDecimal(4, hitralo.getContractAmount());
        		psHitrpfUpdate.setString(5, hitralo.getScheduleName());
        		psHitrpfUpdate.setInt(6, hitralo.getScheduleNumber());
        		psHitrpfUpdate.setLong(7, hitralo.getUniqueNumber());
        		psHitrpfUpdate.addBatch();
        	}
       	psHitrpfUpdate.executeBatch();
        } catch (SQLException e) {
			LOGGER.error("bulkUpdatedHitralo()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psHitrpfUpdate, null);
        }
	}
	
	public List<String> checkHitrrnlRecordByChdrnum(String coy, List<String> chdrnumList){
		
		StringBuilder sqlHitrSelect1 = new StringBuilder(
				"SELECT CHDRNUM FROM HITRRNL WHERE CHDRCOY=? AND ");
		sqlHitrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));

		PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1.toString());
		ResultSet sqlhitrpf1rs = null;
		List<String> hitrpfSearchResult = new ArrayList<>();
		try {
			psHitrSelect.setString(1, coy);
			sqlhitrpf1rs = executeQuery(psHitrSelect);
			while (sqlhitrpf1rs.next()) {
				hitrpfSearchResult.add(sqlhitrpf1rs.getString("CHDRNUM"));
			}

		} catch (SQLException e) {
			LOGGER.error("checkHitrrnlRecordByChdrnum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitrSelect, sqlhitrpf1rs);
		}
		return hitrpfSearchResult;
	}
	
	public List<Hitrpf> searchHitrrnlRecord(String coy, String chdrnum) {

		StringBuilder sqlHitrpf = new StringBuilder("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, ZINTBFND ");
		sqlHitrpf.append(" FROM HITRRNL WHERE CHDRCOY=? AND CHDRNUM=? ");
		sqlHitrpf.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sqlHitrpf.toString());
		ResultSet rs = null;
		List<Hitrpf> hitrrnlList = new LinkedList<>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = ps.executeQuery();
			while (rs.next()) {
				Hitrpf hitrpfObj = new Hitrpf();
				hitrpfObj.setChdrcoy(rs.getString("CHDRCOY"));
				hitrpfObj.setChdrnum(rs.getString("CHDRNUM"));
				hitrpfObj.setLife(rs.getString("LIFE"));
				hitrpfObj.setCoverage(rs.getString("COVERAGE"));
				hitrpfObj.setRider(rs.getString("RIDER"));
				hitrpfObj.setPlanSuffix(rs.getInt("PLNSFX"));
				hitrpfObj.setZintbfnd(rs.getString("ZINTBFND"));
				hitrrnlList.add(hitrpfObj);
			}
		} catch (SQLException e) {
			LOGGER.error("searchHitrrnlRecord() ", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return hitrrnlList;
	}
	
	public List<Hitrpf> searchHitrRecord(String chdrcoy, String chdrnum) {

		StringBuilder sqlHitrSelect1 = new StringBuilder("SELECT * ");
		sqlHitrSelect1.append(" FROM HITR WHERE CHDRCOY=? AND CHDRNUM=? ");
		sqlHitrSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1.toString());
		ResultSet sqlhitrpf1rs = null;
		List<Hitrpf> hitrpfList = new LinkedList<>();
		try {
			psHitrSelect.setString(1, chdrcoy);
			psHitrSelect.setString(2, chdrnum);
			sqlhitrpf1rs = executeQuery(psHitrSelect);
			while (sqlhitrpf1rs.next()) {
				Hitrpf hitrpf = new Hitrpf();
				hitrpf.setChdrcoy(sqlhitrpf1rs.getString("CHDRCOY"));
				hitrpf.setChdrnum(sqlhitrpf1rs.getString("CHDRNUM"));
				hitrpf.setLife(sqlhitrpf1rs.getString("LIFE"));
				hitrpf.setCoverage(sqlhitrpf1rs.getString("COVERAGE"));
				hitrpf.setRider(sqlhitrpf1rs.getString("RIDER"));
				hitrpf.setPlanSuffix(sqlhitrpf1rs.getInt("PLNSFX"));
				hitrpf.setProcSeqNo(sqlhitrpf1rs.getInt("PRCSEQ"));
				hitrpf.setBatccoy(sqlhitrpf1rs.getString("BATCCOY"));
				hitrpf.setBatcbrn(sqlhitrpf1rs.getString("BATCBRN"));
				hitrpf.setBatcactyr(sqlhitrpf1rs.getInt("BATCACTYR"));
				hitrpf.setBatcactmn(sqlhitrpf1rs.getInt("BATCACTMN"));
				hitrpf.setBatctrcde(sqlhitrpf1rs.getString("BATCTRCDE"));
				hitrpf.setBatcbatch(sqlhitrpf1rs.getString("BATCBATCH"));
				hitrpf.setZintbfnd(sqlhitrpf1rs.getString("ZINTBFND"));
				hitrpf.setFundAmount(sqlhitrpf1rs.getBigDecimal("FUNDAMNT"));
				hitrpf.setFundCurrency(sqlhitrpf1rs.getString("FNDCURR").trim());
				hitrpf.setFundRate(sqlhitrpf1rs.getBigDecimal("FUNDRATE"));
				hitrpf.setZrectyp(sqlhitrpf1rs.getString("ZRECTYP"));
				hitrpf.setEffdate(sqlhitrpf1rs.getInt("EFFDATE"));
				hitrpf.setTranno(sqlhitrpf1rs.getInt("TRANNO"));
				hitrpf.setCntcurr(sqlhitrpf1rs.getString("CNTCURR"));
				hitrpf.setContractAmount(sqlhitrpf1rs.getBigDecimal("CNTAMNT"));
				hitrpf.setCnttyp(sqlhitrpf1rs.getString("CNTTYP"));
				hitrpf.setCrtable(sqlhitrpf1rs.getString("CRTABLE"));
				hitrpf.setCovdbtind(sqlhitrpf1rs.getString("COVDBTIND"));
				hitrpf.setZintappind(sqlhitrpf1rs.getString("ZINTAPPIND"));
				hitrpf.setFeedbackInd(sqlhitrpf1rs.getString("FDBKIND"));
				hitrpf.setTriggerModule(sqlhitrpf1rs.getString("TRIGER"));
				hitrpf.setTriggerKey(sqlhitrpf1rs.getString("TRIGKY"));
				hitrpf.setSwitchIndicator(sqlhitrpf1rs.getString("SWCHIND"));
				hitrpf.setSurrenderPercent(sqlhitrpf1rs.getBigDecimal("PERSUR"));
				hitrpf.setSvp(sqlhitrpf1rs.getBigDecimal("SVP"));
				hitrpf.setZlstintdte(sqlhitrpf1rs.getInt("ZLSTINTDTE"));
				hitrpf.setZintrate(sqlhitrpf1rs.getBigDecimal("ZINTRATE"));
				hitrpf.setZinteffdt(sqlhitrpf1rs.getInt("ZINTEFFDT"));
				hitrpf.setZintalloc(sqlhitrpf1rs.getString("ZINTALLOC"));
				hitrpf.setInciNum(sqlhitrpf1rs.getInt("INCINUM"));
				hitrpf.setInciPerd01(sqlhitrpf1rs.getInt("INCIPERD01"));
				hitrpf.setInciPerd02(sqlhitrpf1rs.getInt("INCIPERD02"));
				hitrpf.setInciprm01(sqlhitrpf1rs.getBigDecimal("INCIPRM01"));
				hitrpf.setInciprm02(sqlhitrpf1rs.getBigDecimal("INCIPRM02"));
				hitrpf.setUstmno(sqlhitrpf1rs.getInt("USTMNO"));
				hitrpf.setSacscode(sqlhitrpf1rs.getString("SACSCODE"));
				hitrpf.setSacstyp(sqlhitrpf1rs.getString("SACSTYP"));
				hitrpf.setGenlcde(sqlhitrpf1rs.getString("GENLCDE"));
				hitrpf.setUniqueNumber(sqlhitrpf1rs.getLong("UNIQUE_NUMBER"));
				hitrpfList.add(hitrpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHitrintRecordByChdrnum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitrSelect, sqlhitrpf1rs);
		}
		return hitrpfList;

	}
	
	public List<Hitrpf> searchHitraloRecordForDel(String chdrcoy, String chdrnum) {
		String sqlHitrSelect1 = "SELECT UNIQUE_NUMBER FROM HITRALO WHERE CHDRCOY=? AND CHDRNUM=? ";

		PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1);
		ResultSet sqlhitrpf1rs = null;
		List<Hitrpf> hitrpfList = new ArrayList<>();
		try {
			psHitrSelect.setString(1, chdrcoy);
			psHitrSelect.setString(2, chdrnum);
			sqlhitrpf1rs = executeQuery(psHitrSelect);
			while (sqlhitrpf1rs.next()) {
				Hitrpf hitrpf = new Hitrpf();
				hitrpf.setUniqueNumber(sqlhitrpf1rs.getLong("UNIQUE_NUMBER"));
				hitrpfList.add(hitrpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchHitrRecordForDel()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitrSelect, sqlhitrpf1rs);
		}
		return hitrpfList;
	}
	
	public void deleteHitrpfRecord(List<Hitrpf> hitrpfList) {
		String sql = "DELETE FROM HITRPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Hitrpf u:hitrpfList){
				ps.setLong(1, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteHitrpfRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
 //ILIFE-8786 start
	@Override
	public List<Hitrpf> searchHitrRecord(Hitrpf hitr) {
		StringBuilder sqlHitrSelect1 = new StringBuilder("SELECT * ");
		sqlHitrSelect1.append(" FROM HITR WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND PLNSFX=? AND TRANNO=? AND PRCSEQ=?");
		sqlHitrSelect1
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1.toString());
		ResultSet sqlhitrpf1rs = null;
		List<Hitrpf> hitrpfList = new LinkedList<>();
		try {
			psHitrSelect.setString(1, hitr.getChdrcoy());
			psHitrSelect.setString(2, hitr.getChdrnum());
			psHitrSelect.setString(3, hitr.getLife());
			psHitrSelect.setString(4, hitr.getCoverage());
			psHitrSelect.setInt(5, hitr.getPlanSuffix());
			psHitrSelect.setInt(6, hitr.getTranno());
			psHitrSelect.setInt(7, hitr.getProcSeqNo());
			
			sqlhitrpf1rs = executeQuery(psHitrSelect);
			while (sqlhitrpf1rs.next()) {
				Hitrpf hitrpf = new Hitrpf();
				hitrpf.setChdrcoy(sqlhitrpf1rs.getString("CHDRCOY"));
				hitrpf.setChdrnum(sqlhitrpf1rs.getString("CHDRNUM"));
				hitrpf.setLife(sqlhitrpf1rs.getString("LIFE"));
				hitrpf.setCoverage(sqlhitrpf1rs.getString("COVERAGE"));
				hitrpf.setRider(sqlhitrpf1rs.getString("RIDER"));
				hitrpf.setPlanSuffix(sqlhitrpf1rs.getInt("PLNSFX"));
				hitrpf.setProcSeqNo(sqlhitrpf1rs.getInt("PRCSEQ"));
				hitrpf.setBatccoy(sqlhitrpf1rs.getString("BATCCOY"));
				hitrpf.setBatcbrn(sqlhitrpf1rs.getString("BATCBRN"));
				hitrpf.setBatcactyr(sqlhitrpf1rs.getInt("BATCACTYR"));
				hitrpf.setBatcactmn(sqlhitrpf1rs.getInt("BATCACTMN"));
				hitrpf.setBatctrcde(sqlhitrpf1rs.getString("BATCTRCDE"));
				hitrpf.setBatcbatch(sqlhitrpf1rs.getString("BATCBATCH"));
				hitrpf.setZintbfnd(sqlhitrpf1rs.getString("ZINTBFND"));
				hitrpf.setFundAmount(sqlhitrpf1rs.getBigDecimal("FUNDAMNT"));
				hitrpf.setFundCurrency(sqlhitrpf1rs.getString("FNDCURR").trim());
				hitrpf.setFundRate(sqlhitrpf1rs.getBigDecimal("FUNDRATE"));
				hitrpf.setZrectyp(sqlhitrpf1rs.getString("ZRECTYP"));
				hitrpf.setEffdate(sqlhitrpf1rs.getInt("EFFDATE"));
				hitrpf.setTranno(sqlhitrpf1rs.getInt("TRANNO"));
				hitrpf.setCntcurr(sqlhitrpf1rs.getString("CNTCURR"));
				hitrpf.setContractAmount(sqlhitrpf1rs.getBigDecimal("CNTAMNT"));
				hitrpf.setCnttyp(sqlhitrpf1rs.getString("CNTTYP"));
				hitrpf.setCrtable(sqlhitrpf1rs.getString("CRTABLE"));
				hitrpf.setCovdbtind(sqlhitrpf1rs.getString("COVDBTIND"));
				hitrpf.setZintappind(sqlhitrpf1rs.getString("ZINTAPPIND"));
				hitrpf.setFeedbackInd(sqlhitrpf1rs.getString("FDBKIND"));
				hitrpf.setTriggerModule(sqlhitrpf1rs.getString("TRIGER"));
				hitrpf.setTriggerKey(sqlhitrpf1rs.getString("TRIGKY"));
				hitrpf.setSwitchIndicator(sqlhitrpf1rs.getString("SWCHIND"));
				hitrpf.setSurrenderPercent(sqlhitrpf1rs.getBigDecimal("PERSUR"));
				hitrpf.setSvp(sqlhitrpf1rs.getBigDecimal("SVP"));
				hitrpf.setZlstintdte(sqlhitrpf1rs.getInt("ZLSTINTDTE"));
				hitrpf.setZintrate(sqlhitrpf1rs.getBigDecimal("ZINTRATE"));
				hitrpf.setZinteffdt(sqlhitrpf1rs.getInt("ZINTEFFDT"));
				hitrpf.setZintalloc(sqlhitrpf1rs.getString("ZINTALLOC"));
				hitrpf.setInciNum(sqlhitrpf1rs.getInt("INCINUM"));
				hitrpf.setInciPerd01(sqlhitrpf1rs.getInt("INCIPERD01"));
				hitrpf.setInciPerd02(sqlhitrpf1rs.getInt("INCIPERD02"));
				hitrpf.setInciprm01(sqlhitrpf1rs.getBigDecimal("INCIPRM01"));
				hitrpf.setInciprm02(sqlhitrpf1rs.getBigDecimal("INCIPRM02"));
				hitrpf.setUstmno(sqlhitrpf1rs.getInt("USTMNO"));
				hitrpf.setSacscode(sqlhitrpf1rs.getString("SACSCODE"));
				hitrpf.setSacstyp(sqlhitrpf1rs.getString("SACSTYP"));
				hitrpf.setGenlcde(sqlhitrpf1rs.getString("GENLCDE"));
				hitrpf.setUniqueNumber(sqlhitrpf1rs.getLong("UNIQUE_NUMBER"));
				hitrpfList.add(hitrpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHitrRecord()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitrSelect, sqlhitrpf1rs);
		}
		return hitrpfList;
	}

	@Override
	public void updatedHitrpfRecord(List<Hitrpf> hitrList) {
		if (hitrList != null && !hitrList.isEmpty()) {
            String SQL_Hitr_UPDATE = "UPDATE HITRPF SET CHDRCOY=?,CHDRNUM=?,LIFE=?,COVERAGE=?,RIDER=?,TRANNO=?,BATCCOY=?,BATCBRN=?,BATCACTYR=?,BATCACTMN=?,BATCTRCDE=?,BATCBATCH=?,PRCSEQ=?, "
            		+ "EFFDATE=?,CRTABLE=?,PLNSFX=?,CNTCURR=?,CNTTYP=?,ZINTBFND=?,FNDCURR=?,SACSCODE=?,SACSTYP=?,GENLCDE=?,ZRECTYP=?,CNTAMNT=?,INCIPRM01=?,INCIPRM02=?,USTMNO=?,ZINTRATE=?,"
            		+ "INCINUM=?,INCIPERD01=?,INCIPERD02=?,FUNDAMNT=?,PERSUR=?,FUNDRATE=?,SVP=?,SWCHIND=?,FDBKIND=?,TRIGER=?,TRIGKY=?,ZINTAPPIND=?,ZLSTINTDTE=?,ZINEFFDT=?,FUNDPOOL=?"
            		+ "JOBNM=?,USRPRF=?,DATIME=?  WHERE UNIQUE_NUMBER=?";

            PreparedStatement psHitrUpdate = getPrepareStatement(SQL_Hitr_UPDATE);
            try {
                for (Hitrpf c : hitrList) {
                    psHitrUpdate.setString(1, c.getChdrcoy());
                    psHitrUpdate.setString(2, c.getChdrnum());
                    psHitrUpdate.setString(3, c.getLife());
                    psHitrUpdate.setString(4, c.getCoverage());
                    psHitrUpdate.setString(5, c.getRider());
                    psHitrUpdate.setInt(6, c.getTranno());
                    psHitrUpdate.setString(7, c.getBatccoy());
                    psHitrUpdate.setString(8, c.getBatcbrn());
                    psHitrUpdate.setInt(9, c.getBatcactyr());
                    psHitrUpdate.setInt(10, c.getBatcactmn());
                    psHitrUpdate.setString(11, c.getBatctrcde());
                    psHitrUpdate.setString(12, c.getBatcbatch());
                    psHitrUpdate.setInt(13, c.getProcSeqNo());
                    psHitrUpdate.setInt(14, c.getEffdate());
                    psHitrUpdate.setString(15, c.getCrtable());
                    psHitrUpdate.setInt(16, c.getPlanSuffix());
                    psHitrUpdate.setString(17, c.getCntcurr());
                    psHitrUpdate.setString(18, c.getCnttyp());
                    psHitrUpdate.setString(19, c.getZintbfnd());
                    psHitrUpdate.setString(20, c.getFundCurrency());
                    psHitrUpdate.setString(21, c.getSacscode());
                    psHitrUpdate.setString(22, c.getSacstyp());
                    psHitrUpdate.setString(23, c.getGenlcde());
                    psHitrUpdate.setString(24, c.getZrectyp());
                    psHitrUpdate.setBigDecimal(25, c.getContractAmount());
                    psHitrUpdate.setBigDecimal(26, c.getInciprm01());
                    psHitrUpdate.setBigDecimal(27, c.getInciprm02());
                    psHitrUpdate.setInt(28, c.getUstmno());
                    psHitrUpdate.setBigDecimal(29, c.getZintrate());
                    psHitrUpdate.setInt(30, c.getInciNum());
                    psHitrUpdate.setInt(31, c.getInciPerd01());
                    psHitrUpdate.setInt(32, c.getInciPerd02());
                    psHitrUpdate.setBigDecimal(33, c.getFundAmount());
                    psHitrUpdate.setBigDecimal(34, c.getSurrenderPercent());
                    psHitrUpdate.setBigDecimal(35, c.getFundRate());
                    psHitrUpdate.setBigDecimal(36, c.getSvp());
                    psHitrUpdate.setString(37, c.getSwitchIndicator());
                    psHitrUpdate.setString(38, c.getFeedbackInd());
                    psHitrUpdate.setString(39, c.getTriggerModule());
                    psHitrUpdate.setString(40, c.getTriggerKey());
                    psHitrUpdate.setString(41, c.getZintappind());
                    psHitrUpdate.setInt(42, c.getZlstintdte());
                    psHitrUpdate.setInt(43, c.getZinteffdt());
                    psHitrUpdate.setString(44, c.getFundPool());
                    psHitrUpdate.setString(45, getJobnm());
                    psHitrUpdate.setString(46, getUsrprf());
                    psHitrUpdate.setTimestamp(47, new Timestamp(System.currentTimeMillis()));
                    psHitrUpdate.setLong(48, c.getUniqueNumber());					
                    psHitrUpdate.addBatch();
                }
                psHitrUpdate.executeBatch();
            } catch (SQLException e) {
				LOGGER.error("updatedHitrpfRecord()", e); 
                throw new SQLRuntimeException(e);
            } finally {
                close(psHitrUpdate, null);
            }
        }
	}
 //ILIFE-8786 end
	
	public List<Hitrpf> searchHitrdryRecord(String chdrcoy, String chdrnum) {
		StringBuilder sqlHitrSelect1 = new StringBuilder("SELECT FDBKIND, EFFDATE FROM HITRDRY WHERE CHDRCOY=? AND CHDRNUM=?");
		sqlHitrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, EFFDATE ASC, UNIQUE_NUMBER DESC");
		PreparedStatement psHitrSelect = getPrepareStatement(sqlHitrSelect1.toString());
		ResultSet sqlhitrpf1rs = null;
		List<Hitrpf> hitrpfList = new LinkedList<>();
		try {
			psHitrSelect.setString(1, chdrcoy);
			psHitrSelect.setString(2, chdrnum);
			sqlhitrpf1rs = executeQuery(psHitrSelect);
			while (sqlhitrpf1rs.next()) {
				Hitrpf hitrpf = new Hitrpf();
				hitrpf.setFeedbackInd(sqlhitrpf1rs.getString("FDBKIND"));
				hitrpf.setEffdate(sqlhitrpf1rs.getInt("EFFDATE"));
				hitrpfList.add(hitrpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchHitrdryRecord()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(psHitrSelect, sqlhitrpf1rs);
		}
		return hitrpfList;

	}
}