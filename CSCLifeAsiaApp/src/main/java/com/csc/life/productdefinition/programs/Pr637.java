/*
 * File: Pr637.java
 * Date: 30 August 2009 1:50:26
 * Author: Quipoz Limited
 *
 * Class transformed from PR637.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.screens.Sr637ScreenVars;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*            MEDICAL PROVIDER BANK ACCOUNT DETAILS
*            =====================================
*
* Initialise
* ----------
*
*  Read ZMPV (RETRV) in order to obtain the Medical provider
*  information. Load the screen from the record read.  Store the
*  current  payee  (or if blank the client), the agency currency
*  and factoring house in hidden screen fields.
*
*  If the current bank number  and account number are not blank,
*  look up the client  bank  account details (CLBL). Cross check
*  that the client for the bank details found is the same as the
*  "hidden" (see above) payee. If  they  are not the same, blank
*  out  the current  factoring  house,  bank/branch  number  and
*  account number. (In this case,  the payee has changed, so the
*  old bank account details cannot be applicable.)
*
*  Otherwise (details not blank  and  are  for  correct client),
*  look up the description of the bank/branch (BABR).
*
* Validation
* ----------
*
*  If in enquiry  mode  (WSSP-FLAG  is  "I")  protect the screen
*  prior to output.
*
*  If the screen  is  "KILLed" or when in enquiry mode, skip the
*  validation.
*
*  Validate the individual fields according to the rules defined
*  in the screen and field help.
*
*  Cross check the account details  read  (as  part of the above
*  validation). The client  number  from  these  account details
*  must  be the same as  the  "hidden"  payee.  If  the  account
*  details are for a  single currency (currency not blank), this
*  currency must be the same as the agency currency (NB - do not
*  check   against   the  "hidden"  currency  as  this  can  get
*  overwritten be the account number window).
*
*  If "CALC" was pressed,  re-display  the screen to display the
*  bank/branch and account descriptions.
*
* Updating
* --------
*
*  Skip the updating  if  "KILL"  was  pressed  or if in enquiry
*  mode.
*
*  Store the following in the ZMPV
*            Bank/branch code,
*            Account number.
*
* Next Program
* ------------
*
*  Add one to the pointer and exit.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr637 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR637");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaBankdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 0);
	private FixedLengthStringData wsaaBranchdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 30);
		/* ERRORS */
	private String e182 = "E182";
	private String f906 = "F906";
	private String g599 = "G599";
	private String g900 = "G900";
	private String t745 = "T745";
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Sr637ScreenVars sv = ScreenProgram.getScreenVars( Sr637ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		bankBranchDesc1120,
		exit1190,
		preExit,
		exit2090,
		exit3090
	}

	public Pr637() {
		super();
		screenVars = sv;
		new ScreenModel("Sr637", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		checkForBankDets1050();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		/*RETRV-ZMPV*/
		zmpvIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		/*LOAD-HIDDEN-FIELDS*/
		sv.payrnum.set(zmpvIO.getZmednum());
		sv.numsel.set(zmpvIO.getZmednum());
	}

protected void checkForBankDets1050()
	{
		if (isNE(zmpvIO.getBankkey(),SPACES)
		&& isNE(zmpvIO.getBankacckey(),SPACES)) {
			sv.bankkey.set(zmpvIO.getBankkey());
			sv.bankacckey.set(zmpvIO.getBankacckey());
			bankDetails1100();
		}
		/*EXIT*/
	}

protected void bankDetails1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readAccountDets1110();
				}
				case bankBranchDesc1120: {
					bankBranchDesc1120();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readAccountDets1110()
	{
		if (isEQ(sv.bankkey,SPACES)) {
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(sv.bankacckey,SPACES)) {
			goTo(GotoLabel.bankBranchDesc1120);
		}
		clblIO.setDataKey(SPACES);
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)
		&& isNE(clblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			syserrrec.statuz.set(clblIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(g599);
			sv.bankacckeyErr.set(g599);
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			zmpvIO.setBankkey(SPACES);
			zmpvIO.setBankacckey(SPACES);
			goTo(GotoLabel.exit1190);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
	}

protected void bankBranchDesc1120()
	{
		babrIO.setDataKey(SPACES);
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			syserrrec.statuz.set(babrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(f906);
		}
		wsaaBankkey.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsaaBankdesc);
		sv.branchdesc.set(wsaaBranchdesc);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateFields2030();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateFields2030()
	{
		bankDetails1100();
		if (isEQ(sv.bankkey,SPACES)
		&& isEQ(sv.bankacckey,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.bankkey,SPACES)) {
			sv.bankkeyErr.set(g900);
		}
		if (isEQ(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(e182);
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.bankacckeyErr.set(t745);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
			keepsZmpv3020();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void keepsZmpv3020()
	{
		zmpvIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		zmpvIO.setBankkey(sv.bankkey);
		zmpvIO.setBankacckey(sv.bankacckey);
		zmpvIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
