package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.MerxTemppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MerxTemppfDAO extends BaseDAO<MerxTemppf> {
	
	public boolean insertMerxTempRecords(List<MerxTemppf> merxList, String tempTableName);

}
