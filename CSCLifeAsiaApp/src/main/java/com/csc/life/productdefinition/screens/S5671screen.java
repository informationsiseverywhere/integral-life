package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5671screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5671ScreenVars sv = (S5671ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5671screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5671ScreenVars screenVars = (S5671ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.pgm01.setClassString("");
		screenVars.pgm02.setClassString("");
		screenVars.pgm03.setClassString("");
		screenVars.pgm04.setClassString("");
		screenVars.subprog01.setClassString("");
		screenVars.subprog02.setClassString("");
		screenVars.subprog03.setClassString("");
		screenVars.subprog04.setClassString("");
		screenVars.edtitm01.setClassString("");
		screenVars.edtitm02.setClassString("");
		screenVars.edtitm03.setClassString("");
		screenVars.edtitm04.setClassString("");
		screenVars.trevsub01.setClassString("");
		screenVars.trevsub02.setClassString("");
		screenVars.trevsub03.setClassString("");
		screenVars.trevsub04.setClassString("");
	}

/**
 * Clear all the variables in S5671screen
 */
	public static void clear(VarModel pv) {
		S5671ScreenVars screenVars = (S5671ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.pgm01.clear();
		screenVars.pgm02.clear();
		screenVars.pgm03.clear();
		screenVars.pgm04.clear();
		screenVars.subprog01.clear();
		screenVars.subprog02.clear();
		screenVars.subprog03.clear();
		screenVars.subprog04.clear();
		screenVars.edtitm01.clear();
		screenVars.edtitm02.clear();
		screenVars.edtitm03.clear();
		screenVars.edtitm04.clear();
		screenVars.trevsub01.clear();
		screenVars.trevsub02.clear();
		screenVars.trevsub03.clear();
		screenVars.trevsub04.clear();
	}
}
