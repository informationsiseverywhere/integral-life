package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR634
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr634ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(96);
	public FixedLengthStringData dataFields = new FixedLengthStringData(48).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnam = DD.clntnam.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 48);
	public FixedLengthStringData clntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData zmedprvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 60);
	public FixedLengthStringData[] clntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] zmedprvOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(163);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(65).isAPartOf(subfileArea, 0);
	public ZonedDecimalData dtetrm = DD.dtetrm.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public FixedLengthStringData hflag = DD.hflag.copy().isAPartOf(subfileFields,16);
	public FixedLengthStringData name = DD.name.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(subfileFields,47);
	public FixedLengthStringData zmmccde = DD.zmmccde.copy().isAPartOf(subfileFields,55);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 65);
	public FixedLengthStringData dtetrmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData hflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData nameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData zdoctorErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData zmmccdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 89);
	public FixedLengthStringData[] dtetrmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] nameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] zdoctorOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] zmmccdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 161);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dtetrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sr634screensflWritten = new LongData(0);
	public LongData Sr634screenctlWritten = new LongData(0);
	public LongData Sr634screenWritten = new LongData(0);
	public LongData Sr634windowWritten = new LongData(0);
	public LongData Sr634hideWritten = new LongData(0);
	public LongData Sr634protectWritten = new LongData(0);
	public GeneralTable sr634screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr634screensfl;
	}

	public Sr634ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zdoctorOut,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"52",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dtetrmOut,new String[] {"53",null, "-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmmccdeOut,new String[] {"51",null, "-51",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hflag, zdoctor, name, effdate, dtetrm, zmmccde};
		screenSflOutFields = new BaseData[][] {hflagOut, zdoctorOut, nameOut, effdateOut, dtetrmOut, zmmccdeOut};
		screenSflErrFields = new BaseData[] {hflagErr, zdoctorErr, nameErr, effdateErr, dtetrmErr, zmmccdeErr};
		screenSflDateFields = new BaseData[] {effdate, dtetrm};
		screenSflDateErrFields = new BaseData[] {effdateErr, dtetrmErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp, dtetrmDisp};

		screenFields = new BaseData[] {zmedprv, clntnum, clntnam};
		screenOutFields = new BaseData[][] {zmedprvOut, clntnumOut, clntnamOut};
		screenErrFields = new BaseData[] {zmedprvErr, clntnumErr, clntnamErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr634screen.class;
		screenSflRecord = Sr634screensfl.class;
		screenCtlRecord = Sr634screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr634protect.class;
		hideRecord = Sr634hide.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr634screenctl.lrec.pageSubfile);
	}
}
