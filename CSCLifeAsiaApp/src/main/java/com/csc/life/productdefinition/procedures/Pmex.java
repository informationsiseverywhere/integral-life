
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.dao.AgcmpfDAO;
import com.csc.life.agents.dataaccess.model.Agcmpf;
import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;	//ILIFE-7584
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.recordstructures.PmexAcblRec;
import com.csc.life.productdefinition.recordstructures.PmexRec;
import com.csc.life.productdefinition.recordstructures.PmexVpxRec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Pmex extends SMARTCodeModel{
	
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String SPACES = "";
	private static final String wsaaSubr = "PRMPMEX";
	private Premiumrec premiumrec = new Premiumrec();
	private Premiumrec premiumrecNew = new Premiumrec();
	private PmexRec pmexRec = new PmexRec();	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();	
	private Vpxlextrec vpxlextrec = new Vpxlextrec();
	private Vpxlextrec vpxlextrecNew = new Vpxlextrec();
	private Vpxacblrec vpxacblrec=new Vpxacblrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Freqcpy freqcpy = new Freqcpy();
	
	private Map<String,List<String>> inPutParam = new HashMap<>();
	private Map<String,List<List<String>>> inPutParam2 = new HashMap<>();
	private Map<String,List<List<List<String>>>> inPutParam3 = new HashMap<>();
	private List<Lifepf> lifepfList;
	private PmexVpxRec vpxRec = new PmexVpxRec();
	private PmexAcblRec acblRec = new PmexAcblRec();
	
	private T5675rec t5675rec = new T5675rec();
	private T5688rec t5688rec = new T5688rec();
	private T5645rec t5645rec = new T5645rec();
	private List<Itempf> itempfList;
	private List<Covtpf> covtList;
	private List<Covrpf> covrList;
	
	//DAO calls
  	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
  	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO",CovtpfDAO.class);	
  	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAOP6351",CovrpfDAO.class);	
	
	private Rcvdpf rcvdpf;
	private RcvdpfDAO rcvdpfDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO" , ClntpfDAO.class);
	private Clexpf clexpf;
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO" , ClexpfDAO.class);
	private Lextpf lextpf;
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private Acblpf acblpf;
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO2", AgcmpfDAO.class);  // ILIFE-8163
	private Covtpf covtpfModifyRecord;
	private List<Covrpf> covrpfModifyRecord;
	private Map<String, Rcvdpf> rcvdMap = new HashMap<>();
	private Map<String, Covtpf> covtMap = new HashMap<>();
 	private String t5675 ="T5675";
 	private String t5688 ="T5688";
 	private String t5645 ="T5645";
	private boolean noLifeFound;
	private boolean noCvgFound;
	private boolean modifyFlag;
	private int count = 0;
	private boolean firstTime;
	private LinkageInfoService linkgService;
	private boolean tablereadFlag=false;
	protected Agecalcrec agecalcrec = new Agecalcrec();	//ILIFE-7584
	private Map<String, Clntpf> clntpfMap;
	private List<String> clntnumList;
	private List<Covtpf> updateCovr;
	private String[] key;
	private Map<String, List<String>> covrRiderMap;
	private List<String> cvgRiderList;
	private HashMap<Long,List<com.csc.life.productdefinition.dataaccess.model.Covrpf>> reratedPremKeeps  = new LinkedHashMap<>();
	private Map<String,String> lifeDetails = new HashMap<>();
	protected PackedDecimalData wsaaRcdStore = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator statusValid = new Validator(wsaaValidStatus, "Y");
	private Validator statusNotValid = new Validator(wsaaValidStatus, "N");
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private T5679rec t5679rec = new T5679rec();
	
	@Override
	public void mainline(Object... parmArray)
	{
		reratedPremKeeps = (HashMap<Long,List<com.csc.life.productdefinition.dataaccess.model.Covrpf>>) convertAndSetParam(reratedPremKeeps, parmArray, 3);
		vpxacblrec.vpxacblRec = convertAndSetParam(vpxacblrec.vpxacblRec, parmArray, 2);
		vpxlextrec.vpxlextRec = convertAndSetParam(vpxlextrec.vpxlextRec, parmArray, 1);
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);		
		if(null !=parmArray && parmArray.length>1) {//ILIFE-8537
			vpxlextrecNew = (Vpxlextrec) parmArray[1];
		} 
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
		
	}
	protected void startSubr010()
	{	
		premiumrec.statuz.set(varcom.oK);
		pmexRec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		if(("N").equals(premiumrec.firstCovr.toString())) {
			setPrem();
			exitProgram();
		}
		initialise();
		exitProgram();
	}
	
	protected void initialise(){
		if(("Y").equals(premiumrec.setPmexCall.toString())){
			setCommonFields();
			readChdrData();
			initMap();
			readLifepf();		
			readClntpf();
			readCovtpf();
			readLextpf();
			callSubroutine();
		}
		else{
			if(isLT(vpxlextrec.count,ZERO))//ILIFE-8299
				vpxlextrec.count.set(ZERO);
			callProgram("PRMPM31", premiumrec.premiumRec, vpxlextrec.vpxlextRec, vpxacblrec.vpxacblRec);
			return;
		}
	}
	
	protected void readChdrData(){
		pmexRec.setClntnum(premiumrec.cownnum.toString().trim());
		readClexpf();
	}
//START OF ILIFE-7584
	protected String calculateClientAge(Clntpf clntpf){
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(premiumrec.language.toString());
		agecalcrec.cnttype.set(premiumrec.cnttype.toString());
		agecalcrec.intDate1.set(clntpf.getCltdob());
		agecalcrec.intDate2.set(premiumrec.occdate);
		agecalcrec.company.set("9");
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError9000();
		}
		return agecalcrec.agerating.toString();
	}
//END OF ILIFE-7584
	protected void readClexpf(){
		clexpf = new Clexpf();
		clexpf = clexpfDAO.getClexpfByClntkey("CN", "9", pmexRec.getClntnum());
		if(clexpf != null)
			pmexRec.setRstaflag(clexpf.getRstaflag());
		else
			pmexRec.setRstaflag(SPACE);
	}
	private void initMap(){
		//Life Details
		lifepfList = new ArrayList<>();
		inPutParam.put("lifeId", new ArrayList<String>());
		inPutParam.put("lAge", new ArrayList<String>());
		inPutParam.put("lSex", new ArrayList<String>());
		inPutParam.put("lifeJlife", new ArrayList<String>());
		inPutParam.put("jlAge", new ArrayList<String>());
		inPutParam.put("jlSex", new ArrayList<String>());
		inPutParam.put("occpCode", new ArrayList<String>());
		inPutParam.put("dob", new ArrayList<String>());
		//Coverage Details
		inPutParam.put("noOfCoverages", new ArrayList<String>());
		inPutParam.put("aidsCoverInd", new ArrayList<String>());
		inPutParam.put("totalSumInsured", new ArrayList<String>());
		inPutParam.put("totalPrevSumInsured", new ArrayList<String>());//ILIFE-8502
		inPutParam.put("ncdCode", new ArrayList<String>());
		inPutParam.put("claimAutoIncrease", new ArrayList<String>());
		inPutParam.put("syndicateCode", new ArrayList<String>());
		inPutParam.put("riskExpiryAge", new ArrayList<String>());
		inPutParam.put("oppc", new ArrayList<String>());
		inPutParam.put("zmortpct", new ArrayList<String>());
		inPutParam.put("opcda", new ArrayList<String>());
		inPutParam.put("agerate", new ArrayList<String>());
		inPutParam.put("insprem", new ArrayList<String>());				
		inPutParam.put("count", new ArrayList<String>());
		inPutParam.put("covrcd", new ArrayList<String>());
		inPutParam.put("insprmRsn", new ArrayList<String>());
		inPutParam.put("oppcRsn", new ArrayList<String>());
		
		//lextpf Details
		inPutParam3.put("oppcParentList", new ArrayList<List<List<String>>>()); 
		inPutParam3.put("zmortpctParentList", new ArrayList<List<List<String>>>()); 
		inPutParam3.put("opcdaParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("agerateParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("inspremParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("insprmRsnParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("oppcRsnParentList", new ArrayList<List<List<String>>>());
		
		inPutParam2.put("covrIdParamList",new ArrayList<List<String>>());
		inPutParam2.put("crtableParamList",new ArrayList<List<String>>());
		inPutParam2.put("sumInsParamList",new ArrayList<List<String>>());
		inPutParam2.put("prevSumInsParamList", new ArrayList<List<String>>());//ILIFE-8502
		inPutParam2.put("calcPremParamList", new ArrayList<List<String>>());//ILIFE-8502
		inPutParam2.put("inputPrevPremParamList", new ArrayList<List<String>>());//ILIFE-8537
		inPutParam2.put("mortClsParamList",new ArrayList<List<String>>());		
		inPutParam2.put("riderIdParamList",new ArrayList<List<String>>());	
		inPutParam2.put("durationParamList",new ArrayList<List<String>>());
		inPutParam2.put("linkcovParamList",new ArrayList<List<String>>());
		inPutParam2.put("tpdtypeParamList",new ArrayList<List<String>>());		
		inPutParam2.put("waitPeriodParamList",new ArrayList<List<String>>());
		inPutParam2.put("poltypParamList",new ArrayList<List<String>>());
		inPutParam2.put("benTrmParamList",new ArrayList<List<String>>());
		inPutParam2.put("prmBasisParamList",new ArrayList<List<String>>());
		inPutParam2.put("dialDownOptParamList",new ArrayList<List<String>>());
		inPutParam2.put("lifeLifeParamList", new ArrayList<List<String>>());
		inPutParam2.put("totSingPremParamList", new ArrayList<List<String>>());
		inPutParam2.put("totRegPremParamList", new ArrayList<List<String>>());
		inPutParam2.put("lnkgSubRefNoParamList",new ArrayList<List<String>>());
		inPutParam2.put("lnkgIndParamList",new ArrayList<List<String>>());
		inPutParam2.put("dobParamList",new ArrayList<List<String>>());
		inPutParam2.put("covrcdParamList",new ArrayList<List<String>>());
	}
	protected void readLifepf(){
	
		if("Y".equalsIgnoreCase(premiumrec.validflag.toString())){
			//lifepfList = lifepfDAO.getLifeListData(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString()); 
			lifepfList = lifepfDAO.getLifeRecords(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString(),"1"); 
		}else{
			lifepfList = lifepfDAO.getLifeList(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
		}
			 
			
		if(!lifepfList.isEmpty()){
			setLifeDetails(lifepfList);
		}
	}
	protected void setLifeDetails(List<Lifepf> lifepfList)
	{
		String lifeNo = null;
		int jLifeInd = 0;
		clntnumList = new ArrayList<>();
		for(Lifepf lif : lifepfList){
			if(lifeNo != null){
				if(lif.getLife().equals(lifeNo) && lif.getJlife().equals("00")){
					setLifeData(lif);
				}
				else if(lif.getLife().equals(lifeNo) && lif.getJlife().equals("01")){
					inPutParam.get("lifeJlife").add(lif.getLife());
					inPutParam.get("lAge").add(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
					lifeDetails.put(lif.getLife().concat(lif.getJlife()), String.valueOf(new BigDecimal(lif.getCltdob())));
					inPutParam.get("lSex").add(lif.getCltsex());
					jLifeInd = 1;
				}
				else if(!lif.getLife().equals(lifeNo) && jLifeInd!=1){
					inPutParam.get("lifeJlife").add("00");
					inPutParam.get("jlAge").add(String.valueOf(BigDecimal.ZERO));
					inPutParam.get("jlSex").add(SPACES);					
					inPutParam.get("lifeId").add(lif.getLife());
					inPutParam.get("lAge").add(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
					lifeDetails.put(lif.getLife().concat(lif.getJlife()), String.valueOf(new BigDecimal(lif.getCltdob())));
					inPutParam.get("lSex").add(lif.getCltsex());
				}
				else if(!lif.getLife().equals(lifeNo) && jLifeInd==1){
					jLifeInd=0;
					setLifeData(lif);
				}
				lifeNo = lif.getLife();
			}
			else{			
				inPutParam.get("lifeId").add(lif.getLife());
				inPutParam.get("lAge").add(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
				lifeDetails.put(lif.getLife().concat(lif.getJlife()), String.valueOf(new BigDecimal(lif.getCltdob())));
				inPutParam.get("lSex").add(lif.getCltsex());
				lifeNo = lif.getLife();
				jLifeInd=0;
			}
			inPutParam.get("occpCode").add(lif.getOccup());
			clntnumList.add(lif.getLifcnum());
		}
		if(jLifeInd==0){
			inPutParam.get("lifeJlife").add("00");
			inPutParam.get("jlAge").add(String.valueOf(BigDecimal.ZERO));
			inPutParam.get("jlSex").add(SPACES);		
		}
		pmexRec.setLage(inPutParam.get("lAge"));
		pmexRec.setLsex(inPutParam.get("lSex"));
		pmexRec.setLifeJlife(inPutParam.get("lifeJlife"));
		pmexRec.setJlage(inPutParam.get("jlAge"));
		pmexRec.setJlsex(inPutParam.get("jlSex"));
		pmexRec.setOccpcode(inPutParam.get("occpCode"));
	}
	protected void setLifeData(Lifepf lif){
		inPutParam.get("lifeId").add(lif.getLife());
		inPutParam.get("lAge").add(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
		inPutParam.get("lSex").add(lif.getCltsex());
		lifeDetails.put(lif.getLife().concat(lif.getJlife()), String.valueOf(new BigDecimal(lif.getCltdob())));
	}
	
	protected void readClntpf() {
		inPutParam.put("rstate01", new ArrayList<String>(Collections.nCopies(clntnumList.size(), SPACE)));
		clntpfMap = clntpfDAO.searchClntRecord("CN", "9", clntnumList);
		if(clntpfMap != null && !clntpfMap.isEmpty()) {
			for(Map.Entry<String, Clntpf> clnt: clntpfMap.entrySet()) {
				inPutParam.get("rstate01").set(clntnumList.indexOf(clnt.getValue().getClntnum()), clnt.getValue().getClntStateCd() != null && !clnt.getValue().getClntStateCd().trim().isEmpty() && !clnt.getValue().getClntStateCd().trim().substring(3).isEmpty() ? clnt.getValue().getClntStateCd().trim().substring(3) : SPACES);
				if(clnt.getValue().getClntnum().equals(pmexRec.getClntnum())) {
					pmexRec.setClntSex(clnt.getValue().getCltsex());
					pmexRec.setClntAge(calculateClientAge(clnt.getValue()));	//ILIFE-7584
					pmexRec.setCommTaxInd("Y");//ILIFE-8502
					pmexRec.setStateAtIncep(premiumrec.stateAtIncep.toString());//ILIFE-8502
					
				}
			}
		}
		pmexRec.setRstate01(inPutParam.get("rstate01"));
	}
	
	protected void readCovtpf()
	{
		int rcvd = 0;
		covtList = new ArrayList<>();
		covrList = new ArrayList<>();
		covtpfModifyRecord = new Covtpf();
		Iterator<String> itr = inPutParam.get("lifeId").iterator();
		Iterator<Covtpf> itrtr;
		Iterator<Covrpf> itrtrCovrpf = null;
		List<String> covrRiderList;
		String life;
		Covtpf covt = null;
		covrRiderMap = new HashMap<>();
		String cvg = null;
		cvgRiderList = new ArrayList<>();
		wsaaRcdStore.set(premiumrec.effectdt);
		covrList =covrpfDAO.searchCovrRecordByCoyNumDescUniquNo(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
		covtpfModifyRecord = covtpfDAO.getCovtlnbData(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString(), premiumrec.lifeLife.toString(), premiumrec.covrCoverage.toString(), premiumrec.covrRider.toString());
		covrpfModifyRecord = covrpfDAO.getCovrlnbPF(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString(), premiumrec.lifeLife.toString(), premiumrec.covrCoverage.toString(), premiumrec.covrRider.toString(), premiumrec.plnsfx.toInt());
		if(covtpfModifyRecord != null || covrpfModifyRecord != null){
			modifyFlag = true;
		}
		else{
			modifyFlag = false;
		}
		covtList = covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
		if(covtList != null && !covtList.isEmpty()) {
			key = new String[covtList.size()];
			for(int i=0; i < covtList.size(); i++)
				key[i] = covtList.get(i).getLife() + covtList.get(i).getCoverage() + covtList.get(i).getRider();
		}
		if(!covtList.isEmpty() && !covrList.isEmpty()) {
			tablereadFlag=true;
		}
		if(!("".equals(premiumrec.batcBatctrcde.toString().trim()))) {
			readT5679();
		}
		if(!covrList.isEmpty()){
			itrtrCovrpf = covrList.iterator();
			setCovrData(covt, itrtrCovrpf);
		}

		if(!covtList.isEmpty()){
			while(itr.hasNext()){
				itrtr = covtList.iterator();
				life = itr.next();
				initCovrList();
				initRcvdList();
				initAcblList();
				while(itrtr.hasNext()){
					covt = itrtr.next();
					if(life.equals(covt.getLife())){
						if(cvg != null && !cvg.equals(covt.getCoverage())) {
							cvgRiderList = new ArrayList<>();
						}
						checkCovtData(covt,rcvd,life);
						cvg = covt.getCoverage();
					}
					else if(itr.hasNext() == false){
						noLifeFound = true;
					}
					covrRiderMap.put(covt.getLife()+covt.getCoverage(), cvgRiderList);
				}
				for(int i = 0; i< inPutParam.get("covrId").size(); i++){
					if(premiumrec.lifeLife.toString().equals(life) && !covrRiderMap.containsKey(premiumrec.lifeLife.toString()+premiumrec.covrCoverage.toString())){
						noCvgFound = true;
						break;
					}
					else if(premiumrec.lifeLife.toString().equals(life) && covrRiderMap.containsKey(premiumrec.lifeLife.toString()+premiumrec.covrCoverage.toString())){
						covrRiderList = covrRiderMap.get(premiumrec.lifeLife.toString()+premiumrec.covrCoverage.toString());
						if(!covrRiderList.contains(premiumrec.covrRider.toString())) {
							noCvgFound = true;
							break;
						}
					}
				}
				if(noCvgFound){
					noCvgFound = false;
					setPremrecCovt();
					setLists(noLifeFound, noCvgFound);
				}
				else{
					setLists(noLifeFound,noCvgFound);		//add to main list
				}
			}
		}
		if(covtList.isEmpty() || noLifeFound){
			if(noCvgFound)
				noCvgFound = false;
			noLifeFound = false;
			if(covtList.isEmpty()){
				initCovrList();
				initRcvdList();
				initAcblList();
			}
			setPremrecCovt();
			setLists(noLifeFound, noCvgFound);		//add to main list
		}
		/*if(noCvgFound){
			noCvgFound = false;
			setPremrecCovt();
			setLists(noLifeFound, noCvgFound);
		}*/
		pmexRec.setCovrCoverage(inPutParam2.get("covrIdParamList"));
		pmexRec.setCrtable(inPutParam2.get("crtableParamList"));
		pmexRec.setPrevSumIns(inPutParam2.get("prevSumInsParamList"));//ILIFE-8502
		pmexRec.setInputPrevPrem(inPutParam2.get("inputPrevPremParamList"));//ILIFE-8537
		pmexRec.setCalcPrem(inPutParam2.get("calcPremParamList"));//ILIFE-8502
		pmexRec.setSumin(inPutParam2.get("sumInsParamList"));
		pmexRec.setMortcls(inPutParam2.get("mortClsParamList"));
		pmexRec.setCovrRiderId(inPutParam2.get("riderIdParamList"));
		pmexRec.setDuration(inPutParam2.get("durationParamList"));
		pmexRec.setLinkcov(inPutParam2.get("linkcovParamList"));
		pmexRec.setTpdtype(inPutParam2.get("tpdtypeParamList"));
		pmexRec.setLifeLife(inPutParam2.get("lifeLifeParamList"));
		pmexRec.setPoltyp(inPutParam2.get("poltypParamList"));
		pmexRec.setWaitperiod(inPutParam2.get("waitPeriodParamList"));
		pmexRec.setBentrm(inPutParam2.get("benTrmParamList"));
		pmexRec.setPrmbasis(inPutParam2.get("prmBasisParamList"));
		pmexRec.setDialdownoption(inPutParam2.get("dialDownOptParamList"));
		pmexRec.setLnkgSubRefNo(inPutParam2.get("lnkgSubRefNoParamList"));
		pmexRec.setLnkgind(inPutParam2.get("lnkgIndParamList"));
		pmexRec.setDob(inPutParam2.get("dobParamList"));
		pmexRec.setCovrcd(inPutParam2.get("covrcdParamList"));
	}
	
	protected void setCovrData(Covtpf covt, Iterator<Covrpf> itrtrCovrpf) {
		Covrpf covr;
loop:	while(itrtrCovrpf.hasNext()){
			covt= new Covtpf();
			covr = itrtrCovrpf.next();
			if(!("".equals(premiumrec.batcBatctrcde.toString().trim()))) {
				covrRiskStatus5110(covr);
				if (statusNotValid.isTrue()) {
					continue loop;
				}
				covrPremStatus5120(covr);
				if (statusNotValid.isTrue()) {
					/*continue loop;
					For now it is not required in future if we need it we can do premium status check*/
				}
			}
			if(key != null && key.length >= 0 ) {
				for(int i = 0; i < key.length; i++) {
					if(covr.getLife().equals(key[i].substring(0, 2)) && covr.getCoverage().equals(key[i].substring(2,4)) && covr.getRider().equals(key[i].substring(4))) {
						continue loop;
					}
				}
			}
			covt.setCrtable(covr.getCrtable());
			covt.setCoverage(covr.getCoverage());
			covt.setJlife(covr.getJlife());
			covt.setEffdate(covr.getCrrcd());
			if(reratedPremKeeps != null && !reratedPremKeeps.isEmpty()) {
				long unique_number = covr.getUniqueNumber();
				String life = covr.getLife();
				String covg = covr.getCoverage();
				String rider = covr.getRider();
				if(reratedPremKeeps.get(unique_number) != null){
					 for(int i=0; i<reratedPremKeeps.get(unique_number).size(); i++){
						 if(reratedPremKeeps.get(unique_number).get(i).getLife().equals(life) && 
								 reratedPremKeeps.get(unique_number).get(i).getCoverage().equals(covg) && 
								 reratedPremKeeps.get(unique_number).get(i).getRider().equals(rider)) {
							 covt.setSumins(reratedPremKeeps.get(unique_number).get(i).getSumins());
							 break;
						 }
					 }
				}
			}
			else
				covt.setSumins(covr.getSumins());
			covt.setMortcls(covr.getMortcls());
			covt.setLife(covr.getLife());
			covt.setTpdtype(covr.getTpdtype());
			covt.setRider(covr.getRider());
			covt.setLnkgno(covr.getLnkgno());
			covt.setChdrcoy(covr.getChdrcoy());
			covt.setChdrnum(covr.getChdrnum());
			covt.setPcesdte(covr.getPcesDte());
			covt.setLiencd(covr.getLiencd());
			if(null != covr.getLnkgsubrefno()){
				covt.setLnkgsubrefno(covr.getLnkgsubrefno());
			}
			else
			{
				covt.setLnkgsubrefno(SPACES);
			}
			if(null != covr.getLnkgind()) {
				if(!covr.getLnkgind().equals(" ") && covr.getLnkgind().equals(premiumrec.lnkgind.toString())){
					covt.setLnkgind(covr.getLnkgind());
				}
				else if(!covr.getLnkgind().equals(premiumrec.lnkgind.toString()) && (premiumrec.crtable.toString().equals(covr.getCrtable())))
				{
					covt.setLnkgind(premiumrec.lnkgind.toString());
				}
			}
			else
				covt.setLnkgind(SPACES);
			covtList.add(covt);
		}	
	}
	
	protected void readT5679() {
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(premiumrec.chdrChdrcoy.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(premiumrec.batcBatctrcde.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if (itempf == null) {
			t5679rec.t5679Rec.set(SPACES);
		} else {
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}
	
	protected void covrRiskStatus5110(Covrpf covr) {
		wsaaValidStatus.set("N");
		if (isEQ(covr.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getStatcode(), t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		} else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getStatcode(), t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
	}
	
	protected void covrPremStatus5120(Covrpf covr) {
		wsaaValidStatus.set("N");
		if (isEQ(covr.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getPstatcode(), t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		} else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getPstatcode(), t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
	}
	
	protected void checkCovtData(Covtpf covt,int rcvd,String life)
	{
			noLifeFound = false;
			noCvgFound=false;
			if(premiumrec.crtable.toString().equals(covt.getCrtable()) && premiumrec.lifeLife.toString().equals(life) && modifyFlag 
					&& premiumrec.covrCoverage.toString().equals(covt.getCoverage()) && premiumrec.covrRider.toString().equals(covt.getRider())){
				if(covrList == null || covrList.size()==0){
					wsaaRcdStore.set(covt.getEffdate());
				}else {
					wsaaRcdStore.set(setCovrccd(covt));
				}
				setPremrecCovt();
				readAcblpf(covt);
				modifyFlag = false;
			}
			else{
				inPutParam.get("covrId").add(covt.getCoverage());
				inPutParam.get("crtable").add(covt.getCrtable());
				inPutParam.get("sumIns").add(String.valueOf(covt.getSumins()));
				inPutParam.get("prevSumIns").add(premiumrec.prevSumIns.toString());//ILIFE-8502 
				inPutParam.get("calcPrem").add(premiumrec.calcPrem.toString());//ILIFE-8502
				inPutParam.get("inputPrevPrem").add(premiumrec.inputPrevPrem.toString());//ILIFE-8537 
				inPutParam.get("mortCls").add(covt.getMortcls());
				inPutParam.get("lifeLife").add(covt.getLife());
				if(null != covt.getLnkgsubrefno()){
					inPutParam.get("lnkgSubRefNo").add(covt.getLnkgsubrefno());
				}
				else
				{
					inPutParam.get("lnkgSubRefNo").add(SPACES);
				}
				if(null != covt.getLnkgind()) {
					if(!covt.getLnkgind().equals(" ") && covt.getLnkgind().equals(premiumrec.lnkgind.toString()))
						{
							covt.setLnkgind(covt.getLnkgind());
						}
					else if(!covt.getLnkgind().equals(premiumrec.lnkgind.toString()) && (premiumrec.crtable.toString().equals(covt.getCrtable())))
						{
							covt.setLnkgind(premiumrec.lnkgind.toString());
						}
					inPutParam.get("lnkgInd").add(covt.getLnkgind());
				}else {
					covt.setLnkgind(SPACES);
				}				
				pmexRec.setLiencd(covt.getLiencd());
				if(covt.getTpdtype() != null && !covt.getTpdtype().trim().isEmpty())
					inPutParam.get("tpdtype").add(((Character) covt.getTpdtype().charAt(4)).toString());
				else
					inPutParam.get("tpdtype").add(SPACE);							
				if(covt.getRider()!=null){
					inPutParam.get("riderId").add(covt.getRider());
				}
				else 
					inPutParam.get("riderId").add(SPACE);
				calcDuration(covt);
				//PINNACLE-2823 covrcd need to get the crrcd, not the effect date
				if(covrList == null || covrList.size()==0){
					inPutParam.get("covrcd").add(String.valueOf(covt.getEffdate()));
				}else {
					inPutParam.get("covrcd").add(String.valueOf(setCovrccd(covt)));
				}
				if(null!=covt.getJlife() && isEQ(covt.getJlife().trim(),SPACES))
					covt.setJlife("00");
				inPutParam.get("dob").add(lifeDetails.get(covt.getLife().concat(covt.getJlife())));
				readRcvdpfData(covt, rcvd);
				if(covt.getLnkgno() != null && !covt.getLnkgno().trim().isEmpty()){
					linkgService = new LinkageInfoService();
					inPutParam.get("linkcov").add(linkgService.getLinkageInfo(covt.getLnkgno()));
				}
				else
					inPutParam.get("linkcov").add(SPACE);
				readAcblpf(covt);
			}
			cvgRiderList.add(covt.getRider());
	}
	
	protected int setCovrccd(Covtpf covt) {
		int covrccd = 0;
		boolean covrExistFlag = false;
		for(Covrpf covrobj: covrList) {
			if(covt.getCoverage().equals(covrobj.getCoverage()) 
					&& covt.getLife().equals(covrobj.getLife()) 
					&& covt.getRider().equals(covrobj.getRider())) {
				covrccd = covrobj.getCrrcd();
				covrExistFlag = true;
				break;
			}
		}
		if(!covrExistFlag) {
			covrccd = covt.getEffdate();
		}
		
		return covrccd;
	}
	
	protected void setPremrecCovt(){
		inPutParam.get("covrId").add(premiumrec.covrCoverage.toString());
		inPutParam.get("crtable").add(premiumrec.crtable.toString());
		inPutParam.get("sumIns").add(String.valueOf(premiumrec.sumin.getbigdata()));
		inPutParam.get("prevSumIns").add(String.valueOf(premiumrec.prevSumIns.getbigdata()));//ILIFE-8502
		inPutParam.get("calcPrem").add(String.valueOf(premiumrec.calcPrem.getbigdata()));//ILIFE-8502
		inPutParam.get("inputPrevPrem").add(String.valueOf(premiumrec.inputPrevPrem.getbigdata()));//ILIFE-8537
		inPutParam.get("mortCls").add(premiumrec.mortcls.toString());	
		pmexRec.setLiencd(premiumrec.liencd.toString());
		inPutParam.get("tpdtype").add(premiumrec.tpdtype.toString());	
		inPutParam.get("riderId").add(premiumrec.covrRider.toString());		
		inPutParam.get("duration").add(String.valueOf(premiumrec.duration.toInt()));
		inPutParam.get("lifeLife").add(premiumrec.lifeLife.toString());
		inPutParam.get("poltyp").add(premiumrec.poltyp.toString());
		inPutParam.get("waitPeriod").add(premiumrec.waitperiod.toString());	
		inPutParam.get("benTrm").add(premiumrec.bentrm.toString());	//maxclaimperiod
		inPutParam.get("prmBasis").add(premiumrec.prmbasis.toString());	//prmbasis/prem stepped ind
		inPutParam.get("dialDownOpt").add(premiumrec.dialdownoption.toString());	//dialdown
		inPutParam.get("linkcov").add(premiumrec.linkcov.toString());	//verify code for link cov
		inPutParam.get("totSingPrem").add(vpxacblrec.totSingPrem.toString());
		inPutParam.get("totRegPrem").add(vpxacblrec.totRegPrem.toString());
		inPutParam.get("lnkgSubRefNo").add(premiumrec.lnkgSubRefNo.toString().trim());
		inPutParam.get("lnkgInd").add(premiumrec.lnkgind.toString());
		
		inPutParam.get("covrcd").add(String.valueOf(wsaaRcdStore));
		if(isNE(premiumrec.lifeJlife,SPACES) && isEQ(premiumrec.lifeJlife.trim(),SPACES))
			inPutParam.get("dob").add(lifeDetails.get(premiumrec.lifeLife.toString().concat(premiumrec.lifeJlife.toString())));
		else
			inPutParam.get("dob").add(lifeDetails.get(premiumrec.lifeLife.toString().concat("00")));
	}
	
	protected void initCovrList(){
		inPutParam.put("covrId", new ArrayList<String>());
		inPutParam.put("crtable", new ArrayList<String>());
		inPutParam.put("mortCls", new ArrayList<String>());
		inPutParam.put("prevSumIns", new ArrayList<String>());//ILIFE-8502 
		inPutParam.put("calcPrem", new ArrayList<String>());//ILIFE-8502 
		inPutParam.put("inputPrevPrem", new ArrayList<String>());//ILIFE-8537 
		inPutParam.put("sumIns", new ArrayList<String>());
		inPutParam.put("duration", new ArrayList<String>());
		inPutParam.put("tpdtype", new ArrayList<String>());	
		inPutParam.put("riderId", new ArrayList<String>());
		inPutParam.put("linkcov", new ArrayList<String>());
		inPutParam.put("lifeLife", new ArrayList<String>());
		inPutParam.put("lnkgSubRefNo", new ArrayList<String>());
		inPutParam.put("lnkgInd", new ArrayList<String>());
	}
	protected void initRcvdList(){
		inPutParam.put("waitPeriod", new ArrayList<String>());
		inPutParam.put("poltyp", new ArrayList<String>());
		inPutParam.put("benTrm", new ArrayList<String>());
		inPutParam.put("prmBasis", new ArrayList<String>());
		inPutParam.put("dialDownOpt", new ArrayList<String>());
	}
	protected void initAcblList(){
		inPutParam.put("totSingPrem", new ArrayList<String>());
		inPutParam.put("totRegPrem", new ArrayList<String>());
	}
	
	protected void setLists(boolean noLifeFound, boolean noCvgFound){
		if(!noLifeFound && !noCvgFound){
			inPutParam2.get("covrIdParamList").add(inPutParam.get("covrId"));
			inPutParam2.get("crtableParamList").add(inPutParam.get("crtable"));
			inPutParam2.get("sumInsParamList").add(inPutParam.get("sumIns"));
			inPutParam2.get("prevSumInsParamList").add(inPutParam.get("prevSumIns"));//ILIFE-8502
			inPutParam2.get("calcPremParamList").add(inPutParam.get("calcPrem"));//ILIFE-8502
			inPutParam2.get("inputPrevPremParamList").add(inPutParam.get("inputPrevPrem"));//ILIFE-8537
			inPutParam2.get("mortClsParamList").add(inPutParam.get("mortCls"));
			inPutParam2.get("riderIdParamList").add(inPutParam.get("riderId"));
			inPutParam2.get("durationParamList").add(inPutParam.get("duration"));
			inPutParam2.get("linkcovParamList").add(inPutParam.get("linkcov"));
			inPutParam2.get("poltypParamList").add(inPutParam.get("poltyp"));
			inPutParam2.get("waitPeriodParamList").add(inPutParam.get("waitPeriod"));
			inPutParam2.get("benTrmParamList").add(inPutParam.get("benTrm"));
			inPutParam2.get("prmBasisParamList").add(inPutParam.get("prmBasis"));
			inPutParam2.get("dialDownOptParamList").add(inPutParam.get("dialDownOpt"));
			inPutParam2.get("tpdtypeParamList").add(inPutParam.get("tpdtype"));	
			inPutParam2.get("lifeLifeParamList").add(inPutParam.get("lifeLife"));
			inPutParam2.get("totRegPremParamList").add(inPutParam.get("totRegPrem"));
			inPutParam2.get("totSingPremParamList").add(inPutParam.get("totSingPrem"));
			inPutParam2.get("lnkgSubRefNoParamList").add(inPutParam.get("lnkgSubRefNo"));
			inPutParam2.get("lnkgIndParamList").add(inPutParam.get("lnkgInd"));
			inPutParam2.get("dobParamList").add(inPutParam.get("dob"));
			inPutParam2.get("covrcdParamList").add(inPutParam.get("covrcd"));
		}
	}
	protected void calcDuration(Covtpf c){
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(c.getPcesdte());
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		datcon3rec.freqFactor.add(0.99999);
		inPutParam.get("duration").add(String.valueOf(datcon3rec.freqFactor.toInt()));
	}
	
	protected void readRcvdpfData(Covtpf c, int rcvd)
	{
		rcvdpf = new Rcvdpf();
		rcvdpf.setChdrcoy(c.getChdrcoy());
		rcvdpf.setChdrnum(c.getChdrnum());
		rcvdpf.setLife(c.getLife());
		rcvdpf.setCoverage(c.getCoverage());
		rcvdpf.setRider(c.getRider());
		rcvdpf.setCrtable(c.getCrtable());
		rcvdpf =rcvdpfDAO.readRcvdpf(rcvdpf);
		if(rcvdpf!=null && rcvd==0)
			checkRcvdpfValues(rcvdpf);
		else if(rcvdpf != null && rcvd == 1){
			return;
		}
	}
	protected void checkRcvdpfValues(Rcvdpf rcvdpf)
	{
		if(rcvdpf.getPoltyp() != null)	
			inPutParam.get("poltyp").add(rcvdpf.getPoltyp());//pol indemnity ind
		else
			inPutParam.get("poltyp").add(SPACES);
		if(rcvdpf.getWaitperiod() != null)
			inPutParam.get("waitPeriod").add(rcvdpf.getWaitperiod());//waitperiod
		else
			inPutParam.get("waitPeriod").add(SPACES);
		if(rcvdpf.getBentrm() != null)
			inPutParam.get("benTrm").add(rcvdpf.getBentrm());//maxclaimperiod
		else
			inPutParam.get("benTrm").add(SPACES);
		if(rcvdpf.getPrmbasis() != null){
			if(rcvdpf.getPrmbasis().equals("S"))
				inPutParam.get("prmBasis").add("Y");	//prmbasis/prem stepped ind	
			else
				inPutParam.get("prmBasis").add(SPACES);
		}	
		else
			inPutParam.get("prmBasis").add(SPACES);
		if(rcvdpf.getDialdownoption() != null)
			if(rcvdpf.getDialdownoption().startsWith("0")) {
				inPutParam.get("dialDownOpt").add(rcvdpf.getDialdownoption().substring(1));//dialdown
			}else {
				inPutParam.get("dialDownOpt").add(rcvdpf.getDialdownoption());
			}
		else
			inPutParam.get("dialDownOpt").add(SPACES);
		
	}
	
	protected void readAcblpf(Covtpf covt){
		if(!modifyFlag){
			readT5645();
			readT5688();
			firstTime = true;
			count = getSacscurbalCount(t5688rec);
			acblpf = new Acblpf();
			acblpf.setRldgcoy(covt.getChdrcoy());
			acblpf.setSacscode(t5645rec.sacscode[count].toString());
			acblpf.setRldgacct(covt.getChdrnum());
			acblpf.setSacscode(t5645rec.sacscode[count].toString());
			
			acblpf.setOrigcurr(premiumrec.currcode.toString());
			acblpf.setSacstyp(t5645rec.sacstype[count].toString());
			acblpf = acblpfDAO.getAcblpfRecord(premiumrec.chdrChdrcoy.toString(), acblpf.getSacscode(), acblpf.getRldgacct(), acblpf.getSacstyp(), acblpf.getOrigcurr());
			if(acblpf != null && (count == 1 || count == 2)){
				inPutParam.get("totRegPrem").add(acblpf.getSacscurbal().toString());
			}
			else{
				inPutParam.get("totRegPrem").add(String.valueOf(BigDecimal.ZERO));
			}
			count = getSacscurbalCount(t5688rec);
			/*acblpf = acblpfDAO.getAcblpfRecord(premiumrec.chdrChdrcoy.toString(), acblpf.getSacscode(), acblpf.getRldgacct(), acblpf.getSacstyp(), acblpf.getOrigcurr());*/
			if(acblpf != null && (count == 3 || count == 4)){
				inPutParam.get("totSingPrem").add(acblpf.getSacscurbal().toString());
			}
			else{
				inPutParam.get("totSingPrem").add(String.valueOf(BigDecimal.ZERO));
			}
		}
		else{
			inPutParam.get("totRegPrem").add(vpxacblrec.totRegPrem.toString());
			inPutParam.get("totSingPrem").add(vpxacblrec.totSingPrem.toString());
		}
	}
	
	protected void readT5688(){
		itempfList=itempfDAO.getItdmByFrmdate(premiumrec.chdrChdrcoy.toString(), t5688, premiumrec.cnttype.toString(), premiumrec.ratingdate.toInt());
		if (itempfList.isEmpty()) {
			t5688rec.t5688Rec.set(SPACES);
		}
		else {
			t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}
	
	protected int getSacscurbalCount(T5688rec t5688rec){
		if(t5688rec.comlvlacc.toString().equals("N")){
			if(firstTime){
				firstTime = false;
				count = 1;
			}
			else{
				count = 3;
			}
		}
		else{
			if(firstTime){
				firstTime = false;
				count = 2;
			}
			else{
				count = 4;
			}
		}
		return count;
	}
	
	protected void readT5645(){
		itempfList=itempfDAO.getItdmByFrmdate(premiumrec.chdrChdrcoy.toString(), t5645, "PRMPM19", premiumrec.ratingdate.toInt());
		if (itempfList.isEmpty()) {
			t5645rec.t5645Rec.set(SPACES);
		}
		else {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}
	
	protected void readLextpf(){
		List<Lextpf> lextList;
		lextpf = new Lextpf();
		lextpf.setChdrcoy(premiumrec.chdrChdrcoy.toString());
		lextpf.setChdrnum(premiumrec.chdrChdrnum.toString());
		lextList = lextpfDAO.getLextpfData(lextpf);
		initCommonLext();
		if(lextList != null && !lextList.isEmpty()){
			setLextpfData(lextList);
		}
		else{
			for(int i=0;i<inPutParam2.get("covrIdParamList").size(); i++){
				initLextParamList();
				initLextList();
				for(int j=0; j<inPutParam2.get("covrIdParamList").get(i).size(); j++){
					setLextParamLists();
				}
				inPutParam.get("noOfCoverages").add(String.valueOf(inPutParam2.get("covrIdParamList").get(i).size()));
				inPutParam2.get("countParamList").add(new ArrayList<>(Collections.nCopies(inPutParam2.get("covrIdParamList").get(i).size(), String.valueOf(0))));
				setCommonLext(i);
				setLextParentLists();
			}
			setLextAcblField();
		}
	}
	
	protected void initCommonLext(){
		inPutParam.put("noOfCoverages", new ArrayList<>());
		inPutParam2.put("countParamList",new ArrayList<List<String>>());
		inPutParam2.put("totalSuminParamList",new ArrayList<List<String>>());
		inPutParam2.put("aidsCoverIndParamList", new ArrayList<List<String>>());
		inPutParam2.put("ncdCodeParamList",new ArrayList<List<String>>());
		inPutParam2.put("claimAutoIncreaseParamList", new ArrayList<List<String>>());
		inPutParam2.put("syndicateCodeParamList", new ArrayList<List<String>>());
		inPutParam2.put("riskExpiryAgeParamList", new ArrayList<List<String>>());
	}
	
	protected void setCommonLext(int i){
		inPutParam2.get("aidsCoverIndParamList").add(new ArrayList<>(Collections.nCopies(inPutParam2.get("covrIdParamList").get(i).size(), SPACE)));
		inPutParam2.get("ncdCodeParamList").add(new ArrayList<>(Collections.nCopies(inPutParam2.get("covrIdParamList").get(i).size(), SPACE)));
		inPutParam2.get("claimAutoIncreaseParamList").add(new ArrayList<>(Collections.nCopies(inPutParam2.get("covrIdParamList").get(i).size(), SPACE)));
		inPutParam2.get("syndicateCodeParamList").add(new ArrayList<>(Collections.nCopies(inPutParam2.get("covrIdParamList").get(i).size(), SPACE)));
		inPutParam2.get("riskExpiryAgeParamList").add(new ArrayList<>(Collections.nCopies(inPutParam2.get("covrIdParamList").get(i).size(), SPACE)));
	}
	
	protected void setLextpfData(List<Lextpf> lextList)
	{
		int countCvg, countLext;
		for(int i=0; i<inPutParam.get("lifeId").size(); i++){ //01
			countCvg = 0;
			setCommonLext(i);
			inPutParam.put("count",  new ArrayList<>());
			initLextParamList();
			for(int j=0;j<inPutParam2.get("covrIdParamList").get(i).size(); j++){
				countLext = 0;
				initLextList();
				for(int k=0; k<lextList.size(); k++){
					if(inPutParam.get("lifeId").get(i).equals(lextList.get(k).getLife()) && inPutParam2.get("covrIdParamList").get(i).get(j).equals(lextList.get(k).getCoverage())){
						inPutParam.get("oppc").set(countLext,String.valueOf(lextList.get(k).getOppc()));
						inPutParam.get("zmortpct").set(countLext,String.valueOf(new BigDecimal(lextList.get(k).getZmortpct())));
						inPutParam.get("opcda").set(countLext,lextList.get(k).getOpcda());
						inPutParam.get("agerate").set(countLext,String.valueOf(new BigDecimal(lextList.get(k).getAgerate()))); 
						inPutParam.get("insprem").set(countLext,String.valueOf(new BigDecimal(lextList.get(k).getInsprm())));
						inPutParam.get("insprmRsn").set(countLext, SPACE);
						inPutParam.get("oppcRsn").set(countLext, SPACE);
						countLext++;
					}
				}
			inPutParam.get("count").add(String.valueOf(countLext));	
			setLextParamLists();
			countCvg++;
			}
		inPutParam.get("noOfCoverages").add(String.valueOf(countCvg));
		inPutParam2.get("countParamList").add(inPutParam.get("count"));
		setLextParentLists();
		}
	setLextAcblField();
	}
	protected void initLextParamList(){
		inPutParam2.put("oppcParamList", new ArrayList<List<String>>());
		inPutParam2.put("zmortpctParamList", new ArrayList<List<String>>());
		inPutParam2.put("opcdaParamList", new ArrayList<List<String>>());
		inPutParam2.put("agerateParamList", new ArrayList<List<String>>());
		inPutParam2.put("inspremParamList", new ArrayList<List<String>>());
		inPutParam2.put("insprmRsnParamList", new ArrayList<List<String>>());
		inPutParam2.put("oppcRsnParamList", new ArrayList<List<String>>());
	}
	
	protected void initLextList(){
		inPutParam.put("aidsCoverInd",new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("ncdCode",new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("claimAutoIncrease",new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("syndicateCode",new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("riskExpiryAge",new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("oppc", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("zmortpct",  new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("opcda",new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("agerate", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("insprem",  new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("insprmRsn",new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("oppcRsn",new ArrayList<>(Collections.nCopies(8, SPACE)));
	}
	
	protected void setLextAcblField(){
		//LEXT fields
		vpxRec.setNoOfCoverages(inPutParam.get("noOfCoverages"));
		vpxRec.setAidsCoverInd(inPutParam2.get("aidsCoverIndParamList"));
		vpxRec.setNcdCode(inPutParam2.get("ncdCodeParamList"));
		vpxRec.setClaimAutoIncrease(inPutParam2.get("claimAutoIncreaseParamList"));
		vpxRec.setSyndicateCode(inPutParam2.get("syndicateCodeParamList"));
		vpxRec.setTotalSumInsured(inPutParam2.get("sumInsParamList"));
		vpxRec.setRiskExpiryAge(inPutParam2.get("riskExpiryAgeParamList"));
		vpxRec.setOppc(inPutParam3.get("oppcParentList"));
		vpxRec.setZmortpct(inPutParam3.get("zmortpctParentList"));
		vpxRec.setOpcda(inPutParam3.get("opcdaParentList"));
		vpxRec.setAgerate(inPutParam3.get("agerateParentList"));
		vpxRec.setInsprm(inPutParam3.get("inspremParentList"));
		vpxRec.setCount(inPutParam2.get("countParamList"));
		vpxRec.setInsprmRsn(inPutParam3.get("insprmRsnParentList"));
		vpxRec.setOppcRsn(inPutParam3.get("oppcRsnParentList"));
		//ACBL fields
		acblRec.setTotRegPrem(inPutParam2.get("totRegPremParamList"));
		acblRec.setTotSingPrem(inPutParam2.get("totSingPremParamList"));
	}
	protected void setLextParentLists(){
		inPutParam3.get("oppcParentList").add(inPutParam2.get("oppcParamList"));
		inPutParam3.get("zmortpctParentList").add(inPutParam2.get("zmortpctParamList"));
		inPutParam3.get("opcdaParentList").add(inPutParam2.get("opcdaParamList"));
		inPutParam3.get("agerateParentList").add(inPutParam2.get("agerateParamList"));
		inPutParam3.get("inspremParentList").add(inPutParam2.get("inspremParamList"));
		inPutParam3.get("insprmRsnParentList").add(inPutParam2.get("insprmRsnParamList"));
		inPutParam3.get("oppcRsnParentList").add(inPutParam2.get("oppcRsnParamList"));
	}
	protected void setLextParamLists(){
		inPutParam2.get("oppcParamList").add(inPutParam.get("oppc"));
		inPutParam2.get("zmortpctParamList").add(inPutParam.get("zmortpct"));
		inPutParam2.get("opcdaParamList").add(inPutParam.get("opcda"));
		inPutParam2.get("agerateParamList").add(inPutParam.get("agerate"));
		inPutParam2.get("inspremParamList").add(inPutParam.get("insprem"));
		inPutParam2.get("insprmRsnParamList").add(inPutParam.get("insprmRsn"));
		inPutParam2.get("oppcRsnParamList").add(inPutParam.get("oppcRsn"));
	}

	protected void setCommonFields(){
		pmexRec.setCnttype(premiumrec.cnttype.toString());
		pmexRec.setBillfreq(premiumrec.billfreq.toString());
		pmexRec.setCurrcode(premiumrec.currcode.toString());
		pmexRec.setEffectdt(premiumrec.occdate.getbigdata());
		pmexRec.setPremMethod(premiumrec.premMethod.toString());
		pmexRec.setMop(premiumrec.mop.toString());
		pmexRec.setLanguage(premiumrec.language.toString());
		pmexRec.setRatingdate(premiumrec.ratingdate.toString());
		pmexRec.setReRateDate(premiumrec.reRateDate.getbigdata());		
		pmexRec.setCommTaxInd(premiumrec.commTaxInd.toString());//ILIFE-8502
		pmexRec.setChdrnum(premiumrec.chdrChdrnum.toString());
		pmexRec.setBilfrmdt(premiumrec.bilfrmdt.toString());
		pmexRec.setBiltodt(premiumrec.biltodt.toString());
		//ILIFE-8537 - Starts
		if(premiumrec.crtable.toString().trim().equals("LCP1")){
			pmexRec.setZszprmcd(vpxlextrecNew.zszprmcd.toString());
			pmexRec.setPrat(vpxlextrecNew.prat.toString());
			pmexRec.setCoverc(vpxlextrecNew.coverc.toString());
			pmexRec.setTrmofcontract(vpxlextrecNew.trmofcontract.toString());
			pmexRec.setHpropdte(vpxlextrecNew.hpropdte.getbigdata());
			pmexRec.setPremind(vpxlextrecNew.premind.toString());
		}
		//ILIFE-8537 - Ends
		if(isEQ(premiumrec.validind,SPACES))
			pmexRec.setValidind("1");  //Perform Validation
		else
			pmexRec.setValidind(premiumrec.validind.toString());
	}	
	protected void updateCovtpf()
	{
		int rcvd=1;
		int j;		
		if(covrList != null && covtList != null && !covrList.isEmpty() && !covtList.isEmpty()) {
			updateCovr = new ArrayList<>(covtList);
		}
		covtList = covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
		if(!covtList.isEmpty()){
			for(int k=0; k<inPutParam.get("lifeId").size(); k++){
				j=0;
				for(int i=0; i<covtList.size(); i++){
					if(inPutParam.get("lifeId").get(k).equals(covtList.get(i).getLife()) && pmexRec.getCrtable().get(k).get(j).equals(covtList.get(i).getCrtable())
							&& pmexRec.getCovrCoverage().get(k).get(j).equals(covtList.get(i).getCoverage()) && pmexRec.getCovrRiderId().get(k).get(j).equals(covtList.get(i).getRider())){
						noLifeFound = false;
						covtList.get(i).setInstprem(new BigDecimal(pmexRec.getCalcPrem().get(k).get(j)));
						covtList.get(i).setZbinstprem(new BigDecimal(pmexRec.getCalcBasPrem().get(k).get(j)));
						covtList.get(i).setZlinstprem(new BigDecimal(pmexRec.getCalcLoaPrem().get(k).get(j)));
						covtList.get(i).setCommPrem(new BigDecimal(pmexRec.getCommPremium().get(k).get(j)));
						if(!pmexRec.getZstpduty01().get(k).get(j).equalsIgnoreCase(varcom.mrnf.toString()))
							covtList.get(i).setZstpduty01(new BigDecimal(pmexRec.getZstpduty01().get(k).get(j)));
						else {
							premiumrec.statuz.set(pmexRec.getZstpduty01().get(k).get(j));
							return;
						}
						covtList.get(i).setZclstate(inPutParam.get("rstate01").get(k));
						readRcvdpfData(covtList.get(i), rcvd);
							if(!pmexRec.getOccpclass().isEmpty() && (rcvdpf != null && !pmexRec.getOccpclass().get(k).get(j).trim().isEmpty())){
									rcvdpf.setStatcode(pmexRec.getOccpclass().get(k).get(j).trim().equals("*") ? SPACE : pmexRec.getOccpclass().get(k).get(j).trim());
									rcvdMap.put(premiumrec.chdrChdrnum.toString().trim()+covtList.get(i).getLife().trim()+covtList.get(i).getCoverage().trim()+covtList.get(i).getRider().trim(), rcvdpf);
								}
							if(pmexRec.getCrtable().get(k).get(j).equals(premiumrec.crtable.toString()) && rcvdpf != null && inPutParam.get("lifeId").get(k).equals(premiumrec.lifeLife.toString().trim())
									&& covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString()) && covtList.get(i).getRider().equals(premiumrec.covrRider.toString())){
								noCvgFound = false;
								setupCovtPremiumrec(covtList.get(i));
								setupRcvdPremiumrec(rcvdpf);
							}
							else if(pmexRec.getCrtable().get(k).get(j).equals(premiumrec.crtable.toString()) && rcvdpf == null && inPutParam.get("lifeId").get(k).equals(premiumrec.lifeLife.toString().trim())
									&& covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString()) && covtList.get(i).getRider().equals(premiumrec.covrRider.toString())){
								noCvgFound = false;
								setupCovtPremiumrec(covtList.get(i));
							}
							else if(covtList.get(i).getLife().equals(premiumrec.lifeLife.toString()) && !covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString())){
								noCvgFound = true;
							}
							else if(covtList.get(i).getLife().equals(premiumrec.lifeLife.toString()) && covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString()) && !covtList.get(i).getRider().equals(premiumrec.covrRider.toString())) {
								noCvgFound = true;
							}
						setupPremiumrecForRiskPrmium(i, k, j);
						covtList.get(i).setInstprem(covtList.get(i).getInstprem().add(covtList.get(i).getZstpduty01()));
						covtMap.put(premiumrec.chdrChdrnum.toString().trim()+covtList.get(i).getLife().trim()+covtList.get(i).getCoverage().trim()+covtList.get(i).getRider().trim(), covtList.get(i));
						j++;
					}
					else{
						noLifeFound = true;
					}
				}
			}
			covtpfDAO.bulkUpdateCovtData(covtMap);
			if(rcvdMap.size() > 0){
				rcvdpfDAO.updateOccpclass(rcvdMap);
				rcvdMap.clear();
			}
			covtMap.clear();
		}
		if(covtList.isEmpty() || noCvgFound || noLifeFound){
			setPrem();
		}	
	}
	
	protected void setPrem() {
		if(("Y").equals(premiumrec.proratPremCalcFlag.toString())) {
			premiumrec.calcPrem.set(ZERO);
			premiumrec.calcBasPrem.set(ZERO);
			premiumrec.calcLoaPrem.set(ZERO);
			premiumrec.commissionPrem.set(ZERO);
			premiumrec.zstpduty01.set(ZERO);
		}
		for(int k=0; k<inPutParam.get("lifeId").size(); k++){
			for(int j=0; j<pmexRec.getCrtable().get(k).size(); j++){
				if(("Y").equals(premiumrec.proratPremCalcFlag.toString())) {
					premiumrec.calcPrem.add(Double.parseDouble(pmexRec.getCalcPrem().get(k).get(j)));
					premiumrec.calcBasPrem.add(Double.parseDouble(pmexRec.getCalcBasPrem().get(k).get(j)));
					premiumrec.calcLoaPrem.add(Double.parseDouble(pmexRec.getCalcLoaPrem().get(k).get(j)));
					premiumrec.commissionPrem.add(Double.parseDouble(pmexRec.getCommPremium().get(k).get(j)));
					if(!pmexRec.getZstpduty01().get(k).get(j).equalsIgnoreCase(Varcom.mrnf.toString()))
						premiumrec.zstpduty01.add(Double.parseDouble(pmexRec.getZstpduty01().get(k).get(j)));
					else {
						premiumrec.statuz.set(pmexRec.getZstpduty01().get(k).get(j));
						return;
					}
					premiumrec.rstate01.set(pmexRec.getRstate01().get(k));
					if(!pmexRec.getOccpclass().get(k).get(j).trim().isEmpty()){
						premiumrec.occpclass.set(pmexRec.getOccpclass().get(k).get(j).trim().equals("*") ? SPACE : pmexRec.getOccpclass().get(k).get(j).trim());
					}
				} else 
				if(inPutParam.get("lifeId").get(k).equals(premiumrec.lifeLife.toString()) && pmexRec.getCovrCoverage().get(k).get(j).equals(premiumrec.covrCoverage.toString()) 
						&& pmexRec.getCovrRiderId().get(k).get(j).equals(premiumrec.covrRider.toString())){
					premiumrec.calcPrem.set(new BigDecimal(pmexRec.getCalcPrem().get(k).get(j)));
					premiumrec.calcBasPrem.set(new BigDecimal(pmexRec.getCalcBasPrem().get(k).get(j)));
					premiumrec.calcLoaPrem.set(new BigDecimal(pmexRec.getCalcLoaPrem().get(k).get(j)));
					premiumrec.commissionPrem.set(new BigDecimal(pmexRec.getCommPremium().get(k).get(j))); //IBPLIFE-5237
					if(!pmexRec.getZstpduty01().get(k).get(j).equalsIgnoreCase(varcom.mrnf.toString()))
						premiumrec.zstpduty01.set(new BigDecimal(pmexRec.getZstpduty01().get(k).get(j)));
					else {
						premiumrec.statuz.set(pmexRec.getZstpduty01().get(k).get(j));
						return;
					}
					premiumrec.rstate01.set(pmexRec.getRstate01().get(k));
					if(!pmexRec.getOccpclass().get(k).get(j).trim().isEmpty()){
						premiumrec.occpclass.set(pmexRec.getOccpclass().get(k).get(j).trim().equals("*") ? SPACE : pmexRec.getOccpclass().get(k).get(j).trim());
					}
				}
			}
		}
		
	}
	
	protected void setupCovtPremiumrec(Covtpf covt){
		premiumrec.calcPrem.set(covt.getInstprem());
		premiumrec.calcBasPrem.set(covt.getZbinstprem());
		premiumrec.calcLoaPrem.set(covt.getZlinstprem());
		premiumrec.zstpduty01.set(covt.getZstpduty01());
		premiumrec.rstate01.set(covt.getZclstate());
		premiumrec.commissionPrem.set(covt.getCommPrem()); 
	}
	
	protected void setupRcvdPremiumrec(Rcvdpf rcvd){
		premiumrec.occpclass.set(rcvd.getStatcode());
	}
	
	protected void callSubroutine(){
		readT5675();
		pmexRec.initialise();
		pmexRec.setCalcPrem(inPutParam2.get("calcPremParamList")); //ILIFE-8502
		callProgram("PRMPMEX",pmexRec, vpxRec, acblRec);
		if(pmexRec.statuz.toString().equals(varcom.oK.toString())){
			if("N".equals(premiumrec.updateRequired.toString())){
				setPrem();
				return;
			}
			else{
				if(tablereadFlag)
				{
					updateCovtpf();
					updateCovrpf();
				}
				else{
					if(covrList.isEmpty())
						updateCovtpf();
					else
						updateCovrpf();
				}
			}
		}
		else
			premiumrec.statuz.set(pmexRec.statuz);
	}
	
	protected void readT5675(){
		itempfList=itempfDAO.getAllItemitem("IT",premiumrec.chdrChdrcoy.toString(), t5675, premiumrec.premMethod.toString());
		if (itempfList.isEmpty()) {
			t5675rec.t5675Rec.set(SPACES);
		}
		else {
			t5675rec.t5675Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}
	
	//ILIFE-7845
	protected void setupPremiumrecForRiskPrmium(int i, int k, int j) {
		premiumrecNew.cnttype.set(premiumrec.cnttype);
		premiumrecNew.crtable.set(pmexRec.getCrtable().get(k).get(j));
		premiumrecNew.calcTotPrem.set(new BigDecimal(pmexRec.getZstpduty01().get(k).get(j)).add(new BigDecimal(pmexRec.getCalcPrem().get(k).get(j))));
		callProgram("RISKPREMIUM", premiumrecNew.premiumRec);
		covtList.get(i).setRiskprem(premiumrecNew.riskPrem.getbigdata());
		
	}
	//ILIFE-7845
	
	protected void updateCovrpf()
	{
		int rcvd=1;
		int j;
		if(updateCovr != null && !updateCovr.isEmpty())
			covtList = updateCovr;
		//checkModify();
		if(!covtList.isEmpty()){
			for(int k=0; k<inPutParam.get("lifeId").size(); k++){
				j=0;
				for(int i=0; i<covtList.size(); i++){
					if(inPutParam.get("lifeId").get(k).equals(covtList.get(i).getLife()) && pmexRec.getCrtable().get(k).get(j).equals(covtList.get(i).getCrtable())
							&& pmexRec.getCovrCoverage().get(k).get(j).equals(covtList.get(i).getCoverage()) && pmexRec.getCovrRiderId().get(k).get(j).equals(covtList.get(i).getRider())){
						noLifeFound = false;
						covtList.get(i).setInstprem(new BigDecimal(pmexRec.getCalcPrem().get(k).get(j)));
						covtList.get(i).setZbinstprem(new BigDecimal(pmexRec.getCalcBasPrem().get(k).get(j)));
						covtList.get(i).setZlinstprem(new BigDecimal(pmexRec.getCalcLoaPrem().get(k).get(j)));
						covtList.get(i).setCommPrem(new BigDecimal(pmexRec.getCommPremium().get(k).get(j)));
						if(!pmexRec.getZstpduty01().get(k).get(j).equalsIgnoreCase(varcom.mrnf.toString()))
							covtList.get(i).setZstpduty01(new BigDecimal(pmexRec.getZstpduty01().get(k).get(j)));
						else {
							premiumrec.statuz.set(pmexRec.getZstpduty01().get(k).get(j));
							return;
						}
						covtList.get(i).setZclstate(inPutParam.get("rstate01").get(k));
						readRcvdpfData(covtList.get(i), rcvd);
							if(!pmexRec.getOccpclass().isEmpty() && (rcvdpf != null && !pmexRec.getOccpclass().get(k).get(j).trim().isEmpty())){
									rcvdpf.setStatcode(pmexRec.getOccpclass().get(k).get(j).trim().equals("*") ? SPACE : pmexRec.getOccpclass().get(k).get(j).trim());
									rcvdMap.put(premiumrec.chdrChdrnum.toString().trim()+covtList.get(i).getLife().trim()+covtList.get(i).getCoverage().trim()+covtList.get(i).getRider().trim(), rcvdpf);
								}
							if(pmexRec.getCrtable().get(k).get(j).equals(premiumrec.crtable.toString()) && rcvdpf != null && inPutParam.get("lifeId").get(k).equals(premiumrec.lifeLife.toString().trim()) 
									&& covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString()) && covtList.get(i).getRider().equals(premiumrec.covrRider.toString())){
								noCvgFound = false;
								setupCovtPremiumrec(covtList.get(i));
								setupRcvdPremiumrec(rcvdpf);
							}
							else if(pmexRec.getCrtable().get(k).get(j).equals(premiumrec.crtable.toString()) && rcvdpf == null  && inPutParam.get("lifeId").get(k).equals(premiumrec.lifeLife.toString().trim())
									&& covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString()) && covtList.get(i).getRider().equals(premiumrec.covrRider.toString())){
								noCvgFound = false;
								setupCovtPremiumrec(covtList.get(i));
							}
							else if(covtList.get(i).getLife().equals(premiumrec.lifeLife.toString()) && !covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString())){
								noCvgFound = true;
							}
							else if(covtList.get(i).getLife().equals(premiumrec.lifeLife.toString()) && covtList.get(i).getCoverage().equals(premiumrec.covrCoverage.toString()) && !covtList.get(i).getRider().equals(premiumrec.covrRider.toString())) {
								noCvgFound = true;
							}
						setupPremiumrecForRiskPrmium(i, k, j);
						
						checkComponentModified(i);  // ILIFE-8163
						j++;
					}
					else{
						noLifeFound = true;
					}
				}
			}
			if (!("N".equalsIgnoreCase(premiumrec.flag.toString()))){		
			covrpfDAO.bulkUpdateCovrData(covtMap);
			}
			if(rcvdMap.size() > 0){
				rcvdpfDAO.updateOccpclass(rcvdMap);
				rcvdMap.clear();
			}	
			covtMap.clear();	  
		}
		if(covtList.isEmpty() || noCvgFound || noLifeFound){
			setPrem();
		}	
	}
	
	private void checkComponentModified(int i) {
		List<String> validateCovtRecord = key != null ? Arrays.asList(key) : new ArrayList<>();
		String key_covtpf = covtList.get(i).getLife() + covtList.get(i).getCoverage() + covtList.get(i).getRider();
		if (!("Y".equalsIgnoreCase(premiumrec.flag.toString()))) {
			covtList.get(i).setInstprem(covtList.get(i).getInstprem().add(covtList.get(i).getZstpduty01()));
			covtMap.put(
					premiumrec.chdrChdrnum.toString().trim() + covtList.get(i).getLife().trim()
							+ covtList.get(i).getCoverage().trim() + covtList.get(i).getRider().trim(),
					covtList.get(i));

			updateAgcm(covtList.get(i));
		} else {
			if (!(premiumrec.lifeLife.equals(covtList.get(i).getLife())
					&& (premiumrec.covrCoverage.equals(covtList.get(i).getCoverage())
							&& (premiumrec.covrRider.equals(covtList.get(i).getRider()))))) {
				covtList.get(i).setInstprem(covtList.get(i).getInstprem().add(covtList.get(i).getZstpduty01()));
				if (!validateCovtRecord.contains(key_covtpf)) {
					covtMap.put(
							premiumrec.chdrChdrnum.toString().trim() + covtList.get(i).getLife().trim()
									+ covtList.get(i).getCoverage().trim() + covtList.get(i).getRider().trim(),
							covtList.get(i));
					updateAgcm(covtList.get(i));

				}
			} else
				covtList.get(i).setInstprem(covtList.get(i).getInstprem().add(covtList.get(i).getZstpduty01()));
		}
	}
	
	protected void updateAgcm(Covtpf covtpf)  // ILIFE-8163
	{
		Agcmpf agcmpf=new Agcmpf();
		agcmpf.setAnnprem(covtpf.getInstprem().multiply(BigDecimal.valueOf(Long.parseLong(premiumrec.billfreq.toString()))));
		agcmpf.setChdrcoy(covtpf.getChdrcoy());
		agcmpf.setChdrnum(covtpf.getChdrnum());
		agcmpf.setLife(covtpf.getLife());
		agcmpf.setCoverage(covtpf.getCoverage());
		agcmpf.setRider(covtpf.getRider());
	   agcmpfDAO.updateAgcmRecord(agcmpf);
	}
	
	
	protected void fatalError9000()
	{
		error9010();
		exit9020();
	}
	protected void error9010()
		{
			if (isEQ(syserrrec.statuz, "BOMB")) {
				return ;
			}
			syserrrec.syserrStatuz.set(syserrrec.statuz);
			if (isNE(syserrrec.syserrType, "2")) {
				syserrrec.syserrType.set("1");
			}
			callProgram(Syserr.class, syserrrec.syserrRec);
		}
	
	protected void exit9020()
		{
			premiumrec.statuz.set("BOMB");
			/*EXIT*/
			exitProgram();
		}
	
} 
