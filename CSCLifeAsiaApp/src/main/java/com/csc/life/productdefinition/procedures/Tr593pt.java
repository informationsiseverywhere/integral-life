/*
 * File: Tr593pt.java
 * Date: 30 August 2009 2:43:31
 * Author: Quipoz Limited
 * 
 * Class transformed from TR593PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr593rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR593.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr593pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine001, 32, FILLER).init("Field definition                       S2636");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(27);
	private FixedLengthStringData filler7 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine003, 0, FILLER).init("   Relevant Contract Types:");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(60);
	private FixedLengthStringData filler8 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 3);
	private FixedLengthStringData filler9 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 9);
	private FixedLengthStringData filler10 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 15);
	private FixedLengthStringData filler11 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 21);
	private FixedLengthStringData filler12 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 27);
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 33);
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 39);
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 45);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 51);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 57);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(60);
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 3);
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 9);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 15);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 21);
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 27);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 33);
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 39);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 45);
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 51);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 57);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(60);
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 3);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 9);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 15);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 21);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 27);
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler34 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 39);
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 45);
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 51);
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 57);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(60);
	private FixedLengthStringData filler38 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 3);
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 6, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 9);
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 15);
	private FixedLengthStringData filler41 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 21);
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 27);
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 39);
	private FixedLengthStringData filler45 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 45);
	private FixedLengthStringData filler46 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 57);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr593rec tr593rec = new Tr593rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr593pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr593rec.tr593Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tr593rec.chdrtype01);
		fieldNo006.set(tr593rec.chdrtype02);
		fieldNo007.set(tr593rec.chdrtype03);
		fieldNo008.set(tr593rec.chdrtype04);
		fieldNo009.set(tr593rec.chdrtype05);
		fieldNo010.set(tr593rec.chdrtype06);
		fieldNo011.set(tr593rec.chdrtype07);
		fieldNo012.set(tr593rec.chdrtype08);
		fieldNo013.set(tr593rec.chdrtype09);
		fieldNo014.set(tr593rec.chdrtype10);
		fieldNo015.set(tr593rec.chdrtype11);
		fieldNo016.set(tr593rec.chdrtype12);
		fieldNo017.set(tr593rec.chdrtype13);
		fieldNo018.set(tr593rec.chdrtype14);
		fieldNo019.set(tr593rec.chdrtype15);
		fieldNo020.set(tr593rec.chdrtype16);
		fieldNo021.set(tr593rec.chdrtype17);
		fieldNo022.set(tr593rec.chdrtype18);
		fieldNo023.set(tr593rec.chdrtype19);
		fieldNo024.set(tr593rec.chdrtype20);
		fieldNo025.set(tr593rec.chdrtype21);
		fieldNo026.set(tr593rec.chdrtype22);
		fieldNo027.set(tr593rec.chdrtype23);
		fieldNo028.set(tr593rec.chdrtype24);
		fieldNo029.set(tr593rec.chdrtype25);
		fieldNo030.set(tr593rec.chdrtype26);
		fieldNo031.set(tr593rec.chdrtype27);
		fieldNo032.set(tr593rec.chdrtype28);
		fieldNo033.set(tr593rec.chdrtype29);
		fieldNo034.set(tr593rec.chdrtype30);
		fieldNo035.set(tr593rec.chdrtype31);
		fieldNo036.set(tr593rec.chdrtype32);
		fieldNo037.set(tr593rec.chdrtype33);
		fieldNo038.set(tr593rec.chdrtype34);
		fieldNo039.set(tr593rec.chdrtype35);
		fieldNo040.set(tr593rec.chdrtype36);
		fieldNo041.set(tr593rec.chdrtype37);
		fieldNo042.set(tr593rec.chdrtype38);
		fieldNo043.set(tr593rec.chdrtype39);
		fieldNo044.set(tr593rec.chdrtype40);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
