package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:05
 * Description:
 * Copybook name: LIFACMVREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifacmvrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData lifacmvRec = new FixedLengthStringData(247);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(lifacmvRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(lifacmvRec, 5);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(lifacmvRec, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batckey, 0, FILLER);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public FixedLengthStringData rdocnum = new FixedLengthStringData(9).isAPartOf(lifacmvRec, 28);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(lifacmvRec, 37);
  	public PackedDecimalData jrnseq = new PackedDecimalData(3, 0).isAPartOf(lifacmvRec, 40);
  	public FixedLengthStringData rldgcoy = new FixedLengthStringData(1).isAPartOf(lifacmvRec, 42);
  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(lifacmvRec, 43);
  	public FixedLengthStringData rldgacct = new FixedLengthStringData(16).isAPartOf(lifacmvRec, 45);
  	public FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(lifacmvRec, 61);
  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(lifacmvRec, 64);
  	public PackedDecimalData origamt = new PackedDecimalData(17, 2).isAPartOf(lifacmvRec, 66);
  	public FixedLengthStringData tranref = new FixedLengthStringData(30).isAPartOf(lifacmvRec, 75);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(lifacmvRec, 105);
  	public PackedDecimalData crate = new PackedDecimalData(18, 9).isAPartOf(lifacmvRec, 135);
  	public PackedDecimalData acctamt = new PackedDecimalData(17, 2).isAPartOf(lifacmvRec, 145);
  	public FixedLengthStringData genlcoy = new FixedLengthStringData(1).isAPartOf(lifacmvRec, 154);
  	public FixedLengthStringData genlcur = new FixedLengthStringData(3).isAPartOf(lifacmvRec, 155);
  	public FixedLengthStringData glcode = new FixedLengthStringData(14).isAPartOf(lifacmvRec, 158);
  	public FixedLengthStringData glsign = new FixedLengthStringData(1).isAPartOf(lifacmvRec, 172);
  	public PackedDecimalData contot = new PackedDecimalData(2, 0).isAPartOf(lifacmvRec, 173);
  	public FixedLengthStringData postyear = new FixedLengthStringData(4).isAPartOf(lifacmvRec, 175);
  	public FixedLengthStringData postmonth = new FixedLengthStringData(2).isAPartOf(lifacmvRec, 179);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(lifacmvRec, 181);
  	public PackedDecimalData rcamt = new PackedDecimalData(17, 2).isAPartOf(lifacmvRec, 186);
  	public PackedDecimalData frcdate = new PackedDecimalData(8, 0).isAPartOf(lifacmvRec, 195);
  	public PackedDecimalData transactionDate = new PackedDecimalData(6, 0).isAPartOf(lifacmvRec, 200);
  	public PackedDecimalData transactionTime = new PackedDecimalData(6, 0).isAPartOf(lifacmvRec, 204);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(lifacmvRec, 208);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(lifacmvRec, 212);
  	public FixedLengthStringData suprflag = new FixedLengthStringData(1).isAPartOf(lifacmvRec, 216);
  	public FixedLengthStringData substituteCodes = new FixedLengthStringData(24).isAPartOf(lifacmvRec, 217);
  	public FixedLengthStringData[] substituteCode = FLSArrayPartOfStructure(6, 4, substituteCodes, 0);
  	//cluster support by vhukumagrawa
  	public PackedDecimalData threadNumber = new PackedDecimalData(3, 0).isAPartOf(lifacmvRec, 241);
  	// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
  	public FixedLengthStringData prefix = new FixedLengthStringData(2).isAPartOf(lifacmvRec, 243);
  	public FixedLengthStringData ind = new FixedLengthStringData(1).isAPartOf(lifacmvRec, 245);
  	public FixedLengthStringData agnt = new FixedLengthStringData(1).isAPartOf(lifacmvRec, 246);


	public void initialize() {
		COBOLFunctions.initialize(lifacmvRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifacmvRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}