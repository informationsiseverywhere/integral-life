/*
 * File: T5646pt.java
 * Date: 30 August 2009 2:24:41
 * Author: Quipoz Limited
 * 
 * Class transformed from T5646PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5646rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5646.
*
*
*****************************************************************
* </pre>
*/
public class T5646pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5646rec t5646rec = new T5646rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5646pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5646rec.t5646Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5646rec.agelimit);
		generalCopyLinesInner.fieldNo008.set(t5646rec.ageIssageFrm01);
		generalCopyLinesInner.fieldNo009.set(t5646rec.ageIssageTo01);
		generalCopyLinesInner.fieldNo010.set(t5646rec.factorsa01);
		generalCopyLinesInner.fieldNo011.set(t5646rec.ageIssageFrm02);
		generalCopyLinesInner.fieldNo012.set(t5646rec.ageIssageTo02);
		generalCopyLinesInner.fieldNo013.set(t5646rec.factorsa02);
		generalCopyLinesInner.fieldNo014.set(t5646rec.ageIssageFrm03);
		generalCopyLinesInner.fieldNo015.set(t5646rec.ageIssageTo03);
		generalCopyLinesInner.fieldNo016.set(t5646rec.factorsa03);
		generalCopyLinesInner.fieldNo017.set(t5646rec.ageIssageFrm04);
		generalCopyLinesInner.fieldNo018.set(t5646rec.ageIssageTo04);
		generalCopyLinesInner.fieldNo019.set(t5646rec.factorsa04);
		generalCopyLinesInner.fieldNo020.set(t5646rec.ageIssageFrm05);
		generalCopyLinesInner.fieldNo021.set(t5646rec.ageIssageTo05);
		generalCopyLinesInner.fieldNo022.set(t5646rec.factorsa05);
		generalCopyLinesInner.fieldNo023.set(t5646rec.ageIssageFrm06);
		generalCopyLinesInner.fieldNo024.set(t5646rec.ageIssageTo06);
		generalCopyLinesInner.fieldNo025.set(t5646rec.factorsa06);
		generalCopyLinesInner.fieldNo026.set(t5646rec.ageIssageFrm07);
		generalCopyLinesInner.fieldNo027.set(t5646rec.ageIssageTo07);
		generalCopyLinesInner.fieldNo028.set(t5646rec.factorsa07);
		generalCopyLinesInner.fieldNo029.set(t5646rec.ageIssageFrm08);
		generalCopyLinesInner.fieldNo030.set(t5646rec.ageIssageTo08);
		generalCopyLinesInner.fieldNo031.set(t5646rec.factorsa08);
		generalCopyLinesInner.fieldNo032.set(t5646rec.ageIssageFrm09);
		generalCopyLinesInner.fieldNo033.set(t5646rec.ageIssageTo09);
		generalCopyLinesInner.fieldNo034.set(t5646rec.factorsa09);
		generalCopyLinesInner.fieldNo035.set(t5646rec.ageIssageFrm10);
		generalCopyLinesInner.fieldNo036.set(t5646rec.ageIssageTo10);
		generalCopyLinesInner.fieldNo037.set(t5646rec.factorsa10);
		generalCopyLinesInner.fieldNo038.set(t5646rec.ageIssageFrm11);
		generalCopyLinesInner.fieldNo039.set(t5646rec.ageIssageTo11);
		generalCopyLinesInner.fieldNo040.set(t5646rec.factorsa11);
		generalCopyLinesInner.fieldNo041.set(t5646rec.ageIssageFrm12);
		generalCopyLinesInner.fieldNo042.set(t5646rec.ageIssageTo12);
		generalCopyLinesInner.fieldNo043.set(t5646rec.factorsa12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Sum Assured Factors                       S5646");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(77);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);
	private FixedLengthStringData filler9 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 48, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 62, FILLER).init("Age Limit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine003, 74).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(55);
	private FixedLengthStringData filler11 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine004, 15, FILLER).init("From Age          To Age          Factor");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(56);
	private FixedLengthStringData filler13 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 19).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 35).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine005, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(56);
	private FixedLengthStringData filler16 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 19).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 35).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine006, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(56);
	private FixedLengthStringData filler19 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 19).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 35).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine007, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(56);
	private FixedLengthStringData filler22 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 19).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 35).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine008, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(56);
	private FixedLengthStringData filler25 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 19).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 35).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine009, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(56);
	private FixedLengthStringData filler28 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 19).setPattern("ZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 35).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine010, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(56);
	private FixedLengthStringData filler31 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 19).setPattern("ZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 35).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine011, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(56);
	private FixedLengthStringData filler34 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 19).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 35).setPattern("ZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine012, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(56);
	private FixedLengthStringData filler37 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 19).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 35).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine013, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(56);
	private FixedLengthStringData filler40 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 19).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 35).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine014, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(56);
	private FixedLengthStringData filler43 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 19).setPattern("ZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine015, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 35).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine015, 48).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(56);
	private FixedLengthStringData filler46 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 19).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine016, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 35).setPattern("ZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine016, 48).setPattern("ZZZ.ZZZZ");
}
}
