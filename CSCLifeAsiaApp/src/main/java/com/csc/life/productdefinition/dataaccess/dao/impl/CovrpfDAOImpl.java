package com.csc.life.productdefinition.dataaccess.dao.impl;
 
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;//ILIFE-8187
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;//ILIFE-8187
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovrpfDAOImpl extends BaseDAOImpl<Covrpf> implements CovrpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CovrpfDAOImpl.class);

    public List<Covrpf> searchCovrRecord(String coy, List<String> chdrnumList) {
        StringBuilder sqlCovrSelect1 = new StringBuilder();
        
    	sqlCovrSelect1.append("SELECT CHDRCOY,CHDRNUM,CBCVIN,COVERAGE,CRRCD,CRTABLE,CURRFROM,INSTPREM,");
        sqlCovrSelect1.append("JLIFE,LIFE,PLNSFX,PCESTRM,PRMCUR,PSTATCODE,RRTDAT,RIDER,RCESTRM,SINGP,");
        sqlCovrSelect1.append("STATCODE,SUMINS,UNIQUE_NUMBER,VALIDFLAG,ZLINSTPREM ");
        sqlCovrSelect1.append(", TRANNO, CURRTO, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS, ");
        sqlCovrSelect1.append("REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T, ");
        sqlCovrSelect1.append("LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, "); 
        sqlCovrSelect1.append("CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR,  CBRVPR, ");
        sqlCovrSelect1.append("CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZCLSTATE ");
        sqlCovrSelect1.append("FROM COVRPF WHERE CHDRCOY=? AND VALIDFLAG = '1' AND "); //ILIFE-9041
        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, ");

        sqlCovrSelect1.append("RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
        
        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
        ResultSet sqlcovrpf1rs = null;
        List<Covrpf> covrpfSearchResult = new LinkedList<Covrpf>();
        try {
            psCovrSelect.setString(1, coy);
            sqlcovrpf1rs = executeQuery(psCovrSelect);

            while (sqlcovrpf1rs.next()) {
                Covrpf covrpf = new Covrpf();
                covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(3));
                covrpf.setCoverage(sqlcovrpf1rs.getString(4));
                covrpf.setCrrcd(sqlcovrpf1rs.getInt(5));
                covrpf.setCrtable(sqlcovrpf1rs.getString(6));
                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(7));
                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(8));
                covrpf.setJlife(sqlcovrpf1rs.getString(9));
                covrpf.setLife(sqlcovrpf1rs.getString(10));
                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(11));
                covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(12));
                covrpf.setPremCurrency(sqlcovrpf1rs.getString(13));
                covrpf.setPstatcode(sqlcovrpf1rs.getString(14));
                covrpf.setRerateDate(sqlcovrpf1rs.getInt(15));
                covrpf.setRider(sqlcovrpf1rs.getString(16));
                covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(17));
                covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(18));
                covrpf.setStatcode(sqlcovrpf1rs.getString(19));
                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(20));
                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(21));
                covrpf.setValidflag(sqlcovrpf1rs.getString(22));
                covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(23));
				covrpf.setTranno(sqlcovrpf1rs.getInt(24));
                covrpf.setCurrto(sqlcovrpf1rs.getInt(25));
                covrpf.setStatFund(sqlcovrpf1rs.getString(26));
                covrpf.setStatSect(sqlcovrpf1rs.getString(27));
                covrpf.setStatSubsect(sqlcovrpf1rs.getString(28));
                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(29));
                covrpf.setPremCessDate(sqlcovrpf1rs.getInt(30));
                covrpf.setBenCessDate(sqlcovrpf1rs.getInt(31));
                covrpf.setNextActDate(sqlcovrpf1rs.getInt(32));
                covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(33));
                covrpf.setPremCessAge(sqlcovrpf1rs.getInt(34));
                covrpf.setBenCessAge(sqlcovrpf1rs.getInt(35));
                covrpf.setBenCessTerm(sqlcovrpf1rs.getInt(36));
                covrpf.setSicurr(sqlcovrpf1rs.getString(37));
                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal(38));
                covrpf.setMortcls(sqlcovrpf1rs.getString(39));
                covrpf.setReptcd01(sqlcovrpf1rs.getString(40));
                covrpf.setReptcd02(sqlcovrpf1rs.getString(41));
                covrpf.setReptcd03(sqlcovrpf1rs.getString(42));
                covrpf.setReptcd04(sqlcovrpf1rs.getString(43));
                covrpf.setReptcd05(sqlcovrpf1rs.getString(44));
                covrpf.setReptcd06(sqlcovrpf1rs.getString(45));
                covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal(46));
                covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal(47));
                covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal(48));
                covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal(49));
                covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal(50));
                covrpf.setTermid(sqlcovrpf1rs.getString(51));
                covrpf.setTransactionDate(sqlcovrpf1rs.getInt(52));
                covrpf.setTransactionTime(sqlcovrpf1rs.getInt(53));
                covrpf.setUser(sqlcovrpf1rs.getInt(54));
                covrpf.setLiencd(sqlcovrpf1rs.getString(55));
                covrpf.setRatingClass(sqlcovrpf1rs.getString(56));
                covrpf.setIndexationInd(sqlcovrpf1rs.getString(57));
                covrpf.setBonusInd(sqlcovrpf1rs.getString(58));
                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString(59));
                covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal(60));
                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString(61));
                covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal(62));
                covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal(63));
                covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal(64));
                covrpf.setEstMatDate01(sqlcovrpf1rs.getInt(65));
                covrpf.setEstMatDate02(sqlcovrpf1rs.getInt(66));
                covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal(67));
                covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal(68));
                covrpf.setCampaign(sqlcovrpf1rs.getString(69));
                covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal(70));
                covrpf.setRtrnyrs(sqlcovrpf1rs.getInt(71));
                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString(72));
                covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt(73));
                covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString(74));
                covrpf.setFundSplitPlan(sqlcovrpf1rs.getString(75));
                covrpf.setStatreasn(sqlcovrpf1rs.getString(76));
                covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(77));
                covrpf.setSex(sqlcovrpf1rs.getString(78));
                covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt(79));
                covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt(80));
                covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt(81));
                covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt(82));
                covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt(83));
                covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt(84));
                covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt(85));
                covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt(86));
                covrpf.setJlLsInd(sqlcovrpf1rs.getString(87));
                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(88));
                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(89));
                covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(90));
                covrpf.setReviewProcessing(sqlcovrpf1rs.getInt(91));
                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(92));
                covrpf.setCpiDate(sqlcovrpf1rs.getInt(93));
                covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt(94));
                covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt(95));
                covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt(96));
                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(97));
                covrpf.setPayrseqno(sqlcovrpf1rs.getInt(98));
                covrpf.setBappmeth(sqlcovrpf1rs.getString(99));
                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(100));
                covrpf.setLoadper(sqlcovrpf1rs.getBigDecimal(101));
                covrpf.setRateadj(sqlcovrpf1rs.getBigDecimal(102));
                covrpf.setFltmort(sqlcovrpf1rs.getBigDecimal(103));
                covrpf.setPremadj(sqlcovrpf1rs.getBigDecimal(104));
                covrpf.setAgeadj(sqlcovrpf1rs.getBigDecimal(105));
				covrpf.setZclstate(sqlcovrpf1rs.getString(106));
                covrpfSearchResult.add(covrpf);
            }

        } catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psCovrSelect, sqlcovrpf1rs);
        }
        return covrpfSearchResult;
    }
    // add by yy for ILIFE-2893
    public Map<String, List<Covrpf>> searchCovrMap(String coy, List<String> chdrnumList) {
        StringBuilder sqlCovrSelect1 = new StringBuilder(
                "SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,UNIQUE_NUMBER,RISKPREM,ZSTPDUTY01,ZCLSTATE,TPDTYPE,REINSTATED,PRORATEPREM ");
        sqlCovrSelect1.append("FROM COVRPF WHERE CHDRCOY=? AND VALIDFLAG = '1' AND ");
        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlCovrSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
        ResultSet sqlcovrpf1rs = null;
        Map<String, List<Covrpf>> covrMap = new HashMap<String, List<Covrpf>>();
        try {
            psCovrSelect.setString(1, coy);
            sqlcovrpf1rs = executeQuery(psCovrSelect);

            while (sqlcovrpf1rs.next()) {
                Covrpf covrpf = new Covrpf();
                covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                covrpf.setLife(sqlcovrpf1rs.getString(3));
                covrpf.setJlife(sqlcovrpf1rs.getString(4));
                covrpf.setCoverage(sqlcovrpf1rs.getString(5));
                covrpf.setRider(sqlcovrpf1rs.getString(6));
                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(7));
                covrpf.setValidflag(sqlcovrpf1rs.getString(8));
                covrpf.setTranno(sqlcovrpf1rs.getInt(9));
                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(10));
                covrpf.setCurrto(sqlcovrpf1rs.getInt(11));
                covrpf.setStatcode(sqlcovrpf1rs.getString(12));
                covrpf.setPstatcode(sqlcovrpf1rs.getString(13));
                covrpf.setStatreasn(sqlcovrpf1rs.getString(14));
                covrpf.setCrrcd(sqlcovrpf1rs.getInt(15));
                covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(16));
                covrpf.setSex(sqlcovrpf1rs.getString(17));
                covrpf.setReptcd01(sqlcovrpf1rs.getString(18));
                covrpf.setReptcd02(sqlcovrpf1rs.getString(19));
                covrpf.setReptcd03(sqlcovrpf1rs.getString(20));
                covrpf.setReptcd04(sqlcovrpf1rs.getString(21));
                covrpf.setReptcd05(sqlcovrpf1rs.getString(22));
                covrpf.setReptcd06(sqlcovrpf1rs.getString(23));
                covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal(24));
                covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal(25));
                covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal(26));
                covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal(27));
                covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal(28));
                covrpf.setPremCurrency(sqlcovrpf1rs.getString(29));
                covrpf.setTermid(sqlcovrpf1rs.getString(30));
                covrpf.setTransactionDate(sqlcovrpf1rs.getInt(31));
                covrpf.setTransactionTime(sqlcovrpf1rs.getInt(32));
                covrpf.setUser(sqlcovrpf1rs.getInt(33));
                covrpf.setStatFund(sqlcovrpf1rs.getString(34));
                covrpf.setStatSect(sqlcovrpf1rs.getString(35));
                covrpf.setStatSubsect(sqlcovrpf1rs.getString(36));
                covrpf.setCrtable(sqlcovrpf1rs.getString(37));
                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(38));
                covrpf.setPremCessDate(sqlcovrpf1rs.getInt(39));
                covrpf.setBenCessDate(sqlcovrpf1rs.getInt(40));
                covrpf.setNextActDate(sqlcovrpf1rs.getInt(41));
                covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(42));
                covrpf.setPremCessAge(sqlcovrpf1rs.getInt(43));
                covrpf.setBenCessAge(sqlcovrpf1rs.getInt(44));
                covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(45));
                covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(46));
                covrpf.setBenCessTerm(sqlcovrpf1rs.getInt(47));
                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(48));
                covrpf.setSicurr(sqlcovrpf1rs.getString(49));
                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal(50));
                covrpf.setMortcls(sqlcovrpf1rs.getString(51));
                covrpf.setLiencd(sqlcovrpf1rs.getString(52));
                covrpf.setRatingClass(sqlcovrpf1rs.getString(53));
                covrpf.setIndexationInd(sqlcovrpf1rs.getString(54));
                covrpf.setBonusInd(sqlcovrpf1rs.getString(55));
                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString(56));
                covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal(57));
                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString(58));
                covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal(59));
                covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal(60));
                covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal(61));
                covrpf.setEstMatDate01(sqlcovrpf1rs.getInt(62));
                covrpf.setEstMatDate02(sqlcovrpf1rs.getInt(63));
                covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal(64));
                covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal(65));
                covrpf.setCampaign(sqlcovrpf1rs.getString(66));
                covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal(67));
                covrpf.setRtrnyrs(sqlcovrpf1rs.getInt(68));
                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString(69));
                covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt(70));
                covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString(71));
                covrpf.setFundSplitPlan(sqlcovrpf1rs.getString(72));
                covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt(73));
                covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt(74));
                covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt(75));
                covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt(76));
                covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt(77));
                covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt(78));
                covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt(79));
                covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt(80));
                covrpf.setJlLsInd(sqlcovrpf1rs.getString(81));
                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(82));
                covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(83));
                covrpf.setRerateDate(sqlcovrpf1rs.getInt(84));
                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(85));
                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(86));
                covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(87));
                covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(88));
                covrpf.setReviewProcessing(sqlcovrpf1rs.getInt(89));
                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(90));
                covrpf.setCpiDate(sqlcovrpf1rs.getInt(91));
                covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt(92));
                covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt(93));
                covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt(94));
                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(95));
                covrpf.setPayrseqno(sqlcovrpf1rs.getInt(96));
                covrpf.setBappmeth(sqlcovrpf1rs.getString(97));
                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(98));
                covrpf.setZlinstprem(((sqlcovrpf1rs.getBigDecimal(99)==null )? BigDecimal.ZERO:sqlcovrpf1rs.getBigDecimal(99)));
                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(100));
                covrpf.setRiskprem(((sqlcovrpf1rs.getBigDecimal(101)==null )? BigDecimal.ZERO:sqlcovrpf1rs.getBigDecimal(101))); //ILIFE-7845 
				covrpf.setZstpduty01(sqlcovrpf1rs.getBigDecimal(102));
                covrpf.setZclstate(sqlcovrpf1rs.getString(103));
                covrpf.setTpdtype(sqlcovrpf1rs.getString(104));//ILIFE-8509
                covrpf.setReinstated(sqlcovrpf1rs.getString(105));
                covrpf.setProrateprem(sqlcovrpf1rs.getBigDecimal(106));
                if (covrMap.containsKey(covrpf.getChdrnum())) {
                    covrMap.get(covrpf.getChdrnum()).add(covrpf);
                } else {
                    List<Covrpf> chdrList = new ArrayList<Covrpf>();
                    chdrList.add(covrpf);
                    covrMap.put(covrpf.getChdrnum(), chdrList);
                }
            }

        } catch (SQLException e) {
			LOGGER.error("searchCovrMap()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psCovrSelect, sqlcovrpf1rs);
        }
        return covrMap;

    }
    //ILIFE-2882 by wli31
    public List<Covrpf> searchCovrrnlRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider,double plnsfx ){
    	StringBuilder sql = new StringBuilder();
    	sql.append("select cl.chdrcoy,cl.chdrnum,cl.cbcvin,cl.coverage,cl.crrcd,cl.crtable,cl.currfrom,cl.instprem,cl.jlife,cl.life,cl.plnsfx,cl.pcestrm,cl.prmcur,cl.pstatcode,cl.rrtdat,cl.rider,cl.rcestrm,"
    			+ "cl.singp,cl.statcode,cl.sumins,cl.unique_number,cl.validflag,cl.zlinstprem,cl.rcesdte,cl.anbccd ")
    	.append("from covrrnl cl where cl.chdrcoy=? and cl.chdrnum=? and cl.life=? and cl.coverage=? and cl.rider=? and cl.plnsfx = ? and validflag = '1' order by  chdrcoy,chdrnum,life,coverage,rider,plnsfx,unique_number desc");
    	 PreparedStatement ps = getPrepareStatement(sql.toString());
    	 ResultSet sqlcovrpf1rs = null;
         List<Covrpf> covrrnlist = null;
     	 try {
  			 ps.setString(1, chdrcoy);
  			 ps.setString(2, chdrnum);
  			 ps.setString(3, life);
  			 ps.setString(4, coverage);
  			 ps.setString(5, rider);
  			 ps.setDouble(6, plnsfx);
  			 sqlcovrpf1rs = executeQuery(ps);
     		 covrrnlist = new ArrayList<Covrpf>();
 			 while(sqlcovrpf1rs.next()){
                 Covrpf covrpf = new Covrpf();
                 covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                 covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                 covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(3));
                 covrpf.setCoverage(sqlcovrpf1rs.getString(4));
                 covrpf.setCrrcd(sqlcovrpf1rs.getInt(5));
                 covrpf.setCrtable(sqlcovrpf1rs.getString(6));
                 covrpf.setCurrfrom(sqlcovrpf1rs.getInt(7));
                 covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(8));
                 covrpf.setJlife(sqlcovrpf1rs.getString(9));
                 covrpf.setLife(sqlcovrpf1rs.getString(10));
                 covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(11));
                 covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(12));
                 covrpf.setPremCurrency(sqlcovrpf1rs.getString(13));
                 covrpf.setPstatcode(sqlcovrpf1rs.getString(14));
                 covrpf.setRerateDate(sqlcovrpf1rs.getInt(15));
                 covrpf.setRider(sqlcovrpf1rs.getString(16));
                 covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(17));
                 covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(18));
                 covrpf.setStatcode(sqlcovrpf1rs.getString(19));
                 covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(20));
                 covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(21));
                 covrpf.setValidflag(sqlcovrpf1rs.getString(22));
                 covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(23));
                 covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(24));
                 covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(25));
 				covrrnlist.add(covrpf);
 			}
 		} catch (SQLException e) {
			LOGGER.error("searchCovrByT5679FromB5018dtosTemp()", e); /* IJTI-1479 */
             throw new SQLRuntimeException(e);
         } finally {
             close(ps, null);
         }
     	 return covrrnlist;
    }
    //ILIFE-3138-STARTS
    public List<Covrpf> SearchRecordsforMATY(String coy,String chdrnumFrom,String chdrnumTo,int effdate ){
    	
    	 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,VALIDFLAG,TRANNO,STATCODE,PSTATCODE,CRTABLE,RCESDTE");
         sqlCovrSelect1.append(" FROM COVRPF WHERE CHDRCOY = ?  ");
         sqlCovrSelect1.append(" AND CHDRNUM >= ? AND CHDRNUM <= ? AND ");
         sqlCovrSelect1.append(" RCESDTE <= ?  AND VALIDFLAG='1' ");
         sqlCovrSelect1
                 .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
      
    	
         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
    	 ResultSet sqlcovrpf1rs = null;
         List<Covrpf> covrlist = null;
     	 try {
  			 ps.setString(1,coy);
  			 ps.setString(2, chdrnumFrom);
  			 ps.setString(3, chdrnumTo);
  			ps.setInt(4, effdate);
  			
  			 sqlcovrpf1rs = executeQuery(ps);
     		 covrlist = new ArrayList<Covrpf>();
 			 while(sqlcovrpf1rs.next()){
                 Covrpf covrpf = new Covrpf();
                 covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                 covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                 covrpf.setLife(sqlcovrpf1rs.getString(3));
                 covrpf.setJlife(sqlcovrpf1rs.getString(4));
                 covrpf.setCoverage(sqlcovrpf1rs.getString(5));
                 covrpf.setRider(sqlcovrpf1rs.getString(6));
                 covrpf.setValidflag(sqlcovrpf1rs.getString(7));
                 covrpf.setTranno(sqlcovrpf1rs.getInt(8));
                 covrpf.setStatcode(sqlcovrpf1rs.getString(9));
                 covrpf.setPstatcode(sqlcovrpf1rs.getString(10));
                 covrpf.setCrtable(sqlcovrpf1rs.getString(11));
                 covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(12));
                  				covrlist.add(covrpf);
 			}
 		} catch (SQLException e) {
			LOGGER.error("SearchRecordsforMATY()", e); /* IJTI-1479 */
             throw new SQLRuntimeException(e);
         } finally {
             close(ps, null);
         }
     	 return covrlist;
    }
  
    
    //PINNACLE-1653-STARTS
    public List<Covrpf> searchRecordsforEXPY(String coy,String chdrnumFrom,String chdrnumTo,int effdate ){
    	
    	 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,VALIDFLAG,TRANNO,STATCODE,PSTATCODE,CRTABLE,RCESDTE");
         sqlCovrSelect1.append(" FROM COVRPF WHERE CHDRCOY = ?  ");
         sqlCovrSelect1.append(" AND CHDRNUM >= ? AND CHDRNUM <= ? AND ");
         sqlCovrSelect1.append(" RCESDTE = ?  AND VALIDFLAG='1' ");
         sqlCovrSelect1
                 .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
      
    	
         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
    	 ResultSet sqlcovrpf1rs = null;
         List<Covrpf> covrlist = null;
     	 try {
  			 ps.setString(1,coy);
  			 ps.setString(2, chdrnumFrom);
  			 ps.setString(3, chdrnumTo);
  			ps.setInt(4, effdate);
  			
  			 sqlcovrpf1rs = executeQuery(ps);
     		 covrlist = new ArrayList<Covrpf>();
 			 while(sqlcovrpf1rs.next()){
                 Covrpf covrpf = new Covrpf();
                 covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                 covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                 covrpf.setLife(sqlcovrpf1rs.getString(3));
                 covrpf.setJlife(sqlcovrpf1rs.getString(4));
                 covrpf.setCoverage(sqlcovrpf1rs.getString(5));
                 covrpf.setRider(sqlcovrpf1rs.getString(6));
                 covrpf.setValidflag(sqlcovrpf1rs.getString(7));
                 covrpf.setTranno(sqlcovrpf1rs.getInt(8));
                 covrpf.setStatcode(sqlcovrpf1rs.getString(9));
                 covrpf.setPstatcode(sqlcovrpf1rs.getString(10));
                 covrpf.setCrtable(sqlcovrpf1rs.getString(11));
                 covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(12));
                  				covrlist.add(covrpf);
 			}
 		} catch (SQLException e) {
			LOGGER.error("searchRecordsforEXPY()", e);
             throw new SQLRuntimeException(e);
         } finally {
             close(ps, null);
         }
     	 return covrlist;
    }
    // PINNACLE-1653-END
    
	@Override
	public Covrpf getcovrincrRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int planSuffix) {
		StringBuilder sqlCovrSelect1 = new StringBuilder(
				" SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,UNIQUE_NUMBER ");
  
        sqlCovrSelect1.append(" FROM COVRPF WHERE CHDRCOY = ?  ");
        sqlCovrSelect1.append(" AND CHDRNUM = ? AND ");
        sqlCovrSelect1.append(" LIFE = ?  AND COVERAGE = ? AND RIDER= ? ");
        sqlCovrSelect1.append(" AND PLNSFX= ? AND VALIDFLAG <> '2' ");
        sqlCovrSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
     
   	
        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
		ResultSet rs = null;
		Covrpf covrpf = null;
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			ps.setInt(6,planSuffix);
			
			rs = ps.executeQuery();

			while (rs.next()) {
				covrpf = new Covrpf();
				covrpf.setChdrcoy(rs.getString(1));
                covrpf.setChdrnum(rs.getString(2));
                covrpf.setLife(rs.getString(3));
                covrpf.setJlife(rs.getString(4));
                covrpf.setCoverage(rs.getString(5));
                covrpf.setRider(rs.getString(6));
                covrpf.setPlanSuffix(rs.getInt(7));
                covrpf.setValidflag(rs.getString(8));
                covrpf.setTranno(rs.getInt(9));
                covrpf.setCurrfrom(rs.getInt(10));
                covrpf.setCurrto(rs.getInt(11));
                covrpf.setStatcode(rs.getString(12));
                covrpf.setPstatcode(rs.getString(13));
                covrpf.setStatreasn(rs.getString(14));
                covrpf.setCrrcd(rs.getInt(15));
                covrpf.setAnbAtCcd(rs.getInt(16));
                covrpf.setSex(rs.getString(17));
                covrpf.setReptcd01(rs.getString(18));
                covrpf.setReptcd02(rs.getString(19));
                covrpf.setReptcd03(rs.getString(20));
                covrpf.setReptcd04(rs.getString(21));
                covrpf.setReptcd05(rs.getString(22));
                covrpf.setReptcd06(rs.getString(23));
                covrpf.setCrInstamt01(rs.getBigDecimal(24));
                covrpf.setCrInstamt02(rs.getBigDecimal(25));
                covrpf.setCrInstamt03(rs.getBigDecimal(26));
                covrpf.setCrInstamt04(rs.getBigDecimal(27));
                covrpf.setCrInstamt05(rs.getBigDecimal(28));
                covrpf.setPremCurrency(rs.getString(29));
                covrpf.setTermid(rs.getString(30));
                covrpf.setTransactionDate(rs.getInt(31));
                covrpf.setTransactionTime(rs.getInt(32));
                covrpf.setUser(rs.getInt(33));
                covrpf.setStatFund(rs.getString(34));
                covrpf.setStatSect(rs.getString(35));
                covrpf.setStatSubsect(rs.getString(36));
                covrpf.setCrtable(rs.getString(37));
                covrpf.setRiskCessDate(rs.getInt(38));
                covrpf.setPremCessDate(rs.getInt(39));
                covrpf.setBenCessDate(rs.getInt(40));
                covrpf.setNextActDate(rs.getInt(41));
                covrpf.setRiskCessAge(rs.getInt(42));
                covrpf.setPremCessAge(rs.getInt(43));
                covrpf.setBenCessAge(rs.getInt(44));
                covrpf.setRiskCessTerm(rs.getInt(45));
                covrpf.setPremCessTerm(rs.getInt(46));
                covrpf.setBenCessTerm(rs.getInt(47));
                covrpf.setSumins(rs.getBigDecimal(48));
                covrpf.setSicurr(rs.getString(49));
                covrpf.setVarSumInsured(rs.getBigDecimal(50));
                covrpf.setMortcls(rs.getString(51));
                covrpf.setLiencd(rs.getString(52));
                covrpf.setRatingClass(rs.getString(53));
                covrpf.setIndexationInd(rs.getString(54));
                covrpf.setBonusInd(rs.getString(55));
                covrpf.setDeferPerdCode(rs.getString(56));
                covrpf.setDeferPerdAmt(rs.getBigDecimal(57));
                covrpf.setDeferPerdInd(rs.getString(58));
                covrpf.setTotMthlyBenefit(rs.getBigDecimal(59));
                covrpf.setEstMatValue01(rs.getBigDecimal(60));
                covrpf.setEstMatValue02(rs.getBigDecimal(61));
                covrpf.setEstMatDate01(rs.getInt(62));
                covrpf.setEstMatDate02(rs.getInt(63));
                covrpf.setEstMatInt01(rs.getBigDecimal(64));
                covrpf.setEstMatInt02(rs.getBigDecimal(65));
                covrpf.setCampaign(rs.getString(66));
                covrpf.setStatSumins(rs.getBigDecimal(67));
                covrpf.setRtrnyrs(rs.getInt(68));
                covrpf.setReserveUnitsInd(rs.getString(69));
                covrpf.setReserveUnitsDate(rs.getInt(70));
                covrpf.setChargeOptionsInd(rs.getString(71));
                covrpf.setFundSplitPlan(rs.getString(72));
                covrpf.setPremCessAgeMth(rs.getInt(73));
                covrpf.setPremCessAgeDay(rs.getInt(74));
                covrpf.setPremCessTermMth(rs.getInt(75));
                covrpf.setPremCessTermDay(rs.getInt(76));
                covrpf.setRiskCessAgeMth(rs.getInt(77));
                covrpf.setRiskCessAgeDay(rs.getInt(78));
                covrpf.setRiskCessTermMth(rs.getInt(79));
                covrpf.setRiskCessTermDay(rs.getInt(80));
                covrpf.setJlLsInd(rs.getString(81));
                covrpf.setInstprem(rs.getBigDecimal(82));
                covrpf.setSingp(rs.getBigDecimal(83));
                covrpf.setRerateDate(rs.getInt(84));
                covrpf.setRerateFromDate(rs.getInt(85));
                covrpf.setBenBillDate(rs.getInt(86));
                covrpf.setAnnivProcDate(rs.getInt(87));
                covrpf.setConvertInitialUnits(rs.getInt(88));
                covrpf.setReviewProcessing(rs.getInt(89));
                covrpf.setUnitStatementDate(rs.getInt(90));
                covrpf.setCpiDate(rs.getInt(91));
                covrpf.setInitUnitCancDate(rs.getInt(92));
                covrpf.setExtraAllocDate(rs.getInt(93));			
                covrpf.setInitUnitIncrsDate(rs.getInt(94));
                covrpf.setCoverageDebt(rs.getBigDecimal(95));
                covrpf.setPayrseqno(rs.getInt(96));
                covrpf.setBappmeth(rs.getString(97));
                covrpf.setZbinstprem(rs.getBigDecimal(98));
                covrpf.setZlinstprem(rs.getBigDecimal(99));
                covrpf.setUniqueNumber(rs.getLong(100));
			}
		} catch (SQLException e) {
			LOGGER.error("getcovrincrRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return covrpf;


		
	}
	@Override
	public void updateCovrIndexationind(Covrpf covrincr) {
		 String SQL_COVR_UPDATE = "UPDATE COVRPF SET INDXIN=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE UNIQUE_NUMBER=? ";
		 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
      
         try {
                 psCovrUpdate.setString(1, covrincr.getIndexationInd());
                 psCovrUpdate.setString(2, getJobnm());
                 psCovrUpdate.setString(3, getUsrprf());
                 psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                 psCovrUpdate.setLong(5, covrincr.getUniqueNumber());
                 psCovrUpdate.executeUpdate();
         } catch (SQLException e) {
			LOGGER.error("updateCovrIndexationind()", e); /* IJTI-1479 */
             throw new SQLRuntimeException(e);
         } finally {
             close(psCovrUpdate, null);
         }
		
	}
	

    public List<Covrpf> SearchRecordsforMatd(String coy,String chdrnum, int planSuffix ){
    	
   	 StringBuilder sqlCovrSelect1 = new StringBuilder(
				"SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PRMCUR,CRTABLE,CRRCD,RCESDTE,INSTPREM,SINGP,CBUNST,PSTATCODE,PLNSFX,VALIDFLAG,PAYRSEQNO ");
		sqlCovrSelect1.append(" FROM COVRPF WHERE CHDRCOY = ? and CHDRNUM = ? and PLNSFX = ? AND VALIDFLAG = '1' ");
        
        sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
     
   	
        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
   	 ResultSet sqlcovrpf1rs = null;
        List<Covrpf> covrlist = null;
    	 try {
    		 ps.setString(1, coy);
 			 ps.setString(2, chdrnum);
 			 ps.setInt(3, planSuffix);
 			
 			 sqlcovrpf1rs = executeQuery(ps);
    		 covrlist = new ArrayList<Covrpf>();
			 while(sqlcovrpf1rs.next()){
                Covrpf covrpf = new Covrpf();
                covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
                covrpf.setLife(sqlcovrpf1rs.getString(3));
                covrpf.setJlife(sqlcovrpf1rs.getString(4));
                covrpf.setCoverage(sqlcovrpf1rs.getString(5));
                covrpf.setRider(sqlcovrpf1rs.getString(6));
				covrpf.setPremCurrency(sqlcovrpf1rs.getString(7));
				covrpf.setCrtable(sqlcovrpf1rs.getString(8));
				covrpf.setCrrcd(sqlcovrpf1rs.getInt(9));
				covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(10));
				covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(11));
				covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(12));
				covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(13));
				covrpf.setPstatcode(sqlcovrpf1rs.getString(14));
				covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(15));
				covrpf.setValidflag(sqlcovrpf1rs.getString(16));
				covrpf.setPayrseqno(sqlcovrpf1rs.getInt(17));
                 covrlist.add(covrpf);
			}
		} catch (SQLException e) {
			LOGGER.error("SearchRecordsforMatd()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    	 return covrlist;
   }
	@Override
	public List<Covrpf> getcovrmatRecordsForMature(String chdrcoy, String chdrnum) {
		StringBuilder sqlCovrSelect1 = new StringBuilder(
                "SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,UNIQUE_NUMBER ");
        sqlCovrSelect1.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND PLNSFX = '0' AND VALIDFLAG = '1' ");
        
        sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
     
   	
        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
   	 ResultSet rs = null;
        List<Covrpf> covrmatList = null;
    	 try {
 			 ps.setString(1,chdrcoy);
 			 ps.setString(2, chdrnum); 		
 			 LOGGER.info(ps.toString());
 			 rs = ps.executeQuery();
 			covrmatList = new ArrayList<Covrpf>();
			 while(rs.next()){
                Covrpf covrpf = new Covrpf();
                covrpf.setChdrcoy(rs.getString(1));
                covrpf.setChdrnum(rs.getString(2));
                covrpf.setLife(rs.getString(3));
                covrpf.setJlife(rs.getString(4));
                covrpf.setCoverage(rs.getString(5));
                covrpf.setRider(rs.getString(6));
                covrpf.setPlanSuffix(rs.getInt(7));
                covrpf.setValidflag(rs.getString(8));
                covrpf.setTranno(rs.getInt(9));
                covrpf.setCurrfrom(rs.getInt(10));
                covrpf.setCurrto(rs.getInt(11));
                covrpf.setStatcode(rs.getString(12));
                covrpf.setPstatcode(rs.getString(13));
                covrpf.setStatreasn(rs.getString(14));
                covrpf.setCrrcd(rs.getInt(15));
                covrpf.setAnbAtCcd(rs.getInt(16));
                covrpf.setSex(rs.getString(17));
                covrpf.setReptcd01(rs.getString(18));
                covrpf.setReptcd02(rs.getString(19));
                covrpf.setReptcd03(rs.getString(20));
                covrpf.setReptcd04(rs.getString(21));
                covrpf.setReptcd05(rs.getString(22));
                covrpf.setReptcd06(rs.getString(23));
                covrpf.setCrInstamt01(rs.getBigDecimal(24));
                covrpf.setCrInstamt02(rs.getBigDecimal(25));
                covrpf.setCrInstamt03(rs.getBigDecimal(26));
                covrpf.setCrInstamt04(rs.getBigDecimal(27));
                covrpf.setCrInstamt05(rs.getBigDecimal(28));
                covrpf.setPremCurrency(rs.getString(29));
                covrpf.setTermid(rs.getString(30));
                covrpf.setTransactionDate(rs.getInt(31));
                covrpf.setTransactionTime(rs.getInt(32));
                covrpf.setUser(rs.getInt(33));
                covrpf.setStatFund(rs.getString(34));
                covrpf.setStatSect(rs.getString(35));
                covrpf.setStatSubsect(rs.getString(36));
                covrpf.setCrtable(rs.getString(37));
                covrpf.setRiskCessDate(rs.getInt(38));
                covrpf.setPremCessDate(rs.getInt(39));
                covrpf.setBenCessDate(rs.getInt(40));
                covrpf.setNextActDate(rs.getInt(41));
                covrpf.setRiskCessAge(rs.getInt(42));
                covrpf.setPremCessAge(rs.getInt(43));
                covrpf.setBenCessAge(rs.getInt(44));
                covrpf.setRiskCessTerm(rs.getInt(45));
                covrpf.setPremCessTerm(rs.getInt(46));
                covrpf.setBenCessTerm(rs.getInt(47));
                covrpf.setSumins(rs.getBigDecimal(48));
                covrpf.setSicurr(rs.getString(49));
                covrpf.setVarSumInsured(rs.getBigDecimal(50));
                covrpf.setMortcls(rs.getString(51));
                covrpf.setLiencd(rs.getString(52));
                covrpf.setRatingClass(rs.getString(53));
                covrpf.setIndexationInd(rs.getString(54));
                covrpf.setBonusInd(rs.getString(55));
                covrpf.setDeferPerdCode(rs.getString(56));
                covrpf.setDeferPerdAmt(rs.getBigDecimal(57));
                covrpf.setDeferPerdInd(rs.getString(58));
                covrpf.setTotMthlyBenefit(rs.getBigDecimal(59));
                covrpf.setEstMatValue01(rs.getBigDecimal(60));
                covrpf.setEstMatValue02(rs.getBigDecimal(61));
                covrpf.setEstMatDate01(rs.getInt(62));
                covrpf.setEstMatDate02(rs.getInt(63));
                covrpf.setEstMatInt01(rs.getBigDecimal(64));
                covrpf.setEstMatInt02(rs.getBigDecimal(65));
                covrpf.setCampaign(rs.getString(66));
                covrpf.setStatSumins(rs.getBigDecimal(67));
                covrpf.setRtrnyrs(rs.getInt(68));
                covrpf.setReserveUnitsInd(rs.getString(69));
                covrpf.setReserveUnitsDate(rs.getInt(70));
                covrpf.setChargeOptionsInd(rs.getString(71));
                covrpf.setFundSplitPlan(rs.getString(72));
                covrpf.setPremCessAgeMth(rs.getInt(73));
                covrpf.setPremCessAgeDay(rs.getInt(74));
                covrpf.setPremCessTermMth(rs.getInt(75));
                covrpf.setPremCessTermDay(rs.getInt(76));
                covrpf.setRiskCessAgeMth(rs.getInt(77));
                covrpf.setRiskCessAgeDay(rs.getInt(78));
                covrpf.setRiskCessTermMth(rs.getInt(79));
                covrpf.setRiskCessTermDay(rs.getInt(80));
                covrpf.setJlLsInd(rs.getString(81));
                covrpf.setInstprem(rs.getBigDecimal(82));
                covrpf.setSingp(rs.getBigDecimal(83));
                covrpf.setRerateDate(rs.getInt(84));
                covrpf.setRerateFromDate(rs.getInt(85));
                covrpf.setBenBillDate(rs.getInt(86));
                covrpf.setAnnivProcDate(rs.getInt(87));
                covrpf.setConvertInitialUnits(rs.getInt(88));
                covrpf.setReviewProcessing(rs.getInt(89));
                covrpf.setUnitStatementDate(rs.getInt(90));
                covrpf.setCpiDate(rs.getInt(91));
                covrpf.setInitUnitCancDate(rs.getInt(92));
                covrpf.setExtraAllocDate(rs.getInt(93));			
                covrpf.setInitUnitIncrsDate(rs.getInt(94));
                covrpf.setCoverageDebt(rs.getBigDecimal(95));
                covrpf.setPayrseqno(rs.getInt(96));
                covrpf.setBappmeth(rs.getString(97));
                covrpf.setZbinstprem(rs.getBigDecimal(98));
                covrpf.setZlinstprem(rs.getBigDecimal(99));
                covrpf.setUniqueNumber(rs.getLong(100));
                covrmatList.add(covrpf);
			}
		} catch (SQLException e) {
			LOGGER.error("getcovrmatRecordsForMature()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    	 return covrmatList;
	}
	@Override
	public boolean insertCovrpfList(List<Covrpf> insertCovrpfList) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO COVRPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,ZSTPDUTY01,ZCLSTATE,REINSTATED,TPDTYPE,PRORATEPREM,USRPRF,JOBNM,DATIME,COMMPREM) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
            for (Covrpf covr : insertCovrpfList) {
            	ps.setString(1,covr.getChdrcoy());
                ps.setString(2,covr.getChdrnum());
                ps.setString(3,covr.getLife());
                ps.setString(4,covr.getJlife());
                ps.setString(5,covr.getCoverage());
                ps.setString(6,covr.getRider());
                ps.setInt(7,covr.getPlanSuffix());
                ps.setString(8,covr.getValidflag());
                ps.setInt(9,covr.getTranno());
                ps.setInt(10,covr.getCurrfrom());
                ps.setInt(11,covr.getCurrto());
                ps.setString(12,covr.getStatcode());
                ps.setString(13,covr.getPstatcode());
                ps.setString(14,covr.getStatreasn());
                ps.setInt(15,covr.getCrrcd());
                ps.setInt(16,covr.getAnbAtCcd());
                ps.setString(17,covr.getSex());
                ps.setString(18,covr.getReptcd01());
                ps.setString(19,covr.getReptcd02());
                ps.setString(20,covr.getReptcd03());
                ps.setString(21,covr.getReptcd04());
                ps.setString(22,covr.getReptcd05());
                ps.setString(23,covr.getReptcd06());
                ps.setBigDecimal(24,covr.getCrInstamt01());
                ps.setBigDecimal(25,covr.getCrInstamt02());
                ps.setBigDecimal(26,covr.getCrInstamt03());
                ps.setBigDecimal(27,covr.getCrInstamt04());
                ps.setBigDecimal(28,covr.getCrInstamt05());
                ps.setString(29,covr.getPremCurrency());
                ps.setString(30,covr.getTermid());
                ps.setInt(31,covr.getTransactionDate());
                ps.setInt(32,covr.getTransactionTime());
                ps.setInt(33,covr.getUser());
                ps.setString(34,covr.getStatFund());
                ps.setString(35,covr.getStatSect());
                ps.setString(36,covr.getStatSubsect());
                ps.setString(37,covr.getCrtable());
                ps.setInt(38,covr.getRiskCessDate());
                ps.setInt(39,covr.getPremCessDate());
                ps.setInt(40,covr.getBenCessDate());
                ps.setInt(41,covr.getNextActDate());
                ps.setInt(42,covr.getRiskCessAge());
                ps.setInt(43,covr.getPremCessAge());
                ps.setInt(44,covr.getBenCessAge());
                ps.setInt(45,covr.getRiskCessTerm());
                ps.setInt(46,covr.getPremCessTerm());
                ps.setInt(47,covr.getBenCessTerm());
                ps.setBigDecimal(48,covr.getSumins());
                ps.setString(49,covr.getSicurr());
                ps.setBigDecimal(50,covr.getVarSumInsured());
                ps.setString(51,covr.getMortcls());
                ps.setString(52,covr.getLiencd());
                ps.setString(53,covr.getRatingClass());
                ps.setString(54,covr.getIndexationInd());
                ps.setString(55,covr.getBonusInd());
                ps.setString(56,covr.getDeferPerdCode());
                ps.setBigDecimal(57,covr.getDeferPerdAmt());
                ps.setString(58,covr.getDeferPerdInd());
                ps.setBigDecimal(59,covr.getTotMthlyBenefit());
                ps.setBigDecimal(60,covr.getEstMatValue01());
                ps.setBigDecimal(61,covr.getEstMatValue02());
                ps.setInt(62,covr.getEstMatDate01());
                ps.setInt(63,covr.getEstMatDate02());
                ps.setBigDecimal(64,covr.getEstMatInt01());
                ps.setBigDecimal(65,covr.getEstMatInt02());
                ps.setString(66,covr.getCampaign());
                ps.setBigDecimal(67,covr.getStatSumins());
                ps.setInt(68,covr.getRtrnyrs());
                ps.setString(69,covr.getReserveUnitsInd());
                ps.setInt(70,covr.getReserveUnitsDate());
                ps.setString(71,covr.getChargeOptionsInd());
                ps.setString(72,covr.getFundSplitPlan());
                ps.setInt(73,covr.getPremCessAgeMth());
                ps.setInt(74,covr.getPremCessAgeDay());
                ps.setInt(75,covr.getPremCessTermMth());
                ps.setInt(76,covr.getPremCessTermDay());
                ps.setInt(77,covr.getRiskCessAgeMth());
                ps.setInt(78,covr.getRiskCessAgeDay());
                ps.setInt(79,covr.getRiskCessTermMth());
                ps.setInt(80,covr.getRiskCessTermDay());
                ps.setString(81,covr.getJlLsInd());
                ps.setBigDecimal(82,covr.getInstprem());
                ps.setBigDecimal(83,covr.getSingp());
                ps.setInt(84,covr.getRerateDate());
                ps.setInt(85,covr.getRerateFromDate());
                ps.setInt(86,covr.getBenBillDate());
                ps.setInt(87,covr.getAnnivProcDate());
                ps.setInt(88,covr.getConvertInitialUnits());
                ps.setInt(89,covr.getReviewProcessing());
                ps.setInt(90,covr.getUnitStatementDate());
                ps.setInt(91,covr.getCpiDate());
                ps.setInt(92,covr.getInitUnitCancDate());
                ps.setInt(93,covr.getExtraAllocDate());			
                ps.setInt(94,covr.getInitUnitIncrsDate());
                ps.setBigDecimal(95,covr.getCoverageDebt());
                ps.setLong(96,covr.getPayrseqno());
                ps.setString(97,covr.getBappmeth());
                ps.setBigDecimal(98,covr.getZbinstprem());
                ps.setBigDecimal(99,covr.getZlinstprem());
                ps.setBigDecimal(100,covr.getZstpduty01());
                ps.setString(101,covr.getZclstate());
                ps.setString(102,covr.getReinstated());//ILIFE-8509
                ps.setString(103,covr.getTpdtype());
                ps.setBigDecimal(104, covr.getProrateprem() == null ? BigDecimal.ZERO : covr.getProrateprem());
                ps.setString(105, this.getUsrprf());
				ps.setString(106, this.getJobnm());
				ps.setTimestamp(107, new Timestamp(System.currentTimeMillis()));
				ps.setBigDecimal(108,covr.getCommprem());
           
                ps.addBatch();
            }		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("insertCovrpfList()", e); /* IJTI-1479 */	
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return isInsertSuccessful;
	}		
	
		 public void updateCovrRecord(List<Covrpf> updateCovrpflist, int busDate) {
		        if (updateCovrpflist != null && updateCovrpflist.size() > 0) {
		            String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG=? ,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

		            PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
		            try {
		                for (Covrpf c : updateCovrpflist) {
		                	psCovrUpdate.setString(1,c.getValidflag());
		                	psCovrUpdate.setInt(2, c.getCurrto());
		                	psCovrUpdate.setString(3, getJobnm());
		                	psCovrUpdate.setString(4, getUsrprf());
		                	psCovrUpdate.setTimestamp(5,getDatime());
		                	psCovrUpdate.setLong(6, c.getUniqueNumber());
		                	psCovrUpdate.addBatch();
		                }
		                psCovrUpdate.executeBatch();
		            } catch (SQLException e) {
		            	LOGGER.error("updateCovrRecord()", e); /* IJTI-1479 */
		                throw new SQLRuntimeException(e);
		            } finally {
		                close(psCovrUpdate, null);
		            }
		        }
		    }
 //ILIFE-3138-ENDS
		 /*ILIFE-3149 starts*/
		@Override
		public List<BigDecimal> getTotalStampDuty(String chdrcoy, String chdrnum) {
			StringBuilder sqlCovrSelect1 = new StringBuilder( 
				     "SELECT  SUM(INSTPREM) AS INSTPREM,SUM(ZSTPDUTY01) AS ZSTPDUTY01 ");
					  sqlCovrSelect1.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND STATCODE IN ('IF','PU') AND VALIDFLAG = '1' ");
					 Covrpf covrpf = null;
			         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
		       	   	 ResultSet rs = null;
		       	  List<BigDecimal> covrpfSearchResult = new ArrayList<BigDecimal>();
				    	 try {
				 			 ps.setString(1,chdrcoy);
				 			 ps.setString(2, chdrnum); 		
				 			 LOGGER.info(ps.toString());
				 			 rs = ps.executeQuery();
				 	
							 if(rs.next()){
								 BigDecimal TotalinstPrem = rs.getBigDecimal(1);
								 if((TotalinstPrem == null))
								 {
									 TotalinstPrem= BigDecimal.ZERO;
								 }
								 covrpfSearchResult.add(TotalinstPrem);
								 
								 BigDecimal TotalStampDuty = rs.getBigDecimal(2);
								
								 if((TotalStampDuty == null))
								 {
									 TotalStampDuty= BigDecimal.ZERO;
								 }
								 covrpfSearchResult.add(TotalStampDuty);
							 }
							
		} catch (SQLException e) {
			LOGGER.error("getTotalStampDuty()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
				    	 
		return covrpfSearchResult;
				       
		}
	/**
	 * Fetch the list COVRPF records associated with the Contract
	 * Introduced as part of ILIFE-3392
	 * 
	 * @see com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO#searchCovrRecordForContract(com.csc.life.productdefinition.dataaccess.model.Covrpf)
	 * 
	 * @since ILIFE-16.3
	 * @param com.csc.life.productdefinition.dataaccess.model.Covrpf
	 * @return java.util.List<Covrpf>
	 */

	@Override
	public List<Covrpf> searchCovrRecordForContract(Covrpf covrpfModel) {
		
		// ---------------------------------
		// Initialize variables
		// ---------------------------------
		Covrpf covrpf = null;
		List<Covrpf> covrpfSearchResult = new LinkedList<Covrpf>();
		PreparedStatement psCovrSelect = null;
		ResultSet sqlcovrpf1rs = null;
		StringBuilder sqlCovrSelectBuilder = new StringBuilder();
		
		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovrSelectBuilder.append("SELECT CHDRCOY,CHDRNUM,COVERAGE,CRTABLE,LIFE,PLNSFX,PSTATCODE,RIDER,STATCODE,CRRCD,CURRFROM,UNIQUE_NUMBER,VALIDFLAG,ZCLSTATE ");
//		sqlCovrSelectBuilder.append(" INSTPREM, JLIFE, LIFE, PLNSFX, PCESTRM, PRMCUR, PSTATCODE,");
//		sqlCovrSelectBuilder.append(" RRTDAT, RIDER, RCESTRM, SINGP, STATCODE, SUMINS, UNIQUE_NUMBER, ");
//		sqlCovrSelectBuilder.append(" VALIDFLAG, ZLINSTPREM, PCESDTE ");
		sqlCovrSelectBuilder.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG='1' ");
		sqlCovrSelectBuilder.append(" ORDER BY ");
		sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		
		psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
		
		try {
			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovrSelect.setString(1, covrpfModel.getChdrcoy().trim());
			psCovrSelect.setString(2, covrpfModel.getChdrnum());
			
			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlcovrpf1rs = executeQuery(psCovrSelect);


			while (sqlcovrpf1rs.next()) {
				
				covrpf = new Covrpf();
				
				covrpf.setChdrcoy(sqlcovrpf1rs.getString("Chdrcoy"));
				covrpf.setChdrnum(sqlcovrpf1rs.getString("Chdrnum"));
				covrpf.setCoverage(sqlcovrpf1rs.getString("Coverage"));
				covrpf.setCrtable(sqlcovrpf1rs.getString("Crtable"));
				covrpf.setLife(sqlcovrpf1rs.getString("Life"));
				covrpf.setPlanSuffix(sqlcovrpf1rs.getInt("Plnsfx"));
				covrpf.setPstatcode(sqlcovrpf1rs.getString("Pstatcode"));
				covrpf.setRider(sqlcovrpf1rs.getString("Rider"));
				covrpf.setStatcode(sqlcovrpf1rs.getString("Statcode"));
				covrpf.setCrrcd(sqlcovrpf1rs.getInt("Crrcd"));
				covrpf.setCurrfrom(sqlcovrpf1rs.getInt("Currfrom"));
				covrpf.setUniqueNumber(sqlcovrpf1rs.getLong("Unique_number"));
				covrpf.setValidflag(sqlcovrpf1rs.getString("Validflag"));
				covrpf.setZclstate(sqlcovrpf1rs.getString("ZCLSTATE"));
				
				// ---------------------------------
				// Add to list
				// ---------------------------------
				covrpfSearchResult.add(covrpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrRecordForContract()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrSelect, sqlcovrpf1rs);
		}
		return covrpfSearchResult;
	}

	 public List<Covrpf> selectCovrRecordBegin(Covrpf covrpf){
	   	 List<Covrpf> covrResult = new ArrayList<Covrpf>();
	   	 StringBuilder sb = new StringBuilder("");
	   	 sb.append("SELECT CHDRCOY,CHDRNUM,CBCVIN,COVERAGE,CRRCD,CRTABLE,CURRFROM,INSTPREM,JLIFE,LIFE,PLNSFX,PCESTRM,PRMCUR,PSTATCODE,RRTDAT,RIDER,RCESTRM,SINGP,STATCODE,SUMINS,UNIQUE_NUMBER,VALIDFLAG,ZLINSTPREM ");
	 	 sb.append("FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND VALIDFLAG='1'");//ILB-456
	   	 if(StringUtils.isNotEmpty(covrpf.getCoverage())  && (StringUtils.isNotEmpty(covrpf.getRider())))// ILIFE-7767
	   		 sb.append(" AND COVERAGE=? AND RIDER=? "); 
	   	 if (covrpf.getPlanSuffix() != 9999) {
	   		sb.append(" AND PLNSFX=? ");
	   	 }
	   	 sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
	   	 PreparedStatement ps = getPrepareStatement(sb.toString());
	        ResultSet sql1rs = null;
	        try {
	       	
	         ps.setString(1, covrpf.getChdrcoy().trim());//ILB-456
	       	 ps.setString(2, covrpf.getChdrnum());
	       	 ps.setString(3, covrpf.getLife());
	       	
//	         ps.setString(5, covrpf.getValidflag());
	         if(StringUtils.isNotEmpty(covrpf.getCoverage())  && (StringUtils.isNotEmpty(covrpf.getRider())))// ILIFE-7767
	       	 {
		        	 ps.setString(4, covrpf.getCoverage());
		        	 ps.setString(5, covrpf.getRider());
		        	 if (covrpf.getPlanSuffix() != 9999) {
		        		 ps.setInt(6, covrpf.getPlanSuffix());
		     	   	 }
	       	 } else {
	       		if (covrpf.getPlanSuffix() != 9999) {
	        		 ps.setInt(4, covrpf.getPlanSuffix());
	     	   	 }
	       	 }
	       	
		        	
	       	
	       	 sql1rs = executeQuery(ps);

	            while (sql1rs.next()) {  
	           	 covrpf = new Covrpf();
	                covrpf.setChdrcoy(sql1rs.getString(1));
	                covrpf.setChdrnum(sql1rs.getString(2));
	                covrpf.setConvertInitialUnits(sql1rs.getInt(3));
	                covrpf.setCoverage(sql1rs.getString(4));
	                covrpf.setCrrcd(sql1rs.getInt(5));
	                covrpf.setCrtable(sql1rs.getString(6));
	                covrpf.setCurrfrom(sql1rs.getInt(7));
	                covrpf.setInstprem(sql1rs.getBigDecimal(8));
	                covrpf.setJlife(sql1rs.getString(9));
	                covrpf.setLife(sql1rs.getString(10));
	                covrpf.setPlanSuffix(sql1rs.getInt(11));
	                covrpf.setPremCessTerm(sql1rs.getInt(12));
	                covrpf.setPremCurrency(sql1rs.getString(13));
	                covrpf.setPstatcode(sql1rs.getString(14));
	                covrpf.setRerateDate(sql1rs.getInt(15));
	                covrpf.setRider(sql1rs.getString(16));
	                covrpf.setRiskCessTerm(sql1rs.getInt(17));
	                covrpf.setSingp(sql1rs.getBigDecimal(18));
	                covrpf.setStatcode(sql1rs.getString(19));
	                covrpf.setSumins(sql1rs.getBigDecimal(20));
	                covrpf.setUniqueNumber(sql1rs.getLong(21));
	                covrpf.setValidflag(sql1rs.getString(22));
	                covrpf.setZlinstprem(sql1rs.getBigDecimal(23));
	                covrResult.add(covrpf);
	            }

	        }
	        catch (SQLException e) {
	        	LOGGER.error("selectCovrRecord()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, sql1rs);
	        }
	   	return covrResult;
	   }
    
    public List<Covrpf> selectCovrRecord(Covrpf covrpf){
   	 List<Covrpf> covrResult = new ArrayList<Covrpf>();
   	 StringBuilder sb = new StringBuilder("");
   	 sb.append("SELECT CHDRCOY,CHDRNUM,CBCVIN,COVERAGE,CRRCD,CRTABLE,CURRFROM,INSTPREM,JLIFE,LIFE,PLNSFX,PCESTRM,PRMCUR,PSTATCODE,RRTDAT,RIDER,RCESTRM,SINGP,STATCODE,SUMINS,UNIQUE_NUMBER,VALIDFLAG,ZLINSTPREM,RCESDTE,CBUNST ");
 	 sb.append("FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND PLNSFX=? AND VALIDFLAG=?");//ILB-456
   	 if(StringUtils.isNotEmpty(covrpf.getCoverage())  && (StringUtils.isNotEmpty(covrpf.getRider())))// ILIFE-7767
   		 sb.append(" AND COVERAGE=? AND RIDER=? ");
   	 sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
   	 PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet sql1rs = null;
        try {
       	
         ps.setString(1, covrpf.getChdrcoy().trim());//ILB-456
       	 ps.setString(2, covrpf.getChdrnum());
       	 ps.setString(3, covrpf.getLife());
       	 ps.setInt(4, covrpf.getPlanSuffix());
         ps.setString(5, covrpf.getValidflag());
         if(StringUtils.isNotEmpty(covrpf.getCoverage())  && (StringUtils.isNotEmpty(covrpf.getRider())))// ILIFE-7767
       	 {
	        	 ps.setString(6, covrpf.getCoverage());
	        	 ps.setString(7, covrpf.getRider());
       	 }
       	
	        	
       	
       	 sql1rs = executeQuery(ps);

            while (sql1rs.next()) {  
           	 covrpf = new Covrpf();
                covrpf.setChdrcoy(sql1rs.getString(1));
                covrpf.setChdrnum(sql1rs.getString(2));
                covrpf.setConvertInitialUnits(sql1rs.getInt(3));
                covrpf.setCoverage(sql1rs.getString(4));
                covrpf.setCrrcd(sql1rs.getInt(5));
                covrpf.setCrtable(sql1rs.getString(6));
                covrpf.setCurrfrom(sql1rs.getInt(7));
                covrpf.setInstprem(sql1rs.getBigDecimal(8));
                covrpf.setJlife(sql1rs.getString(9));
                covrpf.setLife(sql1rs.getString(10));
                covrpf.setPlanSuffix(sql1rs.getInt(11));
                covrpf.setPremCessTerm(sql1rs.getInt(12));
                covrpf.setPremCurrency(sql1rs.getString(13));
                covrpf.setPstatcode(sql1rs.getString(14));
                covrpf.setRerateDate(sql1rs.getInt(15));
                covrpf.setRider(sql1rs.getString(16));
                covrpf.setRiskCessTerm(sql1rs.getInt(17));
                covrpf.setSingp(sql1rs.getBigDecimal(18));
                covrpf.setStatcode(sql1rs.getString(19));
                covrpf.setSumins(sql1rs.getBigDecimal(20));
                covrpf.setUniqueNumber(sql1rs.getLong(21));
                covrpf.setValidflag(sql1rs.getString(22));
                covrpf.setZlinstprem(sql1rs.getBigDecimal(23));
                covrpf.setRiskCessDate(sql1rs.getInt(24));
                covrpf.setUnitStatementDate(sql1rs.getInt(25));
                covrResult.add(covrpf);
            }

        }
        catch (SQLException e) {
			LOGGER.error("selectCovrRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, sql1rs);
        }
   	return covrResult;
   }
@Override
    public List<Covrpf> selectCovrSurData(Covrpf covrSurData) {
		long ts = System.currentTimeMillis();
		//IJTI-306 START
		int i=4;
		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append(" CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM,")
			.append(" CURRTO, JLIFE, STATCODE, PSTATCODE, STATREASN, CRRCD, ANBCCD, SEX, PRMCUR, ")
			.append(" TERMID, TRDT, TRTM, USER_T, STFUND, STSECT, CBCVIN, STSSECT, CRTABLE, RCESDTE, ")
			.append(" PCESDTE, BCESDTE, NXTDTE, SUMINS, SICURR, VARSI, MORTCLS, LIENCD, RATCLS, INDXIN, ")
			.append(" BNUSIN, DPCD, DPAMT, DPIND, TMBEN, SINGP, INSTPREM, CAMPAIGN, STSMIN, RTRNYRS, ")
			.append(" PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID, CBUNST, ")
			.append(" PAYRSEQNO FROM VM1DTA.COVRSUR")
			.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		if(covrSurData.getPlanSuffix()!=0){
			sql.append(" AND PLNSFX <= ? ");	
		}else{
			sql.append(" AND PLNSFX >= ? ");	
		}
		if(covrSurData.getLife()!=null && !covrSurData.getLife().equals("")){
			sql.append(" AND LIFE = ? ");	
		}
		if(covrSurData.getCoverage()!=null && !covrSurData.getCoverage().equals("")){
			sql.append(" AND COVERAGE = ? ");	
		}
		if(covrSurData.getRider()!=null && !covrSurData.getRider().equals("")){
			sql.append(" AND RIDER >= ? ");	
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Covrpf> data = new ArrayList<Covrpf>();
		try {
			stmt = getPrepareStatement(sql.toString());
			stmt.setString(1, covrSurData.getChdrcoy().trim());
			stmt.setString(2, covrSurData.getChdrnum());//IJTI-1485
			stmt.setInt(3, covrSurData.getPlanSuffix());
			if(covrSurData.getLife()!=null && !covrSurData.getLife().equals("")){
				stmt.setString(i, covrSurData.getLife());//IJTI-1485
				i+=1;
			}
			if(covrSurData.getCoverage()!=null && !covrSurData.getCoverage().equals("")){
				stmt.setString(i, covrSurData.getCoverage());//IJTI-1485
				i+=1;
			}
			if(covrSurData.getRider()!=null && !covrSurData.getRider().equals("")){
				stmt.setString(i, covrSurData.getRider());//IJTI-1485
			}
			//IJTI-306 START
			rs = stmt.executeQuery();

			Covrpf covrSur = null;
			while(rs.next()) {
				covrSur = new Covrpf();
				covrSur.setChdrcoy(rs.getString("CHDRCOY"));
				covrSur.setChdrnum(rs.getString("CHDRNUM"));
				covrSur.setLife(rs.getString("LIFE"));
				covrSur.setCoverage(rs.getString("COVERAGE"));
				covrSur.setRider(rs.getString("RIDER"));
				covrSur.setPlanSuffix(rs.getInt("PLNSFX"));
				covrSur.setValidflag(rs.getString("VALIDFLAG"));
				covrSur.setTranno(rs.getInt("TRANNO"));
				covrSur.setCurrfrom(rs.getInt("CURRFROM"));
				covrSur.setCurrto(rs.getInt("CURRTO"));
				covrSur.setCrtable(rs.getString("CRTABLE"));
				covrSur.setJlife(rs.getString("JLIFE"));
				covrSur.setStatcode(rs.getString("STATCODE"));
				covrSur.setStatreasn(rs.getString("STATREASN"));
				covrSur.setAnbAtCcd(rs.getInt("ANBCCD"));
				covrSur.setSex(rs.getString("SEX"));
				covrSur.setPremCurrency(rs.getString("PRMCUR"));
				covrSur.setTermid(rs.getString("TERMID"));
				//covrSur.setRiskCessDate(rs.getInt("RCESDTE"));
				covrSur.setCrrcd(rs.getInt("CRRCD"));
				covrSur.setInstprem(rs.getBigDecimal("INSTPREM"));
				covrSur.setSingp(rs.getBigDecimal("SINGP"));
				
				//covrSur.setConvertInitialUnits(rs.getInt(13));
				covrSur.setPstatcode(rs.getString("PSTATCODE"));
				
				data.add(covrSur);
			}
		} catch (SQLException e) {
			LOGGER.error("selectCovrSurr()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt,rs);
			if((System.currentTimeMillis() - ts)>=5000){
				LOGGER.info("SQL Call took longer than 5000 msec");
			}
		}
		return data;
	}
    
    public void updateCovrBulk(List<Covrpf> covrPFList){
    	if(!covrPFList.isEmpty()){
    		StringBuilder updateSql = new StringBuilder();
    		updateSql.append(" UPDATE VM1DTA.COVRPF SET VALIDFLAG=? , CURRTO=? WHERE UNIQUE_NUMBER=? ");
    		PreparedStatement ps = getPrepareStatement(updateSql.toString());
			 try {
				 for (Covrpf covrpf : covrPFList) {
					ps.setString(1, covrpf.getValidflag());
					ps.setInt(2, covrpf.getCurrto());
					ps.setLong(3, covrpf.getUniqueNumber());
					ps.addBatch();
				}
				 ps.executeBatch(); 
			 
		}catch (SQLException e) {
			LOGGER.error("updateCovrBulk()", e); /* IJTI-1479 */
	           throw new SQLRuntimeException(e);
	       } finally {
	           close(ps, null);
	       
			}
	
    		
    	}
    }
    
    public void insertCovrBulk(List<Covrpf> covrPFList){
    	if(!covrPFList.isEmpty()){
    		StringBuilder insertSql = new StringBuilder();
    		insertSql.append(" INSERT INTO VM1DTA.COVRPF (CHDRCOY, CHDRNUM, CBCVIN, COVERAGE, CRRCD, CRTABLE, CURRFROM, INSTPREM, JLIFE, LIFE, PLNSFX, PCESTRM, PRMCUR, PSTATCODE, RRTDAT, RIDER, RCESTRM, SINGP,");
    		insertSql.append(" STATCODE, SUMINS, VALIDFLAG, ZLINSTPREM,");
    		insertSql.append(" TRANNO, CURRTO, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS,");
    		insertSql.append(" REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T,");
    		insertSql.append(" LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE,"); 
    		insertSql.append(" CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR, CBRVPR,");
    		insertSql.append(" CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, USRPRF, JOBNM, DATIME, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ ,RISKPREM, ZSTPDUTY01, ZCLSTATE, REINSTATED, TPDTYPE, PRORATEPREM, COMMPREM ) ");
    		insertSql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    		PreparedStatement ps = getPrepareStatement(insertSql.toString());
    		
			 try {
				 for (Covrpf covrpf : covrPFList) {
					 ps.setString(1,covrpf.getChdrcoy());
					 ps.setString(2,covrpf.getChdrnum());
					 ps.setInt(3,covrpf.getConvertInitialUnits());
					 ps.setString(4,covrpf.getCoverage());
					 ps.setInt(5,covrpf.getCrrcd());
					 ps.setString(6,covrpf.getCrtable());
					 ps.setInt(7,covrpf.getCurrfrom());
					 ps.setBigDecimal(8,covrpf.getInstprem());
					 ps.setString(9,covrpf.getJlife());
					 ps.setString(10,covrpf.getLife());
					 ps.setInt(11,covrpf.getPlanSuffix());
					 ps.setInt(12,covrpf.getPremCessTerm());
					 ps.setString(13,covrpf.getPremCurrency());
					 ps.setString(14,covrpf.getPstatcode());
					 ps.setInt(15,covrpf.getRerateDate());
					 ps.setString(16,covrpf.getRider());
					 ps.setInt(17,covrpf.getRiskCessTerm());
					 ps.setBigDecimal(18,covrpf.getSingp());
					 ps.setString(19,covrpf.getStatcode());
					 ps.setBigDecimal(20,covrpf.getSumins());
					 ps.setString(21,covrpf.getValidflag());
					 ps.setBigDecimal(22,covrpf.getZlinstprem());
					 ps.setInt(23,covrpf.getTranno());
					 ps.setInt(24,covrpf.getCurrto());
					 ps.setString(25,covrpf.getStatFund());
					 ps.setString(26,covrpf.getStatSect());
					 ps.setString(27,covrpf.getStatSubsect());
					 ps.setInt(28,covrpf.getRiskCessDate());
					 ps.setInt(29,covrpf.getPremCessDate());
					 ps.setInt(30,covrpf.getBenCessDate());
					 ps.setInt(31,covrpf.getNextActDate());
					 ps.setInt(32,covrpf.getRiskCessAge());
					 ps.setInt(33,covrpf.getPremCessAge());
					 ps.setInt(34,covrpf.getBenCessAge());
					 ps.setInt(35,covrpf.getBenCessTerm());
					 ps.setString(36,covrpf.getSicurr());
					 ps.setBigDecimal(37,covrpf.getVarSumInsured());
					 ps.setString(38,covrpf.getMortcls());
					 ps.setString(39,covrpf.getReptcd01());
					 ps.setString(40,covrpf.getReptcd02());
					 ps.setString(41,covrpf.getReptcd03());
					 ps.setString(42,covrpf.getReptcd04());
					 ps.setString(43,covrpf.getReptcd05());
					 ps.setString(44,covrpf.getReptcd06());
					 ps.setBigDecimal(45,covrpf.getCrInstamt01());
					 ps.setBigDecimal(46,covrpf.getCrInstamt02());
					 ps.setBigDecimal(47,covrpf.getCrInstamt03());
					 ps.setBigDecimal(48,covrpf.getCrInstamt04());
					 ps.setBigDecimal(49,covrpf.getCrInstamt05());
					 ps.setString(50,covrpf.getTermid());
					 ps.setInt(51,covrpf.getTransactionDate());
					 ps.setInt(52,covrpf.getTransactionTime());
					 ps.setInt(53,covrpf.getUser());
					 ps.setString(54,covrpf.getLiencd());
					 ps.setString(55,covrpf.getRatingClass());
					 ps.setString(56,covrpf.getIndexationInd());
					 ps.setString(57,covrpf.getBonusInd());
					 ps.setString(58,covrpf.getDeferPerdCode());
					 ps.setBigDecimal(59,covrpf.getDeferPerdAmt());
					 ps.setString(60,covrpf.getDeferPerdInd());
					 ps.setBigDecimal(61,covrpf.getTotMthlyBenefit());
					 ps.setBigDecimal(62,covrpf.getEstMatValue01());
					 ps.setBigDecimal(63,covrpf.getEstMatValue02());
					 ps.setInt(64,covrpf.getEstMatDate01());
					 ps.setInt(65,covrpf.getEstMatDate02());
					 ps.setBigDecimal(66,covrpf.getEstMatInt01());
					 ps.setBigDecimal(67,covrpf.getEstMatInt02());
					 ps.setString(68,covrpf.getCampaign());
					 ps.setBigDecimal(69,covrpf.getStatSumins());
					 ps.setInt(70,covrpf.getRtrnyrs());
					 ps.setString(71,covrpf.getReserveUnitsInd());
					 ps.setInt(72,covrpf.getReserveUnitsDate());
					 ps.setString(73,covrpf.getChargeOptionsInd());
					 ps.setString(74,covrpf.getFundSplitPlan());
					 ps.setString(75,covrpf.getStatreasn());
					 ps.setInt(76,covrpf.getAnbAtCcd());
					 ps.setString(77,covrpf.getSex());
					 ps.setInt(78,covrpf.getPremCessAgeMth());
					 ps.setInt(79,covrpf.getPremCessAgeDay());
					 ps.setInt(80,covrpf.getPremCessTermMth());
					 ps.setInt(81,covrpf.getPremCessTermDay());
					 ps.setInt(82,covrpf.getRiskCessAgeMth());
					 ps.setInt(83,covrpf.getRiskCessAgeDay());
					 ps.setInt(84,covrpf.getRiskCessTermMth());
					 ps.setInt(85,covrpf.getRiskCessTermDay());
					 ps.setString(86,covrpf.getJlLsInd());
					 ps.setInt(87,covrpf.getRerateFromDate());
					 ps.setInt(88,covrpf.getBenBillDate());
					 ps.setInt(89,covrpf.getAnnivProcDate());
					 ps.setInt(90,covrpf.getReviewProcessing());
					 ps.setInt(91,covrpf.getUnitStatementDate());
					 ps.setInt(92,covrpf.getCpiDate());
					 ps.setInt(93,covrpf.getInitUnitCancDate());
					 ps.setInt(94,covrpf.getExtraAllocDate());
					 ps.setInt(95,covrpf.getInitUnitIncrsDate());
					 ps.setBigDecimal(96,covrpf.getCoverageDebt());
					 ps.setInt(97,covrpf.getPayrseqno());
					 ps.setString(98,covrpf.getBappmeth());
					 ps.setBigDecimal(99,covrpf.getZbinstprem());
					 ps.setString(100, getUsrprf());
					 ps.setString(101, getJobnm());
					 ps.setTimestamp(102, new Timestamp(System.currentTimeMillis()));
					 ps.setBigDecimal(103,covrpf.getLoadper());
					 ps.setBigDecimal(104,covrpf.getRateadj());
					 ps.setBigDecimal(105,covrpf.getFltmort());
					 ps.setBigDecimal(106,covrpf.getPremadj());
					 ps.setBigDecimal(107,covrpf.getAgeadj());
					 if(null !=covrpf.getRiskprem()) {
					 ps.setBigDecimal(108,covrpf.getRiskprem()); //ILIFE-7845
					  }else{
						  ps.setBigDecimal(108,BigDecimal.ZERO);
					  }
					  ps.setBigDecimal(109, covrpf.getZstpduty01() == null ? BigDecimal.ZERO : covrpf.getZstpduty01());
					  ps.setString(110, covrpf.getZclstate());
					  ps.setString(111, covrpf.getReinstated());//ILIFE-8509
					  ps.setString(112, covrpf.getTpdtype());
					  ps.setBigDecimal(113, covrpf.getProrateprem() == null ? BigDecimal.ZERO : covrpf.getProrateprem());
					  ps.setBigDecimal(114, covrpf.getCommprem() == null ? BigDecimal.ZERO : covrpf.getCommprem());
					ps.addBatch();
				}
				 ps.executeBatch(); 
			 
		}catch (SQLException e) {
				LOGGER.error("Insert Covrpf()", e); /* IJTI-1479 */
	           throw new SQLRuntimeException(e);
	       } finally {
	           close(ps, null);
	       
			}
	
    		
    	}
    }
    //ILIFE-3310 by liwei
    public List<Covrpf> getCovrcovData(String chdrcoy,String chdrnum,String life,String crtable){
		Covrpf covrpf = null;
		PreparedStatement psCovrSelect = null;
		ResultSet sqlcovrpf1rs = null;
		StringBuilder sqlCovrSelectBuilder = new StringBuilder();
		List<Covrpf> list = new ArrayList<Covrpf>();
		sqlCovrSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		sqlCovrSelectBuilder.append(" JOBNM, UNIQUE_NUMBER ");
		sqlCovrSelectBuilder.append(" FROM COVRCOV WHERE CHDRCOY = ? AND CHDRNUM = ? and life=? and crtable=? ");
		sqlCovrSelectBuilder.append(" ORDER BY ");
		sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
		
		try {
			psCovrSelect.setString(1, chdrcoy);
			psCovrSelect.setString(2, chdrnum);
			psCovrSelect.setString(3, life);
			psCovrSelect.setString(4, crtable);
			sqlcovrpf1rs = executeQuery(psCovrSelect);
			while (sqlcovrpf1rs.next()) {
				covrpf = new Covrpf();
				covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
				covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
				covrpf.setLife(sqlcovrpf1rs.getString(3));
				covrpf.setCoverage(sqlcovrpf1rs.getString(4));
				covrpf.setRider(sqlcovrpf1rs.getString(5));
				covrpf.setCrtable(sqlcovrpf1rs.getString(6));
				covrpf.setUserProfile(sqlcovrpf1rs.getString(7));
				covrpf.setJobName(sqlcovrpf1rs.getString(8));
				covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(9));
				list.add(covrpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrRecordForContract()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrSelect, sqlcovrpf1rs);
		}
		return list;
	}
    public Covrpf getCovrridData(String chdrcoy,String chdrnum,String life,String coverage,String crtable){
    		Covrpf covrpf = null;
    		PreparedStatement psCovrSelect = null;
    		ResultSet sqlcovrpf1rs = null;
    		StringBuilder sqlCovrSelectBuilder = new StringBuilder();

    		sqlCovrSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
    		sqlCovrSelectBuilder.append(" JOBNM, UNIQUE_NUMBER ");
    		sqlCovrSelectBuilder.append(" FROM COVRRID WHERE CHDRCOY = ? AND CHDRNUM = ? and life=? and coverage=? and crtable=? ");
    		sqlCovrSelectBuilder.append(" ORDER BY ");
    		sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
    		psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
    		
    		try {
    			psCovrSelect.setString(1, chdrcoy);
    			psCovrSelect.setString(2, chdrnum);
    			psCovrSelect.setString(3, life);
    			psCovrSelect.setString(4, coverage);
    			psCovrSelect.setString(5, crtable);
    			sqlcovrpf1rs = executeQuery(psCovrSelect);
    			while (sqlcovrpf1rs.next()) {
    				covrpf = new Covrpf();
    				covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
    				covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
    				covrpf.setLife(sqlcovrpf1rs.getString(3));
    				covrpf.setCoverage(sqlcovrpf1rs.getString(4));
    				covrpf.setRider(sqlcovrpf1rs.getString(5));
    				covrpf.setCrtable(sqlcovrpf1rs.getString(6));
    				covrpf.setUserProfile(sqlcovrpf1rs.getString(7));
    				covrpf.setJobName(sqlcovrpf1rs.getString(8));
    				covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(9));
    			}

    		} catch (SQLException e) {
    			LOGGER.error("searchCovrRecordForContract()", e); /* IJTI-1479 */
    			throw new SQLRuntimeException(e);
    		} finally {
    			close(psCovrSelect, sqlcovrpf1rs);
    		}
    		return covrpf;
    	}
    
		public List<Covrpf> selectCoverage(Covrpf covrpfData) {
				
				// ---------------------------------
				// Initialize variables
				// ---------------------------------
				Covrpf covrpf = null;
				List<Covrpf> data = new LinkedList<Covrpf>();
				PreparedStatement psCovrSelect = null;
				ResultSet sqlcovrpf1rs = null;
				StringBuilder sqlCovrSelectBuilder = new StringBuilder();
				
				// ---------------------------------
				// Construct Query
				// ---------------------------------
				sqlCovrSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, COVERAGE, CRTABLE,");
				sqlCovrSelectBuilder.append(" JLIFE, LIFE, PLNSFX, PSTATCODE,");
				sqlCovrSelectBuilder.append(" RIDER, STATCODE, SUMINS, ");
				sqlCovrSelectBuilder.append(" VALIDFLAG ");
				sqlCovrSelectBuilder.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ?  AND VALIDFLAG = '1' ");
				sqlCovrSelectBuilder.append(" ORDER BY ");
				sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC");
				
				psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
				
				try {
					// ---------------------------------
					// Set Parameters dynamically
					// ---------------------------------
					psCovrSelect.setString(1, covrpfData.getChdrcoy().trim());
					psCovrSelect.setString(2, covrpfData.getChdrnum());
					psCovrSelect.setString(3, covrpfData.getLife());
					
					
					
					// ---------------------------------
					// Execute Query
					// ---------------------------------
					sqlcovrpf1rs = executeQuery(psCovrSelect);


					while (sqlcovrpf1rs.next()) {
						
						covrpf = new Covrpf();
						
						covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
						covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
						covrpf.setCoverage(sqlcovrpf1rs.getString(3));
						covrpf.setCrtable(sqlcovrpf1rs.getString(4));
						covrpf.setJlife(sqlcovrpf1rs.getString(5));
						covrpf.setLife(sqlcovrpf1rs.getString(6));
						covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(7));
						covrpf.setPstatcode(sqlcovrpf1rs.getString(8));
						covrpf.setRider(sqlcovrpf1rs.getString(9));
						covrpf.setStatcode(sqlcovrpf1rs.getString(10));
						data.add(covrpf);
						
					   }

				} catch (SQLException e) {
					LOGGER.error("selectCovrClMData()", e); /* IJTI-1479 */
					throw new SQLRuntimeException(e);
				} finally {
					close(psCovrSelect,sqlcovrpf1rs);
					
					}
				return data;
			}
		public List<Covrpf> selectCovrClmData(Covrpf covrClmData) {
				
			
			
			
			List<Covrpf> data = new LinkedList<Covrpf>();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sql.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PRMCUR, JLIFE, CRTABLE,RCESDTE, CRRCD,");
			sql.append("INSTPREM, SINGP, CBCVIN, PSTATCODE ");
			sql.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND VALIDFLAG = '1' ");
			sql.append(" ORDER BY ");
			sql.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
			
			stmt = getPrepareStatement(sql.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmt.setString(1, covrClmData.getChdrcoy().trim());
				stmt.setString(2, covrClmData.getChdrnum());
				stmt.setString(3, covrClmData.getLife());
				stmt.setString(4, covrClmData.getCoverage());
				stmt.setString(5, covrClmData.getRider());
				
				
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				rs = executeQuery(stmt);
				Covrpf covrClm = null;
				while(rs.next()) {
					covrClm = new Covrpf();
					covrClm.setChdrcoy(rs.getString(1));
					covrClm.setChdrnum(rs.getString(2));
					covrClm.setLife(rs.getString(3));
					covrClm.setCoverage(rs.getString(4));
					covrClm.setRider(rs.getString(5));
					covrClm.setPremCurrency(rs.getString(6));
					covrClm.setJlife(rs.getString(7));
					covrClm.setCrtable(rs.getString(8));
					covrClm.setRiskCessDate(rs.getInt(9));
					covrClm.setCrrcd(rs.getInt(10));
					covrClm.setInstprem(rs.getBigDecimal(11));
					covrClm.setSingp(rs.getBigDecimal(12));
					covrClm.setConvertInitialUnits(rs.getInt(13));
					covrClm.setPstatcode(rs.getString(14));
					
					data.add(covrClm);
				}
			} catch (SQLException e) {
				LOGGER.error("selectCovrClMData()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(stmt,rs);
				
				}
			return data;
			}

		//IJS-59
		public List<Covrpf> selectCovrClm(Covrpf covrClm) {
				
			// ---------------------------------
			// Initialize variables
			// ---------------------------------
			List<Covrpf> data = new LinkedList();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sql.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PRMCUR, JLIFE, CRTABLE,RCESDTE, CRRCD,");
			sql.append("INSTPREM, SINGP, CBCVIN, PSTATCODE ");
			sql.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = '1' ");
			sql.append(" ORDER BY ");
			sql.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
			
			stmt = getPrepareStatement(sql.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmt.setString(1, covrClm.getChdrcoy().trim());
				stmt.setString(2, covrClm.getChdrnum());
			
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				rs = executeQuery(stmt);
				Covrpf covr = null;
				while(rs.next()) {
					covr = new Covrpf();
					covr.setChdrcoy(rs.getString(1));
					covr.setChdrnum(rs.getString(2));
					covr.setLife(rs.getString(3));
					covr.setCoverage(rs.getString(4));
					covr.setRider(rs.getString(5));
					covr.setPremCurrency(rs.getString(6));
					covr.setJlife(rs.getString(7));
					covr.setCrtable(rs.getString(8));
					covr.setRiskCessDate(rs.getInt(9));
					covr.setCrrcd(rs.getInt(10));
					covr.setInstprem(rs.getBigDecimal(11));
					covr.setSingp(rs.getBigDecimal(12));
					covr.setConvertInitialUnits(rs.getInt(13));
					covr.setPstatcode(rs.getString(14));
					
					data.add(covr);
				}
			} catch (SQLException e) {
				LOGGER.error("selectCovrClm()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(stmt,rs);
				
				}
			return data;
			}
			
		//IJS-59
		
		public List<Covrpf> readCovrRecord(Covrpf covrpfModel) {
			 
			List<Covrpf> covrList = new ArrayList<Covrpf>();
			Covrpf covrpf = new Covrpf();
			StringBuilder sql = new StringBuilder("SELECT CHDRCOY, CHDRNUM, INSTPREM, CRTABLE, ZLINSTPREM, CRRCD, STATCODE, PSTATCODE FROM ");
			sql.append(" COVRENQ ");
			sql.append(" WHERE  CHDRCOY=? AND CHDRNUM=? AND PLNSFX=? AND VALIDFLAG='1' ");
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = getPrepareStatement(sql.toString());
				
				ps.setString(1, covrpfModel.getChdrcoy().trim());
				ps.setString(2, covrpfModel.getChdrnum());
				ps.setInt(3, covrpfModel.getPlanSuffix());
				rs = ps.executeQuery();
				while(rs.next()) {
					covrpf = new Covrpf();
					covrpf.setChdrcoy(rs.getString(1));
					covrpf.setChdrnum(rs.getString(2));
					covrpf.setInstprem(rs.getBigDecimal(3));
					covrpf.setCrtable(rs.getString(4));
					covrpf.setZlinstprem(rs.getBigDecimal(5));
					covrpf.setCrrcd(rs.getInt(6));
					covrpf.setStatcode(rs.getString(7));
					covrpf.setPstatcode(rs.getString(8));
					covrList.add(covrpf);
					
				}
			}
			catch (SQLException e) {
				LOGGER.error("getCovrLifeenqRecords()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return covrList;
		}

		public List<Covrpf> getCovrLifeenqRecords(String chdrcoy, String chdrnum, String lifeflag, String validflag) {
			List<Covrpf> covrList = new ArrayList<Covrpf>();
			Covrpf covrpf;
			StringBuilder sql = new StringBuilder("SELECT DISTINCT COVR.* FROM ");
			sql.append(" LIFEPF LIFE INNER JOIN COVRPF COVR ON LIFE.CHDRCOY=COVR.CHDRCOY AND LIFE.CHDRNUM=COVR.CHDRNUM AND LIFE.LIFE=COVR.LIFE");
			sql.append(" WHERE LIFE.VALIDFLAG=? AND COVR.VALIDFLAG=? AND LIFE.CHDRNUM=? AND LIFE.CHDRCOY=?");
			sql.append(" ORDER BY COVR.CHDRCOY ASC, COVR.CHDRNUM ASC, COVR.LIFE ASC, PLNSFX ASC, COVERAGE ASC, RIDER ASC, COVR.UNIQUE_NUMBER DESC");	//ILIFE-6197
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = getPrepareStatement(sql.toString());
				ps.setString(1, lifeflag);
				ps.setString(2, validflag);
				ps.setString(3, chdrnum);
				ps.setString(4, chdrcoy);
				rs = ps.executeQuery();
				while(rs.next()) {
					covrpf = new Covrpf();
					covrpf.setUniqueNumber(rs.getLong(1));
					covrpf.setChdrcoy(rs.getString(2));
					covrpf.setChdrnum(rs.getString(3));
					covrpf.setLife(rs.getString(4));
					covrpf.setJlife(rs.getString(5));
					covrpf.setCoverage(rs.getString(6));
					covrpf.setRider(rs.getString(7));
					covrpf.setPlanSuffix(rs.getInt(8));
					covrpf.setValidflag(rs.getString(9));
					covrpf.setTranno(rs.getInt(10));
					covrpf.setCurrfrom(rs.getInt(11));
					covrpf.setCurrto(rs.getInt(12));
					covrpf.setStatcode(rs.getString(13));
					covrpf.setPstatcode(rs.getString(14));
					covrpf.setStatreasn(rs.getString(15));
					covrpf.setCrrcd(rs.getInt(16));
					covrpf.setAnbAtCcd(rs.getInt(17));
					covrpf.setSex(rs.getString(18));
					covrpf.setReptcd01(rs.getString(19));
					covrpf.setReptcd02(rs.getString(20));
					covrpf.setReptcd03(rs.getString(21));
					covrpf.setReptcd04(rs.getString(22));
					covrpf.setReptcd05(rs.getString(23));
					covrpf.setReptcd06(rs.getString(24));
					covrpf.setCrInstamt01(BigDecimal.valueOf(rs.getDouble(25)));
					covrpf.setCrInstamt02(BigDecimal.valueOf(rs.getDouble(26)));
					covrpf.setCrInstamt03(BigDecimal.valueOf(rs.getDouble(27)));
					covrpf.setCrInstamt04(BigDecimal.valueOf(rs.getDouble(28)));
					covrpf.setCrInstamt05(BigDecimal.valueOf(rs.getDouble(29)));
					covrpf.setPremCurrency(rs.getString(30));
					covrpf.setTermid(rs.getString(31));
					covrpf.setTransactionDate(rs.getInt(32));
					covrpf.setTransactionTime(rs.getInt(33));
					covrpf.setUser(rs.getInt(34));
					covrpf.setStatFund(rs.getString(35));
					covrpf.setStatSect(rs.getString(36));
					covrpf.setStatSubsect(rs.getString(37));
					covrpf.setCrtable(rs.getString(38));
					covrpf.setRiskCessDate(rs.getInt(39));
					covrpf.setPremCessDate(rs.getInt(40));
					covrpf.setBenCessDate(rs.getInt(41));
					covrpf.setNextActDate(rs.getInt(42));
					covrpf.setRiskCessAge(rs.getInt(43));
					covrpf.setPremCessAge(rs.getInt(44));
					covrpf.setBenCessAge(rs.getInt(45));
					covrpf.setRiskCessTerm(rs.getInt(46));
					covrpf.setPremCessTerm(rs.getInt(47));
					covrpf.setBenCessTerm(rs.getInt(48));
					covrpf.setSumins(BigDecimal.valueOf(rs.getDouble(49)));
					covrpf.setSicurr(rs.getString(50));
					covrpf.setVarSumInsured(BigDecimal.valueOf(rs.getDouble(51)));
					covrpf.setMortcls(rs.getString(52));
					covrpf.setLiencd(rs.getString(53));
					covrpf.setRatingClass(rs.getString(54));
					covrpf.setIndexationInd(rs.getString(55));
					covrpf.setBonusInd(rs.getString(56));
					covrpf.setDeferPerdCode(rs.getString(57));
					covrpf.setDeferPerdAmt(BigDecimal.valueOf(rs.getDouble(58)));
					covrpf.setDeferPerdInd(rs.getString(59));
					covrpf.setTotMthlyBenefit(BigDecimal.valueOf(rs.getDouble(60)));
					covrpf.setEstMatValue01(BigDecimal.valueOf(rs.getDouble(61)));
					covrpf.setEstMatValue02(BigDecimal.valueOf(rs.getDouble(62)));
					covrpf.setEstMatDate01(rs.getInt(63));
					covrpf.setEstMatDate02(rs.getInt(64));
					covrpf.setEstMatInt01(BigDecimal.valueOf(rs.getDouble(65)));
					covrpf.setEstMatInt02(BigDecimal.valueOf(rs.getDouble(66)));
					covrpf.setCampaign(rs.getString(67));
					covrpf.setStatSumins(BigDecimal.valueOf(rs.getDouble(68)));
					covrpf.setRtrnyrs(rs.getInt(69));
					covrpf.setReserveUnitsInd(rs.getString(70));
					covrpf.setReserveUnitsDate(rs.getInt(71));
					covrpf.setChargeOptionsInd(rs.getString(72));
					covrpf.setFundSplitPlan(rs.getString(73));
					covrpf.setPremCessAgeMth(rs.getInt(74));
					covrpf.setPremCessAgeDay(rs.getInt(75));
					covrpf.setPremCessTermMth(rs.getInt(76));
					covrpf.setPremCessTermDay(rs.getInt(77));
					covrpf.setRiskCessAgeMth(rs.getInt(78));
					covrpf.setRiskCessAgeDay(rs.getInt(79));
					covrpf.setRiskCessTermMth(rs.getInt(80));
					covrpf.setRiskCessTermDay(rs.getInt(81));
					covrpf.setJlLsInd(rs.getString(82));
					covrpf.setInstprem(BigDecimal.valueOf(rs.getDouble(83)));
					covrpf.setSingp(BigDecimal.valueOf(rs.getDouble(84)));
					covrpf.setRerateDate(rs.getInt(85));
					covrpf.setRerateFromDate(rs.getInt(86));
					covrpf.setBenBillDate(rs.getInt(87));
					/*covrpf.setCbanpr(rs.getInt(88));
					covrpf.setCbcvin(rs.getInt(89));
					covrpf.setCbrvpr(rs.getString(90));
					covrpf.setCbunst(rs.getString(91));*/
					covrpf.setCpiDate(rs.getInt(92));
					covrpf.setInitUnitCancDate(rs.getInt(93));
					covrpf.setExtraAllocDate(rs.getInt(94));
					covrpf.setInitUnitIncrsDate(rs.getInt(95));
					covrpf.setCoverageDebt(BigDecimal.valueOf(rs.getDouble(96)));
					covrpf.setPayrseqno(rs.getInt(97));
					covrpf.setBappmeth(rs.getString(98));
					covrpf.setZbinstprem(BigDecimal.valueOf(rs.getDouble(99)));
					covrpf.setZlinstprem(BigDecimal.valueOf(rs.getDouble(100)));
					covrpf.setUserProfile(rs.getString(101));
					covrpf.setJobName(rs.getString(102));
					covrpf.setDatime(rs.getString(103));
					covrpf.setLoadper(BigDecimal.valueOf(rs.getDouble(104)));
					covrpf.setRateadj(BigDecimal.valueOf(rs.getDouble(105)));
					covrpf.setFltmort(BigDecimal.valueOf(rs.getDouble(106)));
					covrpf.setPremadj(BigDecimal.valueOf(rs.getDouble(107)));
					covrpf.setAgeadj(BigDecimal.valueOf(rs.getDouble(108)));
					covrpf.setZstpduty01(rs.getBigDecimal(109));
					covrList.add(covrpf);
				}
			}
			catch (SQLException e) {
				LOGGER.error("getCovrLifeenqRecords()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return covrList;
		}

		public Covrpf getCovrRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plansuffix, String validflag) {
			
			Covrpf covrpf = new Covrpf();
			StringBuilder sql = new StringBuilder("SELECT * FROM ");
			sql.append(" COVRPF ");
			sql.append(" WHERE VALIDFLAG=? AND CHDRNUM=? AND CHDRCOY=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?");
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = getPrepareStatement(sql.toString());
				ps.setString(1, validflag);
//ILIFE-8187 start
			//	ps.setString(2, chdrnum);
				ps.setString(2, StringUtil.fillSpace(chdrnum.trim(), DD.chdrnum.length));
			//	ps.setString(3, chdrcoy);
				ps.setString(3, StringUtil.fillSpace(chdrcoy.trim(), DD.chdrcoy.length));
			//	ps.setString(4, life);
				ps.setString(4, StringUtil.fillSpace(life.trim(), DD.life.length));
			//	ps.setString(5, coverage);
				ps.setString(5, StringUtil.fillSpace(coverage.trim(), DD.coverage.length));
			//	ps.setString(6, rider);
				ps.setString(6, StringUtil.fillSpace(rider.trim(), DD.rider.length));
//ILIFE-8187 end
				ps.setInt(7, plansuffix);
				rs = ps.executeQuery();
				while(rs.next()) {
					covrpf.setUniqueNumber(rs.getLong(1));
					covrpf.setChdrcoy(rs.getString(2));
					covrpf.setChdrnum(rs.getString(3));
					covrpf.setLife(rs.getString(4));
					covrpf.setJlife(rs.getString(5));
					covrpf.setCoverage(rs.getString(6));
					covrpf.setRider(rs.getString(7));
					covrpf.setPlanSuffix(rs.getInt(8));
					covrpf.setValidflag(rs.getString(9));
					covrpf.setTranno(rs.getInt(10));
					covrpf.setCurrfrom(rs.getInt(11));
					covrpf.setCurrto(rs.getInt(12));
					covrpf.setStatcode(rs.getString(13));
					covrpf.setPstatcode(rs.getString(14));
					covrpf.setStatreasn(rs.getString(15));
					covrpf.setCrrcd(rs.getInt(16));
					covrpf.setAnbAtCcd(rs.getInt(17));
					covrpf.setSex(rs.getString(18));
					covrpf.setReptcd01(rs.getString(19));
					covrpf.setReptcd02(rs.getString(20));
					covrpf.setReptcd03(rs.getString(21));
					covrpf.setReptcd04(rs.getString(22));
					covrpf.setReptcd05(rs.getString(23));
					covrpf.setReptcd06(rs.getString(24));
					covrpf.setCrInstamt01(BigDecimal.valueOf(rs.getDouble(25)));
					covrpf.setCrInstamt02(BigDecimal.valueOf(rs.getDouble(26)));
					covrpf.setCrInstamt03(BigDecimal.valueOf(rs.getDouble(27)));
					covrpf.setCrInstamt04(BigDecimal.valueOf(rs.getDouble(28)));
					covrpf.setCrInstamt05(BigDecimal.valueOf(rs.getDouble(29)));
					covrpf.setPremCurrency(rs.getString(30));
					covrpf.setTermid(rs.getString(31));
					covrpf.setTransactionDate(rs.getInt(32));
					covrpf.setTransactionTime(rs.getInt(33));
					covrpf.setUser(rs.getInt(34));
					covrpf.setStatFund(rs.getString(35));
					covrpf.setStatSect(rs.getString(36));
					covrpf.setStatSubsect(rs.getString(37));
					covrpf.setCrtable(rs.getString(38));
					covrpf.setRiskCessDate(rs.getInt(39));
					covrpf.setPremCessDate(rs.getInt(40));
					covrpf.setBenCessDate(rs.getInt(41));
					covrpf.setNextActDate(rs.getInt(42));
					covrpf.setRiskCessAge(rs.getInt(43));
					covrpf.setPremCessAge(rs.getInt(44));
					covrpf.setBenCessAge(rs.getInt(45));
					covrpf.setRiskCessTerm(rs.getInt(46));
					covrpf.setPremCessTerm(rs.getInt(47));
					covrpf.setBenCessTerm(rs.getInt(48));
					covrpf.setSumins(BigDecimal.valueOf(rs.getDouble(49)));
					covrpf.setSicurr(rs.getString(50));
					covrpf.setVarSumInsured(BigDecimal.valueOf(rs.getDouble(51)));
					covrpf.setMortcls(rs.getString(52));
					covrpf.setLiencd(rs.getString(53));
					covrpf.setRatingClass(rs.getString(54));
					covrpf.setIndexationInd(rs.getString(55));
					covrpf.setBonusInd(rs.getString(56));
					covrpf.setDeferPerdCode(rs.getString(57));
					covrpf.setDeferPerdAmt(BigDecimal.valueOf(rs.getDouble(58)));
					covrpf.setDeferPerdInd(rs.getString(59));
					covrpf.setTotMthlyBenefit(BigDecimal.valueOf(rs.getDouble(60)));
					covrpf.setEstMatValue01(BigDecimal.valueOf(rs.getDouble(61)));
					covrpf.setEstMatValue02(BigDecimal.valueOf(rs.getDouble(62)));
					covrpf.setEstMatDate01(rs.getInt(63));
					covrpf.setEstMatDate02(rs.getInt(64));
					covrpf.setEstMatInt01(BigDecimal.valueOf(rs.getDouble(65)));
					covrpf.setEstMatInt02(BigDecimal.valueOf(rs.getDouble(66)));
					covrpf.setCampaign(rs.getString(67));
					covrpf.setStatSumins(BigDecimal.valueOf(rs.getDouble(68)));
					covrpf.setRtrnyrs(rs.getInt(69));
					covrpf.setReserveUnitsInd(rs.getString(70));
					covrpf.setReserveUnitsDate(rs.getInt(71));
					covrpf.setChargeOptionsInd(rs.getString(72));
					covrpf.setFundSplitPlan(rs.getString(73));
					covrpf.setPremCessAgeMth(rs.getInt(74));
					covrpf.setPremCessAgeDay(rs.getInt(75));
					covrpf.setPremCessTermMth(rs.getInt(76));
					covrpf.setPremCessTermDay(rs.getInt(77));
					covrpf.setRiskCessAgeMth(rs.getInt(78));
					covrpf.setRiskCessAgeDay(rs.getInt(79));
					covrpf.setRiskCessTermMth(rs.getInt(80));
					covrpf.setRiskCessTermDay(rs.getInt(81));
					covrpf.setJlLsInd(rs.getString(82));
					covrpf.setInstprem(BigDecimal.valueOf(rs.getDouble(83)));
					covrpf.setSingp(BigDecimal.valueOf(rs.getDouble(84)));
					covrpf.setRerateDate(rs.getInt(85));
					covrpf.setRerateFromDate(rs.getInt(86));
					covrpf.setBenBillDate(rs.getInt(87));
					/*covrpf.setCbanpr(rs.getInt(88));
					covrpf.setCbcvin(rs.getInt(89));
					covrpf.setCbrvpr(rs.getString(90));
					covrpf.setCbunst(rs.getString(91));*/
					covrpf.setCpiDate(rs.getInt(92));
					covrpf.setInitUnitCancDate(rs.getInt(93));
					covrpf.setExtraAllocDate(rs.getInt(94));
					covrpf.setInitUnitIncrsDate(rs.getInt(95));
					covrpf.setCoverageDebt(BigDecimal.valueOf(rs.getDouble(96)));
					covrpf.setPayrseqno(rs.getInt(97));
					covrpf.setBappmeth(rs.getString(98));
					covrpf.setZbinstprem(BigDecimal.valueOf(rs.getDouble(99)));
					covrpf.setZlinstprem(BigDecimal.valueOf(rs.getDouble(100)));
					covrpf.setUserProfile(rs.getString(101));
					covrpf.setJobName(rs.getString(102));
					covrpf.setDatime(rs.getString(103));
					covrpf.setLoadper(BigDecimal.valueOf(rs.getDouble(104)));
					covrpf.setRateadj(BigDecimal.valueOf(rs.getDouble(105)));
					covrpf.setFltmort(BigDecimal.valueOf(rs.getDouble(106)));
					covrpf.setPremadj(BigDecimal.valueOf(rs.getDouble(107)));
					covrpf.setAgeadj(BigDecimal.valueOf(rs.getDouble(108)));
					covrpf.setZstpduty01(BigDecimal.valueOf(rs.getDouble(109)));	//ILIFE-7746

					/*ILIFE-6968 start*/
					covrpf.setLnkgno(rs.getString(114));
					covrpf.setLnkgsubrefno(rs.getString(115));
					/*ILIFE-6968 end*/
					covrpf.setTpdtype(rs.getString(116)); //ILIFE-7118

					// ILIFE-6534
					covrpf.setAnnivProcDate(rs.getInt("Cbanpr"));
					covrpf.setLnkgind(rs.getString("LNKGIND"));
					covrpf.setSingpremtype(rs.getString("SINGPREMTYPE"));//ILIFE-7805
				}
			}
			catch (SQLException e) {
				LOGGER.error("getCovrLifeenqRecords()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return covrpf;
		}


		public List<Covrpf> readCovrsurData(Covrpf covrpfModel){
			
			StringBuilder sqlCovrSelect1 = new StringBuilder("SELECT  CHDRCOY, CHDRNUM, PLNSFX, PSTATCODE ");
			sqlCovrSelect1.append(" FROM COVRSUR  ");
			sqlCovrSelect1.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND PLNSFX =?");
			sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, PLNSFX ASC  ");
			

			PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
			
			
			ResultSet sqlcovrpf1rs = null;
			List<Covrpf> covrpfList = new ArrayList<Covrpf>();
		  
			try {
				psCovrSelect.setString(1,covrpfModel.getChdrcoy());
				psCovrSelect.setString(2,covrpfModel.getChdrnum());
				psCovrSelect.setInt(3,covrpfModel.getPlanSuffix());
				sqlcovrpf1rs = psCovrSelect.executeQuery();
				while (sqlcovrpf1rs.next()) {
				   Covrpf covrpf = new Covrpf();
				   covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
				   covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
				   covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(3));
				   covrpf.setPstatcode(sqlcovrpf1rs.getString(4));
				   covrpfList.add(covrpf);
				   }

			} catch (SQLException e) {
				LOGGER.error("readData()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrSelect, sqlcovrpf1rs);
			}
			return covrpfList;

		} 

		public Covrpf readCovrenqData(Covrpf covrpfModel){
			
			StringBuilder sqlCovrSelect1 = new StringBuilder("SELECT  CHDRCOY, CHDRNUM, LIFE, PLNSFX, COVERAGE, RIDER, CRTABLE, PSTATCODE, SUMINS, LIENCD, SINGP, INSTPREM, ");
			sqlCovrSelect1.append(" ZLINSTPREM, ZBINSTPREM, LOADPER, RATEADJ, FLTMORT, PREMADJ, ANBCCD, ");
			sqlCovrSelect1.append("  CRRCD,  CRDEBT, PCESDTE, RCESDTE, STFUND, STSECT, STSSECT,RCESAGE,PCESAGE,RCESTRM,PCESTRM,MORTCLS,CBANPR,RRTDAT,RRTFRM,CBUNST,BBLDAT,CRDEBT");
			sqlCovrSelect1.append(" FROM COVRENQ  ");
			sqlCovrSelect1.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND  LIFE = ? AND PLNSFX = ? AND COVERAGE = ? AND RIDER= ?  ");
			sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, PLNSFX ASC, COVERAGE ASC, RIDER ASC  ");
			

			PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
			
			
			ResultSet sqlcovrpf1rs = null;
			Covrpf covrpf=null;
			try {
				 psCovrSelect.setString(1,covrpfModel.getChdrcoy());
				 psCovrSelect.setString(2,covrpfModel.getChdrnum());
				 psCovrSelect.setString(3,covrpfModel.getLife());
				 psCovrSelect.setInt(4,covrpfModel.getPlanSuffix());
				 psCovrSelect.setString(5,covrpfModel.getCoverage());
				 psCovrSelect.setString(6,covrpfModel.getRider());
				 sqlcovrpf1rs = psCovrSelect.executeQuery();
				 
				 while(sqlcovrpf1rs.next()){
				   covrpf = new Covrpf();
				   covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
				   covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
				   covrpf.setLife(sqlcovrpf1rs.getString(3));
				   covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(4));
				   covrpf.setCoverage(sqlcovrpf1rs.getString(5));
				   covrpf.setRider(sqlcovrpf1rs.getString(6));
				   covrpf.setCrtable(sqlcovrpf1rs.getString(7));
				   covrpf.setPstatcode(sqlcovrpf1rs.getString(8));
				   covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(9));
				   covrpf.setLiencd(sqlcovrpf1rs.getString(10));
				   covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(11));
				   covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(12));
				   covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(13));
				   covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(14));
				   covrpf.setLoadper(sqlcovrpf1rs.getBigDecimal(15));
				   covrpf.setRateadj(sqlcovrpf1rs.getBigDecimal(16));
				   covrpf.setFltmort(sqlcovrpf1rs.getBigDecimal(17));
				   covrpf.setPremadj(sqlcovrpf1rs.getBigDecimal(18));
				   covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(19));
				   covrpf.setCrrcd(sqlcovrpf1rs.getInt(20));
				   covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(21));
				   covrpf.setPremCessDate(sqlcovrpf1rs.getInt(22));
				   covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(23));
				   covrpf.setStatFund(sqlcovrpf1rs.getString(24));
				   covrpf.setStatSect(sqlcovrpf1rs.getString(25));
				   covrpf.setStatSubsect(sqlcovrpf1rs.getString(26));
				   /*ILIFE-4036 starts*/
				   covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(27));
				   covrpf.setPremCessAge(sqlcovrpf1rs.getInt(28));
				   covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(29));
				   covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(30));
				   covrpf.setMortcls(sqlcovrpf1rs.getString(31));
				   covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(32));
				   covrpf.setRerateDate(sqlcovrpf1rs.getInt(33));
				   covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(34));
				   covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(35));
				   covrpf.setBenBillDate(sqlcovrpf1rs.getInt(36));
				   covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(37));
				   /*ILIFE-4036 ends*/
				 }

			} catch (SQLException e) {
				LOGGER.error("readCovrenqData()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrSelect, sqlcovrpf1rs);
			}
			return covrpf;

		}

		public List<String> searchCovrCrtable(String coy, String chdrnum) {

			StringBuilder sqlCovrSelect1 = new StringBuilder();

			sqlCovrSelect1.append("SELECT CRTABLE FROM COVRPF ");
			sqlCovrSelect1.append(" WHERE CHDRCOY=? AND CHDRNUM=? and VALIDFLAG != '2' ");
			//sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX DESC, UNIQUE_NUMBER DESC ");

			PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
			ResultSet sqlcovrpf1rs = null;
			List<String> covrCrtableResult = new LinkedList<String>();
			try {
				psCovrSelect.setString(1, coy);
				psCovrSelect.setString(2, chdrnum);
				sqlcovrpf1rs = executeQuery(psCovrSelect);

				while (sqlcovrpf1rs.next()) {
					covrCrtableResult.add(sqlcovrpf1rs.getString(1));
				}

			} catch (SQLException e) {
			LOGGER.error("searchCovrCrtable()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrSelect, sqlcovrpf1rs);
			}
			return covrCrtableResult;
		}
		/*ILIFE-3149 starts*/
		
		public List<Covrpf> searchCovrpfRecord(Covrpf covrpf) throws SQLRuntimeException{

			StringBuilder sqlSelect = new StringBuilder();

			sqlSelect.append("SELECT UNIQUE_NUMBER, ");
			sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
			sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM, ");
			sqlSelect.append("CURRTO, STATCODE, PSTATCODE, STATREASN, CRRCD, ");
			sqlSelect.append("ANBCCD, SEX, REPTCD01, REPTCD02, REPTCD03, ");
			sqlSelect.append("REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, ");
			sqlSelect.append("CRINST03, CRINST04, CRINST05, PRMCUR, TERMID, ");
			sqlSelect.append("TRDT, TRTM, USER_T, STFUND, STSECT, ");
			sqlSelect.append("STSSECT, CRTABLE, RCESDTE, PCESDTE, BCESDTE, ");
			sqlSelect.append("NXTDTE, RCESAGE, PCESAGE, BCESAGE, RCESTRM, ");
			sqlSelect.append("PCESTRM, BCESTRM, SUMINS, SICURR, VARSI, ");
			sqlSelect.append("MORTCLS, LIENCD, RATCLS, INDXIN, BNUSIN, ");
			sqlSelect.append("DPCD, DPAMT, DPIND, TMBEN, EMV01, ");
			sqlSelect.append("EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, ");
			sqlSelect.append("CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, ");
			sqlSelect.append("CHGOPT, FUNDSP, PCAMTH, PCADAY, PCTMTH, ");
			sqlSelect.append("PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, ");
			sqlSelect.append("JLLSID, INSTPREM, SINGP, RRTDAT, RRTFRM, ");
			sqlSelect.append("BBLDAT, CBANPR, CBCVIN, CBRVPR, CBUNST, ");
			sqlSelect.append("CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, ");
			sqlSelect.append("PAYRSEQNO, BAPPMETH, ZBINSTPREM, ZLINSTPREM, USRPRF, ");
			sqlSelect.append("JOBNM, DATIME, LOADPER, RATEADJ, FLTMORT, ");
			sqlSelect.append("PREMADJ, AGEADJ ");
			sqlSelect.append("FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
			sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");


			PreparedStatement psCovrpfSelect = getPrepareStatement(sqlSelect.toString());
			ResultSet sqlCovrpfRs = null;
			List<Covrpf> outputList = new ArrayList<Covrpf>();

			try {

				psCovrpfSelect.setString(1, covrpf.getChdrcoy().trim());
				psCovrpfSelect.setString(2, covrpf.getChdrnum());
				psCovrpfSelect.setString(3, covrpf.getCoverage());
				psCovrpfSelect.setString(4, covrpf.getRider());
				psCovrpfSelect.setInt(5, covrpf.getPlanSuffix());

				sqlCovrpfRs = executeQuery(psCovrpfSelect);

				while (sqlCovrpfRs.next()) {

					covrpf = new Covrpf();

					covrpf.setUniqueNumber(sqlCovrpfRs.getInt("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(sqlCovrpfRs.getString("CHDRCOY"));
					covrpf.setChdrnum(sqlCovrpfRs.getString("CHDRNUM"));
					covrpf.setLife(sqlCovrpfRs.getString("LIFE"));
					covrpf.setJlife(sqlCovrpfRs.getString("JLIFE"));
					covrpf.setCoverage(sqlCovrpfRs.getString("COVERAGE"));
					covrpf.setRider(sqlCovrpfRs.getString("RIDER"));
					covrpf.setPlanSuffix(sqlCovrpfRs.getInt("PLNSFX"));
					covrpf.setValidflag(sqlCovrpfRs.getString("VALIDFLAG"));
					covrpf.setTranno(sqlCovrpfRs.getInt("TRANNO"));
					covrpf.setCurrfrom(sqlCovrpfRs.getInt("CURRFROM"));
					covrpf.setCurrto(sqlCovrpfRs.getInt("CURRTO"));
					covrpf.setStatcode(sqlCovrpfRs.getString("STATCODE"));
					covrpf.setPstatcode(sqlCovrpfRs.getString("PSTATCODE"));
					covrpf.setStatreasn(sqlCovrpfRs.getString("STATREASN"));
					covrpf.setCrrcd(sqlCovrpfRs.getInt("CRRCD"));
					covrpf.setAnbAtCcd(sqlCovrpfRs.getInt("ANBCCD"));
					covrpf.setSex(sqlCovrpfRs.getString("SEX"));
					covrpf.setReptcd01(sqlCovrpfRs.getString("REPTCD01"));
					covrpf.setReptcd02(sqlCovrpfRs.getString("REPTCD02"));
					covrpf.setReptcd03(sqlCovrpfRs.getString("REPTCD03"));
					covrpf.setReptcd04(sqlCovrpfRs.getString("REPTCD04"));
					covrpf.setReptcd05(sqlCovrpfRs.getString("REPTCD05"));
					covrpf.setReptcd06(sqlCovrpfRs.getString("REPTCD06"));
					covrpf.setCrInstamt01(sqlCovrpfRs.getBigDecimal("CRINST01"));
					covrpf.setCrInstamt02(sqlCovrpfRs.getBigDecimal("CRINST02"));
					covrpf.setCrInstamt03(sqlCovrpfRs.getBigDecimal("CRINST03"));
					covrpf.setCrInstamt04(sqlCovrpfRs.getBigDecimal("CRINST04"));
					covrpf.setCrInstamt05(sqlCovrpfRs.getBigDecimal("CRINST05"));
					covrpf.setPremCurrency(sqlCovrpfRs.getString("PRMCUR"));
					covrpf.setTermid(sqlCovrpfRs.getString("TERMID"));
					covrpf.setTransactionDate(sqlCovrpfRs.getInt("TRDT"));
					covrpf.setTransactionTime(sqlCovrpfRs.getInt("TRTM"));
					covrpf.setUser(sqlCovrpfRs.getInt("USER_T"));
					covrpf.setStatFund(sqlCovrpfRs.getString("STFUND"));
					covrpf.setStatSect(sqlCovrpfRs.getString("STSECT"));
					covrpf.setStatSubsect(sqlCovrpfRs.getString("STSSECT"));
					covrpf.setCrtable(sqlCovrpfRs.getString("CRTABLE"));
					covrpf.setRiskCessDate(sqlCovrpfRs.getInt("RCESDTE"));
					covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
					covrpf.setPremCessDate(sqlCovrpfRs.getInt("PCESDTE"));
					covrpf.setBenCessDate(sqlCovrpfRs.getInt("BCESDTE"));
					covrpf.setNextActDate(sqlCovrpfRs.getInt("NXTDTE"));
					covrpf.setRiskCessAge(sqlCovrpfRs.getInt("RCESAGE"));
					covrpf.setPremCessAge(sqlCovrpfRs.getInt("PCESAGE"));
					covrpf.setBenCessAge(sqlCovrpfRs.getInt("BCESAGE"));
					covrpf.setRiskCessTerm(sqlCovrpfRs.getInt("RCESTRM"));
					covrpf.setPremCessTerm(sqlCovrpfRs.getInt("PCESTRM"));
					covrpf.setBenCessTerm(sqlCovrpfRs.getInt("BCESTRM"));
					covrpf.setSumins(sqlCovrpfRs.getBigDecimal("SUMINS"));
					covrpf.setSicurr(sqlCovrpfRs.getString("SICURR"));
					covrpf.setVarSumInsured(sqlCovrpfRs.getBigDecimal("VARSI"));
					covrpf.setMortcls(sqlCovrpfRs.getString("MORTCLS"));
					covrpf.setLiencd(sqlCovrpfRs.getString("LIENCD"));
					covrpf.setRatingClass(sqlCovrpfRs.getString("RATCLS"));
					covrpf.setIndexationInd(sqlCovrpfRs.getString("INDXIN"));
					covrpf.setBonusInd(sqlCovrpfRs.getString("BNUSIN"));
					covrpf.setDeferPerdCode(sqlCovrpfRs.getString("DPCD"));
					covrpf.setDeferPerdAmt(sqlCovrpfRs.getBigDecimal("DPAMT"));
					covrpf.setDeferPerdInd(sqlCovrpfRs.getString("DPIND"));
					covrpf.setTotMthlyBenefit(sqlCovrpfRs.getBigDecimal("TMBEN"));
					covrpf.setSingp(sqlCovrpfRs.getBigDecimal("SINGP"));
					covrpf.setInstprem(sqlCovrpfRs.getBigDecimal("INSTPREM"));
					covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
					covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
					covrpf.setRerateDate(sqlCovrpfRs.getInt("RRTDAT"));
					covrpf.setRerateFromDate(sqlCovrpfRs.getInt("RRTFRM"));
					covrpf.setBenBillDate(sqlCovrpfRs.getInt("BBLDAT"));
					covrpf.setEstMatValue01(sqlCovrpfRs.getBigDecimal("EMV01"));
					covrpf.setEstMatValue02(sqlCovrpfRs.getBigDecimal("EMV02"));
					covrpf.setEstMatDate01(sqlCovrpfRs.getInt("EMVDTE01"));
					covrpf.setEstMatDate02(sqlCovrpfRs.getInt("EMVDTE02"));
					covrpf.setEstMatInt01(sqlCovrpfRs.getBigDecimal("EMVINT01"));
					covrpf.setEstMatInt02(sqlCovrpfRs.getBigDecimal("EMVINT02"));
					covrpf.setCampaign(sqlCovrpfRs.getString("CAMPAIGN"));
					covrpf.setStatSumins(sqlCovrpfRs.getBigDecimal("STSMIN"));
					covrpf.setRtrnyrs(sqlCovrpfRs.getInt("RTRNYRS"));
					covrpf.setPremCessAgeMth(sqlCovrpfRs.getInt("PCAMTH"));
					covrpf.setPremCessAgeDay(sqlCovrpfRs.getInt("PCADAY"));
					covrpf.setPremCessTermMth(sqlCovrpfRs.getInt("PCTMTH"));
					covrpf.setPremCessTermDay(sqlCovrpfRs.getInt("PCTDAY"));
					covrpf.setRiskCessAgeMth(sqlCovrpfRs.getInt("RCAMTH"));
					covrpf.setRiskCessAgeDay(sqlCovrpfRs.getInt("RCADAY"));
					covrpf.setRiskCessTermMth(sqlCovrpfRs.getInt("RCTMTH"));
					covrpf.setRiskCessTermDay(sqlCovrpfRs.getInt("RCTDAY"));
					covrpf.setJlLsInd(sqlCovrpfRs.getString("JLLSID"));
					covrpf.setCoverageDebt(sqlCovrpfRs.getBigDecimal("CRDEBT"));
					covrpf.setReserveUnitsInd(sqlCovrpfRs.getString("RSUNIN"));
					covrpf.setReserveUnitsDate(sqlCovrpfRs.getInt("RUNDTE"));
					covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
					covrpf.setCpiDate(sqlCovrpfRs.getInt("CPIDTE"));
					covrpf.setBappmeth(sqlCovrpfRs.getString("BAPPMETH"));
					covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
					covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
					covrpf.setUserProfile(sqlCovrpfRs.getString("USRPRF"));
					covrpf.setJobName(sqlCovrpfRs.getString("JOBNM"));
					covrpf.setDatime(sqlCovrpfRs.getString("DATIME"));
					covrpf.setLoadper(sqlCovrpfRs.getBigDecimal("LOADPER"));
					covrpf.setRateadj(sqlCovrpfRs.getBigDecimal("RATEADJ"));
					covrpf.setFltmort(sqlCovrpfRs.getBigDecimal("FLTMORT"));
					covrpf.setPremadj(sqlCovrpfRs.getBigDecimal("PREMADJ"));
					covrpf.setAgeadj(sqlCovrpfRs.getBigDecimal("AGEADJ"));

					outputList.add(covrpf);
				}

			} catch (SQLException e) {
				LOGGER.error("searchCovrpfRecord()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrpfSelect, sqlCovrpfRs);
			}

			return outputList;
		}		
		public List<Covrpf> searchCovrrnwRecord(String chdrcoy,String chdrnum, String drypRunDate){


			StringBuilder sqlSelect = new StringBuilder();

			sqlSelect.append("SELECT UNIQUE_NUMBER, ");
			sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
			sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM, ");
			sqlSelect.append("CURRTO, STATCODE, PSTATCODE, STATREASN, CRRCD, ");
			sqlSelect.append("ANBCCD, SEX, REPTCD01, REPTCD02, REPTCD03, ");
			sqlSelect.append("REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, ");
			sqlSelect.append("CRINST03, CRINST04, CRINST05, PRMCUR, TERMID, ");
			sqlSelect.append("TRDT, TRTM, USER_T, STFUND, STSECT, ");
			sqlSelect.append("STSSECT, CRTABLE, RCESDTE, PCESDTE, BCESDTE, ");
			sqlSelect.append("NXTDTE, RCESAGE, PCESAGE, BCESAGE, RCESTRM, ");
			sqlSelect.append("PCESTRM, BCESTRM, SUMINS, SICURR, VARSI, ");
			sqlSelect.append("MORTCLS, LIENCD, RATCLS, INDXIN, BNUSIN, ");
			sqlSelect.append("DPCD, DPAMT, DPIND, TMBEN, EMV01, ");
			sqlSelect.append("EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, ");
			sqlSelect.append("CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, ");
			sqlSelect.append("CHGOPT, FUNDSP, PCAMTH, PCADAY, PCTMTH, ");
			sqlSelect.append("PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, ");
			sqlSelect.append("JLLSID, INSTPREM, SINGP, RRTDAT, RRTFRM, ");
			sqlSelect.append("BBLDAT, CBANPR, CBCVIN, CBRVPR, CBUNST, ");
			sqlSelect.append("CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, ");
			sqlSelect.append("PAYRSEQNO, BAPPMETH, ZBINSTPREM, ZLINSTPREM, USRPRF, ");
			sqlSelect.append("JOBNM, DATIME, LOADPER, RATEADJ, FLTMORT, ");
			sqlSelect.append("PREMADJ, AGEADJ ");
			sqlSelect.append("FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
			sqlSelect.append("AND CBANPR <= ? ");
			sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, CBANPR ASC, UNIQUE_NUMBER DESC");


			PreparedStatement psCovrpfSelect = getPrepareStatement(sqlSelect.toString());
			ResultSet sqlCovrpfRs = null;
			List<Covrpf> outputList = new ArrayList<Covrpf>();

			try {

				psCovrpfSelect.setString(1, chdrcoy);
				psCovrpfSelect.setString(2, chdrnum);
				psCovrpfSelect.setString(3, drypRunDate);
				
				sqlCovrpfRs = executeQuery(psCovrpfSelect);

				while (sqlCovrpfRs.next()) {
					Covrpf covrpf = new Covrpf();
					covrpf.setUniqueNumber(sqlCovrpfRs.getInt("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(sqlCovrpfRs.getString("CHDRCOY"));
					covrpf.setChdrnum(sqlCovrpfRs.getString("CHDRNUM"));
					covrpf.setLife(sqlCovrpfRs.getString("LIFE"));
					covrpf.setJlife(sqlCovrpfRs.getString("JLIFE"));
					covrpf.setCoverage(sqlCovrpfRs.getString("COVERAGE"));
					covrpf.setRider(sqlCovrpfRs.getString("RIDER"));
					covrpf.setPlanSuffix(sqlCovrpfRs.getInt("PLNSFX"));
					covrpf.setValidflag(sqlCovrpfRs.getString("VALIDFLAG"));
					covrpf.setTranno(sqlCovrpfRs.getInt("TRANNO"));
					covrpf.setCurrfrom(sqlCovrpfRs.getInt("CURRFROM"));
					covrpf.setCurrto(sqlCovrpfRs.getInt("CURRTO"));
					covrpf.setStatcode(sqlCovrpfRs.getString("STATCODE"));
					covrpf.setPstatcode(sqlCovrpfRs.getString("PSTATCODE"));
					covrpf.setStatreasn(sqlCovrpfRs.getString("STATREASN"));
					covrpf.setCrrcd(sqlCovrpfRs.getInt("CRRCD"));
					covrpf.setAnbAtCcd(sqlCovrpfRs.getInt("ANBCCD"));
					covrpf.setSex(sqlCovrpfRs.getString("SEX"));
					covrpf.setReptcd01(sqlCovrpfRs.getString("REPTCD01"));
					covrpf.setReptcd02(sqlCovrpfRs.getString("REPTCD02"));
					covrpf.setReptcd03(sqlCovrpfRs.getString("REPTCD03"));
					covrpf.setReptcd04(sqlCovrpfRs.getString("REPTCD04"));
					covrpf.setReptcd05(sqlCovrpfRs.getString("REPTCD05"));
					covrpf.setReptcd06(sqlCovrpfRs.getString("REPTCD06"));
					covrpf.setCrInstamt01(sqlCovrpfRs.getBigDecimal("CRINST01"));
					covrpf.setCrInstamt02(sqlCovrpfRs.getBigDecimal("CRINST02"));
					covrpf.setCrInstamt03(sqlCovrpfRs.getBigDecimal("CRINST03"));
					covrpf.setCrInstamt04(sqlCovrpfRs.getBigDecimal("CRINST04"));
					covrpf.setCrInstamt05(sqlCovrpfRs.getBigDecimal("CRINST05"));
					covrpf.setPremCurrency(sqlCovrpfRs.getString("PRMCUR"));
					covrpf.setTermid(sqlCovrpfRs.getString("TERMID"));
					covrpf.setTransactionDate(sqlCovrpfRs.getInt("TRDT"));
					covrpf.setTransactionTime(sqlCovrpfRs.getInt("TRTM"));
					covrpf.setUser(sqlCovrpfRs.getInt("USER_T"));
					covrpf.setStatFund(sqlCovrpfRs.getString("STFUND"));
					covrpf.setStatSect(sqlCovrpfRs.getString("STSECT"));
					covrpf.setStatSubsect(sqlCovrpfRs.getString("STSSECT"));
					covrpf.setCrtable(sqlCovrpfRs.getString("CRTABLE"));
					covrpf.setRiskCessDate(sqlCovrpfRs.getInt("RCESDTE"));
					covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
					covrpf.setPremCessDate(sqlCovrpfRs.getInt("PCESDTE"));
					covrpf.setBenCessDate(sqlCovrpfRs.getInt("BCESDTE"));
					covrpf.setNextActDate(sqlCovrpfRs.getInt("NXTDTE"));
					covrpf.setRiskCessAge(sqlCovrpfRs.getInt("RCESAGE"));
					covrpf.setPremCessAge(sqlCovrpfRs.getInt("PCESAGE"));
					covrpf.setBenCessAge(sqlCovrpfRs.getInt("BCESAGE"));
					covrpf.setRiskCessTerm(sqlCovrpfRs.getInt("RCESTRM"));
					covrpf.setPremCessTerm(sqlCovrpfRs.getInt("PCESTRM"));
					covrpf.setBenCessTerm(sqlCovrpfRs.getInt("BCESTRM"));
					covrpf.setSumins(sqlCovrpfRs.getBigDecimal("SUMINS"));
					covrpf.setSicurr(sqlCovrpfRs.getString("SICURR"));
					covrpf.setVarSumInsured(sqlCovrpfRs.getBigDecimal("VARSI"));
					covrpf.setMortcls(sqlCovrpfRs.getString("MORTCLS"));
					covrpf.setLiencd(sqlCovrpfRs.getString("LIENCD"));
					covrpf.setRatingClass(sqlCovrpfRs.getString("RATCLS"));
					covrpf.setIndexationInd(sqlCovrpfRs.getString("INDXIN"));
					covrpf.setBonusInd(sqlCovrpfRs.getString("BNUSIN"));
					covrpf.setDeferPerdCode(sqlCovrpfRs.getString("DPCD"));
					covrpf.setDeferPerdAmt(sqlCovrpfRs.getBigDecimal("DPAMT"));
					covrpf.setDeferPerdInd(sqlCovrpfRs.getString("DPIND"));
					covrpf.setTotMthlyBenefit(sqlCovrpfRs.getBigDecimal("TMBEN"));
					covrpf.setSingp(sqlCovrpfRs.getBigDecimal("SINGP"));
					covrpf.setInstprem(sqlCovrpfRs.getBigDecimal("INSTPREM"));
					covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
					covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
					covrpf.setRerateDate(sqlCovrpfRs.getInt("RRTDAT"));
					covrpf.setRerateFromDate(sqlCovrpfRs.getInt("RRTFRM"));
					covrpf.setBenBillDate(sqlCovrpfRs.getInt("BBLDAT"));
					covrpf.setAnnivProcDate(sqlCovrpfRs.getInt("CBANPR"));
					covrpf.setEstMatValue01(sqlCovrpfRs.getBigDecimal("EMV01"));
					covrpf.setEstMatValue02(sqlCovrpfRs.getBigDecimal("EMV02"));
					covrpf.setEstMatDate01(sqlCovrpfRs.getInt("EMVDTE01"));
					covrpf.setEstMatDate02(sqlCovrpfRs.getInt("EMVDTE02"));
					covrpf.setEstMatInt01(sqlCovrpfRs.getBigDecimal("EMVINT01"));
					covrpf.setEstMatInt02(sqlCovrpfRs.getBigDecimal("EMVINT02"));
					covrpf.setCampaign(sqlCovrpfRs.getString("CAMPAIGN"));
					covrpf.setStatSumins(sqlCovrpfRs.getBigDecimal("STSMIN"));
					covrpf.setRtrnyrs(sqlCovrpfRs.getInt("RTRNYRS"));
					covrpf.setPremCessAgeMth(sqlCovrpfRs.getInt("PCAMTH"));
					covrpf.setPremCessAgeDay(sqlCovrpfRs.getInt("PCADAY"));
					covrpf.setPremCessTermMth(sqlCovrpfRs.getInt("PCTMTH"));
					covrpf.setPremCessTermDay(sqlCovrpfRs.getInt("PCTDAY"));
					covrpf.setRiskCessAgeMth(sqlCovrpfRs.getInt("RCAMTH"));
					covrpf.setRiskCessAgeDay(sqlCovrpfRs.getInt("RCADAY"));
					covrpf.setRiskCessTermMth(sqlCovrpfRs.getInt("RCTMTH"));
					covrpf.setRiskCessTermDay(sqlCovrpfRs.getInt("RCTDAY"));
					covrpf.setJlLsInd(sqlCovrpfRs.getString("JLLSID"));
					covrpf.setCoverageDebt(sqlCovrpfRs.getBigDecimal("CRDEBT"));
					covrpf.setReserveUnitsInd(sqlCovrpfRs.getString("RSUNIN"));
					covrpf.setReserveUnitsDate(sqlCovrpfRs.getInt("RUNDTE"));
					covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
					covrpf.setCpiDate(sqlCovrpfRs.getInt("CPIDTE"));
					covrpf.setBappmeth(sqlCovrpfRs.getString("BAPPMETH"));
					covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
					covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
					covrpf.setUserProfile(sqlCovrpfRs.getString("USRPRF"));
					covrpf.setJobName(sqlCovrpfRs.getString("JOBNM"));
					covrpf.setDatime(sqlCovrpfRs.getString("DATIME"));
					covrpf.setLoadper(sqlCovrpfRs.getBigDecimal("LOADPER"));
					covrpf.setRateadj(sqlCovrpfRs.getBigDecimal("RATEADJ"));
					covrpf.setFltmort(sqlCovrpfRs.getBigDecimal("FLTMORT"));
					covrpf.setPremadj(sqlCovrpfRs.getBigDecimal("PREMADJ"));
					covrpf.setAgeadj(sqlCovrpfRs.getBigDecimal("AGEADJ"));

					outputList.add(covrpf);
				}

			} catch (SQLException e) {
				LOGGER.error("searchCovrrnwRecord()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrpfSelect, sqlCovrpfRs);
			}
			return outputList;
		}

		// ILIFE-3792 by yy
		public int updateCovrProcDate(List<Covrpf> uniqueList) {
			if (!uniqueList.isEmpty()) {
				StringBuilder updateSql = new StringBuilder();
				updateSql.append(" UPDATE COVRPF SET CBANPR=? WHERE UNIQUE_NUMBER=? ");
				PreparedStatement ps = getPrepareStatement(updateSql.toString());
				try {
					for (Covrpf covrpf : uniqueList) {
						ps.setInt(1, covrpf.getAnnivProcDate());
						ps.setLong(2, covrpf.getUniqueNumber());
						ps.addBatch();
					}
					ps.executeBatch();
					return uniqueList.size();
				} catch (SQLException e) {
				LOGGER.error("updateCovrProcDate()", e); /* IJTI-1479 */
					throw new SQLRuntimeException(e);
				} finally {
					close(ps, null);
				}
			}
			return 0;
		}

		@Override
		public List<Covrpf> getCovrbonRecord(Covrpf covrpfModel) {
			
			StringBuilder sqlCovrSelect1 = new StringBuilder();

			sqlCovrSelect1.append("SELECT CHDRCOY, CHDRNUM, STATCODE, PSTATCODE, BNUSIN, CBUNST FROM COVRBON ");
			sqlCovrSelect1.append(" WHERE CHDRCOY=? AND CHDRNUM=? ");
			sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

			PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
			ResultSet sqlcovrpf1rs = null;
			List<Covrpf> covrpfList = new LinkedList<Covrpf>();
			try {
				psCovrSelect.setString(1, covrpfModel.getChdrcoy());
				psCovrSelect.setString(2, covrpfModel.getChdrnum());
				sqlcovrpf1rs = executeQuery(psCovrSelect);
				
				while (sqlcovrpf1rs.next()) {
					   Covrpf covrpf = new Covrpf();
					   covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
					   covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
					   covrpf.setStatcode(sqlcovrpf1rs.getString(3));
					   covrpf.setPstatcode(sqlcovrpf1rs.getString(4));
					   covrpf.setBonusInd(sqlcovrpf1rs.getString(5));	
					   covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(6));
					   covrpfList.add(covrpf);
					  }
				
			} catch (SQLException e) {
			LOGGER.error("readData()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrSelect, sqlcovrpf1rs);
			}
			return covrpfList;

		} 	

		@Override
		public List<Covrpf> readCovrForBilling(String chdrCoy, String chdrNum) {
			
			StringBuilder sql = new StringBuilder("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG,");
			sql.append("TRANNO, CURRFROM, CURRTO, JLIFE, STATCODE, PSTATCODE, STATREASN, CRRCD, ANBCCD,");
			sql.append("SEX, REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, PRMCUR,");
			sql.append("STFUND, STSECT, STSSECT, CRTABLE, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE,");
			sql.append("RCESTRM, PCESTRM, BCESTRM, SUMINS, SICURR, VARSI, MORTCLS, LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, SINGP,");
			sql.append("INSTPREM, ZBINSTPREM, ZLINSTPREM, RRTDAT, RRTFRM, BBLDAT, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN,");
			sql.append("RTRNYRS, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID, CRDEBT, RSUNIN, RUNDTE, PAYRSEQNO, CPIDTE, BAPPMETH FROM COVRPF");
			sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG = '1' ");
			sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			List<Covrpf> covrpfList = new ArrayList<>();
			try{
				ps = getPrepareStatement(sql.toString());
				ps.setString(1, chdrCoy);
				ps.setString(2, chdrNum);
				rs = ps.executeQuery();
				
				while(rs.next()){
					Covrpf covrpf = new Covrpf();
					covrpf.setChdrcoy(rs.getString("CHDRCOY"));
					covrpf.setChdrnum(rs.getString("CHDRNUM"));
					covrpf.setLife(rs.getString("LIFE"));
					covrpf.setCoverage(rs.getString("COVERAGE"));
					covrpf.setRider(rs.getString("RIDER"));
					covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
					covrpf.setValidflag(rs.getString("VALIDFLAG"));
					covrpf.setTranno(rs.getInt("TRANNO"));
					covrpf.setCurrfrom(rs.getInt("CURRFROM"));
					covrpf.setCurrto(rs.getInt("CURRTO"));
					covrpf.setJlife(rs.getString("JLIFE"));
					covrpf.setStatcode(rs.getString("STATCODE"));
					covrpf.setPstatcode(rs.getString("PSTATCODE"));
					covrpf.setStatreasn(rs.getString("STATREASN"));
					covrpf.setCrrcd(rs.getInt("CRRCD"));
					covrpf.setAnbAtCcd(rs.getInt("ANBCCD"));
					covrpf.setSex(rs.getString("SEX"));
					covrpf.setReptcd01(rs.getString("REPTCD01"));
					covrpf.setReptcd02(rs.getString("REPTCD02"));
					covrpf.setReptcd03(rs.getString("REPTCD03"));
					covrpf.setReptcd04(rs.getString("REPTCD04"));
					covrpf.setReptcd05(rs.getString("REPTCD05"));
					covrpf.setReptcd06(rs.getString("REPTCD06"));
					covrpf.setCrInstamt01(rs.getBigDecimal("CRINST01"));
					covrpf.setCrInstamt02(rs.getBigDecimal("CRINST02"));
					covrpf.setCrInstamt03(rs.getBigDecimal("CRINST03"));
					covrpf.setCrInstamt04(rs.getBigDecimal("CRINST04"));
					covrpf.setCrInstamt05(rs.getBigDecimal("CRINST05"));
					covrpf.setPremCurrency(rs.getString("PRMCUR"));
					covrpf.setStatFund(rs.getString("STFUND"));
					covrpf.setStatSect(rs.getString("STSECT"));
					covrpf.setStatSubsect(rs.getString("STSSECT"));
					covrpf.setCrtable(rs.getString("CRTABLE"));
					covrpf.setRiskCessDate(rs.getInt("RCESDTE"));
					covrpf.setPremCessDate(rs.getInt("PCESDTE"));
					covrpf.setBenCessDate(rs.getInt("BCESDTE"));
					covrpf.setNextActDate(rs.getInt("NXTDTE"));
					covrpf.setRiskCessAge(rs.getInt("RCESAGE"));
					covrpf.setPremCessAge(rs.getInt("PCESAGE"));
					covrpf.setBenCessAge(rs.getInt("BCESAGE"));
					covrpf.setRiskCessTerm(rs.getInt("RCESTRM"));
					covrpf.setPremCessTerm(rs.getInt("PCESTRM"));
					covrpf.setBenCessTerm(rs.getInt("BCESTRM"));
					covrpf.setSumins(rs.getBigDecimal("SUMINS"));
					covrpf.setSicurr(rs.getString("SICURR"));
					covrpf.setVarSumInsured(rs.getBigDecimal("VARSI"));
					covrpf.setMortcls(rs.getString("MORTCLS"));
					covrpf.setLiencd(rs.getString("LIENCD"));
					covrpf.setRatingClass(rs.getString("RATCLS"));
					covrpf.setIndexationInd(rs.getString("INDXIN"));
					covrpf.setBonusInd(rs.getString("BNUSIN"));
					covrpf.setDeferPerdCode(rs.getString("DPCD"));
					covrpf.setDeferPerdAmt(rs.getBigDecimal("DPAMT"));
					covrpf.setDeferPerdInd(rs.getString("DPIND"));
					covrpf.setTotMthlyBenefit(rs.getBigDecimal("TMBEN"));
					covrpf.setSingp(rs.getBigDecimal("SINGP"));
					covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
					covrpf.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
					covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
					covrpf.setRerateDate(rs.getInt("RRTDAT"));
					covrpf.setRerateFromDate(rs.getInt("RRTFRM"));
					covrpf.setBenBillDate(rs.getInt("BBLDAT"));
					covrpf.setEstMatValue01(rs.getBigDecimal("EMV01"));
					covrpf.setEstMatValue02(rs.getBigDecimal("EMV02"));
					covrpf.setEstMatDate01(rs.getInt("EMVDTE01"));
					covrpf.setEstMatDate02(rs.getInt("EMVDTE02"));
					covrpf.setEstMatInt01(rs.getBigDecimal("EMVINT01"));
					covrpf.setEstMatInt02(rs.getBigDecimal("EMVINT02"));
					covrpf.setCampaign(rs.getString("CAMPAIGN"));
					covrpf.setStatSumins(rs.getBigDecimal("STSMIN"));
					covrpf.setRtrnyrs(rs.getInt("RTRNYRS"));
					covrpf.setPremCessAgeMth(rs.getInt("PCAMTH"));
					covrpf.setPremCessAgeDay(rs.getInt("PCADAY"));
					covrpf.setPremCessTermMth(rs.getInt("PCTMTH"));
					covrpf.setPremCessTermDay(rs.getInt("PCTDAY"));
					covrpf.setRiskCessAgeMth(rs.getInt("RCAMTH"));
					covrpf.setRiskCessAgeDay(rs.getInt("RCADAY"));
					covrpf.setRiskCessTermMth(rs.getInt("RCTMTH"));
					covrpf.setRiskCessTermDay(rs.getInt("RCTDAY"));
					covrpf.setJlLsInd(rs.getString("JLLSID"));
					covrpf.setCoverageDebt(rs.getBigDecimal("CRDEBT"));
					covrpf.setReserveUnitsInd(rs.getString("RSUNIN"));
					covrpf.setReserveUnitsDate(rs.getInt("RUNDTE"));
					covrpf.setPayrseqno(rs.getInt("PAYRSEQNO"));
					covrpf.setCpiDate(rs.getInt("CPIDTE"));
					covrpf.setBappmeth(rs.getString("BAPPMETH"));
					
					covrpfList.add(covrpf);
				}
			}catch (SQLException e) {
			LOGGER.error("readCovrForBilling()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			}
			finally
			{
				close(ps,rs);			
			}		
			
			return covrpfList;
		}     

         // ILIFE-4187
		 public Map<String,List<Covrpf>> searchCovrudlByChdrnum(List<String> chdrnumList){

		        StringBuilder sqlCovrSelect1 = new StringBuilder(
		                "SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,CURRFROM,CURRTO,VALIDFLAG,CRTABLE,CRDEBT,BBLDAT,STATCODE,PSTATCODE,CRRCD,UNIQUE_NUMBER ");
		        sqlCovrSelect1.append("FROM COVRUDL WHERE ");
		        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		        sqlCovrSelect1
		                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		        ResultSet sqlcovrpf1rs = null;
		        Map<String, List<Covrpf>> covrMap = new HashMap<>();
		        try {
		            sqlcovrpf1rs = executeQuery(psCovrSelect);
		            while (sqlcovrpf1rs.next()) {
		            	int i = 1;
		                Covrpf covrpf = new Covrpf();
		                covrpf.setChdrcoy(sqlcovrpf1rs.getString(i++));
		                covrpf.setChdrnum(sqlcovrpf1rs.getString(i++));
		                covrpf.setLife(sqlcovrpf1rs.getString(i++));
		                covrpf.setCoverage(sqlcovrpf1rs.getString(i++));
		                covrpf.setRider(sqlcovrpf1rs.getString(i++));
		                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(i++));
		                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(i++));
		                covrpf.setCurrto(sqlcovrpf1rs.getInt(i++));
		                covrpf.setValidflag(sqlcovrpf1rs.getString(i++));
		                covrpf.setCrtable(sqlcovrpf1rs.getString(i++));
		                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(i++));
		                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(i++));
		                covrpf.setStatcode(sqlcovrpf1rs.getString(i++));
		                covrpf.setPstatcode(sqlcovrpf1rs.getString(i++));
		                covrpf.setCrrcd(sqlcovrpf1rs.getInt(i++));
		                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(i++));
		               
		                if (covrMap.containsKey(covrpf.getChdrnum())) {
		                    covrMap.get(covrpf.getChdrnum()).add(covrpf);
		                } else {
		                    List<Covrpf> chdrList = new ArrayList<>();
		                    chdrList.add(covrpf);
		                    covrMap.put(covrpf.getChdrnum(), chdrList);
		                }
		            }

		        } catch (SQLException e) {
			LOGGER.error("searchCovrByChdrnum()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psCovrSelect, sqlcovrpf1rs);
		        }
		        return covrMap;
		 }        
		 
		 public void updateCovrDebtRecord(List<Covrpf> updateCovrpfDebtList){
			 String SQL_COVR_UPDATE = "UPDATE COVRUDL SET CRDEBT=CRDEBT+?,JOBNM=?,USRPRF=?,DATIME=?  WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ";
			 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
	      
	         try {
	        	 for(Covrpf c:updateCovrpfDebtList){
	                 psCovrUpdate.setBigDecimal(1, c.getCoverageDebt());
	                 psCovrUpdate.setString(2, getJobnm());
	                 psCovrUpdate.setString(3, getUsrprf());
	                 psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	                 psCovrUpdate.setString(5, c.getChdrcoy());
	                 psCovrUpdate.setString(6, c.getChdrnum());
	                 psCovrUpdate.setString(7, c.getLife());
	                 psCovrUpdate.setString(8, c.getCoverage());
	                 psCovrUpdate.setString(9, c.getRider());
	                 psCovrUpdate.setInt(10, c.getPlanSuffix());
	                 psCovrUpdate.addBatch();
	        	 }
	        	 psCovrUpdate.executeBatch();
	         } catch (SQLException e) {
			LOGGER.error("updateCovrDebtRecord()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(psCovrUpdate, null);
	         }
		 }
		 
		 public void updateCovrStatRecord(List<Covrpf> updateCovrpfDebtList){
			 String SQL_COVR_UPDATE = "UPDATE COVRUDL SET PSTATCODE=?,STATCODE=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ";
			 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
	      
	         try {
	        	 for(Covrpf c:updateCovrpfDebtList){
	        		 psCovrUpdate.setString(1, c.getPstatcode());
	                 psCovrUpdate.setString(2, c.getStatcode());
	                 psCovrUpdate.setString(3, getJobnm());
	                 psCovrUpdate.setString(4, getUsrprf());
	                 psCovrUpdate.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
	                 psCovrUpdate.setString(6, c.getChdrcoy());
	                 psCovrUpdate.setString(7, c.getChdrnum());
	                 psCovrUpdate.setString(8, c.getLife());
	                 psCovrUpdate.setString(9, c.getCoverage());
	                 psCovrUpdate.setString(10, c.getRider());
	                 psCovrUpdate.setInt(11, c.getPlanSuffix());
	                 psCovrUpdate.addBatch();
	        	 }
	        	 psCovrUpdate.executeBatch();
	         } catch (SQLException e) {
			LOGGER.error("updateCovrStatRecord()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(psCovrUpdate, null);
	         }
		 }
		 
		 public Map<String,List<Covrpf>> searchCovruddByChdrnum(List<String> chdrnumList){

		        StringBuilder sqlCovrSelect1 = new StringBuilder(
		                "SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,CURRFROM,CURRTO,VALIDFLAG,CRRCD,CRTABLE,CRDEBT,STATCODE,RCESDTE,ANBCCD ");
		        sqlCovrSelect1.append(" FROM COVRUDD  WHERE ");
		        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		        sqlCovrSelect1
		                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		        ResultSet sqlrs = null;
		        Map<String, List<Covrpf>> covrMap = new HashMap<>();
		        try {
		        	sqlrs = executeQuery(psCovrSelect);
		            while (sqlrs.next()) {
		            	int i = 1;
		                Covrpf c = new Covrpf();
		                c.setUniqueNumber(sqlrs.getLong("unique_number"));
		                c.setChdrcoy(sqlrs.getString("chdrcoy"));
		                c.setChdrnum(sqlrs.getString("chdrnum"));
		                c.setLife(sqlrs.getString("life"));
		                c.setCoverage(sqlrs.getString("coverage"));
		                c.setRider(sqlrs.getString("rider"));
		                c.setPlanSuffix(sqlrs.getInt("plnsfx"));
		                c.setValidflag(sqlrs.getString("validflag"));
		                c.setCurrfrom(sqlrs.getInt("currfrom"));
		                c.setCurrto(sqlrs.getInt("currto"));
		                c.setCrrcd(sqlrs.getInt("crrcd"));
		                c.setCrtable(sqlrs.getString("crtable"));
		                c.setCoverageDebt(sqlrs.getBigDecimal("crdebt"));
		                c.setStatcode(sqlrs.getString("statcode"));
		                c.setRiskCessDate(sqlrs.getInt("rcesdte"));
		                c.setAnbAtCcd(sqlrs.getInt("anbccd"));
		               
		                if (covrMap.containsKey(c.getChdrnum())) {
		                    covrMap.get(c.getChdrnum()).add(c);
		                } else {
		                    List<Covrpf> chdrList = new ArrayList<>();
		                    chdrList.add(c);
		                    covrMap.put(c.getChdrnum(), chdrList);
		                }
		            }

		        } catch (SQLException e) {
			LOGGER.error("searchCovruddByChdrnum()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psCovrSelect, sqlrs);
		        }
		        return covrMap;
		 } 
		 public Map<String,List<Covrpf>> searchCovrmjaByChdrnum(List<String> chdrnumList){

		        StringBuilder sqlCovrSelect1 = new StringBuilder(
		                "SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,"
		                + "CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,"
		                + "TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RSUNIN,RUNDTE,CHGOPT,FUNDSP,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,SINGP,INSTPREM,ZBINSTPREM,ZLINSTPREM,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,RRTDAT,RRTFRM,BBLDAT,CRDEBT,CBANPR,PAYRSEQNO,BAPPMETH,ZSTPDUTY01,ZCLSTATE ");
		        sqlCovrSelect1.append(" FROM COVRMJA  WHERE ");
		        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		        sqlCovrSelect1
		                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		        ResultSet sqlrs = null;
		        Map<String, List<Covrpf>> covrMap = new HashMap<>();
		        try {
		        	sqlrs = executeQuery(psCovrSelect);
		            while (sqlrs.next()) {
		                Covrpf c = new Covrpf();
		                c.setChdrcoy(sqlrs.getString("chdrcoy"));
		                c.setChdrnum(sqlrs.getString("chdrnum"));
		                c.setLife(sqlrs.getString("life"));
		                c.setJlife(sqlrs.getString("jlife"));
		                c.setCoverage(sqlrs.getString("coverage"));
		                c.setRider(sqlrs.getString("rider"));
		                c.setPlanSuffix(sqlrs.getInt("plnsfx"));
		                c.setValidflag(sqlrs.getString("validflag"));
		                c.setTranno(sqlrs.getInt("tranno"));
		                c.setCurrfrom(sqlrs.getInt("currfrom"));
		                c.setCurrto(sqlrs.getInt("currto"));
		                c.setStatcode(sqlrs.getString("statcode"));
		                c.setPstatcode(sqlrs.getString("pstatcode"));
		                c.setStatreasn(sqlrs.getString("statreasn"));
		                c.setCrrcd(sqlrs.getInt("crrcd"));
		                c.setAnbAtCcd(sqlrs.getInt("anbccd"));
		                c.setSex(sqlrs.getString("sex"));
		                c.setReptcd01(sqlrs.getString("reptcd01"));
		                c.setReptcd02(sqlrs.getString("reptcd02"));
		                c.setReptcd03(sqlrs.getString("reptcd03"));
		                c.setReptcd04(sqlrs.getString("reptcd04"));
		                c.setReptcd05(sqlrs.getString("reptcd05"));
		                c.setReptcd06(sqlrs.getString("reptcd06"));
		                c.setCrInstamt01(sqlrs.getBigDecimal("crinst01"));
		                c.setCrInstamt02(sqlrs.getBigDecimal("crinst02"));
		                c.setCrInstamt03(sqlrs.getBigDecimal("crinst03"));
		                c.setCrInstamt04(sqlrs.getBigDecimal("crinst04"));
		                c.setCrInstamt05(sqlrs.getBigDecimal("crinst05"));
		                c.setPremCurrency(sqlrs.getString("prmcur"));
		                c.setTermid(sqlrs.getString("termid"));
		                c.setTransactionDate(sqlrs.getInt("trdt"));
		                c.setTransactionTime(sqlrs.getInt("trtm"));
		                c.setUser(sqlrs.getInt("user_t"));
		                c.setStatFund(sqlrs.getString("stfund"));
		                c.setStatSect(sqlrs.getString("stsect"));
		                c.setStatSubsect(sqlrs.getString("stssect"));
		                c.setCrtable(sqlrs.getString("crtable"));
		                c.setRiskCessDate(sqlrs.getInt("rcesdte"));
		                c.setPremCessDate(sqlrs.getInt("pcesdte"));
		                c.setBenCessDate(sqlrs.getInt("bcesdte"));
		                c.setNextActDate(sqlrs.getInt("nxtdte"));
		                c.setReserveUnitsInd(sqlrs.getString("rsunin"));
		                c.setReserveUnitsDate(sqlrs.getInt("rundte"));
		                c.setChargeOptionsInd(sqlrs.getString("chgopt"));
		                c.setFundSplitPlan(sqlrs.getString("fundsp"));
		                c.setConvertInitialUnits(sqlrs.getInt("cbcvin"));
		                c.setReviewProcessing(sqlrs.getInt("cbrvpr"));
		                c.setUnitStatementDate(sqlrs.getInt("cbunst"));
		                c.setCpiDate(sqlrs.getInt("cpidte"));
		                c.setInitUnitCancDate(sqlrs.getInt("icandt"));
		                c.setExtraAllocDate(sqlrs.getInt("exaldt"));
		                c.setInitUnitIncrsDate(sqlrs.getInt("iincdt"));
		                c.setRiskCessAge(sqlrs.getInt("rcesage"));
		                c.setPremCessAge(sqlrs.getInt("pcesage"));
		                c.setBenCessAge(sqlrs.getInt("bcesage"));
		                c.setRiskCessTerm(sqlrs.getInt("rcestrm"));
		                c.setPremCessTerm(sqlrs.getInt("pcestrm"));
		                c.setBenCessTerm(sqlrs.getInt("bcestrm"));
		                c.setSumins(sqlrs.getBigDecimal("sumins"));
		                c.setSicurr(sqlrs.getString("sicurr"));
		                c.setVarSumInsured(sqlrs.getBigDecimal("varsi"));
		                c.setMortcls(sqlrs.getString("mortcls"));
		                c.setLiencd(sqlrs.getString("liencd"));
		                c.setRatingClass(sqlrs.getString("ratcls"));
		                c.setIndexationInd(sqlrs.getString("indxin"));
		                c.setBonusInd(sqlrs.getString("bnusin"));
		                c.setDeferPerdCode(sqlrs.getString("dpcd"));
		                c.setDeferPerdAmt(sqlrs.getBigDecimal("dpamt"));
		                c.setDeferPerdInd(sqlrs.getString("dpind"));
		                c.setTotMthlyBenefit(sqlrs.getBigDecimal("tmben"));
		                c.setSingp(sqlrs.getBigDecimal("singp"));
		                c.setInstprem(sqlrs.getBigDecimal("instprem"));
		                c.setZbinstprem(sqlrs.getBigDecimal("zbinstprem"));
		                c.setZlinstprem(sqlrs.getBigDecimal("zlinstprem"));
		                c.setEstMatValue01(sqlrs.getBigDecimal("emv01"));
		                c.setEstMatValue02(sqlrs.getBigDecimal("emv02"));
		                c.setEstMatDate01(sqlrs.getInt("emvdte01"));
		                c.setEstMatDate02(sqlrs.getInt("emvdte02"));
		                c.setEstMatInt01(sqlrs.getBigDecimal("emvint01"));
		                c.setEstMatInt02(sqlrs.getBigDecimal("emvint02"));
		                c.setCampaign(sqlrs.getString("campaign"));
		                c.setStatSumins(sqlrs.getBigDecimal("stsmin"));
		                c.setRtrnyrs(sqlrs.getInt("rtrnyrs"));
		                c.setPremCessAgeMth(sqlrs.getInt("pcamth"));
		                c.setPremCessAgeDay(sqlrs.getInt("pcaday"));
		                c.setPremCessTermMth(sqlrs.getInt("pctmth"));
		                c.setPremCessTermDay(sqlrs.getInt("pctday"));
		                c.setRiskCessAgeMth(sqlrs.getInt("rcamth"));
		                c.setRiskCessAgeDay(sqlrs.getInt("rcaday"));
		                c.setRiskCessTermMth(sqlrs.getInt("rctmth"));
		                c.setRiskCessTermDay(sqlrs.getInt("rctday"));
		                c.setJlLsInd(sqlrs.getString("jllsid"));
		                c.setRerateDate(sqlrs.getInt("rrtdat"));
		                c.setRerateFromDate(sqlrs.getInt("rrtfrm"));
		                c.setBenBillDate(sqlrs.getInt("bbldat"));
		                c.setCoverageDebt(sqlrs.getBigDecimal("crdebt"));
		                c.setAnnivProcDate(sqlrs.getInt("cbanpr"));
		                c.setPayrseqno(sqlrs.getInt("payrseqno"));
		                c.setBappmeth(sqlrs.getString("bappmeth"));
		                c.setZstpduty01(sqlrs.getBigDecimal("ZSTPDUTY01"));
		                c.setUniqueNumber(sqlrs.getLong("unique_number"));
		               
		                if (covrMap.containsKey(c.getChdrnum())) {
		                    covrMap.get(c.getChdrnum()).add(c);
		                } else {
		                    List<Covrpf> chdrList = new ArrayList<>();
		                    chdrList.add(c);
		                    covrMap.put(c.getChdrnum(), chdrList);
		                }
		            }

		        } catch (SQLException e) {
			LOGGER.error("searchCovrmjaByChdrnum()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psCovrSelect, sqlrs);
		        }
		        return covrMap;
		 } 
		 public void updateCovrDebt(List<Covrpf> updateCovrpfDebtList){
			 String SQL_COVR_UPDATE = "UPDATE COVRUDD SET CRDEBT=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE UNIQUE_NUMBER=? ";
			 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
	      
	         try {
	        	 for(Covrpf c:updateCovrpfDebtList){
	                 psCovrUpdate.setBigDecimal(1, c.getCoverageDebt());
	                 psCovrUpdate.setString(2, getJobnm());
	                 psCovrUpdate.setString(3, getUsrprf());
	                 psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	                 psCovrUpdate.setLong(5, c.getUniqueNumber());
	                 psCovrUpdate.addBatch();
	        	 }
	        	 psCovrUpdate.executeBatch();
	         } catch (SQLException e) {
			LOGGER.error("updateCovrDebt()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(psCovrUpdate, null);
	         }
		 }
		 
		 public void updateCovrmjaRecord(List<Covrpf> updateCovrpfList){
			 String SQL_COVR_UPDATE = "UPDATE COVRMJA SET TRANNO=?, VALIDFLAG=?, CRDEBT=?, CURRTO=?, JOBNM=?, USRPRF=?,DATIME=?  WHERE UNIQUE_NUMBER=? ";
			 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
	      
	         try {
	        	 for(Covrpf c:updateCovrpfList){
	                 psCovrUpdate.setInt(1, c.getTranno());
	                 psCovrUpdate.setString(2, c.getValidflag());
	                 psCovrUpdate.setBigDecimal(3, c.getCoverageDebt());
	                 psCovrUpdate.setInt(4, c.getCurrto());
	                 psCovrUpdate.setString(5, getJobnm());
	                 psCovrUpdate.setString(6, getUsrprf());
	                 psCovrUpdate.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
	                 psCovrUpdate.setLong(8, c.getUniqueNumber());
	                 psCovrUpdate.addBatch();
	        	 }
	        	 psCovrUpdate.executeBatch();
	         } catch (SQLException e) {
			LOGGER.error("updateCovrmjaRecord()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(psCovrUpdate, null);
	         }
		 }
		 
		    public void updateChunkRecordToCovr(List<Covrpf> covrpfList){
				if (covrpfList != null && covrpfList.size() > 0) {
					StringBuilder sb = new StringBuilder();
					sb.append("update covrpf set CBANPR=? where unique_number=?");
				
					PreparedStatement ps = getPrepareStatement(sb.toString());
					 try {
			               for (Covrpf p : covrpfList) {
			               		ps.setInt(1, p.getAnnivProcDate());
				               	ps.setLong(2, p.getUniqueNumber());
				               	ps.addBatch();
			               }
			               ps.executeBatch();
					}catch (SQLException e) {
				LOGGER.error("updateChunkRecordToCovr()", e); /* IJTI-1479 */
			           throw new SQLRuntimeException(e);
			       } finally {
			           close(ps, null);
			       
					}
				}
		    }		 
		
		// Starts: ILIFE-4349
		@Override
		public boolean updateSumins(Covrpf covrpf) {
			boolean updateStatus = false;
			String SQL_COVR_UPDATE = "UPDATE COVRPF SET SUMINS=? WHERE UNIQUE_NUMBER=? ";
			PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
			try {
				psCovrUpdate.setBigDecimal(1, covrpf.getSumins());
				psCovrUpdate.setLong(2, covrpf.getUniqueNumber());
				int updatedCount = psCovrUpdate.executeUpdate();
				if(updatedCount == 1){
					updateStatus = true;
				}
			} catch (SQLException e) {
			LOGGER.error("updateCovrIndexationind()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrUpdate, null);
			}
			return updateStatus;
		}
		// Ends: ILIFE-4349
		

		public Map<String, List<Covrpf>> searchCovrBenbilldate(String chdrcoy, List<String> chdrnumList){


		        StringBuilder sqlCovrSelect1 = new StringBuilder(
		                "SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,BBLDAT,UNIQUE_NUMBER,CRTABLE,STATCODE,PSTATCODE, ");
		        sqlCovrSelect1.append("VALIDFLAG FROM COVR WHERE CHDRCOY = ? AND ");//IJTI-1767
		        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		        sqlCovrSelect1
		                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
	            
		        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		        
		        ResultSet sqlcovrpf1rs = null;
		        Map<String, List<Covrpf>> covrMap = new HashMap<>();
		        try {
		        	psCovrSelect.setString(1, chdrcoy);
		            sqlcovrpf1rs = executeQuery(psCovrSelect);
		            while (sqlcovrpf1rs.next()) {
		                Covrpf covrpf = new Covrpf();
		                covrpf.setChdrcoy(sqlcovrpf1rs.getString("CHDRCOY"));
		                covrpf.setChdrnum(sqlcovrpf1rs.getString("CHDRNUM"));
		                covrpf.setLife(sqlcovrpf1rs.getString("LIFE"));
		                covrpf.setCoverage(sqlcovrpf1rs.getString("COVERAGE"));
		                covrpf.setRider(sqlcovrpf1rs.getString("RIDER"));
		                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt("PLNSFX"));
		                covrpf.setBenBillDate(sqlcovrpf1rs.getInt("BBLDAT"));
		                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong("UNIQUE_NUMBER"));
		                covrpf.setCrtable(sqlcovrpf1rs.getString("CRTABLE"));
		                covrpf.setStatcode(sqlcovrpf1rs.getString("STATCODE"));
		                covrpf.setPstatcode(sqlcovrpf1rs.getString("PSTATCODE"));
		                covrpf.setValidflag(sqlcovrpf1rs.getString("VALIDFLAG"));//IJTI-1767
		                if (covrMap.containsKey(covrpf.getChdrnum())) {
		                    covrMap.get(covrpf.getChdrnum()).add(covrpf);
		                } else {
		                    List<Covrpf> chdrList = new ArrayList<>();
		                    chdrList.add(covrpf);
		                    covrMap.put(covrpf.getChdrnum(), chdrList);
		                }
		            }
		        } catch (SQLException e) {
			LOGGER.error("searchCovrBenbilldate()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psCovrSelect, sqlcovrpf1rs);
		        }
		        return covrMap;
		 }
		 
		 public void updateCovrBillbendateRecord(List<Covrpf> updateCovrpfList){
			/* String SQL_COVR_UPDATE = "UPDATE COVR SET BENBILLDATE=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";*/
			 String SQL_COVR_UPDATE = "UPDATE COVR SET BBLDAT=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? "; /*ILIFE-4733*/
			 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
	      
	         try {
	        	 for(Covrpf c:updateCovrpfList){
	                 psCovrUpdate.setInt(1, c.getBenBillDate());
	                 psCovrUpdate.setString(2, getJobnm());
	                 psCovrUpdate.setString(3, getUsrprf());
	                 psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	                 psCovrUpdate.setLong(5, c.getUniqueNumber());
	                 psCovrUpdate.addBatch();
	        	 }
	        	 psCovrUpdate.executeBatch();
	         } catch (SQLException e) {
			LOGGER.error("updateCovrBillbendateRecord()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(psCovrUpdate, null);
	         }
		 }
		 
	public void updateVarSumInsuredRecord(List<Covrpf> uniqueList) {
		if (!uniqueList.isEmpty()) {
			StringBuilder updateSql = new StringBuilder();
			updateSql
					.append(" UPDATE COVRPF SET CURRTO=?,VARSI=?,VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ");
			PreparedStatement ps = getPrepareStatement(updateSql.toString());
			try {
				for (Covrpf covrpf : uniqueList) {
					ps.setInt(1, covrpf.getCurrto());
					ps.setBigDecimal(2, covrpf.getVarSumInsured());
					ps.setString(3, covrpf.getValidflag());
					ps.setString(4, getJobnm());
					ps.setString(5, getUsrprf());
					ps.setTimestamp(6, getDatime());
					ps.setLong(7, covrpf.getUniqueNumber());
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateVarSumInsuredRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
	}	
	
	//ILIFE-4399	 
	 public Map<String,List<Covrpf>> searchCovrrnlByChdrnum(List<String> chdrnumList){

		        StringBuilder sqlCovrSelect1 = new StringBuilder(
		        "SELECT * " );  
		        sqlCovrSelect1.append("FROM COVRRNL WHERE ");
		        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		        sqlCovrSelect1
		                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		        ResultSet sqlcovrpf1rs = null;
		        Map<String, List<Covrpf>> covrMap = new HashMap<>();        		
		        try {
		            //Covrpf covrpf = new Covrpf();
		        	psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		            sqlcovrpf1rs = executeQuery(psCovrSelect);
		            while (sqlcovrpf1rs.next()) {
		            	//BeanPropertyRowMapper<Covrpf> mapper = BeanPropertyRowMapper.newInstance(Covrpf.class);
						//covrpf = mapper.mapRow(sqlcovrpf1rs, 1);
		            	//int i = 1;
		                Covrpf covrpf = new Covrpf();
		            	//psCovrSelect.setString(1, );
		                covrpf.setChdrcoy(sqlcovrpf1rs.getString("CHDRCOY"));
		                covrpf.setChdrnum(sqlcovrpf1rs.getString("CHDRNUM"));
		                covrpf.setLife(sqlcovrpf1rs.getString("LIFE"));
		                covrpf.setCoverage(sqlcovrpf1rs.getString("COVERAGE"));
		                covrpf.setRider(sqlcovrpf1rs.getString("RIDER"));
		                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt("PLNSFX"));
		                covrpf.setCurrfrom(sqlcovrpf1rs.getInt("CURRFROM"));
		                covrpf.setCurrto(sqlcovrpf1rs.getInt("CURRTO"));
		                covrpf.setValidflag(sqlcovrpf1rs.getString("VALIDFLAG"));
		                covrpf.setCrtable(sqlcovrpf1rs.getString("CRTABLE"));
		                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal("CRDEBT"));
		                covrpf.setBenBillDate(sqlcovrpf1rs.getInt("BBLDAT"));
		                covrpf.setStatcode(sqlcovrpf1rs.getString("STATCODE"));
		                covrpf.setPstatcode(sqlcovrpf1rs.getString("PSTATCODE"));
		                covrpf.setCrrcd(sqlcovrpf1rs.getInt("CRRCD"));
		                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal("INSTPREM"));
		                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal("SUMINS"));
		                covrpf.setPremCessDate(sqlcovrpf1rs.getInt("PCESDTE"));
		                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt("RCESDTE"));
		                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal("VARSI"));
		                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong("UNIQUE_NUMBER"));
		                covrpf.setPremCurrency(sqlcovrpf1rs.getString("PRMCUR"));  //ILIFE-5329 start by dpuhawan
		                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal("ZBINSTPREM"));
		                covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal("ZLINSTPREM"));
		                covrpf.setZstpduty01(sqlcovrpf1rs.getBigDecimal("ZSTPDUTY01"));
		                covrpf.setZclstate(sqlcovrpf1rs.getString("ZCLSTATE"));
		                covrpf.setJlife(sqlcovrpf1rs.getString("JLIFE"));
		                covrpf.setStatreasn(sqlcovrpf1rs.getString("STATREASN"));
		                covrpf.setSex(sqlcovrpf1rs.getString("SEX"));
		                covrpf.setReptcd01(sqlcovrpf1rs.getString("REPTCD01"));
		                covrpf.setReptcd02(sqlcovrpf1rs.getString("REPTCD02"));
		                covrpf.setReptcd03(sqlcovrpf1rs.getString("REPTCD03"));
		                covrpf.setReptcd04(sqlcovrpf1rs.getString("REPTCD04"));
		                covrpf.setReptcd05(sqlcovrpf1rs.getString("REPTCD05"));
		                covrpf.setReptcd06(sqlcovrpf1rs.getString("REPTCD06"));
		                covrpf.setStatFund(sqlcovrpf1rs.getString("STFUND"));
		                covrpf.setStatSect(sqlcovrpf1rs.getString("STSECT"));
		                covrpf.setStatSubsect(sqlcovrpf1rs.getString("STSSECT"));
		                covrpf.setSicurr(sqlcovrpf1rs.getString("SICURR"));
		                covrpf.setMortcls(sqlcovrpf1rs.getString("MORTCLS"));
		                covrpf.setLiencd(sqlcovrpf1rs.getString("LIENCD"));
		                covrpf.setRatingClass(sqlcovrpf1rs.getString("RATCLS"));
		                covrpf.setIndexationInd(sqlcovrpf1rs.getString("INDXIN"));
		                covrpf.setBonusInd(sqlcovrpf1rs.getString("BNUSIN"));
		                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString("DPCD"));
		                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString("DPIND"));
		                covrpf.setCampaign(sqlcovrpf1rs.getString("CAMPAIGN"));
		                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString("RSUNIN"));
		                covrpf.setJlLsInd(sqlcovrpf1rs.getString("JLLSID"));
		                covrpf.setRerateDate(sqlcovrpf1rs.getInt("RRTDAT"));
		                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt("RRTFRM"));
		                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt("CBUNST"));
		                covrpf.setCpiDate(sqlcovrpf1rs.getInt("CPIDTE"));
		                covrpf.setBappmeth(sqlcovrpf1rs.getString("BAPPMETH"));
		                covrpf.setReinstated(sqlcovrpf1rs.getString("REINSTATED"));//ILIFE-8509
		                covrpf.setTpdtype(sqlcovrpf1rs.getString("TPDTYPE"));
		                covrpf.setProrateprem(sqlcovrpf1rs.getBigDecimal("PRORATEPREM"));
		                covrpf.setPayrseqno(sqlcovrpf1rs.getInt("PAYRSEQNO"));
		                if (covrMap.containsKey(covrpf.getChdrnum())) {
		                    covrMap.get(covrpf.getChdrnum()).add(covrpf);
		                } else {
		                    List<Covrpf> chdrList = new ArrayList<>();
		                    chdrList.add(covrpf);
		                    covrMap.put(covrpf.getChdrnum(), chdrList);
		                }
		            }

		        } catch (SQLException e) {
			LOGGER.error("searchCovrrnlByChdrnum()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psCovrSelect, sqlcovrpf1rs);
		        }
		        return covrMap;
		 } 
	 
	 public boolean updateCovrValidFlag(List<Covrpf> updateCovrpfList){
		 boolean isUpdated = false;
		 String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG='2', CURRTO=?, JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
		 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
      
         try {
        	 for(Covrpf c:updateCovrpfList){
        		 psCovrUpdate.setInt(1, c.getCurrto());
                 psCovrUpdate.setString(2, getJobnm());
                 psCovrUpdate.setString(3, getUsrprf());
                 psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                 psCovrUpdate.setLong(5, c.getUniqueNumber());
                 psCovrUpdate.addBatch();
        	 }
        	 psCovrUpdate.executeBatch();
        	 isUpdated = true;
         } catch (SQLException e) {
			LOGGER.error("updateCovrValidFlag()", e); /* IJTI-1479 */
             throw new SQLRuntimeException(e);
         } finally {
             close(psCovrUpdate, null);
         }
         return isUpdated;
	 }	 
	 
	 // Ticket#ILIFE-4404
	 public Map<String,List<Covrpf>> searchValidCovrByChdrnum(List<String> chdrnumList){

			if(chdrnumList == null || chdrnumList.isEmpty()){
				return null;
			}
			
	        StringBuilder sqlCovrSelect1 = new StringBuilder(
	                "SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,UNIQUE_NUMBER,ZCLSTATE,REINSTATED,TPDTYPE,PRORATEPREM ");
	        sqlCovrSelect1.append("FROM COVRPF WHERE VALIDFLAG = '1' AND ");
	        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	        sqlCovrSelect1
	                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, PLNSFX ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");
	        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
	        ResultSet sqlcovrpf1rs = null;
	        Map<String, List<Covrpf>> covrMap = new HashMap<String, List<Covrpf>>();
	        try {
	            sqlcovrpf1rs = executeQuery(psCovrSelect);

	            while (sqlcovrpf1rs.next()) {
	                Covrpf covrpf = new Covrpf();
	                covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
	                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
	                covrpf.setLife(sqlcovrpf1rs.getString(3));
	                covrpf.setJlife(sqlcovrpf1rs.getString(4));
	                covrpf.setCoverage(sqlcovrpf1rs.getString(5));
	                covrpf.setRider(sqlcovrpf1rs.getString(6));
	                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(7));
	                covrpf.setValidflag(sqlcovrpf1rs.getString(8));
	                covrpf.setTranno(sqlcovrpf1rs.getInt(9));
	                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(10));
	                covrpf.setCurrto(sqlcovrpf1rs.getInt(11));
	                covrpf.setStatcode(sqlcovrpf1rs.getString(12));
	                covrpf.setPstatcode(sqlcovrpf1rs.getString(13));
	                covrpf.setStatreasn(sqlcovrpf1rs.getString(14));
	                covrpf.setCrrcd(sqlcovrpf1rs.getInt(15));
	                covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(16));
	                covrpf.setSex(sqlcovrpf1rs.getString(17));
	                covrpf.setReptcd01(sqlcovrpf1rs.getString(18));
	                covrpf.setReptcd02(sqlcovrpf1rs.getString(19));
	                covrpf.setReptcd03(sqlcovrpf1rs.getString(20));
	                covrpf.setReptcd04(sqlcovrpf1rs.getString(21));
	                covrpf.setReptcd05(sqlcovrpf1rs.getString(22));
	                covrpf.setReptcd06(sqlcovrpf1rs.getString(23));
	                covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal(24));
	                covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal(25));
	                covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal(26));
	                covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal(27));
	                covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal(28));
	                covrpf.setPremCurrency(sqlcovrpf1rs.getString(29));
	                covrpf.setTermid(sqlcovrpf1rs.getString(30));
	                covrpf.setTransactionDate(sqlcovrpf1rs.getInt(31));
	                covrpf.setTransactionTime(sqlcovrpf1rs.getInt(32));
	                covrpf.setUser(sqlcovrpf1rs.getInt(33));
	                covrpf.setStatFund(sqlcovrpf1rs.getString(34));
	                covrpf.setStatSect(sqlcovrpf1rs.getString(35));
	                covrpf.setStatSubsect(sqlcovrpf1rs.getString(36));
	                covrpf.setCrtable(sqlcovrpf1rs.getString(37));
	                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(38));
	                covrpf.setPremCessDate(sqlcovrpf1rs.getInt(39));
	                covrpf.setBenCessDate(sqlcovrpf1rs.getInt(40));
	                covrpf.setNextActDate(sqlcovrpf1rs.getInt(41));
	                covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(42));
	                covrpf.setPremCessAge(sqlcovrpf1rs.getInt(43));
	                covrpf.setBenCessAge(sqlcovrpf1rs.getInt(44));
	                covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(45));
	                covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(46));
	                covrpf.setBenCessTerm(sqlcovrpf1rs.getInt(47));
	                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(48));
	                covrpf.setSicurr(sqlcovrpf1rs.getString(49));
	                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal(50));
	                covrpf.setMortcls(sqlcovrpf1rs.getString(51));
	                covrpf.setLiencd(sqlcovrpf1rs.getString(52));
	                covrpf.setRatingClass(sqlcovrpf1rs.getString(53));
	                covrpf.setIndexationInd(sqlcovrpf1rs.getString(54));
	                covrpf.setBonusInd(sqlcovrpf1rs.getString(55));
	                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString(56));
	                covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal(57));
	                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString(58));
	                covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal(59));
	                covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal(60));
	                covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal(61));
	                covrpf.setEstMatDate01(sqlcovrpf1rs.getInt(62));
	                covrpf.setEstMatDate02(sqlcovrpf1rs.getInt(63));
	                covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal(64));
	                covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal(65));
	                covrpf.setCampaign(sqlcovrpf1rs.getString(66));
	                covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal(67));
	                covrpf.setRtrnyrs(sqlcovrpf1rs.getInt(68));
	                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString(69));
	                covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt(70));
	                covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString(71));
	                covrpf.setFundSplitPlan(sqlcovrpf1rs.getString(72));
	                covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt(73));
	                covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt(74));
	                covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt(75));
	                covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt(76));
	                covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt(77));
	                covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt(78));
	                covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt(79));
	                covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt(80));
	                covrpf.setJlLsInd(sqlcovrpf1rs.getString(81));
	                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(82));
	                covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(83));
	                covrpf.setRerateDate(sqlcovrpf1rs.getInt(84));
	                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(85));
	                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(86));
	                covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(87));
	                covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(88));
	                covrpf.setReviewProcessing(sqlcovrpf1rs.getInt(89));
	                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(90));
	                covrpf.setCpiDate(sqlcovrpf1rs.getInt(91));
	                covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt(92));
	                covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt(93));
	                covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt(94));
	                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(95));
	                covrpf.setPayrseqno(sqlcovrpf1rs.getInt(96));
	                covrpf.setBappmeth(sqlcovrpf1rs.getString(97));
	                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(98));
	                covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(99));
	                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(100));
	                covrpf.setZclstate(sqlcovrpf1rs.getString(101));
	                covrpf.setReinstated(sqlcovrpf1rs.getString(102));//ILIFE-8509
	                covrpf.setTpdtype(sqlcovrpf1rs.getString(103));
	                covrpf.setProrateprem(sqlcovrpf1rs.getBigDecimal(104) == null ? BigDecimal.ZERO : sqlcovrpf1rs.getBigDecimal(104));
	                if (covrMap.containsKey(covrpf.getChdrnum())) {
	                    covrMap.get(covrpf.getChdrnum()).add(covrpf);
	                } else {
	                    List<Covrpf> chdrList = new ArrayList<Covrpf>();
	                    chdrList.add(covrpf);
	                    covrMap.put(covrpf.getChdrnum(), chdrList);
	                }
	            }

	        } catch (SQLException e) {
			LOGGER.error("searchValidCovrByChdrnum()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psCovrSelect, sqlcovrpf1rs);
	        }
	        return covrMap;
	 }
	 public List<Covrpf> getCovrincrRecord(String chdrcoy, String chdrnum, String life) {
		 
		    StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM, CURRTO, "); 
			sql.append("STATCODE, PSTATCODE, CPIDTE, INDXIN, RCESTRM, PCESTRM, INSTPREM, CRTABLE, SUMINS FROM COVRPF WHERE VALIDFLAG = '1' and CHDRCOY=? and CHDRNUM=? and LIFE >= ? and INDXIN != 'P' ");
			sql.append("order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
	     	   	
	        PreparedStatement ps = getPrepareStatement(sql.toString());
			ResultSet rs = null;
			Covrpf covrpf = null;
			List<Covrpf> list = new ArrayList<Covrpf>();
			try {
				ps.setString(1, chdrcoy);
				ps.setString(2, chdrnum);
				ps.setString(3, life);
								
				rs = ps.executeQuery();

				while (rs.next()) {
					covrpf = new Covrpf();
					covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(rs.getString("CHDRCOY"));
	                covrpf.setChdrnum(rs.getString("CHDRNUM"));
	                covrpf.setLife(rs.getString("LIFE"));
	                covrpf.setJlife(rs.getString("JLIFE"));
	                covrpf.setCoverage(rs.getString("COVERAGE"));
	                covrpf.setRider(rs.getString("RIDER"));
	                covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
	                covrpf.setValidflag(rs.getString("VALIDFLAG"));
	                covrpf.setTranno(rs.getInt("TRANNO"));
	                covrpf.setCurrfrom(rs.getInt("CURRFROM"));
	                covrpf.setCurrto(rs.getInt("CURRTO"));
	                covrpf.setStatcode(rs.getString("STATCODE"));
	                covrpf.setPstatcode(rs.getString("PSTATCODE"));             
	                covrpf.setCpiDate(rs.getInt("CPIDTE"));
	                covrpf.setIndexationInd(rs.getString("INDXIN"));
	                covrpf.setRiskCessTerm(rs.getInt("RCESTRM"));
	                covrpf.setPremCessTerm(rs.getInt("PCESTRM"));
	                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
	                covrpf.setCrtable(rs.getString("CRTABLE"));
	                covrpf.setSumins(rs.getBigDecimal("SUMINS"));
	                list.add(covrpf);	                
				}
			} catch (SQLException e) {
			LOGGER.error("getCovrincrRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}

			return list;
			
		}	 


		public List<Covrpf> getCovrsurByComAndNum(String coy, String chdrnum) {

//			StringBuilder sql = new StringBuilder("SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,UNIQUE_NUMBER"); 
			StringBuilder sql = new StringBuilder("SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,");
					sql.append("STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,");
					sql.append("USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,");
					sql.append("RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,");
					sql.append("PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,");
					sql.append("CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,UNIQUE_NUMBER,ZSTPDUTY01,ZCLSTATE,USRPRF,JOBNM,DATIME,LOADPER,RATEADJ,FLTMORT,PREMADJ,AGEADJ,");
					sql.append("LNKGNO,LNKGSUBREFNO,TPDTYPE,LNKGIND,SINGPREMTYPE,RISKPREM,REINSTATED,PRORATEPREM,COMMPREM ");//ILIFE-8537 added ZSTPDUTY01
					sql.append("FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? and VALIDFLAG = '1' ");
					sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, PLNSFX ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC");

			PreparedStatement ps = getPrepareStatement(sql.toString());
			ResultSet rs = null;
			List<Covrpf> list = new ArrayList<Covrpf>();
			Covrpf covrpf = null;
			try {
				ps.setString(1, coy);
				ps.setString(2, chdrnum);
				rs = ps.executeQuery();

				while (rs.next()) {
					covrpf = new Covrpf();
					
					covrpf.setChdrcoy(rs.getString(1));
	                covrpf.setChdrnum(rs.getString(2));
	                covrpf.setLife(rs.getString(3));
	                covrpf.setJlife(rs.getString(4));
	                covrpf.setCoverage(rs.getString(5));
	                covrpf.setRider(rs.getString(6));
	                covrpf.setPlanSuffix(rs.getInt(7));
	                covrpf.setValidflag(rs.getString(8));
	                covrpf.setTranno(rs.getInt(9));
	                covrpf.setCurrfrom(rs.getInt(10));
	                covrpf.setCurrto(rs.getInt(11));
	                covrpf.setStatcode(rs.getString(12));
	                covrpf.setPstatcode(rs.getString(13));
	                covrpf.setStatreasn(rs.getString(14));
	                covrpf.setCrrcd(rs.getInt(15));
	                covrpf.setAnbAtCcd(rs.getInt(16));
	                covrpf.setSex(rs.getString(17));
	                covrpf.setReptcd01(rs.getString(18));
	                covrpf.setReptcd02(rs.getString(19));
	                covrpf.setReptcd03(rs.getString(20));
	                covrpf.setReptcd04(rs.getString(21));
	                covrpf.setReptcd05(rs.getString(22));
	                covrpf.setReptcd06(rs.getString(23));
	                covrpf.setCrInstamt01(rs.getBigDecimal(24));
	                covrpf.setCrInstamt02(rs.getBigDecimal(25));
	                covrpf.setCrInstamt03(rs.getBigDecimal(26));
	                covrpf.setCrInstamt04(rs.getBigDecimal(27));
	                covrpf.setCrInstamt05(rs.getBigDecimal(28));
	                covrpf.setPremCurrency(rs.getString(29));
	                covrpf.setTermid(rs.getString(30));
	                covrpf.setTransactionDate(rs.getInt(31));
	                covrpf.setTransactionTime(rs.getInt(32));
	                covrpf.setUser(rs.getInt(33));
	                covrpf.setStatFund(rs.getString(34));
	                covrpf.setStatSect(rs.getString(35));
	                covrpf.setStatSubsect(rs.getString(36));
	                covrpf.setCrtable(rs.getString(37));
	                covrpf.setRiskCessDate(rs.getInt(38));
	                covrpf.setPremCessDate(rs.getInt(39));
	                covrpf.setBenCessDate(rs.getInt(40));
	                covrpf.setNextActDate(rs.getInt(41));
	                covrpf.setRiskCessAge(rs.getInt(42));
	                covrpf.setPremCessAge(rs.getInt(43));
	                covrpf.setBenCessAge(rs.getInt(44));
	                covrpf.setRiskCessTerm(rs.getInt(45));
	                covrpf.setPremCessTerm(rs.getInt(46));
	                covrpf.setBenCessTerm(rs.getInt(47));
	                covrpf.setSumins(rs.getBigDecimal(48));
	                covrpf.setSicurr(rs.getString(49));
	                covrpf.setVarSumInsured(rs.getBigDecimal(50));
	                covrpf.setMortcls(rs.getString(51));
	                covrpf.setLiencd(rs.getString(52));
	                covrpf.setRatingClass(rs.getString(53));
	                covrpf.setIndexationInd(rs.getString(54));
	                covrpf.setBonusInd(rs.getString(55));
	                covrpf.setDeferPerdCode(rs.getString(56));
	                covrpf.setDeferPerdAmt(rs.getBigDecimal(57));
	                covrpf.setDeferPerdInd(rs.getString(58));
	                covrpf.setTotMthlyBenefit(rs.getBigDecimal(59));
	                covrpf.setEstMatValue01(rs.getBigDecimal(60));
	                covrpf.setEstMatValue02(rs.getBigDecimal(61));
	                covrpf.setEstMatDate01(rs.getInt(62));
	                covrpf.setEstMatDate02(rs.getInt(63));
	                covrpf.setEstMatInt01(rs.getBigDecimal(64));
	                covrpf.setEstMatInt02(rs.getBigDecimal(65));
	                covrpf.setCampaign(rs.getString(66));
	                covrpf.setStatSumins(rs.getBigDecimal(67));
	                covrpf.setRtrnyrs(rs.getInt(68));
	                covrpf.setReserveUnitsInd(rs.getString(69));
	                covrpf.setReserveUnitsDate(rs.getInt(70));
	                covrpf.setChargeOptionsInd(rs.getString(71));
	                covrpf.setFundSplitPlan(rs.getString(72));
	                covrpf.setPremCessAgeMth(rs.getInt(73));
	                covrpf.setPremCessAgeDay(rs.getInt(74));
	                covrpf.setPremCessTermMth(rs.getInt(75));
	                covrpf.setPremCessTermDay(rs.getInt(76));
	                covrpf.setRiskCessAgeMth(rs.getInt(77));
	                covrpf.setRiskCessAgeDay(rs.getInt(78));
	                covrpf.setRiskCessTermMth(rs.getInt(79));
	                covrpf.setRiskCessTermDay(rs.getInt(80));
	                covrpf.setJlLsInd(rs.getString(81));
	                covrpf.setInstprem(rs.getBigDecimal(82));
	                covrpf.setSingp(rs.getBigDecimal(83));
	                covrpf.setRerateDate(rs.getInt(84));
	                covrpf.setRerateFromDate(rs.getInt(85));
	                covrpf.setBenBillDate(rs.getInt(86));
	                covrpf.setAnnivProcDate(rs.getInt(87));
	                covrpf.setConvertInitialUnits(rs.getInt(88));
	                covrpf.setReviewProcessing(rs.getInt(89));
	                covrpf.setUnitStatementDate(rs.getInt(90));
	                covrpf.setCpiDate(rs.getInt(91));
	                covrpf.setInitUnitCancDate(rs.getInt(92));
	                covrpf.setExtraAllocDate(rs.getInt(93));
	                covrpf.setInitUnitIncrsDate(rs.getInt(94));
	                covrpf.setCoverageDebt(rs.getBigDecimal(95));
	                covrpf.setPayrseqno(rs.getInt(96));
	                covrpf.setBappmeth(rs.getString(97));	                          
	                covrpf.setZbinstprem(((rs.getBigDecimal(98)==null )? BigDecimal.ZERO:rs.getBigDecimal(98)));
	                covrpf.setZlinstprem(((rs.getBigDecimal(99)==null )? BigDecimal.ZERO:rs.getBigDecimal(99)));
	                covrpf.setUniqueNumber(rs.getLong(100));
					covrpf.setZstpduty01(((rs.getBigDecimal(101)==null )? BigDecimal.ZERO:rs.getBigDecimal(101)));
					covrpf.setZclstate(rs.getString(102));
					covrpf.setUserProfile(rs.getString(103));
					covrpf.setJobName(rs.getString(104));
					covrpf.setDatime(rs.getString(105));
					covrpf.setLoadper(((rs.getBigDecimal(106)==null )? BigDecimal.ZERO:rs.getBigDecimal(106)));
					covrpf.setRateadj(((rs.getBigDecimal(107)==null )? BigDecimal.ZERO:rs.getBigDecimal(107)));
					covrpf.setFltmort(((rs.getBigDecimal(108)==null )? BigDecimal.ZERO:rs.getBigDecimal(108)));
					covrpf.setPremadj(((rs.getBigDecimal(109)==null )? BigDecimal.ZERO:rs.getBigDecimal(109)));
					covrpf.setAgeadj(((rs.getBigDecimal(110)==null )? BigDecimal.ZERO:rs.getBigDecimal(110)));										
					covrpf.setLnkgno(rs.getString(111));
					covrpf.setLnkgsubrefno(rs.getString(112));
					covrpf.setTpdtype(rs.getString(113));
					covrpf.setLnkgind(rs.getString(114));
					covrpf.setSingpremtype(rs.getString(115));
					covrpf.setRiskprem(((rs.getBigDecimal(116)==null )? BigDecimal.ZERO:rs.getBigDecimal(116)));							
					covrpf.setReinstated(rs.getString(117));
					covrpf.setProrateprem(((rs.getBigDecimal(118)==null )? BigDecimal.ZERO:rs.getBigDecimal(118)));
					covrpf.setCommprem(((rs.getBigDecimal(119)==null )? BigDecimal.ZERO:rs.getBigDecimal(119)));

					list.add(covrpf);
				}

			} catch (SQLException e) {
			LOGGER.error("getCovrsurByComAndNum()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return list;
		}
	
		@Override
		public List<Covrpf> getcovrtrbRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider) {
			StringBuilder sqlCovrSelect1 = new StringBuilder(
					" SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,UNIQUE_NUMBER,LNKGNO,LNKGSUBREFNO,ZSTPDUTY01 ");  //ILIFE-8248
	        sqlCovrSelect1.append(" FROM COVRPF WHERE CHDRCOY = ?  AND CHDRNUM = ? AND  LIFE = ?  AND COVERAGE = ? AND VALIDFLAG = '1' ");
	        if(rider != null)
	        	sqlCovrSelect1.append(" AND RIDER= ?"); 
	        sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
	        
	   	
	        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
			ResultSet rs = null;
			Covrpf covrpf = null;
			
			List<Covrpf> list =  new ArrayList<Covrpf>();
			
			try {
				ps.setString(1, chdrcoy);
				ps.setString(2, chdrnum);
				ps.setString(3, life);
				ps.setString(4, coverage);
				if(rider != null)
				ps.setString(5, rider);
				
				rs = ps.executeQuery();

				while (rs.next()) {
					covrpf = new Covrpf();
					covrpf.setChdrcoy(rs.getString(1));
	                covrpf.setChdrnum(rs.getString(2));
	                covrpf.setLife(rs.getString(3));
	                covrpf.setJlife(rs.getString(4));
	                covrpf.setCoverage(rs.getString(5));
	                covrpf.setRider(rs.getString(6));
	                covrpf.setPlanSuffix(rs.getInt(7));
	                covrpf.setValidflag(rs.getString(8));
	                covrpf.setTranno(rs.getInt(9));
	                covrpf.setCurrfrom(rs.getInt(10));
	                covrpf.setCurrto(rs.getInt(11));
	                covrpf.setStatcode(rs.getString(12));
	                covrpf.setPstatcode(rs.getString(13));
	                covrpf.setStatreasn(rs.getString(14));
	                covrpf.setCrrcd(rs.getInt(15));
	                covrpf.setAnbAtCcd(rs.getInt(16));
	                covrpf.setSex(rs.getString(17));
	                covrpf.setReptcd01(rs.getString(18));
	                covrpf.setReptcd02(rs.getString(19));
	                covrpf.setReptcd03(rs.getString(20));
	                covrpf.setReptcd04(rs.getString(21));
	                covrpf.setReptcd05(rs.getString(22));
	                covrpf.setReptcd06(rs.getString(23));
	                covrpf.setCrInstamt01(rs.getBigDecimal(24));
	                covrpf.setCrInstamt02(rs.getBigDecimal(25));
	                covrpf.setCrInstamt03(rs.getBigDecimal(26));
	                covrpf.setCrInstamt04(rs.getBigDecimal(27));
	                covrpf.setCrInstamt05(rs.getBigDecimal(28));
	                covrpf.setPremCurrency(rs.getString(29));
	                covrpf.setTermid(rs.getString(30));
	                covrpf.setTransactionDate(rs.getInt(31));
	                covrpf.setTransactionTime(rs.getInt(32));
	                covrpf.setUser(rs.getInt(33));
	                covrpf.setStatFund(rs.getString(34));
	                covrpf.setStatSect(rs.getString(35));
	                covrpf.setStatSubsect(rs.getString(36));
	                covrpf.setCrtable(rs.getString(37));
	                covrpf.setRiskCessDate(rs.getInt(38));
	                covrpf.setPremCessDate(rs.getInt(39));
	                covrpf.setBenCessDate(rs.getInt(40));
	                covrpf.setNextActDate(rs.getInt(41));
	                covrpf.setRiskCessAge(rs.getInt(42));
	                covrpf.setPremCessAge(rs.getInt(43));
	                covrpf.setBenCessAge(rs.getInt(44));
	                covrpf.setRiskCessTerm(rs.getInt(45));
	                covrpf.setPremCessTerm(rs.getInt(46));
	                covrpf.setBenCessTerm(rs.getInt(47));
	                covrpf.setSumins(rs.getBigDecimal(48));
	                covrpf.setSicurr(rs.getString(49));
	                covrpf.setVarSumInsured(rs.getBigDecimal(50));
	                covrpf.setMortcls(rs.getString(51));
	                covrpf.setLiencd(rs.getString(52));
	                covrpf.setRatingClass(rs.getString(53));
	                covrpf.setIndexationInd(rs.getString(54));
	                covrpf.setBonusInd(rs.getString(55));
	                covrpf.setDeferPerdCode(rs.getString(56));
	                covrpf.setDeferPerdAmt(rs.getBigDecimal(57));
	                covrpf.setDeferPerdInd(rs.getString(58));
	                covrpf.setTotMthlyBenefit(rs.getBigDecimal(59));
	                covrpf.setEstMatValue01(rs.getBigDecimal(60));
	                covrpf.setEstMatValue02(rs.getBigDecimal(61));
	                covrpf.setEstMatDate01(rs.getInt(62));
	                covrpf.setEstMatDate02(rs.getInt(63));
	                covrpf.setEstMatInt01(rs.getBigDecimal(64));
	                covrpf.setEstMatInt02(rs.getBigDecimal(65));
	                covrpf.setCampaign(rs.getString(66));
	                covrpf.setStatSumins(rs.getBigDecimal(67));
	                covrpf.setRtrnyrs(rs.getInt(68));
	                covrpf.setReserveUnitsInd(rs.getString(69));
	                covrpf.setReserveUnitsDate(rs.getInt(70));
	                covrpf.setChargeOptionsInd(rs.getString(71));
	                covrpf.setFundSplitPlan(rs.getString(72));
	                covrpf.setPremCessAgeMth(rs.getInt(73));
	                covrpf.setPremCessAgeDay(rs.getInt(74));
	                covrpf.setPremCessTermMth(rs.getInt(75));
	                covrpf.setPremCessTermDay(rs.getInt(76));
	                covrpf.setRiskCessAgeMth(rs.getInt(77));
	                covrpf.setRiskCessAgeDay(rs.getInt(78));
	                covrpf.setRiskCessTermMth(rs.getInt(79));
	                covrpf.setRiskCessTermDay(rs.getInt(80));
	                covrpf.setJlLsInd(rs.getString(81));
	                covrpf.setInstprem(rs.getBigDecimal(82));
	                covrpf.setSingp(rs.getBigDecimal(83));
	                covrpf.setRerateDate(rs.getInt(84));
	                covrpf.setRerateFromDate(rs.getInt(85));
	                covrpf.setBenBillDate(rs.getInt(86));
	                covrpf.setAnnivProcDate(rs.getInt(87));
	                covrpf.setConvertInitialUnits(rs.getInt(88));
	                covrpf.setReviewProcessing(rs.getInt(89));
	                covrpf.setUnitStatementDate(rs.getInt(90));
	                covrpf.setCpiDate(rs.getInt(91));
	                covrpf.setInitUnitCancDate(rs.getInt(92));
	                covrpf.setExtraAllocDate(rs.getInt(93));			
	                covrpf.setInitUnitIncrsDate(rs.getInt(94));
	                covrpf.setCoverageDebt(rs.getBigDecimal(95));
	                covrpf.setPayrseqno(rs.getInt(96));
	                covrpf.setBappmeth(rs.getString(97));
	                covrpf.setZbinstprem(rs.getBigDecimal(98));
	                covrpf.setZlinstprem(rs.getBigDecimal(99));
	                covrpf.setUniqueNumber(rs.getLong(100));
	                covrpf.setLnkgno(rs.getString(101));//ILIFE-8248
					covrpf.setLnkgsubrefno(rs.getString(102));//ILIFE-8248
					covrpf.setZstpduty01(rs.getBigDecimal(103)==null?BigDecimal.ZERO:rs.getBigDecimal(103));//ILIFE-8537
	                list.add(covrpf);
				}
			} catch (SQLException e) {
			LOGGER.error("getcovrincrRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}

			return list;
		}

		 //ILIFE-5023
		 public List<Covrpf> getCovrpupRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider){
			 StringBuilder sql = new StringBuilder();
		    	sql.append("select cl.chdrcoy,cl.chdrnum,cl.plnsfx,cl.coverage,cl.crrcd,cl.crtable,cl.life,cl.rider,cl.statcode,cl.pstatcode,cl.sumins ")
		    	.append("from vm1dta.covrpup cl where cl.chdrcoy=? and cl.chdrnum=? and cl.life=? and cl.coverage=? and cl.rider=?  "
		    			+ " order by  chdrcoy,chdrnum,life,coverage,rider,plnsfx,unique_number desc");
		    	 PreparedStatement ps = getPrepareStatement(sql.toString());
		    	 ResultSet sqlcovrpf1rs = null;
		         List<Covrpf> covrpuplist = null;
		     	 try {
		  			 ps.setString(1, chdrcoy);
		  			 ps.setString(2, chdrnum);
		  			 ps.setString(3, life);
		  			 ps.setString(4, coverage);
		  			 ps.setString(5, rider);
		  			 sqlcovrpf1rs = executeQuery(ps);
		  			covrpuplist = new ArrayList<Covrpf>();
		 			 while(sqlcovrpf1rs.next()){
		                 Covrpf covrpf = new Covrpf();
		                 covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
		                 covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
		                 covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(3));
		                 covrpf.setCoverage(sqlcovrpf1rs.getString(4));
		                 covrpf.setCrrcd(sqlcovrpf1rs.getInt(5));
		                 covrpf.setCrtable(sqlcovrpf1rs.getString(6));
		                 covrpf.setLife(sqlcovrpf1rs.getString(7));
		                 covrpf.setRider(sqlcovrpf1rs.getString(8));
		                 covrpf.setStatcode(sqlcovrpf1rs.getString(9));
		                 covrpf.setPstatcode(sqlcovrpf1rs.getString(10));
		                 covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(11));
		                 covrpuplist.add(covrpf);
		 			}
		 		} catch (SQLException e) {
			LOGGER.error("getCovrpupRecord()", e); /* IJTI-1479 */
		             throw new SQLRuntimeException(e);
		         } finally {
		             close(ps, null);
		         }
		     	 return covrpuplist;
		 }

		public List<Covrpf> getCovrpupRecord(String chdrcoy, String chdrnum,
				String life, String coverage) {
			StringBuilder sql1 = new StringBuilder();
			sql1.append("select * from covrpup where chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? ")
					.append(" order by chdrcoy asc, chdrnum asc, plnsfx asc, life asc, coverage asc, rider asc, unique_number desc");

			 PreparedStatement ps = getPrepareStatement(sql1.toString());
	    	 ResultSet sqlcovrpf1rs = null;
	         List<Covrpf> covrpuplist = new ArrayList<Covrpf>();
				 try {
		  			 ps.setString(1, chdrcoy);
		  			 ps.setString(2, chdrnum);
		  			 ps.setString(3, life);
		  			 ps.setString(4, coverage);
		  			 sqlcovrpf1rs = executeQuery(ps);
				
		
	
				while (sqlcovrpf1rs.next()) {
					Covrpf covrpf = new Covrpf();
					covrpf.setChdrcoy(sqlcovrpf1rs.getString("CHDRCOY"));
					covrpf.setChdrnum(sqlcovrpf1rs.getString("CHDRNUM"));
					covrpf.setPlanSuffix(sqlcovrpf1rs.getInt("PLNSFX"));
					covrpf.setCoverage(sqlcovrpf1rs.getString("COVERAGE"));
					covrpf.setCrrcd(sqlcovrpf1rs.getInt("CRRCD"));
					covrpf.setCrtable(sqlcovrpf1rs.getString("CRTABLE"));
					covrpf.setLife(sqlcovrpf1rs.getString("LIFE"));
					covrpf.setRider(sqlcovrpf1rs.getString("RIDER"));
					covrpf.setStatcode(sqlcovrpf1rs.getString("STATCODE"));
					covrpf.setPstatcode(sqlcovrpf1rs.getString("PSTATCODE"));
					covrpf.setSumins(sqlcovrpf1rs.getBigDecimal("SUMINS"));
					covrpuplist.add(covrpf);
				}
				
				
			}
			 catch (SQLException e) {
			LOGGER.error("getCovrpupRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
			
			return covrpuplist;
		}
		
		public List<Covrpf> getCovrmjaByComAndNum(String coy, String chdrnum) {

			StringBuilder sql = new StringBuilder("SELECT * FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? and VALIDFLAG != '2' ");
			sql.append(" order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX DESC, UNIQUE_NUMBER DESC");

			PreparedStatement ps = getPrepareStatement(sql.toString());
			ResultSet rs = null;
			List<Covrpf> list = new ArrayList<Covrpf>();
			Covrpf covrpf = null;
			try {
				ps.setString(1, coy);
				ps.setString(2, chdrnum);
				rs = ps.executeQuery();

				while (rs.next()) {
					covrpf = new Covrpf();
					
					covrpf = new Covrpf();
					covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(rs.getString("CHDRCOY"));
	                covrpf.setChdrnum(rs.getString("CHDRNUM"));
	                covrpf.setLife(rs.getString("LIFE"));
	                covrpf.setJlife(rs.getString("JLIFE"));
	                covrpf.setCoverage(rs.getString("COVERAGE"));
	                covrpf.setRider(rs.getString("RIDER"));
	                covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
	                covrpf.setValidflag(rs.getString("VALIDFLAG"));
	                covrpf.setTranno(rs.getInt("TRANNO"));
	                covrpf.setCurrfrom(rs.getInt("CURRFROM"));
	                covrpf.setCurrto(rs.getInt("CURRTO"));
	                covrpf.setStatcode(rs.getString("STATCODE"));
	                covrpf.setPstatcode(rs.getString("PSTATCODE"));             
	                covrpf.setCpiDate(rs.getInt("CPIDTE"));
	                covrpf.setIndexationInd(rs.getString("INDXIN"));
	                covrpf.setCrtable(rs.getString("CRTABLE"));
	                covrpf.setCrrcd(rs.getInt("CRRCD"));
	                covrpf.setConvertInitialUnits(rs.getInt("CBCVIN"));
	                covrpf.setPremCurrency(rs.getString("PRMCUR"));
	                covrpf.setPstatcode(rs.getString("PSTATCODE"));
	                covrpf.setSingp(rs.getBigDecimal("SINGP"));
	                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
	                covrpf.setRerateDate(rs.getInt("RRTDAT"));
	                covrpf.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
					covrpf.setSumins(rs.getBigDecimal("SUMINS"));
					covrpf.setPremCessDate(rs.getInt("PCESDTE"));
					covrpf.setRiskCessDate(rs.getInt("RCESDTE"));
					covrpf.setLnkgsubrefno(rs.getString("LNKGSUBREFNO"));
					covrpf.setLnkgno(rs.getString("LNKGNO"));
					covrpf.setMortcls(rs.getString("MORTCLS"));
					covrpf.setRerateFromDate(rs.getInt("RRTFRM"));
					covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
					covrpf.setCommprem(rs.getBigDecimal("COMMPREM"));
					covrpf.setZclstate(rs.getString("ZCLSTATE"));
					list.add(covrpf);

				}

			} catch (SQLException e) {
			LOGGER.error("getCovrsurByComAndNum()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return list;
		}
		
		public List<Covrpf> getCovrByComAndNum(String coy, String chdrnum) {

			StringBuilder sql = new StringBuilder("SELECT * FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? and VALIDFLAG != '2' ");
			sql.append(" order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");

			PreparedStatement ps = getPrepareStatement(sql.toString());
			ResultSet rs = null;
			List<Covrpf> list = new ArrayList<Covrpf>();
			Covrpf covrpf = null;
			try {
				ps.setString(1, coy); //ILIFE-8075
				ps.setString(2, chdrnum);
				rs = ps.executeQuery();

				while (rs.next()) {
					covrpf = new Covrpf();
					
					covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(rs.getString("CHDRCOY"));
	                covrpf.setChdrnum(rs.getString("CHDRNUM"));
	                covrpf.setLife(rs.getString("LIFE"));
	                covrpf.setJlife(rs.getString("JLIFE"));
	                covrpf.setCoverage(rs.getString("COVERAGE"));
	                covrpf.setRider(rs.getString("RIDER"));
	                covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
	                covrpf.setValidflag(rs.getString("VALIDFLAG"));
	                covrpf.setTranno(rs.getInt("TRANNO"));
	                covrpf.setCurrfrom(rs.getInt("CURRFROM"));
	                covrpf.setCurrto(rs.getInt("CURRTO"));
	                covrpf.setStatcode(rs.getString("STATCODE"));
	                covrpf.setPstatcode(rs.getString("PSTATCODE"));             
	                covrpf.setCpiDate(rs.getInt("CPIDTE"));
	                covrpf.setIndexationInd(rs.getString("INDXIN"));
	                covrpf.setCrtable(rs.getString("CRTABLE"));
	                covrpf.setCrrcd(rs.getInt("CRRCD"));
	                covrpf.setConvertInitialUnits(rs.getInt("CBCVIN"));
	                covrpf.setPremCurrency(rs.getString("PRMCUR"));
	                covrpf.setPremCessDate(rs.getInt("PCESDTE"));
	                covrpf.setPstatcode(rs.getString("PSTATCODE"));
	                covrpf.setSingp(rs.getBigDecimal("SINGP"));
	                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
	                covrpf.setRerateDate(rs.getInt("RRTDAT"));
	                covrpf.setRerateFromDate(rs.getInt("RRTFRM"));
	                covrpf.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
	                covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
	                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
	                covrpf.setMortcls(rs.getString("MORTCLS"));
	                covrpf.setSumins(rs.getBigDecimal("SUMINS"));
	                covrpf.setRiskCessTerm(rs.getInt("RCESTRM"));
                    covrpf.setPremCessTerm(rs.getInt("PCESTRM"));
                    covrpf.setReinstated(rs.getString("REINSTATED"));//ILIFE-8509
                    covrpf.setSex(rs.getString("SEX"));
                    covrpf.setLnkgno(rs.getString("LNKGNO"));
					list.add(covrpf);

				}

			} catch (SQLException e) {
			LOGGER.error("getCovrByComAndNum()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return list;
		}
		
		public List<Covrpf> getCovrByComAndNumAllFlagRecord(String coy, String chdrnum) {

			StringBuilder sql = new StringBuilder("SELECT * FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1' ");
			sql.append(" order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");

			PreparedStatement ps = getPrepareStatement(sql.toString());
			ResultSet rs = null;
			List<Covrpf> list = new ArrayList<Covrpf>();
			Covrpf covrpf = null;
			try {
				ps.setString(1, coy);
				ps.setString(2, chdrnum);
				rs = ps.executeQuery();

				while (rs.next()) {
					covrpf = new Covrpf();
					
					covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(rs.getString("CHDRCOY"));
	                covrpf.setChdrnum(rs.getString("CHDRNUM"));
	                covrpf.setLife(rs.getString("LIFE"));
	                covrpf.setJlife(rs.getString("JLIFE"));
	                covrpf.setCoverage(rs.getString("COVERAGE"));
	                covrpf.setRider(rs.getString("RIDER"));
	                covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
	                covrpf.setValidflag(rs.getString("VALIDFLAG"));
	                covrpf.setTranno(rs.getInt("TRANNO"));
	                covrpf.setCurrfrom(rs.getInt("CURRFROM"));
	                covrpf.setCurrto(rs.getInt("CURRTO"));
	                covrpf.setStatcode(rs.getString("STATCODE"));
	                covrpf.setPstatcode(rs.getString("PSTATCODE"));             
	                covrpf.setCpiDate(rs.getInt("CPIDTE"));
	                covrpf.setIndexationInd(rs.getString("INDXIN"));
	                covrpf.setCrtable(rs.getString("CRTABLE"));
	                covrpf.setCrrcd(rs.getInt("CRRCD"));
	                covrpf.setConvertInitialUnits(rs.getInt("CBCVIN"));
	                covrpf.setPremCurrency(rs.getString("PRMCUR"));
	                covrpf.setPremCessDate(rs.getInt("PCESDTE"));
	                covrpf.setPstatcode(rs.getString("PSTATCODE"));
	                covrpf.setSingp(rs.getBigDecimal("SINGP"));
	                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
	                covrpf.setRerateDate(rs.getInt("RRTDAT"));
	                covrpf.setRerateFromDate(rs.getInt("RRTFRM"));
	                covrpf.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
	                covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
	                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
	                covrpf.setMortcls(rs.getString("MORTCLS"));
	                covrpf.setSumins(rs.getBigDecimal("SUMINS"));
	                covrpf.setPremCessDate(rs.getInt("PCESDTE"));
					covrpf.setRiskCessDate(rs.getInt("RCESDTE"));
					covrpf.setLnkgsubrefno(rs.getString("LNKGSUBREFNO"));
					covrpf.setLnkgno(rs.getString("LNKGNO"));
					covrpf.setMortcls(rs.getString("MORTCLS"));
					covrpf.setRerateFromDate(rs.getInt("RRTFRM"));
					covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
					list.add(covrpf);

				}

			} catch (SQLException e) {
			LOGGER.error("getCovrByComAndNum()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return list;
		}
		
		 public boolean updateCovrValidAndCurrtoFlag(Covrpf covrpf){
			 boolean isUpdated = false;
			 String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG=?, CURRTO=?, JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
			 PreparedStatement ps = getPrepareStatement(SQL_COVR_UPDATE);
	      
	         try {
	        	     ps.setString(1, covrpf.getValidflag());
	        		 ps.setInt(2, covrpf.getCurrto());
	                 ps.setString(3, getJobnm());
	                 ps.setString(4, getUsrprf());
	                 ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
	                 ps.setLong(6, covrpf.getUniqueNumber());
	                 
	                 ps.executeUpdate();
	        	     isUpdated = true;
	         } catch (SQLException e) {
			LOGGER.error("updateCovrValidFlag()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(ps, null);
	         }
	         return isUpdated;
		 }
		 
		 public boolean updateCovrPstatcode(Covrpf covrpf){
			 boolean isUpdated = false;
			 String SQL_COVR_UPDATE = "UPDATE COVRPF SET PSTATCODE=?,  JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
			 PreparedStatement ps = getPrepareStatement(SQL_COVR_UPDATE);
	      
	         try {
	        	     ps.setString(1, covrpf.getPstatcode());
	                 ps.setString(2, getJobnm());
	                 ps.setString(3, getUsrprf());
	                 ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	                 ps.setLong(5, covrpf.getUniqueNumber());
	                 ps.executeUpdate();
	        	     isUpdated = true;
	         } catch (SQLException e) {
			LOGGER.error("updateCovrValidFlag()", e); /* IJTI-1479 */
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(ps, null);
	         }
	         return isUpdated;
		 }
		 
			public Covrpf getCovrByUniqueNum(long uniqueNumber) {

				StringBuilder sql = new StringBuilder("SELECT * FROM COVRPF WHERE UNIQUE_NUMBER=? ");
				sql.append(" order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");

				PreparedStatement ps = getPrepareStatement(sql.toString());
				ResultSet rs = null;
				Covrpf covrpf = null;
				try {
					ps.setLong(1, uniqueNumber);
					rs = ps.executeQuery();

					if (rs.next()) {
						covrpf = new Covrpf();
						
						covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
						covrpf.setChdrcoy(rs.getString("CHDRCOY"));
		                covrpf.setChdrnum(rs.getString("CHDRNUM"));
		                covrpf.setLife(rs.getString("LIFE"));
		                covrpf.setJlife(rs.getString("JLIFE"));
		                covrpf.setCoverage(rs.getString("COVERAGE"));
		                covrpf.setRider(rs.getString("RIDER"));
		                covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
		                covrpf.setValidflag(rs.getString("VALIDFLAG"));
		                covrpf.setTranno(rs.getInt("TRANNO"));
		                covrpf.setCurrfrom(rs.getInt("CURRFROM"));
		                covrpf.setCurrto(rs.getInt("CURRTO"));
		                covrpf.setStatcode(rs.getString("STATCODE"));
		                covrpf.setPstatcode(rs.getString("PSTATCODE"));             
		                covrpf.setCpiDate(rs.getInt("CPIDTE"));
		                covrpf.setIndexationInd(rs.getString("INDXIN"));
		                covrpf.setCrtable(rs.getString("CRTABLE"));
		                covrpf.setCrrcd(rs.getInt("CRRCD"));
		                covrpf.setConvertInitialUnits(rs.getInt("CBCVIN"));
		                covrpf.setPremCurrency(rs.getString("PRMCUR"));
		                covrpf.setPremCessDate(rs.getInt("PCESDTE"));
		                covrpf.setPstatcode(rs.getString("PSTATCODE"));
		                covrpf.setSingp(rs.getBigDecimal("SINGP"));
		                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
		                covrpf.setRerateDate(rs.getInt("RRTDAT"));
		                covrpf.setRerateFromDate(rs.getInt("RRTFRM"));
		                covrpf.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
		                covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
		                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
		                covrpf.setMortcls(rs.getString("MORTCLS"));
		                covrpf.setSumins(rs.getBigDecimal("SUMINS"));
		                covrpf.setRiskCessDate(rs.getInt("RCESDTE"));
		                covrpf.setZclstate(rs.getString("ZCLSTATE"));//ILIFE-8509
		                covrpf.setZstpduty01(rs.getBigDecimal("ZSTPDUTY01"));
		                covrpf.setTpdtype(rs.getString("TPDTYPE"));
					}

				} catch (SQLException e) {
			LOGGER.error("getCovrByUniqueNum()", e); /* IJTI-1479 */
					throw new SQLRuntimeException(e);
				} finally {
					close(ps, rs);
				}
				return covrpf;
			}
			
			
		    public void insertCovrRecord(List<Covrpf> covrList) {
		        if (covrList != null && !covrList.isEmpty()) {
		           
		    		StringBuilder insertSql = new StringBuilder();
		    		insertSql.append(" INSERT INTO VM1DTA.COVRPF (CHDRCOY, CHDRNUM, CBCVIN, COVERAGE, CRRCD, CRTABLE, CURRFROM, INSTPREM, JLIFE, LIFE, PLNSFX, PCESTRM, PRMCUR, PSTATCODE, RRTDAT, RIDER, RCESTRM, SINGP,");
		    		insertSql.append(" STATCODE, SUMINS, VALIDFLAG, ZLINSTPREM,");
		    		insertSql.append(" TRANNO, CURRTO, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS,");
		    		insertSql.append(" REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T,");
		    		insertSql.append(" LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE,"); 
		    		insertSql.append(" CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR, CBRVPR,");
		    		insertSql.append(" CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, USRPRF, JOBNM, DATIME, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZSTPDUTY01,COMMPREM,ZCLSTATE ) ");//IJS-406
		    		insertSql.append(" SELECT CHDRCOY, CHDRNUM, CBCVIN, COVERAGE, CRRCD, CRTABLE, CURRFROM, INSTPREM, JLIFE, LIFE, PLNSFX, PCESTRM, PRMCUR, ?, ?, RIDER, RCESTRM, SINGP,?, SUMINS, ?, ZLINSTPREM,?, ?, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS,REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T,LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, ?, CBANPR, CBRVPR,CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, ?, PAYRSEQNO, BAPPMETH, ZBINSTPREM, ?, ?, ?, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZSTPDUTY01,COMMPREM,ZCLSTATE ");
		    		insertSql.append(" FROM COVRPF WHERE UNIQUE_NUMBER=? ");
		    		PreparedStatement ps = getPrepareStatement(insertSql.toString());
		    		
		            try {
		                for (Covrpf c : covrList) {
		                	ps.setString(1, c.getPstatcode());
		                	ps.setInt(2, c.getRerateDate());
		                	ps.setString(3, c.getStatcode());
		                	ps.setString(4, c.getValidflag());
		                	ps.setInt(5, c.getTranno());
		                	ps.setInt(6, c.getCurrto());
		                	ps.setInt(7, c.getBenBillDate());
		                	ps.setBigDecimal(8, c.getCoverageDebt());
		                    // JOBNM,USRPRF,DATIME
		                    ps.setString(9, getUsrprf());
		                    ps.setString(10, getJobnm());
		                    ps.setTimestamp(11, new Timestamp(System.currentTimeMillis()));
							ps.setLong(12, c.getUniqueNumber());					
		                    ps.addBatch();
		                }
		                ps.executeBatch();
		            } catch (SQLException e) {
				LOGGER.error("insertCovrRecord()", e); /* IJTI-1479 */
		                throw new SQLRuntimeException(e);
		            } finally {
		                close(ps, null);
		            }
		        }
		    }
		    
		    public Map<String, List<Covrpf>> readCovrData(String company,List<String> chdrnumList) {
				StringBuilder sqlstmt = new StringBuilder();	 
				sqlstmt.append(" SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,SUMINS,REINSTATED,TRANNO,INSTPREM,ZBINSTPREM,ZLINSTPREM,SUMINS,ZSTPDUTY01,STATCODE,PSTATCODE FROM COVRPF WHERE ");
				sqlstmt.append(" CHDRCOY = ? AND " );
				sqlstmt.append(getSqlInStr(" CHDRNUM ", chdrnumList));
				sqlstmt.append(" order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
				PreparedStatement ps = getPrepareStatement(sqlstmt.toString());
				ResultSet rs = null;
				Map<String, List<Covrpf>> covrpfMap = new LinkedHashMap<String, List<Covrpf>>();
				try {
					ps.setString(1, company);
					rs = executeQuery(ps);
					while(rs.next()){
						Covrpf covrpf =  new Covrpf();
						covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
						covrpf.setChdrcoy(rs.getString("CHDRCOY"));
						covrpf.setChdrnum(rs.getString("CHDRNUM"));
						covrpf.setLife(rs.getString("LIFE"));
					    covrpf.setLife(rs.getString("LIFE"));
		                covrpf.setJlife(rs.getString("JLIFE"));
		                covrpf.setCoverage(rs.getString("COVERAGE"));
		                covrpf.setRider(rs.getString("RIDER"));
		                covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
		                covrpf.setValidflag(rs.getString("VALIDFLAG"));
		                covrpf.setSumins(rs.getBigDecimal("SUMINS"));
		                covrpf.setReinstated(rs.getString("REINSTATED"));//ILIFE-8509
		                covrpf.setTranno(rs.getInt("TRANNO"));
		                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
		                covrpf.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
		                covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
		                covrpf.setSumins(rs.getBigDecimal("SUMINS"));
		                covrpf.setZstpduty01(rs.getBigDecimal("ZSTPDUTY01"));
		                covrpf.setStatcode(rs.getString("STATCODE"));
		                covrpf.setPstatcode(rs.getString("PSTATCODE"));
						if (covrpfMap.containsKey(covrpf.getChdrnum())) {
							covrpfMap.get(covrpf.getChdrnum()).add(covrpf);
						} else {
							List<Covrpf> ustmList = new ArrayList<Covrpf>();
							ustmList.add(covrpf);
							covrpfMap.put(covrpf.getChdrnum().trim(), ustmList);
						}
					}
				} catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e); /* IJTI-1479 */
					throw new SQLRuntimeException(e);
				} finally {
					close(ps, rs);
				}
				return covrpfMap;
				
			}

		      public void updateCpiDate(List<Covrpf> uniqueList) {
		            if (uniqueList!=null && !uniqueList.isEmpty()) {
		                StringBuilder updateSql = new StringBuilder();
		                updateSql.append(" UPDATE COVRPF SET CPIDTE=?, USRPRF=?, JOBNM=?, DATIME=? WHERE UNIQUE_NUMBER=? ");
		                PreparedStatement ps = getPrepareStatement(updateSql.toString());
		                try {
		                    for (Covrpf covrpf : uniqueList) {
		                        ps.setInt(1, covrpf.getCpiDate());
                                ps.setString(2, getUsrprf());
                                ps.setString(3, getJobnm());
                                ps.setTimestamp(4, getDatime());
		                        ps.setLong(5, covrpf.getUniqueNumber());
		                        ps.addBatch();
		                    }
		                    ps.executeBatch();
		                } catch (SQLException e) {
				LOGGER.error("updateCpiDate()", e); /* IJTI-1479 */
		                    throw new SQLRuntimeException(e);
		                } finally {
		                    close(ps, null);
		                }
		            }
		        }
		      
	            public void insertCovrRecordForL2POLRNWL(List<Covrpf> covrList) {
	                if (covrList != null && !covrList.isEmpty()) {
	                   
	                    StringBuilder insertSql = new StringBuilder();
	                    insertSql.append(" INSERT INTO VM1DTA.COVRPF (CHDRCOY, CHDRNUM, CBCVIN, COVERAGE, CRRCD, CRTABLE, CURRFROM, INSTPREM, JLIFE, LIFE, PLNSFX, PCESTRM, PRMCUR, PSTATCODE, RRTDAT, RIDER, RCESTRM, SINGP,");
	                    insertSql.append(" STATCODE, SUMINS, VALIDFLAG, ZLINSTPREM,");
	                    insertSql.append(" TRANNO, CURRTO, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS,");
	                    insertSql.append(" REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T,");
	                    insertSql.append(" LIENCD, RATCLS, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE,"); 
	                    insertSql.append(" CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR, CBRVPR,");
	                    insertSql.append(" CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, USRPRF, JOBNM, DATIME, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ, INDXIN,RISKPREM, ZSTPDUTY01, ZCLSTATE, REINSTATED, TPDTYPE, PRORATEPREM ) ");
	                    insertSql.append(" SELECT CHDRCOY, CHDRNUM, CBCVIN, COVERAGE, CRRCD, CRTABLE, ?, INSTPREM, JLIFE, LIFE, PLNSFX, PCESTRM, PRMCUR, PSTATCODE, RRTDAT, RIDER, RCESTRM, SINGP,STATCODE, SUMINS, ?, ZLINSTPREM,?, ?, STFUND, STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, BCESTRM, SICURR, VARSI, MORTCLS,REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, ?, ?, ?, ?,LIENCD, RATCLS, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, CHGOPT, FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR, CBRVPR,CBUNST, CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, ?, ?, ?, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ,?,?, ZSTPDUTY01, ZCLSTATE, REINSTATED, TPDTYPE, PRORATEPREM ");
	                    insertSql.append(" FROM COVRPF WHERE UNIQUE_NUMBER=? ");
	                    PreparedStatement ps = getPrepareStatement(insertSql.toString());
	                    
	                    try {
	                        for (Covrpf c : covrList) {
	                            ps.setInt(1, c.getCurrfrom());
	                            ps.setString(2, c.getValidflag());
	                            ps.setInt(3, c.getTranno());
	                            ps.setInt(4, c.getCurrto());
	                            ps.setString(5, c.getTermid());
	                            ps.setInt(6, c.getTransactionDate());
	                            ps.setInt(7, c.getTransactionTime());
	                            ps.setInt(8, c.getUser());
	                            // JOBNM,USRPRF,DATIME
	                            ps.setString(9, getUsrprf());
	                            ps.setString(10, getJobnm());
	                            ps.setTimestamp(11, getDatime());
	                            ps.setString(12, c.getIndexationInd());
	                            if(null !=c.getRiskprem()) {
	                             ps.setBigDecimal(13, c.getRiskprem()); //ILIFE-7845
	                            }else{
	                            ps.setBigDecimal(13, BigDecimal.ZERO); //ILIFE-7845
	                            }
	                            ps.setLong(14, c.getUniqueNumber());            
	                            ps.addBatch();
	                        }
	                        ps.executeBatch();
	                    } catch (SQLException e) {
				LOGGER.error("insertCovrRecordForL2POLRNWL()", e); /* IJTI-1479 */
	                        throw new SQLRuntimeException(e);
	                    } finally {
	                        close(ps, null);
	                    }
	                }
	            }
	            
	            
	            public List<Covrpf> searchValidCovrpfRecord(String chdrcoy,String chdrnum, String life, String coverage, String rider){

	                StringBuilder sqlSelect = new StringBuilder();

	                sqlSelect.append("SELECT UNIQUE_NUMBER, ");
	                sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
	                sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM, ");
	                sqlSelect.append("CURRTO, STATCODE, PSTATCODE, STATREASN, CRRCD, ");
	                sqlSelect.append("ANBCCD, SEX, REPTCD01, REPTCD02, REPTCD03, ");
	                sqlSelect.append("REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, ");
	                sqlSelect.append("CRINST03, CRINST04, CRINST05, PRMCUR, TERMID, ");
	                sqlSelect.append("TRDT, TRTM, USER_T, STFUND, STSECT, ");
	                sqlSelect.append("STSSECT, CRTABLE, RCESDTE, PCESDTE, BCESDTE, ");
	                sqlSelect.append("NXTDTE, RCESAGE, PCESAGE, BCESAGE, RCESTRM, ");
	                sqlSelect.append("PCESTRM, BCESTRM, SUMINS, SICURR, VARSI, ");
	                sqlSelect.append("MORTCLS, LIENCD, RATCLS, INDXIN, BNUSIN, ");
	                sqlSelect.append("DPCD, DPAMT, DPIND, TMBEN, EMV01, ");
	                sqlSelect.append("EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, ");
	                sqlSelect.append("CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, ");
	                sqlSelect.append("CHGOPT, FUNDSP, PCAMTH, PCADAY, PCTMTH, ");
	                sqlSelect.append("PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, ");
	                sqlSelect.append("JLLSID, INSTPREM, SINGP, RRTDAT, RRTFRM, ");
	                sqlSelect.append("BBLDAT, CBANPR, CBCVIN, CBRVPR, CBUNST, ");
	                sqlSelect.append("CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, ");
	                sqlSelect.append("PAYRSEQNO, BAPPMETH, ZBINSTPREM, ZLINSTPREM, USRPRF, ");
	                sqlSelect.append("JOBNM, DATIME, LOADPER, RATEADJ, FLTMORT, ");
	                sqlSelect.append("PREMADJ, AGEADJ, LNKGNO, LNKGSUBREFNO ");//ILIFE-8248
	                sqlSelect.append("FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND VALIDFLAG ='1' ");
	                sqlSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX DESC, TRANNO DESC, UNIQUE_NUMBER DESC");

	                PreparedStatement psCovrpfSelect = getPrepareStatement(sqlSelect.toString());
	                ResultSet sqlCovrpfRs = null;
	                List<Covrpf> outputList = new ArrayList<Covrpf>();

	                try {

	                    psCovrpfSelect.setString(1, chdrcoy);
	                    psCovrpfSelect.setString(2, chdrnum);
	                    psCovrpfSelect.setString(3, life);
                        psCovrpfSelect.setString(4, coverage);
                        psCovrpfSelect.setString(5, rider);
	                    
	                    sqlCovrpfRs = executeQuery(psCovrpfSelect);

	                    while (sqlCovrpfRs.next()) {
	                        Covrpf covrpf = new Covrpf();
	                        covrpf.setUniqueNumber(sqlCovrpfRs.getInt("UNIQUE_NUMBER"));
	                        covrpf.setChdrcoy(sqlCovrpfRs.getString("CHDRCOY"));
	                        covrpf.setChdrnum(sqlCovrpfRs.getString("CHDRNUM"));
	                        covrpf.setLife(sqlCovrpfRs.getString("LIFE"));
	                        covrpf.setJlife(sqlCovrpfRs.getString("JLIFE"));
	                        covrpf.setCoverage(sqlCovrpfRs.getString("COVERAGE"));
	                        covrpf.setRider(sqlCovrpfRs.getString("RIDER"));
	                        covrpf.setPlanSuffix(sqlCovrpfRs.getInt("PLNSFX"));
	                        covrpf.setValidflag(sqlCovrpfRs.getString("VALIDFLAG"));
	                        covrpf.setTranno(sqlCovrpfRs.getInt("TRANNO"));
	                        covrpf.setCurrfrom(sqlCovrpfRs.getInt("CURRFROM"));
	                        covrpf.setCurrto(sqlCovrpfRs.getInt("CURRTO"));
	                        covrpf.setStatcode(sqlCovrpfRs.getString("STATCODE"));
	                        covrpf.setPstatcode(sqlCovrpfRs.getString("PSTATCODE"));
	                        covrpf.setStatreasn(sqlCovrpfRs.getString("STATREASN"));
	                        covrpf.setCrrcd(sqlCovrpfRs.getInt("CRRCD"));
	                        covrpf.setAnbAtCcd(sqlCovrpfRs.getInt("ANBCCD"));
	                        covrpf.setSex(sqlCovrpfRs.getString("SEX"));
	                        covrpf.setReptcd01(sqlCovrpfRs.getString("REPTCD01"));
	                        covrpf.setReptcd02(sqlCovrpfRs.getString("REPTCD02"));
	                        covrpf.setReptcd03(sqlCovrpfRs.getString("REPTCD03"));
	                        covrpf.setReptcd04(sqlCovrpfRs.getString("REPTCD04"));
	                        covrpf.setReptcd05(sqlCovrpfRs.getString("REPTCD05"));
	                        covrpf.setReptcd06(sqlCovrpfRs.getString("REPTCD06"));
	                        covrpf.setCrInstamt01(sqlCovrpfRs.getBigDecimal("CRINST01"));
	                        covrpf.setCrInstamt02(sqlCovrpfRs.getBigDecimal("CRINST02"));
	                        covrpf.setCrInstamt03(sqlCovrpfRs.getBigDecimal("CRINST03"));
	                        covrpf.setCrInstamt04(sqlCovrpfRs.getBigDecimal("CRINST04"));
	                        covrpf.setCrInstamt05(sqlCovrpfRs.getBigDecimal("CRINST05"));
	                        covrpf.setPremCurrency(sqlCovrpfRs.getString("PRMCUR"));
	                        covrpf.setTermid(sqlCovrpfRs.getString("TERMID"));
	                        covrpf.setTransactionDate(sqlCovrpfRs.getInt("TRDT"));
	                        covrpf.setTransactionTime(sqlCovrpfRs.getInt("TRTM"));
	                        covrpf.setUser(sqlCovrpfRs.getInt("USER_T"));
	                        covrpf.setStatFund(sqlCovrpfRs.getString("STFUND"));
	                        covrpf.setStatSect(sqlCovrpfRs.getString("STSECT"));
	                        covrpf.setStatSubsect(sqlCovrpfRs.getString("STSSECT"));
	                        covrpf.setCrtable(sqlCovrpfRs.getString("CRTABLE"));
	                        covrpf.setRiskCessDate(sqlCovrpfRs.getInt("RCESDTE"));
	                        covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
	                        covrpf.setPremCessDate(sqlCovrpfRs.getInt("PCESDTE"));
	                        covrpf.setBenCessDate(sqlCovrpfRs.getInt("BCESDTE"));
	                        covrpf.setNextActDate(sqlCovrpfRs.getInt("NXTDTE"));
	                        covrpf.setRiskCessAge(sqlCovrpfRs.getInt("RCESAGE"));
	                        covrpf.setPremCessAge(sqlCovrpfRs.getInt("PCESAGE"));
	                        covrpf.setBenCessAge(sqlCovrpfRs.getInt("BCESAGE"));
	                        covrpf.setRiskCessTerm(sqlCovrpfRs.getInt("RCESTRM"));
	                        covrpf.setPremCessTerm(sqlCovrpfRs.getInt("PCESTRM"));
	                        covrpf.setBenCessTerm(sqlCovrpfRs.getInt("BCESTRM"));
	                        covrpf.setSumins(sqlCovrpfRs.getBigDecimal("SUMINS"));
	                        covrpf.setSicurr(sqlCovrpfRs.getString("SICURR"));
	                        covrpf.setVarSumInsured(sqlCovrpfRs.getBigDecimal("VARSI"));
	                        covrpf.setMortcls(sqlCovrpfRs.getString("MORTCLS"));
	                        covrpf.setLiencd(sqlCovrpfRs.getString("LIENCD"));
	                        covrpf.setRatingClass(sqlCovrpfRs.getString("RATCLS"));
	                        covrpf.setIndexationInd(sqlCovrpfRs.getString("INDXIN"));
	                        covrpf.setBonusInd(sqlCovrpfRs.getString("BNUSIN"));
	                        covrpf.setDeferPerdCode(sqlCovrpfRs.getString("DPCD"));
	                        covrpf.setDeferPerdAmt(sqlCovrpfRs.getBigDecimal("DPAMT"));
	                        covrpf.setDeferPerdInd(sqlCovrpfRs.getString("DPIND"));
	                        covrpf.setTotMthlyBenefit(sqlCovrpfRs.getBigDecimal("TMBEN"));
	                        covrpf.setSingp(sqlCovrpfRs.getBigDecimal("SINGP"));
	                        covrpf.setInstprem(sqlCovrpfRs.getBigDecimal("INSTPREM"));
	                        covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
	                        covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
	                        covrpf.setRerateDate(sqlCovrpfRs.getInt("RRTDAT"));
	                        covrpf.setRerateFromDate(sqlCovrpfRs.getInt("RRTFRM"));
	                        covrpf.setBenBillDate(sqlCovrpfRs.getInt("BBLDAT"));
	                        covrpf.setAnnivProcDate(sqlCovrpfRs.getInt("CBANPR"));
	                        covrpf.setEstMatValue01(sqlCovrpfRs.getBigDecimal("EMV01"));
	                        covrpf.setEstMatValue02(sqlCovrpfRs.getBigDecimal("EMV02"));
	                        covrpf.setEstMatDate01(sqlCovrpfRs.getInt("EMVDTE01"));
	                        covrpf.setEstMatDate02(sqlCovrpfRs.getInt("EMVDTE02"));
	                        covrpf.setEstMatInt01(sqlCovrpfRs.getBigDecimal("EMVINT01"));
	                        covrpf.setEstMatInt02(sqlCovrpfRs.getBigDecimal("EMVINT02"));
	                        covrpf.setCampaign(sqlCovrpfRs.getString("CAMPAIGN"));
	                        covrpf.setStatSumins(sqlCovrpfRs.getBigDecimal("STSMIN"));
	                        covrpf.setRtrnyrs(sqlCovrpfRs.getInt("RTRNYRS"));
	                        covrpf.setPremCessAgeMth(sqlCovrpfRs.getInt("PCAMTH"));
	                        covrpf.setPremCessAgeDay(sqlCovrpfRs.getInt("PCADAY"));
	                        covrpf.setPremCessTermMth(sqlCovrpfRs.getInt("PCTMTH"));
	                        covrpf.setPremCessTermDay(sqlCovrpfRs.getInt("PCTDAY"));
	                        covrpf.setRiskCessAgeMth(sqlCovrpfRs.getInt("RCAMTH"));
	                        covrpf.setRiskCessAgeDay(sqlCovrpfRs.getInt("RCADAY"));
	                        covrpf.setRiskCessTermMth(sqlCovrpfRs.getInt("RCTMTH"));
	                        covrpf.setRiskCessTermDay(sqlCovrpfRs.getInt("RCTDAY"));
	                        covrpf.setJlLsInd(sqlCovrpfRs.getString("JLLSID"));
	                        covrpf.setCoverageDebt(sqlCovrpfRs.getBigDecimal("CRDEBT"));
	                        covrpf.setReserveUnitsInd(sqlCovrpfRs.getString("RSUNIN"));
	                        covrpf.setReserveUnitsDate(sqlCovrpfRs.getInt("RUNDTE"));
	                        covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
	                        covrpf.setCpiDate(sqlCovrpfRs.getInt("CPIDTE"));
	                        covrpf.setBappmeth(sqlCovrpfRs.getString("BAPPMETH"));
	                        covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
	                        covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
	                        covrpf.setUserProfile(sqlCovrpfRs.getString("USRPRF"));
	                        covrpf.setJobName(sqlCovrpfRs.getString("JOBNM"));
	                        covrpf.setDatime(sqlCovrpfRs.getString("DATIME"));
	                        covrpf.setLoadper(sqlCovrpfRs.getBigDecimal("LOADPER"));
	                        covrpf.setRateadj(sqlCovrpfRs.getBigDecimal("RATEADJ"));
	                        covrpf.setFltmort(sqlCovrpfRs.getBigDecimal("FLTMORT"));
	                        covrpf.setPremadj(sqlCovrpfRs.getBigDecimal("PREMADJ"));
	                        covrpf.setAgeadj(sqlCovrpfRs.getBigDecimal("AGEADJ"));
	                        covrpf.setLnkgno(sqlCovrpfRs.getString("LNKGNO"));//ILIFE-8248
	                        covrpf.setLnkgsubrefno(sqlCovrpfRs.getString("LNKGSUBREFNO"));//ILIFE-8248
	                        outputList.add(covrpf);
	                    }

	                } catch (SQLException e) {
	                    LOGGER.error("searchValidCovrpfRecord()", e);
	                    throw new SQLRuntimeException(e);
	                } finally {
	                    close(psCovrpfSelect, sqlCovrpfRs);
	                }
	                return outputList;
	            }
				
			 public boolean isExistCovrRecord(String chdrcoy,String chdrnum, String life, String coverage, String rider){

	                StringBuilder sqlSelect = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRNUM FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND ");
	          
	                sqlSelect.append("RIDER = ? AND VALIDFLAG='1' ");

	                PreparedStatement psCovrpfSelect = getPrepareStatement(sqlSelect.toString());
	                ResultSet sqlCovrpfRs = null;
	                boolean flag = false;

	                try {

	                    psCovrpfSelect.setString(1, chdrcoy);
	                    psCovrpfSelect.setString(2, chdrnum);
	                    psCovrpfSelect.setString(3, life);
                        psCovrpfSelect.setString(4, coverage);
                        psCovrpfSelect.setString(5, rider);
	                    
	                    sqlCovrpfRs = executeQuery(psCovrpfSelect);

	                    if (sqlCovrpfRs.next()) {
	                      flag = true;
	                    }

	                } catch (SQLException e) {
	                    LOGGER.error("isExistCovrRecord()", e);
	                    throw new SQLRuntimeException(e);
	                } finally {
	                    close(psCovrpfSelect, sqlCovrpfRs);
	                }
	                return flag;
	            }
			 
			  public List<Covrpf> searchCovrmjaByChdrnumCoy(String chdrnum,String coy){
			        StringBuilder sqlCovrSelect1 = new StringBuilder(
			                "SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,"
			                + "CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,"
			                + "TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RSUNIN,RUNDTE,CHGOPT,FUNDSP,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,SINGP,INSTPREM,ZBINSTPREM,ZLINSTPREM,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,RRTDAT,RRTFRM,BBLDAT,CRDEBT,CBANPR,PAYRSEQNO,BAPPMETH,ZSTPDUTY01,ZCLSTATE,LNKGNO,LNKGSUBREFNO,TPDTYPE,REINSTATED ");//ILIFE-8248
			        sqlCovrSelect1.append(" FROM COVRMJA  WHERE CHDRNUM=? AND CHDRCOY=?  ");
			        sqlCovrSelect1
			                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
			        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
			        ResultSet sqlrs = null;
			        List<Covrpf> covrList = new ArrayList<>();
					try {
			        	psCovrSelect.setString(1, chdrnum);
			        	psCovrSelect.setString(2, coy);
			        	sqlrs = executeQuery(psCovrSelect);
			            while (sqlrs.next()) {
			                Covrpf c = new Covrpf();
			                c.setChdrcoy(sqlrs.getString("chdrcoy"));
			                c.setChdrnum(sqlrs.getString("chdrnum"));
			                c.setLife(sqlrs.getString("life"));
			                c.setJlife(sqlrs.getString("jlife"));
			                c.setCoverage(sqlrs.getString("coverage"));
			                c.setRider(sqlrs.getString("rider"));
			                c.setPlanSuffix(sqlrs.getInt("plnsfx"));
			                c.setValidflag(sqlrs.getString("validflag"));
			                c.setTranno(sqlrs.getInt("tranno"));
			                c.setCurrfrom(sqlrs.getInt("currfrom"));
			                c.setCurrto(sqlrs.getInt("currto"));
			                c.setStatcode(sqlrs.getString("statcode"));
			                c.setPstatcode(sqlrs.getString("pstatcode"));
			                c.setStatreasn(sqlrs.getString("statreasn"));
			                c.setCrrcd(sqlrs.getInt("crrcd"));
			                c.setAnbAtCcd(sqlrs.getInt("anbccd"));
			                c.setSex(sqlrs.getString("sex"));
			                c.setReptcd01(sqlrs.getString("reptcd01"));
			                c.setReptcd02(sqlrs.getString("reptcd02"));
			                c.setReptcd03(sqlrs.getString("reptcd03"));
			                c.setReptcd04(sqlrs.getString("reptcd04"));
			                c.setReptcd05(sqlrs.getString("reptcd05"));
			                c.setReptcd06(sqlrs.getString("reptcd06"));
			                c.setCrInstamt01(sqlrs.getBigDecimal("crinst01"));
			                c.setCrInstamt02(sqlrs.getBigDecimal("crinst02"));
			                c.setCrInstamt03(sqlrs.getBigDecimal("crinst03"));
			                c.setCrInstamt04(sqlrs.getBigDecimal("crinst04"));
			                c.setCrInstamt05(sqlrs.getBigDecimal("crinst05"));
			                c.setPremCurrency(sqlrs.getString("prmcur"));
			                c.setTermid(sqlrs.getString("termid"));
			                c.setTransactionDate(sqlrs.getInt("trdt"));
			                c.setTransactionTime(sqlrs.getInt("trtm"));
			                c.setUser(sqlrs.getInt("user_t"));
			                c.setStatFund(sqlrs.getString("stfund"));
			                c.setStatSect(sqlrs.getString("stsect"));
			                c.setStatSubsect(sqlrs.getString("stssect"));
			                c.setCrtable(sqlrs.getString("crtable"));
			                c.setRiskCessDate(sqlrs.getInt("rcesdte"));
			                c.setPremCessDate(sqlrs.getInt("pcesdte"));
			                c.setBenCessDate(sqlrs.getInt("bcesdte"));
			                c.setNextActDate(sqlrs.getInt("nxtdte"));
			                c.setReserveUnitsInd(sqlrs.getString("rsunin"));
			                c.setReserveUnitsDate(sqlrs.getInt("rundte"));
			                c.setChargeOptionsInd(sqlrs.getString("chgopt"));
			                c.setFundSplitPlan(sqlrs.getString("fundsp"));
			                c.setConvertInitialUnits(sqlrs.getInt("cbcvin"));
			                c.setReviewProcessing(sqlrs.getInt("cbrvpr"));
			                c.setUnitStatementDate(sqlrs.getInt("cbunst"));
			                c.setCpiDate(sqlrs.getInt("cpidte"));
			                c.setInitUnitCancDate(sqlrs.getInt("icandt"));
			                c.setExtraAllocDate(sqlrs.getInt("exaldt"));
			                c.setInitUnitIncrsDate(sqlrs.getInt("iincdt"));
			                c.setRiskCessAge(sqlrs.getInt("rcesage"));
			                c.setPremCessAge(sqlrs.getInt("pcesage"));
			                c.setBenCessAge(sqlrs.getInt("bcesage"));
			                c.setRiskCessTerm(sqlrs.getInt("rcestrm"));
			                c.setPremCessTerm(sqlrs.getInt("pcestrm"));
			                c.setBenCessTerm(sqlrs.getInt("bcestrm"));
			                c.setSumins(sqlrs.getBigDecimal("sumins"));
			                c.setSicurr(sqlrs.getString("sicurr"));
			                c.setVarSumInsured(sqlrs.getBigDecimal("varsi"));
			                c.setMortcls(sqlrs.getString("mortcls"));
			                c.setLiencd(sqlrs.getString("liencd"));
			                c.setRatingClass(sqlrs.getString("ratcls"));
			                c.setIndexationInd(sqlrs.getString("indxin"));
			                c.setBonusInd(sqlrs.getString("bnusin"));
			                c.setDeferPerdCode(sqlrs.getString("dpcd"));
			                c.setDeferPerdAmt(sqlrs.getBigDecimal("dpamt"));
			                c.setDeferPerdInd(sqlrs.getString("dpind"));
			                c.setTotMthlyBenefit(sqlrs.getBigDecimal("tmben"));
			                c.setSingp(sqlrs.getBigDecimal("singp"));
			                c.setInstprem(sqlrs.getBigDecimal("instprem"));
			                c.setZbinstprem(sqlrs.getBigDecimal("zbinstprem"));
			                c.setZlinstprem(sqlrs.getBigDecimal("zlinstprem"));
			                c.setEstMatValue01(sqlrs.getBigDecimal("emv01"));
			                c.setEstMatValue02(sqlrs.getBigDecimal("emv02"));
			                c.setEstMatDate01(sqlrs.getInt("emvdte01"));
			                c.setEstMatDate02(sqlrs.getInt("emvdte02"));
			                c.setEstMatInt01(sqlrs.getBigDecimal("emvint01"));
			                c.setEstMatInt02(sqlrs.getBigDecimal("emvint02"));
			                c.setCampaign(sqlrs.getString("campaign"));
			                c.setStatSumins(sqlrs.getBigDecimal("stsmin"));
			                c.setRtrnyrs(sqlrs.getInt("rtrnyrs"));
			                c.setPremCessAgeMth(sqlrs.getInt("pcamth"));
			                c.setPremCessAgeDay(sqlrs.getInt("pcaday"));
			                c.setPremCessTermMth(sqlrs.getInt("pctmth"));
			                c.setPremCessTermDay(sqlrs.getInt("pctday"));
			                c.setRiskCessAgeMth(sqlrs.getInt("rcamth"));
			                c.setRiskCessAgeDay(sqlrs.getInt("rcaday"));
			                c.setRiskCessTermMth(sqlrs.getInt("rctmth"));
			                c.setRiskCessTermDay(sqlrs.getInt("rctday"));
			                c.setJlLsInd(sqlrs.getString("jllsid"));
			                c.setRerateDate(sqlrs.getInt("rrtdat"));
			                c.setRerateFromDate(sqlrs.getInt("rrtfrm"));
			                c.setBenBillDate(sqlrs.getInt("bbldat"));
			                c.setCoverageDebt(sqlrs.getBigDecimal("crdebt"));
			                c.setAnnivProcDate(sqlrs.getInt("cbanpr"));
			                c.setPayrseqno(sqlrs.getInt("payrseqno"));
			                c.setBappmeth(sqlrs.getString("bappmeth"));
			                c.setZstpduty01(sqlrs.getBigDecimal("ZSTPDUTY01"));
			                c.setLnkgno(sqlrs.getString("LNKGNO"));//ILIFE-8248
			                c.setLnkgsubrefno(sqlrs.getString("LNKGSUBREFNO"));//ILIFE-8248
			                c.setZclstate(sqlrs.getString("ZCLSTATE"));//ILIFE-8509
			                c.setTpdtype(sqlrs.getString("TPDTYPE"));
			                c.setReinstated(sqlrs.getString("REINSTATED"));
			                c.setUniqueNumber(sqlrs.getLong("unique_number"));
			                covrList.add(c);
			            }
			
			        } catch (SQLException e) {
			LOGGER.error("searchCovrmjaByChdrnumCoy()", e); /* IJTI-1479 */
			            throw new SQLRuntimeException(e);
			        } finally {
			            close(psCovrSelect, sqlrs);
			        }
			        return covrList;
			 } 
			 
			 public Map<String, List<Covrpf>> searchCovrlnb(String coy, List<String> chdrnumList) {
			        StringBuilder sqlCovrSelect1 = new StringBuilder(
			                "SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,PRORATEPREM,UNIQUE_NUMBER,ZSTPDUTY01,ZCLSTATE ");//ILIFE-8509
			        sqlCovrSelect1.append("FROM COVRPF WHERE CHDRCOY=? AND ");
			        sqlCovrSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
			        sqlCovrSelect1
			                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
			        PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
			        ResultSet sqlcovrpf1rs = null;
			        Map<String, List<Covrpf>> covrMap = new HashMap<String, List<Covrpf>>();
			        try {
			            psCovrSelect.setString(1, coy);
			            sqlcovrpf1rs = executeQuery(psCovrSelect);

			            while (sqlcovrpf1rs.next()) {
			                Covrpf covrpf = new Covrpf();
			                covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
			                covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
			                covrpf.setLife(sqlcovrpf1rs.getString(3));
			                covrpf.setJlife(sqlcovrpf1rs.getString(4));
			                covrpf.setCoverage(sqlcovrpf1rs.getString(5));
			                covrpf.setRider(sqlcovrpf1rs.getString(6));
			                covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(7));
			                covrpf.setValidflag(sqlcovrpf1rs.getString(8));
			                covrpf.setTranno(sqlcovrpf1rs.getInt(9));
			                covrpf.setCurrfrom(sqlcovrpf1rs.getInt(10));
			                covrpf.setCurrto(sqlcovrpf1rs.getInt(11));
			                covrpf.setStatcode(sqlcovrpf1rs.getString(12));
			                covrpf.setPstatcode(sqlcovrpf1rs.getString(13));
			                covrpf.setStatreasn(sqlcovrpf1rs.getString(14));
			                covrpf.setCrrcd(sqlcovrpf1rs.getInt(15));
			                covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(16));
			                covrpf.setSex(sqlcovrpf1rs.getString(17));
			                covrpf.setReptcd01(sqlcovrpf1rs.getString(18));
			                covrpf.setReptcd02(sqlcovrpf1rs.getString(19));
			                covrpf.setReptcd03(sqlcovrpf1rs.getString(20));
			                covrpf.setReptcd04(sqlcovrpf1rs.getString(21));
			                covrpf.setReptcd05(sqlcovrpf1rs.getString(22));
			                covrpf.setReptcd06(sqlcovrpf1rs.getString(23));
			                covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal(24));
			                covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal(25));
			                covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal(26));
			                covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal(27));
			                covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal(28));
			                covrpf.setPremCurrency(sqlcovrpf1rs.getString(29));
			                covrpf.setTermid(sqlcovrpf1rs.getString(30));
			                covrpf.setTransactionDate(sqlcovrpf1rs.getInt(31));
			                covrpf.setTransactionTime(sqlcovrpf1rs.getInt(32));
			                covrpf.setUser(sqlcovrpf1rs.getInt(33));
			                covrpf.setStatFund(sqlcovrpf1rs.getString(34));
			                covrpf.setStatSect(sqlcovrpf1rs.getString(35));
			                covrpf.setStatSubsect(sqlcovrpf1rs.getString(36));
			                covrpf.setCrtable(sqlcovrpf1rs.getString(37));
			                covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(38));
			                covrpf.setPremCessDate(sqlcovrpf1rs.getInt(39));
			                covrpf.setBenCessDate(sqlcovrpf1rs.getInt(40));
			                covrpf.setNextActDate(sqlcovrpf1rs.getInt(41));
			                covrpf.setRiskCessAge(sqlcovrpf1rs.getInt(42));
			                covrpf.setPremCessAge(sqlcovrpf1rs.getInt(43));
			                covrpf.setBenCessAge(sqlcovrpf1rs.getInt(44));
			                covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt(45));
			                covrpf.setPremCessTerm(sqlcovrpf1rs.getInt(46));
			                covrpf.setBenCessTerm(sqlcovrpf1rs.getInt(47));
			                covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(48));
			                covrpf.setSicurr(sqlcovrpf1rs.getString(49));
			                covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal(50));
			                covrpf.setMortcls(sqlcovrpf1rs.getString(51));
			                covrpf.setLiencd(sqlcovrpf1rs.getString(52));
			                covrpf.setRatingClass(sqlcovrpf1rs.getString(53));
			                covrpf.setIndexationInd(sqlcovrpf1rs.getString(54));
			                covrpf.setBonusInd(sqlcovrpf1rs.getString(55));
			                covrpf.setDeferPerdCode(sqlcovrpf1rs.getString(56));
			                covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal(57));
			                covrpf.setDeferPerdInd(sqlcovrpf1rs.getString(58));
			                covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal(59));
			                covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal(60));
			                covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal(61));
			                covrpf.setEstMatDate01(sqlcovrpf1rs.getInt(62));
			                covrpf.setEstMatDate02(sqlcovrpf1rs.getInt(63));
			                covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal(64));
			                covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal(65));
			                covrpf.setCampaign(sqlcovrpf1rs.getString(66));
			                covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal(67));
			                covrpf.setRtrnyrs(sqlcovrpf1rs.getInt(68));
			                covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString(69));
			                covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt(70));
			                covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString(71));
			                covrpf.setFundSplitPlan(sqlcovrpf1rs.getString(72));
			                covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt(73));
			                covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt(74));
			                covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt(75));
			                covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt(76));
			                covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt(77));
			                covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt(78));
			                covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt(79));
			                covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt(80));
			                covrpf.setJlLsInd(sqlcovrpf1rs.getString(81));
			                covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal(82));
			                covrpf.setSingp(sqlcovrpf1rs.getBigDecimal(83));
			                covrpf.setRerateDate(sqlcovrpf1rs.getInt(84));
			                covrpf.setRerateFromDate(sqlcovrpf1rs.getInt(85));
			                covrpf.setBenBillDate(sqlcovrpf1rs.getInt(86));
			                covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt(87));
			                covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt(88));
			                covrpf.setReviewProcessing(sqlcovrpf1rs.getInt(89));
			                covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(90));
			                covrpf.setCpiDate(sqlcovrpf1rs.getInt(91));
			                covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt(92));
			                covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt(93));
			                covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt(94));
			                covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal(95));
			                covrpf.setPayrseqno(sqlcovrpf1rs.getInt(96));
			                covrpf.setBappmeth(sqlcovrpf1rs.getString(97));
			                covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal(98));
			                covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal(99));
			                covrpf.setProrateprem(sqlcovrpf1rs.getBigDecimal(100));//ILIFE-8509
			                covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(101));
							covrpf.setZstpduty01(sqlcovrpf1rs.getBigDecimal(102));
							covrpf.setZclstate(sqlcovrpf1rs.getString(103));//IJS-424
			                if (covrMap.containsKey(covrpf.getChdrnum())) {
			                    covrMap.get(covrpf.getChdrnum()).add(covrpf);
			                } else {
			                    List<Covrpf> chdrList = new ArrayList<Covrpf>();
			                    chdrList.add(covrpf);
			                    covrMap.put(covrpf.getChdrnum(), chdrList);
			                }
			            }

			        } catch (SQLException e) {
			LOGGER.error("searchCovrMap()", e); /* IJTI-1479 */
			            throw new SQLRuntimeException(e);
			        } finally {
			            close(psCovrSelect, sqlcovrpf1rs);
			        }
			        return covrMap;

			    }		
			  public Covrpf getCovrpfData(String chdrcoy,String chdrnum,String life,String coverage,String crtable){
		    		Covrpf covrpf = null;
		    		PreparedStatement psCovrSelect = null;
		    		ResultSet sqlcovrpf1rs = null;
		    		StringBuilder sqlCovrSelectBuilder = new StringBuilder();

		    		sqlCovrSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		    		sqlCovrSelectBuilder.append(" JOBNM, UNIQUE_NUMBER, SUMINS, RCESDTE, CBUNST, ANBCCD , SEX , MORTCLS, CRRCD, PSTATCODE, ANBCCD  ");
		    		sqlCovrSelectBuilder.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE=? AND COVERAGE=? AND CRTABLE=? AND VALIDFLAG='1'");
		    		sqlCovrSelectBuilder.append(" ORDER BY ");
		    		sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		    		psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
		    		
		    		try {
		    			psCovrSelect.setString(1, chdrcoy);
		    			psCovrSelect.setString(2, chdrnum);
		    			psCovrSelect.setString(3, life);
		    			psCovrSelect.setString(4, coverage);
		    			psCovrSelect.setString(5, crtable);
		    			sqlcovrpf1rs = executeQuery(psCovrSelect);
		    			while (sqlcovrpf1rs.next()) {
		    				covrpf = new Covrpf();
		    				covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
		    				covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
		    				covrpf.setLife(sqlcovrpf1rs.getString(3));
		    				covrpf.setCoverage(sqlcovrpf1rs.getString(4));
		    				covrpf.setRider(sqlcovrpf1rs.getString(5));
		    				covrpf.setCrtable(sqlcovrpf1rs.getString(6));
		    				covrpf.setUserProfile(sqlcovrpf1rs.getString(7));
		    				covrpf.setJobName(sqlcovrpf1rs.getString(8));
		    				covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(9));
		    				covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(10));
		    				covrpf.setRiskCessDate(sqlcovrpf1rs.getInt(11));
		    				covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt(12));
		    				covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(13));
		    				covrpf.setSex(sqlcovrpf1rs.getString(14));
		    				covrpf.setMortcls(sqlcovrpf1rs.getString(15));
		    				covrpf.setCrrcd(sqlcovrpf1rs.getInt(16));
		    				covrpf.setPstatcode(sqlcovrpf1rs.getString(17));
		    				covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt(18));
		    			}

		    		} catch (SQLException e) {
			LOGGER.error("searchCovrRecordForContract()", e); /* IJTI-1479 */
		    			throw new SQLRuntimeException(e);
		    		} finally {
		    			close(psCovrSelect, sqlcovrpf1rs);
		    		}
		    		return covrpf;
		    	}
			@Override
			public void updateCoverPf(Covrpf covrpf) {
				 String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG=?  WHERE CHDRCOY=? AND CHDRNUM=? AND CRTABLE=? AND VALIDFLAG='1' ";
				 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
		      
		         try {		        
		        	 	 psCovrUpdate.setString(1, covrpf.getValidflag());		        			                
		                 psCovrUpdate.setString(2, covrpf.getChdrcoy());
		                 psCovrUpdate.setString(3, covrpf.getChdrnum());
		                 psCovrUpdate.setString(4, covrpf.getCrtable());		               
		               
		                 psCovrUpdate.executeUpdate();
		         } catch (SQLException e) {
			LOGGER.error("updateCoverPf()", e); /* IJTI-1479 */
		             throw new SQLRuntimeException(e);
		         } finally {
		             close(psCovrUpdate, null);
		         }
			 }
			@Override
			public void updateCoverPfStatus(Covrpf covrpf) {
				 String SQL_COVR_UPDATE = "UPDATE COVRPF SET PSTATCODE=?  WHERE CHDRCOY=? AND CHDRNUM=? AND CRTABLE=? AND VALIDFLAG=? ";
				 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
		      
		         try {			        	 	 			                
		                 psCovrUpdate.setString(1, covrpf.getPstatcode());
		                 psCovrUpdate.setString(2, covrpf.getChdrcoy());
		                 psCovrUpdate.setString(3, covrpf.getChdrnum());
		                 psCovrUpdate.setString(4, covrpf.getCrtable());
		                 psCovrUpdate.setString(5, covrpf.getValidflag());
		               
		                 psCovrUpdate.executeUpdate();
		         } catch (SQLException e) {
			LOGGER.error("updateCoverPfStatus()", e); /* IJTI-1479 */
		             throw new SQLRuntimeException(e);
		         } finally {
		             close(psCovrUpdate, null);
		         }
			 }
			@Override
			public void updateUniqueNo(Covrpf covrpf) {
			       
	            String SQL_CHDR_UPDATE = "UPDATE COVRPF SET VALIDFLAG=?, DATIME=?  WHERE CHDRNUM=? AND CRTABLE=? AND VALIDFLAG='2' AND UNIQUE_NUMBER = (select max(UNIQUE_NUMBER) from COVRPF WHERE VALIDFLAG='2' AND CHDRNUM=? AND CRTABLE=?) ";

	            PreparedStatement psCovrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
	          
	            try {                  
	            	psCovrUpdate.setString(1, covrpf.getValidflag());//IJTI-1485
	            	psCovrUpdate.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
	            	psCovrUpdate.setString(3, covrpf.getChdrnum().trim());
	            	psCovrUpdate.setString(4, covrpf.getCrtable().trim());
	            	psCovrUpdate.setString(5, covrpf.getChdrnum().trim());
	            	psCovrUpdate.setString(6, covrpf.getCrtable().trim());
	            	psCovrUpdate.executeUpdate();
	            } catch (SQLException e) {
			LOGGER.error("updateCovrbtDateTime()", e); /* IJTI-1479 */
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(psCovrUpdate, null);
	            }
	        
	    }
			@Override
		public void deleteByFlag(String validflag, String chdrnum, String crtable) {

				 StringBuilder sb = new StringBuilder("");
					sb.append("DELETE FROM COVRPF");
					sb.append(" WHERE VALIDFLAG=? AND CHDRNUM=? AND CRTABLE=? ");
					LOGGER.info(sb.toString());
					PreparedStatement ps = null;
					ResultSet rs = null;
					try{
						ps = getPrepareStatement(sb.toString());	

						    ps.setString(1, validflag);//IJTI-1485
						    ps.setString(2, chdrnum.trim());
						    ps.setString(3, crtable.trim());
						 			    
						    ps.executeUpdate();				
							
						
					}catch (SQLException e) {
			LOGGER.error("deleteByFlag()", e); /* IJTI-1479 */
						throw new SQLRuntimeException(e);
					} finally {
						close(ps, rs);			
					}				
			 
		}
		
		  //ILIFE-7817
			  public boolean updateCrdebt(Covrpf covrpf) {
					StringBuilder sb = new StringBuilder();
					sb.append("UPDATE COVRPF SET CRDEBT=? WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG=? AND LIFE=? AND COVERAGE=? AND RIDER=?");
					PreparedStatement ps = null;
					try {
						ps = getPrepareStatement(sb.toString());
						ps.setBigDecimal(1, covrpf.getCoverageDebt());
						ps.setString(2, covrpf.getChdrcoy());
						ps.setString(3, covrpf.getChdrnum());
						ps.setString(4, covrpf.getValidflag());
						ps.setString(5, covrpf.getLife());
						ps.setString(6, covrpf.getCoverage());
						ps.setString(7, covrpf.getRider());
						if(ps.executeUpdate() > 0)
							return true;
					}
					catch(SQLException e) {
			LOGGER.error("updateCrdebt()", e); /* IJTI-1479 */
			            throw new SQLRuntimeException(e);
					}
					finally {
						close(ps, null);
					}
					return false;
				}

				
			@Override
			  public Covrpf getCovrpfData(String chdrcoy,String chdrnum,String crtable){
		    		Covrpf covrpf = null;
		    		PreparedStatement psCovrSelect = null;
		    		ResultSet sqlcovrpf1rs = null;
		    		StringBuilder sqlCovrSelectBuilder = new StringBuilder();

		    		sqlCovrSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		    		sqlCovrSelectBuilder.append(" JOBNM, UNIQUE_NUMBER, SUMINS ");
		    		sqlCovrSelectBuilder.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND CRTABLE=? AND VALIDFLAG='1' ");
		    		sqlCovrSelectBuilder.append(" ORDER BY ");
		    		sqlCovrSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		    		psCovrSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
		    		
		    		try {
		    			psCovrSelect.setString(1, chdrcoy);
		    			psCovrSelect.setString(2, chdrnum);
		    			psCovrSelect.setString(3, crtable);
		    			sqlcovrpf1rs = executeQuery(psCovrSelect);
		    			while (sqlcovrpf1rs.next()) {
		    				covrpf = new Covrpf();
		    				covrpf.setChdrcoy(sqlcovrpf1rs.getString(1));
		    				covrpf.setChdrnum(sqlcovrpf1rs.getString(2));
		    				covrpf.setLife(sqlcovrpf1rs.getString(3));
		    				covrpf.setCoverage(sqlcovrpf1rs.getString(4));
		    				covrpf.setRider(sqlcovrpf1rs.getString(5));
		    				covrpf.setCrtable(sqlcovrpf1rs.getString(6));
		    				covrpf.setUserProfile(sqlcovrpf1rs.getString(7));
		    				covrpf.setJobName(sqlcovrpf1rs.getString(8));
		    				covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(9));
		    				covrpf.setSumins(sqlcovrpf1rs.getBigDecimal(10));
		    			}

		    		} catch (SQLException e) {
			LOGGER.error("searchCovrRecordForContract()", e); /* IJTI-1479 */
		    			throw new SQLRuntimeException(e);
		    		} finally {
		    			close(psCovrSelect, sqlcovrpf1rs);
		    		}
		    		return covrpf;
		    	}

			@Override
			public void updatebyUniqueNo(Covrpf covrpf) {
			       
	            String SQL_CHDR_UPDATE = "UPDATE COVRPF SET VALIDFLAG=?, CURRTO=?, DATIME=?  WHERE CHDRNUM=? AND CRTABLE=? AND VALIDFLAG='2' AND UNIQUE_NUMBER = (select max(UNIQUE_NUMBER) from COVRPF WHERE VALIDFLAG='2' AND CHDRNUM=? AND CRTABLE=?) ";

	            PreparedStatement psCovrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
	          
	            try {                  
	            	psCovrUpdate.setString(1, covrpf.getValidflag());//IJTI-1485
	            	psCovrUpdate.setInt(2, covrpf.getCurrto());
	            	psCovrUpdate.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
	            	psCovrUpdate.setString(4, covrpf.getChdrnum().trim());
	            	psCovrUpdate.setString(5, covrpf.getCrtable().trim());
	            	psCovrUpdate.setString(6, covrpf.getChdrnum().trim());
	            	psCovrUpdate.setString(7, covrpf.getCrtable().trim());
	            	psCovrUpdate.executeUpdate();
	            } catch (SQLException e) {
			LOGGER.error("updatebyUniqueNo()", e); /* IJTI-1479 */
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(psCovrUpdate, null);
	            }
	        
	    }
			
		@Override
		public void updateCovrpfRecord(Covrpf covrpf) {
		       
            String SQL_CHDR_UPDATE = "UPDATE COVRPF SET cpidte=? WHERE CHDRNUM=? AND CHDRCOY = ? AND LIFE=? AND COVERAGE=? AND RIDER=? AND VALIDFLAG = '1'";

            PreparedStatement psCovrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
          
            try {                  
            	psCovrUpdate.setInt(1, covrpf.getCpiDate());
            	psCovrUpdate.setString(2, covrpf.getChdrnum());
            	psCovrUpdate.setString(3, covrpf.getChdrcoy());
            	psCovrUpdate.setString(4, covrpf.getLife());
            	psCovrUpdate.setString(5, covrpf.getCoverage());
            	psCovrUpdate.setString(6, covrpf.getRider());
            	psCovrUpdate.executeUpdate();
            } catch (SQLException e) {
			LOGGER.error("updateCovrpfRecord()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psCovrUpdate, null);
            }
        
    }
					
    public List<Covrpf> getCovrpfByTranno(String coy, String chdrnum) {

        StringBuilder sql = new StringBuilder("SELECT * FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM=? ");
        sql.append(" ORDER BY TRANNO DESC");
        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        List<Covrpf> list = new ArrayList<Covrpf>();
        Covrpf covrpf = null;
        try {
            ps.setString(1, coy);
            ps.setString(2, chdrnum);

            rs = ps.executeQuery();

            while (rs.next()) {
                covrpf = new Covrpf();
                covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                covrpf.setChdrcoy(rs.getString("CHDRCOY"));
                covrpf.setChdrnum(rs.getString("CHDRNUM"));
                covrpf.setLife(rs.getString("LIFE"));
                covrpf.setJlife(rs.getString("JLIFE"));
                covrpf.setCoverage(rs.getString("COVERAGE"));
                covrpf.setRider(rs.getString("RIDER"));
                covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
                covrpf.setValidflag(rs.getString("VALIDFLAG"));
                covrpf.setTranno(rs.getInt("TRANNO"));
                covrpf.setCurrfrom(rs.getInt("CURRFROM"));
                covrpf.setCurrto(rs.getInt("CURRTO"));
                covrpf.setStatcode(rs.getString("STATCODE"));
                covrpf.setPstatcode(rs.getString("PSTATCODE"));
                covrpf.setCpiDate(rs.getInt("CPIDTE"));
                covrpf.setIndexationInd(rs.getString("INDXIN"));
                covrpf.setCrtable(rs.getString("CRTABLE"));
                covrpf.setCrrcd(rs.getInt("CRRCD"));
                covrpf.setConvertInitialUnits(rs.getInt("CBCVIN"));
                covrpf.setPremCurrency(rs.getString("PRMCUR"));
                covrpf.setPremCessDate(rs.getInt("PCESDTE"));
                covrpf.setPstatcode(rs.getString("PSTATCODE"));
                covrpf.setSingp(rs.getBigDecimal("SINGP"));
                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
                covrpf.setRerateDate(rs.getInt("RRTDAT"));
                covrpf.setRerateFromDate(rs.getInt("RRTFRM"));
                covrpf.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
                covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
                covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
                covrpf.setMortcls(rs.getString("MORTCLS"));
                covrpf.setSumins(rs.getBigDecimal("SUMINS"));
                list.add(covrpf);
            }
        } catch (SQLException e) {
			LOGGER.error("getCovrpfByTranno()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return list;
    }
    public void deleteCovrpfRecord(List<Covrpf> covrpfList) {
        String sql = "DELETE FROM COVRPF WHERE UNIQUE_NUMBER=? ";
        PreparedStatement ps = getPrepareStatement(sql);
        try {
            for(Covrpf covrpf:covrpfList){
                ps.setLong(1, covrpf.getUniqueNumber());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
			LOGGER.error("deleteCovrpfRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    }

    public boolean updateCovrValidFlagTo1(List<Covrpf> updateCovrpfList){
        boolean isUpdated = false;
        String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG='1', CURRTO=?, JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
        PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);

        try {
            for(Covrpf c:updateCovrpfList){
                psCovrUpdate.setInt(1, c.getCurrto());
                psCovrUpdate.setString(2, getJobnm());
                psCovrUpdate.setString(3, getUsrprf());
                psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                psCovrUpdate.setLong(5, c.getUniqueNumber());
                psCovrUpdate.addBatch();
            }
            psCovrUpdate.executeBatch();
            isUpdated = true;
        } catch (SQLException e) {
			LOGGER.error("updateCovrValidFlagTo1()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psCovrUpdate, null);
        }
        return isUpdated;
    }

	@Override
	public void updateCovrpf(Covrpf covrpf) {
	       
        String SQL_CHDR_UPDATE = "UPDATE COVRPF SET RRTDAT=? WHERE CHDRNUM=? AND CHDRCOY = ? AND LIFE=? AND COVERAGE=? AND RIDER=? AND VALIDFLAG = '1'";

        PreparedStatement psCovrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
      
        try {                  
        	psCovrUpdate.setInt(1, covrpf.getRerateDate()); 
        	psCovrUpdate.setString(2, covrpf.getChdrnum());
        	psCovrUpdate.setString(3, covrpf.getChdrcoy());
        	psCovrUpdate.setString(4, covrpf.getLife());
        	psCovrUpdate.setString(5, covrpf.getCoverage());
        	psCovrUpdate.setString(6, covrpf.getRider());
        	psCovrUpdate.executeUpdate();
        } catch (SQLException e) {
			LOGGER.error("updateCovrpf()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psCovrUpdate, null);
        }
	}
		// ILIFE-8123 starts
		@Override
		public Covrpf getByTranno(String chdrcoy,String chdrnum,String tranno,String validflag) {
			Covrpf covrpfRec = null;
		    String sql_select  = "SELECT COVERAGE,LIFE"
		    		+ " FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND TRANNO = ? AND VALIDFLAG = ?";
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {

		    	psSelect = getPrepareStatement(sql_select);//IJTI-1485
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	psSelect.setInt(3, Integer.valueOf(tranno));
		    	psSelect.setString(4, validflag);
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		                
		        	         covrpfRec= new Covrpf();
		        	         covrpfRec.setChdrcoy(chdrcoy);
		        	         covrpfRec.setChdrnum(chdrnum);
		        	         covrpfRec.setLife(tranno);
		        	         covrpfRec.setValidflag(validflag);
		        	         covrpfRec.setCoverage(rs.getString(1));
		        	         covrpfRec.setLife(rs.getString(2));
		                	 
		                  }
		           
		    } catch (SQLException e) {
			LOGGER.error("readRecord()", e); /* IJTI-1479 */
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }
		    return covrpfRec;
		}
    //ILIFE-8123 ends 
		/*ILIFE-8098 Start */
		public List<Covrpf> getSingpremtype(String coy, String chdrnum) {

			StringBuilder sqlCovrSelect1 = new StringBuilder();

			sqlCovrSelect1.append("SELECT TOP 1 SINGPREMTYPE,SINGP FROM COVRPF ");
			sqlCovrSelect1.append(" WHERE CHDRCOY=? AND CHDRNUM=? and VALIDFLAG = '1' AND SINGPREMTYPE != '' OR  SINGPREMTYPE != Null ");
			sqlCovrSelect1.append(" ORDER BY UNIQUE_NUMBER DESC ");

			PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
			ResultSet sqlcovrpf1rs = null;
			Covrpf covrpf = null; 
			 List<Covrpf> list = new ArrayList<Covrpf>();
			try {
				psCovrSelect.setString(1, coy);
				psCovrSelect.setString(2, chdrnum);
				sqlcovrpf1rs = executeQuery(psCovrSelect);

				while (sqlcovrpf1rs.next()) {
					covrpf=new Covrpf();
					covrpf.setSingpremtype(sqlcovrpf1rs.getString("SINGPREMTYPE"));
					covrpf.setSingp(sqlcovrpf1rs.getBigDecimal("SINGP"));
					list.add(covrpf);
				}

			} catch (SQLException e) {
			LOGGER.error("getSingpremtype()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovrSelect, sqlcovrpf1rs);
			}
			return list;
		}
		/*ILIFE-8098 END */
		
		/*	START OF ILIFE-8179	*/
		public void updateCovrRPStat(List<Covrpf> covrpfList) {
			String sql = "UPDATE COVRPF SET STATCODE = ?, PSTATCODE = ?, INSTPREM = ?, ZBINSTPREM = ?, ZLINSTPREM = ?, SUMINS = ?, ZSTPDUTY01 = ?, VALIDFLAG = ?, REINSTATED = ?, JOBNM = ?, USRPRF = ?, DATIME = ? WHERE UNIQUE_NUMBER = ? ";
			PreparedStatement ps = getPrepareStatement(sql);
	        try {
	        	for (Covrpf covrpf : covrpfList) {
	        		ps.setString(1, covrpf.getStatcode());
	        	    ps.setString(2, covrpf.getPstatcode());
	        	    ps.setBigDecimal(3, covrpf.getInstprem());//ILIFE-8509
	        	    ps.setBigDecimal(4, covrpf.getZbinstprem());
	        	    ps.setBigDecimal(5, covrpf.getZlinstprem());
	        	    ps.setBigDecimal(6, covrpf.getSumins());
	        	    ps.setBigDecimal(7, covrpf.getZstpduty01());
	                ps.setString(8, covrpf.getValidflag());
	                ps.setString(9, covrpf.getReinstated());
	                ps.setString(10, getJobnm());
	                ps.setString(11, getUsrprf());
	                ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
	                ps.setLong(13, covrpf.getUniqueNumber());
	                ps.addBatch();
				}
	        	ps.executeBatch(); 
	        } catch (SQLException e) {
			LOGGER.error("updateCovrRPStat()", e); /* IJTI-1479 */
	        	throw new SQLRuntimeException(e);
	        } finally {
	        	close(ps, null);
	        }
		}
		/*	END OF ILIFE-8179	*/
		
		//ILB-566
		public void insertCovrRecord(Covrpf covrpf) {
	                  
	    		StringBuilder insertSql = new StringBuilder();
	    		insertSql.append("INSERT INTO COVRPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,ZSTPDUTY01,ZCLSTATE,USRPRF,JOBNM,DATIME)");
	    		insertSql.append(" SELECT CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,?,?,?,?,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,?,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,ZSTPDUTY01,ZCLSTATE,?,?,? ");
	    		insertSql.append(" FROM COVRPF WHERE UNIQUE_NUMBER=? ");
	    		PreparedStatement ps = getPrepareStatement(insertSql.toString());
	    		
	            try {
	                   	ps.setString(1, covrpf.getValidflag());
	                	ps.setInt(2, covrpf.getTranno());
	                	ps.setInt(3, covrpf.getCurrfrom());
	                	ps.setInt(4, covrpf.getCurrto());
	                	ps.setInt(5, covrpf.getUnitStatementDate());
	                    ps.setString(6, getUsrprf());
	                    ps.setString(7, getJobnm());
	                    ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
						ps.setLong(9, covrpf.getUniqueNumber());					
	                    ps.executeUpdate();
	            } catch (SQLException e) {
			LOGGER.error("insertCovrRecord()", e); /* IJTI-1479 */
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(ps, null);
	            }
	        }
		@Override
		public List<Covrpf> getInstPrem(String chdrcoy,String chdrnum,String life,String coverage, String rider) {		
		    StringBuilder sql  = new StringBuilder("SELECT INSTPREM FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND VALIDFLAG = ? ");
		    List<Covrpf> data = new ArrayList<>();
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {
		    	psSelect = getPrepareStatement(sql.toString());
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	psSelect.setString(3, life);
		    	psSelect.setString(4, coverage);
		    	psSelect.setString(5, rider);
		    	psSelect.setString(6, "1");
		        rs = psSelect.executeQuery();		        
		        while (rs.next()) {		                
		        	   	Covrpf covrpf= new Covrpf();
		        	   	covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));		        	         
		        	   	data.add(covrpf);
		        }		          
		    } catch (SQLException e) {
			LOGGER.error("getInstPrem()", e); /* IJTI-1479 */
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }
		    return data;
		}
		
	public List<Covrpf> getCovrpfByChdrnumLife(Covrpf covrpf)
	{
		StringBuilder sql = new StringBuilder("SELECT CRRCD,CRTABLE,SUMINS,PRMCUR,STATCODE FROM COVRPF");
		sql.append(" WHERE CHDRCOY = ? AND CHDRNUM = ?  AND LIFE = ? AND JLIFE = ?");		
		try(PreparedStatement ps = getPrepareStatement(sql.toString()))
		{
			List<Covrpf> data = new ArrayList<>();
			ps.setString(1, covrpf.getChdrcoy());
			ps.setString(2, covrpf.getChdrnum());
			ps.setString(3, covrpf.getLife());
			ps.setString(4, covrpf.getJlife());			
			try(ResultSet rs = ps.executeQuery())
			{
				while(rs.next())
				{
					Covrpf covr = new Covrpf();
					covr.setCrrcd(rs.getInt("CRRCD"));
					covr.setCrtable(rs.getString("CRTABLE"));
					covr.setSumins(rs.getBigDecimal("SUMINS"));
					covr.setPremCurrency(rs.getString("PRMCUR"));
					covr.setStatcode(rs.getString("STATCODE"));
					data.add(covr);
				}
			}
			return data;
		}
		catch(SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		
	}
	
	public Map<String, BigDecimal> componentChangeCvg(String chdrnum){
		Map<String, BigDecimal> crtableSuminsMap = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COVT.CHDRNUM AS CHDRNUM, ");
		sql.append("COVT.CRTABLE AS CRTABLE, COVT.SUMINS AS SUMINS FROM VM1DTA.COVTPF COVT WHERE COVT.CHDRNUM=? ");
		sql.append("UNION ALL ");
		sql.append("SELECT COVR.CHDRNUM AS CHDRNUM, ");
		sql.append("COVR.CRTABLE AS CRTABLE, COVR.SUMINS AS SUMINS FROM VM1DTA.COVRPF COVR WHERE COVR.CHDRNUM=? ");
		sql.append("AND COVR.VALIDFLAG='1' AND NOT EXISTS (SELECT 1 FROM VM1DTA.COVTPF COVT WHERE COVT.LIFE = COVR.LIFE ");
		sql.append("AND COVT.COVERAGE = COVR.COVERAGE AND COVT.RIDER = COVR.RIDER AND COVT.CHDRNUM=COVR.CHDRNUM)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, chdrnum);
			ps.setString(2, chdrnum);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(!crtableSuminsMap.isEmpty() && crtableSuminsMap.containsKey(rs.getString("CRTABLE")))
					crtableSuminsMap.put(rs.getString("CRTABLE"), crtableSuminsMap.get(rs.getString("CRTABLE")).add(rs.getBigDecimal("SUMINS")));
				else
					crtableSuminsMap.put(rs.getString("CRTABLE"), rs.getBigDecimal("SUMINS"));
			}
		}
		catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs);
		}
		return crtableSuminsMap;
	}

public boolean updateCovrValidFlagByTranno(List<Covrpf> updateCovrpfList){
		 boolean isUpdated = false;
		 String SQL_COVR_UPDATE = "UPDATE COVRPF SET VALIDFLAG='2', CURRTO=?, JOBNM=?, USRPRF=?, DATIME=? WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? ";
		 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
     
        try {
       	 for(Covrpf c:updateCovrpfList){
       		 psCovrUpdate.setInt(1, c.getCurrto());
                psCovrUpdate.setString(2, getJobnm());
                psCovrUpdate.setString(3, getUsrprf());
                psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                psCovrUpdate.setString(5, c.getChdrcoy());//IJTI-1793
                psCovrUpdate.setString(6, c.getChdrnum());
                psCovrUpdate.setInt(7, c.getTranno());
                psCovrUpdate.addBatch();
       	 }
       	 psCovrUpdate.executeBatch();
       	 isUpdated = true;
        } catch (SQLException e) {
			LOGGER.error("updateCovrValidFlagByTranno()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psCovrUpdate, null);
        }
        return isUpdated;
	 }	 
public void updateRerateData(List<Covrpf> covrUpdateList) {

	String SQL_COVR_UPDATE = "UPDATE COVRPF SET INSTPREM=?, ZBINSTPREM=?, ZLINSTPREM=?, ZSTPDUTY01 = ?, JOBNM=?, USRPRF=?, DATIME=?, VALIDFLAG=?, CURRTO=?, COMMPREM=? WHERE UNIQUE_NUMBER = ? ";
	PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);

	try {
		for (Covrpf covrpf : covrUpdateList) {
			psCovrUpdate.setBigDecimal(1, covrpf.getInstprem());
			psCovrUpdate.setBigDecimal(2, covrpf.getZbinstprem());
			psCovrUpdate.setBigDecimal(3, covrpf.getZlinstprem());
			psCovrUpdate.setBigDecimal(4, covrpf.getZstpduty01());
			psCovrUpdate.setString(5, getJobnm());
			psCovrUpdate.setString(6, getUsrprf());
			psCovrUpdate.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
			psCovrUpdate.setString(8, covrpf.getValidflag());
			psCovrUpdate.setInt(9, covrpf.getCurrto());
			psCovrUpdate.setBigDecimal(10, covrpf.getCommprem());
			psCovrUpdate.setLong(11, covrpf.getUniqueNumber());
			psCovrUpdate.addBatch();
		}
		psCovrUpdate.executeBatch();
	} catch (SQLException e) {
		LOGGER.error("updateRerateData()", e);
		throw new SQLRuntimeException(e);
	} finally {
		close(psCovrUpdate, null);
	}
}
	@Override
	public void updateCovrAnvDate(Covrpf covrincr) {
		String SQL_COVR_UPDATE = "UPDATE COVRPF SET CBANPR=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE UNIQUE_NUMBER=? ";
		 PreparedStatement psCovrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
	 
	    try {
	            psCovrUpdate.setInt(1, covrincr.getAnnivProcDate());
	            psCovrUpdate.setString(2, getJobnm());
	            psCovrUpdate.setString(3, getUsrprf());
	            psCovrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	            psCovrUpdate.setLong(5, covrincr.getUniqueNumber());
	            psCovrUpdate.executeUpdate();
	    } catch (SQLException e) {
			LOGGER.error("updateCovrIndexationind()", e);
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(psCovrUpdate, null);
	    }
	}
	
	@Override
	public  BigDecimal getSumInstPrem(String chdrnum) {
		String SUM_INST_PREM = "SELECT SUM(INSTPREM) AS INSTPREM FROM COVRPF WHERE CHDRNUM=? AND VALIDFLAG=1";
		PreparedStatement ps = getPrepareStatement(SUM_INST_PREM);
		ResultSet rs = null;
		BigDecimal ti=BigDecimal.ZERO;
		try {
			ps.setString(1, chdrnum);
			rs = ps.executeQuery();
			while(rs.next()) {
				ti = rs.getBigDecimal(1);
			}
		}
		catch (SQLException e) {
			LOGGER.error("", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, null);
		}
		return ti;
		
	}
	

	/**
	 * PINNACLE-2323 get all coverages greater than or equal to provided coverage
	 */
	public List<Covrpf> searchCovrpfRecordByCoverage(Covrpf covrpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM, ");
		sqlSelect.append("CURRTO, STATCODE, PSTATCODE, STATREASN, CRRCD, ");
		sqlSelect.append("ANBCCD, SEX, REPTCD01, REPTCD02, REPTCD03, ");
		sqlSelect.append("REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, ");
		sqlSelect.append("CRINST03, CRINST04, CRINST05, PRMCUR, TERMID, ");
		sqlSelect.append("TRDT, TRTM, USER_T, STFUND, STSECT, ");
		sqlSelect.append("STSSECT, CRTABLE, RCESDTE, PCESDTE, BCESDTE, ");
		sqlSelect.append("NXTDTE, RCESAGE, PCESAGE, BCESAGE, RCESTRM, ");
		sqlSelect.append("PCESTRM, BCESTRM, SUMINS, SICURR, VARSI, ");
		sqlSelect.append("MORTCLS, LIENCD, RATCLS, INDXIN, BNUSIN, ");
		sqlSelect.append("DPCD, DPAMT, DPIND, TMBEN, EMV01, ");
		sqlSelect.append("EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, ");
		sqlSelect.append("CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, ");
		sqlSelect.append("CHGOPT, FUNDSP, PCAMTH, PCADAY, PCTMTH, ");
		sqlSelect.append("PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, ");
		sqlSelect.append("JLLSID, INSTPREM, SINGP, RRTDAT, RRTFRM, ");
		sqlSelect.append("BBLDAT, CBANPR, CBCVIN, CBRVPR, CBUNST, ");
		sqlSelect.append("CPIDTE, ICANDT, EXALDT, IINCDT, CRDEBT, ");
		sqlSelect.append("PAYRSEQNO, BAPPMETH, ZBINSTPREM, ZLINSTPREM, USRPRF, ");
		sqlSelect.append("JOBNM, DATIME, LOADPER, RATEADJ, FLTMORT, ");
		sqlSelect.append("PREMADJ, AGEADJ ");
		sqlSelect.append("FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ?  AND COVERAGE > = ? AND VALIDFLAG = '1' ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psCovrpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlCovrpfRs = null;
		List<Covrpf> outputList = new ArrayList<Covrpf>();

		try {

			psCovrpfSelect.setString(1, covrpf.getChdrcoy().trim());
			psCovrpfSelect.setString(2, covrpf.getChdrnum());
			psCovrpfSelect.setString(3, covrpf.getLife());
			psCovrpfSelect.setString(4, covrpf.getCoverage());

			sqlCovrpfRs = executeQuery(psCovrpfSelect);

			while (sqlCovrpfRs.next()) {

				covrpf = new Covrpf();

				covrpf.setUniqueNumber(sqlCovrpfRs.getInt("UNIQUE_NUMBER"));
				covrpf.setChdrcoy(sqlCovrpfRs.getString("CHDRCOY"));
				covrpf.setChdrnum(sqlCovrpfRs.getString("CHDRNUM"));
				covrpf.setLife(sqlCovrpfRs.getString("LIFE"));
				covrpf.setJlife(sqlCovrpfRs.getString("JLIFE"));
				covrpf.setCoverage(sqlCovrpfRs.getString("COVERAGE"));
				covrpf.setRider(sqlCovrpfRs.getString("RIDER"));
				covrpf.setPlanSuffix(sqlCovrpfRs.getInt("PLNSFX"));
				covrpf.setValidflag(sqlCovrpfRs.getString("VALIDFLAG"));
				covrpf.setTranno(sqlCovrpfRs.getInt("TRANNO"));
				covrpf.setCurrfrom(sqlCovrpfRs.getInt("CURRFROM"));
				covrpf.setCurrto(sqlCovrpfRs.getInt("CURRTO"));
				covrpf.setStatcode(sqlCovrpfRs.getString("STATCODE"));
				covrpf.setPstatcode(sqlCovrpfRs.getString("PSTATCODE"));
				covrpf.setStatreasn(sqlCovrpfRs.getString("STATREASN"));
				covrpf.setCrrcd(sqlCovrpfRs.getInt("CRRCD"));
				covrpf.setAnbAtCcd(sqlCovrpfRs.getInt("ANBCCD"));
				covrpf.setSex(sqlCovrpfRs.getString("SEX"));
				covrpf.setReptcd01(sqlCovrpfRs.getString("REPTCD01"));
				covrpf.setReptcd02(sqlCovrpfRs.getString("REPTCD02"));
				covrpf.setReptcd03(sqlCovrpfRs.getString("REPTCD03"));
				covrpf.setReptcd04(sqlCovrpfRs.getString("REPTCD04"));
				covrpf.setReptcd05(sqlCovrpfRs.getString("REPTCD05"));
				covrpf.setReptcd06(sqlCovrpfRs.getString("REPTCD06"));
				covrpf.setCrInstamt01(sqlCovrpfRs.getBigDecimal("CRINST01"));
				covrpf.setCrInstamt02(sqlCovrpfRs.getBigDecimal("CRINST02"));
				covrpf.setCrInstamt03(sqlCovrpfRs.getBigDecimal("CRINST03"));
				covrpf.setCrInstamt04(sqlCovrpfRs.getBigDecimal("CRINST04"));
				covrpf.setCrInstamt05(sqlCovrpfRs.getBigDecimal("CRINST05"));
				covrpf.setPremCurrency(sqlCovrpfRs.getString("PRMCUR"));
				covrpf.setTermid(sqlCovrpfRs.getString("TERMID"));
				covrpf.setTransactionDate(sqlCovrpfRs.getInt("TRDT"));
				covrpf.setTransactionTime(sqlCovrpfRs.getInt("TRTM"));
				covrpf.setUser(sqlCovrpfRs.getInt("USER_T"));
				covrpf.setStatFund(sqlCovrpfRs.getString("STFUND"));
				covrpf.setStatSect(sqlCovrpfRs.getString("STSECT"));
				covrpf.setStatSubsect(sqlCovrpfRs.getString("STSSECT"));
				covrpf.setCrtable(sqlCovrpfRs.getString("CRTABLE"));
				covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
				covrpf.setSumins(sqlCovrpfRs.getBigDecimal("SUMINS"));
				covrpf.setSicurr(sqlCovrpfRs.getString("SICURR"));
				covrpf.setVarSumInsured(sqlCovrpfRs.getBigDecimal("VARSI"));
				covrpf.setMortcls(sqlCovrpfRs.getString("MORTCLS"));
				covrpf.setLiencd(sqlCovrpfRs.getString("LIENCD"));
				covrpf.setRatingClass(sqlCovrpfRs.getString("RATCLS"));
				covrpf.setIndexationInd(sqlCovrpfRs.getString("INDXIN"));
				covrpf.setBonusInd(sqlCovrpfRs.getString("BNUSIN"));
				covrpf.setDeferPerdCode(sqlCovrpfRs.getString("DPCD"));
				covrpf.setDeferPerdAmt(sqlCovrpfRs.getBigDecimal("DPAMT"));
				covrpf.setDeferPerdInd(sqlCovrpfRs.getString("DPIND"));
				covrpf.setTotMthlyBenefit(sqlCovrpfRs.getBigDecimal("TMBEN"));
				covrpf.setSingp(sqlCovrpfRs.getBigDecimal("SINGP"));
				covrpf.setInstprem(sqlCovrpfRs.getBigDecimal("INSTPREM"));
				covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
				covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
				covrpf.setRerateDate(sqlCovrpfRs.getInt("RRTDAT"));
				covrpf.setRerateFromDate(sqlCovrpfRs.getInt("RRTFRM"));
				covrpf.setBenBillDate(sqlCovrpfRs.getInt("BBLDAT"));
				covrpf.setEstMatValue01(sqlCovrpfRs.getBigDecimal("EMV01"));
				covrpf.setEstMatValue02(sqlCovrpfRs.getBigDecimal("EMV02"));
				covrpf.setEstMatDate01(sqlCovrpfRs.getInt("EMVDTE01"));
				covrpf.setEstMatDate02(sqlCovrpfRs.getInt("EMVDTE02"));
				covrpf.setEstMatInt01(sqlCovrpfRs.getBigDecimal("EMVINT01"));
				covrpf.setEstMatInt02(sqlCovrpfRs.getBigDecimal("EMVINT02"));
				covrpf.setCampaign(sqlCovrpfRs.getString("CAMPAIGN"));
				covrpf.setStatSumins(sqlCovrpfRs.getBigDecimal("STSMIN"));
				covrpf.setRtrnyrs(sqlCovrpfRs.getInt("RTRNYRS"));
				covrpf.setPremCessAgeMth(sqlCovrpfRs.getInt("PCAMTH"));
				covrpf.setPremCessAgeDay(sqlCovrpfRs.getInt("PCADAY"));
				covrpf.setPremCessTermMth(sqlCovrpfRs.getInt("PCTMTH"));
				covrpf.setPremCessTermDay(sqlCovrpfRs.getInt("PCTDAY"));
				covrpf.setRiskCessAgeMth(sqlCovrpfRs.getInt("RCAMTH"));
				covrpf.setRiskCessAgeDay(sqlCovrpfRs.getInt("RCADAY"));
				covrpf.setRiskCessTermMth(sqlCovrpfRs.getInt("RCTMTH"));
				covrpf.setRiskCessTermDay(sqlCovrpfRs.getInt("RCTDAY"));
				covrpf.setJlLsInd(sqlCovrpfRs.getString("JLLSID"));
				covrpf.setCoverageDebt(sqlCovrpfRs.getBigDecimal("CRDEBT"));
				covrpf.setReserveUnitsInd(sqlCovrpfRs.getString("RSUNIN"));
				covrpf.setReserveUnitsDate(sqlCovrpfRs.getInt("RUNDTE"));
				covrpf.setPayrseqno(sqlCovrpfRs.getInt("PAYRSEQNO"));
				covrpf.setCpiDate(sqlCovrpfRs.getInt("CPIDTE"));
				covrpf.setBappmeth(sqlCovrpfRs.getString("BAPPMETH"));
				covrpf.setZbinstprem(sqlCovrpfRs.getBigDecimal("ZBINSTPREM"));
				covrpf.setZlinstprem(sqlCovrpfRs.getBigDecimal("ZLINSTPREM"));
				covrpf.setUserProfile(sqlCovrpfRs.getString("USRPRF"));
				covrpf.setJobName(sqlCovrpfRs.getString("JOBNM"));
				covrpf.setDatime(sqlCovrpfRs.getString("DATIME"));
				covrpf.setLoadper(sqlCovrpfRs.getBigDecimal("LOADPER"));
				covrpf.setRateadj(sqlCovrpfRs.getBigDecimal("RATEADJ"));
				covrpf.setFltmort(sqlCovrpfRs.getBigDecimal("FLTMORT"));
				covrpf.setPremadj(sqlCovrpfRs.getBigDecimal("PREMADJ"));
				covrpf.setAgeadj(sqlCovrpfRs.getBigDecimal("AGEADJ"));

				outputList.add(covrpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrpfSelect, sqlCovrpfRs);
		}

		return outputList;
	}		
}
