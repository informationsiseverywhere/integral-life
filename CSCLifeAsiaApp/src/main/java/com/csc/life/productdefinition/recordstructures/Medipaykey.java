package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:43
 * Description:
 * Copybook name: MEDIPAYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Medipaykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData medipayFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData medipayKey = new FixedLengthStringData(256).isAPartOf(medipayFileKey, 0, REDEFINE);
  	public FixedLengthStringData medipayChdrcoy = new FixedLengthStringData(1).isAPartOf(medipayKey, 0);
  	public FixedLengthStringData medipayPaidby = new FixedLengthStringData(1).isAPartOf(medipayKey, 1);
  	public FixedLengthStringData medipayExmcode = new FixedLengthStringData(10).isAPartOf(medipayKey, 2);
  	public FixedLengthStringData medipayActiveInd = new FixedLengthStringData(1).isAPartOf(medipayKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(medipayKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(medipayFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		medipayFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}