package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:50
 * Description:
 * Copybook name: TR51GREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr51grec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr51gRec = new FixedLengthStringData(500);
  	public FixedLengthStringData ages = new FixedLengthStringData(36).isAPartOf(tr51gRec, 0);
  	public ZonedDecimalData[] age = ZDArrayPartOfStructure(12, 3, 0, ages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(ages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData age01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData age02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData age03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData age04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData age05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData age06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData age07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData age08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData age09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData age10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public ZonedDecimalData age11 = new ZonedDecimalData(3, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData age12 = new ZonedDecimalData(3, 0).isAPartOf(filler, 33);
  	public FixedLengthStringData suminss = new FixedLengthStringData(204).isAPartOf(tr51gRec, 36);
  	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(12, 17, 2, suminss, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(204).isAPartOf(suminss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData sumins01 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData sumins02 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 17);
  	public ZonedDecimalData sumins03 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 34);
  	public ZonedDecimalData sumins04 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 51);
  	public ZonedDecimalData sumins05 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 68);
  	public ZonedDecimalData sumins06 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData sumins07 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 102);
  	public ZonedDecimalData sumins08 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 119);
  	public ZonedDecimalData sumins09 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 136);
  	public ZonedDecimalData sumins10 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 153);
  	public ZonedDecimalData sumins11 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 170);
  	public ZonedDecimalData sumins12 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 187);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(260).isAPartOf(tr51gRec, 240, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr51gRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr51gRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}