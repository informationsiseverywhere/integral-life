package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SA524
 * @version 1.0 generated on 30/08/09 07:17
 * @author Quipoz
 */
public class Sa524ScreenVars extends SmartVarModel { 
	public FixedLengthStringData dataArea = new FixedLengthStringData(264);
	public FixedLengthStringData dataFields = new FixedLengthStringData(82).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData minmthif = DD.minmthif.copyToZonedDecimal().isAPartOf(dataFields,55);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,68);
	public ZonedDecimalData maxphprd = DD.maxphprd.copyToZonedDecimal().isAPartOf(dataFields,73);
	public FixedLengthStringData prmstatus = DD.prmstatus.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData znfopt = DD.znfopt.copy().isAPartOf(dataFields,77);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 80);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData minmthifErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData maxphprdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData prmstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData znfoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 120);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] minmthifOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] maxphprdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] prmstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] znfoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	
	/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sa524screenWritten = new LongData(0);
	public LongData Sa524protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sa524ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(prmstatusOut,new String[] {null, "01", null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(znfoptOut,new String[] {null, "02", null, null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, minmthif, maxphprd, prmstatus, znfopt};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, minmthifOut, maxphprdOut, prmstatusOut, znfoptOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, minmthifErr, maxphprdErr, prmstatusErr, znfoptErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa524screen.class;
		protectRecord = Sa524protect.class;
	}

}
