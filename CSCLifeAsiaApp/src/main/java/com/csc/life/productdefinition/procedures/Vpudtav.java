package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;


public class Vpudtav extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	
	private ZonedDecimalData wsaaTotestval = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaFirst = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirst, "Y");
	
	private FixedLengthStringData wsaaConstants = new FixedLengthStringData(2);
	private FixedLengthStringData wsccN = new FixedLengthStringData(1)
			.isAPartOf(wsaaConstants, 0).init("N");
	private FixedLengthStringData wsccY = new FixedLengthStringData(1)
			.isAPartOf(wsaaConstants, 1).init("Y");
//	private Syserrrec syserrrec = new Syserrrec();
	private Deathrec deathrec = new Deathrec();
	private Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	
	public Vpudtav() {
		super();
	}

	public void mainline (Object... parmArray) {
		vpmfmtrec = (Vpmfmtrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		deathrec.deathRec.set(vpmcalcrec.linkageArea);
		try {
			main0100();
			vpmcalcrec.linkageArea.set(deathrec.deathRec);
			exitProgram();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	public void main0100() {
		if (firstTime.isTrue()) {
			wsaaTotestval.set(ZERO);
		}

		if (!isEQ(deathrec.endf, wsccY)) {
			compute(wsaaTotestval, 2).set(add(wsaaTotestval, vpmfmtrec.amount01));
			wsaaFirst.set(wsccN);
		} else {
			if (!isEQ(wsaaTotestval, ZERO)) {
				deathrec.estimatedVal.set(deathrec.actualVal);
				deathrec.actualVal.set(ZERO);
			}
		}
		return;
	}
}
