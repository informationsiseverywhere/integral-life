
package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface RgpdetpfDAO extends BaseDAO<Rgpdetpf> {

	public List<Rgpdetpf> searchRpdetpfRecord(List<String> chdrnumList, String coy) ;

	public void insertIntoRpdetpf(Rgpdetpf rpdetpf) ;

	public boolean updateRegpRecIntDate(List<Rgpdetpf> regpBulkOpList) ;
	
	public boolean updateRegpRecCapDate(List<Rgpdetpf> regpBulkOpList) ;
	
	public boolean updateRegpdet(List<Rgpdetpf> regpBulkOpList) ;
	
}
