package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Chevpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ChevpfDAO extends BaseDAO<Chevpf> {
    public void insertChevpfRecord(List<Chevpf> chevpfList);
}