package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.CoevpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Coevpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CoevpfDAOImpl extends BaseDAOImpl<Coevpf> implements CoevpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CoevpfDAOImpl.class);

    public void insertCoevpfRecord(List<Coevpf> coevpfList) {
        if (coevpfList != null) {
            String SQL_COEV_INSERT = "INSERT INTO COEVPF(EFFDATE,CHDRCOY,CHDRNUM,COVERAGE,RIDER,LIFE,CRTABLE,RCESTRM,PCESTRM,SUMINS,CRRCD,ZLINSTPREM,CURRFROM,VALIDFLAG,INSTPREM,AEXTPRM,APREM,OPCDA01,OPCDA02,OPCDA03,OPCDA04,OPCDA05,OPPC01,OPPC02,OPPC03,OPPC04,OPPC05,AGERATE01,AGERATE02,AGERATE03,AGERATE04,AGERATE05,INSPRM01,INSPRM02,INSPRM03,INSPRM04,INSPRM05,ECESTRM01,ECESTRM02,ECESTRM03,ECESTRM04,ECESTRM05,FUNDAMNT01,FUNDAMNT02,FUNDAMNT03,FUNDAMNT04,FUNDAMNT05,VRTFND01,VRTFND02,VRTFND03,VRTFND04,VRTFND05,ANNAME01,ANNAME02,ANNAME03,ANNAME04,ANNAME05,ANNAGE01,ANNAGE02,ANNAGE03,ANNAGE04,ANNAGE05,SANNAME01,SANNAME02,SANNAME03,SANNAME04,SANNAME05,SANNAGE01,SANNAGE02,SANNAGE03,SANNAGE04,SANNAGE05,RGPYMOP01,RGPYMOP02,RGPYMOP03,RGPYMOP04,RGPYMOP05,FPAYDATE01,FPAYDATE02,FPAYDATE03,FPAYDATE04,FPAYDATE05,NPAYDATE01,NPAYDATE02,NPAYDATE03,NPAYDATE04,NPAYDATE05,EPAYDATE01,EPAYDATE02,EPAYDATE03,EPAYDATE04,EPAYDATE05,PYMT01,PYMT02,PYMT03,PYMT04,PYMT05,USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement psCoevInsert = getPrepareStatement(SQL_COEV_INSERT);
            try {
                for (Coevpf c : coevpfList) {
                    psCoevInsert.setInt(1, c.getEffdate());
                    psCoevInsert.setString(2, c.getChdrcoy());
                    psCoevInsert.setString(3, c.getChdrnum());
                    psCoevInsert.setString(4, c.getCoverage());
                    psCoevInsert.setString(5, c.getRider());
                    psCoevInsert.setString(6, c.getLife());
                    psCoevInsert.setString(7, c.getCrtable());
                    psCoevInsert.setInt(8, c.getRiskCessTerm());
                    psCoevInsert.setInt(9, c.getPremCessTerm());
                    psCoevInsert.setBigDecimal(10, c.getSumins());
                    psCoevInsert.setInt(11, c.getCrrcd());
                    psCoevInsert.setBigDecimal(12, c.getZlinstprem());
                    psCoevInsert.setInt(13, c.getCurrfrom());
                    psCoevInsert.setString(14, c.getValidflag());
                    psCoevInsert.setBigDecimal(15, c.getInstprem());
                    psCoevInsert.setBigDecimal(16, c.getAextprm());
                    psCoevInsert.setBigDecimal(17, c.getAprem());
                    psCoevInsert.setString(18, c.getOpcda01());
                    psCoevInsert.setString(19, c.getOpcda01());
                    psCoevInsert.setString(20, c.getOpcda01());
                    psCoevInsert.setString(21, c.getOpcda01());
                    psCoevInsert.setString(22, c.getOpcda01());
                    psCoevInsert.setBigDecimal(23, c.getOppc01());
                    psCoevInsert.setBigDecimal(24, c.getOppc02());
                    psCoevInsert.setBigDecimal(25, c.getOppc03());
                    psCoevInsert.setBigDecimal(26, c.getOppc04());
                    psCoevInsert.setBigDecimal(27, c.getOppc05());
                    psCoevInsert.setInt(28, c.getAgerate01());
                    psCoevInsert.setInt(29, c.getAgerate02());
                    psCoevInsert.setInt(30, c.getAgerate03());
                    psCoevInsert.setInt(31, c.getAgerate04());
                    psCoevInsert.setInt(32, c.getAgerate05());
                    psCoevInsert.setInt(33, c.getInsprm01());
                    psCoevInsert.setInt(34, c.getInsprm02());
                    psCoevInsert.setInt(35, c.getInsprm03());
                    psCoevInsert.setInt(36, c.getInsprm04());
                    psCoevInsert.setInt(37, c.getInsprm05());
                    psCoevInsert.setInt(38, c.getExtCessTerm01());
                    psCoevInsert.setInt(39, c.getExtCessTerm02());
                    psCoevInsert.setInt(40, c.getExtCessTerm03());
                    psCoevInsert.setInt(41, c.getExtCessTerm04());
                    psCoevInsert.setInt(42, c.getExtCessTerm05());
                    psCoevInsert.setBigDecimal(43, c.getFundAmount01());
                    psCoevInsert.setBigDecimal(44, c.getFundAmount02());
                    psCoevInsert.setBigDecimal(45, c.getFundAmount03());
                    psCoevInsert.setBigDecimal(46, c.getFundAmount04());
                    psCoevInsert.setBigDecimal(47, c.getFundAmount05());
                    psCoevInsert.setString(48, c.getUnitVirtualFund01());
                    psCoevInsert.setString(49, c.getUnitVirtualFund02());
                    psCoevInsert.setString(50, c.getUnitVirtualFund03());
                    psCoevInsert.setString(51, c.getUnitVirtualFund04());
                    psCoevInsert.setString(52, c.getUnitVirtualFund05());
                    psCoevInsert.setString(53, c.getAnname01());
                    psCoevInsert.setString(54, c.getAnname02());
                    psCoevInsert.setString(55, c.getAnname03());
                    psCoevInsert.setString(56, c.getAnname04());
                    psCoevInsert.setString(57, c.getAnname05());
                    psCoevInsert.setInt(58, c.getAnnage01());
                    psCoevInsert.setInt(59, c.getAnnage02());
                    psCoevInsert.setInt(60, c.getAnnage03());
                    psCoevInsert.setInt(61, c.getAnnage04());
                    psCoevInsert.setInt(62, c.getAnnage05());
                    psCoevInsert.setString(63, c.getSanname01());
                    psCoevInsert.setString(64, c.getSanname02());
                    psCoevInsert.setString(65, c.getSanname03());
                    psCoevInsert.setString(66, c.getSanname04());
                    psCoevInsert.setString(67, c.getSanname05());
                    psCoevInsert.setInt(68, c.getSannage01());
                    psCoevInsert.setInt(69, c.getSannage02());
                    psCoevInsert.setInt(70, c.getSannage03());
                    psCoevInsert.setInt(71, c.getSannage04());
                    psCoevInsert.setInt(72, c.getSannage05());
                    psCoevInsert.setString(73, c.getRgpymop01());
                    psCoevInsert.setString(74, c.getRgpymop02());
                    psCoevInsert.setString(75, c.getRgpymop03());
                    psCoevInsert.setString(76, c.getRgpymop04());
                    psCoevInsert.setString(77, c.getRgpymop05());
                    psCoevInsert.setInt(78, c.getFirstPaydate01());
                    psCoevInsert.setInt(79, c.getFirstPaydate02());
                    psCoevInsert.setInt(80, c.getFirstPaydate03());
                    psCoevInsert.setInt(81, c.getFirstPaydate04());
                    psCoevInsert.setInt(82, c.getFirstPaydate05());
                    psCoevInsert.setInt(83, c.getNextPaydate01());
                    psCoevInsert.setInt(84, c.getNextPaydate02());
                    psCoevInsert.setInt(85, c.getNextPaydate03());
                    psCoevInsert.setInt(86, c.getNextPaydate04());
                    psCoevInsert.setInt(87, c.getNextPaydate05());
                    psCoevInsert.setInt(88, c.getFinalPaydate01());
                    psCoevInsert.setInt(89, c.getFinalPaydate02());
                    psCoevInsert.setInt(90, c.getFinalPaydate03());
                    psCoevInsert.setInt(91, c.getFinalPaydate04());
                    psCoevInsert.setInt(92, c.getFinalPaydate05());
                    psCoevInsert.setBigDecimal(93, c.getPymt01());
                    psCoevInsert.setBigDecimal(94, c.getPymt02());
                    psCoevInsert.setBigDecimal(95, c.getPymt03());
                    psCoevInsert.setBigDecimal(96, c.getPymt04());
                    psCoevInsert.setBigDecimal(97, c.getPymt05());
                    psCoevInsert.setString(98, getUsrprf());
                    psCoevInsert.setString(99, getJobnm());
                    psCoevInsert.setTimestamp(100, new Timestamp(System.currentTimeMillis()));
                    psCoevInsert.addBatch();
                }
                psCoevInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertCoevpfRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psCoevInsert, null);
            }
        }
    }
}
