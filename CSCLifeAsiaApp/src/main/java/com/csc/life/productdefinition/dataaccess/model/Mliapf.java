package com.csc.life.productdefinition.dataaccess.model;

public class Mliapf {
	
	private long uniqueNumber; //ILIFE-8901
	private String mlcoycde;
	private String mlentity;
	private String securityno;
 //ILIFE-8901 start
	private String mloldic;
	private String mlothic;
	private String clntnaml;
	private int dob;
	private String rskflg;
	private int hpropdte;
	private int mlhsperd;
	private String mlmedcde01;
	private String mlmedcde02;
	private String mlmedcde03;
	private String actn;
	private String indc;
	private String mind;
	private int effdate;
 //ILIFE-8901 end
	private String userProfile;
	private String jobName;
	private String datime;

	
	
	public String getMlcoycde() {
		return mlcoycde;
	}
	public void setMlcoycde(String mlcoycde) {
		this.mlcoycde = mlcoycde;
	}
	public String getMlentity() {
		return mlentity;
	}
	public void setMlentity(String mlentity) {
		this.mlentity = mlentity;
	}
	public String getSecurityno() {
		return securityno;
	}
	public void setSecurityno(String securityno) {
		this.securityno = securityno;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
 //ILIFE-8901 start
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getMloldic() {
		return mloldic;
	}
	public void setMloldic(String mloldic) {
		this.mloldic = mloldic;
	}
	public String getMlothic() {
		return mlothic;
	}
	public void setMlothic(String mlothic) {
		this.mlothic = mlothic;
	}
	public String getClntnaml() {
		return clntnaml;
	}
	public void setClntnaml(String clntnaml) {
		this.clntnaml = clntnaml;
	}
	public int getDob() {
		return dob;
	}
	public void setDob(int dob) {
		this.dob = dob;
	}
	public String getRskflg() {
		return rskflg;
	}
	public void setRskflg(String rskflg) {
		this.rskflg = rskflg;
	}
	public int getHpropdte() {
		return hpropdte;
	}
	public void setHpropdte(int hpropdte) {
		this.hpropdte = hpropdte;
	}
	public int getMlhsperd() {
		return mlhsperd;
	}
	public void setMlhsperd(int mlhsperd) {
		this.mlhsperd = mlhsperd;
	}
	public String getMlmedcde01() {
		return mlmedcde01;
	}
	public void setMlmedcde01(String mlmedcde01) {
		this.mlmedcde01 = mlmedcde01;
	}
	public String getMlmedcde02() {
		return mlmedcde02;
	}
	public void setMlmedcde02(String mlmedcde02) {
		this.mlmedcde02 = mlmedcde02;
	}
	public String getMlmedcde03() {
		return mlmedcde03;
	}
	public void setMlmedcde03(String mlmedcde03) {
		this.mlmedcde03 = mlmedcde03;
	}
	public String getActn() {
		return actn;
	}
	public void setActn(String actn) {
		this.actn = actn;
	}
	public String getIndc() {
		return indc;
	}
	public void setIndc(String indc) {
		this.indc = indc;
	}
	public String getMind() {
		return mind;
	}
	public void setMind(String mind) {
		this.mind = mind;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
 //ILIFE-8901 end
}