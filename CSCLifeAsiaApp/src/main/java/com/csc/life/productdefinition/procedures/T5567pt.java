/*
 * File: T5567pt.java
 * Date: 30 August 2009 2:23:12
 * Author: Quipoz Limited
 * 
 * Class transformed from T5567PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5567.
*
*
*****************************************************************
* </pre>
*/
public class T5567pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Contract Fee Parameters                     S5567");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 0, FILLER).init("Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 13);
	private FixedLengthStringData filler8 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 23, FILLER).init("    Valid to:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(63);
	private FixedLengthStringData filler9 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine004, 27, FILLER).init("Frequency               Contract Fee");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(71);
	private FixedLengthStringData filler11 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 30);
	private FixedLengthStringData filler12 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine005, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(71);
	private FixedLengthStringData filler13 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 30);
	private FixedLengthStringData filler14 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(71);
	private FixedLengthStringData filler15 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 30);
	private FixedLengthStringData filler16 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(71);
	private FixedLengthStringData filler17 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 30);
	private FixedLengthStringData filler18 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(71);
	private FixedLengthStringData filler19 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 30);
	private FixedLengthStringData filler20 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(71);
	private FixedLengthStringData filler21 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 30);
	private FixedLengthStringData filler22 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(71);
	private FixedLengthStringData filler23 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 30);
	private FixedLengthStringData filler24 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(71);
	private FixedLengthStringData filler25 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 30);
	private FixedLengthStringData filler26 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(71);
	private FixedLengthStringData filler27 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 30);
	private FixedLengthStringData filler28 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(71);
	private FixedLengthStringData filler29 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 30);
	private FixedLengthStringData filler30 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 53).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5567rec t5567rec = new T5567rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5567pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5567rec.t5567Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo007.set(t5567rec.billfreq01);
		fieldNo009.set(t5567rec.billfreq02);
		fieldNo011.set(t5567rec.billfreq03);
		fieldNo013.set(t5567rec.billfreq04);
		fieldNo015.set(t5567rec.billfreq05);
		fieldNo017.set(t5567rec.billfreq06);
		fieldNo019.set(t5567rec.billfreq07);
		fieldNo021.set(t5567rec.billfreq08);
		fieldNo023.set(t5567rec.billfreq09);
		fieldNo025.set(t5567rec.billfreq10);
		fieldNo008.set(t5567rec.cntfee01);
		fieldNo010.set(t5567rec.cntfee02);
		fieldNo012.set(t5567rec.cntfee03);
		fieldNo014.set(t5567rec.cntfee04);
		fieldNo016.set(t5567rec.cntfee05);
		fieldNo018.set(t5567rec.cntfee06);
		fieldNo020.set(t5567rec.cntfee07);
		fieldNo022.set(t5567rec.cntfee08);
		fieldNo024.set(t5567rec.cntfee09);
		fieldNo026.set(t5567rec.cntfee10);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
