package com.csc.life.productdefinition.dataaccess.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface CovrpfDAO extends BaseDAO<Covrpf> {
	public List<Covrpf> selectCovrRecordBegin(Covrpf covrpf);
    public List<Covrpf> searchCovrRecord(String coy, List<String> chdrnumList);
    // add by yy for ILIFE-2893
    public Map<String,List<Covrpf>> searchCovrMap(String coy, List<String> chdrnumList);
/**
     * Fetch the list COVRPF records associated with the Contract
     * Introduced as part of ILIFE-3392
     *
     * @param com.csc.life.productdefinition.dataaccess.model.Covrpf the covrpf
     * @return java.util.List<Covrpf>
     */
    public List<Covrpf> searchCovrRecordForContract(Covrpf Covrpf);
    

    
    public List<Covrpf> selectCovrRecord(Covrpf covrpf);
    
    public List<Covrpf> selectCovrSurData(Covrpf covrSurData );
     public void updateCovrBulk(List<Covrpf> covrPFList);
    public void insertCovrBulk(List<Covrpf> covrPFList);
    public List<Covrpf> selectCovrClmData(Covrpf covrClmData );
    /*IJS-59 STARTS */
    public List<Covrpf> selectCovrClm(Covrpf covrClm);
    /*IJS-59 ENDS */
    /*ILIFE-3395 STARTS */
    public List<Covrpf> selectCoverage(Covrpf covrpfData );
    /*ILIFE-3395 ENDS */
    
    //Ilife-3310 by liwei
    public Covrpf getCovrridData(String chdrcoy,String chdrnum,String life,String coverage,String crtable);
    public List<Covrpf> getCovrcovData(String chdrcoy,String chdrnum,String life,String crtable);
    public List<Covrpf> readCovrRecord(Covrpf covrpfModel);
    public List<Covrpf> getCovrLifeenqRecords(String chdrcoy, String chdrnum, String lifeflag, String validflag) ;
    public Covrpf getCovrRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plansuffix, String validflag);
    public List<Covrpf> readCovrsurData(Covrpf covrpfModel);
    public Covrpf readCovrenqData(Covrpf covrpfModel);
    
    public List<String> searchCovrCrtable(String coy, String chdrnum);
   //ILIFE-2882 by wli31
    public List<Covrpf> searchCovrrnlRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider,double plnsfx );
  
    //ILIFE-3138-STARTS
    public List<Covrpf> SearchRecordsforMATY(String coy,String chdrnumFrom,String chdrnumTo,int effdate);
	public Covrpf getcovrincrRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int planSuffix);
	public void updateCovrIndexationind(Covrpf covrincr);
	public List<Covrpf> SearchRecordsforMatd(String chdrcoy, String chdrnum, int planSuffix);
	public List<Covrpf> getcovrmatRecordsForMature(String chdrcoy, String chdrnum);
	
	public boolean insertCovrpfList(List<Covrpf> insertCovrpfList);
	public void updateCovrRecord(List<Covrpf> updateCovrpflist, int busDate);
	public List<BigDecimal> getTotalStampDuty(String chdrcoy, String chdrnum);/*ILIFE-3149 */

 //ILIFE-3138-ENDS
	public List<Covrpf> searchCovrpfRecord(Covrpf covrpf) throws SQLRuntimeException;
    public List<Covrpf> searchCovrrnwRecord(String chdrcoy, String chdrnum, String drypRunDate);
    public int updateCovrProcDate(List<Covrpf> uniqueList);	
    public List<Covrpf> getCovrbonRecord(Covrpf covrpfModel);
    List<Covrpf> readCovrForBilling(String chdrCoy, String chdrNum);
    
    // ILIFE-4187
    public Map<String,List<Covrpf>> searchCovrudlByChdrnum(List<String> chdrnumList);
    public void updateCovrDebtRecord(List<Covrpf> updateCovrpfDebtList);
    public void updateCovrStatRecord(List<Covrpf> updateCovrpfDebtList);
    public Map<String,List<Covrpf>> searchCovruddByChdrnum(List<String> chdrnumList);
    public Map<String,List<Covrpf>> searchCovrmjaByChdrnum(List<String> chdrnumList);
    
    public void updateCovrDebt(List<Covrpf> updateCovrpfDebtList);
    public void updateCovrmjaRecord(List<Covrpf> updateCovrpfList);
    public void updateChunkRecordToCovr(List<Covrpf> covrpfList);
	
	// Start: ILIFE-4349
	public boolean updateSumins(Covrpf covrpf);
	// Ends: ILIFE-4349
	
    public Map<String, List<Covrpf>> searchCovrBenbilldate(String chdrcoy, List<String> chdrnumList);
    public void updateCovrBillbendateRecord(List<Covrpf> updateCovrpfList);
    
    public void updateVarSumInsuredRecord(List<Covrpf> uniqueList);
    
    //ILIFE-4399
    public Map<String,List<Covrpf>> searchCovrrnlByChdrnum(List<String> chdrnumList);
    public boolean updateCovrValidFlag(List<Covrpf> updateCovrpfList);
    
	 // Ticket#ILIFE-4404
	 public Map<String,List<Covrpf>> searchValidCovrByChdrnum(List<String> chdrnumList);
	 //ILIFE-4884
    public List<Covrpf> getCovrincrRecord(String chdrcoy, String chdrnum, String life);
    public List<Covrpf> getCovrsurByComAndNum(String coy, String chdrnum);
    public List<Covrpf> getcovrtrbRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider);
	 //ILIFE-5032
	 public List<Covrpf> getCovrpupRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider);
	 public List<Covrpf> getCovrpupRecord(String chdrcoy, String chdrnum, String life, String coverage); //ilife-5148
	 public List<Covrpf> getCovrmjaByComAndNum(String coy, String chdrnum);
	 public List<Covrpf> getCovrByComAndNum(String coy, String chdrnum);
	 public List<Covrpf> getCovrByComAndNumAllFlagRecord(String coy, String chdrnum);
	 public boolean updateCovrValidAndCurrtoFlag(Covrpf covrpf);
	 public boolean updateCovrPstatcode(Covrpf covrpf);
	 public Covrpf getCovrByUniqueNum(long uniqueNumber);
	 
	 public void insertCovrRecord(List<Covrpf> covrList);
	 public Map<String, List<Covrpf>> readCovrData(String company,List<String> chdrnumList);
	 public void updateCpiDate(List<Covrpf> uniqueList);
	 public void insertCovrRecordForL2POLRNWL(List<Covrpf> covrList);
	 
	 public List<Covrpf> searchValidCovrpfRecord(String chdrcoy,String chdrnum, String life, String coverage, String rider);
	 public boolean isExistCovrRecord(String chdrcoy,String chdrnum, String life, String coverage, String rider);

	 
	 public List<Covrpf> searchCovrmjaByChdrnumCoy(String chdrnum,String coy);
	 public Map<String, List<Covrpf>> searchCovrlnb(String coy, List<String> chdrnumList);
	 public Covrpf getCovrpfData(String chdrcoy,String chdrnum,String life,String coverage,String crtable);
	 
	 public void updateCoverPf(Covrpf covrpf);
	 public void updateCoverPfStatus(Covrpf covrpf);
	 
	 public void updateUniqueNo(Covrpf covrpf);
	 public void deleteByFlag(String validflag, String chdrnum, String crtable);
	 public boolean updateCrdebt(Covrpf covrpf);

	 public Covrpf getCovrpfData(String chdrcoy,String chdrnum,String crtable);
	 public void updatebyUniqueNo(Covrpf covrpf);
     public List<Covrpf> getCovrpfByTranno(String chdrcoy, String chdrnum) ;
     public void deleteCovrpfRecord(List<Covrpf> covrpfList);
     public boolean updateCovrValidFlagTo1(List<Covrpf> updateCovrpfList);
	 public void updateCovrpf(Covrpf covrpf);
     public void updateCovrpfRecord(Covrpf covrpf);
     public Covrpf getByTranno(String chdrcoy,String chdrnum,String tranno,String validflag);//ILIFE-8123 
     public List<Covrpf> getSingpremtype(String coy, String chdrnum); /*ILIFE-8098 */
	 public void updateCovrRPStat(List<Covrpf> covrpfList); //ILIFE-8179
	 public void insertCovrRecord(Covrpf covrpf); //ILB-566
	 public List<Covrpf> getInstPrem(String chdrcoy,String chdrnum,String life,String coverage, String rider);
	 public List<Covrpf> getCovrpfByChdrnumLife(Covrpf covrpf);
	 public Map<String, BigDecimal> componentChangeCvg(String chdrnum);
	 public boolean updateCovrValidFlagByTranno(List<Covrpf> updateCovrpfList);
	 public void updateRerateData(List<Covrpf> covrUpdateList);
	 public void updateCovrAnvDate(Covrpf covrincr);
	public List<Covrpf> searchCovrpfRecordByCoverage(Covrpf covrpf);
	 public BigDecimal getSumInstPrem(String chdrnum);
	public List<Covrpf> searchRecordsforEXPY(String coy, String string, String string2, int int1);
}