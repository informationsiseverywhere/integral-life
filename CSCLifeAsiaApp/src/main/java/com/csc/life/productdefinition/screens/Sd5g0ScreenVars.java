package com.csc.life.productdefinition.screens;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Sd5g0ScreenVars extends SmartVarModel {
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(253);
	public FixedLengthStringData dataFields = new FixedLengthStringData(93).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData salelicensetype = DD.salelicensetype.copy().isAPartOf(dataFields,0);
  	public FixedLengthStringData agentbank = DD.agentbank.copy().isAPartOf(dataFields,30);
  	public FixedLengthStringData teller = DD.teller.copy().isAPartOf(dataFields,31);
  	public FixedLengthStringData bankmang = DD.bankmang.copy().isAPartOf(dataFields,32);
  	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,33);
  	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,34);
  	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,42);
  	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,72);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,77);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,85);
  	
  	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea,93);
  	public FixedLengthStringData salelicensetypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agentbankErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData tellerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankmangErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 133);
	public FixedLengthStringData[] salelicensetypeOut = FLSArrayPartOfStructure	(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agentbankOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] tellerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankmangOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);
	public LongData Sd5g0screenWritten = new LongData(0);
	public LongData Sd5g0protectWritten = new LongData(0);
	
	public FixedLengthStringData salelicensetypeDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return false;
	}
	
	public Sd5g0ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(salelicensetypeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agentbankOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tellerOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankmangOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, itmfrm, itmto, longdesc, salelicensetype, agentbank,teller,bankmang};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut,itmfrmOut, itmtoOut, longdescOut, salelicensetypeOut, agentbankOut,tellerOut,bankmangOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr,itmfrmErr, itmtoErr, longdescErr, salelicensetypeErr, agentbankErr,tellerErr,bankmangErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp,salelicensetypeDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5g0Screen.class;
		protectRecord = Sd5g0protect.class;
	}
}
