/*
 * File: Pr633d2.java
 * Date: 30 August 2009 1:49:59
 * Author: Quipoz Limited
 * 
 * Class transformed from PR633D2.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.dataaccess.ZmpdTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  This  subroutine  is  called  from  OPTSWCH Table T1661. It
*  determines whether any ZMPD records exist for the component
*  being enquired upon in PR633.
*
***********************************************************************
* </pre>
*/
public class Pr633d2 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PR633D2");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String zmpdrec = "ZMPDREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
		/*Medical Provider Doctor Details*/
	private ZmpdTableDAM zmpdIO = new ZmpdTableDAM();
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit0090, 
		errorProg610
	}

	public Pr633d2() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			performs0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit0090();
		}
	}

protected void performs0010()
	{
		zmpvIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		zmpdIO.setDataKey(SPACES);
		zmpdIO.setZmedcoy(zmpvIO.getZmedcoy());
		zmpdIO.setZmedprv(zmpvIO.getZmedprv());
		zmpdIO.setSeqno(1);
		zmpdIO.setFormat(zmpdrec);
		zmpdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpdIO);
		if (isNE(zmpdIO.getStatuz(),varcom.oK)
		&& isNE(zmpdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(zmpdIO.getParams());
			syserrrec.statuz.set(zmpdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zmpdIO.getStatuz(),varcom.oK)) {
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit0090);
		}
	}

protected void exit0090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
