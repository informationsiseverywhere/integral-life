package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:19
 * Description:
 * Copybook name: TR650REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr650rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr650Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ynflag = new FixedLengthStringData(1).isAPartOf(tr650Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(499).isAPartOf(tr650Rec, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr650Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr650Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}