package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.MbnspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Mbnspf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.terminationclaims.dataaccess.dao.impl.MatypfDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MbnspfDAOImpl extends BaseDAOImpl<Mbnspf> implements MbnspfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MbnspfDAOImpl.class);
	
	@Override
	public Mbnspf getmbnsRecord(String chdrcoy, String chdrnum, String life, String coverage,String rider, int yrsinf ) {
		 StringBuilder sqlCovrSelect1 = new StringBuilder("SELECT * FROM MBNSPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ?  AND COVERAGE = ? AND RIDER = ? AND YRSINF = ? ");
        sqlCovrSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, YRSINF ASC, UNIQUE_NUMBER DESC");
     
   	
        PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
		ResultSet rs = null;
		Mbnspf mbnspf = null;

		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			ps.setInt(6, yrsinf);
			
			rs = ps.executeQuery();

			while (rs.next()) {
				mbnspf = new Mbnspf();
				
				mbnspf.setChdrcoy(rs.getString(2));
				mbnspf.setChdrnum(rs.getString(3));
				mbnspf.setLife(rs.getString(4));
				mbnspf.setCoverage(rs.getString(5));
				mbnspf.setRider(rs.getString(6));
				mbnspf.setYrsinf(rs.getInt(7));
				mbnspf.setSumins(rs.getBigDecimal(8));
				mbnspf.setSurrval(rs.getBigDecimal(9));

			}
		} catch (SQLException e) {
			LOGGER.error("getmbnsRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return mbnspf;
	}
	
	@Override
	public void insertMbnspfRecord(Mbnspf mbnspf) {
		if (mbnspf != null) {
		      
			StringBuilder sql_mbnspf_insert = new StringBuilder();
			sql_mbnspf_insert.append("INSERT INTO MBNSPF");
			sql_mbnspf_insert.append("(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,YRSINF,SUMINS,SURRVAL,USRPRF,JOBNM,DATIME)");
			sql_mbnspf_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement psInsert = getPrepareStatement(sql_mbnspf_insert.toString());
			try
			{
					psInsert.setString(1, mbnspf.getChdrcoy());
					psInsert.setString(2, mbnspf.getChdrnum());
					psInsert.setString(3, mbnspf.getLife());
					psInsert.setString(4, mbnspf.getCoverage());
					psInsert.setString(5, mbnspf.getRider());
					psInsert.setInt(6, mbnspf.getYrsinf());
					psInsert.setBigDecimal(7, mbnspf.getSumins());
					psInsert.setBigDecimal(8, mbnspf.getSurrval());
					psInsert.setString(9, this.getUsrprf());
					psInsert.setString(10, this.getJobnm());
					psInsert.setTimestamp(11, new Timestamp(System.currentTimeMillis()));
					psInsert.addBatch();
					int[] rowCount=psInsert.executeBatch();	
			}
			catch (SQLException e) {
                LOGGER.error("insertMbnspfRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, null);
            }
		}
		
	}
	
	@Override
	public boolean updateMbnspfRecord(Mbnspf mbnspf) {
		boolean updateSuccess = false;
		if (mbnspf != null) {

			String sqlUpdate = "UPDATE MBNSPF SET SUMINS = ?, SURRVAL = ?";

			PreparedStatement mbnsUpdate = getPrepareStatement(sqlUpdate);
			try {					
					mbnsUpdate.setBigDecimal(1, mbnspf.getSumins());
					mbnsUpdate.setBigDecimal(2, mbnspf.getSurrval());
					mbnsUpdate.setTimestamp(3,new Timestamp(System.currentTimeMillis()));

					mbnsUpdate.execute();
					updateSuccess = true;
			} catch (SQLException e) {
				LOGGER.error("updateMbnspfRecord", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(mbnsUpdate, null);
			}
		}
		return updateSuccess;
	}
	
}