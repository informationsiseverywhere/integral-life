package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:41
 * Description:
 * Copybook name: TT502REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt502rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt502Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData disccntmeth = new FixedLengthStringData(4).isAPartOf(tt502Rec, 0);
  	public FixedLengthStringData insprms = new FixedLengthStringData(42).isAPartOf(tt502Rec, 4);
  	public ZonedDecimalData[] insprm = ZDArrayPartOfStructure(7, 6, 0, insprms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(insprms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData insprm01 = new ZonedDecimalData(6, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData insprm02 = new ZonedDecimalData(6, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData insprm03 = new ZonedDecimalData(6, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData insprm04 = new ZonedDecimalData(6, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData insprm05 = new ZonedDecimalData(6, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData insprm06 = new ZonedDecimalData(6, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData insprm07 = new ZonedDecimalData(6, 0).isAPartOf(filler, 36);
  	public FixedLengthStringData instprs = new FixedLengthStringData(42).isAPartOf(tt502Rec, 46);
  	public ZonedDecimalData[] instpr = ZDArrayPartOfStructure(7, 6, 0, instprs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(42).isAPartOf(instprs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData instpr01 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData instpr02 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData instpr03 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData instpr04 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData instpr05 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData instpr06 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData instpr07 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 36);
  	public FixedLengthStringData mortalitys = new FixedLengthStringData(7).isAPartOf(tt502Rec, 88);
  	public FixedLengthStringData[] mortality = FLSArrayPartOfStructure(7, 1, mortalitys, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(7).isAPartOf(mortalitys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mortality1 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
  	public FixedLengthStringData mortality2 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
  	public FixedLengthStringData mortality3 = new FixedLengthStringData(1).isAPartOf(filler2, 2);
  	public FixedLengthStringData mortality4 = new FixedLengthStringData(1).isAPartOf(filler2, 3);
  	public FixedLengthStringData mortality5 = new FixedLengthStringData(1).isAPartOf(filler2, 4);
  	public FixedLengthStringData mortality6 = new FixedLengthStringData(1).isAPartOf(filler2, 5);
  	public FixedLengthStringData mortality7 = new FixedLengthStringData(1).isAPartOf(filler2, 6);
  	public FixedLengthStringData mortclss = new FixedLengthStringData(7).isAPartOf(tt502Rec, 95);
  	public FixedLengthStringData[] mortcls = FLSArrayPartOfStructure(7, 1, mortclss, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(7).isAPartOf(mortclss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mortcls01 = new FixedLengthStringData(1).isAPartOf(filler3, 0);
  	public FixedLengthStringData mortcls02 = new FixedLengthStringData(1).isAPartOf(filler3, 1);
  	public FixedLengthStringData mortcls03 = new FixedLengthStringData(1).isAPartOf(filler3, 2);
  	public FixedLengthStringData mortcls04 = new FixedLengthStringData(1).isAPartOf(filler3, 3);
  	public FixedLengthStringData mortcls05 = new FixedLengthStringData(1).isAPartOf(filler3, 4);
  	public FixedLengthStringData mortcls06 = new FixedLengthStringData(1).isAPartOf(filler3, 5);
  	public FixedLengthStringData mortcls07 = new FixedLengthStringData(1).isAPartOf(filler3, 6);
  	public ZonedDecimalData premUnit = new ZonedDecimalData(3, 0).isAPartOf(tt502Rec, 102);
  	public ZonedDecimalData riskunit = new ZonedDecimalData(6, 0).isAPartOf(tt502Rec, 105);
  	public FixedLengthStringData benefitMaxs = new FixedLengthStringData(14).isAPartOf(tt502Rec, 111);
  	public ZonedDecimalData[] benefitMax = ZDArrayPartOfStructure(2, 7, 0, benefitMaxs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(14).isAPartOf(benefitMaxs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData benefitMax01 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData benefitMax02 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 7);
  	public FixedLengthStringData benefitMins = new FixedLengthStringData(14).isAPartOf(tt502Rec, 125);
  	public ZonedDecimalData[] benefitMin = ZDArrayPartOfStructure(2, 7, 0, benefitMins, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(14).isAPartOf(benefitMins, 0, FILLER_REDEFINE);
  	public ZonedDecimalData benefitMin01 = new ZonedDecimalData(7, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData benefitMin02 = new ZonedDecimalData(7, 0).isAPartOf(filler5, 7);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(361).isAPartOf(tt502Rec, 139, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tt502Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt502Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}