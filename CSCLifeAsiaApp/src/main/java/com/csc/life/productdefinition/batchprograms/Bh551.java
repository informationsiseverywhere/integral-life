/*
 * File: Bh551.java
 * Date: 29 August 2009 21:32:18
 * Author: Quipoz Limited
 * 
 * Class transformed from BH551.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.dataaccess.HratTableDAM;
import com.csc.life.newbusiness.tablestructures.T5658rec;
import com.csc.life.productdefinition.recordstructures.Ph551par;
import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This program converts premium rates uploaded from PC file
*   to ITEMPF. The format of the PC file must be in the same
*   format as HRATPF. To create the PC file in the same format
*   as HRATPF, down load HRATPF to PC in EXCEL format.
*   To add records to the PC file, first open the file in EXCEL,
*   cut and paste data from other EXCEL/LOTUS worksheets into the
*   HRAT format file. To convert the data to ITEMPF premium rates,
*   upload the EXCEL file (as DATABASE file with replace option)
*   back to HRATPF and then run the batch schedule(L2UPLDPRM!!).
*   This program allows uploading to T5658 or T5664 only but other
*   uploading can be cloned and done similarly.
*
*
*   The basic procedure division logic is for reading a Primary fi e
*   and the input details are used to update other files according to
*   certain conditions.
*
*   Initialise
*     - Set up Promary file HRAT key.
*
*    Read
*     - Read first primary file record.
*
*    Perform    Until End of File
*
*      Edit
*       - No editing required.
*
*      Update
*       - Check if record already exists on ITEMPF.
*       - If record exists and have the same effective date, updat 
*         the ITDM record with the details from HRAT.
*       - If record exists but with different effective date, upda e
*         the existing ITDM record with a end date(effective date  
*         1 day) and then write a new ITDM record with the details
*         from HRAT.
*       - If record does not exist, write a new ITDM and DESC reco d
*         with details from HRAT.
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  - No. of PC Records Read
*     02  - No. of Records Written
*     03  - No. of Records Updated
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*
*****************************************************************
* </pre>
*/
public class Bh551 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH551");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaScheduleNumber = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaItdmItmto = new ZonedDecimalData(8, 0).init(99999999).setUnsigned();
	private FixedLengthStringData wsaaItdmFunction = new FixedLengthStringData(5).init(SPACES);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t5658 = "T5658";
	private static final String t5664 = "T5664";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private HratTableDAM hratIO = new HratTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T5658rec t5658rec = new T5658rec();
	private T5664rec t5664rec = new T5664rec();
	private Ph551par ph551par = new Ph551par();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setup3011, 
		callItdm3013
	}

	public Bh551() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		/* Open required files.*/
		ph551par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		/* Move fields to primary file key.*/
		hratIO.setParams(SPACES);
		hratIO.setFunction(varcom.begn);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		/*  Call the I/O module or do a Standard COBOL read on*/
		/*     the primary file.*/
		SmartFileCode.execute(appVars, hratIO);
		if (isNE(hratIO.getStatuz(), varcom.oK)
		&& isNE(hratIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hratIO.getParams());
			fatalError600();
		}
		if (isEQ(hratIO.getStatuz(), varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
				case setup3011: 
					setup3011();
				case callItdm3013: 
					callItdm3013();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		/* Update database records.*/
		itdmIO.setParams(SPACES);
		/* See if the record already exists.*/
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(ph551par.itemtabl);
		itdmIO.setItemitem(hratIO.getItemitem());
		itdmIO.setItmfrm(ph551par.efdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/* Enf of file ie. record does not exist.*/
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaItdmFunction.set(varcom.writr);
			wsaaDesc.set("Y");
			wsaaItdmItmto.set(varcom.vrcmMaxDate);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.setup3011);
		}
		/* Key break ie. record does not exist.*/
		if (isNE(bsprIO.getCompany(), itdmIO.getItemcoy())
		|| isNE(ph551par.itemtabl, itdmIO.getItemtabl())
		|| isNE(hratIO.getItemitem(), itdmIO.getItemitem())) {
			itdmIO.setFunction(varcom.nextp);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isNE(bsprIO.getCompany(), itdmIO.getItemcoy())
			|| isNE(ph551par.itemtabl, itdmIO.getItemtabl())
			|| isNE(hratIO.getItemitem(), itdmIO.getItemitem())
			|| isEQ(hratIO.getStatuz(), varcom.endp)) {
				wsaaDesc.set("Y");
				wsaaItdmItmto.set(varcom.vrcmMaxDate);
			}
			else {
				datcon2rec.datcon2Rec.set(SPACES);
				datcon2rec.intDate1.set(itdmIO.getItmfrm());
				datcon2rec.freqFactor.set(-1);
				datcon2rec.frequency.set("DY");
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(itdmIO.getParams());
					fatalError600();
				}
				wsaaItdmItmto.set(datcon2rec.intDate2);
				wsaaDesc.set("N");
			}
			wsaaItdmFunction.set(varcom.writr);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.setup3011);
		}
		/* Record exists. Check if the effective date is the same.*/
		/* If the same, rewrite the record with the new details.*/
		/* If not the same, rewrite the existing record with a end date*/
		/* and write a new record with different effective date.*/
		if (isEQ(itdmIO.getItmfrm(), ph551par.efdate)) {
			wsaaDesc.set("N");
			wsaaItdmFunction.set(varcom.rewrt);
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			wsaaItdmItmto.set(itdmIO.getItmto());
			itdmIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(ph551par.efdate);
			datcon2rec.freqFactor.set(-1);
			datcon2rec.frequency.set("DY");
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			itdmIO.setItmto(datcon2rec.intDate2);
			itdmIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			wsaaItdmFunction.set(varcom.writr);
			wsaaDesc.set("N");
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void setup3011()
	{
		/* If record exists(same effective date), need to read and hold*/
		/* the record for rewrite.*/
		if (isEQ(wsaaItdmFunction, varcom.rewrt)) {
			itdmIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(ph551par.itemtabl, t5664)) {
			setupT56644500();
		}
		else {
			setupT56584600();
		}
		if (isEQ(wsaaItdmFunction, varcom.rewrt)) {
			itdmIO.setFunction(varcom.rewrt);
			goTo(GotoLabel.callItdm3013);
		}
		/* Write a new ITDM and DESC record.*/
		wsaaScheduleNumber.set(bsscIO.getScheduleNumber());
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(ph551par.itemtabl);
		itdmIO.setItemitem(hratIO.getItemitem());
		itdmIO.setItmfrm(ph551par.efdate);
		itdmIO.setItmto(wsaaItdmItmto);
		itdmIO.setTranid(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(bsscIO.getScheduleName(), SPACES);
		stringVariable1.addExpression(wsaaScheduleNumber, SPACES);
		stringVariable1.setStringInto(itdmIO.getTranid());
		itdmIO.setValidflag("1");
		if (isEQ(ph551par.itemtabl, t5658)) {
			itdmIO.setTableprog("P5658");
		}
		else {
			itdmIO.setTableprog("P5664");
		}
		itdmIO.setFunction(varcom.writr);
		/* No need to write a DESC if an ITDM record already exists.*/
		if (isEQ(wsaaDesc, "N")) {
			goTo(GotoLabel.callItdm3013);
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(ph551par.itemtabl);
		descIO.setDescitem(hratIO.getItemitem());
		descIO.setTranid(itdmIO.getTranid());
		descIO.setLongdesc(hratIO.getItemitem());
		descIO.setShortdesc(hratIO.getItemitem());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.writr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.dupr)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void callItdm3013()
	{
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/*NEAR-EXIT*/
		hratIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void setupT56644500()
	{
		para4510();
	}

protected void para4510()
	{
		t5664rec.t5664Rec.set(SPACES);
		t5664rec.insprms.set(hratIO.getInsprms());
		/*  MOVE HRAT-INSTPR            TO T5664-INSTPR.                 */
		t5664rec.instprs.set(hratIO.getInstprs());
		t5664rec.mfacthm.set(hratIO.getMfacthm());
		t5664rec.mfacthy.set(hratIO.getMfacthy());
		t5664rec.mfactm.set(hratIO.getMfactm());
		t5664rec.mfactq.set(hratIO.getMfactq());
		t5664rec.mfactw.set(hratIO.getMfactw());
		t5664rec.mfact2w.set(hratIO.getMfact2w());
		t5664rec.mfact4w.set(hratIO.getMfact4w());
		t5664rec.mfacty.set(hratIO.getMfacty());
		t5664rec.premUnit.set(hratIO.getPremUnit());
		t5664rec.unit.set(hratIO.getUnit());
		t5664rec.disccntmeth.set(hratIO.getDisccntmeth());
		t5664rec.insprem.set(hratIO.getInsprem());
		itdmIO.setGenarea(t5664rec.t5664Rec);
	}

protected void setupT56584600()
	{
		para4610();
	}

protected void para4610()
	{
		t5658rec.t5658Rec.set(SPACES);
		t5658rec.insprms.set(hratIO.getInsprms());
		/*    MOVE HRAT-INSTPR            TO T5658-INSTPR.                 */
		t5658rec.instprs.set(hratIO.getInstprs());
		t5658rec.mfacthm.set(hratIO.getMfacthm());
		t5658rec.mfacthy.set(hratIO.getMfacthy());
		t5658rec.mfactm.set(hratIO.getMfactm());
		t5658rec.mfactq.set(hratIO.getMfactq());
		t5658rec.mfactw.set(hratIO.getMfactw());
		t5658rec.mfact2w.set(hratIO.getMfact2w());
		t5658rec.mfact4w.set(hratIO.getMfact4w());
		t5658rec.mfacty.set(hratIO.getMfacty());
		t5658rec.premUnit.set(hratIO.getPremUnit());
		t5658rec.unit.set(hratIO.getUnit());
		t5658rec.disccntmeth.set(hratIO.getDisccntmeth());
		t5664rec.insprem.set(hratIO.getInsprem());
		itdmIO.setGenarea(t5658rec.t5658Rec);
	}
}
