/*
 * File: P5645.java
 * Date: 30 August 2009 0:32:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P5645.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.S5645ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.csc.util.SmartTableUtility;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
 *
 *
 *****************************************************************
 * </pre>
 */
public class P5645 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5645");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "";
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	/* ERRORS */
	private String e026 = "E026";
	private String e027 = "E027";
	/* FORMATS */
	private String itemrec = "ITEMREC";
	/* Logical File: Extra data screen */
	private DescTableDAM descIO = new DescTableDAM();
	/* Logical File: SMART table reference data */
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5645ScreenVars sv = ScreenProgram.getScreenVars(S5645ScreenVars.class);

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, readPrimaryRecord1020, generalArea1045, other1080, preExit, exit2090, other3080, exit3090, continue4020, other4080
	}

	public P5645() {
		super();
		screenVars = sv;
		new ScreenModel("S5645", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case readPrimaryRecord1020: {
					readPrimaryRecord1020();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				case other1080: {
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1010() {
		if (isNE(wsaaFirstTime, "Y")) {
			goTo(GotoLabel.readPrimaryRecord1020);
		}
		if (isEQ(itemIO.getItemseq(), SPACES)) {
			wsaaSeq.set(ZERO);
		} else {
			wsaaSeq.set(itemIO.getItemseq());
		}
		/* INITIALISE-SCREEN */
		sv.dataArea.set(SPACES);
	}

	protected void readPrimaryRecord1020() {
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq, ZERO)) {
			itemIO.setItemseq(SPACES);
		} else {
			itemIO.setItemseq(wsaaSeq);
		}
		/* READ-RECORD */
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK) && isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* READ-SECONDARY-RECORDS */
	}

	protected void readRecord1031() {
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaFirstTime = "N";
	}

	protected void moveToScreen1040() {
		if (isEQ(itemIO.getStatuz(), varcom.mrnf) && isEQ(itemIO.getItemseq(), SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf) && isEQ(wsspcomn.flag, "I")) {
			scrnparams.errorCode.set(e026);
			wsaaSeq.subtract(1);
			goTo(GotoLabel.other1080);
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5645rec.cnttot01.set(ZERO);
		t5645rec.cnttot02.set(ZERO);
		t5645rec.cnttot03.set(ZERO);
		t5645rec.cnttot04.set(ZERO);
		t5645rec.cnttot05.set(ZERO);
		t5645rec.cnttot06.set(ZERO);
		t5645rec.cnttot07.set(ZERO);
		t5645rec.cnttot08.set(ZERO);
		t5645rec.cnttot09.set(ZERO);
		t5645rec.cnttot10.set(ZERO);
		t5645rec.cnttot11.set(ZERO);
		t5645rec.cnttot12.set(ZERO);
		t5645rec.cnttot13.set(ZERO);
		t5645rec.cnttot14.set(ZERO);
		t5645rec.cnttot15.set(ZERO);
	}

	protected void generalArea1045() {
		sv.cnttots.set(t5645rec.cnttots);
		sv.glmaps.set(t5645rec.glmaps);
		sv.sacscodes.set(t5645rec.sacscodes);
		sv.sacstypes.set(t5645rec.sacstypes);
		sv.signs.set(t5645rec.signs);
		/* CONFIRMATION-FIELDS */
	}

	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

	protected void screenEdit2000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(varcom.oK);
		/* VALIDATE */
		if (isEQ(scrnparams.statuz, varcom.rold) && isEQ(itemIO.getItemseq(), SPACES)) {
			scrnparams.errorCode.set(e027);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu) && isEQ(itemIO.getItemseq(), "99")) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/* OTHER */
	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* EXIT */
	}

	protected void update3000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void preparation3010() {
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
	}

	protected void updatePrimaryRecord3050() {
		itemIO.setFunction(varcom.readr);// IJTI-327
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq, ZERO)) {
			itemIO.setItemseq(SPACES);
		} else {
			itemIO.setItemseq(wsaaSeq);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK) && isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

	protected void updateRecord3055() {
		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
		// IJTI-327 STARTS
		// itemIO.setGenarea(t5645rec.t5645Rec);
		Itempf itempf = SmartTableUtility.getItempfByItemTableDAM(itemIO);
		itempf.setGenarea(t5645rec.t5645Rec.toString().getBytes());
		itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
				.getGENAREAJString(t5645rec.t5645Rec.toString().getBytes(), t5645rec));
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			// itemIO.setFunction(varcom.writr);
			// itemIO.setFormat(itemrec);
			itemDAO.insertSmartTableItem(itempf);
		} else {
			// itemIO.setFunction(varcom.rewrt);
			itemDAO.updateSmartTableItem(itempf);
		}
		/*
		 * SmartFileCode.execute(appVars, itemIO); if
		 * (isNE(itemIO.getStatuz(),varcom.oK)) {
		 * syserrrec.params.set(itemIO.getParams()); fatalError600(); }
		 */
		// IJTI-327 ENDS
	}
	
	
	protected void checkChanges3100() {
		check3100();
	}

	protected void check3100() {
		if (isNE(sv.cnttots, t5645rec.cnttots)) {
			t5645rec.cnttots.set(sv.cnttots);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.glmaps, t5645rec.glmaps)) {
			t5645rec.glmaps.set(sv.glmaps);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sacscodes, t5645rec.sacscodes)) {
			t5645rec.sacscodes.set(sv.sacscodes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sacstypes, t5645rec.sacstypes)) {
			t5645rec.sacstypes.set(sv.sacstypes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.signs, t5645rec.signs)) {
			t5645rec.signs.set(sv.signs);
			wsaaUpdateFlag = "Y";
		}
	}

	protected void whereNext4000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation4020();
				}
				case continue4020: {
					continue4020();
				}
				case other4080: {
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void preparation4020() {
		if (isNE(scrnparams.statuz, varcom.rolu) && isNE(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.continue4020);
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaSeq.add(1);
		} else {
			wsaaSeq.subtract(1);
		}
		goTo(GotoLabel.other4080);
	}

	protected void continue4020() {
		wsspcomn.programPtr.add(1);
		wsaaSeq.set(ZERO);
	}
}
