/*
 * File: Br50i.java
 * Date: 29 August 2009 22:12:45
 * Author: Quipoz Limited
 * 
 * Class transformed from BR50I.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.productdefinition.dataaccess.NterpfTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifeclnt;
import com.csc.life.productdefinition.reports.Rr50iReport;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.procedures.Spcout;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Spcoutrec;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This is a skeleton for a batch mainline program.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*    Read
*     - read first primary file record.
*
*    Perform    Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock the record if it is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Br50i extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rr50iReport printerFile = new Rr50iReport();
	private NterpfTableDAM nterpf = new NterpfTableDAM();
	private FixedLengthStringData printerRec = new FixedLengthStringData(300);
	private NterpfTableDAM nterpfRec = new NterpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR50I");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaToFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaToFilename = new FixedLengthStringData(4).isAPartOf(wsaaToFile, 0);
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaToFile, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaToFile, 6).setUnsigned();
	private PackedDecimalData wsaaCntcount = new PackedDecimalData(6, 0).setUnsigned();
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);
	private PackedDecimalData wsaaSacscurbal = new PackedDecimalData(17, 2);

		/* CONTROL-TOTALS */
	private int ct01 = 1;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rr50iH01 = new FixedLengthStringData(51);
	private FixedLengthStringData rr50ih01O = new FixedLengthStringData(51).isAPartOf(rr50iH01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr50ih01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr50ih01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr50ih01O, 11);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rr50ih01O, 41);

	private FixedLengthStringData rr50iD01 = new FixedLengthStringData(171);
	private FixedLengthStringData rr50id01O = new FixedLengthStringData(171).isAPartOf(rr50iD01, 0);
	private FixedLengthStringData rd01Hprrcvdt = new FixedLengthStringData(10).isAPartOf(rr50id01O, 0);
	private ZonedDecimalData rd01Tdayno = new ZonedDecimalData(3, 0).isAPartOf(rr50id01O, 10);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rr50id01O, 13);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(rr50id01O, 21);
	private FixedLengthStringData rd01Acdes = new FixedLengthStringData(30).isAPartOf(rr50id01O, 24);
	private FixedLengthStringData rd01Occdate = new FixedLengthStringData(10).isAPartOf(rr50id01O, 54);
	private FixedLengthStringData rd01Statcode = new FixedLengthStringData(2).isAPartOf(rr50id01O, 64);
	private ZonedDecimalData rd01Instprem = new ZonedDecimalData(17, 2).isAPartOf(rr50id01O, 66);
	private ZonedDecimalData rd01Premsusp = new ZonedDecimalData(17, 2).isAPartOf(rr50id01O, 83);
	private ZonedDecimalData rd01Totalfee = new ZonedDecimalData(17, 2).isAPartOf(rr50id01O, 100);
	private FixedLengthStringData rd01Eror = new FixedLengthStringData(4).isAPartOf(rr50id01O, 117);
	private FixedLengthStringData rd01Desc = new FixedLengthStringData(50).isAPartOf(rr50id01O, 121);

	private FixedLengthStringData rr50iT01 = new FixedLengthStringData(6);
	private FixedLengthStringData rr50it01O = new FixedLengthStringData(6).isAPartOf(rr50iT01, 0);
	private ZonedDecimalData rt01Cntcount = new ZonedDecimalData(6, 0).isAPartOf(rr50it01O, 0);

	private FixedLengthStringData rr50iE01 = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

		/*Contract header - life new business*/
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Spcoutrec spcoutrec = new Spcoutrec();
	private T5645rec t5645rec = new T5645rec();
	//ILIFE-4940 by liwei
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Map<String, List<Itempf>> t5645ListMap = new HashMap<String, List<Itempf>>();
	private Map<String,Descpf> t1693Map =  new HashMap<String, Descpf>();
	private Acblpf acblpf = new Acblpf();
	private Chdrpf chdrlnbpf = new Chdrpf();

	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		printError3050
	}

	public Br50i() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		initSamrtTable();
		openFiles1020();
		setUpHeadingCompany1030();
		setUpHeadingDates1040();
		readT56451050();
	}

protected void initialise1010()
	{
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaToFilename.set("NTER");
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(NTERPF) ");
		stringVariable1.append("TOFILE(*LIBL/");
		stringVariable1.append(wsaaToFile.toString());
		stringVariable1.append(") ");
		stringVariable1.append("MBR(*FIRST)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		wsaaQcmdexc.set("CHGPRTF FILE(RR50I) PAGESIZE(*SAME 198) CPI(15)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
	}
protected void initSamrtTable(){
	t5645ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), "T5645");
	t1693Map = descDAO.getItems("IT", "0", "T1693", bsscIO.getLanguage().toString());
}
protected void openFiles1020()
	{
		nterpf.openInput();
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaChdrnum.set(SPACES);
		wsaaCntcount.set(ZERO);
	}

protected void setUpHeadingCompany1030()
	{
		boolean descFound = false;
		if(t1693Map != null){
			if(t1693Map.get(bsprIO.getCompany().toString()) != null){
				descFound = true;
			}
		}
		if(descFound){
			rh01Company.set(bsprIO.getCompany());
			rh01Companynm.set(t1693Map.get(bsprIO.getCompany().toString()).getLongdesc());
		}else{
			syserrrec.params.set("T1693");
			fatalError600();
		}
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
	}

protected void readT56451050()
	{
		boolean itemFound = false;
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5645ListMap.containsKey(wsaaProg.toString())) {
			itempfList = t5645ListMap.get(wsaaProg.toString());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;

			}
		}
		if (!itemFound) {
			syserrrec.params.set("T5645");
			fatalError600();
		}
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		nterpf.readNext(nterpfRec);
		if (nterpf.isAtEnd()) {
			wsaaEof.set("Y");
		}
		if (isEQ(wsaaEof,"Y")) {
			printTotLine2100();
			wsspEdterror.set(varcom.endp);
		}
		/*EXIT*/
	}

protected void printTotLine2100()
	{
		/*START*/
		rt01Cntcount.set(wsaaCntcount);
		printerFile.printRr50it01(rr50iT01, indicArea);
		printerFile.printRr50ie01(rr50iE01, indicArea);
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					update3010();
					life3020();
					readAcbl3030();
				}
				case printError3050: {
					printError3050();
					writeDetail3080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		if (isEQ(nterpfRec.chdrnum,wsaaChdrnum)) {
			goTo(GotoLabel.printError3050);
		}
		wsaaCntcount.add(1);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(nterpfRec.hprrcvdt);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rd01Hprrcvdt.set(datcon1rec.extDate);
		rd01Tdayno.set(nterpfRec.tdayno);
		rd01Chdrnum.set(nterpfRec.chdrnum);

		chdrlnbpf = chdrpfDAO.getChdrlnbRecord(nterpfRec.chdrcoy.toString(), nterpfRec.chdrnum.toString());
		if(chdrlnbpf == null){
			fatalError600();	
		}
		rd01Cnttype.set(chdrlnbpf.getCnttype());
		rd01Statcode.set(chdrlnbpf.getStatcode());
	}

protected void life3020()
	{
        List<Lifeclnt> lifeclntList = lifepfDAO.searchLifeClntRecord(Character.toString(chdrlnbpf.getChdrcoy()), chdrlnbpf.getChdrnum(), "00","01",
        		Character.toString(chdrlnbpf.getCowncoy()), chdrlnbpf.getCownpfx());
        if (lifeclntList == null || lifeclntList.size() <= 0) {
            fatalError600();
        } else {//IJTI-320 START
        	Lifeclnt lifepf = lifeclntList.get(0);
    		spcoutrec.field.set(SPACES);
    		StringBuilder stringVariable1 = new StringBuilder();
    		stringVariable1.append(delimitedExp(lifepf.getCllsurname(), "  "));
    		stringVariable1.append(" ");
    		stringVariable1.append(delimitedExp(lifepf.getCllgivname(), "  "));
    		spcoutrec.field.setLeft(stringVariable1.toString());
    		callProgram(Spcout.class, spcoutrec.spcoutRec);
    		rd01Acdes.set(spcoutrec.field);
    		datcon1rec.function.set(varcom.conv);
    		datcon1rec.intDate.set(chdrlnbpf.getOccdate());
    		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    		rd01Occdate.set(datcon1rec.extDate);	
        }
        //IJTI-320 END
	}

protected void readAcbl3030()
	{
		wsaaSacscode.set(t5645rec.sacscode01);
		wsaaSacstyp.set(t5645rec.sacstype01.trim());
		callAcblenq3100();
		rd01Totalfee.set(wsaaSacscurbal);
		wsaaSacscode.set(t5645rec.sacscode02);
		wsaaSacstyp.set(t5645rec.sacstype02.trim());
		callAcblenq3100();
		rd01Premsusp.set(wsaaSacscurbal);
	}

protected void printError3050()
	{
		rd01Instprem.set(nterpfRec.instprem);
		rd01Desc.set(nterpfRec.desc);
		rd01Eror.set(nterpfRec.eror);
		wsaaChdrnum.set(nterpfRec.chdrnum);
	}

protected void writeDetail3080()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRr50ih01(rr50iH01, indicArea);
			wsaaOverflow.set("N");
		}
		printerFile.printRr50id01(rr50iD01, indicArea);
		/*EXIT*/
	}

protected void callAcblenq3100()
	{
		start3100();
	}

protected void start3100(){
	acblpf = null;
	acblpf = acblDao.getAcblpfRecord(Character.toString(chdrlnbpf.getChdrcoy()), wsaaSacscode.toString(), chdrlnbpf.getChdrnum(), wsaaSacstyp.toString().trim(),chdrlnbpf.getCntcurr());
	if(acblpf!=null) {
		wsaaSacscurbal.set(acblpf.getSacscurbal());
	}
}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
