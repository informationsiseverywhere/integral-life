/*
 * File: T6650pt.java
 * Date: 30 August 2009 2:28:59
 * Author: Quipoz Limited
 * 
 * Class transformed from T6650PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T6650rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6650.
*
*
*****************************************************************
* </pre>
*/
public class T6650pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Stamp Duty Paramaters                     S6650");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(50);
	private FixedLengthStringData filler7 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 14);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 24, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine003, 29, FILLER).init("Valid to:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 40);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(58);
	private FixedLengthStringData filler10 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine004, 15, FILLER).init("Sum Assured To        Factor           Rate");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(60);
	private FixedLengthStringData filler12 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 14).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 37).setPattern("ZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine005, 53).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(60);
	private FixedLengthStringData filler15 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine006, 14).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 37).setPattern("ZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine006, 53).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(60);
	private FixedLengthStringData filler18 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine007, 14).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 37).setPattern("ZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(60);
	private FixedLengthStringData filler21 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine008, 14).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 37).setPattern("ZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(60);
	private FixedLengthStringData filler24 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine009, 14).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 37).setPattern("ZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine009, 53).setPattern("ZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(60);
	private FixedLengthStringData filler27 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine010, 14).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 42, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(6, 4).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZ.ZZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6650rec t6650rec = new T6650rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6650pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6650rec.t6650Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6650rec.sumin01);
		fieldNo008.set(t6650rec.fact01);
		fieldNo009.set(t6650rec.rrat01);
		fieldNo010.set(t6650rec.sumin02);
		fieldNo011.set(t6650rec.fact02);
		fieldNo012.set(t6650rec.rrat02);
		fieldNo013.set(t6650rec.sumin03);
		fieldNo014.set(t6650rec.fact03);
		fieldNo015.set(t6650rec.rrat03);
		fieldNo016.set(t6650rec.sumin04);
		fieldNo017.set(t6650rec.fact04);
		fieldNo018.set(t6650rec.rrat04);
		fieldNo019.set(t6650rec.sumin05);
		fieldNo020.set(t6650rec.fact05);
		fieldNo021.set(t6650rec.rrat05);
		fieldNo022.set(t6650rec.sumin06);
		fieldNo023.set(t6650rec.fact06);
		fieldNo024.set(t6650rec.rrat06);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
