package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MliasecTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:40
 * Class transformed from MLIASEC.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MliasecTableDAM extends MliapfTableDAM {

	public MliasecTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MLIASEC");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "SECURITYNO";
		
		QUALIFIEDCOLUMNS = 
		            "MLCOYCDE, " +
		            "MLENTITY, " +
		            "SECURITYNO, " +
		            "MLOLDIC, " +
		            "MLOTHIC, " +
		            "CLNTNAML, " +
		            "DOB, " +
		            "RSKFLG, " +
		            "HPROPDTE, " +
		            "MLHSPERD, " +
		            "MLMEDCDE01, " +
		            "MLMEDCDE02, " +
		            "MLMEDCDE03, " +
		            "ACTN, " +
		            "INDC, " +
		            "MIND, " +
		            "EFFDATE, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "SECURITYNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "SECURITYNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               mlcoycde,
                               mlentity,
                               securityno,
                               mloldic,
                               mlothic,
                               clntnaml,
                               dob,
                               rskflg,
                               hpropdte,
                               mlhsperd,
                               mlmedcde01,
                               mlmedcde02,
                               mlmedcde03,
                               actn,
                               indc,
                               mind,
                               effdate,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getSecurityno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, securityno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(15);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller30.setInternal(securityno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(198);
		
		nonKeyData.set(
					getMlcoycde().toInternal()
					+ getMlentity().toInternal()
					+ nonKeyFiller30.toInternal()
					+ getMloldic().toInternal()
					+ getMlothic().toInternal()
					+ getClntnaml().toInternal()
					+ getDob().toInternal()
					+ getRskflg().toInternal()
					+ getHpropdte().toInternal()
					+ getMlhsperd().toInternal()
					+ getMlmedcde01().toInternal()
					+ getMlmedcde02().toInternal()
					+ getMlmedcde03().toInternal()
					+ getActn().toInternal()
					+ getIndc().toInternal()
					+ getMind().toInternal()
					+ getEffdate().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, mlcoycde);
			what = ExternalData.chop(what, mlentity);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, mloldic);
			what = ExternalData.chop(what, mlothic);
			what = ExternalData.chop(what, clntnaml);
			what = ExternalData.chop(what, dob);
			what = ExternalData.chop(what, rskflg);
			what = ExternalData.chop(what, hpropdte);
			what = ExternalData.chop(what, mlhsperd);
			what = ExternalData.chop(what, mlmedcde01);
			what = ExternalData.chop(what, mlmedcde02);
			what = ExternalData.chop(what, mlmedcde03);
			what = ExternalData.chop(what, actn);
			what = ExternalData.chop(what, indc);
			what = ExternalData.chop(what, mind);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getSecurityno() {
		return securityno;
	}
	public void setSecurityno(Object what) {
		securityno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getMlcoycde() {
		return mlcoycde;
	}
	public void setMlcoycde(Object what) {
		mlcoycde.set(what);
	}	
	public FixedLengthStringData getMlentity() {
		return mlentity;
	}
	public void setMlentity(Object what) {
		mlentity.set(what);
	}	
	public FixedLengthStringData getMloldic() {
		return mloldic;
	}
	public void setMloldic(Object what) {
		mloldic.set(what);
	}	
	public FixedLengthStringData getMlothic() {
		return mlothic;
	}
	public void setMlothic(Object what) {
		mlothic.set(what);
	}	
	public FixedLengthStringData getClntnaml() {
		return clntnaml;
	}
	public void setClntnaml(Object what) {
		clntnaml.set(what);
	}	
	public PackedDecimalData getDob() {
		return dob;
	}
	public void setDob(Object what) {
		setDob(what, false);
	}
	public void setDob(Object what, boolean rounded) {
		if (rounded)
			dob.setRounded(what);
		else
			dob.set(what);
	}	
	public FixedLengthStringData getRskflg() {
		return rskflg;
	}
	public void setRskflg(Object what) {
		rskflg.set(what);
	}	
	public PackedDecimalData getHpropdte() {
		return hpropdte;
	}
	public void setHpropdte(Object what) {
		setHpropdte(what, false);
	}
	public void setHpropdte(Object what, boolean rounded) {
		if (rounded)
			hpropdte.setRounded(what);
		else
			hpropdte.set(what);
	}	
	public PackedDecimalData getMlhsperd() {
		return mlhsperd;
	}
	public void setMlhsperd(Object what) {
		setMlhsperd(what, false);
	}
	public void setMlhsperd(Object what, boolean rounded) {
		if (rounded)
			mlhsperd.setRounded(what);
		else
			mlhsperd.set(what);
	}	
	public FixedLengthStringData getMlmedcde01() {
		return mlmedcde01;
	}
	public void setMlmedcde01(Object what) {
		mlmedcde01.set(what);
	}	
	public FixedLengthStringData getMlmedcde02() {
		return mlmedcde02;
	}
	public void setMlmedcde02(Object what) {
		mlmedcde02.set(what);
	}	
	public FixedLengthStringData getMlmedcde03() {
		return mlmedcde03;
	}
	public void setMlmedcde03(Object what) {
		mlmedcde03.set(what);
	}	
	public FixedLengthStringData getActn() {
		return actn;
	}
	public void setActn(Object what) {
		actn.set(what);
	}	
	public FixedLengthStringData getIndc() {
		return indc;
	}
	public void setIndc(Object what) {
		indc.set(what);
	}	
	public FixedLengthStringData getMind() {
		return mind;
	}
	public void setMind(Object what) {
		mind.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getMlmedcdes() {
		return new FixedLengthStringData(mlmedcde01.toInternal()
										+ mlmedcde02.toInternal()
										+ mlmedcde03.toInternal());
	}
	public void setMlmedcdes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMlmedcdes().getLength()).init(obj);
	
		what = ExternalData.chop(what, mlmedcde01);
		what = ExternalData.chop(what, mlmedcde02);
		what = ExternalData.chop(what, mlmedcde03);
	}
	public FixedLengthStringData getMlmedcde(BaseData indx) {
		return getMlmedcde(indx.toInt());
	}
	public FixedLengthStringData getMlmedcde(int indx) {

		switch (indx) {
			case 1 : return mlmedcde01;
			case 2 : return mlmedcde02;
			case 3 : return mlmedcde03;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMlmedcde(BaseData indx, Object what) {
		setMlmedcde(indx.toInt(), what);
	}
	public void setMlmedcde(int indx, Object what) {

		switch (indx) {
			case 1 : setMlmedcde01(what);
					 break;
			case 2 : setMlmedcde02(what);
					 break;
			case 3 : setMlmedcde03(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		securityno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		mlcoycde.clear();
		mlentity.clear();
		nonKeyFiller30.clear();
		mloldic.clear();
		mlothic.clear();
		clntnaml.clear();
		dob.clear();
		rskflg.clear();
		hpropdte.clear();
		mlhsperd.clear();
		mlmedcde01.clear();
		mlmedcde02.clear();
		mlmedcde03.clear();
		actn.clear();
		indc.clear();
		mind.clear();
		effdate.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}