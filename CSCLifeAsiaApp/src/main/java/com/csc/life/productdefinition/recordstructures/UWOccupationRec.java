package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class UWOccupationRec extends ExternalData {

	public FixedLengthStringData calcuwRec = new FixedLengthStringData(126);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(calcuwRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(calcuwRec, 5);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(calcuwRec, 9);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(calcuwRec, 12);
  	public FixedLengthStringData indusType = new FixedLengthStringData(20).isAPartOf(calcuwRec, 16);
  	public FixedLengthStringData riskClass = new FixedLengthStringData(4).isAPartOf(calcuwRec, 36);
  	public FixedLengthStringData transEffdate = new FixedLengthStringData(8).isAPartOf(calcuwRec, 40);
  	public FixedLengthStringData occupCode = new FixedLengthStringData(4).isAPartOf(calcuwRec, 48);
  	
  	//Outputs
  	public FixedLengthStringData outputUWDec = new FixedLengthStringData(50).isAPartOf(calcuwRec, 52);
  	public FixedLengthStringData outputSplTermCode = new FixedLengthStringData(2).isAPartOf(calcuwRec, 102);
  	public ZonedDecimalData outputSplTermSAPer = new ZonedDecimalData(5, 2).isAPartOf(calcuwRec, 104);
  	public ZonedDecimalData outputSplTermAge = new ZonedDecimalData(3, 0).isAPartOf(calcuwRec, 109);
  	public ZonedDecimalData outputSplTermLoadPer = new ZonedDecimalData(5, 2).isAPartOf(calcuwRec, 112);
  	public ZonedDecimalData outputSplTermRateAdj = new ZonedDecimalData(6, 0).isAPartOf(calcuwRec, 117);
  	public ZonedDecimalData outputMortPerc = new ZonedDecimalData(3, 0).isAPartOf(calcuwRec, 123);
 

  	public void initialize() {
		COBOLFunctions.initialize(calcuwRec);
	}	
	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			calcuwRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}
