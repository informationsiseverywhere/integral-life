package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr567screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 11, 3, 58}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr567ScreenVars sv = (Sr567ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr567screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr567ScreenVars screenVars = (Sr567ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.mnth01.setClassString("");
		screenVars.linstamt01.setClassString("");
		screenVars.mnth02.setClassString("");
		screenVars.mnth03.setClassString("");
		screenVars.mnth04.setClassString("");
		screenVars.mnth05.setClassString("");
		screenVars.mnth06.setClassString("");
		screenVars.mnth07.setClassString("");
		screenVars.linstamt02.setClassString("");
		screenVars.linstamt03.setClassString("");
		screenVars.linstamt04.setClassString("");
		screenVars.linstamt05.setClassString("");
		screenVars.linstamt06.setClassString("");
		screenVars.linstamt07.setClassString("");
		screenVars.datedue01Disp.setClassString("");
		screenVars.datedue02Disp.setClassString("");
		screenVars.datedue03Disp.setClassString("");
		screenVars.datedue04Disp.setClassString("");
		screenVars.datedue05Disp.setClassString("");
		screenVars.datedue06Disp.setClassString("");
		screenVars.datedue07Disp.setClassString("");
	}

/**
 * Clear all the variables in Sr567screen
 */
	public static void clear(VarModel pv) {
		Sr567ScreenVars screenVars = (Sr567ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.mnth01.clear();
		screenVars.linstamt01.clear();
		screenVars.mnth02.clear();
		screenVars.mnth03.clear();
		screenVars.mnth04.clear();
		screenVars.mnth05.clear();
		screenVars.mnth06.clear();
		screenVars.mnth07.clear();
		screenVars.linstamt02.clear();
		screenVars.linstamt03.clear();
		screenVars.linstamt04.clear();
		screenVars.linstamt05.clear();
		screenVars.linstamt06.clear();
		screenVars.linstamt07.clear();
		screenVars.datedue01Disp.clear();
		screenVars.datedue01.clear();
		screenVars.datedue02Disp.clear();
		screenVars.datedue02.clear();
		screenVars.datedue03Disp.clear();
		screenVars.datedue03.clear();
		screenVars.datedue04Disp.clear();
		screenVars.datedue04.clear();
		screenVars.datedue05Disp.clear();
		screenVars.datedue05.clear();
		screenVars.datedue06Disp.clear();
		screenVars.datedue06.clear();
		screenVars.datedue07Disp.clear();
		screenVars.datedue07.clear();
	}
}
