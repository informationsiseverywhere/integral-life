package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:39
 * Description:
 * Copybook name: TH617REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th617rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th617Rec = new FixedLengthStringData(462);
  	public FixedLengthStringData disccntmeth = new FixedLengthStringData(4).isAPartOf(th617Rec, 0);
  	public FixedLengthStringData insprms = new FixedLengthStringData(396).isAPartOf(th617Rec, 4);
  	public PackedDecimalData[] insprm = PDArrayPartOfStructure(99, 6, 0, insprms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(396).isAPartOf(insprms, 0, FILLER_REDEFINE);
  	public PackedDecimalData insprm01 = new PackedDecimalData(6, 0).isAPartOf(filler, 0);
  	public PackedDecimalData insprm02 = new PackedDecimalData(6, 0).isAPartOf(filler, 4);
  	public PackedDecimalData insprm03 = new PackedDecimalData(6, 0).isAPartOf(filler, 8);
  	public PackedDecimalData insprm04 = new PackedDecimalData(6, 0).isAPartOf(filler, 12);
  	public PackedDecimalData insprm05 = new PackedDecimalData(6, 0).isAPartOf(filler, 16);
  	public PackedDecimalData insprm06 = new PackedDecimalData(6, 0).isAPartOf(filler, 20);
  	public PackedDecimalData insprm07 = new PackedDecimalData(6, 0).isAPartOf(filler, 24);
  	public PackedDecimalData insprm08 = new PackedDecimalData(6, 0).isAPartOf(filler, 28);
  	public PackedDecimalData insprm09 = new PackedDecimalData(6, 0).isAPartOf(filler, 32);
  	public PackedDecimalData insprm10 = new PackedDecimalData(6, 0).isAPartOf(filler, 36);
  	public PackedDecimalData insprm11 = new PackedDecimalData(6, 0).isAPartOf(filler, 40);
  	public PackedDecimalData insprm12 = new PackedDecimalData(6, 0).isAPartOf(filler, 44);
  	public PackedDecimalData insprm13 = new PackedDecimalData(6, 0).isAPartOf(filler, 48);
  	public PackedDecimalData insprm14 = new PackedDecimalData(6, 0).isAPartOf(filler, 52);
  	public PackedDecimalData insprm15 = new PackedDecimalData(6, 0).isAPartOf(filler, 56);
  	public PackedDecimalData insprm16 = new PackedDecimalData(6, 0).isAPartOf(filler, 60);
  	public PackedDecimalData insprm17 = new PackedDecimalData(6, 0).isAPartOf(filler, 64);
  	public PackedDecimalData insprm18 = new PackedDecimalData(6, 0).isAPartOf(filler, 68);
  	public PackedDecimalData insprm19 = new PackedDecimalData(6, 0).isAPartOf(filler, 72);
  	public PackedDecimalData insprm20 = new PackedDecimalData(6, 0).isAPartOf(filler, 76);
  	public PackedDecimalData insprm21 = new PackedDecimalData(6, 0).isAPartOf(filler, 80);
  	public PackedDecimalData insprm22 = new PackedDecimalData(6, 0).isAPartOf(filler, 84);
  	public PackedDecimalData insprm23 = new PackedDecimalData(6, 0).isAPartOf(filler, 88);
  	public PackedDecimalData insprm24 = new PackedDecimalData(6, 0).isAPartOf(filler, 92);
  	public PackedDecimalData insprm25 = new PackedDecimalData(6, 0).isAPartOf(filler, 96);
  	public PackedDecimalData insprm26 = new PackedDecimalData(6, 0).isAPartOf(filler, 100);
  	public PackedDecimalData insprm27 = new PackedDecimalData(6, 0).isAPartOf(filler, 104);
  	public PackedDecimalData insprm28 = new PackedDecimalData(6, 0).isAPartOf(filler, 108);
  	public PackedDecimalData insprm29 = new PackedDecimalData(6, 0).isAPartOf(filler, 112);
  	public PackedDecimalData insprm30 = new PackedDecimalData(6, 0).isAPartOf(filler, 116);
  	public PackedDecimalData insprm31 = new PackedDecimalData(6, 0).isAPartOf(filler, 120);
  	public PackedDecimalData insprm32 = new PackedDecimalData(6, 0).isAPartOf(filler, 124);
  	public PackedDecimalData insprm33 = new PackedDecimalData(6, 0).isAPartOf(filler, 128);
  	public PackedDecimalData insprm34 = new PackedDecimalData(6, 0).isAPartOf(filler, 132);
  	public PackedDecimalData insprm35 = new PackedDecimalData(6, 0).isAPartOf(filler, 136);
  	public PackedDecimalData insprm36 = new PackedDecimalData(6, 0).isAPartOf(filler, 140);
  	public PackedDecimalData insprm37 = new PackedDecimalData(6, 0).isAPartOf(filler, 144);
  	public PackedDecimalData insprm38 = new PackedDecimalData(6, 0).isAPartOf(filler, 148);
  	public PackedDecimalData insprm39 = new PackedDecimalData(6, 0).isAPartOf(filler, 152);
  	public PackedDecimalData insprm40 = new PackedDecimalData(6, 0).isAPartOf(filler, 156);
  	public PackedDecimalData insprm41 = new PackedDecimalData(6, 0).isAPartOf(filler, 160);
  	public PackedDecimalData insprm42 = new PackedDecimalData(6, 0).isAPartOf(filler, 164);
  	public PackedDecimalData insprm43 = new PackedDecimalData(6, 0).isAPartOf(filler, 168);
  	public PackedDecimalData insprm44 = new PackedDecimalData(6, 0).isAPartOf(filler, 172);
  	public PackedDecimalData insprm45 = new PackedDecimalData(6, 0).isAPartOf(filler, 176);
  	public PackedDecimalData insprm46 = new PackedDecimalData(6, 0).isAPartOf(filler, 180);
  	public PackedDecimalData insprm47 = new PackedDecimalData(6, 0).isAPartOf(filler, 184);
  	public PackedDecimalData insprm48 = new PackedDecimalData(6, 0).isAPartOf(filler, 188);
  	public PackedDecimalData insprm49 = new PackedDecimalData(6, 0).isAPartOf(filler, 192);
  	public PackedDecimalData insprm50 = new PackedDecimalData(6, 0).isAPartOf(filler, 196);
  	public PackedDecimalData insprm51 = new PackedDecimalData(6, 0).isAPartOf(filler, 200);
  	public PackedDecimalData insprm52 = new PackedDecimalData(6, 0).isAPartOf(filler, 204);
  	public PackedDecimalData insprm53 = new PackedDecimalData(6, 0).isAPartOf(filler, 208);
  	public PackedDecimalData insprm54 = new PackedDecimalData(6, 0).isAPartOf(filler, 212);
  	public PackedDecimalData insprm55 = new PackedDecimalData(6, 0).isAPartOf(filler, 216);
  	public PackedDecimalData insprm56 = new PackedDecimalData(6, 0).isAPartOf(filler, 220);
  	public PackedDecimalData insprm57 = new PackedDecimalData(6, 0).isAPartOf(filler, 224);
  	public PackedDecimalData insprm58 = new PackedDecimalData(6, 0).isAPartOf(filler, 228);
  	public PackedDecimalData insprm59 = new PackedDecimalData(6, 0).isAPartOf(filler, 232);
  	public PackedDecimalData insprm60 = new PackedDecimalData(6, 0).isAPartOf(filler, 236);
  	public PackedDecimalData insprm61 = new PackedDecimalData(6, 0).isAPartOf(filler, 240);
  	public PackedDecimalData insprm62 = new PackedDecimalData(6, 0).isAPartOf(filler, 244);
  	public PackedDecimalData insprm63 = new PackedDecimalData(6, 0).isAPartOf(filler, 248);
  	public PackedDecimalData insprm64 = new PackedDecimalData(6, 0).isAPartOf(filler, 252);
  	public PackedDecimalData insprm65 = new PackedDecimalData(6, 0).isAPartOf(filler, 256);
  	public PackedDecimalData insprm66 = new PackedDecimalData(6, 0).isAPartOf(filler, 260);
  	public PackedDecimalData insprm67 = new PackedDecimalData(6, 0).isAPartOf(filler, 264);
  	public PackedDecimalData insprm68 = new PackedDecimalData(6, 0).isAPartOf(filler, 268);
  	public PackedDecimalData insprm69 = new PackedDecimalData(6, 0).isAPartOf(filler, 272);
  	public PackedDecimalData insprm70 = new PackedDecimalData(6, 0).isAPartOf(filler, 276);
  	public PackedDecimalData insprm71 = new PackedDecimalData(6, 0).isAPartOf(filler, 280);
  	public PackedDecimalData insprm72 = new PackedDecimalData(6, 0).isAPartOf(filler, 284);
  	public PackedDecimalData insprm73 = new PackedDecimalData(6, 0).isAPartOf(filler, 288);
  	public PackedDecimalData insprm74 = new PackedDecimalData(6, 0).isAPartOf(filler, 292);
  	public PackedDecimalData insprm75 = new PackedDecimalData(6, 0).isAPartOf(filler, 296);
  	public PackedDecimalData insprm76 = new PackedDecimalData(6, 0).isAPartOf(filler, 300);
  	public PackedDecimalData insprm77 = new PackedDecimalData(6, 0).isAPartOf(filler, 304);
  	public PackedDecimalData insprm78 = new PackedDecimalData(6, 0).isAPartOf(filler, 308);
  	public PackedDecimalData insprm79 = new PackedDecimalData(6, 0).isAPartOf(filler, 312);
  	public PackedDecimalData insprm80 = new PackedDecimalData(6, 0).isAPartOf(filler, 316);
  	public PackedDecimalData insprm81 = new PackedDecimalData(6, 0).isAPartOf(filler, 320);
  	public PackedDecimalData insprm82 = new PackedDecimalData(6, 0).isAPartOf(filler, 324);
  	public PackedDecimalData insprm83 = new PackedDecimalData(6, 0).isAPartOf(filler, 328);
  	public PackedDecimalData insprm84 = new PackedDecimalData(6, 0).isAPartOf(filler, 332);
  	public PackedDecimalData insprm85 = new PackedDecimalData(6, 0).isAPartOf(filler, 336);
  	public PackedDecimalData insprm86 = new PackedDecimalData(6, 0).isAPartOf(filler, 340);
  	public PackedDecimalData insprm87 = new PackedDecimalData(6, 0).isAPartOf(filler, 344);
  	public PackedDecimalData insprm88 = new PackedDecimalData(6, 0).isAPartOf(filler, 348);
  	public PackedDecimalData insprm89 = new PackedDecimalData(6, 0).isAPartOf(filler, 352);
  	public PackedDecimalData insprm90 = new PackedDecimalData(6, 0).isAPartOf(filler, 356);
  	public PackedDecimalData insprm91 = new PackedDecimalData(6, 0).isAPartOf(filler, 360);
  	public PackedDecimalData insprm92 = new PackedDecimalData(6, 0).isAPartOf(filler, 364);
  	public PackedDecimalData insprm93 = new PackedDecimalData(6, 0).isAPartOf(filler, 368);
  	public PackedDecimalData insprm94 = new PackedDecimalData(6, 0).isAPartOf(filler, 372);
  	public PackedDecimalData insprm95 = new PackedDecimalData(6, 0).isAPartOf(filler, 376);
  	public PackedDecimalData insprm96 = new PackedDecimalData(6, 0).isAPartOf(filler, 380);
  	public PackedDecimalData insprm97 = new PackedDecimalData(6, 0).isAPartOf(filler, 384);
  	public PackedDecimalData insprm98 = new PackedDecimalData(6, 0).isAPartOf(filler, 388);
  	public PackedDecimalData insprm99 = new PackedDecimalData(6, 0).isAPartOf(filler, 392);
  	public FixedLengthStringData instprs = new FixedLengthStringData(44).isAPartOf(th617Rec, 400);
  	public PackedDecimalData[] instpr = PDArrayPartOfStructure(11, 6, 0, instprs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(44).isAPartOf(instprs, 0, FILLER_REDEFINE);
  	public PackedDecimalData instpr01 = new PackedDecimalData(6, 0).isAPartOf(filler1, 0);
  	public PackedDecimalData instpr02 = new PackedDecimalData(6, 0).isAPartOf(filler1, 4);
  	public PackedDecimalData instpr03 = new PackedDecimalData(6, 0).isAPartOf(filler1, 8);
  	public PackedDecimalData instpr04 = new PackedDecimalData(6, 0).isAPartOf(filler1, 12);
  	public PackedDecimalData instpr05 = new PackedDecimalData(6, 0).isAPartOf(filler1, 16);
  	public PackedDecimalData instpr06 = new PackedDecimalData(6, 0).isAPartOf(filler1, 20);
  	public PackedDecimalData instpr07 = new PackedDecimalData(6, 0).isAPartOf(filler1, 24);
  	public PackedDecimalData instpr08 = new PackedDecimalData(6, 0).isAPartOf(filler1, 28);
  	public PackedDecimalData instpr09 = new PackedDecimalData(6, 0).isAPartOf(filler1, 32);
  	public PackedDecimalData instpr10 = new PackedDecimalData(6, 0).isAPartOf(filler1, 36);
  	public PackedDecimalData instpr11 = new PackedDecimalData(6, 0).isAPartOf(filler1, 40);
  	public ZonedDecimalData premUnit = new ZonedDecimalData(3, 0).isAPartOf(th617Rec, 444);
  	public ZonedDecimalData unit = new ZonedDecimalData(9, 0).isAPartOf(th617Rec, 447);
  	public ZonedDecimalData insprem = new ZonedDecimalData(6, 0).isAPartOf(th617Rec, 456);


	public void initialize() {
		COBOLFunctions.initialize(th617Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th617Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}