package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50dscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 12, 3, 21, 10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr50dscreensfl";
		lrec.subfileClass = Sr50dscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 15;
		lrec.pageSubfile = 14;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 7, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50dScreenVars sv = (Sr50dScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50dscreenctlWritten, sv.Sr50dscreensflWritten, av, sv.sr50dscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50dScreenVars screenVars = (Sr50dScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.pnotecat.setClassString("");
		screenVars.jlname01.setClassString("");
		screenVars.jlname02.setClassString("");
		screenVars.desc.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
	}

/**
 * Clear all the variables in Sr50dscreenctl
 */
	public static void clear(VarModel pv) {
		Sr50dScreenVars screenVars = (Sr50dScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.rstate.clear();
		screenVars.cownnum.clear();
		screenVars.lifcnum.clear();
		screenVars.pnotecat.clear();
		screenVars.jlname01.clear();
		screenVars.jlname02.clear();
		screenVars.desc.clear();
		screenVars.pstate.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
	}
}
