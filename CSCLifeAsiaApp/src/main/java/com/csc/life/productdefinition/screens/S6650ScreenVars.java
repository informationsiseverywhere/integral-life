package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6650
 * @version 1.0 generated on 30/08/09 06:56
 * @author Quipoz
 */
public class S6650ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(600);
	public FixedLengthStringData dataFields = new FixedLengthStringData(216).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData facts = new FixedLengthStringData(30).isAPartOf(dataFields, 1);
	public ZonedDecimalData[] fact = ZDArrayPartOfStructure(6, 5, 0, facts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(facts, 0, FILLER_REDEFINE);
	public ZonedDecimalData fact01 = DD.fact.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData fact02 = DD.fact.copyToZonedDecimal().isAPartOf(filler,5);
	public ZonedDecimalData fact03 = DD.fact.copyToZonedDecimal().isAPartOf(filler,10);
	public ZonedDecimalData fact04 = DD.fact.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData fact05 = DD.fact.copyToZonedDecimal().isAPartOf(filler,20);
	public ZonedDecimalData fact06 = DD.fact.copyToZonedDecimal().isAPartOf(filler,25);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,31);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,39);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,47);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData rrats = new FixedLengthStringData(36).isAPartOf(dataFields, 85);
	public ZonedDecimalData[] rrat = ZDArrayPartOfStructure(6, 6, 4, rrats, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(rrats, 0, FILLER_REDEFINE);
	public ZonedDecimalData rrat01 = DD.rrat.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData rrat02 = DD.rrat.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData rrat03 = DD.rrat.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData rrat04 = DD.rrat.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData rrat05 = DD.rrat.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData rrat06 = DD.rrat.copyToZonedDecimal().isAPartOf(filler1,30);
	public FixedLengthStringData sumins = new FixedLengthStringData(90).isAPartOf(dataFields, 121);
	public ZonedDecimalData[] sumin = ZDArrayPartOfStructure(6, 15, 0, sumins, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(90).isAPartOf(sumins, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumin01 = DD.sumin.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData sumin02 = DD.sumin.copyToZonedDecimal().isAPartOf(filler2,15);
	public ZonedDecimalData sumin03 = DD.sumin.copyToZonedDecimal().isAPartOf(filler2,30);
	public ZonedDecimalData sumin04 = DD.sumin.copyToZonedDecimal().isAPartOf(filler2,45);
	public ZonedDecimalData sumin05 = DD.sumin.copyToZonedDecimal().isAPartOf(filler2,60);
	public ZonedDecimalData sumin06 = DD.sumin.copyToZonedDecimal().isAPartOf(filler2,75);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,211);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 216);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData factsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] factErr = FLSArrayPartOfStructure(6, 4, factsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(factsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fact01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData fact02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData fact03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData fact04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData fact05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData fact06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData rratsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] rratErr = FLSArrayPartOfStructure(6, 4, rratsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(rratsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rrat01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData rrat02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData rrat03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData rrat04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData rrat05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData rrat06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] suminErr = FLSArrayPartOfStructure(6, 4, suminsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(suminsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumin01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData sumin02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData sumin03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData sumin04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData sumin05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData sumin06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 312);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData factsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] factOut = FLSArrayPartOfStructure(6, 12, factsOut, 0);
	public FixedLengthStringData[][] factO = FLSDArrayPartOfArrayStructure(12, 1, factOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(72).isAPartOf(factsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fact01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] fact02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] fact03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] fact04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] fact05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] fact06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData rratsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] rratOut = FLSArrayPartOfStructure(6, 12, rratsOut, 0);
	public FixedLengthStringData[][] rratO = FLSDArrayPartOfArrayStructure(12, 1, rratOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(72).isAPartOf(rratsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rrat01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] rrat02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] rrat03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] rrat04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] rrat05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] rrat06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData suminsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(6, 12, suminsOut, 0);
	public FixedLengthStringData[][] suminO = FLSDArrayPartOfArrayStructure(12, 1, suminOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(72).isAPartOf(suminsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumin01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] sumin02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] sumin03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] sumin04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] sumin05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] sumin06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6650screenWritten = new LongData(0);
	public LongData S6650protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6650ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, sumin01, fact01, rrat01, sumin02, fact02, rrat02, sumin03, fact03, rrat03, sumin04, fact04, rrat04, sumin05, fact05, rrat05, sumin06, fact06, rrat06};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, sumin01Out, fact01Out, rrat01Out, sumin02Out, fact02Out, rrat02Out, sumin03Out, fact03Out, rrat03Out, sumin04Out, fact04Out, rrat04Out, sumin05Out, fact05Out, rrat05Out, sumin06Out, fact06Out, rrat06Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, sumin01Err, fact01Err, rrat01Err, sumin02Err, fact02Err, rrat02Err, sumin03Err, fact03Err, rrat03Err, sumin04Err, fact04Err, rrat04Err, sumin05Err, fact05Err, rrat05Err, sumin06Err, fact06Err, rrat06Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6650screen.class;
		protectRecord = S6650protect.class;
	}

}
