package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

public class Vpmmatc extends COBOLConvCodeModel {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String t5687 = "T5687";
	private String tr28x = "TR28X";
	
	private String h053 = "H053";
	private String itemrec = "ITEMREC";
	private String wsaaSubr = "VPMMATC";
	private Syserrrec syserrrec = new Syserrrec();
	private T5687rec t5687rec = new T5687rec();
	private ItemTableDAM  itdmIO = new ItemTableDAM();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Varcom varcom = new Varcom();
	private Matccpy matccpy = new Matccpy();
	 
	private static final String SUBROUTINE_PREFIX = "MATC";

	public Vpmmatc() {
		super();
	}

	public void mainline(Object... parmArray){
		matccpy.maturityRec = convertAndSetParam(matccpy.maturityRec, parmArray, 0); 		
	 	try {
	 		start(); 			
 		}catch (COBOLExitProgramException e) {
		}catch (Exception e) {
 			systemError();
 		}
 	}

	protected void start(){
		syserrrec.subrname.set(wsaaSubr);
		vpmcalcrec.set(SPACES);
		matccpy.status.set(varcom.oK);
		vpmcalcrec.statuz.set(varcom.oK);
		setupKey();
		if(!isEQ(matccpy.status,varcom.oK)){
			return;
		}	
		vpmcalcrec.linkageArea.set(matccpy.maturityRec);
		
		callProgram(Calcmatc.class, vpmcalcrec.vpmcalcRec);
		
		matccpy.maturityRec.set(vpmcalcrec.linkageArea);
		
		if(!isEQ(vpmcalcrec.statuz,varcom.oK)){
			matccpy.status.set(vpmcalcrec.statuz);			
		}
	}

	protected void setupKey(){
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(matccpy.crtable);
		itdmIO.setItmfrm(matccpy.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);		
		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getStatuz(), varcom.oK)||!isEQ(itdmIO.getItemcoy(), matccpy.chdrChdrcoy)||
				!isEQ(itdmIO.getItemtabl(),t5687)||!isEQ(itdmIO.getItemitem(), matccpy.crtable)){
			matccpy.status.set(h053);
			return;
		}
		t5687rec.t5687Rec.set(itdmIO.genarea);
		vpmcalcrec.vpmskey.set(SPACES);
		vpmcalcrec.vpmscoy.set(matccpy.chdrChdrcoy);
		vpmcalcrec.vpmstabl.set(tr28x);
		vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX + matccpy.chdrType.toString().trim());
		vpmcalcrec.extkey.set(t5687rec.maturityCalcMeth);
	}		
	
	protected void systemError(){
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);		
	}	
}