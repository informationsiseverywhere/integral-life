package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:35
 * Description:
 * Copybook name: TR50ZREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr50zrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr50zRec = new FixedLengthStringData(500);
  	public FixedLengthStringData premsubrs = new FixedLengthStringData(120).isAPartOf(tr50zRec, 0);
  	public FixedLengthStringData[] premsubr = FLSArrayPartOfStructure(15, 8, premsubrs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(premsubrs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData premsubr01 = new FixedLengthStringData(8).isAPartOf(filler, 0);
  	public FixedLengthStringData premsubr02 = new FixedLengthStringData(8).isAPartOf(filler, 8);
  	public FixedLengthStringData premsubr03 = new FixedLengthStringData(8).isAPartOf(filler, 16);
  	public FixedLengthStringData premsubr04 = new FixedLengthStringData(8).isAPartOf(filler, 24);
  	public FixedLengthStringData premsubr05 = new FixedLengthStringData(8).isAPartOf(filler, 32);
  	public FixedLengthStringData premsubr06 = new FixedLengthStringData(8).isAPartOf(filler, 40);
  	public FixedLengthStringData premsubr07 = new FixedLengthStringData(8).isAPartOf(filler, 48);
  	public FixedLengthStringData premsubr08 = new FixedLengthStringData(8).isAPartOf(filler, 56);
  	public FixedLengthStringData premsubr09 = new FixedLengthStringData(8).isAPartOf(filler, 64);
  	public FixedLengthStringData premsubr10 = new FixedLengthStringData(8).isAPartOf(filler, 72);
  	public FixedLengthStringData premsubr11 = new FixedLengthStringData(8).isAPartOf(filler, 80);
  	public FixedLengthStringData premsubr12 = new FixedLengthStringData(8).isAPartOf(filler, 88);
  	public FixedLengthStringData premsubr13 = new FixedLengthStringData(8).isAPartOf(filler, 96);
  	public FixedLengthStringData premsubr14 = new FixedLengthStringData(8).isAPartOf(filler, 104);
  	public FixedLengthStringData premsubr15 = new FixedLengthStringData(8).isAPartOf(filler, 112);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(380).isAPartOf(tr50zRec, 120, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr50zRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr50zRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}