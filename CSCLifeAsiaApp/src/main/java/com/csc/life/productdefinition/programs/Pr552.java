/*
 * File: Pr552.java
 * Date: 30 August 2009 1:40:15
 * Author: Quipoz Limited
 * 
 * Class transformed from PR552.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.life.productdefinition.dataaccess.MliachdTableDAM;
import com.csc.life.productdefinition.dataaccess.MlianamTableDAM;
import com.csc.life.productdefinition.dataaccess.MliasecTableDAM;
import com.csc.life.productdefinition.screens.Sr552ScreenVars;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*            Substandard & Hospitalisation
*            =============================
*
*  This is a scroll screen activited throught option switch from
*  Lives Assured (S5005). This screen lists all the clients with IC
*  Number and Contract Number.
*
*  This scrolling can be sorted by Client Name, IC Number or
*  Contract No. By Keying the desired sorting option in the field
*  'Order By'
*
*  To select the required record, enter '1' at the 'Opt' Field.
*
*****************************************************************
* </pre>
*/
public class Pr552 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR552");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0);
	private ZonedDecimalData iy = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaRestartFlag = new FixedLengthStringData(1);
	private Validator restart = new Validator(wsaaRestartFlag, "Y");
	private FixedLengthStringData wsaaLastClntnaml = new FixedLengthStringData(51);

	private FixedLengthStringData wsaaLastActn = new FixedLengthStringData(1);
	private Validator orderByName = new Validator(wsaaLastActn, "1");
	private Validator orderByIc = new Validator(wsaaLastActn, "2");
	private Validator orderByContract = new Validator(wsaaLastActn, "3");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaWsspFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsbbClntnaml = new FixedLengthStringData(51);
	private FixedLengthStringData wsbbSecurityno = new FixedLengthStringData(15);
	private FixedLengthStringData wsbbMlentity = new FixedLengthStringData(15);
	private String w400 = "W400";
		/* FORMATS */
	private String cltsrec = "CLTSREC";
	private String mliarec = "MLIAREC";
	private String mliasecrec = "MLIASECREC";
	private String mlianamrec = "MLIANAMREC";
	private String mliachdrec = "MLIACHDREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Substd & Hosp view by IC no. and Contrac*/
	private MliaTableDAM mliaIO = new MliaTableDAM();
		/*Substandard & Hospitalisatn by Contr No.*/
	private MliachdTableDAM mliachdIO = new MliachdTableDAM();
		/*Substd & Hospitalisatn by Client Name*/
	private MlianamTableDAM mlianamIO = new MlianamTableDAM();
		/*Substandard & Hospitalisation on IDNO*/
	private MliasecTableDAM mliasecIO = new MliasecTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Optswchrec optswchrec = new Optswchrec();
	private Sr552ScreenVars sv = ScreenProgram.getScreenVars( Sr552ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit2090, 
		callNextSfl2120, 
		nextSfl2180, 
		exit2190, 
		callNextSfl4020, 
		nextSfl4080, 
		exit4090, 
		load5010, 
		exit5090, 
		exit5190, 
		exit5290, 
		exit5390
	}

	public Pr552() {
		super();
		screenVars = sv;
		new ScreenModel("Sr552", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaWsspFlag.set(wsspcomn.flag);
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMS");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.clntnum.set(lifelnbIO.getLifcnum());
		sv.cname.set(namadrsrec.name);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(lifelnbIO.getChdrcoy());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.securityno.set(cltsIO.getSecuityno());
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		wsaaRestartFlag.set("Y");
		sv.actn.set("1");
		wsaaLastActn.set("1");
		sv.clntnaml.set(sv.cname);
		wsaaLastClntnaml.set(sv.cname);
		loadOnePage5000();
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.actn,SPACES)) {
			sv.actnErr.set(w400);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.actn,"1")
		&& isNE(sv.actn,"2")
		&& isNE(sv.actn,"3")) {
			sv.actnErr.set(w400);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.actn,wsaaLastActn)
		|| isNE(sv.clntnaml,wsaaLastClntnaml)) {
			wsaaLastActn.set(sv.actn);
			wsaaLastClntnaml.set(sv.clntnaml);
			wsaaRestartFlag.set("Y");
			loadOnePage5000();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			loadOnePage5000();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		validateSubfile2100();
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					n2110();
				}
				case callNextSfl2120: {
					callNextSfl2120();
				}
				case nextSfl2180: {
					nextSfl2180();
				}
				case exit2190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void n2110()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void callNextSfl2120()
	{
		processScreen("SR552", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(sv.optind,SPACES)) {
			goTo(GotoLabel.nextSfl2180);
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.optind);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.optindErr.set(optswchrec.optsStatuz);
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
			scrnparams.function.set(varcom.supd);
			processScreen("SR552", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.params.set(scrnparams.screenParams);
				fatalError600();
			}
		}
	}

protected void nextSfl2180()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.callNextSfl2120);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		wsspcomn.flag.set("I");
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case callNextSfl4020: {
					callNextSfl4020();
				}
				case nextSfl4080: {
					nextSfl4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.function.set(varcom.srdn);
		}
		else {
			scrnparams.function.set(varcom.sstrt);
		}
	}

protected void callNextSfl4020()
	{
		processScreen("SR552", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
				optswchrec.optsSelType.set(SPACES);
				optswchrec.optsSelOptno.set(ZERO);
				optswchCall4500();
			}
			else {
				wsspcomn.flag.set(wsaaWsspFlag);
				wsspcomn.programPtr.add(1);
			}
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.optind,SPACES)) {
			goTo(GotoLabel.nextSfl4080);
		}
		optswchrec.optsSelOptno.set(sv.optind);
		optswchrec.optsSelType.set("L");
		optswchCall4500();
		mliaIO.setSecurityno(sv.idno);
		mliaIO.setMlentity(sv.mlentity);
		mliaIO.setFormat(mliarec);
		mliaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		mliaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			fatalError600();
		}
		sv.optind.set(SPACES);
		wsspcomn.edterror.set("Y");
		scrnparams.function.set(varcom.supd);
		processScreen("SR552", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		goTo(GotoLabel.exit4090);
	}

protected void nextSfl4080()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.callNextSfl4020);
	}

protected void optswchCall4500()
	{
		call4510();
	}

protected void call4510()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK))
		&& (isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void loadOnePage5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case load5010: {
					load5010();
				}
				case exit5090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void load5010()
	{
		if (restart.isTrue()) {
			wsaaStatuz.set(varcom.oK);
			scrnparams.function.set(varcom.sclr);
			processScreen("SR552", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.subfileRrn.set(1);
			scrnparams.subfileMore.set("Y");
		}
		if (orderByName.isTrue()) {
			if (restart.isTrue()) {
				wsaaRestartFlag.set("N");
				mlianamIO.setClntnaml(sv.clntnaml);
				mlianamIO.setFunction(varcom.begn);
			}
			orderByName5100();
		}
		if (orderByIc.isTrue()) {
			if (restart.isTrue()) {
				wsaaRestartFlag.set("N");
				mliasecIO.setSecurityno(sv.clntnaml);
				mliasecIO.setFunction(varcom.begn);
			}
			orderByIc5200();
		}
		if (orderByContract.isTrue()) {
			if (restart.isTrue()) {
				wsaaRestartFlag.set("N");
				mliachdIO.setMlentity(sv.clntnaml);
				mliachdIO.setFunction(varcom.begn);
			}
			orderByContract5300();
		}
		if (isEQ(wsaaStatuz,varcom.endp)) {
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit5090);
		}
		sv.optindOut[varcom.pr.toInt()].set("N");
		sv.optind.set(SPACES);
		sv.clntlname.set(wsbbClntnaml);
		sv.idno.set(wsbbSecurityno);
		sv.mlentity.set(wsbbMlentity);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR552", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		compute(ix, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		iy.setRemainder(ix);
		if (isEQ(iy,0)) {
			goTo(GotoLabel.exit5090);
		}
		goTo(GotoLabel.load5010);
	}

protected void orderByName5100()
	{
		try {
			n5110();
		}
		catch (GOTOException e){
		}
	}

protected void n5110()
	{
		mlianamIO.setFormat(mlianamrec);
		SmartFileCode.execute(appVars, mlianamIO);
		if (isNE(mlianamIO.getStatuz(),varcom.oK)
		&& isNE(mlianamIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mlianamIO.getStatuz());
			syserrrec.params.set(mlianamIO.getParams());
			fatalError600();
		}
		if (isEQ(mlianamIO.getStatuz(),varcom.endp)) {
			if (isEQ(mlianamIO.getFunction(),varcom.begn)) {
				sflAddDummy6000();
			}
			wsaaStatuz.set(varcom.endp);
			goTo(GotoLabel.exit5190);
		}
		wsbbClntnaml.set(mlianamIO.getClntnaml());
		wsbbSecurityno.set(mlianamIO.getSecurityno());
		wsbbMlentity.set(mlianamIO.getMlentity());
		mlianamIO.setFunction(varcom.nextr);
	}

protected void orderByIc5200()
	{
		try {
			n5210();
		}
		catch (GOTOException e){
		}
	}

protected void n5210()
	{
		mliasecIO.setFormat(mliasecrec);
		SmartFileCode.execute(appVars, mliasecIO);
		if (isNE(mliasecIO.getStatuz(),varcom.oK)
		&& isNE(mliasecIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mliasecIO.getStatuz());
			syserrrec.params.set(mliasecIO.getParams());
			fatalError600();
		}
		if (isEQ(mliasecIO.getStatuz(),varcom.endp)) {
			if (isEQ(mliasecIO.getFunction(),varcom.begn)) {
				sflAddDummy6000();
			}
			wsaaStatuz.set(varcom.endp);
			goTo(GotoLabel.exit5290);
		}
		wsbbClntnaml.set(mliasecIO.getClntnaml());
		wsbbSecurityno.set(mliasecIO.getSecurityno());
		wsbbMlentity.set(mliasecIO.getMlentity());
		mliasecIO.setFunction(varcom.nextr);
	}

protected void orderByContract5300()
	{
		try {
			n5310();
		}
		catch (GOTOException e){
		}
	}

protected void n5310()
	{
		mliachdIO.setFormat(mliachdrec);
		SmartFileCode.execute(appVars, mliachdIO);
		if (isNE(mliachdIO.getStatuz(),varcom.oK)
		&& isNE(mliachdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mliachdIO.getStatuz());
			syserrrec.params.set(mliachdIO.getParams());
			fatalError600();
		}
		if (isEQ(mliachdIO.getStatuz(),varcom.endp)) {
			if (isEQ(mliachdIO.getFunction(),varcom.begn)) {
				sflAddDummy6000();
			}
			wsaaStatuz.set(varcom.endp);
			goTo(GotoLabel.exit5390);
		}
		wsbbClntnaml.set(mliachdIO.getClntnaml());
		wsbbSecurityno.set(mliachdIO.getSecurityno());
		wsbbMlentity.set(mliachdIO.getMlentity());
		mliachdIO.setFunction(varcom.nextr);
	}

protected void sflAddDummy6000()
	{
		
		sv.optindOut[varcom.pr.toInt()].set("Y");
		sv.optind.set(SPACES);
		sv.clntlname.set(SPACES);
		sv.idno.set(SPACES);
		sv.mlentity.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR552", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
