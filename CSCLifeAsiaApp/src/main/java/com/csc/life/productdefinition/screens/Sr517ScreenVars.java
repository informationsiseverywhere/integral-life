package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR517
 * @version 1.0 generated on 30/08/09 07:16
 * @author Quipoz
 */
public class Sr517ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1265);
	public FixedLengthStringData dataFields = new FixedLengthStringData(273).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData contitem = DD.contitem.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData ctables = new FixedLengthStringData(200).isAPartOf(dataFields, 9);
	public FixedLengthStringData[] ctable = FLSArrayPartOfStructure(50, 4, ctables, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(200).isAPartOf(ctables, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctable01 = DD.ctable.copy().isAPartOf(filler,0);
	public FixedLengthStringData ctable02 = DD.ctable.copy().isAPartOf(filler,4);
	public FixedLengthStringData ctable03 = DD.ctable.copy().isAPartOf(filler,8);
	public FixedLengthStringData ctable04 = DD.ctable.copy().isAPartOf(filler,12);
	public FixedLengthStringData ctable05 = DD.ctable.copy().isAPartOf(filler,16);
	public FixedLengthStringData ctable06 = DD.ctable.copy().isAPartOf(filler,20);
	public FixedLengthStringData ctable07 = DD.ctable.copy().isAPartOf(filler,24);
	public FixedLengthStringData ctable08 = DD.ctable.copy().isAPartOf(filler,28);
	public FixedLengthStringData ctable09 = DD.ctable.copy().isAPartOf(filler,32);
	public FixedLengthStringData ctable10 = DD.ctable.copy().isAPartOf(filler,36);
	public FixedLengthStringData ctable11 = DD.ctable.copy().isAPartOf(filler,40);
	public FixedLengthStringData ctable12 = DD.ctable.copy().isAPartOf(filler,44);
	public FixedLengthStringData ctable13 = DD.ctable.copy().isAPartOf(filler,48);
	public FixedLengthStringData ctable14 = DD.ctable.copy().isAPartOf(filler,52);
	public FixedLengthStringData ctable15 = DD.ctable.copy().isAPartOf(filler,56);
	public FixedLengthStringData ctable16 = DD.ctable.copy().isAPartOf(filler,60);
	public FixedLengthStringData ctable17 = DD.ctable.copy().isAPartOf(filler,64);
	public FixedLengthStringData ctable18 = DD.ctable.copy().isAPartOf(filler,68);
	public FixedLengthStringData ctable19 = DD.ctable.copy().isAPartOf(filler,72);
	public FixedLengthStringData ctable20 = DD.ctable.copy().isAPartOf(filler,76);
	public FixedLengthStringData ctable21 = DD.ctable.copy().isAPartOf(filler,80);
	public FixedLengthStringData ctable22 = DD.ctable.copy().isAPartOf(filler,84);
	public FixedLengthStringData ctable23 = DD.ctable.copy().isAPartOf(filler,88);
	public FixedLengthStringData ctable24 = DD.ctable.copy().isAPartOf(filler,92);
	public FixedLengthStringData ctable25 = DD.ctable.copy().isAPartOf(filler,96);
	public FixedLengthStringData ctable26 = DD.ctable.copy().isAPartOf(filler,100);
	public FixedLengthStringData ctable27 = DD.ctable.copy().isAPartOf(filler,104);
	public FixedLengthStringData ctable28 = DD.ctable.copy().isAPartOf(filler,108);
	public FixedLengthStringData ctable29 = DD.ctable.copy().isAPartOf(filler,112);
	public FixedLengthStringData ctable30 = DD.ctable.copy().isAPartOf(filler,116);
	public FixedLengthStringData ctable31 = DD.ctable.copy().isAPartOf(filler,120);
	public FixedLengthStringData ctable32 = DD.ctable.copy().isAPartOf(filler,124);
	public FixedLengthStringData ctable33 = DD.ctable.copy().isAPartOf(filler,128);
	public FixedLengthStringData ctable34 = DD.ctable.copy().isAPartOf(filler,132);
	public FixedLengthStringData ctable35 = DD.ctable.copy().isAPartOf(filler,136);
	public FixedLengthStringData ctable36 = DD.ctable.copy().isAPartOf(filler,140);
	public FixedLengthStringData ctable37 = DD.ctable.copy().isAPartOf(filler,144);
	public FixedLengthStringData ctable38 = DD.ctable.copy().isAPartOf(filler,148);
	public FixedLengthStringData ctable39 = DD.ctable.copy().isAPartOf(filler,152);
	public FixedLengthStringData ctable40 = DD.ctable.copy().isAPartOf(filler,156);
	public FixedLengthStringData ctable41 = DD.ctable.copy().isAPartOf(filler,160);
	public FixedLengthStringData ctable42 = DD.ctable.copy().isAPartOf(filler,164);
	public FixedLengthStringData ctable43 = DD.ctable.copy().isAPartOf(filler,168);
	public FixedLengthStringData ctable44 = DD.ctable.copy().isAPartOf(filler,172);
	public FixedLengthStringData ctable45 = DD.ctable.copy().isAPartOf(filler,176);
	public FixedLengthStringData ctable46 = DD.ctable.copy().isAPartOf(filler,180);
	public FixedLengthStringData ctable47 = DD.ctable.copy().isAPartOf(filler,184);
	public FixedLengthStringData ctable48 = DD.ctable.copy().isAPartOf(filler,188);
	public FixedLengthStringData ctable49 = DD.ctable.copy().isAPartOf(filler,192);
	public FixedLengthStringData ctable50 = DD.ctable.copy().isAPartOf(filler,196);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,209);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,217);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,225);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,233);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,263);
	public FixedLengthStringData zrwvflgs = new FixedLengthStringData(5).isAPartOf(dataFields, 268);
	public FixedLengthStringData[] zrwvflg = FLSArrayPartOfStructure(5, 1, zrwvflgs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(5).isAPartOf(zrwvflgs, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01 = DD.zrwvflg.copy().isAPartOf(filler1,0);
	public FixedLengthStringData zrwvflg02 = DD.zrwvflg.copy().isAPartOf(filler1,1);
	public FixedLengthStringData zrwvflg03 = DD.zrwvflg.copy().isAPartOf(filler1,2);
	public FixedLengthStringData zrwvflg04 = DD.zrwvflg.copy().isAPartOf(filler1,3);
	public FixedLengthStringData zrwvflg05 = DD.zrwvflg.copy().isAPartOf(filler1,4);
	
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(248).isAPartOf(dataArea, 273);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData contitemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctablesErr = new FixedLengthStringData(200).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] ctableErr = FLSArrayPartOfStructure(50, 4, ctablesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(200).isAPartOf(ctablesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctable01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData ctable02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData ctable03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData ctable04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData ctable05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData ctable06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData ctable07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData ctable08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData ctable09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData ctable10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData ctable11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData ctable12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData ctable13Err = new FixedLengthStringData(4).isAPartOf(filler2, 48);
	public FixedLengthStringData ctable14Err = new FixedLengthStringData(4).isAPartOf(filler2, 52);
	public FixedLengthStringData ctable15Err = new FixedLengthStringData(4).isAPartOf(filler2, 56);
	public FixedLengthStringData ctable16Err = new FixedLengthStringData(4).isAPartOf(filler2, 60);
	public FixedLengthStringData ctable17Err = new FixedLengthStringData(4).isAPartOf(filler2, 64);
	public FixedLengthStringData ctable18Err = new FixedLengthStringData(4).isAPartOf(filler2, 68);
	public FixedLengthStringData ctable19Err = new FixedLengthStringData(4).isAPartOf(filler2, 72);
	public FixedLengthStringData ctable20Err = new FixedLengthStringData(4).isAPartOf(filler2, 76);
	public FixedLengthStringData ctable21Err = new FixedLengthStringData(4).isAPartOf(filler2, 80);
	public FixedLengthStringData ctable22Err = new FixedLengthStringData(4).isAPartOf(filler2, 84);
	public FixedLengthStringData ctable23Err = new FixedLengthStringData(4).isAPartOf(filler2, 88);
	public FixedLengthStringData ctable24Err = new FixedLengthStringData(4).isAPartOf(filler2, 92);
	public FixedLengthStringData ctable25Err = new FixedLengthStringData(4).isAPartOf(filler2, 96);
	public FixedLengthStringData ctable26Err = new FixedLengthStringData(4).isAPartOf(filler2, 100);
	public FixedLengthStringData ctable27Err = new FixedLengthStringData(4).isAPartOf(filler2, 104);
	public FixedLengthStringData ctable28Err = new FixedLengthStringData(4).isAPartOf(filler2, 108);
	public FixedLengthStringData ctable29Err = new FixedLengthStringData(4).isAPartOf(filler2, 112);
	public FixedLengthStringData ctable30Err = new FixedLengthStringData(4).isAPartOf(filler2, 116);
	public FixedLengthStringData ctable31Err = new FixedLengthStringData(4).isAPartOf(filler2, 120);
	public FixedLengthStringData ctable32Err = new FixedLengthStringData(4).isAPartOf(filler2, 124);
	public FixedLengthStringData ctable33Err = new FixedLengthStringData(4).isAPartOf(filler2, 128);
	public FixedLengthStringData ctable34Err = new FixedLengthStringData(4).isAPartOf(filler2, 132);
	public FixedLengthStringData ctable35Err = new FixedLengthStringData(4).isAPartOf(filler2, 136);
	public FixedLengthStringData ctable36Err = new FixedLengthStringData(4).isAPartOf(filler2, 140);
	public FixedLengthStringData ctable37Err = new FixedLengthStringData(4).isAPartOf(filler2, 144);
	public FixedLengthStringData ctable38Err = new FixedLengthStringData(4).isAPartOf(filler2, 148);
	public FixedLengthStringData ctable39Err = new FixedLengthStringData(4).isAPartOf(filler2, 152);
	public FixedLengthStringData ctable40Err = new FixedLengthStringData(4).isAPartOf(filler2, 156);
	public FixedLengthStringData ctable41Err = new FixedLengthStringData(4).isAPartOf(filler2, 160);
	public FixedLengthStringData ctable42Err = new FixedLengthStringData(4).isAPartOf(filler2, 164);
	public FixedLengthStringData ctable43Err = new FixedLengthStringData(4).isAPartOf(filler2, 168);
	public FixedLengthStringData ctable44Err = new FixedLengthStringData(4).isAPartOf(filler2, 172);
	public FixedLengthStringData ctable45Err = new FixedLengthStringData(4).isAPartOf(filler2, 176);
	public FixedLengthStringData ctable46Err = new FixedLengthStringData(4).isAPartOf(filler2, 180);
	public FixedLengthStringData ctable47Err = new FixedLengthStringData(4).isAPartOf(filler2, 184);
	public FixedLengthStringData ctable48Err = new FixedLengthStringData(4).isAPartOf(filler2, 188);
	public FixedLengthStringData ctable49Err = new FixedLengthStringData(4).isAPartOf(filler2, 192);
	public FixedLengthStringData ctable50Err = new FixedLengthStringData(4).isAPartOf(filler2, 196);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData zrwvflgsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData[] zrwvflgErr = FLSArrayPartOfStructure(5, 4, zrwvflgsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(zrwvflgsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData zrwvflg02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData zrwvflg03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData zrwvflg04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData zrwvflg05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(744).isAPartOf(dataArea, 521);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] contitemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData ctablesOut = new FixedLengthStringData(600).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] ctableOut = FLSArrayPartOfStructure(50, 12, ctablesOut, 0);
	public FixedLengthStringData[][] ctableO = FLSDArrayPartOfArrayStructure(12, 1, ctableOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(600).isAPartOf(ctablesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ctable01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] ctable02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] ctable03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] ctable04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] ctable05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] ctable06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] ctable07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] ctable08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] ctable09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] ctable10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] ctable11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] ctable12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] ctable13Out = FLSArrayPartOfStructure(12, 1, filler4, 144);
	public FixedLengthStringData[] ctable14Out = FLSArrayPartOfStructure(12, 1, filler4, 156);
	public FixedLengthStringData[] ctable15Out = FLSArrayPartOfStructure(12, 1, filler4, 168);
	public FixedLengthStringData[] ctable16Out = FLSArrayPartOfStructure(12, 1, filler4, 180);
	public FixedLengthStringData[] ctable17Out = FLSArrayPartOfStructure(12, 1, filler4, 192);
	public FixedLengthStringData[] ctable18Out = FLSArrayPartOfStructure(12, 1, filler4, 204);
	public FixedLengthStringData[] ctable19Out = FLSArrayPartOfStructure(12, 1, filler4, 216);
	public FixedLengthStringData[] ctable20Out = FLSArrayPartOfStructure(12, 1, filler4, 228);
	public FixedLengthStringData[] ctable21Out = FLSArrayPartOfStructure(12, 1, filler4, 240);
	public FixedLengthStringData[] ctable22Out = FLSArrayPartOfStructure(12, 1, filler4, 252);
	public FixedLengthStringData[] ctable23Out = FLSArrayPartOfStructure(12, 1, filler4, 264);
	public FixedLengthStringData[] ctable24Out = FLSArrayPartOfStructure(12, 1, filler4, 276);
	public FixedLengthStringData[] ctable25Out = FLSArrayPartOfStructure(12, 1, filler4, 288);
	public FixedLengthStringData[] ctable26Out = FLSArrayPartOfStructure(12, 1, filler4, 300);
	public FixedLengthStringData[] ctable27Out = FLSArrayPartOfStructure(12, 1, filler4, 312);
	public FixedLengthStringData[] ctable28Out = FLSArrayPartOfStructure(12, 1, filler4, 324);
	public FixedLengthStringData[] ctable29Out = FLSArrayPartOfStructure(12, 1, filler4, 336);
	public FixedLengthStringData[] ctable30Out = FLSArrayPartOfStructure(12, 1, filler4, 348);
	public FixedLengthStringData[] ctable31Out = FLSArrayPartOfStructure(12, 1, filler4, 360);
	public FixedLengthStringData[] ctable32Out = FLSArrayPartOfStructure(12, 1, filler4, 372);
	public FixedLengthStringData[] ctable33Out = FLSArrayPartOfStructure(12, 1, filler4, 384);
	public FixedLengthStringData[] ctable34Out = FLSArrayPartOfStructure(12, 1, filler4, 396);
	public FixedLengthStringData[] ctable35Out = FLSArrayPartOfStructure(12, 1, filler4, 408);
	public FixedLengthStringData[] ctable36Out = FLSArrayPartOfStructure(12, 1, filler4, 420);
	public FixedLengthStringData[] ctable37Out = FLSArrayPartOfStructure(12, 1, filler4, 432);
	public FixedLengthStringData[] ctable38Out = FLSArrayPartOfStructure(12, 1, filler4, 444);
	public FixedLengthStringData[] ctable39Out = FLSArrayPartOfStructure(12, 1, filler4, 456);
	public FixedLengthStringData[] ctable40Out = FLSArrayPartOfStructure(12, 1, filler4, 468);
	public FixedLengthStringData[] ctable41Out = FLSArrayPartOfStructure(12, 1, filler4, 480);
	public FixedLengthStringData[] ctable42Out = FLSArrayPartOfStructure(12, 1, filler4, 492);
	public FixedLengthStringData[] ctable43Out = FLSArrayPartOfStructure(12, 1, filler4, 504);
	public FixedLengthStringData[] ctable44Out = FLSArrayPartOfStructure(12, 1, filler4, 516);
	public FixedLengthStringData[] ctable45Out = FLSArrayPartOfStructure(12, 1, filler4, 528);
	public FixedLengthStringData[] ctable46Out = FLSArrayPartOfStructure(12, 1, filler4, 540);
	public FixedLengthStringData[] ctable47Out = FLSArrayPartOfStructure(12, 1, filler4, 552);
	public FixedLengthStringData[] ctable48Out = FLSArrayPartOfStructure(12, 1, filler4, 564);
	public FixedLengthStringData[] ctable49Out = FLSArrayPartOfStructure(12, 1, filler4, 576);
	public FixedLengthStringData[] ctable50Out = FLSArrayPartOfStructure(12, 1, filler4, 588);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData zrwvflgsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 684);
	public FixedLengthStringData[] zrwvflgOut = FLSArrayPartOfStructure(5, 12, zrwvflgsOut, 0);
	public FixedLengthStringData[][] zrwvflgO = FLSDArrayPartOfArrayStructure(12, 1, zrwvflgOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(60).isAPartOf(zrwvflgsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrwvflg01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] zrwvflg02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] zrwvflg03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] zrwvflg04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] zrwvflg05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr517screenWritten = new LongData(0);
	public LongData Sr517protectWritten = new LongData(0);
	public FixedLengthStringData isEnquiryMode = new FixedLengthStringData(1);  //ILIFE-7594

	public boolean hasSubfile() {
		return false;
	}


	public Sr517ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zrwvflg01Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrwvflg02Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable01Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable02Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable03Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable04Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable05Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable06Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable07Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable08Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable09Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable10Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable11Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable12Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable13Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable14Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable15Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable16Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable17Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable18Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable19Out,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable20Out,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable21Out,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable22Out,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable23Out,new String[] {"48",null, "-48",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable24Out,new String[] {"49",null, "-49",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable25Out,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable26Out,new String[] {"51",null, "-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable27Out,new String[] {"52",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable28Out,new String[] {"53",null, "-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable29Out,new String[] {"54",null, "-54",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable30Out,new String[] {"55",null, "-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable31Out,new String[] {"56",null, "-56",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable32Out,new String[] {"57",null, "-57",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable33Out,new String[] {"58",null, "-58",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable34Out,new String[] {"59",null, "-59",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable35Out,new String[] {"60",null, "-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable36Out,new String[] {"61",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable37Out,new String[] {"62",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable38Out,new String[] {"63",null, "-63",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable39Out,new String[] {"64",null, "-64",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable40Out,new String[] {"65",null, "-65",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable41Out,new String[] {"66",null, "-66",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable42Out,new String[] {"67",null, "-67",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable43Out,new String[] {"68",null, "-68",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable44Out,new String[] {"69",null, "-69",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable45Out,new String[] {"70",null, "-70",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable46Out,new String[] {"71",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable47Out,new String[] {"72",null, "-72",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable48Out,new String[] {"73",null, "-73",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable49Out,new String[] {"74",null, "-74",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable50Out,new String[] {"75",null, "-75",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrwvflg03Out,new String[] {"76",null, "-76",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrwvflg04Out,new String[] {"77",null, "-77",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrwvflg05Out,new String[] {"78",null, "-78","79", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, zrwvflg01, zrwvflg02, ctable01, ctable02, ctable03, ctable04, ctable05, ctable06, ctable07, ctable08, ctable09, ctable10, ctable11, ctable12, ctable13, ctable14, ctable15, ctable16, ctable17, ctable18, ctable19, ctable20, ctable21, ctable22, ctable23, ctable24, ctable25, ctable26, ctable27, ctable28, ctable29, ctable30, ctable31, ctable32, ctable33, ctable34, ctable35, ctable36, ctable37, ctable38, ctable39, ctable40, ctable41, ctable42, ctable43, ctable44, ctable45, ctable46, ctable47, ctable48, ctable49, ctable50, zrwvflg03, zrwvflg04, zrwvflg05, contitem};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, zrwvflg01Out, zrwvflg02Out, ctable01Out, ctable02Out, ctable03Out, ctable04Out, ctable05Out, ctable06Out, ctable07Out, ctable08Out, ctable09Out, ctable10Out, ctable11Out, ctable12Out, ctable13Out, ctable14Out, ctable15Out, ctable16Out, ctable17Out, ctable18Out, ctable19Out, ctable20Out, ctable21Out, ctable22Out, ctable23Out, ctable24Out, ctable25Out, ctable26Out, ctable27Out, ctable28Out, ctable29Out, ctable30Out, ctable31Out, ctable32Out, ctable33Out, ctable34Out, ctable35Out, ctable36Out, ctable37Out, ctable38Out, ctable39Out, ctable40Out, ctable41Out, ctable42Out, ctable43Out, ctable44Out, ctable45Out, ctable46Out, ctable47Out, ctable48Out, ctable49Out, ctable50Out, zrwvflg03Out, zrwvflg04Out, zrwvflg05Out, contitemOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, zrwvflg01Err, zrwvflg02Err, ctable01Err, ctable02Err, ctable03Err, ctable04Err, ctable05Err, ctable06Err, ctable07Err, ctable08Err, ctable09Err, ctable10Err, ctable11Err, ctable12Err, ctable13Err, ctable14Err, ctable15Err, ctable16Err, ctable17Err, ctable18Err, ctable19Err, ctable20Err, ctable21Err, ctable22Err, ctable23Err, ctable24Err, ctable25Err, ctable26Err, ctable27Err, ctable28Err, ctable29Err, ctable30Err, ctable31Err, ctable32Err, ctable33Err, ctable34Err, ctable35Err, ctable36Err, ctable37Err, ctable38Err, ctable39Err, ctable40Err, ctable41Err, ctable42Err, ctable43Err, ctable44Err, ctable45Err, ctable46Err, ctable47Err, ctable48Err, ctable49Err, ctable50Err, zrwvflg03Err, zrwvflg04Err, zrwvflg05Err, contitemErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr517screen.class;
		protectRecord = Sr517protect.class;
	}

}
