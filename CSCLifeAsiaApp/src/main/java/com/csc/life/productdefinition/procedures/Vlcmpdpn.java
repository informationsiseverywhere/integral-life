/*
 * File: Vlcmpdpn.java
 * Date: 30 August 2009 2:53:37
 * Author: Quipoz Limited
 * 
 * Class transformed from VLCMPDPN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51hrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    The subroutine VLCMPDPN.
*    ========================
*
*   This subroutine will validate certain components are mandatory i.e.
*   component must co-exist while certain components cannot be attached a
*   specified in TR51H.
*
*     - Read table TR51H, using VLSB-CNTTYPE + VLSB-RID1-CRTABLE as keys,
*     if not found then read again by use '***' + VLSB-RID1-CRTABLE
*
*     - Validate according to TR51H
*
*
***********************************************************************
* </pre>
*/
public class Vlcmpdpn extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "VLCMPDPN";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaBuySeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaVlsbSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr51hSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaErrDetail = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaInputAllSpace = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaInputCrtable = new FixedLengthStringData(4);
	private final int wsaaMaxCrtable = 20;
	private final int wsaaMaxInput = 50;

	private FixedLengthStringData wsaaMustBuy = new FixedLengthStringData(1);
	private Validator wsaaMustBuyFound = new Validator(wsaaMustBuy, "Y");

		/* WSAA-BUY-PRODS */
	private FixedLengthStringData[] wsaaBuyRec = FLSInittedArray (20, 5);
	private FixedLengthStringData[] wsaaBuyProd = FLSDArrayPartOfArrayStructure(4, wsaaBuyRec, 0);
	private FixedLengthStringData[] wsaaBuyFlag = FLSDArrayPartOfArrayStructure(1, wsaaBuyRec, 4);

	private FixedLengthStringData wsaaTr51hItmkey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr51hCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr51hItmkey, 0).init(SPACES);
	private FixedLengthStringData wsaaTr51hComponent = new FixedLengthStringData(4).isAPartOf(wsaaTr51hItmkey, 3).init(SPACES);

		/* WSAA-TR51H-ARRAY-1 */
	private FixedLengthStringData[] wsaaTr51hRec1 = FLSInittedArray (20, 4);
	private FixedLengthStringData[] wsaaTr51hKey1 = FLSDArrayPartOfArrayStructure(4, wsaaTr51hRec1, 0);
	private FixedLengthStringData[] wsaaTr51hCrtable = FLSDArrayPartOfArrayStructure(4, wsaaTr51hKey1, 0, HIVALUE);

	private FixedLengthStringData[] wsaaTr51hRec2 = FLSInittedArray (20, 4);
	private FixedLengthStringData[] wsaaTr51hKey2 = FLSDArrayPartOfArrayStructure(4, wsaaTr51hRec2, 0);
	private FixedLengthStringData[] wsaaTr51hCtable = FLSDArrayPartOfArrayStructure(4, wsaaTr51hKey2, 0, HIVALUE);
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String tr51h = "TR51H";
		/* ERRORS */
	private static final String rlbb = "RLBB";
	private static final String rlbc = "RLBC";
	private IntegerData wsaaTr51hIx = new IntegerData();
	private IntegerData wsaaTr51hIz = new IntegerData();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Tr51hrec tr51hrec = new Tr51hrec();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		loadTr51h180, 
		exit190, 
		searchNotBuy340, 
		exit390
	}

	public Vlcmpdpn() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main000()
	{
		/*BEGIN*/
		vlpdsubrec.statuz.set(varcom.oK);
		if (isEQ(vlpdsubrec.rid1Life, SPACES)
		&& isEQ(vlpdsubrec.rid1Jlife, SPACES)
		&& isEQ(vlpdsubrec.rid1Coverage, SPACES)
		&& isEQ(vlpdsubrec.rid1Rider, SPACES)
		&& isEQ(vlpdsubrec.rid1Crtable, SPACES)) {
			return ;
		}
		readTables100();
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		wsaaErrSeq.set(1);
		validation200();
		/*EXIT*/
		exitProgram();
	}

protected void readTables100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin110();
					xxComponent140();
				case loadTr51h180: 
					loadTr51h180();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin110()
	{
		/*  Load table TR51H.*/
		/*  CONTRACT-TYPE + COMPONENT*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51h);
		wsaaTr51hCnttype.set(vlpdsubrec.cnttype);
		wsaaTr51hComponent.set(vlpdsubrec.rid1Crtable);
		itemIO.setItemitem(wsaaTr51hItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.loadTr51h180);
		}
	}

	/**
	* <pre>
	*  XXX + COMPONENT
	* </pre>
	*/
protected void xxComponent140()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51h);
		wsaaTr51hCnttype.set("***");
		wsaaTr51hComponent.set(vlpdsubrec.rid1Crtable);
		itemIO.setItemitem(wsaaTr51hItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
	}

protected void loadTr51h180()
	{
		tr51hrec.tr51hRec.set(itemIO.getGenarea());
		for (wsaaTr51hSeq.set(1); !(isGT(wsaaTr51hSeq, wsaaMaxCrtable)); wsaaTr51hSeq.add(1)){
			wsaaTr51hCrtable[wsaaTr51hSeq.toInt()].set(tr51hrec.crtable[wsaaTr51hSeq.toInt()]);
			wsaaTr51hCtable[wsaaTr51hSeq.toInt()].set(tr51hrec.ctable[wsaaTr51hSeq.toInt()]);
			wsaaBuyProd[wsaaTr51hSeq.toInt()].set(tr51hrec.crtable[wsaaTr51hSeq.toInt()]);
			wsaaBuyFlag[wsaaTr51hSeq.toInt()].set("N");
		}
	}

protected void validation200()
	{
		/*BEGIN*/
		/* Validation Component must buy and must not buy.*/
		wsaaMustBuy.set(SPACES);
		wsaaInputAllSpace.set(SPACES);
		for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq, wsaaMaxInput)
		|| isEQ(wsaaInputAllSpace, "Y")); wsaaVlsbSeq.add(1)){
			chkComponent300();
		}
		/* Validation Component must buy*/
		for (wsaaBuySeq.set(1); !(isGT(wsaaBuySeq, wsaaMaxCrtable)); wsaaBuySeq.add(1)){
			if (isEQ(wsaaBuyFlag[wsaaBuySeq.toInt()], "N")
			&& isNE(wsaaBuyProd[wsaaBuySeq.toInt()], SPACES)) {
				wsaaErrCode.set(rlbc);
				wsaaErrDetail.set(wsaaBuyProd[wsaaBuySeq.toInt()]);
				moveError400();
			}
		}
		/*EXIT*/
	}

protected void chkComponent300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin310();
					searchMustBuy320();
				case searchNotBuy340: 
					searchNotBuy340();
				case exit390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin310()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()], SPACES)) {
			wsaaInputAllSpace.set("Y");
			goTo(GotoLabel.exit390);
		}
		wsaaInputCrtable.set(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()]);
	}

	/**
	* <pre>
	* Search Component must buy
	* </pre>
	*/
protected void searchMustBuy320()
	{
		wsaaTr51hIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaTr51hIx, wsaaTr51hRec1.length); wsaaTr51hIx.add(1)){
				if (isEQ(wsaaTr51hKey1[wsaaTr51hIx.toInt()], wsaaInputCrtable)) {
					break searchlabel1;
				}
			}
			goTo(GotoLabel.searchNotBuy340);
		}
		/*  MOVE WSAA-TR51H-IX          TO WSAA-BUY-SEQ.                 */
		wsaaBuySeq.set(wsaaTr51hIx);
		wsaaBuyFlag[wsaaBuySeq.toInt()].set("Y");
	}

	/**
	* <pre>
	* Validation Component must not buy.
	* </pre>
	*/
protected void searchNotBuy340()
	{
		wsaaTr51hIz.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaTr51hIz, wsaaTr51hRec2.length); wsaaTr51hIz.add(1)){
				if (isEQ(wsaaTr51hKey2[wsaaTr51hIz.toInt()], wsaaInputCrtable)) {
					break searchlabel1;
				}
			}
			return ;
		}
		wsaaErrCode.set(rlbb);
		wsaaErrDetail.set(wsaaInputCrtable);
		moveError400();
	}

protected void moveError400()
	{
		/*BEGIN*/
		vlpdsubrec.errCode[wsaaErrSeq.toInt()].set(wsaaErrCode);
		vlpdsubrec.errDet[wsaaErrSeq.toInt()].set(wsaaErrDetail);
		wsaaErrSeq.add(1);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL-ERRORS*/
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
