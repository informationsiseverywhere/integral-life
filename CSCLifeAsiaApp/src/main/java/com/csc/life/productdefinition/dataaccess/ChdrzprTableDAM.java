package com.csc.life.productdefinition.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrzprTableDAM.java
 * Date: Sun, 30 Aug 2009 03:32:56
 * Class transformed from CHDRZPR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrzprTableDAM extends ChdrpfTableDAM {

	public ChdrzprTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRZPR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "RECODE, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "VALIDFLAG, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "PROCTRANCD, " +
		            "PROCFLAG, " +
		            "PROCID, " +
		            "STATCODE, " +
		            "STATREASN, " +
		            "STATDATE, " +
		            "STATTRAN, " +
		            "TRANLUSED, " +
		            "OCCDATE, " +
		            "CCDATE, " +
		            "CRDATE, " +
		            "ANNAMT01, " +
		            "ANNAMT02, " +
		            "ANNAMT03, " +
		            "ANNAMT04, " +
		            "ANNAMT05, " +
		            "ANNAMT06, " +
		            "RNLTYPE, " +
		            "RNLNOTS, " +
		            "CNTCURR, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               jobName,
                               userProfile,
                               datime,
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               recode,
                               servunit,
                               cnttype,
                               tranno,
                               tranid,
                               validflag,
                               currfrom,
                               currto,
                               proctrancd,
                               procflag,
                               procid,
                               statcode,
                               statreasn,
                               statdate,
                               stattran,
                               tranlused,
                               occdate,
                               ccdate,
                               crdate,
                               annamt01,
                               annamt02,
                               annamt03,
                               annamt04,
                               annamt05,
                               annamt06,
                               rnltype,
                               rnlnots,
                               cntcurr,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller50.setInternal(chdrcoy.toInternal());
	nonKeyFiller60.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(203);
		
		nonKeyData.set(
					getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ getChdrpfx().toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getRecode().toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getProctrancd().toInternal()
					+ getProcflag().toInternal()
					+ getProcid().toInternal()
					+ getStatcode().toInternal()
					+ getStatreasn().toInternal()
					+ getStatdate().toInternal()
					+ getStattran().toInternal()
					+ getTranlused().toInternal()
					+ getOccdate().toInternal()
					+ getCcdate().toInternal()
					+ getCrdate().toInternal()
					+ getAnnamt01().toInternal()
					+ getAnnamt02().toInternal()
					+ getAnnamt03().toInternal()
					+ getAnnamt04().toInternal()
					+ getAnnamt05().toInternal()
					+ getAnnamt06().toInternal()
					+ getRnltype().toInternal()
					+ getRnlnots().toInternal()
					+ getCntcurr().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, recode);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, proctrancd);
			what = ExternalData.chop(what, procflag);
			what = ExternalData.chop(what, procid);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, statreasn);
			what = ExternalData.chop(what, statdate);
			what = ExternalData.chop(what, stattran);
			what = ExternalData.chop(what, tranlused);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, crdate);
			what = ExternalData.chop(what, annamt01);
			what = ExternalData.chop(what, annamt02);
			what = ExternalData.chop(what, annamt03);
			what = ExternalData.chop(what, annamt04);
			what = ExternalData.chop(what, annamt05);
			what = ExternalData.chop(what, annamt06);
			what = ExternalData.chop(what, rnltype);
			what = ExternalData.chop(what, rnlnots);
			what = ExternalData.chop(what, cntcurr);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getRecode() {
		return recode;
	}
	public void setRecode(Object what) {
		recode.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getProctrancd() {
		return proctrancd;
	}
	public void setProctrancd(Object what) {
		proctrancd.set(what);
	}	
	public FixedLengthStringData getProcflag() {
		return procflag;
	}
	public void setProcflag(Object what) {
		procflag.set(what);
	}	
	public FixedLengthStringData getProcid() {
		return procid;
	}
	public void setProcid(Object what) {
		procid.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getStatreasn() {
		return statreasn;
	}
	public void setStatreasn(Object what) {
		statreasn.set(what);
	}	
	public PackedDecimalData getStatdate() {
		return statdate;
	}
	public void setStatdate(Object what) {
		setStatdate(what, false);
	}
	public void setStatdate(Object what, boolean rounded) {
		if (rounded)
			statdate.setRounded(what);
		else
			statdate.set(what);
	}	
	public PackedDecimalData getStattran() {
		return stattran;
	}
	public void setStattran(Object what) {
		setStattran(what, false);
	}
	public void setStattran(Object what, boolean rounded) {
		if (rounded)
			stattran.setRounded(what);
		else
			stattran.set(what);
	}	
	public PackedDecimalData getTranlused() {
		return tranlused;
	}
	public void setTranlused(Object what) {
		setTranlused(what, false);
	}
	public void setTranlused(Object what, boolean rounded) {
		if (rounded)
			tranlused.setRounded(what);
		else
			tranlused.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public PackedDecimalData getCrdate() {
		return crdate;
	}
	public void setCrdate(Object what) {
		setCrdate(what, false);
	}
	public void setCrdate(Object what, boolean rounded) {
		if (rounded)
			crdate.setRounded(what);
		else
			crdate.set(what);
	}	
	public PackedDecimalData getAnnamt01() {
		return annamt01;
	}
	public void setAnnamt01(Object what) {
		setAnnamt01(what, false);
	}
	public void setAnnamt01(Object what, boolean rounded) {
		if (rounded)
			annamt01.setRounded(what);
		else
			annamt01.set(what);
	}	
	public PackedDecimalData getAnnamt02() {
		return annamt02;
	}
	public void setAnnamt02(Object what) {
		setAnnamt02(what, false);
	}
	public void setAnnamt02(Object what, boolean rounded) {
		if (rounded)
			annamt02.setRounded(what);
		else
			annamt02.set(what);
	}	
	public PackedDecimalData getAnnamt03() {
		return annamt03;
	}
	public void setAnnamt03(Object what) {
		setAnnamt03(what, false);
	}
	public void setAnnamt03(Object what, boolean rounded) {
		if (rounded)
			annamt03.setRounded(what);
		else
			annamt03.set(what);
	}	
	public PackedDecimalData getAnnamt04() {
		return annamt04;
	}
	public void setAnnamt04(Object what) {
		setAnnamt04(what, false);
	}
	public void setAnnamt04(Object what, boolean rounded) {
		if (rounded)
			annamt04.setRounded(what);
		else
			annamt04.set(what);
	}	
	public PackedDecimalData getAnnamt05() {
		return annamt05;
	}
	public void setAnnamt05(Object what) {
		setAnnamt05(what, false);
	}
	public void setAnnamt05(Object what, boolean rounded) {
		if (rounded)
			annamt05.setRounded(what);
		else
			annamt05.set(what);
	}	
	public PackedDecimalData getAnnamt06() {
		return annamt06;
	}
	public void setAnnamt06(Object what) {
		setAnnamt06(what, false);
	}
	public void setAnnamt06(Object what, boolean rounded) {
		if (rounded)
			annamt06.setRounded(what);
		else
			annamt06.set(what);
	}	
	public FixedLengthStringData getRnltype() {
		return rnltype;
	}
	public void setRnltype(Object what) {
		rnltype.set(what);
	}	
	public FixedLengthStringData getRnlnots() {
		return rnlnots;
	}
	public void setRnlnots(Object what) {
		rnlnots.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getAnnamts() {
		return new FixedLengthStringData(annamt01.toInternal()
										+ annamt02.toInternal()
										+ annamt03.toInternal()
										+ annamt04.toInternal()
										+ annamt05.toInternal()
										+ annamt06.toInternal());
	}
	public void setAnnamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnnamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, annamt01);
		what = ExternalData.chop(what, annamt02);
		what = ExternalData.chop(what, annamt03);
		what = ExternalData.chop(what, annamt04);
		what = ExternalData.chop(what, annamt05);
		what = ExternalData.chop(what, annamt06);
	}
	public PackedDecimalData getAnnamt(BaseData indx) {
		return getAnnamt(indx.toInt());
	}
	public PackedDecimalData getAnnamt(int indx) {

		switch (indx) {
			case 1 : return annamt01;
			case 2 : return annamt02;
			case 3 : return annamt03;
			case 4 : return annamt04;
			case 5 : return annamt05;
			case 6 : return annamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnnamt(BaseData indx, Object what) {
		setAnnamt(indx, what, false);
	}
	public void setAnnamt(BaseData indx, Object what, boolean rounded) {
		setAnnamt(indx.toInt(), what, rounded);
	}
	public void setAnnamt(int indx, Object what) {
		setAnnamt(indx, what, false);
	}
	public void setAnnamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnnamt01(what, rounded);
					 break;
			case 2 : setAnnamt02(what, rounded);
					 break;
			case 3 : setAnnamt03(what, rounded);
					 break;
			case 4 : setAnnamt04(what, rounded);
					 break;
			case 5 : setAnnamt05(what, rounded);
					 break;
			case 6 : setAnnamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		jobName.clear();
		userProfile.clear();
		datime.clear();
		chdrpfx.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		recode.clear();
		servunit.clear();
		cnttype.clear();
		tranno.clear();
		tranid.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		proctrancd.clear();
		procflag.clear();
		procid.clear();
		statcode.clear();
		statreasn.clear();
		statdate.clear();
		stattran.clear();
		tranlused.clear();
		occdate.clear();
		ccdate.clear();
		crdate.clear();
		annamt01.clear();
		annamt02.clear();
		annamt03.clear();
		annamt04.clear();
		annamt05.clear();
		annamt06.clear();
		rnltype.clear();
		rnlnots.clear();
		cntcurr.clear();		
	}


}