package com.csc.life.productdefinition.procedures;

import java.util.HashMap;

import com.csc.life.terminationclaims.recordstructures.Vpxdtavrec;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.util.QPUtilities;

public class CalcDeath extends Vpmcalc {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private Vpxdtavrec vpxdtavrec = new Vpxdtavrec();

	public CalcDeath() {
		super();
	}

	@Override
	public void mainline(Object... parmArray)
	{
		recordMap = new HashMap<String, ExternalData>();
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		
		recordMap.put("VPMFMTREC", vpmfmtrec);
		recordMap.put("VPXDTAVREC", vpxdtavrec);

		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

}
