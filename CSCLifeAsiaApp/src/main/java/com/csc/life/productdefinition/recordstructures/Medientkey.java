package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:42
 * Description:
 * Copybook name: MEDIENTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Medientkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData medientFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData medientKey = new FixedLengthStringData(256).isAPartOf(medientFileKey, 0, REDEFINE);
  	public FixedLengthStringData medientChdrcoy = new FixedLengthStringData(1).isAPartOf(medientKey, 0);
  	public FixedLengthStringData medientPaidby = new FixedLengthStringData(1).isAPartOf(medientKey, 1);
  	public FixedLengthStringData medientExmcode = new FixedLengthStringData(10).isAPartOf(medientKey, 2);
  	public FixedLengthStringData medientChdrnum = new FixedLengthStringData(8).isAPartOf(medientKey, 12);
  	public PackedDecimalData medientSeqno = new PackedDecimalData(2, 0).isAPartOf(medientKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(234).isAPartOf(medientKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(medientFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		medientFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}