package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6654
 * @version 1.0 generated on 30/08/09 06:56
 * @author Quipoz
 */
public class S6654ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData arrearsMethod = DD.arrsmeth.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData collectsub = DD.collectsub.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData daexpys = new FixedLengthStringData(9).isAPartOf(dataFields, 13);
	public ZonedDecimalData[] daexpy = ZDArrayPartOfStructure(3, 3, 0, daexpys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(9).isAPartOf(daexpys, 0, FILLER_REDEFINE);
	public ZonedDecimalData daexpy01 = DD.daexpy.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData daexpy02 = DD.daexpy.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData daexpy03 = DD.daexpy.copyToZonedDecimal().isAPartOf(filler,6);
	public FixedLengthStringData doctids = new FixedLengthStringData(32).isAPartOf(dataFields, 22);
	public FixedLengthStringData[] doctid = FLSArrayPartOfStructure(4, 8, doctids, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(doctids, 0, FILLER_REDEFINE);
	public FixedLengthStringData doctid01 = DD.doctid.copy().isAPartOf(filler1,0);
	public FixedLengthStringData doctid02 = DD.doctid.copy().isAPartOf(filler1,8);
	public FixedLengthStringData doctid03 = DD.doctid.copy().isAPartOf(filler1,16);
	public FixedLengthStringData doctid04 = DD.doctid.copy().isAPartOf(filler1,24);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,54);
	public ZonedDecimalData leadDays = DD.leadys.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,65);
	public ZonedDecimalData nonForfietureDays = DD.nftdys.copyToZonedDecimal().isAPartOf(dataFields,95);
	public FixedLengthStringData renwdates = new FixedLengthStringData(2).isAPartOf(dataFields, 98);
	public FixedLengthStringData[] renwdate = FLSArrayPartOfStructure(2, 1, renwdates, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(renwdates, 0, FILLER_REDEFINE);
	public FixedLengthStringData renwdate1 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	public FixedLengthStringData renwdate2 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData zrfreqs = new FixedLengthStringData(12).isAPartOf(dataFields, 105);
	public FixedLengthStringData[] zrfreq = FLSArrayPartOfStructure(6, 2, zrfreqs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(zrfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrfreq01 = DD.zrfreq.copy().isAPartOf(filler3,0);
	public FixedLengthStringData zrfreq02 = DD.zrfreq.copy().isAPartOf(filler3,2);
	public FixedLengthStringData zrfreq03 = DD.zrfreq.copy().isAPartOf(filler3,4);
	public FixedLengthStringData zrfreq04 = DD.zrfreq.copy().isAPartOf(filler3,6);
	public FixedLengthStringData zrfreq05 = DD.zrfreq.copy().isAPartOf(filler3,8);
	public FixedLengthStringData zrfreq06 = DD.zrfreq.copy().isAPartOf(filler3,10);
	public FixedLengthStringData zrincrs = new FixedLengthStringData(12).isAPartOf(dataFields, 117);
	public ZonedDecimalData[] zrincr = ZDArrayPartOfStructure(6, 2, 0, zrincrs, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(12).isAPartOf(zrincrs, 0, FILLER_REDEFINE);
	public ZonedDecimalData zrincr01 = DD.zrincr.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData zrincr02 = DD.zrincr.copyToZonedDecimal().isAPartOf(filler4,2);
	public ZonedDecimalData zrincr03 = DD.zrincr.copyToZonedDecimal().isAPartOf(filler4,4);
	public ZonedDecimalData zrincr04 = DD.zrincr.copyToZonedDecimal().isAPartOf(filler4,6);
	public ZonedDecimalData zrincr05 = DD.zrincr.copyToZonedDecimal().isAPartOf(filler4,8);
	public ZonedDecimalData zrincr06 = DD.zrincr.copyToZonedDecimal().isAPartOf(filler4,10);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData arrsmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData collectsubErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData daexpysErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] daexpyErr = FLSArrayPartOfStructure(3, 4, daexpysErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(12).isAPartOf(daexpysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData daexpy01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData daexpy02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData daexpy03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData doctidsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] doctidErr = FLSArrayPartOfStructure(4, 4, doctidsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(16).isAPartOf(doctidsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData doctid01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData doctid02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData doctid03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData doctid04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData leadysErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData nftdysErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData renwdatesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] renwdateErr = FLSArrayPartOfStructure(2, 4, renwdatesErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(renwdatesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData renwdate1Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData renwdate2Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData zrfreqsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] zrfreqErr = FLSArrayPartOfStructure(6, 4, zrfreqsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(zrfreqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrfreq01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData zrfreq02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData zrfreq03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData zrfreq04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData zrfreq05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData zrfreq06Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData zrincrsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] zrincrErr = FLSArrayPartOfStructure(6, 4, zrincrsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(zrincrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrincr01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData zrincr02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData zrincr03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData zrincr04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData zrincr05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData zrincr06Err = new FixedLengthStringData(4).isAPartOf(filler9, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] arrsmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] collectsubOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData daexpysOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] daexpyOut = FLSArrayPartOfStructure(3, 12, daexpysOut, 0);
	public FixedLengthStringData[][] daexpyO = FLSDArrayPartOfArrayStructure(12, 1, daexpyOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(36).isAPartOf(daexpysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] daexpy01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] daexpy02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] daexpy03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData doctidsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] doctidOut = FLSArrayPartOfStructure(4, 12, doctidsOut, 0);
	public FixedLengthStringData[][] doctidO = FLSDArrayPartOfArrayStructure(12, 1, doctidOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(48).isAPartOf(doctidsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] doctid01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] doctid02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] doctid03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] doctid04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] leadysOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] nftdysOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData renwdatesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] renwdateOut = FLSArrayPartOfStructure(2, 12, renwdatesOut, 0);
	public FixedLengthStringData[][] renwdateO = FLSDArrayPartOfArrayStructure(12, 1, renwdateOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(renwdatesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] renwdate1Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] renwdate2Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData zrfreqsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] zrfreqOut = FLSArrayPartOfStructure(6, 12, zrfreqsOut, 0);
	public FixedLengthStringData[][] zrfreqO = FLSDArrayPartOfArrayStructure(12, 1, zrfreqOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(72).isAPartOf(zrfreqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrfreq01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] zrfreq02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] zrfreq03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] zrfreq04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] zrfreq05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData[] zrfreq06Out = FLSArrayPartOfStructure(12, 1, filler13, 60);
	public FixedLengthStringData zrincrsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] zrincrOut = FLSArrayPartOfStructure(6, 12, zrincrsOut, 0);
	public FixedLengthStringData[][] zrincrO = FLSDArrayPartOfArrayStructure(12, 1, zrincrOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(72).isAPartOf(zrincrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrincr01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] zrincr02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] zrincr03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] zrincr04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] zrincr05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] zrincr06Out = FLSArrayPartOfStructure(12, 1, filler14, 60);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6654screenWritten = new LongData(0);
	public LongData S6654protectWritten = new LongData(0);
	
	public static int[] screenPfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3};

	public boolean hasSubfile() {
		return false;
	}


	public S6654ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(leadysOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(daexpy01Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(doctid01Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(daexpy02Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(doctid02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(daexpy03Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(doctid03Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nftdysOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(doctid04Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(arrsmethOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(renwdate1Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(renwdate2Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrfreq01Out,new String[] {"21",null, "-21","50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrfreq02Out,new String[] {"23",null, "-23","50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrfreq03Out,new String[] {"24",null, "-24","50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrfreq04Out,new String[] {"24",null, "-24","50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrfreq05Out,new String[] {"25",null, "-25","50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrfreq06Out,new String[] {"25",null, "-25","50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrincr01Out,new String[] {"26",null, "-26","51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrincr02Out,new String[] {"27",null, "-27","52", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrincr03Out,new String[] {"28",null, "-28","53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrincr04Out,new String[] {"29",null, "-29","54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrincr05Out,new String[] {"30",null, "-30","55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrincr06Out,new String[] {"32",null, "-32","56", null, null, null, null, null, null, null, null});
		fieldIndMap.put(collectsubOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6654screen.class;
		protectRecord = S6654protect.class;
	}
	

	public int getDataAreaSize()
	{
		return 593 ;
	}
	

	public int getDataFieldsSize()
	{
		return 129 ;
	}
	

	public int getErrorIndicatorSize()
	{
		return 116;
	}
	
	public int getOutputFieldSize()
	{
		return 348;
	}
	

	public BaseData[] getscreenFields()
	{
		return new BaseData[] {company, tabl, item, longdesc, leadDays, daexpy01, doctid01, daexpy02, doctid02, daexpy03, doctid03, nonForfietureDays, doctid04, arrearsMethod, zrfreq01, zrfreq02, zrfreq03, zrfreq04, zrfreq05, zrfreq06, zrincr01, zrincr02, zrincr03, zrincr04, zrincr05, zrincr06, collectsub};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, leadysOut, daexpy01Out, doctid01Out, daexpy02Out, doctid02Out, daexpy03Out, doctid03Out, nftdysOut, doctid04Out, arrsmethOut, zrfreq01Out, zrfreq02Out, zrfreq03Out, zrfreq04Out, zrfreq05Out, zrfreq06Out, zrincr01Out, zrincr02Out, zrincr03Out, zrincr04Out, zrincr05Out, zrincr06Out, collectsubOut};
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, leadysErr, daexpy01Err, doctid01Err, daexpy02Err, doctid02Err, daexpy03Err, doctid03Err, nftdysErr, doctid04Err, arrsmethErr, zrfreq01Err, zrfreq02Err, zrfreq03Err, zrfreq04Err, zrfreq05Err, zrfreq06Err, zrincr01Err, zrincr02Err, zrincr03Err, zrincr04Err, zrincr05Err, zrincr06Err, collectsubErr};
	}
	
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};
	}
	
	public BaseData[] getscreenDateErrFields()
	{
		return  new BaseData[] {};
	}
	
	public static int[] getScreenPfInds()
	{
		return screenPfInds;
	}
	
	public BaseData[] getScreenDateFields(){
		return new BaseData[] {};
	}
	
	public BaseData[] getScreenDateErrFields(){
		return new BaseData[] {};
	}
	
	public BaseData[] getScreenDateDispFields(){
		return new BaseData[] {};
	}

}
