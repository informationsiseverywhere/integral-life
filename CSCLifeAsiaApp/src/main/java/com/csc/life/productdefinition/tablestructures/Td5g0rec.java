package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Td5g0rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData td5g0Rec = new FixedLengthStringData(500);

	public FixedLengthStringData salelicensetype = new FixedLengthStringData(30).isAPartOf(td5g0Rec, 0);
	public FixedLengthStringData agentbank = new FixedLengthStringData(1).isAPartOf(td5g0Rec, 30);
	public FixedLengthStringData teller = new FixedLengthStringData(1).isAPartOf(td5g0Rec, 31);
	public FixedLengthStringData bankmang = new FixedLengthStringData(1).isAPartOf(td5g0Rec, 32);
	
	public FixedLengthStringData filler = new FixedLengthStringData(467).isAPartOf(td5g0Rec, 33, FILLER);

	public void initialize() {
		COBOLFunctions.initialize(td5g0Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			td5g0Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}
