/*
 * File: T6654pt.java
 * Date: 30 August 2009 2:29:06
 * Author: Quipoz Limited
 * 
 * Class transformed from T6654PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6654.
*
*
*****************************************************************
* </pre>
*/
public class T6654pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Billing Control Table                    S6654");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(30);
	private FixedLengthStringData filler7 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine003, 11, FILLER).init("Billing Processing:");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(47);
	private FixedLengthStringData filler9 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine004, 23, FILLER).init("Lead Time (Days):");
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine004, 44).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(65);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine005, 11, FILLER).init("Overdue Processing:            Lag Time         Letter");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(67);
	private FixedLengthStringData filler13 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine006, 43, FILLER).init("(Days)        Subroutine");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(66);
	private FixedLengthStringData filler15 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 23, FILLER).init("Overdue 1:");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 44).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 58);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(66);
	private FixedLengthStringData filler18 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 31, FILLER).init("2:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 44).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 58);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(66);
	private FixedLengthStringData filler21 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 31, FILLER).init("3:");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 44).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 58);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(66);
	private FixedLengthStringData filler24 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler25 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine010, 25, FILLER).init("Arrears:");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 44).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 58);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(48);
	private FixedLengthStringData filler27 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine011, 11, FILLER).init("Arrears Method ( when APL ):");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 44);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(44);
	private FixedLengthStringData filler29 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine012, 11, FILLER).init("Default Premium Renewal Date as :");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(71);
	private FixedLengthStringData filler31 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(39).isAPartOf(wsaaPrtLine013, 23, FILLER).init("Risk Commencement Date");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 62);
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 63, FILLER).init("   (Y/N)");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(71);
	private FixedLengthStringData filler34 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine014, 17, FILLER).init("or    Risk Commencement Date + X Frequency");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 62);
	private FixedLengthStringData filler36 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 63, FILLER).init("   (Y/N)");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(56);
	private FixedLengthStringData filler37 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 17, FILLER).init("Freq:");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 29);
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 34);
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 39);
	private FixedLengthStringData filler41 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 44);
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 49);
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 54);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(56);
	private FixedLengthStringData filler44 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler45 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 17, FILLER).init("Increment:");
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 29).setPattern("ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 34).setPattern("ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 39).setPattern("ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 44).setPattern("ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 49).setPattern("ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 54).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(52);
	private FixedLengthStringData filler51 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler52 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine017, 11, FILLER).init("Premium Collection Subroutine:");
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 44);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6654rec t6654rec = new T6654rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6654pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6654rec.t6654Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t6654rec.leadDays);
		fieldNo006.set(t6654rec.daexpy01);
		fieldNo007.set(t6654rec.doctid01);
		fieldNo008.set(t6654rec.daexpy02);
		fieldNo009.set(t6654rec.doctid02);
		fieldNo010.set(t6654rec.daexpy03);
		fieldNo011.set(t6654rec.doctid03);
		fieldNo012.set(t6654rec.nonForfietureDays);
		fieldNo013.set(t6654rec.doctid04);
		fieldNo014.set(t6654rec.arrearsMethod);
		fieldNo015.set(t6654rec.renwdate1);
		fieldNo016.set(t6654rec.renwdate2);
		fieldNo017.set(t6654rec.zrfreq01);
		fieldNo018.set(t6654rec.zrfreq02);
		fieldNo019.set(t6654rec.zrfreq03);
		fieldNo020.set(t6654rec.zrfreq04);
		fieldNo021.set(t6654rec.zrfreq05);
		fieldNo022.set(t6654rec.zrfreq06);
		fieldNo023.set(t6654rec.zrincr01);
		fieldNo024.set(t6654rec.zrincr02);
		fieldNo025.set(t6654rec.zrincr03);
		fieldNo026.set(t6654rec.zrincr04);
		fieldNo027.set(t6654rec.zrincr05);
		fieldNo028.set(t6654rec.zrincr06);
		fieldNo029.set(t6654rec.collectsub);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
