package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LextpfDAO extends BaseDAO<Lextpf> {
    public Map<Long, List<Lextpf>> searchLextRecord(List<Long> covrUQ);
	public List<Lextpf> searchLextRecord(Lextpf lextRec);
	
	public Map<String, List<Lextpf>> searchLextbrrMap(List<String> chdrnumList);
	public List<Lextpf> getLoadAgeLextpf(Lextpf lextRec);//ILIFE-4970 
	//for ILIFE-4405
	Map<String, List<Lextpf>> searchLextpfMap(String coy, List<String> chdrnumList);
	public Lextpf getLextpfRecord(Lextpf lextpf);
	
	public List<Lextpf> getLextpfListRecord(Lextpf lextpf);

	//add by wli31 ILIFE-7308
	public Lextpf getLextbrrRecord(Lextpf lextpf);
	public List<Lextpf> getLextpfData(Lextpf lext);	//ALS-4251
	public boolean checkLextRecord(String chdrcoy, String chdrNum); //ILIFE-8901
    public List<Lextpf> getLextData(Lextpf lext);//ILB-1222
    int deleteByAutoRecord(String chdrcoy, String chdrnum,String flag);
    int getMaxSeqnbr(String chdrcoy, String chdrnum);
}