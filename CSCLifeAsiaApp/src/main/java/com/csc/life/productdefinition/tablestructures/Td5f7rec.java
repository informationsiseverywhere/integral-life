package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:54
 * Description:
 * Copybook name: Sd5f7REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Td5f7rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData td5f7Rec = new FixedLengthStringData(24);  
  	public FixedLengthStringData itmfrm = new FixedLengthStringData(8).isAPartOf(td5f7Rec, 0);
  	public FixedLengthStringData itmto = new FixedLengthStringData(8).isAPartOf(td5f7Rec, 8);
	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(td5f7Rec, 16);
  	public FixedLengthStringData annind = new FixedLengthStringData(1).isAPartOf(td5f7Rec, 18);
	public FixedLengthStringData cmpday = new FixedLengthStringData(2).isAPartOf(td5f7Rec, 19);
  	public FixedLengthStringData cmpmonth = new FixedLengthStringData(2).isAPartOf(td5f7Rec, 21);
	public FixedLengthStringData transind = new FixedLengthStringData(1).isAPartOf(td5f7Rec, 23);

  
	public void initialize() {
		COBOLFunctions.initialize(td5f7Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			td5f7Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}