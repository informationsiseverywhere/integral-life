/*
 * File: P5662.java
 * Date: 30 August 2009 0:33:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P5662.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.S5662ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5662rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5662 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5662");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/* ERRORS */
	private String e031 = "E031";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5662rec t5662rec = new T5662rec();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5662ScreenVars sv = ScreenProgram.getScreenVars( S5662ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1190, 
		preExit, 
		exit2090, 
		exit3900, 
		exit5090
	}

	public P5662() {
		super();
		screenVars = sv;
		new ScreenModel("S5662", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1001();
				}
				case cont1190: {
					cont1190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1001()
	{
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setDataArea(SPACES);
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setStatuz(e031);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5662rec.t5662Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.cont1190);
		}
		t5662rec.bnypc.set(ZERO);
	}

protected void cont1190()
	{
		sv.bnyfctn.set(t5662rec.bnyfctn);
		sv.bnypc.set(t5662rec.bnypc);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2001();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2001()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
		}
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void update3000()
	{
		try {
			loadWsspFields3100();
		}
		catch (GOTOException e){
		}
	}

protected void loadWsspFields3100()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3900);
		}
		updateRec5000();
		/*COMMIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void updateRec5000()
	{
		try {
			para5000();
			compareFields5020();
			writeRecord5080();
		}
		catch (GOTOException e){
		}
	}

protected void para5000()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		/*READ-RECORD*/
		callItemio5100();
	}

protected void compareFields5020()
	{
		wsaaUpdateFlag = "N";
		checkChanges5300();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.exit5090);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
		itemIO.setGenarea(t5662rec.t5662Rec);
	}

protected void writeRecord5080()
	{
		itemIO.setFunction(varcom.rewrt);
		callItemio5100();
	}

protected void callItemio5100()
	{
		/*PARA*/
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.dbparams.set(itemIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkChanges5300()
	{
		/*PARA*/
		if (isNE(sv.bnyfctn,t5662rec.bnyfctn)) {
			t5662rec.bnyfctn.set(sv.bnyfctn);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bnypc,t5662rec.bnypc)) {
			t5662rec.bnypc.set(sv.bnypc);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}
}
