/*
 * File: Rskamt01
 * Date: 05 Jul 2018 5:48:03 PM
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.productdefinition.recordstructures.Rskamtrec;
import com.csc.life.productdefinition.tablestructures.Td5g4rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.QPUtilities;

public class Rskam01 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private static final String wsaaSubrname = "RSKAM01";
	private static final String td5g4 = "TD5G4";
	private Rskamtrec rskamtrec = new Rskamtrec();
	private Td5g4rec td5g4rec = new Td5g4rec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Syserrrec syserrrec = new Syserrrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private static final String Formula1 = "FORM1";


	public Rskam01() {
		super();
	}

public void mainline(Object... parmArray)
	{
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 1);
	rskamtrec.rskamtRec = convertAndSetParam(rskamtrec.rskamtRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main1000()
	{
		start1000();
		exit090();
	}

protected void start1000()
	{	
	rskamtrec.statuz.set("****");
	readTd5g4();
	calcRiskAmount();

}

protected void readTd5g4()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(rskamtrec.chdrcoy.toString());
	itempf.setItemtabl(td5g4);/* IJTI-1523 */
	itempf.setItemitem(rskamtrec.compcode.toString());
	
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		syserrrec.params.set(itempf.getItemtabl() + itempf.getItemitem()); 
		fatalError600();
	}
	td5g4rec.td5g4Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
}


protected void calcRiskAmount()
	{
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass01) && isEQ(td5g4rec.formula01,Formula1))
				{
				rskamtrec.factor.set(td5g4rec.factor01);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass02) && isEQ(td5g4rec.formula02,Formula1))
				{
				rskamtrec.factor.set(td5g4rec.factor02);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass03) && isEQ(td5g4rec.formula03,Formula1))
				{
				rskamtrec.factor.set(td5g4rec.factor03);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass04) && isEQ(td5g4rec.formula04,Formula1))
				{
				rskamtrec.factor.set(td5g4rec.factor04);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass05) && isEQ(td5g4rec.formula05,Formula1))
				{
				rskamtrec.factor.set(td5g4rec.factor05);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass06) && isEQ(td5g4rec.formula06,Formula1))
				{
				rskamtrec.factor.set(td5g4rec.factor06);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass07) && isEQ(td5g4rec.formula07,Formula1))
				{
				rskamtrec.factor.set(td5g4rec.factor07);
				}
			compute(rskamtrec.riskamnt, 2).set(mult(rskamtrec.sumin, rskamtrec.factor));
		
	}


protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
	/*FATAL*/
	syserrrec.subrname.set(wsaaSubrname);
	if (isNE(syserrrec.statuz, SPACES)
	|| isNE(syserrrec.syserrStatuz, SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT-PROGRAM*/
	exitProgram();
	}
}
