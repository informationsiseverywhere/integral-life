/*
 * File: Vpxibin.java
 * Date: 17 Sep 2012 22:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.interestbearing.recordstructures.Ibincalrec;
import com.csc.life.interestbearing.tablestructures.Th507rec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxibinrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* General Data Extract for Interest Bearing Calculation.
*
* Functions:
* ---------
* INIT   - Returns the fields needed for Interest Bearing Calculation
* GETV   - Returns the value of a single variable to the caller.
*
* Outputs:
* --------
* No output is needed for LifeIntBearing VPMS.
*
* Status:
*
*      **** - Everything checks out ok.
*      E049 - Invalid Function
*      H832 - Field not valid
*
**DD/MM/YY*************************************************************
* </pre>
*/
public class Vpxibin extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPXIBIN";
		/* FORMATS */
	private String chdrlifrec = "CHDRLIFREC";
	private String clexrec = "CLEXREC";
	private String itemrec = "ITEMREC";
	private String th507 = "TH507";
		/* ERRORS */
	private String e049 = "E049";
	private String h832 = "H832";
		/* COPYBOOK */
	private Ibincalrec ibincalrec = new Ibincalrec();
	private Vpxibinrec vpxibinrec = new Vpxibinrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	
	private Th507rec th507rec = new Th507rec();

	private ItdmTableDAM itemIO = new ItdmTableDAM();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit100
	}

	public Vpxibin() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vpxibinrec = (Vpxibinrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
		try {
			para000();
			vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
			exit000();
		}
		catch (COBOLExitProgramException e) {
		}
	}


protected void para000()
	{
		syserrrec.subrname.set(wsaaSubr);
		vpxibinrec.statuz.set(varcom.oK);
		if (isEQ(vpxibinrec.function,"INIT")){
			initialize050();
			extract100();
		}
		else if (isEQ(vpxibinrec.function,"GETV")) {
			returnValue200();
		}
		else {
			syserrrec.statuz.set(e049);
			vpxibinrec.statuz.set(e049);
			systemError900();
		}
		
	}

protected void initialize050()
{
	//No output fields to be initialized.
}

protected void extract100()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				search100();
			}
			case exit100: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void search100()
{
	//READ TH507
	itemIO.setParams(SPACES);
	itemIO.setStatuz(varcom.oK);
	
	itemIO.setItemcoy(ibincalrec.company);
	itemIO.setItemtabl(th507);
	itemIO.setItemitem(ibincalrec.fund);
	itemIO.setItmfrm(ibincalrec.lastIntApp);
	itemIO.setFormat(itemrec);

	//Read the file
	itemIO.setFunction(varcom.begn);
	itemIO.setSelectStatementType(SmartFileCode.SELECT_MULTIPLE);
	SmartFileCode.execute(appVars, itemIO);
	//Unexpected result
	if (isNE(itemIO.getStatuz(),varcom.oK)
	&& isNE(itemIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(itemIO.getParams());
		dbError999();
	}
	//Record not found
	if (isEQ(itemIO.getStatuz(), varcom.endp)
			|| isNE(itemIO.getItemcoy(), ibincalrec.company)
			|| isNE(itemIO.getItemitem(), ibincalrec.fund)
			|| isNE(itemIO.getItemtabl(), th507)) {
		ibincalrec.statuz.set(varcom.mrnf);
		goTo(GotoLabel.exit100);
	}
	th507rec.th507Rec.set(itemIO.getGenarea());
	
	vpxibinrec.copybook.set("VPXIBINREC");
	//UPDATE TO LINKAGE COPYBOOK
	ibincalrec.intEffdate.set(itemIO.getItmfrm());
	if (isNE(th507rec.zannintr, ZERO)){
		ibincalrec.intRate.set(th507rec.zannintr);
	}else{
		ibincalrec.intRate.set(th507rec.zdayintr);
	}
	
}
protected void returnValue200()
	{
		start210();
	}


protected void start210()
{
	vpxibinrec.resultValue.set(SPACES);
	String field = vpxibinrec.resultField.getData();/* IJTI-1523 */
	if (isEQ(field, SPACES)){
		vpxibinrec.resultValue.set(SPACES);
	} else {
		//Field not Valid
		vpxibinrec.statuz.set(h832);
	}
}

protected void systemError900()
{
	/*PARA*/
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}
protected void dbError999()
{
	/*PARA*/
	syserrrec.syserrType.set("1");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}

protected void exit000()
{
	exitProgram();
}
}


