/*
 * File: T5661pt.java
 * Date: 30 August 2009 2:25:11
 * Author: Quipoz Limited
 * 
 * Class transformed from T5661PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5661.
*
*
*
***********************************************************************
* </pre>
*/
public class T5661pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Follow-up Codes                         S5661");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(33);
	private FixedLengthStringData filler7 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Default initial status:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine003, 32);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(60);
	private FixedLengthStringData filler8 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Complete statuses:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 32);
	private FixedLengthStringData filler9 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 35);
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 38);
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 41);
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 44);
	private FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 47);
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 50);
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 53);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 56);
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 59);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(64);
	private FixedLengthStringData filler18 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine005, 0, FILLER).init("  Lead Days:");
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 23).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 26, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine005, 36, FILLER).init("Reminder Days:");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 61).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(69);
	private FixedLengthStringData filler21 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  Letter Type:");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 23);
	private FixedLengthStringData filler22 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine006, 36, FILLER).init("Reminder Letter Type:");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 61);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(62);
	private FixedLengthStringData filler24 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  Print Status:");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 23);
	private FixedLengthStringData filler25 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine007, 36, FILLER).init("Reminder Print Status:");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 61);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(62);
	private FixedLengthStringData filler27 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  Duplicate LETC Allowed:");
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 30);
	private FixedLengthStringData filler28 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine008, 36, FILLER).init("Doctor Required:");
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 61);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(24);
	private FixedLengthStringData filler30 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  Default Extended Text:");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(80);
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(78).isAPartOf(wsaaPrtLine010, 2);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(80);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(78).isAPartOf(wsaaPrtLine011, 2);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(80);
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(78).isAPartOf(wsaaPrtLine012, 2);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(80);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(78).isAPartOf(wsaaPrtLine013, 2);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(80);
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(78).isAPartOf(wsaaPrtLine014, 2);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5661rec t5661rec = new T5661rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5661pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5661rec.t5661Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t5661rec.fupstat);
		fieldNo006.set(t5661rec.fuposs01);
		fieldNo007.set(t5661rec.fuposs02);
		fieldNo008.set(t5661rec.fuposs03);
		fieldNo009.set(t5661rec.fuposs04);
		fieldNo010.set(t5661rec.fuposs05);
		fieldNo011.set(t5661rec.fuposs06);
		fieldNo012.set(t5661rec.fuposs07);
		fieldNo013.set(t5661rec.fuposs08);
		fieldNo014.set(t5661rec.fuposs09);
		fieldNo015.set(t5661rec.fuposs10);
		fieldNo016.set(t5661rec.zleadays);
		fieldNo017.set(t5661rec.zelpdays);
		fieldNo018.set(t5661rec.zlettype);
		fieldNo019.set(t5661rec.zlettyper);
		fieldNo020.set(t5661rec.zletstat);
		fieldNo021.set(t5661rec.zletstatr);
		fieldNo022.set(t5661rec.zdalind);
		fieldNo023.set(t5661rec.zdocind);
		fieldNo024.set(t5661rec.message01);
		fieldNo025.set(t5661rec.message02);
		fieldNo026.set(t5661rec.message03);
		fieldNo027.set(t5661rec.message04);
		fieldNo028.set(t5661rec.message05);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
