/*
 * File: T5688pt.java
 * Date: 30 August 2009 2:26:09
 * Author: Quipoz Limited
 * 
 * Class transformed from T5688PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5688.
*
*
*****************************************************************
* </pre>
*/
public class T5688pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(75);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Contract Definition                      S5688");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 26, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 35);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine003, 0, FILLER).init("    Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 18);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 28, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler9 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Contract validation                      Minimum        Maximum     Default");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(75);
	private FixedLengthStringData filler10 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine005, 0, FILLER).init("   Number of policies on plan:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 45).setPattern("ZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 60).setPattern("ZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 73).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(62);
	private FixedLengthStringData filler13 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine006, 13, FILLER).init("lives per policy:");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 60).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(62);
	private FixedLengthStringData filler16 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine007, 13, FILLER).init("joint lives per life:");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 60).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler19 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine008, 0, FILLER).init("   Contract fee method:");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler20 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private FixedLengthStringData filler21 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine008, 42, FILLER).init("Default follow ups:");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 71);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(75);
	private FixedLengthStringData filler22 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine009, 0, FILLER).init("   Minimum deposit method:");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler23 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine009, 42, FILLER).init("Tax Relief Method:");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 71);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(72);
	private FixedLengthStringData filler25 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine010, 0, FILLER).init("   Can/Alt from Inception limit:");
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 33).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 36, FILLER).init(SPACES);
	private FixedLengthStringData filler27 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine010, 42, FILLER).init("New business allowed flag:");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 71);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler28 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine011, 0, FILLER).init("   Non-Forfeiture Option:");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler29 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 36, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine011, 42, FILLER).init("Bill into bankcode:");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 71);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(72);
	private FixedLengthStringData filler31 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine012, 0, FILLER).init("   Allow input of CFI Refund Price Date:");
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 41);
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 42, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine012, 48, FILLER).init("Certificate Allowed:");
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 71);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(24);
	private FixedLengthStringData filler34 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" Life assured validation");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(78);
	private FixedLengthStringData filler35 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine014, 0, FILLER).init("   Age:");
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 33);
	private FixedLengthStringData filler36 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler37 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine014, 42, FILLER).init("Component Level Accounting (Y/N):");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 77);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(78);
	private FixedLengthStringData filler38 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine015, 0, FILLER).init("   Smoking:");
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 33);
	private FixedLengthStringData filler39 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler40 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine015, 42, FILLER).init("Revenue Accounting (Y/N):");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 77);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(70);
	private FixedLengthStringData filler41 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine016, 0, FILLER).init("   Occupation:");
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 33);
	private FixedLengthStringData filler42 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler43 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine016, 42, FILLER).init("(Default is Cash Accounting)");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(80);
	private FixedLengthStringData filler44 = new FixedLengthStringData(80).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" Allowable coverages and riders are held on the Contract Structure table (T5673)");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5688rec t5688rec = new T5688rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5688pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5688rec.t5688Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5688rec.polmin);
		fieldNo008.set(t5688rec.polmax);
		fieldNo009.set(t5688rec.poldef);
		fieldNo010.set(t5688rec.lifemin);
		fieldNo011.set(t5688rec.lifemax);
		fieldNo012.set(t5688rec.jlifemin);
		fieldNo013.set(t5688rec.jlifemax);
		fieldNo014.set(t5688rec.feemeth);
		fieldNo015.set(t5688rec.defFupMeth);
		fieldNo016.set(t5688rec.minDepMth);
		fieldNo017.set(t5688rec.taxrelmth);
		fieldNo018.set(t5688rec.cfiLim);
		fieldNo019.set(t5688rec.nbusallw);
		fieldNo020.set(t5688rec.znfopt);
		fieldNo021.set(t5688rec.bankcode);
		fieldNo024.set(t5688rec.anbrqd);
		fieldNo025.set(t5688rec.comlvlacc);
		fieldNo026.set(t5688rec.smkrqd);
		fieldNo027.set(t5688rec.revacc);
		fieldNo028.set(t5688rec.occrqd);
		fieldNo022.set(t5688rec.zcfidtall);
		fieldNo023.set(t5688rec.zcertall);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
