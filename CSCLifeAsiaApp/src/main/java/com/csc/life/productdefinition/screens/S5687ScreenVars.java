package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5687
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class S5687ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1109);
	public FixedLengthStringData dataFields = new FixedLengthStringData(229).isAPartOf(dataArea, 0);
	public FixedLengthStringData anniversaryMethod = DD.annvry.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData basicCommMeth = DD.bascmeth.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData bascpy = DD.bascpy.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData basscmth = DD.basscmth.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData basscpy = DD.basscpy.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData bastcmth = DD.bastcmth.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData bastcpy = DD.bastcpy.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData bbmeth = DD.bbmeth.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData dcmeth = DD.dcmeth.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData defFupMeth = DD.deffup.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,41);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,49);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData jlPremMeth = DD.jlpremeth.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData jlifePresent = DD.jlpres.copy().isAPartOf(dataFields,69);
	public FixedLengthStringData loanmeth = DD.loanmeth.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData maturityCalcMeth = DD.matumeth.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData nonForfeitMethod = DD.nffeit.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData partsurr = DD.partsurr.copy().isAPartOf(dataFields,112);
	public ZonedDecimalData premGuarPeriod = DD.pguarp.copyToZonedDecimal().isAPartOf(dataFields,116);
	public FixedLengthStringData premmeth = DD.premmeth.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData pumeth = DD.pumeth.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData reptcds = new FixedLengthStringData(18).isAPartOf(dataFields, 127);
	public FixedLengthStringData[] reptcd = FLSArrayPartOfStructure(6, 3, reptcds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(reptcds, 0, FILLER_REDEFINE);
	public FixedLengthStringData reptcd01 = DD.reptcd.copy().isAPartOf(filler,0);
	public FixedLengthStringData reptcd02 = DD.reptcd.copy().isAPartOf(filler,3);
	public FixedLengthStringData reptcd03 = DD.reptcd.copy().isAPartOf(filler,6);
	public FixedLengthStringData reptcd04 = DD.reptcd.copy().isAPartOf(filler,9);
	public FixedLengthStringData reptcd05 = DD.reptcd.copy().isAPartOf(filler,12);
	public FixedLengthStringData reptcd06 = DD.reptcd.copy().isAPartOf(filler,15);
	public FixedLengthStringData riind = DD.riind.copy().isAPartOf(dataFields,145);
	public FixedLengthStringData rnwcpy = DD.rnwcpy.copy().isAPartOf(dataFields,146);
	public ZonedDecimalData rtrnwfreq = DD.rtrnwfreq.copyToZonedDecimal().isAPartOf(dataFields,150);
	public FixedLengthStringData schedCode = DD.schcde.copy().isAPartOf(dataFields,152);
	public FixedLengthStringData sdtyPayMeth = DD.sdpymeth.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData singlePremInd = DD.singpind.copy().isAPartOf(dataFields,168);
	public FixedLengthStringData srvcpy = DD.srvcpy.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,173);
	public FixedLengthStringData stampDutyMeth = DD.stmpmeth.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData statSubSect = DD.stssct.copy().isAPartOf(dataFields,180);
	public FixedLengthStringData svMethod = DD.svmeth.copy().isAPartOf(dataFields,184);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData xfreqAltBasis = DD.xfreqalt.copy().isAPartOf(dataFields,193);
	public FixedLengthStringData zrorcmrg = DD.zrorcmrg.copy().isAPartOf(dataFields,197);
	public FixedLengthStringData zrorcmsp = DD.zrorcmsp.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData zrorcmtu = DD.zrorcmtu.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData zrorpmrg = DD.zrorpmrg.copy().isAPartOf(dataFields,209);
	public FixedLengthStringData zrorpmsp = DD.zrorpmsp.copy().isAPartOf(dataFields,213);
	public FixedLengthStringData zrorpmtu = DD.zrorpmtu.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData zrrcombas = DD.zrrcombas.copy().isAPartOf(dataFields,221);
	public FixedLengthStringData zsbsmeth = DD.zsbsmeth.copy().isAPartOf(dataFields,222);
	public FixedLengthStringData zsredtrm = DD.zsredtrm.copy().isAPartOf(dataFields,226);
	public FixedLengthStringData zszprmcd = DD.zszprmcd.copy().isAPartOf(dataFields,227);
	public FixedLengthStringData lnkgind = DD.jpjlnkind.copy().isAPartOf(dataFields, 228);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(220).isAPartOf(dataArea, 229);
	public FixedLengthStringData annvryErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bascmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bascpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData basscmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData basscpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bastcmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bastcpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bbmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData dcmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData deffupErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlpremethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlpresErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData loanmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData matumethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData nffeitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData partsurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pguarpErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData premmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pumethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData reptcdsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData[] reptcdErr = FLSArrayPartOfStructure(6, 4, reptcdsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(reptcdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData reptcd01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData reptcd02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData reptcd03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData reptcd04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData reptcd05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData reptcd06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData riindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rnwcpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData rtrnwfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData schcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData sdpymethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData singpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData srvcpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData stmpmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData stssctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData svmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData xfreqaltErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData zrorcmrgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData zrorcmspErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData zrorcmtuErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData zrorpmrgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData zrorpmspErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData zrorpmtuErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData zrrcombasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData zsbsmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData zsredtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData zszprmcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData lnkgindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(660).isAPartOf(dataArea, 449);
	public FixedLengthStringData[] annvryOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bascmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bascpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] basscmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] basscpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bastcmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bastcpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bbmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] dcmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] deffupOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlpremethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlpresOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] loanmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] matumethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] nffeitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] partsurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pguarpOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] premmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pumethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData reptcdsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 288);
	public FixedLengthStringData[] reptcdOut = FLSArrayPartOfStructure(6, 12, reptcdsOut, 0);
	public FixedLengthStringData[][] reptcdO = FLSDArrayPartOfArrayStructure(12, 1, reptcdOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(72).isAPartOf(reptcdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] reptcd01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] reptcd02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] reptcd03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] reptcd04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] reptcd05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] reptcd06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] riindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rnwcpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] rtrnwfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] schcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] sdpymethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] singpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] srvcpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] stmpmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] stssctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] svmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] xfreqaltOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] zrorcmrgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] zrorcmspOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] zrorcmtuOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] zrorpmrgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] zrorpmspOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] zrorpmtuOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] zrrcombasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] zsbsmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] zsredtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] zszprmcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[]  lnkgindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5687screenWritten = new LongData(0);
	public LongData S5687protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5687ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(premmethOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlpremethOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlpresOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pguarpOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rtrnwfreqOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(singpindOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stmpmethOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sdpymethOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zsredtrmOut,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bbmethOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nffeitOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annvryOut,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(partsurrOut,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bascmethOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(basscmthOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bastcmthOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bascpyOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(basscpyOut,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bastcpyOut,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(srvcpyOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rnwcpyOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(deffupOut,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(schcdeOut,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stfundOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsectOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stssctOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reptcd01Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reptcd02Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reptcd03Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reptcd04Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reptcd05Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reptcd06Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(matumethOut,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(svmethOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dcmethOut,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pumethOut,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(xfreqaltOut,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(loanmethOut,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorcmrgOut,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorpmrgOut,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorcmspOut,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorpmspOut,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorcmtuOut,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorpmtuOut,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zsbsmethOut,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lnkgindOut,new String[] {null, null, null, "62", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, premmeth, jlPremMeth, jlifePresent, premGuarPeriod, rtrnwfreq, singlePremInd, stampDutyMeth, sdtyPayMeth, zsredtrm, bbmeth, nonForfeitMethod, riind, anniversaryMethod, partsurr, basicCommMeth, basscmth, bastcmth, bascpy, basscpy, bastcpy, srvcpy, rnwcpy, defFupMeth, schedCode, zszprmcd, statFund, statSect, statSubSect, reptcd01, reptcd02, reptcd03, reptcd04, reptcd05, reptcd06, maturityCalcMeth, svMethod, dcmeth, pumeth, xfreqAltBasis, loanmeth, zrorcmrg, zrorpmrg, zrorcmsp, zrorpmsp, zrorcmtu, zrorpmtu, zrrcombas, zsbsmeth,lnkgind};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, premmethOut, jlpremethOut, jlpresOut, pguarpOut, rtrnwfreqOut, singpindOut, stmpmethOut, sdpymethOut, zsredtrmOut, bbmethOut, nffeitOut, riindOut, annvryOut, partsurrOut, bascmethOut, basscmthOut, bastcmthOut, bascpyOut, basscpyOut, bastcpyOut, srvcpyOut, rnwcpyOut, deffupOut, schcdeOut, zszprmcdOut, stfundOut, stsectOut, stssctOut, reptcd01Out, reptcd02Out, reptcd03Out, reptcd04Out, reptcd05Out, reptcd06Out, matumethOut, svmethOut, dcmethOut, pumethOut, xfreqaltOut, loanmethOut, zrorcmrgOut, zrorpmrgOut, zrorcmspOut, zrorpmspOut, zrorcmtuOut, zrorpmtuOut, zrrcombasOut, zsbsmethOut,lnkgindOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, premmethErr, jlpremethErr, jlpresErr, pguarpErr, rtrnwfreqErr, singpindErr, stmpmethErr, sdpymethErr, zsredtrmErr, bbmethErr, nffeitErr, riindErr, annvryErr, partsurrErr, bascmethErr, basscmthErr, bastcmthErr, bascpyErr, basscpyErr, bastcpyErr, srvcpyErr, rnwcpyErr, deffupErr, schcdeErr, zszprmcdErr, stfundErr, stsectErr, stssctErr, reptcd01Err, reptcd02Err, reptcd03Err, reptcd04Err, reptcd05Err, reptcd06Err, matumethErr, svmethErr, dcmethErr, pumethErr, xfreqaltErr, loanmethErr, zrorcmrgErr, zrorpmrgErr, zrorcmspErr, zrorpmspErr, zrorcmtuErr, zrorpmtuErr, zrrcombasErr, zsbsmethErr,lnkgindErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5687screen.class;
		protectRecord = S5687protect.class;
	}

}
