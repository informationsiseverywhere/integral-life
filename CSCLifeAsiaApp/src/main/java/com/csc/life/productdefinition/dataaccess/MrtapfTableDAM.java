package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;


/**
 * 	
 * File: MrtapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:51
 * Class transformed from MRTAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MrtapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 118;
	public FixedLengthStringData mrtarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData mrtapfRecord = mrtarec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(mrtarec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(mrtarec);
	public PackedDecimalData prat = DD.prat.copy().isAPartOf(mrtarec);
	public PackedDecimalData coverc = DD.coverc.copy().isAPartOf(mrtarec);
	public FixedLengthStringData prmdetails = DD.prmdetails.copy().isAPartOf(mrtarec);
	public FixedLengthStringData mlinsopt = DD.mlinsopt.copy().isAPartOf(mrtarec);
	public FixedLengthStringData mlresind = DD.mlresind.copy().isAPartOf(mrtarec);
	public FixedLengthStringData mbnkref = DD.mbnkref.copy().isAPartOf(mrtarec);
	// BRD-139:Start
	public PackedDecimalData loandur = DD.loandur.copy().isAPartOf(mrtarec);
	public FixedLengthStringData intcaltype = DD.intclType.copy().isAPartOf(mrtarec);
	public PackedDecimalData phunitvalue = DD.unitval.copy().isAPartOf(mrtarec);
	// BRD-139:End
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(mrtarec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(mrtarec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(mrtarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.	
	*/	
	public MrtapfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MrtapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MrtapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MrtapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MrtapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MrtapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MrtapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MRTAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PRAT, " +
							"COVERC, " +
							"PRMDETAILS, " +
							"MLINSOPT, " +
							"MLRESIND, " +
							"MBNKREF, " +
							"LOANDUR, " +  //BRD-139
					        "INTCALTYPE, " + //BRD-139
					        "PHUNITVALUE" + 
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     prat,
                                     coverc,
                                     prmdetails,
                                     mlinsopt,
                                     mlresind,
                                     mbnkref,
                                     loandur, //BRD-139
                                     intcaltype, //BRD-139
                                     phunitvalue,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		prat.clear();
  		coverc.clear();
  		prmdetails.clear();
  		mlinsopt.clear();
  		mlresind.clear();
  		mbnkref.clear();
  		loandur.clear();//BRD-139
  		intcaltype.clear();//BRD-139
  		phunitvalue.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMrtarec() {
  		return mrtarec;
	}

	public FixedLengthStringData getMrtapfRecord() {
  		return mrtapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMrtarec(what);
	}

	public void setMrtarec(Object what) {
  		this.mrtarec.set(what);
	}

	public void setMrtapfRecord(Object what) {
  		this.mrtapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(mrtarec.getLength());
		result.set(mrtarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}