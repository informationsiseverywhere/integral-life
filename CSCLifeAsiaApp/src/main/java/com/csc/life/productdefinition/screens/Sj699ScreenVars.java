package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SJ699
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sj699ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(636);
	public FixedLengthStringData dataFields = new FixedLengthStringData(156).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData freqcys = new FixedLengthStringData(24).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(12, 2, freqcys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(freqcys, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01 = DD.freqcy.copy().isAPartOf(filler,0);
	public FixedLengthStringData freqcy02 = DD.freqcy.copy().isAPartOf(filler,2);
	public FixedLengthStringData freqcy03 = DD.freqcy.copy().isAPartOf(filler,4);
	public FixedLengthStringData freqcy04 = DD.freqcy.copy().isAPartOf(filler,6);
	public FixedLengthStringData freqcy05 = DD.freqcy.copy().isAPartOf(filler,8);
	public FixedLengthStringData freqcy06 = DD.freqcy.copy().isAPartOf(filler,10);
	public FixedLengthStringData freqcy07 = DD.freqcy.copy().isAPartOf(filler,12);
	public FixedLengthStringData freqcy08 = DD.freqcy.copy().isAPartOf(filler,14);
	public FixedLengthStringData freqcy09 = DD.freqcy.copy().isAPartOf(filler,16);
	public FixedLengthStringData freqcy10 = DD.freqcy.copy().isAPartOf(filler,18);
	public FixedLengthStringData freqcy11 = DD.freqcy.copy().isAPartOf(filler,20);
	public FixedLengthStringData freqcy12 = DD.freqcy.copy().isAPartOf(filler,22);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,33);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,41);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData zlfacts = new FixedLengthStringData(72).isAPartOf(dataFields, 84);
	public ZonedDecimalData[] zlfact = ZDArrayPartOfStructure(12, 6, 4, zlfacts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(72).isAPartOf(zlfacts, 0, FILLER_REDEFINE);
	public ZonedDecimalData zlfact01 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData zlfact02 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData zlfact03 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData zlfact04 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData zlfact05 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData zlfact06 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData zlfact07 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,36);
	public ZonedDecimalData zlfact08 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,42);
	public ZonedDecimalData zlfact09 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,48);
	public ZonedDecimalData zlfact10 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,54);
	public ZonedDecimalData zlfact11 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,60);
	public ZonedDecimalData zlfact12 = DD.zlfact.copyToZonedDecimal().isAPartOf(filler1,66);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 156);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData freqcysErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] freqcyErr = FLSArrayPartOfStructure(12, 4, freqcysErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(freqcysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData freqcy02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData freqcy03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData freqcy04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData freqcy05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData freqcy06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData freqcy07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData freqcy08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData freqcy09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData freqcy10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData freqcy11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData freqcy12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData zlfactsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] zlfactErr = FLSArrayPartOfStructure(12, 4, zlfactsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(zlfactsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zlfact01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData zlfact02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData zlfact03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData zlfact04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData zlfact05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData zlfact06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData zlfact07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData zlfact08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData zlfact09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData zlfact10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData zlfact11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData zlfact12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 276);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData freqcysOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] freqcyOut = FLSArrayPartOfStructure(12, 12, freqcysOut, 0);
	public FixedLengthStringData[][] freqcyO = FLSDArrayPartOfArrayStructure(12, 1, freqcyOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(144).isAPartOf(freqcysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] freqcy01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] freqcy02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] freqcy03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] freqcy04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] freqcy05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] freqcy06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] freqcy07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] freqcy08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] freqcy09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] freqcy10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] freqcy11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] freqcy12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData zlfactsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] zlfactOut = FLSArrayPartOfStructure(12, 12, zlfactsOut, 0);
	public FixedLengthStringData[][] zlfactO = FLSDArrayPartOfArrayStructure(12, 1, zlfactOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(zlfactsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zlfact01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] zlfact02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] zlfact03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] zlfact04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] zlfact05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] zlfact06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] zlfact07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] zlfact08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] zlfact09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] zlfact10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] zlfact11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] zlfact12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sj699screenWritten = new LongData(0);
	public LongData Sj699protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sj699ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(freqcy01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy08Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy09Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, freqcy01, freqcy02, freqcy03, freqcy05, zlfact02, zlfact01, freqcy04, freqcy07, zlfact03, zlfact04, zlfact05, zlfact06, freqcy06, freqcy08, freqcy09, zlfact07, zlfact08, zlfact09, freqcy10, freqcy11, freqcy12, zlfact10, zlfact11, zlfact12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, freqcy01Out, freqcy02Out, freqcy03Out, freqcy05Out, zlfact02Out, zlfact01Out, freqcy04Out, freqcy07Out, zlfact03Out, zlfact04Out, zlfact05Out, zlfact06Out, freqcy06Out, freqcy08Out, freqcy09Out, zlfact07Out, zlfact08Out, zlfact09Out, freqcy10Out, freqcy11Out, freqcy12Out, zlfact10Out, zlfact11Out, zlfact12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, freqcy01Err, freqcy02Err, freqcy03Err, freqcy05Err, zlfact02Err, zlfact01Err, freqcy04Err, freqcy07Err, zlfact03Err, zlfact04Err, zlfact05Err, zlfact06Err, freqcy06Err, freqcy08Err, freqcy09Err, zlfact07Err, zlfact08Err, zlfact09Err, freqcy10Err, freqcy11Err, freqcy12Err, zlfact10Err, zlfact11Err, zlfact12Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sj699screen.class;
		protectRecord = Sj699protect.class;
	}

}
