/*
 * File: P5661.java
 * Date: 30 August 2009 0:33:23
 * Author: Quipoz Limited
 * 
 * Class transformed from P5661.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.procedures.T5661pt;
import com.csc.life.productdefinition.screens.S5661ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5661 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5661");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected T5661rec t5661rec = getT5661rec();
	
	private Wsspsmart wsspsmart = new Wsspsmart();
	//private S5661ScreenVars sv = ScreenProgram.getScreenVars( S5661ScreenVars.class);
	private S5661ScreenVars sv = getPScreenVars(); 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5661() {
		super();
		screenVars = sv;
		new ScreenModel("S5661", AppVars.getInstance(), sv);
	}
	protected S5661ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S5661ScreenVars.class);
	}
	
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5661rec.zelpdays.set(ZERO);
		t5661rec.zleadays.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.fuposss.set(t5661rec.fuposss);
		sv.fupstat.set(t5661rec.fupstat);
		sv.zdalind.set(t5661rec.zdalind);
		sv.zdocind.set(t5661rec.zdocind);
		sv.zelpdays.set(t5661rec.zelpdays);
		sv.zleadays.set(t5661rec.zleadays);
		sv.zletstat.set(t5661rec.zletstat);
		sv.zletstatr.set(t5661rec.zletstatr);
		sv.zlettype.set(t5661rec.zlettype);
		sv.zlettyper.set(t5661rec.zlettyper);
		sv.message01.set(t5661rec.message01);
		sv.message02.set(t5661rec.message02);
		sv.message03.set(t5661rec.message03);
		sv.message04.set(t5661rec.message04);
		sv.message05.set(t5661rec.message05);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(t5661rec.t5661Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.fuposss,t5661rec.fuposss)) {
			t5661rec.fuposss.set(sv.fuposss);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.fupstat,t5661rec.fupstat)) {
			t5661rec.fupstat.set(sv.fupstat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zdalind,t5661rec.zdalind)) {
			t5661rec.zdalind.set(sv.zdalind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zdocind,t5661rec.zdocind)) {
			t5661rec.zdocind.set(sv.zdocind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zelpdays,t5661rec.zelpdays)) {
			t5661rec.zelpdays.set(sv.zelpdays);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zleadays,t5661rec.zleadays)) {
			t5661rec.zleadays.set(sv.zleadays);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zletstat,t5661rec.zletstat)) {
			t5661rec.zletstat.set(sv.zletstat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zletstatr,t5661rec.zletstatr)) {
			t5661rec.zletstatr.set(sv.zletstatr);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zlettype,t5661rec.zlettype)) {
			t5661rec.zlettype.set(sv.zlettype);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zlettyper,t5661rec.zlettyper)) {
			t5661rec.zlettyper.set(sv.zlettyper);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.message01,t5661rec.message01)) {
			t5661rec.message01.set(sv.message01);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.message02,t5661rec.message02)) {
			t5661rec.message02.set(sv.message02);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.message03,t5661rec.message03)) {
			t5661rec.message03.set(sv.message03);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.message04,t5661rec.message04)) {
			t5661rec.message04.set(sv.message04);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.message05,t5661rec.message05)) {
			t5661rec.message05.set(sv.message05);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5661pt.class, wsaaTablistrec);
		/*EXIT*/
	}
public T5661rec getT5661rec() {
	return new T5661rec();
} 

public String getWsaaUpdateFlag() {
	return wsaaUpdateFlag;
}

public void setWsaaUpdateFlag(String wsaaUpdateFlag) {
	this.wsaaUpdateFlag = wsaaUpdateFlag;
}

}
