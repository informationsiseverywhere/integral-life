/*
 * File: Calccprm
 * Date: 21 Sep 2012 5:48:03 PM
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;
import java.util.HashMap;

import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.quipoz.COBOLFramework.datatype.ExternalData;

public class Calccprm extends Vpmcalc {
	private Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
	private Vpxlextrec vpxlextec = new Vpxlextrec();

	public Calccprm() {
		super();
	}

public void mainline(Object... parmArray)
{
	recordMap = new HashMap<String, ExternalData>();
	recordMap.put("VPMFMTREC", vpmfmtrec);

	vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);

	recordMap.put("VPXCHDRREC", vpxchdrrec);
	recordMap.put("VPXLEXTREC", vpxlextec);
	
	try {
		startSubr010();
		
	}
	catch (COBOLExitProgramException e) {
	}
	
}
}
