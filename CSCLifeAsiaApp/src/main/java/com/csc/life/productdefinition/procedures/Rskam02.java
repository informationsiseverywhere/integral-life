/*
 * File: Rskamt02
 * Date: 05 Jul 2018 5:48:03 PM
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.recordstructures.Rskamtrec;
import com.csc.life.productdefinition.tablestructures.Td5g4rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.QPUtilities;

public class Rskam02 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private static final String wsaaSubrname = "RSKAM02";
	private static final String wsaaFormula = "FORM2";
		/* TABLES */
	private static final String td5g4 = "TD5G4";
	
	private Rskamtrec rskamtrec = new Rskamtrec();
	private Td5g4rec td5g4rec = new Td5g4rec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Syserrrec syserrrec = new Syserrrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();



	public Rskam02() {
		super();
	}

public void mainline(Object... parmArray)
	{
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 1);
	rskamtrec.rskamtRec = convertAndSetParam(rskamtrec.rskamtRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main1000()
	{
		start1000();
		exit090();
	}

protected void start1000()
	{	
	rskamtrec.statuz.set("****");
	readTd5g4();
	calcRiskAmount();
     }

protected void readTd5g4()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(rskamtrec.chdrcoy.toString());
	itempf.setItemtabl(td5g4);
	itempf.setItemitem(rskamtrec.compcode.toString().trim());
	
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		syserrrec.params.set(itempf.getItemtabl() + itempf.getItemitem()); 
		fatalError600();
	}
	td5g4rec.td5g4Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
}


protected void calcRiskAmount()
	{
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass01) && isEQ(td5g4rec.formula01,wsaaFormula))
				{
				rskamtrec.factor.set(td5g4rec.factor01);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass02) && isEQ(td5g4rec.formula02,wsaaFormula))
				{
				rskamtrec.factor.set(td5g4rec.factor02);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass03) && isEQ(td5g4rec.formula03,wsaaFormula))
				{
				rskamtrec.factor.set(td5g4rec.factor03);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass04) && isEQ(td5g4rec.formula04,wsaaFormula))
				{
				rskamtrec.factor.set(td5g4rec.factor04);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass05) && isEQ(td5g4rec.formula05,wsaaFormula))
				{
				rskamtrec.factor.set(td5g4rec.factor05);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass06) && isEQ(td5g4rec.formula06,wsaaFormula))
				{
				rskamtrec.factor.set(td5g4rec.factor06);
				}
			if(isEQ(rskamtrec.lrkcls,td5g4rec.riskclass07) && isEQ(td5g4rec.formula07,wsaaFormula))
				{
				rskamtrec.factor.set(td5g4rec.factor07);
				}
			calcFreq();
			compute(rskamtrec.riskamnt, 2).set(mult(rskamtrec.modeprem,datcon3rec.freqFactor,rskamtrec.premCessTerm,rskamtrec.factor));
		
	}

protected void calcFreq()
{
	
	initialize(datcon2rec.datcon2Rec);
	datcon2rec.intDate1.set(rskamtrec.riskcommDate);
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	
	initialize(datcon3rec.datcon3Rec);
	datcon3rec.intDate1.set(rskamtrec.riskcommDate);
	datcon3rec.intDate2.set(datcon2rec.intDate2);
	datcon3rec.frequency.set(rskamtrec.billingfreq);
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	
	
	}

protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
	/*FATAL*/
	syserrrec.subrname.set(wsaaSubrname);
	if (isNE(syserrrec.statuz, SPACES)
	|| isNE(syserrrec.syserrStatuz, SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT-PROGRAM*/
	exitProgram();
	}
}
