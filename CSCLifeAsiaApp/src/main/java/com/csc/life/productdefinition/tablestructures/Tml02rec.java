package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:55
 * Description:
 * Copybook name: TML02REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tml02rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tml02Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData fupcode = new FixedLengthStringData(3).isAPartOf(tml02Rec, 0);
  	public ZonedDecimalData prcnt = new ZonedDecimalData(5, 2).isAPartOf(tml02Rec, 3);
  	public FixedLengthStringData taxind = new FixedLengthStringData(1).isAPartOf(tml02Rec, 8);
  	public FixedLengthStringData filler = new FixedLengthStringData(491).isAPartOf(tml02Rec, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tml02Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tml02Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}