package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR51C
 * @version 1.0 generated on 30/08/09 07:16
 * @author Quipoz
 */
public class Sr51cScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(780);
	public FixedLengthStringData dataFields = new FixedLengthStringData(300).isAPartOf(dataArea, 0);
	public FixedLengthStringData ages = new FixedLengthStringData(36).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] age = ZDArrayPartOfStructure(12, 3, 0, ages, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(ages, 0, FILLER_REDEFINE);
	public ZonedDecimalData age01 = DD.age.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData age02 = DD.age.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData age03 = DD.age.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData age04 = DD.age.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData age05 = DD.age.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData age06 = DD.age.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData age07 = DD.age.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData age08 = DD.age.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData age09 = DD.age.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData age10 = DD.age.copyToZonedDecimal().isAPartOf(filler,27);
	public ZonedDecimalData age11 = DD.age.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData age12 = DD.age.copyToZonedDecimal().isAPartOf(filler,33);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,37);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,45);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,53);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData suminss = new FixedLengthStringData(204).isAPartOf(dataFields, 91);
	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(12, 17, 2, suminss, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(204).isAPartOf(suminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumins01 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData sumins02 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData sumins03 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData sumins04 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData sumins05 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData sumins06 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData sumins07 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData sumins08 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData sumins09 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,136);
	public ZonedDecimalData sumins10 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,153);
	public ZonedDecimalData sumins11 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,170);
	public ZonedDecimalData sumins12 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,187);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,295);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 300);
	public FixedLengthStringData agesErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] ageErr = FLSArrayPartOfStructure(12, 4, agesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(agesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData age01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData age02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData age03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData age04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData age05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData age06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData age07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData age08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData age09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData age10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData age11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData age12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData suminssErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] suminsErr = FLSArrayPartOfStructure(12, 4, suminssErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(suminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumins01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData sumins02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData sumins03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData sumins04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData sumins05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData sumins06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData sumins07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData sumins08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData sumins09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData sumins10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData sumins11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData sumins12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 420);
	public FixedLengthStringData agesOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] ageOut = FLSArrayPartOfStructure(12, 12, agesOut, 0);
	public FixedLengthStringData[][] ageO = FLSDArrayPartOfArrayStructure(12, 1, ageOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(144).isAPartOf(agesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] age01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] age02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] age03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] age04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] age05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] age06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] age07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] age08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] age09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] age10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] age11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] age12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData suminssOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 12, suminssOut, 0);
	public FixedLengthStringData[][] suminsO = FLSDArrayPartOfArrayStructure(12, 1, suminsOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(suminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumins01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] sumins02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] sumins03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] sumins04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] sumins05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] sumins06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] sumins07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] sumins08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] sumins09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] sumins10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] sumins11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] sumins12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr51cscreenWritten = new LongData(0);
	public LongData Sr51cprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr51cScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(age01Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age02Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age03Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age04Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age05Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age06Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age07Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age08Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age09Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age10Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age11Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(age12Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins01Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins02Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins03Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins04Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins05Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins06Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins07Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins08Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins09Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins10Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins11Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins12Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, age01, age02, age03, age04, age05, age06, age07, age08, age09, age10, age11, age12, sumins01, sumins02, sumins03, sumins04, sumins05, sumins06, sumins07, sumins08, sumins09, sumins10, sumins11, sumins12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, age01Out, age02Out, age03Out, age04Out, age05Out, age06Out, age07Out, age08Out, age09Out, age10Out, age11Out, age12Out, sumins01Out, sumins02Out, sumins03Out, sumins04Out, sumins05Out, sumins06Out, sumins07Out, sumins08Out, sumins09Out, sumins10Out, sumins11Out, sumins12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, age01Err, age02Err, age03Err, age04Err, age05Err, age06Err, age07Err, age08Err, age09Err, age10Err, age11Err, age12Err, sumins01Err, sumins02Err, sumins03Err, sumins04Err, sumins05Err, sumins06Err, sumins07Err, sumins08Err, sumins09Err, sumins10Err, sumins11Err, sumins12Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr51cscreen.class;
		protectRecord = Sr51cprotect.class;
	}

}
