package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZmpvpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:00
 * Class transformed from ZMPVPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZmpvpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 196;
	public FixedLengthStringData zmpvrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zmpvpfRecord = zmpvrec;
	
	public FixedLengthStringData zmedcoy = DD.zmedcoy.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData zmednum = DD.zmednum.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData zmednam = DD.zmednam.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData zmedsts = DD.zmedsts.copy().isAPartOf(zmpvrec);
	public PackedDecimalData dteapp = DD.dteapp.copy().isAPartOf(zmpvrec);
	public PackedDecimalData dtetrm = DD.dtetrm.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData zaracde = DD.zaracde.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zmpvrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zmpvrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZmpvpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZmpvpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZmpvpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZmpvpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpvpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZmpvpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpvpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZMPVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ZMEDCOY, " +
							"ZMEDPRV, " +
							"ZMEDNUM, " +
							"ZMEDNAM, " +
							"ZMEDSTS, " +
							"DTEAPP, " +
							"DTETRM, " +
							"ZDESC, " +
							"PAYMTH, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"ZARACDE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     zmedcoy,
                                     zmedprv,
                                     zmednum,
                                     zmednam,
                                     zmedsts,
                                     dteapp,
                                     dtetrm,
                                     zdesc,
                                     paymth,
                                     bankkey,
                                     bankacckey,
                                     zaracde,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		zmedcoy.clear();
  		zmedprv.clear();
  		zmednum.clear();
  		zmednam.clear();
  		zmedsts.clear();
  		dteapp.clear();
  		dtetrm.clear();
  		zdesc.clear();
  		paymth.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		zaracde.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZmpvrec() {
  		return zmpvrec;
	}

	public FixedLengthStringData getZmpvpfRecord() {
  		return zmpvpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZmpvrec(what);
	}

	public void setZmpvrec(Object what) {
  		this.zmpvrec.set(what);
	}

	public void setZmpvpfRecord(Object what) {
  		this.zmpvpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zmpvrec.getLength());
		result.set(zmpvrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}