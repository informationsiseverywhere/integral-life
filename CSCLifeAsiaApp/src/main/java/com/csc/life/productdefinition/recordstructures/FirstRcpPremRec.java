package com.csc.life.productdefinition.recordstructures;

import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class FirstRcpPremRec extends ExternalData  {
	
	private static final long serialVersionUID = 1L;
	
	private ChdrlnbTableDAM chdrlnbIO;
	private int firstpremdate;	
	private FixedLengthStringData batcBatctrcde;
	private ZonedDecimalData occdate;
	private FixedLengthStringData mop;
	private FixedLengthStringData srcebus;
	private FixedLengthStringData onePCashlessFlag;
	private FixedLengthStringData facthouse;
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		// TODO Auto-generated method stub
		return null;
	}

	public ChdrlnbTableDAM getChdrlnbIO() {
		return chdrlnbIO;
	}

	public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
		this.chdrlnbIO = chdrlnbIO;
	}

	public int getFirstpremdate() {
		return firstpremdate;
	}

	public void setFirstpremdate(int firstpremdate) {
		this.firstpremdate = firstpremdate;
	}

	public FixedLengthStringData getBatcBatctrcde() {
		return batcBatctrcde;
	}

	public void setBatcBatctrcde(FixedLengthStringData batcBatctrcde) {
		this.batcBatctrcde = batcBatctrcde;
	}

	public ZonedDecimalData getOccdate() {
		return occdate;
	}

	public void setOccdate(ZonedDecimalData occdate) {
		this.occdate = occdate;
	}

	public FixedLengthStringData getMop() {
		return mop;
	}

	public void setMop(FixedLengthStringData mop) {
		this.mop = mop;
	}

	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}

	public void setSrcebus(FixedLengthStringData srcebus) {
		this.srcebus = srcebus;
	}

	public FixedLengthStringData getOnePCashlessFlag() {
		return onePCashlessFlag;
	}

	public void setOnePCashlessFlag(FixedLengthStringData onePCashlessFlag) {
		this.onePCashlessFlag = onePCashlessFlag;
	}

	public FixedLengthStringData getFacthouse() {
		return facthouse;
	}

	public void setFacthouse(FixedLengthStringData facthouse) {
		this.facthouse = facthouse;
	}
	
}
