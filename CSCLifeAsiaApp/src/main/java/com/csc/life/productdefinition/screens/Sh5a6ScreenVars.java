package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH5A6
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sh5a6ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,16);
	public ZonedDecimalData hpropdte = DD.hpropdte.copyToZonedDecimal().isAPartOf(dataFields,46);
	public ZonedDecimalData hprrcvdt = DD.hprrcvdt.copyToZonedDecimal().isAPartOf(dataFields,54);
	public ZonedDecimalData huwdcdte = DD.huwdcdte.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData jownname = DD.jownname.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData jownnum = DD.jownnum.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,180);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,188);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,235);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData fatcastatus=DD.fatcastatus.copy().isAPartOf(dataFields,290 );
	public FixedLengthStringData fatcastatusdesc=DD.fatcastatusdesc.copy().isAPartOf(dataFields,296);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea,getDataFieldsSize() );
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData hpropdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData hprrcvdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData huwdcdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jownnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData fatcastatusErr=new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] hpropdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] hprrcvdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] huwdcdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jownnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] fatcastatusOut= FLSArrayPartOfStructure(12, 1, outputIndicators,168 );
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData hpropdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hprrcvdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData huwdcdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fatcastatusFlag = new FixedLengthStringData(10);

	public LongData Sh5a6screenWritten = new LongData(0);
	public LongData Sh5a6protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh5a6ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(huwdcdteOut,new String[] {null, "40",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fatcastatusOut,new String[] {"31","90","-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = getscreenFields();
		screenOutFields =getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh5a6screen.class;
		protectRecord = Sh5a6protect.class;
	}
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {occdate, hpropdte, hprrcvdt, huwdcdte};
	}
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {occdateDisp, hpropdteDisp, hprrcvdtDisp, huwdcdteDisp};//SGS001
	}
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {occdateErr, hpropdteErr, hprrcvdtErr, huwdcdteErr};
	}

	public int getDataAreaSize() {
		return 576;
	}
	public int getDataFieldsSize(){
		return 336;
	}
	public int getErrorIndicatorSize(){
		return 60 ; 
	}
	public int getOutputFieldSize(){
		return 180; 
	}
	
	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, cownnum, lifcnum, occdate, hpropdte, hprrcvdt, huwdcdte, ctypedes, ownername, linsname, jlinsname, jlifcnum, jownnum, jownname,fatcastatus};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, cownnumOut, lifcnumOut, occdateOut, hpropdteOut, hprrcvdtOut, huwdcdteOut, ctypedesOut, ownernameOut, linsnameOut, jlinsnameOut, jlifcnumOut, jownnumOut, jownnameOut,fatcastatusOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[]  {chdrnumErr, cownnumErr, lifcnumErr, occdateErr, hpropdteErr, hprrcvdtErr, huwdcdteErr, ctypedesErr, ownernameErr, linsnameErr, jlinsnameErr, jlifcnumErr, jownnumErr, jownnameErr,fatcastatusErr};
		
	}
	
}
