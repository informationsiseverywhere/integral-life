package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.procedures.vpms.VPMSUtils;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Lifformat extends COBOLConvCodeModel {

	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(Lifformat.class);
	
	private String wsaaSubr = "LIFFORMAT";
	private String h835 = "H835";
	private String rflk = "RFLK";
	private Syserrrec syserrrec = new Syserrrec();
	private Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Varcom varcom = new Varcom();
	private Object recordObject;
	private String whatCoypBook = "";
	
	private Map<String, String> CLASS_MAP = new HashMap<String, String>();
	

	public Lifformat() {
		super();
		CLASS_MAP.put("VPMFMTREC", "com.csc.life.productdefinition.recordstructures.Vpmfmtrec");
		CLASS_MAP.put("PREMIUMREC", "com.csc.life.productdefinition.recordstructures.Premiumrec");
		CLASS_MAP.put("MGFEELREC", "com.csc.life.productdefinition.recordstructures.Mgfeelrec");
		CLASS_MAP.put("POLCALCREC", "com.csc.life.productdefinition.recordstructures.Polcalcrec");
		CLASS_MAP.put("BONUSREC", "com.csc.life.regularprocessing.recordstructures.Bonusrec");
		CLASS_MAP.put("DEATHREC", "com.csc.life.terminationclaims.recordstructures.Deathrec");
		CLASS_MAP.put("EXTPRMREC", "com.csc.life.productdefinition.recordstructures.Extprmrec");
		CLASS_MAP.put("HDIVDREC", "com.csc.life.cashdividends.recordstructures.Hdivdrec");
		CLASS_MAP.put("HDVDCWCREC", "com.csc.life.cashdividends.recordstructures.Hdvdcwcrec");
		CLASS_MAP.put("HDVDINTREC", "com.csc.life.cashdividends.recordstructures.Hdvdintrec");
		CLASS_MAP.put("IBINCALREC", "com.csc.life.interestbearing.recordstructures.Ibincalrec");		
		CLASS_MAP.put("INCRSREC", "com.csc.life.regularprocessing.recordstructures.Incrsrec");
		CLASS_MAP.put("MATCCPY", "com.csc.life.terminationclaims.recordstructures.Matccpy");
		CLASS_MAP.put("OVRDUEREC", " com.csc.life.regularprocessing.recordstructures.Ovrduerec");
		CLASS_MAP.put("PR676CPY", "com.csc.life.terminationclaims.recordstructures.Pr676cpy");
		CLASS_MAP.put("PRASREC", "com.csc.life.annuities.recordstructures.Prasrec");
		CLASS_MAP.put("RLRDTRMREC", "com.csc.life.newbusiness.recordstructures.Rlrdtrmrec");
		CLASS_MAP.put("RWCALCPY", "com.csc.life.unitlinkedprocessing.recordstructures.Rwcalcpy");
		CLASS_MAP.put("SRCALCPY", "com.csc.life.terminationclaims.recordstructures.Srcalcpy");
		CLASS_MAP.put("STDTALLREC", "com.csc.life.productdefinition.recordstructures.Stdtallrec");
		CLASS_MAP.put("TAXCPY", "com.csc.life.terminationclaims.tablestructures.Taxcpy");
		CLASS_MAP.put("TSDTCALREC", "com.csc.life.productdefinition.tablestructures.Tsdtcalrec");
		CLASS_MAP.put("UBBLALLPAR", "com.csc.life.regularprocessing.recordstructures.Ubblallpar");		
		CLASS_MAP.put("VCALCPY", "com.csc.life.annuities.recordstructures.Vcalcpy");
		CLASS_MAP.put("VSTAREC", "com.csc.life.annuities.recordstructures.Vstarec");
		CLASS_MAP.put("VSTCCPY", "com.csc.life.annuities.recordstructures.Vstccpy");
		CLASS_MAP.put("ACTCALCREC", "com.csc.life.underwriting.recordstructures.Actcalcrec");
		CLASS_MAP.put("INTCALCREC", "com.csc.life.contractservicing.recordstructures.Intcalcrec");		
		CLASS_MAP.put("VPXMATCREC", "com.csc.life.productdefinition.recordstructures.Vpxmatcrec");		
	}

	public void mainline(Object... parmArray){
		vpmfmtrec = (Vpmfmtrec)parmArray[1];
		
		//Get copy-book
		whatCoypBook = vpmfmtrec.copybook.toUpperCase().trim();				
		//Second parameter is FixedLengthStringData -- vpmcalcrec.vpmcalcRec
		if(parmArray[0] instanceof FixedLengthStringData){
			vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
						
			Class whatCoypBookClass = null;
			String whatCoypBookPath = null;
			
			whatCoypBookPath = CLASS_MAP.get(whatCoypBook);
			
			try {				
				whatCoypBookClass = ClassUtils.getClass(whatCoypBookPath); // IJTI-713								
			} catch (ClassNotFoundException e) {
				LOGGER.error("Unable to load program {}", whatCoypBookPath);//IJTI-1561
				return;
			}
						
			try {
				if(!whatCoypBook.trim().equals("VPMFMTREC")){ /* IJTI-1479 */					
					recordObject = whatCoypBookClass.newInstance();
					java.lang.reflect.Method method;
					method = recordObject.getClass().getMethod("set",FixedLengthStringData.class);
					method.invoke(recordObject, vpmcalcrec.linkageArea);
				}
			} catch (Exception e) {		
				LOGGER.error("Cannot instantiate {}", whatCoypBookPath);//IJTI-1561
				return;
			}		
		//Second parameter is copy-book object	
		} else if(!(parmArray[0] instanceof Vpmfmtrec)){
			recordObject = parmArray[0];
		}
		
		para100();	
		//Return linkage to calling method
		vpmcalcrec.linkageArea.set(((ExternalData)recordObject).getBaseString());
	}
		
	private void para100(){
		vpmfmtrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		if (isNE(vpmfmtrec.function,"INIT")
			&& isNE(vpmfmtrec.function, "GETV")
			&& isNE(vpmfmtrec.function, "UPDV")){
			syserrrec.statuz.set(h835);
			return;
		}
		
		if (isEQ(vpmfmtrec.function,"INIT")) {			
			initialize(vpmfmtrec.commonArea);
		}else if ("GETV".equalsIgnoreCase(vpmfmtrec.function.getData().trim())) { /* IJTI-1479 */
			returnValue();
		}else if ("UPDV".equalsIgnoreCase(vpmfmtrec.function.getData().trim())) { /* IJTI-1479 */
			updateValue();
		}
	}

	//'GETV' function
	private void returnValue(){
		String fieldName = "";
		if(whatCoypBook.trim().equals("*CONSTANT")){ /* IJTI-1479 */
			vpmfmtrec.resultValue.set(vpmfmtrec.resultField);
		}else if(whatCoypBook.trim().equals("VPMFMTREC")){ /* IJTI-1479 */
			fieldName = vpmfmtrec.resultField.getData().trim(); /* IJTI-1479 */
			if(fieldName.equals("extkey")){
				vpmfmtrec.resultValue.set(vpmcalcrec.extkey);
			}else if(fieldName!=null && fieldName.length()>0){
				Object fieldValue = VPMSUtils.getRecordField(vpmfmtrec, fieldName);
				vpmfmtrec.resultValue.set(VPMSUtils.returnValue(fieldValue));
			}
		}else{
			if(checkCopybookSupported()){
				fieldName = vpmfmtrec.resultField.getData().trim(); /* IJTI-1479 */
				Object value = VPMSUtils.getRecordField(recordObject, fieldName);
				vpmfmtrec.resultValue.set(VPMSUtils.returnValue(value));
			}
		}
	}
	
	//'UPDV' function
	private void updateValue(){
		String fieldName = vpmfmtrec.resultField.getData().trim(); /* IJTI-1479 */
		if(whatCoypBook.trim().equals("VPMFMTREC")){ /* IJTI-1479 */
			VPMSUtils.updateRecordField(vpmfmtrec, fieldName, vpmfmtrec.resultValue);
		}else{			
			if(checkCopybookSupported())
				VPMSUtils.updateRecordField(recordObject, fieldName, vpmfmtrec.resultValue);
		}
	}
	
	private boolean checkCopybookSupported(){
		if (!VPMSUtils.SUPPORTED_RECORDS.contains(vpmfmtrec.copybook)){
			vpmfmtrec.statuz.set(rflk);
			return false;
		}
		if (!VPMSUtils.checkFieldExist(recordObject, vpmfmtrec.resultField.getData().trim())){ /* IJTI-1479 */
			vpmfmtrec.statuz.set(h835);
			return false;
		}
		return true;
	}
	
}


