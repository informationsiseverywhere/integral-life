package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5646screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5646ScreenVars sv = (S5646ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5646screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5646ScreenVars screenVars = (S5646ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.agelimit.setClassString("");
		screenVars.ageIssageFrm01.setClassString("");
		screenVars.ageIssageTo01.setClassString("");
		screenVars.factorsa01.setClassString("");
		screenVars.ageIssageFrm02.setClassString("");
		screenVars.ageIssageTo02.setClassString("");
		screenVars.factorsa02.setClassString("");
		screenVars.ageIssageFrm03.setClassString("");
		screenVars.ageIssageTo03.setClassString("");
		screenVars.factorsa03.setClassString("");
		screenVars.ageIssageFrm04.setClassString("");
		screenVars.ageIssageTo04.setClassString("");
		screenVars.factorsa04.setClassString("");
		screenVars.ageIssageFrm05.setClassString("");
		screenVars.ageIssageTo05.setClassString("");
		screenVars.factorsa05.setClassString("");
		screenVars.ageIssageFrm06.setClassString("");
		screenVars.ageIssageTo06.setClassString("");
		screenVars.factorsa06.setClassString("");
		screenVars.ageIssageFrm07.setClassString("");
		screenVars.ageIssageTo07.setClassString("");
		screenVars.factorsa07.setClassString("");
		screenVars.ageIssageFrm08.setClassString("");
		screenVars.ageIssageTo08.setClassString("");
		screenVars.factorsa08.setClassString("");
		screenVars.ageIssageFrm09.setClassString("");
		screenVars.ageIssageTo09.setClassString("");
		screenVars.factorsa09.setClassString("");
		screenVars.ageIssageFrm10.setClassString("");
		screenVars.ageIssageTo10.setClassString("");
		screenVars.factorsa10.setClassString("");
		screenVars.ageIssageFrm11.setClassString("");
		screenVars.ageIssageTo11.setClassString("");
		screenVars.factorsa11.setClassString("");
		screenVars.ageIssageFrm12.setClassString("");
		screenVars.ageIssageTo12.setClassString("");
		screenVars.factorsa12.setClassString("");
	}

/**
 * Clear all the variables in S5646screen
 */
	public static void clear(VarModel pv) {
		S5646ScreenVars screenVars = (S5646ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.agelimit.clear();
		screenVars.ageIssageFrm01.clear();
		screenVars.ageIssageTo01.clear();
		screenVars.factorsa01.clear();
		screenVars.ageIssageFrm02.clear();
		screenVars.ageIssageTo02.clear();
		screenVars.factorsa02.clear();
		screenVars.ageIssageFrm03.clear();
		screenVars.ageIssageTo03.clear();
		screenVars.factorsa03.clear();
		screenVars.ageIssageFrm04.clear();
		screenVars.ageIssageTo04.clear();
		screenVars.factorsa04.clear();
		screenVars.ageIssageFrm05.clear();
		screenVars.ageIssageTo05.clear();
		screenVars.factorsa05.clear();
		screenVars.ageIssageFrm06.clear();
		screenVars.ageIssageTo06.clear();
		screenVars.factorsa06.clear();
		screenVars.ageIssageFrm07.clear();
		screenVars.ageIssageTo07.clear();
		screenVars.factorsa07.clear();
		screenVars.ageIssageFrm08.clear();
		screenVars.ageIssageTo08.clear();
		screenVars.factorsa08.clear();
		screenVars.ageIssageFrm09.clear();
		screenVars.ageIssageTo09.clear();
		screenVars.factorsa09.clear();
		screenVars.ageIssageFrm10.clear();
		screenVars.ageIssageTo10.clear();
		screenVars.factorsa10.clear();
		screenVars.ageIssageFrm11.clear();
		screenVars.ageIssageTo11.clear();
		screenVars.factorsa11.clear();
		screenVars.ageIssageFrm12.clear();
		screenVars.ageIssageTo12.clear();
		screenVars.factorsa12.clear();
	}
}
