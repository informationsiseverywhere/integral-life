package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:56
 * Description:
 * Copybook name: T6654REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6654rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6654Rec = new FixedLengthStringData(getRecSize());
  	public FixedLengthStringData arrearsMethod = new FixedLengthStringData(4).isAPartOf(t6654Rec, 0);
  	public FixedLengthStringData collectsub = new FixedLengthStringData(8).isAPartOf(t6654Rec, 4);
  	public FixedLengthStringData daexpys = new FixedLengthStringData(9).isAPartOf(t6654Rec, 12);
  	public ZonedDecimalData[] daexpy = ZDArrayPartOfStructure(3, 3, 0, daexpys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(9).isAPartOf(daexpys, 0, FILLER_REDEFINE);
  	public ZonedDecimalData daexpy01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData daexpy02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData daexpy03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public FixedLengthStringData doctids = new FixedLengthStringData(32).isAPartOf(t6654Rec, 21);
  	public FixedLengthStringData[] doctid = FLSArrayPartOfStructure(4, 8, doctids, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(doctids, 0, FILLER_REDEFINE);
  	public FixedLengthStringData doctid01 = new FixedLengthStringData(8).isAPartOf(filler1, 0);
  	public FixedLengthStringData doctid02 = new FixedLengthStringData(8).isAPartOf(filler1, 8);
  	public FixedLengthStringData doctid03 = new FixedLengthStringData(8).isAPartOf(filler1, 16);
  	public FixedLengthStringData doctid04 = new FixedLengthStringData(8).isAPartOf(filler1, 24);
  	public ZonedDecimalData leadDays = new ZonedDecimalData(3, 0).isAPartOf(t6654Rec, 53);
  	public ZonedDecimalData nonForfietureDays = new ZonedDecimalData(3, 0).isAPartOf(t6654Rec, 56);
  	public FixedLengthStringData renwdates = new FixedLengthStringData(2).isAPartOf(t6654Rec, 59);
  	public FixedLengthStringData[] renwdate = FLSArrayPartOfStructure(2, 1, renwdates, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(renwdates, 0, FILLER_REDEFINE);
  	public FixedLengthStringData renwdate1 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
  	public FixedLengthStringData renwdate2 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
  	public FixedLengthStringData zrfreqs = new FixedLengthStringData(12).isAPartOf(t6654Rec, 61);
  	public FixedLengthStringData[] zrfreq = FLSArrayPartOfStructure(6, 2, zrfreqs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(zrfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zrfreq01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData zrfreq02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData zrfreq03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData zrfreq04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData zrfreq05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData zrfreq06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData zrincrs = new FixedLengthStringData(12).isAPartOf(t6654Rec, 73);
  	public ZonedDecimalData[] zrincr = ZDArrayPartOfStructure(6, 2, 0, zrincrs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(12).isAPartOf(zrincrs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrincr01 = new ZonedDecimalData(2, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData zrincr02 = new ZonedDecimalData(2, 0).isAPartOf(filler4, 2);
  	public ZonedDecimalData zrincr03 = new ZonedDecimalData(2, 0).isAPartOf(filler4, 4);
  	public ZonedDecimalData zrincr04 = new ZonedDecimalData(2, 0).isAPartOf(filler4, 6);
  	public ZonedDecimalData zrincr05 = new ZonedDecimalData(2, 0).isAPartOf(filler4, 8);
  	public ZonedDecimalData zrincr06 = new ZonedDecimalData(2, 0).isAPartOf(filler4, 10);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(415).isAPartOf(t6654Rec, 85, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6654Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6654Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getRecSize() {
		
		return 500 ;
	}
	

}