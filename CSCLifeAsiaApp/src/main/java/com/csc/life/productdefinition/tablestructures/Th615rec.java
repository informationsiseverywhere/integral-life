package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:38
 * Description:
 * Copybook name: TH615REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th615rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th615Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData premind = new FixedLengthStringData(1).isAPartOf(th615Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(499).isAPartOf(th615Rec, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th615Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th615Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}