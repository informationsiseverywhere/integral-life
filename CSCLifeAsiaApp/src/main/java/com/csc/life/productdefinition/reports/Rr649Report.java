package com.csc.life.productdefinition.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR649.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr649Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData desc = new FixedLengthStringData(50);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private FixedLengthStringData exmcode = new FixedLengthStringData(10);
	private FixedLengthStringData invref = new FixedLengthStringData(15);
	private FixedLengthStringData name = new FixedLengthStringData(30);
	private ZonedDecimalData nofmbr = new ZonedDecimalData(7, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData zmedfee = new ZonedDecimalData(17, 2);
	private FixedLengthStringData zmedtyp = new FixedLengthStringData(8);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr649Report() {
		super();
	}


	/**
	 * Print the XML for Rr649d01
	 */
	public void printRr649d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		nofmbr.setFieldName("nofmbr");
		nofmbr.setInternal(subString(recordData, 1, 7));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 8, 8));
		zmedtyp.setFieldName("zmedtyp");
		zmedtyp.setInternal(subString(recordData, 16, 8));
		exmcode.setFieldName("exmcode");
		exmcode.setInternal(subString(recordData, 24, 10));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 34, 10));
		invref.setFieldName("invref");
		invref.setInternal(subString(recordData, 44, 15));
		zmedfee.setFieldName("zmedfee");
		zmedfee.setInternal(subString(recordData, 59, 17));
		name.setFieldName("name");
		name.setInternal(subString(recordData, 76, 30));
		desc.setFieldName("desc");
		desc.setInternal(subString(recordData, 106, 50));
		printLayout("Rr649d01",			// Record name
			new BaseData[]{			// Fields:
				nofmbr,
				chdrnum,
				zmedtyp,
				exmcode,
				effdate,
				invref,
				zmedfee,
				name,
				desc
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr649e01
	 */
	public void printRr649e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rr649e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr649h01
	 */
	public void printRr649h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.set(3);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 11, 10));
		time.setFieldName("time");
		time.set(getTime());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 21, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 22, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr649h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				repdate,
				time,
				company,
				companynm,
				pagnbr
			}
		);

		currentPrintLine.add(6);
	}

	/**
	 * Print the XML for Rr649t01
	 */
	public void printRr649t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zmedfee.setFieldName("zmedfee");
		zmedfee.setInternal(subString(recordData, 1, 17));
		printLayout("Rr649t01",			// Record name
			new BaseData[]{			// Fields:
				zmedfee
			}
		);

		currentPrintLine.add(3);
	}


}
