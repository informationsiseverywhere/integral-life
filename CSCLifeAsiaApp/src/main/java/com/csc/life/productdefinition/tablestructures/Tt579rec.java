package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:15
 * Description:
 * Copybook name: TT579REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt579rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt579Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData amta = new ZonedDecimalData(17, 2).isAPartOf(tt579Rec, 0);
  	public FixedLengthStringData crtables = new FixedLengthStringData(60).isAPartOf(tt579Rec, 17);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(15, 4, crtables, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData crtable11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData crtable12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData crtable13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData crtable14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData crtable15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(423).isAPartOf(tt579Rec, 77, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tt579Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt579Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}