/****************************************/
/*Author  :Liwei				  		*/
/*Purpose :Instead of subroutine Lifrtrn*/
/*Date    :2018.10.26					*/
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.dao.DglxpfDAO;
import com.csc.fsu.accounting.dataaccess.dao.RtrnpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.accounting.dataaccess.model.Dglxpf;
import com.csc.fsu.accounting.tablestructures.Tr362rec;
import com.csc.fsu.general.procedures.PostyymmPojo;
import com.csc.fsu.general.procedures.PostyymmUtils;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.smart.procedures.BatcupPojo;
import com.csc.smart.procedures.BatcupUtils;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Rtrnpf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class LifrtrnUtilsImpl implements LifrtrnUtils {
private static final Logger LOGGER = LoggerFactory.getLogger(LifrtrnUtilsImpl.class);
/**
* <pre>
*REMARKS.
*         Life system version of Subsidary Ledger Posting
*
* This  subroutine  updates  the  batch totals then writes an
* RTRN record. Depending on the calling function sub-accounts
* balance can also be posted to.
*
*  Functions:
*
*     PSTW  - Write the RTRN and update the SACS balance.
*     NPSTW - Write the RTRN record only.
*
*  Both functions update the running batch header totals kept
*  by calling BATCUP with a KEEPS function.
*
*  On completion of the transaction, BATCUP must be called with
*  a WRITS function to apply the accumulated totals to the
*  database.
*
*  Statuses:
*
*     **** - successful completion of routine
*     BOMB - database error
*     E049 - Invalid linkage into subroutine
*
*  Linkage Area:
*
*  LIFR-BATCKEY   The key of the opened batch.
*  LIFR-RDOCNUM   Document number for this record. Varies
*                 with the calling subsystem.
*  LIFA-TRANNO    This is the sequence number of the transaction
*                 being done to the document
*  LIFA-JRNSEQ    This is the sequence number user to uniquely
*                 identify a specific manual journal posting.
*  LIFR-RLDGCOY   Ledger kay for this record. Varies with
*  LIFR_RLDGACCT  the calling subsystem.
*  LIFR-GENLCOY   General ledger key. Set up from T5645 prior
*  LIFR_GLCODE    to calling this routine.
*  LIFR-GLSIGN
*  LIFR-CONTOT
*  LIFR-SACSCODE
*  LIFR-SACSTYP
*  LIFR-TRANREF   Internal reference number (e.g. secondary
*                 key). Varies with the calling subsystem.
*  LIFR-TRANDESC  Meaningful description of tranaction.
*  LIFR-ORIGCURR  Original (foreign) currency.
*  LIFR-ACCTCCY   Accounting (local) currency.
*  LIFR-CRATE     Exchange rate at time of transaction between
*                 original and accounting currency.
*  LIFR-EFFDATE   The effective date of the transaction.
*  LIFR-ORIGAMT   The transaction amount in original currency.
*  LIFR-ACCTAMT   The transaction amount in accounting currency.
*  LIFR-SUBSTITUTE-CODES
*                 Used to substitute in GL code if passed
*                     1 - @@@ - Contract type
*                     2 - ***
*                     3 - %%%
*                     4 - !!!
*                     5 - +++
*  "LIFR-TRANID"  Date/time stamp
*
/
***********************************************************************
*                                                                     *
* </pre>
*/
	public static final String ROUTINE = QPUtilities.getThisClass();
	private String wsaaProg = "LIFRTRN";
	private String wsaaToday = "";
		/* WSAA-TRANSACTION-DATE */
	private ZonedDecimalData wsaaTransDate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaTranTestDate = new FixedLengthStringData(8).isAPartOf(wsaaTransDate, 0, REDEFINE);
	private ZonedDecimalData wsaaTranDateCc = new ZonedDecimalData(2, 0).isAPartOf(wsaaTranTestDate, 0).setUnsigned();
	private ZonedDecimalData wsaaTranDateYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaTranTestDate, 2).setUnsigned();

	private String wsaaLastOrigCcy = "";
	private String wsaaLedgerCcy = "";
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();
	private T3629rec t3629rec = new T3629rec();
	private Tr362rec tr362rec = new Tr362rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Varcom varcom = new Varcom();
	protected Syserrrec syserrrec = new Syserrrec();
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DglxpfDAO dglxpfDAO = getApplicationContext().getBean("dglxpfDAO",DglxpfDAO.class); 
	private AcblpfDAO acblDAO = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); 
	private List<Itempf> itempfList = null;
	private RtrnpfDAO rtrnpfDAO =  getApplicationContext().getBean("rtrnpfDAO", RtrnpfDAO.class);
	private Rtrnpf rtrnpf = null;
	private Acblpf acblpf = null;
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private PostyymmUtils postyymmUtils = getApplicationContext().getBean("postyymmUtils", PostyymmUtils.class);
	private PostyymmPojo postyymmPojo;
	private BatcupUtils batcupUtils = getApplicationContext().getBean("batcupUtils", BatcupUtils.class);
	 
	private List<Acblpf> acblRtrnpfList;
	
	public void calcLitrtrn(LifrtrnPojo lifrtrnPojo) {
		lifrtrnPojo.setStatuz("****");
		syserrrec.subrname.set(wsaaProg);
		if (!"PSTW".equals(lifrtrnPojo.getFunction())&& !"NPSTW".equals(lifrtrnPojo.getFunction())) {
			lifrtrnPojo.setStatuz("E409");
			return ;
		}
		checkSchedule24x7Setup4000(lifrtrnPojo);
		writeRtrn1000(lifrtrnPojo);
		if ("PSTW".equals(lifrtrnPojo.getFunction()) && !"".equals(lifrtrnPojo.getSacscode())) {
			updateAcbl2000(lifrtrnPojo);
		}
		callBatcup3000(lifrtrnPojo);
	}
	
	public List<Acblpf> calcLitrtrnAcbl(LifrtrnPojo lifrtrnPojo) {
		acblRtrnpfList = new ArrayList<Acblpf>();
		lifrtrnPojo.setStatuz("****");
		syserrrec.subrname.set(wsaaProg);
		if (!"PSTW".equals(lifrtrnPojo.getFunction())&& !"NPSTW".equals(lifrtrnPojo.getFunction())) {
			lifrtrnPojo.setStatuz("E409");
			return acblRtrnpfList;
		}
		checkSchedule24x7Setup4000(lifrtrnPojo);
		writeRtrn1000(lifrtrnPojo);
		if ("PSTW".equals(lifrtrnPojo.getFunction()) && !"".equals(lifrtrnPojo.getSacscode())) {

			acblpf = new Acblpf();
			acblpf.setRldgcoy(lifrtrnPojo.getRldgcoy());
			acblpf.setSacscode(lifrtrnPojo.getSacscode());
			acblpf.setRldgacct(lifrtrnPojo.getRldgacct());
			acblpf.setSacstyp(lifrtrnPojo.getSacstyp());
			acblpf.setOrigcurr(lifrtrnPojo.getOrigcurr());
			acblpf.setSacscurbal(BigDecimal.ZERO);

			if ("-".equals(lifrtrnPojo.getGlsign())) {
				setPrecision(acblpf.getSacscurbal(), 2);
				acblpf.setSacscurbal(BigDecimal.ZERO.subtract(lifrtrnPojo.getOrigamt()));
			}
			else {
				setPrecision(acblpf.getSacscurbal(), 2);
				if (!(lifrtrnPojo.getOrigamt() == null)) 
					acblpf.setSacscurbal(lifrtrnPojo.getOrigamt());
			}
			zrdecplcPojo.setAmountIn(acblpf.getSacscurbal());
			zrdecplcPojo.setCurrency(acblpf.getOrigcurr());
			a000CallRounding(lifrtrnPojo);
			acblpf.setSacscurbal(new BigDecimal(zrdecplcPojo.getAmountOut().toString().replace("}", "")));
			
			acblRtrnpfList.add(acblpf);			
		}
		callBatcup3000(lifrtrnPojo);
		return acblRtrnpfList;
	}	
	
	public void checkSchedule24x7Setup4000(LifrtrnPojo lifrtrnPojo){
		itempfList = itemDao.getAllItemitem("IT",lifrtrnPojo.getBatccoy(), "TR362","***");
		if (itempfList != null && itempfList.size()> 0 ) {
			tr362rec.tr362Rec.set((StringUtil.rawToString(itempfList.get(0).getGenarea())));
		}
		else {
			tr362rec.tr362Rec.set(SPACES);
		}
	}
	
	public void writeRtrn1000(LifrtrnPojo lifrtrnPojo){
		glSubstitution1011(lifrtrnPojo);
		/*  If accounting currency details not entered,*/
		/*   and the original currency has changed since the last time*/
		/*     - look up the nominal exchange rate.*/
		if ((lifrtrnPojo.getGenlcur() == null || "".equals(lifrtrnPojo.getGenlcur())) && isNE(lifrtrnPojo.getOrigcurr(),wsaaLastOrigCcy)) {
			getRate1100(lifrtrnPojo,lifrtrnPojo.getOrigcurr());
		}

		/*  Write movement record.*/
		rtrnpf = new Rtrnpf();
		rtrnpf.setBatcpfx("BA");
		rtrnpf.setBatccoy(lifrtrnPojo.getBatccoy());
		rtrnpf.setBatcbrn(lifrtrnPojo.getBatcbrn());
		rtrnpf.setBatcactyr(lifrtrnPojo.getBatcactyr());
		rtrnpf.setBatcactmn(lifrtrnPojo.getBatcactmn());
		rtrnpf.setBatctrcde(lifrtrnPojo.getBatctrcde());
		rtrnpf.setBatcbatch(lifrtrnPojo.getBatcbatch());
		rtrnpf.setRdocnum(lifrtrnPojo.getRdocnum());
		rtrnpf.setTranseq(String.valueOf(lifrtrnPojo.getJrnseq()));
		rtrnpf.setRdocpfx("CA");
		rtrnpf.setRdoccoy(lifrtrnPojo.getBatccoy());
		rtrnpf.setRdocnum(lifrtrnPojo.getRdocnum());
		if ("LP".equals(lifrtrnPojo.getSacscode())) {
			rtrnpf.setRdocpfx("CH");
		}
		if ("SC".equals(lifrtrnPojo.getSacscode())) {
			rtrnpf.setRdocpfx("SC");
		}
		rtrnpf.setRldgcoy(lifrtrnPojo.getRldgcoy());
		rtrnpf.setSacscode(lifrtrnPojo.getSacscode());
		rtrnpf.setRldgacct(lifrtrnPojo.getRldgacct());
		rtrnpf.setOrigccy(lifrtrnPojo.getOrigcurr());
		rtrnpf.setSacstyp(lifrtrnPojo.getSacstyp());
		rtrnpf.setOrigamt(lifrtrnPojo.getOrigamt());
		rtrnpf.setTrandesc(lifrtrnPojo.getTrandesc());
		rtrnpf.setCrate(lifrtrnPojo.getCrate());
		rtrnpf.setAcctamt(lifrtrnPojo.getAcctamt());
		rtrnpf.setGenlpfx("GE");
		rtrnpf.setGenlcoy(lifrtrnPojo.getGenlcoy());
		rtrnpf.setGenlcur(lifrtrnPojo.getGenlcur());
		rtrnpf.setGlcode(lifrtrnPojo.getGlcode());
		rtrnpf.setGlsign(lifrtrnPojo.getGlsign());
		postyymmPojo = new PostyymmPojo();
		postyymmPojo.setFunction("GET");
		postyymmPojo.setBatccoy(lifrtrnPojo.getBatccoy());
		postyymmPojo.setBatcbrn(lifrtrnPojo.getBatcbrn());
		postyymmPojo.setBatcactyr(lifrtrnPojo.getBatcactyr());
		postyymmPojo.setBatcactmn(lifrtrnPojo.getBatcactmn());
		postyymmPojo.setBatctrcde(lifrtrnPojo.getBatctrcde());
		postyymmPojo.setBatcbatch(lifrtrnPojo.getBatcbatch());
		postyymmPojo.setEffdate(lifrtrnPojo.getEffdate());
		postyymmPojo.setStatuz(varcom.oK.toString());
		postyymmUtils.callPostyymm(postyymmPojo);
		
		
		if (Objects.nonNull(postyymmPojo) && 
				(Objects.nonNull(postyymmPojo.getStatuz())
						&& (!varcom.oK.toString().equals(postyymmPojo.getStatuz())))) {
			LOGGER.error("postyymmPojo.getStatuz() - {}", postyymmPojo.getStatuz());
			syserrrec.params.set(postyymmPojo);
			syserrrec.statuz.set(postyymmPojo.getStatuz());
			syserr570();				
		}
		
		rtrnpf.setPostyear(postyymmPojo.getPostyear());
		rtrnpf.setPostmonth(postyymmPojo.getPostmonth());
		//PINNACLE-2372
		if(rtrnpf.getPostmonth().length()<2)
		{
			rtrnpf.setPostmonth(String.format("%2s", rtrnpf.getPostmonth()).replace(' ', '0'));
		}
		rtrnpf.setEffdate(lifrtrnPojo.getEffdate());
		rtrnpf.setTranno(lifrtrnPojo.getTranno());
		if ("".equals(wsaaToday)) {
			datcon1rec.function.set("TDAY");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaTransDate.set(datcon1rec.intDate);
			wsaaToday = datcon1rec.intDate.toString();
		}
		else {
			wsaaTransDate.set(wsaaToday);
		}
		if (isEQ(wsaaTranDateCc,ZERO)) {
			if (isGT(wsaaTranDateYy,90)) {
				wsaaTranDateCc.set(19);
			}
			else {
				wsaaTranDateCc.set(20);
			}
		}
		rtrnpf.setTrandate(wsaaTransDate.toInt());
		//rtrnpf.setTrantime(Integer.parseInt(getCobolTime()));  //ILIFE-7901 by dpuhawan
		rtrnpf.setTrantime(Integer.parseInt(getCobolTime().substring(0, 6))); //ILIFE-7901 by dpuhawan
		if (lifrtrnPojo.getGenlcur() == null || "".equals(lifrtrnPojo.getGenlcur())) {
			rtrnpf.setCrate(new BigDecimal(wsaaNominalRate.toString()));
			rtrnpf.setGenlcur(wsaaLedgerCcy);
			/*  Do not forget to round the ACCTAMT*/
			setPrecision(rtrnpf.getAcctamt(), 10);
			rtrnpf.setAcctamt(lifrtrnPojo.getOrigamt().multiply(new BigDecimal(wsaaNominalRate.toString())));
			zrdecplcPojo.setAmountIn(rtrnpf.getAcctamt());
			zrdecplcPojo.setCurrency(rtrnpf.getGenlcur());
			a000CallRounding(lifrtrnPojo);
			rtrnpf.setAcctamt(zrdecplcPojo.getAmountOut());
		}
		zrdecplcPojo.setAmountIn(rtrnpf.getOrigamt());
		zrdecplcPojo.setCurrency(lifrtrnPojo.getOrigcurr());
		a000CallRounding(lifrtrnPojo);
		rtrnpf.setOrigamt(zrdecplcPojo.getAmountOut());

		if ( "".equals(lifrtrnPojo.getPostmonth())
				&& (!(rtrnpf.getBatcactmn() == null))) {
				rtrnpf.setPostmonth(rtrnpf.getBatcactmn().toString());
				//PINNACLE-2372
			  if(rtrnpf.getPostmonth().length()<2) {
				 rtrnpf.setPostmonth(String.format("%2s", rtrnpf.getPostmonth()).replace(' ',  '0'));
			  }
			 
		}
		if ("".equals(lifrtrnPojo.getPostyear())
				&& (!(rtrnpf.getBatcactyr() == null))) {
				rtrnpf.setPostyear(rtrnpf.getBatcactyr().toString());
		}
		rtrnpfDAO.insertRtrnRecord(rtrnpf);
		if (isEQ(tr362rec.appflag, "Y")) {
			diaryUpdate5000(rtrnpf);
		}
	}
	
	public void glSubstitution1011(LifrtrnPojo lifrtrnPojo){
		lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "?", lifrtrnPojo.getBatccoy()));
		lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "##", lifrtrnPojo.getBatcbrn()));
		lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "&&&", lifrtrnPojo.getOrigcurr()));
		if (lifrtrnPojo.getSubstituteCodes()[0] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[0])) {
			lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "@@@", lifrtrnPojo.getSubstituteCodes()[0]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[1] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[1])) {
			lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "***", lifrtrnPojo.getSubstituteCodes()[1]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[2] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[2])) {
			lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "%%%", lifrtrnPojo.getSubstituteCodes()[2]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[3] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[3])) {
			lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "!!!", lifrtrnPojo.getSubstituteCodes()[3]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[4] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[4])) {
			lifrtrnPojo.setGlcode(inspectReplaceAll(lifrtrnPojo.getGlcode(), "+++", lifrtrnPojo.getSubstituteCodes()[4]));
		}

	}
	
	public void getRate1100(LifrtrnPojo lifrtrnPojo, String itemkey){
		Itempf item3629 = readCurrencyRecord1110(itemkey,lifrtrnPojo.getBatccoy());
		if(item3629 != null){
			t3629rec.t3629Rec.set(StringUtil.rawToString(item3629.getGenarea()));
			wsaaLastOrigCcy = lifrtrnPojo.getOrigcurr();
			wsaaLedgerCcy = t3629rec.ledgcurr.toString();
			for(int i = 1; (i <=7 || isEQ(wsaaNominalRate,ZERO)); i++ ){
				if (isGTE(lifrtrnPojo.getEffdate(),t3629rec.frmdate[i])
						&& isLTE(lifrtrnPojo.getEffdate(),t3629rec.todate[i])) {
							wsaaNominalRate.set(t3629rec.scrate[i]);
							break;
						}
			}
			if (isEQ(wsaaNominalRate,ZERO)) {
				if (isNE(t3629rec.contitem,SPACES)) {
					item3629 = readCurrencyRecord1110(t3629rec.contitem.toString(),lifrtrnPojo.getBatccoy());
					getRate1100(lifrtrnPojo,t3629rec.contitem.toString());
				}
				else {
					syserrrec.statuz.set(varcom.mrnf);
					syserr570();
				}
			}
		}
	}
	protected Itempf readCurrencyRecord1110(String itemitem,String coy){
		itempfList = itemDao.getAllItemitem("IT",coy, "T3629",itemitem);
		if (itempfList != null && itempfList.size()> 0 ) {
			return itempfList.get(0);
		}
		return null;
	}
	
	
	protected void a000CallRounding(LifrtrnPojo lifrtrnPojo) {
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setStatuz(varcom.oK.toString());
		zrdecplcPojo.setCompany(lifrtrnPojo.getBatccoy());
		zrdecplcPojo.setBatctrcde(lifrtrnPojo.getBatctrcde());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			syserr570();
		}
	}
	protected void syserr570(){
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		Syserr syserr = new Syserr();
		syserr.mainline(new Object[] { syserrrec.syserrRec });
		/*EXIT*/
		LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
		throw new RuntimeException(new COBOLExitProgramException());
	}
	protected void diaryUpdate5000(Rtrnpf rtrnpf)
	{
		if (rtrnpf.getOrigamt().compareTo(BigDecimal.ZERO) == 0
		&& rtrnpf.getAcctamt().compareTo(BigDecimal.ZERO) == 0) {
			return ;
		}
		if ("".equals(rtrnpf.getGlcode())) {
			return ;
		}
		Dglxpf dglxpf = new Dglxpf();
		dglxpf.setBatccoy(rtrnpf.getBatccoy());
		dglxpf.setBatcbrn(rtrnpf.getBatcbrn());
		dglxpf.setBatcactyr(rtrnpf.getBatcactyr());
		dglxpf.setBatcactmn(rtrnpf.getBatcactmn());
		dglxpf.setBatctrcde(rtrnpf.getBatctrcde());
		dglxpf.setBatcbatch(rtrnpf.getBatcbatch());
		dglxpf.setGenlcoy(rtrnpf.getGenlcoy());
		dglxpf.setGenlcur(rtrnpf.getGenlcur());
		dglxpf.setGenlcde(rtrnpf.getGlcode());
		dglxpf.setEffdate(rtrnpf.getEffdate());
		dglxpf.setOrigcurr(rtrnpf.getOrigccy());
		dglxpf.setPostyear(rtrnpf.getPostyear());
		dglxpf.setPostmonth(rtrnpf.getPostmonth());
		dglxpf.setRldgacct(rtrnpf.getRldgacct());
		if (isEQ(rtrnpf.getGlsign(), "-")) {
			setPrecision(dglxpf.getOrigamt(), 2);
			dglxpf.setOrigamt(new BigDecimal(sub(0, rtrnpf.getOrigamt()).toString()));
			setPrecision(dglxpf.getAcctamt(), 2);
			dglxpf.setAcctamt(new BigDecimal(sub(0, rtrnpf.getAcctamt()).toString()));
		}
		else {
			dglxpf.setOrigamt(rtrnpf.getOrigamt());
			dglxpf.setAcctamt(rtrnpf.getAcctamt());
		}
		dglxpf.setCrtdatime(new Timestamp(System.currentTimeMillis()).toString());
		dglxpfDAO.insertDglxRecord(dglxpf);
	}
	protected void updateAcbl2000(LifrtrnPojo lifrtrnPojo){
		acblpf = acblDAO.getAcblpfRecord(lifrtrnPojo.getRldgcoy(), lifrtrnPojo.getSacscode(), lifrtrnPojo.getRldgacct(),lifrtrnPojo.getSacstyp(),lifrtrnPojo.getOrigcurr() );
		if (acblpf == null) {
			acblpf = new Acblpf();
			acblpf.setRldgcoy(lifrtrnPojo.getRldgcoy());
			acblpf.setSacscode(lifrtrnPojo.getSacscode());
			acblpf.setRldgacct(lifrtrnPojo.getRldgacct());
			acblpf.setSacstyp(lifrtrnPojo.getSacstyp());
			acblpf.setOrigcurr(lifrtrnPojo.getOrigcurr());
			acblpf.setSacscurbal(BigDecimal.ZERO);
		}
		if ("-".equals(lifrtrnPojo.getGlsign())) {
			setPrecision(acblpf.getSacscurbal(), 2);
			acblpf.setSacscurbal(acblpf.getSacscurbal().subtract(lifrtrnPojo.getOrigamt()));
		}
		else {
			setPrecision(acblpf.getSacscurbal(), 2);
			acblpf.setSacscurbal(acblpf.getSacscurbal().add(lifrtrnPojo.getOrigamt()));
		}
		zrdecplcPojo.setAmountIn(acblpf.getSacscurbal());
		zrdecplcPojo.setCurrency(acblpf.getOrigcurr());
		a000CallRounding(lifrtrnPojo);
		acblpf.setSacscurbal(new BigDecimal(zrdecplcPojo.getAmountOut().toString().replace("}", "")));
		List<Acblpf> acblpfList = new ArrayList<Acblpf>();
		acblpfList.add(acblpf);
		if(acblpf.getUnique_number() == null || acblpf.getUnique_number()==0){
			acblDAO.insertAcblpfList(acblpfList);
		}else{
			acblDAO.updateAcblRecord(acblpfList);
		}
	}
	protected void callBatcup3000(LifrtrnPojo lifrtrnPojo){
		BatcupPojo batcupPojo  = new BatcupPojo();
		batcupPojo.setBatcpfx("BA");
		batcupPojo.setBatccoy(lifrtrnPojo.getBatccoy());
		batcupPojo.setBatcbrn(lifrtrnPojo.getBatcbrn());
		batcupPojo.setBatcactyr(lifrtrnPojo.getBatcactyr());
		batcupPojo.setBatcactmn(lifrtrnPojo.getBatcactmn());
		batcupPojo.setBatctrcde(lifrtrnPojo.getBatctrcde());
		batcupPojo.setBatcbatch(lifrtrnPojo.getBatcbatch());
		batcupPojo.setTrancnt(0);
		batcupPojo.setEtreqcnt(0);
		batcupPojo.setSub(lifrtrnPojo.getContot());
		batcupPojo.setAscnt(0);
		batcupPojo.setBcnt(1);
		/* MOVE LIFR-ACCTAMT           TO BCUP-BVAL.                    */
		//batcupPojo.setBval(rtrnpf.getAcctamt().intValue());
		if (Objects.nonNull(rtrnpf.getAcctamt())) 
			batcupPojo.setBval(rtrnpf.getAcctamt().intValue());
		else
			batcupPojo.setBval(0);
		batcupPojo.setFunction("KEEPS");
		batcupPojo.setStatuz(varcom.oK.toString());
		batcupUtils.callBatcup(batcupPojo);
		
		if (Objects.nonNull(batcupPojo.getStatuz())
				&& (!varcom.oK.toString().equals(batcupPojo.getStatuz()))) {
			LOGGER.error("batcupPojo.getStatuz() - {}", batcupPojo.getStatuz());
			syserrrec.params.set(batcupPojo);
			syserrrec.statuz.set(batcupPojo.getStatuz());
			syserr570();				
		}						
	
	}
	protected ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}
}
