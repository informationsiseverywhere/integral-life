package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Vpmcdth extends COBOLConvCodeModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(Vpmcdth.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData tables = new FixedLengthStringData(10);
	private FixedLengthStringData t5687 = new FixedLengthStringData(5)
			.isAPartOf(tables, 0).init("T5687");
	private FixedLengthStringData tr28x = new FixedLengthStringData(5)
			.isAPartOf(tables, 5).init("TR28X");
	private FixedLengthStringData errors = new FixedLengthStringData(4);
	private FixedLengthStringData h053 = new FixedLengthStringData(4)
			.isAPartOf(errors, 0).init("H053");

	private FixedLengthStringData formats = new FixedLengthStringData(20);
	private FixedLengthStringData chdrrec = new FixedLengthStringData(10)
			.isAPartOf(formats, 0).init("CHDRREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10)
			.isAPartOf(formats, 10).init("ITEMREC");
	private T5687rec t5687rec = new T5687rec();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Deathrec deathrec = new Deathrec();

	private static final String SUBROUTINE_PREFIX = "CDTH";

	Varcom varcom = new Varcom();

	public Vpmcdth() {
		super();
	}

	public void mainline (Object... parmArray) {
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		LOGGER.debug("deathrec.deathRec.getData() mainline ", deathrec.deathRec.getData());//IJTI-1561
		try {
			main0100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	public void main0100 (){
		vpmcalcrec.set(SPACES);
		deathrec.status.set(Varcom.oK);
		vpmcalcrec.statuz.set(Varcom.oK);
		LOGGER.debug("deathrec.deathRec.getData() BEFORE ", deathrec.deathRec.getData());//IJTI-1561
		setupKey1000();
		LOGGER.debug("deathrec.deathRec.getData() AFTER ", deathrec.deathRec.getData());//IJTI-1561

		if(!isEQ(deathrec.status , Varcom.oK)){
			return;
		}
		vpmcalcrec.linkageArea.set(deathrec.deathRec);
		callProgram(CalcDeath.class, vpmcalcrec.vpmcalcRec);
		deathrec.deathRec.set(vpmcalcrec.linkageArea);
		if(!isEQ(vpmcalcrec.statuz , Varcom.oK)){
			deathrec.status.set(vpmcalcrec.statuz);
		}
	}

	private void setupKey1000() {
	    LOGGER.debug("deathrec.deathRec.getData()", deathrec.deathRec.getData());//IJTI-1561
		itdmIO.setItemcoy(deathrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(deathrec.crtable);
		itdmIO.setItmfrm(deathrec.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getStatuz() , Varcom.oK)
				|| !isEQ(itdmIO.getItemcoy(), deathrec.chdrChdrcoy)
				|| !isEQ(itdmIO.getItemtabl(),t5687)
				|| !isEQ(itdmIO.getItemitem(), deathrec.crtable)) {
			deathrec.status.set(h053);
			return;
		}
		t5687rec.t5687Rec.set(itdmIO.genarea);
		getChdr1100();
		vpmcalcrec.vpmskey.set(SPACES);
		vpmcalcrec.vpmscoy.set(deathrec.chdrChdrcoy);
		vpmcalcrec.vpmstabl.set(tr28x);
		vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX + chdrIO.cnttype.getData().trim());/* IJTI-1523 */
		vpmcalcrec.extkey.set(t5687rec.dcmeth);
	}

	private void getChdr1100() {
		initialize(chdrIO.getParams());
		chdrIO.setStatuz(Varcom.oK);
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(deathrec.chdrChdrcoy);
		chdrIO.setChdrnum(deathrec.chdrChdrnum);
		chdrIO.setValidflag(1);
		chdrIO.setCurrfrom(varcom.maxdate);
		chdrIO.setFunction(Varcom.begn);
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if(!isEQ(chdrIO.getStatuz() , Varcom.oK)
				|| !isEQ(chdrIO.getChdrnum() , deathrec.chdrChdrnum)){
			chdrIO.cnttype.set(SPACES);
		}
	}
}