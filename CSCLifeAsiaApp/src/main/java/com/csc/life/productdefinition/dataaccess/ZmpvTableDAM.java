package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZmpvTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:15
 * Class transformed from ZMPV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZmpvTableDAM extends ZmpvpfTableDAM {

	public ZmpvTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZMPV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "ZMEDCOY"
		             + ", ZMEDPRV";
		
		QUALIFIEDCOLUMNS = 
		            "ZMEDCOY, " +
		            "ZMEDPRV, " +
		            "ZMEDNUM, " +
		            "ZMEDNAM, " +
		            "ZMEDSTS, " +
		            "DTEAPP, " +
		            "DTETRM, " +
		            "ZDESC, " +
		            "PAYMTH, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "ZARACDE, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "ZMEDCOY ASC, " +
		            "ZMEDPRV ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "ZMEDCOY DESC, " +
		            "ZMEDPRV DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               zmedcoy,
                               zmedprv,
                               zmednum,
                               zmednam,
                               zmedsts,
                               dteapp,
                               dtetrm,
                               zdesc,
                               paymth,
                               bankkey,
                               bankacckey,
                               zaracde,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(245);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getZmedcoy().toInternal()
					+ getZmedprv().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, zmedcoy);
			what = ExternalData.chop(what, zmedprv);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(10);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(zmedcoy.toInternal());
	nonKeyFiller20.setInternal(zmedprv.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(196);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getZmednum().toInternal()
					+ getZmednam().toInternal()
					+ getZmedsts().toInternal()
					+ getDteapp().toInternal()
					+ getDtetrm().toInternal()
					+ getZdesc().toInternal()
					+ getPaymth().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getZaracde().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, zmednum);
			what = ExternalData.chop(what, zmednam);
			what = ExternalData.chop(what, zmedsts);
			what = ExternalData.chop(what, dteapp);
			what = ExternalData.chop(what, dtetrm);
			what = ExternalData.chop(what, zdesc);
			what = ExternalData.chop(what, paymth);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, zaracde);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getZmedcoy() {
		return zmedcoy;
	}
	public void setZmedcoy(Object what) {
		zmedcoy.set(what);
	}
	public FixedLengthStringData getZmedprv() {
		return zmedprv;
	}
	public void setZmedprv(Object what) {
		zmedprv.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getZmednum() {
		return zmednum;
	}
	public void setZmednum(Object what) {
		zmednum.set(what);
	}	
	public FixedLengthStringData getZmednam() {
		return zmednam;
	}
	public void setZmednam(Object what) {
		zmednam.set(what);
	}	
	public FixedLengthStringData getZmedsts() {
		return zmedsts;
	}
	public void setZmedsts(Object what) {
		zmedsts.set(what);
	}	
	public PackedDecimalData getDteapp() {
		return dteapp;
	}
	public void setDteapp(Object what) {
		setDteapp(what, false);
	}
	public void setDteapp(Object what, boolean rounded) {
		if (rounded)
			dteapp.setRounded(what);
		else
			dteapp.set(what);
	}	
	public PackedDecimalData getDtetrm() {
		return dtetrm;
	}
	public void setDtetrm(Object what) {
		setDtetrm(what, false);
	}
	public void setDtetrm(Object what, boolean rounded) {
		if (rounded)
			dtetrm.setRounded(what);
		else
			dtetrm.set(what);
	}	
	public FixedLengthStringData getZdesc() {
		return zdesc;
	}
	public void setZdesc(Object what) {
		zdesc.set(what);
	}	
	public FixedLengthStringData getPaymth() {
		return paymth;
	}
	public void setPaymth(Object what) {
		paymth.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getZaracde() {
		return zaracde;
	}
	public void setZaracde(Object what) {
		zaracde.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		zmedcoy.clear();
		zmedprv.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		zmednum.clear();
		zmednam.clear();
		zmedsts.clear();
		dteapp.clear();
		dtetrm.clear();
		zdesc.clear();
		paymth.clear();
		bankkey.clear();
		bankacckey.clear();
		zaracde.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}