package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Lfevpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LfevpfDAO extends BaseDAO<Lfevpf> {
    public void insertLfevpfRecord(List<Lfevpf> lfevpfList);
}