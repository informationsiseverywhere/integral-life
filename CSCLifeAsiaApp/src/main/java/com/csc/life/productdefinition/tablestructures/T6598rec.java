package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:23
 * Description:
 * Copybook name: T6598REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6598rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6598Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData addprocess = new FixedLengthStringData(7).isAPartOf(t6598Rec, 0);
  	public FixedLengthStringData calcprog = new FixedLengthStringData(7).isAPartOf(t6598Rec, 7);
  	public FixedLengthStringData procesprog = new FixedLengthStringData(7).isAPartOf(t6598Rec, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(479).isAPartOf(t6598Rec, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6598Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6598Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}