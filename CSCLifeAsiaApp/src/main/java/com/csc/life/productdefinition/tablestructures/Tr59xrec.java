package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;


/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:59
 * Description:
 * Copybook name: TR59Xrec
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr59xrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr59xrec = new FixedLengthStringData(44);
	public FixedLengthStringData savsrisk  = new FixedLengthStringData(1).isAPartOf(tr59xrec, 0);
	public FixedLengthStringData otherscheallowed = new FixedLengthStringData(1).isAPartOf(tr59xrec, 1);
	public FixedLengthStringData defaultScheme = new FixedLengthStringData(8).isAPartOf(tr59xrec, 2);
	public FixedLengthStringData dfcontrn = new FixedLengthStringData(30).isAPartOf(tr59xrec, 10);//*ILIFE-3693 -nnaveenkumar 
	public FixedLengthStringData recoveryType = new FixedLengthStringData(1).isAPartOf(tr59xrec, 40);
	public FixedLengthStringData cocontpay = new FixedLengthStringData(1).isAPartOf(tr59xrec, 41);
	public FixedLengthStringData liscpay = new FixedLengthStringData(1).isAPartOf(tr59xrec, 42);
	//public PackedDecimalData cocontpay = new PackedDecimalData(1, 0).isAPartOf(tr59xrec, 40);
	//public PackedDecimalData liscpay = new PackedDecimalData(1, 0).isAPartOf(tr59xrec, 41);
	public FixedLengthStringData polfee = new FixedLengthStringData(1).isAPartOf(tr59xrec, 43);


	public void initialize() {
		COBOLFunctions.initialize(tr59xrec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr59xrec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}