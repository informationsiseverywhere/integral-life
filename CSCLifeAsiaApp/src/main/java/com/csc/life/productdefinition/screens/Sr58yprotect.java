package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for PROTECT
 * @version 1.0 generated on 08/17/16 
 * @author CSC
 */
public class Sr58yprotect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}
	
/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */

	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr58yScreenVars sv = (Sr58yScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr58yprotectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr58yScreenVars screenVars = (Sr58yScreenVars)pv;
	}
	
/**
 * Clear all the variables in Sr58yprotect
 */
	public static void clear(VarModel pv) {
		Sr58yScreenVars screenVars = (Sr58yScreenVars) pv;
	}
}