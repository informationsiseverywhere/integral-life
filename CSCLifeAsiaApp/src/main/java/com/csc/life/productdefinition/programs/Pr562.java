/*
 * File: Pr562.java
 * Date: 30 August 2009 1:41:17
 * Author: Quipoz Limited
 * 
 * Class transformed from PR562.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;

import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsException;
import com.csc.dip.jvpms.runtime.base.VpmsJniSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.runtime.base.VpmsTcpSessionFactory;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.hibernate.dao.FsuCommonDao;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.recordstructures.Rlrdtrmrec;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.screens.Sr562ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  This is a window from Proposal Entry captures additional
*  information relatin to Reducing Term Contract.
*
*****************************************************************
* </pre>
*/
public class Pr562 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pr562.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR562");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaPrat = new PackedDecimalData(6, 2).setUnsigned();
	private ZonedDecimalData wsaaCoverc = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaMlresind = new FixedLengthStringData(3);  /*ILIFE-3691*/
	private FixedLengthStringData wsaaSubprog = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaZsbsmeth = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZsredtrm = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaMbnkref = new FixedLengthStringData(20);
	private static final String f490 = "F490";
	protected static final String e186 = "E186";
	private static final String w531 = "W531";
		/* TABLES */
	protected static final String t5688 = "T5688";
	protected static final String t3623 = "T3623";
	private static final String t5687 = "T5687";
	private static final String th615 = "TH615";
	private static final String t6598 = "T6598";
	private static final String itemrec = "ITEMREC";
	private static final String mrtarec = "MRTAREC";
	private static final String covtlnbrec = "COVTLNBREC";
	private static final String mbnsrec = "MBNSREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private T5687rec t5687rec = new T5687rec();
	private Th615rec th615rec = new Th615rec();
	private T6598rec t6598rec = new T6598rec();
	private Rlrdtrmrec rlrdtrmrec1 = new Rlrdtrmrec();
	//private Sr562ScreenVars sv = ScreenProgram.getScreenVars( Sr562ScreenVars.class);
	private Sr562ScreenVars sv = getPScreenVars();
	
	//BRD-139:Start
	private boolean mrtaPermission = false;
	private ZonedDecimalData wsaaMinvalue = new ZonedDecimalData(3, 0).init(5); 
	private ZonedDecimalData wsaaMaxvalue = new ZonedDecimalData(3, 0).init(15);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private String unitValue;
private int elapsedTime;
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private static final String hpadrec = "HAPDREC";

	
	private Datcon3rec datcon3rec = new Datcon3rec();
	//BRD-139:End
/*ILIFE-3754 starts*/
	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
	private Mrtapf mrtapf=null;
	protected boolean insertSuccess = false;
	protected boolean updateSuccess = false;
	protected boolean recordexist = false;
/*ILIFE-3754 Ends*/	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}
	
	protected Sr562ScreenVars getPScreenVars()
	{
		return ScreenProgram.getScreenVars( Sr562ScreenVars.class);
	}

	public Pr562() {
		super();
		screenVars = sv;
		new ScreenModel("Sr562", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		initialize(rlrdtrmrec1.rlrdtrmRec);
		initialize(wsaaPrat);
		initialize(wsaaCoverc);
		initialize(wsaaMlresind);
		initialize(wsaaZsbsmeth);
		initialize(wsaaZsredtrm);
		initialize(wsaaMbnkref);
		initialize(wsaaFlag);
		initialize(wsaaSubprog);
		sv.coverc.set(ZERO);
		sv.prat.set(ZERO);
		sv.prat.set(9999.99);
		sv.loandur.set(SPACES);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.cnRiskStat.set(chdrlnbIO.getStatcode());
		sv.cnPremStat.set(chdrlnbIO.getPstatcode());
		sv.cntcurr.set(chdrlnbIO.getCntcurr());
		sv.register.set(chdrlnbIO.getRegister());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.descrip.fill("?");
		}
		else {
			sv.descrip.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlnbIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		mrtaPermission = (FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars, "IT")) && isEQ(sv.cnttype,"MRT");/*ILIFE-4548*/
		if(!mrtaPermission){
			sv.mrtaFlag.set("N");
		}
		else {
			sv.mrtaFlag.set("Y");
		}
		/*ILIFE-3754 starts*/
		mrtapf = new Mrtapf();
		mrtapf=mrtapfDAO.getMrtaRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(mrtapf != null){
			recordexist= true;
			sv.prat.set(mrtapf.getPrat());
			sv.coverc.set(mrtapf.getCoverc());
			sv.prmdetails.set(mrtapf.getPrmdetails());
			sv.mlinsopt.set(mrtapf.getMlinsopt());
			sv.mbnkref.set(mrtapf.getMbnkref());
			//BRD-139:Start
			if(mrtaPermission){
			sv.loandur.set(mrtapf.getLoandur());
			sv.intcaltype.set(mrtapf.getIntcalType());
			sv.mlresindvpms.set(mrtapf.getMlresind());
			wsaaMlresind.set(sv.mlresindvpms);
			}else{
				sv.mlresind.set(mrtapf.getMlresind());
				wsaaMlresind.set(sv.mlresind);
			}
			/*ILIFE-3754 ends*/
			//BRD-139:Start
		}
		wsaaPrat.set(sv.prat);
		wsaaCoverc.set(sv.coverc);
	
		wsaaMbnkref.set(sv.mbnkref);
		if(mrtaPermission){
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		    datcon3rec.intDate2.set(datcon1rec.intDate);
		    datcon3rec.frequency.set("12");
		    callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		    if (isNE(datcon3rec.statuz, varcom.oK)) {
			    syserrrec.statuz.set(datcon3rec.statuz);
			    syserrrec.params.set(datcon3rec.datcon3Rec);
			    fatalError600();
		    }
		    
		    elapsedTime=datcon3rec.freqFactor.toInt();
	       	hpadIO.setParams(SPACES);
		    hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		    hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		    hpadIO.setFunction(varcom.readr);
		    hpadIO.setFormat(hpadrec);
		    SmartFileCode.execute(appVars, hpadIO);
		    if (isNE(hpadIO.getStatuz(), varcom.oK)
		    && isNE(hpadIO.getStatuz(), varcom.endp)
	     	&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			 syserrrec.params.set(hpadIO.getParams());
			 fatalError600();
		    }
		    if (isEQ(hpadIO.getStatuz(), varcom.endp)
		    || isEQ(hpadIO.getStatuz(), varcom.mrnf)) {
			   syserrrec.params.set(hpadIO.getParams());
			   fatalError600();
		    }
		   else {
			  try {
				  if(AppVars.getInstance().getAppConfig().isVpmsEnable()){
				getVPMSInformation();
				}
				  /*ILIFE-4548*/
			} catch (VpmsLoadFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		   }
		}
		if(!mrtaPermission){
			sv.loandurOut[varcom.nd.toInt()].set("Y");
			sv.intcaltypeOut[varcom.nd.toInt()].set("Y");
			sv.mlresindvpmsOut[varcom.nd.toInt()].set("Y");
		}
		

		/*	getVPMSInformation();*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkForErrors2080();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE*/
		/**    Validate fields*/
		if(mrtaPermission){
		  try {
			getVPMSInformation();
		} catch (VpmsLoadFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		validationCheck2200();
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set(SPACES);
			return ;
		}
	}

protected void validationCheck2200()
	{
		start2200();
	}

protected void start2200()
	{
		if (isNE(sv.prmdetails,"S")
		&& isNE(sv.prmdetails,"B")) {
			sv.prmdetailsErr.set(f490);
			wsspcomn.edterror.set(SPACES);
		}
		if (isEQ(sv.prmdetails,SPACES)) {
			sv.prmdetailsErr.set(e186);
			wsspcomn.edterror.set(SPACES);
		}
		if (isEQ(sv.prat,9999.99)) {
			sv.pratErr.set(e186);
			wsspcomn.edterror.set(SPACES);
		}
		if (isGT(sv.prat,100)) {
			sv.pratErr.set(w531);
			wsspcomn.edterror.set(SPACES);
		}
		if (isEQ(sv.mlinsopt,SPACES)) {
			sv.mlinsoptErr.set(e186);
			wsspcomn.edterror.set(SPACES);
		}
		if(!mrtaPermission){
		if (isEQ(sv.mlresind,SPACES)) {
			sv.mlresindErr.set(e186);
			wsspcomn.edterror.set(SPACES);
		}
		}
		if(mrtaPermission){
			if(isEQ(sv.loandur, SPACES) || isEQ(sv.loandur,0)){
				sv.loandurErr.set(e186);
				wsspcomn.edterror.set(SPACES);
			}else if(isGT(sv.loandur, wsaaMaxvalue) || (isLT(sv.loandur, wsaaMinvalue))){
				sv.loandurErr.set(w531);
				wsspcomn.edterror.set(SPACES);
			}
			if(isEQ(wsaaMaxvalue, wsaaMinvalue)){
				sv.loandur.setReadOnly();
			}
			if(isEQ(sv.intcaltype, SPACES)){
				sv.intcaltypeErr.set(e186);
				wsspcomn.edterror.set(SPACES);
			}
			if (isEQ(sv.mlresindvpms,SPACES)) {
				sv.mlresindvpmsErr.set(e186);
				wsspcomn.edterror.set(SPACES);
			}
		}
	}

protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		/*ILIFE-3754 starts*/
		if (!recordexist){
		mrtapf = new Mrtapf();
		}
		mrtapf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		mrtapf.setChdrnum(sv.chdrnum.toString());
		mrtapf.setPrat((sv.prat.getbigdata()));//ILIFE-3784
		mrtapf.setCoverc(sv.coverc.toInt());
		mrtapf.setPrmdetails(sv.prmdetails.toString());
		mrtapf.setMlinsopt(sv.mlinsopt.toString());
		mrtapf.setMbnkref(sv.mbnkref.toString());
		//BRD-139:Start
		if(mrtaPermission){
			mrtapf.setLoandur(sv.loandur.toInt());
			mrtapf.setIntcalType(sv.intcaltype.toString());
			if(unitValue!=null && !unitValue.isEmpty())          /*ILIFE-4548*/
			mrtapf.setPhunitvalue(Integer.parseInt(unitValue.trim()));
			else
		    mrtapf.setPhunitvalue(0);
			mrtapf.setMlresind(sv.mlresindvpms.toString());
		}else{
			mrtapf.setMlresind(sv.mlresind.toString());
		}
		//BRD-139:Start
		if(recordexist){
			updateSuccess=mrtapfDAO.updateMrtaRecord(mrtapf);
			if(!updateSuccess){
				syserrrec.params.set("update Fail");
				fatalError600();
			}
		}else{
		insertSuccess=mrtapfDAO.insertMrtapfList(mrtapf);
		if(!insertSuccess){
			syserrrec.params.set("insert Fail");
			fatalError600();
		}
		/*ILIFE-3754 starts*/
		}
		if(!mrtaPermission){
		if (isNE(sv.prat,wsaaPrat)
		|| isNE(sv.coverc,wsaaCoverc)
		|| isNE(sv.mlresind,wsaaMlresind)
		|| isNE(sv.mbnkref,wsaaMbnkref)) {
			readCovtlnb3200();
		}
		}
			if(mrtaPermission){
				if (isNE(sv.prat,wsaaPrat)
						|| isNE(sv.coverc,wsaaCoverc)
						|| isNE(sv.mlresindvpms,wsaaMlresind)
						|| isNE(sv.mbnkref,wsaaMbnkref)) {
							
							readCovtlnb3200();
						}
			}
		
		
	}

protected void readTh615Table3100()
	{
		/*START*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(th615);
		itemIO.setItemitem(sv.mlresind);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			rlrdtrmrec1.statuz.set(itemIO.getStatuz());
		}
		th615rec.th615Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void readCovtlnb3200()
	{
		/*START*/
		covtlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtlnbIO.setLife(SPACES);
		covtlnbIO.setCoverage(SPACES);
		covtlnbIO.setRider(SPACES);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFormat(covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtlnbIO.setStatuz(varcom.oK);
		while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
			readLife3250();
		}
		
		/*EXIT*/
	}

protected void readLife3250()
	{
			start3250();
		}

protected void start3250()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(),covtlnbIO.getChdrnum())) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			wsaaFlag.set("Y");
		}
		if(!mrtaPermission){
		  readTh615Table3100();
		  if (isEQ(th615rec.premind,"Y")
		  && isNE(wsaaFlag,"Y")) {
		    	z500DeleteBenefitSchedule();
		    	covtlnbIO.setFunction(varcom.nextr);
		    	return ;
		  }
		  else {
			  readT5687Table3260();
			   if (isEQ(wsaaZsredtrm,"Y")
			   && isNE(wsaaFlag,"Y")) {
				 if (isNE(sv.coverc,wsaaCoverc)) {
				 	z500DeleteBenefitSchedule();
				 }
				 getMethod3400();
				 calcBenefitSchedule3500();
			    }
	       }
		}
		else {
			readT5687Table3260();
			if (isEQ(wsaaZsredtrm,"Y")
			&& isNE(wsaaFlag,"Y")) {
				if (isNE(sv.coverc,wsaaCoverc)) {
					z500DeleteBenefitSchedule();
				}
				getMethod3400();
				calcBenefitSchedule3500();
			}
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			return ;
		}
		covtlnbIO.setFunction(varcom.nextr);
	}

protected void readT5687Table3260()
	{
		start3260();
	}

protected void start3260()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covtlnbIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaZsbsmeth.set(SPACES);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
			wsaaZsbsmeth.set(t5687rec.zsbsmeth);
			wsaaZsredtrm.set(t5687rec.zsredtrm);
		}
	}

protected void getMethod3400()
	{
		start3400();
	}

protected void start3400()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(itemrec);
		itemIO.setItemtabl(t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaZsbsmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
		wsaaSubprog.set(t6598rec.calcprog);
	}

protected void calcBenefitSchedule3500()
	{
		start3500();
	}

protected void start3500()
	{
		rlrdtrmrec1.chdrcoy.set(chdrlnbIO.getChdrcoy());
		rlrdtrmrec1.chdrnum.set(chdrlnbIO.getChdrnum());
		rlrdtrmrec1.life.set(covtlnbIO.getLife());
		rlrdtrmrec1.coverage.set(covtlnbIO.getCoverage());
		rlrdtrmrec1.rider.set(covtlnbIO.getRider());
		rlrdtrmrec1.sumins.set(covtlnbIO.getSumins());
		rlrdtrmrec1.riskTerm.set(covtlnbIO.getRiskCessTerm());
		callProgram(wsaaSubprog, rlrdtrmrec1.rlrdtrmRec);
		if (isEQ(rlrdtrmrec1.statuz,varcom.bomb)) {
			syserrrec.statuz.set(rlrdtrmrec1.statuz);
			fatalError600();
		}
	}

protected void z500DeleteBenefitSchedule()
	{
		z500Start();
	}

protected void z500Start()
	{
		mbnsIO.setDataArea(SPACES);
		mbnsIO.setDataKey(SPACES);
		mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
		mbnsIO.setLife(covtlnbIO.getLife());
		mbnsIO.setCoverage(covtlnbIO.getCoverage());
		mbnsIO.setRider(covtlnbIO.getRider());
		mbnsIO.setYrsinf(ZERO);
		mbnsIO.setFormat(mbnsrec);
		mbnsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		mbnsIO.setStatuz(varcom.oK);
		while ( !(isEQ(mbnsIO.getStatuz(),varcom.endp))) {
			z600DeleteRecord();
		}
		
	}

protected void z600DeleteRecord()
	{
			z600Start();
		}

protected void z600Start()
	{
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)
		&& isNE(mbnsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrcoy(),mbnsIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(),mbnsIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(),mbnsIO.getLife())
		|| isNE(covtlnbIO.getCoverage(),mbnsIO.getCoverage())
		|| isNE(covtlnbIO.getRider(),mbnsIO.getRider())) {
			mbnsIO.setStatuz(varcom.endp);
		}
		if (isEQ(mbnsIO.getStatuz(),varcom.endp)) {
			return ;
		}
		mbnsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			fatalError600();
		}
		mbnsIO.setFunction(varcom.nextr);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

public static Map<String,Map<String,String>> f4(COBOLAppVars appVars, String field) throws VpmsLoadFailedException{
	
	Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();
	Map<String, String> itemDesc=new HashMap<String,String>();
	IVpmsBaseSessionFactory sessionFactory = new VpmsJniSessionFactory();
	IVpmsBaseSession session;
	//ILIFE-5605 start
	IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();
	if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
		factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
		session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
	}
	else
	{
		session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
	}
	//ILIFE-5605 end
	session.setAttribute("Input Contract Type", "mrt");
	session.setAttribute("Input Region", " "); 
	session.setAttribute("Input Locale", " "); 
	session.setAttribute("Input TransEffDate", "20161010");
	session.setAttribute("Input MTL PLan Cd", "MAS05S"); 
	session.setAttribute("Input Prod Code", "GS1"); 
	session.setAttribute("Input Coverage Code", "mrt1");

	VpmsComputeResult result = session.computeUsingDefaults("Output Fetch MRTA Frequency");
	//sv.mlresind.set(result.getResult());
	String temp1=result.getResult();
    temp1=temp1.replace("||","/n");
  
    String ReducingFreq[] = temp1.split("/n");
    ArrayList<String> ReducingFreqList = new ArrayList<String>();
    ArrayList<String> ReducingFreqList1 = new ArrayList<String>();
    LOGGER.info("parts1={}", Arrays.toString(ReducingFreq));//IJTI-1561
	 
    for (int i = 0; i < ReducingFreq.length; i =i+2) {
    	ReducingFreqList.add(ReducingFreq[i]);
    	ReducingFreqList1.add(ReducingFreq[i+1]);

    }
    for (int i = 0; i < ReducingFreqList.size(); i++) {
    	itemDesc.put( ReducingFreqList.get(i).trim(),  ReducingFreqList1.get(i).trim());
		
        }
	 
	f4map.put(field, itemDesc);
	
    return f4map;
    
} 

	private void getVPMSInformation() throws VpmsLoadFailedException{
		//ILIFE-5605 start
		//IVpmsBaseSessionFactory sessionFactory = new VpmsJniSessionFactory();
		IVpmsBaseSession session;
		IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();
		if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
			factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
			session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
		}
		else
		{
			session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
		}
		//ILIFE-5605 end
		session.setAttribute("Input Contract Type", sv.cnttype.toString());
		session.setAttribute("Input Region", " "); 
		session.setAttribute("Input Locale", " "); 
		session.setAttribute("Input TransEffDate", wsaaToday.toString());
		session.setAttribute("Input MTL PLan Cd", "MAS05S"); 
    	session.setAttribute("Input Prod Code", "GS1"); 
		session.setAttribute("Input Coverage Code", "MRT1");/*ILIFE-3714*/
		session.setAttribute("Input Interest Rate", sv.prat.toString());
		session.setAttribute("Input Face Amount", "10000");/*ILIFE-3714*/
		session.setAttribute("Input No of Months", "1");/*ILIFE-3714*/
		session.setAttribute("Input Loan Duration", sv.loandur.toString());
		session.setAttribute("Input Interest Type", sv.intcaltype.toString());
		VpmsComputeResult result3 = session.computeUsingDefaults("Output Calc MRTA");
		String[] str_array =result3.getResult().split("!");
		int i= str_array.length;
		unitValue=str_array[i-1];
	}

}
