package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:19
 * Description:
 * Copybook name: ZMTMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zmtmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zmtmFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zmtmKey = new FixedLengthStringData(256).isAPartOf(zmtmFileKey, 0, REDEFINE);
  	public FixedLengthStringData zmtmZmedcoy = new FixedLengthStringData(1).isAPartOf(zmtmKey, 0);
  	public FixedLengthStringData zmtmZmedprv = new FixedLengthStringData(10).isAPartOf(zmtmKey, 1);
  	public PackedDecimalData zmtmSeqno = new PackedDecimalData(2, 0).isAPartOf(zmtmKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(zmtmKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zmtmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zmtmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}