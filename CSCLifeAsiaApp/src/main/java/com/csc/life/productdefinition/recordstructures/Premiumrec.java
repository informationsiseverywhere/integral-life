package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:00
 * Description:
 * Copybook name: PREMIUMREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Premiumrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData premiumRec = new FixedLengthStringData(getPremiumRecSize());// ILIFE-6941 ILIFE-6964	ILIFE-7253
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(premiumRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(premiumRec, 5);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(premiumRec, 9);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(premiumRec, 10);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(premiumRec, 18);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(premiumRec, 20);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(premiumRec, 22);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(premiumRec, 24);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(premiumRec, 27);
  	public FixedLengthStringData mop = new FixedLengthStringData(1).isAPartOf(premiumRec, 29);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(premiumRec, 30);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(premiumRec, 32);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(premiumRec, 36);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(premiumRec, 37);
  	public PackedDecimalData effectdt = new PackedDecimalData(8, 0).isAPartOf(premiumRec, 40);
  	public PackedDecimalData termdate = new PackedDecimalData(8, 0).isAPartOf(premiumRec, 45);
  	public PackedDecimalData sumin = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 50);
  	public PackedDecimalData calcPrem = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 59);
  	public PackedDecimalData calcBasPrem = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 68);
  	public PackedDecimalData calcLoaPrem = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 77);
  	public PackedDecimalData ratingdate = new PackedDecimalData(8, 0).isAPartOf(premiumRec, 86);
  	public PackedDecimalData reRateDate = new PackedDecimalData(8, 0).isAPartOf(premiumRec, 91);
  	public PackedDecimalData lage = new PackedDecimalData(3, 0).isAPartOf(premiumRec, 96);
  	public PackedDecimalData jlage = new PackedDecimalData(3, 0).isAPartOf(premiumRec, 98);
  	public FixedLengthStringData lsex = new FixedLengthStringData(1).isAPartOf(premiumRec, 100);
  	public FixedLengthStringData jlsex = new FixedLengthStringData(1).isAPartOf(premiumRec, 101);
  	public ZonedDecimalData duration = new ZonedDecimalData(2, 0).isAPartOf(premiumRec, 102).setUnsigned();
  	public FixedLengthStringData reasind = new FixedLengthStringData(1).isAPartOf(premiumRec, 104);
  	public FixedLengthStringData freqann = new FixedLengthStringData(2).isAPartOf(premiumRec, 105);
  	public FixedLengthStringData arrears = new FixedLengthStringData(1).isAPartOf(premiumRec, 107);
  	public FixedLengthStringData advance = new FixedLengthStringData(1).isAPartOf(premiumRec, 108);
  	public PackedDecimalData guarperd = new PackedDecimalData(3, 0).isAPartOf(premiumRec, 109);
  	public PackedDecimalData intanny = new PackedDecimalData(5, 2).isAPartOf(premiumRec, 111);
  	public PackedDecimalData capcont = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 114);
  	public FixedLengthStringData withprop = new FixedLengthStringData(1).isAPartOf(premiumRec, 123);
  	public FixedLengthStringData withoprop = new FixedLengthStringData(1).isAPartOf(premiumRec, 124);
  	public FixedLengthStringData ppind = new FixedLengthStringData(1).isAPartOf(premiumRec, 125);
  	public FixedLengthStringData nomlife = new FixedLengthStringData(8).isAPartOf(premiumRec, 126);
  	public PackedDecimalData dthpercn = new PackedDecimalData(5, 2).isAPartOf(premiumRec, 134);
  	public PackedDecimalData dthperco = new PackedDecimalData(5, 2).isAPartOf(premiumRec, 137);
  	public PackedDecimalData riskCessTerm = new PackedDecimalData(3, 0).isAPartOf(premiumRec, 140);
  	public PackedDecimalData benCessTerm = new PackedDecimalData(3, 0).isAPartOf(premiumRec, 142);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(premiumRec, 144);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(premiumRec, 147);
  	public FixedLengthStringData benpln = new FixedLengthStringData(2).isAPartOf(premiumRec, 148);
  //*********Ticket #VE-522 Externalisation Code ***************
  	public FixedLengthStringData premMethod = new FixedLengthStringData(4).isAPartOf(premiumRec, 150);//ILIFE238
	public FixedLengthStringData rstaflag = new FixedLengthStringData(1).isAPartOf(premiumRec, 154);//ILIFE290
	
//IVE-750  Start- TRM product - Premium calculation for HBNF rider - PM09 method
	public FixedLengthStringData ccode = new FixedLengthStringData(4).isAPartOf(premiumRec, 155); 
	public FixedLengthStringData livesno = new FixedLengthStringData(1).isAPartOf(premiumRec, 159); 
	public FixedLengthStringData indic = new FixedLengthStringData(1).isAPartOf(premiumRec, 160);
	public PackedDecimalData totprem = new PackedDecimalData(11, 2).isAPartOf(premiumRec, 161);
	public PackedDecimalData percent = new PackedDecimalData(7, 3).isAPartOf(premiumRec, 167); 
//IVE-750  Start- TRM product - Premium calculation for HBNF rider - PM09 method
//IVE-792  RUL Product - Premium Calculation - Integration with latest PA compatible models
	public PackedDecimalData factor = new PackedDecimalData(2,0).isAPartOf(premiumRec, 171);
	public FixedLengthStringData liveno = new FixedLengthStringData(1).isAPartOf(premiumRec, 172); 
	/*BRD-306 START */
	public PackedDecimalData loadper = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 173);
	public PackedDecimalData rateadj = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 190);
	public PackedDecimalData fltmort = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 207);
	public PackedDecimalData premadj = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 224);
	public PackedDecimalData adjustageamt = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 241);
	/*BRD-306 END */
	
	public FixedLengthStringData rstate01 = new FixedLengthStringData(3).isAPartOf(premiumRec, 258);
	public FixedLengthStringData rstate02 = new FixedLengthStringData(3).isAPartOf(premiumRec, 261);
	public PackedDecimalData zstpduty01 = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 264);
	public PackedDecimalData zstpduty02 = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 273);

	public FixedLengthStringData waitperiod = new FixedLengthStringData(3).isAPartOf(premiumRec, 282); 
	public FixedLengthStringData bentrm = new FixedLengthStringData(2).isAPartOf(premiumRec, 285); 
	public FixedLengthStringData prmbasis = new FixedLengthStringData(1).isAPartOf(premiumRec, 287); 
	public FixedLengthStringData poltyp = new FixedLengthStringData(1).isAPartOf(premiumRec, 288); 
	public FixedLengthStringData occpcode = new FixedLengthStringData(4).isAPartOf(premiumRec, 289); 
	public FixedLengthStringData occpclass = new FixedLengthStringData(2).isAPartOf(premiumRec, 293); 
	public FixedLengthStringData dialdownoption = new FixedLengthStringData(3).isAPartOf(premiumRec, 295);
	public FixedLengthStringData linkcov = new FixedLengthStringData(1000).isAPartOf(premiumRec, 298); // ILIFE-6941
	public FixedLengthStringData liencd = new FixedLengthStringData(2).isAPartOf(premiumRec, 1298); // ILIFE-6964
	public FixedLengthStringData tpdtype = new FixedLengthStringData(1).isAPartOf(premiumRec, 1300);
	public FixedLengthStringData setPmexCall = new FixedLengthStringData(1).isAPartOf(premiumRec, 1301);
 	public FixedLengthStringData prevBillFreq = new FixedLengthStringData(2).isAPartOf(premiumRec, 1302);//ILIFE-7343
 	public FixedLengthStringData lnkgind = new FixedLengthStringData(1).isAPartOf(premiumRec, 1304);
 	public FixedLengthStringData updateRequired = new FixedLengthStringData(1).isAPartOf(premiumRec, 1305);//ILIFE-7884
 	public FixedLengthStringData validflag = new FixedLengthStringData(1).isAPartOf(premiumRec, 1306);//ILIFE-7884
 	public FixedLengthStringData lnkgSubRefNo = new FixedLengthStringData(50).isAPartOf(premiumRec, 1307);
 	public PackedDecimalData inputPrevPrem = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 1357);//ILIFE-7524
 	public PackedDecimalData calcTotPrem = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 1374);//ILIFE-7845
 	public PackedDecimalData riskPrem = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 1391);//ILIFE-7845
 	public FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(premiumRec, 1408);
 	public PackedDecimalData occdate = new PackedDecimalData(8).isAPartOf(premiumRec, 1416);
 	public FixedLengthStringData flag = new FixedLengthStringData(1).isAPartOf(premiumRec, 1424);// ILIFE-8163 
 	//ILIFE-8502-starts
 	public FixedLengthStringData commTaxInd = new FixedLengthStringData(2).isAPartOf(premiumRec, 1425);
 	public PackedDecimalData prevSumIns = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 1427);
 	public FixedLengthStringData stateAtIncep = new FixedLengthStringData(8).isAPartOf(premiumRec, 1442);
	public PackedDecimalData commissionPrem = new PackedDecimalData(17, 2).isAPartOf(premiumRec, 1450);
	public FixedLengthStringData validind = new FixedLengthStringData(1).isAPartOf(premiumRec, 1467);
 	//ILIFE-8502-ends
	public FixedLengthStringData bilfrmdt = new FixedLengthStringData(8).isAPartOf(premiumRec, 1468);
	public FixedLengthStringData biltodt = new FixedLengthStringData(8).isAPartOf(premiumRec, 1476);
	public FixedLengthStringData proratPremCalcFlag = new FixedLengthStringData(1).isAPartOf(premiumRec, 1484);
	public FixedLengthStringData firstCovr = new FixedLengthStringData(1).isAPartOf(premiumRec, 1485);
	public FixedLengthStringData batcBatctrcde = new FixedLengthStringData(4).isAPartOf(premiumRec, 1486);
 	


	public void initialize() {
		COBOLFunctions.initialize(premiumRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		premiumRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getPremiumRecSize()
	{
		return 1490;
	}

	

}