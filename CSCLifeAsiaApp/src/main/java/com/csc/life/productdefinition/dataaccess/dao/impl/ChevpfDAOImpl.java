package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.ChevpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Chevpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ChevpfDAOImpl extends BaseDAOImpl<Chevpf> implements ChevpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChevpfDAOImpl.class);

    public void insertChevpfRecord(List<Chevpf> chevpfList) {

        if (chevpfList != null) {
            String SQL_CHEV_INSERT = "INSERT INTO CHEVPF(CHDRCOY,CHDRNUM,EFFDATE,CNTTYPE,RCDATE,OWNNAM,PAYORNAME,HOISSDTE,HISSDTE,AGENTNO,AGTYPE,BILLFREQ,STATCODE,PSTATCODE,PTDATE,BTDATE,TOTLPREM,INSTPREM,CSHMT,LOANVALUE,SURRCHG,SURRVAL,CURRFROM,VALIDFLAG,USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement psChevInsert = getPrepareStatement(SQL_CHEV_INSERT);
            try {
                for (Chevpf c : chevpfList) {
                    psChevInsert.setString(1, c.getChdrcoy());
                    psChevInsert.setString(2, c.getChdrnum());
                    psChevInsert.setInt(3, c.getEffdate());
                    psChevInsert.setString(4, c.getCnttype());
                    psChevInsert.setInt(5, c.getRcdate());
                    psChevInsert.setString(6, c.getOwnnam().substring(0,20));
                    psChevInsert.setString(7, c.getPayorname());
                    psChevInsert.setInt(8, c.getHoissdte());
                    psChevInsert.setInt(9, c.getHissdte());
                    psChevInsert.setString(10, c.getAgentno());
                    psChevInsert.setString(11, c.getAgtype());
                    psChevInsert.setString(12, c.getBillfreq());
                    psChevInsert.setString(13, c.getStatcode());
                    psChevInsert.setString(14, c.getPstatcode());
                    psChevInsert.setInt(15, c.getPtdate());
                    psChevInsert.setInt(16, c.getBtdate());
                    psChevInsert.setBigDecimal(17, c.getTotlprem());
                    psChevInsert.setBigDecimal(18, c.getInstprem());
                    psChevInsert.setBigDecimal(19, c.getCshmt());
                    psChevInsert.setBigDecimal(20, c.getLoanvalue());
                    psChevInsert.setBigDecimal(21, c.getSurrchg());
                    psChevInsert.setBigDecimal(22, c.getSurrval());
                    psChevInsert.setInt(23, c.getCurrfrom());
                    psChevInsert.setString(24, c.getValidflag());
                    psChevInsert.setString(25, getUsrprf());
                    psChevInsert.setString(26, getJobnm());
                    psChevInsert.setTimestamp(27, new Timestamp(System.currentTimeMillis()));
                    psChevInsert.addBatch();
                }
                psChevInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertChevpfRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psChevInsert, null);
            }
        }
    }
}