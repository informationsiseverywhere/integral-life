package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:48
 * Description:
 * Copybook name: TJ699REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tj699rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tj699Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData freqcys = new FixedLengthStringData(24).isAPartOf(tj699Rec, 0);
  	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(12, 2, freqcys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(freqcys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freqcy01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData freqcy02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData freqcy03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData freqcy04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData freqcy05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData freqcy06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData freqcy07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData freqcy08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData freqcy09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData freqcy10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData freqcy11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData freqcy12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData zlfacts = new FixedLengthStringData(72).isAPartOf(tj699Rec, 24);
  	public ZonedDecimalData[] zlfact = ZDArrayPartOfStructure(12, 6, 4, zlfacts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(72).isAPartOf(zlfacts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zlfact01 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 0);
  	public ZonedDecimalData zlfact02 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 6);
  	public ZonedDecimalData zlfact03 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 12);
  	public ZonedDecimalData zlfact04 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 18);
  	public ZonedDecimalData zlfact05 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 24);
  	public ZonedDecimalData zlfact06 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 30);
  	public ZonedDecimalData zlfact07 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 36);
  	public ZonedDecimalData zlfact08 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 42);
  	public ZonedDecimalData zlfact09 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 48);
  	public ZonedDecimalData zlfact10 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 54);
  	public ZonedDecimalData zlfact11 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 60);
  	public ZonedDecimalData zlfact12 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 66);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(404).isAPartOf(tj699Rec, 96, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tj699Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tj699Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}