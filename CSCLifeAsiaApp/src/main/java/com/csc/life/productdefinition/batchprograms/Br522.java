/*
 * File: Br522.java
 * Date: 29 August 2009 22:13:34
 * Author: Quipoz Limited
 * 
 * Class transformed from BR522.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.productdefinition.dataaccess.ActxpfTableDAM;
import com.csc.life.productdefinition.recordstructures.Pr534par;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*
*            ACTUARIAL VALUATION SPLITTER PROGRAM
*           **************************************
*   This program will extract all the contract selected on the
*   parameter prompt screen PR534, that match the contract risk
*   and premium status specified on T5679.
*
*   This extracted records will be processed in BR523.
*
*
*****************************************************************
* </pre>
*/
public class Br522 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlactxpf1rs = null;
	private java.sql.PreparedStatement sqlactxpf1ps = null;
	private java.sql.Connection sqlactxpf1conn = null;
	private String sqlactxpf1 = "";
	private int actxpf1LoopIndex = 0;
	private ActxpfTableDAM actxpf = new ActxpfTableDAM();
	private DiskFileDAM actx01 = new DiskFileDAM("ACTX01");
	private DiskFileDAM actx02 = new DiskFileDAM("ACTX02");
	private DiskFileDAM actx03 = new DiskFileDAM("ACTX03");
	private DiskFileDAM actx04 = new DiskFileDAM("ACTX04");
	private DiskFileDAM actx05 = new DiskFileDAM("ACTX05");
	private DiskFileDAM actx06 = new DiskFileDAM("ACTX06");
	private DiskFileDAM actx07 = new DiskFileDAM("ACTX07");
	private DiskFileDAM actx08 = new DiskFileDAM("ACTX08");
	private DiskFileDAM actx09 = new DiskFileDAM("ACTX09");
	private DiskFileDAM actx10 = new DiskFileDAM("ACTX10");
	private DiskFileDAM actx11 = new DiskFileDAM("ACTX11");
	private DiskFileDAM actx12 = new DiskFileDAM("ACTX12");
	private DiskFileDAM actx13 = new DiskFileDAM("ACTX13");
	private DiskFileDAM actx14 = new DiskFileDAM("ACTX14");
	private DiskFileDAM actx15 = new DiskFileDAM("ACTX15");
	private DiskFileDAM actx16 = new DiskFileDAM("ACTX16");
	private DiskFileDAM actx17 = new DiskFileDAM("ACTX17");
	private DiskFileDAM actx18 = new DiskFileDAM("ACTX18");
	private DiskFileDAM actx19 = new DiskFileDAM("ACTX19");
	private DiskFileDAM actx20 = new DiskFileDAM("ACTX20");
	private ActxpfTableDAM actxpfData = new ActxpfTableDAM();
	private FixedLengthStringData actx01Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx02Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx03Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx04Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx05Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx06Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx07Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx08Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx09Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx10Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx11Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx12Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx13Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx14Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx15Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx16Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx17Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx18Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx19Rec = new FixedLengthStringData(29);
	private FixedLengthStringData actx20Rec = new FixedLengthStringData(29);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR522");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/* ACTX members parameter*/
	private FixedLengthStringData wsaaActxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaActxFn, 0, FILLER).init("ACTX");
	private FixedLengthStringData wsaaActxRunid = new FixedLengthStringData(2).isAPartOf(wsaaActxFn, 4);
	private ZonedDecimalData wsaaActxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaActxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaActxData = FLSInittedArray (1000, 29);
	private FixedLengthStringData[] wsaaChdrpfx = FLSDArrayPartOfArrayStructure(2, wsaaActxData, 0);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaActxData, 2);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaActxData, 3);
	private FixedLengthStringData[] wsaaCnttype = FLSDArrayPartOfArrayStructure(3, wsaaActxData, 11);
	private PackedDecimalData[] wsaaOccdate = PDArrayPartOfArrayStructure(8, 0, wsaaActxData, 14);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaActxData, 19);
	private FixedLengthStringData[] wsaaPstatcode = FLSDArrayPartOfArrayStructure(2, wsaaActxData, 21);
	private PackedDecimalData[] wsaaCurrfrom = PDArrayPartOfArrayStructure(8, 0, wsaaActxData, 23);
	private FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaActxData, 28);
	private FixedLengthStringData wsaaCompanyCode = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData iy = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaDatefrm = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateto = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1);
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private DescTableDAM descIO = new DescTableDAM();
	private Pr534par pr534par = new Pr534par();

	public Br522() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		execSql1030();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set("IVRM");
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaActxRunid.set(bprdIO.getSystemParam04());
		wsaaCompanyCode.set(bprdIO.getCompany());
		wsaaActxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		pr534par.parmRecord.set(bupaIO.getParmarea());
		//TICKET #ILIFE-1150[Batch L2ACTVALU not updating DB], Starts
		if (isEQ(pr534par.chdrnum, SPACES)
		&& isEQ(pr534par.chdrnum1, SPACES)) {
		wsaaChdrnumFrom.set(LOVALUE);
		wsaaChdrnumTo.set(HIVALUES);
		}
		else {
		wsaaChdrnumFrom.set(pr534par.chdrnum);
		wsaaChdrnumTo.set(pr534par.chdrnum1);
		}
		//TICKET #ILIFE-1150 Ends
		if (isEQ(pr534par.indc,"Y")) {
			wsaaChdrnumTo.set(pr534par.chdrnum);
		}
		wsaaDatefrm.set(pr534par.datefrm);
		wsaaDateto.set(pr534par.dateto);
		wsaaFirstTime.set("Y");
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
	}


protected void execSql1030()
	{
		iy.set(1);
		//MIBT-112 corrected column name PSTCDE
		sqlactxpf1 = " SELECT  A.CHDRPFX, A.CHDRCOY, A.CHDRNUM, A.CNTTYPE, A.OCCDATE, A.STATCODE, A.PSTCDE, A.CURRFROM, A.VALIDFLAG, B.HISSDTE, B.HOISSDTE" +
				" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + "  A,  " + getAppVars().getTableNameOverriden("HPADPF") + "  B" +
				" WHERE A.CHDRCOY = ?" +
				" AND A.CHDRCOY = B.CHDRCOY" +
				" AND A.CHDRNUM = B.CHDRNUM" +
				" AND (( A.CHDRNUM >= ?" +
				" AND A.CHDRNUM <= ? )" +   
				" AND ( B.HISSDTE >= ? " + //ILIFE-8332
				" AND B.HISSDTE <= ? ))"  +			
				" AND A.SERVUNIT = 'LP'" +
				" AND A.VALIDFLAG='1'"+ //ILIFE-9041
				" AND B.VALIDFLAG='1'"+ //ILIFE-9041
				" ORDER BY CHDRNUM";
		
		
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlactxpf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.fsu.general.dataaccess.ChdrpfTableDAM(), new com.csc.life.newbusiness.dataaccess.HpadpfTableDAM()});
			sqlactxpf1ps = getAppVars().prepareStatementEmbeded(sqlactxpf1conn, sqlactxpf1);
			getAppVars().setDBString(sqlactxpf1ps, 1, wsaaCompanyCode);
			getAppVars().setDBString(sqlactxpf1ps, 2, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlactxpf1ps, 3, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqlactxpf1ps, 4, wsaaDatefrm);
			getAppVars().setDBNumber(sqlactxpf1ps, 5, wsaaDateto);
			sqlactxpf1rs = getAppVars().executeQuery(sqlactxpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaActxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaActxFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(ACTX");
		stringVariable2.addExpression(iz);
		stringVariable2.addExpression(") TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaActxFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.addExpression(" SEQONLY(*YES 1000)");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz,1)) {
			actx01.openOutput();
		}
		if (isEQ(iz,2)) {
			actx02.openOutput();
		}
		if (isEQ(iz,3)) {
			actx03.openOutput();
		}
		if (isEQ(iz,4)) {
			actx04.openOutput();
		}
		if (isEQ(iz,5)) {
			actx05.openOutput();
		}
		if (isEQ(iz,6)) {
			actx06.openOutput();
		}
		if (isEQ(iz,7)) {
			actx07.openOutput();
		}
		if (isEQ(iz,8)) {
			actx08.openOutput();
		}
		if (isEQ(iz,9)) {
			actx09.openOutput();
		}
		if (isEQ(iz,10)) {
			actx10.openOutput();
		}
		if (isEQ(iz,11)) {
			actx11.openOutput();
		}
		if (isEQ(iz,12)) {
			actx12.openOutput();
		}
		if (isEQ(iz,13)) {
			actx13.openOutput();
		}
		if (isEQ(iz,14)) {
			actx14.openOutput();
		}
		if (isEQ(iz,15)) {
			actx15.openOutput();
		}
		if (isEQ(iz,16)) {
			actx16.openOutput();
		}
		if (isEQ(iz,17)) {
			actx17.openOutput();
		}
		if (isEQ(iz,18)) {
			actx18.openOutput();
		}
		if (isEQ(iz,19)) {
			actx19.openOutput();
		}
		if (isEQ(iz,20)) {
			actx20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaOccdate[wsaaInd.toInt()].set(ZERO);
		wsaaCurrfrom[wsaaInd.toInt()].set(ZERO);
		wsaaChdrcoy[wsaaInd.toInt()].set(LOVALUE);
		wsaaChdrnum[wsaaInd.toInt()].set(LOVALUE);
		wsaaCnttype[wsaaInd.toInt()].set(LOVALUE);
		wsaaStatcode[wsaaInd.toInt()].set(LOVALUE);
		wsaaPstatcode[wsaaInd.toInt()].set(LOVALUE);
		wsaaValidflag[wsaaInd.toInt()].set(LOVALUE);
		wsaaChdrpfx[wsaaInd.toInt()].set(LOVALUE);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFiles2010();
		}

protected void readFiles2010()
	{
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
        // Changed for ILIFE-2483
//		if (eofInBlock.isTrue()) {
//			wsspEdterror.set(varcom.endp);
//			return ;
//		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {//ILIFE-2984
			for (actxpf1LoopIndex = 1; isLTE(actxpf1LoopIndex,wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlactxpf1rs); actxpf1LoopIndex++ ){
				getAppVars().getDBObject(sqlactxpf1rs, 1, wsaaChdrpfx[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 2, wsaaChdrcoy[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 3, wsaaChdrnum[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 4, wsaaCnttype[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 5, wsaaOccdate[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 6, wsaaStatcode[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 7, wsaaPstatcode[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 8, wsaaCurrfrom[actxpf1LoopIndex]);
				getAppVars().getDBObject(sqlactxpf1rs, 9, wsaaValidflag[actxpf1LoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  An SQLCODE = +100 is returned when :-*/
		/*      a) no rows are returned on the first fetch*/
		/*      b) the last row of the cursor is either in the block or is*/
		/*         the last row of the block.*/
		/*  We must continue processing to the 3000- section for case b)*/
		//ILIFE-526 
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		|| isEQ(wsaaChdrnum[wsaaInd.toInt()], LOVALUE) || 
		isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
			return;
		}
		//ILIFE-526
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
		// Changed for ILIFE-2483
		wsaaEofInBlock.set("N");
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the         */
		/*  to the previous we should move to the next output file member. */
		/*  The condition is here to allow for checking the last chdrnum   */
		/*  of the old block with the first of the new and to move to the  */
		/*  next file member to write to if they have changed.             */
		if (isNE(wsaaChdrnum[wsaaInd.toInt()],wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all ACTX data for the same contract until    */
		/*  the CHDRNUM on ACTX has changed,                               */
		/*    OR                                                           */
		/*  The end of an incomplete block is reached.                     */
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()],wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		actxpfData.chdrpfx.set(wsaaChdrpfx[wsaaInd.toInt()]);
		actxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		actxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		actxpfData.cnttype.set(wsaaCnttype[wsaaInd.toInt()]);
		actxpfData.occdate.set(wsaaOccdate[wsaaInd.toInt()]);
		actxpfData.statcode.set(wsaaStatcode[wsaaInd.toInt()]);
		actxpfData.pstatcode.set(wsaaPstatcode[wsaaInd.toInt()]);
		actxpfData.currfrom.set(wsaaCurrfrom[wsaaInd.toInt()]);
		actxpfData.validflag.set(wsaaValidflag[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy,1)) {
			actx01.write(actxpfData);
		}
		if (isEQ(iy,2)) {
			actx02.write(actxpfData);
		}
		if (isEQ(iy,3)) {
			actx03.write(actxpfData);
		}
		if (isEQ(iy,4)) {
			actx04.write(actxpfData);
		}
		if (isEQ(iy,5)) {
			actx05.write(actxpfData);
		}
		if (isEQ(iy,6)) {
			actx06.write(actxpfData);
		}
		if (isEQ(iy,7)) {
			actx07.write(actxpfData);
		}
		if (isEQ(iy,8)) {
			actx08.write(actxpfData);
		}
		if (isEQ(iy,9)) {
			actx09.write(actxpfData);
		}
		if (isEQ(iy,10)) {
			actx10.write(actxpfData);
		}
		if (isEQ(iy,11)) {
			actx11.write(actxpfData);
		}
		if (isEQ(iy,12)) {
			actx12.write(actxpfData);
		}
		if (isEQ(iy,13)) {
			actx13.write(actxpfData);
		}
		if (isEQ(iy,14)) {
			actx14.write(actxpfData);
		}
		if (isEQ(iy,15)) {
			actx15.write(actxpfData);
		}
		if (isEQ(iy,16)) {
			actx16.write(actxpfData);
		}
		if (isEQ(iy,17)) {
			actx17.write(actxpfData);
		}
		if (isEQ(iy,18)) {
			actx18.write(actxpfData);
		}
		if (isEQ(iy,19)) {
			actx19.write(actxpfData);
		}
		if (isEQ(iy,20)) {
			actx20.write(actxpfData);
		}
		/*    Log the number of extracted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		wsaaInd.add(1);
		//ILIFE-2984
		if(isLTE(wsaaInd,wsaaRowsInBlock)){
		  /*  Check for an incomplete block retrieved.*/
		  if (isEQ(wsaaChdrnum[wsaaInd.toInt()], LOVALUE)
		  && isLTE(wsaaInd,wsaaRowsInBlock)) {
			wsaaEofInBlock.set("Y");
		  }
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlactxpf1conn, sqlactxpf1ps, sqlactxpf1rs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			actx01.close();
		}
		if (isEQ(iz,2)) {
			actx02.close();
		}
		if (isEQ(iz,3)) {
			actx03.close();
		}
		if (isEQ(iz,4)) {
			actx04.close();
		}
		if (isEQ(iz,5)) {
			actx05.close();
		}
		if (isEQ(iz,6)) {
			actx06.close();
		}
		if (isEQ(iz,7)) {
			actx07.close();
		}
		if (isEQ(iz,8)) {
			actx08.close();
		}
		if (isEQ(iz,9)) {
			actx09.close();
		}
		if (isEQ(iz,10)) {
			actx10.close();
		}
		if (isEQ(iz,11)) {
			actx11.close();
		}
		if (isEQ(iz,12)) {
			actx12.close();
		}
		if (isEQ(iz,13)) {
			actx13.close();
		}
		if (isEQ(iz,14)) {
			actx14.close();
		}
		if (isEQ(iz,15)) {
			actx15.close();
		}
		if (isEQ(iz,16)) {
			actx16.close();
		}
		if (isEQ(iz,17)) {
			actx17.close();
		}
		if (isEQ(iz,18)) {
			actx18.close();
		}
		if (isEQ(iz,19)) {
			actx19.close();
		}
		if (isEQ(iz,20)) {
			actx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-T5679-RISK-STAT--INNER
 */

}
