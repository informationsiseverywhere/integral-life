package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:52
 * Description:
 * Copybook name: T5676REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5676rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5676Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(t5676Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(490).isAPartOf(t5676Rec, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5676Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5676Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}