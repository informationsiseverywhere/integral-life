package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LfevpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:43
 * Class transformed from LFEVPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LfevpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 603;
	public FixedLengthStringData lfevrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData lfevpfRecord = lfevrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(lfevrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(lfevrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(lfevrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(lfevrec);
	public FixedLengthStringData alname01 = DD.alname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData alname02 = DD.alname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData alname03 = DD.alname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData alname04 = DD.alname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData alname05 = DD.alname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlname01 = DD.jlname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlname02 = DD.jlname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlname03 = DD.jlname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlname04 = DD.jlname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlname05 = DD.jlname.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smokeind01 = DD.smokeind.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smokeind02 = DD.smokeind.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smokeind03 = DD.smokeind.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smokeind04 = DD.smokeind.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smokeind05 = DD.smokeind.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smkrqd01 = DD.smkrqd.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smkrqd02 = DD.smkrqd.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smkrqd03 = DD.smkrqd.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smkrqd04 = DD.smkrqd.copy().isAPartOf(lfevrec);
	public FixedLengthStringData smkrqd05 = DD.smkrqd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbAtCcd01 = DD.anbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbAtCcd02 = DD.anbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbAtCcd03 = DD.anbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbAtCcd04 = DD.anbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbAtCcd05 = DD.anbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbccd01 = DD.janbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbccd02 = DD.janbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbccd03 = DD.janbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbccd04 = DD.janbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbccd05 = DD.janbccd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbisd01 = DD.anbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbisd02 = DD.anbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbisd03 = DD.anbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbisd04 = DD.anbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData anbisd05 = DD.anbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbisd01 = DD.janbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbisd02 = DD.janbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbisd03 = DD.janbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbisd04 = DD.janbisd.copy().isAPartOf(lfevrec);
	public PackedDecimalData janbisd05 = DD.janbisd.copy().isAPartOf(lfevrec);
	public FixedLengthStringData sex01 = DD.sex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData sex02 = DD.sex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData sex03 = DD.sex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData sex04 = DD.sex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData sex05 = DD.sex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlsex01 = DD.jlsex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlsex02 = DD.jlsex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlsex03 = DD.jlsex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlsex04 = DD.jlsex.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jlsex05 = DD.jlsex.copy().isAPartOf(lfevrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(lfevrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(lfevrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(lfevrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(lfevrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(lfevrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LfevpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LfevpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LfevpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LfevpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LfevpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LfevpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LfevpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LFEVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"EFFDATE, " +
							"ALNAME01, " +
							"ALNAME02, " +
							"ALNAME03, " +
							"ALNAME04, " +
							"ALNAME05, " +
							"JLNAME01, " +
							"JLNAME02, " +
							"JLNAME03, " +
							"JLNAME04, " +
							"JLNAME05, " +
							"SMOKEIND01, " +
							"SMOKEIND02, " +
							"SMOKEIND03, " +
							"SMOKEIND04, " +
							"SMOKEIND05, " +
							"SMKRQD01, " +
							"SMKRQD02, " +
							"SMKRQD03, " +
							"SMKRQD04, " +
							"SMKRQD05, " +
							"ANBCCD01, " +
							"ANBCCD02, " +
							"ANBCCD03, " +
							"ANBCCD04, " +
							"ANBCCD05, " +
							"JANBCCD01, " +
							"JANBCCD02, " +
							"JANBCCD03, " +
							"JANBCCD04, " +
							"JANBCCD05, " +
							"ANBISD01, " +
							"ANBISD02, " +
							"ANBISD03, " +
							"ANBISD04, " +
							"ANBISD05, " +
							"JANBISD01, " +
							"JANBISD02, " +
							"JANBISD03, " +
							"JANBISD04, " +
							"JANBISD05, " +
							"SEX01, " +
							"SEX02, " +
							"SEX03, " +
							"SEX04, " +
							"SEX05, " +
							"JLSEX01, " +
							"JLSEX02, " +
							"JLSEX03, " +
							"JLSEX04, " +
							"JLSEX05, " +
							"CURRFROM, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     effdate,
                                     alname01,
                                     alname02,
                                     alname03,
                                     alname04,
                                     alname05,
                                     jlname01,
                                     jlname02,
                                     jlname03,
                                     jlname04,
                                     jlname05,
                                     smokeind01,
                                     smokeind02,
                                     smokeind03,
                                     smokeind04,
                                     smokeind05,
                                     smkrqd01,
                                     smkrqd02,
                                     smkrqd03,
                                     smkrqd04,
                                     smkrqd05,
                                     anbAtCcd01,
                                     anbAtCcd02,
                                     anbAtCcd03,
                                     anbAtCcd04,
                                     anbAtCcd05,
                                     janbccd01,
                                     janbccd02,
                                     janbccd03,
                                     janbccd04,
                                     janbccd05,
                                     anbisd01,
                                     anbisd02,
                                     anbisd03,
                                     anbisd04,
                                     anbisd05,
                                     janbisd01,
                                     janbisd02,
                                     janbisd03,
                                     janbisd04,
                                     janbisd05,
                                     sex01,
                                     sex02,
                                     sex03,
                                     sex04,
                                     sex05,
                                     jlsex01,
                                     jlsex02,
                                     jlsex03,
                                     jlsex04,
                                     jlsex05,
                                     currfrom,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		effdate.clear();
  		alname01.clear();
  		alname02.clear();
  		alname03.clear();
  		alname04.clear();
  		alname05.clear();
  		jlname01.clear();
  		jlname02.clear();
  		jlname03.clear();
  		jlname04.clear();
  		jlname05.clear();
  		smokeind01.clear();
  		smokeind02.clear();
  		smokeind03.clear();
  		smokeind04.clear();
  		smokeind05.clear();
  		smkrqd01.clear();
  		smkrqd02.clear();
  		smkrqd03.clear();
  		smkrqd04.clear();
  		smkrqd05.clear();
  		anbAtCcd01.clear();
  		anbAtCcd02.clear();
  		anbAtCcd03.clear();
  		anbAtCcd04.clear();
  		anbAtCcd05.clear();
  		janbccd01.clear();
  		janbccd02.clear();
  		janbccd03.clear();
  		janbccd04.clear();
  		janbccd05.clear();
  		anbisd01.clear();
  		anbisd02.clear();
  		anbisd03.clear();
  		anbisd04.clear();
  		anbisd05.clear();
  		janbisd01.clear();
  		janbisd02.clear();
  		janbisd03.clear();
  		janbisd04.clear();
  		janbisd05.clear();
  		sex01.clear();
  		sex02.clear();
  		sex03.clear();
  		sex04.clear();
  		sex05.clear();
  		jlsex01.clear();
  		jlsex02.clear();
  		jlsex03.clear();
  		jlsex04.clear();
  		jlsex05.clear();
  		currfrom.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLfevrec() {
  		return lfevrec;
	}

	public FixedLengthStringData getLfevpfRecord() {
  		return lfevpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLfevrec(what);
	}

	public void setLfevrec(Object what) {
  		this.lfevrec.set(what);
	}

	public void setLfevpfRecord(Object what) {
  		this.lfevpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(lfevrec.getLength());
		result.set(lfevrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}