/*
 * File: Vlpdclpr.java
 * Date: December 3, 2013 4:06:32 AM ICT
 * Author: CSC
 * 
 * Class transformed from VLPDCLPR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;
import com.csc.fsu.clients.dataaccess.ClprTableDAM;
import com.csc.fsu.clients.tablestructures.Tr52jrec;
import com.csc.fsu.general.dataaccess.ClrrforTableDAM;
import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* Subroutine VLPDCLPR.
* ====================
*
*    This subroutine will be processed to validate Client profilin 
*    and various proof by product. It will be called by the main
*    validation subroutine VLPDRULE in NB pre-issue validation, an 
*    component add/modify cross check product validation.
*
****************************************************************** ****
* </pre>
*/
public class Vlpdclpr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "VLPDCLPR";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaTr52jSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrDet = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaErdSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private final int wsaaMaxError = 20;
	private final int wsaaMaxClrrrole = 20;
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaTr52jItmkey = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaTr52jCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52jItmkey, 0).init(SPACES);

		/* WSAA-TR52J-ARRAY */
	private FixedLengthStringData[] wsaaTr52jRec = FLSInittedArray (20, 2);
	private FixedLengthStringData[] wsaaTr52jKey = FLSDArrayPartOfArrayStructure(2, wsaaTr52jRec, 0);
	private FixedLengthStringData[] wsaaTr52jClrrrole = FLSDArrayPartOfArrayStructure(2, wsaaTr52jKey, 0, HIVALUE);

	private FixedLengthStringData wsaaErrDetails = new FixedLengthStringData(160);
	private FixedLengthStringData[] wsaaErrRec = FLSArrayPartOfStructure(20, 8, wsaaErrDetails, 0);
	private FixedLengthStringData[] wsaaErrCode = FLSDArrayPartOfArrayStructure(4, wsaaErrRec, 0);
	private FixedLengthStringData[] wsaaErrDeta = FLSDArrayPartOfArrayStructure(4, wsaaErrRec, 4);

	private FixedLengthStringData wsaaCheck = new FixedLengthStringData(1);
	private Validator check = new Validator(wsaaCheck, "Y");
	private Validator notCheck = new Validator(wsaaCheck, "N");
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String clprrec = "CLPRREC";
	private static final String clrrforrec = "CLRRFORREC";
		/* TABLES */
	private static final String tr52j = "TR52J";
		/* ERRORS */
	private static final String rlbw = "RLBW";
	private static final String rlbx = "RLBX";
	private static final String rlby = "RLBY";
	private static final String rlbz = "RLBZ";
	private IntegerData wsaaTr52jIx = new IntegerData();
	private ClprTableDAM clprIO = new ClprTableDAM();
	private ClrrforTableDAM clrrforIO = new ClrrforTableDAM();
//	private ItemTableDAM itemIO = new ItemTableDAM(); //ILIFE-8723
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Tr52jrec tr52jrec = new Tr52jrec();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();
	 //ILIFE-8723 start
	private Itempf itempf = new Itempf();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
 //ILIFE-8723 end

	public Vlpdclpr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main000()
	{
		begin010();
		exit090();
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		initialize(wsaaErrDetails);
		wsaaCheck.set("Y");
		if (isEQ(vlpdsubrec.cnttype, SPACES)) {
			return ;
		}
		readTr52j100();
		if (notCheck.isTrue()) {
			return ;
		}
		readClrrfor200();
		wsaaErdSeq.set(0);
		for (wsaaErrSeq.set(1); !(isGT(wsaaErrSeq, wsaaMaxError)); wsaaErrSeq.add(1)){
			if (isNE(wsaaErrCode[wsaaErrSeq.toInt()], SPACES)) {
				wsaaErdSeq.add(1);
				vlpdsubrec.errCode[wsaaErdSeq.toInt()].set(wsaaErrCode[wsaaErrSeq.toInt()]);
				vlpdsubrec.errDet[wsaaErdSeq.toInt()].set(wsaaErrDeta[wsaaErrSeq.toInt()]);
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTr52j100()
	{
		begin110();
		read120();
	}

protected void begin110()
	{
		wsaaTr52jCnttype.set(vlpdsubrec.cnttype);
	}

protected void read120()
	{
	 //ILIFE-8723 start
		List<Itempf> tr52jList = itemDAO.getAllitemsbyCurrency(smtpfxcpy.item.toString(), vlpdsubrec.chdrcoy.toString(), "Tr52j", wsaaTr52jItmkey.toString());
		if(tr52jList.size()>0) {
			tr52jrec.tr52jRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			if (isNE(wsaaTr52jItmkey, "***")) {
				wsaaTr52jItmkey.set("***");
				read120();
				return ;
			}
			else {
				wsaaCheck.set("N");
				return ;
			}
		}
	
		/*itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(vlpdsubrec.chdrcoy.toString());
		itempf.setItemtabl(tr52j);
		itempf.setItemitem(wsaaTr52jItmkey.toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf != null) {
			tr52jrec.tr52jRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			if (isNE(wsaaTr52jItmkey, "***")) {
				wsaaTr52jItmkey.set("***");
				read120();
				return ;
			}
			else {
				wsaaCheck.set("N");
				return ;
			}
		}*/
		/*itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr52j);
		itemIO.setItemitem(wsaaTr52jItmkey);
		itemIO.setFunction(varcom.begn);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItempfx(), smtpfxcpy.item)
		|| isNE(itemIO.getItemcoy(), vlpdsubrec.chdrcoy)
		|| isNE(itemIO.getItemtabl(), tr52j)
		|| isNE(itemIO.getItemitem(), wsaaTr52jItmkey)) {
			if (isNE(wsaaTr52jItmkey, "***")) {
				wsaaTr52jItmkey.set("***");
				read120();
				return ;
			}
			else {
				itemIO.setStatuz(varcom.endp);
				wsaaCheck.set("N");
				return ;
			}
		}
		tr52jrec.tr52jRec.set(itemIO.getGenarea());*/
 //ILIFE-8723 end
	}

protected void readClrrfor200()
	{
		begin201();
		call202();
	}

protected void begin201()
	{
		clrrforIO.setParams(SPACES);
		clrrforIO.setForecoy(vlpdsubrec.chdrcoy);
		clrrforIO.setForenum(vlpdsubrec.chdrnum);
		clrrforIO.setClrrrole(SPACES);
		clrrforIO.setFormat(clrrforrec);
		clrrforIO.setFunction(varcom.begn);
	}

protected void call202()
	{
		SmartFileCode.execute(appVars, clrrforIO);
		if (isNE(clrrforIO.getStatuz(), varcom.oK)
		&& isNE(clrrforIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clrrforIO.getParams());
			syserrrec.statuz.set(clrrforIO.getStatuz());
			fatalError600();
		}
		wsaaClntnum.set(clrrforIO.getForenum());
		if (isEQ(clrrforIO.getStatuz(), varcom.endp)
		|| isNE(clrrforIO.getForecoy(), vlpdsubrec.chdrcoy)
		|| isNE(wsaaClntnum, vlpdsubrec.chdrnum)) {
			clrrforIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(clrrforIO.getUsedToBe(), SPACES)) {
			validation300();
		}
		clrrforIO.setFunction(varcom.nextr);
		call202();
		return ;
	}

protected void validation300()
	{
		begin310();
	}

protected void begin310()
	{
		for (wsaaTr52jSeq.set(1); !(isGT(wsaaTr52jSeq, wsaaMaxClrrrole)); wsaaTr52jSeq.add(1))
{
			/* No processing required. */
		}
		clprIO.setParams(SPACES);
		clprIO.setClntpfx(clrrforIO.getClntpfx());
		clprIO.setClntcoy(clrrforIO.getClntcoy());
		clprIO.setClntnum(clrrforIO.getClntnum());
		clprIO.setFormat(clprrec);
		clprIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clprIO);
		if (isNE(clprIO.getStatuz(), varcom.oK)
		&& isNE(clprIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clprIO.getParams());
			syserrrec.statuz.set(clprIO.getStatuz());
			fatalError600();
		}
		/* Validate Client Risk profile*/
		for (wsaaTr52jSeq.set(1); !(isGT(wsaaTr52jSeq, 5)); wsaaTr52jSeq.add(1)){
			if (isEQ(clrrforIO.getClrrrole(), tr52jrec.clrrrole[wsaaTr52jSeq.toInt()])
			&& isEQ(clprIO.getClrskind(), SPACES)) {
				wsaaErrCode[wsaaTr52jSeq.toInt()].set(rlbw);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(clrrforIO.getClrrrole(), "  ");
				stringVariable1.addExpression(" ", "  ");
				stringVariable1.addExpression("-", "  ");
				stringVariable1.setStringInto(wsaaErrDet);
				wsaaErrDeta[wsaaTr52jSeq.toInt()].set(wsaaErrDet);
			}
		}
		/* Validate Address Proof*/
		for (wsaaTr52jSeq.set(6); !(isGT(wsaaTr52jSeq, 10)); wsaaTr52jSeq.add(1)){
			if (isEQ(clrrforIO.getClrrrole(), tr52jrec.clrrrole[wsaaTr52jSeq.toInt()])
			&& isEQ(clprIO.getAddprf(), SPACES)) {
				wsaaErrCode[wsaaTr52jSeq.toInt()].set(rlbx);
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression(clrrforIO.getClrrrole(), "  ");
				stringVariable2.addExpression(" ", "  ");
				stringVariable2.addExpression("-", "  ");
				stringVariable2.setStringInto(wsaaErrDet);
				wsaaErrDeta[wsaaTr52jSeq.toInt()].set(wsaaErrDet);
			}
		}
		/* Validate Identity Proof*/
		for (wsaaTr52jSeq.set(11); !(isGT(wsaaTr52jSeq, 15)); wsaaTr52jSeq.add(1)){
			if (isEQ(clrrforIO.getClrrrole(), tr52jrec.clrrrole[wsaaTr52jSeq.toInt()])
			&& isEQ(clprIO.getIdnprf(), SPACES)) {
				wsaaErrCode[wsaaTr52jSeq.toInt()].set(rlby);
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(clrrforIO.getClrrrole(), "  ");
				stringVariable3.addExpression(" ", "  ");
				stringVariable3.addExpression("-", "  ");
				stringVariable3.setStringInto(wsaaErrDet);
				wsaaErrDeta[wsaaTr52jSeq.toInt()].set(wsaaErrDet);
			}
		}
		/* Validate Income Proof*/
		for (wsaaTr52jSeq.set(16); !(isGT(wsaaTr52jSeq, 20)); wsaaTr52jSeq.add(1)){
			if (isEQ(clrrforIO.getClrrrole(), tr52jrec.clrrrole[wsaaTr52jSeq.toInt()])
			&& isEQ(clprIO.getIncprf(), SPACES)) {
				wsaaErrCode[wsaaTr52jSeq.toInt()].set(rlbz);
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression(clrrforIO.getClrrrole(), "  ");
				stringVariable4.addExpression(" ", "  ");
				stringVariable4.addExpression("-", "  ");
				stringVariable4.setStringInto(wsaaErrDet);
				wsaaErrDeta[wsaaTr52jSeq.toInt()].set(wsaaErrDet);
			}
		}
	}

protected void fatalError600()
	{
		/*FATAL-ERRORS*/
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
