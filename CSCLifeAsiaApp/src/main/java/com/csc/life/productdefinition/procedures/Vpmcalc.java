/*
 * File: Vpmcalc.java
 * Date: 30 August 2012 1:58:03
 * Author: Quipoz Limited
 * 
 * Class transformed from VPMCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.VpmdTableDAM;
import com.csc.life.productdefinition.procedures.vpms.ComputeResult;
import com.csc.life.productdefinition.procedures.vpms.VPMSUtils;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.Tr28xrec;
import com.csc.life.productdefinition.tablestructures.Tr28yrec;
import com.csc.life.productdefinition.tablestructures.Tr28zrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Vpmcalc extends COBOLConvCodeModel {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(Vpmcalc.class);
	private static final String COMPUTE = "COMP";
	private static final String SET_ATTR = "SETV";
	private static final int MAX_INDEX = 1000;
	private static final String CLOSE_SQUARE = "]";
	private static final String OPEN_SQUARE = "[";
	private static final String CLOSE_PARENTHESIS = ")";
	private static final String OPEN_PARENTHESIS = "(";
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final int MAX_GDE_NUM = 9;
	private static final int MAX_ATTRS_NO = 100;
	private static final int NORMAL_EXT_FIELDS_NO = 20;
	private static final int MAX_EXT_FIELDS_NO = 25;
	private static final int MAX_UPDATE_NUM = 5;
	private static final String RETURN_OK = "0";
	private static final int MAX_PAGE_TR28Y = 25;
	
	private String wsaaSubr = "VPMCALC";
		/* ERRORS */
	private String rflg = "RFLG";
	private String rfli = "RFLI";
	private String rflj = "RFLJ";
	private String rflk = "RFLK";
	private String e211 = "E211";
	
	private String tr28x = "TR28X";
	private String tr28y = "TR28Y";
	private String tr28z = "TR28Z";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String vpmdrec = "VPMDREC";

	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsbbSub = new PackedDecimalData(3, 0).init(0);
	
	private ZonedDecimalData wsaaItemseq = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	
	//Property mapping
	private FixedLengthStringData wsaaPropertyMappingRecs = new FixedLengthStringData(2725);
	private FixedLengthStringData[] wsaaPropertyMappings = FLSArrayPartOfStructure(MAX_EXT_FIELDS_NO, 109, wsaaPropertyMappingRecs, 0);
	private FixedLengthStringData[] wsaaProperty = FLSDArrayPartOfArrayStructure(60, wsaaPropertyMappings, 0);
	private FixedLengthStringData[] wsaaPropCopybook= FLSDArrayPartOfArrayStructure(12, wsaaPropertyMappings, 60);
	protected FixedLengthStringData[] wsaaPropMapField = FLSDArrayPartOfArrayStructure(35, wsaaPropertyMappings, 72);
	private PackedDecimalData[] wsaaPropNoOccurs = PDArrayPartOfArrayStructure(3, 0, wsaaPropertyMappings, 107);
	//TR28Z control variables
	private FixedLengthStringData wsaaDataExtSubr = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFunc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaCopybook = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaMapField = new FixedLengthStringData(20);
	private PackedDecimalData wsaaNumOccurs = new PackedDecimalData(3, 0);
	
	private FixedLengthStringData wsaaAttrReturnValuess = new FixedLengthStringData(8000000);
	private FixedLengthStringData[] wsaaAttrReturnValues = FLSArrayPartOfStructure(MAX_ATTRS_NO, 80000, wsaaAttrReturnValuess, 0);
	private FixedLengthStringData[][] wsaaAttrReturnValue = FLSDArrayPartOfArrayStructure(MAX_INDEX, 80, wsaaAttrReturnValues, 0);

	private FixedLengthStringData wsaaTr28yVpmsprop = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaTr28yCopybk = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaTr28yMaptofield = new FixedLengthStringData(35);
	private PackedDecimalData wsaaTr28yNumoccurs = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaTr28yAttrLbls = new FixedLengthStringData(6000);
	private FixedLengthStringData[] wsaaTr28yAttrLbl = FLSArrayPartOfStructure(MAX_ATTRS_NO, 60, wsaaTr28yAttrLbls, 0);
	private FixedLengthStringData wsaaTr28yAttrValues = new FixedLengthStringData(800);
	private FixedLengthStringData[] wsaaTr28yAttrValue = FLSArrayPartOfStructure(MAX_ATTRS_NO, 8, wsaaTr28yAttrValues, 0);

	private ItemTableDAM itemIO = new ItemTableDAM();
	private VpmdTableDAM vpmdIO = new VpmdTableDAM();
	
	private Syserrrec syserrrec = new Syserrrec();
	protected Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	protected Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
	
	private Tr28xrec tr28xrec = new Tr28xrec();
	private Tr28yrec tr28yrec = new Tr28yrec();
	private Tr28zrec tr28zrec = new Tr28zrec();
	private Varcom varcom = new Varcom();
	
	private LinkedHashMap<String, String> attrsNameValueMap;
	private LinkedHashMap<String, LinkedHashMap> propsAttrsMap;
		
	private Map<String, String> inputAttrsMap;
	private Map<String, ComputeResult> resultsMap;
	private Collection<String> outputPropertySet;
	protected  Map<String, ExternalData> recordMap = new HashMap<String, ExternalData>();;
	
	private ExternalData recordObject;
	
	private int startIdxExtIds = 1;
	private int endIdxExtIds = NORMAL_EXT_FIELDS_NO;
	private String formatSubr;

	
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit290, 
		exit9020, 
		callAnGDE, 
		buildAnProperty, 
		buildAnAttribute, 
		exit100, 
		exit110, 
		exit120, 
		tr28x130, 
		exit105,
		exit720,
		callAnUpdate710
	}

	public Vpmcalc() {
		super();
	}

protected void startSubr010()
	{
		para010();
	}

protected void para010()
	{
		vpmcalcrec.statuz.set(varcom.oK);
		wsaaStatuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		
		initialize100();
		wsaaSub.set(0);
		
		if (isEQ(wsaaStatuz, varcom.oK)){
			callGDE200();
		}
		
		wsaaSub.set(0);
		if (isEQ(wsaaStatuz, varcom.oK)){
			buildVPMSAttributes300();
		}
		if (isEQ(wsaaStatuz, varcom.oK)){
			callVPMSAPI400();
		}
		if (isEQ(wsaaStatuz, varcom.oK)){
			retrieveVPMSCalcResults500();
		}
		if (isEQ(wsaaStatuz, varcom.oK)){
			callDataUpdates700();
		}
		
		//Call VPMS for last 5 Externalized Fields
		if (isEQ(vpmfmtrec.statuz, varcom.endp)){
			wsaaSub.set(NORMAL_EXT_FIELDS_NO);
			startIdxExtIds = NORMAL_EXT_FIELDS_NO + 1;
			endIdxExtIds = MAX_EXT_FIELDS_NO;
			if (isEQ(wsaaStatuz, varcom.oK)){
				buildVPMSAttributes300();
			}
			if (isEQ(wsaaStatuz, varcom.oK)){
				callVPMSAPI400();
			}
			if (isEQ(wsaaStatuz, varcom.oK)){
				retrieveVPMSCalcResults500();
			}
			if (isEQ(wsaaStatuz, varcom.oK)){
				callDataUpdates700();
			}
		}
		
		if (isNE(wsaaStatuz,varcom.oK)) {
			vpmcalcrec.statuz.set(wsaaStatuz);
		}
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para110();
				}
				case tr28x130: {
					readTr28x130();
					updateFormatStatus();
					initFormatSubr();
				}
				case exit105: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para110()
	{
		wsaaSub.set(ZERO);
		wsaaSub1.set(ZERO);
		wsaaSub2.set(ZERO);
		wsbbSub.set(ZERO);
		startIdxExtIds = 1;
		endIdxExtIds = NORMAL_EXT_FIELDS_NO;
		
		tr28xrec.tr28xRec.set(SPACES);
		tr28yrec.tr28yRec.set(SPACES);
		tr28zrec.tr28zRec.set(SPACES);
		
		inputAttrsMap = new HashMap<String, String>();
		resultsMap = new HashMap<String, ComputeResult>();
		outputPropertySet = new ArrayList<String>();
		
		attrsNameValueMap = new LinkedHashMap<String, String>();
		propsAttrsMap = new LinkedHashMap<String, LinkedHashMap>();
		
		for (int loopVar1 = 1; loopVar1 <= MAX_EXT_FIELDS_NO; loopVar1 += 1){
			wsaaPropertyMappings[loopVar1].set(SPACES);
		}
		
		for (int loopVar1 = 1; loopVar1 <= MAX_ATTRS_NO; loopVar1 += 1){
			wsaaTr28yAttrLbl[loopVar1].set(SPACES);
			wsaaTr28yAttrValue[loopVar1].set(SPACES);
			for (int loopVar2 = 1; loopVar2 <= MAX_INDEX; loopVar2 += 1){
				wsaaAttrReturnValue[loopVar1][loopVar2].set(SPACES);
			}
		}
		wsaaTr28yVpmsprop.set(SPACES);
		wsaaTr28yCopybk.set(SPACES);
		wsaaTr28yMaptofield.set(SPACES);
		wsaaTr28yNumoccurs.set(ZERO);
		goTo(GotoLabel.tr28x130);
	}

protected void updateFormatStatus()
{
	//Set up format subroutine for product
	formatSubr = tr28xrec.vpmsubr.getData().trim();/* IJTI-1386 */
	if (isNE(tr28xrec.linkstatuz, SPACE)
			&& isNE(tr28xrec.linkcopybk, SPACE)){
		vpmfmtrec.vpmfmtRec.set(SPACES);
		vpmfmtrec.function.set("UPDV");
		vpmfmtrec.copybook.set(tr28xrec.linkcopybk);
		vpmfmtrec.resultField.set(tr28xrec.linkstatuz);
		vpmfmtrec.resultValue.set(varcom.oK);
		callProgram(formatSubr, vpmcalcrec.vpmcalcRec, vpmfmtrec);
		if (isNE(vpmfmtrec.statuz, varcom.oK)){
			wsaaStatuz.set(vpmfmtrec.statuz);
			vpmcalcrec.statuz.set(vpmfmtrec.statuz);
			goTo(GotoLabel.exit105);
		}
	}
}

protected void initFormatSubr()
{
	//For determination of calculation method using By External Table Key
	vpmfmtrec.extkey.set(vpmcalcrec.extkey);
	vpmfmtrec.function.set("INIT");
	callProgram(formatSubr, vpmcalcrec.vpmcalcRec, vpmfmtrec);
	
	if (isNE(vpmfmtrec.statuz, varcom.oK)){
		wsaaStatuz.set(vpmfmtrec.statuz);
		vpmcalcrec.statuz.set(vpmfmtrec.statuz);
		goTo(GotoLabel.exit105);
	}
}

protected void callGDE200()
	{
		//Loop to call INIT method of GDE subroutines
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					
				}
				case callAnGDE: {
					callAnGDE210();
				}
				case exit100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callAnGDE210()
	{
		wsaaSub.add(1);
		if (isNE(wsaaStatuz, varcom.oK)){
			goTo(GotoLabel.exit100);
		}
		if (isGT(wsaaSub, MAX_GDE_NUM)){
			goTo(GotoLabel.exit100);
		}
		if (isEQ(tr28xrec.dataextract[wsaaSub.toInt()], SPACE)){
			goTo(GotoLabel.callAnGDE);
		}
		// Call "INIT" function of GDE subroutines
		recordObject = recordMap.get(tr28xrec.dataextract[wsaaSub.toInt()].getData().trim() + "REC");/* IJTI-1386 */
		VPMSUtils.setSubrFunction(recordObject, tr28xrec.func[wsaaSub.toInt()].getData().trim());/* IJTI-1386 */
		callProgram(tr28xrec.dataextract[wsaaSub.toInt()].getData().trim(), vpmcalcrec.vpmcalcRec, recordObject);/* IJTI-1386 */
		wsaaStatuz.set(VPMSUtils.getSubrStatuz(recordObject));
		goTo(GotoLabel.callAnGDE);
		
	}

protected void buildVPMSAttributes300()
	{
		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					
				}
				case buildAnProperty: {
					buildAnProperty310();
				}
				case exit120: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void buildAnProperty310() {
	wsaaSub.add(1);
	if (isNE(wsaaStatuz, varcom.oK)){
		goTo(GotoLabel.exit120);
	}
	if (isGT(wsaaSub, endIdxExtIds)){
		goTo(GotoLabel.exit120);
	}
	if (isEQ(tr28xrec.extfldid[wsaaSub.toInt()], SPACE)){
		goTo(GotoLabel.buildAnProperty);
	}
	wsaaSub1.set(0);
	//reset TR28Y
	tr28yrec.tr28yRec.set(SPACES);
	
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				initialize320();
			}
			case buildAnAttribute: {
				buildAnAttribute330();
			}
			case exit110: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
	initProperty360();
	goTo(GotoLabel.buildAnProperty);

}


private void initialize320() {
	//Read TR28Y table
	readTr28y325();
	wsaaProperty[wsaaSub.toInt()].set(wsaaTr28yVpmsprop);
	wsaaPropCopybook[wsaaSub.toInt()].set(wsaaTr28yCopybk);
	wsaaPropMapField[wsaaSub.toInt()].set(wsaaTr28yMaptofield);
	wsaaPropNoOccurs[wsaaSub.toInt()].set(wsaaTr28yNumoccurs);
}

private void buildAnAttribute330() {
	wsaaSub1.add(1);
	if (isNE(wsaaStatuz, varcom.oK)){
		goTo(GotoLabel.exit110);
	}
	if (isGT(wsaaSub1, MAX_ATTRS_NO)){
		goTo(GotoLabel.exit110);
	}
	if (isEQ(wsaaTr28yAttrLbl[wsaaSub1.toInt()], SPACE)){
		goTo(GotoLabel.buildAnAttribute);
	}
	readTr28z335();
	initAttributeValues340();
		
	//reset TR28Y
	tr28zrec.tr28zRec.set(SPACES);
	goTo(GotoLabel.buildAnAttribute);
	
}

private void initAttributeValues340() {
	wsaaDataExtSubr.set(tr28zrec.dataextract);
	wsaaFunc.set(tr28zrec.function);
	wsaaCopybook.set(tr28zrec.copybk);
	wsaaMapField.set(tr28zrec.maptofield);
	wsaaNumOccurs.set(tr28zrec.numoccurs.getData());/* IJTI-1386 */

	//By Constant Value
	if ("*CONSTANT".equalsIgnoreCase(wsaaCopybook.getData().trim())){/* IJTI-1386 */
		if (isEQ(wsaaNumOccurs, ZERO)){
			wsaaAttrReturnValue[wsaaSub1.toInt()][1].set(wsaaMapField);
		} else {
			for (wsbbSub.set(1);! isGT(wsbbSub, wsaaNumOccurs); wsbbSub.add(1)){
				wsaaAttrReturnValue[wsaaSub1.toInt()][wsbbSub.toInt()] = wsaaMapField;
			}
		}
	}

	//Use xxxFORMAT subroutine to get data
	else if (formatSubr.equalsIgnoreCase(wsaaDataExtSubr.getData().trim())){/* IJTI-1386 */

		//By Copybook (eg PREMIUMREC)
		vpmfmtrec.function.set(wsaaFunc);
		vpmfmtrec.copybook.set(wsaaCopybook);
		if (isEQ(wsaaNumOccurs, ZERO)){
			vpmfmtrec.resultField.set(wsaaMapField.getData().trim());/* IJTI-1386 */
			callProgram(formatSubr, vpmcalcrec.vpmcalcRec, vpmfmtrec);
			wsaaStatuz.set(vpmfmtrec.statuz);
			wsaaAttrReturnValue[wsaaSub1.toInt()][1].set(vpmfmtrec.resultValue);
		} else {
			for (wsbbSub.set(1);!isGT(wsbbSub, wsaaNumOccurs) && isEQ(wsaaStatuz, varcom.oK); wsbbSub.add(1)){
				vpmfmtrec.resultField.set(wsaaMapField.getData().trim() + OPEN_SQUARE + (wsbbSub.toInt())+ CLOSE_SQUARE);/* IJTI-1386 */
				callProgram(formatSubr, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				wsaaStatuz.set(vpmfmtrec.statuz);
				wsaaAttrReturnValue[wsaaSub1.toInt()][wsbbSub.toInt()].set(vpmfmtrec.resultValue);
			}
		}
	}
	// By GDE Routine copybook
	else{
		recordObject = recordMap.get(wsaaCopybook.getData().trim());/* IJTI-1386 */
		if (recordObject == null){
			wsaaStatuz.set(rflk);
		} else {
			VPMSUtils.setSubrFunction(recordObject, "GETV");
			String fieldName = "";
			if (isEQ(wsaaNumOccurs, ZERO)){
				fieldName = wsaaMapField.getData().trim();/* IJTI-1386 */
				VPMSUtils.setSubrResultField(recordObject, fieldName);
				callProgram(wsaaDataExtSubr, vpmcalcrec.vpmcalcRec, recordObject);
				wsaaStatuz.set(VPMSUtils.getSubrStatuz(recordObject));
				wsaaAttrReturnValue[wsaaSub1.toInt()][1].set((FixedLengthStringData)VPMSUtils.getSubrResultValue(recordObject));
			} else {
				for (wsbbSub.set(1);! isGT(wsbbSub, wsaaNumOccurs) && isEQ(wsaaStatuz, varcom.oK); wsbbSub.add(1)){
					fieldName = wsaaMapField.getData().trim() +  OPEN_SQUARE + wsbbSub.toInt() + CLOSE_SQUARE;/* IJTI-1386 */
					VPMSUtils.setSubrResultField(recordObject, fieldName);
					callProgram(wsaaDataExtSubr, vpmcalcrec.vpmcalcRec, recordObject);
					wsaaStatuz.set(VPMSUtils.getSubrStatuz(recordObject));
					wsaaAttrReturnValue[wsaaSub1.toInt()][wsbbSub.toInt()].set((FixedLengthStringData)VPMSUtils.getSubrResultValue(recordObject));
				}
			}
		}
	}
	if (isEQ(wsaaStatuz, varcom.oK)){
		populateAttributeMap350();
	}

	wsaaDataExtSubr.set(SPACE);
	wsaaFunc.set(SPACE);
	wsaaCopybook.set(SPACE);
	wsaaMapField.set(SPACE);
	for (wsbbSub.set(1);! isGT(wsbbSub, wsaaNumOccurs); wsbbSub.add(1)){
		wsaaAttrReturnValue[wsaaSub1.toInt()][wsbbSub.toInt()].set(SPACE);
	}
	wsaaNumOccurs.set(ZERO);
}

private void populateAttributeMap350(){
	if (isEQ(wsaaNumOccurs, ZERO)){
		String attrName = wsaaTr28yAttrLbl[wsaaSub1.toInt()].getData().trim();/* IJTI-1386 */
		inputAttrsMap.put(attrName, wsaaAttrReturnValue[wsaaSub1.toInt()][1].getData().trim());/* IJTI-1386 */
		//Save attributes map for logging DB
		attrsNameValueMap.put(attrName, wsaaAttrReturnValue[wsaaSub1.toInt()][1].getData().trim());/* IJTI-1386 */
	} else {
		for (wsbbSub.set(1);! isGT(wsbbSub, wsaaNumOccurs); wsbbSub.add(1)){
			String attrName = wsaaTr28yAttrLbl[wsaaSub1.toInt()].getData().trim() + OPEN_SQUARE + wsbbSub.toInt() + CLOSE_SQUARE;/* IJTI-1386 */
			inputAttrsMap.put(attrName, wsaaAttrReturnValue[wsaaSub1.toInt()][wsbbSub.toInt()] .getData().trim());/* IJTI-1386 */
			//Save attributes map for logging DB
			attrsNameValueMap.put(attrName, wsaaAttrReturnValue[wsaaSub1.toInt()][wsbbSub.toInt()] .getData().trim());/* IJTI-1386 */
		}
	}
}

private void initProperty360() {
	//Add to output set
	String propertyName = wsaaTr28yVpmsprop.getData().trim();/* IJTI-1386 */
	if (isEQ(wsaaTr28yNumoccurs, ZERO)){
		outputPropertySet.add(propertyName);
		//Save map for logging DB
		propsAttrsMap.put(propertyName, attrsNameValueMap);
	} else {
		for (wsbbSub.set(1); ! isGT(wsbbSub, wsaaTr28yNumoccurs); wsbbSub.add(1)){
			outputPropertySet.add(propertyName+ OPEN_PARENTHESIS + wsbbSub.toInt() + CLOSE_PARENTHESIS);
			//Save map for logging DB
			propsAttrsMap.put(propertyName+ OPEN_PARENTHESIS + wsbbSub.toInt() + CLOSE_PARENTHESIS, attrsNameValueMap);
		}
	}
	attrsNameValueMap = new LinkedHashMap<String, String>();
}

protected void callVPMSAPI400() 
	{
		Map<String, String> calcMap = new HashMap<String, String>();
		resultsMap.clear();
		try{
			resultsMap = VPMSUtils.callVPMSAPI(appVars, inputAttrsMap, calcMap, tr28xrec.vpmmodel.getData().trim(), outputPropertySet);/* IJTI-1386 */
		}//IJTI-851-Overly Broad Catch
		catch (ExitSection e) {
			wsaaStatuz.set(e211);
		}
		if (isEQ(tr28xrec.dataLog, "Y")){
			logVpmsData600();
		}
		//Reset map
		propsAttrsMap = new LinkedHashMap<String, LinkedHashMap>();
		
		for (ComputeResult result : resultsMap.values()) {
			if (!RETURN_OK.equals(result.getReturnCode())){
				wsaaStatuz.set(e211);
				LOGGER.error("VPMS error code: ", result.getReturnCode());//IJTI-1561
				LOGGER.error("VPMS error message: ", result.getMessage());//IJTI-1561
				break;
			}
		}
	}

protected void retrieveVPMSCalcResults500()
	{
		for (int loopVar1 = startIdxExtIds; loopVar1 <= endIdxExtIds  && isEQ(wsaaStatuz, varcom.oK); loopVar1 += 1){
			String copyBook = wsaaPropCopybook[loopVar1].getData().trim();/* IJTI-1386 */
			if (isNE(wsaaProperty[loopVar1], SPACE)){
				if (isEQ(wsaaPropNoOccurs[loopVar1], ZERO)){
					String field = wsaaPropMapField[loopVar1].getData().trim();/* IJTI-1386 */
					String property = wsaaProperty[loopVar1].getData().trim();/* IJTI-1386 */
					
					updateRecord510(copyBook, field, property);					
				}
				else {
					for (wsbbSub.set(1); ! isGT(wsbbSub, wsaaPropNoOccurs[loopVar1])  && isEQ(wsaaStatuz, varcom.oK); wsbbSub.add(1)){
						String field = wsaaPropMapField[loopVar1].getData().trim()+ OPEN_SQUARE + wsbbSub.toInt() + CLOSE_SQUARE;/* IJTI-1386 */
						String property = wsaaProperty[loopVar1].getData().trim()+ OPEN_PARENTHESIS + wsbbSub.toInt() + CLOSE_PARENTHESIS;/* IJTI-1386 */
						
						updateRecord510(copyBook, field, property);
					}
				}
			}
		}
		//Tag <005>
		inputAttrsMap.clear();
		outputPropertySet.clear();
//		resultsMap.clear();
	}

protected void updateRecord510(String copyBook, String field, String property){
	ComputeResult comResult = resultsMap.get(property);
	String propValue = comResult.getValue();
	vpmfmtrec.function.set("UPDV");
	vpmfmtrec.copybook.set(copyBook);
	vpmfmtrec.resultField.set(field);
	vpmfmtrec.resultValue.set(propValue);
	
	//Update VPMS return values to main copybook
	if (isEQ(copyBook, tr28xrec.linkcopybk)){
		callProgram(formatSubr, vpmcalcrec.vpmcalcRec, vpmfmtrec);
		wsaaStatuz.set(vpmfmtrec.statuz);
	} 
	else {
		//Update VPMS return values to other records, normally VPMFMTREC
		recordObject = recordMap.get(copyBook);
		if (recordObject == null){
			wsaaStatuz.set(rflk);
		} else {
			callProgram(formatSubr, recordObject, vpmfmtrec);
			wsaaStatuz.set(vpmfmtrec.statuz);
		}
	}
}

protected void logVpmsData600()
{
	Iterator entries = propsAttrsMap.entrySet().iterator();
	while (entries.hasNext()) {
		Entry<String, LinkedHashMap> thisEntry = (Entry) entries.next();
		String property = thisEntry.getKey();
		LinkedHashMap<String, String> attrNameValueMap = thisEntry.getValue();
		ComputeResult comResult = resultsMap.get(property);
		if (comResult == null){
			wsaaStatuz.set(e211);
			break;
		}
		
		String logVpmHandl = "0";//TODO 
		String logVpmsKey = vpmcalcrec.vpmscoy.getData()+ "TR28X" + vpmcalcrec.extkey.getData().trim();/* IJTI-1386 */
		String logVpmModel = tr28xrec.vpmmodel.getData().trim();/* IJTI-1386 */
		String logVpmFunc;
		String logVpmField;
		String logVpmValue;
		String logVpmStat;
		String logVpmRetcd;
		String logVpmMesg = "";
		
		if (RETURN_OK.equals(comResult.getReturnCode())){
			logVpmStat = "****";
			logVpmRetcd = RETURN_OK;
			//Loop to log all Attributes
			for (Map.Entry<String,String> attr : attrNameValueMap.entrySet()){
				logVpmFunc = SET_ATTR;
				logVpmField = attr.getKey();
				logVpmValue = attr.getValue();
				callLogVPMSDB(logVpmHandl, logVpmsKey, logVpmModel, logVpmFunc, logVpmField, 
						logVpmValue, logVpmStat, logVpmRetcd, logVpmMesg);
			}
			//Log Property
			logVpmFunc = COMPUTE;
			logVpmField = property;
			logVpmValue = comResult.getValue();
			callLogVPMSDB(logVpmHandl, logVpmsKey, logVpmModel, logVpmFunc, logVpmField, 
						logVpmValue, logVpmStat, logVpmRetcd, logVpmMesg);
		} else {
			logVpmStat = "FAIL";//TODO
			logVpmRetcd = comResult.getReturnCode();
			//Loop to log all Attributes
			for (Map.Entry<String,String> attr : attrNameValueMap.entrySet()){
				logVpmFunc = SET_ATTR;
				logVpmField = attr.getKey();
				logVpmValue = attr.getValue();
				if (logVpmField.equalsIgnoreCase(comResult.getReferenceField())){
					logVpmMesg = "Error in this field. Message: " + comResult.getMessage();
				} else {
					logVpmMesg = "";
				}
				callLogVPMSDB(logVpmHandl, logVpmsKey, logVpmModel, logVpmFunc, logVpmField, 
						logVpmValue, logVpmStat, logVpmRetcd, logVpmMesg);
			}
			//Log Property
			logVpmFunc = COMPUTE;
			logVpmField = property;
			logVpmValue = comResult.getValue();
			if (!StringUtils.isEmpty(comResult.getReferenceField())){
				logVpmMesg = "Error in field: " + comResult.getReferenceField() + ". Message: " + comResult.getMessage();
			}
			callLogVPMSDB(logVpmHandl, logVpmsKey, logVpmModel, logVpmFunc, logVpmField, 
					logVpmValue, logVpmStat, logVpmRetcd, logVpmMesg);			
		}
	}
	if (isEQ(wsaaStatuz, varcom.oK)){
		appVars.commit();
	}
//	}else {
//		rollback();
//	}

}


protected void callLogVPMSDB(String logVpmHandl, String logVpmsKey, String logVpmModel, String logVpmFunc, String logVpmField,
			String logVpmValue, String logVpmStat, String logVpmRetcd, String logVpmMesg) 
{
	if (isEQ(wsaaStatuz, varcom.oK)){
		vpmdIO.setVpmhandl(logVpmHandl);
		vpmdIO.setVpmskey(logVpmsKey);
		vpmdIO.setVpmsmodel(logVpmModel);
		vpmdIO.setVpmfunc(logVpmFunc);
		vpmdIO.setVpmfield(logVpmField);
		vpmdIO.setVpmvalue(logVpmValue);
		vpmdIO.setVpmstat(logVpmStat);
		vpmdIO.setVpmretcd(logVpmRetcd);
		vpmdIO.setVpmmesg(logVpmMesg);
		vpmdIO.setFunction(varcom.writr);
		vpmdIO.setFormat(vpmdrec);
		SmartFileCode.execute(appVars, vpmdIO);
		if (isNE(vpmdIO.getStatuz(),varcom.oK)) {
			wsaaStatuz.set(varcom.bomb);
		}
	}
}

protected void callDataUpdates700()
{
	//loop to some Data Updates subroutines
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				
			}
			case callAnUpdate710: {
				callAnUpdate710();
			}
			case exit720: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void callAnUpdate710()
{
	wsaaSub2.add(1);
	if (isNE(wsaaStatuz, varcom.oK)){
		goTo(GotoLabel.exit720);
	}
	if (isGT(wsaaSub2, MAX_UPDATE_NUM)){
		goTo(GotoLabel.exit720);
	}
	if (isEQ(tr28xrec.dataupdate[wsaaSub2.toInt()], SPACE)){
		goTo(GotoLabel.callAnUpdate710);
	}
	callProgram(tr28xrec.dataupdate[wsaaSub2.toInt()].getData().trim(), vpmcalcrec.vpmcalcRec, vpmfmtrec);/* IJTI-1386 */
	goTo(GotoLabel.callAnUpdate710);
}

protected void readTr28x130()
{
	itemIO.setParams(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(vpmcalcrec.vpmscoy);
	itemIO.setItemtabl(tr28x);
	itemIO.setItemitem(vpmcalcrec.vpmsitem);
	itemIO.setFormat(itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		wsaaStatuz.set(rflg);
		vpmcalcrec.statuz.set(rflg);
		goTo(GotoLabel.exit105);
	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
		tr28xrec.tr28xRec.set(SPACES);
	}
	else {
		tr28xrec.tr28xRec.set(itemIO.getGenarea());
	}
}

protected void readTr28y325()
{
	wsaaItemseq.set(0);
	readTr28yFirstPage326();
	processTr28yPage327();
	wsaaItemseq.set(1);
	while ( !(isEQ(wsaaItemseq, MAX_PAGE_TR28Y))) {
		readTr28yPage328();
		processTr28yPage327();
		wsaaItemseq.add(1);
	}
}

protected void readTr28yFirstPage326()
{
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(vpmcalcrec.vpmscoy);
	itemIO.setItemtabl(tr28y);
	itemIO.setItemitem(tr28xrec.extfldid[wsaaSub.toInt()]);
	itemIO.setFormat(itemrec);
	itemIO.setItemseq(SPACES);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
			|| isNE(itemIO.getItemcoy(), vpmcalcrec.vpmscoy)
			|| isNE(itemIO.getItemitem(), tr28xrec.extfldid[wsaaSub.toInt()])
			|| isNE(itemIO.getItemtabl(), tr28y)) {
		wsaaStatuz.set(rfli);
		goTo(GotoLabel.exit110);
	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
		tr28yrec.tr28yRec.set(SPACES);
		goTo(GotoLabel.exit110);
	}
	else {
		tr28yrec.tr28yRec.set(itemIO.getGenarea());
		
		wsaaTr28yVpmsprop.set(tr28yrec.vpmsprop);
		wsaaTr28yCopybk.set(tr28yrec.copybk);
		wsaaTr28yMaptofield.set(tr28yrec.maptofield);
		wsaaTr28yNumoccurs.set(tr28yrec.numoccurs);
	}
}

protected void readTr28yPage328()
{
	itemIO.setItemseq(wsaaItemseq);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
			|| isNE(itemIO.getItemcoy(), vpmcalcrec.vpmscoy)
			|| isNE(itemIO.getItemitem(), tr28xrec.extfldid[wsaaSub.toInt()])
			|| isNE(itemIO.getItemtabl(), tr28y)) {
	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
		tr28yrec.tr28yRec.set(SPACES);
	}
	else {
		tr28yrec.tr28yRec.set(itemIO.getGenarea());
	}
}

protected void processTr28yPage327()
{
	int startIndex = 4 * wsaaItemseq.toInt(); 
	for (int i = 1; i < 5; i++){
		wsaaTr28yAttrLbl[startIndex + i].set(tr28yrec.attrslbl[i]);
		wsaaTr28yAttrValue[startIndex + i].set(tr28yrec.attrsvalue[i]);
	}
	tr28yrec.tr28yRec.set(SPACES);
}

protected void readTr28z335()
{
	itemIO.setParams(SPACE);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(vpmcalcrec.vpmscoy);
	itemIO.setItemtabl(tr28z);
	itemIO.setItemitem(wsaaTr28yAttrValue[wsaaSub1.toInt()]);
	itemIO.setFormat(itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		wsaaStatuz.set(rflj);
		tr28zrec.tr28zRec.set(itemIO.getGenarea());
		goTo(GotoLabel.buildAnAttribute);
	}
	tr28zrec.tr28zRec.set(itemIO.getGenarea());
}
}
