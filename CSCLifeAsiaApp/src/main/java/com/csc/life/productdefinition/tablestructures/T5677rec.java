package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:53
 * Description:
 * Copybook name: T5677REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5677rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5677Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData fupcdess = new FixedLengthStringData(64).isAPartOf(t5677Rec, 0);
  	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(16, 4, fupcdess, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(64).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
  	public FixedLengthStringData fupcdes01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData fupcdes02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData fupcdes03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData fupcdes04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData fupcdes05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData fupcdes06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData fupcdes07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData fupcdes08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData fupcdes09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData fupcdes10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData fupcdes11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData fupcdes12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData fupcdes13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData fupcdes14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData fupcdes15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData fupcdes16 = new FixedLengthStringData(4).isAPartOf(filler, 60);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(t5677Rec, 64);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(435).isAPartOf(t5677Rec, 65, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5677Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5677Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}