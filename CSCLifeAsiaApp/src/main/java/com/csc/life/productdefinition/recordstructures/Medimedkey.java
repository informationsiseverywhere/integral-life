package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:43
 * Description:
 * Copybook name: MEDIMEDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Medimedkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData medimedFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData medimedKey = new FixedLengthStringData(256).isAPartOf(medimedFileKey, 0, REDEFINE);
  	public FixedLengthStringData medimedExmcode = new FixedLengthStringData(10).isAPartOf(medimedKey, 0);
  	public FixedLengthStringData medimedInvref = new FixedLengthStringData(15).isAPartOf(medimedKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(231).isAPartOf(medimedKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(medimedFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		medimedFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}