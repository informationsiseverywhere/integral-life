package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Vpxmatcrec extends ExternalData {

	public FixedLengthStringData vpxmatcRec = new FixedLengthStringData(196);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxmatcRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxmatcRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxmatcRec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxmatcRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(18).isAPartOf(vpxmatcRec, 136);
  	//public ZonedDecimalData curuntbal = new ZonedDecimalData(18, 5).isAPartOf(dataRec, 0);
  	//ILIFE-2328
  	public PackedDecimalData curuntbal = new PackedDecimalData(16, 5).isAPartOf(dataRec, 0);
    //ILIFE-7396 start
  	public PackedDecimalData intBonusAcbl = new PackedDecimalData(17, 2).isAPartOf(vpxmatcRec, 154);
  	public PackedDecimalData extBonusAcbl = new PackedDecimalData(17, 2).isAPartOf(vpxmatcRec, 163);
  	public PackedDecimalData trmBonusAcbl = new PackedDecimalData(17, 2).isAPartOf(vpxmatcRec, 172);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(vpxmatcRec, 181);
  	public PackedDecimalData coverCount = new PackedDecimalData(3, 0).isAPartOf(vpxmatcRec, 190);
  	public PackedDecimalData lage = new PackedDecimalData(3, 0).isAPartOf(vpxmatcRec, 192);
  	public FixedLengthStringData lsex = new FixedLengthStringData(1).isAPartOf(vpxmatcRec, 194);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(vpxmatcRec, 195);
  	//ILIFE-7396 end
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxmatcRec);
	}	
	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxmatcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
