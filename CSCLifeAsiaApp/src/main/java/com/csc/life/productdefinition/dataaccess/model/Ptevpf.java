package com.csc.life.productdefinition.dataaccess.model;

public class Ptevpf {
    public String chdrcoy;
    public String chdrnum;
    public int effdate;
    public int ptrneff;
    public int tranno;
    public String validflag;
    public int batcactyr;
    public int batcactmn;
    public String batctrcde;
    public String userProfile;
    public String jobName;
    public String datime;

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public int getEffdate() {
        return effdate;
    }

    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }

    public int getPtrneff() {
        return ptrneff;
    }

    public void setPtrneff(int ptrneff) {
        this.ptrneff = ptrneff;
    }

    public int getTranno() {
        return tranno;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public int getBatcactyr() {
        return batcactyr;
    }

    public void setBatcactyr(int batcactyr) {
        this.batcactyr = batcactyr;
    }

    public int getBatcactmn() {
        return batcactmn;
    }

    public void setBatcactmn(int batcactmn) {
        this.batcactmn = batcactmn;
    }

    public String getBatctrcde() {
        return batctrcde;
    }

    public void setBatctrcde(String batctrcde) {
        this.batctrcde = batctrcde;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }

}