package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr53bscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr53bScreenVars sv = (Sr53bScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr53bscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr53bScreenVars screenVars = (Sr53bScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.scheduleName.setClassString("");
		screenVars.acctmonth.setClassString("");
		screenVars.scheduleNumber.setClassString("");
		screenVars.acctyear.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.bcompany.setClassString("");
		screenVars.jobq.setClassString("");
		screenVars.bbranch.setClassString("");
		screenVars.worku.setClassString("");
		screenVars.extrval.setClassString("");
		screenVars.contractType.setClassString("");
		screenVars.cntdesc.setClassString("");
		screenVars.cnttyp.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.crtable01.setClassString("");
		screenVars.crcode01.setClassString("");
		screenVars.crtabled01.setClassString("");
		screenVars.crtable02.setClassString("");
		screenVars.crcode02.setClassString("");
		screenVars.crtabled02.setClassString("");
		screenVars.crtable03.setClassString("");
		screenVars.crcode03.setClassString("");
		screenVars.crtabled03.setClassString("");
		screenVars.crtable04.setClassString("");
		screenVars.crcode04.setClassString("");
		screenVars.crtabled04.setClassString("");
		screenVars.crtable05.setClassString("");
		screenVars.crcode05.setClassString("");
		screenVars.crtabled05.setClassString("");
		screenVars.crtable06.setClassString("");
		screenVars.crcode06.setClassString("");
		screenVars.crtabled06.setClassString("");
		screenVars.crtable07.setClassString("");
		screenVars.crcode07.setClassString("");
		screenVars.crtabled07.setClassString("");
		screenVars.crtable08.setClassString("");
		screenVars.crcode08.setClassString("");
		screenVars.crtabled08.setClassString("");
		screenVars.crtable09.setClassString("");
		screenVars.crcode09.setClassString("");
		screenVars.crtabled09.setClassString("");
		screenVars.crtable10.setClassString("");
		screenVars.crcode10.setClassString("");
		screenVars.crtabled10.setClassString("");
	}

/**
 * Clear all the variables in Sr53bscreen
 */
	public static void clear(VarModel pv) {
		Sr53bScreenVars screenVars = (Sr53bScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.scheduleName.clear();
		screenVars.acctmonth.clear();
		screenVars.scheduleNumber.clear();
		screenVars.acctyear.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.bcompany.clear();
		screenVars.jobq.clear();
		screenVars.bbranch.clear();
		screenVars.worku.clear();
		screenVars.extrval.clear();
		screenVars.contractType.clear();
		screenVars.cntdesc.clear();
		screenVars.cnttyp.clear();
		screenVars.ctypdesc.clear();
		screenVars.crtable01.clear();
		screenVars.crcode01.clear();
		screenVars.crtabled01.clear();
		screenVars.crtable02.clear();
		screenVars.crcode02.clear();
		screenVars.crtabled02.clear();
		screenVars.crtable03.clear();
		screenVars.crcode03.clear();
		screenVars.crtabled03.clear();
		screenVars.crtable04.clear();
		screenVars.crcode04.clear();
		screenVars.crtabled04.clear();
		screenVars.crtable05.clear();
		screenVars.crcode05.clear();
		screenVars.crtabled05.clear();
		screenVars.crtable06.clear();
		screenVars.crcode06.clear();
		screenVars.crtabled06.clear();
		screenVars.crtable07.clear();
		screenVars.crcode07.clear();
		screenVars.crtabled07.clear();
		screenVars.crtable08.clear();
		screenVars.crcode08.clear();
		screenVars.crtabled08.clear();
		screenVars.crtable09.clear();
		screenVars.crcode09.clear();
		screenVars.crtabled09.clear();
		screenVars.crtable10.clear();
		screenVars.crcode10.clear();
		screenVars.crtabled10.clear();
	}
}
