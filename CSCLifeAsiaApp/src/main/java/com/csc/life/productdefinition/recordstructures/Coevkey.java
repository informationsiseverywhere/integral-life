package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:12
 * Description:
 * Copybook name: COEVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Coevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData coevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData coevKey = new FixedLengthStringData(64).isAPartOf(coevFileKey, 0, REDEFINE);
  	public FixedLengthStringData coevChdrcoy = new FixedLengthStringData(1).isAPartOf(coevKey, 0);
  	public FixedLengthStringData coevChdrnum = new FixedLengthStringData(8).isAPartOf(coevKey, 1);
  	public FixedLengthStringData coevLife = new FixedLengthStringData(2).isAPartOf(coevKey, 9);
  	public FixedLengthStringData coevCoverage = new FixedLengthStringData(2).isAPartOf(coevKey, 11);
  	public FixedLengthStringData coevRider = new FixedLengthStringData(2).isAPartOf(coevKey, 13);
  	public PackedDecimalData coevEffdate = new PackedDecimalData(8, 0).isAPartOf(coevKey, 15);
  	public PackedDecimalData coevCurrfrom = new PackedDecimalData(8, 0).isAPartOf(coevKey, 20);
  	public FixedLengthStringData coevValidflag = new FixedLengthStringData(1).isAPartOf(coevKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(coevKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(coevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		coevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}