package com.csc.life.productdefinition.dataaccess.dao;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MrtpfDAO extends BaseDAO  {
 
	public Integer getMBNSData(String chdrnum, String yrsinf);

}
