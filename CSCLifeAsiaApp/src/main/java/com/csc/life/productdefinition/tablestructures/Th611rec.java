package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:37
 * Description:
 * Copybook name: TH611REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th611rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th611Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cmaxs = new FixedLengthStringData(136).isAPartOf(th611Rec, 0);
  	public ZonedDecimalData[] cmax = ZDArrayPartOfStructure(8, 17, 2, cmaxs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(136).isAPartOf(cmaxs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cmax01 = new ZonedDecimalData(17, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData cmax02 = new ZonedDecimalData(17, 2).isAPartOf(filler, 17);
  	public ZonedDecimalData cmax03 = new ZonedDecimalData(17, 2).isAPartOf(filler, 34);
  	public ZonedDecimalData cmax04 = new ZonedDecimalData(17, 2).isAPartOf(filler, 51);
  	public ZonedDecimalData cmax05 = new ZonedDecimalData(17, 2).isAPartOf(filler, 68);
  	public ZonedDecimalData cmax06 = new ZonedDecimalData(17, 2).isAPartOf(filler, 85);
  	public ZonedDecimalData cmax07 = new ZonedDecimalData(17, 2).isAPartOf(filler, 102);
  	public ZonedDecimalData cmax08 = new ZonedDecimalData(17, 2).isAPartOf(filler, 119);
  	public FixedLengthStringData cmins = new FixedLengthStringData(136).isAPartOf(th611Rec, 136);
  	public ZonedDecimalData[] cmin = ZDArrayPartOfStructure(8, 17, 2, cmins, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(136).isAPartOf(cmins, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cmin01 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData cmin02 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 17);
  	public ZonedDecimalData cmin03 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 34);
  	public ZonedDecimalData cmin04 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 51);
  	public ZonedDecimalData cmin05 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 68);
  	public ZonedDecimalData cmin06 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData cmin07 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 102);
  	public ZonedDecimalData cmin08 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 119);
  	public FixedLengthStringData frequencys = new FixedLengthStringData(16).isAPartOf(th611Rec, 272);
  	public FixedLengthStringData[] frequency = FLSArrayPartOfStructure(8, 2, frequencys, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(frequencys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData frequency01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData frequency02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData frequency03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData frequency04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData frequency05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData frequency06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData frequency07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData frequency08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(212).isAPartOf(th611Rec, 288, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th611Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th611Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}