/*
 * File: Pr526.java
 * Date: 30 August 2009 1:38:28
 * Author: Quipoz Limited
 * 
 * Class transformed from PR526.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.productdefinition.dataaccess.MediivcTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.screens.Sr526ScreenVars;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Medical Bill Scrolled By Invoice Number
*
*
***********************************************************************
* </pre>
*/
public class Pr526 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR526");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaPrevZdocno = new FixedLengthStringData(15);
	private String wsaaLargeName = "LGNMS";
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaEntityDesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSubfileCnt = new ZonedDecimalData(5, 0).setUnsigned();
		/* FORMATS */
	private String agntrec = "AGNTREC";
	private String mediivcrec = "MEDIIVCREC";
	private String zmpvrec = "ZMPVREC";
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Medical Bill Paymt Dtls Inv No & Chdr*/
	private MediivcTableDAM mediivcIO = new MediivcTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Sr526ScreenVars sv = ScreenProgram.getScreenVars( Sr526ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1110, 
		exit1111, 
		preExit, 
		exit2090
	}

	public Pr526() {
		super();
		screenVars = sv;
		new ScreenModel("Sr526", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaPrevZdocno.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		sv.zdocno.set(wsspwindow.value);
		loadSubfile1100();
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void loadSubfile1100()
	{
		start1100();
	}

protected void start1100()
	{
		wsaaSubfileCnt.set(1);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		mediivcIO.setDataArea(SPACES);
		mediivcIO.setStatuz(varcom.oK);
		mediivcIO.setChdrcoy(wsspcomn.company);
		mediivcIO.setInvref(sv.zdocno);
		mediivcIO.setChdrnum(SPACES);
		mediivcIO.setSeqno(ZERO);
		mediivcIO.setFormat(mediivcrec);
		mediivcIO.setFunction(varcom.begn);
		while ( !(isEQ(mediivcIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			loopMediivc1110();
		}
		
		if (isEQ(mediivcIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		if (isEQ(scrnparams.subfileRrn,ZERO)) {
			sv.initialiseSubfileArea();
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			scrnparams.function.set(varcom.sadd);
			screenIo9000();
		}
	}

protected void loopMediivc1110()
	{
		try {
			start1110();
			nextr1110();
		}
		catch (GOTOException e){
		}
	}

protected void start1110()
	{
		SmartFileCode.execute(appVars, mediivcIO);
		if (isNE(mediivcIO.getStatuz(),varcom.oK)
		&& isNE(mediivcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mediivcIO.getStatuz());
			syserrrec.params.set(mediivcIO.getParams());
			fatalError600();
		}
		if (isNE(mediivcIO.getStatuz(),varcom.oK)
		|| isNE(mediivcIO.getChdrcoy(),mediivcIO.getChdrcoy())) {
			mediivcIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1110);
		}
		sv.invref.set(mediivcIO.getInvref());
		sv.chdrnum.set(mediivcIO.getChdrnum());
		sv.exmcode.set(mediivcIO.getExmcode());
		getEntityDesc1111();
		sv.longdesc.set(wsaaEntityDesc);
		sv.selectOut[varcom.nd.toInt()].set("N");
		sv.selectOut[varcom.pr.toInt()].set("N");
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaSubfileCnt.add(1);
	}

protected void nextr1110()
	{
		mediivcIO.setFunction(varcom.nextr);
	}

protected void getEntityDesc1111()
	{
		try {
			start1111();
		}
		catch (GOTOException e){
		}
	}

protected void start1111()
	{
		wsaaClntnum.set(SPACES);
		if (isEQ(mediivcIO.getPaidby(),"A")){
			agntClntnum1112();
		}
		else if (isEQ(mediivcIO.getPaidby(),"D")){
			medProvClntnum1113();
		}
		else if (isEQ(mediivcIO.getPaidby(),"C")){
			wsaaClntnum.set(sv.exmcode);
		}
		if (isEQ(wsaaClntnum,SPACES)) {
			wsaaEntityDesc.set(SPACES);
			goTo(GotoLabel.exit1111);
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.statuz.set(varcom.oK);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
		wsaaEntityDesc.set(namadrsrec.name);
	}

protected void agntClntnum1112()
	{
		start1112();
	}

protected void start1112()
	{
		agntIO.setDataArea(SPACES);
		agntIO.setStatuz(varcom.oK);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(sv.exmcode);
		agntIO.setFormat(agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isEQ(agntIO.getStatuz(),varcom.oK)) {
			wsaaClntnum.set(agntIO.getClntnum());
		}
	}

protected void medProvClntnum1113()
	{
		start1113();
	}

protected void start1113()
	{
		zmpvIO.setDataArea(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.exmcode);
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)
		&& isNE(zmpvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpvIO.getStatuz(),varcom.oK)) {
			wsaaClntnum.set(zmpvIO.getZmednum());
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.zdocno,wsaaPrevZdocno)) {
			wsaaPrevZdocno.set(sv.zdocno);
			loadSubfile1100();
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			roluSubfile2100();
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void roluSubfile2100()
	{
		/*START*/
		wsaaSubfileCnt.set(1);
		while ( !(isEQ(mediivcIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			loopMediivc1110();
		}
		
		if (isEQ(mediivcIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		/*VALIDATION*/
		if (isNE(sv.select,SPACES)) {
			wsspwindow.value.set(sv.invref);
		}
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(SPACES);
		scrnparams.function.set("HIDEW");
		processScreen("SR526", sv);
		/*EXIT*/
	}

protected void screenIo9000()
	{
		/*START*/
		processScreen("SR526", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
