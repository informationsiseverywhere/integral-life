package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import com.csc.life.productdefinition.dataaccess.model.MerxTemppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br649DAO extends BaseDAO<MerxTemppf> {
	public List<MerxTemppf> loadDataByBatch(int batchID);
	public int populateBr649Temp(int batchExtractSize, String merxtempTable);
}

