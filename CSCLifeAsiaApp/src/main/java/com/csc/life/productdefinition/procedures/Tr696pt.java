/*
 * File: Tr696pt.java
 * Date: 30 August 2009 2:44:22
 * Author: Quipoz Limited
 * 
 * Class transformed from TR696PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr696rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR696.
*
*
*****************************************************************
* </pre>
*/
public class Tr696pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Coverage With Different SA Bands               SR696");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(25);
	private FixedLengthStringData filler9 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Simplified Code    :");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 23);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(73);
	private FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(65).isAPartOf(wsaaPrtLine005, 8, FILLER).init("Sum Assured Band         From Sum Assured          To Sum Assured");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler12 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 15);
	private FixedLengthStringData filler13 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler15 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 15);
	private FixedLengthStringData filler16 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler18 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 15);
	private FixedLengthStringData filler19 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler21 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 15);
	private FixedLengthStringData filler22 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler24 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 15);
	private FixedLengthStringData filler25 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler27 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 15);
	private FixedLengthStringData filler28 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(73);
	private FixedLengthStringData filler30 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 15);
	private FixedLengthStringData filler31 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(73);
	private FixedLengthStringData filler33 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 15);
	private FixedLengthStringData filler34 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(73);
	private FixedLengthStringData filler36 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 15);
	private FixedLengthStringData filler37 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(73);
	private FixedLengthStringData filler39 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 15);
	private FixedLengthStringData filler40 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine015, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 31).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(28);
	private FixedLengthStringData filler42 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr696rec tr696rec = new Tr696rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr696pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr696rec.tr696Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tr696rec.simcrtable);
		fieldNo009.set(tr696rec.frsumins01);
		fieldNo010.set(tr696rec.tosumins01);
		fieldNo008.set(tr696rec.saband01);
		fieldNo013.set(tr696rec.tosumins02);
		fieldNo016.set(tr696rec.tosumins03);
		fieldNo019.set(tr696rec.tosumins04);
		fieldNo022.set(tr696rec.tosumins05);
		fieldNo025.set(tr696rec.tosumins06);
		fieldNo028.set(tr696rec.tosumins07);
		fieldNo031.set(tr696rec.tosumins08);
		fieldNo034.set(tr696rec.tosumins09);
		fieldNo037.set(tr696rec.tosumins10);
		fieldNo012.set(tr696rec.frsumins02);
		fieldNo015.set(tr696rec.frsumins03);
		fieldNo018.set(tr696rec.frsumins04);
		fieldNo021.set(tr696rec.frsumins05);
		fieldNo024.set(tr696rec.frsumins06);
		fieldNo027.set(tr696rec.frsumins07);
		fieldNo030.set(tr696rec.frsumins08);
		fieldNo033.set(tr696rec.frsumins09);
		fieldNo036.set(tr696rec.frsumins10);
		fieldNo011.set(tr696rec.saband02);
		fieldNo014.set(tr696rec.saband03);
		fieldNo017.set(tr696rec.saband04);
		fieldNo020.set(tr696rec.saband05);
		fieldNo023.set(tr696rec.saband06);
		fieldNo026.set(tr696rec.saband07);
		fieldNo029.set(tr696rec.saband08);
		fieldNo032.set(tr696rec.saband09);
		fieldNo035.set(tr696rec.saband10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
