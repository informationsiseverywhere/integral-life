package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MedipfDAO extends BaseDAO<Medipf> {
	public boolean updateMedipf(List<Medipf> mediList);

	public List<Medipf> searchMediRecord(String coy, String chdrnum);

}
