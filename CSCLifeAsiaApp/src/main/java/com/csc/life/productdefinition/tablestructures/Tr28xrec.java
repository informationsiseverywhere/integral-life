package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:52
 * Description:
 * Copybook name: T5675REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr28xrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr28xRec = new FixedLengthStringData(500);
  	public FixedLengthStringData dataextracts = new FixedLengthStringData(90).isAPartOf(tr28xRec, 0);
  	public FixedLengthStringData[] dataextract = FLSArrayPartOfStructure(9, 10, dataextracts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(90).isAPartOf(dataextracts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData dataextract01 = new FixedLengthStringData(10).isAPartOf(filler, 0);
  	public FixedLengthStringData dataextract02 = new FixedLengthStringData(10).isAPartOf(filler, 10);
  	public FixedLengthStringData dataextract03 = new FixedLengthStringData(10).isAPartOf(filler, 20);
  	public FixedLengthStringData dataextract04 = new FixedLengthStringData(10).isAPartOf(filler, 30);
  	public FixedLengthStringData dataextract05 = new FixedLengthStringData(10).isAPartOf(filler, 40);
  	public FixedLengthStringData dataextract06 = new FixedLengthStringData(10).isAPartOf(filler, 50);
  	public FixedLengthStringData dataextract07 = new FixedLengthStringData(10).isAPartOf(filler, 60);
  	public FixedLengthStringData dataextract08 = new FixedLengthStringData(10).isAPartOf(filler, 70);
  	public FixedLengthStringData dataextract09 = new FixedLengthStringData(10).isAPartOf(filler, 80);
  	
  	public FixedLengthStringData funcs = new FixedLengthStringData(45).isAPartOf(tr28xRec, 90);
  	public FixedLengthStringData[] func = FLSArrayPartOfStructure(9, 5, funcs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(45).isAPartOf(funcs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData func01 = new FixedLengthStringData(5).isAPartOf(filler1, 0); 
  	public FixedLengthStringData func02 = new FixedLengthStringData(5).isAPartOf(filler1, 5); 
  	public FixedLengthStringData func03 = new FixedLengthStringData(5).isAPartOf(filler1, 10);
  	public FixedLengthStringData func04 = new FixedLengthStringData(5).isAPartOf(filler1, 15);
  	public FixedLengthStringData func05 = new FixedLengthStringData(5).isAPartOf(filler1, 20);
  	public FixedLengthStringData func06 = new FixedLengthStringData(5).isAPartOf(filler1, 25);
  	public FixedLengthStringData func07 = new FixedLengthStringData(5).isAPartOf(filler1, 30);
  	public FixedLengthStringData func08 = new FixedLengthStringData(5).isAPartOf(filler1, 35);
  	public FixedLengthStringData func09 = new FixedLengthStringData(5).isAPartOf(filler1, 40);
  	
  	public FixedLengthStringData vpmmodel = new FixedLengthStringData(20).isAPartOf(tr28xRec, 135);
  	public FixedLengthStringData linkstatuz = new FixedLengthStringData(20).isAPartOf(tr28xRec, 155);
  	public FixedLengthStringData linkcopybk = new FixedLengthStringData(10).isAPartOf(tr28xRec, 175);
  	
  	public FixedLengthStringData dataupdates = new FixedLengthStringData(50).isAPartOf(tr28xRec, 185);
  	public FixedLengthStringData[] dataupdate = FLSArrayPartOfStructure(5, 10, dataupdates, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(dataupdates, 0, FILLER_REDEFINE);
  	public FixedLengthStringData dataupdate01 = new FixedLengthStringData(10).isAPartOf(filler2, 0);
  	public FixedLengthStringData dataupdate02 = new FixedLengthStringData(10).isAPartOf(filler2, 10);
  	public FixedLengthStringData dataupdate03 = new FixedLengthStringData(10).isAPartOf(filler2, 20);
  	public FixedLengthStringData dataupdate04 = new FixedLengthStringData(10).isAPartOf(filler2, 30);  	
  	public FixedLengthStringData dataupdate05 = new FixedLengthStringData(10).isAPartOf(filler2, 40);  	
  	
  	public FixedLengthStringData extfldids = new FixedLengthStringData(200).isAPartOf(tr28xRec, 235);
  	public FixedLengthStringData[] extfldid = FLSArrayPartOfStructure(25, 8, extfldids, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(200).isAPartOf(extfldids, 0, FILLER_REDEFINE);
  	public FixedLengthStringData extfldid01 = new FixedLengthStringData(8).isAPartOf(filler3, 0);  
  	public FixedLengthStringData extfldid02 = new FixedLengthStringData(8).isAPartOf(filler3, 8);  
  	public FixedLengthStringData extfldid03 = new FixedLengthStringData(8).isAPartOf(filler3, 16); 
  	public FixedLengthStringData extfldid04 = new FixedLengthStringData(8).isAPartOf(filler3, 24); 
  	public FixedLengthStringData extfldid05 = new FixedLengthStringData(8).isAPartOf(filler3, 32); 
  	public FixedLengthStringData extfldid06 = new FixedLengthStringData(8).isAPartOf(filler3, 40); 
  	public FixedLengthStringData extfldid07 = new FixedLengthStringData(8).isAPartOf(filler3, 48); 
  	public FixedLengthStringData extfldid08 = new FixedLengthStringData(8).isAPartOf(filler3, 56); 
  	public FixedLengthStringData extfldid09 = new FixedLengthStringData(8).isAPartOf(filler3, 64); 
  	public FixedLengthStringData extfldid10 = new FixedLengthStringData(8).isAPartOf(filler3, 72); 
  	public FixedLengthStringData extfldid11 = new FixedLengthStringData(8).isAPartOf(filler3, 80); 
  	public FixedLengthStringData extfldid12 = new FixedLengthStringData(8).isAPartOf(filler3, 88); 
  	public FixedLengthStringData extfldid13 = new FixedLengthStringData(8).isAPartOf(filler3, 96); 
  	public FixedLengthStringData extfldid14 = new FixedLengthStringData(8).isAPartOf(filler3, 104);
  	public FixedLengthStringData extfldid15 = new FixedLengthStringData(8).isAPartOf(filler3, 112);
  	public FixedLengthStringData extfldid16 = new FixedLengthStringData(8).isAPartOf(filler3, 120);
  	public FixedLengthStringData extfldid17 = new FixedLengthStringData(8).isAPartOf(filler3, 128);
  	public FixedLengthStringData extfldid18 = new FixedLengthStringData(8).isAPartOf(filler3, 136);
  	public FixedLengthStringData extfldid19 = new FixedLengthStringData(8).isAPartOf(filler3, 144);
  	public FixedLengthStringData extfldid20 = new FixedLengthStringData(8).isAPartOf(filler3, 152);
  	public FixedLengthStringData extfldid21 = new FixedLengthStringData(8).isAPartOf(filler3, 160);
  	public FixedLengthStringData extfldid22 = new FixedLengthStringData(8).isAPartOf(filler3, 168);
  	public FixedLengthStringData extfldid23 = new FixedLengthStringData(8).isAPartOf(filler3, 176);
  	public FixedLengthStringData extfldid24 = new FixedLengthStringData(8).isAPartOf(filler3, 184);
  	public FixedLengthStringData extfldid25 = new FixedLengthStringData(8).isAPartOf(filler3, 192); 
  	
  	public FixedLengthStringData dataLog = new FixedLengthStringData(1).isAPartOf(tr28xRec, 435);
  	public FixedLengthStringData vpmsubr = new FixedLengthStringData(10).isAPartOf(tr28xRec, 436);

  	public FixedLengthStringData filler4 = new FixedLengthStringData(54).isAPartOf(tr28xRec, 446, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr28xRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr28xRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}