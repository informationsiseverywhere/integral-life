/*
 * File: T5689pt.java
 * Date: 30 August 2009 2:26:15
 * Author: Quipoz Limited
 * 
 * Class transformed from T5689PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5689.
*
*
*****************************************************************
* </pre>
*/
public class T5689pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(75);
	private FixedLengthStringData filler1 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 25, FILLER).init("Method of Payment/Billing Frequencies        S5689");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 26, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(46);
	private FixedLengthStringData filler7 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 19);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 29, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 36);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(78);
	private FixedLengthStringData filler9 = new FixedLengthStringData(78).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Mop    F   DD    Mop   F   DD    Mop   F   DD    Mop   FDD    Mop   F   DD");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(78);
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 2);
	private FixedLengthStringData filler11 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 7);
	private FixedLengthStringData filler12 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 12).setPattern("ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 19);
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 23);
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 28).setPattern("ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 35);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 39);
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 44).setPattern("ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 51);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 55);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 60).setPattern("ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 67);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 71);
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 76).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(78);
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 2);
	private FixedLengthStringData filler26 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 7);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 12).setPattern("ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 19);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 23);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 28).setPattern("ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 35);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 39);
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 44).setPattern("ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 51);
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 55);
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 60).setPattern("ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 67);
	private FixedLengthStringData filler38 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 71);
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 76).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(78);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 2);
	private FixedLengthStringData filler41 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 7);
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 12).setPattern("ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 19);
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 23);
	private FixedLengthStringData filler45 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 28).setPattern("ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 35);
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 39);
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 44).setPattern("ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 55);
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 60).setPattern("ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 67);
	private FixedLengthStringData filler53 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 71);
	private FixedLengthStringData filler54 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 76).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(78);
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 2);
	private FixedLengthStringData filler56 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 7);
	private FixedLengthStringData filler57 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 12).setPattern("ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 19);
	private FixedLengthStringData filler59 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 23);
	private FixedLengthStringData filler60 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 28).setPattern("ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 35);
	private FixedLengthStringData filler62 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 39);
	private FixedLengthStringData filler63 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 44).setPattern("ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 51);
	private FixedLengthStringData filler65 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 55);
	private FixedLengthStringData filler66 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 60).setPattern("ZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 67);
	private FixedLengthStringData filler68 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 71);
	private FixedLengthStringData filler69 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 76).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(78);
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 2);
	private FixedLengthStringData filler71 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 7);
	private FixedLengthStringData filler72 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 12).setPattern("ZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 19);
	private FixedLengthStringData filler74 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 23);
	private FixedLengthStringData filler75 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 28).setPattern("ZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 35);
	private FixedLengthStringData filler77 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 39);
	private FixedLengthStringData filler78 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 44).setPattern("ZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 51);
	private FixedLengthStringData filler80 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 55);
	private FixedLengthStringData filler81 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 60).setPattern("ZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 67);
	private FixedLengthStringData filler83 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 71);
	private FixedLengthStringData filler84 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 76).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(78);
	private FixedLengthStringData filler85 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 2);
	private FixedLengthStringData filler86 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 7);
	private FixedLengthStringData filler87 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 12).setPattern("ZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 19);
	private FixedLengthStringData filler89 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 23);
	private FixedLengthStringData filler90 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 28).setPattern("ZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 35);
	private FixedLengthStringData filler92 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 39);
	private FixedLengthStringData filler93 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 44).setPattern("ZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler95 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 55);
	private FixedLengthStringData filler96 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 60).setPattern("ZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 67);
	private FixedLengthStringData filler98 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 71);
	private FixedLengthStringData filler99 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 76).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(30);
	private FixedLengthStringData filler100 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler101 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine011, 7, FILLER).init("Mop = Method of Payment");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(22);
	private FixedLengthStringData filler102 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler103 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 7, FILLER).init("F   = Frequency");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(29);
	private FixedLengthStringData filler104 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler105 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine013, 7, FILLER).init("DD  = Day of the month");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5689rec t5689rec = new T5689rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5689pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5689rec.t5689Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5689rec.mop01);
		fieldNo008.set(t5689rec.billfreq01);
		fieldNo009.set(t5689rec.rendd01);
		fieldNo010.set(t5689rec.mop02);
		fieldNo011.set(t5689rec.billfreq02);
		fieldNo012.set(t5689rec.rendd02);
		fieldNo013.set(t5689rec.mop03);
		fieldNo014.set(t5689rec.billfreq03);
		fieldNo015.set(t5689rec.rendd03);
		fieldNo016.set(t5689rec.mop04);
		fieldNo017.set(t5689rec.billfreq04);
		fieldNo018.set(t5689rec.rendd04);
		fieldNo019.set(t5689rec.mop05);
		fieldNo020.set(t5689rec.billfreq05);
		fieldNo021.set(t5689rec.rendd05);
		fieldNo022.set(t5689rec.mop06);
		fieldNo023.set(t5689rec.billfreq06);
		fieldNo024.set(t5689rec.rendd06);
		fieldNo025.set(t5689rec.mop07);
		fieldNo026.set(t5689rec.billfreq07);
		fieldNo027.set(t5689rec.rendd07);
		fieldNo028.set(t5689rec.mop08);
		fieldNo029.set(t5689rec.billfreq08);
		fieldNo030.set(t5689rec.rendd08);
		fieldNo031.set(t5689rec.mop09);
		fieldNo032.set(t5689rec.billfreq09);
		fieldNo033.set(t5689rec.rendd09);
		fieldNo034.set(t5689rec.mop10);
		fieldNo035.set(t5689rec.billfreq10);
		fieldNo036.set(t5689rec.rendd10);
		fieldNo037.set(t5689rec.mop11);
		fieldNo038.set(t5689rec.billfreq11);
		fieldNo039.set(t5689rec.rendd11);
		fieldNo040.set(t5689rec.mop12);
		fieldNo041.set(t5689rec.billfreq12);
		fieldNo042.set(t5689rec.rendd12);
		fieldNo043.set(t5689rec.mop13);
		fieldNo044.set(t5689rec.billfreq13);
		fieldNo045.set(t5689rec.rendd13);
		fieldNo046.set(t5689rec.mop14);
		fieldNo047.set(t5689rec.billfreq14);
		fieldNo048.set(t5689rec.rendd14);
		fieldNo049.set(t5689rec.mop15);
		fieldNo050.set(t5689rec.billfreq15);
		fieldNo051.set(t5689rec.rendd15);
		fieldNo052.set(t5689rec.mop16);
		fieldNo053.set(t5689rec.billfreq16);
		fieldNo054.set(t5689rec.rendd16);
		fieldNo055.set(t5689rec.mop17);
		fieldNo056.set(t5689rec.billfreq17);
		fieldNo057.set(t5689rec.rendd17);
		fieldNo058.set(t5689rec.mop18);
		fieldNo059.set(t5689rec.billfreq18);
		fieldNo060.set(t5689rec.rendd18);
		fieldNo061.set(t5689rec.mop19);
		fieldNo062.set(t5689rec.billfreq19);
		fieldNo063.set(t5689rec.rendd19);
		fieldNo064.set(t5689rec.mop20);
		fieldNo065.set(t5689rec.billfreq20);
		fieldNo066.set(t5689rec.rendd20);
		fieldNo067.set(t5689rec.mop21);
		fieldNo068.set(t5689rec.billfreq21);
		fieldNo069.set(t5689rec.rendd21);
		fieldNo070.set(t5689rec.mop22);
		fieldNo071.set(t5689rec.billfreq22);
		fieldNo072.set(t5689rec.rendd22);
		fieldNo073.set(t5689rec.mop23);
		fieldNo074.set(t5689rec.billfreq23);
		fieldNo075.set(t5689rec.rendd23);
		fieldNo076.set(t5689rec.mop24);
		fieldNo077.set(t5689rec.billfreq24);
		fieldNo078.set(t5689rec.rendd24);
		fieldNo079.set(t5689rec.mop25);
		fieldNo080.set(t5689rec.billfreq25);
		fieldNo081.set(t5689rec.rendd25);
		fieldNo082.set(t5689rec.mop26);
		fieldNo083.set(t5689rec.billfreq26);
		fieldNo084.set(t5689rec.rendd26);
		fieldNo085.set(t5689rec.mop27);
		fieldNo086.set(t5689rec.billfreq27);
		fieldNo087.set(t5689rec.rendd27);
		fieldNo088.set(t5689rec.mop28);
		fieldNo089.set(t5689rec.billfreq28);
		fieldNo090.set(t5689rec.rendd28);
		fieldNo091.set(t5689rec.mop29);
		fieldNo092.set(t5689rec.billfreq29);
		fieldNo093.set(t5689rec.rendd29);
		fieldNo094.set(t5689rec.mop30);
		fieldNo095.set(t5689rec.billfreq30);
		fieldNo096.set(t5689rec.rendd30);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
