package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR51I
 * @version 1.0 generated on 30/08/09 07:17
 * @author Quipoz
 */
public class Sr51iScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(192);
	public FixedLengthStringData dataFields = new FixedLengthStringData(64).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData indcs = new FixedLengthStringData(4).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] indc = FLSArrayPartOfStructure(2, 2, indcs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(indcs, 0, FILLER_REDEFINE);
	public FixedLengthStringData indc01 = DD.indc.copy().isAPartOf(filler,0);
	public FixedLengthStringData indc02 = DD.indc.copy().isAPartOf(filler,2);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,5);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,13);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,21);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 64);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData indcsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] indcErr = FLSArrayPartOfStructure(2, 4, indcsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(indcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData indc01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData indc02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 96);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData indcsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] indcOut = FLSArrayPartOfStructure(2, 12, indcsOut, 0);
	public FixedLengthStringData[][] indcO = FLSDArrayPartOfArrayStructure(12, 1, indcOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(indcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] indc01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] indc02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr51iscreenWritten = new LongData(0);
	public LongData Sr51iprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr51iScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, indc01, indc02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, indc01Out, indc02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, indc01Err, indc02Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr51iscreen.class;
		protectRecord = Sr51iprotect.class;
	}

}
