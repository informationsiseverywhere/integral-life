package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MedipfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:49
 * Class transformed from MEDIPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MedipfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 118;
	public FixedLengthStringData medirec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData medipfRecord = medirec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(medirec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(medirec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(medirec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(medirec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(medirec);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(medirec);
	public FixedLengthStringData exmcode = DD.exmcode.copy().isAPartOf(medirec);
	public PackedDecimalData zmedfee = DD.zmedfee.copy().isAPartOf(medirec);
	public FixedLengthStringData paidby = DD.paidby.copy().isAPartOf(medirec);
	public FixedLengthStringData invref = DD.invref.copy().isAPartOf(medirec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(medirec);
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(medirec);
	public PackedDecimalData paydte = DD.paydte.copy().isAPartOf(medirec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(medirec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(medirec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(medirec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(medirec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MedipfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MedipfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MedipfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MedipfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MedipfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MedipfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MedipfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MEDIPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"SEQNO, " +
							"ZMEDTYP, " +
							"EXMCODE, " +
							"ZMEDFEE, " +
							"PAIDBY, " +
							"INVREF, " +
							"EFFDATE, " +
							"ACTIND, " +
							"PAYDTE, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     seqno,
                                     zmedtyp,
                                     exmcode,
                                     zmedfee,
                                     paidby,
                                     invref,
                                     effdate,
                                     activeInd,
                                     paydte,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		seqno.clear();
  		zmedtyp.clear();
  		exmcode.clear();
  		zmedfee.clear();
  		paidby.clear();
  		invref.clear();
  		effdate.clear();
  		activeInd.clear();
  		paydte.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMedirec() {
  		return medirec;
	}

	public FixedLengthStringData getMedipfRecord() {
  		return medipfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMedirec(what);
	}

	public void setMedirec(Object what) {
  		this.medirec.set(what);
	}

	public void setMedipfRecord(Object what) {
  		this.medipfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(medirec.getLength());
		result.set(medirec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}