package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UtrspfDAO extends BaseDAO<Utrspf> {
    public Map<Long, List<Utrspf>> searchUtrsRecord(List<Long> covrUQ);
	public List<Utrspf> searchUtrsRecord(Utrspf utrspfData);
	public Map<String, List<Utrspf>> searchUtrsRecordByChdrnum(List<String> chdrnumList);
	public void insertUtrspfRecord(List<Utrspf> urList);
	public void updateUtrspfRecord(List<Utrspf> urList);
	
	public Map<String, List<Utrspf>> searchUtrs5104Record(List<String> chdrnumList);
	public Map<String, List<Utrspf>> searchUtrsRecord(String chdrcoy, List<String> chdrnumList);
	
	public List<Utrspf> searchUtrsRecord(String chdrcoy, String chdrnum);
	public List<Utrspf> getUtrsRecord(String chdrcoy, String chdrnum,String life, String coverage,String rider);
	public List<Utrspf> searchUtrssurRecord(Utrspf utrspfData); //ILB-1097

}