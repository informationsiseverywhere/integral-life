/*
 * File: Pr652.java
 * Date: 30 August 2009 1:51:21
 * Author: Quipoz Limited
 * 
 * Class transformed from PR652.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.recordstructures.Wsspdocs;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.MediTableDAM;
import com.csc.life.productdefinition.dataaccess.MediactTableDAM;
import com.csc.life.productdefinition.dataaccess.MediivcTableDAM;
import com.csc.life.productdefinition.dataaccess.MedimedTableDAM;
import com.csc.life.productdefinition.dataaccess.MedipayTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.recordstructures.Mediactkey;
import com.csc.life.productdefinition.recordstructures.Mediivckey;
import com.csc.life.productdefinition.recordstructures.Medikey;
import com.csc.life.productdefinition.recordstructures.Medimedkey;
import com.csc.life.productdefinition.recordstructures.Medipaykey;
import com.csc.life.productdefinition.screens.Sr652ScreenVars;
import com.csc.life.productdefinition.tablestructures.Tr549rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Wsspio;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wssprec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  Medical Bill Approval
*  ---------------------
*
***********************************************************************
* </pre>
*/
public class Pr652 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR652");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaUserArea = new FixedLengthStringData(768);
	private FixedLengthStringData wsaaSave1Batchkey = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaSave1Batchkey2 = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaAlwActiveInd = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaItmActiveInd = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaRoluFileId = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaMaxOpt = 4;
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private String wsaaLargeName = "LGNMS";
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaEntityDesc = new FixedLengthStringData(30);
	private String wsaaApproved = "1";
	private ZonedDecimalData wsaaSubfileCnt = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaPrevZdocno = new FixedLengthStringData(15);
	private FixedLengthStringData wsaaPrevChdrsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPrevPaidby = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrevExmcode = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPrevMdblactf = new FixedLengthStringData(1);
		/* WSAA-TRAN-CODE */
	private String trcdeProposalEnquiry = "T509";
	private String trcdeContractEnquiry = "T609";

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1).init("N");
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");

	private FixedLengthStringData wsaaFoundAllowableAction = new FixedLengthStringData(1).init

("N");
	private Validator foundAllowableAction = new Validator(wsaaFoundAllowableAction, "Y");
		/* ERRORS */
	private String e944 = "E944";
		/* TABLES */
	private String tr549 = "TR549";
		/* FORMATS */
	private String agntrec = "AGNTREC";
	private String chdrrec = "CHDRREC";
	private String chdrenqrec = "CHDRENQREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String medirec = "MEDIREC";
	private String medimedrec = "MEDIMEDREC";
	private String mediivcrec = "MEDIIVCREC";
	private String mediactrec = "MEDIACTREC";
	private String medipayrec = "MEDIPAYREC";
	private String zmpvrec = "ZMPVREC";
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Contract header file*/
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Medical Bill Payment Detail*/
	private MediTableDAM mediIO = new MediTableDAM();
		/*Medical Bill viewed by action*/
	private MediactTableDAM mediactIO = new MediactTableDAM();
		/*Medical Bill Paymt Dtls Inv No & Chdr*/
	private MediivcTableDAM mediivcIO = new MediivcTableDAM();
		/*Med Bill Pymt by Entity, Invoice*/
	private MedimedTableDAM medimedIO = new MedimedTableDAM();
		/*Medical Bill viewed by paid to*/
	private MedipayTableDAM medipayIO = new MedipayTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Optswchrec optswchrec = new Optswchrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr549rec tr549rec = new Tr549rec();
	private Batckey wsaaBatckey = new Batckey();
	private Desckey wsaaDesckey = new Desckey();
	private Mediactkey wsaaMediactkey = new Mediactkey();
	private Mediivckey wsaaMediivckey = new Mediivckey();
	private Medikey wsaaMedikey = new Medikey();
	private Medimedkey wsaaMedimedkey = new Medimedkey();
	private Medipaykey wsaaMedipaykey = new Medipaykey();
	private Wsspdocs wsspdocs = new Wsspdocs();
	private Wssprec wssprec = new Wssprec();
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Sr652ScreenVars sv = ScreenProgram.getScreenVars( Sr652ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit2090, 
		updateErrorIndicators2670, 
		exit3090, 
		nextRec3100, 
		exit3100, 
		next4200, 
		exit4200, 
		a1180Nextr, 
		a1190Exit, 
		a2180Nextr, 
		a2190Exit, 
		a3180Nextr, 
		a3190Exit, 
		a4180Nextr, 
		a4190Exit, 
		a5180Nextr, 
		a5190Exit, 
		b1090Exit
	}

	public Pr652() {
		super();
		screenVars = sv;
		new ScreenModel("Sr652", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, 

parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 

0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (isEQ(wsaaBatckey.batcBatctrcde,trcdeContractEnquiry)) {
				callWsspio1120();
			}
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaPrevZdocno.set(SPACES);
		wsaaPrevChdrsel.set(SPACES);
		wsaaPrevPaidby.set(SPACES);
		wsaaPrevExmcode.set(SPACES);
		wsaaPrevMdblactf.set(SPACES);
		wsaaScrnStatuz.set(SPACES);
		wsaaRoluFileId.set(SPACES);
		sv.pgmnam.set(wsaaProg);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		initOptswch1100();
		wsaaRoluFileId.set("1");
		wsaaMediivckey.mediivcInvref.set(SPACES);
		wsaaMediivckey.mediivcChdrnum.set(SPACES);
		a1000BuildSubfileMediivc();
		scrnparams.subfileRrn.set(1);
	}

protected void initOptswch1100()
	{
		begin1100();
	}

protected void begin1100()
	{
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, 

wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		for (ix.set(1); !(isGT(ix,wsaaMaxOpt)
		|| isEQ(optswchrec.optsType[ix.toInt()],SPACES)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")) {
				sv.optdsc[ix.toInt()].set(optswchrec.optsDsc[ix.toInt()]);
			}
		}
	}

protected void callWsspio1120()
	{
		start1120();
	}

protected void start1120()
	{
		wsspcomn.batchkey.set(wsaaSave1Batchkey);
		wsspcomn.batchkey2.set(wsaaSave1Batchkey2);
		wsspdocs.userArea.set(wsaaUserArea);
		wsaaBatckey.set(wsspcomn.batchkey);
		wssprec.wsspuser.set(wsaaUserArea);
		wssprec.wsspcomn.set(wsspcomn.commonArea);
		wssprec.function.set(varcom.rewrt);
		callProgram(Wsspio.class, wssprec.params);
		if (isNE(wssprec.statuz,varcom.oK)) {
			syserrrec.params.set(wssprec.params);
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
		
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsaaScrnStatuz,varcom.insr)) {
			wsaaPrevZdocno.set(SPACES);
			wsaaPrevChdrsel.set(SPACES);
			wsaaPrevPaidby.set(SPACES);
			wsaaPrevExmcode.set(SPACES);
			wsaaPrevMdblactf.set(SPACES);
		}
	}

protected void validateScreen2010()
	{
		if (isNE(sv.zdocno,SPACES)) {
			validateFilterZdocno2100();
		}
		else {
			if (isNE(sv.chdrsel,SPACES)) {
				validateFilterChdrsel2110();
			}
			else {
				if (isNE(sv.paidby,SPACES)) {
					validateFilterPaidby2120();
				}
				else {
					if (isNE(sv.exmcode,SPACES)) {
						validateFilterExmcode2130();
					}
					else {
						if (isNE(sv.mdblactf,SPACES)) {
							validateFilterMdblact2140();
						}
						else {
							validateFilterZdocno2100();
						}
					}
				}
			}
		}

		if (isEQ(wsaaScrnStatuz,varcom.insr)) {

			wsspcomn.edterror.set("Y");		
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaScrnStatuz,varcom.rolu)) {
			roluSubfile2200();
			wsspcomn.edterror.set("Y");
		}
	}
protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateFilterZdocno2100()
	{
		start2100();
	}

protected void start2100()
	{
		if (isNE(sv.zdocno,wsaaPrevZdocno)
		|| isNE(sv.chdrsel,wsaaPrevChdrsel)
		|| isNE(sv.paidby,wsaaPrevPaidby)
		|| isNE(sv.exmcode,wsaaPrevExmcode)
		|| isNE(sv.mdblactf,wsaaPrevMdblactf)) {
			wsaaPrevZdocno.set(sv.zdocno);
			wsaaPrevChdrsel.set(sv.chdrsel);
			wsaaPrevPaidby.set(sv.paidby);
			wsaaPrevExmcode.set(sv.exmcode);
			wsaaPrevMdblactf.set(sv.mdblactf);
			wsaaMediivckey.mediivcInvref.set(sv.zdocno);
			wsaaMediivckey.mediivcChdrnum.set(sv.chdrsel);
			a1000BuildSubfileMediivc();
			wsaaRoluFileId.set("1");
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateFilterChdrsel2110()
	{
		/*START*/
		if (isNE(sv.zdocno,wsaaPrevZdocno)
		|| isNE(sv.chdrsel,wsaaPrevChdrsel)
		|| isNE(sv.paidby,wsaaPrevPaidby)
		|| isNE(sv.exmcode,wsaaPrevExmcode)
		|| isNE(sv.mdblactf,wsaaPrevMdblactf)) {
			wsaaPrevZdocno.set(sv.zdocno);
			wsaaPrevChdrsel.set(sv.chdrsel);
			wsaaPrevPaidby.set(sv.paidby);
			wsaaPrevExmcode.set(sv.exmcode);
			wsaaPrevMdblactf.set(sv.mdblactf);
			wsaaMedikey.mediChdrnum.set(sv.chdrsel);
			a2000BuildSubfileMedi();
			wsaaRoluFileId.set("2");
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateFilterPaidby2120()
	{
		start2120();
	}

protected void start2120()
	{
		if (isNE(sv.zdocno,wsaaPrevZdocno)
		|| isNE(sv.chdrsel,wsaaPrevChdrsel)
		|| isNE(sv.paidby,wsaaPrevPaidby)
		|| isNE(sv.exmcode,wsaaPrevExmcode)
		|| isNE(sv.mdblactf,wsaaPrevMdblactf)) {
			wsaaPrevZdocno.set(sv.zdocno);
			wsaaPrevChdrsel.set(sv.chdrsel);
			wsaaPrevPaidby.set(sv.paidby);
			wsaaPrevExmcode.set(sv.exmcode);
			wsaaPrevMdblactf.set(sv.mdblactf);
			wsaaMedipaykey.medipayPaidby.set(sv.paidby);
			wsaaMedipaykey.medipayExmcode.set(sv.exmcode);
			wsaaMedipaykey.medipayActiveInd.set(sv.mdblactf);
			a3000BuildSubfileMedipay();
			wsaaRoluFileId.set("3");
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateFilterExmcode2130()
	{
		/*START*/
		if (isNE(sv.zdocno,wsaaPrevZdocno)
		|| isNE(sv.chdrsel,wsaaPrevChdrsel)
		|| isNE(sv.paidby,wsaaPrevPaidby)
		|| isNE(sv.exmcode,wsaaPrevExmcode)
		|| isNE(sv.mdblactf,wsaaPrevMdblactf)) {
			wsaaPrevZdocno.set(sv.zdocno);
			wsaaPrevChdrsel.set(sv.chdrsel);
			wsaaPrevPaidby.set(sv.paidby);
			wsaaPrevExmcode.set(sv.exmcode);
			wsaaPrevMdblactf.set(sv.mdblactf);
			wsaaMedimedkey.medimedExmcode.set(sv.exmcode);
			a4000BuildSubfileMedimed();
			wsaaRoluFileId.set("4");
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateFilterMdblact2140()
	{
		/*START*/
		if (isNE(sv.zdocno,wsaaPrevZdocno)
		|| isNE(sv.chdrsel,wsaaPrevChdrsel)
		|| isNE(sv.paidby,wsaaPrevPaidby)
		|| isNE(sv.exmcode,wsaaPrevExmcode)
		|| isNE(sv.mdblactf,wsaaPrevMdblactf)) {
			wsaaPrevZdocno.set(sv.zdocno);
			wsaaPrevChdrsel.set(sv.chdrsel);
			wsaaPrevPaidby.set(sv.paidby);
			wsaaPrevExmcode.set(sv.exmcode);
			wsaaPrevMdblactf.set(sv.mdblactf);
			wsaaMediactkey.mediactActiveInd.set(sv.mdblactf);
			a5000BuildSubfileMediact();
			wsaaRoluFileId.set("5");
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void roluSubfile2200()
	{
		/*START*/
		wsaaSubfileCnt.set(1);
		if (isEQ(wsaaRoluFileId,"1")){
			roluMediivc2210();
		}
		else if (isEQ(wsaaRoluFileId,"2")){
			roluMedi2220();
		}
		else if (isEQ(wsaaRoluFileId,"3")){
			roluMedipay2230();
		}
		else if (isEQ(wsaaRoluFileId,"4")){
			roluMedimed2240();
		}
		else if (isEQ(wsaaRoluFileId,"5")){
			roluMediact2250();
		}
		/*EXIT*/
	}

protected void roluMediivc2210()
	{
		/*START*/
		while ( !(isEQ(mediivcIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a1100LoopMediivc();
		}
		
		if (isEQ(mediivcIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void roluMedi2220()
	{
		/*START*/
		while ( !(isEQ(mediIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a2100LoopMedi();
		}
		
		if (isEQ(mediIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void roluMedipay2230()
	{
		/*START*/
		while ( !(isEQ(medipayIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a3100LoopMedipay();
		}
		
		if (isEQ(medipayIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void roluMedimed2240()
	{
		/*START*/
		while ( !(isEQ(medimedIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a4100LoopMedimed();
		}
		
		if (isEQ(medimedIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void roluMediact2250()
	{
		/*START*/
		while ( !(isEQ(mediactIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a5100LoopMediact();
		}
		
		if (isEQ(mediactIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isNE(sv.select,SPACES)) {
			chckSelection2610();
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				sv.selectErr.set(optswchrec.optsStatuz);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		if (isNE(sv.mdblact,sv.actn)) {
			wsaaAlwActiveInd.set(sv.mdblact);
			wsaaItmActiveInd.set(sv.actn);
			b4000ValidateActionInd();
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void chckSelection2610()
	{
		start2610();
	}

protected void start2610()
	{
		optswchrec.optsStatuz.set(varcom.oK);
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelCode.set(SPACES);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, 

wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		|| isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			updateSubfile3100();
		}
		
	}

protected void updateSubfile3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3100();
				}
				case nextRec3100: {
					nextRec3100();
				}
				case exit3100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3100()
	{
		processScreen("SR652", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3100);
		}
		if (isEQ(sv.mdblact,sv.actn)) {
			goTo(GotoLabel.nextRec3100);
		}
		mediIO.setDataArea(SPACES);
		mediIO.setChdrcoy(wsspcomn.company);
		mediIO.setChdrnum(sv.chdrnum);
		mediIO.setSeqno(sv.seqno);
		mediIO.setFormat(medirec);
		mediIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		mediIO.setActiveInd(sv.mdblact);
		mediIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
	}

protected void nextRec3100()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void whereNext4000()
	{
		nextProgram4010();
		stckOpts4010();
	}

protected void nextProgram4010()
	{
		wsaaUserArea.set(wsspdocs.userArea);
		wsaaSave1Batchkey.set(wsspcomn.batchkey);
		wsaaSave1Batchkey2.set(wsspcomn.batchkey2);
		wsspdocs.subfileRrn.set(scrnparams.subfileRrn);
		wsspdocs.subfileEnd.set(scrnparams.subfileEnd);
		scrnparams.statuz.set(varcom.oK);
		wsaaFoundSelection.set("N");
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz,varcom.endp)
		|| foundSelection.isTrue())) {
			lineSels4200();
		}
		
	}

protected void stckOpts4010()
	{
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, 

wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(wsaaScrnStatuz,varcom.oK)) {
				scrnparams.subfileRrn.set(wsspdocs.subfileRrn);
				scrnparams.subfileEnd.set(wsspdocs.subfileEnd);
			}
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void lineSels4200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin4200();
				}
				case next4200: {
					next4200();
				}
				case exit4200: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin4200()
	{
		screenIo9000();
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit4200);
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.next4200);
		}
		for (ix.set(1); !(isGT(ix,wsaaMaxOpt)
		|| foundSelection.isTrue()); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")
			&& isEQ(optswchrec.optsCode[ix.toInt()],sv.select)) {
				optswchrec.optsInd[ix.toInt()].set("X");
				wsaaFoundSelection.set("Y");
				if (isEQ(sv.select,"1")) {
					rlseFiles4210();
					keepsFilesS4220();
					if (isEQ(sv.validflag,"1")) {
						keepsFilesP63534230();
						wsaaBatckey.set(wsspcomn.batchkey);
						wsaaBatckey.batcBatctrcde.set

(trcdeContractEnquiry);
						wsspcomn.batchkey.set(wsaaBatckey);
					}
					if (isEQ(sv.validflag,"3")) {
						wsaaBatckey.set(wsspcomn.batchkey);
						wsaaBatckey.batcBatctrcde.set

(trcdeProposalEnquiry);
						wsspcomn.batchkey.set(wsaaBatckey);
					}
				}
			}
		}
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelCode.set(SPACES);
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		wsspdocs.subfileRrn.set(scrnparams.subfileRrn);
		wsspdocs.subfileEnd.set(scrnparams.subfileEnd);
	}

protected void next4200()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void rlseFiles4210()
	{
		start4210();
	}

protected void start4210()
	{
		chdrIO.setFunction(varcom.rlse);
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.rlse);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void keepsFilesS4220()
	{
		start4220();
	}

protected void start4220()
	{
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(wsspcomn.company);
		chdrIO.setChdrnum(sv.chdrnum);
		chdrIO.setValidflag(sv.validflag);
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.keeps);
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrnum);
		chdrlnbIO.setFunction(varcom.reads);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void keepsFilesP63534230()
	{
		/*START*/
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrnum);
		chdrenqIO.setFunction(varcom.reads);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void screenIo9000()
	{
		/*START*/
		processScreen("SR652", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a1000BuildSubfileMediivc()
	{
		a1000Start();
	}

protected void a1000Start()
	{
		wsaaSubfileCnt.set(1);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		mediivcIO.setDataArea(SPACES);
		mediivcIO.setStatuz(varcom.oK);
		mediivcIO.setChdrcoy(wsspcomn.company);
		mediivcIO.setInvref(wsaaMediivckey.mediivcInvref);
		mediivcIO.setChdrnum(wsaaMediivckey.mediivcChdrnum);
		mediivcIO.setSeqno(ZERO);
		mediivcIO.setFormat(mediivcrec);
		mediivcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mediivcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediivcIO.setFitKeysSearch("CHDRCOY");
		while ( !(isEQ(mediivcIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a1100LoopMediivc();
		}
		
		if (isEQ(mediivcIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		if (isEQ(scrnparams.subfileRrn,ZERO)) {
			c1000AddBlankSubfile();
		}
	}

protected void a1100LoopMediivc()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a1100Start();
				}
				case a1180Nextr: {
					a1180Nextr();
				}
				case a1190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a1100Start()
	{
		SmartFileCode.execute(appVars, mediivcIO);
		if (isNE(mediivcIO.getStatuz(),varcom.oK)
		&& isNE(mediivcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mediivcIO.getStatuz());
			syserrrec.params.set(mediivcIO.getParams());
			fatalError600();
		}
		if (isEQ(mediivcIO.getStatuz(),varcom.endp)
		|| isNE(mediivcIO.getChdrcoy(),wsspcomn.company)) {
			mediivcIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a1190Exit);
		}
		if (isEQ(mediivcIO.getActiveInd(),"4")) {
			goTo(GotoLabel.a1180Nextr);
		}
		if (isNE(sv.zdocno,SPACES)
		&& isNE(sv.zdocno,mediivcIO.getInvref())) {
			goTo(GotoLabel.a1180Nextr);
		}
		if (isNE(sv.chdrsel,SPACES)
		&& isNE(sv.chdrsel,mediivcIO.getChdrnum())) {
			goTo(GotoLabel.a1180Nextr);
		}
		if (isNE(sv.paidby,SPACES)
		&& isNE(sv.paidby,mediivcIO.getPaidby())) {
			goTo(GotoLabel.a1180Nextr);
		}
		if (isNE(sv.exmcode,SPACES)
		&& isNE(sv.exmcode,mediivcIO.getExmcode())) {
			goTo(GotoLabel.a1180Nextr);
		}
		if (isNE(sv.mdblactf,SPACES)
		&& isNE(sv.mdblactf,mediivcIO.getActiveInd())) {
			goTo(GotoLabel.a1180Nextr);
		}
		if (isEQ(wsaaScrnStatuz,varcom.insr)) {
			wsaaAlwActiveInd.set(wsaaApproved);
			wsaaItmActiveInd.set(mediivcIO.getActiveInd());
			b4000ValidateActionInd();
			if (foundAllowableAction.isTrue()) {
				sv.mdblact.set(wsaaApproved);
			}
			else {
				goTo(GotoLabel.a1180Nextr);
			}
		}
		else {
			wsaaSubfileCnt.add(1);
			sv.mdblact.set(mediivcIO.getActiveInd());
		}
		sv.select.set(SPACES);
		sv.invref.set(mediivcIO.getInvref());
		sv.chdrnum.set(mediivcIO.getChdrnum());
		sv.payto.set(mediivcIO.getPaidby());
		sv.ent.set(mediivcIO.getExmcode());
		b1000GetEntityDesc();
		sv.descript.set(wsaaEntityDesc);
		wsaaDesckey.descDesctabl.set(tr549);
		wsaaDesckey.descDescitem.set(sv.mdblact);
		b2000ReadDesc();
		sv.shortdesc.set(descIO.getShortdesc());
		sv.life.set(mediivcIO.getLife());
		sv.jlife.set(mediivcIO.getJlife());
		sv.premium.set(mediivcIO.getZmedfee());
		sv.zmedtyp.set(mediivcIO.getZmedtyp());
		sv.effdate.set(mediivcIO.getEffdate());
		sv.seqno.set(mediivcIO.getSeqno());
		b3000ReadChdrenq();
		sv.validflag.set(chdrenqIO.getValidflag());
		sv.actn.set(mediivcIO.getActiveInd());
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.mdblactOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		sv.mdblactOut[varcom.nd.toInt()].set("N");
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}

protected void a1180Nextr()
	{
		mediivcIO.setFunction(varcom.nextr);
	}

protected void a2000BuildSubfileMedi()
	{
		a2000Start();
	}

protected void a2000Start()
	{
		wsaaSubfileCnt.set(1);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		mediIO.setDataArea(SPACES);
		mediIO.setStatuz(varcom.oK);
		mediIO.setChdrcoy(wsspcomn.company);
		mediIO.setChdrnum(wsaaMedikey.mediChdrnum);
		mediIO.setSeqno(ZERO);
		mediIO.setFormat(medirec);
		mediIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mediIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(mediIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a2100LoopMedi();
		}
		
		if (isEQ(mediIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		if (isEQ(scrnparams.subfileRrn,ZERO)) {
			c1000AddBlankSubfile();
		}
	}

protected void a2100LoopMedi()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a2100Start();
				}
				case a2180Nextr: {
					a2180Nextr();
				}
				case a2190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a2100Start()
	{
		SmartFileCode.execute(appVars, mediIO);
		if (isNE(mediIO.getStatuz(),varcom.oK)
		&& isNE(mediIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mediIO.getStatuz());
			syserrrec.params.set(mediIO.getParams());
			fatalError600();
		}
		if (isEQ(mediIO.getStatuz(),varcom.endp)
		|| isNE(mediIO.getChdrcoy(),wsspcomn.company)
		|| isNE(mediIO.getChdrnum(),wsaaMedikey.mediChdrnum)) {
			mediIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a2190Exit);
		}
		if (isEQ(mediIO.getActiveInd(),"4")) {
			goTo(GotoLabel.a2180Nextr);
		}
		if (isNE(sv.chdrsel,SPACES)
		&& isNE(sv.chdrsel,mediIO.getChdrnum())) {
			goTo(GotoLabel.a2180Nextr);
		}
		if (isNE(sv.paidby,SPACES)
		&& isNE(sv.paidby,mediIO.getPaidby())) {
			goTo(GotoLabel.a2180Nextr);
		}
		if (isNE(sv.exmcode,SPACES)
		&& isNE(sv.exmcode,mediIO.getExmcode())) {
			goTo(GotoLabel.a2180Nextr);
		}
		if (isNE(sv.mdblactf,SPACES)
		&& isNE(sv.mdblactf,mediIO.getActiveInd())) {
			goTo(GotoLabel.a2180Nextr);
		}
		if (isEQ(wsaaScrnStatuz,varcom.insr)) {
			wsaaAlwActiveInd.set(wsaaApproved);
			wsaaItmActiveInd.set(mediIO.getActiveInd());
			b4000ValidateActionInd();
			if (foundAllowableAction.isTrue()) {
				sv.mdblact.set(wsaaApproved);
			}
			else {
				goTo(GotoLabel.a2180Nextr);
			}
		}
		else {
			wsaaSubfileCnt.add(1);
			sv.mdblact.set(mediIO.getActiveInd());
		}
		sv.select.set(SPACES);
		sv.invref.set(mediIO.getInvref());
		sv.chdrnum.set(mediIO.getChdrnum());
		sv.payto.set(mediIO.getPaidby());
		sv.ent.set(mediIO.getExmcode());
		b1000GetEntityDesc();
		sv.descript.set(wsaaEntityDesc);
		wsaaDesckey.descDesctabl.set(tr549);
		wsaaDesckey.descDescitem.set(sv.mdblact);
		b2000ReadDesc();
		sv.shortdesc.set(descIO.getShortdesc());
		sv.life.set(mediIO.getLife());
		sv.jlife.set(mediIO.getJlife());
		sv.premium.set(mediIO.getZmedfee());
		sv.zmedtyp.set(mediIO.getZmedtyp());
		sv.effdate.set(mediIO.getEffdate());
		sv.seqno.set(mediIO.getSeqno());
		b3000ReadChdrenq();
		sv.validflag.set(chdrenqIO.getValidflag());
		sv.actn.set(mediIO.getActiveInd());
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.mdblactOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		sv.mdblactOut[varcom.nd.toInt()].set("N");
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}

protected void a2180Nextr()
	{
		mediIO.setFunction(varcom.nextr);
	}

protected void a3000BuildSubfileMedipay()
	{
		a3000Start();
	}

protected void a3000Start()
	{
		wsaaSubfileCnt.set(1);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		medipayIO.setDataArea(SPACES);
		medipayIO.setStatuz(varcom.oK);
		medipayIO.setChdrcoy(wsspcomn.company);
		medipayIO.setPaidby(wsaaMedipaykey.medipayPaidby);
		medipayIO.setExmcode(wsaaMedipaykey.medipayExmcode);
		medipayIO.setActiveInd(wsaaMedipaykey.medipayActiveInd);
		medipayIO.setFormat(medipayrec);
		medipayIO.setFunction(varcom.begn);		
		//performance improvement -- Anjali
		medipayIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		medipayIO.setFitKeysSearch("CHDRCOY", "PAIDBY");
		while ( !(isEQ(medipayIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a3100LoopMedipay();
		}
		
		if (isEQ(medipayIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		if (isEQ(scrnparams.subfileRrn,ZERO)) {
			c1000AddBlankSubfile();
		}
	}

protected void a3100LoopMedipay()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a3100Start();
				}
				case a3180Nextr: {
					a3180Nextr();
				}
				case a3190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a3100Start()
	{
		SmartFileCode.execute(appVars, medipayIO);
		if (isNE(medipayIO.getStatuz(),varcom.oK)
		&& isNE(medipayIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(medipayIO.getStatuz());
			syserrrec.params.set(medipayIO.getParams());
			fatalError600();
		}
		if (isEQ(medipayIO.getStatuz(),varcom.endp)
		|| isNE(medipayIO.getChdrcoy(),wsspcomn.company)
		|| isNE(medipayIO.getPaidby(),wsaaMedipaykey.medipayPaidby)) {
			medipayIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a3190Exit);
		}
		if (isEQ(medipayIO.getActiveInd(),"4")) {
			goTo(GotoLabel.a3180Nextr);
		}
		if (isNE(sv.paidby,SPACES)
		&& isNE(sv.paidby,medipayIO.getPaidby())) {
			goTo(GotoLabel.a3180Nextr);
		}
		if (isNE(sv.exmcode,SPACES)
		&& isNE(sv.exmcode,medipayIO.getExmcode())) {
			goTo(GotoLabel.a3180Nextr);
		}
		if (isNE(sv.mdblactf,SPACES)
		&& isNE(sv.mdblactf,medipayIO.getActiveInd())) {
			goTo(GotoLabel.a3180Nextr);
		}
		if (isEQ(wsaaScrnStatuz,varcom.insr)) {
			wsaaAlwActiveInd.set(wsaaApproved);
			wsaaItmActiveInd.set(medipayIO.getActiveInd());
			b4000ValidateActionInd();
			if (foundAllowableAction.isTrue()) {
				sv.mdblact.set(wsaaApproved);
			}
			else {
				goTo(GotoLabel.a3180Nextr);
			}
		}
		else {
			wsaaSubfileCnt.add(1);
			sv.mdblact.set(medipayIO.getActiveInd());
		}
		sv.select.set(SPACES);
		sv.invref.set(medipayIO.getInvref());
		sv.chdrnum.set(medipayIO.getChdrnum());
		sv.payto.set(medipayIO.getPaidby());
		sv.ent.set(medipayIO.getExmcode());
		b1000GetEntityDesc();
		sv.descript.set(wsaaEntityDesc);
		wsaaDesckey.descDesctabl.set(tr549);
		wsaaDesckey.descDescitem.set(sv.mdblact);
		b2000ReadDesc();
		sv.shortdesc.set(descIO.getShortdesc());
		sv.life.set(medipayIO.getLife());
		sv.jlife.set(medipayIO.getJlife());
		sv.premium.set(medipayIO.getZmedfee());
		sv.zmedtyp.set(medipayIO.getZmedtyp());
		sv.effdate.set(medipayIO.getEffdate());
		sv.seqno.set(medipayIO.getSeqno());
		b3000ReadChdrenq();
		sv.validflag.set(chdrenqIO.getValidflag());
		sv.actn.set(medipayIO.getActiveInd());
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.mdblactOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		sv.mdblactOut[varcom.nd.toInt()].set("N");
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}

protected void a3180Nextr()
	{
		medipayIO.setFunction(varcom.nextr);
	}

protected void a4000BuildSubfileMedimed()
	{
		a4000Start();
	}

protected void a4000Start()
	{
		wsaaSubfileCnt.set(1);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		medimedIO.setDataArea(SPACES);
		medimedIO.setStatuz(varcom.oK);
		medimedIO.setExmcode(wsaaMedimedkey.medimedExmcode);
		medimedIO.setInvref(SPACES);
		medimedIO.setFormat(medimedrec);
		medimedIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		medimedIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		medimedIO.setFitKeysSearch("EXMCODE");
		while ( !(isEQ(medimedIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a4100LoopMedimed();
		}
		
		if (isEQ(medimedIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		if (isEQ(scrnparams.subfileRrn,ZERO)) {
			c1000AddBlankSubfile();
		}
	}

protected void a4100LoopMedimed()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a4100Start();
				}
				case a4180Nextr: {
					a4180Nextr();
				}
				case a4190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a4100Start()
	{
		SmartFileCode.execute(appVars, medimedIO);
		if (isNE(medimedIO.getStatuz(),varcom.oK)
		&& isNE(medimedIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(medimedIO.getStatuz());
			syserrrec.params.set(medimedIO.getParams());
			fatalError600();
		}
		if (isEQ(medimedIO.getStatuz(),varcom.endp)
		|| isNE(medimedIO.getExmcode(),wsaaMedimedkey.medimedExmcode)) {
			medimedIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a4190Exit);
		}
		if (isEQ(medimedIO.getActiveInd(),"4")) {
			goTo(GotoLabel.a4180Nextr);
		}
		if (isNE(sv.exmcode,SPACES)
		&& isNE(sv.exmcode,medimedIO.getExmcode())) {
			goTo(GotoLabel.a4180Nextr);
		}
		if (isNE(sv.mdblactf,SPACES)
		&& isNE(sv.mdblactf,medimedIO.getActiveInd())) {
			goTo(GotoLabel.a4180Nextr);
		}
		if (isEQ(wsaaScrnStatuz,varcom.insr)) {
			wsaaAlwActiveInd.set(wsaaApproved);
			wsaaItmActiveInd.set(medimedIO.getActiveInd());
			b4000ValidateActionInd();
			if (foundAllowableAction.isTrue()) {
				sv.mdblact.set(wsaaApproved);
			}
			else {
				goTo(GotoLabel.a4180Nextr);
			}
		}
		else {
			wsaaSubfileCnt.add(1);
			sv.mdblact.set(medimedIO.getActiveInd());
		}
		sv.select.set(SPACES);
		sv.invref.set(medimedIO.getInvref());
		sv.chdrnum.set(medimedIO.getChdrnum());
		sv.payto.set(medimedIO.getPaidby());
		sv.ent.set(medimedIO.getExmcode());
		b1000GetEntityDesc();
		sv.descript.set(wsaaEntityDesc);
		wsaaDesckey.descDesctabl.set(tr549);
		wsaaDesckey.descDescitem.set(sv.mdblact);
		b2000ReadDesc();
		sv.shortdesc.set(descIO.getShortdesc());
		sv.life.set(medimedIO.getLife());
		sv.jlife.set(medimedIO.getJlife());
		sv.premium.set(medimedIO.getZmedfee());
		sv.zmedtyp.set(medimedIO.getZmedtyp());
		sv.effdate.set(medimedIO.getEffdate());
		sv.seqno.set(medimedIO.getSeqno());
		b3000ReadChdrenq();
		sv.validflag.set(chdrenqIO.getValidflag());
		sv.actn.set(medimedIO.getActiveInd());
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.mdblactOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		sv.mdblactOut[varcom.nd.toInt()].set("N");
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}

protected void a4180Nextr()
	{
		medimedIO.setFunction(varcom.nextr);
	}

protected void a5000BuildSubfileMediact()
	{
		a5000Start();
	}

protected void a5000Start()
	{
		wsaaSubfileCnt.set(1);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		mediactIO.setDataArea(SPACES);
		mediactIO.setStatuz(varcom.oK);
		mediactIO.setChdrcoy(wsspcomn.company);
		mediactIO.setActiveInd(wsaaMediactkey.mediactActiveInd);
		mediactIO.setInvref(SPACES);
		mediactIO.setChdrnum(SPACES);
		mediactIO.setFormat(mediactrec);
		mediactIO.setFunction(varcom.begn);			
		//performance improvement -- Anjali
		mediactIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mediactIO.setFitKeysSearch("CHDRCOY" , "ACTIND");
		while ( !(isEQ(mediactIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSubfileCnt,sv.subfilePage))) {
			a5100LoopMediact();
		}
		
		if (isEQ(mediactIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		if (isEQ(scrnparams.subfileRrn,ZERO)) {
			c1000AddBlankSubfile();
		}
	}

protected void a5100LoopMediact()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a5100Start();
				}
				case a5180Nextr: {
					a5180Nextr();
				}
				case a5190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a5100Start()
	{
		SmartFileCode.execute(appVars, mediactIO);
		if (isNE(mediactIO.getStatuz(),varcom.oK)
		&& isNE(mediactIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mediactIO.getStatuz());
			syserrrec.params.set(mediactIO.getParams());
			fatalError600();
		}
		if (isEQ(mediactIO.getStatuz(),varcom.endp)
		|| isNE(mediactIO.getChdrcoy(),wsspcomn.company)
		|| isNE(mediactIO.getActiveInd(),wsaaMediactkey.mediactActiveInd)) {
			mediactIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a5190Exit);
		}
		if (isEQ(mediactIO.getActiveInd(),"4")) {
			goTo(GotoLabel.a5180Nextr);
		}
		if (isNE(sv.mdblactf,SPACES)
		&& isNE(sv.mdblactf,mediactIO.getActiveInd())) {
			goTo(GotoLabel.a5180Nextr);
		}
		if (isEQ(wsaaScrnStatuz,varcom.insr)) {
			wsaaAlwActiveInd.set(wsaaApproved);
			wsaaItmActiveInd.set(mediactIO.getActiveInd());
			b4000ValidateActionInd();
			if (foundAllowableAction.isTrue()) {
				sv.mdblact.set(wsaaApproved);
			}
			else {
				goTo(GotoLabel.a5180Nextr);
			}
		}
		else {
			wsaaSubfileCnt.add(1);
			sv.mdblact.set(mediactIO.getActiveInd());
		}
		sv.select.set(SPACES);
		sv.invref.set(mediactIO.getInvref());
		sv.chdrnum.set(mediactIO.getChdrnum());
		sv.payto.set(mediactIO.getPaidby());
		sv.ent.set(mediactIO.getExmcode());
		b1000GetEntityDesc();
		sv.descript.set(wsaaEntityDesc);
		wsaaDesckey.descDesctabl.set(tr549);
		wsaaDesckey.descDescitem.set(sv.mdblact);
		b2000ReadDesc();
		sv.shortdesc.set(descIO.getShortdesc());
		sv.life.set(mediactIO.getLife());
		sv.jlife.set(mediactIO.getJlife());
		sv.premium.set(mediactIO.getZmedfee());
		sv.zmedtyp.set(mediactIO.getZmedtyp());
		sv.effdate.set(mediactIO.getEffdate());
		sv.seqno.set(mediactIO.getSeqno());
		b3000ReadChdrenq();
		sv.validflag.set(chdrenqIO.getValidflag());
		sv.actn.set(mediactIO.getActiveInd());
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.mdblactOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		sv.mdblactOut[varcom.nd.toInt()].set("N");
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}

protected void a5180Nextr()
	{
		mediactIO.setFunction(varcom.nextr);
	}

protected void b1000GetEntityDesc()
	{
		try {
			b1000Begin();
		}
		catch (GOTOException e){
		}
	}

protected void b1000Begin()
	{
		wsaaClntnum.set(SPACES);
		if (isEQ(sv.payto,"A")){
			b1100AgntClntnum();
		}
		else if (isEQ(sv.payto,"D")){
			b1200MedProvClntnum();
		}
		else if (isEQ(sv.payto,"C")){
			wsaaClntnum.set(sv.ent);
		}
		if (isEQ(wsaaClntnum,SPACES)) {
			wsaaEntityDesc.set(SPACES);
			goTo(GotoLabel.b1090Exit);
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.statuz.set(varcom.oK);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
		wsaaEntityDesc.set(namadrsrec.name);
	}

protected void b1100AgntClntnum()
	{
		b1100Begin();
	}

protected void b1100Begin()
	{
		agntIO.setDataArea(SPACES);
		agntIO.setStatuz(varcom.oK);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(sv.ent);
		agntIO.setFormat(agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isEQ(agntIO.getStatuz(),varcom.oK)) {
			wsaaClntnum.set(agntIO.getClntnum());
		}
	}

protected void b1200MedProvClntnum()
	{
		b1200Begin();
	}

protected void b1200Begin()
	{
		zmpvIO.setDataArea(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.ent);
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)
		&& isNE(zmpvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpvIO.getStatuz(),varcom.oK)) {
			wsaaClntnum.set(zmpvIO.getZmednum());
		}
	}

protected void b2000ReadDesc()
	{
		b2000Begin();
	}

protected void b2000Begin()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaDesckey.descDesctabl);
		descIO.setDescitem(wsaaDesckey.descDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
	}

protected void b3000ReadChdrenq()
	{
		/*B3000-START*/
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrnum);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		//Start ILIFE-1068
		//if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
		if (isNE(chdrenqIO.getStatuz(),varcom.oK) && isNE(chdrenqIO.getStatuz(),varcom.mrnf)) 

{
		//Start ILIFE-1068	
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*B3090-EXIT*/
	}

protected void b4000ValidateActionInd()
	{
		/*B4000-START*/
		b4100ReadTr549();
		wsaaFoundAllowableAction.set("N");
		for (ix.set(1); !(isGT(ix,4)
		|| foundAllowableAction.isTrue()); ix.add(1)){
			if (isNE(tr549rec.mdblact[ix.toInt()],SPACES)
			&& isEQ(tr549rec.mdblact[ix.toInt()],wsaaAlwActiveInd)) {
				wsaaFoundAllowableAction.set("Y");
			}
		}
		if (foundAllowableAction.isTrue()) {
			sv.mdblactErr.set(SPACES);
		}
		else {
			sv.mdblactErr.set(e944);
		}
		/*B4090-EXIT*/
	}

protected void b4100ReadTr549()
	{
		b4100Start();
	}

protected void b4100Start()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr549);
		itemIO.setItemitem(wsaaItmActiveInd);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		tr549rec.tr549Rec.set(itemIO.getGenarea());
	}

protected void c1000AddBlankSubfile()
	{
		c1000Start();
	}

protected void c1000Start()
	{
		sv.initialiseSubfileArea();
		sv.select.set(SPACES);
		sv.invref.set(SPACES);
		sv.chdrnum.set(SPACES);
		sv.payto.set(SPACES);
		sv.ent.set(SPACES);
		sv.descript.set(SPACES);
		sv.mdblact.set(SPACES);
		sv.shortdesc.set(SPACES);
		sv.life.set(SPACES);
		sv.jlife.set(SPACES);
		sv.premium.set(ZERO);
		sv.zmedtyp.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.seqno.set(ZERO);
		sv.validflag.set(SPACES);
		sv.actn.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.mdblactOut[varcom.pr.toInt()].set("Y");
		sv.selectOut[varcom.nd.toInt()].set("Y");
		sv.mdblactOut[varcom.nd.toInt()].set("Y");
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}
}
