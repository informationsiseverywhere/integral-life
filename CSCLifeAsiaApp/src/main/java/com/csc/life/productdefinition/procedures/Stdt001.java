/*
 * File: Stdt001.java
 * Date: 30 August 2009 2:15:38
 * Author: Quipoz Limited
 * 
 * Class transformed from STDT001.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.productdefinition.dataaccess.ChdrsdcTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrsdcTableDAM;
import com.csc.life.productdefinition.recordstructures.Stdtallrec;
import com.csc.life.productdefinition.tablestructures.T6650rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* SPECIFICATION FOR STDT001 (Stamp Duty Calculation)
*
* BACKGROUND.
* ----------
*
* This subroutine is called after obtaining it from table T6652.
* The stamp duty calculation method from T5687 is used to access
* this table.
*
* The logical view COVRSDC, for COVR, is to be created. It will
* contain the same key as COVR with the sum assured being added as
* the item of main interest.
*
* PROCESSING.
* ----------
*
* In order to obtain the Stamp Duty Rates read the T6650, keyed
* by;- Currency and effective date.  The data on T6650 is accessed
* by sum assured up to a certain amount.
*
* Read COVRSDC
*
*   - keyed by, Company, Contract number, Life, Coverage, Rider
*     and Plan suffix.
*
*   - read COVR, logical view COVRSDC and obtain the current Sum m
*     Assured. Read the COVRSDC again (no more records will be
*     present for New Business) and obtain a history record.
*     Subtract the Sum Assured (history) from the Sum Assured
*     (current) and if the difference is greater than zero, then
*     process for Stamp Duty, otherwise exit with a value of zero
*     in the Stamp Duty field.
*
* Policy Level
*
*   - check for the Plan Suffix equal to zero, if it is not equalal
*     to zero, then process for Stamp Duty as normal (see Calculate
*     -te Stamp Duty below).
*
*   - if the Plan Suffix is equal to zero, then divide the Sum
*     Assured by the number of policies summarised (from the
*     contract header, read the CHDRPF with a logical view of
*     CHDRSDC). The field required is POLSUM. The Stamp Duty
*     calculation is then performed and before the Stamp Duty
*     amount is passed back it is multiplied by the number of
*     policies in the POLSUM field.
*
* Calculate Stamp Duty
*
*   - please note that the amount calculated per entry of Sum
*     Assured is at the rate per factor or part thereof. That is,
*     if the sum assured is for $6500, the amount being calculated
*     would regard this as $7000 (i.e. if the factor was 1000).
*
*     The Sum Assured may satisfy more than one entry, so all the
*     entries are checked until the Sum Assured does not exceed  ne
*     one of the limits on the table.
*
*     For each entry range that the Sum Assured satisfies, the
*     amount in the range must be divided by the factor and
*     multiplied by the rate. This amount is stored. The next    ge
*     range is checked and the same applies.
*
*     It may occur within the range,that the Sum Assured does nott
*     exceed or even match the limit. In this case the factor and
*     the rate only apply to the balance of the Sum Assured      n
*     within this range and not to the range limit itself.
*
*     For every entry range satisfied, a stamp duty amount is
*     calculated. When all the ranges are satisfied the total
*     amount accumulated is returned as the stamp duty applicable
*     for this currency.
*
*     If the Plan Suffix is equal to zero (see the section on
*     policy level above), multiply the Stamp Duty amount by the
*     number of policy summerised (POLSUM).
*
* The Stamp Duty is then returned through the linkage.
*
*****************************************************************
* </pre>
*/
public class Stdt001 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "STDT001";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaCovrsdcCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSuminsCur = new PackedDecimalData(15, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSuminsHis = new PackedDecimalData(15, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSumAssured = new PackedDecimalData(15, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaStampDuty = new PackedDecimalData(15, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaRangeDuty = new PackedDecimalData(15, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSaRem = new PackedDecimalData(15, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaFraction = new PackedDecimalData(5, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaRemainder = new PackedDecimalData(2, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaEndFlag = new FixedLengthStringData(1);
	private Validator wsaaEndRange = new Validator(wsaaEndFlag, "Y");
		/* TABLES */
	private static final String t6650 = "T6650";
		/* ERRORS */
	private static final String r075 = "R075";
	private ChdrsdcTableDAM chdrsdcIO = new ChdrsdcTableDAM();
	private CovrsdcTableDAM covrsdcIO = new CovrsdcTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T6650rec t6650rec = new T6650rec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Stdtallrec stdtallrec = new Stdtallrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit290, 
		exit390, 
		exit490
	}

	public Stdt001() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		stdtallrec.stdt001Rec = convertAndSetParam(stdtallrec.stdt001Rec, parmArray, 0);
		try {
			subroutineMain000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void subroutineMain000()
	{
		/*MAIN*/
		/*    Main Control paragraph controlling the subroutine processing*/
		initialise100();
		process200();
		if (isGT(wsaaSumAssured, ZERO)) {
			planSuffix300();
			stampDuty400();
			termination500();
		}
		else {
			stdtallrec.stampDuty.set(ZERO);
		}
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise100()
	{
		/*INITIALISE*/
		/*    Set up the system variables.*/
		syserrrec.subrname.set(wsaaSubr);
		wsaaSub1.set(ZERO);
		wsaaSumAssured.set(ZERO);
		wsaaStampDuty.set(ZERO);
		wsaaRangeDuty.set(ZERO);
		wsaaSaRem.set(ZERO);
		wsaaFraction.set(ZERO);
		wsaaRemainder.set(ZERO);
		/*EXIT*/
	}

	/**
	* <pre>
	*      ALL PROCESSING
	* </pre>
	*/
protected void process200()
	{
		try {
			stampDutyRatetable210();
			readCovrsdc220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void stampDutyRatetable210()
	{
		/*    Read table T6650 using ITDM to obtain the Stamp Duty Rates.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(stdtallrec.company);
		itdmIO.setItemtabl(t6650);
		itdmIO.setItemitem(stdtallrec.cntcurr);
		itdmIO.setItmfrm(stdtallrec.effdate);
		/*    MOVE READR                  TO ITDM-FUNCTION.                */
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), stdtallrec.company)
		|| isNE(itdmIO.getItemtabl(), t6650)
		|| isNE(itdmIO.getItemitem(), stdtallrec.cntcurr)) {
			itdmIO.setItemcoy(stdtallrec.company);
			itdmIO.setItemtabl(t6650);
			itdmIO.setItemitem(stdtallrec.cntcurr);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(r075);
			syserr700();
		}
		t6650rec.t6650Rec.set(itdmIO.getGenarea());
	}

protected void readCovrsdc220()
	{
		/*    Read Logical View twice : Once for the Current Sum Assured*/
		/*    and then for the Historic Sum Assured.*/
		covrsdcIO.setDataKey(SPACES);
		covrsdcIO.setChdrcoy(stdtallrec.company);
		covrsdcIO.setChdrnum(stdtallrec.chdrnum);
		covrsdcIO.setLife(stdtallrec.life);
		covrsdcIO.setCoverage(stdtallrec.coverage);
		covrsdcIO.setRider(stdtallrec.rider);
		covrsdcIO.setPlanSuffix(stdtallrec.plnsfx);
		covrsdcIO.setFunction(varcom.readr);
		/*    Read Logical View for a Sum Assured Current and Historical*/
		/*    Subtract one form the other giving the Sum Assured.*/
		wsaaCovrsdcCount.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 2); loopVar1 += 1){
			sumAssuredCurhis230();
		}
		compute(wsaaSumAssured, 0).set(sub(wsaaSuminsHis, wsaaSuminsHis));
		goTo(GotoLabel.exit290);
	}

protected void sumAssuredCurhis230()
	{
		/*    Read Logical View for the sum assured.*/
		SmartFileCode.execute(appVars, covrsdcIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaCovrsdcCount, 1)) {
			wsaaCovrsdcCount.add(1);
		}
		else {
			wsaaSuminsHis.set(covrsdcIO.getSumins());
		}
		covrsdcIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*    CHECK PLAN SUFFIX FOR STAMP DUTY CALCULATION.
	* </pre>
	*/
protected void planSuffix300()
	{
		try {
			checkPlanSuffix310();
			readChdrsdc320();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkPlanSuffix310()
	{
		/*    Check for Plan Suffix greater then zero.*/
		if (isNE(stdtallrec.plnsfx, ZERO)) {
			goTo(GotoLabel.exit390);
		}
	}

protected void readChdrsdc320()
	{
		/*    Read Logical View for the number of policies held.*/
		chdrsdcIO.setDataKey(SPACES);
		chdrsdcIO.setChdrcoy(stdtallrec.company);
		chdrsdcIO.setChdrnum(stdtallrec.chdrnum);
		chdrsdcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrsdcIO);
		if (isNE(chdrsdcIO.getStatuz(), varcom.oK)
		&& isNE(chdrsdcIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(chdrsdcIO.getStatuz());
			syserrrec.params.set(chdrsdcIO.getParams());
			fatalError600();
		}
		/*CALC-SUM-ASSURED*/
		/*    Calculate the amount of 'Sum Assured' for a single policy.*/
		compute(wsaaSumAssured, 0).set(div(wsaaSumAssured, chdrsdcIO.getPolsum()));
	}

	/**
	* <pre>
	*    CLACULATE STAMP DUTY.
	* </pre>
	*/
protected void stampDuty400()
	{
		try {
			mainStampDuty410();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void mainStampDuty410()
	{
		/*    Calculate the  amount of Stamp Duty due within the Range of*/
		/*    Rates for the total sum assured.*/
		wsaaSaRem.set(wsaaSumAssured);
		for (wsaaSub1.set(1); !(wsaaEndRange.isTrue()); wsaaSub1.add(1)){
			findRange420();
		}
		goTo(GotoLabel.exit490);
	}

protected void findRange420()
	{
		/*    The sum assured may satisfy more than one entry, so all the*/
		/*    entries are checked until the sum  assured does not  exceed*/
		/*    one of the limits on the table.*/
		/*  IF WSAA-SUM-ASSURED         NOT > T6650-SI (WSAA-SUB1<D96NUM>*/
		if (isLTE(wsaaSumAssured, t6650rec.sumin[wsaaSub1.toInt()])) {
			lastRange440();
		}
		else {
			fullRange430();
		}
		/*    Accumulate all the rates satisfied within the ranges.*/
		wsaaStampDuty.add(wsaaRangeDuty);
	}

protected void fullRange430()
	{
		/*    The full amount that is contained  between  the current and*/
		/*    previous ranges is calculated on that current rate.*/
		compute(wsaaRangeDuty, 5).setRounded(mult((div(t6650rec.sumin[wsaaSub1.toInt()], t6650rec.fact[wsaaSub1.toInt()])), t6650rec.rrat[wsaaSub1.toInt()]));
		/*  SUBTRACT T6650-SI (WSAA-SUB1)                        <D96NUM>*/
		compute(wsaaSaRem, 0).set(sub(wsaaSumAssured, t6650rec.sumin[wsaaSub1.toInt()]));
	}

protected void lastRange440()
	{
		/*    When the  sum  assured  falls  below  the  range limit, the*/
		/*    balance within this range is used.*/
		/*    Exit Stamp Duty Calculation.*/
		/*    The fraction is always rounded up  to the nearest whole num*/
		/*    -ber of the Factor.*/
		compute(wsaaFraction, 0).setDivide(wsaaSaRem, (t6650rec.fact[wsaaSub1.toInt()]));
		wsaaRemainder.setRemainder(wsaaFraction);
		if (isGT(wsaaRemainder, ZERO)) {
			wsaaFraction.add(1);
		}
		compute(wsaaRangeDuty, 5).setRounded((mult(wsaaFraction, t6650rec.rrat[wsaaSub1.toInt()])));
		wsaaEndFlag.set("Y");
	}

	/**
	* <pre>
	*      TERMINATION
	* </pre>
	*/
protected void termination500()
	{
		/*TERMINATION*/
		/*    Multiply the calculated stamp duty by the number of policies*/
		/*    found in CHDR.*/
		if (isNE(stdtallrec.plnsfx, ZERO)) {
			compute(stdtallrec.stampDuty, 2).set(mult(wsaaStampDuty, chdrsdcIO.getPolsum()));
		}
		stdtallrec.stampDuty.set(wsaaStampDuty);
		/*EXIT*/
	}

	/**
	* <pre>
	*      REPORT A FATAL ERROR CONDITION.
	* </pre>
	*/
protected void fatalError600()
	{
		/*FATAL-ERROR*/
		/*    if a fatal error occurs then dump to the system.*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		stdtallrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void syserr700()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		stdtallrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
