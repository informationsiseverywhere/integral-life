package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5677
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class S5677ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(445);
	public FixedLengthStringData dataFields = new FixedLengthStringData(109).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData fupcdess = new FixedLengthStringData(64).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(16, 4, fupcdess, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(64).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01 = DD.fupcdes.copy().isAPartOf(filler,0);
	public FixedLengthStringData fupcdes02 = DD.fupcdes.copy().isAPartOf(filler,4);
	public FixedLengthStringData fupcdes03 = DD.fupcdes.copy().isAPartOf(filler,8);
	public FixedLengthStringData fupcdes04 = DD.fupcdes.copy().isAPartOf(filler,12);
	public FixedLengthStringData fupcdes05 = DD.fupcdes.copy().isAPartOf(filler,16);
	public FixedLengthStringData fupcdes06 = DD.fupcdes.copy().isAPartOf(filler,20);
	public FixedLengthStringData fupcdes07 = DD.fupcdes.copy().isAPartOf(filler,24);
	public FixedLengthStringData fupcdes08 = DD.fupcdes.copy().isAPartOf(filler,28);
	public FixedLengthStringData fupcdes09 = DD.fupcdes.copy().isAPartOf(filler,32);
	public FixedLengthStringData fupcdes10 = DD.fupcdes.copy().isAPartOf(filler,36);
	public FixedLengthStringData fupcdes11 = DD.fupcdes.copy().isAPartOf(filler,40);
	public FixedLengthStringData fupcdes12 = DD.fupcdes.copy().isAPartOf(filler,44);
	public FixedLengthStringData fupcdes13 = DD.fupcdes.copy().isAPartOf(filler,48);
	public FixedLengthStringData fupcdes14 = DD.fupcdes.copy().isAPartOf(filler,52);
	public FixedLengthStringData fupcdes15 = DD.fupcdes.copy().isAPartOf(filler,56);
	public FixedLengthStringData fupcdes16 = DD.fupcdes.copy().isAPartOf(filler,60);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 109);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData fupcdessErr = new FixedLengthStringData(64).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] fupcdesErr = FLSArrayPartOfStructure(16, 4, fupcdessErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(64).isAPartOf(fupcdessErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData fupcdes02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData fupcdes03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData fupcdes04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData fupcdes05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData fupcdes06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData fupcdes07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData fupcdes08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData fupcdes09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData fupcdes10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData fupcdes11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData fupcdes12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData fupcdes13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData fupcdes14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData fupcdes15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData fupcdes16Err = new FixedLengthStringData(4).isAPartOf(filler1, 60);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 193);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData fupcdessOut = new FixedLengthStringData(192).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(16, 12, fupcdessOut, 0);
	public FixedLengthStringData[][] fupcdesO = FLSDArrayPartOfArrayStructure(12, 1, fupcdesOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(192).isAPartOf(fupcdessOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fupcdes01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] fupcdes02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] fupcdes03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] fupcdes04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] fupcdes05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] fupcdes06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] fupcdes07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] fupcdes08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] fupcdes09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] fupcdes10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] fupcdes11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] fupcdes12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] fupcdes13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] fupcdes14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] fupcdes15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public FixedLengthStringData[] fupcdes16Out = FLSArrayPartOfStructure(12, 1, filler2, 180);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5677screenWritten = new LongData(0);
	public LongData S5677protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5677ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(fupcdes01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes13Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes14Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes15Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes16Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {language, company, tabl, item, longdesc, fupcdes01, fupcdes02, fupcdes03, fupcdes04, fupcdes05, fupcdes06, fupcdes07, fupcdes08, fupcdes09, fupcdes10, fupcdes11, fupcdes12, fupcdes13, fupcdes14, fupcdes15, fupcdes16};
		screenOutFields = new BaseData[][] {languageOut, companyOut, tablOut, itemOut, longdescOut, fupcdes01Out, fupcdes02Out, fupcdes03Out, fupcdes04Out, fupcdes05Out, fupcdes06Out, fupcdes07Out, fupcdes08Out, fupcdes09Out, fupcdes10Out, fupcdes11Out, fupcdes12Out, fupcdes13Out, fupcdes14Out, fupcdes15Out, fupcdes16Out};
		screenErrFields = new BaseData[] {languageErr, companyErr, tablErr, itemErr, longdescErr, fupcdes01Err, fupcdes02Err, fupcdes03Err, fupcdes04Err, fupcdes05Err, fupcdes06Err, fupcdes07Err, fupcdes08Err, fupcdes09Err, fupcdes10Err, fupcdes11Err, fupcdes12Err, fupcdes13Err, fupcdes14Err, fupcdes15Err, fupcdes16Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5677screen.class;
		protectRecord = S5677protect.class;
	}

}
