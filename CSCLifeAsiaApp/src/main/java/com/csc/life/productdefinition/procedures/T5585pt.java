/*
 * File: T5585pt.java
 * Date: 30 August 2009 2:23:22
 * Author: Quipoz Limited
 * 
 * Class transformed from T5585PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5585.
*
*
*****************************************************************
* </pre>
*/
public class T5585pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5585rec t5585rec = new T5585rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5585pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5585rec.t5585Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5585rec.sexageadj);
		generalCopyLinesInner.fieldNo008.set(t5585rec.hghlowage);
		generalCopyLinesInner.fieldNo009.set(t5585rec.agedif01);
		generalCopyLinesInner.fieldNo010.set(t5585rec.addage01);
		generalCopyLinesInner.fieldNo011.set(t5585rec.agedif02);
		generalCopyLinesInner.fieldNo012.set(t5585rec.addage02);
		generalCopyLinesInner.fieldNo013.set(t5585rec.agelimit01);
		generalCopyLinesInner.fieldNo014.set(t5585rec.ageadj01);
		generalCopyLinesInner.fieldNo015.set(t5585rec.agedif03);
		generalCopyLinesInner.fieldNo016.set(t5585rec.addage03);
		generalCopyLinesInner.fieldNo017.set(t5585rec.agedif04);
		generalCopyLinesInner.fieldNo018.set(t5585rec.addage04);
		generalCopyLinesInner.fieldNo019.set(t5585rec.agelimit02);
		generalCopyLinesInner.fieldNo020.set(t5585rec.ageadj02);
		generalCopyLinesInner.fieldNo021.set(t5585rec.agedif05);
		generalCopyLinesInner.fieldNo022.set(t5585rec.addage05);
		generalCopyLinesInner.fieldNo023.set(t5585rec.agedif06);
		generalCopyLinesInner.fieldNo024.set(t5585rec.addage06);
		generalCopyLinesInner.fieldNo025.set(t5585rec.agelimit03);
		generalCopyLinesInner.fieldNo026.set(t5585rec.ageadj03);
		generalCopyLinesInner.fieldNo027.set(t5585rec.agedif07);
		generalCopyLinesInner.fieldNo028.set(t5585rec.addage07);
		generalCopyLinesInner.fieldNo029.set(t5585rec.agedif08);
		generalCopyLinesInner.fieldNo030.set(t5585rec.addage08);
		generalCopyLinesInner.fieldNo031.set(t5585rec.agelimit04);
		generalCopyLinesInner.fieldNo032.set(t5585rec.ageadj04);
		generalCopyLinesInner.fieldNo033.set(t5585rec.agedif09);
		generalCopyLinesInner.fieldNo034.set(t5585rec.addage09);
		generalCopyLinesInner.fieldNo035.set(t5585rec.agedif10);
		generalCopyLinesInner.fieldNo036.set(t5585rec.addage10);
		generalCopyLinesInner.fieldNo037.set(t5585rec.agelimit05);
		generalCopyLinesInner.fieldNo038.set(t5585rec.ageadj05);
		generalCopyLinesInner.fieldNo039.set(t5585rec.agedif11);
		generalCopyLinesInner.fieldNo040.set(t5585rec.addage11);
		generalCopyLinesInner.fieldNo041.set(t5585rec.agedif12);
		generalCopyLinesInner.fieldNo042.set(t5585rec.addage12);
		generalCopyLinesInner.fieldNo043.set(t5585rec.agelimit06);
		generalCopyLinesInner.fieldNo044.set(t5585rec.ageadj06);
		generalCopyLinesInner.fieldNo045.set(t5585rec.agedif13);
		generalCopyLinesInner.fieldNo046.set(t5585rec.addage13);
		generalCopyLinesInner.fieldNo047.set(t5585rec.agedif14);
		generalCopyLinesInner.fieldNo048.set(t5585rec.addage14);
		generalCopyLinesInner.fieldNo049.set(t5585rec.agelimit07);
		generalCopyLinesInner.fieldNo050.set(t5585rec.ageadj07);
		generalCopyLinesInner.fieldNo051.set(t5585rec.agedif15);
		generalCopyLinesInner.fieldNo052.set(t5585rec.addage15);
		generalCopyLinesInner.fieldNo053.set(t5585rec.agedif16);
		generalCopyLinesInner.fieldNo054.set(t5585rec.addage16);
		generalCopyLinesInner.fieldNo055.set(t5585rec.agelimit08);
		generalCopyLinesInner.fieldNo056.set(t5585rec.ageadj08);
		generalCopyLinesInner.fieldNo057.set(t5585rec.agedif17);
		generalCopyLinesInner.fieldNo058.set(t5585rec.addage17);
		generalCopyLinesInner.fieldNo059.set(t5585rec.agedif18);
		generalCopyLinesInner.fieldNo060.set(t5585rec.addage18);
		generalCopyLinesInner.fieldNo061.set(t5585rec.agelimit09);
		generalCopyLinesInner.fieldNo062.set(t5585rec.ageadj09);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Joint Life Premium Parameters             S5585");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(77);
	private FixedLengthStringData filler7 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 14);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 24, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 31);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 41, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 47, FILLER).init("Sex:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine003, 53);
	private FixedLengthStringData filler11 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 54, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 63, FILLER).init("High / Low:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine003, 76);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(73);
	private FixedLengthStringData filler13 = new FixedLengthStringData(73).isAPartOf(wsaaPrtLine004, 0, FILLER).init("    Age         Addition       Age       Addition        UpTo     Adjust");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(72);
	private FixedLengthStringData filler14 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Difference      To Age     Difference    To Age          Age      (+/-)");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(72);
	private FixedLengthStringData filler15 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 5).setPattern("ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler17 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 32).setPattern("ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler19 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 59).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(72);
	private FixedLengthStringData filler21 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 5).setPattern("ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler23 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 32).setPattern("ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler25 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 59).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(72);
	private FixedLengthStringData filler27 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 5).setPattern("ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler29 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 32).setPattern("ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler31 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(72);
	private FixedLengthStringData filler33 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 5).setPattern("ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler35 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 32).setPattern("ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler37 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(72);
	private FixedLengthStringData filler39 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 5).setPattern("ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler41 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 32).setPattern("ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler43 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(72);
	private FixedLengthStringData filler45 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 5).setPattern("ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler47 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 32).setPattern("ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler49 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 59).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(72);
	private FixedLengthStringData filler51 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 5).setPattern("ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler53 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 32).setPattern("ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler55 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(72);
	private FixedLengthStringData filler57 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 5).setPattern("ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler59 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 32).setPattern("ZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler61 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 59).setPattern("ZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 69).setPattern("ZZ-");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(72);
	private FixedLengthStringData filler63 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 5).setPattern("ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 19).setPattern("ZZZ-");
	private FixedLengthStringData filler65 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 32).setPattern("ZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 44).setPattern("ZZZ-");
	private FixedLengthStringData filler67 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 69).setPattern("ZZ-");
}
}
