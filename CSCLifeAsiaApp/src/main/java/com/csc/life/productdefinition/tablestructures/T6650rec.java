package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:54
 * Description:
 * Copybook name: T6650REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6650rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6650Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData facts = new FixedLengthStringData(30).isAPartOf(t6650Rec, 0);
  	public ZonedDecimalData[] fact = ZDArrayPartOfStructure(6, 5, 0, facts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(facts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData fact01 = new ZonedDecimalData(5, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData fact02 = new ZonedDecimalData(5, 0).isAPartOf(filler, 5);
  	public ZonedDecimalData fact03 = new ZonedDecimalData(5, 0).isAPartOf(filler, 10);
  	public ZonedDecimalData fact04 = new ZonedDecimalData(5, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData fact05 = new ZonedDecimalData(5, 0).isAPartOf(filler, 20);
  	public ZonedDecimalData fact06 = new ZonedDecimalData(5, 0).isAPartOf(filler, 25);
  	public FixedLengthStringData rrats = new FixedLengthStringData(36).isAPartOf(t6650Rec, 30);
  	public ZonedDecimalData[] rrat = ZDArrayPartOfStructure(6, 6, 4, rrats, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(rrats, 0, FILLER_REDEFINE);
  	public ZonedDecimalData rrat01 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 0);
  	public ZonedDecimalData rrat02 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 6);
  	public ZonedDecimalData rrat03 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 12);
  	public ZonedDecimalData rrat04 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 18);
  	public ZonedDecimalData rrat05 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 24);
  	public ZonedDecimalData rrat06 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 30);
  	public FixedLengthStringData sumins = new FixedLengthStringData(90).isAPartOf(t6650Rec, 66);
  	public ZonedDecimalData[] sumin = ZDArrayPartOfStructure(6, 15, 0, sumins, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(90).isAPartOf(sumins, 0, FILLER_REDEFINE);
  	public ZonedDecimalData sumin01 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData sumin02 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 15);
  	public ZonedDecimalData sumin03 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 30);
  	public ZonedDecimalData sumin04 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 45);
  	public ZonedDecimalData sumin05 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 60);
  	public ZonedDecimalData sumin06 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 75);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(344).isAPartOf(t6650Rec, 156, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6650Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6650Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}