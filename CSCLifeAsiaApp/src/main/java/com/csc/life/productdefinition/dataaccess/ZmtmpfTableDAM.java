package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZmtmpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:00
 * Class transformed from ZMTMPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZmtmpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 129;
	public FixedLengthStringData zmtmrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zmtmpfRecord = zmtmrec;
	
	public FixedLengthStringData zmedcoy = DD.zmedcoy.copy().isAPartOf(zmtmrec);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(zmtmrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(zmtmrec);
	public FixedLengthStringData txtline = DD.txtline.copy().isAPartOf(zmtmrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zmtmrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zmtmrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zmtmrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZmtmpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZmtmpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZmtmpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZmtmpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmtmpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZmtmpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmtmpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZMTMPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ZMEDCOY, " +
							"ZMEDPRV, " +
							"SEQNO, " +
							"TXTLINE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     zmedcoy,
                                     zmedprv,
                                     seqno,
                                     txtline,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		zmedcoy.clear();
  		zmedprv.clear();
  		seqno.clear();
  		txtline.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZmtmrec() {
  		return zmtmrec;
	}

	public FixedLengthStringData getZmtmpfRecord() {
  		return zmtmpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZmtmrec(what);
	}

	public void setZmtmrec(Object what) {
  		this.zmtmrec.set(what);
	}

	public void setZmtmpfRecord(Object what) {
  		this.zmtmpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zmtmrec.getLength());
		result.set(zmtmrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}