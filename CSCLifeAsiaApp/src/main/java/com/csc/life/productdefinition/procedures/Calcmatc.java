package com.csc.life.productdefinition.procedures;

import java.util.HashMap;

import com.csc.life.productdefinition.recordstructures.Vpxmatcrec;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.util.QPUtilities;

public class Calcmatc extends Vpmcalc {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private Vpxmatcrec vpxmatcrec = new Vpxmatcrec();

	public Calcmatc() {
		super();
	}

	public void mainline(Object... parmArray)
	{	
		recordMap = new HashMap<String, ExternalData>();
		recordMap.put("VPMFMTREC", vpmfmtrec);
		
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		
		recordMap.put("VPXMATCREC", vpxmatcrec);
		
		try {
			startSubr010();
			
		}catch (Exception e) {
		}		
	}
	
}
