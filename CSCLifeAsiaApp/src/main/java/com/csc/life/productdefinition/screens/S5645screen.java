package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5645screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5645ScreenVars sv = (S5645ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5645screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5645ScreenVars screenVars = (S5645ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.sacscode01.setClassString("");
		screenVars.sacstype01.setClassString("");
		screenVars.glmap01.setClassString("");
		screenVars.sign01.setClassString("");
		screenVars.cnttot01.setClassString("");
		screenVars.sacscode02.setClassString("");
		screenVars.sacstype02.setClassString("");
		screenVars.glmap02.setClassString("");
		screenVars.sign02.setClassString("");
		screenVars.cnttot02.setClassString("");
		screenVars.sacscode03.setClassString("");
		screenVars.sacstype03.setClassString("");
		screenVars.glmap03.setClassString("");
		screenVars.sign03.setClassString("");
		screenVars.cnttot03.setClassString("");
		screenVars.sacscode04.setClassString("");
		screenVars.sacstype04.setClassString("");
		screenVars.glmap04.setClassString("");
		screenVars.sign04.setClassString("");
		screenVars.cnttot04.setClassString("");
		screenVars.sacscode05.setClassString("");
		screenVars.sacstype05.setClassString("");
		screenVars.glmap05.setClassString("");
		screenVars.sign05.setClassString("");
		screenVars.cnttot05.setClassString("");
		screenVars.sacscode06.setClassString("");
		screenVars.sacstype06.setClassString("");
		screenVars.glmap06.setClassString("");
		screenVars.sign06.setClassString("");
		screenVars.cnttot06.setClassString("");
		screenVars.sacscode07.setClassString("");
		screenVars.sacstype07.setClassString("");
		screenVars.glmap07.setClassString("");
		screenVars.sign07.setClassString("");
		screenVars.cnttot07.setClassString("");
		screenVars.sacscode08.setClassString("");
		screenVars.sacstype08.setClassString("");
		screenVars.glmap08.setClassString("");
		screenVars.sign08.setClassString("");
		screenVars.cnttot08.setClassString("");
		screenVars.sacscode09.setClassString("");
		screenVars.sacstype09.setClassString("");
		screenVars.glmap09.setClassString("");
		screenVars.sign09.setClassString("");
		screenVars.cnttot09.setClassString("");
		screenVars.sacscode10.setClassString("");
		screenVars.sacstype10.setClassString("");
		screenVars.glmap10.setClassString("");
		screenVars.sign10.setClassString("");
		screenVars.cnttot10.setClassString("");
		screenVars.sacscode11.setClassString("");
		screenVars.sacstype11.setClassString("");
		screenVars.glmap11.setClassString("");
		screenVars.sign11.setClassString("");
		screenVars.cnttot11.setClassString("");
		screenVars.sacscode12.setClassString("");
		screenVars.sacstype12.setClassString("");
		screenVars.glmap12.setClassString("");
		screenVars.sign12.setClassString("");
		screenVars.cnttot12.setClassString("");
		screenVars.sacscode13.setClassString("");
		screenVars.sacstype13.setClassString("");
		screenVars.glmap13.setClassString("");
		screenVars.sign13.setClassString("");
		screenVars.cnttot13.setClassString("");
		screenVars.sacscode14.setClassString("");
		screenVars.sacstype14.setClassString("");
		screenVars.glmap14.setClassString("");
		screenVars.sign14.setClassString("");
		screenVars.cnttot14.setClassString("");
		screenVars.sacscode15.setClassString("");
		screenVars.sacstype15.setClassString("");
		screenVars.glmap15.setClassString("");
		screenVars.sign15.setClassString("");
		screenVars.cnttot15.setClassString("");
	}

/**
 * Clear all the variables in S5645screen
 */
	public static void clear(VarModel pv) {
		S5645ScreenVars screenVars = (S5645ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.sacscode01.clear();
		screenVars.sacstype01.clear();
		screenVars.glmap01.clear();
		screenVars.sign01.clear();
		screenVars.cnttot01.clear();
		screenVars.sacscode02.clear();
		screenVars.sacstype02.clear();
		screenVars.glmap02.clear();
		screenVars.sign02.clear();
		screenVars.cnttot02.clear();
		screenVars.sacscode03.clear();
		screenVars.sacstype03.clear();
		screenVars.glmap03.clear();
		screenVars.sign03.clear();
		screenVars.cnttot03.clear();
		screenVars.sacscode04.clear();
		screenVars.sacstype04.clear();
		screenVars.glmap04.clear();
		screenVars.sign04.clear();
		screenVars.cnttot04.clear();
		screenVars.sacscode05.clear();
		screenVars.sacstype05.clear();
		screenVars.glmap05.clear();
		screenVars.sign05.clear();
		screenVars.cnttot05.clear();
		screenVars.sacscode06.clear();
		screenVars.sacstype06.clear();
		screenVars.glmap06.clear();
		screenVars.sign06.clear();
		screenVars.cnttot06.clear();
		screenVars.sacscode07.clear();
		screenVars.sacstype07.clear();
		screenVars.glmap07.clear();
		screenVars.sign07.clear();
		screenVars.cnttot07.clear();
		screenVars.sacscode08.clear();
		screenVars.sacstype08.clear();
		screenVars.glmap08.clear();
		screenVars.sign08.clear();
		screenVars.cnttot08.clear();
		screenVars.sacscode09.clear();
		screenVars.sacstype09.clear();
		screenVars.glmap09.clear();
		screenVars.sign09.clear();
		screenVars.cnttot09.clear();
		screenVars.sacscode10.clear();
		screenVars.sacstype10.clear();
		screenVars.glmap10.clear();
		screenVars.sign10.clear();
		screenVars.cnttot10.clear();
		screenVars.sacscode11.clear();
		screenVars.sacstype11.clear();
		screenVars.glmap11.clear();
		screenVars.sign11.clear();
		screenVars.cnttot11.clear();
		screenVars.sacscode12.clear();
		screenVars.sacstype12.clear();
		screenVars.glmap12.clear();
		screenVars.sign12.clear();
		screenVars.cnttot12.clear();
		screenVars.sacscode13.clear();
		screenVars.sacstype13.clear();
		screenVars.glmap13.clear();
		screenVars.sign13.clear();
		screenVars.cnttot13.clear();
		screenVars.sacscode14.clear();
		screenVars.sacstype14.clear();
		screenVars.glmap14.clear();
		screenVars.sign14.clear();
		screenVars.cnttot14.clear();
		screenVars.sacscode15.clear();
		screenVars.sacstype15.clear();
		screenVars.glmap15.clear();
		screenVars.sign15.clear();
		screenVars.cnttot15.clear();
	}
}
