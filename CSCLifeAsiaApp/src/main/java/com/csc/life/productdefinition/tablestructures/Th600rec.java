package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:31
 * Description:
 * Copybook name: TH600REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th600rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th600Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData liacoy = new FixedLengthStringData(2).isAPartOf(th600Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(th600Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th600Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th600Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}