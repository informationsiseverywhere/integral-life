package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChevpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:25
 * Class transformed from CHEVPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChevpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 226;
	public FixedLengthStringData chevrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData chevpfRecord = chevrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(chevrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(chevrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(chevrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(chevrec);
	public PackedDecimalData rcdate = DD.rcdate.copy().isAPartOf(chevrec);
	public FixedLengthStringData ownnam = DD.ownnam.copy().isAPartOf(chevrec);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(chevrec);
	public PackedDecimalData hoissdte = DD.hoissdte.copy().isAPartOf(chevrec);
	public PackedDecimalData hissdte = DD.hissdte.copy().isAPartOf(chevrec);
	public FixedLengthStringData agentno = DD.agentno.copy().isAPartOf(chevrec);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(chevrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(chevrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(chevrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(chevrec);
	public PackedDecimalData ptdate = DD.ptdate.copy().isAPartOf(chevrec);
	public PackedDecimalData btdate = DD.btdate.copy().isAPartOf(chevrec);
	public PackedDecimalData totlprem = DD.totlprem.copy().isAPartOf(chevrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(chevrec);
	public PackedDecimalData cshmt = DD.cshmt.copy().isAPartOf(chevrec);
	public PackedDecimalData loanvalue = DD.loanvalue.copy().isAPartOf(chevrec);
	public PackedDecimalData surrchg = DD.surrchg.copy().isAPartOf(chevrec);
	public PackedDecimalData surrval = DD.surrval.copy().isAPartOf(chevrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(chevrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(chevrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(chevrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(chevrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(chevrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ChevpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ChevpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ChevpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ChevpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ChevpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ChevpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ChevpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CHEVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"EFFDATE, " +
							"CNTTYPE, " +
							"RCDATE, " +
							"OWNNAM, " +
							"PAYORNAME, " +
							"HOISSDTE, " +
							"HISSDTE, " +
							"AGENTNO, " +
							"AGTYPE, " +
							"BILLFREQ, " +
							"STATCODE, " +
							"PSTATCODE, " +
							"PTDATE, " +
							"BTDATE, " +
							"TOTLPREM, " +
							"INSTPREM, " +
							"CSHMT, " +
							"LOANVALUE, " +
							"SURRCHG, " +
							"SURRVAL, " +
							"CURRFROM, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     effdate,
                                     cnttype,
                                     rcdate,
                                     ownnam,
                                     payorname,
                                     hoissdte,
                                     hissdte,
                                     agentno,
                                     agtype,
                                     billfreq,
                                     statcode,
                                     pstatcode,
                                     ptdate,
                                     btdate,
                                     totlprem,
                                     instprem,
                                     cshmt,
                                     loanvalue,
                                     surrchg,
                                     surrval,
                                     currfrom,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		effdate.clear();
  		cnttype.clear();
  		rcdate.clear();
  		ownnam.clear();
  		payorname.clear();
  		hoissdte.clear();
  		hissdte.clear();
  		agentno.clear();
  		agtype.clear();
  		billfreq.clear();
  		statcode.clear();
  		pstatcode.clear();
  		ptdate.clear();
  		btdate.clear();
  		totlprem.clear();
  		instprem.clear();
  		cshmt.clear();
  		loanvalue.clear();
  		surrchg.clear();
  		surrval.clear();
  		currfrom.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getChevrec() {
  		return chevrec;
	}

	public FixedLengthStringData getChevpfRecord() {
  		return chevpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setChevrec(what);
	}

	public void setChevrec(Object what) {
  		this.chevrec.set(what);
	}

	public void setChevpfRecord(Object what) {
  		this.chevpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(chevrec.getLength());
		result.set(chevrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}