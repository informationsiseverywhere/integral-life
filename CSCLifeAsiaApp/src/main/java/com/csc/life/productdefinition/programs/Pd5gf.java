
package com.csc.life.productdefinition.programs;

/**
 * 
 * China Freelook Cancellation Rules
 * 
 * This new screen is for new Smart table TD5GF which has all the fields to be
 * configured for Freelook cancellation.
 * 
 * The Items configured in the table are Product types having below fields:
 * 
 * I. 	Date Basis: This is used in calculating the cooling off period for freelook cancellation.
	 		It has 4 valid options as below:
	 		1. Policy Issue Date
	 		2. Acknowledgement Date
	 		3. Deemed Received Date
	 		4. Min (Deemed Received Date and Acknowledgement Date)
 * 
 * II.  Gender Fields(4 rows): Allowable input is only M or F
 * 		If no valid value is provided "Entry not allowed" error message will be thrown
 * 
 * III. From Age(4 rows): Allowable input is from 0 to 999
 * 
 * IV.  To Age: Allowable input is from 0 to 999
 * 
 * V.	Cooling off :This is the number of days allowed for the cooling off 
 * 					period for freelook cancellation for China
 *
 */

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.productdefinition.screens.Sd5gfScreenVars;
import com.csc.life.productdefinition.tablestructures.Td5gfrec;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5gf extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5GF");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Td5gfrec td5gfrec = new Td5gfrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5gfScreenVars sv = ScreenProgram.getScreenVars( Sd5gfScreenVars.class);
	private final Itemkey wsaaItemkey = new Itemkey();
    private final Desckey wsaaDesckey = new Desckey();
    private Descpf descpf = new Descpf();
    private final DescDAO descDao = getApplicationContext().getBean("descDAO", DescDAO.class);
    private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);	
    private Itempf itempf = new Itempf();
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private String wsaaUpdateFlag = "N";
	private ErrorsInner errorsInner = new ErrorsInner();
    Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
    private static final Logger LOGGER = LoggerFactory.getLogger(Pd5gf.class);


	public Pd5gf() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5gf", AppVars.getInstance(), sv);
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	@Override
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.error("Exception",e);
		}
	}
	@Override
	public void processBo(Object... parmArray) {
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
	
			try {
				processBoMainline(sv, sv.dataArea, parmArray);
			} catch (COBOLExitProgramException e) {
				 LOGGER.error("Exception",e);
			}
	}
	@Override
	protected void initialise1000()
	{
		initialise1010();	
		readRecord1020();
		moveToScreen1030();
		generalArea1040();
	}

	protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		td5gfrec.td5gfRec.set(SPACES);
		
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		
		itempf = itempfDAO.findItemByItdm(wsaaItemkey.itemItemcoy.toString(), wsaaItemkey.itemItemtabl.toString(),
				wsaaItemkey.itemItemitem.toString());

		if (itempf == null ) {
			fatalError600();
		}
	}//End initialise1010()
    
	protected void readRecord1020() {

		descpf = descDao.getdescData(wsaaDesckey.descDescpfx.toString(), wsaaDesckey.descDesctabl.toString(),
			wsaaDesckey.descDescitem.toString().trim(), wsaaDesckey.descDesccoy.toString(),
			wsaaDesckey.descLanguage.toString());

		if (descpf == null) {
			fatalError600();
		}

    }//End readRecord1020()
	
	 protected void moveToScreen1030() {
			sv.company.set(wsaaItemkey.itemItemcoy);
			sv.tabl.set(wsaaItemkey.itemItemtabl);
			sv.item.set(wsaaItemkey.itemItemitem);
			sv.longdesc.set(descpf.getLongdesc());
			td5gfrec.td5gfRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}//End of moveToScreen1030()
	 
     protected void generalArea1040() {
		sv.ratebas.set(td5gfrec.ratebas);
		wsaaSub1.set(0);
		sv.gender01.set(td5gfrec.gender01);
		sv.gender02.set(td5gfrec.gender02);
		sv.gender03.set(td5gfrec.gender03);
		sv.gender04.set(td5gfrec.gender04);
		
		sv.frmage01.set(td5gfrec.frmage01);
		sv.frmage02.set(td5gfrec.frmage02);
		sv.frmage03.set(td5gfrec.frmage03);
		sv.frmage04.set(td5gfrec.frmage04);
		
		sv.toage01.set(td5gfrec.toage01);
		sv.toage02.set(td5gfrec.toage02);
		sv.toage03.set(td5gfrec.toage03);
		sv.toage04.set(td5gfrec.toage04);
		
		sv.coolingoff1.set(td5gfrec.coolingoff1);
		sv.coolingoff2.set(td5gfrec.coolingoff2);
		sv.coolingoff3.set(td5gfrec.coolingoff3);
		sv.coolingoff4.set(td5gfrec.coolingoff4);
		
		if(isEQ(wsspcomn.flag, "C")){
			sv.isCreateMode=true;
			wsaaSub1.set(0);
			for (int loopVar1 = 0; loopVar1 != 4; loopVar1 += 1){
				wsaaSub1.add(1);
				td5gfrec.gnder[wsaaSub1.toInt()].set(ZERO);
				sv.gnder[wsaaSub1.toInt()].set(SPACES);
			}
			
			wsaaSub1.set(0);
			for (int loopVar1 = 0; loopVar1 != 4; loopVar1 += 1){
				wsaaSub1.add(1);
				td5gfrec.frmage[wsaaSub1.toInt()].set(ZERO);
				sv.frmage[wsaaSub1.toInt()].set(SPACES);
			}
			wsaaSub1.set(0);
			for (int loopVar1 = 0; loopVar1 != 4; loopVar1 += 1){
				wsaaSub1.add(1);
				td5gfrec.toage[wsaaSub1.toInt()].set(ZERO);
				sv.toage[wsaaSub1.toInt()].set(SPACES);
			}
			wsaaSub1.set(0);
			for (int loopVar1 = 0; loopVar1 != 4; loopVar1 += 1){
				wsaaSub1.add(1);
				td5gfrec.coolingoff[wsaaSub1.toInt()].set(ZERO);
				sv.coolingoff[wsaaSub1.toInt()].set(SPACES);
			}
		}    
	}//End of generalArea1040()
	
	@Override
    protected void preScreenEdit() {
		try {
			preStart();
		} catch (final COBOLExitProgramException e) {
			LOGGER.error("Exception",e);
		}
    }

    protected void preStart() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(Varcom.prot);
		}
    }	
	
	@Override
    protected void screenEdit2000() {
		validateFields2010();
		exit2020();
    }
	protected void validateFields2010()
	{
			if (isEQ(wsspcomn.flag, "I")) {
				exit2020() ;
			}
			/*Validation for Gender Fields with valid values as 'M' or 'F'*/
			if(!((sv.gender01.toString().equals("M")) || (sv.gender01.toString().equals("F"))) && isNE(sv.gender01,SPACES))
			{
				sv.gender01Err.set(errorsInner.rrc3);
			}
			if(!((sv.gender02.toString().equals("M")) || (sv.gender02.toString().equals("F"))) && isNE(sv.gender02,SPACES)) 
			{
				sv.gender02Err.set(errorsInner.rrc3);
			}
			if(!((sv.gender03.toString().equals("M")) || (sv.gender03.toString().equals("F"))) && isNE(sv.gender03,SPACES)) 
			{
				sv.gender03Err.set(errorsInner.rrc3);
			}
			if(!((sv.gender04.toString().equals("M")) || (sv.gender04.toString().equals("F"))) && isNE(sv.gender04,SPACES))
			{
				sv.gender04Err.set(errorsInner.rrc3);
			}
			if(isEQ(sv.ratebas,SPACES))
			{
				sv.ratebasErr.set(errorsInner.rrcq);
			}
			else if(isNE(sv.ratebas,1) && isNE(sv.ratebas,2) && isNE(sv.ratebas,3) && isNE(sv.ratebas,4) )
			{
				sv.ratebasErr.set(errorsInner.rrcp);
			}
		wsspcomn.edterror.set(varcom.oK);
	}

	protected void exit2020()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
				wsspcomn.edterror.set("Y");
		}
	}
	@Override
	protected void update3000() {
		preparation3010();
		updatePrimaryRecord3020();
		updateRecord3030();
    }
	protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ratebas, td5gfrec.ratebas)) {
			td5gfrec.ratebas.set(sv.ratebas);
			wsaaUpdateFlag = "Y";
		}
		checkChanges3011();
		if (isNE(wsaaUpdateFlag,"Y")) {
			exit2020();
		}		
	}
			
	 protected void checkChanges3011() 
	 {
		
		wsaaSub1.set(0);
		for (int loopVar1 = 0; loopVar1 != 4; loopVar1 += 1){
			wsaaSub1.add(1);
			if (isNE(sv.gnder[wsaaSub1.toInt()],td5gfrec.gnder[wsaaSub1.toInt()])) {
				td5gfrec.gnder[wsaaSub1.toInt()].set(sv.gnder[wsaaSub1.toInt()]);
				wsaaUpdateFlag = "Y";
			}
		}
		
		if (isNE(sv.frmage01, td5gfrec.frmage01)) {
			td5gfrec.frmage01.set(sv.frmage01);
			wsaaUpdateFlag = "Y";
		}

		if (isNE(sv.frmage02, td5gfrec.frmage02)) {
			td5gfrec.frmage02.set(sv.frmage02);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.frmage03, td5gfrec.frmage03)) {
			td5gfrec.frmage03.set(sv.frmage03);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.frmage04, td5gfrec.frmage04)) {
			td5gfrec.frmage04.set(sv.frmage04);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.toage01, td5gfrec.toage01)) {
			td5gfrec.toage01.set(sv.frmage01);
			wsaaUpdateFlag = "Y";
		}

		if (isNE(sv.toage02, td5gfrec.toage02)) {
			td5gfrec.toage02.set(sv.toage02);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.toage03, td5gfrec.toage03)) {
			td5gfrec.toage03.set(sv.toage03);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.toage04, td5gfrec.toage04)) {
			td5gfrec.toage04.set(sv.toage04);
			wsaaUpdateFlag = "Y";
		}
		
		if (isNE(sv.coolingoff1,td5gfrec.coolingoff1)) {
			td5gfrec.coolingoff1.set(sv.coolingoff1);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.coolingoff2,td5gfrec.coolingoff2)) {
			td5gfrec.coolingoff2.set(sv.coolingoff2);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.coolingoff3,td5gfrec.coolingoff3)) {
			td5gfrec.coolingoff3.set(sv.coolingoff3);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.coolingoff4,td5gfrec.coolingoff4)) {
			td5gfrec.coolingoff4.set(sv.coolingoff4);
			wsaaUpdateFlag = "Y";
		}
    }	
	protected void updatePrimaryRecord3020() {
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
    }	
	protected void updateRecord3030() {
		itempf.setTranid(varcom.vrcmCompTranid.trim());
		itempf.setTableprog(wsaaProg.trim());
		td5gfrec.ratebas.set(sv.ratebas);
		td5gfrec.gender01.set(sv.gender01);
		td5gfrec.gender02.set(sv.gender02);
		td5gfrec.gender03.set(sv.gender03);
		td5gfrec.gender04.set(sv.gender04);
		td5gfrec.frmage01.set(sv.frmage01);
		td5gfrec.frmage02.set(sv.frmage02);
		td5gfrec.frmage03.set(sv.frmage03);
		td5gfrec.frmage04.set(sv.frmage04);
		td5gfrec.toage01.set(sv.toage01);
		td5gfrec.toage02.set(sv.toage02);
		td5gfrec.toage03.set(sv.toage03);
		td5gfrec.toage04.set(sv.toage04);
		td5gfrec.coolingoff1.set(sv.coolingoff1);
		td5gfrec.coolingoff2.set(sv.coolingoff2);
		td5gfrec.coolingoff3.set(sv.coolingoff3);
		td5gfrec.coolingoff4.set(sv.coolingoff4);
		
		itempf.setGenarea(td5gfrec.td5gfRec.toString().getBytes());
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
    }	
	@Override
	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
	private static final class ErrorsInner {
		private FixedLengthStringData rrc3 = new FixedLengthStringData(4).init("RRC3");
		private FixedLengthStringData rrcp = new FixedLengthStringData(4).init("RRCP");
		private FixedLengthStringData rrcq = new FixedLengthStringData(4).init("RRCQ");
	}
}
