package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5585screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5585ScreenVars sv = (S5585ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5585screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5585ScreenVars screenVars = (S5585ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.sexageadj.setClassString("");
		screenVars.hghlowage.setClassString("");
		screenVars.agedif01.setClassString("");
		screenVars.addage01.setClassString("");
		screenVars.agedif02.setClassString("");
		screenVars.addage02.setClassString("");
		screenVars.agelimit01.setClassString("");
		screenVars.ageadj01.setClassString("");
		screenVars.agedif03.setClassString("");
		screenVars.addage03.setClassString("");
		screenVars.agedif04.setClassString("");
		screenVars.addage04.setClassString("");
		screenVars.agelimit02.setClassString("");
		screenVars.ageadj02.setClassString("");
		screenVars.agedif05.setClassString("");
		screenVars.addage05.setClassString("");
		screenVars.agedif06.setClassString("");
		screenVars.addage06.setClassString("");
		screenVars.agelimit03.setClassString("");
		screenVars.ageadj03.setClassString("");
		screenVars.agedif07.setClassString("");
		screenVars.addage07.setClassString("");
		screenVars.agedif08.setClassString("");
		screenVars.addage08.setClassString("");
		screenVars.agelimit04.setClassString("");
		screenVars.ageadj04.setClassString("");
		screenVars.agedif09.setClassString("");
		screenVars.addage09.setClassString("");
		screenVars.agedif10.setClassString("");
		screenVars.addage10.setClassString("");
		screenVars.agelimit05.setClassString("");
		screenVars.ageadj05.setClassString("");
		screenVars.agedif11.setClassString("");
		screenVars.addage11.setClassString("");
		screenVars.agedif12.setClassString("");
		screenVars.addage12.setClassString("");
		screenVars.agelimit06.setClassString("");
		screenVars.ageadj06.setClassString("");
		screenVars.agedif13.setClassString("");
		screenVars.addage13.setClassString("");
		screenVars.agedif14.setClassString("");
		screenVars.addage14.setClassString("");
		screenVars.agelimit07.setClassString("");
		screenVars.ageadj07.setClassString("");
		screenVars.agedif15.setClassString("");
		screenVars.addage15.setClassString("");
		screenVars.agedif16.setClassString("");
		screenVars.addage16.setClassString("");
		screenVars.agelimit08.setClassString("");
		screenVars.ageadj08.setClassString("");
		screenVars.agedif17.setClassString("");
		screenVars.addage17.setClassString("");
		screenVars.agedif18.setClassString("");
		screenVars.addage18.setClassString("");
		screenVars.agelimit09.setClassString("");
		screenVars.ageadj09.setClassString("");
	}

/**
 * Clear all the variables in S5585screen
 */
	public static void clear(VarModel pv) {
		S5585ScreenVars screenVars = (S5585ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.sexageadj.clear();
		screenVars.hghlowage.clear();
		screenVars.agedif01.clear();
		screenVars.addage01.clear();
		screenVars.agedif02.clear();
		screenVars.addage02.clear();
		screenVars.agelimit01.clear();
		screenVars.ageadj01.clear();
		screenVars.agedif03.clear();
		screenVars.addage03.clear();
		screenVars.agedif04.clear();
		screenVars.addage04.clear();
		screenVars.agelimit02.clear();
		screenVars.ageadj02.clear();
		screenVars.agedif05.clear();
		screenVars.addage05.clear();
		screenVars.agedif06.clear();
		screenVars.addage06.clear();
		screenVars.agelimit03.clear();
		screenVars.ageadj03.clear();
		screenVars.agedif07.clear();
		screenVars.addage07.clear();
		screenVars.agedif08.clear();
		screenVars.addage08.clear();
		screenVars.agelimit04.clear();
		screenVars.ageadj04.clear();
		screenVars.agedif09.clear();
		screenVars.addage09.clear();
		screenVars.agedif10.clear();
		screenVars.addage10.clear();
		screenVars.agelimit05.clear();
		screenVars.ageadj05.clear();
		screenVars.agedif11.clear();
		screenVars.addage11.clear();
		screenVars.agedif12.clear();
		screenVars.addage12.clear();
		screenVars.agelimit06.clear();
		screenVars.ageadj06.clear();
		screenVars.agedif13.clear();
		screenVars.addage13.clear();
		screenVars.agedif14.clear();
		screenVars.addage14.clear();
		screenVars.agelimit07.clear();
		screenVars.ageadj07.clear();
		screenVars.agedif15.clear();
		screenVars.addage15.clear();
		screenVars.agedif16.clear();
		screenVars.addage16.clear();
		screenVars.agelimit08.clear();
		screenVars.ageadj08.clear();
		screenVars.agedif17.clear();
		screenVars.addage17.clear();
		screenVars.agedif18.clear();
		screenVars.addage18.clear();
		screenVars.agelimit09.clear();
		screenVars.ageadj09.clear();
	}
}
