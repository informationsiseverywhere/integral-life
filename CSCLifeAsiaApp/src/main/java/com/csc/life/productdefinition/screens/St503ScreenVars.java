package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for ST503
 * @version 1.0 generated on 30/08/09 07:25
 * @author Quipoz
 */
public class St503ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(882);
	public FixedLengthStringData dataFields = new FixedLengthStringData(258).isAPartOf(dataArea, 0);
	public FixedLengthStringData bvolumes = new FixedLengthStringData(96).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] bvolume = ZDArrayPartOfStructure(16, 6, 0, bvolumes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(96).isAPartOf(bvolumes, 0, FILLER_REDEFINE);
	public ZonedDecimalData bvolume01 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData bvolume02 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData bvolume03 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData bvolume04 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData bvolume05 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData bvolume06 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData bvolume07 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,36);
	public ZonedDecimalData bvolume08 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,42);
	public ZonedDecimalData bvolume09 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,48);
	public ZonedDecimalData bvolume10 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,54);
	public ZonedDecimalData bvolume11 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,60);
	public ZonedDecimalData bvolume12 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,66);
	public ZonedDecimalData bvolume13 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,72);
	public ZonedDecimalData bvolume14 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,78);
	public ZonedDecimalData bvolume15 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,84);
	public ZonedDecimalData bvolume16 = DD.bvolume.copyToZonedDecimal().isAPartOf(filler,90);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData insprms = new FixedLengthStringData(96).isAPartOf(dataFields, 97);
	public ZonedDecimalData[] insprm = ZDArrayPartOfStructure(16, 6, 0, insprms, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(96).isAPartOf(insprms, 0, FILLER_REDEFINE);
	public ZonedDecimalData insprm01 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData insprm02 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData insprm03 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData insprm04 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData insprm05 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData insprm06 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData insprm07 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,36);
	public ZonedDecimalData insprm08 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,42);
	public ZonedDecimalData insprm09 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,48);
	public ZonedDecimalData insprm10 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,54);
	public ZonedDecimalData insprm11 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,60);
	public ZonedDecimalData insprm12 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,66);
	public ZonedDecimalData insprm13 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,72);
	public ZonedDecimalData insprm14 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,78);
	public ZonedDecimalData insprm15 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,84);
	public ZonedDecimalData insprm16 = DD.insprm.copyToZonedDecimal().isAPartOf(filler1,90);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,193);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,201);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,209);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,217);
	public ZonedDecimalData riskunit = DD.riskunit.copyToZonedDecimal().isAPartOf(dataFields,247);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,253);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 258);
	public FixedLengthStringData bvolumesErr = new FixedLengthStringData(64).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] bvolumeErr = FLSArrayPartOfStructure(16, 4, bvolumesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(64).isAPartOf(bvolumesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bvolume01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData bvolume02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData bvolume03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData bvolume04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData bvolume05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData bvolume06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData bvolume07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData bvolume08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData bvolume09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData bvolume10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData bvolume11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData bvolume12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData bvolume13Err = new FixedLengthStringData(4).isAPartOf(filler2, 48);
	public FixedLengthStringData bvolume14Err = new FixedLengthStringData(4).isAPartOf(filler2, 52);
	public FixedLengthStringData bvolume15Err = new FixedLengthStringData(4).isAPartOf(filler2, 56);
	public FixedLengthStringData bvolume16Err = new FixedLengthStringData(4).isAPartOf(filler2, 60);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData insprmsErr = new FixedLengthStringData(64).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] insprmErr = FLSArrayPartOfStructure(16, 4, insprmsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(64).isAPartOf(insprmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData insprm01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData insprm02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData insprm03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData insprm04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData insprm05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData insprm06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData insprm07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData insprm08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData insprm09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData insprm10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData insprm11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData insprm12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData insprm13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData insprm14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData insprm15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData insprm16Err = new FixedLengthStringData(4).isAPartOf(filler3, 60);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData riskunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(468).isAPartOf(dataArea, 414);
	public FixedLengthStringData bvolumesOut = new FixedLengthStringData(192).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] bvolumeOut = FLSArrayPartOfStructure(16, 12, bvolumesOut, 0);
	public FixedLengthStringData[][] bvolumeO = FLSDArrayPartOfArrayStructure(12, 1, bvolumeOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(192).isAPartOf(bvolumesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bvolume01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] bvolume02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] bvolume03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] bvolume04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] bvolume05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] bvolume06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] bvolume07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] bvolume08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] bvolume09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] bvolume10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] bvolume11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] bvolume12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] bvolume13Out = FLSArrayPartOfStructure(12, 1, filler4, 144);
	public FixedLengthStringData[] bvolume14Out = FLSArrayPartOfStructure(12, 1, filler4, 156);
	public FixedLengthStringData[] bvolume15Out = FLSArrayPartOfStructure(12, 1, filler4, 168);
	public FixedLengthStringData[] bvolume16Out = FLSArrayPartOfStructure(12, 1, filler4, 180);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData insprmsOut = new FixedLengthStringData(192).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] insprmOut = FLSArrayPartOfStructure(16, 12, insprmsOut, 0);
	public FixedLengthStringData[][] insprmO = FLSDArrayPartOfArrayStructure(12, 1, insprmOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(192).isAPartOf(insprmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] insprm01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] insprm02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] insprm03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] insprm04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] insprm05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] insprm06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] insprm07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] insprm08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] insprm09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] insprm10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] insprm11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] insprm12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] insprm13Out = FLSArrayPartOfStructure(12, 1, filler5, 144);
	public FixedLengthStringData[] insprm14Out = FLSArrayPartOfStructure(12, 1, filler5, 156);
	public FixedLengthStringData[] insprm15Out = FLSArrayPartOfStructure(12, 1, filler5, 168);
	public FixedLengthStringData[] insprm16Out = FLSArrayPartOfStructure(12, 1, filler5, 180);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] riskunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData St503screenWritten = new LongData(0);
	public LongData St503protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public St503ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, riskunit, bvolume01, insprm01, bvolume02, insprm02, bvolume03, insprm03, bvolume04, insprm04, bvolume05, insprm05, bvolume06, insprm06, bvolume07, insprm07, bvolume08, insprm08, bvolume09, insprm09, bvolume10, insprm10, bvolume11, insprm11, bvolume12, insprm12, bvolume13, insprm13, bvolume14, insprm14, bvolume15, insprm15, bvolume16, insprm16};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, riskunitOut, bvolume01Out, insprm01Out, bvolume02Out, insprm02Out, bvolume03Out, insprm03Out, bvolume04Out, insprm04Out, bvolume05Out, insprm05Out, bvolume06Out, insprm06Out, bvolume07Out, insprm07Out, bvolume08Out, insprm08Out, bvolume09Out, insprm09Out, bvolume10Out, insprm10Out, bvolume11Out, insprm11Out, bvolume12Out, insprm12Out, bvolume13Out, insprm13Out, bvolume14Out, insprm14Out, bvolume15Out, insprm15Out, bvolume16Out, insprm16Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, riskunitErr, bvolume01Err, insprm01Err, bvolume02Err, insprm02Err, bvolume03Err, insprm03Err, bvolume04Err, insprm04Err, bvolume05Err, insprm05Err, bvolume06Err, insprm06Err, bvolume07Err, insprm07Err, bvolume08Err, insprm08Err, bvolume09Err, insprm09Err, bvolume10Err, insprm10Err, bvolume11Err, insprm11Err, bvolume12Err, insprm12Err, bvolume13Err, insprm13Err, bvolume14Err, insprm14Err, bvolume15Err, insprm15Err, bvolume16Err, insprm16Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = St503screen.class;
		protectRecord = St503protect.class;
	}

}
