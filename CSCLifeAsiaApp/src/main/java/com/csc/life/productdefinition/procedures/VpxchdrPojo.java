package com.csc.life.productdefinition.procedures;

public class VpxchdrPojo {

	public String function;
  	public String statuz;
  	public String copybook;
  	public String result;
  	public String resultField;
  	public String resultValue;
  	public String dataRec;
  	public int occdate;
  	public String billchnl;
  	public String cownnum;
  	public String jownnum;
  	public String cnttype;
  	public String rstaflag;
  	
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getCopybook() {
		return copybook;
	}
	public void setCopybook(String copybook) {
		this.copybook = copybook;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultField() {
		return resultField;
	}
	public void setResultField(String resultField) {
		this.resultField = resultField;
	}
	public String getResultValue() {
		return resultValue;
	}
	public void setResultValue(String resultValue) {
		this.resultValue = resultValue;
	}
	public String getDataRec() {
		return dataRec;
	}
	public void setDataRec(String dataRec) {
		this.dataRec = dataRec;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getJownnum() {
		return jownnum;
	}
	public void setJownnum(String jownnum) {
		this.jownnum = jownnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getRstaflag() {
		return rstaflag;
	}
	public void setRstaflag(String rstaflag) {
		this.rstaflag = rstaflag;
	}
  	
  	
}
