/*
 * File: Vlcmptrm.java
 * Date: 30 August 2009 2:53:40
 * Author: Quipoz Limited
 * 
 * Class transformed from VLCMPTRM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51irec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*  Subroutine VLPDSA02.
*  ===================
*
*    This subroutine will be processed to validate Risk/Prem Term
*    against Table TR51I
*    by check following,
*       Risk/Prem Term of component1 can't __ (Risk/Prem Term
*                                              of Component 2)
*
*     - Read table TR51I, using VLSB-RID1-CRTABLE + VLSB-RID2-CRTABLE as
*     keys
*
*
***********************************************************************
* </pre>
*/
public class Vlcmptrm extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VLCMPTRM";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaVlsbSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaErrDetail = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaInputAllSpace = new FixedLengthStringData(1);
	private int wsaaMaxInput = 50;
		/* WSAA-RID1-FIELDS */
	private FixedLengthStringData wsaaRid1Crtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRid1Rtrm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaRid1Ptrm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
		/* WSAA-RID2-FIELDS */
	private FixedLengthStringData wsaaRid2Crtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRid2Rtrm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaRid2Ptrm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaTr51iItmkey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr51iCompnt1 = new FixedLengthStringData(4).isAPartOf(wsaaTr51iItmkey, 0).init(SPACES);
	private FixedLengthStringData wsaaTr51iCompnt2 = new FixedLengthStringData(4).isAPartOf(wsaaTr51iItmkey, 4).init(SPACES);
		/* FORMATS */
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String tr51i = "TR51I";
		/* ERRORS */
	private String rlat = "RLAT";
	private String rlau = "RLAU";
	private String rlav = "RLAV";
	private String rlaw = "RLAW";
	private String rlax = "RLAX";
	private String rlay = "RLAY";
	private String e984 = "E984";
	private String rgal = "RGAL";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Tr51irec tr51irec = new Tr51irec();
	private Varcom varcom = new Varcom();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit190, 
		exit390, 
		exit690
	}

	public Vlcmptrm() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		try {
			begin010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		if ((isEQ(vlpdsubrec.rid1Life,SPACES)
		&& isEQ(vlpdsubrec.rid1Jlife,SPACES)
		&& isEQ(vlpdsubrec.rid1Coverage,SPACES)
		&& isEQ(vlpdsubrec.rid1Rider,SPACES)
		&& isEQ(vlpdsubrec.rid1Crtable,SPACES))
		|| (isEQ(vlpdsubrec.rid2Life,SPACES)
		&& isEQ(vlpdsubrec.rid2Jlife,SPACES)
		&& isEQ(vlpdsubrec.rid2Coverage,SPACES)
		&& isEQ(vlpdsubrec.rid2Rider,SPACES)
		&& isEQ(vlpdsubrec.rid2Crtable,SPACES))) {
			goTo(GotoLabel.exit090);
		}
		readTables100();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit090);
		}
		wsaaErrCode.set(SPACES);
		wsaaErrSeq.set(1);
		validation200();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTables100()
	{
		try {
			begin110();
		}
		catch (GOTOException e){
		}
	}

protected void begin110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51i);
		wsaaTr51iCompnt1.set(vlpdsubrec.rid1Crtable);
		wsaaTr51iCompnt2.set(vlpdsubrec.rid2Crtable);
		itemIO.setItemitem(wsaaTr51iItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
		tr51irec.tr51iRec.set(itemIO.getGenarea());
	}

protected void validation200()
	{
		begin210();
	}

protected void begin210()
	{
		wsaaRid1Crtable.set(SPACES);
		wsaaRid2Crtable.set(SPACES);
		wsaaInputAllSpace.set(SPACES);
		for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq,wsaaMaxInput)
		|| isEQ(wsaaInputAllSpace,"Y")
		|| (isNE(wsaaRid1Crtable,SPACES)
		&& isNE(wsaaRid2Crtable,SPACES))); wsaaVlsbSeq.add(1)){
			chkComponent300();
		}
		if (isEQ(wsaaRid1Crtable,SPACES)
		|| isEQ(wsaaRid2Crtable,SPACES)) {
			syserrrec.statuz.set(rgal);
			fatalError600();
		}
		if (isEQ(tr51irec.indc01,"EQ")){
			if (isNE(wsaaRid1Rtrm,wsaaRid2Rtrm)) {
				wsaaErrCode.set(rlau);
			}
		}
		else if (isEQ(tr51irec.indc01,"LE")){
			if (isGT(wsaaRid1Rtrm,wsaaRid2Rtrm)) {
				wsaaErrCode.set(rlat);
			}
		}
		else if (isEQ(tr51irec.indc01,"LT")){
			if (isGTE(wsaaRid1Rtrm,wsaaRid2Rtrm)) {
				wsaaErrCode.set(rlav);
			}
		}
		else{
			wsaaErrCode.set(e984);
		}
		if (isNE(wsaaErrCode,SPACES)) {
			wsaaErrDetail.set(SPACES);
			moveError400();
		}
		if (isEQ(tr51irec.indc02,"EQ")){
			if (isNE(wsaaRid1Ptrm,wsaaRid2Ptrm)) {
				wsaaErrCode.set(rlax);
			}
		}
		else if (isEQ(tr51irec.indc02,"LE")){
			if (isGT(wsaaRid1Ptrm,wsaaRid2Ptrm)) {
				wsaaErrCode.set(rlaw);
			}
		}
		else if (isEQ(tr51irec.indc02,"LT")){
			if (isGTE(wsaaRid1Ptrm,wsaaRid2Ptrm)) {
				wsaaErrCode.set(rlay);
			}
		}
		else{
			wsaaErrCode.set(e984);
		}
		if (isNE(wsaaErrCode,SPACES)) {
			wsaaErrDetail.set(SPACES);
			moveError400();
		}
	}

protected void chkComponent300()
	{
		try {
			begin310();
		}
		catch (GOTOException e){
		}
	}

protected void begin310()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()],SPACES)
		&& isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()],SPACES)) {
			wsaaInputAllSpace.set("Y");
			goTo(GotoLabel.exit390);
		}
		if (isEQ(wsaaRid1Crtable,SPACES)) {
			if (isEQ(vlpdsubrec.rid1Life,vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Jlife,vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Coverage,vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Rider,vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid1Crtable,vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()])) {
				wsaaRid1Crtable.set(vlpdsubrec.rid1Crtable);
				wsaaRid1Rtrm.set(vlpdsubrec.inputRtrm[wsaaVlsbSeq.toInt()]);
				wsaaRid1Ptrm.set(vlpdsubrec.inputPtrm[wsaaVlsbSeq.toInt()]);
			}
		}
		if (isEQ(wsaaRid2Crtable,SPACES)) {
			if (isEQ(vlpdsubrec.rid2Life,vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Jlife,vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Coverage,vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Rider,vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()])
			&& isEQ(vlpdsubrec.rid2Crtable,vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()])) {
				wsaaRid2Crtable.set(vlpdsubrec.rid2Crtable);
				wsaaRid2Rtrm.set(vlpdsubrec.inputRtrm[wsaaVlsbSeq.toInt()]);
				wsaaRid2Ptrm.set(vlpdsubrec.inputPtrm[wsaaVlsbSeq.toInt()]);
			}
		}
	}

protected void moveError400()
	{
		/*BEGIN*/
		vlpdsubrec.errCode[wsaaErrSeq.toInt()].set(wsaaErrCode);
		vlpdsubrec.errDet[wsaaErrSeq.toInt()].set(wsaaErrDetail);
		wsaaErrSeq.add(1);
		/*EXIT*/
	}

protected void fatalError600()
	{
		try {
			fatalErrors610();
		}
		catch (GOTOException e){
		}
		finally{
			exit690();
		}
	}

protected void fatalErrors610()
	{
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit690()
	{
		exitProgram();
	}
}
