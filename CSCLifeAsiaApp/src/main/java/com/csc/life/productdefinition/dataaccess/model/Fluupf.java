package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

// sqlfluppf1 = " SELECT  CHDRCOY, CHDRNUM, FUPNO, TRANNO, FUPTYP, LIFE, JLIFE, FUPCDE, FUPSTS, FUPDT, UNIQUE_NUMBER " +
public class Fluupf 
{
	
	private long uniqueNumber;	
	private String chdrcoy;
	private String chdrnum;
	private String fupno;
	private long tranno;
	private String fuptyp;
	private String life;
	private String jlife;
	private String  fupcde;  
	private String  fupsts;
	private long  fupdt;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	
	public long getUniqueNumber()
	{
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) 
	{
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() 
	{
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy)
	{
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum()
	{
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) 
	{
		this.chdrnum = chdrnum;
	}
	public String getFupno() 
	{
		return fupno;
	}
	public void setFupno(String fupno)
	{
		this.fupno = fupno;
	}
	public String getFuptyp()
	{
		return fuptyp;
	}
	public void setFuptyp(String fuptyp)
	{
		this.fuptyp = fuptyp;
	}
	 
	public String getLife() 
	{
		return life;
	}
	public void setLife(String life)
	{
		this.life = life;
	}
	public String getJlife() 
	{
		return jlife;
	}
	public void setJlife(String jlife)
	{
		this.jlife = jlife;
	}
	public String getFupcde()
	{
		return fupcde;
	}
	public void setFupcde(String fupcde) 
	{
		this.fupcde = fupcde;
	}
	public String getFupsts()
	{
		return fupsts;
	}
	public void setFupsts(String fupsts) 
	{
		this.fupsts = fupsts;
	}
	public long getFupdt() 
	{
		return fupdt;
	}
	public void setFupdt(long fupdt) 
	{
		this.fupdt = fupdt;
	}	
	public long getTranno()
	{
		return tranno;
	}
	public void setTranno(long l) 
	{
		this.tranno = l;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}        
	public void clearAll() {
		
		this.uniqueNumber=0;	
		this.chdrcoy="";
		this.chdrnum="";
		this.fupno="";
		this.tranno=0;
		this.fuptyp="";
		this.life="";
		this.jlife="";
		this.fupcde="";  
		this.fupsts="";
		this.fupdt=0;
		
	}
}