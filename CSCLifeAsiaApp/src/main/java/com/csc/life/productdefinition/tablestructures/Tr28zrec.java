package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:52
 * Description:
 * Copybook name: TR28ZREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr28zrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr28zRec = new FixedLengthStringData(500);
  	public FixedLengthStringData dataextract = new FixedLengthStringData(10).isAPartOf(tr28zRec, 0);
  	public FixedLengthStringData function = new FixedLengthStringData(10).isAPartOf(tr28zRec, 10);
  	public FixedLengthStringData copybk = new FixedLengthStringData(12).isAPartOf(tr28zRec, 20);
  	public FixedLengthStringData maptofield = new FixedLengthStringData(35).isAPartOf(tr28zRec, 32);
  	public FixedLengthStringData numoccurs = new FixedLengthStringData(3).isAPartOf(tr28zRec, 67);
  	public FixedLengthStringData filler = new FixedLengthStringData(430).isAPartOf(tr28zRec, 70, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr28zRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr28zRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}