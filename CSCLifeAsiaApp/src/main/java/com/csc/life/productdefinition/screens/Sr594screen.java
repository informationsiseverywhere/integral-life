package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr594screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr594ScreenVars sv = (Sr594ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr594screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr594ScreenVars screenVars = (Sr594ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.uwplntyp01.setClassString("");
		screenVars.undlimit01.setClassString("");
		screenVars.usundlim01.setClassString("");
		screenVars.uwplntyp02.setClassString("");
		screenVars.undlimit02.setClassString("");
		screenVars.usundlim02.setClassString("");
		screenVars.uwplntyp03.setClassString("");
		screenVars.undlimit03.setClassString("");
		screenVars.usundlim03.setClassString("");
		screenVars.uwplntyp04.setClassString("");
		screenVars.undlimit04.setClassString("");
		screenVars.usundlim04.setClassString("");
		screenVars.uwplntyp05.setClassString("");
		screenVars.undlimit05.setClassString("");
		screenVars.usundlim05.setClassString("");
		screenVars.uwplntyp06.setClassString("");
		screenVars.undlimit06.setClassString("");
		screenVars.usundlim06.setClassString("");
		screenVars.uwplntyp07.setClassString("");
		screenVars.undlimit07.setClassString("");
		screenVars.usundlim07.setClassString("");
		screenVars.uwplntyp08.setClassString("");
		screenVars.undlimit08.setClassString("");
		screenVars.usundlim08.setClassString("");
		screenVars.uwplntyp09.setClassString("");
		screenVars.undlimit09.setClassString("");
		screenVars.usundlim09.setClassString("");
	}

/**
 * Clear all the variables in Sr594screen
 */
	public static void clear(VarModel pv) {
		Sr594ScreenVars screenVars = (Sr594ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.uwplntyp01.clear();
		screenVars.undlimit01.clear();
		screenVars.usundlim01.clear();
		screenVars.uwplntyp02.clear();
		screenVars.undlimit02.clear();
		screenVars.usundlim02.clear();
		screenVars.uwplntyp03.clear();
		screenVars.undlimit03.clear();
		screenVars.usundlim03.clear();
		screenVars.uwplntyp04.clear();
		screenVars.undlimit04.clear();
		screenVars.usundlim04.clear();
		screenVars.uwplntyp05.clear();
		screenVars.undlimit05.clear();
		screenVars.usundlim05.clear();
		screenVars.uwplntyp06.clear();
		screenVars.undlimit06.clear();
		screenVars.usundlim06.clear();
		screenVars.uwplntyp07.clear();
		screenVars.undlimit07.clear();
		screenVars.usundlim07.clear();
		screenVars.uwplntyp08.clear();
		screenVars.undlimit08.clear();
		screenVars.usundlim08.clear();
		screenVars.uwplntyp09.clear();
		screenVars.undlimit09.clear();
		screenVars.usundlim09.clear();
	}
}
