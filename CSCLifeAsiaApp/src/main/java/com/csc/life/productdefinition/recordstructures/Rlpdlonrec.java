package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:05
 * Description:
 * Copybook name: RLPDLONREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rlpdlonrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(141);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rec, 5);
  	public FixedLengthStringData processed = new FixedLengthStringData(1).isAPartOf(rec, 9);
  	public FixedLengthStringData validate = new FixedLengthStringData(1).isAPartOf(rec, 10);
  	public FixedLengthStringData pstw = new FixedLengthStringData(5).isAPartOf(rec, 11);
  	public FixedLengthStringData sacstype = new FixedLengthStringData(2).isAPartOf(rec, 16);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rec, 18);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rec, 19);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rec, 27);
  	public PackedDecimalData transeq = new PackedDecimalData(3, 0).isAPartOf(rec, 30);
  	public FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rec, 32);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(rec, 62);
  	public FixedLengthStringData authCode = new FixedLengthStringData(4).isAPartOf(rec, 67);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rec, 71);
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(rec, 72);
  	public PackedDecimalData prmdepst = new PackedDecimalData(15, 2).isAPartOf(rec, 75);
  	public PackedDecimalData loanno = new PackedDecimalData(2, 0).isAPartOf(rec, 83);
  	public FixedLengthStringData loantype = new FixedLengthStringData(1).isAPartOf(rec, 85);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(21).isAPartOf(rec, 86);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batchkey, 0);
  	public FixedLengthStringData prefix = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData actyear = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData actmonth = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData trcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batchkey, 19, FILLER);
  	public FixedLengthStringData tranid = new FixedLengthStringData(22).isAPartOf(rec, 107);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(tranid, 0);
  	public ZonedDecimalData tranidN = new ZonedDecimalData(18, 0).isAPartOf(tranid, 4).setUnsigned();
  	public ZonedDecimalData tranidX = new ZonedDecimalData(18, 0).isAPartOf(tranidN, 0, REDEFINE);
  	public ZonedDecimalData date_var = new ZonedDecimalData(6, 0).isAPartOf(tranidX, 0).setUnsigned();
  	public ZonedDecimalData time = new ZonedDecimalData(6, 0).isAPartOf(tranidX, 6).setUnsigned();
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(tranidX, 12).setUnsigned();
  	public FixedLengthStringData doctkey = new FixedLengthStringData(12).isAPartOf(rec, 129);
  	public FixedLengthStringData doctPrefix = new FixedLengthStringData(2).isAPartOf(doctkey, 0);
  	public FixedLengthStringData doctCompany = new FixedLengthStringData(1).isAPartOf(doctkey, 2);
  	public FixedLengthStringData doctNumber = new FixedLengthStringData(9).isAPartOf(doctkey, 3);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}